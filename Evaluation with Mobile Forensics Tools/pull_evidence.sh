#   .pull_evidence.sh pixel4 <dev_id>

instrumented_device=$2
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
location=$1_$current_time

mkdir -p $location/data_extracted_msg_send

python3 ./scripts_for_generating_traffic/whatsapp_backup.py $instrumented_device
sleep 5

adb -s $instrumented_device shell dumpsys > $location/data_extracted_msg_send/dumpsys.txt
adb -s $instrumented_device logcat -d -v time >> $location/data_extracted_msg_send/logcat.txt

adb -s $instrumented_device pull -a /sdcard/jitmflogs $location/data_extracted_msg_send/jitmflogs  
# adb -s $instrumented_device pull -a /storage/emulated/0/Android/data/com.whatsapp/files/jitmflogs $location/data_extracted_msg_send/jitmflogs # FOR ANDROID 11
adb -s $instrumented_device pull -a /sdcard/WhatsApp $location/data_extracted_msg_send/ 

cat $location/data_extracted_msg_send/jitmflogs/*.jitmflog > $location/data_extracted_msg_send/jitmflogs/merged-logs.jitmflog

awk '!seen[$0]++' $location/data_extracted_msg_send/jitmflogs/merged-logs.jitmflog > $location/data_extracted_msg_send/jitmflogs/merged-logs_uniq.jitmflog

input_file=$location/data_extracted_msg_send/jitmflogs/merged-logs_uniq.jitmflog
output_file=$location/data_extracted_msg_send/jitmflogs/merged-logs_uniq_b64d.jitmflog

while IFS= read -r line
do
    var2=`echo "$line" | jq -r .object`
    time=`echo "$line" | jq -r .time_of_message_event`

    strs=`echo $var2 |  base64 -d | strings`
    readarray -t msg_array <<<"$strs"
    
    number=`echo "$strs" | grep "@s.whatsapp.netsr" | uniq | tr -dc '0-9'"\n"`

    text=`echo "${msg_array[-1]}"`

    str=`echo $line | jq 'del(.object)'`

    evidence_object='{"evidence_object": {"text": '$text', "number": '$number', "time_sent":'$time'}}'

    str=`echo $str | jq -c --arg text "$text" --arg number "$number" --arg time "$time" --arg object "$var2" '. + {evidence_object: {"text": $text, "number": $number, "time_sent": $time, "original_object": $object }}'`

    echo "$str" >> "$output_file"
done < "$input_file"


adb -s $instrumented_device shell pm uninstall -k com.whatsapp
adb -s $instrumented_device install /mnt/2TBdrive/gitlab/experiment_3/apps/instrumented/com.whatsapp_2.21.14.25_arm64-v8a.jitmf_debuggable.apk
sleep 5
adb -s $instrumented_device shell run-as com.whatsapp cp -r /data/data/com.whatsapp /sdcard/ && adb -s $instrumented_device pull -a /sdcard/com.whatsapp $location/data_extracted_msg_send/com.whatsapp

adb -s $instrumented_device backup -apk -shared -all -f $location/data_extracted_msg_send/backup.ab
