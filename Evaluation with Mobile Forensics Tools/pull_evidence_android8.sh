#!/bin/bash
device=$1
location=$2

path='/data/data/com.whatsapp/'

for i in $(adb -s $device shell run-as com.whatsapp find $path -type f); do
    RELATIVE_PATH=${i:24}
    DIR="$(dirname "$RELATIVE_PATH")"
    FILE="$(basename "${RELATIVE_PATH}")"
    mkdir -p $location/$DIR
    adb -d shell "run-as com.whatsapp cat '$i'" > $location/$DIR/$FILE    
done;


# space in name makes it more difficult to extract
mkdir -p "$location/files/WhatsApp Images"
adb -d shell "run-as com.whatsapp cat '/data/data/com.whatsapp/files/WhatsApp Images/.nomedia'" > "$location/files/WhatsApp Images/.nomedia"
mkdir -p "$location/files/WhatsApp Video"
adb -d shell "run-as com.whatsapp cat '/data/data/com.whatsapp/files/WhatsApp Images/.nomedia'" > "$location/files/WhatsApp Video/.nomedia"
