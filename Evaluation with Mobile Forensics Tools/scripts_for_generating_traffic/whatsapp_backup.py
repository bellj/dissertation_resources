import re
import sys
import os

from com.dtmilano.android.viewclient import ViewClient
from com.dtmilano.android.adb.adbclient import AdbClient

_s = 3

device = sys.argv[1]
sys.argv = [sys.argv[0]]

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity(component='com.whatsapp/com.whatsapp.Main')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.dump(window=-1)

vc.findViewWithContentDescriptionOrRaise(u'''More options''').touch()
vc.dump(window=-1)
vc.findViewWithTextOrRaise(u'Settings').touch()
vc.dump(window=-1)
# menuitem_delete.touch()
vc.findViewWithTextOrRaise(u'Chats').touch()
vc.dump(window=-1)
vc.findViewWithTextOrRaise(u'Chat backup', root=vc.findViewByIdOrRaise('id/no_id/28')).touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.findViewWithTextOrRaise(u'BACK UP').touch()
vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)

device.shell('input keyevent KEYCODE_BACK')