#!/bin/bash
## To get timings - OFFLINE
# echo "OffLINE - time"
# full_logs="./results/offline*"
# for parent_dir in $full_logs
# do  
#     test=$parent_dir/*
#     for log in $test
#     do  
#         dir_name=`basename ${log}`
#         scenario=$(echo $dir_name | cut -d_ -f1)
#         category=$(echo $dir_name | cut -d_ -f2)
#         tp=$(echo $dir_name | cut -d_ -f3)
        
#         time_sent=$(tail -n 1 $log/"send_log.log" | cut -d, -f1)
#         log_dir="$log/telegram_jitmflogs"

#         if [[ $scenario == *"spying"* ]]; then
#             count_files=`ls -1 $log_dir/"received_log.log" 2>/dev/null | wc -l`
#         else
#             count_files=`ls -1 $log/"received_log.log" 2>/dev/null | wc -l`
#         fi
#         if [[ $count_files != 0 ]]; then
#             if [[ $scenario == *"spying"* ]]; then
#                 time_received=$(head -n 1 $log_dir/"received_log.log" | cut -d, -f1)
#             else
#                 time_received=$(head -n 1 $log/"received_log.log" | cut -d, -f1)
#             fi
#         fi
#         DIFF=$((time_received-time_sent))
#         echo "telegram,$scenario,$tp,$category,$DIFF"
#     done
#     echo ""
# done
# echo "ONLINE-time"
## To get timings - ONLINE
# full_logs="./results/online*"
# for parent_dir in $full_logs
# do  
#     test=$parent_dir/*
#     for log in $test
#     do  
#         dir_name=`basename ${log}`
#         scenario=$(echo $dir_name | cut -d_ -f1)
#         category=$(echo $dir_name | cut -d_ -f2)
#         tp=$(echo $dir_name | cut -d_ -f3)
        
#         time_sent=$(tail -n 1 $log/"send_log.log" | cut -d, -f1)
#         log_dir="$log/telegram_jitmflogs"

#         if [[ $scenario == *"spying"* ]]; then
#             count_files=`ls -1 $log_dir/"received_log.log" 2>/dev/null | wc -l`
#         else
#             count_files=`ls -1 $log/"received_log.log" 2>/dev/null | wc -l`
#         fi
#         if [[ $count_files != 0 ]]; then
#             if [[ $scenario == *"spying"* ]]; then
#                 time_received=$(head -n 1 $log_dir/"received_log.log" | cut -d, -f1)
#             else
#                 time_received=$(head -n 1 $log/"received_log.log" | cut -d, -f1)
#             fi
#         fi
#         DIFF=$((time_received-time_sent))
#         echo "telegram,$scenario,$tp,$category,$DIFF"
#     done
#     echo ""
# done
# echo "OffLINE-storage"
# # get size of hprofs 
# full_logs="./results/offline*"
# for parent_dir in $full_logs
# do  
#     test=$parent_dir/*
#     for log in $test
#     do  
#         dir_name=`basename ${log}`
#         scenario=$(echo $dir_name | cut -d_ -f1)
#         category=$(echo $dir_name | cut -d_ -f2)
#         tp=$(echo $dir_name | cut -d_ -f3)
        
#         log_dir="$log/telegram_jitmflogs"

#         full_dump_size=$(find $log_dir -type f \( -iname \*.jitmfheaplog -o -iname \*.hprof \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )
     
#         echo "$scenario|$category|$tp|telegram|"${full_dump_size:-0}""
#     done
#     echo ""
# done
# echo "OnLINE-storage"
# # get size of jitmflogs 
# full_logs="./results/online*"
# for parent_dir in $full_logs
# do  
#     test=$parent_dir/*
#     for log in $test
#     do  
#         dir_name=`basename ${log}`
#         scenario=$(echo $dir_name | cut -d_ -f1)
#         category=$(echo $dir_name | cut -d_ -f2)
#         tp=$(echo $dir_name | cut -d_ -f3)
        
#         log_dir="$log/telegram_jitmflogs"

#         filtered_dump_size=$(find $log_dir -type f \( -iname \*.jitmflog \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )
     
#         echo "$scenario|$category|$tp|telegram|"${filtered_dump_size:-0}""
#     done
#     echo ""
# done

