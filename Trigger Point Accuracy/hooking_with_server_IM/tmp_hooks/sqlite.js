'use strict';




//libsqlite3.dylib




// var sqlite3_open = Module.findExportByName('libsqlite.so ', 'sqlite3_open');



// Interceptor.attach(Module.findExportByName('libsqlite.so', 'sqlite3_prepare'), {
//     onEnter: function(args) {
//         console.log('DB: ' + Memory.readUtf16String(args[0]) + '\tSQL: ' + Memory.readUtf16String(args[1]));
//     }
// });



// 'use strict'; // libsqlite3.dylib 

// var sqlite3_prepare_v2 = Module.findExportByName('libsqlite.so', 'sqlite3_prepare_v2'); 
// Interceptor.attach(sqlite3_prepare_v2, {         
//     onEnter: function(args) { 
//         console.log('SQL: ');         
//     } 
// });

const trace = require('frida-trace');

const func = trace.func;
const argIn = trace.argIn;
const argOut = trace.argOut;
const retval = trace.retval;

const types = trace.types;
const pointer = types.pointer;
const INT = types.INT;
const POINTER = types.POINTER;
const UTF8 = types.UTF8;

trace({
  module: 'libsqlite3.dylib',
  functions: [
    func('sqlite3_open', retval(INT), [
      argIn('filename', UTF8),
      argOut('ppDb', pointer(POINTER), when('result', isZero)),
    ]),
    func('sqlite3_prepare_v2', retval(INT), [
      argIn('db', POINTER),
      argIn('zSql', [UTF8, bind('length', 'nByte')]),
      argIn('nByte', INT),
      argOut('ppStmt', pointer(POINTER), when('result', isZero)),
    ])
  ],
  callbacks: {
    onEvent(event) {
      console.log('onEvent! ' + JSON.stringify(event, null, 2));
    },
    onEnter(event, context) {
      event.trace = Thread.backtrace(context)
        .map(DebugSymbol.fromAddress)
        .filter(x => x.name);
    },
    onError(e) {
      console.error(e);
    }
  }
});

function isZero(value) {
  return value === 0;
}