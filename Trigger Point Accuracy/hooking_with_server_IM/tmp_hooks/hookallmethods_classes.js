var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";


function getTime()
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    return dateTime
}

function trace(pattern)
{
	var type = (pattern.toString().indexOf("!") === -1) ? "java" : "module";

	if (type === "module") {
		var res = new ApiResolver("module");
		var matches = res.enumerateMatchesSync(pattern);
		var targets = uniqBy(matches, JSON.stringify);
		targets.forEach(function(target) {
			traceModule(target.address, target.name);
		});

	} else if (type === "java") {
		var found = false;
		Java.enumerateLoadedClasses({
			onMatch: function(aClass) {

				if (aClass.startsWith(pattern)){
					console.log(aClass)
					
					traceClass(aClass);
				}
               
			},
			onComplete: function() {}
		});
		if (!found) {
			try {
				traceMethod(pattern);
			}
			catch(err) {}
		}
	}
}

function traceMethod(targetClassMethod)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
    try{
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){}
    console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
    Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {
            var out = '{"time": '+getTime()+', "method": '+targetClassMethod+','
			Java.perform(function() {
				console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
			});
			for (var j = 0; j < arguments.length; j++) {
                var term_char = ', '
                var arg = arguments[j];
                
				out += '"arg[' + j + ']": ' + arguments[j]+term_char
			}

			var retval = this[targetMethod].apply(this, arguments);
			out += '"retval": ' + retval +'}\n'
			console.error(out);
			Log.v(TAG_L, out);
			return retval;
		}
	}
}

function uniqBy(array, key)
{
        var seen = {};
        return array.filter(function(item) {
                var k = key(item);
                return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        });
}

function traceClass(targetClass) {
    var hook = Java.use(targetClass);
	var methods = hook.class.getDeclaredMethods();
	var parsedMethods = [];
	methods.forEach(function(method) {
		parsedMethods.push(method.toString().replace(targetClass + ".", "TOKEN").match(/\sTOKEN(.*)\(/)[1]);
	});

	var targets = uniqBy(parsedMethods, JSON.stringify);
	targets.forEach(function(targetMethod) {
		try{
		traceMethod(targetClass + "." + targetMethod);
		}
		catch(err){}
	});
}


setTimeout(function () {

Java.perform(function(){
		
	// trace('org.telegram.SQLite.SQLitePreparedStatement.prepare') //receive
	trace('org.telegram.messenger.MessagesController.processLoadedMessages')
	// trace('org.telegram.messenger.GcmPushListenerService')
	// trace('org.telegram.tgnet.ConnectionsManager') //send
// 	java.lang.Exception
// 	at org.telegram.messenger.SendMessagesHelper.sendMessage(Native Method)
// 	at org.telegram.ui.Components.ChatActivityEnterView.processSendingText(ChatActivityEnterView.java:3831)
// 	at org.telegram.ui.Components.ChatActivityEnterView.sendMessageInternal(ChatActivityEnterView.java:3796)
// 	at org.telegram.ui.Components.ChatActivityEnterView.sendMessage(ChatActivityEnterView.java:3740)
// 	at org.telegram.ui.Components.ChatActivityEnterView.lambda$new$14$ChatActivityEnterView(ChatActivityEnterView.java:2705)
// 	at org.telegram.ui.Components.-$$Lambda$ChatActivityEnterView$rXJQBzkQ-6_4ly_28Plp5SExQgU.onClick(Unknown Source:2)
// 	at android.view.View.performClick(View.java:7125)
// 	at android.view.View.performClickInternal(View.java:7102)
// 	at android.view.View.access$3500(View.java:801)
// 	at android.view.View$PerformClick.run(View.java:27336)
// 	at android.os.Handler.handleCallback(Handler.java:883)
// 	at android.os.Handler.dispatchMessage(Handler.java:100)
// 	at android.os.Looper.loop(Looper.java:214)
// 	at android.app.ActivityThread.main(ActivityThread.java:7356)
// 	at java.lang.reflect.Method.invoke(Native Method)
// 	at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:492)
// 	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:930)

// java.lang.Exception
// 	at org.telegram.messenger.SendMessagesHelper.sendMessage(Native Method)
// 	at org.telegram.messenger.SendMessagesHelper.sendMessage(SendMessagesHelper.java:2350)
// 	at org.telegram.messenger.SendMessagesHelper.sendMessage(Native Method)
// 	at org.telegram.ui.Components.ChatActivityEnterView.processSendingText(ChatActivityEnterView.java:3831)
// 	at org.telegram.ui.Components.ChatActivityEnterView.sendMessageInternal(ChatActivityEnterView.java:3796)
// 	at org.telegram.ui.Components.ChatActivityEnterView.sendMessage(ChatActivityEnterView.java:3740)
// 	at org.telegram.ui.Components.ChatActivityEnterView.lambda$new$14$ChatActivityEnterView(ChatActivityEnterView.java:2705)
// 	at org.telegram.ui.Components.-$$Lambda$ChatActivityEnterView$rXJQBzkQ-6_4ly_28Plp5SExQgU.onClick(Unknown Source:2)
// 	at android.view.View.performClick(View.java:7125)
// 	at android.view.View.performClickInternal(View.java:7102)
// 	at android.view.View.access$3500(View.java:801)
// 	at android.view.View$PerformClick.run(View.java:27336)
// 	at android.os.Handler.handleCallback(Handler.java:883)
// 	at android.os.Handler.dispatchMessage(Handler.java:100)
// 	at android.os.Looper.loop(Looper.java:214)
// 	at android.app.ActivityThread.main(ActivityThread.java:7356)
// 	at java.lang.reflect.Method.invoke(Native Method)
// 	at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:492)
// 	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:930)

// trace('org.telegram.ui.Cells.ChatMessageCell.getMessageObject') //send





	// trace('org.telegram.messenger') //to test
	

// trace('org.telegram.ui.ChatActivity$ChatActivityAdapter.updateRowWithMessageObject') 

// trace('org.telegram.messenger.SmsReceiver')
// trace('org.telegram.messenger.PopupReplyReceiver')
// trace('org.telegram.messenger.MessagesStorage.putMessages')
// trace('com.google.firebase.iid.FirebaseInstanceIdReceiver')
// trace('com.google.firebase.iid.FirebaseInstanceIdReceiver')
// trace('org.telegram.messenger.GcmPushListenerService')
// trace('com.google.firebase.messaging.FirebaseMessagingService')
// trace('org.telegram.messenger.AutoMessageReplyReceiver')

// trace('org.telegram.messenger.MessagesStorage')
// trace('org.telegram.messenger.MessageObject')


// trace('org.telegram.messenger.AutoMessageReplyReceiver')
	// trace('org.telegram.messenger.NotificationsController.addToPopupMessages')  //receive - with obj in arg


})
}, 0);