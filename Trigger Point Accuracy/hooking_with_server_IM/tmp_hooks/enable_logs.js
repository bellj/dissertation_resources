


// function getMessageObjects(targetClassMethod) {
// 	var time = getTime()
	
// 	var klass = "org.telegram.messenger.MessageObject"
// 	Java.choose(klass, {
// 		onMatch: function (instance) {
			
// 			var object = parseObject(instance)
// 			if(object != ''){
// 				jitmfLog += '{"time": '+time+', "method": '+targetClassMethod+', "object": '
// 				jitmfLog += object+'}\n'
// 			}
	
// 		}, onComplete: function () {
// 			complete = true
// 		}
// 	});
// }

function traceMethod(targetClassMethod) {
	var delim = targetClassMethod.lastIndexOf(".");
	var hookLog = ''
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)
	var hook = Java.use(targetClass);
	var overloadCount = 0
	try {
		overloadCount = hook[targetMethod].overloads.length;
	} catch (err) { }

	for (var i = 0; i < overloadCount; i++) {
		hook[targetMethod].overloads[i].implementation = function () {			
				hookLog = ''
				jitmfHeapLog = ''
				jitmfLog = ''
            var retval = this[targetMethod].apply(this, arguments);	
            var FL = Java.use('org.telegram.messenger.FileLog') 
            console.log(FL.initied.value)
			return retval;
		}
	}
}

setTimeout(function () {
    if (Java.available){
        Java.perform(function(){
            var BuildVars = Java.use('org.telegram.messenger.BuildVars')                
            BuildVars.DEBUG_VERSION.value = true;
            BuildVars.LOGS_ENABLED.value = true;
            console.log('test')
        })
        traceMethod('org.telegram.messenger.FileLog.init')
    }

}, 0);