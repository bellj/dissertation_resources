var LOG_DIR = "telegram_jitmflogs";
function getTime(file) {
	var time = Math.round((new Date()).getTime() / 1000)
	return time.toString()
}

function getUnixTime(){
	return Math.round(new Date().getTime()/1000);
}

function getExternalStorageDirectory() {
	const Env = Java.use("android.os.Environment")
	var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
	return external_storage_dir
}

function putInFile(fileName, contents) {
	var file = new File(fileName, "a+");
	file.write(contents)
	file.close()
}

function traceReceiveMethod(targetClassMethod) {
	console.log("uhiouhouihouih")
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;
	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)
	var hook = Java.use(targetClass);
	var overloadCount = 0

	var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/received_log.log"
	// var hookLogName = getExternalStorageDirectory() +"/received_log.log"
	try {
		overloadCount = hook[targetMethod].overloads.length;
	} catch (err) { }

	for (var i = 0; i < overloadCount; i++) {
		hook[targetMethod].overloads[i].implementation = function () {
			var time = getTime()
			var hookLog = getUnixTime() + ', Messages populated\n'
			putInFile(hookLogName, hookLog)
			var retval = this[targetMethod].apply(this, arguments);
			return retval;
		}
	}
}

setTimeout(function () {

	Java.perform(function () {
		traceReceiveMethod('org.telegram.messenger.MessagesController.processLoadedMessages') //android.intent.action.BADGE_COUNT_UPDATE
	})
}, 0);