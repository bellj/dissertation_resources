#!/usr/bin/env monkeyrunner
# Copyright 2010, The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import subprocess
import signal
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
import datetime

# sets a variable with the package's internal name
# sets a variable with the package's internal name
package = 'org.telegram.messenger'

# sets a variable with the name of an Activity in the package
activity = 'org.telegram.ui.LaunchActivity'

# sets the name of the component to start
runComponent = package + '/' + activity

#vars for use in screen comparison later
REF = 'reference.png'
SCR = 'screenshot.png'
CMP = 'comparison.png'

#the acceptance threshold for image comparison
ACCEPTANCE = 0.9

# The format of the file we are parsing is very carfeully constructed.
# Each line corresponds to a single command.  The line is split into 2
# parts with a | character.  Text to the left of the pipe denotes
# which command to run.  The text to the right of the pipe is a python
# dictionary (it can be evaled into existence) that specifies the
# arguments for the command.  In most cases, this directly maps to the
# keyword argument dictionary that could be passed to the underlying
# command. 

# Lookup table to map command strings to functions that implement that
# command.
CMD_MAP = {
    'TOUCH': lambda dev, arg: dev.touch(**arg),
    'DRAG': lambda dev, arg: dev.drag(**arg),
    'PRESS': lambda dev, arg: dev.press(**arg),
    'TYPE': lambda dev, arg: dev.type(**arg),
    'WAIT': lambda dev, arg: MonkeyRunner.sleep(**arg)
    }

# Process a single file for the specified device.
def process_file(fp, device):
    i=0
    for line in fp:
        i+=1
        (cmd, rest) = line.split('|')
        try:
            # Parse the pydict
            rest = eval(rest)
        except:
            print 'unable to parse options'
            continue

        if cmd not in CMD_MAP:
            print 'unknown command: ' + cmd
            continue

        CMD_MAP[cmd](device, rest)

def exitGracefully(signum, frame):
    print "Exiting Gracefully..."
    subprocess.call(['./kill_monkey.sh'])
    sys.exit(1)
 
 
def log(fn, device, current_time):
    f_log = open(fn, 'a')

    f_log.write("Wrote msg: "+current_time.strftime("%d/%m/%Y, %H:%M:%S"))
    f_log.close()

def main():
    file = sys.argv[1]
    file2 = sys.argv[2]
    fp = open(file, 'r')
     
    device = MonkeyRunner.waitForConnection()
	
    # # Runs the component
    device.startActivity(component=runComponent)
    MonkeyRunner.sleep(2)
	
    process_file(fp, device)
    current_time = datetime.datetime.now()  
    MonkeyRunner.sleep(1)
    log(file2+'/monkey_runs.log', device, current_time) # Write logs')
	
    # result = device.takeSnapshot()
    # result.writeToFile('shot2.png','png')
    # reference = MonkeyImage.loadFromFile('reference.png')
    # if not screenshot.sameAs(reference, ACCEPTANCE):
    #    print "comparison failed, getting visual comparison..."
    #    subprocess.call(["/usr/bin/compare", REF, SCR, CMP])
    # fp.close()
    

if __name__ == '__main__':
    signal.signal(signal.SIGINT, exitGracefully)
    main()
