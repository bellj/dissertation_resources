# open chat, click on text box, type in text and send
# run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" .
text=$(echo "${2}" | sed "s:%s: :g")
/root/Android/Sdk/platform-tools/adb -s $3 shell input tap 864 518
/root/Android/Sdk/platform-tools/adb -s $3 shell input tap 886 2694
sleep 2
/root/Android/Sdk/platform-tools/adb -s $3 shell input keyboard text "$2"
/root/Android/Sdk/platform-tools/adb -s $3 shell input tap 1359 1726
sleep 2
/root/Android/Sdk/platform-tools/adb -s $3 shell input tap 301 2867
echo "$(date +%s),$text" >> $1/send_log.log
