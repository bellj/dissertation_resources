var LOG_DIR = "telegram_jitmflogs";
var APP = "telegram";
var SCENARIO = "cp";
var TYPE = "device_file"
var MONITOR_CLASS = "JITMFFileSizeEvent";

var jitmfHeapLog = ''
var jitmfObjsLog = ''
var jitmfLog = ''

function getTime(file) {
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	if (file) {
		dateTime = date + '_' + time;
	}
	return dateTime
}

function getExternalStorageDirectory() {
	const Env = Java.use("android.os.Environment")
	var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
	return external_storage_dir
}

function putInFile(fileName, contents, mode) {
	mode = typeof mode !== 'undefined' ? mode : 'a+';
	var file = new File(fileName, mode);
	file.write(contents)
	file.close()
}

function dumpHeap(targetClassMethod) {
	var memdump_filename = APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".hprof"
	jitmfHeapLog += '{"time": ' + getTime() + ', "method": ' + targetClassMethod + ', "object": [' + memdump_filename + ']}\n'
	const Debug = Java.use("android.os.Debug")
	Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

setImmediate(function () {
	Java.perform(function () {
		var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".hooklog"
		var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmfheaplog"
		var jitmfObjsLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmfObjslog"
		var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmflog"
		var found = true

		Interceptor.attach(Module.getExportByName('libc.so', 'open'), {
			// onEnter: function (args) {
			// 	var fd = args[0].toInt32();

			// 	if (Socket.type(fd) === null)
			// 		found = true
			// 	else
			// 		return
			// },
			onLeave: function (args) {
				if (found) {
					console.log("found")
					var monitor = null;
					try {
						const ActivityThread = Java.use("android.app.ActivityThread")
						var application = ActivityThread.currentApplication();
						if (application !== null) {
							var dir = application.getApplicationContext().getFilesDir().getParent().toString();
							var full_class_name = application.getApplicationContext().getPackageName().toString() + '.' + MONITOR_CLASS
							Java.perform(function () {
								var exists = false;
								Java.choose(full_class_name, {
									"onMatch": function (instance) {
										monitor = instance
										exists = true
									},
									"onComplete": function () {
										if (!exists) {
											var a = Java.use(full_class_name);
											Java.scheduleOnMainThread(function () {
												monitor = a.$new();
											});
										}
										if (monitor !== null) {
											var res = parseInt(monitor.monitorDirSize(dir))
											if (res > 0) {
												var time = getTime()
												var hookLog = time + ',' + SCENARIO + ',' + TYPE + '\n'
												dumpHeap(SCENARIO)
												putInFile(hookLogName, hookLog)
												putInFile(jitmfHeapLogName, jitmfHeapLog)
												getMessageObjects(SCENARIO)
												if (jitmfLog != '') {
													putInFile(jitmfLogName, jitmfLog)
												}
												jitmfHeapLog = ''
												jitmfLog = ''
											}
										}
									}
								});
							});
						}
					} catch (err) { }
				}
            }
        });
    });
});