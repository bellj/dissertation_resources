import frida
import sys
import time
import subprocess 
import os
import signal
from datetime import datetime

test_name=""


hook_list=[
'cp|3rdparty|com.google.android.gms.gcm.GcmReceiver.onReceive|com.pushbullet.android:background',
'cp|androidsp|android.telephony.SmsManager.sendTextMessage|com.pushbullet.android',
'cp|androidsp|android.content.ContentResolver.insert|com.pushbullet.android',
'cp|appsp|com.pushbullet.android.providers.syncables.SyncablesProvider.insert|com.pushbullet.android',
'cp|appsp|com.pushbullet.android.sms.SmsSentReceiver.onReceive|com.pushbullet.android:background',
'cp|appsp|com.pushbullet.android.sms.SmsSyncService.a|com.pushbullet.android:background',
'cp|native|write|com.pushbullet.android:background|disk', #disk!!!
'cp|native|read|com.pushbullet.android:background',
'cp|device|network|com.pushbullet.android:background|read',
'cp|device|file|com.pushbullet.android:background|write',
'spying|3rdparty|com.google.android.gms.gcm.GcmReceiver.onReceive|com.pushbullet.android:background',
'spying|androidsp|android.content.ContentResolver.registerContentObserver|com.pushbullet.android:background',
'spying|appsp|com.pushbullet.android.gcm.GcmService.a|com.pushbullet.android',
'spying|appsp|com.pushbullet.android.sms.SmsSyncService.a|com.pushbullet.android:background',
'spying|native|write|com.pushbullet.android:background',
'spying|native|read|com.pushbullet.android:background|disk', #disk!!!
'spying|device|network|com.pushbullet.android:background|write',
'spying|device|file|com.pushbullet.android:background|write' #(write to file)
]

filename="hook_template.js"
native_filename="hook_native.js" 
 
hook_invoke=''
deviceUT='emulator-5554'
outfile = open('stdout','w')

#not working
def check_frida():
    try:
        subprocess.check_output(['frida-ps', '-D', 'emulator-5554'])
    except subprocess.CalledProcessError:
        subprocess.Popen(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s', 'emulator-5554', 'shell', '"/data/local/tmp/frida-server &"'], stdout=outfile, stderr=outfile)

if len(sys.argv) > 1:
    counter = int(sys.argv[1])
    test_name =  str(sys.argv[2])
else:
    counter=0

for count in range(0,10):
    for hook in hook_list:
        now = datetime.now()
        print("** hook: "+hook+" ** start: "+ now.strftime("%m/%d/%Y, %H:%M:%S")+" **")
    
        counter+=1
        var=hook.split('|')
        scenario = var[0]
        category = var[1]
        method = var[2]
        procname=var[3]

        if scenario=="spying":
            message="Receiving"
        else:
            message="Sending"
        
        dir_name='results/'+test_name+str(count)+'/'+scenario+"_"+category+"_"+method
        subprocess.call(['mkdir', '-p', dir_name])
        scriptname=dir_name+"/hook.js"
        
        subprocess.call(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s',deviceUT,'shell', 'am', 'force-stop',procname])
        time.sleep(2)
        subprocess.call(['sudo','android_scripts/launch_app.sh', deviceUT])
        hook_invoke = 'hook_template.py'
        
        if category =='native':
            disk=''
            if len(var)==5:
                native_filename="hook_native_file.js"
            with open(native_filename, "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category+"_"+method).replace('[FUNCTION]', method))
        elif (category =='device' and method!="bcast_event"):
            with open("hook_"+category+"_"+method+".js", "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        if method=="file":
                            fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category+"_"+method).replace('[FUNCTION]', var[4])) 
                        else:
                            fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category+"_"+method).replace('[FUNCTION]', var[4]).replace('"[SENDING]"', "true" if scenario == 'spying' else "false"))
        else:
            with open(filename, "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        if method=="bcast_event":
                            fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category).replace('[TRACE_METHOD]', var[4]))
                        else:
                            fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category).replace('[TRACE_METHOD]', method))


        print("*** CALLING PYTHON *** >>> "+scenario+"  |  "+category+"  |  "+method)

        p = subprocess.Popen(['python3', hook_invoke, scriptname, procname],  stdout=outfile, stderr=outfile) 
        time.sleep(5)
        messageToSend = message+str(counter)

        print("*** CALLING *** >>> `sudo ./post_exec.sh "+scenario+" "+dir_name+" "+messageToSend+" "+deviceUT+"`")
        subprocess.call(['sudo','./post_exec.sh', scenario, dir_name, messageToSend, deviceUT])
    
        pid = p.pid
        os.kill(pid, signal.SIGINT)

        time.sleep(5)

        now = datetime.now()
        print("end: ", now.strftime("%m/%d/%Y, %H:%M:%S"))
    print("end round "+str(count)+". Delete messages")