#!/bin/bash

path="./results/both_*"

echo "Phase 1"
for file in $(find $path -name '*.hprof')
do
# echo $file
  strings "$file" > "${file}.strings"
done

sleep 3
echo "Phase 2"
for file in $(find $path -name '*.hprof.strings')
do
	FILEPATH_NOEXT=${file%.*.*}
	IFS='_' read -r -a array <<< `basename ${file}`;
	scenario=${array[1]}
	if [[ $scenario == *"spying"* ]]; then
		grep -ri '{"type":"push","targets":\["stream","android","ios"],"push":{"type":"sms_changed"' $file > $FILEPATH_NOEXT.dataObjs
		grep -ri '{"key":.*"type":"sms".*"body":.*}' $file >> $FILEPATH_NOEXT.dataObjs
		grep -ri '{"type":"push","push":{"type":"sms_changed","source_device_iden":.*}}' $file >> $FILEPATH_NOEXT.dataObjs
	else
		grep -ri '{"active":.*"message":.*}}' $file >> $FILEPATH_NOEXT.dataObjs
		grep -ri '{"type":"push","targets":\["stream","android","ios"],"push":{"conversation_iden":' $file >> $FILEPATH_NOEXT.dataObjs
		grep -ri '{"key":.*"type":"sms".*"body":.*}' $file >> $FILEPATH_NOEXT.dataObjs
		grep -ri '{"type":"push","push":{"type":"sms_changed","source_device_iden":.*}}' $file >> $FILEPATH_NOEXT.dataObjs
	fi
	# rm -rf $file
done

sleep 3
echo "Phase 3"
for file in $(find $path -name '*.jitmfheaplog')
do
	dir=$(dirname "${file}")
	while IFS= read -r line
	do
		filename=$(grep -oP '\[\K.*?(?=])' <<< "$line")
		prepend=$(echo $line | cut -d[ -f1)
		while IFS= read -r line2
		do
		echo $prepend$line2} >> ${file%.jitmfheaplog}.filtered_heap
		done < $dir/"${filename%.hprof}.dataObjs"

	done < "$file"
done
echo "Finished"