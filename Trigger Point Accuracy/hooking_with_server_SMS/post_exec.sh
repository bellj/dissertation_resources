#!/bin/bash
/root/Android/Sdk/platform-tools/adb -s $4 shell rm -rf /sdcard/pushbullet_jitmflogs/*

if [[ $1 == *"spying"* ]]; then
    ./android_scripts/receive_sms.sh $2 $3 #simulate msg receive
else
    # via curl
    # ./android_scripts/send_curl_sms.sh $2 $3
    # via selenium
    sudo -u jennifer python3 ./android_scripts/selenium_pb_send.py $3
    echo "$(date +%s),${3}," >> $2/send_log.log
fi

now=$(date +"%Y-%m-%d_%T")
/root/Android/Sdk/platform-tools/adb -s $4 logcat -d > $2/logcat_${now}.txt

sleep 5
/root/Android/Sdk/platform-tools/adb -s $4 pull /sdcard/pushbullet_jitmflogs $2
/root/Android/Sdk/platform-tools/adb -s $4 shell rm -rf /sdcard/pushbullet_jitmflogs/*
sleep 5
/root/Android/Sdk/platform-tools/adb -s $4 pull /sdcard/pushbullet_jitmflogs $2
/root/Android/Sdk/platform-tools/adb -s $4 shell rm -rf /sdcard/pushbullet_jitmflogs/*
