from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
import sys
import time
executor_url = "http://127.0.0.1:48131"
session_id = "9efd40f8-76d5-4e6e-8de7-d17dc4cb80ba"

def attach_to_session(executor_url, session_id):
    original_execute = WebDriver.execute
    def new_command_execute(self, command, params=None):
        if command == "newSession":
            # Mock the response
            return {'success': 0, 'value': None, 'sessionId': session_id}
        else:
            return original_execute(self, command, params)
    # Patch the function before creating the driver object
    WebDriver.execute = new_command_execute
    driver = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
    driver.session_id = session_id
    # Replace the patched function with original function
    WebDriver.execute = original_execute
    return driver

bro = attach_to_session(executor_url, session_id)
bro.get("https://www.pushbullet.com/#sms")
time.sleep(2)
bro.find_element(By.ID, "message").click()
bro.find_element(By.ID, "message").send_keys(sys.argv[1])
time.sleep(2)
bro.find_element(By.ID, "send-button").click()
time.sleep(6)