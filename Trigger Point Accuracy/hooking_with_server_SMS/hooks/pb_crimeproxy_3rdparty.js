var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "3rdparty";


// function getTime(file)
// {
//     var today = new Date();
//     var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
//     var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
// 	var dateTime = date+' '+time;
// 	if(file){
// 		dateTime = date+'_'+time;
// 	}
//     return dateTime
// }

function traceMethod(targetClassMethod, value)
{
	// var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	// if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
	
	try {
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){ }
	
	console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	// Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	
	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {

			// var out = '{"time": '+getTime()+', "method": '+targetClassMethod+','

			// var publish = false
			
			// if (typeof value === 'undefined'){
			// 	const Env = Java.use("android.os.Environment") 
			// 	const Debug = Java.use("android.os.Debug")
			// 	var dir = Env.getExternalStorageDirectory().getAbsolutePath() 
				// Debug.dumpHprofData(dir+"/"+APP+"_"+targetClassMethod+"_"+SCENARIO+"_"+TYPE+"_"+getTime(true)+".hprof");   

			// } else if (value != -1){
			// 	publish = true
			// 	out += '"data_obj":'+arguments[value]+','
			// }

			var retval = this[targetMethod].apply(this, arguments);
			console.log('DAHAL')

			// if (value === -1){
			// 	publish = true
			// 	out += '"data_obj":'+retval+'}\n'
			// }

			// if(publish){
			// 	console.error(out);
			// 	Log.v(TAG_L, out);
			// }
			return retval;
		}
	}
}

setTimeout(function () {
	Java.perform(function(){
		traceMethod('com.google.firebase.iid.FirebaseInstanceIdReceiver.onReceive') // grep -ri '{"iden":'
		// traceMethod('com.google.android.gms.gcm.GcmReceiver.onReceive') // grep -ri '{"iden":'
	})
}, 0);