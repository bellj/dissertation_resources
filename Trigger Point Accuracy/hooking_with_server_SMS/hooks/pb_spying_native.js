var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "native";

var list_a = []

// function getTime(file)
// {
//     var today = new Date();
//     var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
//     var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//     var dateTime = date+' '+time;
//     if(file){
// 		dateTime = date+'_'+time;
// 	}
// 	return dateTime
// }

// REQUIRES TO BE HOOKED ON START -> THAT IS WHEN THE SOCKET IS CREATED

Interceptor.attach(Module.findExportByName('libc.so', 'socket'), {
	onLeave: function (retval) {
		var fd = retval.toInt32();

		if (fd !== -1) {
			console.log("socket " + fd.toString())
			console.log(fd)
			list_a.push(fd)
			console.log(lista)
		}
	}
});


['write', 'send', 'sendto'].forEach(function (name) {
	Interceptor.attach(Module.getExportByName('libc.so', name), {
		onEnter: function (args) {

			// var out = '{"time": '+getTime()+', "method": libc_open, '
			var fd = args[0].toInt32();
			if (list_a.indexOf(fd) > -1) {
				console.log("Writing to PB socket: "+name + ' ' + fd.toString())
			}
		}
	});
});

