var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "appsp";

function getTime(file)
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;
	if(file){
		dateTime = date+'_'+time;
	}
    return dateTime
}

function traceMethod(targetClassMethod, value)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
	
	try {
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){ }
	
	console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	var publish = false
	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {
			var publish = false
			var dbg = '{"time": '+getTime()+', "method": '+targetClassMethod+','
			var out = dbg

			if (value >= 0){
				var dataObj = arguments[value]
				
				if (typeof dataObj != 'undefined'){
					if(targetClassMethod === 'com.pushbullet.android.c.f.a'  || targetClassMethod === 'com.pushbullet.android.providers.syncables.SyncablesProvider.insert'){
						if (dataObj.toString().includes("data={")){
							publish = true
							out += '"data_obj":'+dataObj+'}\n'
						}
					}
					else{
						publish = true
						out += '"data_obj":'+dataObj+'}\n'
					}
				}
				else{
					publish = false
				}
			}	
			var retval = this[targetMethod].apply(this, arguments);
			
			if (value === -1){
				
				var dataObj = retval
				if (targetClassMethod === 'com.pushbullet.android.c.ad.d'){
					dataObj = JSON.stringify(JSON.parse(dataObj.toString()).texts[0]);
				}
				if (typeof dataObj != 'undefined' && dataObj.includes("data")){
					out += '"data_obj":'+dataObj.toString()+'}\n'
					publish = true
				}
				else{
					publish = false
				}
			}
			if(publish){

				var filename = APP+"_"+SCENARIO+"_"+TYPE+".jitmflog";  
				var file = new File("/sdcard/"+filename,"a+");
				console.error(out);
				Log.v(TAG_L, out);
				file.write(out)
				file.close()
			}

			return retval;
		}
	}
}

setTimeout(function () {
	Java.perform(function(){
		// traceMethod('com.pushbullet.android.providers.syncables.SyncablesProvider.insert', 1) //ok
		// traceMethod('com.pushbullet.android.sms.h.a', 1) //ok

	})
}, 0);