var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "native";

function getTime(file)
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    if(file){
		dateTime = date+'_'+time;
	}
	return dateTime
}

Interceptor.attach(Module.getExportByName('libc.so', 'open'), {
	onEnter: function (args) {
		var out = '{"time": '+getTime()+', "method": libc_open, '
		this.flag = false;
		var filename = Memory.readCString(ptr(args[0]));
		out += '"filename": '+filename
		if (filename.toString().includes('com.pushbullet.android/databases/syncables.db-journal')){
			console.log('filename =', filename)
			const Env = Java.use("android.os.Environment") 
			const Debug = Java.use("android.os.Debug")
			var dir = Env.getExternalStorageDirectory().getAbsolutePath() 
			var memdump_filename = dir+"/"+APP+"_"+"open"+"_"+SCENARIO+"_"+TYPE+"_"+getTime(true)+".hprof"

			// Debug.dumpHprofData(memdump_filename)

			out+= ', "data_object": ['+memdump_filename+']}'
			var filename = APP+"_"+SCENARIO+"_"+TYPE+".jitmflog";  
			var file = new File("/sdcard/"+filename,"a+");
			console.log(out)
			file.write(out)
			file.close()
	}

	}
});

