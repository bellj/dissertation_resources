var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "androidsp";

function getTime(file)
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    if(file){
		dateTime = date+'_'+time;
	}
    return dateTime
}

function traceMethod(targetClassMethod, value)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
	
	try {
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){ }
	
	// console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	// Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");

	for (var i = 0; i < overloadCount; i++) {
		hook[targetMethod].overloads[i].implementation = function() {

            // var filename = APP+"_"+SCENARIO+"_"+TYPE+".jitmflog";  
            // var file = new File("/sdcard/"+filename,"a+");
            // var dbg = '{"time": '+getTime()+', "method": '+targetClassMethod+','
            // var out = dbg
            
            var out = '{"time": '+getTime()+', "method": '+targetClassMethod+','
            
            // if (targetClassMethod === 'android.telephony.SmsManager.sendTextMessage'){
            //     out += '"data_obj":{"address": '+arguments[0]+', "message": '+arguments[2]+'}}\n'
            // }
            // else if (targetClassMethod === 'android.content.ContentResolver.insert'){
            //     var dataObj = arguments[value].toString().split(' ')[2]
            //     out += '"data_obj": "'+dataObj+'"}}\n'
            // }
                
            var retval = this[targetMethod].apply(this, arguments);
            
            console.error(out);
            // Log.v(TAG_L, out);
            // file.write(out)
            // file.close()
            return retval;
		}
	}
}

setTimeout(function () {
	Java.perform(function(){
		// traceMethod('android.telephony.SmsManager.sendTextMessage') //message and number
		traceMethod('android.content.ContentResolver.insert', 1)
		
	})
}, 0);