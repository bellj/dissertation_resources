
var ok=false;
var path='';
['fopen', 'open', 'read', 'write'].forEach(function (name) {
    Interceptor.attach(Module.getExportByName('libc.so', name), {
        onEnter: function (args) {
            var fd = args[0].toInt32();

            if (Socket.type(fd) === null) {
                console.log("Not network")
                console.log(name)
                ok=true
                if (name === "read") {
                    var bfr = args[1],
                        sz = args[2].toInt32();
                    path = (TraceSysFD["fd-" + args[0].toInt32()] != null) ? TraceSysFD["fd-" + args[0].toInt32()] : "[unknow path]";

                    console.log("[Libc::read] Read FD (" + path + "," + bfr + "," + sz + ")\n");

                }
                if (name === "open") {
                    path = Memory.readCString(args[0]);
                    console.log(path)
                }
                if (name === "write") {
                    var bfr = args[1],
                        sz = args[2].toInt32();
                    path = (TraceSysFD["fd-" + args[0].toInt32()] != null) ? TraceSysFD["fd-" + args[0].toInt32()] : "[unknow path]";

                    console.log("[Libc::write] Write FD (" + path + "," + bfr + "," + sz )
                }
            }
        },
        onLeave: function(ret) {
            if(ok && name==="open"){
                console.log("[Libc::open] Open file '" + this.path + "' (fd: " + ret.toInt32() + ")");
            }
        }
    });
});