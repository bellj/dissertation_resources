"use strict";

var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var time=0;


function traceMethod(targetClassMethod)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
    try{
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){}
    console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	
	
	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {

	            Java.perform(function() {
					console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
                });
		
            var out = '{"Times": '+(time+1).toString()+', "method": '+targetClassMethod+','
		
			for (var j = 0; j < arguments.length; j++) {
                var term_char = ', '
                var arg = arguments[j];
                
				out += '"arg[' + j + ']": ' + arguments[j]+term_char
			}

			var retval = this[targetMethod].apply(this, arguments);
			out += '"retval": ' + retval +'}\n'
			console.error(out);
			Log.v(TAG_L, out);
			return retval;
		}
	}
}
setTimeout(function () {

Java.perform(function(){
		
// ---------------------   SMS RECV  ----------------------------------------------------
	// python3 hook.py hook_methods.js com.pushbullet.android:background

	    traceMethod('com.google.android.gms.gcm.GcmReceiver.onReceive')
		traceMethod('com.pushbullet.android.sms.SmsObserverService.onStartCommand')
		traceMethod('com.pushbullet.android.sms.SmsObserverService.onCreate')
		// traceMethod('com.pushbullet.android.sms.SmsSyncService.a')
		traceMethod('com.pushbullet.android.sms.SmsSyncService.e')
		// traceMethod('com.pushbullet.android.sms.k.onChange')
		// traceMethod('com.pushbullet.android.sms.l.run')
		traceMethod('com.pushbullet.android.sms.SmsReceiver.onReceive')
		// traceMethod('com.pushbullet.android.base.BaseIntentService.onHandleIntent')

// --------------------------------------------------------------------------------------
	//python3 hook.py hook_methods.js com.pushbullet.android
		
		// traceMethod('com.pushbullet.android.gcm.GcmService.a')
		// traceMethod('android.content.Intent.setAction')
		// traceMethod('android.content.Intent$1.createFromParcel')

// ---------------------   SMS SEND  ----------------------------------------------------
	// python3 hook.py hook_methods.js com.pushbullet.android:background
	    // traceMethod('com.google.android.gms.gcm.GcmReceiver.onReceive')
		// traceMethod('com.pushbullet.android.sms.SmsObserverService.onCreate')
		// traceMethod('com.pushbullet.android.sms.SmsSyncService.a')
		// traceMethod('com.pushbullet.android.sms.k.onChange')
		// traceMethod('com.pushbullet.android.sms.l.run')
		// traceMethod('com.pushbullet.android.sms.SmsSentReceiver.onReceive')
		// traceMethod('com.pushbullet.android.sms.SmsObserverService.onStartCommand')
        

})
}, 0);