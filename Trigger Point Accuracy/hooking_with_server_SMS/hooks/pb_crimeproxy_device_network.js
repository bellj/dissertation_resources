var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var APP = "pushbullet";
var SCENARIO = "crimeproxy";
var TYPE = "device";
var MONITOR_CLASS = "JITMFTrafficObserver";

function getTime(file)
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    if(file){
		dateTime = date+'_'+time;
	}
	return dateTime
}

Interceptor.attach(Module.getExportByName('libc.so', 'write'), {
	onEnter: function (args) {
		// var out = '{"time": '+getTime()+', "method": monitor_file_size_on_close, '
		this.flag = false;
		var monitor = null;
		try {
			// const Env = Java.use("android.os.Environment") 
			const ActivityThread = Java.use("android.app.ActivityThread")
			// const Debug = Java.use("android.os.Debug")
			var application = ActivityThread.currentApplication();
			if (application!==null)
			{
				// var dir = application.getApplicationContext().getFilesDir().getParent().toString();
				// console.log(dir)
				var full_class_name = application.getApplicationContext().getPackageName().toString()+'.'+MONITOR_CLASS
				Java.perform(function(){

					
					var exists=false;
					Java.choose(full_class_name, { 
						"onMatch":function(instance){
							monitor = instance
							exists = true
						},
						"onComplete":function() {
							if (!exists){
								var a=Java.use(full_class_name);
								Java.scheduleOnMainThread(function(){
									monitor=a.$new();
									monitor.start(application.getApplicationContext());
								});
							}
							if (monitor!== null){
								try{
									var recvd = parseInt(monitor.check(false))

									if (recvd >= 5000 && recvd <=8000){
										console.log("Message was sent")
									}
								}
								catch(e){
									
								}
								// console.log(res)
							// 	if (res.toString() === 'true'){
							// 		console.log("KIBER")
							// 		out +=', "data_object": ['+memdump_filename+']}'

							// 		var filename = APP+"_"+SCENARIO+"_"+TYPE+".jitmflog";  
							// 		var file = new File("/sdcard/"+filename,"a+");
									
							// 		var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath() 
							// 		var memdump_filename = external_storage_dir+"/"+APP+"_"+"open"+"_"+SCENARIO+"_"+TYPE+"_"+getTime(true)+".hprof"

							// 		Debug.dumpHprofData(memdump_filename)
							// 		file.write(out)
							// 		file.close()
							// 	}
							// 	else{
							// 		console.log("MA KIBIRX")
							// 	}
							}
						}
					});
				});
			}
		} catch(err){}
	}
});

