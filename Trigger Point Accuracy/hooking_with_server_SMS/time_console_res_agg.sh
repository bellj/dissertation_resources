#!/bin/bash
strings=(
"cp|3rdparty|com.google.android.gms.gcm.GcmReceiver.onReceive|com.pushbullet.android:background"
"cp|androidsp|android.telephony.SmsManager.sendTextMessage|com.pushbullet.android"
"cp|androidsp|android.content.ContentResolver.insert|com.pushbullet.android"
"cp|appsp|com.pushbullet.android.providers.syncables.SyncablesProvider.insert|com.pushbullet.android"
"cp|appsp|com.pushbullet.android.sms.SmsSentReceiver.onReceive|com.pushbullet.android:background"
"cp|appsp|com.pushbullet.android.sms.SmsSyncService.a|com.pushbullet.android:background"
"cp|native|write_to_disk|com.pushbullet.android:background"
"cp|native|read_from_network|com.pushbullet.android:background"
"cp|device|network_read|com.pushbullet.android:background"
"cp|device|file_write|com.pushbullet.android:background"
"spying|3rdparty|com.google.android.gms.gcm.GcmReceiver.onReceive|com.pushbullet.android:background"
"spying|androidsp|android.content.ContentResolver.registerContentObserver|com.pushbullet.android:background"
"spying|appsp|com.pushbullet.android.gcm.GcmService.a|com.pushbullet.android"
"spying|appsp|com.pushbullet.android.sms.SmsSyncService.a|com.pushbullet.android:background"
"spying|native|write_to_network|com.pushbullet.android:background"
"spying|native|read_from_disk|com.pushbullet.android:background"
"spying|device|network_write|com.pushbullet.android:background"
"spying|device|file_write|com.pushbullet.android:background"
)

## To get timings
# console_logs="./results/console_log_offline*.log"

# for log in $console_logs
# do  
#     i=0
#     while IFS= read -r line || [ -n "$line" ]
#     do
#         echo "${strings[i]}|$line"
#         let "i++"
#     done < "$log"
#     echo ""
# done

full_logs="./results/online*"
for parent_dir in $full_logs
do  
    test=$parent_dir/*
    for log in $test
    do  
        dir_name=`basename ${log}`
        scenario=$(echo $dir_name | cut -d_ -f1)
        category=$(echo $dir_name | cut -d_ -f2)
        tp=$(echo $dir_name | cut -d_ -f3)
        
        log_dir="$log/pushbullet_jitmflogs"

        # full_dump_size=$(find $log_dir -type f \( -iname \*.jitmfheaplog -o -iname \*.hprof \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )
        filtered_dump_size=$(find $log_dir -type f \( -iname \*.jitmflog \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )

        echo "$scenario|$category|$tp|pushbullet|"${filtered_dump_size:-0}""
    done
    echo ""
done
echo "offline"
full_logs="./results/offline*"
for parent_dir in $full_logs
do  
    test=$parent_dir/*
    for log in $test
    do  
        dir_name=`basename ${log}`
        scenario=$(echo $dir_name | cut -d_ -f1)
        category=$(echo $dir_name | cut -d_ -f2)
        tp=$(echo $dir_name | cut -d_ -f3)
        
        log_dir="$log/pushbullet_jitmflogs"

        full_dump_size=$(find $log_dir -type f \( -iname \*.jitmfheaplog -o -iname \*.hprof \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )
        # filtered_dump_size=$(find $log_dir -type f \( -iname \*.jitmflog \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )

        echo "$scenario|$category|$tp|pushbullet|"${full_dump_size:-0}""
    done
    echo ""
done

# full_logs="./results/both_offline*"
# for parent_dir in $full_logs
# do
#     test=$parent_dir/*
#     for log in $test
#     do
#         found_offline="NOT FOUND"
#         found_online="NOT FOUND"
#         dir_name=`basename ${log}` 
#         scenario=$(echo $dir_name | cut -d_ -f1)
#         category=$(echo $dir_name | cut -d_ -f2)
#         tp=$(echo $dir_name | cut -d_ -f3)

#         if [[ $scenario == "cp" ]]; then
#             msg=$(cat $log/"send_log.log" | cut -d, -f2)
#         else
#             msg=$(cat $log/"received_log.log" | cut -d, -f2)
#         fi

#         log_dir="$log/pushbullet_jitmflogs"
#         if grep -qr "${msg}" $log_dir/*.filtered_heap; then
#             found_offline="FOUND"
#         else
#             found_offline="NOT FOUND"
#         fi

#         if grep -qr "${msg}" $log_dir/*.jitmflog; then
#             found_online="FOUND"
#         else
#             found_online="NOT FOUND"
#         fi
#         echo "$scenario|$category|$tp|pushbullet|"${found_online}"|"${found_offline}""
#     done
#     echo ""
# done
