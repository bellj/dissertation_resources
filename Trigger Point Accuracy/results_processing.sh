#!/bin/bash

app=$1
results_dir=$2

if [[ $app == *"telegram_pentest"* ]]; then
    # results_dir='spying_only'
    results_path="./hooking_with_server_IM_PENTEST/results/$results_dir/*/"
    agg_results_path="./telegram_pentest_${results_dir}_results.txt"
    extension="jitmflog"
    app='telegram'
    proper_test_name="telegram_pentest"
elif [[ $app == *"telegram"* ]]; then
    results_path="./hooking_with_server_IM/results/$results_dir/*/"
    agg_results_path="./telegram_${results_dir}_results.txt"
    extension="jitmflog"
else
    results_path="./hooking_with_server_SMS/results/${results_dir}/*/"
    agg_results_path="./pushbullet_${results_dir}_results.txt"
    extension="dataObjs"
fi

logs=$app"_jitmflogs"
toAppend="app,scenario,tp,category,found in jitmf,found in hprof,found in logcat,# hits,full_dump_size,filtered_dump_size,time_sent,time_received"
echo $toAppend >> $agg_results_path

for dir in $results_path
do    

    timesFnd=0
    dir_name=`basename ${dir}`
    scenario=$(echo $dir_name | cut -d_ -f1)
    category=$(echo $dir_name | cut -d_ -f2)
    tp=$(echo $dir_name | cut -d_ -f3)

    log_dir=$dir$logs

    time_received=0
    if [[ $app == *"telegram"* ]]; then
        msg_sent=$(cat $dir"send_log.log" | cut -d, -f2)
        time_sent=$(cat $dir"send_log.log" | cut -d, -f1)
        
        if [[ $scenario == *"spying"* ]]; then
            count_files=`ls -1 $log_dir/"received_log.log" 2>/dev/null | wc -l`
        else
            count_files=`ls -1 $dir/"received_log.log" 2>/dev/null | wc -l`
        fi
        if [[ $count_files != 0 ]]; then
            if [[ $scenario == *"spying"* ]]; then
                # echo "ehllo "$log_dir
                time_received=$(head -n 1 $log_dir/"received_log.log" | cut -d, -f1)
                # time_received=$(date -d "$time_received" +"%s")
            else
                time_received=$(head -n 1 $dir"received_log.log" | cut -d, -f1)
            fi
        fi
    else        
        if [[ $scenario == *"spying"* ]]; then
            count_files=`ls -1 $dir"received_log.log" 2>/dev/null | wc -l`
            if [[ $count_files != 0 ]]; then
                msg_sent=$(cat $dir"received_log.log" | cut -d, -f2)
                time_sent=$(cat $dir"received_log.log" | cut -d, -f1)   
            fi
        else
            msg_sent=$(cat $dir"send_log.log" | cut -d, -f2)
            time_sent=$(cat $dir"send_log.log" | cut -d, -f1)
        fi
    fi

    found_jitmf="NOT FOUND"
    if [[ $proper_test_name == "telegram_pentest" ]] && [[ $scenario == *"spying"* ]]; then
        send_log=$dir"send_log.log"
        lines=0
        found_jitmf='0'
        found_hprof_cnt=0
        found_logcat_cnt=0
        fnd_cnt=0
        timesFnd=`grep -rFo "${msg_sent}" $log_dir/*.$extension | wc -l`
        while IFS= read -r line
        do
            if [[ $line == "" ]]; then
                echo "blank" 
            else
                msg_sent=$(echo $line | cut -d, -f2)

                if grep -qr "${msg_sent}" $log_dir/*.$extension; then
                    let "fnd_cnt++"
                else
                    echo "NOT FOUND IN MY LOGS " $msg_sent
                fi

                if grep -qr "${msg_sent}" $log_dir/*; then
                    let "found_hprof_cnt++"
                else
                    echo "NOT FOUND IN HPROF ${msg_sent}"
                fi

                if grep -qr "${msg_sent}" $dir'logcat'*'.txt'; then
                    let "found_logcat_cnt++"
                else
                    echo "NOT FOUND in logcat"
                fi

                let "lines++"
            fi
        done < "$send_log"
        found_jitmf=$fnd_cnt"/"$lines
        found_hprof=$found_hprof_cnt"/"$lines
        found_logcat=$found_logcat_cnt"/"$lines
    else
        count_files=`ls -1 $log_dir/*.$extension 2>/dev/null | wc -l`
        if [[ $count_files != 0 ]]; then
            timesFnd=`grep -rFo "${msg_sent}" $log_dir/*.$extension | wc -l`
            if grep -qr "${msg_sent}" $log_dir/*.$extension; then
                found_jitmf="FOUND"
            else
                found_jitmf="NOT FOUND"
            fi
        fi
        
        found_hprof="NOT FOUND"
        no_hprof_files=`ls -1 $log_dir/*.hprof 2>/dev/null | wc -l`
        if [[ $no_hprof_files != 0 ]]; then

            no_fnd=`grep -lr "${msg_sent}" $log_dir/*.hprof | wc -l`
            if [[ $no_fnd > 0 ]]; then
                echo $no_fnd/$no_hprof_files
            else
                echo 0/0
            fi
            if grep -qr "${msg_sent}" $log_dir/*.hprof; then
                found_hprof="FOUND"
            else
                found_hprof="NOT FOUND"
            fi
        else
                echo 0/0
        fi

        found_logcat="NOT FOUND in logcat"
        if grep -qr "${msg_sent}" $dir'logcat'*'.txt'; then
            found_logcat="FOUND in logcat"
        else
            found_logcat="NOT FOUND in logcat"
        fi
    fi

    hits=0
    count_files=`ls -1 $log_dir/*.hooklog 2>/dev/null | wc -l`
    if [[ $count_files != 0 ]]; then
    # if test -f "$log_dir/*.hooklog"; then
        hooklog=$(find $log_dir -name *.'hooklog')
        while IFS= read -r line
        do
            let "hits++"
        done < "$hooklog"
    fi


    echo $dir,$hits,$timesFnd

    full_dump_size=$(find $log_dir -type f \( -iname \*.jitmfheaplog -o -iname \*.hprof \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )
    filtered_dump_size=$(find $log_dir -type f \( -iname \*.jitmfheaplog -o -iname \*.$extension \)  -exec ls -l {} \; | awk '{ Total += $5} END { print Total }' )

    toAppend=$app,$scenario,$tp,$category,$found_jitmf,$found_hprof,$found_logcat,$hits,"${full_dump_size:-0}","${filtered_dump_size:-0}",$time_sent,"${time_received:-0}"
    echo $toAppend >> $agg_results_path
done