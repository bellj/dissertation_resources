package com.example.trustedcontainervirtualapp.abs;

/**
 * @author Lody
 */
public interface BasePresenter {
	void start();
}
