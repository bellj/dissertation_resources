package com.example.trustedcontainervirtualapp.widgets;

import com.example.trustedcontainervirtualapp.App;

/**
 * @author Lody
 */
public class ViewHelper {

    public static int dip2px(float dpValue) {
        final float scale = App.getApp().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

}
