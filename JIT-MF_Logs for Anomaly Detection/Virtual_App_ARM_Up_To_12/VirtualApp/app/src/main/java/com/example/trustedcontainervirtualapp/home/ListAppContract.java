package com.example.trustedcontainervirtualapp.home;

import java.util.List;

import com.example.trustedcontainervirtualapp.abs.BasePresenter;
import com.example.trustedcontainervirtualapp.abs.BaseView;
import com.example.trustedcontainervirtualapp.home.models.AppInfo;

/**
 * @author Lody
 * @version 1.0
 */
/*package*/ class ListAppContract {
    interface ListAppView extends BaseView<ListAppPresenter> {

        void startLoading();

        void loadFinish(List<AppInfo> infoList);
    }

    interface ListAppPresenter extends BasePresenter {

    }
}
