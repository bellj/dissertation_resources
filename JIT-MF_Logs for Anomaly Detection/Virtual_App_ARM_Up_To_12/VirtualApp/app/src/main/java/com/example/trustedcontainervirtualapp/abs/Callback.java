package com.example.trustedcontainervirtualapp.abs;

/**
 * @author Lody
 */

public interface Callback<T> {
    void callback(T result);
}
