import sys
import os
import glob
import shutil
import json
from google.cloud import bigquery

def clean_slate(path):
    for dir in glob.glob(os.path.join(path,"**","newly_parsed_jitmf"), recursive=True):
        print(f"REMOVING {dir}")
        shutil.rmtree(dir)

def merge_files(jitmf_logfiles, new_jitmf_folder):
    if not os.path.exists(os.path.join(new_jitmf_folder,"all_jitmflogs_MERGED.jitmflog")):
        with open(os.path.join(new_jitmf_folder,"all_jitmflogs_MERGED.jitmflog"), 'w') as outfile:
            for fname in jitmf_logfiles:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
    return os.path.join(new_jitmf_folder,"all_jitmflogs_MERGED.jitmflog")

def validate_entries_in_files(jitmflogs_uniq, new_jitmf_folder):
    content=''
    new_content=''
    with open(jitmflogs_uniq) as merged_uniq_file:
        content=' '.join(merged_uniq_file.read().splitlines())+'\n'
        content = content.replace(' {"time": "', '\n{"time": "')
        content = content.replace(', "object": "{"Query Executed"',', "object": {"Query_Executed"' )
        content = content.replace('"}"}\n','"}}\n' )
        content = content.replace(',"trigger_point(s)":',',"trigger_point":' )

    for l in content.splitlines():
        beginning = l.split('"Query_Executed": "')[0]
        middle = l.split('"Query_Executed": "')[1]
        middle = middle[:-3]
        middle=json.dumps(middle).strip('"')
        middle=middle.strip("\\")
        new_content+=beginning+'"Query_Executed": "'+middle+'"}}\n'

    outfile_name=os.path.join(new_jitmf_folder,"all_jitmflogs_FORMATTED.jsonl")   
    if not os.path.exists(os.path.join(new_jitmf_folder,"all_jitmflogs_FORMATTED.jsonl")):
        outfile = open(outfile_name, "w")
        outfile.write(new_content)
        outfile.close()

    return outfile_name

def unique_entry_in_files(jitmflogs_all,new_jitmf_folder):
    # print("MAKING UNIQUE")
    if not os.path.exists(os.path.join(new_jitmf_folder,"all_jitmflogs_FORMATTED_UNIQ.jsonl")):
        lines_seen = set() # holds lines already seen
        outfile = open(os.path.join(new_jitmf_folder,"all_jitmflogs_FORMATTED_UNIQ.jsonl"), "w")
        for line in open(jitmflogs_all, "r"):
            if line not in lines_seen: # not a duplicate
                outfile.write(line)
                lines_seen.add(line)
        outfile.close()
    return os.path.join(new_jitmf_folder,"all_jitmflogs_FORMATTED_UNIQ.jsonl")

def bq_load(app_name,jsonl_file,table_name):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/jennifer/Downloads/utility-ratio-120412-c39bf207a207.json'
    client = bigquery.Client()
    if app_name=="telegram":
        table_id="grr_2."+table_name
    else:
        table_id="grr_"+app_name+"."+table_name
    
    job_config = bigquery.LoadJobConfig(
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON, autodetect=True,
    )

    with open(jsonl_file, "rb") as source_file:
        job = client.load_table_from_file(source_file, table_id, job_config=job_config)
    job.result()  # Waits for the job to complete.
    table = client.get_table(table_id)  # Make an API request.
    print(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )

results_path=app=sys.argv[1]
# clean_slate(results_path)

# for app_results in next(os.walk(results_path))[1]:
app_results=results_path
if not app_results=="backup":
    app_name = os.path.basename(app_results).split("_")[0]
    # if(app_name=="signal"):
    print(app_results)
    new_jitmf_folder=os.path.join(results_path, app_results,"newly_parsed_jitmf")
    if not os.path.exists(new_jitmf_folder):
        os.mkdir(new_jitmf_folder)

    jitmf_logfiles=glob.glob(os.path.join(results_path,app_results,"**","*.jitmflog"), recursive=True)

    jitmflogs_all=merge_files(jitmf_logfiles, new_jitmf_folder)
    jsonl_file=validate_entries_in_files(jitmflogs_all,new_jitmf_folder)
    jsonl_uniq=unique_entry_in_files(jsonl_file,new_jitmf_folder)
    print(f"Your JSONL file is ready {jsonl_uniq}")
    # if app_name=="telegram":
    try:
        bq_load(app_name,jsonl_uniq,"new_jitmf_data")
        print(f"{app_name} SUCCEEDED")
    except Exception as e: print(f"{app_name} FAILED WITH : {e}")
    try:
        app_jsonl=glob.glob(os.path.join(results_path,app_results,app_name+".jsonl"))[0]
        bq_load(app_name,app_jsonl,app_name+"_data")
    except Exception as e: print(f"{app_name} FAILED WITH : {e}")

