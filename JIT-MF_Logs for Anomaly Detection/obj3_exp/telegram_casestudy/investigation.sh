#! /bin/bash
echo -e "Removing residue logs on device"
adb shell rm -rf /sdcard/com.example.trustedcontainervirtualapp
flow_folder_name=$1
GRR_OUTPUT_PATH=$2

echo "Starting up ReLF"
adb shell am force-stop org.nexus_lab.relf.mobile
sleep 2
adb shell am start -n org.nexus_lab.relf.mobile/org.nexus_lab.relf.mobile.MainActivity
echo "ReLF set up"

echo -e "Collection starting at `date`\n"

CSRFTOKEN=`curl http://127.0.0.1:8000   -H 'Authorization: Basic YWRtaW46ZGVtbw==' -o /dev/null -s -c - | grep csrftoken  | cut -f 7`; \

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/C.156d477f550056db/flows -d \
        '{
            "flow": {
                "args": {
                "knowledgeBase": {},
                "@type": "type.googleapis.com/ArtifactCollectorFlowArgs",
                "artifactList": [
                    "AdvancedAndroidUserProfiles",
                    "AndroidAccounts",
                    "AndroidBluetoothInfo",
                    "AndroidBatteryInfo",
                    "AndroidCallLogs",
                    "AndroidContactInfo",
                    "AndroidDeviceInfo",
                    "AndroidLocation",
                    "AndroidMessages",
                    "AndroidNfcInfo",
                    "AndroidPackages",
                    "AndroidSensorInfo",
                    "AndroidStorageInfo",
                    "AndroidSystemSettings",
                    "AndroidTelephonyInfo",
                    "AndroidUserProfiles",
                    "AndroidVpnProfiles",
                    "AndroidWiFiInfo",
                    "EnumerateLinuxUsers"
                ],
                "splitOutputByArtifact": true
                },
                "name": "ArtifactCollectorFlow",
                "runnerArgs": {
                "outputPlugins": [
                    {
                    "pluginArgs": "",
                    "pluginName": "BigQueryOutputPlugin"
                    }
                ]
                }
            }
        }')
flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
echo -e "AndroidArtifactFlow with flow ID: $flow_id started at `date -d @$(($startedAt/1000000))`\n"

sleep 5

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/C.156d477f550056db/flows -d \
    '{
        "flow": {
            "args": {
            "action": {
                "actionType": "DOWNLOAD"
            },
            "paths": [
                "/sdcard/jitmflogs/**.jitmflog"
            ],
            "@type": "type.googleapis.com/FileFinderArgs"
            },
            "name": "FileFinder"
        }
    }')
JITMF_flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
echo -e "FileFinder: JIT-MF with flow ID: $JITMF_flow_id started at `date -d @$(($startedAt/1000000))`.\nWaiting 10 minutes to download ....\n"

sleep 5

echo -e "Copying VirtualApp Telegram components to sdcard\n"
adb shell run-as com.example.trustedcontainervirtualapp cp -r /data/data/com.example.trustedcontainervirtualapp /sdcard/

echo -e "Copied\n"

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/C.156d477f550056db/flows -d \
    '{
        "flow": {
            "args": {
            "action": {
                "actionType": "DOWNLOAD"
            },
            "paths": [
                "/sdcard/com.example.trustedcontainervirtualapp/**7/cache4.db**"
            ],
            "@type": "type.googleapis.com/FileFinderArgs"
            },
            "name": "FileFinder"
        }
    }')
TELEGRAM_flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
echo -e "FileFinder: Telegram with flow ID: $TELEGRAM_flow_id started at `date -d @$(($startedAt/1000000))`.\nWaiting 10 minutes to download ....\n "

sleep 600
echo -e "Starting JIT-MF download ...\n"
# download files as zip to folder
docker exec relf-server mkdir /tmp/grr_output/${flow_folder_name}
docker exec relf-server grr_api_shell 'http://localhost:8000' --basic_auth_username 'admin' --basic_auth_password 'demo' --exec_code 'grrapi.Client("C.156d477f550056db").Flow("'${JITMF_flow_id}'").GetFilesArchive().WriteToFile("/tmp/grr_output/'${flow_folder_name}'/flow_results_jitmf.zip")'

sleep 5
echo -e "Starting TELEGRAM download ...\n"

# download files as zip to folder
docker exec relf-server grr_api_shell 'http://localhost:8000' --basic_auth_username 'admin' --basic_auth_password 'demo' --exec_code 'grrapi.Client("C.156d477f550056db").Flow("'${TELEGRAM_flow_id}'").GetFilesArchive().WriteToFile("/tmp/grr_output/'${flow_folder_name}'/flow_results_telegram.zip")'


# donwnload flow locally as well
curl -H 'GET' 'http://localhost:8000/api/clients/C.156d477f550056db/flows/'$flow_id'/exported-results/csv-zip'  -H 'Accept: application/json, text/plain, */*'   -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8,mt;q=0.7'   -H 'Authorization: Basic YWRtaW46ZGVtbw=='   -H 'Connection: keep-alive'   -H 'Cookie: csrftoken='$CSRFTOKEN''   -H 'Referer: http://localhost:8000/'   -H 'Sec-Fetch-Dest: empty'   -H 'Sec-Fetch-Mode: cors'   -H 'Sec-Fetch-Site: same-origin'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'   -H 'sec-ch-ua: "Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Linux"'   --compressed --output $GRR_OUTPUT_PATH"/"$flow_folder_name"/flow_results_androidartefacts.zip"


echo -e "Collection finished at `date`"