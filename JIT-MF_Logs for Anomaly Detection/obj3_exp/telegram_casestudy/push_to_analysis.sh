#! /bin/bash
RESULTS_PATH=$1

telegaram_file="$RESULTS_PATH/flow_results_telegram.zip"
jitmf_file="$RESULTS_PATH/flow_results_jitmf.zip"

unzip $telegaram_file -d ${telegaram_file%????} 
telegram_out_dir="${telegaram_file%????}"
tg_db_path=`find $telegram_out_dir -name cache4.db -size +4k`

python3 telegram_extract.py $tg_db_path $RESULTS_PATH/telegram_data.csv

# jitmf

unzip $jitmf_file -d ${jitmf_file%????} 
jitmf_out_dir="${jitmf_file%????}"

find $jitmf_out_dir -type f -name '*.jitmflog' -exec cat {} \; >> $RESULTS_PATH/all_jitmflogs.txt
sed -n -i '/REPLACE INTO messages_v2/p' $RESULTS_PATH/all_jitmflogs.txt
sed -i 's/"}"}/"}}/g' $RESULTS_PATH/all_jitmflogs.txt
sed -i 's/"{"Query /{"Query_/g' $RESULTS_PATH/all_jitmflogs.txt
sed -i 's/trigger_point(s)/trigger_points/g' $RESULTS_PATH/all_jitmflogs.txt

while IFS= read -r line
do    
    string_to_check_og=`echo $line | grep -o -P '(?<=REPLACE INTO messages_v2 VALUES\().*(?=\)"}})'`
    string_to_check_updated=$( echo "$string_to_check_og" | sed 's/"/\\"/g' )

    new_line=`echo $line | grep -o -P "(.*)REPLACE INTO messages_v2 VALUES\("`$string_to_check_updated'"}}'

    echo "$new_line" >> $RESULTS_PATH/all_jitmflogs_escaped.jsonl

done < $RESULTS_PATH/all_jitmflogs.txt

sort $RESULTS_PATH/all_jitmflogs_escaped.jsonl > $RESULTS_PATH/all_jitmflogs_escaped_sorted.jsonl
uniq $RESULTS_PATH/all_jitmflogs_escaped_sorted.jsonl > $RESULTS_PATH/all_jitmflogs_escaped_uniq.jsonl

bq load --autodetect --source_format=CSV grr_2.telegram_data $RESULTS_PATH/telegram_data.csv
sleep 5
bq load --autodetect --source_format=NEWLINE_DELIMITED_JSON grr_2.jitmf_data $RESULTS_PATH/all_jitmflogs_escaped.jsonl