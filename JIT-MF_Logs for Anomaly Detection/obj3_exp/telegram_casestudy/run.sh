#! /bin/bash
set -x
# ./exp3_pt3.sh

# Assumes:
# RELF installed
# GRR Server running
# RELF and GRR communicate with eachother
# TG on Vapp installed

# adb push ./setup_on_phone/telegram_sqlite_driver.js /data/local/tmp/frida-scripts/script1.js

dev="$1"
min_sleep=2
med_sleep=30
max_sleep=1800
one_hour=3600
two_hours=7200
five_hours=18000

GRR_OUTPUT_PATH="/mnt/2TBdrive/gitlab/experiment_5/obj3_exp/results"

echo "Date: `date` Device: $dev"

curr_dir=`pwd`

python3 normal_traffic_simulation.py "$dev" 
adb shell am force-stop  com.example.trustedcontainervirtualapp
sleep $med_sleep

#  STEALTHY ATTACK STARTS NOW
source env/bin/activate
python3 receive_telegram_msg.py "DHL: Your parcel is arriving, track here: https://flexisales.com/dhl?eep7j88cc5z3"
deactivate

sleep $min_sleep
adb shell am start -a android.intent.action.VIEW -d "https://flexisales.com/dhl?eep7j88cc5z3"
sleep $min_sleep

adb -s $dev install $curr_dir/demo.apk

python3 normal_traffic_simulation.py "$dev"
adb shell am force-stop  com.example.trustedcontainervirtualapp 
sleep $med_sleep
# sends to DHL Message to all contacts in TG and deletes from victim pov
python3 stealthy_attack_simulation.py "$dev" 
adb shell am force-stop  com.example.trustedcontainervirtualapp
sleep $min_sleep

adb -s $dev uninstall -k com.example.demo
cd $curr_dir
#  STEALTHY ATTACK ENDS NOW

sleep $max_sleep #alert after 30 minutes of the malware being uninstalled?

source env/bin/activate
python3 receive_telegram_msg.py "Hey, I think something is wrong with your phone. You sent me a suspicious message!"
deactivate

echo ">>> NOW `date` <<<"

flow_folder_name=`date +"%FT%T"`

sleep $max_sleep 
# START INVESTIGATION
./investigation.sh $flow_folder_name $GRR_OUTPUT_PATH

sleep $min_sleep

./push_to_analysis.sh $GRR_OUTPUT_PATH/$flow_folder_name

adb -s $dev uninstall com.example.demo