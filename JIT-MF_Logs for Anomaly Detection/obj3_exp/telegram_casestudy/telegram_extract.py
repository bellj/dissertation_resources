import sqlite3
from sqlite3 import Error
import string
import csv
import sys

conn = sqlite3.connect(sys.argv[1])
cur = conn.cursor()
cur.execute("SELECT mid,uid,read_state,send_state,date,data,out,ttl,media,replydata,imp,mention,forwards,replies_data,thread_reply_id,is_channel,reply_to_message_id,custom_params,group_id FROM messages_v2")
rows = cur.fetchall()
new_rows = []
printable = set(string.printable)
for row in rows:
    new_row = list(row)
    x = bytearray(new_row[5])
    new_row[5] = ''.join(chr(j) for j in x)
    new_row[5] = "".join(list(filter(lambda x: x in printable, new_row[5])))
    new_row[5] = new_row[5].replace('\n', '')
    new_rows.append(new_row)


with open(sys.argv[2], 'w') as f:
    writer = csv.writer(f)
    writer.writerow(['mid','uid','read_state','send_state','date','data','out','ttl','media','replydata','imp','mention','forwards','replies_data','thread_reply_id','is_channel','reply_to_message_id','custom_params','group_id'])
    for row in new_rows:
        writer.writerow(row)