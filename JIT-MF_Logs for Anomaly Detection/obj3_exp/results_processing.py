import csv

telegram_JITMFEDR_values={}
signal_JITMFEDR_values={}
whatsapp_JITMFEDR_values={}
slack_JITMFEDR_values={}
skype_JITMFEDR_values={}
imo_JITMFEDR_values={}
kik_JITMFEDR_values={}
viber_JITMFEDR_values={}
messenger_JITMFEDR_values={}
messengerlite_JITMFEDR_values={}
telegram_EDRONLY_values={}
signal_EDRONLY_values={}
whatsapp_EDRONLY_values={}
slack_EDRONLY_values={}
skype_EDRONLY_values={}
imo_EDRONLY_values={}
kik_EDRONLY_values={}
viber_EDRONLY_values={}
messenger_EDRONLY_values={}
messengerlite_EDRONLY_values={}

install_JITMFEDR_values={}
propogation_JITMFEDR_values={}
deletion_JITMFEDR_values={}
uninstall_JITMFEDR_values={}
install_EDRONLY_values={}
propogation_EDRONLY_values={}
deletion_EDRONLY_values={}
uninstall_EDRONLY_values={}

events=["install","propogation","deletion","uninstall"]
# apps=["telegram","signal","whatsapp","slack","skype","imo","kik","viber","messenger","messengerlite"]
apps=["telegram","signal","whatsapp","slack","skype","imo","kik","messenger"]
value_type=["JITMFEDR","EDRONLY"]

def calc_prec_recall_f1():
    for a in apps:
        for e in events:
            for v in value_type:
                
                globals()[f"{a.lower()}_{v}_values"][e]["r"]=globals()[f"{a.lower()}_{v}_values"][e]["tp"]/(globals()[f"{a.lower()}_{v}_values"][e]["tp"]+globals()[f"{a.lower()}_{v}_values"][e]["fn"])

                if(globals()[f"{a.lower()}_{v}_values"][e]["r"]==0):
                    globals()[f"{a.lower()}_{v}_values"][e]["p"]="N/A"
                else:    
                    globals()[f"{a.lower()}_{v}_values"][e]["p"]=globals()[f"{a.lower()}_{v}_values"][e]["tp"]/(globals()[f"{a.lower()}_{v}_values"][e]["tp"]+globals()[f"{a.lower()}_{v}_values"][e]["fp"])

def average():
    for v in value_type:
        
        for a in apps:
            prec_avg=[]
            recall_avg=[]
            for e in events:
                if globals()[f"{a.lower()}_{v}_values"][e]["p"]=="N/A":
                    prec_avg.append(0)
                else:
                    prec_avg.append(globals()[f"{a.lower()}_{v}_values"][e]["p"])
                recall_avg.append(globals()[f"{a.lower()}_{v}_values"][e]["r"])

            globals()[f"{a.lower()}_{v}_values"]["precision_avg"]=sum(prec_avg) / len(prec_avg)
            globals()[f"{a.lower()}_{v}_values"]["recall_avg"]=sum(recall_avg) / len(recall_avg)
            globals()[f"{a.lower()}_{v}_values"]["f1_avg"]=2*((globals()[f"{a.lower()}_{v}_values"]["precision_avg"] * globals()[f"{a.lower()}_{v}_values"]["recall_avg"]) / (globals()[f"{a.lower()}_{v}_values"]["precision_avg"] + globals()[f"{a.lower()}_{v}_values"]["recall_avg"]))

def average_event():
    for v in value_type:
        for e in events:
            event_prec_avg=[]
            event_rec_avg=[]
            for a in apps:
                if globals()[f"{a.lower()}_{v}_values"][e]["p"]=="N/A":
                    event_prec_avg.append(0)
                else:
                    event_prec_avg.append(globals()[f"{a.lower()}_{v}_values"][e]["p"])
                event_rec_avg.append(globals()[f"{a.lower()}_{v}_values"][e]["r"])

            print(f"{e} {v} {event_prec_avg} {event_rec_avg}")
            globals()[f"{e}_{v}_values"]["precision_avg"]=sum(event_prec_avg) / len(event_prec_avg)
            if len(event_rec_avg)==0:
                globals()[f"{e}_{v}_values"]["recall_avg"]="N/A"
            else:    
                globals()[f"{e}_{v}_values"]["recall_avg"]=sum(event_rec_avg) / len(event_rec_avg)

for a in apps:
    for e in events:
        for vt in value_type:
            globals()[f"{a.lower()}_{vt}_values"]["app_name"]=a
            globals()[f"{a.lower()}_{vt}_values"]["value_type"]={vt}
            globals()[f"{a.lower()}_{vt}_values"]["f1_avg"]=0
            globals()[f"{a.lower()}_{vt}_values"]["precision_avg"]=0
            globals()[f"{a.lower()}_{vt}_values"]["recall_avg"]=0
            globals()[f"{a.lower()}_{vt}_values"][e]={"value":None, "gt":None, "fn":None,"tp":None,"fp":None,"p":None,"r":None, "f1_score":None}

with open('./results/obj3_results.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        app_name=row[0]
        type=row[1]
        install=row[2]
        propogation=row[3]
        deletion=row[4]
        uninstall=row[5]
        
        if app_name.lower() not in ['messengerlite','viber']:
            if type=="GT":
                for e in events:
                    globals()[f"{app_name.lower()}_EDRONLY_values"][e]["gt"] = int(globals()[f"{e}"])
                    globals()[f"{app_name.lower()}_JITMFEDR_values"][e]["gt"] = int(globals()[f"{e}"])
                continue
            else:
                if type=="EDR+JITMF":
                    vt="JITMFEDR" 
                elif type=="EDR_only":
                    vt="EDRONLY" 
            
            for e in events:
                globals()[f"{app_name.lower()}_{vt}_values"][e]["value"] = int(globals()[f"{e}"])
                # ensuring fp =0
                if globals()[f"{app_name.lower()}_{vt}_values"][e]["value"] >= globals()[f"{app_name.lower()}_{vt}_values"][e]["gt"]:
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["fp"]=globals()[f"{app_name.lower()}_{vt}_values"][e]["value"] - globals()[f"{app_name.lower()}_{vt}_values"][e]["gt"]
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["tp"]=globals()[f"{app_name.lower()}_{vt}_values"][e]["gt"]
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["fn"]=0
                
                if globals()[f"{app_name.lower()}_{vt}_values"][e]["value"] < globals()[f"{app_name.lower()}_{vt}_values"][e]["gt"]:
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["fn"]= abs(globals()[f"{app_name.lower()}_{vt}_values"][e]["gt"] - globals()[f"{app_name.lower()}_{vt}_values"][e]["value"])
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["tp"]=globals()[f"{app_name.lower()}_{vt}_values"][e]["value"]
                    globals()[f"{app_name.lower()}_{vt}_values"][e]["fp"]=0
            
            line_count += 1

calc_prec_recall_f1()
average()
average_event()

print("app,event,EDR+JITMF recall,EDR+JITMF precision,EDR ONLY recall,EDR ONLY precision")
for a in apps:
    for e in events:    
        print(a+","+e+","+str(globals()[f"{a.lower()}_JITMFEDR_values"][e]["r"])+","+str(globals()[f"{a.lower()}_JITMFEDR_values"][e]["p"])+","+str(globals()[f"{a.lower()}_EDRONLY_values"][e]["r"])+","+str(globals()[f"{a.lower()}_EDRONLY_values"][e]["p"]))

print("\n\napp,EDR+JITMF AVG precision,EDR+JITMF AVG recall,EDR+JITMF AVG F1,EDR ONLY AVG precision,EDR ONLY AVG recall,EDR ONLY AVG F1")
for a in apps:
    print(a+","+str(globals()[f"{a.lower()}_JITMFEDR_values"]["precision_avg"])+","+str(globals()[f"{a.lower()}_JITMFEDR_values"]["recall_avg"])+","+str(globals()[f"{a.lower()}_JITMFEDR_values"]["f1_avg"])+","+str(globals()[f"{a.lower()}_EDRONLY_values"]["precision_avg"])+","+str(globals()[f"{a.lower()}_EDRONLY_values"]["recall_avg"])+","+str(globals()[f"{a.lower()}_EDRONLY_values"]["f1_avg"]))
        
print("\n\napp,EDR+JITMF AVG precision,EDR+JITMF AVG recall,EDR ONLY AVG precision,EDR ONLY AVG recall")
for e in events:
    print(e+","+str(globals()[f"{e}_JITMFEDR_values"]["precision_avg"])+","+str(globals()[f"{e}_JITMFEDR_values"]["recall_avg"])+","+str(globals()[f"{e}_EDRONLY_values"]["precision_avg"])+","+str(globals()[f"{e}_EDRONLY_values"]["recall_avg"]))