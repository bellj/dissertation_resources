import re
import sys
import time
import os
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

MAX_WAIT=10
_s = 20
_s2 = 1

def send_message(v):
    # sending message
    v.touch()
    vc.sleep(_s2)
    dump_handler()   
    view = vc.findViewById("com.Slack:id/message_input_field")
    view.touch()
    message_text="DHL: https://flexisales.com/dhl?eep7j88cc5z3"
    device.type(message_text)
    dump_handler()
    vc.sleep(15)
    vc.findViewById("com.Slack:id/message_send_btn").touch()
    print("SLACK | Normal traffic | Sent message text="+message_text+". Current time ... ----> "+ str(datetime.now()))
    dump_handler()
    vc.sleep(_s2)
    vc.findViewWithContentDescriptionOrRaise(u'''Back''').touch()
    dump_handler()
    vc.sleep(_s2)

def delete_last_message():
    list_l=vc.findViewsWithAttribute('resource-id', 'com.Slack:id/msg_text')
    last_message=list_l[-1]

    (x,y)=last_message.getXY()
    y+=10
    last_message.device.drag((x,y), (x,y), 3000, 1)
    dump_handler()
    device.dragDip((185.0, 499.0), (191.0, 175.5), 200, 20, 0)
    vc.sleep(5)
    dump_handler()
    vc.findViewsWithAttribute('class', 'android.view.ViewGroup')[-1].touch()
    vc.sleep(_s2)
    dump_handler()

    vc.findViewWithTextOrRaise(u'Delete').touch()
    print("SLACK | Deleted message with text="+last_message.getText()+". Current time ... ----> "+ str(datetime.now()))

    
def send_and_delete_message(v):
    send_message(v)
    delete_last_message()
    dump_handler()
    vc.sleep(_s2)
    vc.findViewWithContentDescriptionOrRaise(u'''Back''').touch()
    vc.sleep(_s2)

def get_contacts():
    views=[]

    for tv in vc.findViewsWithAttribute('resource-id', 'com.Slack:id/text_primary'):
        text = tv.getText().lower()
        if("slackbot" not in text):
            views.append(tv)
    
    return views

def dump_handler():
    number_of_retries = 10
    sleep_time = 5
    counter = 0
    e=None
    while (counter < number_of_retries):
        try:
            vc.dump(window=-1)
            return
        except Exception as exc:
            e=exc
            time.sleep(sleep_time)
            counter += 1
    print("[[ FAILED TO EXECUTE VC.DUMP LATEST EXCEPTION ]]" + str(e))

device = sys.argv[1]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('com.example.trustedcontainervirtualapp/com.example.trustedcontainervirtualapp.MainActivity')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.sleep(60)
dump_handler()

views=get_contacts()

for v in views:
    send_and_delete_message(v)

device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s2)
device.shell('input keyevent KEYCODE_HOME')
