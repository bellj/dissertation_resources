-- WHATSAPP
CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.whatsapp`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    --SELECT '2023-02-20 21:57:29 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsapp_data" as source FROM `utility-ratio-120412.grr_whatsapp.whatsapp_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-20 21:09:32 UTC" 
    AND event_date <= "2023-02-20 21:57:29 UTC" 
    -- AND event_date >= "2023-02-20 21:14:00 UTC" 
    -- AND event_date <= "2023-02-27 21:57:29" 
    )
    GROUP BY ed
    
    )
);

-- MESSENGER

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.messenger`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
   --SELECT '2023-02-22 17:01:58 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_messenger.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(timestamp_ms) AS event_date, CONCAT("mid: ",message_id,"| uid: ",sender_id,"| in/out: ",sender_id,"| message: ",text) AS event_details, "messenger_data" as source FROM `utility-ratio-120412.grr_messenger.messenger_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-22 16:19:21 UTC" 
    AND event_date <= "2023-02-22 17:01:58 UTC" 
    )
    GROUP BY ed
    )
);

-- IMO

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.imo`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    --SELECT '2023-02-27 16:13:00 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_imo.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp/1000000 as INT64)) AS event_date, CONCAT("mid: ",msg_id,"| uid: ",group_msg_id,"| in/out: ",message_state,"| message: ",last_message) AS event_details, "imo_data" as source FROM `utility-ratio-120412.grr_imo.imo_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-27 16:01:04 UTC" 
    AND event_date <= "2023-02-27 16:13:16 UTC" 
    )
    GROUP BY ed
    )
);

-- KIK

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.kik`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    -- SELECT '2023-02-27 10:11:00 UTC' AS ed, 0 as discrepancy
    -- UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_kik.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",uid,"| uid: ",partner_jid,"| in/out: ",was_me,"| message: ",body) AS event_details, "kik_data" as source FROM `utility-ratio-120412.grr_kik.kik_data`    
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-27 09:58:35 UTC" 
    AND event_date <= "2023-02-27 13:11:14 UTC" 
    )
    GROUP BY ed
    )
);


-- SIGNAL

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.signal`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    --SELECT '2023-03-05 18:14:00 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_signal.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(receipt_timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",recipient_id,"| in/out: ",receipt_timestamp,"| message: ",body) AS event_details, "signal_data" as source FROM `utility-ratio-120412.grr_signal.signal_data`  
    )
    WHERE event_date IS NOT NULL 
    AND  event_date >= "2023-03-05 17:03:45 UTC" 
    AND event_date <= "2023-03-05 18:14:58" 
    )
    GROUP BY ed
    )
);

-- SKYPE

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.skype`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    --SELECT '2023-02-23 07:44:00 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_skype.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(nsp_data.createdTime) AS event_date, CONCAT("mid: ",sm.id ,"| uid: ",nsp_data.conversationId,"| in/out: ", nsp_data._isMyMessage ,"| message: ",sm.content,"| deleted: ",nsp_data.properties.deletetime) AS event_details, "skype_data" as source FROM `utility-ratio-120412.grr_skype.skype_data`  CROSS JOIN UNNEST(nsp_data._serverMessages) AS sm  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-23 06:58:18 UTC" 
    AND event_date <= "2023-02-23 07:44:42 UTC" 
    )
    GROUP BY ed
    )
);

-- SLACK

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.slack`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    -- SELECT '2023-02-23 11:16:00 UTC' AS ed, 0 as discrepancy
    -- UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_slack.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_SECONDS(cast(message_json.ts as int64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",channel_id,"| in/out: ",channel_id,"| message: ",message_json.text) AS event_details, "slack_data" as source FROM `utility-ratio-120412.grr_slack.slack_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-23 10:21:49 UTC" 
    AND event_date <= "2023-02-23 11:16:05 UTC" 
    )
    GROUP BY ed
    )
);
-- TELEGRAM

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.telegram`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    --SELECT '2023-02-19 15:51:00 UTC' AS ed, 0 as discrepancy
    --UNION ALL
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_2.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_2.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_2.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_2.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_2.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_2.telegram_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-19 15:10:00 UTC" 
    AND event_date <= "2023-02-19 15:51:32 UTC"
    )
    GROUP BY ed
    )
);

-- WHATSAPPBUSINESS

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.whatsappbusiness`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsappbusiness.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsappbusiness.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsappbusiness.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsappbusiness.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_whatsappbusiness.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsappbusiness_data" as source FROM `utility-ratio-120412.grr_whatsappbusiness.whatsappbusiness_data`  
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-03-18 10:54:48 UTC" 
    AND event_date <= "2023-03-18 11:36:21 UTC"
    )
    GROUP BY ed
    )
);

-- PUSHMESSENGER

CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.pushmessenger`
OPTIONS(
    model_type='arima_plus',
    TIME_SERIES_DATA_COL='discrepancy',
    TIME_SERIES_TIMESTAMP_COL='ed',
    DECOMPOSE_TIME_SERIES=TRUE
)
AS (
    SELECT ed, greatest(( (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) ), 0) as discrepancy
FROM(
SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
    FROM 
    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
    from (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_pushmessenger.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_pushmessenger.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_pushmessenger.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_pushmessenger.AutoExportedAndroidSmsMms` 
        UNION ALL
        select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
        from (
        SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_pushmessenger.new_jitmf_data`
        )
        WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
        GROUP BY event_date
        UNION ALL
        SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "pushmessenger_data" as source FROM `utility-ratio-120412.grr_pushmessenger.pushmessenger_data`
    )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-03-15 12:34:25 UTC" 
    AND event_date <= "2023-03-15 13:24:14 UTC"
    )
    GROUP BY ed
    )
);