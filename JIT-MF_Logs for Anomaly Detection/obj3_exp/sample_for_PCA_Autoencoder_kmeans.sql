-- OLD MODELS WITHOUT CERTAIN FEATURES
-- CREATE OR REPLACE TABLE `utility-ratio-120412.grr_app_models.whatsapp_table` AS
-- SELECT event_date, COUNT(*) as total, COUNTIF(source='whatsapp_data') AS whatsapp_data, COUNTIF(source='new_jitmf_data') AS new_jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number
-- FROM
-- (
--     SELECT TIMESTAMP_SECONDS(60 * DIV(unix_seconds(event_date), 60)) as event_date, event_details, source
--         from (
--             SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidCallLog` 
--             UNION ALL
--             SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSensorInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSmsMms` 
--             UNION ALL
--             select TIMESTAMP_SECONDS(time) AS event_date, event_details, source as source
--             from (
--             SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`
--             )
--             WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
--             UNION ALL
--             SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsapp_data" as source FROM `utility-ratio-120412.grr_whatsapp.whatsapp_data`    
--         )
--     WHERE event_date IS NOT NULL 
--     AND event_date >= "2023-02-20 21:14:00 UTC" 
--     AND event_date <= "2023-02-27 21:57:29" 
-- )
-- GROUP BY event_date;



-- CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.whatsapp_kmeans_jitmf`
-- OPTIONS(model_type='kmeans', standardize_features = true, num_clusters=1) AS
-- SELECT * EXCEPT(event_date)
-- FROM `utility-ratio-120412.grr_app_models.whatsapp_table`;


-- SELECT *
-- FROM
--   ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.whatsapp_kmeans_jitmf`,
--   STRUCT(0.2 AS contamination),
--   TABLE `utility-ratio-120412.grr_app_models.whatsapp_table`)
-- WHERE is_anomaly = true




-- CREATE OR REPLACE TABLE `utility-ratio-120412.grr_app_models.imo_table` AS
-- SELECT event_date, COUNT(*) as total, COUNTIF(source='imo_data') AS imo_data, COUNTIF(source='new_jitmf_data') AS new_jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number
-- FROM
-- (
--     SELECT TIMESTAMP_SECONDS(60 * DIV(unix_seconds(event_date), 60)) as event_date, event_details, source
--         from (
--             SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidCallLog` 
--             UNION ALL
--             SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidPackageInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSensorInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSmsMms` 
--             UNION ALL
--             select TIMESTAMP_SECONDS(time) AS event_date, event_details, source as source
--             from (
--             SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_imo.new_jitmf_data`
--             )
--             WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
--             UNION ALL
--             SELECT TIMESTAMP_MILLIS(cast(timestamp/1000000 as INT64)) AS event_date, CONCAT("mid: ",msg_id,"| uid: ",group_msg_id,"| in/out: ",message_state,"| message: ",last_message) AS event_details, "imo_data" as source FROM `utility-ratio-120412.grr_imo.imo_data` 
--         )
--     WHERE event_date IS NOT NULL 
--     AND event_date >= "2023-02-27 16:04:04 UTC" 
--     AND event_date <= "2023-02-27 16:13:16 UTC" 
-- )
-- GROUP BY event_date;



-- CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.imo_kmeans_jitmf`
-- OPTIONS(model_type='kmeans', standardize_features = true, num_clusters=1) AS
-- SELECT * EXCEPT(event_date)
-- FROM `utility-ratio-120412.grr_app_models.imo_table`;


-- SELECT *
-- FROM
--   ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.imo_kmeans_jitmf`,
--   STRUCT(0.2 AS contamination),
--   TABLE `utility-ratio-120412.grr_app_models.imo_table`)
-- WHERE is_anomaly = true





-- CREATE OR REPLACE TABLE `utility-ratio-120412.grr_app_models.kik_table` AS
-- SELECT event_date, COUNT(*) as total, COUNTIF(source='kik_data') AS kik_data, COUNTIF(source='new_jitmf_data') AS new_jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number
-- FROM
-- (
--     SELECT TIMESTAMP_SECONDS(60 * DIV(unix_seconds(event_date), 60)) as event_date, event_details, source
--         from (
--             SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidCallLog` 
--             UNION ALL
--             SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidPackageInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSensorInfo` 
--             UNION ALL
--             SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSmsMms` 
--             UNION ALL
--             select TIMESTAMP_SECONDS(time) AS event_date, event_details, source as source
--             from (
--             SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_kik.new_jitmf_data`
--             )
--             WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
--             UNION ALL
--             SELECT TIMESTAMP_MILLIS(cast(timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",uid,"| uid: ",partner_jid,"| in/out: ",was_me,"| message: ",body) AS event_details, "kik_data" as source FROM `utility-ratio-120412.grr_kik.kik_data` 
--         )
--     WHERE event_date IS NOT NULL 
--     AND event_date >= "2023-02-27 10:32:02 UTC" 
--     AND event_date <= "2023-02-27 13:11:14 UTC" 
-- )
-- GROUP BY event_date;



-- CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.kik_kmeans_jitmf`
-- OPTIONS(model_type='kmeans', standardize_features = true, num_clusters=1) AS
-- SELECT * EXCEPT(event_date)
-- FROM `utility-ratio-120412.grr_app_models.kik_table`;


-- SELECT *
-- FROM
--   ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.kik_kmeans_jitmf`,
--   STRUCT(0.2 AS contamination),
--   TABLE `utility-ratio-120412.grr_app_models.kik_table`)
-- WHERE is_anomaly = true




CREATE OR REPLACE TABLE `utility-ratio-120412.grr_app_models.messenger_table` AS
SELECT event_date, COUNT(*) as total_events, COUNTIF(source='AutoExportedAndroidCallLog') AS autoexportedandroidcalllog_data, COUNTIF(source='AutoExportedAndroidSmsMms') AS autoexportedandroidsmsmms_data, COUNTIF(source='AutoExportedAndroidSensorInfo') AS autoexportedandroidsensorinfo_data, COUNTIF(source='messenger_data') AS messenger_data, COUNTIF(source='AutoExportedAndroidPackageInfo') AS package_info_data, COUNTIF(source='new_jitmf_data') AS jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number, COUNTIF(CONTAINS_SUBSTR(event_details, "SELECT")) as select_number, COUNTIF(CONTAINS_SUBSTR(event_details, "messages(") or CONTAINS_SUBSTR(event_details, "from messages ")) as messages_table
FROM
(
    SELECT TIMESTAMP_SECONDS(60 * DIV(unix_seconds(event_date), 60)) as event_date, event_details, source
        from (
            SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidCallLog` 
            UNION ALL
            SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidPackageInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSensorInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSmsMms` 
            UNION ALL
            select TIMESTAMP_SECONDS(time) AS event_date, event_details, source as source
            from (
            SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_messenger.new_jitmf_data`
            )
            WHERE (upper(event_details) like 'SELECT%' or upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
            UNION ALL
            SELECT TIMESTAMP_MILLIS(timestamp_ms) AS event_date, CONCAT("mid: ",message_id,"| uid: ",sender_id,"| in/out: ",sender_id,"| message: ",text) AS event_details, "messenger_data" as source FROM `utility-ratio-120412.grr_messenger.messenger_data` 
        )
    WHERE event_date IS NOT NULL 
    AND event_date >= "2023-02-22 16:22:44 UTC" 
    AND event_date <= "2023-02-22 17:01:58 UTC" 
)
GROUP BY event_date;



CREATE OR REPLACE MODEL `utility-ratio-120412.grr_app_models.messenger_kmeans_jitmf`
OPTIONS(model_type='kmeans', standardize_features = true, num_clusters=1) AS
SELECT * EXCEPT(event_date)
FROM `utility-ratio-120412.grr_app_models.messenger_table`;


SELECT *
FROM
  ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.messenger_kmeans_jitmf`,
  STRUCT(0.3 AS contamination),
  TABLE `utility-ratio-120412.grr_app_models.messenger_table`)
WHERE is_anomaly = true



CREATE MODEL `utility-ratio-120412.grr_app_models.messenger_autoencoder_jitmf`
OPTIONS(
  model_type='autoencoder',
  activation_fn='relu',
  batch_size=8,
  dropout=0.2,  
  hidden_units=[32, 16, 4, 16, 32],
  learn_rate=0.001,
  l1_reg_activation=0.0001,
  max_iterations=10,
  optimizer='adam'
) AS 
SELECT * EXCEPT(event_date)
FROM `utility-ratio-120412.grr_app_models.messenger_table`;


SELECT
  *
FROM
  ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.messenger_autoencoder_jitmf`,
STRUCT(0.3 AS contamination),
TABLE `utility-ratio-120412.grr_app_models.messenger_table`)
WHERE is_anomaly = true;


CREATE MODEL `utility-ratio-120412.grr_app_models.messenger_pca_jitmf`
OPTIONS
  ( MODEL_TYPE='PCA', PCA_EXPLAINED_VARIANCE_RATIO=0.8) AS
SELECT * EXCEPT(event_date)
FROM `utility-ratio-120412.grr_app_models.messenger_table`;


SELECT
  *
FROM
  ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.messenger_autoencoder_jitmf`,
STRUCT(0.3 AS contamination),
TABLE `utility-ratio-120412.grr_app_models.messenger_table`)
WHERE is_anomaly = true;