#! /bin/bash -x
echo -e "Removing residue logs on device"
adb shell rm -rf /sdcard/com.example.trustedcontainervirtualapp
APP=$1
flow_folder_name=$2
GRR_OUTPUT_PATH=$3
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    RELF_CLIENT="C.156d477f550056db" #PC
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # RELF_CLIENT="C.b182519a33b82b83" #LAPTOP
    # RELF_CLIENT="C.280d72acaa9643b2"
    # RELF_CLIENT="C.a537bf5931a37a97"
    RELF_CLIENT="C.94ec55e2d210a7c7"
fi

whatsapp_ARTEFACT_PATH="/sdcard/com.example.trustedcontainervirtualapp/**7/msgstore.db**" 
messenger_ARTEFACT_PATH="/sdcard/com.example.trustedcontainervirtualapp/**7/msys_database**" 
telegram_ARTEFACT_PATH="/sdcard/com.example.trustedcontainervirtualapp/**7/cache4.db**"
skype_ARTEFACT_PATH=""
viber_ARTEFACT_PATH=""
kakao_ARTEFACT_PATH=""
messengerite_ARTEFACT_PATH="/sdcard/com.example.trustedcontainervirtualapp/**7/msys_database**"
imo_ARTEFACT_PATH=""
slack_ARTEFACT_PATH=""
signal_ARTEFACT_PATH="/sdcard/com.example.trustedcontainervirtualapp/**7/signal**.db**"

variable=$APP"_ARTEFACT_PATH"
echo  ${!variable}


# echo "Starting up ReLF"
# adb shell am force-stop org.nexus_lab.relf.mobile
# sleep 2
# adb shell am start -n org.nexus_lab.relf.mobile/org.nexus_lab.relf.mobile.MainActivity
# sleep 30
# echo "ReLF set up"

echo -e "Collection starting at `date`\n"

CSRFTOKEN=`curl http://127.0.0.1:8000   -H 'Authorization: Basic YWRtaW46ZGVtbw==' -o /dev/null -s -c - | grep csrftoken  | cut -f 7`; 

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/$RELF_CLIENT/flows -d \
        '{
            "flow": {
                "args": {
                "knowledgeBase": {},
                "@type": "type.googleapis.com/ArtifactCollectorFlowArgs",
                "artifactList": [
                    "AdvancedAndroidUserProfiles",
                    "AndroidAccounts",
                    "AndroidBluetoothInfo",
                    "AndroidBatteryInfo",
                    "AndroidCallLogs",
                    "AndroidContactInfo",
                    "AndroidDeviceInfo",
                    "AndroidLocation",
                    "AndroidMessages",
                    "AndroidNfcInfo",
                    "AndroidPackages",
                    "AndroidSensorInfo",
                    "AndroidStorageInfo",
                    "AndroidSystemSettings",
                    "AndroidTelephonyInfo",
                    "AndroidUserProfiles",
                    "AndroidVpnProfiles",
                    "AndroidWiFiInfo",
                    "EnumerateLinuxUsers"
                ],
                "splitOutputByArtifact": true
                },
                "name": "ArtifactCollectorFlow",
                "runnerArgs": {
                "outputPlugins": [
                    {
                    "pluginArgs": "",
                    "pluginName": "BigQueryOutputPlugin"
                    }
                ]
                }
            }
        }')
flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo -e "AndroidArtifactFlow with flow ID: $flow_id started at `date -d @$(($startedAt/1000000))`\n"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo -e "AndroidArtifactFlow with flow ID: $flow_id started at `date -r $(expr $startedAt / 1000000)`\n"
fi

sleep 5

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/$RELF_CLIENT/flows -d \
    '{
        "flow": {
            "args": {
            "action": {
                "actionType": "DOWNLOAD"
            },
            "paths": [
                "/sdcard/jitmflogs/**.jitmflog"
            ],
            "@type": "type.googleapis.com/FileFinderArgs"
            },
            "name": "FileFinder"
        }
    }')
JITMF_flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo -e "FileFinder: JIT-MF with flow ID: $JITMF_flow_id started at `date -d @$(($startedAt/1000000))`.\nWaiting 10 minutes to download ....\n"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo -e "FileFinder: JIT-MF with flow ID: $JITMF_flow_id started at `date -r $(expr $startedAt / 1000000)`.\nWaiting 10 minutes to download ....\n"
fi
sleep 5

echo -e "Copying VirtualApp $APP components to sdcard\n"
adb shell run-as com.example.trustedcontainervirtualapp cp -r /data/data/com.example.trustedcontainervirtualapp /sdcard/
adb pull /sdcard/com.example.trustedcontainervirtualapp $GRR_OUTPUT_PATH"/"$flow_folder_name
variable=$APP"_ARTEFACT_PATH"

echo -e "Copied\n"

resp=$(curl -s -X POST -H 'Authorization: Basic YWRtaW46ZGVtbw==' -H "Content-Type: application/json" -H "X-CSRFToken: $CSRFTOKEN" \
	http://127.0.0.1:8000/api/v2/clients/$RELF_CLIENT/flows -d \
    '{
        "flow": {
            "args": {
            "action": {
                "actionType": "DOWNLOAD"
            },
            "paths": [
                "'${!variable}'"
            ],
            "@type": "type.googleapis.com/FileFinderArgs"
            },
            "name": "FileFinder"
        }
    }')
APP_flow_id=`echo $resp | cut -c 5- | jq '.flowId' | tr -d \"`
startedAt=`echo $resp | cut -c 5- | jq '.startedAt' | tr -d \"`
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo -e "FileFinder: $APP with flow ID: $APP_flow_id started at `date -d @$(($startedAt/1000000))`.\nWaiting 10 minutes to download ....\n "
elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo -e "FileFinder: $APP with flow ID: $APP_flow_id started at `date -r $(expr $startedAt / 1000000)`.\nWaiting 10 minutes to download ....\n "
fi


sleep 600
# JITMF_flow_id="F:31D7FBB9"
# APP_flow_id="F:42E7FE01"
# flow_id="F:FD71E05A"

echo -e "Starting JIT-MF download ...\n"
# download files as zip to folder
docker exec relf-server grr_api_shell 'http://localhost:8000' --basic_auth_username 'admin' --basic_auth_password 'demo' --exec_code 'grrapi.Client("'$RELF_CLIENT'").Flow("'${JITMF_flow_id}'").GetFilesArchive().WriteToFile("/tmp/grr_output/'${flow_folder_name}'/flow_results_jitmf.zip")'

sleep 5
echo -e "Starting $APP download ...\n"

# download files as zip to folder
docker exec relf-server grr_api_shell 'http://localhost:8000' --basic_auth_username 'admin' --basic_auth_password 'demo' --exec_code 'grrapi.Client("'$RELF_CLIENT'").Flow("'${APP_flow_id}'").GetFilesArchive().WriteToFile("/tmp/grr_output/'${flow_folder_name}'/flow_results_'$APP'.zip")'

# donwnload flow locally as well
curl -H 'GET' 'http://localhost:8000/api/clients/'$RELF_CLIENT'/flows/'$flow_id'/exported-results/csv-zip'  -H 'Accept: application/json, text/plain, */*'   -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8,mt;q=0.7'   -H 'Authorization: Basic YWRtaW46ZGVtbw=='   -H 'Connection: keep-alive'   -H 'Cookie: csrftoken='$CSRFTOKEN''   -H 'Referer: http://localhost:8000/'   -H 'Sec-Fetch-Dest: empty'   -H 'Sec-Fetch-Mode: cors'   -H 'Sec-Fetch-Site: same-origin'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'   -H 'sec-ch-ua: "Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Linux"'   --compressed --output $GRR_OUTPUT_PATH"/"$flow_folder_name"/flow_results_androidartefacts.zip"

echo -e "Collection finished at `date`"