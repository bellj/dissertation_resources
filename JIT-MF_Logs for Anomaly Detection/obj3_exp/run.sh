#! /bin/bash
set -x
# ./exp3_pt3.sh

# Assumes:
# RELF installed
# GRR Server running
# RELF and GRR communicate with eachother
# TG on Vapp installed

# adb push ./setup_on_phone/telegram_sqlite_driver.js /data/local/tmp/frida-scripts/script1.js

dev="$1"
min_sleep=2
med_sleep=30
max_sleep=1800
one_hour=3600
two_hours=7200
five_hours=18000

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    EXPERIMENT_5_DIR=/mnt/2TBdrive/gitlab/experiment_5 #HOME PC
elif [[ "$OSTYPE" == "darwin"* ]]; then
    EXPERIMENT_5_DIR=/Users/jennifer/gitlab_repos/virtualapp_research/experiment5 #LAPTOP
    # ln -s -f /usr/local/bin/python3.11 /usr/local/bin/python3
fi

function monitor_adb () {
    echo "Checkin adb"
    # if [[ ! "$(adb shell echo 1 &)" ]]; then
        adb kill-server 2>/dev/null 
        adb start-server  2>/dev/null 
    # fi
}

GRR_OUTPUT_PATH="$EXPERIMENT_5_DIR/obj3_exp/results"

# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/telegram.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/whatsapp.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/messenger.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/skype.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/viber.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/kakao.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/messengerlite.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/imo.apk" "$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/slack.apk")

# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/telegram.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/whatsapp.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/messengerlite.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/messenger.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/skype.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/slack.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/viber.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/signal.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/imo.apk")
# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/kik.apk")

# appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/pushmessenger.apk")
appstorerun=("$EXPERIMENT_5_DIR/obj3_exp/multiple_apps/whatsappbusiness.apk")

apptotal=${#appstorerun[@]}
for f in ${appstorerun[@]}; do
    # flow_folder_name="2023-03-15T13:34:25"
    flow_folder_name=`date +"%FT%T"`
    
    echo "Date: `date` Device: $dev"

    curr_dir=`pwd`

    # ./move_apps.sh $f
    # echo ">>>SIGNUP/LOGIN NOW!!!!<<< "
    # sleep 60

    app=`basename $f .apk`
    mkdir $GRR_OUTPUT_PATH/$app"_"$flow_folder_name

    python3 "$app"_normal_traffic_simulation.py "$dev" 
    adb shell am force-stop  com.example.trustedcontainervirtualapp
    sleep $med_sleep

    #  STEALTHY ATTACK STARTS NOW
    source env/bin/activate
    python3 receive_msg.py $app "DHL: Your parcel is arriving, track here: https://flexisales.com/dhl?eep7j88cc5z3" $GRR_OUTPUT_PATH/$app"_"$flow_folder_name 10
    deactivate

    sleep $min_sleep
    adb shell am start -a android.intent.action.VIEW -d "https://flexisales.com/dhl?eep7j88cc5z3"
    sleep $min_sleep

    adb -s $dev install $curr_dir/demo.apk

    python3 "$app"_normal_traffic_simulation.py "$dev"
    adb shell am force-stop  com.example.trustedcontainervirtualapp 
    sleep $med_sleep
    monitor_adb
    # sends to DHL Message to all contacts in TG and deletes from victim pov
    python3 "$app"_stealthy_attack_simulation.py "$dev" 
    adb shell am force-stop  com.example.trustedcontainervirtualapp
    sleep $min_sleep

    adb -s $dev uninstall -k com.example.demo
    cd $curr_dir
    #  STEALTHY ATTACK ENDS NOW

    # sleep $max_sleep #alert after 30 minutes of the malware being uninstalled?

    source env/bin/activate
    python3 receive_msg.py $app "Hey, I think something is wrong with your phone. You sent me a suspicious message!" $GRR_OUTPUT_PATH/$app"_"$flow_folder_name 1800
    deactivate

    echo ">>> Running RELF NOW `date` <<<"    
    # $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/grr-server-relf.sh $app &

    sleep $max_sleep 
    echo "HEYY CREATE A BACKUP!! "
    adb -s $dev pull /sdcard/Signal/Backups $GRR_OUTPUT_PATH/$app"_"$flow_folder_name
    # START INVESTIGATION
    monitor_adb
    ./investigation.sh $app $app"_"$flow_folder_name $GRR_OUTPUT_PATH

    sleep $min_sleep
    monitor_adb
    ./push_to_analysis.sh $app $GRR_OUTPUT_PATH/$app"_"$flow_folder_name
    python3 push_to_analysis.py $GRR_OUTPUT_PATH/$app"_"$flow_folder_name

    # adb -s $dev uninstall com.example.demo
done