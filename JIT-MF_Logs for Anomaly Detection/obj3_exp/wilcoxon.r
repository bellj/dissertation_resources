# https://www.statskingdom.com/175wilcoxon_signed_ranks.html

> x01 <- c(0.35,0.33,0.35,0.33,0.15,0.17)
> x02 <- c(0.51,0.51,0.51,0.51,0.58,0.60)
> x03 <- c(0.65,0.71,0.65,0.71,0.66,0.79)
> x04 <- c(0.68,0.70,0.68,0.70,0.78,0.78)
> x05 <- c(0.86,0.93,0.86,0.93,1.00,1.00)
> 
> wilcox.test(x05, x04, alternative = "two.sided", mu = 0, paired = TRUE, exact = FALSE, correct = TRUE)

	Wilcoxon signed rank test with continuity correction

data:  x05 and x04
V = 21, p-value = 0.03451
alternative hypothesis: true location shift is not equal to 0

> group <- c(rep("x04", length(x04)), rep("x05", length(x05)))
> df <- data.frame(weight = c(x04, x05), group = group)
> if(!"rstatix" %in% installed.packages()){install.packages("rstatix")}
> library(rstatix)
> wilcox_effsize(df,weight ~ group, paired = TRUE)
# A tibble: 1 × 7
  .y.    group1 group2 effsize    n1    n2 magnitude
* <chr>  <chr>  <chr>    <dbl> <int> <int> <ord>    
1 weight x04    x05      0.906     6     6 large    
> 
> 
> wilcox.test(x05, x03, alternative = "two.sided", mu = 0, paired = TRUE, exact = FALSE, correct = TRUE)

	Wilcoxon signed rank test with continuity correction

data:  x05 and x03
V = 21, p-value = 0.03351
alternative hypothesis: true location shift is not equal to 0

> group <- c(rep("x03", length(x03)), rep("x05", length(x05)))
> df <- data.frame(weight = c(x03, x05), group = group)
> if(!"rstatix" %in% installed.packages()){install.packages("rstatix")}
> library(rstatix)
> wilcox_effsize(df,weight ~ group, paired = TRUE)
# A tibble: 1 × 7
  .y.    group1 group2 effsize    n1    n2 magnitude
* <chr>  <chr>  <chr>    <dbl> <int> <int> <ord>    
1 weight x03    x05      0.911     6     6 large    
> 
> 
> wilcox.test(x05, x02, alternative = "two.sided", mu = 0, paired = TRUE, exact = FALSE, correct = TRUE)

	Wilcoxon signed rank test with continuity correction

data:  x05 and x02
V = 21, p-value = 0.03351
alternative hypothesis: true location shift is not equal to 0

> group <- c(rep("x02", length(x02)), rep("x05", length(x05)))
> df <- data.frame(weight = c(x02, x05), group = group)
> if(!"rstatix" %in% installed.packages()){install.packages("rstatix")}
> library(rstatix)
> wilcox_effsize(df,weight ~ group, paired = TRUE)
# A tibble: 1 × 7
  .y.    group1 group2 effsize    n1    n2 magnitude
* <chr>  <chr>  <chr>    <dbl> <int> <int> <ord>    
1 weight x02    x05      0.911     6     6 large    
> 
> wilcox.test(x05, x01, alternative = "two.sided", mu = 0, paired = TRUE, exact = FALSE, correct = TRUE)

	Wilcoxon signed rank test with continuity correction

data:  x05 and x01
V = 21, p-value = 0.03501
alternative hypothesis: true location shift is not equal to 0

> group <- c(rep("x01", length(x01)), rep("x05", length(x05)))
> df <- data.frame(weight = c(x01, x05), group = group)
> if(!"rstatix" %in% installed.packages()){install.packages("rstatix")}
> library(rstatix)
> wilcox_effsize(df,weight ~ group, paired = TRUE)
# A tibble: 1 × 7
  .y.    group1 group2 effsize    n1    n2 magnitude
* <chr>  <chr>  <chr>    <dbl> <int> <int> <ord>    
1 weight x01    x05      0.904     6     6 large    
> 
> 
> 
> 
> wilcox.test(x04, x03, alternative = "two.sided", mu = 0, paired = TRUE, exact = FALSE, correct = TRUE)

	Wilcoxon signed rank test with continuity correction

data:  x04 and x03
V = 15, p-value = 0.3951
alternative hypothesis: true location shift is not equal to 0

> group <- c(rep("x03", length(x03)), rep("x04", length(x04)))
> df <- data.frame(weight = c(x03, x04), group = group)
> if(!"rstatix" %in% installed.packages()){install.packages("rstatix")}
> library(rstatix)
> wilcox_effsize(df,weight ~ group, paired = TRUE)
# A tibble: 1 × 7
  .y.    group1 group2 effsize    n1    n2 magnitude
* <chr>  <chr>  <chr>    <dbl> <int> <int> <ord>    
1 weight x03    x04      0.391     6     6 moderate 