

# read_file_path = 'output_ml_FORMATTED_ARIMAONLY.log'
read_file_path = 'output_ml_FORMATTED_20230420-102355.log'
write_file_path = 'full_result_set.csv'

feature_correlation_mapping = {
    'SELECT TIMESTAMP_SECONDS(1 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 1)) AS event_date, min(event_details) as event_details, min(source) as source|GROUP BY event_date': 'grouped_by_second',
    'SELECT TIMESTAMP_SECONDS(time) AS event_date,  event_details, source' : 'no_grouping'
}

with open(write_file_path, 'w') as result_file:
    with open(read_file_path, 'r') as f:
        all_lines = f.readlines()
        ad_method = ''
        ad_threshold = ''
        log_attributes = ''
        feature_correlation = ''
        print(len(all_lines))
        for line in all_lines:
            if 'Anomaly Detection (AD) Method: ' in line:
                ad_method = line.replace('Anomaly Detection (AD) Method: ','').replace('\n','')
            elif 'Anomaly Detection (AD) Threashold:' in line:
                ad_threshold = line.replace('Anomaly Detection (AD) Threashold: ','').replace('\n','')
            elif 'Log attributes for AD: ' in line:
                log_attributes =  line.replace('Log attributes for AD: ','').replace('\n','')
            elif 'Feature Correlation: ' in line:
                feature_correlation = feature_correlation_mapping[line.replace('Feature Correlation: ','').replace('\n','')]
            else:
                result_file.write(f'{ad_method},{ad_threshold},{log_attributes},{feature_correlation},{line}')
        
