import sys
import os
import glob
import shutil
import json
from google.cloud import bigquery
apps=["signal","whatsapp","slack","skype","imo","kik","messenger","pushmessenger","whatsappbusiness"]
def bq_load(app_name,jsonl_file,table_name):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/jennifer/Downloads/utility-ratio-120412-c39bf207a207.json'
    client = bigquery.Client()
    if app_name=="telegram":
        table_id="grr_2."+table_name
    else:
        table_id="grr_"+app_name+"."+table_name
    
    job_config = bigquery.LoadJobConfig(
        source_format=bigquery.SourceFormat.CSV, autodetect=True,
    )

    with open(jsonl_file, "rb") as source_file:
        job = client.load_table_from_file(source_file, table_id, job_config=job_config)
    job.result()  # Waits for the job to complete.
    table = client.get_table(table_id)  # Make an API request.
    print(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )

results_path=app='/Users/jennifer/gitlab_repos/virtualapp_research/experiment5/obj3_exp/results/all_telegram_data_FOR_ALL_CASE_STUDIES/all_telegram_logs.csv'

for a in apps:
    try:
        bq_load(a,results_path,"all_telegram_data")
        print(f"{a} SUCCEEDED")
    except Exception as e: print(f"{a} FAILED WITH : {e}")
