# MODELS: /Users/jennifer/gitlab_repos/virtualapp_research/experiment5/obj3_exp/model_generator_UPDATED.sql

from google.cloud import bigquery
import logging
import os
import re
import datetime
import math
import time

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/patrickattard/Downloads/utility-ratio-120412-c39bf207a207.json'

# anom_config_options=[0.95,0.9,0.85,0.8]
anom_config_options=[0.95]
model_options=[]
timestr = time.strftime("%Y%m%d-%H%M%S")
print(f"TIME : {timestr}")

formatted_filename=f"output_ml_FORMATTED_{timestr}.log"
output_filename=f"output_ml_{timestr}.log"

client = bigquery.Client()

# logging.basicConfig(level=logging.WARNING, format='%(message)s')
logging.basicConfig(
    filename=output_filename,
    filemode="a",
    level=logging.WARNING,
    format="%(message)s"
)

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

ground_truth={
    "telegram":{"total_sent":6, "total_deleted":6, "install_event":1, "infection_event":1, "uninstall_event":1},
    "signal":{"total_sent":2, "total_deleted":2, "install_event":1, "infection_event":1, "uninstall_event":1},
    "whatsapp":{"total_sent":4, "total_deleted":4, "install_event":1, "infection_event":1, "uninstall_event":1},
    "slack":{"total_sent":4, "total_deleted":4, "install_event":1, "infection_event":1, "uninstall_event":1},
    "skype":{"total_sent":4, "total_deleted":4, "install_event":1, "infection_event":1, "uninstall_event":1},
    "imo":{"total_sent":3, "total_deleted":3, "install_event":1, "infection_event":1, "uninstall_event":1},
    "kik":{"total_sent":3, "total_deleted":3, "install_event":1, "infection_event":1, "uninstall_event":1},
    "messenger":{"total_sent":3, "total_deleted":3, "install_event":1, "infection_event":1, "uninstall_event":1},
    "pushmessenger":{"total_sent":1, "total_deleted":1, "install_event":1, "infection_event":1, "uninstall_event":1},
    "whatsappbusiness":{"total_sent":2, "total_deleted":2, "install_event":1, "infection_event":1, "uninstall_event":1}
}

apps={
    "skype":
        {"table_query":'SELECT TIMESTAMP_MILLIS(nsp_data.createdTime) AS event_date, CONCAT("mid: ",sm.id ,"| uid: ",nsp_data.conversationId,"| in/out: ", nsp_data._isMyMessage ,"| message: ",sm.content,"| deleted: ",nsp_data.properties.deletetime) AS event_details, "skype_data" as source FROM `utility-ratio-120412.grr_skype.skype_data`  CROSS JOIN UNNEST(nsp_data._serverMessages) AS sm' ,
        "msg_id":'^.*cuid["]*:[\s"]*([0-9]{13})["|\|]',"start_date":"2023-02-23 06:58:18 UTC","end_date":"2023-02-23 07:44:42 UTC", 
        "msg_keyword_regex":"INSERT OR REPLACE INTO messagesv12.*,\"content\":\"(.*?)\"",
        "msg_keyword_position":1, 
        "table_name":"messagesv12",
        "propogation_query": """SELECT * FROM (SELECT Timestamp_seconds(time)  AS event_date, Concat(object.query_executed,"|",event) AS event_details,"new_jitmf_data" AS source, regexp_extract(object.query_executed, r'^*id["]*:[\s"]*([0-9]{13})["|\|]') AS id FROM   `utility-ratio-120412.grr_skype.new_jitmf_data`) t1 WHERE      t1.event_date >= "2023-02-23 06:58:18 UTC" AND        t1.event_date <= "2023-02-23 07:44:42 UTC" AND t1.event_details LIKE '%DHL: Your parcel is arriving, track here:%' AND        t1.event_details LIKE '%INSERT OR REPLACE INTO messagesv12%' AND  t1.event_details NOT LIKE '%""deletetime""%' AND        t1.id IS NOT NULL ORDER BY event_date""",
        "delete_query": """SELECT * FROM (SELECT Timestamp_seconds(time)  AS event_date, Concat(object.query_executed,"|",event)  AS event_details,"new_jitmf_data"  AS source, regexp_extract(object.query_executed, r'^*id["]*:[\s"]*([0-9]{13})["|\|]') AS id FROM   `utility-ratio-120412.grr_skype.new_jitmf_data`) t1 WHERE      t1.event_date >= "2023-02-23 06:58:18 UTC" AND        t1.event_date <= "2023-02-23 07:44:42 UTC" AND        t1.event_details LIKE '%DHL: Your parcel is arriving, track here:%' AND        t1.event_details LIKE '%INSERT OR REPLACE INTO messagesv12%' AND        t1.event_details LIKE '%"deletetime":"%' AND        t1.id is not null ORDER BY event_date""", #"""SELECT min(event_date), min(event_details), min(source) FROM (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source, REGEXP_EXTRACT(object.Query_Executed, r'^id["]:[\s"]*([0-9]{13})["|\|]') as ID FROM `utility-ratio-120412.grr_skype.new_jitmf_data`) WHERE event_date >= "2023-02-23 07:01:06 UTC"  AND event_date <= "2023-02-23 07:44:42 UTC" AND event_details LIKE '%messagesv12%' AND ((event_details LIKE '%1677135904835%' AND upper(event_details) LIKE '%DELETE%' ) OR (event_details LIKE '%1677136027986%' AND upper(event_details) LIKE '%DELETE%') OR (event_details LIKE '%1677136130994%'AND upper(event_details) LIKE '%DELETE%') OR (event_details LIKE '%1677136220922%' AND upper(event_details) LIKE '%DELETE%' )) GROUP BY ID""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_skype.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-23 06:58:18 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-23 07:44:42 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "telegram":
        {
         "table_query":'SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_2.telegram_data`' ,
         "msg_id":"^.*\(([0-9]{5}),.*\)", 
         "table_name":"messages_v2",
         "msg_keyword_regex":"REPLACE INTO messages_v2 VALUES\(([0-9]{5}\,.*)",
         "msg_keyword_position":6,
         "start_date":"2023-02-19 15:09:00 UTC",
         "end_date":"2023-02-19 15:51:32 UTC",
         "propogation_query": """SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_2.new_jitmf_data`  WHERE TIMESTAMP_SECONDS(time) >= "2023-02-19 15:10:00 UTC" AND TIMESTAMP_SECONDS(time) <= "2023-02-19 15:51:32 UTC" AND object.Query_Executed like '%DHL: Your parcel is arriving, track here: https://%flex%' AND object.Query_Executed not like '%18446744073709340000%';""",
         "delete_query": """SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_2.new_jitmf_data`  WHERE TIMESTAMP_SECONDS(time) >= "2023-02-19 15:10:00 UTC" AND TIMESTAMP_SECONDS(time) <= "2023-02-19 15:51:32 UTC" AND object.Query_Executed LIKE '%DELETE%' AND object.Query_Executed LIKE '%messages_v2%' AND (object.Query_Executed LIKE '%19031%' OR object.Query_Executed LIKE '%19032%' OR object.Query_Executed LIKE '%19033%' OR object.Query_Executed LIKE '%19034%' OR object.Query_Executed LIKE '%19035%' OR object.Query_Executed LIKE '%19036%')""",
         "infection_query": """SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_2.telegram_data` WHERE TIMESTAMP_SECONDS(date) >= "2023-02-19 15:09:00 UTC" AND TIMESTAMP_SECONDS(date) <= "2023-02-19 15:51:32 UTC" AND  data like '%DHL%'""",
         "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_2.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "signal":
        {"table_query": 'SELECT TIMESTAMP_MILLIS(cast(receipt_timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",recipient_id,"| in/out: ",receipt_timestamp,"| message: ",body) AS event_details, "signal_data" as source FROM `utility-ratio-120412.grr_signal.signal_data`',
         "msg_id":" _id = [REPLACED_ID]", "start_date":"2023-03-05 17:03:45 UTC","end_date":"2023-03-05 18:14:58 UTC", 
         "msg_keyword_regex":"INSERT INTO message.*VALUES (.*)",
         "msg_keyword_position":8,
         "table_name":"message",
         "propogation_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`) WHERE event_date >= "2023-03-05 17:03:45 UTC" AND event_date <= "2023-03-05 18:14:58" AND event_details like '%DHL: Your parcel is arriving, track here: https://flexisales.com/%' AND  event_details like '%INSERT INTO message%'""",
         "delete_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`) WHERE event_date >= "2023-03-05 17:03:45 UTC" AND event_date <= "2023-03-05 18:14:58" AND event_details LIKE '%DELETE FROM message%' and (event_details like '%12%' OR event_details like '%13%')""",
         "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_signal.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-03-05 17:03:45 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-03-05 18:14:58 UTC" AND  data like '%DHL%'""",
         "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "whatsapp":
        {"table_query":'SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsapp_data" as source FROM `utility-ratio-120412.grr_whatsapp.whatsapp_data`' ,
        "msg_id":"^.*,([1-9]),", "start_date":"2023-02-20 21:09:32 UTC","end_date":"2023-02-20 21:57:29 UTC", 
        "msg_keyword_regex":"INSERT INTO message.*VALUES (.*)",
        "msg_keyword_position":15,
        "table_name":"message",
        "propogation_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`) WHERE event_date >= "2023-02-20 21:09:32 UTC" AND event_date <= "2023-02-20 21:57:29"  AND event_details like '%DHL: Your parcel is arriving, track here: https://flexisales.com/%' AND  event_details like '%INSERT INTO message%'""",
        "delete_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`) WHERE event_date >= "2023-02-20 21:09:32 UTC" AND event_date <= "2023-02-27 21:57:29" and event_details like '%DELETE FROM message WHERE%' and event_details not like '%undefined%' and event_details not like '%null%'""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_whatsapp.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-20 21:09:32 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-20 21:57:29 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "slack":
        {"table_query": 'SELECT TIMESTAMP_MILLIS(cast(message_json.ts as int64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",channel_id,"| in/out: ",channel_id,"| message: ",message_json.text) AS event_details, "slack_data" as source FROM `utility-ratio-120412.grr_slack.slack_data`',
        "msg_id":"([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})",
        "channel_id":'"channel":"[A-Z0-9]{11}',"start_date":"2023-02-23 10:21:49 UTC","end_date":"2023-02-23 11:16:05 UTC",
        "msg_keyword_regex":"INSERT INTO messages.*\"text\":\"(.*?)\"",
        "msg_keyword_position":1, 
        "table_name":"messages",
        "propogation_query": """select * from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source, REGEXP_EXTRACT(object.Query_Executed, r'"channel":"[A-Z0-9]{11}"') as channel  FROM `utility-ratio-120412.grr_slack.new_jitmf_data`) t1 inner join ( select channel, max(event_date) as event_date from( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source, REGEXP_EXTRACT(object.Query_Executed, r'"channel":"[A-Z0-9]{11}"') as channel  FROM `utility-ratio-120412.grr_slack.new_jitmf_data`  ) WHERE event_date >= "2023-02-23 10:21:49 UTC"  AND event_date <= "2023-02-23 11:16:05 UTC"   AND event_details like '%DHL%'  AND  event_details like '%INSERT INTO messages%'  GROUP BY channel ) t2 on t1.event_date = t2.event_date WHERE t1.event_date >= "2023-02-23 10:21:49 UTC"  AND t1.event_date <= "2023-02-23 11:16:05 UTC"   AND t1.event_details like '%DHL%'  AND  t1.event_details like '%INSERT INTO messages%'""",
        "delete_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source, REGEXP_EXTRACT(object.Query_Executed, r'id[\s]=[\s]([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})') as id FROM `utility-ratio-120412.grr_slack.new_jitmf_data`) WHERE event_date >= "2023-02-23 10:21:49 UTC" AND event_date <= "2023-02-23 11:16:05 UTC" AND  event_details like '%DELETE FROM messages WHERE local_id = %' AND event_details not like '%local_id = null%' AND ((id LIKE '%dd2cea42-9224-444c-a6dd-a93e3b926c75%') OR (id LIKE '%9750d112-8115-4b7b-b082-edca7b5f04a0%') OR (id LIKE '%efcc467a-a0a3-4639-ba7d-b4d8235d719c%') OR (id LIKE '%c6a25b5e-0fb9-4bda-9685-831fc3d7369c%'))""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_slack.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-23 10:21:49 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-23 11:16:05 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "imo":
        {"table_query": 'SELECT TIMESTAMP_MILLIS(cast(timestamp/1000000 as INT64)) AS event_date, CONCAT("mid: ",msg_id,"| uid: ",group_msg_id,"| in/out: ",message_state,"| message: ",last_message) AS event_details, "imo_data" as source FROM `utility-ratio-120412.grr_imo.imo_data`' ,
        "msg_id": "\(([0-9]+).*,",
        "start_date":"2023-02-27 16:01:04 UTC","end_date":"2023-02-27 16:13:16 UTC", 
        "msg_keyword_regex":"INSERT INTO message.*VALUES (.*)",
        "msg_keyword_position":4,
        "table_name":"messages",
        "propogation_query": """ SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_imo.new_jitmf_data`) WHERE event_date >= "2023-02-27 16:01:04 UTC"  AND event_date <= "2023-02-27 16:13:16 UTC"  AND  event_details like '%INSERT INTO messages%' AND event_details like '%DHL%'""",
        "delete_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_imo.new_jitmf_data`) WHERE event_date >= "2023-02-27 16:01:04 UTC"  AND event_date <= "2023-02-27 16:13:16 UTC"  AND  event_details like '%DELETE FROM messages%' AND (event_details like '%1109846348587321%' or event_details like '%1106220383085401%' or event_details like '%1102962116618439%')""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_imo.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-27 16:01:04 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-27 16:13:16 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "kik":
        {"table_query": 'SELECT TIMESTAMP_MILLIS(cast(timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",uid,"| uid: ",partner_jid,"| in/out: ",was_me,"| message: ",body) AS event_details, "kik_data" as source FROM `utility-ratio-120412.grr_kik.kik_data`' ,
        "msg_id":"([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})", "start_date":"2023-02-27 09:58:35 UTC","end_date":"2023-02-27 13:11:14 UTC", 
        "msg_keyword_regex":"INSERT INTO messagesTable.*VALUES (.*)",
        "msg_keyword_position":6,
        "table_name":"messagesTable",
        "propogation_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_kik.new_jitmf_data`) WHERE event_date >= "2023-02-27 09:58:35 UTC"  AND event_date <= "2023-02-27 13:11:14 UTC"  AND  event_details like '%INSERT INTO messagesTable%' AND event_details like '%DHL%'""",
        "delete_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_kik.new_jitmf_data`) WHERE event_date >= "2023-02-27 09:58:35 UTC"  AND event_date <= "2023-02-27 13:11:14 UTC"   AND  event_details like '%DELETE FROM messagesTable%' """,
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_kik.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-27 09:58:35 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-27 13:11:14 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "messenger":
        {"table_query":'SELECT TIMESTAMP_MILLIS(timestamp_ms) AS event_date, CONCAT("mid: ",message_id,"| uid: ",sender_id,"| in/out: ",sender_id,"| message: ",text) AS event_details, "messenger_data" as source FROM `utility-ratio-120412.grr_messenger.messenger_data`' ,
        "msg_id":"(mid\.\$[a-z0-9A-Z]+)", "start_date":"2023-02-22 16:19:21 UTC","end_date":"2023-02-22 17:01:58 UTC", 
        "msg_keyword_regex":"INSERT OR IGNORE INTO messages VALUES(.*)",
        "msg_keyword_position":5,
        "table_name":"messages",
        "propogation_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_messenger.new_jitmf_data`) WHERE event_date >= "2023-02-22 16:19:21 UTC"  AND event_date <= "2023-02-22 17:01:58 UTC" AND event_details like '%DHL%' AND  event_details like '%INSERT OR IGNORE INTO messages VALUES%'""",
        "delete_query": """SELECT * FROM( SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_messenger.new_jitmf_data`) WHERE event_date >= "2023-02-22 16:19:21 UTC"  AND event_date <= "2023-02-22 17:01:58 UTC" AND  event_details like '%DELETE FROM messages WHERE ((message_id)%'""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_messenger.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-02-22 16:19:21 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-02-22 17:01:58 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    },
    "pushmessenger":
        {"table_query": 'SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "pushmessenger_data" as source FROM `utility-ratio-120412.grr_pushmessenger.pushmessenger_data`',
        "msg_id":"^.*\(([0-9]{5}),.*\)", 
        "start_date":"2023-03-15 12:34:25 UTC",
        "end_date":"2023-03-15 13:24:14 UTC", 
        "table_name":"messages_v2",
        "msg_keyword_regex":"REPLACE INTO messages_v2 VALUES\(([0-9]{5}\,.*)",
        "msg_keyword_position":6,
        "propogation_query": """SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_pushmessenger.new_jitmf_data`  WHERE TIMESTAMP_SECONDS(time) >= "2023-03-15 12:34:25 UTC" AND TIMESTAMP_SECONDS(time) <=  "2023-03-15 13:24:14 UTC" AND object.Query_Executed like '%DHL: Your parcel is arriving, track here: https://%flex%' AND object.Query_Executed not like '%18446744073709340000%' AND object.Query_Executed LIKE '%messages_v2%'""",
        "delete_query": """SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_pushmessenger.new_jitmf_data`  WHERE TIMESTAMP_SECONDS(time) >= "2023-03-15 12:34:25 UTC" AND TIMESTAMP_SECONDS(time) <=  "2023-03-15 13:24:14 UTC" AND object.Query_Executed LIKE '%DELETE%' AND object.Query_Executed LIKE '%messages_v2%' AND (object.Query_Executed LIKE '%19087%' OR object.Query_Executed LIKE '%19090%')""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_pushmessenger.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-03-15 12:34:25 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-03-15 13:24:14 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_pushmessenger.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    }
    ,
    "whatsappbusiness":
        {"table_query": 'SELECT TIMESTAMP_MILLIS(cast(timestamp as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsappbusiness_data" as source FROM `utility-ratio-120412.grr_whatsappbusiness.whatsappbusiness_data`',
        "msg_id":"^.*,([1-9]),", "start_date":"2023-03-18 10:54:48 UTC","end_date":"2023-03-18 11:36:21 UTC", 
        "msg_keyword_regex":"INSERT INTO message.*VALUES (.*)",
        "msg_keyword_position":15,
        "table_name":"message",
        "propogation_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsappbusiness.new_jitmf_data`) WHERE event_date >= "2023-03-18 10:54:48 UTC" AND event_date <= "2023-03-18 11:36:21 UTC"  AND event_details like '%DHL: Your parcel is arriving, track here: https://flexisales.com/%' AND  event_details like '%INSERT INTO message%'""",
        "delete_query": """SELECT  *  from (SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsappbusiness.new_jitmf_data`) WHERE event_date >= "2023-03-18 10:54:48 UTC" AND event_date <= "2023-03-18 11:36:21 UTC" and event_details like '%DELETE FROM message WHERE%' AND event_details not like '%undefined%' AND event_details not like '%null%'""",
        "infection_query": """SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_whatsappbusiness.all_telegram_data` WHERE PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") >= "2023-03-18 10:54:48 UTC" AND PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") <= "2023-03-18 11:36:21 UTC" AND  data like '%DHL%'""",
        "infection_install_query": """SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsappbusiness.AutoExportedAndroidPackageInfo` where packageName like '%.demo%'"""
    }
}

def generate_kmeans_models(seconds):
   for a in apps:
        name=a
        logging.critical(f"Generating model for {a}")
        app_name=a
        if a =="telegram":
            all_telegram_tbl_union=""
            app_name="2"
        else:
            all_telegram_tbl_union=f"""UNION ALL
                        SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""
        app_table_query=apps[a]["table_query"]
        app_start_date=apps[a]["start_date"]
        app_end_date=apps[a]["end_date"]
        app_table_name=apps[a]["table_name"]
        model_query=f"""CREATE TABLE IF NOT EXISTS `utility-ratio-120412.grr_app_models.{name}_table_{seconds}s` AS
                (
                    SELECT event_date, COUNT(*) as total_events, COUNTIF(source='AutoExportedAndroidCallLog') AS autoexportedandroidcalllog_data, COUNTIF(source='AutoExportedAndroidSmsMms') AS autoexportedandroidsmsmms_data, COUNTIF(source='AutoExportedAndroidSensorInfo') AS autoexportedandroidsensorinfo_data, COUNTIF(source='messenger_data') AS messenger_data, COUNTIF(source='AutoExportedAndroidPackageInfo') AS package_info_data, COUNTIF(source='new_jitmf_data') AS jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number, COUNTIF(CONTAINS_SUBSTR(event_details, "SELECT")) as select_number
                    FROM(
                        SELECT *
                        FROM 
                        (
                            SELECT TIMESTAMP_SECONDS({seconds} * DIV(unix_seconds(event_date), {seconds})) as event_date, event_details, source
                            from (
                                SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
                                UNION ALL
                                SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
                                UNION ALL
                                SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
                                UNION ALL
                                SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
                                UNION ALL
                                SELECT * from (
                                    select * from (
                                        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
                                )
                                WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%' or upper(event_details) like 'SELECT%'  )
                                AND (lower(event_details) like '% {app_table_name.lower} %' OR lower(event_details) like '% {app_table_name}(%')    
                                )
                                UNION ALL
                                {app_table_query}
                                {all_telegram_tbl_union}  
                            )
                            WHERE event_date IS NOT NULL 
                            AND event_date >= "{app_start_date}"
                            AND event_date <= "{app_end_date}"
                        )
                    )
                    GROUP BY event_date
                );
                
                CREATE MODEL IF NOT EXISTS `utility-ratio-120412.grr_app_models.{name}_kmeans_{seconds}s`
                OPTIONS(model_type='kmeans', standardize_features = true, num_clusters=1) AS
                SELECT * EXCEPT(event_date)
                FROM `utility-ratio-120412.grr_app_models.{name}_table_{seconds}s`
                """
        # logging.warning(model_query)
        # print(model_query)
        query_job = client.query(model_query) 
        results = query_job.result() 
        for row in results:
            logging.warning("{} : {} views".format(row.url, row.view_count))

def generate_autoencoder_models(seconds):
   for a in apps:
        name=a
        logging.critical(f"Generating model for {a}")
       
        model_query=f"""CREATE MODEL IF NOT EXISTS `utility-ratio-120412.grr_app_models.{name}_autoencoder_{seconds}s`
                OPTIONS(
                model_type='autoencoder',
                activation_fn='relu',
                batch_size=8,
                dropout=0.2,  
                hidden_units=[32, 16, 4, 16, 32],
                learn_rate=0.001,
                l1_reg_activation=0.0001,
                max_iterations=10,
                optimizer='adam'
                ) AS 
                SELECT * EXCEPT(event_date)
                FROM `utility-ratio-120412.grr_app_models.{name}_table_{seconds}s`;
                """
        # logging.warning(model_query)
        # print(model_query)
        query_job = client.query(model_query) 
        results = query_job.result() 
        for row in results:
            logging.warning("{} : {} views".format(row.url, row.view_count))

def generate_pca_models(seconds):
   for a in apps:
        name=a
        logging.critical(f"Generating model for {a}")
       
        model_query=f"""CREATE MODEL IF NOT EXISTS `utility-ratio-120412.grr_app_models.{name}_pca_{seconds}s`
                OPTIONS
                ( MODEL_TYPE='PCA', PCA_EXPLAINED_VARIANCE_RATIO=0.8) AS
                SELECT * EXCEPT(event_date)
                FROM `utility-ratio-120412.grr_app_models.{name}_table_{seconds}s`;
                """
        # logging.warning(model_query)
        query_job = client.query(model_query) 
        results = query_job.result() 
        for row in results:
            logging.warning("{} : {} views".format(row.url, row.view_count))

def generate_arima_models(discrepancy_query,name):
   for a in apps:
        logging.critical(f"Generating model: {name} for {a}")
        app_name=a
        model_name=a
        if a =="telegram":
            all_telegram_tbl_union=""
            app_name="2"
        else:
            all_telegram_tbl_union=f"""UNION ALL
                        SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""
        app_table_query=apps[a]["table_query"]
        app_start_date=apps[a]["start_date"]
        app_end_date=apps[a]["end_date"]
        app_table_name=apps[a]["table_name"]
        model_query=f"""CREATE MODEL IF NOT EXISTS `utility-ratio-120412.grr_app_models.{model_name}{name}`
                OPTIONS(
                    model_type='arima_plus',
                    TIME_SERIES_DATA_COL='discrepancy',
                    TIME_SERIES_TIMESTAMP_COL='ed',
                    DECOMPOSE_TIME_SERIES=TRUE
                )
                AS (
                    {discrepancy_query}
                    FROM(
                    SELECT ed, count(case when source like '%jitmf%' then 1 else null end) AS jitmf_value, count(case when source not like '%jitmf%' then 1 else null end) as non_jitmf_value
                    FROM 
                    (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
                    from (
                        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
                        UNION ALL
                        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
                        UNION ALL
                        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
                        UNION ALL
                        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
                        UNION ALL
                        SELECT * from (
                            select * from (
                                SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
                            )
                            WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%' or upper(event_details) like 'SELECT%'  )
                            AND (lower(event_details) like '% {app_table_name} %' OR lower(event_details) like '% {app_table_name}(%')    
                        )
                        UNION ALL
                        {app_table_query}
                        {all_telegram_tbl_union}  
                    )
                    WHERE event_date IS NOT NULL 
                    AND event_date >= "{app_start_date}"
                    AND event_date <= "{app_end_date}"
                    )
                    GROUP BY ed
                    )
                );"""
        # logging.warning(model_query)
        query_job = client.query(model_query) 
        results = query_job.result() 
        for row in results:
            logging.warning("{} : {} views".format(row.url, row.view_count))

def get_log_count(a):
    app_name = a
    if a =="telegram":
        all_telegram_tbl_union=""
        app_name="2"
    else:
        all_telegram_tbl_union=f"""UNION ALL
                    SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""
    app_table_query=apps[a]["table_query"]
    app_start_date=apps[a]["start_date"]
    app_end_date=apps[a]["end_date"]
    app_table_name=apps[a]["table_name"]
    model_query=f"""
                SELECT COUNT(*)
                FROM 
                (select TIMESTAMP_TRUNC(event_date, MINUTE) AS ed, * 
                from (
                    SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
                    UNION ALL
                    SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
                    UNION ALL
                    SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
                    UNION ALL
                    SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
                    UNION ALL
                    SELECT * from (
                        select * from (
                            SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
                        )
                        WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%' or upper(event_details) like 'SELECT%'  )
                        AND (lower(event_details) like '% {app_table_name} %' OR lower(event_details) like '% {app_table_name}(%')    
                    )
                    UNION ALL
                    {app_table_query}
                    {all_telegram_tbl_union}  
                )
                WHERE event_date IS NOT NULL 
                AND event_date >= "{app_start_date}"
                AND event_date <= "{app_end_date}"
                )"""
    # logging.warning(model_query)
    query_job = client.query(model_query) 
    results = query_job.result() 
    return int(next(results)[0])

def get_jitmf_events(my_app_name,aps,corr_conf):
    result_set=[]
    corr_conf_parts=corr_conf.split("|")
    corr_conf_p2=""
    for ap in aps:
        if my_app_name =="telegram":
            app_name="2"
        else:
            app_name=my_app_name

        app_table_name=apps[my_app_name]["table_name"]
        msg_keyword=apps[my_app_name]["msg_keyword_regex"] # checking if the entry is the type of insert that inserts messages
        msg_keyword_position=apps[my_app_name]["msg_keyword_position"]
        if len(corr_conf_parts)>1:
            corr_conf_p2=corr_conf_parts[1]
        query = f"""
            {corr_conf_parts[0]}
            from (
                select * from (
                    SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
                )
                WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'REPLACE%'  )
                AND (lower(event_details) like '% {app_table_name.lower()} %' OR lower(event_details) like '% {app_table_name.lower()}(%')    
                AND (  TIMESTAMP_SECONDS(time) >= '{ap.strftime("%Y-%m-%d %H:%M:00")}') 
                AND (  TIMESTAMP_SECONDS(time) <= '{ap.strftime("%Y-%m-%d %H:%M:59")}') 
                order by TIMESTAMP_SECONDS(time) asc
            )
            {corr_conf_p2}
            """
        query_job = client.query(query) 
        results = query_job.result() 
        results_time = set()
        for row in results:
            # matches = re.findall(msg_keyword, row[1], re.IGNORECASE)  # checking if the entry is the type of insert that inserts messages
            matches = re.finditer(msg_keyword, row[1], re.IGNORECASE)
            for match in matches:
                msg_keyword_match = match.group(1)
                if len(msg_keyword_match) and len(msg_keyword_match.split(",")) > msg_keyword_position:
                    content=(msg_keyword_match.split(",")[msg_keyword_position]).strip().rsplit(' ',1)[0]
                    if len(content)>=3 and content !="undefined" and content !="null" :
                        if my_app_name == "skype":
                            if row[0].strftime("%Y-%m-%d %H:%M:%S") not in results_time:
                                result_set.append(row) # We are only inserting 1 jitmf event per second to avoid duplication
                                results_time.add(row[0].strftime("%Y-%m-%d %H:%M:%S"))
                            else:
                                result_set = [rs for rs in result_set if rs[0].strftime("%Y-%m-%d %H:%M:%S") != row[0].strftime("%Y-%m-%d %H:%M:%S")]
                                result_set.append(row) # Updating this to the latest version
                        else:
                            result_set.append(row)

    return result_set

def correl_timeperiod(result_set,my_app_name,starttime,endtime):
    app_name=my_app_name
    if my_app_name =="telegram":
        all_telegram_tbl_union=""
        app_name="2"
    else:
        all_telegram_tbl_union=f"""UNION ALL
            SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""

    query = f"""select * from (select  *
        from (
            SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
            UNION ALL
            SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
            {all_telegram_tbl_union} 
        ) 
        WHERE event_date IS NOT NULL 
        AND event_date >= "{starttime.strftime("%Y-%m-%d %H:%M:%S")}"
        AND event_date <= "{endtime.strftime("%Y-%m-%d %H:%M:%S")}"
        )  """ 
    query_result = client.query(query) 
    for row in query_result:
        if row not in result_set:
            result_set.append(row)
    
    return result_set

def correl_string(result_set,my_app_name,keyword_content, correlate_other_jitmf_events=False, delete_only=False):
    app_table_query=apps[my_app_name]["table_query"]
    app_start_date=apps[my_app_name]["start_date"]
    app_end_date=apps[my_app_name]["end_date"]

    app_name=my_app_name
    if my_app_name =="telegram":
        all_telegram_tbl_union=""
        app_name="2"
    else:
        all_telegram_tbl_union=f"""UNION ALL
                    SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""
    app_table_name=apps[my_app_name]["table_name"]
    msg_keyword=apps[my_app_name]["msg_keyword_regex"]
    msg_keyword_position=apps[my_app_name]["msg_keyword_position"]
    keyword_content = keyword_content.replace('href=\\','href') # MANUALLY ESCAPING THIS
    query = f"""select * from (select  *
        from (
            SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
            UNION ALL
            SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
            UNION ALL
            --SELECT TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
            SELECT TIMESTAMP_SECONDS(time) AS event_date, event_details, source
            from (
            select * from (
                SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
            )
            WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
            AND (lower(event_details) like '% {app_table_name.lower()} %' OR lower(event_details) like '% {app_table_name.lower()}(%')    
            )
            --GROUP BY event_date
            UNION ALL
            {app_table_query}
            {all_telegram_tbl_union} 
        ) 
        WHERE event_date IS NOT NULL 
        AND event_date >= "{app_start_date}"
        AND event_date <= "{app_end_date}" 
        AND event_details like "%{keyword_content}%"
        {('AND (event_details like "DELETE FROM '+app_table_name+' %" OR event_details like "%deletetime%")' if delete_only else '')}
        )  """
    query_result = client.query(query) 
    # logging.error(query)
    results_time = set([x[0].strftime("%Y-%m-%d %H:%M:%S") for x in result_set])
    for row in query_result:
        if row not in result_set:
            if correlate_other_jitmf_events and row[2] == "new_jitmf_data":
                matches = re.finditer(msg_keyword, row[1], re.IGNORECASE)
                if list(matches):
                    if my_app_name == "skype":
                        if row[0].strftime("%Y-%m-%d %H:%M:%S") not in results_time: # We are only inserting 1 jitmf event per second to avoid duplication
                            result_set.append(row)
                            results_time.add(row[0].strftime("%Y-%m-%d %H:%M:%S"))
                        elif list(matches):
                            result_set = [rs for rs in result_set if rs[0].strftime("%Y-%m-%d %H:%M:%S") != row[0].strftime("%Y-%m-%d %H:%M:%S")]
                            result_set.append(row) # Updating this to the latest version
                    else:
                        result_set.append(row)
            else:
                result_set.append(row)
    
    return result_set

def filter_and_correlate(result_set,my_app_name):
    # logging.error(f"OG RESULT SET: {len(result_set)}")
    correlated_results=[]
    mid_regex=apps[my_app_name]["msg_id"]
    msg_keyword=apps[my_app_name]["msg_keyword_regex"]
    msg_keyword_position=apps[my_app_name]["msg_keyword_position"]

    logging.error(f"Result Set OG:>> {result_set}")
    # correlate feature: message content
    for row in result_set:
        msg_keyword_match=""
        mid_keyword_match=""
        logcontent=row[1]
        logging.warning(f"LOGCONTENT>> {logcontent}")

        if row not in correlated_results:
            logging.error(f"ROW>>> {row}")
            correlated_results.append(row)
        
        content=""
        
        matches = re.finditer(msg_keyword, logcontent, re.IGNORECASE)
        for match in matches:
            msg_keyword_match = match.group(1)
            logging.error(f"MSG_KEYWORD_MATCH>>> {msg_keyword_match}")
            break

        # MSG_KEYWORD_MATCH>>> DHL: Your parcel is arriving, track here: <a href=\
        logging.error(f"keyword len {len(msg_keyword_match.split(','))}")
        if len(msg_keyword_match) and len(msg_keyword_match.split(",")) > msg_keyword_position:
        # if len(msg_keyword_match) and msg_keyword_position:
            if "http" in content: # because /2 needs to be moder than floor() = 1
                content=(msg_keyword_match.split(",")[msg_keyword_position]).strip().rsplit(' ',1)[0]
            else:
                content=(msg_keyword_match.split(",")[msg_keyword_position]).strip()
            logging.error(f"content to correlate>> {content}")
            if len(content)>=3 and content !="undefined" and content !="null" :
                if "http" in content: # because /2 needs to be moder than floor() = 1
                    content=content[0:math.floor(len(content)/2)-1]
                correlated_results=correl_string(correlated_results,my_app_name,content,correlate_other_jitmf_events=True)

    # correlate feature: message id (getting ID for signal is different)
    if my_app_name=="signal":
        if len(correlated_results)>0:
            sortedlist = sorted(correlated_results, key=lambda d: d[0]) 
            starttime=sortedlist[0][0]
            endtime=sortedlist[-1][0]
            query=f"""select * from (
                    select  REGEXP_EXTRACT((event_details), r'^.*message._id = ([0-9]+)') as ID from (
                    SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`  )
                    WHERE event_date >= '{starttime}' 
                    AND event_date <= '{endtime}'
                    AND event_details like '%message._id =%'
                    AND  event_details like '%message%'
                    group by ID)
                    where ID is not null"""
            logging.error(query)
            query_job = client.query(query) 
            msg_ids=[x[0] for x in query_job]
            for mid in msg_ids:
                content=apps[my_app_name]["msg_id"].replace("[REPLACED_ID]",mid)
                logging.error(f"content to correlate>> {content}")
                correlated_results=correl_string(correlated_results,my_app_name,content, delete_only=True)
    else:
        for row in correlated_results:
            msg_keyword_match=""
            mid_keyword_match=""
            content=""
            logcontent=row[1]
            matches = re.finditer(msg_keyword, logcontent, re.IGNORECASE)
            for match in matches:
                msg_keyword_match = match.group(1)
                break

            if len(msg_keyword_match) and  len(msg_keyword_match.split(",")) > msg_keyword_position:
                # content=msg_keyword_match.split(",")[msg_keyword_position]
                logging.error(f">>>>>MSG_KEYWORKD>> {msg_keyword} >>>>LOG CONTENT>> {logcontent} >>>>>CONTENT>> {content} >>>>>mid regex>> {mid_regex}")
                matches = re.finditer(mid_regex, logcontent, re.IGNORECASE)
                for match in matches:
                    # logging.error(f"{match.group(1)}")
                    mid_keyword_match = match.group(1)
                    content=mid_keyword_match
                    break

                if len(content) and content !="undefined" and content !="null":
                    correlated_results=correl_string(correlated_results,my_app_name,content, delete_only=True)

    # logging.warning(f"CORREL RESULTS AFTER MID: {len(correlated_results)}")
    #  correlate feature: timeperiod > correlate other sources in that time period:
    if len(correlated_results) > 0:
        sortedlist = sorted(correlated_results, key=lambda d: d[0]) 
        # logging.warning(f"Start time: {sortedlist[0][0]}< End time: > {sortedlist[-1][0]}")
        correlated_results=correl_timeperiod(correlated_results,my_app_name,sortedlist[0][0],sortedlist[-1][0])

    return correlated_results

def evaluation(result_set,correlated_events,app):
    # Evaluation
    total_inserts = 0
    total_deletes = 0
    found_inserts = 0
    found_inserts_from_anomalies = 0
    found_deletes = 0
    found_deletes_from_anomalies = 0
    total_infection_inserts=0
    found_infection_inserts=0
    found_infection_inserts_from_anomalies=0
    total_install=0
    found_install=0
    found_install_from_anomalies=0
    total_uninstall=0
    found_uninstall=0
    found_uninstall_from_anomalies=0

    events={
        "infection_event":{},
        "install_event":{},
        "send_event":{},
        "delete_event":{},
        "uninstall_event":{},
        "infection_event_from_anomalies":{},
        "install_event_from_anomalies":{},
        "send_event_from_anomalies":{},
        "delete_event_from_anomalies":{},
        "uninstall_event_from_anomalies":{}
    }    

    query_job = client.query(apps[app]["propogation_query"])
    if app == "skype": # Added this to group by 1 second each event for skype
        id_to_rows_to_match = {}
        for row in query_job:
            row_id = row[3]
            if row_id not in id_to_rows_to_match:
                id_to_rows_to_match[row_id] = []
                total_inserts += 1
            id_to_rows_to_match[row_id].append(row)
        
        for key in id_to_rows_to_match:
            for row_to_match in id_to_rows_to_match[key]:
                if any(x[0] == row_to_match[0] and x[1] == row_to_match[1] for x in correlated_events):
                    found_inserts += 1
                    break

            for row_to_match in id_to_rows_to_match[key]:
                if any(x[0] == row_to_match[0] and x[1] == row_to_match[1] for x in result_set):
                    found_inserts_from_anomalies += 1
                    break
    else:
        for row in query_job:
            total_inserts += 1
            found_inserts += (1 if any(x[0] == row[0] and x[1] == row[1] for x in correlated_events) else 0)
            found_inserts_from_anomalies += (1 if any(x[0] == row[0] and x[1] == row[1] for x in result_set) else 0)
    events["send_event"]["tp"]=found_inserts
    events["send_event"]["fp"]=len(correlated_events)-found_inserts
    events["send_event"]["fn"]=max(total_inserts-found_inserts,0)

    events["send_event_from_anomalies"]["tp"]=found_inserts_from_anomalies
    events["send_event_from_anomalies"]["fp"]=len(result_set)-events["send_event_from_anomalies"]["tp"]
    events["send_event_from_anomalies"]["fn"]=max(total_inserts-events["send_event_from_anomalies"]["tp"],0)

    query_job = client.query(apps[app]["delete_query"]) 
    if app == "skype": # Added this to group by 1 second each event for skype
        id_to_rows_to_match = {}
        for row in query_job:
            row_id = row[3]
            if row_id not in id_to_rows_to_match:
                id_to_rows_to_match[row_id] = []
                total_deletes += 1
            id_to_rows_to_match[row_id].append(row)
        
        for key in id_to_rows_to_match:
            for row_to_match in id_to_rows_to_match[key]:
                if any(x[0] == row_to_match[0] and x[1] == row_to_match[1] for x in correlated_events):
                    found_deletes += 1
                    break

            for row_to_match in id_to_rows_to_match[key]:
                if any(x[0] == row_to_match[0] and x[1] == row_to_match[1] for x in result_set):
                    found_deletes_from_anomalies += 1
                    break
    else:
        for row in query_job:
            total_deletes += 1
            found_deletes += (1 if any(row[0] == x[0] and x[1] == row[1] for x in correlated_events) else 0)
            found_deletes_from_anomalies += (1 if any(row[0] == x[0] and x[1] == row[1] for x in result_set) else 0)
    events["delete_event"]["tp"]=found_deletes
    events["delete_event"]["fp"]=len(correlated_events)-found_deletes
    events["delete_event"]["fn"]=max(total_deletes-found_deletes,0)

    events["delete_event_from_anomalies"]["tp"]=found_deletes_from_anomalies
    events["delete_event_from_anomalies"]["fp"]=len(result_set)-events["delete_event_from_anomalies"]["tp"]
    events["delete_event_from_anomalies"]["fn"]=max(total_deletes-events["delete_event_from_anomalies"]["tp"],0)

    query_job = client.query(apps[app]["infection_query"]) 
    for row in query_job:
        total_infection_inserts += 1
        found_infection_inserts += (1 if any(x[0] == row[0] and x[1] == row[1] for x in correlated_events) else 0)
        found_infection_inserts_from_anomalies += (1 if any(x[0] == row[0] and x[1] == row[1] for x in result_set) else 0)
    events["infection_event"]["tp"]=found_infection_inserts
    events["infection_event"]["fp"]=len(correlated_events)-found_infection_inserts
    events["infection_event"]["fn"]=max(total_infection_inserts-found_infection_inserts,0)

    events["infection_event_from_anomalies"]["tp"]=found_infection_inserts_from_anomalies
    events["infection_event_from_anomalies"]["fp"]=len(result_set)-events["infection_event_from_anomalies"]["tp"]
    events["infection_event_from_anomalies"]["fn"]=max(total_infection_inserts-events["infection_event_from_anomalies"]["tp"],0)

    query_job = client.query(apps[app]["infection_install_query"]) 
    for row in query_job:
        total_install += 1
        found_install += (1 if any(row[0] == x[0] and x[1] == row[1] for x in correlated_events) else 0)
        found_install_from_anomalies += (1 if any(row[0] == x[0] and x[1] == row[1] for x in result_set) else 0)
    events["install_event"]["tp"]=found_install
    events["install_event"]["fp"]=len(correlated_events)-found_install
    events["install_event"]["fn"]=max(total_install-found_install,0)

    events["install_event_from_anomalies"]["tp"]=found_install_from_anomalies
    events["install_event_from_anomalies"]["fp"]=len(result_set)-events["install_event_from_anomalies"]["tp"]
    events["install_event_from_anomalies"]["fn"]=max(total_install-events["install_event_from_anomalies"]["tp"],0)

    found_uninstall=0
    total_uninstall=1
    events["uninstall_event"]["tp"]=found_uninstall
    events["uninstall_event"]["fp"]=len(correlated_events)-found_uninstall
    events["uninstall_event"]["fn"]=max(total_uninstall-found_uninstall,0)

    events["uninstall_event_from_anomalies"]["tp"]=found_uninstall_from_anomalies
    events["uninstall_event_from_anomalies"]["fp"]=len(result_set)-found_uninstall_from_anomalies
    events["uninstall_event_from_anomalies"]["fn"]=max(total_uninstall-found_uninstall_from_anomalies,0)

    logging.critical(correlated_events)
        
    logging.critical(f"APP>>>> {app} ANOMALIES DETECTED: {len(result_set)} CORRELATED EVENTS: {len(correlated_events)}")
    logging.critical(f"MSG INFECTION EVENT>>>> Ground truth: {ground_truth[app]['infection_event']} Total : {total_infection_inserts} Found : {found_infection_inserts} ")
    logging.critical(f"MSG INSTALL EVENT>>>> Ground truth: {ground_truth[app]['install_event']} Total : {total_install} Found : {found_install} ")
    logging.critical(f"MSG SEND EVENT>>>> Ground truth: {ground_truth[app]['total_sent']} Total : {total_inserts} Found : {found_inserts} ")
    logging.critical(f"MSG DELETE EVENT>>>> Ground truth: {ground_truth[app]['total_deleted']} Total : {total_deletes} Found : {found_deletes} ")
    logging.critical(f"MSG UNINSTALL EVENT>>>> Ground truth: {ground_truth[app]['uninstall_event']} Total : {total_uninstall} Found : {found_uninstall} ")
    logging.critical(f"MSG OVERALL PRECISION>>>> ")

    global formatted_filename

    total_logs = get_log_count(app)

    total_tp = events["send_event"]["tp"] + events["delete_event"]["tp"] + events["infection_event"]["tp"] + events["install_event"]["tp"]
    total_fp = (len(correlated_events) - total_tp)
    total_fn = events["send_event"]["fn"] + events["delete_event"]["fn"] + events["infection_event"]["fn"] + events["install_event"]["fn"]
    total_tn = total_logs - total_tp
    for e in events:
        if "_from_anomalies" in e:
            continue
        if((events[e]["tp"]+events[e]["fp"])==0):
            precision="-"
        else:
            precision=events[e]["tp"]/(events[e]["tp"]+events[e]["fp"])
        if((events[e]["tp"]+events[e]["fp"])==0):
            recall="-"
        else:
            recall=events[e]["tp"]/(events[e]["tp"]+events[e]["fn"])
        if (precision==0 and recall==0) or (precision=="-") or (recall=="-"):
            f1="-"
        else:
            f1=2*((precision*recall)/(precision+recall))

        output(app+","+e+","+str(recall)+","+str(precision)+","+str(f1),formatted_filename)

    
    # Calculate overall precision and f1
    if((total_tp+total_fp)==0):
        precision="-"
    else:
        precision=total_tp/(total_tp+total_fp)
    if((total_tp+total_fp)==0):
        recall="-"
    else:
        recall=total_tp/(total_tp+total_fn)
    if (precision==0 and recall==0) or (precision=="-") or (recall=="-"):
        f1="-"
    else:
        f1=2*((precision*recall)/(precision+recall))

    if((total_tn + total_fp)==0):
        specificity="-"
    else:
        specificity=total_tn/(total_tn + total_fp)
        
    output(app+",overall,"+str(recall)+","+str(precision)+","+str(f1)+","+str(specificity)+","+str(total_tp + total_fp)+","+str(total_logs)+","+str((total_tp + total_fp)/total_logs),formatted_filename)

    total_tp_from_anomalies = events["send_event_from_anomalies"]["tp"] + events["delete_event_from_anomalies"]["tp"] + events["infection_event_from_anomalies"]["tp"] + events["install_event_from_anomalies"]["tp"]
    total_fp_from_anomalies = (len(result_set) - total_tp_from_anomalies)
    total_fn_from_anomalies = events["send_event_from_anomalies"]["fn"] + events["delete_event_from_anomalies"]["fn"] + events["infection_event_from_anomalies"]["fn"] + events["install_event_from_anomalies"]["fn"]
    for e in events:
        if not "_from_anomalies" in e:
            continue
        if((events[e]["tp"]+events[e]["fp"])==0):
            precision="-"
        else:
            precision=events[e]["tp"]/(events[e]["tp"]+events[e]["fp"])
        if((events[e]["tp"]+events[e]["fp"])==0):
            recall="-"
        else:
            recall=events[e]["tp"]/(events[e]["tp"]+events[e]["fn"])
        if (precision==0 and recall==0) or (precision=="-") or (recall=="-"):
            f1="-"
        else:
            f1=2*((precision*recall)/(precision+recall))
        output(app+","+e+","+str(recall)+","+str(precision)+","+str(f1),formatted_filename)
    

    if((total_tp_from_anomalies+total_fp_from_anomalies)==0):
        precision="-"
    else:
        precision=total_tp_from_anomalies/(total_tp_from_anomalies+total_fp_from_anomalies)
    if((total_tp_from_anomalies+total_fp_from_anomalies)==0):
        recall="-"
    else:
        recall=total_tp_from_anomalies/(total_tp_from_anomalies+total_fn_from_anomalies)
    if (precision==0 and recall==0) or (precision=="-") or (recall=="-"):
        f1="-"
    else:
        f1=2*((precision*recall)/(precision+recall))
    output(app+",overall_from_anomalies,"+str(recall)+","+str(precision)+","+str(f1)+","+str(total_tp_from_anomalies + total_fp_from_anomalies)+","+str(total_logs)+","+str((total_tp_from_anomalies + total_fp_from_anomalies)/total_logs),formatted_filename)  

def arima_vanilla(threshold,name,corr_conf):
    global method
    method="vanilla_arima"
    # WHERE is_anomaly=true AND round(discrepancy)>0 - discrepancy >0
    for app in apps:
        query = f"""
            SELECT * 
            FROM ML.DETECT_ANOMALIES(
            MODEL `utility-ratio-120412.grr_app_models.{app}{name}`,
            STRUCT ({str(threshold)} AS anomaly_prob_threshold))
            WHERE is_anomaly=true
            ORDER BY ed"""

        query_job = client.query(query) 
        timeslices=[]
        
        for row in query_job:
            date_query_result=str(row[0])[:-9]
            date_query_result_in_dt=datetime.datetime.strptime(date_query_result,"%Y-%m-%d %H:%M")
            timeslices.append(date_query_result_in_dt)

        logging.error(f"APP: {app}")
        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%s") for x in timeslices])

        result_set=get_jitmf_events(app,timeslices,corr_conf)

        correlated_events=filter_and_correlate(result_set,app)
        evaluation(result_set,correlated_events,app)

def arima_add_minute(threshold,name,corr_conf):
    global method
    method="arima_add_minute"
    for app in apps:
        new_timeslices=[]
        query = f"""
            SELECT * 
            FROM ML.DETECT_ANOMALIES(
            MODEL `utility-ratio-120412.grr_app_models.{app}{name}`,
            STRUCT ({str(threshold)} AS anomaly_prob_threshold))
            where is_anomaly=true
            ORDER BY ed"""

        query_job = client.query(query) 
        timeslices=[]
        
        for row in query_job:
            date_query_result=str(row[0])[:-9]
            date_query_result_in_dt=datetime.datetime.strptime(date_query_result,"%Y-%m-%d %H:%M")
            timeslices.append(date_query_result_in_dt)

        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%s") for x in timeslices])
        for t in timeslices:
            new_t=t + datetime.timedelta(minutes=1)
            if new_t not in timeslices and new_t not in new_timeslices:
                new_timeslices.append(new_t)
        
        for t in new_timeslices:
            timeslices.append(t)

        # print("TLAQNA!")
        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%s") for x in timeslices])
        result_set=get_jitmf_events(app,timeslices,corr_conf)

        logging.warning(f"APP: {app}")
        correlated_events=filter_and_correlate(result_set,app)

        evaluation(result_set,correlated_events,app)

def output(str,filename):
    logging.warning(str)
    print(str)
    with open(filename,'a') as f:
        f.write(str+"\n")

def classification_vanilla(threshold, corr_conf, classification_type,seconds):
    time_position={'autoencoder':2,'pca':2,'kmeans':3}
    global method
    method="vanilla_classification"
    for app in apps:
        query = f"""
            SELECT *
            FROM
            ML.DETECT_ANOMALIES(MODEL `utility-ratio-120412.grr_app_models.{app}_{classification_type}_{seconds}s`,
            STRUCT({str(threshold)} AS contamination),
            TABLE `utility-ratio-120412.grr_app_models.{app}_table_{seconds}s`)
            WHERE is_anomaly = true
            ORDER BY event_date"""

        query_job = client.query(query) 
        timeslices=[]
        
        for row in query_job:
            timeslices.append(row[time_position[classification_type]])

        logging.error(f"APP: {app}")
        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%S") for x in timeslices])

        result_set=get_jitmf_events(app,timeslices,corr_conf)

        correlated_events=filter_and_correlate(result_set,app)
        evaluation(result_set,correlated_events,app)

def classification_add_minute(threshold, corr_conf, classification_type,seconds):
    time_position={'autoencoder':2,'pca':2,'kmeans':3}
    global method
    method="classification_add_minute"
    for app in apps:
        new_timeslices=[]
        query = f"""
            SELECT * 
            FROM ML.DETECT_ANOMALIES(
            MODEL `utility-ratio-120412.grr_app_models.{app}_{classification_type}_{seconds}s`,
            STRUCT ({str(threshold)} AS contamination),
            TABLE `utility-ratio-120412.grr_app_models.{app}_table_{seconds}s`)
            where is_anomaly=true
            ORDER BY event_date"""

        query_job = client.query(query) 
        timeslices=[]
        for row in query_job:
            timeslices.append(row[time_position[classification_type]])

        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%S") for x in timeslices])
        for t in timeslices:
            new_t=t + datetime.timedelta(minutes=1)
            if new_t not in timeslices and new_t not in new_timeslices:
                new_timeslices.append(new_t)
        
        for t in new_timeslices:
            timeslices.append(t)

        # print("TLAQNA!")
        logging.error([datetime.datetime.strftime(x,"%Y-%m-%d %H:%M:%S") for x in timeslices])
        result_set=get_jitmf_events(app,timeslices,corr_conf)

        logging.warning(f"APP: {app}")
        correlated_events=filter_and_correlate(result_set,app)

        evaluation(result_set,correlated_events,app)

arima_model_options=["arima_vanilla"]
arima_config_anom_thresholds=[0.95,0.9,0.85]
arima_config_anom_log_attributes={"_st_scaler_NO_greatest":"SELECT ed, (ML.STANDARD_SCALER(jitmf_value) OVER()) - (ML.STANDARD_SCALER(non_jitmf_value) OVER()) as discrepancy","ARIMA_MIN_MAX_SCALER":"SELECT ed, (ML.MIN_MAX_SCALER(jitmf_value) OVER()) - (ML.MIN_MAX_SCALER(non_jitmf_value) OVER()) as discrepancy"}
config_correlation=["SELECT TIMESTAMP_SECONDS(time) AS event_date,  event_details, source"]

for c in arima_config_anom_log_attributes:    
    # generate_arima_models(arima_config_anom_log_attributes[c],c)
    for m in arima_model_options:
        for t in arima_config_anom_thresholds:
            for corr_conf in config_correlation:
                output("Anomaly Detection (AD) Method: "+m,formatted_filename)
                output("Anomaly Detection (AD) Threashold: "+str(t),formatted_filename)
                output("Log attributes for AD: "+c,formatted_filename)
                output("Feature Correlation: "+corr_conf,formatted_filename)

                globals()[m](t,c,corr_conf)
                time.sleep(5)

classification_model_options=["classification_vanilla"]
classification_config_anom_thresholds=[0.5,0.4, 0.3, 0.2, 0.1]
classification_time_grouping=["30","60"]
classification_type = ['kmeans','autoencoder','pca']
config_correlation=["SELECT TIMESTAMP_SECONDS(time) AS event_date,  event_details, source"]

for ct in classification_type:
    for tg in classification_time_grouping:
        # globals()["generate_"+ct+"_models"](tg)
        for m in classification_model_options:
            for t in classification_config_anom_thresholds:
                for corr_conf in config_correlation:
                    output("Anomaly Detection (AD) Method: >"+ct+" < Mode: > "+m+"<  Time grouping: >"+tg+"<",formatted_filename)
                    output("Anomaly Detection (AD) Threashold: "+str(t),formatted_filename)
                    output("Log attributes for AD: "+ct,formatted_filename)
                    output("Feature Correlation: "+corr_conf,formatted_filename)

                    globals()[m](t,corr_conf, ct,tg)
                    time.sleep(5)