ADB_DEVICE="96RAY0YTEX"
if [[ "$OSTYPE" == "linux-gnu"* ]]; then #HOME PC
    EXPERIMENT_5_DIR=/mnt/2TBdrive/gitlab/experiment_5
elif [[ "$OSTYPE" == "darwin"* ]]; then #LAPTOP
    EXPERIMENT_5_DIR=/Users/jennifer/gitlab_repos/virtualapp_research/experiment5
fi


DRIVER_LOCATION="$EXPERIMENT_5_DIR/obj3_exp/setup_on_phone/drivers/template.js"
VIRTUALAPPAPDIR="$EXPERIMENT_5_DIR/obj3_exp/setup_on_phone/virtualapp.apk"
VIRTUALAPPPKGNAME="com.example.trustedcontainervirtualapp"

adb -s $ADB_DEVICE shell pm clear $VIRTUALAPPPKGNAME
adb -s $ADB_DEVICE uninstall $VIRTUALAPPPKGNAME
adb -s $ADB_DEVICE install -g $VIRTUALAPPAPDIR

apk_to_push="$1"
pkgname=`basename $apk_to_push .apk`

old_apk=`adb -s $ADB_DEVICE shell ls /sdcard/*.apk 2>/dev/null`
adb -s $ADB_DEVICE shell rm -rf $old_apk
adb -s $ADB_DEVICE push $apk_to_push /sdcard
if [[ "$OSTYPE" == "linux-gnu"* ]]; then #HOME PC
    sed -i 's/var APP = "any";/var APP = "'$pkgname'";/g' $DRIVER_LOCATION
elif [[ "$OSTYPE" == "darwin"* ]]; then #LAPTOP
    sed -i -e 's/var APP = "any";/var APP = "'$pkgname'";/g' $DRIVER_LOCATION
fi
adb -s $ADB_DEVICE push $DRIVER_LOCATION /data/local/tmp/frida-scripts/script1.js 
if [[ "$OSTYPE" == "linux-gnu"* ]]; then #HOME PC
    sed -i 's/var APP = "'$pkgname'";/var APP = "any";/g' $DRIVER_LOCATION
elif [[ "$OSTYPE" == "darwin"* ]]; then #LAPTOP
    sed -i -e 's/var APP = "'$pkgname'";/var APP = "any";/g' $DRIVER_LOCATION
fi

adb shell rm -f /sdcard/jitmflogs/*


