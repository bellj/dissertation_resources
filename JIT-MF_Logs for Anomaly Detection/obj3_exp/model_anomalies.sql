 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.whatsapp`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.telegram`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.messenger`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.kik`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.imo`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.whatsappbusiness`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.pushmessenger`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.slack`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.skype`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;

 SELECT * 
FROM ML.DETECT_ANOMALIES(
    MODEL `utility-ratio-120412.grr_app_models.signal`,
    STRUCT (0.95 AS anomaly_prob_threshold))
ORDER BY anomaly_probability DESC;