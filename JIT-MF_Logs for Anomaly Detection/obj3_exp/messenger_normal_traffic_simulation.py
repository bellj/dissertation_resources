import re
import sys
import time
import os
import random
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

MAX_WAIT=10
_s = 20
_s2 = 1
_s3 = 1

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def send_message(v,   msg_counter):
    # sending message
    v.touch()
    vc.sleep(_s3)
    dump_handler()    
    view = vc.findViewsWithAttribute('class', 'android.widget.EditText')[0]
    view.touch()
    message_text="Hey, how are you? "+str(msg_counter)+"_"+str(get_random_string(15))
    msg_counter+=1
    view.setText(message_text)
    vc.sleep(_s2)
    dump_handler()
    vc.sleep(_s2)
    vc.findViewWithContentDescriptionOrRaise(u'''Send''').touch()
    print("MESSENGER | Normal traffic | Sent message text="+message_text+". Current time ... ----> "+ str(datetime.now()))
    dump_handler()
    vc.sleep(_s2)
    
def get_contacts():
    views=[]
    views.append(vc.findViewByIdOrRaise("id/no_id/10"))
    views.append(vc.findViewByIdOrRaise("id/no_id/11"))
    views.append(vc.findViewByIdOrRaise("id/no_id/12"))

    return views

def dump_handler():
    number_of_retries = 10
    sleep_time = 5
    counter = 0
    e=None
    while (counter < number_of_retries):
        try:
            vc.dump(window=-1)
            return
        except Exception as exc:
            e=exc
            time.sleep(sleep_time)
            counter += 1
    print("[[ FAILED TO EXECUTE VC.DUMP LATEST EXCEPTION ]]" + str(e))

device = sys.argv[1]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('com.example.trustedcontainervirtualapp/com.example.trustedcontainervirtualapp.MainActivity')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.sleep(_s+20)
dump_handler()
views=get_contacts()

print(len(views))
for i in range(2):
    randint = random.randint(1,(len(views)-1))
    v=views[randint]
    send_message(v,i)
    vc.findViewWithContentDescriptionOrRaise(u'''Back''').touch()
    vc.sleep(_s2+ random.randint(1,10))

device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s2)
device.shell('input keyevent KEYCODE_HOME')
