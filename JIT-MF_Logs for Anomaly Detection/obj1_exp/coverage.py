# libraries.json (library details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/getlibraries
# json[1-12].json (most downloaded 550 Google Playstore app details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/browse offset 0 limit 50 (till offset 500)

import json
import os
import matplotlib.pyplot as plt
import numpy as np

# directory = '/mnt/2TBdrive/test/exp4/PT4_COVERAGE/'
directory = './results'

# FINANCE - NETWORK
def run_stats(marketCategory, specific_lib,sorted_libs=None):
    # print(" ******************************************** "+marketCategory+" "+specific_lib+" ******************************************** ")
    files = [os.path.join(directory,f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory,f)) if marketCategory in f]
    og_data={}
    libraries_data={}
    communication_apps=0


    for filename in files:
        with open(filename) as json_file:
            data = json.load(json_file)
            for app in data['apps']:
                if app["marketCategory"] == marketCategory:
                    communication_apps+=1
                if 'libraries' in app:
                    og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": app['libraries']}
                else:
                    og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": []}

    total_apps=len(og_data.keys())

    with open(os.path.join(directory,"libraries.json")) as lib_json_file:
        data_2 = json.load(lib_json_file)
        for lib in data_2['libraries']:
            libraries_data[lib['id']]={"name": lib['name'], "id": lib['id'], "tags":lib['tags'], "count_all":0,"count_finance":0, "app_list":[]}

    for app in og_data:
        string_to_print=og_data[app]['marketCategory']+"~"+og_data[app]['name']
        for l in libraries_data:
            if specific_lib in libraries_data[l]["tags"]:
                if l in og_data[app]["libraries"]:
                    string_to_print+="~1"
                    libraries_data[l]["count_all"]+=1
                    libraries_data[l]["app_list"].append(app)
                else:
                    string_to_print+="~0"

    filtered_libs = list(filter(lambda x: (specific_lib in x['tags']), libraries_data.values()))
    if not sorted_libs:
        sorted_libs = sorted(filtered_libs, key = lambda x: x["count_all"], reverse=True)
    else:
        for l in sorted_libs:
            l["count_all"] = libraries_data[l["id"]]["count_all"]
            l["app_list"] = libraries_data[l["id"]]["app_list"]

    apps_found=set()

    print("\n"+specific_lib+", "+marketCategory)
    print(" ,0")
    for l in sorted_libs:
        apps_found.update(l["app_list"])
        perc_apps=(len(apps_found)/total_apps)
        print(l["name"]+","+str(perc_apps))
    return sorted_libs

sorted_libs_network=run_stats("POPULAR","network")
sorted_libs_database=run_stats("POPULAR","database")
run_stats("FINANCE","network",sorted_libs_network)
run_stats("FINANCE","database",sorted_libs_database)
run_stats("COMMUNICATION","network",sorted_libs_network)
run_stats("COMMUNICATION","database",sorted_libs_database)




# libraries.json (library details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/getlibraries
# json[1-12].json (most downloaded 550 Google Playstore app details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/browse offset 0 limit 50 (till offset 500)

# import json
# import os
# import matplotlib.pyplot as plt
# import numpy as np

# # directory = '/mnt/2TBdrive/test/exp4/PT4_COVERAGE/'
# directory = './results'
# marketCategory = "FINANCE"

# # FINANCE - NETWORK
# def run_stats(marketCategory, specific_lib):
#     # print(" ******************************************** "+marketCategory+" "+specific_lib+" ******************************************** ")
#     files = [os.path.join(directory,f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory,f)) if marketCategory in f]
#     og_data={}
#     libraries_data={}
#     communication_apps=0


#     for filename in files:
#         with open(filename) as json_file:
#             data = json.load(json_file)
#             for app in data['apps']:
#                 if app["marketCategory"] == marketCategory:
#                     communication_apps+=1
#                 if 'libraries' in app:
#                     og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": app['libraries']}
#                 else:
#                     og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": []}

#     total_apps=len(og_data.keys())

#     with open(os.path.join(directory,"libraries.json")) as lib_json_file:
#         data_2 = json.load(lib_json_file)
#         for lib in data_2['libraries']:
#             libraries_data[lib['id']]={"name": lib['name'], "tags":lib['tags'], "count_all":0,"count_finance":0, "app_list":[]}

#     for app in og_data:
#         string_to_print=og_data[app]['marketCategory']+"~"+og_data[app]['name']
#         for l in libraries_data:
#             if specific_lib in libraries_data[l]["tags"]:
#                 if l in og_data[app]["libraries"]:
#                     string_to_print+="~1"
#                     libraries_data[l]["count_all"]+=1
#                     libraries_data[l]["app_list"].append(app)
#                 else:
#                     string_to_print+="~0"

#     filtered_libs = list(filter(lambda x: (specific_lib in x['tags']), libraries_data.values()))
#     sorted_libs = sorted(filtered_libs, key = lambda x: x["count_all"], reverse=True)
#     apps_found=set()

#     print("\n"+specific_lib+", "+marketCategory)
#     print(" ,0")
#     for l in sorted_libs:
#         apps_found.update(l["app_list"])
#         perc_apps=(len(apps_found)/total_apps)
#         print(l["name"]+","+str(perc_apps))

# run_stats("FINANCE","network")
# run_stats("FINANCE","database")
# run_stats("COMMUNICATION","network")
# run_stats("COMMUNICATION","database")
# run_stats("POPULAR","network")
# run_stats("POPULAR","database")
