#! /bin/bash 
APP=$1
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    EXPERIMENT_5_DIR=/mnt/2TBdrive/gitlab/experiment_5 #HOME PC
elif [[ "$OSTYPE" == "darwin"* ]]; then
    EXPERIMENT_5_DIR=/Users/jennifer/gitlab_repos/virtualapp_research/experiment5 #LAPTOP
fi

rm -rf $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-config
rm -rf $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-datastore
cp -r $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/clean/grr-config $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/
cp -r $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/clean/grr-datastore $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    sed -i 's/grr_2/grr_'$APP'/g' $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-config/server.local.yaml
elif [[ "$OSTYPE" == "darwin"* ]]; then
    sed -i -e 's/grr_2/grr_'$APP'/g' $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-config/server.local.yaml
fi

docker run \
    --name relf-server \
    -p 0.0.0.0:8000:8000 \
    -p 0.0.0.0:8080:8080 \
    -e EXTERNAL_HOSTNAME="localhost" \
    -e ADMIN_PASSWORD="demo" \
    -v $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-config:/etc/grr  \
    -v $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-docker/grr-datastore:/var/grr-datastore \
    -v $EXPERIMENT_5_DIR/obj3_exp/results:/tmp/grr_output \
    -v $EXPERIMENT_5_DIR/EDRs/GRR/mobile_relf/relf-server/grr/artifacts/flow_templates:/grr/grr/artifacts/flow_templates \
    ghcr.io/nexus-lab/relf-server