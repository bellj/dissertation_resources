import sys
import json
import os
import csv

modes = ['PLAIN' ,'VAPPONLY' ,'VAPPFRIDA' ]
logcat_crash='logcatcrash'
cpumem='cpumem'
apps_tested=[]

def get_averages_cpu_mem(filename):
    cpu_totals=[]
    mem_totals=[]
    total_cnt=0

    with open(filename) as file:
        next(file)
        for line in file:
            total_cnt+=1
            if not line.rstrip():
                continue
            cpu,mem=line.split(',')
            cpu_totals.append(float(cpu))
            mem_totals.append(float(mem))

    avg_cpu = sum(cpu_totals) / total_cnt if cpu_totals else None
    avg_mem = sum(mem_totals) / total_cnt if mem_totals else None

    return avg_cpu,avg_mem

results_folder=sys.argv[1]
apps_tested_filename = os.path.join(results_folder,"apps_tested.log")
with open(apps_tested_filename) as file:
    apps_tested = [line.rstrip() for line in file if len(line.rstrip()) >0 ]

app_dict={}
for app in apps_tested:
    app_dict[app]={"name":app,"cpu_plain":None,"mem_plain":None,"crash_plain":None,
                                "cpu_vapponly":None,"mem_vapponly":None,"crash_vapponly":None,
                                "cpu_vappfrida":None,"mem_vappfrida":None,"crash_vappfrida":None}

    for m in modes:
        logcat_crash_value=False
        cpumem_filename=cpumem+"_"+app+"_"+m+".csv"
        cpumem_path = os.path.join(results_folder,cpumem_filename)
        
        if m == "PLAIN" :
            avg_cpu,avg_mem = get_averages_cpu_mem(cpumem_path)
        else:
            avg_cpu_1,avg_mem_1 = get_averages_cpu_mem(cpumem_path)

            # cpumem_filename_2 -> seperate vapp process 
            cpumem_filename_2=cpumem+"_"+app+"_virtualapp_"+m+".csv"
            cpumem_path_2 = os.path.join(results_folder,cpumem_filename_2)
            avg_cpu_2,avg_mem_2 = get_averages_cpu_mem(cpumem_path_2)

            avg_cpu = (avg_cpu_1+avg_cpu_2) if avg_cpu_1!=None and avg_cpu_2!=None else None
            avg_mem = (avg_mem_1+avg_mem_2) if avg_mem_1!=None and avg_mem_2!=None else None
            
        app_dict[app]["cpu_"+m.lower()]=avg_cpu
        app_dict[app]["mem_"+m.lower()]=avg_mem

        logcatcrash_filename=logcat_crash+"_"+app+"_"+m+".log"
        logcatcrash_path = os.path.join(results_folder,logcatcrash_filename)
        monkeylog_filename="monkey_"+app+"_"+m+".log"
        monkeylog_path = os.path.join(results_folder,monkeylog_filename)

        with open(logcatcrash_path) as file:
            logcat_crash_value='FATAL' in file.read().upper() or logcat_crash_value

        # with open(monkeylog_path) as file:
        #     logcat_crash_value='Monkey aborted due to error.' in file.read() or logcat_crash_value
            
        app_dict[app]["crash_"+m.lower()]=logcat_crash_value 
                
print(json.dumps(app_dict))
csv_columns = ["name","cpu_plain","mem_plain","crash_plain","cpu_vapponly","mem_vapponly","crash_vapponly","cpu_vappfrida","mem_vappfrida","crash_vappfrida"]

headers=','.join(csv_columns)
with open(os.path.join(results_folder,"overheads_result.csv"),"w") as f:
    f.write(headers+"\n")
    for a in app_dict:
        line=str(app_dict[a]["name"])+","+str(app_dict[a]["cpu_plain"])+","+str(app_dict[a]["mem_plain"])+","+str(app_dict[a]["crash_plain"])+","+str(app_dict[a]["cpu_vapponly"])+","+str(app_dict[a]["mem_vapponly"])+","+str(app_dict[a]["crash_vapponly"])+","+str(app_dict[a]["cpu_vappfrida"])+","+str(app_dict[a]["mem_vappfrida"])+","+str(app_dict[a]["crash_vappfrida"])
        f.write(line+"\n")