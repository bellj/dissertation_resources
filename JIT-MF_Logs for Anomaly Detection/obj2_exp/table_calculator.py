import sys
import csv
import json

data_dict={}
list_names=[]

google_count =0
csv_file_path=sys.argv[1]
with open(csv_file_path) as file:
    next(file)
    for line in file:
        name,cpu_plain,mem_plain,crash_plain,cpu_vapponly,mem_vapponly,crash_vapponly,cpu_vappfrida,mem_vappfrida,crash_vappfrida=line.split(",")
        if name not in list_names:
            list_names.append(name)
        if('google' in name):
            #print(name)
            continue
        if("true" in [crash_vapponly.lower(), crash_vappfrida.lower(), crash_plain.lower()] or "None" in [cpu_vappfrida, cpu_vapponly,cpu_plain]):
            # print(name)
            continue
        if not name in data_dict:
            data_dict[name]={"name":name,"cpu_plain":[],"mem_plain":[],"cpu_vapponly":[],"mem_vapponly":[],"cpu_vappfrida":[],"mem_vappfrida":[]}
        data_dict[name]["cpu_plain"].append(float(cpu_plain))
        data_dict[name]["mem_plain"].append(float(mem_plain))
        data_dict[name]["cpu_vapponly"].append(float(cpu_vapponly))
        data_dict[name]["mem_vapponly"].append(float(mem_vapponly))
        data_dict[name]["cpu_vappfrida"].append(float(cpu_vappfrida))
        data_dict[name]["mem_vappfrida"].append(float(mem_vappfrida))
        
min_pp_cpu_vapp=[]
avg_pp_cpu_vapp=[]
max_pp_cpu_vapp=[]
min_pp_cpu_vappfrida=[]
avg_pp_cpu_vappfrida=[]
max_pp_cpu_vappfrida=[]
min_pp_mem_vapp=[]
avg_pp_mem_vapp=[]
max_pp_mem_vapp=[]
min_pp_mem_vappfrida=[]
avg_pp_mem_vappfrida=[]
max_pp_mem_vappfrida=[]

# print(json.dumps(data_dict))
for app in data_dict.values():
    # print(app)
    min_pp_cpu_vapp.append((min(app["cpu_vapponly"])-min(app["cpu_plain"])))
    avg_pp_cpu_vapp.append(((sum(app["cpu_vapponly"]) / len(app["cpu_vapponly"])) - (sum(app["cpu_plain"]) / len(app["cpu_plain"]))))
    max_pp_cpu_vapp.append((max(app["cpu_vapponly"])-max(app["cpu_plain"])))
    min_pp_mem_vapp.append((min(app["mem_vapponly"])-min(app["mem_plain"])))
    avg_pp_mem_vapp.append(((sum(app["mem_vapponly"]) / len(app["mem_vapponly"])) -(sum(app["mem_plain"]) / len(app["mem_plain"]))))
    max_pp_mem_vapp.append((max(app["mem_vapponly"])-max(app["mem_plain"])))

    min_pp_cpu_vappfrida.append((min(app["cpu_vappfrida"])-min(app["cpu_plain"])))
    avg_pp_cpu_vappfrida.append(((sum(app["cpu_vappfrida"]) / len(app["cpu_vappfrida"])) - (sum(app["cpu_vapponly"]) / len(app["cpu_vapponly"]))))
    max_pp_cpu_vappfrida.append((max(app["cpu_vappfrida"])/max(app["cpu_vapponly"])))
    min_pp_mem_vappfrida.append((min(app["mem_vappfrida"])-min(app["mem_vapponly"])))
    avg_pp_mem_vappfrida.append(((sum(app["mem_vappfrida"]) / len(app["mem_vappfrida"])) - (sum(app["mem_vapponly"]) / len(app["mem_vapponly"]))))
    max_pp_mem_vappfrida.append((max(app["mem_vappfrida"])-max(app["mem_vapponly"])))

    # min_pp_cpu_vappfrida.append((min(app["cpu_vappfrida"])-min(i for i in app["cpu_plain"] if i > 0)))
    # avg_pp_cpu_vappfrida.append(((sum(app["cpu_vappfrida"]) / len(app["cpu_vappfrida"])) - (sum(app["cpu_plain"]) / len(app["cpu_plain"]))))
    # max_pp_cpu_vappfrida.append((max(app["cpu_vappfrida"])/max(app["cpu_plain"])))
    # min_pp_mem_vappfrida.append((min(app["mem_vappfrida"])-min(app["mem_plain"])))
    # avg_pp_mem_vappfrida.append(((sum(app["mem_vappfrida"]) / len(app["mem_vappfrida"])) - (sum(app["mem_plain"]) / len(app["mem_plain"]))))
    # max_pp_mem_vappfrida.append((max(app["mem_vappfrida"])-max(app["mem_plain"])))
    
    # min_pp_cpu_vapp.append(((min(app["cpu_vapponly"])/min(i for i in app["cpu_plain"] if i > 0))-1)*100)
    # avg_pp_cpu_vapp.append((((sum(app["cpu_vapponly"]) / len(app["cpu_vapponly"])) / (sum(app["cpu_plain"]) / len(app["cpu_plain"])))-1)*100)
    # max_pp_cpu_vapp.append(((max(app["cpu_vapponly"])/max(app["cpu_plain"]))-1)*100)
    # min_pp_cpu_vappfrida.append(((min(app["cpu_vappfrida"])/min(i for i in app["cpu_plain"] if i > 0))-1)*100)
    # avg_pp_cpu_vappfrida.append((((sum(app["cpu_vappfrida"]) / len(app["cpu_vappfrida"])) / (sum(app["cpu_plain"]) / len(app["cpu_plain"])))-1)*100)
    # max_pp_cpu_vappfrida.append(((max(app["cpu_vappfrida"])/max(app["cpu_plain"]))-1)*100)
    # min_pp_mem_vapp.append(((min(app["mem_vapponly"])/min(app["mem_plain"]))-1)*100)
    # avg_pp_mem_vapp.append((((sum(app["mem_vapponly"]) / len(app["mem_vapponly"])) / (sum(app["mem_plain"]) / len(app["mem_plain"])))-1)*100)
    # max_pp_mem_vapp.append(((max(app["mem_vapponly"])/max(app["mem_plain"]))-1)*100)
    # min_pp_mem_vappfrida.append(((min(app["mem_vappfrida"])/min(app["mem_plain"]))-1)*100)
    # avg_pp_mem_vappfrida.append((((sum(app["mem_vappfrida"]) / len(app["mem_vappfrida"])) / (sum(app["mem_plain"]) / len(app["mem_plain"])))-1)*100)
    # max_pp_mem_vappfrida.append(((max(app["mem_vappfrida"])/max(app["mem_plain"]))-1)*100)


# print(min_pp_cpu_vapp)
# print(avg_pp_cpu_vapp)
# print(max_pp_cpu_vapp)
# print(min_pp_cpu_vappfrida)
# print(avg_pp_cpu_vappfrida)
# print(max_pp_cpu_vappfrida)
# print(min_pp_mem_vapp)
# print(avg_pp_mem_vapp)
# print(max_pp_mem_vapp)
# print(min_pp_mem_vappfrida)
# print(avg_pp_mem_vappfrida)
# print(max_pp_mem_vappfrida)

# print(data_dict.keys())

# print(list_names)
for n in list_names:
    if 'google' in n:
        google_count+=1
    if 'google' not in n and n not in data_dict.keys():
        print(f'{n} DID NOT WORK')

print(google_count)
print(f"#apps valid : {len(data_dict.keys())} |  #apps total : {len(list_names)-google_count}")
print(f"% of apps worked : {(len(data_dict.keys()) /(len(list_names)-google_count))*100}")
print(f"cpu_min,{(round(sum(min_pp_cpu_vapp)/len(min_pp_cpu_vapp),2)):+},{(round(sum(min_pp_cpu_vappfrida)/len(min_pp_cpu_vappfrida),2)):+}")
print(f"cpu_avg,{(round(sum(avg_pp_cpu_vapp)/len(avg_pp_cpu_vapp),2)):+},{(round(sum(avg_pp_cpu_vappfrida)/len(avg_pp_cpu_vappfrida),2)):+}")
print(f"cpu_max,{(round(sum(max_pp_cpu_vapp)/len(max_pp_cpu_vapp),2)):+},{(round(sum(max_pp_cpu_vappfrida)/len(max_pp_cpu_vappfrida),2)):+}")
print(f"mem_min,{(round(sum(min_pp_mem_vapp)/len(min_pp_mem_vapp),2)):+},{(round(sum(min_pp_mem_vappfrida)/len(min_pp_mem_vappfrida),2)):+}")
print(f"mem_avg,{(round(sum(avg_pp_mem_vapp)/len(avg_pp_mem_vapp),2)):+},{(round(sum(avg_pp_mem_vappfrida)/len(avg_pp_mem_vappfrida),2)):+}")
print(f"mem_max,{(round(sum(max_pp_mem_vapp)/len(max_pp_mem_vapp),2)):+},{(round(sum(max_pp_mem_vappfrida)/len(max_pp_mem_vappfrida),2)):+}")

# print(list_names)
# print(data_dict.keys())
# print(min_pp_cpu_vapp)