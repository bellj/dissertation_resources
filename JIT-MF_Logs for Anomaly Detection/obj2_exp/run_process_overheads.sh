# folder="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/results/results_apkpure_multipleruns"
# folder="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/results/results_apkpure_multipleruns_WITHOUT_FRESH_INSTALL-2023-02-07T02:00:26"
# folder="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/results/results_apkpure_multipleruns_WITHOUT_FRESH_INSTALL-2023-02-17T10:01:14"
# folder="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/results/results_apkpure_multipleruns_WITHOUT_FRESH_INSTALL-2023-03-06T09:54:25"
folder="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/results/results_apkpure_multipleruns_WITHOUT_FRESH_INSTALL-2023-03-06T19:56:32"
# for f in "${folder}"/results_apkpure_mulruns* ; do
for f in "${folder}"/results_* ; do
    python3 /mnt/2TBdrive/gitlab/experiment_5/obj2_exp/process_overheads.py $f
done

# for f in "${folder}"/results_apkpure_mulruns* ; do
for f in "${folder}"/results_* ; do
    cat $f/overheads_result.csv >> overall_result.csv
done
sed -i '/name,cpu_plain,mem_plain,crash_plain,cpu_vapponly,mem_vapponly,crash_vapponly,cpu_vappfrida,mem_vappfrida,crash_vappfrida/d' overall_result.csv
echo "name,cpu_plain,mem_plain,crash_plain,cpu_vapponly,mem_vapponly,crash_vapponly,cpu_vappfrida,mem_vappfrida,crash_vappfrida"|cat - overall_result.csv > /tmp/out && mv /tmp/out overall_result.csv