ADB_DEVICE="100.94.189.18:5555"
EXPERIMENT_5_DIR=/mnt/2TBdrive/gitlab/experiment_5
#EXPERIMENT_5_DIR=/Users/jennifer/gitlab_repos/virtualapp_research/experiment5

DRIVER_LOCATION="$EXPERIMENT_5_DIR/obj2_exp/drivers"

apk_to_push="$1"
pkgname=`basename $apk_to_push .apk`
frida="$2"

old_apk=`adb -s $ADB_DEVICE shell ls /sdcard/*.apk 2>/dev/null`
adb -s $ADB_DEVICE shell rm -rf $old_apk
adb -s $ADB_DEVICE push $apk_to_push /sdcard

if [[ -z "$2" ]]; then
    driver="nofrida_template.js"
    adb -s $ADB_DEVICE push $DRIVER_LOCATION/$driver /data/local/tmp/frida-scripts/script1.js 
else 
    driver="template.js"
    sed -i 's/var APP = "any";/var APP = "'$pkgname'";/g' $DRIVER_LOCATION/$driver
    adb -s $ADB_DEVICE push $DRIVER_LOCATION/$driver /data/local/tmp/frida-scripts/script1.js 
    sed -i 's/var APP = "'$pkgname'";/var APP = "any";/g' $DRIVER_LOCATION/$driver
fi




