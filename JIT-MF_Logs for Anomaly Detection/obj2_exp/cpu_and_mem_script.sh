process="$1"
file="$2"
ADB_DEVICE="100.94.189.18:5555"

echo "cpu%,mem%" > $file

while [ true ]
do
    pid=`adb -s $ADB_DEVICE shell pidof $process`
    # column 9 is cpu, column 10 is memory
    echo `adb -s $ADB_DEVICE shell top -b -n 1 -d 1 -p $pid | grep $process | awk '{ print $9","$10 }'` >> $file
    sleep 1
done
