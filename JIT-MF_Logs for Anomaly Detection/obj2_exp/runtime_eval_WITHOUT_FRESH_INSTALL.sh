#! /bin/bash
# TO START APP: adb -s $ADB_DEVICE shell monkey -p $apprunning -c android.intent.category.LAUNCHER 1 && sleep 50
ADB_DEVICE="100.94.189.18:5555"

APKDOWNLOADFOLDER="/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps"
VIRTUALAPPAPDIR="/mnt/2TBdrive/gitlab/experiment_5/obj3_exp/setup_on_phone"
VIRTUALAPPPKGNAME="com.example.trustedcontainervirtualapp"
MONKEYTHROTTLE=20000
MONKEYEVENTS=20
DEFAULT_SLEEP=10

# numberofruns=10
numberofruns=5
runmode=( "VAPPFRIDA" "VAPPONLY" "PLAIN")
# appstorerun=("/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.orca.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.books.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.instagram.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.kiloo.subwaysurf.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.powerpoint.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.word.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.player.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.mxtech.videoplayer.ad.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.netflix.mediaclient.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.sbrowser.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.touchtype.swiftkey.apk" )
# appstorerun=("/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.wellbeing.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.calendar.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.lite.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.messaging.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.searchlite.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.googleassistant.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.ar.lens.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.play.games.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.videos.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.word.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.calculator.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.videoplayer.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.powerpoint.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.touchtype.swiftkey.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.subscriptions.red.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.app.samsungprintservice.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.player.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.nbu.files.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.xiaomi.midrop.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.deskclock.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.gms.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.ims.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.inputmethod.latin.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.skydrive.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.tts.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.photos.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.sbrowser.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.linkedin.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.youtube.music.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.truecaller.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.excel.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.dialer.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.translate.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.orca.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.katana.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.spotify.music.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.instagram.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.calculator.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.kiloo.subwaysurf.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.snapchat.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.king.candycrushsaga.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/org.telegram.messenger.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.books.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.shealth.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.whatsapp.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.lenovo.anyshare.gps.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.netflix.mediaclient.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.apps.maps.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.skype.raider.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.dropbox.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.twitter.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.mxtech.videoplayer.ad.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.imo.android.imoim.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.google.android.gm.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.zhiliaoapp.musically.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.viber.voip.apk" )

# without google
# appstorerun=("/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.lite.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.word.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.videoplayer.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.powerpoint.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.touchtype.swiftkey.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.app.samsungprintservice.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.player.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.xiaomi.midrop.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.skydrive.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.sbrowser.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.linkedin.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.truecaller.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.microsoft.office.excel.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.orca.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.facebook.katana.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.spotify.music.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.instagram.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.calculator.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.kiloo.subwaysurf.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.snapchat.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.king.candycrushsaga.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/org.telegram.messenger.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.shealth.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.whatsapp.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.lenovo.anyshare.gps.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.netflix.mediaclient.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.skype.raider.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.dropbox.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.twitter.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.mxtech.videoplayer.ad.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.imo.android.imoim.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.zhiliaoapp.musically.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.viber.voip.apk")
appstorerun=("/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.dropbox.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.imo.android.imoim.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.instagram.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.lenovo.anyshare.gps.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.miui.videoplayer.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.mxtech.videoplayer.ad.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.netflix.mediaclient.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.sec.android.app.shealth.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.twitter.android.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.viber.voip.apk" "/mnt/2TBdrive/gitlab/experiment_5/obj2_exp/apkpure_apps/com.zhiliaoapp.musically.apk")

apptotal=${#appstorerun[@]}
appnumber=0
parentfolder="./results/results_apkpure_multipleruns_WITHOUT_FRESH_INSTALL-`date +"%FT%T"`"
mkdir -p $parentfolder

for f in ${appstorerun[@]}; do
    packagename=`basename $f .apk`        
    ((appnumber=appnumber+1))
    
    echo ">>>>>>>> At app: $packagename $appnumber/$apptotal. TIME:`date` ... "

    for mode in ${runmode[@]}; do
        echo "Clearing logcat and starting $mode MODE. TIME:`date` ... "
        adb -s $ADB_DEVICE logcat -c
        newapp=false
        unset CPUMEMPID_2
        unset CPUMEMPID_1
        # installation and setup for each mode
        for ((j=1; j <= $numberofruns; j++)); do
            runresultsfoldername=$parentfolder"/results_${packagename}_${j}"
            mkdir -p $runresultsfoldername
            
            echo "******* Started run number $j at TIME:`date` ... ******* "
            if [[ "$mode" == "PLAIN" ]]; then
                if [[ -z $(adb -s $ADB_DEVICE shell pm list packages $packagename) ]]; then
                    newapp=true
                    adb -s $ADB_DEVICE install $f  
                fi
                apprunning=$packagename
                ./cpu_and_mem_script.sh $packagename $runresultsfoldername"/cpumem_"$packagename"_"$mode".csv" 2>/dev/null &
                CPUMEMPID_1=$!
            else
                if [[ "$mode" == "VAPPONLY" ]]; then
                    echo $packagename >> $runresultsfoldername/apps_tested.log
                    if [[ -z $(adb -s $ADB_DEVICE shell pm list packages $VIRTUALAPPPKGNAME) ]]; then
                        adb -s $ADB_DEVICE install -g $VIRTUALAPPAPDIR"/virtualapp_NO_FRIDA.apk"
                        ./move_apps.sh $f
                    fi
                elif [[ "$mode" == "VAPPFRIDA" ]]; then
                    if [[ -z $(adb -s $ADB_DEVICE shell pm list packages $VIRTUALAPPPKGNAME) ]]; then
                        adb -s $ADB_DEVICE install -g $VIRTUALAPPAPDIR"/virtualapp.apk"
                        ./move_apps.sh $f "true"
                    fi
                fi
                apprunning=$VIRTUALAPPPKGNAME                    
                ./cpu_and_mem_script.sh $packagename $runresultsfoldername"/cpumem_"$packagename"_"$mode".csv" 2>/dev/null &
                CPUMEMPID_1=$!
                ./cpu_and_mem_script.sh $apprunning $runresultsfoldername"/cpumem_"$packagename"_virtualapp_"$mode".csv" 2>/dev/null &
                CPUMEMPID_2=$!
            fi 
            sleep 3                
            echo -e "CPU and MEM script started with pid $CPUMEMPID_1 $CPUMUMPID_2.\nStarting monkey exerciser. TIME:`date` ...  "
    
            adb -s $ADB_DEVICE shell monkey -p $apprunning  --throttle $MONKEYTHROTTLE -v $MONKEYEVENTS  -s 100 > $runresultsfoldername"/monkey_"$packagename"_"$mode".log"  2>/dev/null
            echo -e "Finished monkey exerciser. TIME:`date` ...  "
            sleep $DEFAULT_SLEEP
            adb -s $ADB_DEVICE shell am force-stop $apprunning    

            kill $CPUMEMPID_1
            if [[ ! -z "$CPUMEMPID_2" ]]; then
                kill $CPUMEMPID_2
            fi
            
            adb -s $ADB_DEVICE logcat -d -v time > $runresultsfoldername"/logcat_"$packagename"_"$mode".log"
            adb -s $ADB_DEVICE logcat -d -b crash -v time > $runresultsfoldername"/logcatcrash_"$packagename"_"$mode".log"
            if [[ "$mode" == "VAPPFRIDA" ]]; then
                adb -s $ADB_DEVICE pull /sdcard/jitmflogs/ $runresultsfoldername/jitmflogs
                adb -s $ADB_DEVICE shell rm -rf /sdcard/jitmflogs/*
                echo "Pulled jitmflogs and remptied folder on SDCARD. TIME:`date` ...  "
            fi  
            echo -e "Monkey exerciser finished.\nKilled CPU and MEM script started with pid $CPUMEMPID_1 $CPUMEMPID_2.\nCollected logcats. TIME:`date` ...  "
            echo "Sleeping 5s before next iteration. RESULTS AT: $runresultsfoldername"
            sleep $DEFAULT_SLEEP
        done
        # uninstallation and teardown of setup for each mode                
        if [[ "$mode" == "PLAIN" && "$newapp" = true ]]; then
            adb -s $ADB_DEVICE shell pm clear $apprunning
            adb -s $ADB_DEVICE uninstall $apprunning
            echo "Uninstalled app $apprunning. TIME:`date` ...  "
        elif [[ "$mode" == "VAPPONLY" ]]; then
            adb -s $ADB_DEVICE shell pm clear $apprunning
            adb -s $ADB_DEVICE uninstall $apprunning
        elif [[ "$mode" == "VAPPFRIDA" ]]; then
            adb -s $ADB_DEVICE shell pm clear $apprunning
            adb -s $ADB_DEVICE uninstall $apprunning
        fi  
    done
done
