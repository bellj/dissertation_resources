adb -s ${1} push frida-server /data/local/tmp/
adb -s ${1} shell "chmod 755 /data/local/tmp/frida-server"
adb -s ${1} shell "/data/local/tmp/frida-server &"