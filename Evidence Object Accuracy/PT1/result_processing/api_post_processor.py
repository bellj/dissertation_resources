import json
import glob
import sys
import os
from datetime import datetime
import pytz
import string
import traceback
import re
import base64

# sys.dont_write_bytecode = True

lines_seen = set()
telegram_id_map={"1679923803":{"name":"contact_phone;;;","phone":"35679247196"},"5181266731":{"name":"target_phone;;;", "phone":"35699626972"}}
whatsapp_id_map={"35679247196@s.whatsapp.net":{"name":"contact_phone","phone":"+35679247196"},"_":{"name":"target_phone","phone":"+35699626972"}}

signal_id_map={"3":{"name":"contact_phone","phone":"+35679247196"},"_":{"name":"target_phone","phone":"+35699626972"}}
signal_thread_id_map={"1":"3","3":"3","?":"3","n":"3","2":"3"}

def decode_string(strr):
    # strr=incoming_hex_string.decode('utf8', errors='ignore')
    
    strr=re.sub(r'[^\x00-\x7f]',r'', strr)
    test=""
    for c in strr:
        if ((ord(c) >= 32 and ord(c) <= 125)):
            test+=c
    return test

def check_telegram_entry(evidence_object, data, event_type):
    add = True
    invalid_id =False
    evidence_json = json.loads(evidence_object)
    query = evidence_json["Query Executed"]

    message={}
    
    key_indicator="REPLACE INTO messages_v2 VALUES("
    if key_indicator in query:
        query_args = query.split(key_indicator)[1].replace(')','').split(', ')
        if int(query_args[0]) >= 18446744073709340000:
            invalid_id=True
        else:
            message["message_number"] = query_args[0]
            message["date"] = query_args[4]
            message["text"] = query_args[5]

            if(int(query_args[6])==1):
                message["type"]="sent"            
                message["to_id"]=query_args[1]
                message["to_name"]=telegram_id_map[query_args[1]]["name"]
                message["to_phone"]=telegram_id_map[query_args[1]]["phone"]
                message["from_name"]=telegram_id_map["5181266731"]["name"]
                message["from_phone"]=telegram_id_map["5181266731"]["phone"]             
                message["from_id"]="5181266731"
            elif(int(query_args[6])==0):                
                message["type"]="received"
                message["to_name"]=telegram_id_map["5181266731"]["name"]
                message["to_phone"]=telegram_id_map["5181266731"]["phone"]             
                message["to_id"]="5181266731"
                message["from_name"]=telegram_id_map[query_args[1]]["name"]
                message["from_phone"]=telegram_id_map[query_args[1]]["phone"]
                message["from_id"]=query_args[1]
         
    if (json.dumps(message) in lines_seen or not (key_indicator in query) or invalid_id): #removes entry if duplicate
        add &= False

    return (add,message)

def check_signal_entry(evidence_object, data, event_type):
    add = True
    invalid =False
    evidence_json = json.loads(evidence_object)
    query = evidence_json["Query Executed"]

    message={}
    key_indicator="INSERT INTO sms(" 
    key_indicator_1="UPDATE thread SET snippet="
    key_indicator_2=" WHERE _id = "

    if key_indicator in query:
        if( not "lookpink" in query):
            query_args = query.split('VALUES (')[1].split(')')[0].split(',')
            if(query_args[0] in ['undefined','null']):
                invalid=True
            else:
                contact_id =query_args[2][0]
                try:
                    if(query_args[10]=='10485780' or query_args[9]=='10485780'):           
                        message["date"] = query_args[8]
                        message["text"] = query_args[7]     
                        message["type"]="received"
                        message["to_name"]=signal_id_map["_"]["name"]
                        message["to_phone"]=signal_id_map["_"]["phone"]             
                        message["to_id"]="_"
                        message["from_name"]=signal_id_map[contact_id]["name"]
                        message["from_phone"]=signal_id_map[contact_id]["phone"]
                        message["from_id"]=contact_id
                    else:
                        message["date"] = query_args[5]
                        message["text"] = query_args[6]
                        message["type"]="sent"            
                        message["to_id"]=contact_id
                        message["to_name"]=signal_id_map[contact_id]["name"]
                        message["to_phone"]=signal_id_map[contact_id]["phone"]
                        message["from_name"]=signal_id_map["_"]["name"]
                        message["from_phone"]=signal_id_map["_"]["phone"]     
                except Exception as e:
                    # print(query)
                    # print(e)
                    # traceback.print_exc()
                    print("api_post_processor  "+str(e))
        else:
            invalid=True
    elif key_indicator_1 in query:
        values="snippet="+query.split(key_indicator_1)[1].split(key_indicator_2)[0]
        # print("Query >>> "+query)
        if( not "lookpink" in query):
            # print("Query 2>>> "+query)
            query_args = dict((x.strip(), y.strip())
                for x, y in (element.split('=') 
                for element in values.split(',')))
            if(query_args['snippet'] in ['undefined','null']):
                invalid=True
            else:
                contact_id=signal_thread_id_map[query.split(key_indicator_2)[1][0]]
                message["date"] = query_args['date']
                message["text"] = query_args['snippet']   
        
                if(query_args['snippet_type']=='10485780'):             
                    message["type"]="received"
                    message["to_name"]=signal_id_map["_"]["name"]
                    message["to_phone"]=signal_id_map["_"]["phone"]             
                    message["to_id"]="_"
                    message["from_name"]=signal_id_map[contact_id]["name"]
                    message["from_phone"]=signal_id_map[contact_id]["phone"]
                    message["from_id"]=contact_id
                else:
                    message["type"]="sent"            
                    message["to_id"]=contact_id
                    message["to_name"]=signal_id_map[contact_id]["name"]
                    message["to_phone"]=signal_id_map[contact_id]["phone"]
                    message["from_name"]=signal_id_map["_"]["name"]
                    message["from_phone"]=signal_id_map["_"]["phone"]     
        else:
            invalid=True
    if json.dumps(message) in lines_seen:
        invalid=True
        
    else:
        if (key_indicator in query or key_indicator_1 in query) and not invalid:
            for l in lines_seen:
                text_in_set = json.loads(l)["text"]
                if message["text"] in text_in_set:
                    invalid=True

    if (not (key_indicator in query or key_indicator_1 in query) or invalid): #removes entry if duplicate
        add &= False
   
    return (add,message)

def check_whatsapp_entry(evidence_object, data, event_type):
    add = True
    invalid =False
    evidence_json = json.loads(evidence_object)
    query = evidence_json["Query Executed"]

    message={}
    key_indicator="INSERT INTO message_ftsv2(fts_jid,docid,content,fts_namespace) VALUES ("
    key_indicator_1="INSERT INTO message(sort_id,starred,recipient_count,broadcast,origination_flags,message_type,key_id,text_data,origin,receipt_server_timestamp,status,sender_jid_row_id,from_me,timestamp,participant_hash,lookup_tables,received_timestamp,chat_row_id,message_add_on_flags) VALUES ("
    #8
    if key_indicator in query:    
        query_args = query.split(key_indicator)[1].split(')')[0].split(',')
        if(query_args[0] in ['undefined','null']):
            invalid=True
        message["text"] = query_args[2]
    elif key_indicator_1 in query:
        query_args = query.split(key_indicator_1)[1].split(')\n')[0].split(',')
        if(query_args[7] in ['undefined','null']):
            invalid=True
        message["text"] = query_args[7]

    if not len(message) or json.dumps(message) in lines_seen:
        invalid=True        
    else:
        if (key_indicator in query or key_indicator_1 in query) and not invalid:
            for l in lines_seen:
                text_in_set = json.loads(l)["text"]
                if message["text"].lower() == text_in_set.lower():
                    invalid=True

    
    if (not (key_indicator in query or key_indicator_1 in query) or invalid): #removes entry if duplicate
        add &= False
        
    return (add,message)

def process_api_based_logs(app_name,log_dir,output_dir):
    global lines_seen
    lines_seen= set()
    try:
        read_files = glob.glob(log_dir+"/*.jitmflog")
        with open(os.path.join(output_dir,"merged-logs.jitmflog"), "a") as outfile:
            for f in read_files:
                infile = open(f, "r").read().replace('\n', ' ').replace('} {"time": "','}\n{"time": "').split('\n')
                for line in infile:
                    line+='\n'
                    line=line.replace('"object": "{"Query Executed"','"object": {"Query Executed"').replace('"}"}\n','"}}\n')
                    query=line.split('"Query Executed": "')[1].split('"}}\n')[0]
                    line=line.replace(query,decode_string(query).replace('\\','\\\\').replace('"','\\"'))
                    line=line.strip()
                    if len(line.strip()) >0:
                        try: 
                            data = json.loads(line)
                            outfile.write(line+"\n")
                        except ValueError:
                            continue

        outfile = open(os.path.join(output_dir,"merged-logs-uniq.jitmflog"), "w")

        with open(os.path.join(output_dir,"merged-logs.jitmflog")) as f:
            for line in f:                
                data = json.loads(line)
                evidence_object=json.dumps(data['object'])
            
                if len(evidence_object) > 2 :
                    add_line, entry = globals()["check_%s_entry" % app_name](evidence_object, data, data["event"])
                    data['object']=entry
                    if add_line: # not a duplicate
                        outfile.write(json.dumps(data))
                        outfile.write('\n')                        
                        
                        lines_seen.add(json.dumps(entry))

        path_value = os.path.realpath(outfile.name) 
        outfile.close()
        return path_value
    except:
        return "ERROR No file created"

def main():
    process_api_based_logs('whatsapp','/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/test/','/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/test/')

if __name__ == "__main__":
    main()
