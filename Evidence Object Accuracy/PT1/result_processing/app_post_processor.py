import json
import glob
import sys
import os
from datetime import datetime
import pytz
import string
import base64

sys.dont_write_bytecode = True

lines_seen = set()
telegram_id_map={"1679923803":{"name":"contact_phone;;;","phone":"35679247196"},"5181266731":{"name":"target_phone;;;", "phone":"35699626972"}}
		
def create_json(evidence_object, date_time):
    entry={}
    evidence_json = json.loads(evidence_object)
    entry["to_id"] = evidence_json["to_id"]
    entry["from_id"] = evidence_json["from_id"]
    entry["message_number"] = evidence_json["message_id"]
    entry["date"] = date_time
    entry["text"] = evidence_json["text"]
    return json.dumps(entry)

def check_entry(evidence_object, data, event_type):
    add = True
    evidence_json = json.loads(evidence_object)
    if('Telegram' in data['event']):
        evidence_json["to_name"]=telegram_id_map[evidence_json['to_id']]['name']
        evidence_json["to_phone"]=telegram_id_map[evidence_json['to_id']]['phone']
        evidence_json['from_name']=telegram_id_map[evidence_json['from_id']]['name']
        evidence_json['from_phone']=telegram_id_map[evidence_json['from_id']]['phone']

    time = evidence_json["date"]
    
    entry = create_json(evidence_object, time)

    if entry in lines_seen: #removes entry if duplicate
        add &= False

    return (add,evidence_json,entry)

def process_app_based_logs(log_dir,output_dir):
    global lines_seen
    lines_seen= set()
    try:
        read_files = glob.glob(log_dir+"/*.jitmflog")
        with open(os.path.join(output_dir,"merged-logs.jitmflog"), "a") as outfile:
            for f in read_files:
                counter=0
                with open(f, "r") as infile:
                    line_2 = ""
                    not_json=False
                    for line in infile:
                        line=line.strip()
                        if len(line.strip()) >0:
                            if not_json:
                                line_2 = line_2.strip()
                                line_2 +=  line if len(line_2) == 0 else " "+line
                            else:
                                line_2 += line
                            try: 
                                data = json.loads(line_2)
                                outfile.write(line_2+"\n")
                                line_2 = ""
                                not_json=False
                            except ValueError:
                                not_json=True

        outfile = open(os.path.join(output_dir,"merged-logs-uniq.jitmflog"), "w")

        with open(os.path.join(output_dir,"merged-logs.jitmflog")) as f:
            for line in f:
                data = json.loads(line)
                evidence_object=json.dumps(data['object'])

                if len(evidence_object) > 2 :
                    add_line, evidence_json, entry = check_entry(evidence_object, data, data["event"])
                    data['object']=evidence_json
                
                    if add_line: # not a duplicate
                        outfile.write(json.dumps(data))
                        outfile.write('\n')
                        
                        lines_seen.add(entry)

        path_value = os.path.realpath(outfile.name) 
        outfile.close()
        return path_value
    except:
        return "ERROR No file created"
