import os, sys, glob, shutil
import json
import string,re
import sqlite3
import time, pytz, datetime
import traceback
import app_post_processor
import api_post_processor

# sys.path.insert(0, '/home/uom-lab-locard/jennifer-exp4/PT1/result_processing/signal_for_android_decryption')
# sys.path.insert(0, '/mnt/2TBdrive/gitlab/experiment_4/PT1/result_processing/signal_for_android_decryption')
sys.dont_write_bytecode = True

# import decrypt_backup

# results_folder="/home/uom-lab-locard/jennifer-exp4/PT1/results/numbered_msgs"
results_folder="/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/new/numbered_messages"
# results_folder="/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/new"
# results_folder="/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/TG_WA"
# results_folder="/mnt/2TBdrive/test/exp4/PT1_RESULTS_PARSE/SIGNAL"

app_data={"com.whatsapp":{"name":"WhatsApp","jitmf_evidence":"evidence/sdcard/jitmflogs/","database":"evidence/data/com.whatsapp/databases/msgstore.db"},"org.telegram.messenger.web":{"name":"Telegram","jitmf_evidence":"evidence/sdcard/jitmflogs/","database":"evidence/data/org.telegram.messenger.web/files/cache4.db"},"org.thoughtcrime.securesms":{"name":"Signal","jitmf_evidence":"evidence/sdcard/jitmflogs/","database":"evidence/data/*.backup"}}
# app_data={"org.thoughtcrime.securesms":{"name":"Signal","jitmf_evidence":"evidence/sdcard/jitmflogs/","database":"evidence/data/*.backup"}}

contact_name="contact_phone"
contact_phone="35679247196"
target_name="target_phone"
target_phone="35699626972"

messages_number="20"

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def decode_string(incoming_hex_string):
    strr=incoming_hex_string.decode('utf8', errors='ignore')
    
    strr=re.sub(r'[^\x00-\x7f]',r'', strr)
    test=""
    for c in strr:
        if ((ord(c) > 32 and ord(c) <125)):
            test+=c
    return test

def connect_and_query(db_file, query):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        for q in query:
            cur.execute(q)
        rows = cur.fetchall()

        return rows
    except:
        print("Something went wrong")

def get_timestamp(time_str):
    time_str=time_str.strip()
    local_time = pytz.timezone("Europe/Malta")
    try:
        test_date = datetime.datetime.strptime(time_str, "%d/%m/%Y %H:%M:%S")
    except Exception as e:
        if("does not match format '%d/%m/%Y %H:%M:%S'" in str(e)):
            test_date = datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S.%f")

    local_datetime = local_time.localize(test_date, is_dst=None)
    utc_datetime = local_datetime.astimezone(pytz.utc)
    ans = utc_datetime.replace(tzinfo=pytz.utc).timestamp()

    return str(ans)

def generate_ground_truth(execution_run_file):
    ground_truth={}

    with open(os.path.join(results_folder, execution_run_file)) as f:
        batch_counter=0
        list_of_messages=[]
        app_name=""
        driver_type=""
        start_time=""
        end_time=""
        for line in f:
            key_indicator="[**] Starting App "
            if key_indicator in line:
                batch_counter=0
                app_name=line.split(key_indicator)[1].split(" ...")[0]
                ground_truth[app_name]={}
            key_indicator="[**] Starting Driver "
            if key_indicator in line:
                batch_counter=0
                driver_type=line.split(key_indicator)[1].split(" ... ----> ")[0]
                start_time=line.split(key_indicator)[1].split(" ... ----> ")[1]
                list_of_messages=[]
                end_time=""
            try:
                key_indicator=app_data[app_name]["name"].upper()+" | Sent message ["
                if (key_indicator in line):
                    message={}
                    start="text="
                    end=". Current time ... ----> "
                    message["text"]=(line.split(start))[1].split(end)[0]
                    message["date"]=get_timestamp(line.split(end)[1].strip())
                    start="Sent message ["
                    end="/"+messages_number+"]"
                    message["message_number"]=int((line.split(start))[1].split(end)[0])
                    if(message["message_number"]==1):
                        batch_counter+=1
                    
                    if batch_counter <2:
                        message["msg_type"]="sent"
                        message["to_name"]=contact_name
                        message["to_phone"]=contact_phone
                        message["from_name"]=target_name
                        message["from_phone"]=target_phone
                        list_of_messages.append(message)            
                        end_time=message["date"]
                    else:                      
                        if message["date"] < end_time:
                            message["msg_type"]="received"
                            message["to_name"]=target_name
                            message["to_phone"]=target_phone
                            message["from_name"]=contact_name
                            message["from_phone"]=contact_phone
                            list_of_messages.append(message)            
            except Exception as e: # work on python 3.x
                continue  
                
            key_indicator="[**] Finished Driver"
            if (key_indicator in line):
                # end_time=line.split(key_indicator)[1].split(" ... ----> ")[1]
                ground_truth[app_name][driver_type]={"start_time":get_timestamp(start_time),"end_time":end_time,"messages":list_of_messages}
    return ground_truth

def generate_telegram_baseline_events(execution_run_folder,baseline_events,ground_truth):
    app_name="org.telegram.messenger.web"
    drivers=["api_based","app_based"]

    baseline_events[app_name]={}
    db_path = app_data[app_name]["database"]

    for driver_type in drivers:    
        start_time = ground_truth[driver_type]["start_time"]
        end_time = ground_truth[driver_type]["end_time"]
        telegram_db = os.path.join(execution_run_folder,app_name,driver_type,db_path)
        # m.out - 0 incoming 1 outgoing

        messages = connect_and_query(telegram_db,["""select m.mid, m.uid, m.date as d, m.data, 
                                                CASE 
                                                WHEN m.out=0 THEN "received"
                                                WHEN m.out=1 THEN "sent"
                                                END AS "Message Direction",
                                                u.name, u.data
                                                from messages_v2 m
                                                left outer join users u
                                                on m.uid = u.uid
                                                where (d >= """+start_time+""" and d < """+end_time+""")
                                                """])
        nested_driver={}
        list_of_messages=[]
        for m in messages:
            message={}

            message["msg_type"]=m[4]                        
            message["text"]=decode_string(m[3])
            message["date"]=m[2]
            message["message_number"]=m[0]

            if(m[4]=="sent"):
                message["to_name"]=m[5]
                message["to_phone"]=decode_string(m[6])
                message["to_id"]=m[1]
                message["from_name"]=target_name
                message["from_phone"]=target_phone
                message["from_id"]="5181266731"
            elif(m[4]=="received"):                
                message["to_name"]=target_name
                message["to_phone"]=target_phone
                message["to_id"]="5181266731"
                message["from_name"]=m[5]
                message["from_phone"]=decode_string(m[6])
                message["from_id"]=m[1]

            list_of_messages.append(message)    
        baseline_events[app_name][driver_type]={"messages":list_of_messages}
    return baseline_events

def generate_whatsapp_baseline_events(execution_run_folder,baseline_events,ground_truth):
    app_name="com.whatsapp"
    drivers=["api_based","app_based"]

    baseline_events[app_name]={}
    db_path = app_data[app_name]["database"]

    for driver_type in drivers:    
        start_time = ground_truth[driver_type]["start_time"]
        end_time = ground_truth[driver_type]["end_time"]

        whatsapp_msg_db = os.path.join(execution_run_folder,app_name,driver_type,db_path)
        path, filename = os.path.split(whatsapp_msg_db)
        whatsapp_contact_db = os.path.join(path,"wa.db")

        messages = connect_and_query(whatsapp_msg_db,["""ATTACH DATABASE '"""+whatsapp_contact_db+"""' AS "whatsapp_db" """,
                                                    """SELECT m._id as mid, jid._id as jid, m.timestamp/1000 as d, m.text_data AS "Message", 
                                                    CASE
                                                    WHEN m.from_me=0 THEN "received"
                                                    WHEN m.from_me=1 THEN "sent"
                                                    END AS "Message Direction",
                                                    wa_contacts.wa_name AS "contact", jid.user, jid.raw_string
                                                    FROM message m
                                                    JOIN chat ON chat._id=m.chat_row_id
                                                    JOIN jid ON jid._id=chat.jid_row_id
                                                    JOIN wa_contacts ON wa_contacts.jid=jid.raw_string
                                                    where (d >= """+start_time+""" and d < """+end_time+""")
                                                """])
        nested_driver={}
        list_of_messages=[]
        for m in messages:
            message={}
            message["msg_type"]=m[4]                        
            message["text"]=m[3]
            message["date"]=m[2]
            message["message_number"]=m[0]

            if(m[4]=="sent"):
                message["to_name"]=m[5]
                message["to_phone"]=m[6]
                message["to_id"]=m[1]
                message["to_whatsapp_name"]=m[7]
                message["from_name"]=target_name
                message["from_phone"]=target_phone
                message["from_id"]=""
            elif(m[4]=="received"):                
                message["to_name"]=target_name
                message["to_phone"]=target_phone
                message["to_id"]=""
                message["from_name"]=m[5]
                message["from_phone"]=m[6]
                message["from_id"]=m[1]
                message["from_whatsapp_name"]=m[7]
            list_of_messages.append(message)    
        baseline_events[app_name][driver_type]={"messages":list_of_messages}
    return baseline_events

def generate_signal_baseline_events(execution_run_folder,baseline_events,ground_truth):
    app_name="org.thoughtcrime.securesms"
    drivers=["api_based","app_based"]

    baseline_events[app_name]={}
    db_path = app_data[app_name]["database"]
    list_of_messages=[]
    
    for driver_type in drivers:    
        list_of_messages=[]    

        # driver_data = baseline_events[app_name].get(driver_type)
    
        if not ground_truth.get(driver_type):
            baseline_events[app_name][driver_type]={"messages":list_of_messages}
            continue

        start_time = ground_truth[driver_type]["start_time"]
        end_time = ground_truth[driver_type]["end_time"]
        og_signal_db = glob.glob(os.path.join(execution_run_folder,app_name,driver_type,db_path))

        if len(og_signal_db)>1:
            greater_date=None
            greater_file=''
            for f in og_signal_db:
                path, filename = os.path.split(f) 
                d1=filename.split('-',1)[1].split('.backup')[0]
                formatted_date=time.strptime(d1,'%Y-%m-%d-%H-%M-%S')
                if greater_date==None or formatted_date > greater_date:
                    greater_date=formatted_date
                    greater_file=os.path.join(path,filename)
            og_signal_db = greater_file
        elif len(og_signal_db)==1:
            og_signal_db = og_signal_db[0]    
        else:
            baseline_events[app_name][driver_type]={"messages":list_of_messages}
            continue           

        path, filename = os.path.split(og_signal_db) 
        decrypted_signal_db = os.path.join(path,"decrypted_signal_db")

        if os.path.exists(os.path.join(decrypted_signal_db,"database.sqlite")):
            signal_db = os.path.join(decrypted_signal_db,"database.sqlite")
        else:
            signal_db = decrypt_backup.experiment_main(og_signal_db,"321619803539442762180775720302",decrypted_signal_db)
            
        messages = connect_and_query(signal_db,["""SELECT m._id as mid, m.address as uid, m.date/1000 as d, m.body AS "Message", 
                                                    CASE
                                                    WHEN m.type=10485780 THEN "received"
                                                    WHEN m.type=10485783 THEN "sent"
                                                    END AS "Message Direction",
                                                    r.profile_joined_name AS "contact", r.phone
                                                    FROM sms m
                                                    JOIN recipient r ON r._id=uid
                                                    where (d >= """+start_time+""" and d < """+end_time+""")
                                                """])
    
        nested_driver={}            
        for m in messages:
            message={}
            message["msg_type"]=m[4]                        
            message["text"]=m[3]
            message["date"]=m[2]
            message["message_number"]=m[0]

            if(m[4]=="sent"):
                message["to_name"]=m[5]
                message["to_phone"]=m[6]
                message["to_id"]=m[1]
                message["from_name"]=target_name
                message["from_phone"]=target_phone
                message["from_id"]=""
            elif(m[4]=="received"):                
                message["to_name"]=target_name
                message["to_phone"]=target_phone
                message["to_id"]=""
                message["from_name"]=m[5]
                message["from_phone"]=m[6]
                message["from_id"]=m[1]
            list_of_messages.append(message)    
        baseline_events[app_name][driver_type]={"messages":list_of_messages}
    return baseline_events

def generate_jitmf_app_events(execution_run_folder,jitmf_events,ground_truth):
    # for app_name in app_data.keys():
    for app_name in ground_truth.keys():
        list_of_messages=[]
        
        if not app_name in jitmf_events:
            jitmf_events[app_name]={}
        
        if not 'app_based' in ground_truth[app_name]:
            jitmf_events[app_name]['app_based']={"messages":list_of_messages}    
            continue
        start_time = ground_truth[app_name]['app_based']['start_time']
        end_time = ground_truth[app_name]['app_based']['end_time']
        
        jitmflogs_path = app_data[app_name]["jitmf_evidence"]

        app_based_evidence = os.path.join(execution_run_folder,app_name,"app_based",jitmflogs_path)
        merged_log = os.path.join(app_based_evidence,'merged-logs-uniq.jitmflog')

        if not os.path.exists(merged_log):
            merged_log = app_post_processor.process_app_based_logs(app_based_evidence,app_based_evidence)
        
        if not "ERROR" in merged_log:
            with open(merged_log,'r') as processed_file:
                for line in processed_file:
                    data = json.loads(line)

                    if app_name == "com.whatsapp" or app_name == "org.thoughtcrime.securesms":
                        if data['object'].get('date', '').strip():
                            data['object']['date']=str(int(data['object']['date'])/1000)

                    if app_name == "com.whatsapp" or (float(data['object']['date'])>float(start_time) and float(data['object']['date'])<float(end_time) ):
                        message=data['object']
                        list_of_messages.append(message)
        jitmf_events[app_name]['app_based']={"messages":list_of_messages}
    return jitmf_events 

def generate_jitmf_api_events(execution_run_folder,jitmf_events,ground_truth):
    for app_name in ground_truth.keys():    
        list_of_messages=[]
        if not app_name in jitmf_events:
            jitmf_events[app_name]={}

        if not 'api_based' in ground_truth[app_name]:
            jitmf_events[app_name]['api_based']={"messages":list_of_messages}    
            continue

        start_time = ground_truth[app_name]['api_based']['start_time']
        end_time = ground_truth[app_name]['api_based']['end_time']
        
        jitmflogs_path = app_data[app_name]["jitmf_evidence"]

        app_based_evidence = os.path.join(execution_run_folder,app_name,"api_based",jitmflogs_path)
        merged_log = os.path.join(app_based_evidence,'merged-logs-uniq.jitmflog')

        if not os.path.exists(merged_log):
            fn_name=app_data[app_name]["name"].lower()            
            merged_log=api_post_processor.process_api_based_logs(fn_name,app_based_evidence,app_based_evidence)
        
        if not "ERROR" in merged_log:
            with open(merged_log,'r') as processed_file:
                for line in processed_file:
                    data = json.loads(line)
                
                    if app_name == "com.whatsapp" or app_name == "org.thoughtcrime.securesms":
                        if data['object'].get('date', '').strip():
                            data['object']['date']=str(int(data['object']['date'])/1000)

                    if app_name != "org.telegram.messenger.web" or (app_name == "org.telegram.messenger.web" and float(data['object']['date'])>float(start_time) and float(data['object']['date'])<float(end_time) ):
                        message=data['object']
                        list_of_messages.append(message)
        
        jitmf_events[app_name]['api_based']={"messages":list_of_messages}
    return jitmf_events

def get_stats(execution_run_folder,app_name):

    stats = {'app_based':{'crashes':'','size_bytes':''},'api_based':{'crashes':'','size_bytes':''}}

    for driver in stats.keys():
        
        root_path = os.path.join(execution_run_folder,app_name,driver)
        logs_path = os.path.join(root_path,app_data[app_name]['jitmf_evidence'])
        crash_path = os.path.join(root_path,'logcatcrashes.txt')
        
        if os.path.exists(logs_path):    
            if os.path.exists(crash_path):
                with open(crash_path) as f:
                    matches = re.findall("Fatal signal.*\(SIGSEGV\).*"+app_name[-15:].replace('.','\.')+"|Fatal signal.*\(SIGABRT\).*"+app_name[-15:].replace('.','\.'),f.read())
                if len(matches):
                    stats[driver]['crashes']='1'            
            else:
                stats[driver]['crashes']='0'    
            
            file_size_sum=0
            for f in glob.glob(os.path.join(logs_path,'*.jitmflog')):
                if not 'merged-logs' in f:
                    file_size_sum+= os.stat(f).st_size
            
            stats[driver]['size_bytes']=str(file_size_sum)
        else:
            stats[driver]['crashes']='n/a'
            stats[driver]['size_bytes']='n/a'
    return stats

def compare_old(filename, execution_run_folder,ground_truth,baseline_events,jitmf_events):
    for app_name in ground_truth.keys():
        stats = get_stats(execution_run_folder,app_name)

        gt_api = ground_truth[app_name].get('api_based',{}).get('messages',[])
        gt_app = ground_truth[app_name].get('app_based',{}).get('messages',[])

        bl_api = baseline_events[app_name].get('api_based',{}).get('messages',[])
        bl_app = baseline_events[app_name].get('app_based',{}).get('messages',[])

        jitmf_api = jitmf_events[app_name].get('api_based',{}).get('messages',[])
        jitmf_app = jitmf_events[app_name].get('app_based',{}).get('messages',[])

        # print(execution_run_folder+"|"+app_name+"|api_based|"+str(len(gt_api))+"|"+str(len(bl_api))+"|"+str(len(jitmf_api)))
        # print(execution_run_folder+"|"+app_name+"|app_based|"+str(len(gt_app))+"|"+str(len(bl_app))+"|"+str(len(jitmf_app)))

        found_api=0
        for m in gt_api:
            gt_text = m['text']
            found=False
            for m2 in jitmf_api:
                jitmf_text=m2['text']
                if gt_text.lower() in jitmf_text.lower():
                    found_api+=1
                    found=True
                    break
            # if not found:
                # print(f"{os.path.basename(execution_run_folder)} | {app_name} | api_based | {m}")

        found_app=0
        for m in gt_app:
            gt_text = m['text']
            found=False
            for m2 in jitmf_app:
                jitmf_text=m2['text']
                if gt_text.lower() in jitmf_text.lower():
                    found_app+=1
                    found=True
                    break
            # if not found:
                # print(f"{os.path.basename(execution_run_folder)} | {app_name} | app_based | {m}")

        bl_api_cnt=0
        for m in gt_api:
            gt_text = m['text']
            for m2 in bl_api:
                bl_text=m2['text']
                if bl_text and gt_text.lower() in bl_text.lower():
                    bl_api_cnt+=1
                    break

        bl_app_cnt=0
        for m in gt_app:
            gt_text = m['text']
            for m2 in bl_app:
                bl_text=m2['text']
                if bl_text and gt_text.lower() in bl_text.lower():
                    bl_app_cnt+=1
                    break
        
        if len(gt_api) == 0 or bl_api_cnt==0 or found_api==0:
            pass
        else:
            print(os.path.basename(execution_run_folder)+","+app_name+",api_based,"+str(len(gt_api))+","+str(bl_api_cnt)+","+str(found_api)+","+stats['api_based']['size_bytes']+","+stats['api_based']['crashes'])
        if len(gt_app) == 0 or bl_app_cnt==0 or found_app==0:
            pass
        else:
            print(os.path.basename(execution_run_folder)+","+app_name+",app_based,"+str(len(gt_app))+","+str(bl_app_cnt)+","+str(found_app)+","+stats['app_based']['size_bytes']+","+stats['app_based']['crashes'])
            
def compare_new(filename, execution_run_folder,ground_truth,jitmf_events):
    for app_name in ground_truth.keys():
        stats = get_stats(execution_run_folder,app_name)

        gt_api = ground_truth[app_name].get('api_based',{}).get('messages',[])
        gt_app = ground_truth[app_name].get('app_based',{}).get('messages',[])

        jitmf_api = jitmf_events[app_name].get('api_based',{}).get('messages',[])
        jitmf_app = jitmf_events[app_name].get('app_based',{}).get('messages',[])

        not_found_list=[]

        found_api=0
        for m in jitmf_app:
            app_text = m['text']
            found=False
            for m2 in jitmf_api:
                api_text=m2['text']
                api_text_filtered=re.search("[n|N]ormal_message_[0-9]([0-9]?)",api_text)
                if api_text_filtered:
                    api_text_filtered = api_text_filtered.group()
                else:
                    api_text_filtered = ''
                if app_text.lower() == api_text_filtered.lower():
                    found_api+=1
                    found=True
                    break
            if not found:
                if ("pink" in app_text.lower() or "first" in app_text.lower() or "dummy" in app_text.lower() or app_text.lower()==''):
                    found_api+=1
                    found=True
                else:
                    not_found_list.append(m)
        
        if len(gt_api) == 0 or found_api==0:
            pass
        else:
            # if(found_api < len(jitmf_app) and app_name!="com.whatsapp"):
                # print(execution_run_folder)
                # print(app_name)
                # print(not_found_list)
                # print([m['text'] for m in jitmf_api])
                # print([m['text'] for m in jitmf_app])

            print(os.path.basename(execution_run_folder)+","+app_name+","+str(found_api)+","+str(len(jitmf_app))+","+stats['api_based']['size_bytes']+","+stats['api_based']['crashes'])

print("folder,app_name,infrastructure based driver events,baseline_events (app_based driver events),size_bytes,crashes")                
directory = os.fsencode(results_folder)
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
    
        try:            
            search_by_time = filename.split("-",1)[1].split(".txt")[0]
            execution_run_folder=glob.glob(os.path.join(results_folder,"results*_NOISE_MSGS_LOOP_*_EVIDENCE_WAIT_*-"+search_by_time))[0]

            ground_truth = generate_ground_truth(filename)
            # baseline_events={}
            jitmf_events={}

            # for app_name in ground_truth.keys():
            #     baseline_events = globals()["generate_%s_baseline_events" % app_data[app_name]['name'].lower()](execution_run_folder,baseline_events,ground_truth[app_name])

            jitmf_events = generate_jitmf_app_events(execution_run_folder,jitmf_events,ground_truth)
            jitmf_events = generate_jitmf_api_events(execution_run_folder,jitmf_events,ground_truth)

            # compare(filename,execution_run_folder,ground_truth,baseline_events,jitmf_events)
            compare_new(filename,execution_run_folder,ground_truth,jitmf_events)
        except Exception as e:
            # print(e)
            # traceback.print_exc()
            pass
