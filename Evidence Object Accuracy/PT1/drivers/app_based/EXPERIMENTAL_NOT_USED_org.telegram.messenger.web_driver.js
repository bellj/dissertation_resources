var LOG_DIR = "jitmflogs";
var APP = "telegram";
var TYPE = "app_native";
var EVENT = "Telegram Message Present";
// const trigger_points = ["send","recv"]; // "send" -> CP trigger point | "recv" -> SP trigger point
var sqlite3_prepare_v2 = 'sqlite3_prepare_v2'
var sqlite3_prepare16_v2 = 'sqlite3_prepare16_v2'

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function trace(pattern)
{
	var res = new ApiResolver("module");
	var matches = res.enumerateMatchesSync(pattern);
	var targets = uniqBy(matches, JSON.stringify);
	return targets[0].address
}

function uniqBy(array, key)
{
	var seen = {};
	return array.filter(function(item) {
		var k = key(item);
		return seen.hasOwnProperty(k) ? false : (seen[k] = true);
	});
}

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance) {
    var object = ''
    try {
        var messageObject = Java.cast(instance, Java.use("org.telegram.messenger.MessageObject")) 
        var text = Java.cast(messageObject.messageText.value, Java.use("java.lang.CharSequence"));

        var messageOwner = Java.cast(messageObject.messageOwner.value, Java.use("org.telegram.tgnet.TLRPC$Message"));
        var fromuser = Java.cast(messageOwner.from_id.value, Java.use("org.telegram.tgnet.TLRPC$TL_peerUser"));
        var peeruser = Java.cast(messageOwner.peer_id.value, Java.use("org.telegram.tgnet.TLRPC$TL_peerUser"));
            
        var to_username='';
        var to_phone='';
        var from_username='';
        var from_phone='';       
        Java.choose("org.telegram.messenger.MessagesController", {
            onMatch: function (instance) {                
                to_username = '';
                to_phone = '';
                var TLRPCUser = Java.use("org.telegram.tgnet.TLRPC$TL_user");                      

                var longintClass = Java.use("java.lang.Long");

                var longObj_id = longintClass.valueOf(peeruser.user_id.value.toString());
                var to_user = Java.cast(instance.getUser(longObj_id), TLRPCUser);
                to_username = to_user.username.value;
                to_phone = to_user.phone.value; // TO GET TO AND FROM RENAMING MIDDLE OF "TO"

                longObj_id = longintClass.valueOf(fromuser.user_id.value.toString());
                var from_user = Java.cast(instance.getUser(longObj_id), TLRPCUser);
                from_username = from_user.username.value;
                from_phone = from_user.phone.value; // TO GET TO AND FROM RENAMING MIDDLE OF "TO"

                if(to_username.length>0){
                    return 'stop';
                }
                
            },
            onComplete: function () { }
        });
        object = '{"date": "' + messageOwner.date.value + '", "message_id": "' + messageOwner.id.value + '", "text": "' + text + '", "to_id": "' + peeruser.user_id.value+ '", "to_name": "' + to_username +'", "to_phone": "' + to_phone  + '", "from_id": "' + fromuser.user_id.value+ '", "from_name": "' + from_username +'", "from_phone": "' + from_phone  + '"}'
        
    } catch (err) { }
    
    return object
}

function getMessageObjects(time,TP) {
    var klass = "org.telegram.messenger.MessageObject"
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = parseObject(instance)
            if (object != '') {
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": '
                jitmfLog += object + '}\n'
            }

        }, onComplete: function () {
            complete = true
        }
    });
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        var predicateFlag = false;
        var samplingFlag = true;
        var dumpFlag = false;

        var impl = trace("exports:*!"+sqlite3_prepare_v2)
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf8String(args[1]);
            },
            onLeave: function(retval) {
                var TP=sqlite3_prepare_v2
                if (sql_query.includes("UPDATE") || sql_query.includes("REPLACE")){
                    predicateFlag = true
                }
                dumpFlag = samplingFlag && predicateFlag;
                console.log(dumpFlag)    
                if (dumpFlag) {
                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                }
                jitmfHeapLog = ''
                jitmfLog = ''
            }  
        });

        impl = trace("exports:*!"+sqlite3_prepare16_v2)
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf16String(args[1]);
            },
            onLeave: function(retval) {
                var TP=sqlite3_prepare16_v2
                if (sql_query.includes("UPDATE") || sql_query.includes("REPLACE")){
                    predicateFlag = true
                }
                dumpFlag = samplingFlag && predicateFlag;
                    
                if (dumpFlag) {
                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                }
                jitmfHeapLog = ''
                jitmfLog = ''
            }  
        });
    });
});