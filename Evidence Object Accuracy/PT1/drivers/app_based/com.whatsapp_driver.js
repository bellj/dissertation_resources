var LOG_DIR = "jitmflogs";
var APP = "whatsapp";
var TYPE = "app_sqlite";
var EVENT = "Whatsapp Message Sent";
var trigger_point = "android.database.sqlite.SQLiteDatabase";

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    Log.v(TAG_L, "[*] external_storage_dir "+external_storage_dir)
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance) {
    // sample of instance
    // 1Ph key=Key[id=3EB0857B3C7478760A93, from_me=false, remote_jid=363043366859585@g.us] media_wa_type=0

    var object = ''

    var to_id=''
    var from_id=''
    var to_name=''
    var from_id=''
    var from_name=''
    var to_phone=''
    var from_phone=''
    var text=''
    var message_id=''
    var date=''

    var x1ph = instance.toString().split(" ");
    var contact = x1ph[3].split("=")[1] //contact

    message_id = x1ph[1].split("=")[2] //key
    if (x1ph[2].split("=")[1].includes("false")){ //from me
        to_name="phone_owner"
        from_id=contact
    } else {
        to_id= contact
        from_name="phone_owner"
    }
    
    try {
        Java.choose("X.2za", {
            onMatch: function (instance_2) { 
                var x3id= Java.use("X.3Id");
                var x3idinstance = Java.cast(instance_2,x3id);
                var test=x3idinstance.A03(instance);
                var parid=Java.cast(test,Java.use("android.util.Pair"))
                text=parid.second.value;

                if(test.toString().length>0){
                    return 'stop';
                }
            },
            onComplete: function () { }
        })
           
        object = '{"date": "' + date + '", "message_id": "' + message_id + '", "text": "' + text + '", "to_id": "' + to_id+ '", "to_name": "' + to_name +'", "to_phone": "' + to_phone  + '", "from_id": "' + from_id+ '", "from_name": "' + from_name +'", "from_phone": "' + from_phone  + '"}'
        
    } catch (err) { console.log(err)}
    
    return object
}

function getMessageObjects(time,TP) {
    var klass = "X.1Ph"
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = parseObject(instance)
            if (object != '') {
                console.log(instance)
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": '
                jitmfLog += object + '}\n'
            }

        }, onComplete: function () {
            complete = true
        }
    });
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + trigger_point + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
    
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        Log.v(TAG_L,hookLogName)
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
                
        var timestamp =""
        var targetMethod = "insert";
        var targetClassMethod = trigger_point+"."+targetMethod;
        var hook = Java.use(trigger_point);
        var overloadCount = hook[targetMethod].overloads.length;

        console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]");

        for (var i = 0; i < overloadCount; i++) {

            hook[targetMethod].overloads[i].implementation = function() {
                dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,trigger_point)
                var retval = this[targetMethod].apply(this, arguments);
                return retval;
            }
        }        
    });
});
