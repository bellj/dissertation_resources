var LOG_DIR = "jitmflogs";
var APP = "signal";
var TYPE = "app_native";
var EVENT = "Signal Message Sent";
var trigger_point = "net.zetetic.database.sqlcipher.SQLiteDatabase";

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var external_storage_dir = "";

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("androidx.core.content.ContextCompat");
    var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
    var context = currentApplication.getApplicationContext();
    external_storage_dir = Env.getExternalFilesDirs(context,null)[0].getAbsolutePath();
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null;
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(trigger_point)
    }

   
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    }

}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var Log = Java.use("android.util.Log");
        getExternalStorageDirectory();
        var hookLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        
        var sqliteDatabase = Java.use(trigger_point);
        
        var timeSent=''
        var text=''
        var to_id=''
        var from_id=''
        var from_name=''
        var from_username=''
        var from_phone=''
        var message_id=''

        sqliteDatabase.insertWithOnConflict.overload('java.lang.String', 'java.lang.String', 'android.content.ContentValues', 'int').implementation = function(var0, var1, var2, var3) {
            try{    
                var time = getTime();
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + trigger_point + '.insertWithOnConflict", "object": "'
                if (var2.containsKey("receipt_timestamp")){
                    text = var2.get("body")
                    timeSent = var2.get("date_sent")
                }
                if (var2.containsKey("serialized_data")){
                    to_id = var2.get("queue_key")
                    from_id="phone_owner"
                    from_name="phone_owner"
                    from_username="phone_owner"
                    from_phone="phone_owner"
                    message_id = JSON.parse(var2.get("serialized_data")).longs.message_id
                   
                    var item = '{"date": "' + timeSent + '", "message_id": "' + message_id + '", "text": "' + text + '", "to_id": "' + to_id+ '", "to_name": "", "to_phone": "", "from_id": "' + from_id+ '", "from_name": "' + from_username +'", "from_phone": "' + from_phone  + '"}';

                    jitmfLog += item + '"}\n'

                    Log.v(TAG_L,jitmfLog)
                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,trigger_point)
                }
            }
            catch (err) { 
                Log.v(TAG_L,"eroor"+err)
            }
            var insertValueRes = this.insertWithOnConflict(var0, var1, var2,var3);
            return insertValueRes;
        };
        
    });
});