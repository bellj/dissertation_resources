// n8G"QY<J9"QYK4&c (TEXT HERE ) c=

var LOG_DIR = "jitmflogs";
var APP = "any";
var TYPE = "API";
var EVENT = "Message Sent";

var sqlite3_clear_bindings = 'sqlite3_clear_bindings'
var sqlite3_prepare_v2 = 'sqlite3_prepare_v2'
var sqlite3_prepare16_v2 = 'sqlite3_prepare16_v2'
var sqlite3_bind_int   = 'sqlite3_bind_int'
var sqlite3_bind_int64   = 'sqlite3_bind_int64'
var sqlite3_bind_text  = 'sqlite3_bind_text'
var sqlite3_bind_text16  = 'sqlite3_bind_text16'
var sqlite3_bind_blob  = 'sqlite3_bind_blob'
var triggers_all= sqlite3_clear_bindings+ "|"+sqlite3_prepare_v2+ "|"+sqlite3_prepare16_v2+ "|"+sqlite3_bind_int+ "|"+sqlite3_bind_int64+ "|"+sqlite3_bind_text+ "|"+sqlite3_bind_text16+ "|"+sqlite3_bind_blob

var sql_query = '';
var query_dict={}
var og_query_dict={}

var jitmfHeapLog = ''
var inFile = false

var online=true

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function trace(pattern)
{
	var res = new ApiResolver("module");
	var matches = res.enumerateMatchesSync(pattern);
	var targets = uniqBy(matches, JSON.stringify);
	return targets[0].address
}

function check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName){
    var value = query_dict[address]
    if( value.indexOf('?') === -1 ){

        var time = getTime();
        var item = '{"Query Executed": "'+value+'"}'

        var jitmfLog = '{"time": "' + time + '","event": "' +EVENT+'","trigger_point(s)": "'+triggers_all+'", "object": "'
        jitmfLog += item + '"}\n'

        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,triggers_all, jitmfLog)
    }
}

function uniqBy(array, key)
{
	var seen = {};
	return array.filter(function(item) {
		var k = key(item);
		return seen.hasOwnProperty(k) ? false : (seen[k] = true);
	});
}

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    Log.v(TAG_L, "[*] external_storage_dir "+external_storage_dir)

    // const Env = Java.use("androidx.core.content.ContextCompat");
    // var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
    // var context = currentApplication.getApplicationContext();
    // external_storage_dir = Env.getExternalFilesDirs(context,null)[0].getAbsolutePath();

    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{      
        var file = new File(fileName, mode);
        console.log("Contents before: "+contents)
        contents=contents.replace(/[^\x20-\x7E]/g, '')+'\n';
        console.log("Contents AFTER: "+contents)
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP, jitmfLog){
    var time = getTime();
    var hookLog = time + ',' + triggers_all + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(triggers_all)
    }
    
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    } 
}

setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";

        var impl = trace("exports:*!"+sqlite3_bind_int64)
        Interceptor.attach(impl, {
            onEnter: function(args) {
                var address = args[0].toString()
                query_dict[address] = query_dict[address].replace('?',parseInt(args[2].toString(),16))
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_int)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                query_dict[address] = query_dict[address].replace('?',parseInt(args[2].toString(),16))
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_text)
        Interceptor.attach(impl, {
            onEnter: function(args) {  
                var address = args[0].toString()
                query_dict[address] = query_dict[address].replace('?',Memory.readUtf8String(args[2]))
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_text16)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                query_dict[address] = query_dict[address].replace('?',Memory.readUtf16String(args[2]))
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_blob)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                var mem = Memory.readByteArray(args[2],parseInt(args[3]))
                console.log("TESTT")
                console.log(mem)
                var b = new Uint8Array(mem);
                var str = "";
                for(var i = 0; i < b.length; i++) {						
                    str+=String.fromCharCode(b[i] & 0xff)
                }
                console.log("STR::: "+str)
                console.log("STR2: "+str.replace(/[^\x20-\x7E]/g, ''));
                console.log("QUERY BEFORE>>> "+query_dict[address])
                query_dict[address] = query_dict[address].replace('?',str)
                console.log("QUERY AFTER>>> "+query_dict[address])
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
            }
        });

        impl = trace("exports:*!"+sqlite3_prepare_v2)
        var address_og=null
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf8String(args[1]);
                address_og=args[3];        
            },
            onLeave: function(retval) {
                if(address_og!=null && sql_query!=null){
                    query_dict[address_og.readPointer()]=sql_query
                    og_query_dict[address_og.readPointer()]=sql_query
                }
            }  
        });

        impl = trace("exports:*!"+sqlite3_prepare16_v2)
        var address_og=null
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf16String(args[1]);
                address_og=args[3];        
            },
            onLeave: function(retval) {
                if(address_og!=null && sql_query!=null){
                    query_dict[address_og.readPointer()]=sql_query
                    og_query_dict[address_og.readPointer()]=sql_query
                }
            }  
        });

        impl = trace("exports:*!"+sqlite3_clear_bindings)
        var address_og=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                var address = args[0].toString()
                query_dict[address] = og_query_dict[address]      
            }   
        });
    })
})



// impl = trace_2("exports:*!"+sqlite3_any_stmt)