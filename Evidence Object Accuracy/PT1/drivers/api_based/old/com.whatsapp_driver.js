var LOG_DIR = "jitmflogs";
var APP = "whatsapp";
var TYPE = "API";
var EVENT = "Whatsapp Message Sent";
var trigger_point = "android.database.sqlite.SQLiteDatabase";

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    Log.v(TAG_L, "[*] external_storage_dir "+external_storage_dir)
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + trigger_point + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(trigger_point)
    }
    
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    }
    
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        Log.v(TAG_L,hookLogName)
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
                
        var sqliteDatabase = Java.use(trigger_point);
        var timestamp =""
        
        sqliteDatabase.update.overload('java.lang.String', 'android.content.ContentValues', 'java.lang.String', '[Ljava.lang.String;').implementation = function(var0, var1, var2, var3) {
            Log.v(TAG_L,"[*] IN")
            if (var1.containsKey("sort_timestamp"))
            {
                timestamp = var1.get("sort_timestamp")
            }
            var updateRes = this.update(var0, var1, var2, var3);
            return updateRes;
        };

        sqliteDatabase.insert.overload('java.lang.String', 'java.lang.String', 'android.content.ContentValues').implementation = function(var0, var1, var2) {
            try{    
                if (var2.containsKey("item")){
                    var item = var2.get("item") 
                    var time = getTime();
                    jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + trigger_point + '.insert", "object": "'
                    jitmfLog += item + '","time_of_message_event": "'+timestamp+'"}\n'

                    Log.v(TAG_L,jitmfLog)
                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,trigger_point)
                }
            }
            catch (err) { 
                Log.v(TAG_L,"eroor"+err)
            }
            var insertValueRes = this.insert(var0, var1, var2);
            return insertValueRes;
        };
        
    });
});
