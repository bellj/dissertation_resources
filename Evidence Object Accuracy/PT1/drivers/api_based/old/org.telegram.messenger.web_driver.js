var LOG_DIR = "jitmflogs";
var APP = "telegram";
var TYPE = "API";
var EVENT = "Telegram Message Sent";
var trigger_point_1 = "sqlite3_prepare_v2";
var trigger_point_2 = "sqlite3_bind_int64";
var trigger_point_3 = "sqlite3_bind_blob";
var triggers_all =""

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function uniqBy(array, key)
{
	var seen = {};
	return array.filter(function(item) {
		var k = key(item);
		return seen.hasOwnProperty(k) ? false : (seen[k] = true);
	});
}

function trace(pattern)
{
	var res = new ApiResolver("module");
	var matches = res.enumerateMatchesSync(pattern);
	var targets = uniqBy(matches, JSON.stringify);
	return targets[0].address
}


function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    Log.v(TAG_L, "[*] external_storage_dir "+external_storage_dir)
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + triggers_all + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(triggers_all)
    }
    
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    }
    
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        Log.v(TAG_L,hookLogName)
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
         
        var timestamp =""

        var address=null
        var replace_query=false
		var waiting_for_insert=false
		var current_user_object=null
		var waiting_for_insert_address =null
		var jitmf_object=null
		var param_counter=0

        var impl=trace("exports:*!"+trigger_point_1)
		Interceptor.attach(impl,{
			onEnter: function(args) {
				if(Memory.readCString(args[1]).includes("REPLACE INTO messages_v2"))
				{
					address=args[3]
                    replace_query=true
				}
				if(Memory.readCString(args[1]).includes("REPLACE INTO users"))
				{
					waiting_for_insert_address=args[3]
                    waiting_for_insert=true
				}
			},
			onLeave: function(retval) {
				if(address!=null && replace_query){
					jitmf_object={}
					jitmf_object["address"]=address.readPointer()
                    replace_query=false
				}
				if(waiting_for_insert_address!=null && waiting_for_insert){
					current_user_object={}
					current_user_object["address"]=waiting_for_insert_address.readPointer()
                    waiting_for_insert=false
				}
			}
		});

		impl=trace("exports:*!"+trigger_point_2)
		Interceptor.attach(impl,{
			onEnter: function(args) {
				if(jitmf_object!=null && jitmf_object["address"].toString() === args[0].toString()){
					if(param_counter===0){
						jitmf_object["mid"]=parseInt(args[2].toString(),16)
						param_counter++
					}
					else if(param_counter===1){
						jitmf_object["uid"]=parseInt(args[2].toString(),16)
						param_counter++
					}
					else if(param_counter===2 || param_counter===3){
						param_counter++
					}
					else if(param_counter===4){
						jitmf_object["timestamp"]=parseInt(args[2].toString(),16)
						param_counter++
					}
				}				       				
			}
		});

		impl=trace("exports:*!"+trigger_point_3)
		Interceptor.attach(impl,{
			onEnter: function(args) {
				if(current_user_object!=null && current_user_object["address"].toString()===args[0].toString()){
					var mem = Memory.readByteArray(args[2],parseInt(args[3]))
					var b = new Uint8Array(mem);
					var str = "";
					for(var i = 25; i < b.length-8; i++) {						
						str+=String.fromCharCode(b[i] & 0xff)
					}
					current_user_object["name"]=str.split(/\s+/)[0]
                    current_user_object["phone"]=str.split(/\s+/)[1]
				}	
				if(jitmf_object!=null && jitmf_object["address"].toString() === args[0].toString()){
					var mem = Memory.readByteArray(args[2],parseInt(args[3]))
					var b = new Uint8Array(mem);
					var str = "";
					for(var i = 41; i < b.length-8; i++) {						
						str+=String.fromCharCode(b[i] & 0xff)
					}
					jitmf_object["text"]=str;
					if(current_user_object!=null){
						jitmf_object["username"]=current_user_object["name"]
                        jitmf_object["phone"]=current_user_object["phone"]
					}
					param_counter++
				}			
			},
			onLeave: function(retval) {
				if(param_counter==6){
                    var time = getTime();
                    var item = '{"date": "' + jitmf_object["timestamp"] + '", "message_id": "' + jitmf_object["mid"] + '", "text": "' + jitmf_object["text"] + '", "to_id": "' + jitmf_object["uid"] + '", "to_name": "' + jitmf_object["username"] +'", "to_phone": "' + jitmf_object["phone"]  + '", "from_id": "' + "phone_owner"+ '", "from_name": "' + "phone_owner" +'", "from_phone": "' + "phone_owner"  + '"}'


                    triggers_all= trigger_point_1 +"|"+trigger_point_2+"|"+trigger_point_3

                    jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point(s)": "'+triggers_all+'", "object": "'
                    jitmfLog += item + '"}\n'

                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,triggers_all)

					jitmf_object=null
                    jitmfLog=''
					address=null
					param_counter=0
				}
			}
		});



















        
    });
});
