// n8G"QY<J9"QYK4&c (TEXT HERE ) c=
// ?SK4Qwpatrick35699424312?pb
// THIS IS THE GENERIC SQLITE DRIVER

var LOG_DIR = "jitmflogs";
var APP = "any";
var TYPE = "API";
var EVENT = "Message Sent";

var sqlite3_finalize = 'sqlite3_finalize'
var sqlite3_clear_bindings = 'sqlite3_clear_bindings'
var sqlite3_prepare_v2 = 'sqlite3_prepare_v2'
var sqlite3_prepare16_v2 = 'sqlite3_prepare16_v2'
var sqlite3_bind_int   = 'sqlite3_bind_int'
var sqlite3_bind_int64   = 'sqlite3_bind_int64'
var sqlite3_bind_text  = 'sqlite3_bind_text'
var sqlite3_bind_text16  = 'sqlite3_bind_text16'
var sqlite3_bind_blob  = 'sqlite3_bind_blob'
var triggers_all= sqlite3_clear_bindings+ "|"+sqlite3_prepare_v2+ "|"+sqlite3_prepare16_v2+ "|"+sqlite3_bind_int+ "|"+sqlite3_bind_int64+ "|"+sqlite3_bind_text+ "|"+sqlite3_bind_text16+ "|"+sqlite3_bind_blob+"|"+sqlite3_finalize

var sql_query = '';
var query_dict={}
var og_query_dict={}
var external_storage_dir = "";

var jitmfHeapLog = ''
var inFile = false

var online=true

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function trace(pattern)
{
	var res = new ApiResolver("module");
	var matches = res.enumerateMatchesSync(pattern);
	var targets = uniqBy(matches, JSON.stringify);
	return targets[0].address
}

function check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName){
    try{
        var values = query_dict[address]["Values"]
        var query = query_dict[address]["Query"]

        var log_entry=query;

        for (var i = 0; i < values.length; i++) { 
            log_entry = log_entry.replace('?',values[i])
        }

        var time = getTime();
        var item = '{"Query Executed": "'+log_entry+'"}'

        var jitmfLog = '{"time": "' + time + '","event": "' +EVENT+'","trigger_point(s)": "'+triggers_all+'", "object": "'
        jitmfLog += item + '"}\n'

        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,triggers_all, jitmfLog)
    }
    catch(err){}
}

function replace_bind_value(position, address, value){
    position = parseInt(position.toString(),16)
    try{
        query_dict[address]["Values"][position-1]=value
    }
    catch (err) {}
}

function uniqBy(array, key)
{
	var seen = {};
	return array.filter(function(item) {
		var k = key(item);
		return seen.hasOwnProperty(k) ? false : (seen[k] = true);
	});
}

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
        const Env = Java.use("androidx.core.content.ContextCompat");
        var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
        var context = currentApplication.getApplicationContext();
        external_storage_dir = Env.getExternalFilesDirs(context,null)[0].getAbsolutePath();
    }   


function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{      
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()

    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP, jitmfLog){
    var time = getTime();
    var hookLog = time + ',' + triggers_all + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(triggers_all)
    }
    
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    } 
}

setImmediate(function () {
    Java.perform(function () {
        var Log = Java.use("android.util.Log");
        getExternalStorageDirectory();
        var hookLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        
        var impl = trace("exports:*!"+sqlite3_bind_int64)
        Interceptor.attach(impl, {
            onEnter: function(args) {
                var address = args[0].toString()
                replace_bind_value(args[1], address, parseInt(args[2].toString(),16))
                
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_int)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                replace_bind_value(args[1], address, parseInt(args[2].toString(),16))
                
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_text)
        Interceptor.attach(impl, {
            onEnter: function(args) {  
                var address = args[0].toString()
                replace_bind_value(args[1], address, Memory.readUtf8String(args[2]))
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_text16)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                replace_bind_value(args[1], address, Memory.readUtf16String(args[2]))
                
            }
        });

        impl = trace("exports:*!"+sqlite3_bind_blob)
        Interceptor.attach(impl, {
            onEnter: function(args) {       
                var address = args[0].toString()
                var mem = Memory.readByteArray(args[2],parseInt(args[3]))
                var b = new Uint8Array(mem);
                var str = "";
                for(var i = 0; i < b.length; i++) {						
                    str+=String.fromCharCode(b[i] & 0xff)
                }
                str=str.replace(/[^\x20-\x7E]/g, '');
                replace_bind_value(args[1], address,str)
            }
        });

        impl = trace("exports:*!"+sqlite3_prepare_v2)
        var address_og=null
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf8String(args[1]);
                address_og=args[3];        
            },
            onLeave: function(retval) {
                if(address_og!=null && sql_query!=null){
                    var counter =0
                    for(var i=0; i<sql_query.length;i++) {
                        if (sql_query[i] === "?") counter++;
                    }
                    var indices = Array.apply(null, Array(counter))
                    query_dict[address_og.readPointer()]={"Query": sql_query, "Values": indices}
                }
            }  
        });

        impl = trace("exports:*!"+sqlite3_prepare16_v2)
        var address_og=null
        var sql_query=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                sql_query = Memory.readUtf16String(args[1]);
                address_og=args[3];        
            },
            onLeave: function(retval) {
                if(address_og!=null && sql_query!=null){
                    var counter =0
                    for(var i=0; i<sql_query.length;i++) {
                        if (sql_query[i] === "?") counter++;
                    }
                    var indices = Array.apply(null, Array(counter))
                    query_dict[address_og.readPointer()]={"Query": sql_query, "Values": indices}
                }
            }  
        });

        impl = trace("exports:*!"+sqlite3_clear_bindings)
        var address_og=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                var address = args[0].toString()   
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
                try{
                    for (var i = 0; i < query_dict[address]["Values"].length; i++) { 
                        query_dict[address]["Values"][i]=null
                    }
                }
                catch(err){}
            }   
        });

        impl = trace("exports:*!"+sqlite3_finalize)
        var address_og=null
        Interceptor.attach(impl, {
            onEnter: function(args) {
                var address = args[0].toString()
                check_for_qs(address,hookLogName,jitmfLogName,jitmfHeapLogName)
                try{
                    delete query_dict[address]
                }
                catch(err){}
                
            }   
        });
    })
})