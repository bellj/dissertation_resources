import re
import sys
import os
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

def getViewList(view, viewList):
    # print(view.getId())
    if view.getId() == 'com.whatsapp:id/message_text':
        viewList.append(view)
    for ch in view.children:
        getViewList(ch, viewList)
    return viewList

contact_name='contact_phone'
_s = 3

device = sys.argv[1]
target_device = device
contact_device = sys.argv[2]
folder_name = sys.argv[3]
sys.argv = [sys.argv[0]]

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity(component='com.whatsapp/com.whatsapp.Main')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.dump(window=-1)
vc.sleep(_s)

vc.findViewWithTextOrRaise(contact_name).touch()
vc.dump(window=-1)
vc.sleep(_s)

listviews = getViewList(vc.root,[])
for v in listviews:
    if (('pink' in v.getText().lower()) or ('flixonline' in v.getText().lower())):
        (x,y) = v.getXY()
        v.device.drag((x,y), (x,y), 2000, 1)
        vc.sleep(_s)
        vc.dump(window=-1)

        menuitem_delete = vc.findViewByIdOrRaise('com.whatsapp:id/menuitem_delete')
        menuitem_delete.touch()
        vc.sleep(_s)
        vc.dump(window=-1)

        delete_for_me_button = vc.findViewByIdOrRaise('android:id/button1')
        delete_for_me_button.touch()
        vc.sleep(_s)
        vc.dump(window=-1)
        print("Tried Deleting WhatsApp Pink message. Current time ... ----> "+ str(datetime.now()))

os.system("adb -s "+contact_device+" exec-out screencap -p > "+folder_name+"/screen_on_contact.png")
os.system("adb -s "+target_device+" exec-out screencap -p > "+folder_name+"/screen_on_target.png")

vc.findViewByIdOrRaise("com.whatsapp:id/back").touch()
vc.sleep(_s)
device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s)
device.shell('input keyevent KEYCODE_HOME')