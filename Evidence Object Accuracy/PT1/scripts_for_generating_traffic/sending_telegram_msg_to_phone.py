import re
import sys
import time
import os
import random
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def dump_handler():
    number_of_retries = 10
    sleep_time = 5
    counter = 0
    e=None
    while (counter < number_of_retries):
        try:
            vc.dump(window=-1)
            return
        except Exception as exc:
            e=exc
            time.sleep(sleep_time)
            counter += 1
    print("[[ FAILED TO EXECUTE VC.DUMP LATEST EXCEPTION ]]" + str(e))

MAX_WAIT=10
_s = 5

device = sys.argv[1]
no_msgs = int(sys.argv[2])
msg_counter = int(sys.argv[4])
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('org.telegram.messenger.web/org.telegram.ui.LaunchActivity')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.sleep(_s)
dump_handler()

view=None
for i in vc.getViewIds():
    v = vc.findViewById(i)
    if v.getClass() == "android.view.ViewGroup":
        view=v
        break

view.touch()
dump_handler()
vc.sleep(_s)

view=None
for i in vc.getViewIds():
    v = vc.findViewById(i)
    if v.getClass() == "android.widget.EditText":
        view=v
        break

for i in range(no_msgs):
    # message_text="Normal_message_"+str(get_random_string(15))
    message_text="Normal_message_"+str(msg_counter)
    msg_counter+=1
    wait_between_messages=random.randint(1,MAX_WAIT)
    view.setText(message_text)
    dump_handler()
    vc.sleep(_s)
    vc.findViewWithContentDescriptionOrRaise(u'''Send''').touch()
    print("TELEGRAM | Sent message ["+str(i+1)+"/"+str(no_msgs)+"]: | text="+message_text+". Current time ... ----> "+ str(datetime.now()))
    dump_handler()
    vc.sleep(_s)
    time.sleep(wait_between_messages)

dump_handler()
vc.findViewWithContentDescriptionOrRaise(u'''Go back''').touch()
device.shell('input keyevent KEYCODE_BACK')