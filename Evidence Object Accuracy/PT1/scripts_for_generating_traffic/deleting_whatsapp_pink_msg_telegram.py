import re
import sys
import time
import os
import random
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

def getViewList(v, viewList):
    if viewList is None or len(viewList) ==0:
        p = v.getParent()
        if (('pink' in v.getText().lower()) or ('flixonline' in v.getText().lower())):
            return v
    else:
        for c in viewList:
            x = getViewList(c, c.getChildren())
            if x is not None:
                return x
    return None

MAX_WAIT=10
_s = 3

device = sys.argv[1]
target_device = device
contact_device = sys.argv[2]
folder_name = sys.argv[3]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('org.telegram.messenger.web/org.telegram.ui.LaunchActivity')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.sleep(_s)
vc.dump(window=-1)

view=None
for i in vc.getViewIds():
    v = vc.findViewById(i)
    if v.getClass() == "android.view.ViewGroup":
        view=v
        break

view.touch()
vc.dump(window=-1)
vc.sleep(_s)

view_returned=None
for i in vc.getViewIds():
    v = vc.findViewById(i)
    view_returned = getViewList(v, v.getChildren())
    if view_returned is not None:
        break

vc.dump(window=-1)
vc.sleep(_s)
(x,y) = view_returned.getXY()
y=y+10
view_returned.device.drag((x,y), (x,y), 3000, 1)

vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithContentDescriptionOrRaise(u'''Delete''').touch()
vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithTextOrRaise(u'DELETE').touch()
vc.sleep(_s)
vc.dump(window=-1)
print("Tried Deleting WhatsApp Pink message. Current time ... ----> "+ str(datetime.now()))

os.system("adb -s "+contact_device+" exec-out screencap -p > "+folder_name+"/screen_on_contact.png")
os.system("adb -s "+target_device+" exec-out screencap -p > "+folder_name+"/screen_on_target.png")

vc.findViewWithContentDescriptionOrRaise(u'''Go back''').touch()
device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s)
device.shell('input keyevent KEYCODE_HOME')