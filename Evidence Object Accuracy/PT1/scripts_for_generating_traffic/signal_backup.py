import re
import sys
import time
import os
import random
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

MAX_WAIT=10
_s = 2

device = sys.argv[1]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('org.thoughtcrime.securesms/.RoutingActivity')

kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)
vc.sleep(_s)
vc.dump(window=-1)
vc.sleep(_s)

vc.findViewWithContentDescriptionOrRaise(u'''More options''').touch()

vc.sleep(_s)
vc.dump(window=-1)

# v = vc.findViewWithTextOrRaise("Settings").touch()
vc.findViewByIdOrRaise("id/no_id/9").touch()
vc.sleep(_s)
vc.dump(window=-1)
# v = vc.findViewWithTextOrRaise("Chats").touch()
vc.findViewByIdOrRaise("id/no_id/17").touch()
vc.sleep(_s)
vc.dump(window=-1)
# v = vc.findViewWithTextOrRaise("Chat Backups").touch()
vc.findViewByIdOrRaise("id/no_id/23").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.findViewWithTextOrRaise(u'Create backup').touch()
# vc.findViewByIdOrRaise("id/no_id/12")
print("WAITING FOR BACKUP TO FINISH")
vc.sleep(20)
vc.dump(window=-1)

vc.sleep(_s)
device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s)
device.shell('input keyevent KEYCODE_HOME')