import re
import sys
import time
import os
import random
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

def getViewList(view, viewList):
    if view.getId() == 'org.thoughtcrime.securesms:id/conversation_item_body':
        viewList.append(view)
    for ch in view.children:
        getViewList(ch, viewList)
    return viewList

MAX_WAIT=10
_s = 3

device = sys.argv[1]
target_device = device
contact_device = sys.argv[2]
folder_name = sys.argv[3]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('org.thoughtcrime.securesms/.RoutingActivity')
time.sleep(5)
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)
vc.dump(window=-1)
vc.sleep(_s)

v = vc.findViewById("org.thoughtcrime.securesms:id/conversation_list_item_name")

v.touch()
vc.dump(window=-1)
vc.sleep(_s)

listviews = getViewList(vc.root,[])
for v in listviews:
    if (('pink' in v.getText().lower()) or ('flixonline' in v.getText().lower())):
        (x,y) = v.getXY()
        v.device.drag((x,y), (x,y), 2000, 1)
        vc.sleep(_s)
        vc.dump(window=-1)
        break

device.touch(1010.0, 2663.3333333333335, 0)
vc.sleep(_s)
vc.dump(window=-1)

vc.findViewWithTextOrRaise(u'Delete for me').touch()
vc.sleep(_s)
vc.dump(window=-1)
print("Tried Deleting WhatsApp Pink message. Current time ... ----> "+ str(datetime.now()))

os.system("adb -s "+contact_device+" exec-out screencap -p > "+folder_name+"/screen_on_contact.png")
os.system("adb -s "+target_device+" exec-out screencap -p > "+folder_name+"/screen_on_target.png")

device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s)
device.shell('input keyevent KEYCODE_HOME')