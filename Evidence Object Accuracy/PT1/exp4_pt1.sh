#! /bin/bash 
# ./exp4_pt1.sh 2>&1 | tee ./results/exp4_execution_run-`date +"%FT%T"`.txt && ./rsync.sh

app_list=("com.whatsapp" "org.telegram.messenger.web" "org.thoughtcrime.securesms")
drivers_folder="./drivers"
NOISE_MESSAGES=20
EVIDENCE_WAIT=900

to_append=${1:-''}

results_folder_name="./results/results_${to_append}_NOISE_MSGS_LOOP_${NOISE_MESSAGES}_EVIDENCE_WAIT_${EVIDENCE_WAIT}-`date +"%FT%T"`"
echo $results_folder_name

mkdir -p $results_folder_name

echo "[**] Starting EXPERIMENT ... ----> `date +'%d/%m/%Y %T'`"
for app in ${app_list[@]}; do
    echo "[**] Starting App $app ... ----> `date +'%d/%m/%Y %T'`"
    for f in $drivers_folder/*; do
        echo "[**] Starting Driver `basename $f` ... ----> `date +'%d/%m/%Y %T'`"
        folder_name="${results_folder_name}/${app}/`basename $f`"
        data_folder_name="${folder_name}/evidence"
        mkdir -p $folder_name

        file=`find $f -name "$app"_driver.js`

        if [[ ! -z "$file" ]]; then 
            
            nohup ~/Android/Sdk/tools/emulator -avd exp4_target_jennifer -no-snapshot-save > /dev/null 2>&1 &
            sleep 2
            nohup ~/Android/Sdk/tools/emulator -avd exp4_contact_jennifer -no-snapshot-save > /dev/null 2>&1 &
            sleep 10

            ADBDEVICES=$(adb devices)
            contact_emu=""
            malicious_emu=""

            while read line; do 
                vars=( $line )

                if [[ "${vars[1]}" = "device" ]]; then
                    port=`cut -d "-" -f2 <<< "${vars[0]}"`
                    # the below doesn't work on UNI machine for some reason...
                    # avd_name=$(adb -s ${vars[0]} emu avd name | head -n 1)
                    telnet_answer=`(sleep 0.5; echo 'avd name') | telnet 127.0.0.1 $port`
                    avd_name=`echo $telnet_answer | sed -e 's/.*OK \(.*\) OK.*/\1/'`

                    if [[ "$avd_name" == *"target"* ]]; then
                        malicious_emu="${vars[0]}"
                    else
                        contact_emu="${vars[0]}"
                    fi
                fi
            done <<< "$ADBDEVICES"
            echo "MALICIOUS_EMU >> "$malicious_emu
            echo "CONTACT_EMU >> "$contact_emu
            
            echo "[**] Starting app $app ..."
            adb -s $malicious_emu shell monkey -p $app -c android.intent.category.LAUNCHER 1

            if [[ $app = "com.whatsapp" ]]; then
                app_name="WhatsApp"
            elif [[ $app = "org.telegram.messenger.web" ]]; then
                app_name="Telegram"
            elif [[ $app = "org.thoughtcrime.securesms" ]]; then
                app_name="Signal"
            fi
            app_name_lower=`echo "$app_name" | awk '{print tolower($0)}'`

            adb -s $malicious_emu shell mkdir /sdcard/jitmflogs/
            adb -s $malicious_emu shell mkdir /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/

            sleep 10

            echo "[**] Restarting wifi"
            adb -s $malicious_emu shell svc wifi disable
            adb -s $contact_emu shell svc wifi disable
            sleep 3
            adb -s $malicious_emu shell svc wifi enable
            adb -s $contact_emu shell svc wifi enable
            sleep 5

            adb -s $malicious_emu root
            adb -s $contact_emu root
            sleep 3
            echo "[**] Pushing FRIDA Server ... "
            ./push_and_start_frida.sh $malicious_emu > /dev/null 2>&1 &	
            sleep 5
            adb -s $malicious_emu  shell dumpsys gfxinfo ${app} reset 
            echo "[**] Setting FRIDA Script ... >>-- frida -U ${app_name} -l $file > /dev/null 2>&1 &  --<<"
            # echo "[**] Setting FRIDA Script ... >>-- frida -U -f ${app} -l $file --no-pause > /dev/null 2>&1 &  --<<"
            frida -U ${app_name} -l $file > /dev/null 2>&1 &
            sleep 5

            echo "[**] Installing WhatsApp Pink ... "
            ./whatsapp_pink_install.sh $malicious_emu
            sleep 5

            adb -s $malicious_emu logcat -c
            adb -s $malicious_emu shell rm -rf /sdcard/$app/*
            adb -s $malicious_emu shell rm -rf /sdcard/jitmflogs/*

            echo "[**] Starting MESSAGE RUN ... ----> `date +'%d/%m/%Y %T'`"

            adb -s $malicious_emu shell rm -rf /sdcard/$app_name/*
            adb -s $malicious_emu shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/*

            echo "[**] Sending $app_name_lower $NOISE_MESSAGES messages , Deleting Whatsapp Pink message and sending $app_name_lower $NOISE_MESSAGES messages... "
                
            python3 ./scripts_for_generating_traffic/sending_${app_name_lower}_msg_to_phone.py "$contact_emu" $NOISE_MESSAGES 'target_phone' 1 &
            sleep 30 
            python3 ./scripts_for_generating_traffic/deleting_whatsapp_pink_msg_${app_name_lower}.py "$malicious_emu" "$contact_emu" "$folder_name"
            sleep 10

            adb -s $malicious_emu uninstall com.pik.pink
            sleep 5
            python3 ./scripts_for_generating_traffic/sending_${app_name_lower}_msg_to_phone.py "$malicious_emu" $NOISE_MESSAGES 'contact_phone' 21 &


            echo "[**] Finished MESSAGE RUN. Waiting 30 mins to grab evidence ... ----> `date +'%d/%m/%Y %T'`"
            sleep $EVIDENCE_WAIT #grab evidence after 30 mins

            mkdir -p $data_folder_name/sdcard
            mkdir -p $data_folder_name/data
            
            echo "[**] Grabbing evidence ... ----> `date +'%d/%m/%Y %T'`"            
            adb -s $malicious_emu shell dumpsys gfxinfo ${app} | grep Janky >> $folder_name/janky.txt 
            adb -s $malicious_emu logcat -d  -b crash -v time >> $folder_name/logcatcrashes.txt
            adb -s $malicious_emu logcat -d -v time >> $folder_name/logcat.txt
         
            if [[ $app = "org.thoughtcrime.securesms" ]]; then
                echo "[**] Removing any previous backups ..."
                adb -s $malicious_emu shell rm -rf /sdcard/Download/*
                sleep 5
                echo "[**] Creating backup ..."
                python3 ./scripts_for_generating_traffic/${app_name_lower}_backup.py "$malicious_emu" 
                sleep 10
                echo "[**] Pulling backup ..."
                adb -s $malicious_emu shell find "/sdcard/Download" -iname "*.backup" | tr -d '\015' | while read line; do adb -s $malicious_emu pull -a "$line" $data_folder_name/data; done;

                echo "[**] Getting sdcard logs ..."
                adb -s $malicious_emu pull -a /sdcard/Android/data/$app $data_folder_name/sdcard
                echo "[**] Getting data logs..."
                adb -s $malicious_emu pull -a /data/data/org.thoughtcrime.securesms $data_folder_name/data
              
                echo "[**] Getting jitmf logs..."
                adb -s $malicious_emu pull -a /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs $data_folder_name/sdcard
            else
                echo "[**] Getting sdcard logs ..."
                adb -s $malicious_emu pull -a /sdcard/Android/data/$app $data_folder_name/sdcard
                echo "[**] Getting data logs..."
                adb -s $malicious_emu pull -a /data/data/$app $data_folder_name/data
                              
                echo "[**] Getting jitmf logs..."
                adb -s $malicious_emu pull -a /sdcard/jitmflogs $data_folder_name/sdcard
            fi

            adb -s $malicious_emu pull /data/anr/ $folder_name/ 
            echo "[**] Getting app usage..."
            adb -s $malicious_emu pull -a /data/system/usagestats/usage-history.xml $folder_name/
            echo "[**] Finished grabbing evidence ... ----> `date +'%d/%m/%Y %T'`"
            
            echo "[**] Resetting ..."
            adb -s $malicious_emu shell rm -rf /sdcard/jitmflogs/*
            adb -s $malicious_emu shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/*
            adb -s $malicious_emu shell rm -rf /data/anr/*
            adb -s $malicious_emu shell am force-stop ${app}
            echo "Killing emulators"
            adb -s $malicious_emu emu kill
            adb -s $contact_emu emu kill
            sleep 20
        fi
        echo "[**] Finished Driver `basename $f` ... ----> `date +'%d/%m/%Y %T'`"
    done 
    echo "[**] Finished App $app ... ----> `date +'%d/%m/%Y %T'`"   
done
echo "[**] Finished EXPERIMENT ... ----> `date +'%d/%m/%Y %T'`"