# libraries.json (library details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/getlibraries
# json[1-12].json (most downloaded 550 Google Playstore app details) obtained using AppBrain api call: https://api.appbrain.com/v2/info/browse offset 0 limit 50 (till offset 500)

import json
import os
import matplotlib.pyplot as plt
import numpy as np

directory = '/mnt/2TBdrive/test/exp4/PT4_COVERAGE/'
 
files = [os.path.join(directory,f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory,f)) if f!="libraries.json"]
og_data={}
libraries_data={}
communication_apps=0
total_app=550

for filename in files:
    with open(filename) as json_file:
        data = json.load(json_file)
        for app in data['apps']:
            if app["marketCategory"] == "COMMUNICATION":
                communication_apps+=1
            if 'libraries' in app:
                og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": app['libraries']}
            else:
                og_data[app['package']]={"name": app['name'], "marketCategory":app['marketCategory'], "estimatedDownloads":app['estimatedDownloads'], "libraries": []}

with open(os.path.join(directory,"libraries.json")) as lib_json_file:
    data_2 = json.load(lib_json_file)
    for lib in data_2['libraries']:
        libraries_data[lib['id']]={"name": lib['name'], "tags":lib['tags'], "count_all":0,"count_messaging":0}

print(f"category|app|{'|'.join([libraries_data[l]['name'] for l in libraries_data if 'database' in libraries_data[l]['tags'] or 'network' in libraries_data[l]['tags']])}")
for app in og_data:
    string_to_print=og_data[app]['marketCategory']+"|"+og_data[app]['name']
    for l in libraries_data:
        if "database" in libraries_data[l]["tags"] or "network" in libraries_data[l]["tags"]:
            if l in og_data[app]["libraries"]:
                string_to_print+="|1"
            else:
                string_to_print+="|0"
    print(string_to_print)


for l in libraries_data:
    count=libraries_data[l]["count_all"]
    count_messaging=libraries_data[l]["count_messaging"]
    for app in og_data:
        if l in og_data[app]["libraries"]:
            count+=1
            if og_data[app]["marketCategory"] =="COMMUNICATION":
                count_messaging+=1
    libraries_data[l]["count_all"]=count
    libraries_data[l]["count_messaging"]=count_messaging

graph_data_all={'database':[],'network':[]}
lib_mapping_network=[]
lib_mapping_database=[]
graph_data_messaging={'database':[],'network':[]}

# print("library,lib_typ,% of total apps,% of messaging apps")
for l in libraries_data:
    if "database" in libraries_data[l]["tags"]:
        perc_all=(libraries_data[l]["count_all"]/total_app)*100
        perc_mess=(libraries_data[l]["count_messaging"]/communication_apps)*100
        graph_data_all['database'].append(perc_all)
        graph_data_messaging['database'].append(perc_mess)
        lib_mapping_database.append(libraries_data[l]['name'])
        # print(f"{libraries_data[l]['name']},database,{perc_all},{perc_mess}")
    if "network" in libraries_data[l]["tags"]:
        perc_all=(libraries_data[l]["count_all"]/total_app)*100
        perc_mess=(libraries_data[l]["count_messaging"]/communication_apps)*100
        graph_data_all['network'].append(perc_all)
        graph_data_messaging['network'].append(perc_mess)
        lib_mapping_network.append(libraries_data[l]['name'])

        # print(f"{libraries_data[l]['name']},network,{perc_all},{perc_mess}")

# print(graph_data_all)
# print(graph_data_messaging)
# print(lib_mapping_database)
# print(lib_mapping_network)

# labels = libraries_data.keys()

# graph_data={'database': {'Android Architecture Components':92.72727273, 'Firebase':83.63636364, 'Realm':1.090909091, 'OrmLite':2, 'greenDAO':3.818181818, 'DBFlow':0.3636363636, 'ActiveAndroid':0, 'SugarORM':0, 'MongoDB Java Driver':0},'network':{'Retrofit':20.36363636,'Volley':16.72727273,'okHttp':5.818181818,'Grpc':9.272727273,'Apache Http Auth':6,'Android Asynchronous Http Client':1.818181818,'HttpClient for Android':2.909090909,'Apache HttpMime API':4,'AndroidAsync':0.1818181818,'Fast Android Networking':0.1818181818,'dnsjava':2.363636364,'Android Smart Image View':0,'JCraft':0.3636363636,'The Java CIFS Client Library (JCIFS)':0.1818181818,'RoboSpice':0,'HttpClient for Android':0,'Basic HTTP Client':0.1818181818,'Project Kenai - jbosh':0,'GeckoView':0.1818181818,'MongoDB Java Driver':0,'Bump':0}}

# plt.bar(range(len(graph_data)))

# countries = {'NG': [1405, 7392], 'IN': [5862, 9426], 'GB': [11689, 11339], 'ID': [7969, 2987]}

# plt.bar(range(len(graph_data_all)), np.array(list(graph_data_all.values()),dtype=object)[:,0], bottom=0, align='center')
# for i in range(1, 9):
#     plt.bar(range(len(graph_data_all)), np.array(list(graph_data_all.values()),dtype=object)[:,i], bottom=np.array(list(graph_data_all.values()),dtype=object)[:,i-1], align='center')


# plt.xticks(range(len(graph_data_all)), graph_data_all.keys())
# plt.show()

# countries = {'database': [92.72727272727272, 83.63636363636363, 1.090909090909091, 2.0, 3.8181818181818183, 0.36363636363636365, 0.0, 0.0, 0.0]}

# plt.bar(range(len(countries)), np.array(list(countries.values()))[:,0], bottom=0, align='center',label=lib_mapping_database[0])
# for i in range(1, 9):
#     plt.bar(range(len(countries)), np.array(list(countries.values()),dtype=object)[:,i], bottom=np.array(list(countries.values()),dtype=object)[:,i-1], align='center',label=lib_mapping_database[i])
# # plt.bar(range(len(countries)), np.array(list(countries.values()))[:,1], bottom=np.array(list(countries.values()))[:,0], align='center')
# plt.xticks(range(len(countries)), countries.keys())
# plt.legend()
# plt.show()