# top 500 apps from AndroidRank
# ./app_sqlite_search.sh 2>/dev/null

list_of_apps=("com.whatsapp" "com.facebook.orca" "com.android.chrome" "com.snapchat.android" "com.UCMobile.intl" "com.truecaller" "com.viber.voip" "jp.naver.line.android" "com.google.android.gm" "com.skype.raider" "org.telegram.messenger" "com.google.android.apps.tachyon" "com.whatsapp.w4b" "com.opera.mini.native" "com.google.android.apps.messaging" "com.imo.android.imoim" "com.yahoo.mobile.client.android.mail" "com.tencent.mm" "com.facebook.mlite" "com.sec.android.app.sbrowser" "com.uc.browser.en" "org.mozilla.firefox" "com.discord" "com.opera.browser" "com.kakao.talk" "com.transsion.phoenix" "com.jb.gosms" "mobi.mgeek.TunnyBrowser" "kik.android" "app.source.getcontact" "org.thoughtcrime.securesms" "ru.mail.mailapp" "com.zing.zalo" "com.azarlive.android" "com.imo.android.imoimbeta" "com.sec.spp.push" "com.jio.join" "com.turkcell.bip" "com.google.android.ims" "com.enflick.android.TextNow" "com.michatapp.im" "com.psiphon3" "ru.beeline.services" "com.callapp.contacts" "com.google.android.contacts" "im.thebot.messenger" "com.brave.browser" "ru.tele2.mytele2" "com.cloudmosa.puffinFree" "com.drilens.wamr" "com.icq.mobile.client" "com.loudtalks" "com.verizon.messaging.vzmsgs" "com.vladlee.easyblacklist" "com.opera.mini.native.beta" "com.hsv.freeadblockerbrowser" "gogolook.callgogolook2" "com.psiphon3.subscription" "com.mcent.browser" "com.services.movistar.ar" "org.telegram.plus" "com.eyecon.global" "ru.mail" "ar.com.personal" "com.picmax.wemoji" "com.groupme.android" "de.gmx.mobile.android.mail" "com.microsoft.emmx" "com.zong.customercare" "com.kyivstar.mykyivstar" "ru.megafon.mlk" "il.co.smedia.callrecorder.yoni" "com.asus.contacts" "com.jiochat.jiochatapp" "com.enlightment.voicecallrecorder" "ru.yandex.mail" "com.my.mail" "de.web.mobile.android.mail" "com.imo.android.imoimhd" "com.textra" "com.mytel.myid" "stickerwhatsapp.com.stickers" "com.ucturbo" "kz.beeline.odp" "mobi.drupe.app" "com.touchtalent.bobbleapp" "com.dsi.ant.service.socket" "com.glidetalk.glideapp" "me.talkyou.app.im" "com.p1.chompsms" "de.telekom.mail" "com.eci.citizen" "stickermaker.android.stickermaker" "com.dstukalov.walocalstoragestickers" "com.google.android.apps.googlevoice" "com.apusapps.browser" "polis.app.callrecorder" "com.juphoon.justalk" "com.opera.browser.beta" "com.skt.prod.dialer" "co.happybits.marcopolo" "com.movistar.android.mimovistar.es" "com.xplus.messenger" "com.google.android.wearable.app" "it.medieval.blueftp" "org.mozilla.firefox_beta" "com.webascender.callerid" "browser4g.fast.internetwebexplorer" "com.mobile.number.locator.phone.gps.map" "mm.cws.telenor.app" "net.omobio.dialogsc" "com.rebelvox.voxer" "com.syncme.syncmeapp" "com.kaspersky.who_calls" "com.bingo.livetalk" "com.trtf.blue" "com.hb.dialer.free" "com.tempmail" "com.tencent.mobileqq" "com.speaktranslate.tts.speechtotext.voicetyping.translator" "com.speakandtranslate.voicetranslator.alllanguages" "com.tuenti.messenger" "ir.ilmili.telegraph" "com.majedev.superbeam" "com.omesti.myumobile" "com.mobile.truecall.tracker.locator.teccreations" "com.contapps.android" "wastickerapps.stickersforwhatsapp" "com.mrnumber.blocker" "lifeisbetteron.com" "de.eplus.mappecc.client.android.alditalk" "com.vzw.ecid" "com.viettel.mocha.app" "com.dartit.RTcabinet" "com.elgramert.org" "com.skitto" "org.torproject.android" "com.mentisco.freewificonnect" "my.photo.picture.keyboard.keyboard.theme" "com.calea.echo" "org.adblockplus.browser" "com.facebook.talk" "com.mobiles.numberbookdirectory" "com.avast.android.vpn" "com.talkatone.android" "com.bf.browser" "com.ecosia.android" "by.mts.client" "com.droid.caller.id.phone.number.location" "vpn.lavpn.unblock" "com.allinone.callerid" "de.telekom.android.customercenter" "org.torproject.torbrowser" "livemobilelocationtracker.teccreations" "kr.core.technology.wifi.hotspot" "com.simpler.dialer" "com.michatapp.im.lite" "com.movistarmx.mx.app" "com.texty.sms" "com.chrome.dev" "com.catbag.lovemessages" "com.smscolorful.formessenger.messages" "nu.bi.moya" "com.arena.banglalinkmela.app" "com.rebtel.android" "org.vidogram.messenger" "pm.tap.vpn" "com.revesoft.itelmobiledialer.dialer" "es.vodafone.mobile.mivodafone" "com.smsrobot.call.blocker.caller.id.callmaster" "com.att.miatt" "com.numbuster.android" "com.coccoc.trinhduyet" "com.ayoba.ayoba" "me.nextplus.smsfreetext.phonecalls" "com.ringapp" "com.enflick.android.tn2ndLine" "za.co.vodacom.android.app" "com.imo.android.imous" "org.litewhite.callblocker" "com.proWAStickerApps.stickersemojis.WAStickerApps.emojis" "rs.telenor.mymenu" "net.idt.um.android.bossrevapp" "com.af.waradar" "net.daum.android.daum" "com.cloudmagic.mail" "com.google.android.gm.lite" "com.fulldive.mobile" "com.att.callprotect" "com.opera.touch" "com.guibais.whatsauto" "jp.co.yahoo.android.ybrowser" "com.powerasdev.wetel" "com.couchgram.privacycall" "com.simpler.contacts" "com.flexaspect.android.everycallcontrol" "com.clay.redtritium" "net.iGap" "ro.telekom.myaccount" "com.utternik.utternik" "lv.indycall.client" "cl.wom.transformacion.appwommobile" "com.youmail.android.vvm" "com.tmobile.services.nameid" "sms.app.messages.app.message.box.message.me" "com.proWAStickerApps.memesmexicanos.stickersmexico" "com.studiokuma.callfilter" "com.bluesoft.clonappmessenger" "com.tracfone.straighttalk.myaccount" "com.abtalk.freecall" "com.personal.sticker.maker.wastickerapps" "com.mail.mobile.android.mail" "com.matchtech.cafe" "org.mozilla.focus" "com.fsck.k9" "com.stickotext.main" "com.kivra.Kivra" "com.speaktranslate.englishalllanguaguestranslator.ivoicetranslation" "com.robokiller.app" "com.esealed.dalily" "org.whiteglow.antinuisance" "com.isharing.isharing" "ca.bell.selfserve.mybellmobile" "com.aleskovacic.messenger" "com.turkcell.yaani" "reactivephone.msearch" "ru.ok.messages" "de.resolution.yf_android" "com.google.audio.hearing.visualization.accessibility.scribe" "com.textmeinc.textme3" "com.quantum.callerid" "it.pagopa.io.app" "and.p2l" "it.italiaonline.mail" "com.ram.itsl" "com.sungeram.app" "pl.gadugadu" "com.pdanet" "ai.rune.tincan" "com.tsdc.selfcare" "info.indiapost" "com.sec.android.app.sbrowser.beta" "com.esim.numero" "com.brilliant.connect.com.bd" "com.wephoneapp" "com.nexon.nxplay" "com.proWAStickerApps.catsmemes.memesdegatos" "org.mistergroup.shouldianswer" "cz.seznam.sbrowser" "com.finazzi.distquake" "sk.forbis.messenger" "v.d.d.answercall" "com.kiwibrowser.browser" "xyz.klinker.messenger" "com.jaredco.calleridannounce" "com.monogramm.sr" "com.smsBlocker" "com.giffgaffmobile.controller" "com.banana.studio.sms" "park.outlook.sign.in.client" "com.smartwatch.bluetooth.sync.notifications" "com.telos.app.im" "com.blizzard.messenger" "com.tohsoft.mail.email.emailclient" "com.nam.fbwrapper" "call.free.international.phone.call" "com.whitepages.search" "com.zoiper.android.app" "com.aironlabs.whatwebplus" "com.orange.bluu.jazztel" "caller.id.phone.number.block" "com.mensajes.borrados.deleted.messages" "com.turkcaller.numarasorgulama" "www.extremedroid.com.a4gltetoolkit" "nl.oberon.tmobile.my" "co.kitetech.messenger" "ru.vsms" "com.lebara.wallet" "org.easyweb.browser" "com.github.shadowsocks" "jp.co.yahoo.android.ymail" "com.juvomobileinc.tigoshop.sv" "jp.co.rakuten.mobile.rcs" "bolo.codeplay.com.bolo" "com.meihillman.callrecorder" "com.OnSoft.android.BluetoothChat" "com.jio.web" "com.softinit.iquitos.mainapp" "ca.virginmobile.myaccount.virginmobile" "finarea.MobileVoip" "tkstudio.autoresponderforwa" "net.daum.android.mail" "com.androminigsm.fscifree" "com.telenordigital.contacts" "com.ciamedia.caller.id" "walkie.talkie.among.us.friends" "com.foxfi" "com.connectivityapps.hotmail" "explore.web.browser" "free.call.international.phone.wifi.calling" "com.popa.video.status.download" "com.opera.gx" "com.gemtechnologies.gem4me" "com.memeandsticker.personal" "com.validio.kontaktkarte.dialer" "com.dish.mydish" "com.sonelli.juicessh" "de.eue.mobile.android.mail" "com.waveapplication" "Uxpp.UC" "com.mglab.scm" "pl.wp.wppoczta" "kz.post" "com.firebreak.whatsclonemessenger" "org.sibgram.tmessenger" "com.fastsigninemail.securemail.bestemail" "com.myvodafone.android" "com.imessage.text.ios" "com.jb.gosms.pctheme.newmessengerversion2017" "de.blinkt.openvpn" "com.vox.mosippro" "com.enterkomug.justlo" "com.whatsphone.messenger.im" "com.promessage.message" "free.call.international.phone.calling" "fast.secure.light.browser" "me.number.app.im" "wastickerapps.funnyemoji.stickers" "com.beint.zangi" "quick.browser.secure" "bg.telenor.mytelenor" "com.dolphin.browser.engine" "com.safeum.android" "org.jitsi.meet" "com.comcast.hsf" "privacy.explorer.fast.safe.browser" "com.handcent.app.nextsms" "qa.ooredoo.android" "com.google.android.accessibility.soundamplifier" "sk.o2.mojeo2" "com.kcell.myactiv" "org.prowl.torquefree" "com.orange.vvm" "com.hushed.release" "com.truemobile.caller.location.callerid" "com.rediff.mail.and" "com.killermobile.totalrecall" "com.adhoclabs.burner" "com.vtg.app.myunitel" "com.microsoft.android.smsorganizer" "br.com.claro.flex" "com.dartushinc.callername" "com.osmino.wifispot" "ch.protonmail.android" "com.tracfone.tracfone.myaccount" "org.connectbot" "com.yoigo.miyoigo" "com.att.miunefonmx" "info.myapp.allemailaccess" "com.WAStickerApps.love" "sms.mms.messages.text.free" "pl.interia.poczta_next" "pl.wp.pocztao2" "com.nymgo.android" "net.whatscall.freecall" "jp.co.rakuten.broadband.sim" "com.ktix007.talk" "com.plantpurple.wastickerapps.emojidom.free" "com.telefonica.movistar" "com.mysms.android.sms" "com.orange.orangeetmoi" "com.opplysning180.no" "com.iMe.android" "com.sfr.android.sfrmail" "hr.infinum.mojvip" "me.freecall.callglobal" "com.tunetalk.jmango.tunetalkimsi" "the.best.gram" "com.RSen.Widget.Hangouts" "spanish.stickers.wastickerapps" "com.mobo.filtertel" "com.orange.phone" "com.directoriotigo.hwm" "com.mobiu.browser" "com.ufoneselfcare" "me.freecall.callindia" "com.beint.pinngle" "sa.jawwy.app2" "com.wastickerapps.whatsapp.stickers" "com.tabletmessenger" "com.mail.hotmail.outlook.email" "customstickermaker.whatsappstickers.personalstickersforwhatsapp" "org.randomchattalkstrangerieltsspeaking.app" "com.easycodes.memesbr" "com.browser.tssomas" "com.concentriclivers.mms.com.android.mms" "com.cisco.im" "com.omantel" "flyjam.InstantTraductor" "nu.tommie.inbrowser" "in.gov.eci.garuda" "pl.onet.mail" "com.vivaldi.browser" "callerid.truecaller.trackingnumber.phonenumbertracker.block" "com.androhelm.antivirus.free2" "com.ace.freevpn" "com.turkcell.sesplus" "jp.xrea.poca.antennapict" "co.golder.me" "com.maildroid" "com.rimnicapgrt.org" "com.ezetop.world" "fr.freemobile.android.vvm" "com.jb.gosms.theme.sweet" "com.guca.whatssemcontato" "bg.vivacom.mobile" "com.wstick.hk" "com.intersvyaz.lk" "com.ertelecom.agent" "com.cuiet.blockCalls" "com.stickerswhats.wastickerapps" "com.magic.wastickerapps.whatsapp.stickers" "com.ekaisar.android.eb" "com.cake.browser" "com.frontier.selfserve" "com.easyway.freewifi" "com.noxgroup.app.browser" "jp.co.yahoo.android.ymobile.mail" "org.aka.messenger" "com.rms.live.mobilelocation.calculatedistance.trackmylocation.gpscoordinates.findplaces.getlocation" "de.md.meinmd" "com.mizmowireless.vvm" "com.crazystudio.mms6" "dk.tacit.android.foldersync.lite" "jp.co.rakuten.denwa" "com.snrblabs.grooveip" "com.mixlr.android" "ru.tinkoff.mvno" "com.touwolf.cllappmovil" "com.whatdir.stickers" "kha.prog.mikrotik" "com.xbrowser.play" "com.whatweb.clone" "jp.wifishare.townwifi" "truecaller.caller.callerid.name.phone.dialer" "com.samsung.accessory" "com.openvacs.android.oto" "de.avm.android.wlanapp" "com.kaweapp.webexplorer" "com.flowerstickers.wastickerapps" "com.sh.smart.caller" "com.talpa.hibrowser" "com.telepacket.TpSmart" "com.contapps.android.messaging" "com.recommended.videocall" "com.messengerfree.forchat" "com.naver.whale" "com.stickers.tamilandastickers" "com.nhn.android.mail" "in.number.locator" "com.WAStickerApps.lovestickersticker" "com.androidworks.videocalling" "org.adblockplus.adblockplussbrowser" "de.telekom.mds.mbp" "com.zinn.currentmobiletrackerlocation" "com.att.mobile.android.vvm" "com.kddi.cs.app001" "com.bengali.voicetyping.keyboard.speechtotext" "masih.vahida.serverwalkietalkie" "live.hala" "u.see.browser.for.uc.browser" "com.rsa.securidapp" "sparking.mobile.location.lions.llc" "mobi.skred.app" "com.tipsmessenger.whatsmassenger2019" "maratische.android.phonesmscodes" "u.browser.for.lite.uc.browser" "com.antiporn.pornoblock.safebrowser" "com.kddi.android.cmail" "com.uscc.ecid" "com.glowapps.memestickerswhatsapp" "com.citadini.dev.memescomfrasesbr" "com.loveshayarisn" "com.woow.talk" "com.nttdocomo.android.msg" "com.htc.vte" "com.vt.love.photo.frame" "com.jio.messages" "com.vinsmart.messaging" "com.bell.ptt" "com.cloudmosa.puffinTV" "com.tvwebbrowser.v22")
APK_DOWNLOAD_FOLDER="/mnt/2TBdrive/gitlab/experiment_4/PT4/apps/"
APK_DOWNLOAD_DECOMPILED_FOLDER="/mnt/2TBdrive/gitlab/experiment_4/PT4/apps/decompiled/"

decompile_apks_downloaded(){
    for f in $APK_DOWNLOAD_FOLDER/*.apk
    do
        let "r=r+1"
        echo "[**] >> APK $r of $apk_cnt "
        now="$(date +'%d/%m/%Y %T')"

        if [ ! -d $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/} ]; then
            echo "[**] Starting Decompilation of ${f##*/} at: $now"
            ../PT3/apktool/apktool.sh 2.5 d $f -o $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
            if [ "$?" -ne 0 ]; then
                # apktool command failed;
                rm -rf $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                ./apktool/apktool.sh 2.6.1 d $f -o $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                echo $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                if [ "$?" -ne 0 ]; then
                # apktool command failed;
                    rm -rf $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                    ./apktool/apktool.sh 2.6.1 d -r $f -o $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                    echo $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                fi
            fi
            now="$(date +'%d/%m/%Y %T')"
            echo "[**] Finished Decompilation of ${f##*/} at: $now"
        else
            echo "[**] APK already decompiled"
        fi
    done
}

search(){
    echo "app,FOUND | NOT FOUND shared object in libs folder, FOUND | NOT FOUND SQLITE function calls in APP smali code, FOUND | NOT FOUND SQLITE function calls in APP smali code (unfiltered) , simple sqlite , io/realm , ormlite , mongodb, firebase,firebasedb"
    for app in ${list_of_apps[@]}; do
        folder_name="/home/uom-lab-locard/jennifer-exp4/PT2/results/apks_downloaded_apkpure/decompiled/$app.apk"

        if [ -d $folder_name ]; then
            
            if [ -d $folder_name/lib/ ]; then
                if grep -riq sqlite $folder_name/lib/ ; then 
                    ans_lib="FOUND"
                else
                    ans_lib="NOT_FOUND" 
                fi
            else
                ans_lib="NOT_FOUND" 
            fi

            subfolder_name_with_extension=${folder_name##*/}
            # echo $subfolder_name_with_extension
            subfolder_name=${subfolder_name_with_extension%.*}
            subfolder_to_search=`echo  "$subfolder_name" |cut -d '_' -f1  | tr . /`
            # echo $subfolder_to_search
            
            
            if grep -PRiq --include *.smali "Landroid/database/sqlite.*;->" $folder_name/smali*/$subfolder_to_search ; then
                ans_andsqlite_pkg="FOUND"
            else
                ans_andsqlite_pkg="NOT_FOUND" 
            fi


            if grep -PRiq --include *.smali "Landroid/database/sqlite.*;->" $folder_name/smali*/ ; then
                ans_andsqlite_pkg_nofilter="FOUND"
            else
                ans_andsqlite_pkg_nofilter="NOT FOUND"
            fi

            if grep -PRiq --include *.smali "sqlite" $folder_name/smali*/ ; then
                ans_sqlite="FOUND"
            else
                ans_sqlite="NOT FOUND"
            fi

            if grep -PRiq --include *.smali "io/realm" $folder_name/smali*/; then
    #            grep -ri --include *.smali "greenrobot" $folder_name/smali*/
                ans_realm="FOUND"
            else
                ans_realm="NOT_FOUND" 
            fi
            
            
            if grep -PRiq --include *.smali "ormlite" $folder_name/smali*/; then
    #            grep -ri --include *.smali "greenrobot" $folder_name/smali*/
                ans_ormlite="FOUND"
            else
                ans_ormlite="NOT_FOUND" 
            fi

            if grep -PRiq --include *.smali "mongodb" $folder_name/smali*/; then
    #            grep -ri --include *.smali "greenrobot" $folder_name/smali*/
                ans_mongodb="FOUND"
            else
                ans_mongodb="NOT_FOUND" 
            fi
            
            if grep -PRiq --include *.smali "firebase" $folder_name/smali*/; then
    #            grep -ri --include *.smali "greenrobot" $folder_name/smali*/
                ans_firebase="FOUND"
            else
                ans_firebase="NOT_FOUND" 
            fi
            
            if grep -PRiq --include *.smali "firebasedatabase" $folder_name/smali*/; then
    #            grep -ri --include *.smali "greenrobot" $folder_name/smali*/
                ans_firebasedb="FOUND"
            else
                ans_firebasedb="NOT_FOUND" 
            fi


            echo $app,${ans_lib},${ans_andsqlite_pkg},${ans_andsqlite_pkg_nofilter},${ans_sqlite},${ans_realm},${ans_ormlite},${ans_mongodb},${ans_firebase},${ans_firebasedb}

            
            # grep -Fri sqlite $f/lib/ 
            # grep -Fri sqlite $f/lib/ | cut -d '/' -f1 | uniq
            # echo "`basename $f`|${app_date[`basename $f`]}"
        fi

    done
}

# decompile_apks_downloaded
search