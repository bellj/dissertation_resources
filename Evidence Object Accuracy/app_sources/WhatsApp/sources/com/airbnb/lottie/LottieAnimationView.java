package com.airbnb.lottie;

import X.AbstractC11510gP;
import X.AbstractC12000hD;
import X.AbstractC12010hE;
import X.AbstractC12810iX;
import X.AnonymousClass028;
import X.AnonymousClass03X;
import X.AnonymousClass09T;
import X.AnonymousClass09u;
import X.AnonymousClass0AA;
import X.AnonymousClass0B0;
import X.AnonymousClass0HG;
import X.AnonymousClass0J3;
import X.AnonymousClass0K0;
import X.AnonymousClass0K1;
import X.AnonymousClass0MI;
import X.AnonymousClass0SF;
import X.AnonymousClass0UV;
import X.C015007d;
import X.C04340Li;
import X.C04840Ng;
import X.C04980Nu;
import X.C05540Py;
import X.C06000Ru;
import X.C06120Sg;
import X.C06430To;
import X.C06550Ub;
import X.C07830a9;
import X.C07850aB;
import X.C07900aG;
import X.C07910aH;
import X.C07920aI;
import X.CallableC10480eh;
import X.CallableC10490ei;
import X.CallableC10500ej;
import X.CallableC10510ek;
import X.CallableC10520el;
import X.CallableC10550eo;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Choreographer;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes.dex */
public class LottieAnimationView extends AnonymousClass03X {
    public static final AbstractC12010hE A0J = new C07900aG();
    public int A00;
    public int A01 = 0;
    public int A02 = 0;
    public C05540Py A03;
    public AbstractC12010hE A04;
    public C06120Sg A05;
    public AnonymousClass0J3 A06 = AnonymousClass0J3.AUTOMATIC;
    public String A07;
    public boolean A08 = false;
    public boolean A09 = true;
    public boolean A0A = false;
    public boolean A0B;
    public boolean A0C = false;
    public boolean A0D = false;
    public boolean A0E = false;
    public final AnonymousClass0AA A0F = new AnonymousClass0AA();
    public final AbstractC12010hE A0G = new C07910aH(this);
    public final AbstractC12010hE A0H = new C07920aI(this);
    public final Set A0I = new HashSet();

    public LottieAnimationView(Context context) {
        super(context, null);
        A04(null, R.attr.lottieAnimationViewStyle);
    }

    public LottieAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A04(attributeSet, R.attr.lottieAnimationViewStyle);
    }

    public LottieAnimationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A04(attributeSet, i);
    }

    public void A00() {
        this.A08 = false;
        this.A0D = false;
        this.A0E = false;
        this.A0C = false;
        AnonymousClass0AA r1 = this.A0F;
        r1.A0L.clear();
        AnonymousClass09T r12 = r1.A0K;
        Choreographer.getInstance().removeFrameCallback(r12);
        r12.A07 = false;
        A03();
    }

    public void A01() {
        if (isShown()) {
            this.A0F.A01();
            A03();
            return;
        }
        this.A0C = true;
    }

    public final void A02() {
        C06120Sg r2 = this.A05;
        if (r2 != null) {
            AbstractC12010hE r1 = this.A0G;
            synchronized (r2) {
                r2.A02.remove(r1);
            }
            C06120Sg r22 = this.A05;
            AbstractC12010hE r12 = this.A0H;
            synchronized (r22) {
                r22.A01.remove(r12);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0022, code lost:
        if (android.os.Build.VERSION.SDK_INT < 28) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0028, code lost:
        if (r2.A03 > 4) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0037, code lost:
        if (r1 == 25) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
            r4 = this;
            X.0J3 r0 = r4.A06
            int r0 = r0.ordinal()
            r3 = 2
            switch(r0) {
                case 0: goto L_0x0016;
                case 1: goto L_0x000b;
                default: goto L_0x000a;
            }
        L_0x000a:
            r3 = 1
        L_0x000b:
            int r0 = r4.getLayerType()
            if (r3 == r0) goto L_0x0015
            r0 = 0
            r4.setLayerType(r3, r0)
        L_0x0015:
            return
        L_0x0016:
            X.0Py r2 = r4.A03
            if (r2 == 0) goto L_0x002b
            boolean r0 = r2.A0C
            if (r0 == 0) goto L_0x0025
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 28
            if (r1 >= r0) goto L_0x0025
            goto L_0x000a
        L_0x0025:
            int r1 = r2.A03
            r0 = 4
            if (r1 <= r0) goto L_0x002b
            goto L_0x000a
        L_0x002b:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x000a
            r0 = 24
            if (r1 == r0) goto L_0x000a
            r0 = 25
            if (r1 != r0) goto L_0x000b
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airbnb.lottie.LottieAnimationView.A03():void");
    }

    public final void A04(AttributeSet attributeSet, int i) {
        String string;
        boolean z = false;
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C04340Li.A00, i, 0);
        this.A09 = obtainStyledAttributes.getBoolean(1, true);
        boolean hasValue = obtainStyledAttributes.hasValue(10);
        boolean hasValue2 = obtainStyledAttributes.hasValue(5);
        boolean hasValue3 = obtainStyledAttributes.hasValue(16);
        if (hasValue) {
            if (!hasValue2) {
                int resourceId = obtainStyledAttributes.getResourceId(10, 0);
                if (resourceId != 0) {
                    setAnimation(resourceId);
                }
            } else {
                throw new IllegalArgumentException("lottie_rawRes and lottie_fileName cannot be used at the same time. Please use only one at once.");
            }
        } else if (hasValue2) {
            String string2 = obtainStyledAttributes.getString(5);
            if (string2 != null) {
                setAnimation(string2);
            }
        } else if (hasValue3 && (string = obtainStyledAttributes.getString(16)) != null) {
            setAnimationFromUrl(string);
        }
        this.A02 = obtainStyledAttributes.getResourceId(4, 0);
        if (obtainStyledAttributes.getBoolean(0, false)) {
            this.A0D = true;
            this.A08 = true;
        }
        if (obtainStyledAttributes.getBoolean(8, false)) {
            this.A0F.A0K.setRepeatCount(-1);
        }
        if (obtainStyledAttributes.hasValue(13)) {
            setRepeatMode(obtainStyledAttributes.getInt(13, 1));
        }
        if (obtainStyledAttributes.hasValue(12)) {
            setRepeatCount(obtainStyledAttributes.getInt(12, -1));
        }
        if (obtainStyledAttributes.hasValue(15)) {
            setSpeed(obtainStyledAttributes.getFloat(15, 1.0f));
        }
        setImageAssetsFolder(obtainStyledAttributes.getString(7));
        setProgress(obtainStyledAttributes.getFloat(9, 0.0f));
        boolean z2 = obtainStyledAttributes.getBoolean(3, false);
        AnonymousClass0AA r4 = this.A0F;
        r4.A0E(z2);
        if (obtainStyledAttributes.hasValue(2)) {
            r4.A0A(new C06430To("**"), new AnonymousClass0SF(new AnonymousClass09u(C015007d.A00(getContext(), obtainStyledAttributes.getResourceId(2, -1)).getDefaultColor())), AbstractC12810iX.A00);
        }
        if (obtainStyledAttributes.hasValue(14)) {
            r4.A00 = obtainStyledAttributes.getFloat(14, 1.0f);
        }
        if (obtainStyledAttributes.hasValue(11)) {
            int i2 = obtainStyledAttributes.getInt(11, 0);
            if (i2 >= AnonymousClass0J3.values().length) {
                i2 = 0;
            }
            setRenderMode(AnonymousClass0J3.values()[i2]);
        }
        setIgnoreDisabledSystemAnimations(obtainStyledAttributes.getBoolean(6, false));
        obtainStyledAttributes.recycle();
        if (AnonymousClass0UV.A01(getContext()) != 0.0f) {
            z = true;
        }
        r4.A0H = Boolean.valueOf(z).booleanValue();
        A03();
        this.A0B = true;
    }

    @Override // android.view.View
    public void buildDrawingCache(boolean z) {
        this.A01++;
        super.buildDrawingCache(z);
        if (this.A01 == 1 && getWidth() > 0 && getHeight() > 0 && getLayerType() == 1 && getDrawingCache(z) == null) {
            setRenderMode(AnonymousClass0J3.HARDWARE);
        }
        this.A01--;
        AnonymousClass0MI.A00();
    }

    public C05540Py getComposition() {
        return this.A03;
    }

    public long getDuration() {
        C05540Py r2 = this.A03;
        if (r2 != null) {
            return (long) ((float) ((long) (((r2.A00 - r2.A02) / r2.A01) * 1000.0f)));
        }
        return 0;
    }

    public int getFrame() {
        return (int) this.A0F.A0K.A00;
    }

    public String getImageAssetsFolder() {
        return this.A0F.A09;
    }

    public float getMaxFrame() {
        return this.A0F.A0K.A00();
    }

    public float getMinFrame() {
        return this.A0F.A0K.A01();
    }

    public C04840Ng getPerformanceTracker() {
        C05540Py r0 = this.A0F.A04;
        if (r0 != null) {
            return r0.A0D;
        }
        return null;
    }

    public float getProgress() {
        AnonymousClass09T r1 = this.A0F.A0K;
        C05540Py r0 = r1.A06;
        if (r0 == null) {
            return 0.0f;
        }
        float f = r1.A00;
        float f2 = r0.A02;
        return (f - f2) / (r0.A00 - f2);
    }

    public int getRepeatCount() {
        return this.A0F.A0K.getRepeatCount();
    }

    public int getRepeatMode() {
        return this.A0F.A0K.getRepeatMode();
    }

    public float getScale() {
        return this.A0F.A00;
    }

    public float getSpeed() {
        return this.A0F.A0K.A03;
    }

    @Override // android.widget.ImageView, android.graphics.drawable.Drawable.Callback, android.view.View
    public void invalidateDrawable(Drawable drawable) {
        Drawable drawable2 = getDrawable();
        AnonymousClass0AA r0 = this.A0F;
        if (drawable2 == r0) {
            super.invalidateDrawable(r0);
        } else {
            super.invalidateDrawable(drawable);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode() && (this.A08 || this.A0D)) {
            A01();
            this.A08 = false;
            this.A0D = false;
        }
        if (Build.VERSION.SDK_INT < 23) {
            onVisibilityChanged(this, getVisibility());
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        AnonymousClass0AA r2 = this.A0F;
        AnonymousClass09T r1 = r2.A0K;
        if (r1 != null && r1.A07) {
            this.A0D = false;
            this.A0E = false;
            this.A0C = false;
            r2.A0L.clear();
            r1.cancel();
            A03();
            this.A0D = true;
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof AnonymousClass0B0)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0B0 r3 = (AnonymousClass0B0) parcelable;
        super.onRestoreInstanceState(r3.getSuperState());
        String str = r3.A04;
        this.A07 = str;
        if (!TextUtils.isEmpty(str)) {
            setAnimation(this.A07);
        }
        int i = r3.A01;
        this.A00 = i;
        if (i != 0) {
            setAnimation(i);
        }
        setProgress(r3.A00);
        if (r3.A06) {
            A01();
        }
        this.A0F.A09 = r3.A05;
        setRepeatMode(r3.A03);
        setRepeatCount(r3.A02);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        float f;
        boolean z;
        AnonymousClass0B0 r5 = new AnonymousClass0B0(super.onSaveInstanceState());
        r5.A04 = this.A07;
        r5.A01 = this.A00;
        AnonymousClass0AA r4 = this.A0F;
        AnonymousClass09T r3 = r4.A0K;
        C05540Py r0 = r3.A06;
        if (r0 == null) {
            f = 0.0f;
        } else {
            float f2 = r3.A00;
            float f3 = r0.A02;
            f = (f2 - f3) / (r0.A00 - f3);
        }
        r5.A00 = f;
        if (r3.A07 || (!AnonymousClass028.A0q(this) && this.A0D)) {
            z = true;
        } else {
            z = false;
        }
        r5.A06 = z;
        r5.A05 = r4.A09;
        r5.A03 = r3.getRepeatMode();
        r5.A02 = r3.getRepeatCount();
        return r5;
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        if (!this.A0B) {
            return;
        }
        if (isShown()) {
            if (this.A0E) {
                if (isShown()) {
                    this.A0F.A02();
                    A03();
                } else {
                    this.A0C = false;
                    this.A0E = true;
                }
            } else if (this.A0C) {
                A01();
            }
            this.A0E = false;
            this.A0C = false;
            return;
        }
        AnonymousClass09T r0 = this.A0F.A0K;
        if (r0 != null && r0.A07) {
            A00();
            this.A0E = true;
        }
    }

    public void setAnimation(int i) {
        String str;
        C06120Sg A06;
        this.A00 = i;
        this.A07 = null;
        if (isInEditMode()) {
            A06 = new C06120Sg(new CallableC10480eh(this, i), true);
        } else {
            boolean z = this.A09;
            Context context = getContext();
            if (z) {
                str = C06550Ub.A08(context, i);
            } else {
                str = null;
            }
            A06 = C06550Ub.A06(str, new CallableC10550eo(context.getApplicationContext(), str, new WeakReference(context), i));
        }
        setCompositionTask(A06);
    }

    public void setAnimation(InputStream inputStream, String str) {
        setCompositionTask(C06550Ub.A06(str, new CallableC10500ej(inputStream, str)));
    }

    public void setAnimation(String str) {
        String str2;
        C06120Sg A06;
        this.A07 = str;
        this.A00 = 0;
        if (isInEditMode()) {
            A06 = new C06120Sg(new CallableC10490ei(this, str), true);
        } else {
            boolean z = this.A09;
            Context context = getContext();
            if (z) {
                StringBuilder sb = new StringBuilder("asset_");
                sb.append(str);
                str2 = sb.toString();
            } else {
                str2 = null;
            }
            A06 = C06550Ub.A06(str2, new CallableC10520el(context.getApplicationContext(), str, str2));
        }
        setCompositionTask(A06);
    }

    @Deprecated
    public void setAnimationFromJson(String str) {
        setAnimationFromJson(str, null);
    }

    public void setAnimationFromJson(String str, String str2) {
        setAnimation(new ByteArrayInputStream(str.getBytes()), str2);
    }

    public void setAnimationFromUrl(String str) {
        String str2;
        boolean z = this.A09;
        Context context = getContext();
        if (z) {
            StringBuilder sb = new StringBuilder("url_");
            sb.append(str);
            str2 = sb.toString();
        } else {
            str2 = null;
        }
        setCompositionTask(C06550Ub.A06(str2, new CallableC10510ek(context, str, str2)));
    }

    public void setAnimationFromUrl(String str, String str2) {
        setCompositionTask(C06550Ub.A06(str2, new CallableC10510ek(getContext(), str, str2)));
    }

    public void setApplyingOpacityToLayersEnabled(boolean z) {
        this.A0F.A0C = z;
    }

    public void setCacheComposition(boolean z) {
        this.A09 = z;
    }

    public void setComposition(C05540Py r6) {
        boolean z;
        float f;
        float f2;
        AnonymousClass0AA r3 = this.A0F;
        r3.setCallback(this);
        this.A03 = r6;
        this.A0A = true;
        boolean z2 = false;
        if (r3.A04 != r6) {
            r3.A0D = false;
            r3.A00();
            r3.A04 = r6;
            r3.A03();
            AnonymousClass09T r4 = r3.A0K;
            boolean z3 = false;
            if (r4.A06 == null) {
                z3 = true;
            }
            r4.A06 = r6;
            if (z3) {
                f = (float) ((int) Math.max(r4.A02, r6.A02));
                f2 = Math.min(r4.A01, r6.A00);
            } else {
                f = (float) ((int) r6.A02);
                f2 = r6.A00;
            }
            r4.A07(f, (float) ((int) f2));
            float f3 = r4.A00;
            r4.A00 = 0.0f;
            r4.A06((float) ((int) f3));
            r4.A03();
            r3.A04(r4.getAnimatedFraction());
            r3.A00 = r3.A00;
            ArrayList arrayList = r3.A0L;
            Iterator it = new ArrayList(arrayList).iterator();
            while (it.hasNext()) {
                AbstractC12000hD r0 = (AbstractC12000hD) it.next();
                if (r0 != null) {
                    r0.Aax(r6);
                }
                it.remove();
            }
            arrayList.clear();
            r6.A0D.A00 = r3.A0F;
            Drawable.Callback callback = r3.getCallback();
            if (callback instanceof ImageView) {
                ImageView imageView = (ImageView) callback;
                imageView.setImageDrawable(null);
                imageView.setImageDrawable(r3);
            }
            z2 = true;
        }
        this.A0A = false;
        A03();
        if (getDrawable() == r3) {
            if (!z2) {
                return;
            }
        } else if (!z2) {
            AnonymousClass09T r02 = r3.A0K;
            if (r02 == null) {
                z = false;
            } else {
                z = r02.A07;
            }
            setImageDrawable(null);
            setImageDrawable(r3);
            if (z) {
                r3.A02();
            }
        }
        onVisibilityChanged(this, getVisibility());
        requestLayout();
        Iterator it2 = this.A0I.iterator();
        if (it2.hasNext()) {
            it2.next();
            throw new NullPointerException("onCompositionLoaded");
        }
    }

    private void setCompositionTask(C06120Sg r2) {
        this.A03 = null;
        this.A0F.A00();
        A02();
        r2.A01(this.A0G);
        r2.A00(this.A0H);
        this.A05 = r2;
    }

    public void setFailureListener(AbstractC12010hE r1) {
        this.A04 = r1;
    }

    public void setFallbackResource(int i) {
        this.A02 = i;
    }

    public void setFontAssetDelegate(AnonymousClass0K0 r2) {
        AnonymousClass0AA r0 = this.A0F;
        r0.A02 = r2;
        C04980Nu r02 = r0.A06;
        if (r02 != null) {
            r02.A00 = r2;
        }
    }

    public void setFrame(int i) {
        this.A0F.A05(i);
    }

    public void setIgnoreDisabledSystemAnimations(boolean z) {
        this.A0F.A0B = z;
    }

    public void setImageAssetDelegate(AbstractC11510gP r2) {
        AnonymousClass0AA r0 = this.A0F;
        r0.A03 = r2;
        C06000Ru r02 = r0.A07;
        if (r02 != null) {
            r02.A00 = r2;
        }
    }

    public void setImageAssetsFolder(String str) {
        this.A0F.A09 = str;
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        A02();
        super.setImageBitmap(bitmap);
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        A02();
        super.setImageDrawable(drawable);
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageResource(int i) {
        A02();
        super.setImageResource(i);
    }

    public void setMaxFrame(int i) {
        this.A0F.A06(i);
    }

    public void setMaxFrame(String str) {
        this.A0F.A0B(str);
    }

    public void setMaxProgress(float f) {
        AnonymousClass0AA r2 = this.A0F;
        C05540Py r0 = r2.A04;
        if (r0 == null) {
            r2.A0L.add(new C07850aB(r2, f));
            return;
        }
        float f2 = r0.A02;
        r2.A06((int) (f2 + (f * (r0.A00 - f2))));
    }

    public void setMinAndMaxFrame(String str) {
        this.A0F.A0C(str);
    }

    public void setMinFrame(int i) {
        this.A0F.A07(i);
    }

    public void setMinFrame(String str) {
        this.A0F.A0D(str);
    }

    public void setMinProgress(float f) {
        AnonymousClass0AA r2 = this.A0F;
        C05540Py r0 = r2.A04;
        if (r0 == null) {
            r2.A0L.add(new C07830a9(r2, f));
            return;
        }
        float f2 = r0.A02;
        r2.A07((int) (f2 + (f * (r0.A00 - f2))));
    }

    public void setOutlineMasksAndMattes(boolean z) {
        AnonymousClass0AA r1 = this.A0F;
        if (r1.A0E != z) {
            r1.A0E = z;
            AnonymousClass0HG r0 = r1.A08;
            if (r0 != null) {
                r0.A05(z);
            }
        }
    }

    public void setPerformanceTrackingEnabled(boolean z) {
        AnonymousClass0AA r0 = this.A0F;
        r0.A0F = z;
        C05540Py r02 = r0.A04;
        if (r02 != null) {
            r02.A0D.A00 = z;
        }
    }

    public void setProgress(float f) {
        this.A0F.A04(f);
    }

    public void setRenderMode(AnonymousClass0J3 r1) {
        this.A06 = r1;
        A03();
    }

    public void setRepeatCount(int i) {
        this.A0F.A0K.setRepeatCount(i);
    }

    public void setRepeatMode(int i) {
        this.A0F.A0K.setRepeatMode(i);
    }

    public void setSafeMode(boolean z) {
        this.A0F.A0G = z;
    }

    public void setScale(float f) {
        boolean z;
        AnonymousClass0AA r2 = this.A0F;
        r2.A00 = f;
        if (getDrawable() == r2) {
            AnonymousClass09T r0 = r2.A0K;
            if (r0 == null) {
                z = false;
            } else {
                z = r0.A07;
            }
            setImageDrawable(null);
            setImageDrawable(r2);
            if (z) {
                r2.A02();
            }
        }
    }

    public void setSpeed(float f) {
        this.A0F.A0K.A03 = f;
    }

    public void setTextDelegate(AnonymousClass0K1 r2) {
        this.A0F.A05 = r2;
    }

    @Override // android.view.View
    public void unscheduleDrawable(Drawable drawable) {
        AnonymousClass0AA r2;
        AnonymousClass09T r1;
        AnonymousClass09T r0;
        if (!this.A0A) {
            AnonymousClass0AA r02 = this.A0F;
            if (drawable == r02 && (r0 = r02.A0K) != null && r0.A07) {
                A00();
            } else if ((drawable instanceof AnonymousClass0AA) && (r1 = (r2 = (AnonymousClass0AA) drawable).A0K) != null && r1.A07) {
                r2.A0L.clear();
                Choreographer.getInstance().removeFrameCallback(r1);
                r1.A07 = false;
            }
        }
        super.unscheduleDrawable(drawable);
    }
}
