package com.google.android.gms.vision.clearcut;

import X.AbstractC14980mM;
import X.AbstractC15000mO;
import X.C56492ky;
import android.os.Bundle;

/* loaded from: classes3.dex */
public class LoggingConnectionCallbacks implements AbstractC14980mM, AbstractC15000mO {
    @Override // X.AbstractC14990mN
    public void onConnected(Bundle bundle) {
        throw new NoSuchMethodError();
    }

    @Override // X.AbstractC15010mP
    public void onConnectionFailed(C56492ky r2) {
        throw new NoSuchMethodError();
    }

    @Override // X.AbstractC14990mN
    public void onConnectionSuspended(int i) {
        throw new NoSuchMethodError();
    }
}
