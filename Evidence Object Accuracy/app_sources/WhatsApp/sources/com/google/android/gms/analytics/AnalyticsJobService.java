package com.google.android.gms.analytics;

import X.AbstractC116385Vf;
import X.C14160kx;
import X.C15110ma;
import X.C56582lF;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

/* loaded from: classes2.dex */
public final class AnalyticsJobService extends JobService implements AbstractC116385Vf {
    public C15110ma A00;

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    @Override // X.AbstractC116385Vf
    public boolean A6s(int i) {
        return stopSelfResult(i);
    }

    @Override // X.AbstractC116385Vf
    public final void Ago(JobParameters jobParameters, boolean z) {
        jobFinished(jobParameters, false);
    }

    @Override // android.app.Service
    public final void onCreate() {
        super.onCreate();
        C15110ma r0 = this.A00;
        if (r0 == null) {
            r0 = new C15110ma(this);
            this.A00 = r0;
        }
        C56582lF r1 = C14160kx.A00(r0.A00).A0C;
        C14160kx.A01(r1);
        r1.A09("Local AnalyticsService is starting up");
    }

    @Override // android.app.Service
    public final void onDestroy() {
        C15110ma r0 = this.A00;
        if (r0 == null) {
            r0 = new C15110ma(this);
            this.A00 = r0;
        }
        C56582lF r1 = C14160kx.A00(r0.A00).A0C;
        C14160kx.A01(r1);
        r1.A09("Local AnalyticsService is shutting down");
        super.onDestroy();
    }

    @Override // android.app.Service
    public final int onStartCommand(Intent intent, int i, int i2) {
        C15110ma r0 = this.A00;
        if (r0 == null) {
            r0 = new C15110ma(this);
            this.A00 = r0;
        }
        r0.A03(intent, i2);
        return 2;
    }

    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        C15110ma r0 = this.A00;
        if (r0 == null) {
            r0 = new C15110ma(this);
            this.A00 = r0;
        }
        r0.A01(jobParameters);
        return true;
    }
}
