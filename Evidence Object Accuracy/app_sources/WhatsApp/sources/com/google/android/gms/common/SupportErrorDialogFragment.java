package com.google.android.gms.common;

import X.C13020j0;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/* loaded from: classes2.dex */
public class SupportErrorDialogFragment extends DialogFragment {
    public Dialog A00;
    public Dialog A01;
    public DialogInterface.OnCancelListener A02;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog dialog = this.A00;
        if (dialog != null) {
            return dialog;
        }
        ((DialogFragment) this).A0E = false;
        Dialog dialog2 = this.A01;
        if (dialog2 != null) {
            return dialog2;
        }
        Context A0p = A0p();
        C13020j0.A01(A0p);
        AlertDialog create = new AlertDialog.Builder(A0p).create();
        this.A01 = create;
        return create;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.A02;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }
}
