package com.google.android.gms.maps.model;

import X.AnonymousClass1U5;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13020j0;
import X.C13290jS;
import X.C95654e8;
import X.C98984jZ;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes2.dex */
public final class LatLngBounds extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98984jZ();
    public final LatLng A00;
    public final LatLng A01;

    public LatLngBounds(LatLng latLng, LatLng latLng2) {
        C13020j0.A02(latLng, "southwest must not be null.");
        C13020j0.A02(latLng2, "northeast must not be null.");
        double d = latLng2.A00;
        double d2 = latLng.A00;
        boolean A1W = C12990iw.A1W((d > d2 ? 1 : (d == d2 ? 0 : -1)));
        Object[] A1a = C12980iv.A1a();
        A1a[0] = Double.valueOf(d2);
        A1a[1] = Double.valueOf(d);
        if (A1W) {
            this.A01 = latLng;
            this.A00 = latLng2;
            return;
        }
        throw C12970iu.A0f(String.format("southern latitude exceeds northern latitude (%s > %s)", A1a));
    }

    public boolean A00(LatLng latLng) {
        C13020j0.A02(latLng, "point must not be null.");
        double d = latLng.A00;
        LatLng latLng2 = this.A01;
        if (latLng2.A00 > d) {
            return false;
        }
        LatLng latLng3 = this.A00;
        if (d > latLng3.A00) {
            return false;
        }
        double d2 = latLng.A01;
        double d3 = latLng2.A01;
        double d4 = latLng3.A01;
        int i = (d3 > d2 ? 1 : (d3 == d2 ? 0 : -1));
        if (d3 <= d4) {
            if (i > 0) {
                return false;
            }
        } else if (i <= 0) {
            return true;
        }
        if (d2 <= d4) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof LatLngBounds) {
                LatLngBounds latLngBounds = (LatLngBounds) obj;
                if (!this.A01.equals(latLngBounds.A01) || !this.A00.equals(latLngBounds.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A00, A1a);
    }

    @Override // java.lang.Object
    public String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(this.A01, "southwest");
        r2.A00(this.A00, "northeast");
        return r2.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0B(parcel, this.A01, 2, i, false);
        C95654e8.A0B(parcel, this.A00, 3, i, false);
        C95654e8.A06(parcel, A01);
    }
}
