package com.google.android.gms.maps;

import X.AnonymousClass1U5;
import X.C13290jS;
import X.C95654e8;
import X.C99034je;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;

/* loaded from: classes2.dex */
public final class GoogleMapOptions extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C99034je();
    public int A00 = -1;
    public CameraPosition A01;
    public LatLngBounds A02 = null;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Float A0F = null;
    public Float A0G = null;

    public static Boolean A03(byte b) {
        if (b == 0) {
            return Boolean.FALSE;
        }
        if (b != 1) {
            return null;
        }
        return Boolean.TRUE;
    }

    public GoogleMapOptions() {
    }

    public GoogleMapOptions(CameraPosition cameraPosition, LatLngBounds latLngBounds, Float f, Float f2, byte b, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9, byte b10, byte b11, byte b12, int i) {
        this.A03 = A03(b);
        this.A04 = A03(b2);
        this.A00 = i;
        this.A01 = cameraPosition;
        this.A05 = A03(b3);
        this.A06 = A03(b4);
        this.A07 = A03(b5);
        this.A08 = A03(b6);
        this.A09 = A03(b7);
        this.A0A = A03(b8);
        this.A0B = A03(b9);
        this.A0C = A03(b10);
        this.A0D = A03(b11);
        this.A0F = f;
        this.A0G = f2;
        this.A02 = latLngBounds;
        this.A0E = A03(b12);
    }

    public static byte A00(Boolean bool) {
        if (bool != null) {
            return !bool.booleanValue() ? (byte) 0 : 1;
        }
        return -1;
    }

    public static void A04(Parcel parcel, byte b, int i) {
        parcel.writeInt(i | 262144);
        parcel.writeInt(b);
    }

    @Override // java.lang.Object
    public String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(Integer.valueOf(this.A00), "MapType");
        r2.A00(this.A0B, "LiteMode");
        r2.A00(this.A01, "Camera");
        r2.A00(this.A06, "CompassEnabled");
        r2.A00(this.A05, "ZoomControlsEnabled");
        r2.A00(this.A07, "ScrollGesturesEnabled");
        r2.A00(this.A08, "ZoomGesturesEnabled");
        r2.A00(this.A09, "TiltGesturesEnabled");
        r2.A00(this.A0A, "RotateGesturesEnabled");
        r2.A00(this.A0E, "ScrollGesturesEnabledDuringRotateOrZoom");
        r2.A00(this.A0C, "MapToolbarEnabled");
        r2.A00(this.A0D, "AmbientEnabled");
        r2.A00(this.A0F, "MinZoomPreference");
        r2.A00(this.A0G, "MaxZoomPreference");
        r2.A00(this.A02, "LatLngBoundsForCameraTarget");
        r2.A00(this.A03, "ZOrderOnTop");
        r2.A00(this.A04, "UseViewLifecycleInFragment");
        return r2.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        A04(parcel, A00(this.A03), 2);
        A04(parcel, A00(this.A04), 3);
        C95654e8.A07(parcel, 4, this.A00);
        C95654e8.A0B(parcel, this.A01, 5, i, false);
        A04(parcel, A00(this.A05), 6);
        A04(parcel, A00(this.A06), 7);
        A04(parcel, A00(this.A07), 8);
        A04(parcel, A00(this.A08), 9);
        A04(parcel, A00(this.A09), 10);
        A04(parcel, A00(this.A0A), 11);
        A04(parcel, A00(this.A0B), 12);
        A04(parcel, A00(this.A0C), 14);
        A04(parcel, A00(this.A0D), 15);
        Float f = this.A0F;
        if (f != null) {
            parcel.writeInt(262160);
            parcel.writeFloat(f.floatValue());
        }
        Float f2 = this.A0G;
        if (f2 != null) {
            parcel.writeInt(262161);
            parcel.writeFloat(f2.floatValue());
        }
        C95654e8.A0B(parcel, this.A02, 18, i, false);
        A04(parcel, A00(this.A0E), 19);
        C95654e8.A06(parcel, A01);
    }
}
