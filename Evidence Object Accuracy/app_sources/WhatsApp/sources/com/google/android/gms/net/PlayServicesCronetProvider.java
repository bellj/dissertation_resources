package com.google.android.gms.net;

import X.AnonymousClass29w;
import X.AnonymousClass29x;
import X.C12960it;
import X.C12980iv;
import X.C13020j0;
import X.C65233Is;
import X.C95564dy;
import android.content.Context;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import org.chromium.net.CronetEngine;
import org.chromium.net.CronetProvider;
import org.chromium.net.ExperimentalCronetEngine;
import org.chromium.net.ICronetEngineBuilder;

/* loaded from: classes2.dex */
public class PlayServicesCronetProvider extends CronetProvider {
    public static final String NATIVE_CRONET_ENGINE_BUILDER_IMPL = "org.chromium.net.impl.NativeCronetEngineBuilderImpl";
    public static final String TAG = "PlayServicesCronet";

    @Override // org.chromium.net.CronetProvider
    public String getName() {
        return "Google-Play-Services-Cronet-Provider";
    }

    public PlayServicesCronetProvider(Context context) {
        super(context);
    }

    @Override // org.chromium.net.CronetProvider
    public CronetEngine.Builder createBuilder() {
        C95564dy r0;
        try {
            C65233Is.A00(this.mContext);
            try {
                synchronized (C65233Is.A03) {
                    r0 = C65233Is.A01;
                }
                C13020j0.A01(r0);
                ClassLoader classLoader = r0.A00.getClassLoader();
                C13020j0.A01(classLoader);
                return new ExperimentalCronetEngine.Builder((ICronetEngineBuilder) classLoader.loadClass(NATIVE_CRONET_ENGINE_BUILDER_IMPL).asSubclass(ICronetEngineBuilder.class).getConstructor(Context.class).newInstance(this.mContext));
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                throw new RuntimeException("Unable to construct the implementation of the Cronet Engine Builder: org.chromium.net.impl.NativeCronetEngineBuilderImpl", e);
            }
        } catch (AnonymousClass29w e2) {
            throw new IllegalStateException("Google Play Services Cronet provider is unavailable on this device.", e2);
        } catch (AnonymousClass29x e3) {
            throw new IllegalStateException("Google Play Services Cronet provider is not enabled. Call com.google.android.gms.net.CronetProviderInstaller.installIfNeeded(Context) to enable it.", e3);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this || ((obj instanceof PlayServicesCronetProvider) && this.mContext.equals(((CronetProvider) obj).mContext))) {
            return true;
        }
        return false;
    }

    @Override // org.chromium.net.CronetProvider
    public String getVersion() {
        String str;
        tryToInstallCronetProvider();
        synchronized (C65233Is.A03) {
            str = C65233Is.A00;
        }
        return str;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = PlayServicesCronetProvider.class;
        return C12960it.A06(this.mContext, A1a);
    }

    @Override // org.chromium.net.CronetProvider
    public boolean isEnabled() {
        tryToInstallCronetProvider();
        return C65233Is.A01();
    }

    private void tryToInstallCronetProvider() {
        String str;
        try {
            C65233Is.A00(this.mContext);
        } catch (AnonymousClass29w unused) {
            if (Log.isLoggable(TAG, 4)) {
                str = "Google-Play-Services-Cronet-Provider is unavailable.";
                Log.i(TAG, str);
            }
        } catch (AnonymousClass29x unused2) {
            if (Log.isLoggable(TAG, 4)) {
                str = "Google-Play-Services-Cronet-Provider is not installed yet.";
                Log.i(TAG, str);
            }
        }
    }
}
