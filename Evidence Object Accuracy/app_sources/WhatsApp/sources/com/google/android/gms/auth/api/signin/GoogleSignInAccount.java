package com.google.android.gms.auth.api.signin;

import X.AbstractC115095Qe;
import X.AnonymousClass1U5;
import X.C108454z5;
import X.C12970iu;
import X.C12990iw;
import X.C95654e8;
import X.C98594iw;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class GoogleSignInAccount extends AnonymousClass1U5 implements ReflectedParcelable {
    public static AbstractC115095Qe A0D = C108454z5.A00;
    public static final Parcelable.Creator CREATOR = new C98594iw();
    public long A00;
    public Uri A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public List A0A;
    public Set A0B = C12970iu.A12();
    public final int A0C;

    public GoogleSignInAccount(Uri uri, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List list, int i, long j) {
        this.A0C = i;
        this.A02 = str;
        this.A03 = str2;
        this.A04 = str3;
        this.A05 = str4;
        this.A01 = uri;
        this.A06 = str5;
        this.A00 = j;
        this.A07 = str6;
        this.A0A = list;
        this.A08 = str7;
        this.A09 = str8;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj != this) {
                if (obj instanceof GoogleSignInAccount) {
                    GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
                    if (googleSignInAccount.A07.equals(this.A07)) {
                        HashSet hashSet = new HashSet(googleSignInAccount.A0A);
                        hashSet.addAll(googleSignInAccount.A0B);
                        HashSet hashSet2 = new HashSet(this.A0A);
                        hashSet2.addAll(this.A0B);
                        if (hashSet.equals(hashSet2)) {
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = (this.A07.hashCode() + 527) * 31;
        HashSet hashSet = new HashSet(this.A0A);
        hashSet.addAll(this.A0B);
        return C12990iw.A08(hashSet, hashCode);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A0C);
        C95654e8.A0D(parcel, this.A02, 2, false);
        C95654e8.A0D(parcel, this.A03, 3, false);
        C95654e8.A0D(parcel, this.A04, 4, false);
        C95654e8.A0D(parcel, this.A05, 5, false);
        C95654e8.A0B(parcel, this.A01, 6, i, false);
        C95654e8.A0D(parcel, this.A06, 7, false);
        C95654e8.A08(parcel, 8, this.A00);
        C95654e8.A0D(parcel, this.A07, 9, false);
        C95654e8.A0F(parcel, this.A0A, 10, false);
        C95654e8.A0D(parcel, this.A08, 11, false);
        C95654e8.A0D(parcel, this.A09, 12, false);
        C95654e8.A06(parcel, A01);
    }
}
