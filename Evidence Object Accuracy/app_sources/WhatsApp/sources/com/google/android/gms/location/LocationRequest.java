package com.google.android.gms.location;

import X.AnonymousClass1U5;
import X.C95654e8;
import X.C98964jX;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.ReflectedParcelable;
import java.util.Arrays;

/* loaded from: classes2.dex */
public final class LocationRequest extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98964jX();
    public float A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public boolean A07;
    public boolean A08;

    @Deprecated
    public LocationRequest() {
        this.A01 = 102;
        this.A03 = 3600000;
        this.A04 = 600000;
        this.A07 = false;
        this.A05 = Long.MAX_VALUE;
        this.A02 = Integer.MAX_VALUE;
        this.A00 = 0.0f;
        this.A06 = 0;
        this.A08 = false;
    }

    public LocationRequest(float f, int i, int i2, long j, long j2, long j3, long j4, boolean z, boolean z2) {
        this.A01 = i;
        this.A03 = j;
        this.A04 = j2;
        this.A07 = z;
        this.A05 = j3;
        this.A02 = i2;
        this.A00 = f;
        this.A06 = j4;
        this.A08 = z2;
    }

    public static void A00(long j) {
        if (j < 0) {
            StringBuilder sb = new StringBuilder(38);
            sb.append("invalid interval: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof LocationRequest) {
            LocationRequest locationRequest = (LocationRequest) obj;
            if (this.A01 == locationRequest.A01) {
                long j = this.A03;
                long j2 = locationRequest.A03;
                if (j == j2 && this.A04 == locationRequest.A04 && this.A07 == locationRequest.A07 && this.A05 == locationRequest.A05 && this.A02 == locationRequest.A02 && this.A00 == locationRequest.A00) {
                    long j3 = this.A06;
                    if (j3 >= j) {
                        j = j3;
                    }
                    long j4 = locationRequest.A06;
                    if (j4 >= j2) {
                        j2 = j4;
                    }
                    if (j == j2 && this.A08 == locationRequest.A08) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A01), Long.valueOf(this.A03), Float.valueOf(this.A00), Long.valueOf(this.A06)});
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("Request[");
        int i = this.A01;
        if (i != 100) {
            str = i != 102 ? i != 104 ? i != 105 ? "???" : "PRIORITY_NO_POWER" : "PRIORITY_LOW_POWER" : "PRIORITY_BALANCED_POWER_ACCURACY";
        } else {
            str = "PRIORITY_HIGH_ACCURACY";
        }
        sb.append(str);
        if (i != 105) {
            sb.append(" requested=");
            sb.append(this.A03);
            sb.append("ms");
        }
        sb.append(" fastest=");
        sb.append(this.A04);
        sb.append("ms");
        long j = this.A06;
        if (j > this.A03) {
            sb.append(" maxWait=");
            sb.append(j);
            sb.append("ms");
        }
        float f = this.A00;
        if (f > 0.0f) {
            sb.append(" smallestDisplacement=");
            sb.append(f);
            sb.append("m");
        }
        long j2 = this.A05;
        if (j2 != Long.MAX_VALUE) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(j2 - elapsedRealtime);
            sb.append("ms");
        }
        int i2 = this.A02;
        if (i2 != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(i2);
        }
        sb.append(']');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A01);
        C95654e8.A08(parcel, 2, this.A03);
        C95654e8.A08(parcel, 3, this.A04);
        C95654e8.A09(parcel, 4, this.A07);
        C95654e8.A08(parcel, 5, this.A05);
        C95654e8.A07(parcel, 6, this.A02);
        C95654e8.A05(parcel, this.A00, 7);
        C95654e8.A08(parcel, 8, this.A06);
        C95654e8.A09(parcel, 9, this.A08);
        C95654e8.A06(parcel, A01);
    }
}
