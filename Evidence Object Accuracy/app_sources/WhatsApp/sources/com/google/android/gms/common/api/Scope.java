package com.google.android.gms.common.api;

import X.AnonymousClass1U5;
import X.C13020j0;
import X.C95654e8;
import X.C98664j3;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes2.dex */
public final class Scope extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98664j3();
    public final int A00;
    public final String A01;

    @Override // java.lang.Object
    public String toString() {
        return this.A01;
    }

    public Scope(int i, String str) {
        C13020j0.A07(str, "scopeUri must not be null or empty");
        this.A00 = i;
        this.A01 = str;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Scope)) {
            return false;
        }
        return this.A01.equals(((Scope) obj).A01);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A01.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0D(parcel, this.A01, 2, false);
        C95654e8.A06(parcel, A01);
    }
}
