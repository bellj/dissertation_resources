package com.google.android.gms.common.api.internal;

import X.AbstractC108234yj;
import X.AbstractC50792Ra;
import X.AnonymousClass1U8;
import X.AnonymousClass1UK;
import X.AnonymousClass2RR;
import X.AnonymousClass4IR;
import X.AnonymousClass5HF;
import X.AnonymousClass5SX;
import X.C1091150l;
import X.C1091250m;
import X.C13020j0;
import X.C50822Rd;
import X.HandlerC56522l9;
import android.os.Looper;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@KeepName
/* loaded from: classes2.dex */
public abstract class BasePendingResult extends AnonymousClass1UK {
    public static final ThreadLocal A0D = new AnonymousClass5HF();
    public AnonymousClass5SX A00;
    public Status A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final HandlerC56522l9 A05;
    public final Object A06;
    public final WeakReference A07;
    public final ArrayList A08;
    public final CountDownLatch A09;
    public final AtomicReference A0A;
    public volatile boolean A0B;
    public volatile AbstractC108234yj A0C;

    @Deprecated
    public BasePendingResult() {
        this.A06 = new Object();
        this.A09 = new CountDownLatch(1);
        this.A08 = new ArrayList();
        this.A0A = new AtomicReference();
        this.A03 = false;
        this.A05 = new HandlerC56522l9(Looper.getMainLooper());
        this.A07 = new WeakReference(null);
    }

    public BasePendingResult(AnonymousClass1U8 r3) {
        Looper mainLooper;
        this.A06 = new Object();
        this.A09 = new CountDownLatch(1);
        this.A08 = new ArrayList();
        this.A0A = new AtomicReference();
        this.A03 = false;
        if (r3 != null) {
            mainLooper = r3.A03();
        } else {
            mainLooper = Looper.getMainLooper();
        }
        this.A05 = new HandlerC56522l9(mainLooper);
        this.A07 = new WeakReference(r3);
    }

    public static final AnonymousClass5SX A00(BasePendingResult basePendingResult) {
        AnonymousClass5SX r2;
        synchronized (basePendingResult.A06) {
            C13020j0.A04("Result has already been consumed.", !basePendingResult.A0B);
            boolean z = false;
            if (basePendingResult.A09.getCount() == 0) {
                z = true;
            }
            C13020j0.A04("Result is not ready.", z);
            r2 = basePendingResult.A00;
            basePendingResult.A00 = null;
            basePendingResult.A0B = true;
        }
        AnonymousClass4IR r0 = (AnonymousClass4IR) basePendingResult.A0A.getAndSet(null);
        if (r0 != null) {
            r0.A00.A01.remove(basePendingResult);
        }
        C13020j0.A01(r2);
        return r2;
    }

    private final void A01(AnonymousClass5SX r6) {
        this.A00 = r6;
        this.A01 = r6.AGw();
        this.A09.countDown();
        ArrayList arrayList = this.A08;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((AbstractC50792Ra) arrayList.get(i)).AON(this.A01);
        }
        arrayList.clear();
    }

    public AnonymousClass5SX A02(Status status) {
        if (this instanceof C50822Rd) {
            return new C1091250m(status, null);
        }
        if (!(this instanceof AnonymousClass2RR)) {
            return status;
        }
        return new C1091150l(status, null);
    }

    public void A03() {
        synchronized (this.A06) {
            if (!this.A02 && !this.A0B) {
                this.A02 = true;
                A01(A02(Status.A05));
            }
        }
    }

    public final void A04() {
        boolean z = true;
        if (!this.A03 && !((Boolean) A0D.get()).booleanValue()) {
            z = false;
        }
        this.A03 = z;
    }

    public final void A05(AnonymousClass5SX r8) {
        synchronized (this.A06) {
            if (!this.A04 && !this.A02) {
                CountDownLatch countDownLatch = this.A09;
                countDownLatch.getCount();
                int i = (countDownLatch.getCount() > 0 ? 1 : (countDownLatch.getCount() == 0 ? 0 : -1));
                boolean z = false;
                if (i == 0) {
                    z = true;
                }
                C13020j0.A04("Results have already been set", !z);
                C13020j0.A04("Result has already been consumed", !this.A0B);
                A01(r8);
            }
        }
    }

    @Deprecated
    public final void A06(Status status) {
        synchronized (this.A06) {
            if (this.A09.getCount() != 0) {
                A05(A02(status));
                this.A04 = true;
            }
        }
    }
}
