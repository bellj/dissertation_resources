package com.google.android.gms.auth.api.signin.internal;

import X.ActivityC000900k;
import X.C112265Cs;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13020j0;
import X.C64813Gz;
import X.C65263Iv;
import X.C67443Rn;
import android.accounts.Account;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@KeepName
/* loaded from: classes2.dex */
public class SignInHubActivity extends ActivityC000900k {
    public static boolean A05;
    public int A00;
    public Intent A01;
    public SignInConfiguration A02;
    public boolean A03 = false;
    public boolean A04;

    @Override // android.app.Activity, android.view.Window.Callback
    public final boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return true;
    }

    public final void A1U(int i) {
        Status status = new Status(i, null);
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("googleSignInStatus", status);
        setResult(0, A0A);
        finish();
        A05 = false;
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public final void onActivityResult(int i, int i2, Intent intent) {
        GoogleSignInAccount googleSignInAccount;
        if (!this.A03) {
            setResult(0);
            if (i == 40962) {
                int i3 = 8;
                if (intent != null) {
                    SignInAccount signInAccount = (SignInAccount) intent.getParcelableExtra("signInAccount");
                    if (signInAccount != null && (googleSignInAccount = signInAccount.A00) != null) {
                        C64813Gz A00 = C64813Gz.A00(this);
                        GoogleSignInOptions googleSignInOptions = this.A02.A00;
                        synchronized (A00) {
                            C65263Iv r5 = A00.A02;
                            C13020j0.A01(googleSignInOptions);
                            String str = googleSignInAccount.A07;
                            r5.A05("defaultGoogleSignInAccount", str);
                            C13020j0.A01(googleSignInOptions);
                            String A01 = C65263Iv.A01("googleSignInAccount", str);
                            JSONObject jSONObject = new JSONObject();
                            try {
                                String str2 = googleSignInAccount.A02;
                                if (str2 != null) {
                                    jSONObject.put("id", str2);
                                }
                                String str3 = googleSignInAccount.A03;
                                if (str3 != null) {
                                    jSONObject.put("tokenId", str3);
                                }
                                String str4 = googleSignInAccount.A04;
                                if (str4 != null) {
                                    jSONObject.put("email", str4);
                                }
                                String str5 = googleSignInAccount.A05;
                                if (str5 != null) {
                                    jSONObject.put("displayName", str5);
                                }
                                String str6 = googleSignInAccount.A08;
                                if (str6 != null) {
                                    jSONObject.put("givenName", str6);
                                }
                                String str7 = googleSignInAccount.A09;
                                if (str7 != null) {
                                    jSONObject.put("familyName", str7);
                                }
                                Uri uri = googleSignInAccount.A01;
                                if (uri != null) {
                                    jSONObject.put("photoUrl", uri.toString());
                                }
                                String str8 = googleSignInAccount.A06;
                                if (str8 != null) {
                                    jSONObject.put("serverAuthCode", str8);
                                }
                                jSONObject.put("expirationTime", googleSignInAccount.A00);
                                jSONObject.put("obfuscatedIdentifier", str);
                                JSONArray jSONArray = new JSONArray();
                                List list = googleSignInAccount.A0A;
                                Scope[] scopeArr = (Scope[]) list.toArray(new Scope[list.size()]);
                                Arrays.sort(scopeArr, C112265Cs.A00);
                                for (Scope scope : scopeArr) {
                                    jSONArray.put(scope.A01);
                                }
                                jSONObject.put("grantedScopes", jSONArray);
                                jSONObject.remove("serverAuthCode");
                                r5.A05(A01, jSONObject.toString());
                                String A012 = C65263Iv.A01("googleSignInOptions", str);
                                JSONObject jSONObject2 = new JSONObject();
                                try {
                                    JSONArray jSONArray2 = new JSONArray();
                                    ArrayList arrayList = googleSignInOptions.A08;
                                    Collections.sort(arrayList, GoogleSignInOptions.A0B);
                                    Iterator it = arrayList.iterator();
                                    while (it.hasNext()) {
                                        jSONArray2.put(((Scope) it.next()).A01);
                                    }
                                    jSONObject2.put("scopes", jSONArray2);
                                    Account account = googleSignInOptions.A00;
                                    if (account != null) {
                                        jSONObject2.put("accountName", account.name);
                                    }
                                    jSONObject2.put("idTokenRequested", googleSignInOptions.A06);
                                    jSONObject2.put("forceCodeForRefreshToken", googleSignInOptions.A0A);
                                    jSONObject2.put("serverAuthRequested", googleSignInOptions.A09);
                                    String str9 = googleSignInOptions.A01;
                                    if (!TextUtils.isEmpty(str9)) {
                                        jSONObject2.put("serverClientId", str9);
                                    }
                                    String str10 = googleSignInOptions.A02;
                                    if (!TextUtils.isEmpty(str10)) {
                                        jSONObject2.put("hostedDomain", str10);
                                    }
                                    r5.A05(A012, jSONObject2.toString());
                                    A00.A00 = googleSignInAccount;
                                    A00.A01 = googleSignInOptions;
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                            } catch (JSONException e2) {
                                throw new RuntimeException(e2);
                            }
                        }
                        intent.removeExtra("signInAccount");
                        intent.putExtra("googleSignInAccount", googleSignInAccount);
                        this.A04 = true;
                        this.A00 = i2;
                        this.A01 = intent;
                        A0W().A02(new C67443Rn(this));
                        A05 = false;
                        return;
                    } else if (intent.hasExtra("errorCode") && (i3 = intent.getIntExtra("errorCode", 8)) == 13) {
                        i3 = 12501;
                    }
                }
                A1U(i3);
            }
        }
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public final void onCreate(Bundle bundle) {
        String packageName;
        int i;
        super.onCreate(bundle);
        Intent intent = getIntent();
        String action = intent.getAction();
        if ("com.google.android.gms.auth.NO_IMPL".equals(action)) {
            i = 12500;
        } else {
            boolean equals = action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN");
            if (equals || action.equals("com.google.android.gms.auth.APPAUTH_SIGN_IN")) {
                SignInConfiguration signInConfiguration = (SignInConfiguration) intent.getBundleExtra("config").getParcelable("config");
                if (signInConfiguration == null) {
                    Log.e("AuthSignInClient", "Activity started with invalid configuration.");
                    setResult(0);
                } else {
                    this.A02 = signInConfiguration;
                    if (bundle != null) {
                        boolean z = bundle.getBoolean("signingInGoogleApiClients");
                        this.A04 = z;
                        if (z) {
                            this.A00 = bundle.getInt("signInResultCode");
                            this.A01 = (Intent) bundle.getParcelable("signInResultData");
                            A0W().A02(new C67443Rn(this));
                            A05 = false;
                            return;
                        }
                        return;
                    } else if (A05) {
                        setResult(0);
                        i = 12502;
                    } else {
                        A05 = true;
                        Intent A0E = C12990iw.A0E(action);
                        if (equals) {
                            packageName = "com.google.android.gms";
                        } else {
                            packageName = getPackageName();
                        }
                        A0E.setPackage(packageName);
                        A0E.putExtra("config", this.A02);
                        try {
                            startActivityForResult(A0E, 40962);
                            return;
                        } catch (ActivityNotFoundException unused) {
                            this.A03 = true;
                            Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
                            A1U(17);
                            return;
                        }
                    }
                }
            } else {
                Log.e("AuthSignInClient", C12960it.A0c(String.valueOf(intent.getAction()), "Unknown action: "));
            }
            finish();
            return;
        }
        A1U(i);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public final void onDestroy() {
        super.onDestroy();
        A05 = false;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("signingInGoogleApiClients", this.A04);
        if (this.A04) {
            bundle.putInt("signInResultCode", this.A00);
            bundle.putParcelable("signInResultData", this.A01);
        }
    }
}
