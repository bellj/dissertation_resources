package com.google.android.gms.auth;

import X.AnonymousClass1U5;
import X.C12980iv;
import X.C13020j0;
import X.C13300jT;
import X.C95654e8;
import X.C98634j0;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import java.util.List;

/* loaded from: classes3.dex */
public class TokenData extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98634j0();
    public final int A00;
    public final Long A01;
    public final String A02;
    public final String A03;
    public final List A04;
    public final boolean A05;
    public final boolean A06;

    public TokenData(Long l, String str, String str2, List list, int i, boolean z, boolean z2) {
        this.A00 = i;
        C13020j0.A05(str);
        this.A03 = str;
        this.A01 = l;
        this.A05 = z;
        this.A06 = z2;
        this.A04 = list;
        this.A02 = str2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof TokenData) {
            TokenData tokenData = (TokenData) obj;
            if (TextUtils.equals(this.A03, tokenData.A03) && C13300jT.A00(this.A01, tokenData.A01) && this.A05 == tokenData.A05 && this.A06 == tokenData.A06 && C13300jT.A00(this.A04, tokenData.A04) && C13300jT.A00(this.A02, tokenData.A02)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[6];
        objArr[0] = this.A03;
        objArr[1] = this.A01;
        objArr[2] = Boolean.valueOf(this.A05);
        objArr[3] = Boolean.valueOf(this.A06);
        objArr[4] = this.A04;
        return C12980iv.A0B(this.A02, objArr, 5);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        boolean A0K = C95654e8.A0K(parcel, this.A03);
        Long l = this.A01;
        if (l != null) {
            parcel.writeInt(524291);
            parcel.writeLong(l.longValue());
        }
        C95654e8.A09(parcel, 4, this.A05);
        C95654e8.A09(parcel, 5, this.A06);
        C95654e8.A0E(parcel, this.A04, 6);
        C95654e8.A0D(parcel, this.A02, 7, A0K);
        C95654e8.A06(parcel, A00);
    }
}
