package com.google.android.gms.common.api;

import X.AnonymousClass1U5;
import X.AnonymousClass3A7;
import X.AnonymousClass5SX;
import X.C12960it;
import X.C12980iv;
import X.C13290jS;
import X.C13300jT;
import X.C56492ky;
import X.C95654e8;
import X.C98674j4;
import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes2.dex */
public final class Status extends AnonymousClass1U5 implements AnonymousClass5SX, ReflectedParcelable {
    public static final Status A05 = new Status(16, null);
    public static final Status A06 = new Status(18, null);
    public static final Status A07 = new Status(8, null);
    public static final Status A08 = new Status(14, null);
    public static final Status A09 = new Status(0, null);
    public static final Status A0A = new Status(15, null);
    public static final Status A0B = new Status(17, null);
    public static final Parcelable.Creator CREATOR = new C98674j4();
    public final int A00;
    public final int A01;
    public final PendingIntent A02;
    public final C56492ky A03;
    public final String A04;

    public Status(PendingIntent pendingIntent, C56492ky r2, String str, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A04 = str;
        this.A02 = pendingIntent;
        this.A03 = r2;
    }

    @Override // X.AnonymousClass5SX
    public Status AGw() {
        return this;
    }

    public Status(int i, String str) {
        this(null, null, str, 1, i);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof Status) {
            Status status = (Status) obj;
            if (this.A00 == status.A00 && this.A01 == status.A01 && C13300jT.A00(this.A04, status.A04) && C13300jT.A00(this.A02, status.A02) && C13300jT.A00(this.A03, status.A03)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[5];
        C12960it.A1O(objArr, this.A00);
        C12980iv.A1T(objArr, this.A01);
        objArr[2] = this.A04;
        objArr[3] = this.A02;
        return C12980iv.A0B(this.A03, objArr, 4);
    }

    @Override // java.lang.Object
    public String toString() {
        C13290jS r2 = new C13290jS(this);
        String str = this.A04;
        if (str == null) {
            str = AnonymousClass3A7.A00(this.A01);
        }
        r2.A00(str, "statusCode");
        r2.A00(this.A02, "resolution");
        return r2.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A01);
        C95654e8.A0D(parcel, this.A04, 2, false);
        C95654e8.A0B(parcel, this.A02, 3, i, false);
        C95654e8.A0B(parcel, this.A03, 4, i, false);
        C95654e8.A07(parcel, 1000, this.A00);
        C95654e8.A06(parcel, A01);
    }
}
