package com.google.android.gms.analytics;

import X.C13020j0;
import X.C14160kx;
import X.C14170ky;
import X.C15050mT;
import X.C56572lE;
import X.C56582lF;
import X.C88904Hw;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;

/* loaded from: classes2.dex */
public class CampaignTrackingReceiver extends BroadcastReceiver {
    public static Boolean A00;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        C14160kx A002 = C14160kx.A00(context);
        C56582lF r6 = A002.A0C;
        C14160kx.A01(r6);
        if (intent == null) {
            str = "CampaignTrackingReceiver received null intent";
        } else {
            String stringExtra = intent.getStringExtra("referrer");
            String action = intent.getAction();
            r6.A0D("CampaignTrackingReceiver received", action);
            if (!"com.android.vending.INSTALL_REFERRER".equals(action) || TextUtils.isEmpty(stringExtra)) {
                str = "CampaignTrackingReceiver received unexpected intent without referrer extra";
            } else {
                int intValue = ((Number) C88904Hw.A0g.A00()).intValue();
                int length = stringExtra.length();
                if (length > intValue) {
                    r6.A07(Integer.valueOf(length), Integer.valueOf(intValue), "Campaign data exceed the maximum supported size and will be clipped. size, limit");
                    stringExtra = stringExtra.substring(0, intValue);
                }
                BroadcastReceiver.PendingResult goAsync = goAsync();
                C56572lE r4 = A002.A06;
                C14160kx.A01(r4);
                RunnableBRunnable0Shape0S0100000_I0 runnableBRunnable0Shape0S0100000_I0 = new RunnableBRunnable0Shape0S0100000_I0(goAsync);
                C13020j0.A07(stringExtra, "campaign param can't be empty");
                C14170ky r2 = ((C15050mT) r4).A00.A03;
                C13020j0.A01(r2);
                r2.A03.submit(new RunnableBRunnable0Shape1S1200000_I1(r4, runnableBRunnable0Shape0S0100000_I0, stringExtra, 2));
                return;
            }
        }
        r6.A0A(str);
    }
}
