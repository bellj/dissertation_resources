package com.google.android.gms.auth.api.signin;

import X.BinderC56512l8;
import X.C12960it;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/* loaded from: classes2.dex */
public final class RevocationBoundService extends Service {
    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        if ("com.google.android.gms.auth.api.signin.RevocationBoundService.disconnect".equals(intent.getAction()) || "com.google.android.gms.auth.api.signin.RevocationBoundService.clearClientState".equals(intent.getAction())) {
            if (Log.isLoggable("RevocationService", 2)) {
                Log.v("RevocationService", C12960it.A0c(String.valueOf(intent.getAction()), "RevocationBoundService handling "));
            }
            return new BinderC56512l8(this);
        }
        Log.w("RevocationService", C12960it.A0c(String.valueOf(intent.getAction()), "Unknown action sent to RevocationBoundService: "));
        return null;
    }
}
