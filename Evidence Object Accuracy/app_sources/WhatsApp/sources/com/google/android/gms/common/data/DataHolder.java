package com.google.android.gms.common.data;

import X.AnonymousClass1U5;
import X.AnonymousClass4PJ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C95654e8;
import X.C98684j5;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import java.io.Closeable;

@KeepName
/* loaded from: classes3.dex */
public final class DataHolder extends AnonymousClass1U5 implements Closeable {
    public static final AnonymousClass4PJ A08 = new AnonymousClass4PJ(new String[0]);
    public static final Parcelable.Creator CREATOR = new C98684j5();
    public Bundle A00;
    public boolean A01 = false;
    public int[] A02;
    public final int A03;
    public final int A04;
    public final Bundle A05;
    public final CursorWindow[] A06;
    public final String[] A07;

    public DataHolder(Bundle bundle, CursorWindow[] cursorWindowArr, String[] strArr, int i, int i2) {
        this.A03 = i;
        this.A07 = strArr;
        this.A06 = cursorWindowArr;
        this.A04 = i2;
        this.A05 = bundle;
    }

    public final void A00() {
        this.A00 = C12970iu.A0D();
        int i = 0;
        while (true) {
            String[] strArr = this.A07;
            if (i >= strArr.length) {
                break;
            }
            this.A00.putInt(strArr[i], i);
            i++;
        }
        CursorWindow[] cursorWindowArr = this.A06;
        int length = cursorWindowArr.length;
        this.A02 = new int[length];
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            this.A02[i3] = i2;
            i2 += cursorWindowArr[i3].getNumRows() - (i2 - cursorWindowArr[i3].getStartPosition());
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            if (!this.A01) {
                this.A01 = true;
                int i = 0;
                while (true) {
                    CursorWindow[] cursorWindowArr = this.A06;
                    if (i >= cursorWindowArr.length) {
                        break;
                    }
                    cursorWindowArr[i].close();
                    i++;
                }
            }
        }
    }

    @Override // java.lang.Object
    public final void finalize() {
        boolean z;
        try {
            if (this.A06.length > 0) {
                synchronized (this) {
                    z = this.A01;
                }
                if (!z) {
                    close();
                    String obj = toString();
                    StringBuilder A0t = C12980iv.A0t(C12970iu.A07(obj) + 178);
                    A0t.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                    A0t.append(obj);
                    Log.e("DataBuffer", C12960it.A0d(")", A0t));
                }
            }
        } finally {
            super.finalize();
        }
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        String[] strArr = this.A07;
        if (strArr != null) {
            int A02 = C95654e8.A02(parcel, 1);
            parcel.writeStringArray(strArr);
            C95654e8.A06(parcel, A02);
        }
        C95654e8.A0I(parcel, this.A06, 2, i);
        C95654e8.A07(parcel, 3, this.A04);
        C95654e8.A03(this.A05, parcel, 4);
        C95654e8.A07(parcel, 1000, this.A03);
        C95654e8.A06(parcel, A00);
        if ((i & 1) != 0) {
            close();
        }
    }
}
