package com.google.android.gms.vision.clearcut;

import X.AnonymousClass4PS;
import X.C80113rj;
import android.content.Context;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class DynamiteClearcutLogger {
    public static final ExecutorService zza;
    public AnonymousClass4PS zzb = new AnonymousClass4PS();
    public VisionClearcutLogger zzc;

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 2, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Executors.defaultThreadFactory());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        zza = Executors.unconfigurableExecutorService(threadPoolExecutor);
    }

    public DynamiteClearcutLogger(Context context) {
        this.zzc = new VisionClearcutLogger(context);
    }

    public final void zza(int i, C80113rj r11) {
        boolean z;
        if (i == 3) {
            AnonymousClass4PS r8 = this.zzb;
            synchronized (r8.A02) {
                long currentTimeMillis = System.currentTimeMillis();
                if (r8.A00 + r8.A01 > currentTimeMillis) {
                    z = false;
                } else {
                    r8.A00 = currentTimeMillis;
                    z = true;
                }
            }
            if (!z) {
                Object[] objArr = new Object[0];
                if (Log.isLoggable("Vision", 2)) {
                    Log.v("Vision", String.format("Skipping image analysis log due to rate limiting", objArr));
                    return;
                }
                return;
            }
        }
        zza.execute(new RunnableBRunnable0Shape1S0201000_I1(this, r11, i, 1));
    }
}
