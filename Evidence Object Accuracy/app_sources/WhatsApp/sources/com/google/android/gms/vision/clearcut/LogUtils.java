package com.google.android.gms.vision.clearcut;

import X.AbstractC80173rp;
import X.AnonymousClass4DY;
import X.AnonymousClass50T;
import X.AnonymousClass5I0;
import X.AnonymousClass5Z6;
import X.C12960it;
import X.C15080mX;
import X.C72463ee;
import X.C78563p7;
import X.C80013rZ;
import X.C80043rc;
import X.C80083rg;
import X.C80103ri;
import X.C80113rj;
import X.C80163ro;
import android.content.Context;
import android.content.pm.PackageManager;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public class LogUtils {
    public static C80043rc zza(Context context) {
        C80013rZ A06 = AnonymousClass50T.A06(C80043rc.zzf);
        String packageName = context.getPackageName();
        C80013rZ.A00(A06);
        C80043rc r1 = (C80043rc) A06.A00;
        r1.zzc |= 1;
        r1.zzd = packageName;
        String zzb = zzb(context);
        if (zzb != null) {
            C80013rZ.A00(A06);
            C80043rc r12 = (C80043rc) A06.A00;
            r12.zzc |= 2;
            r12.zze = zzb;
        }
        return (C80043rc) ((AbstractC80173rp) A06.A01());
    }

    public static C80113rj zza(long j, int i, String str, String str2, List list, C78563p7 r12) {
        C80013rZ r3 = (C80013rZ) C80083rg.zzg.A06(5);
        C80013rZ r5 = (C80013rZ) C80163ro.zzl.A06(5);
        C80013rZ.A00(r5);
        C80163ro r4 = (C80163ro) r5.A00;
        int i2 = r4.zzc | 1;
        r4.zzc = i2;
        r4.zzd = str2;
        int i3 = i2 | 16;
        r4.zzc = i3;
        r4.zzi = j;
        r4.zzc = i3 | 32;
        r4.zzj = (long) i;
        AnonymousClass5Z6 r1 = r4.zzk;
        if (!((AnonymousClass5I0) r1).A00) {
            r1 = r1.Agl(C72463ee.A05(r1));
            r4.zzk = r1;
        }
        AnonymousClass50T.A07(list, r1);
        ArrayList A0l = C12960it.A0l();
        A0l.add(r5.A01());
        C80013rZ.A00(r3);
        C80083rg r2 = (C80083rg) r3.A00;
        AnonymousClass5Z6 r13 = r2.zzf;
        if (!((AnonymousClass5I0) r13).A00) {
            r13 = r13.Agl(C72463ee.A05(r13));
            r2.zzf = r13;
        }
        AnonymousClass50T.A07(A0l, r13);
        C80013rZ A06 = AnonymousClass50T.A06(C80103ri.zzi);
        C80013rZ.A00(A06);
        C80103ri r52 = (C80103ri) A06.A00;
        int i4 = r52.zzc | 4;
        r52.zzc = i4;
        r52.zzf = (long) r12.A01;
        int i5 = i4 | 2;
        r52.zzc = i5;
        r52.zze = (long) r12.A00;
        long j2 = (long) r12.A02;
        int i6 = i5 | 8;
        r52.zzc = i6;
        r52.zzg = j2;
        long j3 = r12.A04;
        r52.zzc = i6 | 16;
        r52.zzh = j3;
        C80013rZ.A00(r3);
        C80083rg r14 = (C80083rg) r3.A00;
        r14.zzd = (C80103ri) ((AbstractC80173rp) A06.A01());
        r14.zzc |= 1;
        C80013rZ A062 = AnonymousClass50T.A06(C80113rj.zzi);
        C80013rZ.A00(A062);
        C80113rj r15 = (C80113rj) A062.A00;
        r15.zzf = (C80083rg) ((AbstractC80173rp) r3.A01());
        r15.zzc |= 4;
        return (C80113rj) ((AbstractC80173rp) A062.A01());
    }

    public static String zzb(Context context) {
        try {
            return C15080mX.A00(context).A00.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            AnonymousClass4DY.A00("Unable to find calling package info for %s", e, context.getPackageName());
            return null;
        }
    }
}
