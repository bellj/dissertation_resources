package com.google.android.gms.common.api.internal;

import X.AbstractC72443eb;
import X.AbstractC95064d1;
import X.AbstractRunnableC76303lQ;
import X.AnonymousClass1UE;
import X.AnonymousClass3BW;
import X.AnonymousClass5Sa;
import X.AnonymousClass5Yh;
import X.C108324ys;
import X.C108364yw;
import X.C108394yz;
import X.C108404z0;
import X.C12960it;
import X.C12980iv;
import X.C56492ky;
import X.C64793Gx;
import X.C72453ed;
import X.C72463ee;
import X.C77733nl;
import X.C77833nw;
import X.C77843nx;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* loaded from: classes3.dex */
public class RunnablezaavShape12S0200000_I1 extends AbstractRunnableC76303lQ {
    public Object A00;
    public Object A01;
    public final int A02 = 1;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnablezaavShape12S0200000_I1(C108324ys r2, ArrayList arrayList) {
        super(r2);
        this.A00 = r2;
        this.A01 = arrayList;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnablezaavShape12S0200000_I1(C108324ys r2, Map map) {
        super(r2);
        this.A00 = r2;
        this.A01 = map;
    }

    @Override // X.AbstractRunnableC76303lQ
    public final void A00() {
        Set hashSet;
        AnonymousClass5Yh r1;
        switch (this.A02) {
            case 0:
                C108324ys r3 = (C108324ys) this.A00;
                C64793Gx r5 = new C64793Gx(r3.A0D);
                ArrayList A0l = C12960it.A0l();
                ArrayList A0l2 = C12960it.A0l();
                Map map = (Map) this.A01;
                Iterator A10 = C72453ed.A10(map);
                while (A10.hasNext()) {
                    AbstractC72443eb r12 = (AbstractC72443eb) A10.next();
                    if (!r12.Aad() || ((C108404z0) map.get(r12)).A02) {
                        A0l2.add(r12);
                    } else {
                        A0l.add(r12);
                    }
                }
                int i = -1;
                int i2 = 0;
                if (A0l.isEmpty()) {
                    int size = A0l2.size();
                    while (i2 < size) {
                        i = r5.A00(r3.A0B, (AbstractC72443eb) A0l2.get(i2));
                        i2++;
                        if (i == 0) {
                            if (r3.A07 && (r1 = r3.A05) != null) {
                                AbstractC95064d1 r13 = (AbstractC95064d1) r1;
                                r13.A7X(new C108394yz(r13));
                            }
                            Iterator A102 = C72453ed.A10(map);
                            while (A102.hasNext()) {
                                AbstractC72443eb r14 = (AbstractC72443eb) A102.next();
                                AnonymousClass5Sa r2 = (AnonymousClass5Sa) map.get(r14);
                                if (!r14.Aad() || r5.A00(r3.A0B, r14) == 0) {
                                    r14.A7X(r2);
                                } else {
                                    C72463ee.A0R(r3.A0F.A06, new C77833nw(r3, r2));
                                }
                            }
                            return;
                        }
                    }
                    C56492ky r22 = new C56492ky(i, null);
                    C72463ee.A0R(r3.A0F.A06, new C77843nx(r22, this, r3));
                    return;
                }
                int size2 = A0l.size();
                while (true) {
                    if (i2 < size2) {
                        i = r5.A00(r3.A0B, (AbstractC72443eb) A0l.get(i2));
                        i2++;
                        if (i != 0) {
                        }
                    } else if (i == 0) {
                    }
                }
                C56492ky r22 = new C56492ky(i, null);
                C72463ee.A0R(r3.A0F.A06, new C77843nx(r22, this, r3));
                return;
            case 1:
                C108324ys r6 = (C108324ys) this.A00;
                C108364yw r8 = r6.A0F;
                C77733nl r52 = r8.A05;
                AnonymousClass3BW r15 = r6.A0G;
                if (r15 == null) {
                    hashSet = Collections.emptySet();
                } else {
                    hashSet = new HashSet(r15.A05);
                    Map map2 = r15.A04;
                    Iterator A103 = C72453ed.A10(map2);
                    while (A103.hasNext()) {
                        AnonymousClass1UE r23 = (AnonymousClass1UE) A103.next();
                        if (!r8.A0A.containsKey(r23.A01)) {
                            map2.get(r23);
                            throw C12980iv.A0n("zaa");
                        }
                    }
                }
                r52.A04 = hashSet;
                AbstractList abstractList = (AbstractList) this.A01;
                int size3 = abstractList.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    ((AbstractC72443eb) abstractList.get(i3)).AG7(r6.A04, r52.A04);
                }
                return;
            default:
                super.A00();
                return;
        }
    }
}
