package com.google.android.gms.flags.impl;

import X.AbstractBinderC73303g0;
import X.AbstractBinderC79573qo;
import X.AnonymousClass5DV;
import X.AnonymousClass5Y2;
import X.BinderC56502l7;
import X.C12960it;
import X.C72453ed;
import X.C87684Cm;
import X.C98424if;
import X.CallableC112515Dr;
import X.CallableC112525Ds;
import X.CallableC112535Dt;
import X.CallableC112545Du;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public class FlagProviderImpl extends AbstractBinderC73303g0 implements AnonymousClass5Y2 {
    public boolean zzu;
    public SharedPreferences zzv;

    public FlagProviderImpl() {
        this(0);
        this.zzu = false;
    }

    public FlagProviderImpl(int i) {
        super("com.google.android.gms.flags.IFlagProvider");
    }

    public static Object A00(Callable callable) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        try {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.LAX);
            return callable.call();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public static AnonymousClass5Y2 asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.flags.IFlagProvider");
        return queryLocalInterface instanceof AnonymousClass5Y2 ? (AnonymousClass5Y2) queryLocalInterface : new C98424if(iBinder);
    }

    @Override // X.AbstractBinderC73303g0
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z;
        if (i != 1) {
            if (i == 2) {
                z = getBooleanFlagValue(parcel.readString(), C12960it.A1S(parcel.readInt()), parcel.readInt());
            } else if (i == 3) {
                z = getIntFlagValue(parcel.readString(), parcel.readInt(), parcel.readInt());
            } else if (i == 4) {
                long longFlagValue = getLongFlagValue(parcel.readString(), parcel.readLong(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeLong(longFlagValue);
                return true;
            } else if (i != 5) {
                return false;
            } else {
                String stringFlagValue = getStringFlagValue(parcel.readString(), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeString(stringFlagValue);
                return true;
            }
            parcel2.writeNoException();
            parcel2.writeInt(z ? 1 : 0);
            return true;
        }
        init(AbstractBinderC79573qo.A01(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }

    public boolean getBooleanFlagValue(String str, boolean z, int i) {
        Boolean bool;
        if (!this.zzu) {
            return z;
        }
        SharedPreferences sharedPreferences = this.zzv;
        Boolean valueOf = Boolean.valueOf(z);
        try {
            bool = (Boolean) A00(new CallableC112515Dr(sharedPreferences, valueOf, str));
        } catch (Exception e) {
            Log.w("FlagDataUtils", C72453ed.A0r("Flag value not available, returning default: ", e.getMessage()));
            bool = valueOf;
        }
        return bool.booleanValue();
    }

    public int getIntFlagValue(String str, int i, int i2) {
        Integer num;
        if (!this.zzu) {
            return i;
        }
        SharedPreferences sharedPreferences = this.zzv;
        Integer valueOf = Integer.valueOf(i);
        try {
            num = (Integer) A00(new CallableC112525Ds(sharedPreferences, valueOf, str));
        } catch (Exception e) {
            Log.w("FlagDataUtils", C72453ed.A0r("Flag value not available, returning default: ", e.getMessage()));
            num = valueOf;
        }
        return num.intValue();
    }

    public long getLongFlagValue(String str, long j, int i) {
        Long l;
        if (!this.zzu) {
            return j;
        }
        SharedPreferences sharedPreferences = this.zzv;
        Long valueOf = Long.valueOf(j);
        try {
            l = (Long) A00(new CallableC112535Dt(sharedPreferences, valueOf, str));
        } catch (Exception e) {
            Log.w("FlagDataUtils", C72453ed.A0r("Flag value not available, returning default: ", e.getMessage()));
            l = valueOf;
        }
        return l.longValue();
    }

    public String getStringFlagValue(String str, String str2, int i) {
        if (!this.zzu) {
            return str2;
        }
        try {
            return (String) A00(new CallableC112545Du(this.zzv, str, str2));
        } catch (Exception e) {
            Log.w("FlagDataUtils", C72453ed.A0r("Flag value not available, returning default: ", e.getMessage()));
            return str2;
        }
    }

    public void init(IObjectWrapper iObjectWrapper) {
        SharedPreferences sharedPreferences;
        Context context = (Context) BinderC56502l7.A00(iObjectWrapper);
        if (!this.zzu) {
            try {
                Context createPackageContext = context.createPackageContext("com.google.android.gms", 0);
                synchronized (SharedPreferences.class) {
                    sharedPreferences = C87684Cm.A00;
                    if (sharedPreferences == null) {
                        sharedPreferences = (SharedPreferences) A00(new AnonymousClass5DV(createPackageContext));
                        C87684Cm.A00 = sharedPreferences;
                    }
                }
                this.zzv = sharedPreferences;
                this.zzu = true;
            } catch (PackageManager.NameNotFoundException unused) {
            } catch (Exception e) {
                Log.w("FlagProviderImpl", C72453ed.A0r("Could not retrieve sdk flags, continuing with defaults: ", e.getMessage()));
            }
        }
    }
}
