package com.google.android.gms.wearable;

import X.AnonymousClass1U5;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13300jT;
import X.C72453ed;
import X.C95654e8;
import X.C99534kS;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes3.dex */
public class ConnectionConfiguration extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C99534kS();
    public String A00;
    public String A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final String A05;
    public final String A06;
    public final boolean A07;
    public volatile String A08;
    public volatile boolean A09;

    public ConnectionConfiguration(String str, String str2, String str3, String str4, String str5, int i, int i2, boolean z, boolean z2, boolean z3) {
        this.A05 = str;
        this.A06 = str2;
        this.A03 = i;
        this.A04 = i2;
        this.A07 = z;
        this.A09 = z2;
        this.A08 = str3;
        this.A02 = z3;
        this.A00 = str4;
        this.A01 = str5;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj instanceof ConnectionConfiguration) {
            ConnectionConfiguration connectionConfiguration = (ConnectionConfiguration) obj;
            if (C13300jT.A00(this.A05, connectionConfiguration.A05) && C13300jT.A00(this.A06, connectionConfiguration.A06) && C13300jT.A00(Integer.valueOf(this.A03), Integer.valueOf(connectionConfiguration.A03)) && C13300jT.A00(Integer.valueOf(this.A04), Integer.valueOf(connectionConfiguration.A04)) && C13300jT.A00(Boolean.valueOf(this.A07), Boolean.valueOf(connectionConfiguration.A07)) && C13300jT.A00(Boolean.valueOf(this.A02), Boolean.valueOf(connectionConfiguration.A02))) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        Object[] objArr = new Object[6];
        objArr[0] = this.A05;
        objArr[1] = this.A06;
        C12990iw.A1V(objArr, this.A03);
        objArr[3] = Integer.valueOf(this.A04);
        objArr[4] = Boolean.valueOf(this.A07);
        return C12980iv.A0B(Boolean.valueOf(this.A02), objArr, 5);
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder A0k = C12960it.A0k("ConnectionConfiguration[ ");
        String valueOf = String.valueOf(this.A05);
        A0k.append(C72453ed.A0s("Name=", valueOf, valueOf.length()));
        A0k.append(C12960it.A0c(String.valueOf(this.A06), ", Address="));
        A0k.append(C12960it.A0e(", Type=", C12980iv.A0t(18), this.A03));
        A0k.append(C12960it.A0e(", Role=", C12980iv.A0t(18), this.A04));
        boolean z = this.A07;
        StringBuilder A0t = C12980iv.A0t(15);
        A0t.append(", Enabled=");
        A0t.append(z);
        C12970iu.A1V(A0t, A0k);
        boolean z2 = this.A09;
        StringBuilder A0t2 = C12980iv.A0t(19);
        A0t2.append(", IsConnected=");
        A0t2.append(z2);
        C12970iu.A1V(A0t2, A0k);
        A0k.append(C12960it.A0c(String.valueOf(this.A08), ", PeerNodeId="));
        boolean z3 = this.A02;
        StringBuilder A0t3 = C12980iv.A0t(20);
        A0t3.append(", BtlePriority=");
        A0t3.append(z3);
        C12970iu.A1V(A0t3, A0k);
        A0k.append(C12960it.A0c(String.valueOf(this.A00), ", NodeId="));
        A0k.append(C12960it.A0c(String.valueOf(this.A01), ", PackageName="));
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        boolean A0K = C95654e8.A0K(parcel, this.A05);
        C95654e8.A0D(parcel, this.A06, 3, A0K);
        C95654e8.A07(parcel, 4, this.A03);
        C95654e8.A07(parcel, 5, this.A04);
        C95654e8.A09(parcel, 6, this.A07);
        C95654e8.A09(parcel, 7, this.A09);
        C95654e8.A0D(parcel, this.A08, 8, A0K);
        C95654e8.A09(parcel, 9, this.A02);
        C95654e8.A0D(parcel, this.A00, 10, A0K);
        C95654e8.A0D(parcel, this.A01, 11, A0K);
        C95654e8.A06(parcel, A00);
    }
}
