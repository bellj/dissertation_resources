package com.google.android.gms.common.api.internal;

import X.AbstractC116815Wz;
import X.AnonymousClass4IO;
import X.C12960it;
import X.C65863Lh;
import X.DialogInterface$OnCancelListenerC56312kg;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class LifecycleCallback {
    public final AbstractC116815Wz A00;

    public LifecycleCallback(AbstractC116815Wz r1) {
        this.A00 = r1;
    }

    public void A04(Bundle bundle) {
    }

    public void A05(Bundle bundle) {
    }

    public void A00() {
        if (this instanceof DialogInterface$OnCancelListenerC56312kg) {
            DialogInterface$OnCancelListenerC56312kg r1 = (DialogInterface$OnCancelListenerC56312kg) this;
            if (!r1.A01.isEmpty()) {
                r1.A03.A05(r1);
            }
        }
    }

    public void A01() {
        if (this instanceof DialogInterface$OnCancelListenerC56312kg) {
            DialogInterface$OnCancelListenerC56312kg r1 = (DialogInterface$OnCancelListenerC56312kg) this;
            r1.A05 = true;
            if (!r1.A01.isEmpty()) {
                r1.A03.A05(r1);
            }
        }
    }

    public void A02() {
        if (this instanceof DialogInterface$OnCancelListenerC56312kg) {
            DialogInterface$OnCancelListenerC56312kg r3 = (DialogInterface$OnCancelListenerC56312kg) this;
            r3.A05 = false;
            C65863Lh r2 = r3.A03;
            synchronized (C65863Lh.A0I) {
                if (r2.A01 == r3) {
                    r2.A01 = null;
                    r2.A0A.clear();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(int r9, int r10, android.content.Intent r11) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.DialogInterface$OnCancelListenerC56312kg
            if (r0 == 0) goto L_0x0035
            r5 = r8
            X.2kg r5 = (X.DialogInterface$OnCancelListenerC56312kg) r5
            java.util.concurrent.atomic.AtomicReference r4 = r5.A04
            java.lang.Object r6 = r4.get()
            X.4MG r6 = (X.AnonymousClass4MG) r6
            r0 = 1
            if (r9 == r0) goto L_0x0036
            r0 = 2
            if (r9 != r0) goto L_0x005f
            X.29i r2 = r5.A02
            X.5Wz r0 = r5.A00
            android.app.Activity r1 = r0.ADs()
            X.C13020j0.A01(r1)
            r0 = 12451000(0xbdfcb8, float:1.7447567E-38)
            int r2 = r2.A00(r1, r0)
            if (r2 == 0) goto L_0x006a
            if (r6 == 0) goto L_0x0035
            X.2ky r0 = r6.A01
            int r1 = r0.A01
            r0 = 18
            if (r1 != r0) goto L_0x0061
            if (r2 != r0) goto L_0x0061
        L_0x0035:
            return
        L_0x0036:
            r0 = -1
            if (r10 == r0) goto L_0x006a
            if (r10 != 0) goto L_0x005f
            if (r6 == 0) goto L_0x0035
            r3 = 13
            if (r11 == 0) goto L_0x0047
            java.lang.String r0 = "<<ResolutionFailureErrorDetail>>"
            int r3 = r11.getIntExtra(r0, r3)
        L_0x0047:
            r2 = 0
            X.2ky r0 = r6.A01
            java.lang.String r1 = r0.toString()
            r0 = 1
            X.2ky r7 = new X.2ky
            r7.<init>(r2, r1, r0, r3)
            int r1 = r6.A00
            r4.set(r2)
        L_0x0059:
            X.3Lh r0 = r5.A03
            r0.A04(r7, r1)
            return
        L_0x005f:
            if (r6 == 0) goto L_0x0035
        L_0x0061:
            X.2ky r7 = r6.A01
            int r1 = r6.A00
            r0 = 0
            r4.set(r0)
            goto L_0x0059
        L_0x006a:
            r0 = 0
            r4.set(r0)
            X.3Lh r0 = r5.A03
            android.os.Handler r1 = r0.A06
            r0 = 3
            android.os.Message r0 = r1.obtainMessage(r0)
            r1.sendMessage(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.LifecycleCallback.A03(int, int, android.content.Intent):void");
    }

    public static AbstractC116815Wz getChimeraLifecycleFragmentImpl(AnonymousClass4IO r0) {
        throw C12960it.A0U("Method not available in SDK.");
    }
}
