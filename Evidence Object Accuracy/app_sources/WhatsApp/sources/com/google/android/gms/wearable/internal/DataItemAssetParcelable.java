package com.google.android.gms.wearable.internal;

import X.AnonymousClass1U5;
import X.AnonymousClass5R2;
import X.C12960it;
import X.C13020j0;
import X.C95654e8;
import X.C99244jz;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.ReflectedParcelable;

@KeepName
/* loaded from: classes3.dex */
public class DataItemAssetParcelable extends AnonymousClass1U5 implements ReflectedParcelable, AnonymousClass5R2 {
    public static final Parcelable.Creator CREATOR = new C99244jz();
    public final String A00;
    public final String A01;

    public DataItemAssetParcelable(AnonymousClass5R2 r2) {
        DataItemAssetParcelable dataItemAssetParcelable = (DataItemAssetParcelable) r2;
        String str = dataItemAssetParcelable.A00;
        C13020j0.A01(str);
        this.A00 = str;
        String str2 = dataItemAssetParcelable.A01;
        C13020j0.A01(str2);
        this.A01 = str2;
    }

    public DataItemAssetParcelable(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder A0k = C12960it.A0k("DataItemAssetParcelable[@");
        A0k.append(Integer.toHexString(hashCode()));
        String str = this.A00;
        if (str == null) {
            A0k.append(",noid");
        } else {
            A0k.append(",");
            A0k.append(str);
        }
        A0k.append(", key=");
        A0k.append(this.A01);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0D(parcel, this.A01, 3, C95654e8.A0K(parcel, this.A00));
        C95654e8.A06(parcel, A00);
    }
}
