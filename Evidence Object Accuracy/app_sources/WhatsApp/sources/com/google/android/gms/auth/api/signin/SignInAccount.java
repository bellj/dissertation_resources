package com.google.android.gms.auth.api.signin;

import X.AnonymousClass1U5;
import X.C13020j0;
import X.C95654e8;
import X.C98614iy;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes2.dex */
public class SignInAccount extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98614iy();
    public GoogleSignInAccount A00;
    @Deprecated
    public String A01;
    @Deprecated
    public String A02;

    public SignInAccount(GoogleSignInAccount googleSignInAccount, String str, String str2) {
        this.A00 = googleSignInAccount;
        C13020j0.A07(str, "8.3 and 8.4 SDKs require non-null email");
        this.A01 = str;
        C13020j0.A07(str2, "8.3 and 8.4 SDKs require non-null userId");
        this.A02 = str2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0D(parcel, this.A01, 4, false);
        C95654e8.A0B(parcel, this.A00, 7, i, false);
        C95654e8.A0D(parcel, this.A02, 8, false);
        C95654e8.A06(parcel, A01);
    }
}
