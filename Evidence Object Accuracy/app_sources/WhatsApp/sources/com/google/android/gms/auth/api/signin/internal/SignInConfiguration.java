package com.google.android.gms.auth.api.signin.internal;

import X.AnonymousClass1U5;
import X.AnonymousClass4Un;
import X.C13020j0;
import X.C95654e8;
import X.C98584iv;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.ReflectedParcelable;

/* loaded from: classes2.dex */
public final class SignInConfiguration extends AnonymousClass1U5 implements ReflectedParcelable {
    public static final Parcelable.Creator CREATOR = new C98584iv();
    public GoogleSignInOptions A00;
    public final String A01;

    public SignInConfiguration(GoogleSignInOptions googleSignInOptions, String str) {
        C13020j0.A05(str);
        this.A01 = str;
        this.A00 = googleSignInOptions;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj instanceof SignInConfiguration) {
            SignInConfiguration signInConfiguration = (SignInConfiguration) obj;
            if (this.A01.equals(signInConfiguration.A01)) {
                GoogleSignInOptions googleSignInOptions = this.A00;
                GoogleSignInOptions googleSignInOptions2 = signInConfiguration.A00;
                if (googleSignInOptions == null) {
                    if (googleSignInOptions2 == null) {
                        return true;
                    }
                } else if (googleSignInOptions.equals(googleSignInOptions2)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        AnonymousClass4Un r1 = new AnonymousClass4Un();
        r1.A00(this.A01);
        r1.A00(this.A00);
        return r1.A00;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0D(parcel, this.A01, 2, false);
        C95654e8.A0B(parcel, this.A00, 5, i, false);
        C95654e8.A06(parcel, A01);
    }
}
