package com.google.android.gms.analytics;

import X.C12990iw;
import X.C14160kx;
import X.C15060mU;
import X.C15110ma;
import X.C15130mc;
import X.C56582lF;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/* loaded from: classes2.dex */
public final class AnalyticsReceiver extends BroadcastReceiver {
    public C15060mU A00;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (this.A00 == null) {
            this.A00 = new C15060mU();
        }
        C56582lF r4 = C14160kx.A00(context).A0C;
        C14160kx.A01(r4);
        if (intent == null) {
            r4.A0A("AnalyticsReceiver called with null intent");
            return;
        }
        String action = intent.getAction();
        r4.A0D("Local AnalyticsReceiver got", action);
        if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(action)) {
            boolean A00 = C15110ma.A00(context);
            Intent A0E = C12990iw.A0E("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
            A0E.setComponent(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsService"));
            A0E.setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
            synchronized (C15060mU.A02) {
                context.startService(A0E);
                if (A00) {
                    try {
                        if (C15060mU.A00 == null) {
                            C15130mc r2 = new C15130mc(context, "Analytics WakeLock");
                            C15060mU.A00 = r2;
                            synchronized (r2.A0B) {
                                r2.A08 = false;
                            }
                        }
                        C15060mU.A00.A02(1000);
                    } catch (SecurityException unused) {
                        r4.A0A("Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
                    }
                }
            }
        }
    }
}
