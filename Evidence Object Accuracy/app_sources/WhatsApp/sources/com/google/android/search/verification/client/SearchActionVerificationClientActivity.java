package com.google.android.search.verification.client;

import X.C12990iw;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.whatsapp.VoiceMessagingService;

/* loaded from: classes2.dex */
public abstract class SearchActionVerificationClientActivity extends Activity {
    public abstract Class getServiceClass();

    @Override // android.app.Activity
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent A0D = C12990iw.A0D(this, VoiceMessagingService.class);
        A0D.putExtra(SearchActionVerificationClientService.EXTRA_INTENT, getIntent());
        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundService(A0D);
        } else {
            startService(A0D);
        }
        finish();
    }
}
