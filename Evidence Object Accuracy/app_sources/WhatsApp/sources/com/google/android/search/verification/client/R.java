package com.google.android.search.verification.client;

/* loaded from: classes3.dex */
public final class R {

    /* loaded from: classes3.dex */
    public final class attr {
        public static int alpha = 0x7f010001;
        public static int coordinatorLayoutStyle = 0x7f010000;
        public static int font = 0x7f010011;
        public static int fontProviderAuthority = 0x7f01000a;
        public static int fontProviderCerts = 0x7f01000d;
        public static int fontProviderFetchStrategy = 0x7f01000e;
        public static int fontProviderFetchTimeout = 0x7f01000f;
        public static int fontProviderPackage = 0x7f01000b;
        public static int fontProviderQuery = 0x7f01000c;
        public static int fontStyle = 0x7f010010;
        public static int fontVariationSettings = 0x7f010013;
        public static int fontWeight = 0x7f010012;
        public static int keylines = 0x7f010002;
        public static int layout_anchor = 0x7f010005;
        public static int layout_anchorGravity = 0x7f010007;
        public static int layout_behavior = 0x7f010004;
        public static int layout_dodgeInsetEdges = 0x7f010009;
        public static int layout_insetEdge = 0x7f010008;
        public static int layout_keyline = 0x7f010006;
        public static int statusBarBackground = 0x7f010003;
        public static int ttcIndex = 0x7f010014;
    }

    /* loaded from: classes3.dex */
    public final class color {
        public static int notification_action_color_filter = 0x7f040000;
        public static int notification_icon_bg_color = 0x7f040001;
        public static int notification_material_background_media_default_color = 0x7f040002;
        public static int primary_text_default_material_dark = 0x7f040003;
        public static int ripple_material_light = 0x7f040004;
        public static int secondary_text_default_material_dark = 0x7f040005;
        public static int secondary_text_default_material_light = 0x7f040006;
    }

    /* loaded from: classes3.dex */
    public final class dimen {
        public static int compat_button_inset_horizontal_material = 0x7f050000;
        public static int compat_button_inset_vertical_material = 0x7f050001;
        public static int compat_button_padding_horizontal_material = 0x7f050002;
        public static int compat_button_padding_vertical_material = 0x7f050003;
        public static int compat_control_corner_material = 0x7f050004;
        public static int compat_notification_large_icon_max_height = 0x7f050005;
        public static int compat_notification_large_icon_max_width = 0x7f050006;
        public static int notification_action_icon_size = 0x7f050007;
        public static int notification_action_text_size = 0x7f050008;
        public static int notification_big_circle_margin = 0x7f050009;
        public static int notification_content_margin_start = 0x7f05000a;
        public static int notification_large_icon_height = 0x7f05000b;
        public static int notification_large_icon_width = 0x7f05000c;
        public static int notification_main_column_padding_top = 0x7f05000d;
        public static int notification_media_narrow_margin = 0x7f05000e;
        public static int notification_right_icon_size = 0x7f05000f;
        public static int notification_right_side_padding_top = 0x7f050010;
        public static int notification_small_icon_background_padding = 0x7f050011;
        public static int notification_small_icon_size_as_large = 0x7f050012;
        public static int notification_subtext_size = 0x7f050013;
        public static int notification_top_pad = 0x7f050014;
        public static int notification_top_pad_large_text = 0x7f050015;
        public static int subtitle_corner_radius = 0x7f050016;
        public static int subtitle_outline_width = 0x7f050017;
        public static int subtitle_shadow_offset = 0x7f050018;
        public static int subtitle_shadow_radius = 0x7f050019;
    }

    /* loaded from: classes3.dex */
    public final class drawable {
        public static int notification_action_background = 0x7f020000;
        public static int notification_bg = 0x7f020001;
        public static int notification_bg_low = 0x7f020002;
        public static int notification_bg_low_normal = 0x7f020003;
        public static int notification_bg_low_pressed = 0x7f020004;
        public static int notification_bg_normal = 0x7f020005;
        public static int notification_bg_normal_pressed = 0x7f020006;
        public static int notification_icon_background = 0x7f020007;
        public static int notification_template_icon_bg = 0x7f020008;
        public static int notification_template_icon_low_bg = 0x7f020009;
        public static int notification_tile_bg = 0x7f02000a;
        public static int notify_panel_notification_icon_bg = 0x7f02000b;
    }

    /* loaded from: classes3.dex */
    public final class id {
        public static int action0 = 0x7f060000;
        public static int action_container = 0x7f060001;
        public static int action_divider = 0x7f060002;
        public static int action_image = 0x7f060003;
        public static int action_text = 0x7f060004;
        public static int actions = 0x7f060005;
        public static int all = 0x7f060006;
        public static int async = 0x7f060007;
        public static int blocking = 0x7f060008;
        public static int bottom = 0x7f060009;
        public static int cancel_action = 0x7f06000a;
        public static int center = 0x7f06000b;
        public static int center_horizontal = 0x7f06000c;
        public static int center_vertical = 0x7f06000d;
        public static int chronometer = 0x7f06000e;
        public static int clip_horizontal = 0x7f06000f;
        public static int clip_vertical = 0x7f060010;
        public static int end = 0x7f060011;
        public static int end_padder = 0x7f060012;
        public static int fill = 0x7f060013;
        public static int fill_horizontal = 0x7f060014;
        public static int fill_vertical = 0x7f060015;
        public static int forever = 0x7f060016;
        public static int icon = 0x7f060017;
        public static int icon_group = 0x7f060018;
        public static int info = 0x7f060019;
        public static int italic = 0x7f06001a;
        public static int left = 0x7f06001b;
        public static int line1 = 0x7f06001c;
        public static int line3 = 0x7f06001d;
        public static int media_actions = 0x7f06001e;
        public static int none = 0x7f06001f;
        public static int normal = 0x7f060020;
        public static int notification_background = 0x7f060021;
        public static int notification_main_column = 0x7f060022;
        public static int notification_main_column_container = 0x7f060023;
        public static int right = 0x7f060024;
        public static int right_icon = 0x7f060025;
        public static int right_side = 0x7f060026;
        public static int start = 0x7f060027;
        public static int status_bar_latest_event_content = 0x7f060028;
        public static int tag_accessibility_heading = 0x7f060029;
        public static int tag_accessibility_pane_title = 0x7f06002a;
        public static int tag_screen_reader_focusable = 0x7f06002b;
        public static int tag_transition_group = 0x7f06002c;
        public static int tag_unhandled_key_event_manager = 0x7f06002d;
        public static int tag_unhandled_key_listeners = 0x7f06002e;
        public static int text = 0x7f06002f;
        public static int text2 = 0x7f060030;
        public static int time = 0x7f060031;
        public static int title = 0x7f060032;
        public static int top = 0x7f060033;
    }

    /* loaded from: classes3.dex */
    public final class integer {
        public static int cancel_button_image_alpha = 0x7f070000;
        public static int status_bar_notification_info_maxnum = 0x7f070001;
    }

    /* loaded from: classes3.dex */
    public final class layout {
        public static int notification_action = 0x7f030000;
        public static int notification_action_tombstone = 0x7f030001;
        public static int notification_media_action = 0x7f030002;
        public static int notification_media_cancel_action = 0x7f030003;
        public static int notification_template_big_media = 0x7f030004;
        public static int notification_template_big_media_custom = 0x7f030005;
        public static int notification_template_big_media_narrow = 0x7f030006;
        public static int notification_template_big_media_narrow_custom = 0x7f030007;
        public static int notification_template_custom_big = 0x7f030008;
        public static int notification_template_icon_group = 0x7f030009;
        public static int notification_template_lines_media = 0x7f03000a;
        public static int notification_template_media = 0x7f03000b;
        public static int notification_template_media_custom = 0x7f03000c;
        public static int notification_template_part_chronometer = 0x7f03000d;
        public static int notification_template_part_time = 0x7f03000e;
    }

    /* loaded from: classes3.dex */
    public final class string {
        public static int google_assistant_verification_channel_name = 0x7f080000;
        public static int google_assistant_verification_notification_title = 0x7f080001;
        public static int status_bar_notification_info_overflow = 0x7f080002;
    }

    /* loaded from: classes3.dex */
    public final class style {
        public static int TextAppearance_Compat_Notification = 0x7f090000;
        public static int TextAppearance_Compat_Notification_Info = 0x7f090001;
        public static int TextAppearance_Compat_Notification_Info_Media = 0x7f090002;
        public static int TextAppearance_Compat_Notification_Line2 = 0x7f090003;
        public static int TextAppearance_Compat_Notification_Line2_Media = 0x7f090004;
        public static int TextAppearance_Compat_Notification_Media = 0x7f090005;
        public static int TextAppearance_Compat_Notification_Time = 0x7f090006;
        public static int TextAppearance_Compat_Notification_Time_Media = 0x7f090007;
        public static int TextAppearance_Compat_Notification_Title = 0x7f090008;
        public static int TextAppearance_Compat_Notification_Title_Media = 0x7f090009;
        public static int Widget_Compat_NotificationActionContainer = 0x7f09000a;
        public static int Widget_Compat_NotificationActionText = 0x7f09000b;
        public static int Widget_Support_CoordinatorLayout = 0x7f09000c;
    }

    /* loaded from: classes3.dex */
    public final class styleable {
        public static int[] ColorStateListItem = {16843173, 16843551, com.whatsapp.R.anim.abc_fade_out};
        public static int ColorStateListItem_alpha = 0x00000002;
        public static int ColorStateListItem_android_alpha = 0x00000001;
        public static int ColorStateListItem_android_color = 0x00000000;
        public static int[] CoordinatorLayout = {com.whatsapp.R.anim.abc_grow_fade_in_from_bottom, com.whatsapp.R.anim.abc_popup_enter};
        public static int[] CoordinatorLayout_Layout = {16842931, com.whatsapp.R.anim.abc_popup_exit, com.whatsapp.R.anim.abc_shrink_fade_out_from_bottom, com.whatsapp.R.anim.abc_slide_in_bottom, com.whatsapp.R.anim.abc_slide_in_top, com.whatsapp.R.anim.abc_slide_out_bottom, com.whatsapp.R.anim.abc_slide_out_top};
        public static int CoordinatorLayout_Layout_android_layout_gravity = 0x00000000;
        public static int CoordinatorLayout_Layout_layout_anchor = 0x00000002;
        public static int CoordinatorLayout_Layout_layout_anchorGravity = 0x00000004;
        public static int CoordinatorLayout_Layout_layout_behavior = 0x00000001;
        public static int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 0x00000006;
        public static int CoordinatorLayout_Layout_layout_insetEdge = 0x00000005;
        public static int CoordinatorLayout_Layout_layout_keyline = 0x00000003;
        public static int CoordinatorLayout_keylines = 0x00000000;
        public static int CoordinatorLayout_statusBarBackground = 0x00000001;
        public static int[] FontFamily = {com.whatsapp.R.anim.abc_tooltip_enter, com.whatsapp.R.anim.abc_tooltip_exit, com.whatsapp.R.anim.bottom_down, com.whatsapp.R.anim.bottom_down_fast, com.whatsapp.R.anim.bottom_down_short, com.whatsapp.R.anim.bottom_up};
        public static int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.whatsapp.R.anim.bottom_up_fast, com.whatsapp.R.anim.bottom_up_short, com.whatsapp.R.anim.btn_checkbox_to_checked_box_inner_merged_animation, com.whatsapp.R.anim.btn_checkbox_to_checked_box_outer_merged_animation, com.whatsapp.R.anim.btn_checkbox_to_checked_icon_null_animation};
        public static int FontFamilyFont_android_font = 0x00000000;
        public static int FontFamilyFont_android_fontStyle = 0x00000002;
        public static int FontFamilyFont_android_fontVariationSettings = 0x00000004;
        public static int FontFamilyFont_android_fontWeight = 0x00000001;
        public static int FontFamilyFont_android_ttcIndex = 0x00000003;
        public static int FontFamilyFont_font = 0x00000006;
        public static int FontFamilyFont_fontStyle = 0x00000005;
        public static int FontFamilyFont_fontVariationSettings = 0x00000008;
        public static int FontFamilyFont_fontWeight = 0x00000007;
        public static int FontFamilyFont_ttcIndex = 0x00000009;
        public static int FontFamily_fontProviderAuthority = 0x00000000;
        public static int FontFamily_fontProviderCerts = 0x00000003;
        public static int FontFamily_fontProviderFetchStrategy = 0x00000004;
        public static int FontFamily_fontProviderFetchTimeout = 0x00000005;
        public static int FontFamily_fontProviderPackage = 0x00000001;
        public static int FontFamily_fontProviderQuery = 0x00000002;
        public static int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static int[] GradientColorItem = {16843173, 16844052};
        public static int GradientColorItem_android_color = 0x00000000;
        public static int GradientColorItem_android_offset = 0x00000001;
        public static int GradientColor_android_centerColor = 0x00000007;
        public static int GradientColor_android_centerX = 0x00000003;
        public static int GradientColor_android_centerY = 0x00000004;
        public static int GradientColor_android_endColor = 0x00000001;
        public static int GradientColor_android_endX = 0x0000000a;
        public static int GradientColor_android_endY = 0x0000000b;
        public static int GradientColor_android_gradientRadius = 0x00000005;
        public static int GradientColor_android_startColor = 0x00000000;
        public static int GradientColor_android_startX = 0x00000008;
        public static int GradientColor_android_startY = 0x00000009;
        public static int GradientColor_android_tileMode = 0x00000006;
        public static int GradientColor_android_type = 0x00000002;
    }
}
