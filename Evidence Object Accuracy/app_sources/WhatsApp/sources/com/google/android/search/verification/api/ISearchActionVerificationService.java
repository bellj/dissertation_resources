package com.google.android.search.verification.api;

import X.AnonymousClass5Pp;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* loaded from: classes3.dex */
public interface ISearchActionVerificationService extends IInterface {
    int getVersion();

    boolean isSearchAction(Intent intent, Bundle bundle);

    /* loaded from: classes2.dex */
    public abstract class Stub extends Binder implements IInterface, ISearchActionVerificationService {
        public static final String DESCRIPTOR = "com.google.android.search.verification.api.ISearchActionVerificationService";
        public static final int TRANSACTION_getVersion = 2;
        public static final int TRANSACTION_isSearchAction = 1;
        public static AnonymousClass5Pp globalInterceptor;

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        public Stub() {
            this(DESCRIPTOR);
        }

        public Stub(String str) {
            attachInterface(this, str);
        }

        public static ISearchActionVerificationService asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface instanceof ISearchActionVerificationService) {
                return (ISearchActionVerificationService) queryLocalInterface;
            }
            return new Proxy(iBinder);
        }

        public boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) {
            boolean z;
            if (i == 1) {
                z = isSearchAction((Intent) C12970iu.A0F(parcel, Intent.CREATOR), (Bundle) C12970iu.A0F(parcel, Bundle.CREATOR));
            } else if (i != 2) {
                return false;
            } else {
                z = getVersion();
            }
            parcel2.writeNoException();
            parcel2.writeInt(z ? 1 : 0);
            return true;
        }

        public static synchronized void installTransactionInterceptorPackagePrivate(AnonymousClass5Pp r2) {
            synchronized (Stub.class) {
                if (r2 != null) {
                    globalInterceptor = r2;
                } else {
                    throw C12970iu.A0f("null interceptor");
                }
            }
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (routeToSuperOrEnforceInterface(i, parcel, parcel2, i2)) {
                return true;
            }
            return dispatchTransaction(i, parcel, parcel2, i2);
        }

        public boolean routeToSuperOrEnforceInterface(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i > 16777215) {
                return super.onTransact(i, parcel, parcel2, i2);
            }
            parcel.enforceInterface(getInterfaceDescriptor());
            return false;
        }

        /* loaded from: classes2.dex */
        public class Proxy implements IInterface, ISearchActionVerificationService {
            public final String mDescriptor;
            public final IBinder mRemote;

            public Proxy(IBinder iBinder) {
                this(iBinder, Stub.DESCRIPTOR);
            }

            public Proxy(IBinder iBinder, String str) {
                this.mRemote = iBinder;
                this.mDescriptor = str;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.mRemote;
            }

            @Override // com.google.android.search.verification.api.ISearchActionVerificationService
            public int getVersion() {
                Parcel transactAndReadException = transactAndReadException(2, obtainAndWriteInterfaceToken());
                int readInt = transactAndReadException.readInt();
                transactAndReadException.recycle();
                return readInt;
            }

            @Override // com.google.android.search.verification.api.ISearchActionVerificationService
            public boolean isSearchAction(Intent intent, Bundle bundle) {
                Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
                if (intent == null) {
                    obtainAndWriteInterfaceToken.writeInt(0);
                } else {
                    obtainAndWriteInterfaceToken.writeInt(1);
                    intent.writeToParcel(obtainAndWriteInterfaceToken, 0);
                }
                if (bundle == null) {
                    obtainAndWriteInterfaceToken.writeInt(0);
                } else {
                    obtainAndWriteInterfaceToken.writeInt(1);
                    bundle.writeToParcel(obtainAndWriteInterfaceToken, 0);
                }
                Parcel transactAndReadException = transactAndReadException(1, obtainAndWriteInterfaceToken);
                boolean A1S = C12960it.A1S(transactAndReadException.readInt());
                transactAndReadException.recycle();
                return A1S;
            }

            public Parcel obtainAndWriteInterfaceToken() {
                Parcel obtain = Parcel.obtain();
                obtain.writeInterfaceToken(this.mDescriptor);
                return obtain;
            }

            public Parcel transactAndReadException(int i, Parcel parcel) {
                try {
                    parcel = Parcel.obtain();
                    C12990iw.A18(this.mRemote, parcel, parcel, i);
                    return parcel;
                } catch (RuntimeException e) {
                    throw e;
                } finally {
                    parcel.recycle();
                }
            }

            public void transactAndReadExceptionReturnVoid(int i, Parcel parcel) {
                Parcel obtain = Parcel.obtain();
                try {
                    C12990iw.A18(this.mRemote, parcel, obtain, i);
                } finally {
                    parcel.recycle();
                    obtain.recycle();
                }
            }

            public void transactOneway(int i, Parcel parcel) {
                try {
                    this.mRemote.transact(i, parcel, null, 1);
                } finally {
                    parcel.recycle();
                }
            }
        }
    }
}
