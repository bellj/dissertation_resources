package com.google.android.search.verification.client;

import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.search.verification.api.ISearchActionVerificationService;

/* loaded from: classes2.dex */
public abstract class SearchActionVerificationClientService extends IntentService {
    public static final int CONNECTION_TIMEOUT_IN_MS = 1000;
    public static final String EXTRA_INTENT = "SearchActionVerificationClientExtraIntent";
    public static final long MS_TO_NS = 1000000;
    public static final String NOTIFICATION_CHANNEL_ID = "Assistant_verifier";
    public static final int NOTIFICATION_ID = 10000;
    public static final String REMOTE_ASSISTANT_GO_SERVICE_ACTION = "com.google.android.apps.assistant.go.verification.VERIFICATION_SERVICE";
    public static final String REMOTE_GSA_SERVICE_ACTION = "com.google.android.googlequicksearchbox.SEARCH_ACTION_VERIFICATION_SERVICE";
    public static final String SEND_MESSAGE_ERROR_MESSAGE = "com.google.android.voicesearch.extra.ERROR_MESSAGE";
    public static final String SEND_MESSAGE_RESULT_RECEIVER = "com.google.android.voicesearch.extra.SEND_MESSAGE_RESULT_RECEIVER";
    public static final String TAG = "SAVerificationClientS";
    public static final int TIME_TO_SLEEP_IN_MS = 50;
    public final Intent assistantGoServiceIntent = C12990iw.A0E(REMOTE_ASSISTANT_GO_SERVICE_ACTION).setPackage(SearchActionVerificationClientUtil.ASSISTANT_GO_PACKAGE);
    public SearchActionVerificationServiceConnection assistantGoVerificationServiceConnection;
    public final long connectionTimeout = 1000;
    public final boolean dbg = isDebugMode();
    public final Intent gsaServiceIntent = C12990iw.A0E(REMOTE_GSA_SERVICE_ACTION).setPackage(SearchActionVerificationClientUtil.SEARCH_APP_PACKAGE);
    public SearchActionVerificationServiceConnection searchActionVerificationServiceConnection;

    public long getConnectionTimeout() {
        return 1000;
    }

    public boolean isTestingMode() {
        return false;
    }

    public abstract void performAction(Intent intent, boolean z, Bundle bundle);

    public SearchActionVerificationClientService() {
        super("SearchActionVerificationClientService");
    }

    private void createChannel() {
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getApplicationContext().getResources().getString(R.string.google_assistant_verification_channel_name), 2);
        notificationChannel.enableVibration(false);
        notificationChannel.enableLights(false);
        notificationChannel.setShowBadge(false);
        ((NotificationManager) getApplicationContext().getSystemService(NotificationManager.class)).createNotificationChannel(notificationChannel);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0050, code lost:
        if (X.C12960it.A1W(r10.assistantGoVerificationServiceConnection.iRemoteService) != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (X.C12960it.A1W(r10.searchActionVerificationServiceConnection.iRemoteService) != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean installedServicesConnected() {
        /*
            r10 = this;
            java.lang.String r5 = "com.google.android.googlequicksearchbox"
            boolean r3 = r10.isPackageInstalled(r5)
            r9 = 0
            r8 = 1
            if (r3 == 0) goto L_0x0015
            com.google.android.search.verification.client.SearchActionVerificationClientService$SearchActionVerificationServiceConnection r0 = r10.searchActionVerificationServiceConnection
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r0.iRemoteService
            boolean r0 = X.C12960it.A1W(r0)
            r7 = 0
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r7 = 1
        L_0x0016:
            boolean r0 = r10.dbg
            r6 = 2
            r1 = 3
            java.lang.String r4 = "SAVerificationClientS"
            if (r0 == 0) goto L_0x003f
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r2[r9] = r5
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            r2[r8] = r0
            com.google.android.search.verification.client.SearchActionVerificationClientService$SearchActionVerificationServiceConnection r0 = r10.searchActionVerificationServiceConnection
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r0.iRemoteService
            boolean r0 = X.C12960it.A1W(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2[r6] = r0
            java.lang.String r0 = "GSA app %s installed: %s connected %s"
            java.lang.String r0 = java.lang.String.format(r0, r2)
            android.util.Log.d(r4, r0)
        L_0x003f:
            java.lang.String r5 = "com.google.android.apps.assistant"
            boolean r3 = r10.isPackageInstalled(r5)
            if (r3 == 0) goto L_0x0052
            com.google.android.search.verification.client.SearchActionVerificationClientService$SearchActionVerificationServiceConnection r0 = r10.assistantGoVerificationServiceConnection
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r0.iRemoteService
            boolean r0 = X.C12960it.A1W(r0)
            r2 = 0
            if (r0 == 0) goto L_0x0053
        L_0x0052:
            r2 = 1
        L_0x0053:
            boolean r0 = r10.dbg
            if (r0 == 0) goto L_0x0078
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r9] = r5
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            r1[r8] = r0
            com.google.android.search.verification.client.SearchActionVerificationClientService$SearchActionVerificationServiceConnection r0 = r10.assistantGoVerificationServiceConnection
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r0.iRemoteService
            boolean r0 = X.C12960it.A1W(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r1[r6] = r0
            java.lang.String r0 = "AssistantGo app %s installed: %s connected %s"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.d(r4, r0)
        L_0x0078:
            if (r7 == 0) goto L_0x007d
            if (r2 == 0) goto L_0x007d
            r9 = 1
        L_0x007d:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.search.verification.client.SearchActionVerificationClientService.installedServicesConnected():boolean");
    }

    private boolean isDebugMode() {
        return C12960it.A1T("user".equals(Build.TYPE) ? 1 : 0);
    }

    private boolean isPackageInstalled(String str) {
        ApplicationInfo applicationInfo;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(str, 0);
            if (packageInfo == null || (applicationInfo = packageInfo.applicationInfo) == null) {
                return false;
            }
            if (applicationInfo.enabled) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, String.format("Couldn't find package name %s", str), e);
            return false;
        }
    }

    private boolean isPackageSafe(String str) {
        if (isPackageInstalled(str)) {
            return isDebugMode() || SearchActionVerificationClientUtil.isPackageGoogleSigned(this, str);
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean maybePerformActionIfVerified(java.lang.String r9, android.content.Intent r10, com.google.android.search.verification.client.SearchActionVerificationClientService.SearchActionVerificationServiceConnection r11) {
        /*
            r8 = this;
            java.lang.String r0 = "com.google.android.googlequicksearchbox"
            boolean r0 = r9.equals(r0)
            r4 = 1
            java.lang.String r7 = "SAVerificationClientS"
            r3 = 0
            if (r0 != 0) goto L_0x0026
            java.lang.String r0 = "com.google.android.apps.assistant"
            boolean r0 = r9.equals(r0)
            if (r0 != 0) goto L_0x0026
            boolean r0 = r8.dbg
            if (r0 == 0) goto L_0x0025
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r1[r3] = r9
            java.lang.String r0 = "Unsupported package %s for verification."
        L_0x001e:
            java.lang.String r0 = java.lang.String.format(r0, r1)
        L_0x0022:
            android.util.Log.d(r7, r0)
        L_0x0025:
            return r3
        L_0x0026:
            boolean r0 = r8.isDebugMode()
            if (r0 != 0) goto L_0x003d
            boolean r0 = com.google.android.search.verification.client.SearchActionVerificationClientUtil.isPackageGoogleSigned(r8, r9)
            if (r0 != 0) goto L_0x003d
            boolean r0 = r8.dbg
            if (r0 == 0) goto L_0x0025
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r1[r3] = r9
            java.lang.String r0 = "Cannot verify the intent with package %s in unsafe mode."
            goto L_0x001e
        L_0x003d:
            java.lang.String r1 = "SearchActionVerificationClientExtraIntent"
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x0061
            boolean r0 = r8.dbg
            if (r0 == 0) goto L_0x0025
            java.lang.String r2 = java.lang.String.valueOf(r10)
            int r0 = r2.length()
            int r0 = r0 + 28
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "No extra, nothing to check: "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r2, r1)
            goto L_0x0022
        L_0x0061:
            android.os.Parcelable r5 = r10.getParcelableExtra(r1)
            android.content.Intent r5 = (android.content.Intent) r5
            boolean r0 = r8.dbg
            if (r0 == 0) goto L_0x006e
            com.google.android.search.verification.client.SearchActionVerificationClientUtil.logIntentWithExtras(r5)
        L_0x006e:
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r11.iRemoteService
            boolean r1 = X.C12960it.A1W(r0)
            r0 = 2
            java.lang.String r6 = "VerificationService is not connected to %s, unable to check intent: %s"
            if (r1 == 0) goto L_0x009f
            java.lang.String r2 = "%s Service API version: %s"
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            r1[r3] = r9     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            com.google.android.search.verification.api.ISearchActionVerificationService r0 = r11.iRemoteService     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            int r0 = r0.getVersion()     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            X.C12960it.A1P(r1, r0, r4)     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            java.lang.String r0 = java.lang.String.format(r2, r1)     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            android.util.Log.i(r7, r0)     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            android.os.Bundle r0 = X.C12970iu.A0D()     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            boolean r4 = r11.isVerified(r5, r0)     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            r8.performAction(r5, r4, r0)     // Catch: RemoteException -> 0x009b, Exception -> 0x00ad
            goto L_0x00c5
        L_0x009b:
            r2 = move-exception
            java.lang.String r1 = "Remote exception: "
            goto L_0x00b0
        L_0x009f:
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r3] = r9
            r0[r4] = r10
            java.lang.String r0 = java.lang.String.format(r6, r0)
            android.util.Log.e(r7, r0)
            goto L_0x00c3
        L_0x00ad:
            r2 = move-exception
            java.lang.String r1 = "Exception: "
        L_0x00b0:
            java.lang.String r0 = r2.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r0 = X.C12960it.A0c(r0, r1)
            android.util.Log.e(r7, r0)
            java.lang.String r6 = r2.getMessage()
        L_0x00c3:
            r4 = 0
            goto L_0x00c7
        L_0x00c5:
            java.lang.String r6 = ""
        L_0x00c7:
            java.lang.String r1 = "com.google.android.voicesearch.extra.SEND_MESSAGE_RESULT_RECEIVER"
            boolean r0 = r5.hasExtra(r1)
            if (r0 == 0) goto L_0x00e8
            android.os.Bundle r0 = r5.getExtras()
            android.os.Parcelable r2 = r0.getParcelable(r1)
            android.os.ResultReceiver r2 = (android.os.ResultReceiver) r2
            android.os.Bundle r1 = X.C12970iu.A0D()
            java.lang.String r0 = "com.google.android.voicesearch.extra.ERROR_MESSAGE"
            r1.putString(r0, r6)
            if (r4 != 0) goto L_0x00e5
            r3 = -1
        L_0x00e5:
            r2.send(r3, r1)
        L_0x00e8:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.search.verification.client.SearchActionVerificationClientService.maybePerformActionIfVerified(java.lang.String, android.content.Intent, com.google.android.search.verification.client.SearchActionVerificationClientService$SearchActionVerificationServiceConnection):boolean");
    }

    @Override // android.app.IntentService, android.app.Service
    public final void onCreate() {
        if (this.dbg) {
            Log.d(TAG, "onCreate");
        }
        super.onCreate();
        this.searchActionVerificationServiceConnection = new SearchActionVerificationServiceConnection();
        if (isPackageSafe(SearchActionVerificationClientUtil.SEARCH_APP_PACKAGE)) {
            bindService(this.gsaServiceIntent, this.searchActionVerificationServiceConnection, 1);
        }
        this.assistantGoVerificationServiceConnection = new SearchActionVerificationServiceConnection();
        if (isPackageSafe(SearchActionVerificationClientUtil.ASSISTANT_GO_PACKAGE)) {
            bindService(this.assistantGoServiceIntent, this.assistantGoVerificationServiceConnection, 1);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            postForegroundNotification();
        }
    }

    @Override // android.app.IntentService, android.app.Service
    public final void onDestroy() {
        if (this.dbg) {
            Log.d(TAG, "onDestroy");
        }
        super.onDestroy();
        SearchActionVerificationServiceConnection searchActionVerificationServiceConnection = this.searchActionVerificationServiceConnection;
        if (C12960it.A1W(searchActionVerificationServiceConnection.iRemoteService)) {
            unbindService(searchActionVerificationServiceConnection);
        }
        SearchActionVerificationServiceConnection searchActionVerificationServiceConnection2 = this.assistantGoVerificationServiceConnection;
        if (C12960it.A1W(searchActionVerificationServiceConnection2.iRemoteService)) {
            unbindService(searchActionVerificationServiceConnection2);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            stopForeground(true);
        }
    }

    @Override // android.app.IntentService
    public final void onHandleIntent(Intent intent) {
        String str;
        if (intent != null) {
            long nanoTime = System.nanoTime();
            while (!installedServicesConnected() && System.nanoTime() - nanoTime < this.connectionTimeout * MS_TO_NS) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    if (this.dbg) {
                        String valueOf = String.valueOf(e);
                        StringBuilder A0t = C12980iv.A0t(valueOf.length() + 33);
                        A0t.append("Unexpected InterruptedException: ");
                        Log.d(TAG, C12960it.A0d(valueOf, A0t));
                    }
                }
            }
            if (maybePerformActionIfVerified(SearchActionVerificationClientUtil.SEARCH_APP_PACKAGE, intent, this.searchActionVerificationServiceConnection)) {
                str = "Verified the intent with GSA.";
            } else {
                Log.i(TAG, "Unable to verify the intent with GSA.");
                if (maybePerformActionIfVerified(SearchActionVerificationClientUtil.ASSISTANT_GO_PACKAGE, intent, this.assistantGoVerificationServiceConnection)) {
                    str = "Verified the intent with Assistant Go.";
                } else {
                    str = "Unable to verify the intent with Assistant Go.";
                }
            }
            Log.i(TAG, str);
        } else if (this.dbg) {
            Log.d(TAG, "Unable to verify null intent");
        }
    }

    public void postForegroundNotification() {
        createChannel();
        startForeground(NOTIFICATION_ID, new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID).setGroup(NOTIFICATION_CHANNEL_ID).setContentTitle(getApplicationContext().getResources().getString(R.string.google_assistant_verification_notification_title)).setSmallIcon(17301545).setPriority(-2).setVisibility(1).build());
    }

    /* loaded from: classes2.dex */
    public class SearchActionVerificationServiceConnection implements ServiceConnection {
        public ISearchActionVerificationService iRemoteService;

        public SearchActionVerificationServiceConnection() {
        }

        public ISearchActionVerificationService getRemoteService() {
            return this.iRemoteService;
        }

        private boolean isConnected() {
            return C12960it.A1W(this.iRemoteService);
        }

        public boolean isVerified(Intent intent, Bundle bundle) {
            ISearchActionVerificationService iSearchActionVerificationService = this.iRemoteService;
            return iSearchActionVerificationService != null && iSearchActionVerificationService.isSearchAction(intent, bundle);
        }

        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (SearchActionVerificationClientService.this.dbg) {
                Log.d(SearchActionVerificationClientService.TAG, "onServiceConnected");
            }
            this.iRemoteService = ISearchActionVerificationService.Stub.asInterface(iBinder);
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            this.iRemoteService = null;
            if (SearchActionVerificationClientService.this.dbg) {
                Log.d(SearchActionVerificationClientService.TAG, "onServiceDisconnected");
            }
        }
    }
}
