package com.google.android.exoplayer2.upstream;

import X.AbstractC67743Ss;
import X.AnonymousClass3H3;
import X.AnonymousClass490;
import X.C12960it;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes2.dex */
public final class RawResourceDataSource extends AbstractC67743Ss {
    public long A00;
    public AssetFileDescriptor A01;
    public Uri A02;
    public InputStream A03;
    public boolean A04;
    public final Resources A05;
    public final String A06;

    public RawResourceDataSource(Context context) {
        super(false);
        this.A05 = context.getResources();
        this.A06 = context.getPackageName();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A02;
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r10) {
        int parseInt;
        String A0d;
        Uri uri = r10.A04;
        this.A02 = uri;
        if (TextUtils.equals("rawresource", uri.getScheme()) || (TextUtils.equals("android.resource", uri.getScheme()) && uri.getPathSegments().size() == 1 && uri.getLastPathSegment().matches("\\d+"))) {
            try {
                parseInt = Integer.parseInt(uri.getLastPathSegment());
            } catch (NumberFormatException unused) {
                throw new AnonymousClass490("Resource identifier must be an integer.");
            }
        } else if (TextUtils.equals("android.resource", uri.getScheme())) {
            String path = uri.getPath();
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            String host = uri.getHost();
            StringBuilder A0h = C12960it.A0h();
            if (TextUtils.isEmpty(host)) {
                A0d = "";
            } else {
                A0d = C12960it.A0d(":", C12960it.A0j(host));
            }
            A0h.append(A0d);
            parseInt = this.A05.getIdentifier(C12960it.A0d(path, A0h), "raw", this.A06);
            if (parseInt == 0) {
                throw new AnonymousClass490("Resource not found.");
            }
        } else {
            throw new AnonymousClass490("URI must either use scheme rawresource or android.resource");
        }
        A01();
        AssetFileDescriptor openRawResourceFd = this.A05.openRawResourceFd(parseInt);
        this.A01 = openRawResourceFd;
        if (openRawResourceFd != null) {
            FileInputStream fileInputStream = new FileInputStream(openRawResourceFd.getFileDescriptor());
            this.A03 = fileInputStream;
            try {
                fileInputStream.skip(openRawResourceFd.getStartOffset());
                long j = r10.A03;
                if (fileInputStream.skip(j) >= j) {
                    long j2 = r10.A02;
                    long j3 = -1;
                    if (j2 != -1) {
                        this.A00 = j2;
                    } else {
                        long length = openRawResourceFd.getLength();
                        if (length != -1) {
                            j3 = length - j;
                        }
                        this.A00 = j3;
                    }
                    this.A04 = true;
                    A03(r10);
                    return this.A00;
                }
                throw new EOFException();
            } catch (IOException e) {
                throw new AnonymousClass490(e);
            }
        } else {
            throw new AnonymousClass490(C12960it.A0b("Resource is compressed: ", uri));
        }
    }

    public static Uri buildRawResourceUri(int i) {
        return Uri.parse(C12960it.A0W(i, "rawresource:///"));
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A02 = null;
        try {
            try {
                InputStream inputStream = this.A03;
                if (inputStream != null) {
                    inputStream.close();
                }
                try {
                    this.A03 = null;
                    try {
                        AssetFileDescriptor assetFileDescriptor = this.A01;
                        if (assetFileDescriptor != null) {
                            assetFileDescriptor.close();
                        }
                        this.A01 = null;
                        if (this.A04) {
                            this.A04 = false;
                            A00();
                        }
                    } catch (IOException e) {
                        throw new AnonymousClass490(e);
                    }
                } catch (Throwable th) {
                    this.A01 = null;
                    if (this.A04) {
                        this.A04 = false;
                        A00();
                    }
                    throw th;
                }
            } catch (IOException e2) {
                throw new AnonymousClass490(e2);
            }
        } catch (Throwable th2) {
            try {
                this.A03 = null;
                try {
                    AssetFileDescriptor assetFileDescriptor2 = this.A01;
                    if (assetFileDescriptor2 != null) {
                        assetFileDescriptor2.close();
                    }
                    this.A01 = null;
                    if (this.A04) {
                        this.A04 = false;
                        A00();
                    }
                    throw th2;
                } catch (IOException e3) {
                    throw new AnonymousClass490(e3);
                }
            } catch (Throwable th3) {
                this.A01 = null;
                if (this.A04) {
                    this.A04 = false;
                    A00();
                }
                throw th3;
            }
        }
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        long j = this.A00;
        if (j != 0) {
            if (j != -1) {
                try {
                    i2 = (int) Math.min(j, (long) i2);
                } catch (IOException e) {
                    throw new AnonymousClass490(e);
                }
            }
            int read = this.A03.read(bArr, i, i2);
            if (read != -1) {
                long j2 = this.A00;
                if (j2 != -1) {
                    this.A00 = j2 - ((long) read);
                }
                A02(read);
                return read;
            } else if (this.A00 != -1) {
                throw new AnonymousClass490(new EOFException());
            }
        }
        return -1;
    }
}
