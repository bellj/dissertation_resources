package com.google.android.exoplayer2.ext.opus;

import X.AbstractC106564vu;
import X.AbstractC76693m3;
import X.AnonymousClass4CJ;
import X.C12960it;
import X.C72463ee;
import X.C76703m4;
import X.C76763mA;
import X.C87534Bw;
import X.C91844Tk;
import X.C95314dV;
import com.google.android.exoplayer2.decoder.SimpleOutputBuffer;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class OpusDecoder extends AbstractC106564vu {
    public int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final long A04;
    public final boolean A05;

    private native void opusClose(long j);

    private native int opusDecode(long j, long j2, ByteBuffer byteBuffer, int i, SimpleOutputBuffer simpleOutputBuffer);

    private native int opusGetErrorCode(long j);

    private native String opusGetErrorMessage(long j);

    private native long opusInit(int i, int i2, int i3, int i4, int i5, byte[] bArr);

    private native void opusReset(long j);

    private native int opusSecureDecode(long j, long j2, ByteBuffer byteBuffer, int i, SimpleOutputBuffer simpleOutputBuffer, int i2, ExoMediaCrypto exoMediaCrypto, int i3, byte[] bArr, byte[] bArr2, int i4, int[] iArr, int[] iArr2);

    private native void opusSetFloatOutput();

    /* JADX DEBUG: Multi-variable search result rejected for r13v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public OpusDecoder(List list, int i, boolean z) {
        super(new C76763mA[16], new SimpleOutputBuffer[16]);
        int i2;
        int i3;
        boolean z2;
        int size = list.size();
        int i4 = 1;
        if (size != 1 && size != 3) {
            throw new C76703m4("Invalid initialization data size");
        } else if (size != 3 || (C72463ee.A0b(list, 1).length == 8 && C72463ee.A0b(list, 2).length == 8)) {
            if (list.size() == 3) {
                i2 = AbstractC106564vu.A04(list, 1);
            } else {
                byte[] A0b = C72463ee.A0b(list, 0);
                i2 = (A0b[10] & 255) | ((A0b[11] & 255) << 8);
            }
            this.A02 = i2;
            if (list.size() == 3) {
                i3 = AbstractC106564vu.A04(list, 2);
            } else {
                i3 = 3840;
            }
            this.A03 = i3;
            byte[] A0b2 = C72463ee.A0b(list, 0);
            int length = A0b2.length;
            if (length >= 19) {
                int i5 = A0b2[9] & 255;
                this.A01 = i5;
                if (i5 <= 8) {
                    short s = (short) (((A0b2[17] & 255) << 8) | (A0b2[16] & 255));
                    byte[] bArr = new byte[8];
                    if (A0b2[18] == 0) {
                        if (i5 <= 2) {
                            boolean A1V = C12960it.A1V(i5, 2);
                            bArr[0] = 0;
                            bArr[1] = 1;
                            z2 = A1V;
                        } else {
                            throw new C76703m4("Invalid header, missing stream map");
                        }
                    } else if (length >= i5 + 21) {
                        i4 = A0b2[19] & 255;
                        System.arraycopy(A0b2, 21, bArr, 0, i5);
                        z2 = A0b2[20] & 255;
                    } else {
                        throw new C76703m4("Invalid header length");
                    }
                    int i6 = z2 ? 1 : 0;
                    int i7 = z2 ? 1 : 0;
                    int i8 = z2 ? 1 : 0;
                    long opusInit = opusInit(48000, i5, i4, i6, s, bArr);
                    this.A04 = opusInit;
                    if (opusInit != 0) {
                        int i9 = super.A00;
                        C76763mA[] r3 = this.A0B;
                        int length2 = r3.length;
                        C95314dV.A04(C12960it.A1V(i9, length2));
                        for (C76763mA r0 : r3) {
                            r0.A01(i);
                        }
                        this.A05 = z;
                        if (z) {
                            opusSetFloatOutput();
                            return;
                        }
                        return;
                    }
                    throw new C76703m4("Failed to initialize decoder");
                }
                throw new C76703m4(C12960it.A0W(i5, "Invalid channel count: "));
            }
            throw new C76703m4("Invalid header length");
        } else {
            throw new C76703m4("Invalid pre-skip or seek pre-roll");
        }
    }

    @Override // X.AbstractC106564vu
    public /* bridge */ /* synthetic */ AnonymousClass4CJ A05(C76763mA r24, AbstractC76693m3 r25, boolean z) {
        int opusDecode;
        int i;
        SimpleOutputBuffer simpleOutputBuffer = (SimpleOutputBuffer) r25;
        if (z) {
            opusReset(this.A04);
            if (r24.A00 == 0) {
                i = this.A02;
            } else {
                i = this.A03;
            }
            this.A00 = i;
        }
        ByteBuffer byteBuffer = r24.A01;
        C91844Tk r6 = r24.A05;
        boolean A1V = C12960it.A1V(r24.flags & 1073741824, 1073741824);
        long j = this.A04;
        long j2 = r24.A00;
        int limit = byteBuffer.limit();
        if (A1V) {
            opusDecode = opusSecureDecode(j, j2, byteBuffer, limit, simpleOutputBuffer, 48000, null, r6.A00, r6.A03, r6.A02, r6.A01, r6.A04, r6.A05);
        } else {
            opusDecode = opusDecode(j, j2, byteBuffer, limit, simpleOutputBuffer);
        }
        if (opusDecode >= 0) {
            ByteBuffer byteBuffer2 = simpleOutputBuffer.data;
            byteBuffer2.position(0);
            byteBuffer2.limit(opusDecode);
            int i2 = this.A00;
            if (i2 <= 0) {
                return null;
            }
            int i3 = this.A01 << 1;
            int i4 = i2 * i3;
            if (opusDecode <= i4) {
                this.A00 = i2 - (opusDecode / i3);
                simpleOutputBuffer.addFlag(Integer.MIN_VALUE);
                byteBuffer2.position(opusDecode);
                return null;
            }
            this.A00 = 0;
            byteBuffer2.position(i4);
            return null;
        } else if (opusDecode == -2) {
            String A0d = C12960it.A0d(opusGetErrorMessage(j), C12960it.A0k("Drm error: "));
            return new C76703m4(A0d, new C87534Bw(opusGetErrorCode(j), A0d));
        } else {
            return new C76703m4(C12960it.A0d(opusGetErrorMessage((long) opusDecode), C12960it.A0k("Decode error: ")));
        }
    }

    @Override // X.AnonymousClass5XF
    public String getName() {
        return C12960it.A0d(OpusLibrary.opusGetVersion(), C12960it.A0k("libopus"));
    }

    @Override // X.AbstractC106564vu, X.AnonymousClass5XF
    public void release() {
        super.release();
        opusClose(this.A04);
    }
}
