package com.google.android.exoplayer2.decoder;

import X.AbstractC76693m3;
import X.AnonymousClass5SI;
import X.C72453ed;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public class SimpleOutputBuffer extends AbstractC76693m3 {
    public ByteBuffer data;
    public final AnonymousClass5SI owner;

    public SimpleOutputBuffer(AnonymousClass5SI r1) {
        this.owner = r1;
    }

    @Override // X.AnonymousClass4YO
    public void clear() {
        this.flags = 0;
        ByteBuffer byteBuffer = this.data;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
    }

    public ByteBuffer init(long j, int i) {
        this.timeUs = j;
        ByteBuffer byteBuffer = this.data;
        if (byteBuffer == null || byteBuffer.capacity() < i) {
            this.data = C72453ed.A0x(i);
        }
        this.data.position(0);
        this.data.limit(i);
        return this.data;
    }

    @Override // X.AbstractC76693m3
    public void release() {
        this.owner.Aa8(this);
    }
}
