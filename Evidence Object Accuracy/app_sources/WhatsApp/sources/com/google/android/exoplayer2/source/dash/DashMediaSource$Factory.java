package com.google.android.exoplayer2.source.dash;

import X.AbstractC47452At;
import X.AnonymousClass5QA;
import X.AnonymousClass5QC;
import X.C106624w0;
import X.C107644xi;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public final class DashMediaSource$Factory implements AnonymousClass5QA {
    public List A00 = Collections.emptyList();
    public final AnonymousClass5QC A01;
    public final AbstractC47452At A02;

    public DashMediaSource$Factory(AbstractC47452At r2) {
        this.A01 = new C107644xi(r2);
        this.A02 = r2;
        new C106624w0();
    }
}
