package com.google.android.exoplayer2.source.smoothstreaming;

import X.AbstractC47452At;
import X.AnonymousClass5QA;
import X.AnonymousClass5QG;
import X.C106624w0;
import X.C107684xm;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public final class SsMediaSource$Factory implements AnonymousClass5QA {
    public List A00 = Collections.emptyList();
    public final AnonymousClass5QG A01;
    public final AbstractC47452At A02;

    public SsMediaSource$Factory(AbstractC47452At r2) {
        this.A01 = new C107684xm(r2);
        this.A02 = r2;
        new C106624w0();
    }
}
