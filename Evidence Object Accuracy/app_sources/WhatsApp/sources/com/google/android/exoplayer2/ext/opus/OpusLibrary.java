package com.google.android.exoplayer2.ext.opus;

import X.AnonymousClass4ZR;
import X.C12960it;

/* loaded from: classes3.dex */
public final class OpusLibrary {
    public static native String opusGetVersion();

    public static native boolean opusIsSecureDecodeSupported();

    static {
        synchronized (AnonymousClass4ZR.class) {
            if (AnonymousClass4ZR.A01.add("goog.exo.opus")) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(AnonymousClass4ZR.A00);
                A0h.append(", ");
                AnonymousClass4ZR.A00 = C12960it.A0d("goog.exo.opus", A0h);
            }
        }
    }
}
