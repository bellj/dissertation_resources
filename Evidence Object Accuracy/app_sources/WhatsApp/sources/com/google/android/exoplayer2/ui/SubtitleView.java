package com.google.android.exoplayer2.ui;

import X.AnonymousClass3JZ;
import X.AnonymousClass5SS;
import X.AnonymousClass5ST;
import X.C12980iv;
import X.C72453ed;
import X.C73723gh;
import X.C74103hK;
import X.C93834ao;
import X.C94144bK;
import X.C95274dQ;
import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public final class SubtitleView extends FrameLayout implements AnonymousClass5SS {
    public float A00;
    public float A01;
    public int A02;
    public View A03;
    public C95274dQ A04;
    public AnonymousClass5ST A05;
    public List A06;
    public boolean A07;
    public boolean A08;

    public SubtitleView(Context context) {
        this(context, null);
    }

    public SubtitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A06 = Collections.emptyList();
        this.A04 = C95274dQ.A06;
        this.A01 = 0.0533f;
        this.A00 = 0.08f;
        this.A08 = true;
        this.A07 = true;
        C73723gh r0 = new C73723gh(context, attributeSet);
        this.A05 = r0;
        this.A03 = r0;
        addView(r0);
        this.A02 = 1;
    }

    public void A00() {
        setStyle(getUserCaptionStyle());
    }

    public void A01() {
        setFractionalTextSize(getUserCaptionFontScale() * 0.0533f);
    }

    public final void A02() {
        this.A05.AfK(this.A04, getCuesWithStylingPreferencesApplied(), this.A01, this.A00, 0);
    }

    @Override // X.AnonymousClass5SS
    public void AOn(List list) {
        setCues(list);
    }

    private List getCuesWithStylingPreferencesApplied() {
        C94144bK r6;
        if (this.A08 && this.A07) {
            return this.A06;
        }
        ArrayList A0w = C12980iv.A0w(this.A06.size());
        for (int i = 0; i < this.A06.size(); i++) {
            C93834ao r1 = (C93834ao) this.A06.get(i);
            CharSequence charSequence = r1.A0E;
            if (!this.A08) {
                r6 = new C94144bK(r1);
                r6.A05 = -3.4028235E38f;
                r6.A09 = Integer.MIN_VALUE;
                r6.A0F = false;
                if (charSequence != null) {
                    r6.A0E = charSequence.toString();
                }
            } else {
                if (!this.A07 && charSequence != null) {
                    r6 = new C94144bK(r1);
                    r6.A05 = -3.4028235E38f;
                    r6.A09 = Integer.MIN_VALUE;
                    if (charSequence instanceof Spanned) {
                        SpannableString valueOf = SpannableString.valueOf(charSequence);
                        for (AbsoluteSizeSpan absoluteSizeSpan : (AbsoluteSizeSpan[]) valueOf.getSpans(0, valueOf.length(), AbsoluteSizeSpan.class)) {
                            valueOf.removeSpan(absoluteSizeSpan);
                        }
                        for (RelativeSizeSpan relativeSizeSpan : (RelativeSizeSpan[]) valueOf.getSpans(0, valueOf.length(), RelativeSizeSpan.class)) {
                            valueOf.removeSpan(relativeSizeSpan);
                        }
                        r6.A0E = valueOf;
                    }
                }
                A0w.add(r1);
            }
            r1 = r6.A00();
            A0w.add(r1);
        }
        return A0w;
    }

    private float getUserCaptionFontScale() {
        CaptioningManager captioningManager;
        if (AnonymousClass3JZ.A01 < 19 || isInEditMode() || (captioningManager = (CaptioningManager) getContext().getSystemService("captioning")) == null || !captioningManager.isEnabled()) {
            return 1.0f;
        }
        return captioningManager.getFontScale();
    }

    private C95274dQ getUserCaptionStyle() {
        CaptioningManager captioningManager;
        if (AnonymousClass3JZ.A01 < 19 || isInEditMode() || (captioningManager = (CaptioningManager) getContext().getSystemService("captioning")) == null || !captioningManager.isEnabled()) {
            return C95274dQ.A06;
        }
        return C95274dQ.A00(captioningManager.getUserStyle());
    }

    public void setApplyEmbeddedFontSizes(boolean z) {
        this.A07 = z;
        A02();
    }

    public void setApplyEmbeddedStyles(boolean z) {
        this.A08 = z;
        A02();
    }

    public void setBottomPaddingFraction(float f) {
        this.A00 = f;
        A02();
    }

    public void setCues(List list) {
        if (list == null) {
            list = Collections.emptyList();
        }
        this.A06 = list;
        A02();
    }

    public void setFractionalTextSize(float f) {
        this.A01 = f;
        A02();
    }

    public void setStyle(C95274dQ r1) {
        this.A04 = r1;
        A02();
    }

    private void setView(View view) {
        removeView(this.A03);
        View view2 = this.A03;
        if (view2 instanceof C74103hK) {
            ((C74103hK) view2).A04.destroy();
        }
        this.A03 = view;
        this.A05 = (AnonymousClass5ST) view;
        addView(view);
    }

    public void setViewType(int i) {
        if (this.A02 != i) {
            if (i == 1) {
                setView(new C73723gh(getContext(), null));
            } else if (i == 2) {
                setView(new C74103hK(getContext()));
            } else {
                throw C72453ed.A0h();
            }
            this.A02 = i;
        }
    }
}
