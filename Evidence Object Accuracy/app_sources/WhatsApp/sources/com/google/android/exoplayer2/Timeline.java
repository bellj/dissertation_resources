package com.google.android.exoplayer2;

import X.AbstractC76613lv;
import X.AbstractC76623lw;
import X.AnonymousClass4YJ;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C56072kH;
import X.C72463ee;
import X.C76493lj;
import X.C76503lk;
import X.C76593lt;
import X.C76603lu;
import X.C76633lx;
import X.C77163mq;
import X.C94374bh;
import X.C94404bl;
import X.C95314dV;
import android.util.Pair;
import java.util.Arrays;

/* loaded from: classes3.dex */
public abstract class Timeline {
    public static final Timeline A00 = new C76593lt();

    public abstract C94404bl A0B(C94404bl v, int i, long j);

    public int A00() {
        if ((this instanceof C76633lx) || (this instanceof C76603lu)) {
            return 1;
        }
        if (this instanceof AbstractC76613lv) {
            return ((AbstractC76613lv) this).A00.A00();
        }
        if (this instanceof C76593lt) {
            return 0;
        }
        if (!(this instanceof C76493lj)) {
            return ((C76503lk) this).A00;
        }
        C76493lj r0 = (C76493lj) this;
        return r0.A00 * r0.A02;
    }

    public int A01() {
        if ((this instanceof C76633lx) || (this instanceof C76603lu)) {
            return 1;
        }
        if (this instanceof AbstractC76613lv) {
            return ((AbstractC76613lv) this).A00.A01();
        }
        if (this instanceof C76593lt) {
            return 0;
        }
        if (!(this instanceof C76493lj)) {
            return ((C76503lk) this).A01;
        }
        C76493lj r0 = (C76493lj) this;
        return r0.A01 * r0.A02;
    }

    public int A02(int i, int i2, boolean z) {
        int i3;
        if (this instanceof AbstractC76613lv) {
            AbstractC76613lv r3 = (AbstractC76613lv) this;
            boolean z2 = r3 instanceof C77163mq;
            int A02 = r3.A00.A02(i, i2, z);
            if (!z2 || A02 != -1) {
                return A02;
            }
            return r3.A05(z);
        } else if (!(this instanceof AbstractC76623lw)) {
            if (i2 != 0) {
                if (i2 == 1) {
                    return i;
                }
                if (i2 != 2) {
                    throw C72463ee.A0D();
                } else if (i == A06(z)) {
                    return A05(z);
                }
            } else if (i == A06(z)) {
                return -1;
            }
            return i + 1;
        } else {
            AbstractC76623lw r4 = (AbstractC76623lw) this;
            int i4 = 0;
            if (!(r4 instanceof C76493lj)) {
                int[] iArr = ((C76503lk) r4).A04;
                int i5 = i + 1;
                i3 = Arrays.binarySearch(iArr, i5);
                if (i3 >= 0) {
                    do {
                        i3--;
                        if (i3 < 0) {
                            break;
                        }
                    } while (iArr[i3] == i5);
                } else {
                    i3 = -(i3 + 2);
                }
            } else {
                i3 = i / ((C76493lj) r4).A01;
            }
            int A0F = r4.A0F(i3);
            Timeline A0G = r4.A0G(i3);
            int i6 = i - A0F;
            if (i2 != 2) {
                i4 = i2;
            }
            int A022 = A0G.A02(i6, i4, z);
            if (A022 != -1) {
                return A0F + A022;
            }
            while (true) {
                if (!z) {
                    if (i3 >= r4.A00 - 1) {
                        break;
                    }
                    i3++;
                } else {
                    i3 = r4.A01.AEe(i3);
                }
                if (i3 == -1) {
                    break;
                }
                Timeline A0G2 = r4.A0G(i3);
                if (!C12960it.A1T(A0G2.A01())) {
                    if (i3 != -1) {
                        return r4.A0F(i3) + A0G2.A05(z);
                    }
                }
            }
            if (i2 == 2) {
                return r4.A05(z);
            }
            return -1;
        }
    }

    public final int A03(AnonymousClass4YJ r5, C94404bl r6, int i, int i2, boolean z) {
        int i3 = A09(r5, i, false).A00;
        if (A0B(r6, i3, 0).A01 != i) {
            return i + 1;
        }
        int A02 = A02(i3, i2, z);
        if (A02 == -1) {
            return -1;
        }
        return A0B(r6, A02, 0).A00;
    }

    public int A04(Object obj) {
        int A05;
        int A04;
        if (!(this instanceof C76633lx)) {
            if (this instanceof C76603lu) {
                return obj == C56072kH.A02 ? 0 : -1;
            }
            if (this instanceof AbstractC76613lv) {
                return ((AbstractC76613lv) this).A00.A04(obj);
            }
            if (this instanceof C76593lt) {
                return -1;
            }
            AbstractC76623lw r4 = (AbstractC76623lw) this;
            if (!(obj instanceof Pair)) {
                return -1;
            }
            Pair pair = (Pair) obj;
            Object obj2 = pair.first;
            Object obj3 = pair.second;
            if (!(r4 instanceof C76493lj)) {
                Number number = (Number) ((C76503lk) r4).A02.get(obj2);
                if (number == null) {
                    return -1;
                }
                A05 = number.intValue();
            } else if (!(obj2 instanceof Integer)) {
                return -1;
            } else {
                A05 = C12960it.A05(obj2);
            }
            if (A05 == -1 || (A04 = r4.A0G(A05).A04(obj3)) == -1) {
                return -1;
            }
            return r4.A0E(A05) + A04;
        } else if (C76633lx.A09.equals(obj)) {
            return 0;
        } else {
            return -1;
        }
    }

    public int A05(boolean z) {
        if (this instanceof AbstractC76613lv) {
            return ((AbstractC76613lv) this).A00.A05(z);
        }
        if (!(this instanceof AbstractC76623lw)) {
            return C12960it.A1T(A01()) ? -1 : 0;
        }
        AbstractC76623lw r5 = (AbstractC76623lw) this;
        int i = r5.A00;
        if (i == 0) {
            return -1;
        }
        int i2 = 0;
        if (z) {
            i2 = r5.A01.AD2();
        }
        do {
            Timeline A0G = r5.A0G(i2);
            if (!C12960it.A1T(A0G.A01())) {
                return r5.A0F(i2) + A0G.A05(z);
            }
            if (z) {
                i2 = r5.A01.AEe(i2);
                continue;
            } else if (i2 >= i - 1) {
                return -1;
            } else {
                i2++;
                continue;
            }
        } while (i2 != -1);
        return -1;
    }

    public int A06(boolean z) {
        int i;
        if (this instanceof AbstractC76613lv) {
            return ((AbstractC76613lv) this).A00.A06(z);
        }
        if (this instanceof AbstractC76623lw) {
            AbstractC76623lw r4 = (AbstractC76623lw) this;
            int i2 = r4.A00;
            if (i2 != 0) {
                if (z) {
                    i = r4.A01.ADm();
                } else {
                    i = i2 - 1;
                }
                do {
                    Timeline A0G = r4.A0G(i);
                    if (C12960it.A1T(A0G.A01())) {
                        if (!z) {
                            if (i <= 0) {
                                break;
                            }
                            i--;
                            continue;
                        } else {
                            i = r4.A01.AFt(i);
                            continue;
                        }
                    } else {
                        return r4.A0F(i) + A0G.A06(z);
                    }
                } while (i != -1);
            }
            return -1;
        } else if (C12960it.A1T(A01())) {
            return -1;
        } else {
            return A01() - 1;
        }
    }

    public final Pair A07(AnonymousClass4YJ r9, C94404bl r10, int i, long j) {
        return A08(r9, r10, i, j, 0);
    }

    public final Pair A08(AnonymousClass4YJ r10, C94404bl r11, int i, long j, long j2) {
        C95314dV.A00(i, A01());
        A0B(r11, i, j2);
        if (j == -9223372036854775807L) {
            j = 0;
        }
        int i2 = r11.A00;
        long j3 = 0 + j;
        while (true) {
            long j4 = A09(r10, i2, true).A01;
            if (j4 == -9223372036854775807L || j3 < j4 || i2 >= r11.A01) {
                break;
            }
            j3 -= j4;
            i2++;
        }
        return Pair.create(r10.A05, Long.valueOf(j3));
    }

    public AnonymousClass4YJ A09(AnonymousClass4YJ r11, int i, boolean z) {
        Object obj;
        Integer num;
        int i2;
        Object valueOf;
        if (this instanceof C76633lx) {
            C76633lx r1 = (C76633lx) this;
            C95314dV.A00(i, 1);
            if (z) {
                obj = C76633lx.A09;
            } else {
                obj = null;
            }
            long j = r1.A01;
            C94374bh r0 = C94374bh.A04;
            r11.A04 = null;
            r11.A05 = obj;
            r11.A00 = 0;
            r11.A01 = j;
            r11.A02 = -0;
            r11.A03 = r0;
            return r11;
        } else if (this instanceof C76603lu) {
            Object obj2 = null;
            if (z) {
                num = C12980iv.A0i();
                obj2 = C56072kH.A02;
            } else {
                num = null;
            }
            C94374bh r12 = C94374bh.A04;
            r11.A04 = num;
            r11.A05 = obj2;
            r11.A00 = 0;
            r11.A01 = -9223372036854775807L;
            r11.A02 = 0;
            r11.A03 = r12;
            return r11;
        } else if (this instanceof AbstractC76613lv) {
            return ((AbstractC76613lv) this).A00.A09(r11, i, z);
        } else {
            if (!(this instanceof C76593lt)) {
                AbstractC76623lw r5 = (AbstractC76623lw) this;
                boolean z2 = r5 instanceof C76493lj;
                if (!z2) {
                    int[] iArr = ((C76503lk) r5).A03;
                    int i3 = i + 1;
                    i2 = Arrays.binarySearch(iArr, i3);
                    if (i2 >= 0) {
                        do {
                            i2--;
                            if (i2 < 0) {
                                break;
                            }
                        } while (iArr[i2] == i3);
                    } else {
                        i2 = -(i2 + 2);
                    }
                } else {
                    i2 = i / ((C76493lj) r5).A00;
                }
                int A0F = r5.A0F(i2);
                r5.A0G(i2).A09(r11, i - r5.A0E(i2), z);
                r11.A00 += A0F;
                if (z) {
                    if (!z2) {
                        valueOf = ((C76503lk) r5).A06[i2];
                    } else {
                        valueOf = Integer.valueOf(i2);
                    }
                    r11.A05 = Pair.create(valueOf, r11.A05);
                }
                return r11;
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public AnonymousClass4YJ A0A(AnonymousClass4YJ r5, Object obj) {
        int i;
        if (!(this instanceof AbstractC76623lw)) {
            return A09(r5, A04(obj), true);
        }
        AbstractC76623lw r3 = (AbstractC76623lw) this;
        Pair pair = (Pair) obj;
        Object obj2 = pair.first;
        Object obj3 = pair.second;
        if (!(r3 instanceof C76493lj)) {
            Number number = (Number) ((C76503lk) r3).A02.get(obj2);
            if (number != null) {
                i = number.intValue();
            }
            i = -1;
        } else {
            if (obj2 instanceof Integer) {
                i = C12960it.A05(obj2);
            }
            i = -1;
        }
        int A0F = r3.A0F(i);
        r3.A0G(i).A0A(r5, obj3);
        r5.A00 += A0F;
        r5.A05 = obj;
        return r5;
    }

    public Object A0C(int i) {
        int i2;
        Object valueOf;
        if (this instanceof C76633lx) {
            C95314dV.A00(i, 1);
            return C76633lx.A09;
        } else if (this instanceof C76603lu) {
            return C56072kH.A02;
        } else {
            if (this instanceof AbstractC76613lv) {
                return ((AbstractC76613lv) this).A00.A0C(i);
            }
            if (!(this instanceof C76593lt)) {
                AbstractC76623lw r5 = (AbstractC76623lw) this;
                boolean z = r5 instanceof C76493lj;
                if (!z) {
                    int[] iArr = ((C76503lk) r5).A03;
                    int i3 = i + 1;
                    i2 = Arrays.binarySearch(iArr, i3);
                    if (i2 >= 0) {
                        do {
                            i2--;
                            if (i2 < 0) {
                                break;
                            }
                        } while (iArr[i2] == i3);
                    } else {
                        i2 = -(i2 + 2);
                    }
                } else {
                    i2 = i / ((C76493lj) r5).A00;
                }
                Object A0C = r5.A0G(i2).A0C(i - r5.A0E(i2));
                if (!z) {
                    valueOf = ((C76503lk) r5).A06[i2];
                } else {
                    valueOf = Integer.valueOf(i2);
                }
                return Pair.create(valueOf, A0C);
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public final boolean A0D() {
        return C12960it.A1T(A01());
    }

    public boolean equals(Object obj) {
        Timeline timeline;
        int A01;
        int A002;
        if (this != obj) {
            if ((obj instanceof Timeline) && (timeline = (Timeline) obj).A01() == (A01 = A01()) && timeline.A00() == (A002 = A00())) {
                C94404bl r10 = new C94404bl();
                AnonymousClass4YJ r6 = new AnonymousClass4YJ();
                C94404bl r9 = new C94404bl();
                AnonymousClass4YJ r5 = new AnonymousClass4YJ();
                int i = 0;
                while (true) {
                    if (i < A01) {
                        if (!A0B(r10, i, 0).equals(timeline.A0B(r9, i, 0))) {
                            break;
                        }
                        i++;
                    } else {
                        for (int i2 = 0; i2 < A002; i2++) {
                            if (A09(r6, i2, true).equals(timeline.A09(r5, i2, true))) {
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        C94404bl r6 = new C94404bl();
        AnonymousClass4YJ r5 = new AnonymousClass4YJ();
        int A01 = A01();
        int i2 = 217 + A01;
        int i3 = 0;
        while (true) {
            i = i2 * 31;
            if (i3 >= A01) {
                break;
            }
            i2 = C12990iw.A08(C72463ee.A0B(r6, this, i3), i);
            i3++;
        }
        int A002 = A00();
        int i4 = i + A002;
        for (int i5 = 0; i5 < A002; i5++) {
            i4 = C12990iw.A08(A09(r5, i5, true), i4 * 31);
        }
        return i4;
    }
}
