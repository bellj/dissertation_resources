package com.google.android.exoplayer2.source.hls;

import X.AbstractC107894y8;
import X.AbstractC116835Xb;
import X.AbstractC47452At;
import X.AnonymousClass5QA;
import X.AnonymousClass5QD;
import X.AnonymousClass5QE;
import X.C106624w0;
import X.C107654xj;
import java.util.Collections;
import java.util.List;

/* loaded from: classes3.dex */
public final class HlsMediaSource$Factory implements AnonymousClass5QA {
    public AbstractC116835Xb A00 = AbstractC116835Xb.A00;
    public AnonymousClass5QE A01 = AbstractC107894y8.A00;
    public List A02 = Collections.emptyList();
    public final AnonymousClass5QD A03;

    public HlsMediaSource$Factory(AbstractC47452At r2) {
        this.A03 = new C107654xj(r2);
        new C106624w0();
    }
}
