package com.google.android.exoplayer2.ui;

import X.AnonymousClass5QL;
import X.C100564m7;
import X.C107824y0;
import X.C12960it;
import X.C12980iv;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class TrackSelectionView extends LinearLayout {
    public C100564m7 A00;
    public AnonymousClass5QL A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final SparseArray A05;
    public final LayoutInflater A06;
    public final CheckedTextView A07;
    public final CheckedTextView A08;
    public final ViewOnClickCListenerShape6S0100000_I1 A09;

    public TrackSelectionView(Context context) {
        this(context, null);
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOrientation(1);
        this.A05 = new SparseArray();
        setSaveFromParentEnabled(false);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16843534});
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        LayoutInflater from = LayoutInflater.from(context);
        this.A06 = from;
        ViewOnClickCListenerShape6S0100000_I1 viewOnClickCListenerShape6S0100000_I1 = new ViewOnClickCListenerShape6S0100000_I1(this);
        this.A09 = viewOnClickCListenerShape6S0100000_I1;
        this.A01 = new C107824y0(getResources());
        this.A00 = C100564m7.A03;
        CheckedTextView checkedTextView = (CheckedTextView) from.inflate(17367055, (ViewGroup) this, false);
        this.A08 = checkedTextView;
        checkedTextView.setBackgroundResource(resourceId);
        checkedTextView.setText(R.string.exo_track_selection_none);
        checkedTextView.setEnabled(false);
        checkedTextView.setFocusable(true);
        checkedTextView.setOnClickListener(viewOnClickCListenerShape6S0100000_I1);
        checkedTextView.setVisibility(8);
        addView(checkedTextView);
        addView(from.inflate(R.layout.exo_list_divider, (ViewGroup) this, false));
        CheckedTextView checkedTextView2 = (CheckedTextView) from.inflate(17367055, (ViewGroup) this, false);
        this.A07 = checkedTextView2;
        checkedTextView2.setBackgroundResource(resourceId);
        checkedTextView2.setText(R.string.exo_track_selection_auto);
        checkedTextView2.setEnabled(false);
        checkedTextView2.setFocusable(true);
        checkedTextView2.setOnClickListener(viewOnClickCListenerShape6S0100000_I1);
        addView(checkedTextView2);
    }

    public final void A00() {
        for (int childCount = getChildCount() - 1; childCount >= 3; childCount--) {
            removeViewAt(childCount);
        }
        this.A08.setEnabled(false);
        this.A07.setEnabled(false);
    }

    public boolean getIsDisabled() {
        return this.A04;
    }

    public List getOverrides() {
        SparseArray sparseArray = this.A05;
        ArrayList A0w = C12980iv.A0w(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); i++) {
            A0w.add(sparseArray.valueAt(i));
        }
        return A0w;
    }

    public void setAllowAdaptiveSelections(boolean z) {
        if (this.A02 != z) {
            this.A02 = z;
            A00();
        }
    }

    public void setAllowMultipleOverrides(boolean z) {
        if (this.A03 != z) {
            this.A03 = z;
            if (!z) {
                SparseArray sparseArray = this.A05;
                if (sparseArray.size() > 1) {
                    for (int size = sparseArray.size() - 1; size > 0; size--) {
                        sparseArray.remove(size);
                    }
                }
            }
            A00();
        }
    }

    public void setShowDisableOption(boolean z) {
        this.A08.setVisibility(C12960it.A02(z ? 1 : 0));
    }

    public void setTrackNameProvider(AnonymousClass5QL r1) {
        this.A01 = r1;
        A00();
    }
}
