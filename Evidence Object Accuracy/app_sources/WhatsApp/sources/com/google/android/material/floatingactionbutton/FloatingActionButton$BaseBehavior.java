package com.google.android.material.floatingactionbutton;

import X.AbstractC009304r;
import X.AnonymousClass0B5;
import X.AnonymousClass2QV;
import X.C50572Qb;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

/* loaded from: classes2.dex */
public class FloatingActionButton$BaseBehavior extends AbstractC009304r {
    public Rect A00;
    public boolean A01;

    public FloatingActionButton$BaseBehavior() {
        this.A01 = true;
    }

    public FloatingActionButton$BaseBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A08);
        this.A01 = obtainStyledAttributes.getBoolean(0, true);
        obtainStyledAttributes.recycle();
    }

    @Override // X.AbstractC009304r
    public /* bridge */ /* synthetic */ boolean A02(Rect rect, View view, CoordinatorLayout coordinatorLayout) {
        AnonymousClass2QV r8 = (AnonymousClass2QV) view;
        Rect rect2 = r8.A0C;
        rect.set(r8.getLeft() + rect2.left, r8.getTop() + rect2.top, r8.getRight() - rect2.right, r8.getBottom() - rect2.bottom);
        return true;
    }

    @Override // X.AbstractC009304r
    public /* bridge */ /* synthetic */ boolean A06(View view, View view2, CoordinatorLayout coordinatorLayout) {
        AnonymousClass2QV r3 = (AnonymousClass2QV) view;
        if (view2 instanceof AppBarLayout) {
            A0J(coordinatorLayout, (AppBarLayout) view2, r3);
            return false;
        }
        ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
        if (!(layoutParams instanceof AnonymousClass0B5) || !(((AnonymousClass0B5) layoutParams).A0A instanceof BottomSheetBehavior)) {
            return false;
        }
        A0I(view2, r3);
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ boolean A0G(android.view.View r7, androidx.coordinatorlayout.widget.CoordinatorLayout r8, int r9) {
        /*
            r6 = this;
            X.2QV r7 = (X.AnonymousClass2QV) r7
            java.util.List r5 = r8.A07(r7)
            int r4 = r5.size()
            r3 = 0
        L_0x000b:
            if (r3 >= r4) goto L_0x001f
            java.lang.Object r2 = r5.get(r3)
            android.view.View r2 = (android.view.View) r2
            boolean r0 = r2 instanceof com.google.android.material.appbar.AppBarLayout
            if (r0 == 0) goto L_0x007a
            com.google.android.material.appbar.AppBarLayout r2 = (com.google.android.material.appbar.AppBarLayout) r2
            boolean r0 = r6.A0J(r8, r2, r7)
        L_0x001d:
            if (r0 == 0) goto L_0x008f
        L_0x001f:
            r8.A0D(r7, r9)
            android.graphics.Rect r4 = r7.A0C
            if (r4 == 0) goto L_0x005e
            int r0 = r4.centerX()
            if (r0 <= 0) goto L_0x005e
            int r0 = r4.centerY()
            if (r0 <= 0) goto L_0x005e
            android.view.ViewGroup$MarginLayoutParams r5 = X.C12970iu.A0H(r7)
            int r2 = r7.getRight()
            int r1 = r8.getWidth()
            int r0 = r5.rightMargin
            int r1 = r1 - r0
            if (r2 < r1) goto L_0x006c
            int r3 = r4.right
        L_0x0045:
            int r2 = r7.getBottom()
            int r1 = r8.getHeight()
            int r0 = r5.bottomMargin
            int r1 = r1 - r0
            if (r2 < r1) goto L_0x0060
            int r0 = r4.bottom
        L_0x0054:
            if (r0 == 0) goto L_0x0059
            X.AnonymousClass028.A0Y(r7, r0)
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            X.AnonymousClass028.A0X(r7, r3)
        L_0x005e:
            r0 = 1
            return r0
        L_0x0060:
            int r1 = r7.getTop()
            int r0 = r5.topMargin
            if (r1 > r0) goto L_0x0059
            int r0 = r4.top
            int r0 = -r0
            goto L_0x0054
        L_0x006c:
            int r1 = r7.getLeft()
            int r0 = r5.leftMargin
            if (r1 > r0) goto L_0x0078
            int r0 = r4.left
            int r3 = -r0
            goto L_0x0045
        L_0x0078:
            r3 = 0
            goto L_0x0045
        L_0x007a:
            android.view.ViewGroup$LayoutParams r1 = r2.getLayoutParams()
            boolean r0 = r1 instanceof X.AnonymousClass0B5
            if (r0 == 0) goto L_0x008f
            X.0B5 r1 = (X.AnonymousClass0B5) r1
            X.04r r0 = r1.A0A
            boolean r0 = r0 instanceof com.google.android.material.bottomsheet.BottomSheetBehavior
            if (r0 == 0) goto L_0x008f
            boolean r0 = r6.A0I(r2, r7)
            goto L_0x001d
        L_0x008f:
            int r3 = r3 + 1
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.floatingactionbutton.FloatingActionButton$BaseBehavior.A0G(android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout, int):boolean");
    }

    @Override // X.AbstractC009304r
    public void A0H(AnonymousClass0B5 r2) {
        if (r2.A01 == 0) {
            r2.A01 = 80;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (((X.AnonymousClass2QW) r6).A00 != 0) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0I(android.view.View r5, X.AnonymousClass2QV r6) {
        /*
            r4 = this;
            android.view.ViewGroup$LayoutParams r1 = r6.getLayoutParams()
            X.0B5 r1 = (X.AnonymousClass0B5) r1
            boolean r0 = r4.A01
            if (r0 == 0) goto L_0x0017
            int r1 = r1.A05
            int r0 = r5.getId()
            if (r1 != r0) goto L_0x0017
            int r0 = r6.A00
            r1 = 1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r1 = 0
        L_0x0018:
            r3 = 0
            if (r1 != 0) goto L_0x001c
            return r3
        L_0x001c:
            android.view.ViewGroup$MarginLayoutParams r0 = X.C12970iu.A0H(r6)
            int r2 = r5.getTop()
            int r1 = X.C13000ix.A00(r6)
            int r0 = r0.topMargin
            int r1 = r1 + r0
            if (r2 >= r1) goto L_0x0032
            r6.A03(r3)
        L_0x0030:
            r0 = 1
            return r0
        L_0x0032:
            r6.A04(r3)
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.floatingactionbutton.FloatingActionButton$BaseBehavior.A0I(android.view.View, X.2QV):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (((X.AnonymousClass2QW) r6).A00 != 0) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0J(androidx.coordinatorlayout.widget.CoordinatorLayout r4, com.google.android.material.appbar.AppBarLayout r5, X.AnonymousClass2QV r6) {
        /*
            r3 = this;
            android.view.ViewGroup$LayoutParams r1 = r6.getLayoutParams()
            X.0B5 r1 = (X.AnonymousClass0B5) r1
            boolean r0 = r3.A01
            if (r0 == 0) goto L_0x0017
            int r1 = r1.A05
            int r0 = r5.getId()
            if (r1 != r0) goto L_0x0017
            int r0 = r6.A00
            r1 = 1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r1 = 0
        L_0x0018:
            r2 = 0
            if (r1 != 0) goto L_0x001c
            return r2
        L_0x001c:
            android.graphics.Rect r0 = r3.A00
            if (r0 != 0) goto L_0x0026
            android.graphics.Rect r0 = X.C12980iv.A0J()
            r3.A00 = r0
        L_0x0026:
            X.AnonymousClass3ID.A01(r0, r5, r4)
            int r1 = r0.bottom
            int r0 = r5.getMinimumHeightForVisibleOverlappingContent()
            if (r1 > r0) goto L_0x0036
            r6.A03(r2)
        L_0x0034:
            r0 = 1
            return r0
        L_0x0036:
            r6.A04(r2)
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.floatingactionbutton.FloatingActionButton$BaseBehavior.A0J(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, X.2QV):boolean");
    }
}
