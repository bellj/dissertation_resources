package com.google.android.material.transformation;

import X.AnonymousClass2R0;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class FabTransformationScrimBehavior extends ExpandableTransformationBehavior {
    public final AnonymousClass2R0 A00 = new AnonymousClass2R0(0);
    public final AnonymousClass2R0 A01 = new AnonymousClass2R0(75);

    public FabTransformationScrimBehavior() {
    }

    public FabTransformationScrimBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
