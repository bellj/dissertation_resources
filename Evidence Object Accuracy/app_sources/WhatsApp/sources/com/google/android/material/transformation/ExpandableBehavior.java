package com.google.android.material.transformation;

import X.AbstractC009304r;
import X.AnonymousClass2QV;
import X.AnonymousClass2QX;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* loaded from: classes2.dex */
public abstract class ExpandableBehavior extends AbstractC009304r {
    public int A00 = 0;

    public abstract boolean A0I(View view, View view2, boolean z, boolean z2);

    public ExpandableBehavior() {
    }

    public ExpandableBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AbstractC009304r
    public boolean A05(View view, View view2, CoordinatorLayout coordinatorLayout) {
        if (this instanceof FabTransformationScrimBehavior) {
            return view2 instanceof AnonymousClass2QV;
        }
        if (view.getVisibility() == 8) {
            throw C12960it.A0U("This behavior cannot be attached to a GONE view. Set the view to INVISIBLE instead.");
        } else if (!(view2 instanceof AnonymousClass2QV)) {
            return false;
        } else {
            int i = ((AnonymousClass2QV) view2).A0F.A00;
            if (i == 0 || i == view.getId()) {
                return true;
            }
            return false;
        }
    }

    @Override // X.AbstractC009304r
    public boolean A06(View view, View view2, CoordinatorLayout coordinatorLayout) {
        int i;
        AnonymousClass2QX r6 = (AnonymousClass2QX) view2;
        boolean z = ((AnonymousClass2QV) r6).A0F.A01;
        int i2 = this.A00;
        if (!z) {
            i = 2;
            if (i2 != 1) {
                return false;
            }
        } else if (i2 != 0 && i2 != 2) {
            return false;
        } else {
            i = 1;
        }
        this.A00 = i;
        return A0I((View) r6, view, z, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r3 != false) goto L_0x0035;
     */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0G(android.view.View r6, androidx.coordinatorlayout.widget.CoordinatorLayout r7, int r8) {
        /*
            r5 = this;
            boolean r0 = X.AnonymousClass028.A0r(r6)
            if (r0 != 0) goto L_0x0044
            java.util.List r3 = r7.A07(r6)
            int r2 = r3.size()
            r1 = 0
        L_0x000f:
            if (r1 >= r2) goto L_0x0044
            java.lang.Object r4 = r3.get(r1)
            android.view.View r4 = (android.view.View) r4
            boolean r0 = r5.A05(r6, r4, r7)
            if (r0 == 0) goto L_0x0046
            X.2QX r4 = (X.AnonymousClass2QX) r4
            if (r4 == 0) goto L_0x0044
            r0 = r4
            X.2QV r0 = (X.AnonymousClass2QV) r0
            X.2Qg r0 = r0.A0F
            boolean r3 = r0.A01
            r1 = 1
            int r0 = r5.A00
            if (r3 == 0) goto L_0x0030
            if (r0 == 0) goto L_0x0035
            r1 = 2
        L_0x0030:
            if (r0 != r1) goto L_0x0044
            r2 = 2
            if (r3 == 0) goto L_0x0036
        L_0x0035:
            r2 = 1
        L_0x0036:
            r5.A00 = r2
            android.view.ViewTreeObserver r1 = r6.getViewTreeObserver()
            X.3Nt r0 = new X.3Nt
            r0.<init>(r6, r4, r5, r2)
            r1.addOnPreDrawListener(r0)
        L_0x0044:
            r0 = 0
            return r0
        L_0x0046:
            int r1 = r1 + 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.transformation.ExpandableBehavior.A0G(android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout, int):boolean");
    }
}
