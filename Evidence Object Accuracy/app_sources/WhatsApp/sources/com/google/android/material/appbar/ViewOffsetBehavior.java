package com.google.android.material.appbar;

import X.AbstractC009304r;
import X.AnonymousClass0B5;
import X.C018408o;
import X.C05660Ql;
import X.C12980iv;
import X.C92334Vm;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import java.util.List;

/* loaded from: classes2.dex */
public class ViewOffsetBehavior extends AbstractC009304r {
    public int A00 = 0;
    public C92334Vm A01;

    public ViewOffsetBehavior() {
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AbstractC009304r
    public boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
        int i2;
        if (!(this instanceof HeaderScrollingViewBehavior)) {
            coordinatorLayout.A0D(view, i);
        } else {
            HeaderScrollingViewBehavior headerScrollingViewBehavior = (HeaderScrollingViewBehavior) this;
            List A07 = coordinatorLayout.A07(view);
            int size = A07.size();
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    break;
                }
                View view2 = (View) A07.get(i3);
                if (!(view2 instanceof AppBarLayout)) {
                    i3++;
                } else if (view2 != null) {
                    AnonymousClass0B5 r5 = (AnonymousClass0B5) view.getLayoutParams();
                    Rect rect = headerScrollingViewBehavior.A02;
                    rect.set(coordinatorLayout.getPaddingLeft() + ((ViewGroup.MarginLayoutParams) r5).leftMargin, view2.getBottom() + ((ViewGroup.MarginLayoutParams) r5).topMargin, C12980iv.A08(coordinatorLayout) - ((ViewGroup.MarginLayoutParams) r5).rightMargin, ((coordinatorLayout.getHeight() + view2.getBottom()) - coordinatorLayout.getPaddingBottom()) - ((ViewGroup.MarginLayoutParams) r5).bottomMargin);
                    C018408o r2 = coordinatorLayout.A06;
                    if (r2 != null && coordinatorLayout.getFitsSystemWindows() && !view.getFitsSystemWindows()) {
                        rect.left += r2.A04();
                        rect.right -= r2.A05();
                    }
                    Rect rect2 = headerScrollingViewBehavior.A03;
                    int i4 = r5.A02;
                    if (i4 == 0) {
                        i4 = 8388659;
                    }
                    C05660Ql.A01(i4, view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
                    int i5 = 0;
                    if (headerScrollingViewBehavior.A00 != 0) {
                        float A0I = headerScrollingViewBehavior.A0I(view2);
                        int i6 = headerScrollingViewBehavior.A00;
                        int i7 = (int) (A0I * ((float) i6));
                        if (i7 >= 0) {
                            i5 = i7;
                            if (i7 > i6) {
                                i5 = i6;
                            }
                        }
                    }
                    view.layout(rect2.left, rect2.top - i5, rect2.right, rect2.bottom - i5);
                    i2 = rect2.top - view2.getBottom();
                }
            }
            coordinatorLayout.A0D(view, i);
            i2 = 0;
            headerScrollingViewBehavior.A01 = i2;
        }
        C92334Vm r22 = this.A01;
        if (r22 == null) {
            r22 = new C92334Vm(view);
            this.A01 = r22;
        }
        View view3 = r22.A03;
        r22.A01 = view3.getTop();
        r22.A00 = view3.getLeft();
        r22.A00();
        int i8 = this.A00;
        if (i8 == 0) {
            return true;
        }
        C92334Vm r1 = this.A01;
        if (r1.A02 != i8) {
            r1.A02 = i8;
            r1.A00();
        }
        this.A00 = 0;
        return true;
    }
}
