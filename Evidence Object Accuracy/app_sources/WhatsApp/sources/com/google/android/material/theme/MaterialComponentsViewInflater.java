package com.google.android.material.theme;

import X.AnonymousClass07P;
import X.AnonymousClass0BS;
import android.content.Context;
import android.util.AttributeSet;
import com.google.android.material.button.MaterialButton;

/* loaded from: classes3.dex */
public class MaterialComponentsViewInflater extends AnonymousClass07P {
    @Override // X.AnonymousClass07P
    public AnonymousClass0BS createButton(Context context, AttributeSet attributeSet) {
        return new MaterialButton(context, attributeSet);
    }
}
