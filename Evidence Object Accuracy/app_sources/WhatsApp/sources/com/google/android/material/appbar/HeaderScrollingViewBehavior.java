package com.google.android.material.appbar;

import X.AbstractC009304r;
import X.AnonymousClass0B5;
import X.C12980iv;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class HeaderScrollingViewBehavior extends ViewOffsetBehavior {
    public int A00;
    public int A01 = 0;
    public final Rect A02 = C12980iv.A0J();
    public final Rect A03 = C12980iv.A0J();

    public HeaderScrollingViewBehavior() {
    }

    public HeaderScrollingViewBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AbstractC009304r
    public boolean A07(View view, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4) {
        int totalScrollRange;
        int i5 = view.getLayoutParams().height;
        if (i5 == -1 || i5 == -2) {
            List A07 = coordinatorLayout.A07(view);
            int size = A07.size();
            int i6 = 0;
            while (true) {
                if (i6 >= size) {
                    break;
                }
                View view2 = (View) A07.get(i6);
                if (!(view2 instanceof AppBarLayout)) {
                    i6++;
                } else if (view2 != null) {
                    if (view2.getFitsSystemWindows() && !view.getFitsSystemWindows()) {
                        view.setFitsSystemWindows(true);
                        if (view.getFitsSystemWindows()) {
                            view.requestLayout();
                            return true;
                        }
                    }
                    int size2 = View.MeasureSpec.getSize(i3);
                    if (size2 == 0) {
                        size2 = coordinatorLayout.getHeight();
                    }
                    int measuredHeight = size2 - view2.getMeasuredHeight();
                    if (!(this instanceof AppBarLayout.ScrollingViewBehavior)) {
                        totalScrollRange = view2.getMeasuredHeight();
                    } else {
                        totalScrollRange = ((AppBarLayout) view2).getTotalScrollRange();
                    }
                    int i7 = measuredHeight + totalScrollRange;
                    int i8 = Integer.MIN_VALUE;
                    if (i5 == -1) {
                        i8 = 1073741824;
                    }
                    coordinatorLayout.A0E(view, i, i2, View.MeasureSpec.makeMeasureSpec(i7, i8));
                    return true;
                }
            }
        }
        return false;
    }

    public float A0I(View view) {
        int i;
        int i2;
        if (!(this instanceof AppBarLayout.ScrollingViewBehavior)) {
            return 1.0f;
        }
        if (!(view instanceof AppBarLayout)) {
            return 0.0f;
        }
        AppBarLayout appBarLayout = (AppBarLayout) view;
        int totalScrollRange = appBarLayout.getTotalScrollRange();
        int downNestedPreScrollRange = appBarLayout.getDownNestedPreScrollRange();
        AbstractC009304r r1 = ((AnonymousClass0B5) appBarLayout.getLayoutParams()).A0A;
        if (r1 instanceof AppBarLayout.BaseBehavior) {
            i = ((HeaderBehavior) r1).A0I();
        } else {
            i = 0;
        }
        if ((downNestedPreScrollRange == 0 || totalScrollRange + i > downNestedPreScrollRange) && (i2 = totalScrollRange - downNestedPreScrollRange) != 0) {
            return (((float) i) / ((float) i2)) + 1.0f;
        }
        return 0.0f;
    }
}
