package com.google.android.material.chip;

import X.AbstractC016707w;
import X.AbstractC115795Sy;
import X.AnonymousClass028;
import X.AnonymousClass02S;
import X.AnonymousClass06A;
import X.AnonymousClass08K;
import X.AnonymousClass0DW;
import X.AnonymousClass2RB;
import X.AnonymousClass2Zd;
import X.C015007d;
import X.C015607k;
import X.C016607v;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C50612Qf;
import X.C53672ep;
import X.C64353Ff;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.whatsapp.R;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* loaded from: classes2.dex */
public class Chip extends AppCompatCheckBox implements AbstractC115795Sy {
    public static final Rect A0D = C12980iv.A0J();
    public static final int[] A0E = {16842913};
    public int A00;
    public RippleDrawable A01;
    public View.OnClickListener A02;
    public CompoundButton.OnCheckedChangeListener A03;
    public AnonymousClass2Zd A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public final Rect A09;
    public final RectF A0A;
    public final AnonymousClass08K A0B;
    public final C53672ep A0C;

    public Chip(Context context) {
        this(context, null);
    }

    public Chip(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.chipStyle);
    }

    /* JADX WARNING: Removed duplicated region for block: B:76:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x024f  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0277  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x029a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Chip(android.content.Context r13, android.util.AttributeSet r14, int r15) {
        /*
        // Method dump skipped, instructions count: 731
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public final void A02() {
        AnonymousClass2Zd r2;
        Drawable drawable;
        if (!TextUtils.isEmpty(getText()) && (r2 = this.A04) != null) {
            float f = r2.A04 + r2.A01 + r2.A0C + r2.A0B;
            if ((r2.A0i && (drawable = r2.A0W) != null && (!(drawable instanceof AbstractC016707w) || ((C016607v) ((AbstractC016707w) drawable)).A02 != null)) || (r2.A0V != null && r2.A0h && isChecked())) {
                AnonymousClass2Zd r22 = this.A04;
                f += r22.A0A + r22.A09 + r22.A02;
            }
            AnonymousClass2Zd r23 = this.A04;
            if (r23.A0j && r23.A04() != null) {
                f += r23.A08 + r23.A06 + r23.A07;
            }
            if (((float) AnonymousClass028.A06(this)) != f) {
                AnonymousClass028.A0e(this, AnonymousClass028.A07(this), getPaddingTop(), (int) f, getPaddingBottom());
            }
        }
    }

    public final void A03(C64353Ff r4) {
        TextPaint paint = getPaint();
        paint.drawableState = this.A04.getState();
        r4.A01(getContext(), paint, this.A0B);
    }

    @Override // X.AbstractC115795Sy
    public void ANx() {
        A02();
        requestLayout();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    @Override // android.view.View
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        Field declaredField;
        C53672ep r4;
        if (motionEvent.getAction() == 10) {
            try {
                declaredField = AnonymousClass0DW.class.getDeclaredField("mHoveredVirtualViewId");
                declaredField.setAccessible(true);
                r4 = this.A0C;
            } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
                Log.e("Chip", "Unable to send Accessibility Exit event", e);
            }
            if (C12960it.A05(declaredField.get(r4)) != Integer.MIN_VALUE) {
                Method declaredMethod = AnonymousClass0DW.class.getDeclaredMethod("updateHoveredVirtualView", Integer.TYPE);
                declaredMethod.setAccessible(true);
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, Integer.MIN_VALUE, 0);
                declaredMethod.invoke(r4, objArr);
            }
        }
        return this.A0C.A0J(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.A0C.A0I(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.widget.TextView, android.widget.CompoundButton, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void drawableStateChanged() {
        /*
            r4 = this;
            super.drawableStateChanged()
            X.2Zd r0 = r4.A04
            if (r0 == 0) goto L_0x0089
            android.graphics.drawable.Drawable r0 = r0.A0X
            if (r0 == 0) goto L_0x0089
            boolean r0 = r0.isStateful()
            if (r0 == 0) goto L_0x0089
            X.2Zd r2 = r4.A04
            boolean r1 = r4.isEnabled()
            boolean r0 = r4.A05
            if (r0 == 0) goto L_0x001d
            int r1 = r1 + 1
        L_0x001d:
            boolean r0 = r4.A06
            if (r0 == 0) goto L_0x0023
            int r1 = r1 + 1
        L_0x0023:
            boolean r0 = r4.A07
            if (r0 == 0) goto L_0x0029
            int r1 = r1 + 1
        L_0x0029:
            boolean r0 = r4.isChecked()
            if (r0 == 0) goto L_0x0031
            int r1 = r1 + 1
        L_0x0031:
            int[] r1 = new int[r1]
            r3 = 0
            boolean r0 = r4.isEnabled()
            if (r0 == 0) goto L_0x0040
            r0 = 16842910(0x101009e, float:2.3694E-38)
            r1[r3] = r0
            r3 = 1
        L_0x0040:
            boolean r0 = r4.A05
            if (r0 == 0) goto L_0x004b
            r0 = 16842908(0x101009c, float:2.3693995E-38)
            r1[r3] = r0
            int r3 = r3 + 1
        L_0x004b:
            boolean r0 = r4.A06
            if (r0 == 0) goto L_0x0056
            r0 = 16843623(0x1010367, float:2.3696E-38)
            r1[r3] = r0
            int r3 = r3 + 1
        L_0x0056:
            boolean r0 = r4.A07
            if (r0 == 0) goto L_0x0061
            r0 = 16842919(0x10100a7, float:2.3694026E-38)
            r1[r3] = r0
            int r3 = r3 + 1
        L_0x0061:
            boolean r0 = r4.isChecked()
            if (r0 == 0) goto L_0x006c
            r0 = 16842913(0x10100a1, float:2.369401E-38)
            r1[r3] = r0
        L_0x006c:
            int[] r0 = r2.A0o
            boolean r0 = java.util.Arrays.equals(r0, r1)
            if (r0 != 0) goto L_0x0089
            r2.A0o = r1
            boolean r0 = r2.A0S()
            if (r0 == 0) goto L_0x0089
            int[] r0 = r2.getState()
            boolean r0 = r2.A0T(r0, r1)
            if (r0 == 0) goto L_0x0089
            r4.invalidate()
        L_0x0089:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.drawableStateChanged():void");
    }

    public Drawable getCheckedIcon() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0V;
        }
        return null;
    }

    public ColorStateList getChipBackgroundColor() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0L;
        }
        return null;
    }

    public float getChipCornerRadius() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A00;
        }
        return 0.0f;
    }

    public Drawable getChipDrawable() {
        return this.A04;
    }

    public float getChipEndPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A01;
        }
        return 0.0f;
    }

    public Drawable getChipIcon() {
        Drawable drawable;
        AnonymousClass2Zd r0 = this.A04;
        if (r0 == null || (drawable = r0.A0W) == null) {
            return null;
        }
        if (drawable instanceof AbstractC016707w) {
            return ((C016607v) ((AbstractC016707w) drawable)).A02;
        }
        return drawable;
    }

    public float getChipIconSize() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A02;
        }
        return 0.0f;
    }

    public ColorStateList getChipIconTint() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0M;
        }
        return null;
    }

    public float getChipMinHeight() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A03;
        }
        return 0.0f;
    }

    public float getChipStartPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A04;
        }
        return 0.0f;
    }

    public ColorStateList getChipStrokeColor() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0N;
        }
        return null;
    }

    public float getChipStrokeWidth() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A05;
        }
        return 0.0f;
    }

    @Deprecated
    public CharSequence getChipText() {
        return getText();
    }

    public Drawable getCloseIcon() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A04();
        }
        return null;
    }

    public CharSequence getCloseIconContentDescription() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0c;
        }
        return null;
    }

    public float getCloseIconEndPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A06;
        }
        return 0.0f;
    }

    public float getCloseIconSize() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A07;
        }
        return 0.0f;
    }

    public float getCloseIconStartPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A08;
        }
        return 0.0f;
    }

    public ColorStateList getCloseIconTint() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0O;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public RectF getCloseIconTouchBounds() {
        RectF rectF = this.A0A;
        rectF.setEmpty();
        AnonymousClass2Zd r4 = this.A04;
        if (!(r4 == null || r4.A04() == null)) {
            Rect bounds = r4.getBounds();
            rectF.setEmpty();
            if (r4.A0S()) {
                float f = r4.A01 + r4.A06 + r4.A07 + r4.A08 + r4.A0B;
                if (C015607k.A01(r4) == 0) {
                    float f2 = (float) bounds.right;
                    rectF.right = f2;
                    rectF.left = f2 - f;
                } else {
                    float f3 = (float) bounds.left;
                    rectF.left = f3;
                    rectF.right = f3 + f;
                }
                rectF.top = (float) bounds.top;
                rectF.bottom = (float) bounds.bottom;
            }
        }
        return rectF;
    }

    /* access modifiers changed from: private */
    public Rect getCloseIconTouchBoundsInt() {
        RectF closeIconTouchBounds = getCloseIconTouchBounds();
        Rect rect = this.A09;
        rect.set((int) closeIconTouchBounds.left, (int) closeIconTouchBounds.top, (int) closeIconTouchBounds.right, (int) closeIconTouchBounds.bottom);
        return rect;
    }

    @Override // android.widget.TextView
    public TextUtils.TruncateAt getEllipsize() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0Y;
        }
        return null;
    }

    @Override // android.widget.TextView, android.view.View
    public void getFocusedRect(Rect rect) {
        if (this.A00 == 0) {
            rect.set(getCloseIconTouchBoundsInt());
        } else {
            super.getFocusedRect(rect);
        }
    }

    public C50612Qf getHideMotionSpec() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0Z;
        }
        return null;
    }

    public float getIconEndPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A09;
        }
        return 0.0f;
    }

    public float getIconStartPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0A;
        }
        return 0.0f;
    }

    public ColorStateList getRippleColor() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0Q;
        }
        return null;
    }

    public C50612Qf getShowMotionSpec() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0a;
        }
        return null;
    }

    @Override // android.widget.TextView
    public CharSequence getText() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0d;
        }
        return "";
    }

    private C64353Ff getTextAppearance() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0b;
        }
        return null;
    }

    public float getTextEndPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0B;
        }
        return 0.0f;
    }

    public float getTextStartPadding() {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            return r0.A0C;
        }
        return 0.0f;
    }

    @Override // android.widget.TextView, android.widget.CompoundButton, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (isChecked()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, A0E);
        }
        return onCreateDrawableState;
    }

    @Override // android.widget.TextView, android.widget.CompoundButton, android.view.View
    public void onDraw(Canvas canvas) {
        AnonymousClass2Zd r0;
        if (TextUtils.isEmpty(getText()) || (r0 = this.A04) == null || r0.A0l) {
            super.onDraw(canvas);
            return;
        }
        int save = canvas.save();
        float chipStartPadding = getChipStartPadding() + this.A04.A02() + getTextStartPadding();
        if (AnonymousClass028.A05(this) != 0) {
            chipStartPadding = -chipStartPadding;
        }
        canvas.translate(chipStartPadding, 0.0f);
        super.onDraw(canvas);
        canvas.restoreToCount(save);
    }

    @Override // android.widget.TextView, android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        int i2 = Integer.MIN_VALUE;
        if (z) {
            i2 = -1;
        }
        setFocusedVirtualView(i2);
        invalidate();
        super.onFocusChanged(z, i, rect);
        this.A0C.A0E(z, i, rect);
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        boolean contains;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 7) {
            if (actionMasked == 10) {
                contains = false;
            }
            return super.onHoverEvent(motionEvent);
        }
        contains = getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY());
        setCloseIconHovered(contains);
        return super.onHoverEvent(motionEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c  */
    @Override // android.widget.TextView, android.view.KeyEvent.Callback, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onKeyDown(int r6, android.view.KeyEvent r7) {
        /*
            r5 = this;
            int r1 = r7.getKeyCode()
            r0 = 61
            r3 = 1
            if (r1 == r0) goto L_0x006a
            r0 = 66
            if (r1 == r0) goto L_0x0052
            switch(r1) {
                case 21: goto L_0x0025;
                case 22: goto L_0x0015;
                case 23: goto L_0x0052;
                default: goto L_0x0010;
            }
        L_0x0010:
            boolean r0 = super.onKeyDown(r6, r7)
            return r0
        L_0x0015:
            boolean r0 = r7.hasNoModifiers()
            if (r0 == 0) goto L_0x0010
            int r0 = X.AnonymousClass028.A05(r5)
            r4 = 1
            if (r0 == r3) goto L_0x0023
            r4 = 0
        L_0x0023:
            r4 = r4 ^ r3
            goto L_0x0033
        L_0x0025:
            boolean r0 = r7.hasNoModifiers()
            if (r0 == 0) goto L_0x0010
            int r0 = X.AnonymousClass028.A05(r5)
            r4 = 1
            if (r0 == r3) goto L_0x0033
            r4 = 0
        L_0x0033:
            int r1 = r5.A00
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != r0) goto L_0x003d
            r0 = -1
            r5.setFocusedVirtualView(r0)
        L_0x003d:
            r2 = -1
            r1 = 0
            int r0 = r5.A00
            if (r4 == 0) goto L_0x004c
            if (r0 != r2) goto L_0x0010
            r5.setFocusedVirtualView(r1)
        L_0x0048:
            r5.invalidate()
            return r3
        L_0x004c:
            if (r0 != 0) goto L_0x0010
            r5.setFocusedVirtualView(r2)
            goto L_0x0048
        L_0x0052:
            int r1 = r5.A00
            r0 = -1
            if (r1 == r0) goto L_0x0090
            if (r1 != 0) goto L_0x0010
            r1 = 0
            r5.playSoundEffect(r1)
            android.view.View$OnClickListener r0 = r5.A02
            if (r0 == 0) goto L_0x0064
            r0.onClick(r5)
        L_0x0064:
            X.2ep r0 = r5.A0C
            r0.A0A(r1, r3)
            return r3
        L_0x006a:
            boolean r0 = r7.hasNoModifiers()
            if (r0 == 0) goto L_0x0088
            r4 = 2
        L_0x0071:
            android.view.ViewParent r2 = r5.getParent()
            r1 = r5
        L_0x0076:
            android.view.View r1 = r1.focusSearch(r4)
            if (r1 == 0) goto L_0x0010
            if (r1 == r5) goto L_0x0084
            android.view.ViewParent r0 = r1.getParent()
            if (r0 == r2) goto L_0x0076
        L_0x0084:
            r1.requestFocus()
            return r3
        L_0x0088:
            boolean r0 = r7.hasModifiers(r3)
            if (r0 == 0) goto L_0x0010
            r4 = 1
            goto L_0x0071
        L_0x0090:
            r5.performClick()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    @Override // android.widget.TextView, android.widget.Button, android.view.View
    public PointerIcon onResolvePointerIcon(MotionEvent motionEvent, int i) {
        if (!getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()) || !isEnabled()) {
            return null;
        }
        return PointerIcon.getSystemIcon(getContext(), 1002);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004a, code lost:
        if (r0 != false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        if (r3 != 3) goto L_0x0020;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    @Override // android.widget.TextView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r5) {
        /*
            r4 = this;
            int r3 = r5.getActionMasked()
            android.graphics.RectF r2 = r4.getCloseIconTouchBounds()
            float r1 = r5.getX()
            float r0 = r5.getY()
            boolean r1 = r2.contains(r1, r0)
            r2 = 0
            r0 = 1
            if (r3 == 0) goto L_0x004f
            if (r3 == r0) goto L_0x0032
            r0 = 2
            if (r3 == r0) goto L_0x0028
            r0 = 3
            if (r3 == r0) goto L_0x004d
        L_0x0020:
            boolean r0 = super.onTouchEvent(r5)
            if (r0 == 0) goto L_0x0027
        L_0x0026:
            r2 = 1
        L_0x0027:
            return r2
        L_0x0028:
            boolean r0 = r4.A07
            if (r0 == 0) goto L_0x0020
            if (r1 != 0) goto L_0x0026
            r4.setCloseIconPressed(r2)
            goto L_0x0026
        L_0x0032:
            boolean r0 = r4.A07
            if (r0 == 0) goto L_0x004d
            r4.playSoundEffect(r2)
            android.view.View$OnClickListener r0 = r4.A02
            r1 = 1
            if (r0 == 0) goto L_0x0041
            r0.onClick(r4)
        L_0x0041:
            X.2ep r0 = r4.A0C
            r0.A0A(r2, r1)
            r0 = 1
        L_0x0047:
            r4.setCloseIconPressed(r2)
            if (r0 != 0) goto L_0x0026
            goto L_0x0020
        L_0x004d:
            r0 = 0
            goto L_0x0047
        L_0x004f:
            if (r1 == 0) goto L_0x0020
            r4.setCloseIconPressed(r0)
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        if (drawable == this.A04 || drawable == this.A01) {
            super.setBackground(drawable);
            return;
        }
        throw C12980iv.A0u("Do not set the background; Chip manages its own background drawable.");
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        throw C12980iv.A0u("Do not set the background color; Chip manages its own background drawable.");
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        if (drawable == this.A04 || drawable == this.A01) {
            super.setBackgroundDrawable(drawable);
            return;
        }
        throw C12980iv.A0u("Do not set the background drawable; Chip manages its own background drawable.");
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.view.View
    public void setBackgroundResource(int i) {
        throw C12980iv.A0u("Do not set the background resource; Chip manages its own background drawable.");
    }

    @Override // android.view.View
    public void setBackgroundTintList(ColorStateList colorStateList) {
        throw C12980iv.A0u("Do not set the background tint list; Chip manages its own background drawable.");
    }

    @Override // android.view.View
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        throw C12980iv.A0u("Do not set the background tint mode; Chip manages its own background drawable.");
    }

    public void setCheckable(boolean z) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0M(z);
        }
    }

    public void setCheckableResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0M(r1.A0p.getResources().getBoolean(i));
        }
    }

    @Override // android.widget.CompoundButton, android.widget.Checkable
    public void setChecked(boolean z) {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
        AnonymousClass2Zd r0 = this.A04;
        if (r0 == null) {
            this.A08 = z;
        } else if (r0.A0g) {
            boolean isChecked = isChecked();
            super.setChecked(z);
            if (isChecked != z && (onCheckedChangeListener = this.A03) != null) {
                onCheckedChangeListener.onCheckedChanged(this, z);
            }
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0G(drawable);
        }
    }

    @Deprecated
    public void setCheckedIconEnabled(boolean z) {
        setCheckedIconVisible(z);
    }

    @Deprecated
    public void setCheckedIconEnabledResource(int i) {
        setCheckedIconVisible(i);
    }

    public void setCheckedIconResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0G(C015007d.A01(r1.A0p, i));
        }
    }

    public void setCheckedIconVisible(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0N(r1.A0p.getResources().getBoolean(i));
        }
    }

    public void setCheckedIconVisible(boolean z) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0N(z);
        }
    }

    public void setChipBackgroundColor(ColorStateList colorStateList) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A0L != colorStateList) {
            r1.A0L = colorStateList;
            AnonymousClass2Zd.A01(r1);
        }
    }

    public void setChipBackgroundColorResource(int i) {
        ColorStateList A00;
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null && r2.A0L != (A00 = C015007d.A00(r2.A0p, i))) {
            r2.A0L = A00;
            AnonymousClass2Zd.A01(r2);
        }
    }

    public void setChipCornerRadius(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A00 != f) {
            r1.A00 = f;
            r1.invalidateSelf();
        }
    }

    public void setChipCornerRadiusResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A00 != A00) {
                r2.A00 = A00;
                r2.invalidateSelf();
            }
        }
    }

    public void setChipDrawable(AnonymousClass2Zd r5) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != r5) {
            if (r1 != null) {
                r1.A0f = C12970iu.A10(null);
            }
            this.A04 = r5;
            r5.A0f = C12970iu.A10(this);
            if (AnonymousClass2RB.A00) {
                this.A01 = new RippleDrawable(AnonymousClass2RB.A02(this.A04.A0Q), this.A04, null);
                AnonymousClass2Zd r2 = this.A04;
                if (r2.A0n) {
                    r2.A0n = false;
                    r2.A0P = null;
                    AnonymousClass2Zd.A01(r2);
                }
                setBackground(this.A01);
                return;
            }
            AnonymousClass2Zd r22 = this.A04;
            if (!r22.A0n) {
                r22.A0n = true;
                r22.A0P = AnonymousClass2RB.A02(r22.A0Q);
                AnonymousClass2Zd.A01(r22);
            }
            setBackground(this.A04);
        }
    }

    public void setChipEndPadding(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A01 != f) {
            r1.A01 = f;
            r1.invalidateSelf();
            r1.A05();
        }
    }

    public void setChipEndPaddingResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A01 != A00) {
                r2.A01 = A00;
                r2.invalidateSelf();
                r2.A05();
            }
        }
    }

    public void setChipIcon(Drawable drawable) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0H(drawable);
        }
    }

    @Deprecated
    public void setChipIconEnabled(boolean z) {
        setChipIconVisible(z);
    }

    @Deprecated
    public void setChipIconEnabledResource(int i) {
        setChipIconVisible(i);
    }

    public void setChipIconResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0H(C015007d.A01(r1.A0p, i));
        }
    }

    public void setChipIconSize(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A06(f);
        }
    }

    public void setChipIconSizeResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A06(AnonymousClass2Zd.A00(r1, i));
        }
    }

    public void setChipIconTint(ColorStateList colorStateList) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0C(colorStateList);
        }
    }

    public void setChipIconTintResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0C(C015007d.A00(r1.A0p, i));
        }
    }

    public void setChipIconVisible(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0O(r1.A0p.getResources().getBoolean(i));
        }
    }

    public void setChipIconVisible(boolean z) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0O(z);
        }
    }

    public void setChipMinHeight(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A03 != f) {
            r1.A03 = f;
            r1.invalidateSelf();
            r1.A05();
        }
    }

    public void setChipMinHeightResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A03 != A00) {
                r2.A03 = A00;
                r2.invalidateSelf();
                r2.A05();
            }
        }
    }

    public void setChipStartPadding(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A04 != f) {
            r1.A04 = f;
            r1.invalidateSelf();
            r1.A05();
        }
    }

    public void setChipStartPaddingResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A04 != A00) {
                r2.A04 = A00;
                r2.invalidateSelf();
                r2.A05();
            }
        }
    }

    public void setChipStrokeColor(ColorStateList colorStateList) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A0N != colorStateList) {
            r1.A0N = colorStateList;
            AnonymousClass2Zd.A01(r1);
        }
    }

    public void setChipStrokeColorResource(int i) {
        ColorStateList A00;
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null && r2.A0N != (A00 = C015007d.A00(r2.A0p, i))) {
            r2.A0N = A00;
            AnonymousClass2Zd.A01(r2);
        }
    }

    public void setChipStrokeWidth(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A05 != f) {
            r1.A05 = f;
            r1.A0r.setStrokeWidth(f);
            r1.invalidateSelf();
        }
    }

    public void setChipStrokeWidthResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A05 != A00) {
                r2.A05 = A00;
                r2.A0r.setStrokeWidth(A00);
                r2.invalidateSelf();
            }
        }
    }

    @Deprecated
    public void setChipText(CharSequence charSequence) {
        setText(charSequence);
    }

    @Deprecated
    public void setChipTextResource(int i) {
        setText(getResources().getString(i));
    }

    public void setCloseIcon(Drawable drawable) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0I(drawable);
        }
    }

    public void setCloseIconContentDescription(CharSequence charSequence) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null && r2.A0c != charSequence) {
            AnonymousClass02S A00 = new AnonymousClass06A().A00();
            r2.A0c = A00.A02(A00.A01, charSequence);
            r2.invalidateSelf();
        }
    }

    @Deprecated
    public void setCloseIconEnabled(boolean z) {
        setCloseIconVisible(z);
    }

    @Deprecated
    public void setCloseIconEnabledResource(int i) {
        setCloseIconVisible(i);
    }

    public void setCloseIconEndPadding(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A07(f);
        }
    }

    public void setCloseIconEndPaddingResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A07(AnonymousClass2Zd.A00(r1, i));
        }
    }

    private void setCloseIconFocused(boolean z) {
        if (this.A05 != z) {
            this.A05 = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconHovered(boolean z) {
        if (this.A06 != z) {
            this.A06 = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconPressed(boolean z) {
        if (this.A07 != z) {
            this.A07 = z;
            refreshDrawableState();
        }
    }

    public void setCloseIconResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0I(C015007d.A01(r1.A0p, i));
        }
    }

    public void setCloseIconSize(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A08(f);
        }
    }

    public void setCloseIconSizeResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A08(AnonymousClass2Zd.A00(r1, i));
        }
    }

    public void setCloseIconStartPadding(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A09(f);
        }
    }

    public void setCloseIconStartPaddingResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A09(AnonymousClass2Zd.A00(r1, i));
        }
    }

    public void setCloseIconTint(ColorStateList colorStateList) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0D(colorStateList);
        }
    }

    public void setCloseIconTintResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0D(C015007d.A00(r1.A0p, i));
        }
    }

    public void setCloseIconVisible(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0P(r1.A0p.getResources().getBoolean(i));
        }
    }

    public void setCloseIconVisible(boolean z) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0P(z);
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw C12980iv.A0u("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        } else {
            throw C12980iv.A0u("Please set end drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw C12980iv.A0u("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        } else {
            throw C12980iv.A0u("Please set end drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        if (i != 0) {
            throw C12980iv.A0u("Please set start drawable using R.attr#chipIcon.");
        } else if (i3 == 0) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(i, i2, i3, i4);
        } else {
            throw C12980iv.A0u("Please set end drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw C12980iv.A0u("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw C12980iv.A0u("Please set end drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        if (i != 0) {
            throw C12980iv.A0u("Please set start drawable using R.attr#chipIcon.");
        } else if (i3 == 0) {
            super.setCompoundDrawablesWithIntrinsicBounds(i, i2, i3, i4);
        } else {
            throw C12980iv.A0u("Please set end drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw C12980iv.A0u("Please set left drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw C12980iv.A0u("Please set right drawable using R.attr#closeIcon.");
        }
    }

    @Override // android.widget.TextView
    public void setEllipsize(TextUtils.TruncateAt truncateAt) {
        if (this.A04 == null) {
            return;
        }
        if (truncateAt != TextUtils.TruncateAt.MARQUEE) {
            super.setEllipsize(truncateAt);
            AnonymousClass2Zd r0 = this.A04;
            if (r0 != null) {
                r0.A0Y = truncateAt;
                return;
            }
            return;
        }
        throw C12980iv.A0u("Text within a chip are not allowed to scroll.");
    }

    private void setFocusedVirtualView(int i) {
        int i2 = this.A00;
        if (i2 != i) {
            if (i2 == 0) {
                setCloseIconFocused(false);
            }
            this.A00 = i;
            if (i == 0) {
                setCloseIconFocused(true);
            }
        }
    }

    @Override // android.widget.TextView
    public void setGravity(int i) {
        if (i != 8388627) {
            Log.w("Chip", "Chip text must be vertically center and start aligned");
        } else {
            super.setGravity(i);
        }
    }

    public void setHideMotionSpec(C50612Qf r2) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0Z = r2;
        }
    }

    public void setHideMotionSpecResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0Z = C50612Qf.A00(r1.A0p, i);
        }
    }

    public void setIconEndPadding(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0A(f);
        }
    }

    public void setIconEndPaddingResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0A(AnonymousClass2Zd.A00(r1, i));
        }
    }

    public void setIconStartPadding(float f) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0B(f);
        }
    }

    public void setIconStartPaddingResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0B(AnonymousClass2Zd.A00(r1, i));
        }
    }

    @Override // android.widget.TextView
    public void setLines(int i) {
        if (i <= 1) {
            super.setLines(i);
            return;
        }
        throw C12980iv.A0u("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setMaxLines(int i) {
        if (i <= 1) {
            super.setMaxLines(i);
            return;
        }
        throw C12980iv.A0u("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setMaxWidth(int i) {
        super.setMaxWidth(i);
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0K = i;
        }
    }

    @Override // android.widget.TextView
    public void setMinLines(int i) {
        if (i <= 1) {
            super.setMinLines(i);
            return;
        }
        throw C12980iv.A0u("Chip does not support multi-line text");
    }

    public void setOnCheckedChangeListenerInternal(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.A03 = onCheckedChangeListener;
    }

    public void setOnCloseIconClickListener(View.OnClickListener onClickListener) {
        this.A02 = onClickListener;
    }

    public void setRippleColor(ColorStateList colorStateList) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0E(colorStateList);
        }
    }

    public void setRippleColorResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0E(C015007d.A00(r1.A0p, i));
        }
    }

    public void setShowMotionSpec(C50612Qf r2) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0a = r2;
        }
    }

    public void setShowMotionSpecResource(int i) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null) {
            r1.A0a = C50612Qf.A00(r1.A0p, i);
        }
    }

    @Override // android.widget.TextView
    public void setSingleLine(boolean z) {
        if (z) {
            super.setSingleLine(z);
            return;
        }
        throw C12980iv.A0u("Chip does not support multi-line text");
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.A04 != null) {
            if (charSequence == null) {
                charSequence = "";
            }
            AnonymousClass02S A00 = new AnonymousClass06A().A00();
            CharSequence A02 = A00.A02(A00.A01, charSequence);
            if (this.A04.A0l) {
                A02 = null;
            }
            super.setText(A02, bufferType);
            AnonymousClass2Zd r0 = this.A04;
            if (r0 != null) {
                r0.A0L(charSequence);
            }
        }
    }

    @Override // android.widget.TextView
    public void setTextAppearance(int i) {
        super.setTextAppearance(i);
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            r2.A0K(new C64353Ff(r2.A0p, i));
        }
        C64353Ff textAppearance = getTextAppearance();
        if (textAppearance != null) {
            textAppearance.A02(getContext(), getPaint(), this.A0B);
            A03(getTextAppearance());
        }
    }

    public void setTextAppearance(C64353Ff r5) {
        AnonymousClass2Zd r0 = this.A04;
        if (r0 != null) {
            r0.A0K(r5);
        }
        C64353Ff textAppearance = getTextAppearance();
        if (textAppearance != null) {
            textAppearance.A02(getContext(), getPaint(), this.A0B);
            A03(r5);
        }
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            r2.A0K(new C64353Ff(r2.A0p, i));
        }
        C64353Ff textAppearance = getTextAppearance();
        if (textAppearance != null) {
            textAppearance.A02(context, getPaint(), this.A0B);
            A03(getTextAppearance());
        }
    }

    public void setTextAppearanceResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            r2.A0K(new C64353Ff(r2.A0p, i));
        }
        setTextAppearance(getContext(), i);
    }

    public void setTextEndPadding(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A0B != f) {
            r1.A0B = f;
            r1.invalidateSelf();
            r1.A05();
        }
    }

    public void setTextEndPaddingResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A0B != A00) {
                r2.A0B = A00;
                r2.invalidateSelf();
                r2.A05();
            }
        }
    }

    public void setTextStartPadding(float f) {
        AnonymousClass2Zd r1 = this.A04;
        if (r1 != null && r1.A0C != f) {
            r1.A0C = f;
            r1.invalidateSelf();
            r1.A05();
        }
    }

    public void setTextStartPaddingResource(int i) {
        AnonymousClass2Zd r2 = this.A04;
        if (r2 != null) {
            float A00 = AnonymousClass2Zd.A00(r2, i);
            if (r2.A0C != A00) {
                r2.A0C = A00;
                r2.invalidateSelf();
                r2.A05();
            }
        }
    }
}
