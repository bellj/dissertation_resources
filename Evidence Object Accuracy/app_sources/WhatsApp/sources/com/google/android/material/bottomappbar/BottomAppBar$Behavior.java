package com.google.android.material.bottomappbar;

import X.C12980iv;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;

/* loaded from: classes3.dex */
public class BottomAppBar$Behavior extends HideBottomViewOnScrollBehavior {
    public final Rect A00 = new Rect();

    public BottomAppBar$Behavior() {
    }

    public BottomAppBar$Behavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AbstractC009304r
    public /* bridge */ /* synthetic */ boolean A0E(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i, int i2) {
        throw C12980iv.A0n("getHideOnScroll");
    }

    @Override // com.google.android.material.behavior.HideBottomViewOnScrollBehavior, X.AbstractC009304r
    public /* bridge */ /* synthetic */ boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
        throw C12980iv.A0n("findDependentFab");
    }

    @Override // com.google.android.material.behavior.HideBottomViewOnScrollBehavior
    public /* bridge */ /* synthetic */ void A0I(View view) {
        super.A0I(null);
        throw C12980iv.A0n("findDependentFab");
    }

    @Override // com.google.android.material.behavior.HideBottomViewOnScrollBehavior
    public /* bridge */ /* synthetic */ void A0J(View view) {
        super.A0J(null);
        throw C12980iv.A0n("findDependentFab");
    }
}
