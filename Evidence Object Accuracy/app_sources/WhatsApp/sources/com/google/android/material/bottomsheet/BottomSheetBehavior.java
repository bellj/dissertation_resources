package com.google.android.material.bottomsheet;

import X.AbstractC009304r;
import X.AnonymousClass028;
import X.AnonymousClass04X;
import X.AnonymousClass0B5;
import X.AnonymousClass2UH;
import X.C06260Su;
import X.C50572Qb;
import X.C53702et;
import X.C74423hx;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class BottomSheetBehavior extends AbstractC009304r {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B = 4;
    public VelocityTracker A0C;
    public C06260Su A0D;
    public AnonymousClass2UH A0E;
    public WeakReference A0F;
    public WeakReference A0G;
    public Map A0H;
    public boolean A0I = true;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public final AnonymousClass04X A0P = new C53702et(this);

    public BottomSheetBehavior() {
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int dimensionPixelSize;
        int i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A02);
        TypedValue peekValue = obtainStyledAttributes.peekValue(2);
        A0L((peekValue == null || (dimensionPixelSize = peekValue.data) != -1) ? obtainStyledAttributes.getDimensionPixelSize(2, -1) : dimensionPixelSize);
        this.A0J = obtainStyledAttributes.getBoolean(1, false);
        boolean z = obtainStyledAttributes.getBoolean(0, true);
        if (this.A0I != z) {
            this.A0I = z;
            if (this.A0G != null) {
                int i2 = this.A08 - this.A07;
                this.A02 = z ? Math.max(i2, this.A03) : i2;
            }
            if (!z || this.A0B != 6) {
                i = this.A0B;
            } else {
                i = 3;
            }
            A0K(i);
        }
        this.A0N = obtainStyledAttributes.getBoolean(3, false);
        obtainStyledAttributes.recycle();
        this.A00 = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    public static BottomSheetBehavior A00(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof AnonymousClass0B5) {
            AbstractC009304r r1 = ((AnonymousClass0B5) layoutParams).A0A;
            if (r1 instanceof BottomSheetBehavior) {
                return (BottomSheetBehavior) r1;
            }
            throw new IllegalArgumentException("The view is not associated with BottomSheetBehavior");
        }
        throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
    }

    @Override // X.AbstractC009304r
    public Parcelable A08(View view, CoordinatorLayout coordinatorLayout) {
        return new C74423hx(View.BaseSavedState.EMPTY_STATE, this.A0B);
    }

    @Override // X.AbstractC009304r
    public void A09(Parcelable parcelable, View view, CoordinatorLayout coordinatorLayout) {
        int i = ((C74423hx) parcelable).A00;
        if (i == 1 || i == 2) {
            i = 4;
        }
        this.A0B = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007d, code lost:
        if (r1 < java.lang.Math.abs(r3 - r2)) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x009d, code lost:
        if (r1 < java.lang.Math.abs(r3 - r2)) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a4, code lost:
        r5 = r2;
     */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(android.view.View r9, android.view.View r10, androidx.coordinatorlayout.widget.CoordinatorLayout r11, int r12) {
        /*
            r8 = this;
            int r0 = r9.getTop()
            boolean r7 = r8.A0I
            if (r7 == 0) goto L_0x00a7
            int r5 = r8.A03
        L_0x000a:
            r6 = 3
            if (r0 != r5) goto L_0x0011
            r8.A0K(r6)
        L_0x0010:
            return
        L_0x0011:
            java.lang.ref.WeakReference r0 = r8.A0F
            java.lang.Object r0 = r0.get()
            if (r10 != r0) goto L_0x0010
            boolean r0 = r8.A0L
            if (r0 == 0) goto L_0x0010
            int r0 = r8.A06
            r4 = 0
            if (r0 > 0) goto L_0x0034
            boolean r0 = r8.A0J
            if (r0 == 0) goto L_0x0064
            android.view.VelocityTracker r2 = r8.A0C
            if (r2 != 0) goto L_0x0054
            r0 = 0
        L_0x002b:
            boolean r0 = r8.A0O(r9, r0)
            if (r0 == 0) goto L_0x0064
            int r5 = r8.A08
            r6 = 5
        L_0x0034:
            X.0Su r1 = r8.A0D
            int r0 = r9.getLeft()
            boolean r0 = r1.A0H(r9, r0, r5)
            if (r0 == 0) goto L_0x0050
            r0 = 2
            r8.A0K(r0)
            r1 = 2
            com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0
            r0.<init>(r9, r8, r6, r1)
            r9.postOnAnimation(r0)
        L_0x004d:
            r8.A0L = r4
            return
        L_0x0050:
            r8.A0K(r6)
            goto L_0x004d
        L_0x0054:
            r1 = 1000(0x3e8, float:1.401E-42)
            float r0 = r8.A00
            r2.computeCurrentVelocity(r1, r0)
            android.view.VelocityTracker r1 = r8.A0C
            int r0 = r8.A01
            float r0 = r1.getYVelocity(r0)
            goto L_0x002b
        L_0x0064:
            int r0 = r8.A06
            if (r0 != 0) goto L_0x00a1
            int r3 = r9.getTop()
            if (r7 == 0) goto L_0x0080
            int r5 = r8.A03
            int r0 = r3 - r5
            int r1 = java.lang.Math.abs(r0)
            int r2 = r8.A02
            int r3 = r3 - r2
            int r0 = java.lang.Math.abs(r3)
            if (r1 >= r0) goto L_0x00a4
            goto L_0x0034
        L_0x0080:
            int r5 = r8.A04
            if (r3 >= r5) goto L_0x0090
            int r0 = r8.A02
            int r0 = r3 - r0
            int r0 = java.lang.Math.abs(r0)
            if (r3 >= r0) goto L_0x009f
            r5 = 0
            goto L_0x0034
        L_0x0090:
            int r0 = r3 - r5
            int r1 = java.lang.Math.abs(r0)
            int r2 = r8.A02
            int r3 = r3 - r2
            int r0 = java.lang.Math.abs(r3)
            if (r1 >= r0) goto L_0x00a4
        L_0x009f:
            r6 = 6
            goto L_0x0034
        L_0x00a1:
            int r5 = r8.A02
            goto L_0x00a5
        L_0x00a4:
            r5 = r2
        L_0x00a5:
            r6 = 4
            goto L_0x0034
        L_0x00a7:
            r5 = 0
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomsheet.BottomSheetBehavior.A0A(android.view.View, android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout, int):void");
    }

    @Override // X.AbstractC009304r
    public void A0B(View view, View view2, CoordinatorLayout coordinatorLayout, int[] iArr, int i, int i2, int i3) {
        int i4;
        int i5;
        if (i3 != 1 && view2 == this.A0F.get()) {
            int top = view.getTop();
            int i6 = top - i2;
            if (i2 > 0) {
                if (this.A0I) {
                    i5 = this.A03;
                } else {
                    i5 = 0;
                }
                if (i6 < i5) {
                    int i7 = top - i5;
                    iArr[1] = i7;
                    AnonymousClass028.A0Y(view, -i7);
                    i4 = 3;
                    A0K(i4);
                }
                iArr[1] = i2;
                AnonymousClass028.A0Y(view, -i2);
                A0K(1);
            } else if (i2 < 0 && !view2.canScrollVertically(-1)) {
                int i8 = this.A02;
                if (i6 > i8 && !this.A0J) {
                    int i9 = top - i8;
                    iArr[1] = i9;
                    AnonymousClass028.A0Y(view, -i9);
                    i4 = 4;
                    A0K(i4);
                }
                iArr[1] = i2;
                AnonymousClass028.A0Y(view, -i2);
                A0K(1);
            }
            A0J(view.getTop());
            this.A06 = i2;
            this.A0L = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0083, code lost:
        if (r11.A0I(r10, r1, r8.A05) != false) goto L_0x0085;
     */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0C(android.view.MotionEvent r9, android.view.View r10, androidx.coordinatorlayout.widget.CoordinatorLayout r11) {
        /*
            r8 = this;
            boolean r0 = r10.isShown()
            r2 = 0
            r4 = 1
            if (r0 != 0) goto L_0x000b
            r8.A0K = r4
        L_0x000a:
            return r2
        L_0x000b:
            int r5 = r9.getActionMasked()
            if (r5 != 0) goto L_0x001e
            r0 = -1
            r8.A01 = r0
            android.view.VelocityTracker r0 = r8.A0C
            if (r0 == 0) goto L_0x001e
            r0.recycle()
            r0 = 0
            r8.A0C = r0
        L_0x001e:
            android.view.VelocityTracker r0 = r8.A0C
            if (r0 != 0) goto L_0x0028
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r8.A0C = r0
        L_0x0028:
            r0.addMovement(r9)
            r3 = 0
            r6 = -1
            if (r5 == 0) goto L_0x004e
            if (r5 == r4) goto L_0x0043
            r0 = 3
            if (r5 == r0) goto L_0x0043
        L_0x0034:
            boolean r0 = r8.A0K
            if (r0 != 0) goto L_0x0089
            X.0Su r0 = r8.A0D
            if (r0 == 0) goto L_0x0089
            boolean r0 = r0.A0E(r9)
            if (r0 == 0) goto L_0x0089
            return r4
        L_0x0043:
            r8.A0O = r2
            r8.A01 = r6
            boolean r0 = r8.A0K
            if (r0 == 0) goto L_0x0034
            r8.A0K = r2
            return r2
        L_0x004e:
            float r0 = r9.getX()
            int r1 = (int) r0
            float r0 = r9.getY()
            int r7 = (int) r0
            r8.A05 = r7
            java.lang.ref.WeakReference r0 = r8.A0F
            if (r0 == 0) goto L_0x0078
            java.lang.Object r0 = r0.get()
            android.view.View r0 = (android.view.View) r0
            if (r0 == 0) goto L_0x0078
            boolean r0 = r11.A0I(r0, r1, r7)
            if (r0 == 0) goto L_0x0078
            int r0 = r9.getActionIndex()
            int r0 = r9.getPointerId(r0)
            r8.A01 = r0
            r8.A0O = r4
        L_0x0078:
            int r0 = r8.A01
            if (r0 != r6) goto L_0x0085
            int r0 = r8.A05
            boolean r1 = r11.A0I(r10, r1, r0)
            r0 = 1
            if (r1 == 0) goto L_0x0086
        L_0x0085:
            r0 = 0
        L_0x0086:
            r8.A0K = r0
            goto L_0x0034
        L_0x0089:
            java.lang.ref.WeakReference r0 = r8.A0F
            if (r0 == 0) goto L_0x0093
            java.lang.Object r3 = r0.get()
            android.view.View r3 = (android.view.View) r3
        L_0x0093:
            r0 = 2
            if (r5 != r0) goto L_0x000a
            if (r3 == 0) goto L_0x000a
            boolean r0 = r8.A0K
            if (r0 != 0) goto L_0x000a
            int r0 = r8.A0B
            if (r0 == r4) goto L_0x000a
            float r0 = r9.getX()
            int r1 = (int) r0
            float r0 = r9.getY()
            int r0 = (int) r0
            boolean r0 = r11.A0I(r3, r1, r0)
            if (r0 != 0) goto L_0x000a
            X.0Su r0 = r8.A0D
            if (r0 == 0) goto L_0x000a
            int r0 = r8.A05
            float r1 = (float) r0
            float r0 = r9.getY()
            float r1 = r1 - r0
            float r1 = java.lang.Math.abs(r1)
            X.0Su r0 = r8.A0D
            int r0 = r0.A06
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x000a
            r2 = 1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomsheet.BottomSheetBehavior.A0C(android.view.MotionEvent, android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout):boolean");
    }

    @Override // X.AbstractC009304r
    public boolean A0D(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        if (!view.isShown()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (this.A0B == 1 && actionMasked == 0) {
            return true;
        }
        C06260Su r0 = this.A0D;
        if (r0 != null) {
            r0.A07(motionEvent);
        }
        if (actionMasked == 0) {
            this.A01 = -1;
            VelocityTracker velocityTracker = this.A0C;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.A0C = null;
            }
        }
        VelocityTracker velocityTracker2 = this.A0C;
        if (velocityTracker2 == null) {
            velocityTracker2 = VelocityTracker.obtain();
            this.A0C = velocityTracker2;
        }
        velocityTracker2.addMovement(motionEvent);
        if (actionMasked == 2 && !this.A0K) {
            float abs = Math.abs(((float) this.A05) - motionEvent.getY());
            C06260Su r1 = this.A0D;
            if (abs > ((float) r1.A06)) {
                r1.A09(view, motionEvent.getPointerId(motionEvent.getActionIndex()));
            }
        }
        return !this.A0K;
    }

    @Override // X.AbstractC009304r
    public boolean A0E(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i, int i2) {
        this.A06 = 0;
        this.A0L = false;
        if ((i & 2) != 0) {
            return true;
        }
        return false;
    }

    @Override // X.AbstractC009304r
    public boolean A0F(View view, View view2, CoordinatorLayout coordinatorLayout, float f, float f2) {
        if (view2 != this.A0F.get() || this.A0B == 3) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0076  */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0G(android.view.View r11, androidx.coordinatorlayout.widget.CoordinatorLayout r12, int r13) {
        /*
            r10 = this;
            boolean r0 = r12.getFitsSystemWindows()
            r3 = 1
            if (r0 == 0) goto L_0x0010
            boolean r0 = r11.getFitsSystemWindows()
            if (r0 != 0) goto L_0x0010
            r11.setFitsSystemWindows(r3)
        L_0x0010:
            int r5 = r11.getTop()
            r12.A0D(r11, r13)
            int r0 = r12.getHeight()
            r10.A08 = r0
            boolean r0 = r10.A0M
            if (r0 == 0) goto L_0x00b8
            int r2 = r10.A0A
            if (r2 != 0) goto L_0x0032
            android.content.res.Resources r1 = r12.getResources()
            r0 = 2131165801(0x7f070269, float:1.794583E38)
            int r2 = r1.getDimensionPixelSize(r0)
            r10.A0A = r2
        L_0x0032:
            int r1 = r10.A08
            int r0 = r12.getWidth()
            int r0 = r0 * 9
            int r0 = r0 >> 4
            int r1 = r1 - r0
            int r0 = java.lang.Math.max(r2, r1)
        L_0x0041:
            r10.A07 = r0
            r2 = 0
            int r1 = r10.A08
            int r0 = r11.getHeight()
            int r1 = r1 - r0
            int r9 = java.lang.Math.max(r2, r1)
            r10.A03 = r9
            int r8 = r10.A08
            r7 = 2
            int r6 = r8 / r7
            r10.A04 = r6
            boolean r4 = r10.A0I
            int r0 = r10.A07
            int r2 = r8 - r0
            if (r4 == 0) goto L_0x0064
            int r2 = java.lang.Math.max(r2, r9)
        L_0x0064:
            r10.A02 = r2
            int r1 = r10.A0B
            r0 = 3
            if (r1 != r0) goto L_0x0096
            r2 = 0
            if (r4 == 0) goto L_0x006f
            r2 = r9
        L_0x006f:
            X.AnonymousClass028.A0Y(r11, r2)
        L_0x0072:
            X.0Su r0 = r10.A0D
            if (r0 != 0) goto L_0x0083
            X.04X r2 = r10.A0P
            android.content.Context r1 = r12.getContext()
            X.0Su r0 = new X.0Su
            r0.<init>(r1, r12, r2)
            r10.A0D = r0
        L_0x0083:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r11)
            r10.A0G = r0
            android.view.View r1 = r10.A0I(r11)
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r1)
            r10.A0F = r0
            return r3
        L_0x0096:
            r0 = 6
            if (r1 != r0) goto L_0x009d
            X.AnonymousClass028.A0Y(r11, r6)
            goto L_0x0072
        L_0x009d:
            boolean r0 = r10.A0J
            if (r0 == 0) goto L_0x00a8
            r0 = 5
            if (r1 != r0) goto L_0x00a8
            X.AnonymousClass028.A0Y(r11, r8)
            goto L_0x0072
        L_0x00a8:
            r0 = 4
            if (r1 == r0) goto L_0x006f
            if (r1 == r3) goto L_0x00af
            if (r1 != r7) goto L_0x0072
        L_0x00af:
            int r0 = r11.getTop()
            int r5 = r5 - r0
            X.AnonymousClass028.A0Y(r11, r5)
            goto L_0x0072
        L_0x00b8:
            int r0 = r10.A09
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomsheet.BottomSheetBehavior.A0G(android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout, int):boolean");
    }

    public View A0I(View view) {
        if (AnonymousClass028.A0s(view)) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View A0I = A0I(viewGroup.getChildAt(i));
                if (A0I != null) {
                    return A0I;
                }
            }
        }
        return null;
    }

    public void A0J(int i) {
        AnonymousClass2UH r3;
        int i2;
        float f;
        View view = (View) this.A0G.get();
        if (view != null && (r3 = this.A0E) != null) {
            int i3 = this.A02;
            float f2 = (float) (i3 - i);
            if (i > i3) {
                f = (float) (this.A08 - i3);
            } else {
                if (this.A0I) {
                    i2 = this.A03;
                } else {
                    i2 = 0;
                }
                f = (float) (i3 - i2);
            }
            r3.A00(view, f2 / f);
        }
    }

    public void A0K(int i) {
        View view;
        AnonymousClass2UH r0;
        boolean z;
        int i2;
        if (this.A0B != i) {
            this.A0B = i;
            if (i == 6 || i == 3) {
                z = true;
            } else {
                if (i == 5 || i == 4) {
                    z = false;
                }
                view = (View) this.A0G.get();
                if (!(view == null || (r0 = this.A0E) == null)) {
                    r0.A01(view, i);
                    return;
                }
            }
            WeakReference weakReference = this.A0G;
            if (weakReference != null) {
                ViewParent parent = ((View) weakReference.get()).getParent();
                if (parent instanceof CoordinatorLayout) {
                    ViewGroup viewGroup = (ViewGroup) parent;
                    int childCount = viewGroup.getChildCount();
                    if (z) {
                        if (this.A0H == null) {
                            this.A0H = new HashMap(childCount);
                        }
                    }
                    for (int i3 = 0; i3 < childCount; i3++) {
                        View childAt = viewGroup.getChildAt(i3);
                        if (childAt != this.A0G.get()) {
                            if (!z) {
                                Map map = this.A0H;
                                if (map != null && map.containsKey(childAt)) {
                                    i2 = ((Number) this.A0H.get(childAt)).intValue();
                                }
                            } else {
                                this.A0H.put(childAt, Integer.valueOf(childAt.getImportantForAccessibility()));
                                i2 = 4;
                            }
                            AnonymousClass028.A0a(childAt, i2);
                        }
                    }
                    if (!z) {
                        this.A0H = null;
                    }
                }
            }
            view = (View) this.A0G.get();
            if (view == null) {
            }
        }
    }

    public final void A0L(int i) {
        WeakReference weakReference;
        View view;
        boolean z = this.A0M;
        if (i == -1) {
            if (!z) {
                this.A0M = true;
            } else {
                return;
            }
        } else if (z || this.A09 != i) {
            this.A0M = false;
            this.A09 = Math.max(0, i);
            this.A02 = this.A08 - i;
        } else {
            return;
        }
        if (this.A0B == 4 && (weakReference = this.A0G) != null && (view = (View) weakReference.get()) != null) {
            view.requestLayout();
        }
    }

    public final void A0M(int i) {
        if (i != this.A0B) {
            WeakReference weakReference = this.A0G;
            if (weakReference != null) {
                View view = (View) weakReference.get();
                if (view != null) {
                    ViewParent parent = view.getParent();
                    if (parent == null || !parent.isLayoutRequested() || !AnonymousClass028.A0q(view)) {
                        A0N(view, i);
                    } else {
                        view.post(new RunnableBRunnable0Shape0S0201000_I0(view, this, i, 1));
                    }
                }
            } else if (i == 4 || i == 3 || i == 6 || (this.A0J && i == 5)) {
                this.A0B = i;
            }
        }
    }

    public void A0N(View view, int i) {
        int i2;
        if (i == 4) {
            i2 = this.A02;
        } else if (i == 6) {
            int i3 = this.A04;
            if (!this.A0I || i3 > (i2 = this.A03)) {
                i2 = i3;
            } else {
                i = 3;
            }
        } else if (i == 3) {
            i2 = this.A0I ? this.A03 : 0;
        } else if (!this.A0J || i != 5) {
            StringBuilder sb = new StringBuilder("Illegal state argument: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        } else {
            i2 = this.A08;
        }
        if (this.A0D.A0H(view, view.getLeft(), i2)) {
            A0K(2);
            view.postOnAnimation(new RunnableBRunnable0Shape0S0201000_I0(view, this, i, 2));
            return;
        }
        A0K(i);
    }

    public boolean A0O(View view, float f) {
        if (this.A0N) {
            return true;
        }
        if (view.getTop() >= this.A02 && Math.abs((((float) view.getTop()) + (f * 0.1f)) - ((float) this.A02)) / ((float) this.A09) > 0.5f) {
            return true;
        }
        return false;
    }
}
