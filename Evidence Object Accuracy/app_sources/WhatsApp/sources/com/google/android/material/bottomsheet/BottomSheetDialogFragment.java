package com.google.android.material.bottomsheet;

import X.DialogC53322dr;
import android.app.Dialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialogFragment;

/* loaded from: classes2.dex */
public class BottomSheetDialogFragment extends AppCompatDialogFragment {
    @Override // androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        return new DialogC53322dr(A0p(), A18());
    }
}
