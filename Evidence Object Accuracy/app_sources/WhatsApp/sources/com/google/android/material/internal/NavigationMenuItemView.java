package com.google.android.material.internal;

import X.AbstractC12290hg;
import X.AnonymousClass00X;
import X.AnonymousClass028;
import X.AnonymousClass04D;
import X.AnonymousClass04v;
import X.C015607k;
import X.C07340Xp;
import X.C12960it;
import X.C12980iv;
import X.C50572Qb;
import X.C50582Qc;
import X.C74303hl;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class NavigationMenuItemView extends LinearLayoutCompat implements AbstractC12290hg {
    public static final int[] A0G = {16842912};
    public int A00;
    public ColorStateList A01;
    public Drawable A02;
    public Drawable A03;
    public FrameLayout A04;
    public C07340Xp A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final int A0B;
    public final Rect A0C;
    public final Rect A0D;
    public final CheckedTextView A0E;
    public final AnonymousClass04v A0F;

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0D = C12980iv.A0J();
        this.A0C = C12980iv.A0J();
        this.A00 = 119;
        this.A09 = true;
        this.A07 = false;
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A0A, new int[0], i, 0);
        this.A00 = A00.getInt(1, this.A00);
        Drawable drawable = A00.getDrawable(0);
        if (drawable != null) {
            setForeground(drawable);
        }
        this.A09 = A00.getBoolean(2, true);
        A00.recycle();
        C74303hl r2 = new C74303hl(this);
        this.A0F = r2;
        setOrientation(0);
        LayoutInflater.from(context).inflate(R.layout.design_navigation_menu_item, (ViewGroup) this, true);
        this.A0B = context.getResources().getDimensionPixelSize(R.dimen.design_navigation_icon_size);
        CheckedTextView checkedTextView = (CheckedTextView) findViewById(R.id.design_menu_item_text);
        this.A0E = checkedTextView;
        checkedTextView.setDuplicateParentStateEnabled(true);
        AnonymousClass028.A0g(checkedTextView, r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0092, code lost:
        if (r5.A05.getActionView() == null) goto L_0x0094;
     */
    @Override // X.AbstractC12290hg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AIt(X.C07340Xp r6, int r7) {
        /*
            r5 = this;
            r5.A05 = r6
            boolean r0 = r6.isVisible()
            int r0 = X.C12960it.A02(r0)
            r5.setVisibility(r0)
            android.graphics.drawable.Drawable r0 = r5.getBackground()
            if (r0 != 0) goto L_0x0045
            android.util.TypedValue r4 = new android.util.TypedValue
            r4.<init>()
            android.content.res.Resources$Theme r2 = X.C12980iv.A0I(r5)
            r1 = 2130968801(0x7f0400e1, float:1.7546266E38)
            r0 = 1
            boolean r0 = r2.resolveAttribute(r1, r4, r0)
            if (r0 == 0) goto L_0x00c1
            android.graphics.drawable.StateListDrawable r3 = new android.graphics.drawable.StateListDrawable
            r3.<init>()
            int[] r2 = com.google.android.material.internal.NavigationMenuItemView.A0G
            int r1 = r4.data
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r3.addState(r2, r0)
            int[] r2 = android.view.ViewGroup.EMPTY_STATE_SET
            r1 = 0
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r3.addState(r2, r0)
        L_0x0042:
            r5.setBackground(r3)
        L_0x0045:
            boolean r0 = r6.isCheckable()
            r5.setCheckable(r0)
            boolean r0 = r6.isChecked()
            r5.setChecked(r0)
            boolean r0 = r6.isEnabled()
            r5.setEnabled(r0)
            java.lang.CharSequence r0 = r6.getTitle()
            r5.setTitle(r0)
            android.graphics.drawable.Drawable r0 = r6.getIcon()
            r5.setIcon(r0)
            android.view.View r0 = r6.getActionView()
            r5.setActionView(r0)
            java.lang.CharSequence r0 = r6.getContentDescription()
            r5.setContentDescription(r0)
            java.lang.CharSequence r0 = r6.getTooltipText()
            X.AnonymousClass0KD.A00(r5, r0)
            X.0Xp r1 = r5.A05
            java.lang.CharSequence r0 = r1.getTitle()
            if (r0 != 0) goto L_0x0094
            android.graphics.drawable.Drawable r0 = r1.getIcon()
            if (r0 != 0) goto L_0x0094
            X.0Xp r0 = r5.A05
            android.view.View r1 = r0.getActionView()
            r0 = 1
            if (r1 != 0) goto L_0x0095
        L_0x0094:
            r0 = 0
        L_0x0095:
            android.widget.CheckedTextView r1 = r5.A0E
            if (r0 == 0) goto L_0x00b1
            r0 = 8
            r1.setVisibility(r0)
            android.widget.FrameLayout r0 = r5.A04
            if (r0 == 0) goto L_0x00b0
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r1 = (android.widget.LinearLayout.LayoutParams) r1
            r0 = -1
        L_0x00a9:
            r1.width = r0
            android.widget.FrameLayout r0 = r5.A04
            r0.setLayoutParams(r1)
        L_0x00b0:
            return
        L_0x00b1:
            r0 = 0
            r1.setVisibility(r0)
            android.widget.FrameLayout r0 = r5.A04
            if (r0 == 0) goto L_0x00b0
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r1 = (android.widget.LinearLayout.LayoutParams) r1
            r0 = -2
            goto L_0x00a9
        L_0x00c1:
            r3 = 0
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.internal.NavigationMenuItemView.AIt(X.0Xp, int):void");
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.A03;
        if (drawable != null) {
            if (this.A07) {
                this.A07 = false;
                Rect rect = this.A0D;
                Rect rect2 = this.A0C;
                int right = getRight() - getLeft();
                int bottom = getBottom() - getTop();
                if (this.A09) {
                    rect.set(0, 0, right, bottom);
                } else {
                    rect.set(getPaddingLeft(), getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
                }
                Gravity.apply(this.A00, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), rect, rect2);
                drawable.setBounds(rect2);
            }
            drawable.draw(canvas);
        }
    }

    @Override // android.view.View
    public void drawableHotspotChanged(float f, float f2) {
        super.drawableHotspotChanged(f, f2);
        Drawable drawable = this.A03;
        if (drawable != null) {
            drawable.setHotspot(f, f2);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.A03;
        if (drawable != null && drawable.isStateful()) {
            this.A03.setState(getDrawableState());
        }
    }

    @Override // android.view.View
    public Drawable getForeground() {
        return this.A03;
    }

    @Override // android.view.View
    public int getForegroundGravity() {
        return this.A00;
    }

    @Override // X.AbstractC12290hg
    public C07340Xp getItemData() {
        return this.A05;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.A03;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        C07340Xp r1 = this.A05;
        if (r1 != null && r1.isCheckable() && r1.isChecked()) {
            ViewGroup.mergeDrawableStates(onCreateDrawableState, A0G);
        }
        return onCreateDrawableState;
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.A07 = z | this.A07;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.A07 = true;
    }

    private void setActionView(View view) {
        if (view != null) {
            FrameLayout frameLayout = this.A04;
            if (frameLayout == null) {
                frameLayout = (FrameLayout) ((ViewStub) findViewById(R.id.design_menu_item_action_area_stub)).inflate();
                this.A04 = frameLayout;
            }
            frameLayout.removeAllViews();
            this.A04.addView(view);
        }
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
        if (this.A06 != z) {
            this.A06 = z;
            this.A0F.A00(this.A0E, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        }
    }

    public void setChecked(boolean z) {
        refreshDrawableState();
        this.A0E.setChecked(z);
    }

    @Override // android.view.View
    public void setForeground(Drawable drawable) {
        Drawable drawable2 = this.A03;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
                unscheduleDrawable(this.A03);
            }
            this.A03 = drawable;
            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                C12980iv.A16(drawable, this);
                if (this.A00 == 119) {
                    drawable.getPadding(C12980iv.A0J());
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    @Override // android.view.View
    public void setForegroundGravity(int i) {
        if (this.A00 != i) {
            if ((8388615 & i) == 0) {
                i |= 8388611;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.A00 = i;
            if (i == 119 && this.A03 != null) {
                this.A03.getPadding(C12980iv.A0J());
            }
            requestLayout();
        }
    }

    public void setHorizontalPadding(int i) {
        setPadding(i, 0, i, 0);
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            if (this.A08) {
                Drawable.ConstantState constantState = drawable.getConstantState();
                if (constantState != null) {
                    drawable = constantState.newDrawable();
                }
                drawable = C015607k.A03(drawable).mutate();
                C015607k.A04(this.A01, drawable);
            }
            int i = this.A0B;
            drawable.setBounds(0, 0, i, i);
        } else if (this.A0A) {
            if (this.A02 == null) {
                Drawable A04 = AnonymousClass00X.A04(C12980iv.A0I(this), getResources(), R.drawable.navigation_empty_icon);
                this.A02 = A04;
                if (A04 != null) {
                    int i2 = this.A0B;
                    A04.setBounds(0, 0, i2, i2);
                }
            }
            drawable = this.A02;
        }
        AnonymousClass04D.A05(drawable, null, null, null, this.A0E);
    }

    public void setIconPadding(int i) {
        this.A0E.setCompoundDrawablePadding(i);
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.A01 = colorStateList;
        this.A08 = C12960it.A1W(colorStateList);
        C07340Xp r0 = this.A05;
        if (r0 != null) {
            setIcon(r0.getIcon());
        }
    }

    public void setNeedsEmptyIcon(boolean z) {
        this.A0A = z;
    }

    public void setTextAppearance(int i) {
        AnonymousClass04D.A08(this.A0E, i);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.A0E.setTextColor(colorStateList);
    }

    public void setTitle(CharSequence charSequence) {
        this.A0E.setText(charSequence);
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A03;
    }
}
