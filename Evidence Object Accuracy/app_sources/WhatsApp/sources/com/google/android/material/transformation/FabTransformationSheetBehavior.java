package com.google.android.material.transformation;

import android.content.Context;
import android.util.AttributeSet;
import java.util.Map;

/* loaded from: classes2.dex */
public class FabTransformationSheetBehavior extends FabTransformationBehavior {
    public Map A00;

    public FabTransformationSheetBehavior() {
    }

    public FabTransformationSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        if ((((X.AnonymousClass0B5) r2.getLayoutParams()).A0A instanceof com.google.android.material.transformation.FabTransformationScrimBehavior) == false) goto L_0x0033;
     */
    @Override // com.google.android.material.transformation.ExpandableTransformationBehavior, com.google.android.material.transformation.ExpandableBehavior
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0I(android.view.View r7, android.view.View r8, boolean r9, boolean r10) {
        /*
            r6 = this;
            android.view.ViewParent r5 = r8.getParent()
            boolean r0 = r5 instanceof androidx.coordinatorlayout.widget.CoordinatorLayout
            if (r0 == 0) goto L_0x0068
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            int r4 = r5.getChildCount()
            if (r9 == 0) goto L_0x0017
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>(r4)
            r6.A00 = r0
        L_0x0017:
            r3 = 0
        L_0x0018:
            if (r3 >= r4) goto L_0x0063
            android.view.View r2 = r5.getChildAt(r3)
            android.view.ViewGroup$LayoutParams r0 = r2.getLayoutParams()
            boolean r0 = r0 instanceof X.AnonymousClass0B5
            if (r0 == 0) goto L_0x0033
            android.view.ViewGroup$LayoutParams r0 = r2.getLayoutParams()
            X.0B5 r0 = (X.AnonymousClass0B5) r0
            X.04r r0 = r0.A0A
            boolean r1 = r0 instanceof com.google.android.material.transformation.FabTransformationScrimBehavior
            r0 = 1
            if (r1 != 0) goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            if (r2 == r8) goto L_0x0051
            if (r0 != 0) goto L_0x0051
            if (r9 != 0) goto L_0x0054
            java.util.Map r0 = r6.A00
            if (r0 == 0) goto L_0x0051
            boolean r0 = r0.containsKey(r2)
            if (r0 == 0) goto L_0x0051
            java.util.Map r0 = r6.A00
            java.lang.Object r0 = r0.get(r2)
            int r0 = X.C12960it.A05(r0)
        L_0x004e:
            X.AnonymousClass028.A0a(r2, r0)
        L_0x0051:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0054:
            java.util.Map r1 = r6.A00
            int r0 = r2.getImportantForAccessibility()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.put(r2, r0)
            r0 = 4
            goto L_0x004e
        L_0x0063:
            if (r9 != 0) goto L_0x0068
            r0 = 0
            r6.A00 = r0
        L_0x0068:
            boolean r0 = super.A0I(r7, r8, r9, r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.transformation.FabTransformationSheetBehavior.A0I(android.view.View, android.view.View, boolean, boolean):boolean");
    }
}
