package com.google.android.material.snackbar;

import X.AnonymousClass028;
import X.AnonymousClass5R7;
import X.C12960it;
import X.C50572Qb;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SnackbarContentLayout extends LinearLayout implements AnonymousClass5R7 {
    public int A00;
    public int A01;
    public Button A02;
    public TextView A03;

    public SnackbarContentLayout(Context context) {
        this(context, null);
    }

    public SnackbarContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A0D);
        this.A01 = obtainStyledAttributes.getDimensionPixelSize(0, -1);
        this.A00 = obtainStyledAttributes.getDimensionPixelSize(2, -1);
        obtainStyledAttributes.recycle();
    }

    public final boolean A00(int i, int i2, int i3) {
        boolean z;
        if (i != getOrientation()) {
            setOrientation(i);
            z = true;
        } else {
            z = false;
        }
        if (this.A03.getPaddingTop() == i2 && this.A03.getPaddingBottom() == i3) {
            return z;
        }
        TextView textView = this.A03;
        if (AnonymousClass028.A0t(textView)) {
            AnonymousClass028.A0e(textView, AnonymousClass028.A07(textView), i2, AnonymousClass028.A06(textView), i3);
            return true;
        }
        textView.setPadding(textView.getPaddingLeft(), i2, textView.getPaddingRight(), i3);
        return true;
    }

    public Button getActionView() {
        return this.A02;
    }

    public TextView getMessageView() {
        return this.A03;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.A03 = C12960it.A0J(this, R.id.snackbar_text);
        this.A02 = (Button) findViewById(R.id.snackbar_action);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @Override // android.widget.LinearLayout, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r7, int r8) {
        /*
            r6 = this;
            super.onMeasure(r7, r8)
            int r1 = r6.A01
            if (r1 <= 0) goto L_0x0014
            int r0 = r6.getMeasuredWidth()
            if (r0 <= r1) goto L_0x0014
            int r7 = X.C12980iv.A04(r1)
            super.onMeasure(r7, r8)
        L_0x0014:
            android.content.res.Resources r1 = r6.getResources()
            r0 = 2131165825(0x7f070281, float:1.7945878E38)
            int r5 = r1.getDimensionPixelSize(r0)
            android.content.res.Resources r1 = r6.getResources()
            r0 = 2131165824(0x7f070280, float:1.7945876E38)
            int r4 = r1.getDimensionPixelSize(r0)
            android.widget.TextView r0 = r6.A03
            android.text.Layout r0 = r0.getLayout()
            int r0 = r0.getLineCount()
            r3 = 0
            r2 = 1
            if (r0 <= r2) goto L_0x0050
            int r1 = r6.A00
            if (r1 <= 0) goto L_0x0051
            android.widget.Button r0 = r6.A02
            int r0 = r0.getMeasuredWidth()
            if (r0 <= r1) goto L_0x0051
            int r0 = r5 - r4
            boolean r0 = r6.A00(r2, r5, r0)
        L_0x004a:
            if (r0 == 0) goto L_0x004f
            super.onMeasure(r7, r8)
        L_0x004f:
            return
        L_0x0050:
            r5 = r4
        L_0x0051:
            boolean r0 = r6.A00(r3, r5, r5)
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.snackbar.SnackbarContentLayout.onMeasure(int, int):void");
    }
}
