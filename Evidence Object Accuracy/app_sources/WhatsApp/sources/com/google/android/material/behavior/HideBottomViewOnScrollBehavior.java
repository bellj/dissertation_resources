package com.google.android.material.behavior;

import X.AbstractC009304r;
import X.C12960it;
import X.C50732Qs;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;

/* loaded from: classes2.dex */
public class HideBottomViewOnScrollBehavior extends AbstractC009304r {
    public int A00 = 2;
    public int A01 = 0;
    public ViewPropertyAnimator A02;

    public HideBottomViewOnScrollBehavior() {
    }

    public HideBottomViewOnScrollBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AbstractC009304r
    public void A00(View view, View view2, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4) {
        int i5 = this.A00;
        if (i5 != 1) {
            if (i2 > 0) {
                A0I(view);
                return;
            } else if (i5 == 2) {
                return;
            }
        }
        if (i2 < 0) {
            A0J(view);
        }
    }

    @Override // X.AbstractC009304r
    public boolean A04(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i) {
        return C12960it.A1V(i, 2);
    }

    @Override // X.AbstractC009304r
    public boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
        this.A01 = view.getMeasuredHeight();
        return false;
    }

    public void A0I(View view) {
        ViewPropertyAnimator viewPropertyAnimator = this.A02;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
            view.clearAnimation();
        }
        this.A00 = 1;
        int i = this.A01;
        this.A02 = view.animate().translationY((float) i).setInterpolator(C50732Qs.A01).setDuration(175).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 0));
    }

    public void A0J(View view) {
        ViewPropertyAnimator viewPropertyAnimator = this.A02;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
            view.clearAnimation();
        }
        this.A00 = 2;
        this.A02 = view.animate().translationY((float) 0).setInterpolator(C50732Qs.A04).setDuration(225).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 0));
    }
}
