package com.google.android.material.button;

import X.AnonymousClass028;
import X.AnonymousClass04D;
import X.AnonymousClass0BS;
import X.C015007d;
import X.C015607k;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C50572Qb;
import X.C50582Qc;
import X.C50592Qd;
import X.C50602Qe;
import X.C64823Ha;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class MaterialButton extends AnonymousClass0BS {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public ColorStateList A04;
    public PorterDuff.Mode A05;
    public Drawable A06;
    public final C64823Ha A07;

    public MaterialButton(Context context) {
        this(context, null);
    }

    public MaterialButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.materialButtonStyle);
    }

    public MaterialButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Drawable insetDrawable;
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A0B, new int[0], i, 2131952637);
        this.A02 = A00.getDimensionPixelSize(9, 0);
        this.A05 = C50602Qe.A00(PorterDuff.Mode.SRC_IN, A00.getInt(12, -1));
        this.A04 = C50592Qd.A00(getContext(), A00, 11);
        this.A06 = C50592Qd.A01(getContext(), A00, 7);
        this.A00 = A00.getInteger(8, 1);
        this.A03 = A00.getDimensionPixelSize(10, 0);
        C64823Ha r6 = new C64823Ha(this);
        this.A07 = r6;
        int i2 = 0;
        r6.A02 = A00.getDimensionPixelOffset(0, 0);
        r6.A03 = A00.getDimensionPixelOffset(1, 0);
        r6.A04 = A00.getDimensionPixelOffset(2, 0);
        r6.A01 = A00.getDimensionPixelOffset(3, 0);
        r6.A00 = A00.getDimensionPixelSize(6, 0);
        r6.A05 = A00.getDimensionPixelSize(15, 0);
        r6.A09 = C50602Qe.A00(PorterDuff.Mode.SRC_IN, A00.getInt(5, -1));
        MaterialButton materialButton = r6.A0L;
        r6.A06 = C50592Qd.A00(materialButton.getContext(), A00, 4);
        r6.A08 = C50592Qd.A00(materialButton.getContext(), A00, 14);
        r6.A07 = C50592Qd.A00(materialButton.getContext(), A00, 13);
        Paint paint = r6.A0I;
        C12990iw.A13(paint);
        paint.setStrokeWidth((float) r6.A05);
        ColorStateList colorStateList = r6.A08;
        paint.setColor(colorStateList != null ? colorStateList.getColorForState(materialButton.getDrawableState(), 0) : i2);
        int A07 = AnonymousClass028.A07(materialButton);
        int paddingTop = materialButton.getPaddingTop();
        int A06 = AnonymousClass028.A06(materialButton);
        int paddingBottom = materialButton.getPaddingBottom();
        if (C64823Ha.A0M) {
            insetDrawable = r6.A00();
        } else {
            GradientDrawable gradientDrawable = new GradientDrawable();
            r6.A0D = gradientDrawable;
            gradientDrawable.setCornerRadius(((float) r6.A00) + 1.0E-5f);
            r6.A0D.setColor(-1);
            Drawable A03 = C015607k.A03(r6.A0D);
            r6.A0A = A03;
            C015607k.A04(r6.A06, A03);
            PorterDuff.Mode mode = r6.A09;
            if (mode != null) {
                C015607k.A07(mode, r6.A0A);
            }
            GradientDrawable gradientDrawable2 = new GradientDrawable();
            r6.A0F = gradientDrawable2;
            gradientDrawable2.setCornerRadius(((float) r6.A00) + 1.0E-5f);
            r6.A0F.setColor(-1);
            Drawable A032 = C015607k.A03(r6.A0F);
            r6.A0B = A032;
            C015607k.A04(r6.A07, A032);
            insetDrawable = new InsetDrawable((Drawable) new LayerDrawable(new Drawable[]{r6.A0A, r6.A0B}), r6.A02, r6.A04, r6.A03, r6.A01);
        }
        super.setBackgroundDrawable(insetDrawable);
        AnonymousClass028.A0e(materialButton, A07 + r6.A02, paddingTop + r6.A04, A06 + r6.A03, paddingBottom + r6.A01);
        A00.recycle();
        setCompoundDrawablePadding(this.A02);
        A00();
    }

    public final void A00() {
        Drawable drawable = this.A06;
        if (drawable != null) {
            Drawable mutate = drawable.mutate();
            this.A06 = mutate;
            C015607k.A04(this.A04, mutate);
            PorterDuff.Mode mode = this.A05;
            if (mode != null) {
                C015607k.A07(mode, this.A06);
            }
            int i = this.A03;
            if (i == 0) {
                i = this.A06.getIntrinsicWidth();
            }
            int i2 = this.A03;
            if (i2 == 0) {
                i2 = this.A06.getIntrinsicHeight();
            }
            Drawable drawable2 = this.A06;
            int i3 = this.A01;
            drawable2.setBounds(i3, 0, i + i3, i2);
        }
        AnonymousClass04D.A05(this.A06, null, null, null, this);
    }

    public final boolean A01() {
        C64823Ha r0 = this.A07;
        return r0 != null && !r0.A0H;
    }

    @Override // android.view.View
    public ColorStateList getBackgroundTintList() {
        return getSupportBackgroundTintList();
    }

    @Override // android.view.View
    public PorterDuff.Mode getBackgroundTintMode() {
        return getSupportBackgroundTintMode();
    }

    public int getCornerRadius() {
        if (A01()) {
            return this.A07.A00;
        }
        return 0;
    }

    public Drawable getIcon() {
        return this.A06;
    }

    public int getIconGravity() {
        return this.A00;
    }

    public int getIconPadding() {
        return this.A02;
    }

    public int getIconSize() {
        return this.A03;
    }

    public ColorStateList getIconTint() {
        return this.A04;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.A05;
    }

    public ColorStateList getRippleColor() {
        if (A01()) {
            return this.A07.A07;
        }
        return null;
    }

    public ColorStateList getStrokeColor() {
        if (A01()) {
            return this.A07.A08;
        }
        return null;
    }

    public int getStrokeWidth() {
        if (A01()) {
            return this.A07.A05;
        }
        return 0;
    }

    @Override // X.AnonymousClass0BS, X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        if (A01()) {
            return this.A07.A06;
        }
        return super.getSupportBackgroundTintList();
    }

    @Override // X.AnonymousClass0BS, X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (A01()) {
            return this.A07.A09;
        }
        return super.getSupportBackgroundTintMode();
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (Build.VERSION.SDK_INT < 21 && A01()) {
            C64823Ha r4 = this.A07;
            if (canvas != null && r4.A08 != null && r4.A05 > 0) {
                Rect rect = r4.A0J;
                rect.set(r4.A0L.getBackground().getBounds());
                RectF rectF = r4.A0K;
                float f = (float) rect.left;
                float f2 = ((float) r4.A05) / 2.0f;
                rectF.set(f + f2 + ((float) r4.A02), ((float) rect.top) + f2 + ((float) r4.A04), (((float) rect.right) - f2) - ((float) r4.A03), (((float) rect.bottom) - f2) - ((float) r4.A01));
                float f3 = ((float) r4.A00) - (((float) r4.A05) / 2.0f);
                canvas.drawRoundRect(rectF, f3, f3, r4.A0I);
            }
        }
    }

    @Override // X.AnonymousClass0BS, android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        C64823Ha r4;
        super.onLayout(z, i, i2, i3, i4);
        if (Build.VERSION.SDK_INT == 21 && (r4 = this.A07) != null) {
            int i5 = i4 - i2;
            int i6 = i3 - i;
            GradientDrawable gradientDrawable = r4.A0E;
            if (gradientDrawable != null) {
                gradientDrawable.setBounds(r4.A02, r4.A04, i6 - r4.A03, i5 - r4.A01);
            }
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A06 != null && this.A00 == 2) {
            int measureText = (int) getPaint().measureText(C12980iv.A0q(this));
            int i3 = this.A03;
            if (i3 == 0) {
                i3 = this.A06.getIntrinsicWidth();
            }
            int measuredWidth = (((((getMeasuredWidth() - measureText) - AnonymousClass028.A06(this)) - i3) - this.A02) - AnonymousClass028.A07(this)) / 2;
            if (AnonymousClass028.A05(this) == 1) {
                measuredWidth = -measuredWidth;
            }
            if (this.A01 != measuredWidth) {
                this.A01 = measuredWidth;
                A00();
            }
        }
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        GradientDrawable gradientDrawable;
        if (A01()) {
            C64823Ha r1 = this.A07;
            if (C64823Ha.A0M) {
                gradientDrawable = r1.A0C;
            } else {
                gradientDrawable = r1.A0D;
            }
            if (gradientDrawable != null) {
                gradientDrawable.setColor(i);
                return;
            }
            return;
        }
        super.setBackgroundColor(i);
    }

    @Override // X.AnonymousClass0BS, android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        if (A01()) {
            if (drawable != getBackground()) {
                Log.i("MaterialButton", "Setting a custom background is not supported.");
                C64823Ha r2 = this.A07;
                r2.A0H = true;
                MaterialButton materialButton = r2.A0L;
                materialButton.setSupportBackgroundTintList(r2.A06);
                materialButton.setSupportBackgroundTintMode(r2.A09);
            } else {
                getBackground().setState(drawable.getState());
                return;
            }
        }
        super.setBackgroundDrawable(drawable);
    }

    @Override // X.AnonymousClass0BS, android.view.View
    public void setBackgroundResource(int i) {
        Drawable drawable;
        if (i != 0) {
            drawable = C015007d.A01(getContext(), i);
        } else {
            drawable = null;
        }
        setBackgroundDrawable(drawable);
    }

    @Override // android.view.View
    public void setBackgroundTintList(ColorStateList colorStateList) {
        setSupportBackgroundTintList(colorStateList);
    }

    @Override // android.view.View
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        setSupportBackgroundTintMode(mode);
    }

    public void setCornerRadius(int i) {
        if (A01()) {
            this.A07.A02(i);
        }
    }

    public void setCornerRadiusResource(int i) {
        if (A01()) {
            setCornerRadius(C12990iw.A07(this, i));
        }
    }

    public void setIcon(Drawable drawable) {
        if (this.A06 != drawable) {
            this.A06 = drawable;
            A00();
        }
    }

    public void setIconGravity(int i) {
        this.A00 = i;
    }

    public void setIconPadding(int i) {
        if (this.A02 != i) {
            this.A02 = i;
            setCompoundDrawablePadding(i);
        }
    }

    public void setIconResource(int i) {
        Drawable drawable;
        if (i != 0) {
            drawable = C015007d.A01(getContext(), i);
        } else {
            drawable = null;
        }
        setIcon(drawable);
    }

    public void setIconSize(int i) {
        if (i < 0) {
            throw C12970iu.A0f("iconSize cannot be less than 0");
        } else if (this.A03 != i) {
            this.A03 = i;
            A00();
        }
    }

    public void setIconTint(ColorStateList colorStateList) {
        if (this.A04 != colorStateList) {
            this.A04 = colorStateList;
            A00();
        }
    }

    public void setIconTintMode(PorterDuff.Mode mode) {
        if (this.A05 != mode) {
            this.A05 = mode;
            A00();
        }
    }

    public void setIconTintResource(int i) {
        setIconTint(C015007d.A00(getContext(), i));
    }

    public void setInternalBackground(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public void setRippleColor(ColorStateList colorStateList) {
        if (A01()) {
            this.A07.A03(colorStateList);
        }
    }

    public void setRippleColorResource(int i) {
        if (A01()) {
            setRippleColor(C015007d.A00(getContext(), i));
        }
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        if (A01()) {
            C64823Ha r3 = this.A07;
            if (r3.A08 != colorStateList) {
                r3.A08 = colorStateList;
                Paint paint = r3.A0I;
                int i = 0;
                if (colorStateList != null) {
                    i = colorStateList.getColorForState(r3.A0L.getDrawableState(), 0);
                }
                paint.setColor(i);
                if (!C64823Ha.A0M) {
                    r3.A0L.invalidate();
                } else if (r3.A0G != null) {
                    super.setBackgroundDrawable(r3.A00());
                }
            }
        }
    }

    public void setStrokeColorResource(int i) {
        if (A01()) {
            setStrokeColor(C015007d.A00(getContext(), i));
        }
    }

    public void setStrokeWidth(int i) {
        if (A01()) {
            C64823Ha r2 = this.A07;
            if (r2.A05 != i) {
                r2.A05 = i;
                r2.A0I.setStrokeWidth((float) i);
                if (!C64823Ha.A0M) {
                    r2.A0L.invalidate();
                } else if (r2.A0G != null) {
                    super.setBackgroundDrawable(r2.A00());
                }
            }
        }
    }

    public void setStrokeWidthResource(int i) {
        if (A01()) {
            setStrokeWidth(C12990iw.A07(this, i));
        }
    }

    @Override // X.AnonymousClass0BS, X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        boolean A01 = A01();
        C64823Ha r1 = this.A07;
        if (A01) {
            if (r1.A06 != colorStateList) {
                r1.A06 = colorStateList;
                if (C64823Ha.A0M) {
                    r1.A01();
                    return;
                }
                Drawable drawable = r1.A0A;
                if (drawable != null) {
                    C015607k.A04(colorStateList, drawable);
                }
            }
        } else if (r1 != null) {
            super.setSupportBackgroundTintList(colorStateList);
        }
    }

    @Override // X.AnonymousClass0BS, X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        boolean A01 = A01();
        C64823Ha r1 = this.A07;
        if (A01) {
            if (r1.A09 != mode) {
                r1.A09 = mode;
                if (C64823Ha.A0M) {
                    r1.A01();
                    return;
                }
                Drawable drawable = r1.A0A;
                if (drawable != null && mode != null) {
                    C015607k.A07(mode, drawable);
                }
            }
        } else if (r1 != null) {
            super.setSupportBackgroundTintMode(mode);
        }
    }
}
