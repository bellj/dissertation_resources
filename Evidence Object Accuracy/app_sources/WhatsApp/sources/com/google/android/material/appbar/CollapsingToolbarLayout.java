package com.google.android.material.appbar;

import X.AbstractC117155Ys;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass3TV;
import X.C015607k;
import X.C018408o;
import X.C104074rm;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C47742Cf;
import X.C50572Qb;
import X.C50582Qc;
import X.C50732Qs;
import X.C52902bs;
import X.C92334Vm;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public class CollapsingToolbarLayout extends FrameLayout {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public long A08;
    public ValueAnimator A09;
    public Drawable A0A;
    public Drawable A0B;
    public View A0C;
    public View A0D;
    public Toolbar A0E;
    public C018408o A0F;
    public AbstractC117155Ys A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public final Rect A0L;
    public final C47742Cf A0M;

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0J = true;
        this.A0L = C12980iv.A0J();
        this.A06 = -1;
        C47742Cf r5 = new C47742Cf(this);
        this.A0M = r5;
        r5.A0O = C50732Qs.A00;
        r5.A03();
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A05, new int[0], i, 2131952625);
        int i2 = A00.getInt(3, 8388691);
        if (r5.A0M != i2) {
            r5.A0M = i2;
            r5.A03();
        }
        int i3 = A00.getInt(0, 8388627);
        if (r5.A0K != i3) {
            r5.A0K = i3;
            r5.A03();
        }
        int dimensionPixelSize = A00.getDimensionPixelSize(4, 0);
        this.A01 = dimensionPixelSize;
        this.A02 = dimensionPixelSize;
        this.A04 = dimensionPixelSize;
        this.A03 = dimensionPixelSize;
        if (A00.hasValue(7)) {
            this.A03 = A00.getDimensionPixelSize(7, 0);
        }
        if (A00.hasValue(6)) {
            this.A02 = A00.getDimensionPixelSize(6, 0);
        }
        if (A00.hasValue(8)) {
            this.A04 = A00.getDimensionPixelSize(8, 0);
        }
        if (A00.hasValue(5)) {
            this.A01 = A00.getDimensionPixelSize(5, 0);
        }
        this.A0H = A00.getBoolean(14, true);
        setTitle(A00.getText(13));
        r5.A09(2131952301);
        r5.A08(2131952273);
        if (A00.hasValue(9)) {
            r5.A09(A00.getResourceId(9, 0));
        }
        if (A00.hasValue(1)) {
            r5.A08(A00.getResourceId(1, 0));
        }
        this.A06 = A00.getDimensionPixelSize(11, -1);
        this.A08 = (long) A00.getInt(10, 600);
        setContentScrim(A00.getDrawable(2));
        setStatusBarScrim(A00.getDrawable(12));
        this.A07 = A00.getResourceId(15, -1);
        A00.recycle();
        setWillNotDraw(false);
        AnonymousClass028.A0h(this, new C104074rm(this));
    }

    public final int A00(View view) {
        C92334Vm r0 = (C92334Vm) view.getTag(R.id.view_offset_helper);
        if (r0 == null) {
            r0 = new C92334Vm(view);
            view.setTag(R.id.view_offset_helper, r0);
        }
        return ((getHeight() - r0.A01) - view.getHeight()) - ((FrameLayout.LayoutParams) view.getLayoutParams()).bottomMargin;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:31:0x0027 */
    public final void A01() {
        if (this.A0J) {
            Toolbar toolbar = null;
            this.A0E = null;
            this.A0D = null;
            int i = this.A07;
            if (i != -1) {
                Toolbar toolbar2 = (Toolbar) findViewById(i);
                this.A0E = toolbar2;
                if (toolbar2 != null) {
                    ViewParent parent = toolbar2.getParent();
                    View view = toolbar2;
                    while (parent != this && parent != null) {
                        if (parent instanceof View) {
                            view = (View) parent;
                        }
                        parent = parent.getParent();
                        view = view;
                    }
                    this.A0D = view;
                }
            }
            if (this.A0E == null) {
                int childCount = getChildCount();
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        break;
                    }
                    View childAt = getChildAt(i2);
                    if (childAt instanceof Toolbar) {
                        toolbar = (Toolbar) childAt;
                        break;
                    }
                    i2++;
                }
                this.A0E = toolbar;
            }
            A02();
            this.A0J = false;
        }
    }

    public final void A02() {
        View view;
        if (!this.A0H && (view = this.A0C) != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.A0C);
            }
        }
        if (this.A0H && this.A0E != null) {
            View view2 = this.A0C;
            if (view2 == null) {
                view2 = new View(getContext());
                this.A0C = view2;
            }
            if (view2.getParent() == null) {
                this.A0E.addView(this.A0C, -1, -1);
            }
        }
    }

    public final void A03() {
        if (this.A0A != null || this.A0B != null) {
            setScrimsShown(C12990iw.A1Y(getHeight() + this.A00, getScrimVisibleHeightTrigger()));
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C52902bs;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        C018408o r0;
        int A06;
        Drawable drawable;
        super.draw(canvas);
        A01();
        if (this.A0E == null && (drawable = this.A0A) != null && this.A05 > 0) {
            drawable.mutate().setAlpha(this.A05);
            this.A0A.draw(canvas);
        }
        if (this.A0H && this.A0I) {
            this.A0M.A0C(canvas);
        }
        if (this.A0B != null && this.A05 > 0 && (r0 = this.A0F) != null && (A06 = r0.A06()) > 0) {
            this.A0B.setBounds(0, -this.A00, getWidth(), A06 - this.A00);
            this.A0B.mutate().setAlpha(this.A05);
            this.A0B.draw(canvas);
        }
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        boolean z;
        Drawable drawable = this.A0A;
        if (drawable != null && this.A05 > 0) {
            View view2 = this.A0D;
            if (view2 == null || view2 == this) {
                view2 = this.A0E;
            }
            if (view == view2) {
                drawable.mutate().setAlpha(this.A05);
                this.A0A.draw(canvas);
                z = true;
                if (super.drawChild(canvas, view, j) && !z) {
                    return false;
                }
            }
        }
        z = false;
        return super.drawChild(canvas, view, j) ? true : true;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void drawableStateChanged() {
        boolean z;
        ColorStateList colorStateList;
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.A0B;
        boolean z2 = false;
        if (drawable != null && drawable.isStateful()) {
            z2 = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.A0A;
        if (drawable2 != null && drawable2.isStateful()) {
            z2 |= drawable2.setState(drawableState);
        }
        C47742Cf r1 = this.A0M;
        if (r1 != null) {
            r1.A0c = drawableState;
            ColorStateList colorStateList2 = r1.A0P;
            if ((colorStateList2 == null || !colorStateList2.isStateful()) && ((colorStateList = r1.A0Q) == null || !colorStateList.isStateful())) {
                z = false;
            } else {
                r1.A03();
                z = true;
            }
            z2 |= z;
        }
        if (z2) {
            invalidate();
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new C52902bs(layoutParams);
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C52902bs(getContext(), attributeSet);
    }

    public int getCollapsedTitleGravity() {
        return this.A0M.A0K;
    }

    public Typeface getCollapsedTitleTypeface() {
        Typeface typeface = this.A0M.A0T;
        if (typeface == null) {
            return Typeface.DEFAULT;
        }
        return typeface;
    }

    public Drawable getContentScrim() {
        return this.A0A;
    }

    public int getExpandedTitleGravity() {
        return this.A0M.A0M;
    }

    public int getExpandedTitleMarginBottom() {
        return this.A01;
    }

    public int getExpandedTitleMarginEnd() {
        return this.A02;
    }

    public int getExpandedTitleMarginStart() {
        return this.A03;
    }

    public int getExpandedTitleMarginTop() {
        return this.A04;
    }

    public Typeface getExpandedTitleTypeface() {
        Typeface typeface = this.A0M.A0V;
        if (typeface == null) {
            return Typeface.DEFAULT;
        }
        return typeface;
    }

    public int getScrimAlpha() {
        return this.A05;
    }

    public long getScrimAnimationDuration() {
        return this.A08;
    }

    public int getScrimVisibleHeightTrigger() {
        int i;
        int i2 = this.A06;
        if (i2 >= 0) {
            return i2;
        }
        C018408o r0 = this.A0F;
        if (r0 != null) {
            i = r0.A06();
        } else {
            i = 0;
        }
        int minimumHeight = getMinimumHeight();
        if (minimumHeight > 0) {
            return Math.min((minimumHeight << 1) + i, getHeight());
        }
        return getHeight() / 3;
    }

    public Drawable getStatusBarScrim() {
        return this.A0B;
    }

    public CharSequence getTitle() {
        if (this.A0H) {
            return this.A0M.A0W;
        }
        return null;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            setFitsSystemWindows(((View) parent).getFitsSystemWindows());
            AbstractC117155Ys r0 = this.A0G;
            if (r0 == null) {
                r0 = new AnonymousClass3TV(this);
                this.A0G = r0;
            }
            ((AppBarLayout) parent).A01(r0);
            AnonymousClass028.A0R(this);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        List list;
        ViewParent parent = getParent();
        AbstractC117155Ys r1 = this.A0G;
        if (!(r1 == null || !(parent instanceof AppBarLayout) || (list = ((AppBarLayout) parent).A05) == null)) {
            list.remove(r1);
        }
        super.onDetachedFromWindow();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003f, code lost:
        if (r13.A0C.getVisibility() != 0) goto L_0x0041;
     */
    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r14, int r15, int r16, int r17, int r18) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.CollapsingToolbarLayout.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        A01();
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i2);
        C018408o r0 = this.A0F;
        if (r0 != null) {
            i3 = r0.A06();
        } else {
            i3 = 0;
        }
        if (mode == 0 && i3 > 0) {
            super.onMeasure(i, C12980iv.A04(getMeasuredHeight() + i3));
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Drawable drawable = this.A0A;
        if (drawable != null) {
            drawable.setBounds(0, 0, i, i2);
        }
    }

    public void setCollapsedTitleGravity(int i) {
        C47742Cf r1 = this.A0M;
        if (r1.A0K != i) {
            r1.A0K = i;
            r1.A03();
        }
    }

    public void setCollapsedTitleTextAppearance(int i) {
        this.A0M.A08(i);
    }

    public void setCollapsedTitleTextColor(int i) {
        setCollapsedTitleTextColor(ColorStateList.valueOf(i));
    }

    public void setCollapsedTitleTextColor(ColorStateList colorStateList) {
        C47742Cf r1 = this.A0M;
        if (r1.A0P != colorStateList) {
            r1.A0P = colorStateList;
            r1.A03();
        }
    }

    public void setCollapsedTitleTypeface(Typeface typeface) {
        C47742Cf r1 = this.A0M;
        if (r1.A0T != typeface) {
            r1.A0T = typeface;
            r1.A03();
        }
    }

    public void setContentScrim(Drawable drawable) {
        Drawable drawable2 = this.A0A;
        if (drawable2 != drawable) {
            Drawable drawable3 = null;
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            if (drawable != null) {
                drawable3 = drawable.mutate();
            }
            this.A0A = drawable3;
            if (drawable3 != null) {
                C12980iv.A17(drawable3, this);
                this.A0A.setCallback(this);
                this.A0A.setAlpha(this.A05);
            }
            postInvalidateOnAnimation();
        }
    }

    public void setContentScrimColor(int i) {
        setContentScrim(new ColorDrawable(i));
    }

    public void setContentScrimResource(int i) {
        setContentScrim(AnonymousClass00T.A04(getContext(), i));
    }

    public void setExpandedTitleColor(int i) {
        setExpandedTitleTextColor(ColorStateList.valueOf(i));
    }

    public void setExpandedTitleGravity(int i) {
        C47742Cf r1 = this.A0M;
        if (r1.A0M != i) {
            r1.A0M = i;
            r1.A03();
        }
    }

    public void setExpandedTitleMarginBottom(int i) {
        this.A01 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginEnd(int i) {
        this.A02 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginStart(int i) {
        this.A03 = i;
        requestLayout();
    }

    public void setExpandedTitleMarginTop(int i) {
        this.A04 = i;
        requestLayout();
    }

    public void setExpandedTitleTextAppearance(int i) {
        this.A0M.A09(i);
    }

    public void setExpandedTitleTextColor(ColorStateList colorStateList) {
        C47742Cf r1 = this.A0M;
        if (r1.A0Q != colorStateList) {
            r1.A0Q = colorStateList;
            r1.A03();
        }
    }

    public void setExpandedTitleTypeface(Typeface typeface) {
        C47742Cf r1 = this.A0M;
        if (r1.A0V != typeface) {
            r1.A0V = typeface;
            r1.A03();
        }
    }

    public void setScrimAlpha(int i) {
        Toolbar toolbar;
        if (i != this.A05) {
            if (!(this.A0A == null || (toolbar = this.A0E) == null)) {
                toolbar.postInvalidateOnAnimation();
            }
            this.A05 = i;
            postInvalidateOnAnimation();
        }
    }

    public void setScrimAnimationDuration(long j) {
        this.A08 = j;
    }

    public void setScrimVisibleHeightTrigger(int i) {
        if (this.A06 != i) {
            this.A06 = i;
            A03();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (isInEditMode() != false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setScrimsShown(boolean r6) {
        /*
            r5 = this;
            boolean r0 = X.AnonymousClass028.A0r(r5)
            if (r0 == 0) goto L_0x000d
            boolean r0 = r5.isInEditMode()
            r1 = 1
            if (r0 == 0) goto L_0x000e
        L_0x000d:
            r1 = 0
        L_0x000e:
            boolean r0 = r5.A0K
            if (r0 == r6) goto L_0x0055
            r4 = 255(0xff, float:3.57E-43)
            if (r1 == 0) goto L_0x0065
            if (r6 != 0) goto L_0x0019
            r4 = 0
        L_0x0019:
            r5.A01()
            android.animation.ValueAnimator r0 = r5.A09
            if (r0 != 0) goto L_0x0059
            android.animation.ValueAnimator r2 = new android.animation.ValueAnimator
            r2.<init>()
            r5.A09 = r2
            long r0 = r5.A08
            r2.setDuration(r0)
            android.animation.ValueAnimator r1 = r5.A09
            int r0 = r5.A05
            if (r4 <= r0) goto L_0x0056
            android.animation.TimeInterpolator r0 = X.C50732Qs.A01
        L_0x0034:
            r1.setInterpolator(r0)
            android.animation.ValueAnimator r1 = r5.A09
            r0 = 1
            X.C12980iv.A11(r1, r5, r0)
        L_0x003d:
            android.animation.ValueAnimator r3 = r5.A09
            int[] r2 = X.C13000ix.A07()
            r1 = 0
            int r0 = r5.A05
            r2[r1] = r0
            r0 = 1
            r2[r0] = r4
            r3.setIntValues(r2)
            android.animation.ValueAnimator r0 = r5.A09
            r0.start()
        L_0x0053:
            r5.A0K = r6
        L_0x0055:
            return
        L_0x0056:
            android.animation.TimeInterpolator r0 = X.C50732Qs.A04
            goto L_0x0034
        L_0x0059:
            boolean r0 = r0.isRunning()
            if (r0 == 0) goto L_0x003d
            android.animation.ValueAnimator r0 = r5.A09
            r0.cancel()
            goto L_0x003d
        L_0x0065:
            if (r6 != 0) goto L_0x0068
            r4 = 0
        L_0x0068:
            r5.setScrimAlpha(r4)
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.CollapsingToolbarLayout.setScrimsShown(boolean):void");
    }

    public void setStatusBarScrim(Drawable drawable) {
        Drawable drawable2 = this.A0B;
        if (drawable2 != drawable) {
            Drawable drawable3 = null;
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            if (drawable != null) {
                drawable3 = drawable.mutate();
            }
            this.A0B = drawable3;
            if (drawable3 != null) {
                if (drawable3.isStateful()) {
                    this.A0B.setState(getDrawableState());
                }
                C015607k.A0D(AnonymousClass028.A05(this), this.A0B);
                this.A0B.setVisible(C12960it.A1T(getVisibility()), false);
                this.A0B.setCallback(this);
                this.A0B.setAlpha(this.A05);
            }
            postInvalidateOnAnimation();
        }
    }

    public void setStatusBarScrimColor(int i) {
        setStatusBarScrim(new ColorDrawable(i));
    }

    public void setStatusBarScrimResource(int i) {
        setStatusBarScrim(AnonymousClass00T.A04(getContext(), i));
    }

    public void setTitle(CharSequence charSequence) {
        this.A0M.A0D(charSequence);
        setContentDescription(getTitle());
    }

    public void setTitleEnabled(boolean z) {
        if (z != this.A0H) {
            this.A0H = z;
            setContentDescription(getTitle());
            A02();
            requestLayout();
        }
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean A1T = C12960it.A1T(i);
        Drawable drawable = this.A0B;
        if (!(drawable == null || drawable.isVisible() == A1T)) {
            this.A0B.setVisible(A1T, false);
        }
        Drawable drawable2 = this.A0A;
        if (drawable2 != null && drawable2.isVisible() != A1T) {
            this.A0A.setVisible(A1T, false);
        }
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A0A || drawable == this.A0B;
    }
}
