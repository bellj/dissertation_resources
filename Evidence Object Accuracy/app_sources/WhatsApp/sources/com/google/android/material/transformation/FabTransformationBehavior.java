package com.google.android.material.transformation;

import X.AnonymousClass028;
import X.AnonymousClass0B5;
import X.AnonymousClass2R0;
import X.AnonymousClass4MM;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C50732Qs;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class FabTransformationBehavior extends ExpandableTransformationBehavior {
    public final Rect A00 = C12980iv.A0J();
    public final RectF A01 = C12980iv.A0K();
    public final RectF A02 = C12980iv.A0K();
    public final int[] A03 = C13000ix.A07();

    public FabTransformationBehavior() {
    }

    public FabTransformationBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public static final float A00(AnonymousClass2R0 r8, AnonymousClass4MM r9, float f) {
        long j = r8.A02;
        long j2 = r8.A03;
        AnonymousClass2R0 A03 = r9.A00.A03("expansion");
        float f2 = ((float) (((A03.A02 + A03.A03) + 17) - j)) / ((float) j2);
        TimeInterpolator timeInterpolator = r8.A04;
        if (timeInterpolator == null) {
            timeInterpolator = C50732Qs.A02;
        }
        return f + (timeInterpolator.getInterpolation(f2) * (0.0f - f));
    }

    public static final void A01(View view, View view2, AnonymousClass4MM r7, List list, boolean z, boolean z2) {
        Property property;
        float[] fArr;
        float f;
        float A00 = AnonymousClass028.A00(view2) - AnonymousClass028.A00(view);
        if (z) {
            if (!z2) {
                view2.setTranslationZ(-A00);
            }
            property = View.TRANSLATION_Z;
            fArr = new float[1];
            f = 0.0f;
        } else {
            property = View.TRANSLATION_Z;
            fArr = new float[1];
            f = -A00;
        }
        fArr[0] = f;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view2, property, fArr);
        r7.A00.A03("elevation").A00(ofFloat);
        list.add(ofFloat);
    }

    @Override // X.AbstractC009304r
    public void A0H(AnonymousClass0B5 r2) {
        if (r2.A01 == 0) {
            r2.A01 = 80;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fe, code lost:
        if ((r3 instanceof android.view.ViewGroup) != false) goto L_0x0100;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x013f A[LOOP:0: B:48:0x013d->B:49:0x013f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0199  */
    @Override // com.google.android.material.transformation.ExpandableTransformationBehavior
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.animation.AnimatorSet A0J(android.view.View r23, android.view.View r24, boolean r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 447
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.transformation.FabTransformationBehavior.A0J(android.view.View, android.view.View, boolean, boolean):android.animation.AnimatorSet");
    }

    public final void A0K(RectF rectF, View view) {
        rectF.set(0.0f, 0.0f, C12990iw.A02(view), C12990iw.A03(view));
        int[] iArr = this.A03;
        view.getLocationInWindow(iArr);
        rectF.offsetTo((float) iArr[0], (float) iArr[1]);
        rectF.offset((float) ((int) (-view.getTranslationX())), (float) ((int) (-view.getTranslationY())));
    }
}
