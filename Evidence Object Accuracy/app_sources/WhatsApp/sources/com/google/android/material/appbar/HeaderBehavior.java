package com.google.android.material.appbar;

import X.C92334Vm;
import android.content.Context;
import android.util.AttributeSet;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.OverScroller;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import java.lang.ref.WeakReference;

/* loaded from: classes2.dex */
public abstract class HeaderBehavior extends ViewOffsetBehavior {
    public int A00 = -1;
    public int A01;
    public int A02 = -1;
    public VelocityTracker A03;
    public OverScroller A04;
    public Runnable A05;
    public boolean A06;

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002c, code lost:
        if (r2 != 3) goto L_0x002e;
     */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0C(android.view.MotionEvent r7, android.view.View r8, androidx.coordinatorlayout.widget.CoordinatorLayout r9) {
        /*
            r6 = this;
            int r0 = r6.A02
            if (r0 >= 0) goto L_0x0012
            android.content.Context r0 = r9.getContext()
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            int r0 = r0.getScaledTouchSlop()
            r6.A02 = r0
        L_0x0012:
            int r0 = r7.getAction()
            r5 = 2
            r4 = 1
            if (r0 != r5) goto L_0x001f
            boolean r0 = r6.A06
            if (r0 == 0) goto L_0x001f
            return r4
        L_0x001f:
            int r2 = r7.getActionMasked()
            r3 = 0
            if (r2 == 0) goto L_0x0063
            r1 = -1
            if (r2 == r4) goto L_0x0056
            if (r2 == r5) goto L_0x0038
            r0 = 3
            if (r2 == r0) goto L_0x0056
        L_0x002e:
            android.view.VelocityTracker r0 = r6.A03
            if (r0 == 0) goto L_0x0035
            r0.addMovement(r7)
        L_0x0035:
            boolean r0 = r6.A06
            return r0
        L_0x0038:
            int r0 = r6.A00
            if (r0 == r1) goto L_0x002e
            int r0 = r7.findPointerIndex(r0)
            if (r0 == r1) goto L_0x002e
            float r0 = r7.getY(r0)
            int r2 = (int) r0
            int r0 = r6.A01
            int r1 = X.C12980iv.A05(r2, r0)
            int r0 = r6.A02
            if (r1 <= r0) goto L_0x002e
            r6.A06 = r4
            r6.A01 = r2
            goto L_0x002e
        L_0x0056:
            r6.A06 = r3
            r6.A00 = r1
            android.view.VelocityTracker r0 = r6.A03
            if (r0 == 0) goto L_0x002e
            r0.recycle()
            r0 = 0
            goto L_0x008b
        L_0x0063:
            r6.A06 = r3
            float r0 = r7.getX()
            int r2 = (int) r0
            float r0 = r7.getY()
            int r1 = (int) r0
            boolean r0 = r6.A0L()
            if (r0 == 0) goto L_0x002e
            boolean r0 = r9.A0I(r8, r2, r1)
            if (r0 == 0) goto L_0x002e
            r6.A01 = r1
            int r0 = r7.getPointerId(r3)
            r6.A00 = r0
            android.view.VelocityTracker r0 = r6.A03
            if (r0 != 0) goto L_0x002e
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
        L_0x008b:
            r6.A03 = r0
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.HeaderBehavior.A0C(android.view.MotionEvent, android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r7 != 3) goto L_0x002b;
     */
    @Override // X.AbstractC009304r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0D(android.view.MotionEvent r20, android.view.View r21, androidx.coordinatorlayout.widget.CoordinatorLayout r22) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.HeaderBehavior.A0D(android.view.MotionEvent, android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout):boolean");
    }

    public int A0I() {
        int i;
        if (!(this instanceof AppBarLayout.BaseBehavior)) {
            C92334Vm r0 = super.A01;
            if (r0 != null) {
                return r0.A02;
            }
            return 0;
        }
        AppBarLayout.BaseBehavior baseBehavior = (AppBarLayout.BaseBehavior) this;
        C92334Vm r02 = ((ViewOffsetBehavior) baseBehavior).A01;
        if (r02 != null) {
            i = r02.A02;
        } else {
            i = 0;
        }
        return i + baseBehavior.A02;
    }

    /* JADX WARNING: Removed duplicated region for block: B:64:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A0J(android.view.View r18, androidx.coordinatorlayout.widget.CoordinatorLayout r19, int r20, int r21, int r22) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.HeaderBehavior.A0J(android.view.View, androidx.coordinatorlayout.widget.CoordinatorLayout, int, int, int):int");
    }

    public void A0K(View view, CoordinatorLayout coordinatorLayout, int i) {
        A0J(view, coordinatorLayout, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public boolean A0L() {
        View view;
        if (!(this instanceof AppBarLayout.BaseBehavior)) {
            return false;
        }
        WeakReference weakReference = ((AppBarLayout.BaseBehavior) this).A05;
        if (weakReference == null || ((view = (View) weakReference.get()) != null && view.isShown() && !view.canScrollVertically(-1))) {
            return true;
        }
        return false;
    }
}
