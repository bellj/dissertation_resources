package com.google.android.material.chip;

import X.AnonymousClass3NK;
import X.AnonymousClass5R5;
import X.C102424p7;
import X.C12980iv;
import X.C50572Qb;
import X.C50582Qc;
import X.C52542bC;
import X.C73743gj;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ChipGroup extends C52542bC {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass5R5 A03;
    public AnonymousClass3NK A04;
    public boolean A05;
    public boolean A06;
    public final C102424p7 A07;

    public ChipGroup(Context context) {
        this(context, null);
    }

    public ChipGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.chipGroupStyle);
    }

    public ChipGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A07 = new C102424p7(this);
        this.A04 = new AnonymousClass3NK(this);
        this.A00 = -1;
        this.A05 = false;
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A04, new int[0], i, 2131952652);
        int dimensionPixelOffset = A00.getDimensionPixelOffset(1, 0);
        setChipSpacingHorizontal(A00.getDimensionPixelOffset(2, dimensionPixelOffset));
        setChipSpacingVertical(A00.getDimensionPixelOffset(3, dimensionPixelOffset));
        super.A02 = A00.getBoolean(4, false);
        setSingleSelection(A00.getBoolean(5, false));
        int resourceId = A00.getResourceId(0, -1);
        if (resourceId != -1) {
            this.A00 = resourceId;
        }
        A00.recycle();
        super.setOnHierarchyChangeListener(this.A04);
    }

    public final void A00(int i, boolean z) {
        View findViewById = findViewById(i);
        if (findViewById instanceof Chip) {
            this.A05 = true;
            ((CompoundButton) findViewById).setChecked(z);
            this.A05 = false;
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof Chip) {
            CompoundButton compoundButton = (CompoundButton) view;
            if (compoundButton.isChecked()) {
                int i2 = this.A00;
                if (i2 != -1 && this.A06) {
                    A00(i2, false);
                }
                this.A00 = compoundButton.getId();
            }
        }
        super.addView(view, i, layoutParams);
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof C73743gj);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new C73743gj();
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C73743gj(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new C73743gj(layoutParams);
    }

    public int getCheckedChipId() {
        if (this.A06) {
            return this.A00;
        }
        return -1;
    }

    public int getChipSpacingHorizontal() {
        return this.A01;
    }

    public int getChipSpacingVertical() {
        return this.A02;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        int i = this.A00;
        if (i != -1) {
            A00(i, true);
            this.A00 = this.A00;
        }
    }

    private void setCheckedId(int i) {
        this.A00 = i;
    }

    public void setChipSpacing(int i) {
        setChipSpacingHorizontal(i);
        setChipSpacingVertical(i);
    }

    public void setChipSpacingHorizontal(int i) {
        if (this.A01 != i) {
            this.A01 = i;
            super.A00 = i;
            requestLayout();
        }
    }

    public void setChipSpacingHorizontalResource(int i) {
        setChipSpacingHorizontal(getResources().getDimensionPixelOffset(i));
    }

    public void setChipSpacingResource(int i) {
        setChipSpacing(getResources().getDimensionPixelOffset(i));
    }

    public void setChipSpacingVertical(int i) {
        if (this.A02 != i) {
            this.A02 = i;
            super.A01 = i;
            requestLayout();
        }
    }

    public void setChipSpacingVerticalResource(int i) {
        setChipSpacingVertical(getResources().getDimensionPixelOffset(i));
    }

    @Deprecated
    public void setDividerDrawableHorizontal(Drawable drawable) {
        throw C12980iv.A0u("Changing divider drawables have no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setDividerDrawableVertical(Drawable drawable) {
        throw C12980iv.A0u("Changing divider drawables have no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setFlexWrap(int i) {
        throw C12980iv.A0u("Changing flex wrap not allowed. ChipGroup exposes a singleLine attribute instead.");
    }

    public void setOnCheckedChangeListener(AnonymousClass5R5 r1) {
        this.A03 = r1;
    }

    @Override // android.view.ViewGroup
    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.A04.A00 = onHierarchyChangeListener;
    }

    @Deprecated
    public void setShowDividerHorizontal(int i) {
        throw C12980iv.A0u("Changing divider modes has no effect. ChipGroup do not use divider drawables as spacing.");
    }

    @Deprecated
    public void setShowDividerVertical(int i) {
        throw C12980iv.A0u("Changing divider modes has no effect. ChipGroup do not use divider drawables as spacing.");
    }

    public void setSingleLine(int i) {
        super.A02 = getResources().getBoolean(i);
    }

    public void setSingleSelection(int i) {
        setSingleSelection(getResources().getBoolean(i));
    }

    public void setSingleSelection(boolean z) {
        if (this.A06 != z) {
            this.A06 = z;
            this.A05 = true;
            for (int i = 0; i < getChildCount(); i++) {
                View childAt = getChildAt(i);
                if (childAt instanceof Chip) {
                    ((CompoundButton) childAt).setChecked(false);
                }
            }
            this.A05 = false;
            this.A00 = -1;
        }
    }
}
