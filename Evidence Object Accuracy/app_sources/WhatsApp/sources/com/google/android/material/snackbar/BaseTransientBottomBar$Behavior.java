package com.google.android.material.snackbar;

import X.C64883Hh;
import X.C88954Ic;
import android.view.MotionEvent;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.behavior.SwipeDismissBehavior;

/* loaded from: classes2.dex */
public class BaseTransientBottomBar$Behavior extends SwipeDismissBehavior {
    public final C88954Ic A00 = new C88954Ic(this);

    @Override // com.google.android.material.behavior.SwipeDismissBehavior, X.AbstractC009304r
    public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        C88954Ic r2 = this.A00;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked == 1 || actionMasked == 3) {
                C64883Hh.A00().A03(r2.A00);
            }
        } else if (coordinatorLayout.A0I(view, (int) motionEvent.getX(), (int) motionEvent.getY())) {
            C64883Hh.A00().A02(r2.A00);
        }
        return super.A0C(motionEvent, view, coordinatorLayout);
    }
}
