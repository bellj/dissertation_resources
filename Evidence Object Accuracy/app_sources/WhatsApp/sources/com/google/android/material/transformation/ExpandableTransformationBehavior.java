package com.google.android.material.transformation;

import X.AnonymousClass2R0;
import X.AnonymousClass2R4;
import X.C12960it;
import X.C12990iw;
import X.C72813fD;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public abstract class ExpandableTransformationBehavior extends ExpandableBehavior {
    public AnimatorSet A00;

    public ExpandableTransformationBehavior() {
    }

    public ExpandableTransformationBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // com.google.android.material.transformation.ExpandableBehavior
    public boolean A0I(View view, View view2, boolean z, boolean z2) {
        AnimatorSet animatorSet = this.A00;
        boolean z3 = false;
        if (animatorSet != null) {
            z3 = true;
            animatorSet.cancel();
        }
        AnimatorSet A0J = A0J(view, view2, z, z3);
        this.A00 = A0J;
        C12990iw.A0w(A0J, this, 1);
        this.A00.start();
        if (!z2) {
            this.A00.end();
        }
        return true;
    }

    public AnimatorSet A0J(View view, View view2, boolean z, boolean z2) {
        AnonymousClass2R0 r4;
        Property property;
        float[] fArr;
        FabTransformationScrimBehavior fabTransformationScrimBehavior = (FabTransformationScrimBehavior) this;
        ArrayList A0l = C12960it.A0l();
        if (z) {
            r4 = fabTransformationScrimBehavior.A01;
        } else {
            r4 = fabTransformationScrimBehavior.A00;
        }
        float f = 0.0f;
        if (z) {
            if (!z2) {
                view2.setAlpha(0.0f);
            }
            property = View.ALPHA;
            fArr = new float[1];
            f = 1.0f;
        } else {
            property = View.ALPHA;
            fArr = new float[1];
        }
        fArr[0] = f;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view2, property, fArr);
        r4.A00(ofFloat);
        A0l.add(ofFloat);
        AnimatorSet animatorSet = new AnimatorSet();
        AnonymousClass2R4.A00(animatorSet, A0l);
        animatorSet.addListener(new C72813fD(view2, fabTransformationScrimBehavior, z));
        return animatorSet;
    }
}
