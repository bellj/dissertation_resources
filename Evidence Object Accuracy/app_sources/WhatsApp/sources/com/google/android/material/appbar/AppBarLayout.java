package com.google.android.material.appbar;

import X.AbstractC009304r;
import X.AbstractC115785Sx;
import X.AbstractC117155Ys;
import X.AnonymousClass028;
import X.AnonymousClass02D;
import X.AnonymousClass0B5;
import X.C018408o;
import X.C104064rl;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C50572Qb;
import X.C50582Qc;
import X.C50732Qs;
import X.C53072cg;
import X.C53642em;
import X.C65203Ip;
import X.C65463Jr;
import X.C92334Vm;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import java.lang.ref.WeakReference;
import java.util.List;

@CoordinatorLayout.DefaultBehavior(Behavior.class)
/* loaded from: classes2.dex */
public class AppBarLayout extends LinearLayout {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public C018408o A04;
    public List A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public int[] A0A;

    @Deprecated
    public float getTargetElevation() {
        return 0.0f;
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    public AppBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A03 = -1;
        this.A00 = -1;
        this.A01 = -1;
        this.A02 = 0;
        setOrientation(1);
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            C65203Ip.A00(this);
            C65203Ip.A02(this, attributeSet);
        }
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A00, new int[0], 0, 2131952622);
        setBackground(A00.getDrawable(0));
        if (A00.hasValue(4)) {
            this.A02 = C12990iw.A04(A00.getBoolean(4, false) ? 1 : 0) | 0 | 0;
            requestLayout();
        }
        if (i >= 21 && A00.hasValue(3)) {
            C65203Ip.A01(this, (float) A00.getDimensionPixelSize(3, 0));
        }
        if (i >= 26) {
            if (A00.hasValue(2)) {
                setKeyboardNavigationCluster(A00.getBoolean(2, false));
            }
            if (A00.hasValue(1)) {
                setTouchscreenBlocksFocus(A00.getBoolean(1, false));
            }
        }
        this.A07 = A00.getBoolean(5, false);
        A00.recycle();
        AnonymousClass028.A0h(this, new C104064rl(this));
    }

    public static C53072cg A00(ViewGroup.LayoutParams layoutParams) {
        if (Build.VERSION.SDK_INT >= 19 && (layoutParams instanceof LinearLayout.LayoutParams)) {
            return new C53072cg((LinearLayout.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C53072cg((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C53072cg(layoutParams);
    }

    public void A01(AbstractC117155Ys r2) {
        List list = this.A05;
        if (list == null) {
            list = C12960it.A0l();
            this.A05 = list;
        }
        if (!list.contains(r2)) {
            this.A05.add(r2);
        }
    }

    @Override // android.widget.LinearLayout, android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C53072cg;
    }

    public int getDownNestedPreScrollRange() {
        int topInset;
        int i = this.A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            C53072cg r4 = (C53072cg) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i3 = r4.A00;
            if ((i3 & 5) == 5) {
                int i4 = i2 + ((LinearLayout.LayoutParams) r4).topMargin + ((LinearLayout.LayoutParams) r4).bottomMargin;
                if ((i3 & 8) != 0) {
                    i2 = i4 + childAt.getMinimumHeight();
                } else {
                    if ((i3 & 2) != 0) {
                        topInset = childAt.getMinimumHeight();
                    } else {
                        topInset = getTopInset();
                    }
                    i2 = i4 + (measuredHeight - topInset);
                }
            } else if (i2 > 0) {
                break;
            }
        }
        int max = Math.max(0, i2);
        this.A00 = max;
        return max;
    }

    public int getDownNestedScrollRange() {
        int i = this.A01;
        if (i != -1) {
            return i;
        }
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            C53072cg r3 = (C53072cg) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + ((LinearLayout.LayoutParams) r3).topMargin + ((LinearLayout.LayoutParams) r3).bottomMargin;
            int i4 = r3.A00;
            if ((i4 & 1) == 0) {
                break;
            }
            i3 += measuredHeight;
            if ((i4 & 2) != 0) {
                i3 -= childAt.getMinimumHeight() + getTopInset();
                break;
            }
            i2++;
        }
        int max = Math.max(0, i3);
        this.A01 = max;
        return max;
    }

    public final int getMinimumHeightForVisibleOverlappingContent() {
        int childCount;
        int topInset = getTopInset();
        int minimumHeight = getMinimumHeight();
        if (minimumHeight != 0 || ((childCount = getChildCount()) >= 1 && (minimumHeight = getChildAt(childCount - 1).getMinimumHeight()) != 0)) {
            return (minimumHeight << 1) + topInset;
        }
        return getHeight() / 3;
    }

    public int getPendingAction() {
        return this.A02;
    }

    public final int getTopInset() {
        C018408o r0 = this.A04;
        if (r0 != null) {
            return r0.A06();
        }
        return 0;
    }

    public final int getTotalScrollRange() {
        int i = this.A03;
        if (i != -1) {
            return i;
        }
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            C53072cg r3 = (C53072cg) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i4 = r3.A00;
            if ((i4 & 1) == 0) {
                break;
            }
            i3 += measuredHeight + ((LinearLayout.LayoutParams) r3).topMargin + ((LinearLayout.LayoutParams) r3).bottomMargin;
            if ((i4 & 2) != 0) {
                i3 -= childAt.getMinimumHeight();
                break;
            }
            i2++;
        }
        int max = Math.max(0, i3 - getTopInset());
        this.A03 = max;
        return max;
    }

    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r1 == false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
        if (r1 == false) goto L_0x003d;
     */
    @Override // android.view.View, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int[] onCreateDrawableState(int r7) {
        /*
            r6 = this;
            int[] r4 = r6.A0A
            if (r4 != 0) goto L_0x0009
            r0 = 4
            int[] r4 = new int[r0]
            r6.A0A = r4
        L_0x0009:
            int r0 = r4.length
            int r7 = r7 + r0
            int[] r5 = super.onCreateDrawableState(r7)
            r1 = 0
            boolean r3 = r6.A08
            r0 = 2130969530(0x7f0403ba, float:1.7547744E38)
            if (r3 != 0) goto L_0x0018
            int r0 = -r0
        L_0x0018:
            r4[r1] = r0
            r2 = 1
            if (r3 == 0) goto L_0x0024
            boolean r1 = r6.A09
            r0 = 2130969531(0x7f0403bb, float:1.7547747E38)
            if (r1 != 0) goto L_0x0028
        L_0x0024:
            r0 = 2130969531(0x7f0403bb, float:1.7547747E38)
            int r0 = -r0
        L_0x0028:
            r4[r2] = r0
            r1 = 2
            r0 = 2130969529(0x7f0403b9, float:1.7547742E38)
            if (r3 != 0) goto L_0x0031
            int r0 = -r0
        L_0x0031:
            r4[r1] = r0
            r2 = 3
            if (r3 == 0) goto L_0x003d
            boolean r1 = r6.A09
            r0 = 2130969528(0x7f0403b8, float:1.754774E38)
            if (r1 != 0) goto L_0x0041
        L_0x003d:
            r0 = 2130969528(0x7f0403b8, float:1.754774E38)
            int r0 = -r0
        L_0x0041:
            r4[r2] = r0
            int[] r0 = android.widget.LinearLayout.mergeDrawableStates(r5, r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.onCreateDrawableState(int):int[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    @Override // android.widget.LinearLayout, android.view.View, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r7, int r8, int r9, int r10, int r11) {
        /*
            r6 = this;
            super.onLayout(r7, r8, r9, r10, r11)
            r0 = -1
            r6.A03 = r0
            r6.A00 = r0
            r6.A01 = r0
            r5 = 0
            r6.A06 = r5
            int r2 = r6.getChildCount()
            r1 = 0
        L_0x0012:
            r4 = 1
            if (r1 >= r2) goto L_0x0025
            android.view.View r0 = r6.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            X.2cg r0 = (X.C53072cg) r0
            android.view.animation.Interpolator r0 = r0.A01
            if (r0 == 0) goto L_0x0052
            r6.A06 = r4
        L_0x0025:
            boolean r0 = r6.A07
            if (r0 != 0) goto L_0x0044
            int r3 = r6.getChildCount()
            r2 = 0
        L_0x002e:
            if (r2 >= r3) goto L_0x0045
            android.view.View r0 = r6.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            X.2cg r0 = (X.C53072cg) r0
            int r1 = r0.A00
            r0 = r1 & 1
            if (r0 != r4) goto L_0x004f
            r0 = r1 & 10
            if (r0 == 0) goto L_0x004f
        L_0x0044:
            r5 = 1
        L_0x0045:
            boolean r0 = r6.A08
            if (r0 == r5) goto L_0x004e
            r6.A08 = r5
            r6.refreshDrawableState()
        L_0x004e:
            return
        L_0x004f:
            int r2 = r2 + 1
            goto L_0x002e
        L_0x0052:
            int r1 = r1 + 1
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.A03 = -1;
        this.A00 = -1;
        this.A01 = -1;
    }

    public void setExpanded(boolean z) {
        boolean A0r = AnonymousClass028.A0r(this);
        int A04 = C12990iw.A04(z ? 1 : 0);
        int i = 0;
        if (A0r) {
            i = 4;
        }
        this.A02 = A04 | i | 8;
        requestLayout();
    }

    public void setLiftOnScroll(boolean z) {
        this.A07 = z;
    }

    @Override // android.widget.LinearLayout
    public void setOrientation(int i) {
        if (i == 1) {
            super.setOrientation(i);
            return;
        }
        throw C12970iu.A0f("AppBarLayout is always vertical and does not support horizontal orientation");
    }

    @Deprecated
    public void setTargetElevation(float f) {
        if (Build.VERSION.SDK_INT >= 21) {
            C65203Ip.A01(this, f);
        }
    }

    /* loaded from: classes2.dex */
    public class BaseBehavior extends HeaderBehavior {
        public float A00;
        public int A01;
        public int A02;
        public int A03 = -1;
        public ValueAnimator A04;
        public WeakReference A05;
        public boolean A06;

        public BaseBehavior() {
        }

        public BaseBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ void A01(View view, View view2, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4, int i5) {
            boolean A1U;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (i4 < 0) {
                A0J(appBarLayout, coordinatorLayout, A0I() - i4, -appBarLayout.getDownNestedScrollRange(), 0);
                if (i5 == 1 && A0I() == 0 && (view2 instanceof AnonymousClass02D)) {
                    ((AnonymousClass02D) view2).AeR(1);
                }
            }
            if (appBarLayout.A07 && appBarLayout.A09 != (A1U = C12960it.A1U(view2.getScrollY()))) {
                appBarLayout.A09 = A1U;
                appBarLayout.refreshDrawableState();
            }
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ boolean A07(View view, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4) {
            if (C12970iu.A0H(view).height != -2) {
                return false;
            }
            coordinatorLayout.A0E(view, i, i2, View.MeasureSpec.makeMeasureSpec(0, 0));
            return true;
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ Parcelable A08(View view, CoordinatorLayout coordinatorLayout) {
            int i;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            AbsSavedState absSavedState = View.BaseSavedState.EMPTY_STATE;
            C92334Vm r0 = ((ViewOffsetBehavior) this).A01;
            if (r0 != null) {
                i = r0.A02;
            } else {
                i = 0;
            }
            int childCount = appBarLayout.getChildCount();
            boolean z = false;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                int bottom = childAt.getBottom() + i;
                if (childAt.getTop() + i <= 0 && bottom >= 0) {
                    C53642em r2 = new C53642em(absSavedState);
                    r2.A01 = i2;
                    if (bottom == childAt.getMinimumHeight() + appBarLayout.getTopInset()) {
                        z = true;
                    }
                    r2.A02 = z;
                    r2.A00 = ((float) bottom) / C12990iw.A03(childAt);
                    return r2;
                }
            }
            return absSavedState;
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ void A09(Parcelable parcelable, View view, CoordinatorLayout coordinatorLayout) {
            if (parcelable instanceof C53642em) {
                C53642em r2 = (C53642em) parcelable;
                this.A03 = r2.A01;
                this.A00 = r2.A00;
                this.A06 = r2.A02;
                return;
            }
            this.A03 = -1;
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ void A0A(View view, View view2, CoordinatorLayout coordinatorLayout, int i) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (this.A01 == 0 || i == 1) {
                A0M(coordinatorLayout, appBarLayout);
            }
            this.A05 = C12970iu.A10(view2);
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ void A0B(View view, View view2, CoordinatorLayout coordinatorLayout, int[] iArr, int i, int i2, int i3) {
            int i4;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (i2 != 0) {
                int i5 = -appBarLayout.getTotalScrollRange();
                if (i2 < 0) {
                    i4 = appBarLayout.getDownNestedPreScrollRange() + i5;
                } else {
                    i4 = 0;
                }
                if (i5 != i4) {
                    iArr[1] = A0J(appBarLayout, coordinatorLayout, A0I() - i2, i5, i4);
                    if (i3 == 1) {
                        int A0I = A0I();
                        if (i2 < 0) {
                            if (A0I != 0) {
                                return;
                            }
                        } else if (A0I != (-appBarLayout.getDownNestedScrollRange())) {
                            return;
                        }
                        if (view2 instanceof AnonymousClass02D) {
                            ((AnonymousClass02D) view2).AeR(1);
                        }
                    }
                }
            }
        }

        @Override // X.AbstractC009304r
        public /* bridge */ /* synthetic */ boolean A0E(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i, int i2) {
            boolean z;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if ((i & 2) == 0 || (!appBarLayout.A07 && (appBarLayout.getTotalScrollRange() == 0 || coordinatorLayout.getHeight() - view2.getHeight() > appBarLayout.getHeight()))) {
                z = false;
            } else {
                z = true;
                ValueAnimator valueAnimator = this.A04;
                if (valueAnimator != null) {
                    valueAnimator.cancel();
                }
            }
            this.A05 = null;
            this.A01 = i2;
            return z;
        }

        @Override // com.google.android.material.appbar.ViewOffsetBehavior, X.AbstractC009304r
        public /* bridge */ /* synthetic */ boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
            int i2;
            int i3;
            int round;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            boolean A0G = super.A0G(appBarLayout, coordinatorLayout, i);
            int i4 = appBarLayout.A02;
            int i5 = this.A03;
            if (i5 >= 0 && (i4 & 8) == 0) {
                View childAt = appBarLayout.getChildAt(i5);
                int i6 = -childAt.getBottom();
                if (this.A06) {
                    round = childAt.getMinimumHeight() + appBarLayout.getTopInset();
                } else {
                    round = Math.round(C12990iw.A03(childAt) * this.A00);
                }
                A0K(appBarLayout, coordinatorLayout, i6 + round);
            } else if (i4 != 0) {
                boolean A1S = C12960it.A1S(i4 & 4);
                if ((i4 & 2) != 0) {
                    int i7 = -appBarLayout.getTotalScrollRange();
                    if (A1S) {
                        A0N(coordinatorLayout, appBarLayout, i7);
                    } else {
                        A0K(appBarLayout, coordinatorLayout, i7);
                    }
                } else if ((i4 & 1) != 0) {
                    if (A1S) {
                        A0N(coordinatorLayout, appBarLayout, 0);
                    } else {
                        A0K(appBarLayout, coordinatorLayout, 0);
                    }
                }
            }
            appBarLayout.A02 = 0;
            this.A03 = -1;
            C92334Vm r0 = ((ViewOffsetBehavior) this).A01;
            if (r0 != null) {
                i2 = r0.A02;
            } else {
                i2 = 0;
            }
            int i8 = -appBarLayout.getTotalScrollRange();
            int i9 = 0;
            if (i2 < i8) {
                i2 = i8;
            } else if (i2 > 0) {
                i2 = 0;
            }
            C92334Vm r1 = ((ViewOffsetBehavior) this).A01;
            if (r1 == null) {
                ((ViewOffsetBehavior) this).A00 = i2;
            } else if (r1.A02 != i2) {
                r1.A02 = i2;
                r1.A00();
            }
            C92334Vm r02 = ((ViewOffsetBehavior) this).A01;
            if (r02 != null) {
                i9 = r02.A02;
            }
            A0O(coordinatorLayout, appBarLayout, i9, 0, true);
            C92334Vm r03 = ((ViewOffsetBehavior) this).A01;
            if (r03 != null) {
                i3 = r03.A02;
            } else {
                i3 = 0;
            }
            List list = appBarLayout.A05;
            if (list != null) {
                int size = list.size();
                for (int i10 = 0; i10 < size; i10++) {
                    AbstractC115785Sx r04 = (AbstractC115785Sx) appBarLayout.A05.get(i10);
                    if (r04 != null) {
                        r04.ATD(appBarLayout, i3);
                    }
                }
            }
            return A0G;
        }

        public final void A0M(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            int A0I = A0I();
            int childCount = appBarLayout.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = appBarLayout.getChildAt(i);
                int top = childAt.getTop();
                int bottom = childAt.getBottom();
                C53072cg r2 = (C53072cg) childAt.getLayoutParams();
                if ((r2.A00 & 32) == 32) {
                    top -= ((LinearLayout.LayoutParams) r2).topMargin;
                    bottom += ((LinearLayout.LayoutParams) r2).bottomMargin;
                }
                int i2 = -A0I;
                if (top <= i2 && bottom >= i2) {
                    if (i >= 0) {
                        View childAt2 = appBarLayout.getChildAt(i);
                        C53072cg r6 = (C53072cg) childAt2.getLayoutParams();
                        int i3 = r6.A00;
                        if ((i3 & 17) == 17) {
                            int i4 = -childAt2.getTop();
                            int i5 = -childAt2.getBottom();
                            if (i == appBarLayout.getChildCount() - 1) {
                                i5 += appBarLayout.getTopInset();
                            }
                            if ((i3 & 2) == 2) {
                                i5 += childAt2.getMinimumHeight();
                            } else if ((i3 & 5) == 5) {
                                int minimumHeight = childAt2.getMinimumHeight() + i5;
                                if (A0I < minimumHeight) {
                                    i4 = minimumHeight;
                                } else {
                                    i5 = minimumHeight;
                                }
                            }
                            if ((i3 & 32) == 32) {
                                i4 += ((LinearLayout.LayoutParams) r6).topMargin;
                                i5 -= ((LinearLayout.LayoutParams) r6).bottomMargin;
                            }
                            if (A0I < (i5 + i4) / 2) {
                                i4 = i5;
                            }
                            int i6 = -appBarLayout.getTotalScrollRange();
                            if (i4 >= i6) {
                                i6 = i4;
                                if (i4 > 0) {
                                    i6 = 0;
                                }
                            }
                            A0N(coordinatorLayout, appBarLayout, i6);
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                }
            }
        }

        public final void A0N(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            int A03;
            int A05 = C12980iv.A05(A0I(), i);
            float abs = Math.abs(0.0f);
            if (abs > 0.0f) {
                A03 = Math.round((((float) A05) / abs) * 1000.0f) * 3;
            } else {
                A03 = (int) (((((float) A05) / C12990iw.A03(appBarLayout)) + 1.0f) * 150.0f);
            }
            int A0I = A0I();
            ValueAnimator valueAnimator = this.A04;
            if (A0I != i) {
                if (valueAnimator == null) {
                    ValueAnimator valueAnimator2 = new ValueAnimator();
                    this.A04 = valueAnimator2;
                    valueAnimator2.setInterpolator(C50732Qs.A00);
                    this.A04.addUpdateListener(new C65463Jr(coordinatorLayout, this, appBarLayout));
                } else {
                    valueAnimator.cancel();
                }
                this.A04.setDuration((long) Math.min(A03, 600));
                this.A04.setIntValues(C12970iu.A1a(A0I, i));
                this.A04.start();
            } else if (valueAnimator != null && valueAnimator.isRunning()) {
                this.A04.cancel();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
            if (r2 < r1) goto L_0x0040;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void A0O(androidx.coordinatorlayout.widget.CoordinatorLayout r7, com.google.android.material.appbar.AppBarLayout r8, int r9, int r10, boolean r11) {
            /*
                r6 = this;
                int r3 = java.lang.Math.abs(r9)
                int r2 = r8.getChildCount()
                r1 = 0
            L_0x0009:
                if (r1 >= r2) goto L_0x00a2
                android.view.View r4 = r8.getChildAt(r1)
                int r0 = r4.getTop()
                if (r3 < r0) goto L_0x00b0
                int r0 = r4.getBottom()
                if (r3 > r0) goto L_0x00b0
                android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
                X.2cg r0 = (X.C53072cg) r0
                int r1 = r0.A00
                r0 = r1 & 1
                r5 = 1
                if (r0 == 0) goto L_0x0040
                int r3 = r4.getMinimumHeight()
                if (r10 <= 0) goto L_0x00ab
                r0 = r1 & 12
                if (r0 == 0) goto L_0x00ab
            L_0x0032:
                int r2 = -r9
                int r1 = r4.getBottom()
                int r1 = r1 - r3
                int r0 = r8.getTopInset()
                int r1 = r1 - r0
                r4 = 1
                if (r2 >= r1) goto L_0x0041
            L_0x0040:
                r4 = 0
            L_0x0041:
                boolean r0 = r8.A07
                if (r0 == 0) goto L_0x005e
                int r3 = r7.getChildCount()
                r2 = 0
            L_0x004a:
                if (r2 >= r3) goto L_0x005e
                android.view.View r1 = r7.getChildAt(r2)
                boolean r0 = r1 instanceof X.AnonymousClass02E
                if (r0 == 0) goto L_0x00a8
                if (r1 == 0) goto L_0x005e
                int r0 = r1.getScrollY()
                if (r0 > 0) goto L_0x005d
                r5 = 0
            L_0x005d:
                r4 = r5
            L_0x005e:
                boolean r0 = r8.A09
                if (r0 == r4) goto L_0x00a6
                r8.A09 = r4
                r8.refreshDrawableState()
                r0 = 1
            L_0x0068:
                if (r11 != 0) goto L_0x009f
                if (r0 == 0) goto L_0x00a2
                X.0OY r0 = r7.A0C
                X.00O r0 = r0.A00
                java.lang.Object r0 = r0.get(r8)
                java.util.Collection r0 = (java.util.Collection) r0
                java.util.List r4 = r7.A0F
                r4.clear()
                if (r0 == 0) goto L_0x0080
                r4.addAll(r0)
            L_0x0080:
                int r3 = r4.size()
                r2 = 0
            L_0x0085:
                if (r2 >= r3) goto L_0x00a2
                java.lang.Object r0 = r4.get(r2)
                android.view.View r0 = (android.view.View) r0
                android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
                X.0B5 r0 = (X.AnonymousClass0B5) r0
                X.04r r1 = r0.A0A
                boolean r0 = r1 instanceof com.google.android.material.appbar.AppBarLayout.ScrollingViewBehavior
                if (r0 == 0) goto L_0x00a3
                com.google.android.material.appbar.HeaderScrollingViewBehavior r1 = (com.google.android.material.appbar.HeaderScrollingViewBehavior) r1
                int r0 = r1.A00
                if (r0 == 0) goto L_0x00a2
            L_0x009f:
                r8.jumpDrawablesToCurrentState()
            L_0x00a2:
                return
            L_0x00a3:
                int r2 = r2 + 1
                goto L_0x0085
            L_0x00a6:
                r0 = 0
                goto L_0x0068
            L_0x00a8:
                int r2 = r2 + 1
                goto L_0x004a
            L_0x00ab:
                r0 = r1 & 2
                if (r0 == 0) goto L_0x0040
                goto L_0x0032
            L_0x00b0:
                int r1 = r1 + 1
                goto L_0x0009
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.BaseBehavior.A0O(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void");
        }
    }

    /* loaded from: classes2.dex */
    public class ScrollingViewBehavior extends HeaderScrollingViewBehavior {
        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A0C);
            ((HeaderScrollingViewBehavior) this).A00 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
            obtainStyledAttributes.recycle();
        }

        @Override // X.AbstractC009304r
        public boolean A03(Rect rect, View view, CoordinatorLayout coordinatorLayout, boolean z) {
            AppBarLayout appBarLayout;
            List A07 = coordinatorLayout.A07(view);
            int size = A07.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    appBarLayout = null;
                    break;
                }
                View view2 = (View) A07.get(i);
                if (view2 instanceof AppBarLayout) {
                    appBarLayout = (AppBarLayout) view2;
                    break;
                }
                i++;
            }
            boolean z2 = false;
            if (appBarLayout != null) {
                rect.offset(view.getLeft(), view.getTop());
                Rect rect2 = this.A02;
                rect2.set(0, 0, coordinatorLayout.getWidth(), coordinatorLayout.getHeight());
                if (!rect2.contains(rect)) {
                    z2 = true;
                    int i2 = 0;
                    if (!z) {
                        i2 = 4;
                    }
                    appBarLayout.A02 = 2 | i2 | 8;
                    appBarLayout.requestLayout();
                }
            }
            return z2;
        }

        @Override // X.AbstractC009304r
        public boolean A05(View view, View view2, CoordinatorLayout coordinatorLayout) {
            return view2 instanceof AppBarLayout;
        }

        @Override // X.AbstractC009304r
        public boolean A06(View view, View view2, CoordinatorLayout coordinatorLayout) {
            boolean A1U;
            AbstractC009304r r1 = ((AnonymousClass0B5) view2.getLayoutParams()).A0A;
            if (r1 instanceof BaseBehavior) {
                int bottom = (view2.getBottom() - view.getTop()) + ((BaseBehavior) r1).A02 + ((HeaderScrollingViewBehavior) this).A01;
                int i = 0;
                if (((HeaderScrollingViewBehavior) this).A00 != 0) {
                    float A0I = A0I(view2);
                    int i2 = ((HeaderScrollingViewBehavior) this).A00;
                    int i3 = (int) (A0I * ((float) i2));
                    if (i3 >= 0) {
                        i = i3;
                        if (i3 > i2) {
                            i = i2;
                        }
                    }
                }
                AnonymousClass028.A0Y(view, bottom - i);
            }
            if (!(view2 instanceof AppBarLayout)) {
                return false;
            }
            AppBarLayout appBarLayout = (AppBarLayout) view2;
            if (!appBarLayout.A07 || appBarLayout.A09 == (A1U = C12960it.A1U(view.getScrollY()))) {
                return false;
            }
            appBarLayout.A09 = A1U;
            appBarLayout.refreshDrawableState();
            return false;
        }
    }

    /* loaded from: classes3.dex */
    public class Behavior extends BaseBehavior {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }
    }
}
