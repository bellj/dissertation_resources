package com.google.android.material.tabs;

import X.AbstractC12350hm;
import X.AbstractC467727m;
import X.AnonymousClass01A;
import X.AnonymousClass028;
import X.AnonymousClass07O;
import X.AnonymousClass0DQ;
import X.AnonymousClass2cj;
import X.AnonymousClass3FN;
import X.AnonymousClass3S8;
import X.AnonymousClass51D;
import X.C015007d;
import X.C07420Xx;
import X.C105714uV;
import X.C50572Qb;
import X.C50582Qc;
import X.C50592Qd;
import X.C50732Qs;
import X.C53092ck;
import X.C72993fV;
import X.C95764eJ;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import com.whatsapp.R;
import com.whatsapp.WaTabLayout;
import com.whatsapp.WaViewPager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ViewPager.DecorView
/* loaded from: classes2.dex */
public class TabLayout extends HorizontalScrollView {
    public static final AbstractC12350hm A0d = new AnonymousClass0DQ(16);
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public ValueAnimator A0D;
    public ColorStateList A0E;
    public ColorStateList A0F;
    public ColorStateList A0G;
    public DataSetObserver A0H;
    public Drawable A0I;
    public AnonymousClass01A A0J;
    public ViewPager A0K;
    public C105714uV A0L;
    public AbstractC467727m A0M;
    public AbstractC467727m A0N;
    public AnonymousClass3FN A0O;
    public AnonymousClass3S8 A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public final int A0U;
    public final int A0V;
    public final int A0W;
    public final int A0X;
    public final RectF A0Y;
    public final AbstractC12350hm A0Z;
    public final AnonymousClass2cj A0a;
    public final ArrayList A0b;
    public final ArrayList A0c;

    public TabLayout(Context context) {
        this(context, null);
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.tabStyle);
    }

    /* JADX INFO: finally extract failed */
    public TabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0c = new ArrayList();
        this.A0Y = new RectF();
        this.A07 = Integer.MAX_VALUE;
        this.A0b = new ArrayList();
        this.A0Z = new C07420Xx(12);
        setHorizontalScrollBarEnabled(false);
        AnonymousClass2cj r5 = new AnonymousClass2cj(context, this);
        this.A0a = r5;
        super.addView(r5, 0, new FrameLayout.LayoutParams(-2, -1));
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A0E, new int[]{22}, i, 2131952630);
        r5.setSelectedIndicatorHeight(A00.getDimensionPixelSize(10, -1));
        r5.setSelectedIndicatorColor(A00.getColor(7, 0));
        setSelectedTabIndicator(C50592Qd.A01(context, A00, 5));
        setSelectedTabIndicatorGravity(A00.getInt(9, 0));
        setTabIndicatorFullWidth(A00.getBoolean(8, true));
        int dimensionPixelSize = A00.getDimensionPixelSize(15, 0);
        this.A08 = dimensionPixelSize;
        this.A09 = dimensionPixelSize;
        this.A0B = dimensionPixelSize;
        this.A0A = dimensionPixelSize;
        this.A0A = A00.getDimensionPixelSize(18, dimensionPixelSize);
        this.A0B = A00.getDimensionPixelSize(19, this.A0B);
        this.A09 = A00.getDimensionPixelSize(17, this.A09);
        this.A08 = A00.getDimensionPixelSize(16, this.A08);
        int resourceId = A00.getResourceId(22, 2131952308);
        this.A0C = resourceId;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(resourceId, AnonymousClass07O.A0M);
        try {
            this.A01 = (float) obtainStyledAttributes.getDimensionPixelSize(0, 0);
            this.A0G = C50592Qd.A00(context, obtainStyledAttributes, 3);
            obtainStyledAttributes.recycle();
            if (A00.hasValue(23)) {
                this.A0G = C50592Qd.A00(context, A00, 23);
            }
            if (A00.hasValue(21)) {
                this.A0G = new ColorStateList(new int[][]{HorizontalScrollView.SELECTED_STATE_SET, HorizontalScrollView.EMPTY_STATE_SET}, new int[]{A00.getColor(21, 0), this.A0G.getDefaultColor()});
            }
            this.A0E = C50592Qd.A00(context, A00, 3);
            A00.getInt(4, -1);
            this.A0F = C50592Qd.A00(context, A00, 20);
            this.A05 = A00.getInt(6, 300);
            this.A0V = A00.getDimensionPixelSize(13, -1);
            this.A0U = A00.getDimensionPixelSize(12, -1);
            this.A0X = A00.getResourceId(0, 0);
            this.A02 = A00.getDimensionPixelSize(1, 0);
            this.A03 = A00.getInt(14, 1);
            this.A04 = A00.getInt(2, 0);
            this.A0Q = A00.getBoolean(11, false);
            this.A0T = A00.getBoolean(24, false);
            A00.recycle();
            Resources resources = getResources();
            this.A00 = (float) resources.getDimensionPixelSize(R.dimen.design_tab_text_size_2line);
            this.A0W = resources.getDimensionPixelSize(R.dimen.design_tab_scrollable_min_width);
            A07();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    private void A00(int i) {
        if (i != -1) {
            if (getWindowToken() != null && AnonymousClass028.A0r(this)) {
                AnonymousClass2cj r5 = this.A0a;
                int childCount = r5.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    if (r5.getChildAt(i2).getWidth() > 0) {
                    }
                }
                int scrollX = getScrollX();
                int A02 = A02(i, 0.0f);
                if (scrollX != A02) {
                    A08();
                    this.A0D.setIntValues(scrollX, A02);
                    this.A0D.start();
                }
                r5.A01(i, this.A05);
                return;
            }
            A09(0.0f, i, true, true);
        }
    }

    public int A01(int i) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i));
    }

    public final int A02(int i, float f) {
        View view;
        int i2;
        int i3 = 0;
        if (this.A03 != 0) {
            return 0;
        }
        AnonymousClass2cj r3 = this.A0a;
        View childAt = r3.getChildAt(i);
        int i4 = i + 1;
        if (i4 < r3.getChildCount()) {
            view = r3.getChildAt(i4);
        } else {
            view = null;
        }
        if (childAt != null) {
            i2 = childAt.getWidth();
        } else {
            i2 = 0;
        }
        if (view != null) {
            i3 = view.getWidth();
        }
        int left = (childAt.getLeft() + (i2 >> 1)) - (getWidth() >> 1);
        int i5 = (int) (((float) (i2 + i3)) * 0.5f * f);
        if (AnonymousClass028.A05(this) == 0) {
            return left + i5;
        }
        return left - i5;
    }

    public AnonymousClass3FN A03() {
        C53092ck r1;
        CharSequence charSequence;
        AnonymousClass3FN r2 = (AnonymousClass3FN) A0d.A5a();
        if (r2 == null) {
            r2 = new AnonymousClass3FN();
        }
        r2.A03 = this;
        AbstractC12350hm r0 = this.A0Z;
        if (r0 == null || (r1 = (C53092ck) r0.A5a()) == null) {
            r1 = new C53092ck(getContext(), this);
        }
        r1.setTab(r2);
        r1.setFocusable(true);
        r1.setMinimumWidth(getTabMinWidth());
        if (TextUtils.isEmpty(r2.A04)) {
            charSequence = r2.A05;
        } else {
            charSequence = r2.A04;
        }
        r1.setContentDescription(charSequence);
        r2.A02 = r1;
        return r2;
    }

    public AnonymousClass3FN A04(int i) {
        if (i < 0) {
            return null;
        }
        ArrayList arrayList = this.A0c;
        if (i < arrayList.size()) {
            return (AnonymousClass3FN) arrayList.get(i);
        }
        return null;
    }

    public void A05() {
        int currentItem;
        A06();
        AnonymousClass01A r0 = this.A0J;
        if (r0 != null) {
            int A01 = r0.A01();
            for (int i = 0; i < A01; i++) {
                AnonymousClass3FN A03 = A03();
                A03.A02(this.A0J.A04(i));
                A0F(A03, this.A0c.size(), false);
            }
            ViewPager viewPager = this.A0K;
            if (viewPager != null && A01 > 0 && (currentItem = viewPager.getCurrentItem()) != getSelectedTabPosition() && currentItem < this.A0c.size()) {
                A0G(A04(currentItem), true);
            }
        }
    }

    public void A06() {
        AnonymousClass2cj r3 = this.A0a;
        int childCount = r3.getChildCount();
        while (true) {
            childCount--;
            if (childCount < 0) {
                break;
            }
            C53092ck r1 = (C53092ck) r3.getChildAt(childCount);
            r3.removeViewAt(childCount);
            if (r1 != null) {
                r1.setTab(null);
                r1.setSelected(false);
                this.A0Z.Aa6(r1);
            }
            requestLayout();
        }
        Iterator it = this.A0c.iterator();
        while (it.hasNext()) {
            AnonymousClass3FN r2 = (AnonymousClass3FN) it.next();
            it.remove();
            r2.A03 = null;
            r2.A02 = null;
            r2.A06 = null;
            r2.A05 = null;
            r2.A04 = null;
            r2.A00 = -1;
            r2.A01 = null;
            A0d.Aa6(r2);
        }
        this.A0O = null;
    }

    public final void A07() {
        int i;
        if (this.A03 == 0) {
            i = Math.max(0, this.A02 - this.A0A);
        } else {
            i = 0;
        }
        AnonymousClass2cj r2 = this.A0a;
        AnonymousClass028.A0e(r2, i, 0, 0, 0);
        int i2 = this.A03;
        if (i2 == 0) {
            r2.setGravity(8388611);
        } else if (i2 == 1) {
            r2.setGravity(1);
        }
        A0H(true);
    }

    public final void A08() {
        if (this.A0D == null) {
            ValueAnimator valueAnimator = new ValueAnimator();
            this.A0D = valueAnimator;
            valueAnimator.setInterpolator(C50732Qs.A02);
            this.A0D.setDuration((long) this.A05);
            this.A0D.addUpdateListener(new C95764eJ(this));
        }
    }

    public void A09(float f, int i, boolean z, boolean z2) {
        int round = Math.round(((float) i) + f);
        if (round >= 0) {
            AnonymousClass2cj r1 = this.A0a;
            if (round < r1.getChildCount()) {
                if (z2) {
                    ValueAnimator valueAnimator = r1.A06;
                    if (valueAnimator != null && valueAnimator.isRunning()) {
                        r1.A06.cancel();
                    }
                    r1.A05 = i;
                    r1.A00 = f;
                    r1.A00();
                }
                ValueAnimator valueAnimator2 = this.A0D;
                if (valueAnimator2 != null && valueAnimator2.isRunning()) {
                    this.A0D.cancel();
                }
                scrollTo(A02(i, f), 0);
                if (z) {
                    setSelectedTabView(round);
                }
            }
        }
    }

    public void A0A(int i, int i2) {
        setTabTextColors(new ColorStateList(new int[][]{HorizontalScrollView.SELECTED_STATE_SET, HorizontalScrollView.EMPTY_STATE_SET}, new int[]{i2, i}));
    }

    public void A0B(AnonymousClass01A r3, boolean z) {
        DataSetObserver dataSetObserver;
        AnonymousClass01A r1 = this.A0J;
        if (!(r1 == null || (dataSetObserver = this.A0H) == null)) {
            r1.A08(dataSetObserver);
        }
        this.A0J = r3;
        if (z && r3 != null) {
            DataSetObserver dataSetObserver2 = this.A0H;
            if (dataSetObserver2 == null) {
                dataSetObserver2 = new C72993fV(this);
                this.A0H = dataSetObserver2;
            }
            r3.A07(dataSetObserver2);
        }
        A05();
    }

    public final void A0C(ViewPager viewPager, boolean z) {
        List list;
        List list2;
        ViewPager viewPager2 = this.A0K;
        if (viewPager2 != null) {
            AnonymousClass3S8 r1 = this.A0P;
            if (!(r1 == null || (list2 = viewPager2.A0c) == null)) {
                list2.remove(r1);
            }
            C105714uV r12 = this.A0L;
            if (!(r12 == null || (list = this.A0K.A0b) == null)) {
                list.remove(r12);
            }
        }
        AbstractC467727m r2 = this.A0M;
        if (r2 != null) {
            this.A0b.remove(r2);
            this.A0M = null;
        }
        if (viewPager != null) {
            this.A0K = viewPager;
            AnonymousClass3S8 r13 = this.A0P;
            if (r13 == null) {
                r13 = new AnonymousClass3S8(this);
                this.A0P = r13;
            }
            r13.A01 = 0;
            r13.A00 = 0;
            viewPager.A0G(r13);
            AnonymousClass51D r0 = new AnonymousClass51D(viewPager);
            this.A0M = r0;
            A0D(r0);
            AnonymousClass01A r02 = viewPager.A0V;
            if (r02 != null) {
                A0B(r02, true);
            }
            C105714uV r14 = this.A0L;
            if (r14 == null) {
                r14 = new C105714uV(this);
                this.A0L = r14;
            }
            r14.A00 = true;
            List list3 = viewPager.A0b;
            if (list3 == null) {
                list3 = new ArrayList();
                viewPager.A0b = list3;
            }
            list3.add(r14);
            A09(0.0f, viewPager.getCurrentItem(), true, true);
        } else {
            this.A0K = null;
            A0B(null, false);
        }
        this.A0R = z;
    }

    public void A0D(AbstractC467727m r3) {
        ArrayList arrayList = this.A0b;
        if (!arrayList.contains(r3)) {
            arrayList.add(r3);
        }
    }

    public void A0E(AnonymousClass3FN r3) {
        ArrayList arrayList = this.A0c;
        A0F(r3, arrayList.size(), arrayList.isEmpty());
    }

    public void A0F(AnonymousClass3FN r8, int i, boolean z) {
        float f;
        if (r8.A03 == this) {
            r8.A00 = i;
            ArrayList arrayList = this.A0c;
            arrayList.add(i, r8);
            int size = arrayList.size();
            while (true) {
                i++;
                if (i >= size) {
                    break;
                }
                ((AnonymousClass3FN) arrayList.get(i)).A00 = i;
            }
            C53092ck r6 = r8.A02;
            AnonymousClass2cj r5 = this.A0a;
            int i2 = r8.A00;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
            if (this.A03 == 1 && this.A04 == 0) {
                layoutParams.width = 0;
                f = 1.0f;
            } else {
                layoutParams.width = -2;
                f = 0.0f;
            }
            layoutParams.weight = f;
            r5.addView(r6, i2, layoutParams);
            if (z) {
                r8.A00();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
    }

    public void A0G(AnonymousClass3FN r6, boolean z) {
        int i;
        AnonymousClass3FN r4 = this.A0O;
        if (r4 != r6) {
            if (r6 != null) {
                i = r6.A00;
            } else {
                i = -1;
            }
            if (z) {
                if ((r4 == null || r4.A00 == -1) && i != -1) {
                    A09(0.0f, i, true, true);
                } else {
                    A00(i);
                }
                if (i != -1) {
                    setSelectedTabView(i);
                }
            }
            this.A0O = r6;
            if (r4 != null) {
                ArrayList arrayList = this.A0b;
                int size = arrayList.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    arrayList.get(size);
                }
            }
            if (r6 != null) {
                ArrayList arrayList2 = this.A0b;
                int size2 = arrayList2.size();
                while (true) {
                    size2--;
                    if (size2 >= 0) {
                        ((AbstractC467727m) arrayList2.get(size2)).AXO(r6);
                    } else {
                        return;
                    }
                }
            }
        } else if (r4 != null) {
            ArrayList arrayList3 = this.A0b;
            int size3 = arrayList3.size();
            while (true) {
                size3--;
                if (size3 >= 0) {
                    ((AbstractC467727m) arrayList3.get(size3)).AXN(r6);
                } else {
                    A00(r6.A00);
                    return;
                }
            }
        }
    }

    public void A0H(boolean z) {
        float f;
        int i = 0;
        while (true) {
            AnonymousClass2cj r1 = this.A0a;
            if (i < r1.getChildCount()) {
                View childAt = r1.getChildAt(i);
                childAt.setMinimumWidth(getTabMinWidth());
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
                if (this.A03 == 1 && this.A04 == 0) {
                    layoutParams.width = 0;
                    f = 1.0f;
                } else {
                    layoutParams.width = -2;
                    f = 0.0f;
                }
                layoutParams.weight = f;
                if (z) {
                    childAt.requestLayout();
                }
                i++;
            } else {
                return;
            }
        }
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view) {
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view, int i) {
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    @Override // android.widget.HorizontalScrollView, android.view.ViewGroup, android.view.ViewManager
    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    private int getDefaultHeight() {
        ArrayList arrayList = this.A0c;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList.get(i);
        }
        return 48;
    }

    public int getSelectedTabPosition() {
        AnonymousClass3FN r0 = this.A0O;
        if (r0 != null) {
            return r0.A00;
        }
        return -1;
    }

    public int getTabCount() {
        return this.A0c.size();
    }

    public int getTabGravity() {
        return this.A04;
    }

    public ColorStateList getTabIconTint() {
        return this.A0E;
    }

    public int getTabIndicatorGravity() {
        return this.A06;
    }

    public int getTabMaxWidth() {
        return this.A07;
    }

    private int getTabMinWidth() {
        int i = this.A0V;
        if (i != -1) {
            return i;
        }
        if (this.A03 == 0) {
            return this.A0W;
        }
        return 0;
    }

    public int getTabMode() {
        return this.A03;
    }

    public ColorStateList getTabRippleColor() {
        return this.A0F;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.A0a.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    public Drawable getTabSelectedIndicator() {
        return this.A0I;
    }

    public ColorStateList getTabTextColors() {
        return this.A0G;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.A0K == null) {
            ViewParent parent = getParent();
            if (parent instanceof ViewPager) {
                A0C((ViewPager) parent, true);
            }
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.A0R) {
            setupWithViewPager(null);
            this.A0R = false;
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        C53092ck r5;
        Drawable drawable;
        int i = 0;
        while (true) {
            AnonymousClass2cj r1 = this.A0a;
            if (i < r1.getChildCount()) {
                View childAt = r1.getChildAt(i);
                if ((childAt instanceof C53092ck) && (drawable = (r5 = (C53092ck) childAt).A01) != null) {
                    drawable.setBounds(r5.getLeft(), r5.getTop(), r5.getRight(), r5.getBottom());
                    r5.A01.draw(canvas);
                }
                i++;
            } else {
                super.onDraw(canvas);
                return;
            }
        }
    }

    @Override // android.widget.HorizontalScrollView, android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int A01 = A01(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            i2 = View.MeasureSpec.makeMeasureSpec(Math.min(A01, View.MeasureSpec.getSize(i2)), 1073741824);
        } else if (mode == 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(A01, 1073741824);
        }
        int size = View.MeasureSpec.getSize(i);
        if (View.MeasureSpec.getMode(i) != 0) {
            int i3 = this.A0U;
            if (i3 <= 0) {
                i3 = size - A01(56);
            }
            this.A07 = i3;
        }
        super.onMeasure(i, i2);
        if (getChildCount() == 1) {
            View childAt = getChildAt(0);
            int i4 = this.A03;
            if (i4 != 0) {
                if (i4 != 1 || childAt.getMeasuredWidth() == getMeasuredWidth()) {
                    return;
                }
            } else if (childAt.getMeasuredWidth() >= getMeasuredWidth()) {
                return;
            }
            childAt.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), HorizontalScrollView.getChildMeasureSpec(i2, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
        }
    }

    public void setInlineLabel(boolean z) {
        ImageView imageView;
        if (this.A0Q != z) {
            this.A0Q = z;
            int i = 0;
            while (true) {
                AnonymousClass2cj r1 = this.A0a;
                if (i < r1.getChildCount()) {
                    View childAt = r1.getChildAt(i);
                    if (childAt instanceof C53092ck) {
                        C53092ck r2 = (C53092ck) childAt;
                        r2.setOrientation(!r2.A08.A0Q ? 1 : 0);
                        TextView textView = r2.A05;
                        if (textView == null && r2.A03 == null) {
                            textView = r2.A06;
                            imageView = r2.A04;
                        } else {
                            imageView = r2.A03;
                        }
                        r2.A02(imageView, textView);
                    }
                    i++;
                } else {
                    A07();
                    return;
                }
            }
        }
    }

    public void setInlineLabelResource(int i) {
        setInlineLabel(getResources().getBoolean(i));
    }

    @Deprecated
    public void setOnTabSelectedListener(AbstractC467727m r3) {
        AbstractC467727m r1 = this.A0N;
        if (r1 != null) {
            this.A0b.remove(r1);
        }
        this.A0N = r3;
        if (r3 != null) {
            A0D(r3);
        }
    }

    public void setScrollAnimatorListener(Animator.AnimatorListener animatorListener) {
        A08();
        this.A0D.addListener(animatorListener);
    }

    public void setSelectedTabIndicator(int i) {
        Drawable drawable;
        if (i != 0) {
            drawable = C015007d.A01(getContext(), i);
        } else {
            drawable = null;
        }
        setSelectedTabIndicator(drawable);
    }

    public void setSelectedTabIndicator(Drawable drawable) {
        if (this.A0I != drawable) {
            this.A0I = drawable;
            this.A0a.postInvalidateOnAnimation();
        }
    }

    public void setSelectedTabIndicatorColor(int i) {
        this.A0a.setSelectedIndicatorColor(i);
    }

    public void setSelectedTabIndicatorGravity(int i) {
        if (this.A06 != i) {
            this.A06 = i;
            this.A0a.postInvalidateOnAnimation();
        }
    }

    @Deprecated
    public void setSelectedTabIndicatorHeight(int i) {
        this.A0a.setSelectedIndicatorHeight(i);
    }

    private void setSelectedTabView(int i) {
        AnonymousClass2cj r5 = this.A0a;
        int childCount = r5.getChildCount();
        if (i < childCount) {
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = r5.getChildAt(i2);
                boolean z = true;
                boolean z2 = false;
                if (i2 == i) {
                    z2 = true;
                }
                childAt.setSelected(z2);
                if (i2 != i) {
                    z = false;
                }
                childAt.setActivated(z);
            }
        }
    }

    public void setTabGravity(int i) {
        if (this.A04 != i) {
            this.A04 = i;
            A07();
        }
    }

    public void setTabIconTint(ColorStateList colorStateList) {
        if (this.A0E != colorStateList) {
            this.A0E = colorStateList;
            ArrayList arrayList = this.A0c;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                C53092ck r0 = ((AnonymousClass3FN) arrayList.get(i)).A02;
                if (r0 != null) {
                    r0.A00();
                }
            }
        }
    }

    public void setTabIconTintResource(int i) {
        setTabIconTint(C015007d.A00(getContext(), i));
    }

    public void setTabIndicatorFullWidth(boolean z) {
        this.A0S = z;
        this.A0a.postInvalidateOnAnimation();
    }

    public void setTabMode(int i) {
        if (i != this.A03) {
            this.A03 = i;
            A07();
        }
    }

    public void setTabRippleColor(ColorStateList colorStateList) {
        if (this.A0F != colorStateList) {
            this.A0F = colorStateList;
            int i = 0;
            while (true) {
                AnonymousClass2cj r1 = this.A0a;
                if (i < r1.getChildCount()) {
                    View childAt = r1.getChildAt(i);
                    if (childAt instanceof C53092ck) {
                        ((C53092ck) childAt).A01(getContext());
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void setTabRippleColorResource(int i) {
        setTabRippleColor(C015007d.A00(getContext(), i));
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.A0G != colorStateList) {
            this.A0G = colorStateList;
            ArrayList arrayList = this.A0c;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                C53092ck r0 = ((AnonymousClass3FN) arrayList.get(i)).A02;
                if (r0 != null) {
                    r0.A00();
                }
            }
        }
    }

    @Deprecated
    public void setTabsFromPagerAdapter(AnonymousClass01A r2) {
        A0B(r2, false);
    }

    public void setUnboundedRipple(boolean z) {
        if (this.A0T != z) {
            this.A0T = z;
            int i = 0;
            while (true) {
                AnonymousClass2cj r1 = this.A0a;
                if (i < r1.getChildCount()) {
                    View childAt = r1.getChildAt(i);
                    if (childAt instanceof C53092ck) {
                        ((C53092ck) childAt).A01(getContext());
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void setUnboundedRippleResource(int i) {
        setUnboundedRipple(getResources().getBoolean(i));
    }

    public void setupWithViewPager(ViewPager viewPager) {
        if (!(this instanceof WaTabLayout) || viewPager == null || (viewPager instanceof WaViewPager)) {
            A0C(viewPager, false);
            return;
        }
        throw new IllegalArgumentException("WaTabLayout should only be setup with WaViewPager");
    }

    @Override // android.widget.HorizontalScrollView, android.widget.FrameLayout, android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }
}
