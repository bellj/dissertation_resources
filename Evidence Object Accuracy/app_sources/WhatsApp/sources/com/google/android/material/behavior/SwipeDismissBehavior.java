package com.google.android.material.behavior;

import X.AbstractC009304r;
import X.AnonymousClass04X;
import X.AnonymousClass5R4;
import X.C06260Su;
import X.C53712eu;
import android.view.MotionEvent;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* loaded from: classes2.dex */
public class SwipeDismissBehavior extends AbstractC009304r {
    public float A00 = 0.5f;
    public float A01 = 0.0f;
    public int A02 = 2;
    public C06260Su A03;
    public AnonymousClass5R4 A04;
    public boolean A05;
    public final AnonymousClass04X A06 = new C53712eu(this);

    @Override // X.AbstractC009304r
    public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        boolean z = this.A05;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            z = coordinatorLayout.A0I(view, (int) motionEvent.getX(), (int) motionEvent.getY());
            this.A05 = z;
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.A05 = false;
        }
        if (!z) {
            return false;
        }
        C06260Su r2 = this.A03;
        if (r2 == null) {
            r2 = new C06260Su(coordinatorLayout.getContext(), coordinatorLayout, this.A06);
            this.A03 = r2;
        }
        return r2.A0E(motionEvent);
    }

    @Override // X.AbstractC009304r
    public boolean A0D(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        C06260Su r0 = this.A03;
        if (r0 == null) {
            return false;
        }
        r0.A07(motionEvent);
        return true;
    }
}
