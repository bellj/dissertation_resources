package com.google.android.material.textfield;

import X.AbstractC015707l;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass02T;
import X.AnonymousClass02U;
import X.AnonymousClass04D;
import X.AnonymousClass08F;
import X.AnonymousClass2Ze;
import X.AnonymousClass3ID;
import X.C004602b;
import X.C011905s;
import X.C013406h;
import X.C014706y;
import X.C015007d;
import X.C015607k;
import X.C100684mJ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C47742Cf;
import X.C50572Qb;
import X.C50582Qc;
import X.C50602Qe;
import X.C50732Qs;
import X.C53592eh;
import X.C53652en;
import X.C64493Ft;
import X.C65313Jb;
import X.C87734Cr;
import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Parcelable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.material.internal.CheckableImageButton;
import com.whatsapp.R;
import java.lang.reflect.Method;

/* loaded from: classes2.dex */
public class TextInputLayout extends LinearLayout {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public ValueAnimator A0A;
    public ColorStateList A0B;
    public ColorStateList A0C;
    public ColorStateList A0D;
    public PorterDuff.Mode A0E;
    public Typeface A0F;
    public Drawable A0G;
    public Drawable A0H;
    public Drawable A0I;
    public Drawable A0J;
    public GradientDrawable A0K;
    public EditText A0L;
    public TextView A0M;
    public CheckableImageButton A0N;
    public CharSequence A0O;
    public CharSequence A0P;
    public CharSequence A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public final int A0e;
    public final int A0f;
    public final int A0g;
    public final int A0h;
    public final int A0i;
    public final int A0j;
    public final int A0k;
    public final int A0l;
    public final int A0m;
    public final int A0n;
    public final Rect A0o;
    public final RectF A0p;
    public final FrameLayout A0q;
    public final C47742Cf A0r;
    public final C64493Ft A0s;

    public TextInputLayout(Context context) {
        this(context, null);
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.textInputStyle);
    }

    public TextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0s = new C64493Ft(this);
        this.A0o = C12980iv.A0J();
        this.A0p = C12980iv.A0K();
        C47742Cf r4 = new C47742Cf(this);
        this.A0r = r4;
        setOrientation(1);
        setWillNotDraw(false);
        setAddStatesFromChildren(true);
        FrameLayout frameLayout = new FrameLayout(context);
        this.A0q = frameLayout;
        frameLayout.setAddStatesFromChildren(true);
        addView(frameLayout);
        TimeInterpolator timeInterpolator = C50732Qs.A03;
        r4.A0O = timeInterpolator;
        r4.A03();
        r4.A0N = timeInterpolator;
        r4.A03();
        if (r4.A0K != 8388659) {
            r4.A0K = 8388659;
            r4.A03();
        }
        int[] iArr = C50572Qb.A0G;
        C50582Qc.A01(context, attributeSet, i, 2131952631);
        C50582Qc.A02(context, attributeSet, iArr, new int[0], i, 2131952631);
        C013406h A00 = C013406h.A00(context, attributeSet, iArr, i, 2131952631);
        TypedArray typedArray = A00.A02;
        this.A0X = typedArray.getBoolean(21, true);
        setHint(typedArray.getText(1));
        this.A0W = typedArray.getBoolean(20, true);
        this.A0e = context.getResources().getDimensionPixelOffset(R.dimen.mtrl_textinput_box_bottom_offset);
        this.A0g = context.getResources().getDimensionPixelOffset(R.dimen.mtrl_textinput_box_label_cutout_padding);
        this.A0f = typedArray.getDimensionPixelOffset(4, 0);
        this.A03 = typedArray.getDimension(8, 0.0f);
        this.A02 = typedArray.getDimension(7, 0.0f);
        this.A00 = typedArray.getDimension(5, 0.0f);
        this.A01 = typedArray.getDimension(6, 0.0f);
        this.A04 = typedArray.getColor(2, 0);
        this.A09 = typedArray.getColor(9, 0);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.mtrl_textinput_box_stroke_width_default);
        this.A0h = dimensionPixelSize;
        this.A0i = context.getResources().getDimensionPixelSize(R.dimen.mtrl_textinput_box_stroke_width_focused);
        this.A07 = dimensionPixelSize;
        setBoxBackgroundMode(typedArray.getInt(3, 0));
        if (typedArray.hasValue(0)) {
            ColorStateList A01 = A00.A01(0);
            this.A0C = A01;
            this.A0B = A01;
        }
        this.A0l = AnonymousClass00T.A00(context, R.color.mtrl_textinput_default_box_stroke_color);
        this.A0m = AnonymousClass00T.A00(context, R.color.mtrl_textinput_disabled_color);
        this.A0n = AnonymousClass00T.A00(context, R.color.mtrl_textinput_hovered_box_stroke_color);
        if (typedArray.getResourceId(22, -1) != -1) {
            setHintTextAppearance(typedArray.getResourceId(22, 0));
        }
        int resourceId = typedArray.getResourceId(16, 0);
        boolean z = typedArray.getBoolean(15, false);
        int resourceId2 = typedArray.getResourceId(19, 0);
        boolean z2 = typedArray.getBoolean(18, false);
        CharSequence text = typedArray.getText(17);
        boolean z3 = typedArray.getBoolean(11, false);
        setCounterMaxLength(typedArray.getInt(12, -1));
        this.A0k = typedArray.getResourceId(14, 0);
        this.A0j = typedArray.getResourceId(13, 0);
        this.A0b = typedArray.getBoolean(25, false);
        this.A0I = A00.A02(24);
        this.A0Q = typedArray.getText(23);
        if (typedArray.hasValue(26)) {
            this.A0T = true;
            this.A0D = A00.A01(26);
        }
        if (typedArray.hasValue(27)) {
            this.A0U = true;
            this.A0E = C50602Qe.A00(null, typedArray.getInt(27, -1));
        }
        A00.A04();
        setHelperTextEnabled(z2);
        setHelperText(text);
        setHelperTextTextAppearance(resourceId2);
        setErrorEnabled(z);
        setErrorTextAppearance(resourceId);
        setCounterEnabled(z3);
        A06();
        AnonymousClass028.A0a(this, 2);
    }

    public static void A00(ViewGroup viewGroup, boolean z) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            childAt.setEnabled(z);
            if (childAt instanceof ViewGroup) {
                A00((ViewGroup) childAt, z);
            }
        }
    }

    public static Drawable[] A01(TextView textView) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            return AnonymousClass08F.A05(textView);
        }
        if (i < 17) {
            return textView.getCompoundDrawables();
        }
        boolean z = true;
        if (AnonymousClass08F.A00(textView) != 1) {
            z = false;
        }
        Drawable[] compoundDrawables = textView.getCompoundDrawables();
        if (!z) {
            return compoundDrawables;
        }
        Drawable drawable = compoundDrawables[2];
        Drawable drawable2 = compoundDrawables[0];
        compoundDrawables[0] = drawable;
        compoundDrawables[2] = drawable2;
        return compoundDrawables;
    }

    public final int A02() {
        float A00;
        if (this.A0X) {
            int i = this.A05;
            if (i == 0 || i == 1) {
                A00 = C12980iv.A00(this.A0r);
            } else if (i == 2) {
                A00 = C12980iv.A00(this.A0r) / 2.0f;
            }
            return (int) A00;
        }
        return 0;
    }

    public void A03() {
        Drawable background;
        Drawable background2;
        boolean z;
        TextView textView;
        int currentTextColor;
        EditText editText = this.A0L;
        if (editText != null && (background = editText.getBackground()) != null) {
            int i = Build.VERSION.SDK_INT;
            if ((i == 21 || i == 22) && (background2 = this.A0L.getBackground()) != null && !this.A0V) {
                Drawable newDrawable = background2.getConstantState().newDrawable();
                if (background2 instanceof DrawableContainer) {
                    Drawable.ConstantState constantState = newDrawable.getConstantState();
                    if (!C87734Cr.A01) {
                        try {
                            Method declaredMethod = DrawableContainer.class.getDeclaredMethod("setConstantState", DrawableContainer.DrawableContainerState.class);
                            C87734Cr.A00 = declaredMethod;
                            declaredMethod.setAccessible(true);
                        } catch (NoSuchMethodException unused) {
                            Log.e("DrawableUtils", "Could not fetch setConstantState(). Oh well.");
                        }
                        C87734Cr.A01 = true;
                    }
                    Method method = C87734Cr.A00;
                    if (method != null) {
                        try {
                            method.invoke(background2, constantState);
                            z = true;
                        } catch (Exception unused2) {
                            Log.e("DrawableUtils", "Could not invoke setConstantState(). Oh well.");
                        }
                        this.A0V = z;
                    }
                    z = false;
                    this.A0V = z;
                }
                if (!this.A0V) {
                    this.A0L.setBackground(newDrawable);
                    this.A0V = true;
                    A07();
                }
            }
            if (C014706y.A03(background)) {
                background = background.mutate();
            }
            C64493Ft r1 = this.A0s;
            if (r1.A05()) {
                textView = r1.A0A;
                if (textView == null) {
                    currentTextColor = -1;
                    background.setColorFilter(C011905s.A00(PorterDuff.Mode.SRC_IN, currentTextColor));
                }
            } else if (!this.A0S || (textView = this.A0M) == null) {
                C015607k.A08(background);
                this.A0L.refreshDrawableState();
                return;
            }
            currentTextColor = textView.getCurrentTextColor();
            background.setColorFilter(C011905s.A00(PorterDuff.Mode.SRC_IN, currentTextColor));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0.hasFocus() == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r4 = this;
            android.graphics.drawable.GradientDrawable r0 = r4.A0K
            if (r0 == 0) goto L_0x003f
            int r0 = r4.A05
            if (r0 == 0) goto L_0x003f
            android.widget.EditText r0 = r4.A0L
            r3 = 1
            if (r0 == 0) goto L_0x0014
            boolean r0 = r0.hasFocus()
            r2 = 1
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            r2 = 0
        L_0x0015:
            android.widget.EditText r0 = r4.A0L
            if (r0 == 0) goto L_0x006b
            boolean r0 = r0.isHovered()
            if (r0 == 0) goto L_0x006b
        L_0x001f:
            int r1 = r4.A05
            r0 = 2
            if (r1 != r0) goto L_0x003f
            boolean r0 = r4.isEnabled()
            if (r0 != 0) goto L_0x0043
            int r0 = r4.A0m
        L_0x002c:
            r4.A06 = r0
            if (r3 != 0) goto L_0x0032
            if (r2 == 0) goto L_0x0040
        L_0x0032:
            boolean r0 = r4.isEnabled()
            if (r0 == 0) goto L_0x0040
            int r0 = r4.A0i
        L_0x003a:
            r4.A07 = r0
            r4.A05()
        L_0x003f:
            return
        L_0x0040:
            int r0 = r4.A0h
            goto L_0x003a
        L_0x0043:
            X.3Ft r1 = r4.A0s
            boolean r0 = r1.A05()
            if (r0 == 0) goto L_0x0051
            android.widget.TextView r0 = r1.A0A
            if (r0 != 0) goto L_0x0059
            r0 = -1
            goto L_0x002c
        L_0x0051:
            boolean r0 = r4.A0S
            if (r0 == 0) goto L_0x005e
            android.widget.TextView r0 = r4.A0M
            if (r0 == 0) goto L_0x005e
        L_0x0059:
            int r0 = r0.getCurrentTextColor()
            goto L_0x002c
        L_0x005e:
            if (r2 == 0) goto L_0x0063
            int r0 = r4.A09
            goto L_0x002c
        L_0x0063:
            if (r3 == 0) goto L_0x0068
            int r0 = r4.A0n
            goto L_0x002c
        L_0x0068:
            int r0 = r4.A0l
            goto L_0x002c
        L_0x006b:
            r3 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.A04():void");
    }

    public final void A05() {
        int i;
        Drawable drawable;
        if (this.A0K != null) {
            int i2 = this.A05;
            if (i2 == 1) {
                this.A07 = 0;
            } else if (i2 == 2 && this.A09 == 0) {
                this.A09 = this.A0C.getColorForState(getDrawableState(), this.A0C.getDefaultColor());
            }
            EditText editText = this.A0L;
            if (editText != null && this.A05 == 2) {
                if (editText.getBackground() != null) {
                    this.A0G = this.A0L.getBackground();
                }
                this.A0L.setBackground(null);
            }
            EditText editText2 = this.A0L;
            if (!(editText2 == null || this.A05 != 1 || (drawable = this.A0G) == null)) {
                editText2.setBackground(drawable);
            }
            int i3 = this.A07;
            if (i3 > -1 && (i = this.A06) != 0) {
                this.A0K.setStroke(i3, i);
            }
            this.A0K.setCornerRadii(getCornerRadiiAsArray());
            this.A0K.setColor(this.A04);
            invalidate();
        }
    }

    public final void A06() {
        Drawable drawable;
        Drawable drawable2 = this.A0I;
        if (drawable2 == null) {
            return;
        }
        if (this.A0T || this.A0U) {
            Drawable mutate = C015607k.A03(drawable2).mutate();
            this.A0I = mutate;
            if (this.A0T) {
                C015607k.A04(this.A0D, mutate);
            }
            if (this.A0U) {
                C015607k.A07(this.A0E, this.A0I);
            }
            CheckableImageButton checkableImageButton = this.A0N;
            if (checkableImageButton != null && checkableImageButton.getDrawable() != (drawable = this.A0I)) {
                this.A0N.setImageDrawable(drawable);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07() {
        /*
            r2 = this;
            int r1 = r2.A05
            r0 = 0
            if (r1 == 0) goto L_0x0017
            r0 = 2
            if (r1 != r0) goto L_0x0024
            boolean r0 = r2.A0X
            if (r0 == 0) goto L_0x0024
            android.graphics.drawable.GradientDrawable r0 = r2.A0K
            boolean r0 = r0 instanceof X.AnonymousClass2Ze
            if (r0 != 0) goto L_0x0024
            X.2Ze r0 = new X.2Ze
            r0.<init>()
        L_0x0017:
            r2.A0K = r0
        L_0x0019:
            int r0 = r2.A05
            if (r0 == 0) goto L_0x0020
            r2.A09()
        L_0x0020:
            r2.A0B()
            return
        L_0x0024:
            android.graphics.drawable.GradientDrawable r0 = r2.A0K
            if (r0 != 0) goto L_0x0019
            android.graphics.drawable.GradientDrawable r0 = new android.graphics.drawable.GradientDrawable
            r0.<init>()
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.A07():void");
    }

    public final void A08() {
        AnonymousClass02T r1;
        float measureText;
        float f;
        float f2;
        float measureText2;
        if (A0H()) {
            RectF rectF = this.A0p;
            C47742Cf r5 = this.A0r;
            CharSequence charSequence = r5.A0W;
            if (AnonymousClass028.A05(r5.A0i) == 1) {
                r1 = AnonymousClass02U.A02;
            } else {
                r1 = AnonymousClass02U.A01;
            }
            boolean AK0 = r1.AK0(charSequence, 0, charSequence.length());
            Rect rect = r5.A0d;
            if (!AK0) {
                f = (float) rect.left;
            } else {
                float f3 = (float) rect.right;
                if (r5.A0W == null) {
                    measureText = 0.0f;
                } else {
                    TextPaint textPaint = r5.A0h;
                    textPaint.setTextSize(r5.A05);
                    textPaint.setTypeface(r5.A0T);
                    CharSequence charSequence2 = r5.A0W;
                    measureText = textPaint.measureText(charSequence2, 0, charSequence2.length());
                }
                f = f3 - measureText;
            }
            rectF.left = f;
            rectF.top = (float) rect.top;
            if (!AK0) {
                if (r5.A0W == null) {
                    measureText2 = 0.0f;
                } else {
                    TextPaint textPaint2 = r5.A0h;
                    textPaint2.setTextSize(r5.A05);
                    textPaint2.setTypeface(r5.A0T);
                    CharSequence charSequence3 = r5.A0W;
                    measureText2 = textPaint2.measureText(charSequence3, 0, charSequence3.length());
                }
                f2 = f + measureText2;
            } else {
                f2 = (float) rect.right;
            }
            rectF.right = f2;
            float A00 = ((float) rect.top) + C12980iv.A00(r5);
            rectF.bottom = A00;
            float f4 = rectF.left;
            float f5 = (float) this.A0g;
            float f6 = f4 - f5;
            rectF.left = f6;
            float f7 = rectF.top - f5;
            rectF.top = f7;
            float f8 = rectF.right + f5;
            rectF.right = f8;
            float f9 = A00 + f5;
            rectF.bottom = f9;
            ((AnonymousClass2Ze) this.A0K).A00(f6, f7, f8, f9);
        }
    }

    public final void A09() {
        FrameLayout frameLayout = this.A0q;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) frameLayout.getLayoutParams();
        int A02 = A02();
        if (A02 != layoutParams.topMargin) {
            layoutParams.topMargin = A02;
            frameLayout.requestLayout();
        }
    }

    public final void A0A() {
        boolean z;
        EditText editText = this.A0L;
        if (editText != null) {
            if (!this.A0b || (!(editText.getTransformationMethod() instanceof PasswordTransformationMethod) && !this.A0c)) {
                z = false;
            } else {
                z = true;
            }
            CheckableImageButton checkableImageButton = this.A0N;
            if (z) {
                if (checkableImageButton == null) {
                    LayoutInflater A0E = C12960it.A0E(this);
                    FrameLayout frameLayout = this.A0q;
                    CheckableImageButton checkableImageButton2 = (CheckableImageButton) A0E.inflate(R.layout.design_text_input_password_icon, (ViewGroup) frameLayout, false);
                    this.A0N = checkableImageButton2;
                    checkableImageButton2.setImageDrawable(this.A0I);
                    this.A0N.setContentDescription(this.A0Q);
                    frameLayout.addView(this.A0N);
                    C12960it.A10(this.A0N, this, 2);
                }
                EditText editText2 = this.A0L;
                if (editText2 != null && editText2.getMinimumHeight() <= 0) {
                    this.A0L.setMinimumHeight(this.A0N.getMinimumHeight());
                }
                this.A0N.setVisibility(0);
                this.A0N.setChecked(this.A0c);
                Drawable drawable = this.A0J;
                if (drawable == null) {
                    drawable = new ColorDrawable();
                    this.A0J = drawable;
                }
                drawable.setBounds(0, 0, this.A0N.getMeasuredWidth(), 1);
                Drawable[] A01 = A01(this.A0L);
                Drawable drawable2 = A01[2];
                Drawable drawable3 = this.A0J;
                if (drawable2 != drawable3) {
                    this.A0H = drawable2;
                }
                AnonymousClass04D.A05(A01[0], A01[1], drawable3, A01[3], this.A0L);
                this.A0N.setPadding(this.A0L.getPaddingLeft(), this.A0L.getPaddingTop(), this.A0L.getPaddingRight(), this.A0L.getPaddingBottom());
                return;
            }
            if (checkableImageButton != null && checkableImageButton.getVisibility() == 0) {
                this.A0N.setVisibility(8);
            }
            if (this.A0J != null) {
                Drawable[] A012 = A01(this.A0L);
                if (A012[2] == this.A0J) {
                    AnonymousClass04D.A05(A012[0], A012[1], this.A0H, A012[3], this.A0L);
                    this.A0J = null;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B() {
        /*
            r7 = this;
            int r0 = r7.A05
            if (r0 == 0) goto L_0x0097
            android.graphics.drawable.GradientDrawable r0 = r7.A0K
            if (r0 == 0) goto L_0x0097
            android.widget.EditText r0 = r7.A0L
            if (r0 == 0) goto L_0x0097
            int r0 = r7.getRight()
            if (r0 == 0) goto L_0x0097
            android.widget.EditText r0 = r7.A0L
            int r6 = r0.getLeft()
            android.widget.EditText r2 = r7.A0L
            if (r2 == 0) goto L_0x009d
            int r1 = r7.A05
            r0 = 1
            if (r1 == r0) goto L_0x0098
            r0 = 2
            if (r1 != r0) goto L_0x009d
            int r1 = r2.getTop()
            int r0 = r7.A02()
            int r1 = r1 + r0
        L_0x002d:
            android.widget.EditText r0 = r7.A0L
            int r5 = r0.getRight()
            android.widget.EditText r0 = r7.A0L
            int r4 = r0.getBottom()
            int r0 = r7.A0e
            int r4 = r4 + r0
            int r0 = r7.A05
            r3 = 2
            if (r0 != r3) goto L_0x004a
            int r2 = r7.A0i
            int r0 = r2 >> 1
            int r6 = r6 + r0
            int r1 = r1 - r0
            int r5 = r5 - r0
            int r2 = r2 / r3
            int r4 = r4 + r2
        L_0x004a:
            android.graphics.drawable.GradientDrawable r0 = r7.A0K
            r0.setBounds(r6, r1, r5, r4)
            r7.A05()
            android.widget.EditText r0 = r7.A0L
            if (r0 == 0) goto L_0x0097
            android.graphics.drawable.Drawable r5 = r0.getBackground()
            if (r5 == 0) goto L_0x0097
            boolean r0 = X.C014706y.A03(r5)
            if (r0 == 0) goto L_0x0066
            android.graphics.drawable.Drawable r5 = r5.mutate()
        L_0x0066:
            android.graphics.Rect r1 = X.C12980iv.A0J()
            android.widget.EditText r0 = r7.A0L
            X.AnonymousClass3ID.A01(r1, r0, r7)
            android.graphics.Rect r4 = r5.getBounds()
            int r1 = r4.left
            int r0 = r4.right
            if (r1 == r0) goto L_0x0097
            android.graphics.Rect r1 = X.C12980iv.A0J()
            r5.getPadding(r1)
            int r3 = r4.left
            int r0 = r1.left
            int r3 = r3 - r0
            int r2 = r4.right
            int r0 = r1.right
            int r0 = r0 << 1
            int r2 = r2 + r0
            int r1 = r4.top
            android.widget.EditText r0 = r7.A0L
            int r0 = r0.getBottom()
            r5.setBounds(r3, r1, r2, r0)
        L_0x0097:
            return
        L_0x0098:
            int r1 = r2.getTop()
            goto L_0x002d
        L_0x009d:
            r1 = 0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.A0B():void");
    }

    public void A0C(float f) {
        C47742Cf r4 = this.A0r;
        if (r4.A0B != f) {
            if (this.A0A == null) {
                ValueAnimator valueAnimator = new ValueAnimator();
                this.A0A = valueAnimator;
                valueAnimator.setInterpolator(C50732Qs.A02);
                this.A0A.setDuration(167L);
                this.A0A.addUpdateListener(new C65313Jb(this));
            }
            this.A0A.setFloatValues(r4.A0B, f);
            this.A0A.start();
        }
    }

    public void A0D(int i) {
        int i2;
        boolean z = this.A0S;
        int i3 = this.A08;
        TextView textView = this.A0M;
        if (i3 == -1) {
            textView.setText(String.valueOf(i));
            this.A0M.setContentDescription(null);
            this.A0S = false;
        } else {
            if (AnonymousClass028.A03(textView) == 1) {
                AnonymousClass028.A0Z(this.A0M, 0);
            }
            boolean z2 = false;
            if (i > this.A08) {
                z2 = true;
            }
            this.A0S = z2;
            if (z != z2) {
                TextView textView2 = this.A0M;
                if (z2) {
                    i2 = this.A0j;
                } else {
                    i2 = this.A0k;
                }
                A0E(textView2, i2);
                if (this.A0S) {
                    AnonymousClass028.A0Z(this.A0M, 1);
                }
            }
            TextView textView3 = this.A0M;
            Context context = getContext();
            Object[] objArr = new Object[2];
            Integer valueOf = Integer.valueOf(i);
            objArr[0] = valueOf;
            C12960it.A1P(objArr, this.A08, 1);
            textView3.setText(context.getString(R.string.character_counter_pattern, objArr));
            TextView textView4 = this.A0M;
            Context context2 = getContext();
            Object[] objArr2 = new Object[2];
            objArr2[0] = valueOf;
            C12960it.A1P(objArr2, this.A08, 1);
            textView4.setContentDescription(context2.getString(R.string.character_counter_content_description, objArr2));
        }
        if (this.A0L != null && z != this.A0S) {
            A0G(false, false);
            A04();
            A03();
        }
    }

    public void A0E(TextView textView, int i) {
        try {
            AnonymousClass04D.A08(textView, i);
        } catch (Exception unused) {
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (textView.getTextColors().getDefaultColor() != -65281) {
                return;
            }
            AnonymousClass04D.A08(textView, 2131952245);
            C12960it.A0s(getContext(), textView, R.color.design_error);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r1 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(boolean r4) {
        /*
            r3 = this;
            boolean r0 = r3.A0b
            if (r0 == 0) goto L_0x0034
            android.widget.EditText r0 = r3.A0L
            int r2 = r0.getSelectionEnd()
            android.widget.EditText r0 = r3.A0L
            if (r0 == 0) goto L_0x0017
            android.text.method.TransformationMethod r0 = r0.getTransformationMethod()
            boolean r1 = r0 instanceof android.text.method.PasswordTransformationMethod
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            android.widget.EditText r1 = r3.A0L
            if (r0 == 0) goto L_0x0035
            r0 = 0
            r1.setTransformationMethod(r0)
            r1 = 1
        L_0x0021:
            r3.A0c = r1
            com.google.android.material.internal.CheckableImageButton r0 = r3.A0N
            r0.setChecked(r1)
            if (r4 == 0) goto L_0x002f
            com.google.android.material.internal.CheckableImageButton r0 = r3.A0N
            r0.jumpDrawablesToCurrentState()
        L_0x002f:
            android.widget.EditText r0 = r3.A0L
            r0.setSelection(r2)
        L_0x0034:
            return
        L_0x0035:
            android.text.method.PasswordTransformationMethod r0 = android.text.method.PasswordTransformationMethod.getInstance()
            r1.setTransformationMethod(r0)
            r1 = 0
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.A0F(boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0012, code lost:
        if (android.text.TextUtils.isEmpty(r0.getText()) != false) goto L_0x0014;
     */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00f5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0G(boolean r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 279
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.A0G(boolean, boolean):void");
    }

    public final boolean A0H() {
        return this.A0X && !TextUtils.isEmpty(this.A0O) && (this.A0K instanceof AnonymousClass2Ze);
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(layoutParams);
            layoutParams2.gravity = (layoutParams2.gravity & -113) | 16;
            FrameLayout frameLayout = this.A0q;
            frameLayout.addView(view, layoutParams2);
            frameLayout.setLayoutParams(layoutParams);
            A09();
            setEditText((EditText) view);
            return;
        }
        super.addView(view, i, layoutParams);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchProvideAutofillStructure(ViewStructure viewStructure, int i) {
        EditText editText;
        if (this.A0P == null || (editText = this.A0L) == null) {
            super.dispatchProvideAutofillStructure(viewStructure, i);
            return;
        }
        boolean z = this.A0a;
        this.A0a = false;
        CharSequence hint = editText.getHint();
        this.A0L.setHint(this.A0P);
        try {
            super.dispatchProvideAutofillStructure(viewStructure, i);
        } finally {
            this.A0L.setHint(hint);
            this.A0a = z;
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        this.A0d = true;
        super.dispatchRestoreInstanceState(sparseArray);
        this.A0d = false;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        GradientDrawable gradientDrawable = this.A0K;
        if (gradientDrawable != null) {
            gradientDrawable.draw(canvas);
        }
        super.draw(canvas);
        if (this.A0X) {
            this.A0r.A0C(canvas);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void drawableStateChanged() {
        ColorStateList colorStateList;
        if (!this.A0Z) {
            boolean z = true;
            this.A0Z = true;
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            if (!AnonymousClass028.A0r(this) || !isEnabled()) {
                z = false;
            }
            A0G(z, false);
            A03();
            A0B();
            A04();
            C47742Cf r1 = this.A0r;
            if (r1 != null) {
                r1.A0c = drawableState;
                ColorStateList colorStateList2 = r1.A0P;
                if ((colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = r1.A0Q) != null && colorStateList.isStateful())) {
                    r1.A03();
                    invalidate();
                }
            }
            this.A0Z = false;
        }
    }

    private Drawable getBoxBackground() {
        int i = this.A05;
        if (i == 1 || i == 2) {
            return this.A0K;
        }
        throw new IllegalStateException();
    }

    public int getBoxBackgroundColor() {
        return this.A04;
    }

    public float getBoxCornerRadiusBottomEnd() {
        return this.A00;
    }

    public float getBoxCornerRadiusBottomStart() {
        return this.A01;
    }

    public float getBoxCornerRadiusTopEnd() {
        return this.A02;
    }

    public float getBoxCornerRadiusTopStart() {
        return this.A03;
    }

    public int getBoxStrokeColor() {
        return this.A09;
    }

    private float[] getCornerRadiiAsArray() {
        float f;
        float[] fArr = new float[8];
        if (!C12970iu.A1W(AnonymousClass028.A05(this))) {
            float f2 = this.A03;
            fArr[0] = f2;
            fArr[1] = f2;
            float f3 = this.A02;
            fArr[2] = f3;
            fArr[3] = f3;
            float f4 = this.A00;
            fArr[4] = f4;
            fArr[5] = f4;
            f = this.A01;
        } else {
            float f5 = this.A02;
            fArr[0] = f5;
            fArr[1] = f5;
            float f6 = this.A03;
            fArr[2] = f6;
            fArr[3] = f6;
            float f7 = this.A01;
            fArr[4] = f7;
            fArr[5] = f7;
            f = this.A00;
        }
        fArr[6] = f;
        fArr[7] = f;
        return fArr;
    }

    public int getCounterMaxLength() {
        return this.A08;
    }

    public CharSequence getCounterOverflowDescription() {
        TextView textView;
        if (!this.A0R || !this.A0S || (textView = this.A0M) == null) {
            return null;
        }
        return textView.getContentDescription();
    }

    public ColorStateList getDefaultHintTextColor() {
        return this.A0B;
    }

    public EditText getEditText() {
        return this.A0L;
    }

    public CharSequence getError() {
        C64493Ft r1 = this.A0s;
        if (r1.A0E) {
            return r1.A0C;
        }
        return null;
    }

    public int getErrorCurrentTextColors() {
        TextView textView = this.A0s.A0A;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public final int getErrorTextCurrentColor() {
        TextView textView = this.A0s.A0A;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public CharSequence getHelperText() {
        C64493Ft r1 = this.A0s;
        if (r1.A0F) {
            return r1.A0D;
        }
        return null;
    }

    public int getHelperTextCurrentTextColor() {
        TextView textView = this.A0s.A0B;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public CharSequence getHint() {
        if (this.A0X) {
            return this.A0O;
        }
        return null;
    }

    public final float getHintCollapsedTextHeight() {
        return C12980iv.A00(this.A0r);
    }

    public final int getHintCurrentCollapsedTextColor() {
        C47742Cf r0 = this.A0r;
        int[] iArr = r0.A0c;
        if (iArr != null) {
            return r0.A0P.getColorForState(iArr, 0);
        }
        return r0.A0P.getDefaultColor();
    }

    public CharSequence getPasswordVisibilityToggleContentDescription() {
        return this.A0Q;
    }

    public Drawable getPasswordVisibilityToggleDrawable() {
        return this.A0I;
    }

    public Typeface getTypeface() {
        return this.A0F;
    }

    @Override // android.widget.LinearLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        EditText editText;
        int i5;
        super.onLayout(z, i, i2, i3, i4);
        if (this.A0K != null) {
            A0B();
        }
        if (this.A0X && (editText = this.A0L) != null) {
            Rect rect = this.A0o;
            AnonymousClass3ID.A01(rect, editText, this);
            int compoundPaddingLeft = rect.left + this.A0L.getCompoundPaddingLeft();
            int compoundPaddingRight = rect.right - this.A0L.getCompoundPaddingRight();
            int i6 = this.A05;
            if (i6 == 1) {
                i5 = getBoxBackground().getBounds().top + this.A0f;
            } else if (i6 != 2) {
                i5 = getPaddingTop();
            } else {
                i5 = getBoxBackground().getBounds().top - A02();
            }
            C47742Cf r6 = this.A0r;
            r6.A0B(compoundPaddingLeft, rect.top + this.A0L.getCompoundPaddingTop(), compoundPaddingRight, rect.bottom - this.A0L.getCompoundPaddingBottom());
            r6.A0A(compoundPaddingLeft, i5, compoundPaddingRight, (i4 - i2) - getPaddingBottom());
            r6.A03();
            if (A0H() && !this.A0Y) {
                A08();
            }
        }
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        A0A();
        super.onMeasure(i, i2);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C53652en)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C53652en r2 = (C53652en) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r2).A00);
        setError(r2.A00);
        if (r2.A01) {
            A0F(true);
        }
        requestLayout();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        C53652en r1 = new C53652en(super.onSaveInstanceState());
        if (this.A0s.A05()) {
            r1.A00 = getError();
        }
        r1.A01 = this.A0c;
        return r1;
    }

    public void setBoxBackgroundColor(int i) {
        if (this.A04 != i) {
            this.A04 = i;
            A05();
        }
    }

    public void setBoxBackgroundColorResource(int i) {
        setBoxBackgroundColor(AnonymousClass00T.A00(getContext(), i));
    }

    public void setBoxBackgroundMode(int i) {
        if (i != this.A05) {
            this.A05 = i;
            A07();
        }
    }

    public void setBoxStrokeColor(int i) {
        if (this.A09 != i) {
            this.A09 = i;
            A04();
        }
    }

    public void setCounterEnabled(boolean z) {
        int length;
        if (this.A0R != z) {
            if (z) {
                C004602b r1 = new C004602b(getContext(), null);
                this.A0M = r1;
                r1.setId(R.id.textinput_counter);
                Typeface typeface = this.A0F;
                if (typeface != null) {
                    this.A0M.setTypeface(typeface);
                }
                this.A0M.setMaxLines(1);
                A0E(this.A0M, this.A0k);
                this.A0s.A02(this.A0M, 2);
                EditText editText = this.A0L;
                if (editText == null) {
                    length = 0;
                } else {
                    length = editText.getText().length();
                }
                A0D(length);
            } else {
                this.A0s.A03(this.A0M, 2);
                this.A0M = null;
            }
            this.A0R = z;
        }
    }

    public void setCounterMaxLength(int i) {
        if (this.A08 != i) {
            if (i <= 0) {
                i = -1;
            }
            this.A08 = i;
            if (this.A0R) {
                EditText editText = this.A0L;
                A0D(editText == null ? 0 : editText.getText().length());
            }
        }
    }

    public void setDefaultHintTextColor(ColorStateList colorStateList) {
        this.A0B = colorStateList;
        this.A0C = colorStateList;
        if (this.A0L != null) {
            A0G(false, false);
        }
    }

    private void setEditText(EditText editText) {
        TextInputLayout textInputLayout;
        EditText editText2;
        if (this.A0L == null) {
            if (!(editText instanceof TextInputEditText)) {
                Log.i("TextInputLayout", "EditText added is not a TextInputEditText. Please switch to using that class instead.");
            }
            this.A0L = editText;
            A07();
            setTextInputAccessibilityDelegate(new C53592eh(this));
            EditText editText3 = this.A0L;
            if (editText3 == null || !(editText3.getTransformationMethod() instanceof PasswordTransformationMethod)) {
                C47742Cf r1 = this.A0r;
                Typeface typeface = this.A0L.getTypeface();
                r1.A0V = typeface;
                r1.A0T = typeface;
                r1.A03();
            }
            C47742Cf r3 = this.A0r;
            float textSize = this.A0L.getTextSize();
            if (r3.A0F != textSize) {
                r3.A0F = textSize;
                r3.A03();
            }
            int gravity = this.A0L.getGravity();
            int i = (gravity & -113) | 48;
            if (r3.A0K != i) {
                r3.A0K = i;
                r3.A03();
            }
            if (r3.A0M != gravity) {
                r3.A0M = gravity;
                r3.A03();
            }
            this.A0L.addTextChangedListener(new C100684mJ(this));
            if (this.A0B == null) {
                this.A0B = this.A0L.getHintTextColors();
            }
            if (this.A0X) {
                if (TextUtils.isEmpty(this.A0O)) {
                    CharSequence hint = this.A0L.getHint();
                    this.A0P = hint;
                    setHint(hint);
                    this.A0L.setHint((CharSequence) null);
                }
                this.A0a = true;
            }
            if (this.A0M != null) {
                A0D(this.A0L.getText().length());
            }
            C64493Ft r0 = this.A0s;
            LinearLayout linearLayout = r0.A09;
            if (!(linearLayout == null || (editText2 = (textInputLayout = r0.A0I).A0L) == null)) {
                AnonymousClass028.A0e(linearLayout, AnonymousClass028.A07(editText2), 0, AnonymousClass028.A06(textInputLayout.A0L), 0);
            }
            A0A();
            A0G(false, true);
            return;
        }
        throw C12970iu.A0f("We already have an EditText, can only have one");
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        A00(this, z);
        super.setEnabled(z);
    }

    public void setError(CharSequence charSequence) {
        C64493Ft r3 = this.A0s;
        if (!r3.A0E) {
            if (!TextUtils.isEmpty(charSequence)) {
                setErrorEnabled(true);
            } else {
                return;
            }
        }
        if (!TextUtils.isEmpty(charSequence)) {
            Animator animator = r3.A06;
            if (animator != null) {
                animator.cancel();
            }
            r3.A0C = charSequence;
            r3.A0A.setText(charSequence);
            int i = r3.A00;
            if (i != 1) {
                r3.A01 = 1;
            }
            r3.A01(i, r3.A01, r3.A06(r3.A0A, charSequence));
            return;
        }
        r3.A00();
    }

    public void setErrorEnabled(boolean z) {
        C64493Ft r3 = this.A0s;
        if (r3.A0E != z) {
            Animator animator = r3.A06;
            if (animator != null) {
                animator.cancel();
            }
            if (z) {
                C004602b r1 = new C004602b(r3.A0H, null);
                r3.A0A = r1;
                r1.setId(R.id.textinput_error);
                Typeface typeface = r3.A07;
                if (typeface != null) {
                    r3.A0A.setTypeface(typeface);
                }
                int i = r3.A03;
                r3.A03 = i;
                TextView textView = r3.A0A;
                if (textView != null) {
                    r3.A0I.A0E(textView, i);
                }
                r3.A0A.setVisibility(4);
                AnonymousClass028.A0Z(r3.A0A, 1);
                r3.A02(r3.A0A, 0);
            } else {
                r3.A00();
                r3.A03(r3.A0A, 0);
                r3.A0A = null;
                TextInputLayout textInputLayout = r3.A0I;
                textInputLayout.A03();
                textInputLayout.A04();
            }
            r3.A0E = z;
        }
    }

    public void setErrorTextAppearance(int i) {
        C64493Ft r0 = this.A0s;
        r0.A03 = i;
        TextView textView = r0.A0A;
        if (textView != null) {
            r0.A0I.A0E(textView, i);
        }
    }

    public void setErrorTextColor(ColorStateList colorStateList) {
        TextView textView = this.A0s.A0A;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setHelperText(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            C64493Ft r3 = this.A0s;
            if (!r3.A0F) {
                setHelperTextEnabled(true);
            }
            Animator animator = r3.A06;
            if (animator != null) {
                animator.cancel();
            }
            r3.A0D = charSequence;
            r3.A0B.setText(charSequence);
            int i = r3.A00;
            if (i != 2) {
                r3.A01 = 2;
            }
            r3.A01(i, r3.A01, r3.A06(r3.A0B, charSequence));
        } else if (this.A0s.A0F) {
            setHelperTextEnabled(false);
        }
    }

    public void setHelperTextColor(ColorStateList colorStateList) {
        TextView textView = this.A0s.A0B;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setHelperTextEnabled(boolean z) {
        C64493Ft r4 = this.A0s;
        if (r4.A0F != z) {
            Animator animator = r4.A06;
            if (animator != null) {
                animator.cancel();
            }
            if (z) {
                C004602b r1 = new C004602b(r4.A0H, null);
                r4.A0B = r1;
                r1.setId(R.id.textinput_helper_text);
                Typeface typeface = r4.A07;
                if (typeface != null) {
                    r4.A0B.setTypeface(typeface);
                }
                r4.A0B.setVisibility(4);
                AnonymousClass028.A0Z(r4.A0B, 1);
                int i = r4.A04;
                r4.A04 = i;
                TextView textView = r4.A0B;
                if (textView != null) {
                    AnonymousClass04D.A08(textView, i);
                }
                r4.A02(r4.A0B, 1);
            } else {
                Animator animator2 = r4.A06;
                if (animator2 != null) {
                    animator2.cancel();
                }
                int i2 = r4.A00;
                if (i2 == 2) {
                    r4.A01 = 0;
                }
                r4.A01(i2, r4.A01, r4.A06(r4.A0B, null));
                r4.A03(r4.A0B, 1);
                r4.A0B = null;
                TextInputLayout textInputLayout = r4.A0I;
                textInputLayout.A03();
                textInputLayout.A04();
            }
            r4.A0F = z;
        }
    }

    public void setHelperTextTextAppearance(int i) {
        C64493Ft r0 = this.A0s;
        r0.A04 = i;
        TextView textView = r0.A0B;
        if (textView != null) {
            AnonymousClass04D.A08(textView, i);
        }
    }

    public void setHint(CharSequence charSequence) {
        if (this.A0X) {
            setHintInternal(charSequence);
            sendAccessibilityEvent(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        }
    }

    public void setHintAnimationEnabled(boolean z) {
        this.A0W = z;
    }

    public void setHintEnabled(boolean z) {
        if (z != this.A0X) {
            this.A0X = z;
            if (!z) {
                this.A0a = false;
                if (!TextUtils.isEmpty(this.A0O) && TextUtils.isEmpty(this.A0L.getHint())) {
                    this.A0L.setHint(this.A0O);
                }
                setHintInternal(null);
            } else {
                CharSequence hint = this.A0L.getHint();
                if (!TextUtils.isEmpty(hint)) {
                    if (TextUtils.isEmpty(this.A0O)) {
                        setHint(hint);
                    }
                    this.A0L.setHint((CharSequence) null);
                }
                this.A0a = true;
            }
            if (this.A0L != null) {
                A09();
            }
        }
    }

    private void setHintInternal(CharSequence charSequence) {
        if (!TextUtils.equals(charSequence, this.A0O)) {
            this.A0O = charSequence;
            this.A0r.A0D(charSequence);
            if (!this.A0Y) {
                A08();
            }
        }
    }

    public void setHintTextAppearance(int i) {
        C47742Cf r0 = this.A0r;
        r0.A08(i);
        this.A0C = r0.A0P;
        if (this.A0L != null) {
            A0G(false, false);
            A09();
        }
    }

    public void setPasswordVisibilityToggleContentDescription(int i) {
        CharSequence charSequence;
        if (i != 0) {
            charSequence = getResources().getText(i);
        } else {
            charSequence = null;
        }
        setPasswordVisibilityToggleContentDescription(charSequence);
    }

    public void setPasswordVisibilityToggleContentDescription(CharSequence charSequence) {
        this.A0Q = charSequence;
        CheckableImageButton checkableImageButton = this.A0N;
        if (checkableImageButton != null) {
            checkableImageButton.setContentDescription(charSequence);
        }
    }

    public void setPasswordVisibilityToggleDrawable(int i) {
        Drawable drawable;
        if (i != 0) {
            drawable = C015007d.A01(getContext(), i);
        } else {
            drawable = null;
        }
        setPasswordVisibilityToggleDrawable(drawable);
    }

    public void setPasswordVisibilityToggleDrawable(Drawable drawable) {
        this.A0I = drawable;
        CheckableImageButton checkableImageButton = this.A0N;
        if (checkableImageButton != null) {
            checkableImageButton.setImageDrawable(drawable);
        }
    }

    public void setPasswordVisibilityToggleEnabled(boolean z) {
        EditText editText;
        if (this.A0b != z) {
            this.A0b = z;
            if (!z && this.A0c && (editText = this.A0L) != null) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
            this.A0c = false;
            A0A();
        }
    }

    public void setPasswordVisibilityToggleTintList(ColorStateList colorStateList) {
        this.A0D = colorStateList;
        this.A0T = true;
        A06();
    }

    public void setPasswordVisibilityToggleTintMode(PorterDuff.Mode mode) {
        this.A0E = mode;
        this.A0U = true;
        A06();
    }

    public void setTextInputAccessibilityDelegate(C53592eh r2) {
        EditText editText = this.A0L;
        if (editText != null) {
            AnonymousClass028.A0g(editText, r2);
        }
    }

    public void setTypeface(Typeface typeface) {
        if (typeface != this.A0F) {
            this.A0F = typeface;
            C47742Cf r0 = this.A0r;
            r0.A0V = typeface;
            r0.A0T = typeface;
            r0.A03();
            C64493Ft r1 = this.A0s;
            if (typeface != r1.A07) {
                r1.A07 = typeface;
                TextView textView = r1.A0A;
                if (textView != null) {
                    textView.setTypeface(typeface);
                }
                TextView textView2 = r1.A0B;
                if (textView2 != null) {
                    textView2.setTypeface(typeface);
                }
            }
            TextView textView3 = this.A0M;
            if (textView3 != null) {
                textView3.setTypeface(typeface);
            }
        }
    }
}
