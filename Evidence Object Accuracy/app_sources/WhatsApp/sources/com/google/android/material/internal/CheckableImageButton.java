package com.google.android.material.internal;

import X.AnonymousClass028;
import X.AnonymousClass08i;
import X.C74403hv;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.ImageButton;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class CheckableImageButton extends AnonymousClass08i implements Checkable {
    public static final int[] A01 = {16842912};
    public boolean A00;

    public CheckableImageButton(Context context) {
        this(context, null);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.imageButtonStyle);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        AnonymousClass028.A0g(this, new C74403hv(this));
    }

    @Override // android.widget.Checkable
    public boolean isChecked() {
        return this.A00;
    }

    @Override // android.widget.ImageView, android.view.View
    public int[] onCreateDrawableState(int i) {
        if (!this.A00) {
            return super.onCreateDrawableState(i);
        }
        int[] iArr = A01;
        return ImageButton.mergeDrawableStates(super.onCreateDrawableState(i + iArr.length), iArr);
    }

    @Override // android.widget.Checkable
    public void setChecked(boolean z) {
        if (this.A00 != z) {
            this.A00 = z;
            refreshDrawableState();
            sendAccessibilityEvent(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        }
    }

    @Override // android.widget.Checkable
    public void toggle() {
        setChecked(!this.A00);
    }
}
