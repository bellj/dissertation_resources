package com.google.android.apps.pixelmigrate.migrate.ios.appdatareader;

import android.os.IInterface;
import android.os.ParcelFileDescriptor;

/* loaded from: classes3.dex */
public interface IAppDataReaderService extends IInterface {
    ParcelFileDescriptor AD1();
}
