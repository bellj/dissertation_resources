package com.google.protobuf;

import sun.misc.Unsafe;

/* loaded from: classes3.dex */
public final class UnsafeUtil {
    public static final long A00;
    public static final Unsafe A01;
    public static final boolean A02;
    public static final boolean A03;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0068 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    static {
        /*
        // Method dump skipped, instructions count: 211
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.UnsafeUtil.<clinit>():void");
    }

    public static void A00(byte[] bArr, byte b, long j) {
        A01.putByte(bArr, j, b);
    }
}
