package com.google.protobuf;

import X.AbstractC27881Jp;
import X.AbstractC40931sa;
import X.AnonymousClass1G1;
import X.AnonymousClass494;
import X.AnonymousClass4CO;
import X.AnonymousClass4Z8;
import X.C27851Jm;
import X.C27861Jn;
import X.C40921sY;
import X.C56842m2;
import java.util.logging.Level;
import java.util.logging.Logger;

/* loaded from: classes2.dex */
public abstract class CodedOutputStream extends AbstractC40931sa {
    public static final long A00 = UnsafeUtil.A00;
    public static final Logger A01 = Logger.getLogger(CodedOutputStream.class.getName());
    public static final boolean A02 = UnsafeUtil.A02;

    public static int A01(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) != 0) {
            return (i & -268435456) == 0 ? 4 : 5;
        }
        return 3;
    }

    public static int A08(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        int i = 2;
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int A00(int i) {
        return A01((i << 3) | 0) + 1;
    }

    public static int A02(int i, int i2) {
        int i3;
        int A012 = A01((i << 3) | 0);
        if (i2 >= 0) {
            i3 = A01(i2);
        } else {
            i3 = 10;
        }
        return A012 + i3;
    }

    public static int A03(int i, int i2) {
        int i3;
        int A012 = A01((i << 3) | 0);
        if (i2 >= 0) {
            i3 = A01(i2);
        } else {
            i3 = 10;
        }
        return A012 + i3;
    }

    public static int A04(int i, int i2) {
        return A01((i << 3) | 0) + A01(i2);
    }

    public static int A05(int i, long j) {
        return A01((i << 3) | 0) + A08(j);
    }

    public static int A06(int i, long j) {
        return A01((i << 3) | 0) + A08(j);
    }

    public static int A07(int i, String str) {
        return A01((i << 3) | 0) + A0B(str);
    }

    public static int A09(AbstractC27881Jp r3, int i) {
        int A012 = A01((i << 3) | 0);
        int A03 = r3.A03();
        return A012 + A01(A03) + A03;
    }

    public static int A0A(AnonymousClass1G1 r3, int i) {
        int A012 = A01((i << 3) | 0);
        int AGd = r3.AGd();
        return A012 + A01(AGd) + AGd;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:6:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v4, types: [int] */
    public static int A0B(String str) {
        int i;
        try {
            str = AnonymousClass4Z8.A00(str);
            i = str;
        } catch (AnonymousClass4CO unused) {
            i = str.getBytes(C27851Jm.A03).length;
        }
        return A01(i) + i;
    }

    public void A0C(int i) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r4 = (C56842m2) this;
            if (A02) {
                int i2 = r4.A01;
                int i3 = r4.A00;
                if (i2 - i3 >= 10) {
                    long j = A00 + ((long) i3);
                    while (true) {
                        int i4 = i & -128;
                        byte[] bArr = r4.A02;
                        if (i4 == 0) {
                            UnsafeUtil.A00(bArr, (byte) i, j);
                            r4.A00++;
                            return;
                        }
                        UnsafeUtil.A00(bArr, (byte) ((i & 127) | 128), j);
                        r4.A00++;
                        i >>>= 7;
                        j = 1 + j;
                    }
                }
            }
            while ((i & -128) != 0) {
                try {
                    byte[] bArr2 = r4.A02;
                    int i5 = r4.A00;
                    r4.A00 = i5 + 1;
                    bArr2[i5] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                } catch (IndexOutOfBoundsException e) {
                    throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r4.A00), Integer.valueOf(r4.A01), 1), e);
                }
            }
            byte[] bArr3 = r4.A02;
            int i6 = r4.A00;
            r4.A00 = i6 + 1;
            bArr3[i6] = (byte) i;
            return;
        }
        C40921sY r1 = (C40921sY) this;
        r1.A0Q(10);
        r1.A0P(i);
    }

    public void A0D(int i, int i2) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r4 = (C56842m2) this;
            r4.A0C((i << 3) | 5);
            try {
                byte[] bArr = r4.A02;
                int i3 = r4.A00;
                int i4 = i3 + 1;
                r4.A00 = i4;
                bArr[i3] = (byte) (i2 & 255);
                int i5 = i4 + 1;
                r4.A00 = i5;
                bArr[i4] = (byte) ((i2 >> 8) & 255);
                int i6 = i5 + 1;
                r4.A00 = i6;
                bArr[i5] = (byte) ((i2 >> 16) & 255);
                r4.A00 = i6 + 1;
                bArr[i6] = (byte) ((i2 >> 24) & 255);
            } catch (IndexOutOfBoundsException e) {
                throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r4.A00), Integer.valueOf(r4.A01), 1), e);
            }
        } else {
            C40921sY r2 = (C40921sY) this;
            r2.A0Q(14);
            r2.A0P((i << 3) | 5);
            byte[] bArr2 = r2.A04;
            int i7 = r2.A00;
            int i8 = i7 + 1;
            r2.A00 = i8;
            bArr2[i7] = (byte) (i2 & 255);
            int i9 = i8 + 1;
            r2.A00 = i9;
            bArr2[i8] = (byte) ((i2 >> 8) & 255);
            int i10 = i9 + 1;
            r2.A00 = i10;
            bArr2[i9] = (byte) ((i2 >> 16) & 255);
            r2.A00 = i10 + 1;
            bArr2[i10] = (byte) ((i2 >> 24) & 255);
            r2.A01 += 4;
        }
    }

    public void A0E(int i, int i2) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r2 = (C56842m2) this;
            r2.A0C((i << 3) | 0);
            if (i2 >= 0) {
                r2.A0C(i2);
            } else {
                r2.A0O((long) i2);
            }
        } else {
            C40921sY r22 = (C40921sY) this;
            r22.A0Q(20);
            r22.A0P((i << 3) | 0);
            if (i2 >= 0) {
                r22.A0P(i2);
            } else {
                r22.A0R((long) i2);
            }
        }
    }

    public void A0F(int i, int i2) {
        if (!(this instanceof C40921sY)) {
            A0C((i << 3) | 0);
            A0C(i2);
            return;
        }
        C40921sY r2 = (C40921sY) this;
        r2.A0Q(20);
        r2.A0P((i << 3) | 0);
        r2.A0P(i2);
    }

    public void A0G(int i, long j) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r4 = (C56842m2) this;
            r4.A0C((i << 3) | 1);
            try {
                byte[] bArr = r4.A02;
                int i2 = r4.A00;
                int i3 = i2 + 1;
                r4.A00 = i3;
                bArr[i2] = (byte) (((int) j) & 255);
                int i4 = i3 + 1;
                r4.A00 = i4;
                bArr[i3] = (byte) (((int) (j >> 8)) & 255);
                int i5 = i4 + 1;
                r4.A00 = i5;
                bArr[i4] = (byte) (((int) (j >> 16)) & 255);
                int i6 = i5 + 1;
                r4.A00 = i6;
                bArr[i5] = (byte) (((int) (j >> 24)) & 255);
                int i7 = i6 + 1;
                r4.A00 = i7;
                bArr[i6] = (byte) (((int) (j >> 32)) & 255);
                int i8 = i7 + 1;
                r4.A00 = i8;
                bArr[i7] = (byte) (((int) (j >> 40)) & 255);
                int i9 = i8 + 1;
                r4.A00 = i9;
                bArr[i8] = (byte) (((int) (j >> 48)) & 255);
                r4.A00 = i9 + 1;
                bArr[i9] = (byte) (((int) (j >> 56)) & 255);
            } catch (IndexOutOfBoundsException e) {
                throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r4.A00), Integer.valueOf(r4.A01), 1), e);
            }
        } else {
            C40921sY r3 = (C40921sY) this;
            r3.A0Q(18);
            r3.A0P((i << 3) | 1);
            byte[] bArr2 = r3.A04;
            int i10 = r3.A00;
            int i11 = i10 + 1;
            r3.A00 = i11;
            bArr2[i10] = (byte) ((int) (j & 255));
            int i12 = i11 + 1;
            r3.A00 = i12;
            bArr2[i11] = (byte) ((int) ((j >> 8) & 255));
            int i13 = i12 + 1;
            r3.A00 = i13;
            bArr2[i12] = (byte) ((int) ((j >> 16) & 255));
            int i14 = i13 + 1;
            r3.A00 = i14;
            bArr2[i13] = (byte) ((int) (255 & (j >> 24)));
            int i15 = i14 + 1;
            r3.A00 = i15;
            bArr2[i14] = (byte) (((int) (j >> 32)) & 255);
            int i16 = i15 + 1;
            r3.A00 = i16;
            bArr2[i15] = (byte) (((int) (j >> 40)) & 255);
            int i17 = i16 + 1;
            r3.A00 = i17;
            bArr2[i16] = (byte) (((int) (j >> 48)) & 255);
            r3.A00 = i17 + 1;
            bArr2[i17] = (byte) (((int) (j >> 56)) & 255);
            r3.A01 += 8;
        }
    }

    public void A0H(int i, long j) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r2 = (C56842m2) this;
            r2.A0C((i << 3) | 0);
            r2.A0O(j);
            return;
        }
        C40921sY r22 = (C40921sY) this;
        r22.A0Q(20);
        r22.A0P((i << 3) | 0);
        r22.A0R(j);
    }

    public void A0I(int i, String str) {
        int i2;
        int i3;
        int A002;
        if (!(this instanceof C40921sY)) {
            C56842m2 r4 = (C56842m2) this;
            r4.A0C((i << 3) | 2);
            int i4 = r4.A00;
            try {
                int length = str.length();
                int A012 = A01(length * 3);
                int A013 = A01(length);
                if (A013 == A012) {
                    int i5 = i4 + A013;
                    r4.A00 = i5;
                    A002 = AnonymousClass4Z8.A00.A00(str, r4.A02, i5, r4.A01 - i5);
                    r4.A00 = i4;
                    r4.A0C((A002 - i4) - A013);
                } else {
                    r4.A0C(AnonymousClass4Z8.A00(str));
                    byte[] bArr = r4.A02;
                    int i6 = r4.A00;
                    A002 = AnonymousClass4Z8.A00.A00(str, bArr, i6, r4.A01 - i6);
                }
                r4.A00 = A002;
            } catch (AnonymousClass4CO e) {
                r4.A00 = i4;
                r4.A0M(e, str);
            } catch (IndexOutOfBoundsException e2) {
                throw new AnonymousClass494(e2);
            }
        } else {
            C40921sY r3 = (C40921sY) this;
            r3.A0C((i << 3) | 2);
            try {
                int length2 = str.length();
                int i7 = length2 * 3;
                int A014 = A01(i7);
                int i8 = A014 + i7;
                int i9 = r3.A02;
                if (i8 > i9) {
                    byte[] bArr2 = new byte[i7];
                    int A003 = AnonymousClass4Z8.A00.A00(str, bArr2, 0, i7);
                    r3.A0C(A003);
                    r3.A0N(bArr2, 0, A003);
                    return;
                }
                if (i8 > i9 - r3.A00) {
                    r3.A0O();
                }
                int A015 = A01(length2);
                int i10 = r3.A00;
                try {
                    if (A015 == A014) {
                        int i11 = i10 + A015;
                        r3.A00 = i11;
                        i3 = AnonymousClass4Z8.A00.A00(str, r3.A04, i11, i9 - i11);
                        r3.A00 = i10;
                        i2 = (i3 - i10) - A015;
                        r3.A0P(i2);
                    } else {
                        i2 = AnonymousClass4Z8.A00(str);
                        r3.A0P(i2);
                        i3 = AnonymousClass4Z8.A00.A00(str, r3.A04, r3.A00, i2);
                    }
                    r3.A00 = i3;
                    r3.A01 += i2;
                } catch (AnonymousClass4CO e3) {
                    r3.A01 -= r3.A00 - i10;
                    r3.A00 = i10;
                    throw e3;
                } catch (ArrayIndexOutOfBoundsException e4) {
                    throw new AnonymousClass494(e4);
                }
            } catch (AnonymousClass4CO e5) {
                r3.A0M(e5, str);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public void A0J(int i, boolean z) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r5 = (C56842m2) this;
            r5.A0C((i << 3) | 0);
            byte b = z ? (byte) 1 : 0;
            try {
                byte[] bArr = r5.A02;
                int i2 = r5.A00;
                r5.A00 = i2 + 1;
                bArr[i2] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r5.A00), Integer.valueOf(r5.A01), 1), e);
            }
        } else {
            C40921sY r4 = (C40921sY) this;
            r4.A0Q(11);
            r4.A0P((i << 3) | 0);
            byte b2 = z ? (byte) 1 : 0;
            byte[] bArr2 = r4.A04;
            int i3 = r4.A00;
            r4.A00 = i3 + 1;
            bArr2[i3] = b2;
            r4.A01++;
        }
    }

    public void A0K(AbstractC27881Jp r4, int i) {
        A0C((i << 3) | 2);
        A0C(r4.A03());
        C27861Jn r42 = (C27861Jn) r4;
        A0N(r42.bytes, r42.A05(), r42.A03());
    }

    public void A0L(AnonymousClass1G1 r3, int i) {
        A0C((i << 3) | 2);
        A0C(r3.AGd());
        r3.AgI(this);
    }

    public final void A0M(AnonymousClass4CO r4, String str) {
        A01.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) r4);
        byte[] bytes = str.getBytes(C27851Jm.A03);
        try {
            int length = bytes.length;
            A0C(length);
            A0N(bytes, 0, length);
        } catch (AnonymousClass494 e) {
            throw e;
        } catch (IndexOutOfBoundsException e2) {
            throw new AnonymousClass494(e2);
        }
    }

    public void A0N(byte[] bArr, int i, int i2) {
        if (!(this instanceof C40921sY)) {
            C56842m2 r4 = (C56842m2) this;
            try {
                System.arraycopy(bArr, i, r4.A02, r4.A00, i2);
                r4.A00 += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r4.A00), Integer.valueOf(r4.A01), Integer.valueOf(i2)), e);
            }
        } else {
            C40921sY r5 = (C40921sY) this;
            int i3 = r5.A02;
            int i4 = r5.A00;
            int i5 = i3 - i4;
            if (i5 >= i2) {
                System.arraycopy(bArr, i, r5.A04, i4, i2);
                r5.A00 += i2;
            } else {
                byte[] bArr2 = r5.A04;
                System.arraycopy(bArr, i, bArr2, i4, i5);
                int i6 = i + i5;
                i2 -= i5;
                r5.A00 = i3;
                r5.A01 += i5;
                r5.A0O();
                if (i2 <= i3) {
                    System.arraycopy(bArr, i6, bArr2, 0, i2);
                    r5.A00 = i2;
                } else {
                    r5.A03.write(bArr, i6, i2);
                }
            }
            r5.A01 += i2;
        }
    }
}
