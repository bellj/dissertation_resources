package com.google.common.base;

import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import java.util.logging.Level;
import java.util.logging.Logger;

/* loaded from: classes2.dex */
public final class Strings {
    public static String A00(String str, Object... objArr) {
        int length;
        String valueOf = String.valueOf(str);
        int i = 0;
        int i2 = 0;
        while (true) {
            length = objArr.length;
            if (i2 >= length) {
                break;
            }
            Object obj = objArr[i2];
            if (obj == null) {
                obj = "null";
            } else {
                try {
                    obj = obj.toString();
                } catch (Exception e) {
                    String A0s = C12980iv.A0s(obj);
                    String hexString = Integer.toHexString(System.identityHashCode(obj));
                    StringBuilder A0i = C12960it.A0i(hexString, A0s.length() + 1);
                    A0i.append(A0s);
                    A0i.append('@');
                    String A0d = C12960it.A0d(hexString, A0i);
                    Logger.getLogger("com.google.common.base.Strings").log(Level.WARNING, C12960it.A0c(String.valueOf(A0d), "Exception during lenientFormat for "), (Throwable) e);
                    String A0s2 = C12980iv.A0s(e);
                    StringBuilder A0t = C12980iv.A0t(C12970iu.A07(A0d) + 9 + A0s2.length());
                    A0t.append("<");
                    A0t.append(A0d);
                    A0t.append(" threw ");
                    A0t.append(A0s2);
                    obj = C12960it.A0d(">", A0t);
                }
            }
            objArr[i2] = obj;
            i2++;
        }
        int length2 = valueOf.length();
        StringBuilder A0t2 = C12980iv.A0t((length << 4) + length2);
        int i3 = 0;
        while (i < length) {
            int indexOf = valueOf.indexOf("%s", i3);
            if (indexOf == -1) {
                break;
            }
            A0t2.append((CharSequence) valueOf, i3, indexOf);
            A0t2.append(objArr[i]);
            i3 = indexOf + 2;
            i++;
        }
        A0t2.append((CharSequence) valueOf, i3, length2);
        if (i < length) {
            A0t2.append(" [");
            A0t2.append(objArr[i]);
            for (int i4 = i + 1; i4 < length; i4++) {
                A0t2.append(", ");
                A0t2.append(objArr[i4]);
            }
            A0t2.append(']');
        }
        return A0t2.toString();
    }
}
