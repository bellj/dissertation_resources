package com.google.firebase.iid;

import X.AbstractC115765Sv;
import X.AnonymousClass3TN;
import X.AnonymousClass3TO;
import X.C13030j1;
import X.C13130jC;
import X.C13270jQ;
import X.C13430ji;
import X.C13450jk;
import X.C13460jl;
import X.C13470jm;
import X.C13480jn;
import X.C13510jq;
import X.C13600jz;
import X.C13700kB;
import X.C13710kC;
import X.C13720kD;
import X.C13730kE;
import X.C16070oO;
import X.C26061Bw;
import X.C88984If;
import X.ThreadFactoryC13440jj;
import X.ThreadFactoryC16540pC;
import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class FirebaseInstanceId {
    public static C13450jk A08;
    public static ScheduledExecutorService A09;
    public static final long A0A = TimeUnit.HOURS.toSeconds(8);
    public boolean A00 = false;
    public final C13030j1 A01;
    public final C13720kD A02;
    public final C13430ji A03;
    public final C13730kE A04;
    public final C13710kC A05;
    public final C13510jq A06;
    public final Executor A07;

    public FirebaseInstanceId(C13030j1 r23, C13270jQ r24, C13130jC r25) {
        r23.A02();
        Context context = r23.A00;
        C13430ji r0 = new C13430ji(context);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue();
        ThreadFactory threadFactory = ThreadFactoryC13440jj.A00;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, 30, timeUnit, linkedBlockingQueue, threadFactory);
        ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(0, 1, 30, timeUnit, new LinkedBlockingQueue(), threadFactory);
        if (C13430ji.A00(r23) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (A08 == null) {
                    r23.A02();
                    A08 = new C13450jk(context);
                }
            }
            this.A01 = r23;
            this.A03 = r0;
            this.A06 = new C13510jq(r23, r0, r25, threadPoolExecutor);
            this.A07 = threadPoolExecutor2;
            this.A05 = new C13710kC(A08);
            this.A02 = new C13720kD(r24, this);
            this.A04 = new C13730kE(threadPoolExecutor);
            threadPoolExecutor2.execute(new RunnableBRunnable0Shape0S0100000_I0(this, 13));
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    public static C16070oO A00(String str, String str2) {
        C16070oO r4;
        C16070oO r2;
        C13450jk r3 = A08;
        synchronized (r3) {
            r4 = null;
            String string = r3.A01.getString(C13450jk.A01(str, str2), null);
            if (!TextUtils.isEmpty(string)) {
                if (string.startsWith("{")) {
                    try {
                        JSONObject jSONObject = new JSONObject(string);
                        r2 = new C16070oO(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
                    } catch (JSONException e) {
                        String valueOf = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 23);
                        sb.append("Failed to parse token: ");
                        sb.append(valueOf);
                        Log.w("FirebaseInstanceId", sb.toString());
                    }
                } else {
                    r2 = new C16070oO(string, null, 0);
                }
                r4 = r2;
            }
        }
        return r4;
    }

    public static String A01() {
        C13470jm r0;
        C13460jl r8;
        Context context;
        C13480jn e;
        File A04;
        C13450jk r3 = A08;
        synchronized (r3) {
            Map map = r3.A03;
            r0 = (C13470jm) map.get("");
            if (r0 == null) {
                try {
                    r8 = r3.A02;
                    context = r3.A00;
                    e = null;
                    try {
                        A04 = C13460jl.A04(context);
                    } catch (C13480jn e2) {
                        e = e2;
                    }
                } catch (C13480jn unused) {
                    Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
                    getInstance(C13030j1.A00()).A07();
                    r0 = r3.A02.A07(r3.A00);
                }
                try {
                    if (A04.exists()) {
                        try {
                            r0 = C13460jl.A02(A04);
                        } catch (C13480jn | IOException e3) {
                            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                                String valueOf = String.valueOf(e3);
                                StringBuilder sb = new StringBuilder(valueOf.length() + 39);
                                sb.append("Failed to read ID from file, retrying: ");
                                sb.append(valueOf);
                                Log.d("FirebaseInstanceId", sb.toString());
                            }
                            try {
                                r0 = C13460jl.A02(A04);
                            } catch (IOException e4) {
                                String valueOf2 = String.valueOf(e4);
                                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 45);
                                sb2.append("IID file exists, but failed to read from it: ");
                                sb2.append(valueOf2);
                                Log.w("FirebaseInstanceId", sb2.toString());
                                throw new C13480jn(e4);
                            }
                        }
                        C13460jl.A06(context, r0);
                        map.put("", r0);
                    }
                    r0 = C13460jl.A01(context.getSharedPreferences("com.google.android.gms.appid", 0));
                    if (r0 != null) {
                        C13460jl.A00(context, r0, false);
                    } else if (e != null) {
                        throw e;
                    } else {
                        r0 = r8.A07(context);
                    }
                    map.put("", r0);
                } catch (C13480jn e5) {
                    throw e5;
                }
            }
        }
        return r0.A01;
    }

    public static void A02(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            ScheduledExecutorService scheduledExecutorService = A09;
            if (scheduledExecutorService == null) {
                scheduledExecutorService = new ScheduledThreadPoolExecutor(1, new ThreadFactoryC16540pC("FirebaseInstanceId"));
                A09 = scheduledExecutorService;
            }
            scheduledExecutorService.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    public static boolean A03() {
        if (!Log.isLoggable("FirebaseInstanceId", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3);
        }
        return true;
    }

    public final Object A04(C13600jz r4) {
        try {
            return C13700kB.A00(r4, TimeUnit.MILLISECONDS, C26061Bw.A0L);
        } catch (InterruptedException | TimeoutException unused) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                    A07();
                }
                throw cause;
            } else if (cause instanceof RuntimeException) {
                throw cause;
            } else {
                throw new IOException(e);
            }
        }
    }

    public String A05(String str, String str2) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            if (str2.isEmpty() || str2.equalsIgnoreCase("fcm") || str2.equalsIgnoreCase("gcm")) {
                str2 = "*";
            }
            C13600jz r5 = new C13600jz();
            r5.A08(null);
            Executor executor = this.A07;
            AnonymousClass3TN r3 = new AbstractC115765Sv(this, str, str2) { // from class: X.3TN
                public final FirebaseInstanceId A00;
                public final String A01;
                public final String A02;

                {
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // X.AbstractC115765Sv
                public final Object Ael(C13600jz r13) {
                    C13600jz r52;
                    FirebaseInstanceId firebaseInstanceId = this.A00;
                    String str3 = this.A01;
                    String str4 = this.A02;
                    String A01 = FirebaseInstanceId.A01();
                    C16070oO A00 = FirebaseInstanceId.A00(str3, str4);
                    if (!firebaseInstanceId.A0B(A00)) {
                        C88984If r0 = new C88984If(A00.A01);
                        C13600jz r53 = new C13600jz();
                        r53.A08(r0);
                        return r53;
                    }
                    C13730kE r7 = firebaseInstanceId.A04;
                    AnonymousClass4RD r4 = 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0028: CONSTRUCTOR  (r4v0 'r4' X.4RD) = 
                          (r6v0 'firebaseInstanceId' com.google.firebase.iid.FirebaseInstanceId)
                          (r2v0 'A01' java.lang.String)
                          (r5v0 'str3' java.lang.String)
                          (r3v0 'str4' java.lang.String)
                         call: X.4RD.<init>(com.google.firebase.iid.FirebaseInstanceId, java.lang.String, java.lang.String, java.lang.String):void type: CONSTRUCTOR in method: X.3TN.Ael(X.0jz):java.lang.Object, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.4RD, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 19 more
                        */
                    /*
                        this = this;
                        com.google.firebase.iid.FirebaseInstanceId r6 = r12.A00
                        java.lang.String r5 = r12.A01
                        java.lang.String r3 = r12.A02
                        java.lang.String r2 = com.google.firebase.iid.FirebaseInstanceId.A01()
                        X.0oO r1 = com.google.firebase.iid.FirebaseInstanceId.A00(r5, r3)
                        boolean r0 = r6.A0B(r1)
                        if (r0 != 0) goto L_0x0024
                        java.lang.String r1 = r1.A01
                        X.4If r0 = new X.4If
                        r0.<init>(r1)
                        X.0jz r5 = new X.0jz
                        r5.<init>()
                        r5.A08(r0)
                        return r5
                    L_0x0024:
                        X.0kE r7 = r6.A04
                        X.4RD r4 = new X.4RD
                        r4.<init>(r6, r2, r5, r3)
                        monitor-enter(r7)
                        android.util.Pair r8 = X.C12990iw.A0L(r5, r3)     // Catch: all -> 0x00cd
                        java.util.Map r6 = r7.A00     // Catch: all -> 0x00cd
                        java.lang.Object r5 = r6.get(r8)     // Catch: all -> 0x00cd
                        X.0jz r5 = (X.C13600jz) r5     // Catch: all -> 0x00cd
                        r0 = 3
                        if (r5 == 0) goto L_0x005e
                        java.lang.String r3 = "FirebaseInstanceId"
                        boolean r0 = android.util.Log.isLoggable(r3, r0)     // Catch: all -> 0x00cd
                        if (r0 == 0) goto L_0x00cb
                        java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch: all -> 0x00cd
                        int r0 = r2.length()     // Catch: all -> 0x00cd
                        int r0 = r0 + 29
                        java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)     // Catch: all -> 0x00cd
                        java.lang.String r0 = "Joining ongoing request for: "
                        r1.append(r0)     // Catch: all -> 0x00cd
                        java.lang.String r0 = X.C12960it.A0d(r2, r1)     // Catch: all -> 0x00cd
                        android.util.Log.d(r3, r0)     // Catch: all -> 0x00cd
                        goto L_0x00cb
                    L_0x005e:
                        java.lang.String r3 = "FirebaseInstanceId"
                        boolean r0 = android.util.Log.isLoggable(r3, r0)     // Catch: all -> 0x00cd
                        if (r0 == 0) goto L_0x0080
                        java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch: all -> 0x00cd
                        int r0 = r2.length()     // Catch: all -> 0x00cd
                        int r0 = r0 + 24
                        java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)     // Catch: all -> 0x00cd
                        java.lang.String r0 = "Making new request for: "
                        r1.append(r0)     // Catch: all -> 0x00cd
                        java.lang.String r0 = X.C12960it.A0d(r2, r1)     // Catch: all -> 0x00cd
                        android.util.Log.d(r3, r0)     // Catch: all -> 0x00cd
                    L_0x0080:
                        com.google.firebase.iid.FirebaseInstanceId r11 = r4.A00     // Catch: all -> 0x00cd
                        java.lang.String r10 = r4.A01     // Catch: all -> 0x00cd
                        java.lang.String r9 = r4.A02     // Catch: all -> 0x00cd
                        java.lang.String r4 = r4.A03     // Catch: all -> 0x00cd
                        X.0jq r1 = r11.A06     // Catch: all -> 0x00cd
                        android.os.Bundle r0 = X.C12970iu.A0D()     // Catch: all -> 0x00cd
                        X.0jz r0 = r1.A01(r0, r10, r9, r4)     // Catch: all -> 0x00cd
                        X.0jz r5 = r1.A03(r0)     // Catch: all -> 0x00cd
                        java.util.concurrent.Executor r3 = r11.A07     // Catch: all -> 0x00cd
                        X.512 r2 = new X.512     // Catch: all -> 0x00cd
                        r2.<init>(r11, r9, r4, r10)     // Catch: all -> 0x00cd
                        X.0jz r4 = new X.0jz     // Catch: all -> 0x00cd
                        r4.<init>()     // Catch: all -> 0x00cd
                        X.0k6 r1 = r5.A03     // Catch: all -> 0x00cd
                        X.3TP r0 = new X.3TP     // Catch: all -> 0x00cd
                        r0.<init>(r2, r4, r3)     // Catch: all -> 0x00cd
                        r1.A00(r0)     // Catch: all -> 0x00cd
                        r5.A04()     // Catch: all -> 0x00cd
                        java.util.concurrent.Executor r3 = r7.A01     // Catch: all -> 0x00cd
                        X.50q r2 = new X.50q     // Catch: all -> 0x00cd
                        r2.<init>(r8, r7)     // Catch: all -> 0x00cd
                        X.0jz r5 = new X.0jz     // Catch: all -> 0x00cd
                        r5.<init>()     // Catch: all -> 0x00cd
                        X.0k6 r1 = r4.A03     // Catch: all -> 0x00cd
                        X.3TO r0 = new X.3TO     // Catch: all -> 0x00cd
                        r0.<init>(r2, r5, r3)     // Catch: all -> 0x00cd
                        r1.A00(r0)     // Catch: all -> 0x00cd
                        r4.A04()     // Catch: all -> 0x00cd
                        r6.put(r8, r5)     // Catch: all -> 0x00cd
                    L_0x00cb:
                        monitor-exit(r7)
                        return r5
                    L_0x00cd:
                        r0 = move-exception
                        monitor-exit(r7)
                        throw r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3TN.Ael(X.0jz):java.lang.Object");
                }
            };
            C13600jz r2 = new C13600jz();
            r5.A03.A00(new AnonymousClass3TO(r3, r2, executor));
            r5.A04();
            return ((C88984If) A04(r2)).A00;
        }
        throw new IOException("MAIN_THREAD");
    }

    public final void A06() {
        boolean z;
        if (!A0B(A00(C13430ji.A00(this.A01), "*"))) {
            C13710kC r2 = this.A05;
            synchronized (r2) {
                z = false;
                if (r2.A00() != null) {
                    z = true;
                }
            }
            if (!z) {
                return;
            }
        }
        A08();
    }

    public final synchronized void A07() {
        A08.A02();
        if (this.A02.A00()) {
            A08();
        }
    }

    public final synchronized void A08() {
        if (!this.A00) {
            A09(0);
        }
    }

    public final synchronized void A09(long j) {
        A02(new RunnableBRunnable0Shape0S0300100_I0(this, this.A05, Math.min(Math.max(30L, j << 1), A0A)), j);
        this.A00 = true;
    }

    public final synchronized void A0A(boolean z) {
        this.A00 = z;
    }

    public final boolean A0B(C16070oO r8) {
        if (r8 != null) {
            String A05 = this.A03.A05();
            if (System.currentTimeMillis() <= r8.A00 + C16070oO.A03 && A05.equals(r8.A02)) {
                return false;
            }
        }
        return true;
    }

    public static FirebaseInstanceId getInstance(C13030j1 r2) {
        r2.A02();
        return (FirebaseInstanceId) r2.A02.A02(FirebaseInstanceId.class);
    }
}
