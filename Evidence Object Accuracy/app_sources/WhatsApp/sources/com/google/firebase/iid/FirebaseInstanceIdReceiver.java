package com.google.firebase.iid;

import X.AnonymousClass01S;
import X.AnonymousClass3LJ;
import X.C63723Cr;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;

/* loaded from: classes2.dex */
public final class FirebaseInstanceIdReceiver extends AnonymousClass01S {
    public static AnonymousClass3LJ A00;

    public static int A00(BroadcastReceiver broadcastReceiver, Context context, Intent intent) {
        AnonymousClass3LJ r4;
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        if (broadcastReceiver.isOrderedBroadcast()) {
            broadcastReceiver.setResultCode(-1);
        }
        synchronized (FirebaseInstanceIdReceiver.class) {
            r4 = A00;
            if (r4 == null) {
                r4 = new AnonymousClass3LJ(context);
                A00 = r4;
            }
        }
        BroadcastReceiver.PendingResult goAsync = broadcastReceiver.goAsync();
        synchronized (r4) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "new intent queued in the bind-strategy delivery");
            }
            r4.A04.add(new C63723Cr(goAsync, intent, r4.A05));
            r4.A00();
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f8, code lost:
        if (r10.getApplicationInfo().targetSdkVersion < 26) goto L_0x00fa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(android.content.Context r10, android.content.Intent r11) {
        /*
        // Method dump skipped, instructions count: 643
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceIdReceiver.A01(android.content.Context, android.content.Intent):void");
    }

    @Override // android.content.BroadcastReceiver
    public final void onReceive(Context context, Intent intent) {
        Intent intent2;
        if (intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            if (!(parcelableExtra instanceof Intent) || (intent2 = (Intent) parcelableExtra) == null) {
                A01(context, intent);
            } else {
                A01(context, intent2);
            }
        }
    }
}
