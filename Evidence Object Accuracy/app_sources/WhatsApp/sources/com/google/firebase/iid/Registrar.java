package com.google.firebase.iid;

import X.AbstractC13110jA;
import X.C13020j0;
import X.C13030j1;
import X.C13080j7;
import X.C13090j8;
import X.C13120jB;
import X.C13130jC;
import X.C13140jD;
import X.C13270jQ;
import X.C13350ja;
import X.C13360jb;
import X.C13370jc;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public final class Registrar {
    public final List getComponents() {
        C13090j8 r4 = new C13090j8(FirebaseInstanceId.class, new Class[0]);
        r4.A01(new C13140jD(C13030j1.class, 1));
        r4.A01(new C13140jD(C13270jQ.class, 1));
        r4.A01(new C13140jD(C13130jC.class, 1));
        AbstractC13110jA r0 = C13350ja.A00;
        C13020j0.A02(r0, "Null factory");
        r4.A02 = r0;
        boolean z = false;
        if (r4.A00 == 0) {
            z = true;
        }
        C13020j0.A04("Instantiation type has already been set.", z);
        r4.A00 = 1;
        C13080j7 A00 = r4.A00();
        C13090j8 r1 = new C13090j8(C13360jb.class, new Class[0]);
        r1.A01(new C13140jD(FirebaseInstanceId.class, 1));
        AbstractC13110jA r02 = C13370jc.A00;
        C13020j0.A02(r02, "Null factory");
        r1.A02 = r02;
        return Arrays.asList(A00, r1.A00(), C13120jB.A00("fire-iid", "20.0.0"));
    }
}
