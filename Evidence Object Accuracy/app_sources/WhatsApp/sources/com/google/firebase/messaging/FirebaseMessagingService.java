package com.google.firebase.messaging;

import X.AbstractServiceC51732Yj;
import X.AnonymousClass3G4;
import X.AnonymousClass3J2;
import X.AnonymousClass4MN;
import X.AnonymousClass4PV;
import X.C005602s;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13020j0;
import X.C13570jw;
import X.C13600jz;
import X.C13700kB;
import X.C251618i;
import X.C472729v;
import X.C56432ks;
import X.C64903Hj;
import X.C71093cN;
import X.C81393tw;
import X.C87554By;
import X.CallableC71393cr;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.app.NotificationCompat$BigPictureStyle;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.whatsapp.push.GcmListenerService;
import com.whatsapp.push.RegistrationIntentService;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* loaded from: classes2.dex */
public class FirebaseMessagingService extends AbstractServiceC51732Yj {
    public static final Queue A00 = new ArrayDeque(10);

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractServiceC51732Yj
    public final void A02(Intent intent) {
        int i;
        C13600jz A01;
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.RECEIVE".equals(action) || "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(action)) {
            String stringExtra = intent.getStringExtra("google.message_id");
            if (TextUtils.isEmpty(stringExtra)) {
                A01 = new C13600jz();
                A01.A08(null);
            } else {
                Bundle A0D = C12970iu.A0D();
                A0D.putString("google.message_id", stringExtra);
                C13570jw A002 = C13570jw.A00(this);
                synchronized (A002) {
                    i = A002.A00;
                    A002.A00 = i + 1;
                }
                A01 = A002.A01(new C81393tw(i, A0D));
            }
            try {
                if (!TextUtils.isEmpty(stringExtra)) {
                    Queue queue = A00;
                    if (queue.contains(stringExtra)) {
                        if (Log.isLoggable("FirebaseMessaging", 3)) {
                            Log.d("FirebaseMessaging", C12960it.A0c(String.valueOf(stringExtra), "Received duplicate message: "));
                        }
                        C13700kB.A00(A01, TimeUnit.SECONDS, 1);
                        return;
                    }
                    if (queue.size() >= 10) {
                        queue.remove();
                    }
                    queue.add(stringExtra);
                }
                C13700kB.A00(A01, TimeUnit.SECONDS, 1);
                return;
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                String valueOf = String.valueOf(e);
                StringBuilder A0t = C12980iv.A0t(valueOf.length() + 20);
                A0t.append("Message ack failed: ");
                Log.w("FirebaseMessaging", C12960it.A0d(valueOf, A0t));
                return;
            }
            String stringExtra2 = intent.getStringExtra("message_type");
            if (stringExtra2 == null) {
                stringExtra2 = "gcm";
            }
            switch (stringExtra2.hashCode()) {
                case -2062414158:
                    if (stringExtra2.equals("deleted_messages")) {
                        if (this instanceof GcmListenerService) {
                            C251618i r2 = (C251618i) ((GcmListenerService) this).A00.get();
                            synchronized (r2) {
                                r2.A00(null, null, null, null, null, null, null, null, null, null, null, null, 0, false);
                                break;
                            }
                        }
                    }
                    Log.w("FirebaseMessaging", C12960it.A0c(stringExtra2, "Received message with unknown type: "));
                    break;
                case 102161:
                    if (stringExtra2.equals("gcm")) {
                        if (AnonymousClass3G4.A01(intent)) {
                            AnonymousClass3G4.A00(intent, "_nr");
                        }
                        Bundle extras = intent.getExtras();
                        if (extras == null) {
                            extras = C12970iu.A0D();
                        }
                        extras.remove("androidx.contentpager.content.wakelockid");
                        if (C64903Hj.A01(extras)) {
                            C64903Hj r22 = new C64903Hj(extras);
                            ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
                            AnonymousClass4PV r12 = new AnonymousClass4PV(this, r22, newSingleThreadExecutor);
                            try {
                                C64903Hj r10 = r12.A01;
                                if (!r10.A08("gcm.n.noui")) {
                                    Context context = r12.A00;
                                    if (!((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
                                        if (!C472729v.A02()) {
                                            SystemClock.sleep(10);
                                        }
                                        int myPid = Process.myPid();
                                        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
                                        if (runningAppProcesses != null) {
                                            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
                                            while (true) {
                                                if (it.hasNext()) {
                                                    ActivityManager.RunningAppProcessInfo next = it.next();
                                                    if (next.pid == myPid) {
                                                        if (next.importance == 100) {
                                                            newSingleThreadExecutor.shutdown();
                                                            if (AnonymousClass3G4.A01(intent)) {
                                                                AnonymousClass3G4.A00(intent, "_nf");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    String A06 = r10.A06("gcm.n.image");
                                    C71093cN r4 = null;
                                    if (!TextUtils.isEmpty(A06)) {
                                        try {
                                            C71093cN r23 = new C71093cN(new URL(A06));
                                            r4 = r23;
                                            Executor executor = r12.A02;
                                            CallableC71393cr r122 = new Callable(r23) { // from class: X.3cr
                                                public final C71093cN A00;

                                                {
                                                    this.A00 = r1;
                                                }

                                                @Override // java.util.concurrent.Callable
                                                public final Object call() {
                                                    byte[] bArr;
                                                    C71093cN r3 = this.A00;
                                                    URL url = r3.A01;
                                                    String valueOf2 = String.valueOf(url);
                                                    StringBuilder A0t2 = C12980iv.A0t(valueOf2.length() + 22);
                                                    A0t2.append("Starting download of: ");
                                                    Log.i("FirebaseMessaging", C12960it.A0d(valueOf2, A0t2));
                                                    URLConnection openConnection = url.openConnection();
                                                    if (openConnection.getContentLength() <= 1048576) {
                                                        InputStream inputStream = openConnection.getInputStream();
                                                        try {
                                                            r3.A02 = inputStream;
                                                            C629239e r32 = new C629239e(inputStream);
                                                            ArrayDeque arrayDeque = new ArrayDeque(20);
                                                            int i2 = DefaultCrypto.BUFFER_SIZE;
                                                            int i3 = 0;
                                                            while (true) {
                                                                if (i3 < 2147483639) {
                                                                    int min = Math.min(i2, 2147483639 - i3);
                                                                    byte[] bArr2 = new byte[min];
                                                                    arrayDeque.add(bArr2);
                                                                    int i4 = 0;
                                                                    while (i4 < min) {
                                                                        int read = r32.read(bArr2, i4, min - i4);
                                                                        if (read == -1) {
                                                                            bArr = new byte[i3];
                                                                            int i5 = i3;
                                                                            while (i5 > 0) {
                                                                                byte[] bArr3 = (byte[]) arrayDeque.removeFirst();
                                                                                int min2 = Math.min(i5, bArr3.length);
                                                                                System.arraycopy(bArr3, 0, bArr, i3 - i5, min2);
                                                                                i5 -= min2;
                                                                            }
                                                                        } else {
                                                                            i4 += read;
                                                                            i3 += read;
                                                                        }
                                                                    }
                                                                    long j = ((long) i2) << 1;
                                                                    i2 = j > 2147483647L ? Integer.MAX_VALUE : j < -2147483648L ? Integer.MIN_VALUE : (int) j;
                                                                } else if (r32.read() == -1) {
                                                                    bArr = new byte[2147483639];
                                                                    int i6 = 2147483639;
                                                                    while (true) {
                                                                        byte[] bArr4 = (byte[]) arrayDeque.removeFirst();
                                                                        int min3 = Math.min(i6, bArr4.length);
                                                                        System.arraycopy(bArr4, 0, bArr, 2147483639 - i6, min3);
                                                                        i6 -= min3;
                                                                        if (i6 <= 0) {
                                                                            break;
                                                                        }
                                                                    }
                                                                } else {
                                                                    throw new OutOfMemoryError("input is too large to fit in a byte array");
                                                                }
                                                            }
                                                            if (inputStream != null) {
                                                                inputStream.close();
                                                            }
                                                            if (Log.isLoggable("FirebaseMessaging", 2)) {
                                                                int length = bArr.length;
                                                                String valueOf3 = String.valueOf(url);
                                                                StringBuilder A0t3 = C12980iv.A0t(valueOf3.length() + 34);
                                                                A0t3.append("Downloaded ");
                                                                A0t3.append(length);
                                                                A0t3.append(" bytes from ");
                                                                Log.v("FirebaseMessaging", C12960it.A0d(valueOf3, A0t3));
                                                            }
                                                            int length2 = bArr.length;
                                                            if (length2 <= 1048576) {
                                                                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, length2);
                                                                if (decodeByteArray != null) {
                                                                    if (Log.isLoggable("FirebaseMessaging", 3)) {
                                                                        String valueOf4 = String.valueOf(url);
                                                                        StringBuilder A0t4 = C12980iv.A0t(valueOf4.length() + 31);
                                                                        A0t4.append("Successfully downloaded image: ");
                                                                        Log.d("FirebaseMessaging", C12960it.A0d(valueOf4, A0t4));
                                                                    }
                                                                    return decodeByteArray;
                                                                }
                                                                String valueOf5 = String.valueOf(url);
                                                                StringBuilder A0t5 = C12980iv.A0t(valueOf5.length() + 24);
                                                                A0t5.append("Failed to decode image: ");
                                                                throw C12990iw.A0i(C12960it.A0d(valueOf5, A0t5));
                                                            }
                                                            throw C12990iw.A0i("Image exceeds max size of 1048576");
                                                        } catch (Throwable th) {
                                                            try {
                                                                throw th;
                                                            } catch (Throwable th2) {
                                                                if (inputStream != null) {
                                                                    try {
                                                                        inputStream.close();
                                                                        throw th2;
                                                                    } catch (Throwable th3) {
                                                                        C13490jo.A00.A00(th, th3);
                                                                        throw th2;
                                                                    }
                                                                }
                                                                throw th2;
                                                            }
                                                        }
                                                    } else {
                                                        throw C12990iw.A0i("Content-Length exceeds max size of 1048576");
                                                    }
                                                }
                                            };
                                            C13020j0.A02(executor, "Executor must not be null");
                                            C13600jz r5 = new C13600jz();
                                            executor.execute(new RunnableBRunnable0Shape0S0200000_I0(r5, 2, r122));
                                            r4.A00 = r5;
                                        } catch (MalformedURLException unused) {
                                            Log.w("FirebaseMessaging", C12960it.A0c(String.valueOf(A06), "Not downloading image, bad URL: "));
                                        }
                                    }
                                    AnonymousClass4MN A012 = AnonymousClass3J2.A01(context, r10);
                                    C005602s r52 = A012.A00;
                                    if (r4 != null) {
                                        try {
                                            try {
                                                C13600jz r13 = r4.A00;
                                                C13020j0.A01(r13);
                                                Bitmap bitmap = (Bitmap) C13700kB.A00(r13, TimeUnit.SECONDS, 5);
                                                r52.A06(bitmap);
                                                NotificationCompat$BigPictureStyle notificationCompat$BigPictureStyle = new NotificationCompat$BigPictureStyle();
                                                notificationCompat$BigPictureStyle.A00 = bitmap;
                                                notificationCompat$BigPictureStyle.A01 = true;
                                                r52.A08(notificationCompat$BigPictureStyle);
                                            } catch (TimeoutException unused2) {
                                                Log.w("FirebaseMessaging", "Failed to download image in time, showing notification without it");
                                                r4.close();
                                            }
                                        } catch (InterruptedException unused3) {
                                            Log.w("FirebaseMessaging", "Interrupted while downloading image, showing notification without it");
                                            r4.close();
                                            Thread.currentThread().interrupt();
                                        } catch (ExecutionException e2) {
                                            String valueOf2 = String.valueOf(e2.getCause());
                                            StringBuilder A0t2 = C12980iv.A0t(valueOf2.length() + 26);
                                            A0t2.append("Failed to download image: ");
                                            Log.w("FirebaseMessaging", C12960it.A0d(valueOf2, A0t2));
                                        }
                                    }
                                    if (Log.isLoggable("FirebaseMessaging", 3)) {
                                        Log.d("FirebaseMessaging", "Showing notification");
                                    }
                                    ((NotificationManager) context.getSystemService("notification")).notify(A012.A01, 0, r52.A01());
                                }
                                break;
                            } finally {
                                newSingleThreadExecutor.shutdown();
                            }
                        }
                        C56432ks r3 = new C56432ks(extras);
                        if (this instanceof GcmListenerService) {
                            Map A02 = r3.A02();
                            String A0t3 = C12970iu.A0t("id", A02);
                            String A0t4 = C12970iu.A0t("ip", A02);
                            String A0t5 = C12970iu.A0t("cl_sess", A02);
                            String A0t6 = C12970iu.A0t("mmsov", A02);
                            String A0t7 = C12970iu.A0t("fbips", A02);
                            String A0t8 = C12970iu.A0t("er_ri", A02);
                            boolean equals = "1".equals(A02.get("notify"));
                            ((C251618i) ((GcmListenerService) this).A00.get()).A00(Integer.valueOf(r3.A00()), Integer.valueOf(r3.A01()), A0t3, A0t4, A0t5, A0t6, A0t7, A0t8, C12970iu.A0t("push_id", A02), C12970iu.A0t("push_event_id", A02), C12970iu.A0t("push_ts", A02), C12970iu.A0t("pn", A02), 0, equals);
                            break;
                        }
                    }
                    Log.w("FirebaseMessaging", C12960it.A0c(stringExtra2, "Received message with unknown type: "));
                    break;
                case 814694033:
                    if (stringExtra2.equals("send_error")) {
                        if (intent.getStringExtra("google.message_id") == null) {
                            intent.getStringExtra("message_id");
                        }
                        new C87554By(intent.getStringExtra("error"));
                        break;
                    }
                    Log.w("FirebaseMessaging", C12960it.A0c(stringExtra2, "Received message with unknown type: "));
                    break;
                case 814800675:
                    if (stringExtra2.equals("send_event")) {
                        intent.getStringExtra("google.message_id");
                        break;
                    }
                    Log.w("FirebaseMessaging", C12960it.A0c(stringExtra2, "Received message with unknown type: "));
                    break;
                default:
                    Log.w("FirebaseMessaging", C12960it.A0c(stringExtra2, "Received message with unknown type: "));
                    break;
            }
        } else if ("com.google.firebase.messaging.NOTIFICATION_DISMISS".equals(action)) {
            if (AnonymousClass3G4.A01(intent)) {
                AnonymousClass3G4.A00(intent, "_nd");
            }
        } else if ("com.google.firebase.messaging.NEW_TOKEN".equals(action)) {
            intent.getStringExtra("token");
            if (this instanceof GcmListenerService) {
                RegistrationIntentService.A01(this);
            }
        } else {
            Log.d("FirebaseMessaging", C12960it.A0c(String.valueOf(intent.getAction()), "Unknown intent action: "));
        }
    }
}
