package com.whatsapp.blockbusiness.blockreasonlist;

import X.AbstractC14440lR;
import X.AnonymousClass014;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass10Y;
import X.C15550nR;
import X.C16120oU;
import X.C16170oZ;
import X.C16700pc;
import X.C22160yd;
import X.C238013b;
import X.C254119h;
import X.C27691It;
import android.app.Application;

/* loaded from: classes2.dex */
public final class BlockReasonListViewModel extends AnonymousClass014 {
    public final Application A00;
    public final AnonymousClass017 A01;
    public final AnonymousClass016 A02;
    public final C16170oZ A03;
    public final C238013b A04;
    public final C15550nR A05;
    public final C254119h A06;
    public final C22160yd A07;
    public final AnonymousClass10Y A08;
    public final C16120oU A09;
    public final C27691It A0A = new C27691It();
    public final AbstractC14440lR A0B;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BlockReasonListViewModel(Application application, C16170oZ r3, C238013b r4, C15550nR r5, C254119h r6, C22160yd r7, AnonymousClass10Y r8, C16120oU r9, AbstractC14440lR r10) {
        super(application);
        C16700pc.A0E(r10, 2);
        C16700pc.A0E(r7, 3);
        C16700pc.A0E(r9, 4);
        C16700pc.A0E(r3, 5);
        C16700pc.A0E(r5, 6);
        C16700pc.A0E(r8, 7);
        C16700pc.A0E(r4, 8);
        C16700pc.A0E(r6, 9);
        this.A0B = r10;
        this.A07 = r7;
        this.A09 = r9;
        this.A03 = r3;
        this.A05 = r5;
        this.A08 = r8;
        this.A04 = r4;
        this.A06 = r6;
        Application application2 = ((AnonymousClass014) this).A00;
        C16700pc.A0B(application2);
        this.A00 = application2;
        AnonymousClass016 r0 = new AnonymousClass016();
        this.A02 = r0;
        this.A01 = r0;
    }
}
