package com.whatsapp.blockbusiness.blockreasonlist;

import X.AbstractC16710pd;
import X.ActivityC13810kN;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01Y;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass03C;
import X.AnonymousClass0F5;
import X.AnonymousClass19M;
import X.AnonymousClass1P3;
import X.AnonymousClass2Er;
import X.AnonymousClass4Yq;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C16630pM;
import X.C16700pc;
import X.C41861uH;
import X.C54382gd;
import X.C627138h;
import X.C71873de;
import X.C72143e5;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.ViewOnClickCListenerShape1S1100000_I1;
import com.whatsapp.FAQTextView;
import com.whatsapp.R;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListViewModel;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* loaded from: classes2.dex */
public final class BlockReasonListFragment extends Hilt_BlockReasonListFragment {
    public CheckBox A00;
    public RecyclerView A01;
    public C54382gd A02;
    public Button A03;
    public C15550nR A04;
    public C15610nY A05;
    public AnonymousClass01d A06;
    public AnonymousClass018 A07;
    public AnonymousClass19M A08;
    public C14850m9 A09;
    public C16630pM A0A;
    public final AbstractC16710pd A0B = AnonymousClass4Yq.A00(new C71873de(this));

    public static final void A00(Bundle bundle, BlockReasonListFragment blockReasonListFragment, List list) {
        C16700pc.A0E(blockReasonListFragment, 0);
        C16700pc.A0E(list, 2);
        AnonymousClass19M r3 = blockReasonListFragment.A08;
        if (r3 != null) {
            AnonymousClass01d r1 = blockReasonListFragment.A06;
            if (r1 != null) {
                AnonymousClass018 r2 = blockReasonListFragment.A07;
                if (r2 != null) {
                    C16630pM r4 = blockReasonListFragment.A0A;
                    if (r4 != null) {
                        blockReasonListFragment.A02 = new C54382gd(r1, r2, r3, r4, list, new C72143e5(blockReasonListFragment));
                        if (bundle != null) {
                            int i = bundle.getInt("selectedItem");
                            String string = bundle.getString("text", "");
                            C16700pc.A0B(string);
                            C54382gd r22 = blockReasonListFragment.A02;
                            if (r22 == null) {
                                throw C16700pc.A06("adapter");
                            }
                            r22.A00 = i;
                            r22.A01 = string;
                            Object A02 = AnonymousClass01Y.A02(r22.A06, i);
                            if (A02 != null) {
                                r22.A07.AJ4(A02);
                            }
                            r22.A02();
                        }
                        RecyclerView recyclerView = blockReasonListFragment.A01;
                        if (recyclerView == null) {
                            throw C16700pc.A06("recyclerView");
                        }
                        C54382gd r0 = blockReasonListFragment.A02;
                        if (r0 == null) {
                            throw C16700pc.A06("adapter");
                        }
                        recyclerView.setAdapter(r0);
                        return;
                    }
                    throw C16700pc.A06("sharedPreferencesFactory");
                }
                throw C16700pc.A06("whatsAppLocale");
            }
            throw C16700pc.A06("systemServices");
        }
        throw C16700pc.A06("emojiLoader");
    }

    public static final void A01(BlockReasonListFragment blockReasonListFragment, String str) {
        C16700pc.A0E(blockReasonListFragment, 0);
        C16700pc.A0E(str, 1);
        boolean z = blockReasonListFragment.A03().getBoolean("show_success_toast");
        boolean z2 = blockReasonListFragment.A03().getBoolean("from_spam_panel");
        CheckBox checkBox = blockReasonListFragment.A00;
        String str2 = null;
        if (checkBox == null) {
            throw C16700pc.A06("reportCheckbox");
        }
        boolean isChecked = checkBox.isChecked();
        String string = blockReasonListFragment.A03().getString("entry_point");
        if (string != null) {
            ActivityC13810kN r8 = (ActivityC13810kN) blockReasonListFragment.A0C();
            BlockReasonListViewModel blockReasonListViewModel = (BlockReasonListViewModel) blockReasonListFragment.A0B.getValue();
            C54382gd r0 = blockReasonListFragment.A02;
            if (r0 == null) {
                throw C16700pc.A06("adapter");
            }
            AnonymousClass2Er r02 = (AnonymousClass2Er) AnonymousClass01Y.A02(r0.A06, r0.A00);
            if (r02 != null) {
                str2 = r02.A00;
            }
            C54382gd r03 = blockReasonListFragment.A02;
            if (r03 == null) {
                throw C16700pc.A06("adapter");
            }
            String obj = r03.A01.toString();
            C16700pc.A0E(r8, 0);
            UserJid userJid = UserJid.get(str);
            C16700pc.A0B(userJid);
            C15370n3 A0B = blockReasonListViewModel.A05.A0B(userJid);
            String str3 = null;
            if (obj != null && !AnonymousClass03C.A0J(obj)) {
                str3 = obj;
            }
            if (z2) {
                C12990iw.A1N(new C627138h(r8, r8, blockReasonListViewModel.A03, new AnonymousClass1P3() { // from class: X.53w
                    @Override // X.AnonymousClass1P3
                    public final void AVM(boolean z3) {
                        BlockReasonListViewModel blockReasonListViewModel2 = BlockReasonListViewModel.this;
                        C16700pc.A0E(blockReasonListViewModel2, 0);
                        blockReasonListViewModel2.A0A.A0B(null);
                    }
                }, blockReasonListViewModel.A06, A0B, str2, str3, string, false, isChecked), blockReasonListViewModel.A0B);
                return;
            }
            blockReasonListViewModel.A04.A08(r8, new AnonymousClass1P3() { // from class: X.53x
                @Override // X.AnonymousClass1P3
                public final void AVM(boolean z3) {
                    BlockReasonListViewModel blockReasonListViewModel2 = BlockReasonListViewModel.this;
                    C16700pc.A0E(blockReasonListViewModel2, 0);
                    blockReasonListViewModel2.A0A.A0B(null);
                }
            }, A0B, str2, str3, string, true, z);
            return;
        }
        throw C12970iu.A0f("Required value was null.");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        C16700pc.A0E(bundle, 0);
        super.A0w(bundle);
        C54382gd r0 = this.A02;
        if (r0 == null) {
            throw C16700pc.A06("adapter");
        }
        bundle.putInt("selectedItem", r0.A00);
        C54382gd r02 = this.A02;
        if (r02 == null) {
            throw C16700pc.A06("adapter");
        }
        bundle.putString("text", r02.A01.toString());
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        String string = A03().getString("jid");
        if (string != null) {
            View A01 = C16700pc.A01(layoutInflater, viewGroup, R.layout.block_reason_list_fragment);
            View findViewById = A01.findViewById(R.id.block_reason_list);
            RecyclerView recyclerView = (RecyclerView) findViewById;
            recyclerView.getContext();
            recyclerView.setLayoutManager(new LinearLayoutManager(1));
            AnonymousClass0F5 r6 = new AnonymousClass0F5(recyclerView.getContext());
            Drawable A04 = AnonymousClass00T.A04(recyclerView.getContext(), R.drawable.divider_gray);
            if (A04 != null) {
                r6.A01 = A04;
            }
            recyclerView.A0l(r6);
            recyclerView.A0h = true;
            C16700pc.A0B(findViewById);
            this.A01 = recyclerView;
            AnonymousClass028.A0l(A01.findViewById(R.id.reason_for_blocking), true);
            UserJid userJid = UserJid.get(string);
            C16700pc.A0B(userJid);
            C15550nR r0 = this.A04;
            if (r0 != null) {
                C15370n3 A0B = r0.A0B(userJid);
                C14850m9 r02 = this.A09;
                if (r02 != null) {
                    boolean A00 = C41861uH.A00(r02, userJid);
                    int i = R.string.business_block_header;
                    if (A00) {
                        i = R.string.wac_block_header;
                    }
                    FAQTextView fAQTextView = (FAQTextView) A01.findViewById(R.id.blocking_info);
                    Object[] objArr = new Object[1];
                    C15610nY r1 = this.A05;
                    if (r1 != null) {
                        fAQTextView.setEducationTextFromNamedArticle(new SpannableString(C12970iu.A0q(this, r1.A0B(A0B, -1, true), objArr, 0, i)), "chats", "controls-when-messaging-businesses");
                        this.A00 = (CheckBox) C16700pc.A03(A01, R.id.report_biz_checkbox);
                        if (A03().getBoolean("show_report_upsell")) {
                            C12980iv.A1B(A01, R.id.report_biz_setting, 0);
                        }
                        Button button = (Button) C16700pc.A03(A01, R.id.block_button);
                        this.A03 = button;
                        if (button == null) {
                            throw C16700pc.A06("blockButton");
                        }
                        button.setOnClickListener(new ViewOnClickCListenerShape1S1100000_I1(0, string, this));
                        Button button2 = this.A03;
                        if (button2 == null) {
                            throw C16700pc.A06("blockButton");
                        }
                        C14850m9 r12 = this.A09;
                        if (r12 != null) {
                            button2.setEnabled(C41861uH.A00(r12, UserJid.get(string)));
                            return A01;
                        }
                        throw C16700pc.A06("abProps");
                    }
                    throw C16700pc.A06("waContactNames");
                }
                throw C16700pc.A06("abProps");
            }
            throw C16700pc.A06("contactManager");
        }
        throw C12970iu.A0f("Required value was null.");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        String string = A03().getString("jid");
        if (string != null) {
            BlockReasonListViewModel blockReasonListViewModel = (BlockReasonListViewModel) this.A0B.getValue();
            UserJid userJid = UserJid.get(string);
            C16700pc.A0B(userJid);
            blockReasonListViewModel.A0B.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(blockReasonListViewModel, 18, userJid));
            return;
        }
        throw C12970iu.A0f("Required value was null.");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        AbstractC16710pd r3 = this.A0B;
        ((BlockReasonListViewModel) r3.getValue()).A01.A05(A0G(), new AnonymousClass02B(bundle, this) { // from class: X.4u3
            public final /* synthetic */ Bundle A00;
            public final /* synthetic */ BlockReasonListFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                BlockReasonListFragment.A00(this.A00, this.A01, (List) obj);
            }
        });
        C12970iu.A1P(A0G(), ((BlockReasonListViewModel) r3.getValue()).A0A, this, 5);
    }
}
