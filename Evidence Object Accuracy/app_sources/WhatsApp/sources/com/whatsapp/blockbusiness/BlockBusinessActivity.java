package com.whatsapp.blockbusiness;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C16700pc;
import X.C41861uH;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public final class BlockBusinessActivity extends ActivityC13790kL {
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String string;
        super.onCreate(bundle);
        setContentView(R.layout.block_business_activity);
        String stringExtra = getIntent().getStringExtra("jid_extra");
        if (stringExtra != null) {
            if (C41861uH.A00(((ActivityC13810kN) this).A0C, UserJid.get(stringExtra))) {
                string = C12960it.A0X(this, "WhatsApp", new Object[1], 0, R.string.wac_block_text);
            } else {
                string = getString(R.string.block_business_title);
            }
            C16700pc.A0B(string);
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0M(true);
                A1U.A0I(string);
            }
            if (bundle == null) {
                Intent intent = getIntent();
                C004902f A0P = C12970iu.A0P(this);
                String stringExtra2 = intent.getStringExtra("entry_point_extra");
                if (stringExtra2 != null) {
                    boolean booleanExtra = intent.getBooleanExtra("show_success_toast_extra", false);
                    boolean booleanExtra2 = intent.getBooleanExtra("from_spam_panel_extra", false);
                    boolean booleanExtra3 = intent.getBooleanExtra("show_report_upsell", false);
                    Bundle A0D = C12970iu.A0D();
                    A0D.putString("jid", stringExtra);
                    A0D.putString("entry_point", stringExtra2);
                    A0D.putBoolean("show_success_toast", booleanExtra);
                    A0D.putBoolean("from_spam_panel", booleanExtra2);
                    A0D.putBoolean("show_report_upsell", booleanExtra3);
                    BlockReasonListFragment blockReasonListFragment = new BlockReasonListFragment();
                    blockReasonListFragment.A0U(A0D);
                    A0P.A07(blockReasonListFragment, R.id.container);
                    if (!A0P.A0E) {
                        A0P.A0F = false;
                        A0P.A0J.A0d(A0P, false);
                        return;
                    }
                    throw C12960it.A0U("This transaction is already being added to the back stack");
                }
                throw C12970iu.A0f("Required value was null.");
            }
            return;
        }
        throw C12970iu.A0f("Required value was null.");
    }
}
