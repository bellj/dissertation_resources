package com.whatsapp.chatinfo;

import X.AbstractC14640lm;
import X.AnonymousClass018;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C12970iu;
import X.C14850m9;
import X.C15550nR;
import X.C15600nX;
import X.C16120oU;
import X.C252018m;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class ChatInfoActivity$EncryptionExplanationDialogFragment extends Hilt_ChatInfoActivity_EncryptionExplanationDialogFragment {
    public AnonymousClass12P A00;
    public C15550nR A01;
    public AnonymousClass018 A02;
    public C15600nX A03;
    public AnonymousClass19M A04;
    public C14850m9 A05;
    public C16120oU A06;
    public AnonymousClass11G A07;
    public C252018m A08;

    public static ChatInfoActivity$EncryptionExplanationDialogFragment A00(AbstractC14640lm r6) {
        ChatInfoActivity$EncryptionExplanationDialogFragment chatInfoActivity$EncryptionExplanationDialogFragment = new ChatInfoActivity$EncryptionExplanationDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r6.getRawString());
        A0D.putInt("provider_category", 1);
        A0D.putString("display_name", null);
        chatInfoActivity$EncryptionExplanationDialogFragment.A0U(A0D);
        return chatInfoActivity$EncryptionExplanationDialogFragment;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0085  */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r11) {
        /*
            r10 = this;
            android.os.Bundle r3 = r10.A03()
            java.lang.String r0 = "jid"
            java.lang.String r2 = r3.getString(r0)
            java.lang.String r0 = "display_name"
            java.lang.String r9 = r3.getString(r0)
            java.lang.String r1 = "provider_category"
            r0 = 0
            int r6 = r3.getInt(r1, r0)
            X.0nR r1 = r10.A01
            X.0lm r0 = X.AbstractC14640lm.A01(r2)
            X.AnonymousClass009.A06(r0, r2)
            X.0n3 r5 = r1.A0B(r0)
            X.0m9 r8 = r10.A05
            X.11G r0 = r10.A07
            X.00k r7 = r10.A0B()
            com.whatsapp.jid.Jid r1 = r5.A0D
            boolean r0 = r0.A01(r1)
            if (r0 != 0) goto L_0x00c5
            boolean r0 = X.C38311ns.A00(r8, r1)
            if (r0 != 0) goto L_0x00c5
            r4 = 1
            if (r6 == r4) goto L_0x0050
            r3 = 0
            r2 = 2
            if (r6 == r2) goto L_0x00b4
            r0 = 3
            if (r6 == r0) goto L_0x00a0
            r0 = 4
            if (r6 == r0) goto L_0x00a0
            java.lang.String r0 = "providerCategoryToModal unexpected argument value for providerCategory: "
            java.lang.String r0 = X.C12960it.A0W(r6, r0)
            com.whatsapp.util.Log.e(r0)
        L_0x0050:
            r0 = 2131887998(0x7f12077e, float:1.9410619E38)
        L_0x0053:
            java.lang.String r2 = r7.getString(r0)
        L_0x0057:
            X.02e r3 = X.C12970iu.A0O(r10)
            X.00k r1 = r10.A0B()
            X.19M r0 = r10.A04
            java.lang.CharSequence r0 = X.AbstractC36671kL.A05(r1, r0, r2)
            r3.A0A(r0)
            r2 = 1
            r3.A0B(r2)
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            r0 = 22
            X.C12970iu.A1K(r3, r10, r0, r1)
            r1 = 2131889088(0x7f120bc0, float:1.941283E38)
            X.3KN r0 = new X.3KN
            r0.<init>(r6)
            r3.A00(r1, r0)
            boolean r0 = r5.A0K()
            if (r0 != 0) goto L_0x009b
            com.whatsapp.jid.Jid r0 = r5.A0D
            boolean r0 = X.C15380n4.A0F(r0)
            if (r0 != 0) goto L_0x009b
            if (r6 != r2) goto L_0x009b
            r2 = 2131888860(0x7f120adc, float:1.9412367E38)
            r1 = 2
            com.facebook.redex.IDxCListenerShape3S0200000_1_I1 r0 = new com.facebook.redex.IDxCListenerShape3S0200000_1_I1
            r0.<init>(r5, r1, r10)
            r3.setPositiveButton(r2, r0)
        L_0x009b:
            X.04S r0 = r3.create()
            return r0
        L_0x00a0:
            X.AnonymousClass009.A05(r9)
            boolean r0 = X.AnonymousClass3GN.A00(r8, r1)
            r1 = 2131887376(0x7f120510, float:1.9409357E38)
            if (r0 == 0) goto L_0x00ba
            r1 = 2131887375(0x7f12050f, float:1.9409355E38)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r3] = r9
            goto L_0x00c0
        L_0x00b4:
            X.AnonymousClass009.A05(r9)
            r1 = 2131887374(0x7f12050e, float:1.9409353E38)
        L_0x00ba:
            java.lang.Object[] r0 = new java.lang.Object[r2]
            r0[r3] = r9
            r0[r4] = r9
        L_0x00c0:
            java.lang.String r2 = r7.getString(r1, r0)
            goto L_0x0057
        L_0x00c5:
            r0 = 2131887377(0x7f120511, float:1.940936E38)
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ChatInfoActivity$EncryptionExplanationDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }
}
