package com.whatsapp.chatinfo;

import X.AbstractActivityC33001d7;
import X.AbstractC13980ke;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.AbstractC30111Wd;
import X.AbstractC33021d9;
import X.AbstractC33031dA;
import X.AbstractC33331dp;
import X.AbstractC454421p;
import X.AbstractC58392on;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass11G;
import X.AnonymousClass11I;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass131;
import X.AnonymousClass13P;
import X.AnonymousClass13Q;
import X.AnonymousClass14X;
import X.AnonymousClass150;
import X.AnonymousClass17R;
import X.AnonymousClass18U;
import X.AnonymousClass18W;
import X.AnonymousClass18X;
import X.AnonymousClass198;
import X.AnonymousClass19H;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass19Q;
import X.AnonymousClass19Z;
import X.AnonymousClass1AT;
import X.AnonymousClass1BB;
import X.AnonymousClass1BC;
import X.AnonymousClass1J1;
import X.AnonymousClass1VZ;
import X.AnonymousClass1lO;
import X.AnonymousClass2Cu;
import X.AnonymousClass2Dn;
import X.AnonymousClass2Dx;
import X.AnonymousClass2E2;
import X.AnonymousClass2Ew;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2TT;
import X.AnonymousClass2VA;
import X.AnonymousClass2VB;
import X.AnonymousClass37S;
import X.AnonymousClass3D0;
import X.AnonymousClass3DR;
import X.AnonymousClass3Dn;
import X.AnonymousClass44V;
import X.C102904pt;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15810nw;
import X.C15860o1;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16120oU;
import X.C16170oZ;
import X.C16370ot;
import X.C16590pI;
import X.C17070qD;
import X.C17170qN;
import X.C17220qS;
import X.C17900ra;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C19840ul;
import X.C19850um;
import X.C19990v2;
import X.C20000v3;
import X.C20020v5;
import X.C20050v8;
import X.C20510vs;
import X.C20670w8;
import X.C20710wC;
import X.C20730wE;
import X.C20750wG;
import X.C20830wO;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22100yW;
import X.C22280yp;
import X.C22330yu;
import X.C22600zL;
import X.C22670zS;
import X.C22680zT;
import X.C22700zV;
import X.C22710zW;
import X.C237913a;
import X.C238013b;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C244415n;
import X.C245015t;
import X.C246716k;
import X.C248917h;
import X.C249317l;
import X.C251118d;
import X.C252718t;
import X.C252818u;
import X.C253519b;
import X.C254219i;
import X.C254719n;
import X.C255719x;
import X.C25781Au;
import X.C27131Gd;
import X.C27151Gf;
import X.C28391Mz;
import X.C28811Pc;
import X.C30151Wh;
import X.C30721Yo;
import X.C32341c0;
import X.C32971d2;
import X.C33181da;
import X.C33701ew;
import X.C35391hk;
import X.C36531k2;
import X.C41341tN;
import X.C41351tO;
import X.C41421tV;
import X.C41861uH;
import X.C42641vY;
import X.C42941w9;
import X.C48962Ip;
import X.C48972Iq;
import X.C48982Ir;
import X.C51442Ut;
import X.C60322we;
import X.C63393Bk;
import X.C68993Xn;
import X.C69023Xq;
import X.C84363zB;
import X.C92434Vw;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewStub;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.ListItemWithLeftIcon;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.ui.media.MediaCard;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UnknownFormatConversionException;

/* loaded from: classes2.dex */
public class ContactInfoActivity extends AbstractActivityC33001d7 implements AbstractC13980ke, AbstractC33021d9, AbstractC33031dA {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public ViewStub A05;
    public ImageView A06;
    public LinearLayout A07;
    public ListView A08;
    public C253519b A09;
    public C22680zT A0A;
    public C48962Ip A0B;
    public C48972Iq A0C;
    public C48982Ir A0D;
    public AnonymousClass18U A0E;
    public C237913a A0F;
    public C20670w8 A0G;
    public WaTextView A0H;
    public AnonymousClass2TT A0I;
    public AnonymousClass2VA A0J;
    public C254719n A0K;
    public C14650lo A0L;
    public AnonymousClass10A A0M;
    public C246716k A0N;
    public C25781Au A0O;
    public AnonymousClass2VB A0P;
    public C238013b A0Q;
    public AnonymousClass1BC A0R;
    public C251118d A0S;
    public AnonymousClass11I A0T;
    public AnonymousClass3Dn A0U;
    public C22330yu A0V;
    public AnonymousClass1lO A0W;
    public AnonymousClass37S A0X;
    public AnonymousClass2Dx A0Y;
    public AnonymousClass2Ew A0Z;
    public AnonymousClass3DR A0a;
    public AnonymousClass10S A0b;
    public C22700zV A0c;
    public C15610nY A0d;
    public C248917h A0e;
    public AnonymousClass1J1 A0f;
    public C21270x9 A0g;
    public AnonymousClass131 A0h;
    public C20730wE A0i;
    public C20020v5 A0j;
    public C16590pI A0k;
    public C17170qN A0l;
    public C16370ot A0m;
    public C20830wO A0n;
    public C21320xE A0o;
    public AnonymousClass12H A0p;
    public C242114q A0q;
    public C20510vs A0r;
    public C15370n3 A0s;
    public C22100yW A0t;
    public C245015t A0u;
    public C22050yP A0v;
    public C16120oU A0w;
    public C51442Ut A0x;
    public C244215l A0y;
    public AnonymousClass11G A0z;
    public C16030oK A10;
    public C244415n A11;
    public C17220qS A12;
    public C19840ul A13;
    public C22280yp A14;
    public AnonymousClass1AT A15;
    public C63393Bk A16;
    public C20750wG A17;
    public AnonymousClass17R A18;
    public AnonymousClass12F A19;
    public AnonymousClass19H A1A;
    public AnonymousClass12U A1B;
    public C92434Vw A1C;
    public StatusesViewModel A1D;
    public MediaCard A1E;
    public AnonymousClass198 A1F;
    public C254219i A1G;
    public AnonymousClass3D0 A1H;
    public AnonymousClass19Z A1I;
    public AnonymousClass18W A1J;
    public AnonymousClass18X A1K;
    public CharSequence A1L;
    public Integer A1M;
    public boolean A1N;
    public boolean A1O;
    public boolean A1P;
    public boolean A1Q;
    public final Handler A1R;
    public final CompoundButton.OnCheckedChangeListener A1S;
    public final AnonymousClass2Cu A1T;
    public final AnonymousClass2Dn A1U;
    public final C27131Gd A1V;
    public final C27151Gf A1W;
    public final AbstractC18860tB A1X;
    public final AbstractC33331dp A1Y;
    public final AnonymousClass13Q A1Z;
    public final AnonymousClass13P A1a;
    public final AbstractView$OnClickListenerC34281fs A1b;
    public final Runnable A1c;

    public ContactInfoActivity() {
        this(0);
        this.A1b = new ViewOnClickCListenerShape13S0100000_I0(this, 35);
        this.A1N = false;
        this.A1S = new CompoundButton.OnCheckedChangeListener() { // from class: X.3OK
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ContactInfoActivity contactInfoActivity = ContactInfoActivity.this;
                if (z) {
                    UserJid A2v = contactInfoActivity.A2v();
                    AnonymousClass009.A05(A2v);
                    C12960it.A16(MuteDialogFragment.A00(A2v), contactInfoActivity);
                    return;
                }
                ((ActivityC13830kP) contactInfoActivity).A05.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(contactInfoActivity, 36));
            }
        };
        this.A1W = new C32971d2(this);
        this.A1V = new C36531k2(this);
        this.A1U = new C60322we(this);
        this.A1Y = new AnonymousClass44V(this);
        this.A1T = new C84363zB(this);
        this.A1X = new C35391hk(this);
        this.A1Z = new C68993Xn(this);
        this.A1a = new C69023Xq(this);
        this.A1R = new Handler(Looper.getMainLooper());
        this.A1c = new RunnableBRunnable0Shape3S0100000_I0_3(this, 39);
    }

    public ContactInfoActivity(int i) {
        this.A1O = false;
        A0R(new C102904pt(this));
    }

    public static String A03(Context context, C22680zT r12, C14650lo r13, C15550nR r14, AnonymousClass131 r15, C16590pI r16, AnonymousClass018 r17, C15370n3 r18, UserJid userJid, String str) {
        C30721Yo r5 = new C30721Yo(r13, r14, r16, r17);
        r5.A08.A01 = str;
        r5.A0A(userJid, C248917h.A01(r18), "WORK", 2, true);
        Bitmap A00 = r15.A00(context, r18, 0.0f, 96);
        if (A00 != null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(4096);
                if (A00.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream)) {
                    r5.A09 = byteArrayOutputStream.toByteArray();
                }
                byteArrayOutputStream.close();
            } catch (IOException e) {
                Log.e(e);
            }
        }
        try {
            return new C41421tV(r12, r17).A01(r5);
        } catch (C41341tN e2) {
            Log.e(new C41351tO(e2));
            return null;
        }
    }

    @Override // X.AbstractActivityC33011d8, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A1O) {
            this.A1O = true;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r3.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r3.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC33001d7) this).A0L = (AnonymousClass14X) r1.AFM.get();
            ((AbstractActivityC33001d7) this).A09 = (C19990v2) r1.A3M.get();
            ((AbstractActivityC33001d7) this).A01 = (C16170oZ) r1.AM4.get();
            ((AbstractActivityC33001d7) this).A0A = (C15650ng) r1.A4m.get();
            super.A0P = (AnonymousClass19O) r1.ACO.get();
            ((AbstractActivityC33001d7) this).A06 = (C15550nR) r1.A45.get();
            ((AbstractActivityC33001d7) this).A02 = (C19850um) r1.A2v.get();
            ((AbstractActivityC33001d7) this).A08 = (AnonymousClass018) r1.ANb.get();
            ((AbstractActivityC33001d7) this).A0K = (C17070qD) r1.AFC.get();
            ((AbstractActivityC33001d7) this).A04 = (C243915i) r1.A37.get();
            ((AbstractActivityC33001d7) this).A0H = (C20710wC) r1.A8m.get();
            ((AbstractActivityC33001d7) this).A0D = (C20000v3) r1.AAG.get();
            ((AbstractActivityC33001d7) this).A0E = (C20050v8) r1.AAV.get();
            ((AbstractActivityC33001d7) this).A0F = (C15660nh) r1.ABE.get();
            ((AbstractActivityC33001d7) this).A0N = (C15860o1) r1.A3H.get();
            ((AbstractActivityC33001d7) this).A0I = (C17900ra) r1.AF5.get();
            ((AbstractActivityC33001d7) this).A03 = (AnonymousClass19Q) r1.A2u.get();
            ((AbstractActivityC33001d7) this).A07 = (C15890o4) r1.AN1.get();
            ((AbstractActivityC33001d7) this).A0B = (AnonymousClass1BB) r1.A68.get();
            ((AbstractActivityC33001d7) this).A0J = (C22710zW) r1.AF7.get();
            ((AbstractActivityC33001d7) this).A0O = (C255719x) r1.A5X.get();
            ((AbstractActivityC33001d7) this).A0C = (C15600nX) r1.A8x.get();
            ((AbstractActivityC33001d7) this).A0G = (AnonymousClass150) r1.A63.get();
            this.A1J = (AnonymousClass18W) r1.AIp.get();
            this.A0F = (C237913a) r1.ACt.get();
            this.A0k = (C16590pI) r1.AMg.get();
            this.A1A = (AnonymousClass19H) r1.AJN.get();
            this.A1B = (AnonymousClass12U) r1.AJd.get();
            this.A0w = (C16120oU) r1.ANE.get();
            this.A1I = (AnonymousClass19Z) r1.A2o.get();
            this.A0E = (AnonymousClass18U) r1.AAU.get();
            this.A0G = (C20670w8) r1.AMw.get();
            this.A13 = (C19840ul) r1.A1Q.get();
            this.A0g = (C21270x9) r1.A4A.get();
            this.A11 = (C244415n) r1.AAg.get();
            this.A18 = (AnonymousClass17R) r1.AIz.get();
            this.A12 = (C17220qS) r1.ABt.get();
            this.A09 = (C253519b) r1.A4C.get();
            this.A0R = (AnonymousClass1BC) r1.AJA.get();
            this.A0d = (C15610nY) r1.AMe.get();
            this.A14 = (C22280yp) r1.AFw.get();
            this.A1K = (AnonymousClass18X) r1.AIo.get();
            this.A0Q = (C238013b) r1.A1Z.get();
            this.A0b = (AnonymousClass10S) r1.A46.get();
            this.A0A = (C22680zT) r1.AGW.get();
            this.A0p = (AnonymousClass12H) r1.AC5.get();
            this.A19 = (AnonymousClass12F) r1.AJM.get();
            this.A1F = (AnonymousClass198) r1.A0J.get();
            this.A0j = (C20020v5) r1.A3B.get();
            this.A0v = (C22050yP) r1.A7v.get();
            C14900mE r5 = (C14900mE) r1.A8X.get();
            C16590pI r8 = (C16590pI) r1.AMg.get();
            C18790t3 r7 = (C18790t3) r1.AJw.get();
            this.A16 = new C63393Bk(r5, (C15450nH) r1.AII.get(), r7, r8, (C14850m9) r1.A04.get(), (C16120oU) r1.ANE.get(), r1.A3O(), (C22600zL) r1.AHm.get());
            this.A0r = (C20510vs) r1.AMJ.get();
            this.A1G = (C254219i) r1.A0K.get();
            this.A0m = (C16370ot) r1.A2b.get();
            this.A0V = (C22330yu) r1.A3I.get();
            this.A0i = (C20730wE) r1.A4J.get();
            this.A0z = (AnonymousClass11G) r1.AKq.get();
            this.A17 = (C20750wG) r1.AGL.get();
            this.A0q = (C242114q) r1.AJq.get();
            this.A0c = (C22700zV) r1.AMN.get();
            this.A0N = (C246716k) r1.A2X.get();
            this.A0K = (C254719n) r1.A2U.get();
            this.A0o = (C21320xE) r1.A4Y.get();
            this.A0u = (C245015t) r1.A5h.get();
            this.A0e = (C248917h) r1.AMf.get();
            this.A0L = (C14650lo) r1.A2V.get();
            this.A0t = (C22100yW) r1.A3g.get();
            this.A10 = (C16030oK) r1.AAd.get();
            this.A0S = (C251118d) r1.A2D.get();
            this.A0M = (AnonymousClass10A) r1.A2W.get();
            this.A0h = (AnonymousClass131) r1.A49.get();
            this.A0n = (C20830wO) r1.A4W.get();
            this.A0y = (C244215l) r1.A8y.get();
            this.A15 = (AnonymousClass1AT) r1.AG2.get();
            this.A0B = (C48962Ip) r3.A00.get();
            this.A0l = (C17170qN) r1.AMt.get();
            this.A0T = (AnonymousClass11I) r1.A2F.get();
            this.A0O = (C25781Au) r1.A2S.get();
            this.A0C = (C48972Iq) r3.A0Y.get();
            this.A0U = new AnonymousClass3Dn((C15570nT) r1.AAr.get(), (C251118d) r1.A2D.get());
            this.A0D = (C48982Ir) r3.A0Z.get();
        }
    }

    @Override // X.AbstractActivityC33001d7
    public void A2t(ArrayList arrayList) {
        super.A2t(arrayList);
    }

    public final long A2u() {
        long j = this.A0s.A0B;
        if (j == 0) {
            return 60000;
        }
        long currentTimeMillis = System.currentTimeMillis() - ((ActivityC13790kL) this).A05.A02(j);
        if (currentTimeMillis < 60000) {
            return 500;
        }
        return currentTimeMillis < 3600000 ? 5000 : 20000;
    }

    public UserJid A2v() {
        Jid A0B = this.A0s.A0B(UserJid.class);
        AnonymousClass009.A05(A0B);
        return (UserJid) A0B;
    }

    public void A2w() {
        C42641vY r0;
        String str;
        Cursor query;
        A37(12);
        UserJid A2v = A2v();
        C15370n3 r12 = this.A0s;
        C14900mE r4 = ((ActivityC13810kN) this).A05;
        C16590pI r10 = this.A0k;
        C15550nR r8 = ((AbstractActivityC33001d7) this).A06;
        AnonymousClass018 r11 = ((AbstractActivityC33001d7) this).A08;
        ContentResolver contentResolver = getContentResolver();
        C22680zT r6 = this.A0A;
        C14650lo r7 = this.A0L;
        AnonymousClass131 r9 = this.A0h;
        if (r12.A0L()) {
            str = A03(this, r6, r7, r8, r9, r10, r11, r12, A2v, C15610nY.A02(r12, false));
            if (str == null) {
                r4.A07(R.string.unable_to_share_contact, 0);
                return;
            }
        } else if (r12.A0C != null) {
            Uri A05 = r8.A05(contentResolver, r12);
            Uri uri = null;
            if (A05 != null && (query = contentResolver.query(A05, null, null, null, null)) != null) {
                try {
                    if (query.moveToNext()) {
                        uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, query.getString(query.getColumnIndexOrThrow("lookup")));
                    }
                    query.close();
                    if (uri != null) {
                        r0 = new C42641vY(this);
                        r0.A0P = "text/x-vcard";
                        r0.A00 = uri;
                        startActivity(r0.A00());
                    }
                    return;
                } catch (Throwable th) {
                    try {
                        query.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                return;
            }
        } else if (!r12.A0J() || (str = A03(this, r6, r7, r8, r9, r10, r11, r12, A2v, r12.A0D())) == null) {
            return;
        }
        r0 = new C42641vY(this);
        r0.A0P = "text/x-vcard";
        r0.A0O = str;
        startActivity(r0.A00());
    }

    @Deprecated
    public final void A2x() {
        if (this.A1N) {
            return;
        }
        if ((A3B() && ((ActivityC13810kN) this).A0C.A07(638)) || this.A1N) {
            AnonymousClass37S r1 = this.A0X;
            if (r1 != null) {
                r1.A03(true);
            }
            AnonymousClass37S r2 = new AnonymousClass37S(this, ((ActivityC13810kN) this).A0A, A2v(), ((AbstractActivityC33001d7) this).A0N);
            this.A0X = r2;
            ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
        }
    }

    public final void A2y() {
        int i;
        C15370n3 r2 = this.A0s;
        int i2 = 0;
        if (!r2.A0X && !C41861uH.A00(((ActivityC13810kN) this).A0C, r2.A0D)) {
            ((ActivityC13810kN) this).A05.A07(R.string.no_profile_photo, 0);
            this.A17.A01(A2v(), null, this.A0s.A05, 2);
        } else if (!super.A0Q) {
            if (C28391Mz.A02()) {
                i = getWindow().getStatusBarColor();
            } else {
                i = 0;
            }
            if (C28391Mz.A04()) {
                i2 = getWindow().getNavigationBarColor();
            }
            Intent A0N = C14960mK.A0N(this, A2v(), null, 0.0f, i, 0, i2, 0, true);
            boolean z = this.A1N;
            AnonymousClass2Ew r1 = this.A0Z;
            int i3 = R.id.profile_picture_image;
            if (z) {
                i3 = R.id.wds_profile_picture;
            }
            startActivity(A0N, AbstractC454421p.A05(this, AnonymousClass028.A0D(r1, i3), this.A0I.A00(R.string.transition_photo)));
        }
    }

    public final void A2z() {
        try {
            startActivityForResult(this.A1G.A00(this.A0s, A2v(), false), 10);
            this.A1F.A02(false, 2);
        } catch (ActivityNotFoundException unused) {
            ((ActivityC13810kN) this).A05.A07(R.string.group_add_contact_failed, 0);
        }
    }

    public final void A30() {
        int i;
        int A00;
        String string;
        int i2;
        if (this.A0s == null) {
            return;
        }
        if (!(AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.block_contact_btn) instanceof ListItemWithLeftIcon)) {
            TextView textView = (TextView) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.block_contact_text);
            ImageView imageView = (ImageView) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.block_contact_icon);
            if (this.A0Q.A0I(A2v())) {
                int A002 = AnonymousClass00T.A00(this, R.color.contactInfoFooterUnblock);
                imageView.setColorFilter(A002);
                textView.setTextColor(A002);
                i = R.string.unblock;
            } else {
                int A003 = AnonymousClass00T.A00(this, R.color.red_button_text);
                imageView.setColorFilter(A003);
                textView.setTextColor(A003);
                i = R.string.block;
            }
            textView.setText(i);
        } else if (this.A0s != null) {
            ListItemWithLeftIcon listItemWithLeftIcon = (ListItemWithLeftIcon) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.block_contact_btn);
            if (this.A0Q.A0I(A2v())) {
                A00 = AnonymousClass00T.A00(this, R.color.contactInfoFooterUnblockV2);
                if (C41861uH.A00(((ActivityC13810kN) this).A0C, A2v())) {
                    i2 = R.string.wac_cannot_send_to_blocked_contact;
                    string = getString(i2, "WhatsApp");
                    listItemWithLeftIcon.setIconColor(A00);
                    listItemWithLeftIcon.setTitleTextColor(A00);
                    listItemWithLeftIcon.setTitle(string);
                }
                if (A3B()) {
                    string = getString(R.string.unblock);
                } else {
                    string = getString(R.string.unblock_button_text_on_contact_info, this.A0d.A04(this.A0s));
                }
                listItemWithLeftIcon.setIconColor(A00);
                listItemWithLeftIcon.setTitleTextColor(A00);
                listItemWithLeftIcon.setTitle(string);
            }
            A00 = AnonymousClass00T.A00(this, R.color.red_button_text);
            if (C41861uH.A00(((ActivityC13810kN) this).A0C, A2v())) {
                i2 = R.string.wac_block_text;
                string = getString(i2, "WhatsApp");
                listItemWithLeftIcon.setIconColor(A00);
                listItemWithLeftIcon.setTitleTextColor(A00);
                listItemWithLeftIcon.setTitle(string);
            }
            if (A3B()) {
                string = getString(R.string.block);
            } else {
                string = getString(R.string.block_button_text_on_contact_info, this.A0d.A04(this.A0s));
            }
            listItemWithLeftIcon.setIconColor(A00);
            listItemWithLeftIcon.setTitleTextColor(A00);
            listItemWithLeftIcon.setTitle(string);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004f, code lost:
        if (r30.A0s.A0L() == false) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A31() {
        /*
        // Method dump skipped, instructions count: 1198
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.A31():void");
    }

    public final void A32() {
        if (this.A0s != null) {
            findViewById(R.id.actions_card).setVisibility(0);
            A2q(AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.mute_layout), this.A1S, ((AbstractActivityC33001d7) this).A0N.A08(A2v().getRawString()));
        }
    }

    public final void A33() {
        AbstractC58392on r4 = (AbstractC58392on) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.ephemeral_msg_view);
        if (!this.A0c.A02(A2v())) {
            int A01 = ((AbstractActivityC33001d7) this).A09.A01(A2v());
            String A03 = C32341c0.A03(this, A01, false, false);
            r4.setIcon(R.drawable.ic_group_ephemeral_v2);
            r4.setVisibility(0);
            r4.setOnClickListener(new ViewOnClickCListenerShape0S0101000_I0(this, A01, 3));
            r4.setDescription(A03);
            A2n(((AbstractActivityC33001d7) this).A0N.A08(A2g().getRawString()).A00);
            return;
        }
        r4.setVisibility(8);
    }

    public final void A34() {
        View A05 = AnonymousClass00T.A05(this, R.id.live_location_card);
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.live_location_info);
        boolean A0f = this.A10.A0f(A2v());
        int size = this.A10.A09(A2v()).size();
        if (size != 0 || A0f) {
            A05.setVisibility(0);
            if (!A0f || size != 0) {
                String A0F = ((AbstractActivityC33001d7) this).A08.A0F(this.A0d.A04(this.A0s));
                int i = R.string.contact_info_live_location_description_friend_is_sharing;
                if (A0f) {
                    i = R.string.contact_info_live_location_description_you_and_friend_are_sharing;
                }
                textView.setText(getString(i, A0F));
                return;
            }
            textView.setText(R.string.contact_info_live_location_description_you_are_sharing);
            return;
        }
        A05.setVisibility(8);
    }

    public final void A35() {
        if (this.A0s != null) {
            C33181da A08 = ((AbstractActivityC33001d7) this).A0N.A08(A2v().getRawString());
            View findViewById = findViewById(R.id.notifications_info);
            int i = 0;
            if (findViewById != null) {
                if (!A08.A0I) {
                    i = 8;
                }
                findViewById.setVisibility(i);
                return;
            }
            ListItemWithLeftIcon listItemWithLeftIcon = (ListItemWithLeftIcon) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.notifications_layout);
            if (!A08.A0I) {
                i = 8;
            }
            listItemWithLeftIcon.setDescriptionVisibility(i);
        }
    }

    public final void A36() {
        TextView textView = (TextView) findViewById(R.id.status_info);
        long j = this.A0s.A0B;
        if (j != 0) {
            try {
                CharSequence relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(((ActivityC13790kL) this).A05.A02(j), System.currentTimeMillis(), 0, 0);
                if (!TextUtils.equals(this.A1L, relativeTimeSpanString)) {
                    this.A1L = relativeTimeSpanString;
                    textView.setText(relativeTimeSpanString);
                }
            } catch (UnknownFormatConversionException e) {
                Log.e(e);
                textView.setText("");
            }
        } else {
            textView.setText("");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if (r1.A08() == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r1.A09() == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A37(int r10) {
        /*
            r9 = this;
            X.0n3 r0 = r9.A0s
            boolean r0 = r0.A0J()
            if (r0 == 0) goto L_0x0039
            X.0m9 r1 = r9.A0C
            r0 = 543(0x21f, float:7.61E-43)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0039
            X.1Au r2 = r9.A0O
            com.whatsapp.jid.UserJid r0 = r9.A2v()
            java.lang.String r5 = X.C15380n4.A03(r0)
            r3 = 0
            java.lang.Integer r4 = r9.A1M
            X.2VA r1 = r9.A0J
            if (r1 == 0) goto L_0x002a
            boolean r0 = r1.A09()
            r7 = 1
            if (r0 != 0) goto L_0x002b
        L_0x002a:
            r7 = 0
        L_0x002b:
            if (r1 == 0) goto L_0x0034
            boolean r0 = r1.A08()
            r8 = 1
            if (r0 != 0) goto L_0x0035
        L_0x0034:
            r8 = 0
        L_0x0035:
            r6 = r10
            r2.A05(r3, r4, r5, r6, r7, r8)
        L_0x0039:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.A37(int):void");
    }

    public final void A38(int i) {
        if (this.A0s.A0J()) {
            this.A0O.A01(this.A0P, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003a, code lost:
        if (((X.ActivityC13810kN) r14).A0C.A07(1370) == false) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A39(java.util.List r15) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.A39(java.util.List):void");
    }

    public final void A3A(boolean z, boolean z2) {
        C20830wO r3 = this.A0n;
        UserJid nullable = UserJid.getNullable(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(nullable);
        C15370n3 A01 = r3.A01(nullable);
        AnonymousClass009.A05(A01);
        this.A0s = A01;
        boolean A0J = A01.A0J();
        View view = this.A00;
        if (A0J) {
            if (view == null) {
                ViewStub viewStub = (ViewStub) this.A04.findViewById(R.id.business_details_card_stub);
                viewStub.setLayoutResource(R.layout.business_details_card_v3);
                View inflate = viewStub.inflate();
                this.A00 = inflate;
                C14830m7 r0 = ((ActivityC13790kL) this).A05;
                C14850m9 r02 = ((ActivityC13810kN) this).A0C;
                AnonymousClass18W r03 = this.A1J;
                C14900mE r04 = ((ActivityC13810kN) this).A05;
                C15570nT r05 = ((ActivityC13790kL) this).A01;
                C16590pI r06 = this.A0k;
                AnonymousClass12U r07 = this.A1B;
                C16120oU r08 = this.A0w;
                AnonymousClass19Z r09 = this.A1I;
                AnonymousClass18U r010 = this.A0E;
                AnonymousClass12P r011 = ((ActivityC13790kL) this).A00;
                C244415n r012 = this.A11;
                C17220qS r013 = this.A12;
                AnonymousClass01d r014 = ((ActivityC13810kN) this).A08;
                C15610nY r015 = this.A0d;
                AnonymousClass018 r016 = ((AbstractActivityC33001d7) this).A08;
                AnonymousClass2Ew r017 = this.A0Z;
                AnonymousClass18X r018 = this.A1K;
                C63393Bk r15 = this.A16;
                C22700zV r14 = this.A0c;
                C246716k r13 = this.A0N;
                C254719n r12 = this.A0K;
                C14650lo r11 = this.A0L;
                AnonymousClass19Q r10 = ((AbstractActivityC33001d7) this).A03;
                C17170qN r9 = this.A0l;
                this.A0J = new AnonymousClass2VA(inflate, r011, r04, r010, r05, r12, r11, r13, r10, this.A0O, this.A0P, this.A0U, this, r017, r14, r015, r014, r0, r06, r9, r016, this.A0s, r02, r08, r012, r013, r15, r07, r09, r03, r018, this.A1M);
                this.A1E.setTopShadowVisibility(0);
            }
            AnonymousClass2VA r6 = this.A0J;
            if (r6 != null) {
                boolean z3 = this.A1P;
                C15370n3 r8 = this.A0s;
                AbstractView$OnClickListenerC34281fs r92 = this.A1b;
                if (C41861uH.A00(r6.A0h, r6.A03())) {
                    C16590pI r1 = r6.A0e;
                    ArrayList arrayList = new ArrayList();
                    arrayList.add("blog.whatsapp.com");
                    C30151Wh r4 = new C30151Wh();
                    r4.A04 = AnonymousClass1VZ.A00;
                    r4.A09 = r1.A00.getResources().getString(R.string.wac_profile_business_description, "WhatsApp");
                    List list = r4.A0F;
                    list.clear();
                    list.addAll(arrayList);
                    r6.A0H = r4.A00();
                    r6.A05(r6.A0G, r8, r92, false, false, false);
                    return;
                }
                r6.A0T.A03(new AbstractC30111Wd(r8, r92, z, z2, z3) { // from class: X.53R
                    public final /* synthetic */ C15370n3 A01;
                    public final /* synthetic */ AbstractView$OnClickListenerC34281fs A02;
                    public final /* synthetic */ boolean A03;
                    public final /* synthetic */ boolean A04;
                    public final /* synthetic */ boolean A05;

                    {
                        this.A03 = r4;
                        this.A04 = r5;
                        this.A05 = r6;
                        this.A01 = r2;
                        this.A02 = r3;
                    }

                    @Override // X.AbstractC30111Wd
                    public final void ANP(C30141Wg r82) {
                        AnonymousClass2VA r019 = AnonymousClass2VA.this;
                        boolean z4 = this.A03;
                        boolean z5 = this.A04;
                        boolean z6 = this.A05;
                        C15370n3 r2 = this.A01;
                        AbstractView$OnClickListenerC34281fs r32 = this.A02;
                        r019.A0H = r82;
                        r019.A05(r019.A0G, r2, r32, z4, z5, z6);
                    }
                }, r6.A03());
            }
        } else if (view != null) {
            view.setVisibility(8);
            this.A1E.setTopShadowVisibility(8);
            findViewById(R.id.header_bottom_shadow).setVisibility(8);
        }
    }

    public boolean A3B() {
        C15370n3 r0 = this.A0s;
        return r0 != null && r0.A0J();
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC13980ke
    public void AMQ() {
        AnonymousClass3DR r0 = this.A0a;
        if (r0 != null) {
            r0.A02.A01(false);
        }
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r2) {
        StatusesViewModel statusesViewModel = this.A1D;
        if (statusesViewModel != null) {
            statusesViewModel.A09(r2);
        }
    }

    @Override // X.AbstractC13980ke
    public void AYC() {
        AnonymousClass3DR r0 = this.A0a;
        if (r0 != null) {
            r0.A02.A01(true);
        }
    }

    @Override // X.AbstractActivityC33001d7, android.app.Activity
    public void finishAfterTransition() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A03.setTransitionName(null);
            TransitionSet transitionSet = new TransitionSet();
            Slide slide = new Slide(48);
            slide.addTarget(this.A03);
            transitionSet.addTransition(slide);
            Slide slide2 = new Slide(80);
            slide2.addTarget(this.A08);
            transitionSet.addTransition(slide2);
            getWindow().setReturnTransition(transitionSet);
            this.A0Z.setStatusData(null);
        }
        super.finishAfterTransition();
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10) {
            this.A0i.A06();
            this.A1F.A00();
        } else if (i == 12) {
            A35();
        } else if (i == 100 && i2 == -1) {
            A3A(false, false);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A38(13);
        super.onBackPressed();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x017d, code lost:
        if (r10 != false) goto L_0x017f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x03bb  */
    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r19) {
        /*
        // Method dump skipped, instructions count: 1028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        String str;
        C15370n3 r0 = this.A0s;
        if (r0 != null && !C41861uH.A00(((ActivityC13810kN) this).A0C, r0.A0D)) {
            C15370n3 r1 = this.A0s;
            if (r1.A0C != null || r1.A0L()) {
                menu.add(0, 7, 0, R.string.share_contact);
            }
            ((ActivityC13790kL) this).A01.A08();
            int i = 3;
            C28811Pc r12 = this.A0s.A0C;
            int i2 = R.string.add_contact;
            if (r12 != null) {
                menu.add(0, 6, 0, R.string.edit_contact_in_address_book);
                i = 1;
                i2 = R.string.view_contact_in_address_book;
            }
            menu.add(0, i, 0, i2);
            if (this.A0U.A01(this.A0s)) {
                menu.add(0, 15, 0, R.string.biz_dir_see_more_like_this);
            }
            menu.add(0, 5, 0, R.string.verify_identity);
            Jid A0B = this.A0s.A0B(UserJid.class);
            if (A0B != null && (str = A0B.user) != null && str.startsWith("91") && this.A0s.A0J()) {
                menu.add(0, 10, 0, R.string.e_commerce_compliance_more_information);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x009b, code lost:
        if (r0.A09() == false) goto L_0x009d;
     */
    @Override // X.AbstractActivityC33001d7, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDestroy() {
        /*
        // Method dump skipped, instructions count: 289
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.onDestroy():void");
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onMenuOpened(int i, Menu menu) {
        if (this.A0U.A01(this.A0s)) {
            C25781Au r3 = this.A0O;
            boolean z = false;
            if (this.A0s.A0C != null) {
                z = true;
            }
            r3.A02(this.A0P, 16, z);
        }
        return super.onMenuOpened(i, menu);
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00fa  */
    @Override // X.ActivityC13810kN, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onOptionsItemSelected(android.view.MenuItem r7) {
        /*
        // Method dump skipped, instructions count: 264
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ContactInfoActivity.onOptionsItemSelected(android.view.MenuItem):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A14.A04(A2v());
        AnonymousClass2VA r0 = this.A0J;
        if (r0 != null && r0.A03() != null) {
            AnonymousClass2VA r2 = this.A0J;
            r2.A07(r2.A03(), true);
        }
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("groups_in_common_list_expanded", this.A0W.A03);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
    }

    public void populatePhoneNumber(View view) {
        View findViewById = view.findViewById(R.id.phone_container);
        if (findViewById != null) {
            if (findViewById.getVisibility() != 0) {
                findViewById.setVisibility(0);
            }
            TextView textView = (TextView) findViewById.findViewById(R.id.title_tv);
            View findViewById2 = findViewById.findViewById(R.id.primary_action_btn);
            TextView textView2 = (TextView) findViewById.findViewById(R.id.subtitle_tv);
            View findViewById3 = findViewById.findViewById(R.id.primary_action_icon);
            View findViewById4 = findViewById.findViewById(R.id.secondary_action_btn);
            View findViewById5 = findViewById.findViewById(R.id.third_action_btn);
            C42941w9.A03(textView);
            String A01 = C248917h.A01(this.A0s);
            textView.setText(A01);
            CharSequence A00 = C15610nY.A00(this, ((AbstractActivityC33001d7) this).A08, this.A0s);
            textView2.setText(A00);
            boolean isEmpty = TextUtils.isEmpty(A00);
            int i = 0;
            if (isEmpty) {
                i = 8;
            }
            textView2.setVisibility(i);
            ViewOnClickCListenerShape1S0100000_I0_1 viewOnClickCListenerShape1S0100000_I0_1 = new ViewOnClickCListenerShape1S0100000_I0_1(this, 10);
            findViewById3.setOnClickListener(viewOnClickCListenerShape1S0100000_I0_1);
            findViewById2.setOnClickListener(viewOnClickCListenerShape1S0100000_I0_1);
            findViewById2.setOnLongClickListener(new AnonymousClass2E2(((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A08, A01));
            if (!this.A0s.A0J() || !this.A0c.A02(A2v())) {
                findViewById4.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 14));
                findViewById5.setVisibility(0);
                findViewById5.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 6));
                return;
            }
            findViewById4.setVisibility(8);
            findViewById5.setVisibility(8);
        }
    }
}
