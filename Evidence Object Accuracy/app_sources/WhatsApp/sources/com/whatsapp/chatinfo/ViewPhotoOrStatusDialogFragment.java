package com.whatsapp.chatinfo;

import X.AbstractC33031dA;
import X.AnonymousClass0OC;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ViewPhotoOrStatusDialogFragment extends Hilt_ViewPhotoOrStatusDialogFragment {
    public AbstractC33031dA A00;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A00 = null;
    }

    @Override // com.whatsapp.chatinfo.Hilt_ViewPhotoOrStatusDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC33031dA) {
            this.A00 = (AbstractC33031dA) context;
            return;
        }
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(context, A0h);
        throw new ClassCastException(C12960it.A0d(" must implement ViewPhotoOrStatusDialogClickListener", A0h));
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String[] stringArray = A02().getStringArray(R.array.profile_photo_actions);
        C004802e A0K = C12960it.A0K(this);
        IDxCListenerShape8S0100000_1_I1 iDxCListenerShape8S0100000_1_I1 = new IDxCListenerShape8S0100000_1_I1(this, 11);
        AnonymousClass0OC r0 = A0K.A01;
        r0.A0M = stringArray;
        r0.A05 = iDxCListenerShape8S0100000_1_I1;
        return A0K.create();
    }
}
