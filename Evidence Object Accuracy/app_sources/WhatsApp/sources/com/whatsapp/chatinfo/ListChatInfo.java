package com.whatsapp.chatinfo;

import X.AbstractActivityC33001d7;
import X.AbstractC116275Uu;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.AbstractC33331dp;
import X.AbstractC36671kL;
import X.AbstractC454421p;
import X.AbstractC58392on;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10S;
import X.AnonymousClass11C;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass14X;
import X.AnonymousClass150;
import X.AnonymousClass193;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass19Q;
import X.AnonymousClass1AT;
import X.AnonymousClass1BB;
import X.AnonymousClass1J1;
import X.AnonymousClass1YM;
import X.AnonymousClass1YO;
import X.AnonymousClass23N;
import X.AnonymousClass2Dn;
import X.AnonymousClass2Ew;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass2TT;
import X.AnonymousClass36H;
import X.AnonymousClass41R;
import X.AnonymousClass44W;
import X.AnonymousClass4RZ;
import X.AnonymousClass5U9;
import X.C004802e;
import X.C102204ol;
import X.C102914pu;
import X.C1114559l;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15810nw;
import X.C15860o1;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C17070qD;
import X.C17900ra;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19850um;
import X.C19990v2;
import X.C20000v3;
import X.C20050v8;
import X.C20710wC;
import X.C20730wE;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22230yk;
import X.C22330yu;
import X.C22410z2;
import X.C22670zS;
import X.C22710zW;
import X.C231510o;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C248917h;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253719d;
import X.C254219i;
import X.C255719x;
import X.C25621Ac;
import X.C27131Gd;
import X.C28421Nd;
import X.C29901Ve;
import X.C30461Xm;
import X.C35381hj;
import X.C36021jC;
import X.C36071jH;
import X.C36521k1;
import X.C36581k7;
import X.C38131nZ;
import X.C60342wg;
import X.C68753Wp;
import X.C70063ag;
import X.DialogC58332oe;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.group.view.custom.GroupDetailsCard;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class ListChatInfo extends AbstractActivityC33001d7 {
    public View A00;
    public ListView A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public C22330yu A05;
    public C36581k7 A06;
    public C60342wg A07;
    public AnonymousClass2Ew A08;
    public AnonymousClass10S A09;
    public C15610nY A0A;
    public AnonymousClass1J1 A0B;
    public C21270x9 A0C;
    public C20730wE A0D;
    public C21320xE A0E;
    public C25621Ac A0F;
    public AnonymousClass12H A0G;
    public C242114q A0H;
    public C15370n3 A0I;
    public C15370n3 A0J;
    public C231510o A0K;
    public AnonymousClass193 A0L;
    public C16120oU A0M;
    public C253719d A0N;
    public C244215l A0O;
    public GroupDetailsCard A0P;
    public C22410z2 A0Q;
    public C22230yk A0R;
    public C16630pM A0S;
    public AnonymousClass1AT A0T;
    public AnonymousClass12F A0U;
    public AnonymousClass12U A0V;
    public AnonymousClass198 A0W;
    public C254219i A0X;
    public boolean A0Y;
    public final AnonymousClass2Dn A0Z;
    public final C27131Gd A0a;
    public final AbstractC18860tB A0b;
    public final AbstractC33331dp A0c;
    public final ArrayList A0d;

    public ListChatInfo() {
        this(0);
        this.A0d = new ArrayList();
        this.A0a = new C36521k1(this);
        this.A0Z = new AnonymousClass41R(this);
        this.A0c = new AnonymousClass44W(this);
        this.A0b = new C35381hj(this);
    }

    public ListChatInfo(int i) {
        this.A0Y = false;
        A0R(new C102914pu(this));
    }

    public static /* synthetic */ void A02(ListChatInfo listChatInfo) {
        ArrayList arrayList = listChatInfo.A0d;
        arrayList.clear();
        HashSet hashSet = new HashSet(((AbstractActivityC33001d7) listChatInfo).A0C.A02(listChatInfo.A2u()).A06().A00);
        C15570nT r0 = ((ActivityC13790kL) listChatInfo).A01;
        r0.A08();
        hashSet.remove(r0.A05);
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            C15370n3 A0B = ((AbstractActivityC33001d7) listChatInfo).A06.A0B((AbstractC14640lm) it.next());
            if (!arrayList.contains(A0B)) {
                arrayList.add(A0B);
            }
        }
        listChatInfo.A2x();
        listChatInfo.A31();
    }

    @Override // X.AbstractActivityC33011d8, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0Y) {
            this.A0Y = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC33001d7) this).A0L = (AnonymousClass14X) r1.AFM.get();
            ((AbstractActivityC33001d7) this).A09 = (C19990v2) r1.A3M.get();
            ((AbstractActivityC33001d7) this).A01 = (C16170oZ) r1.AM4.get();
            ((AbstractActivityC33001d7) this).A0A = (C15650ng) r1.A4m.get();
            super.A0P = (AnonymousClass19O) r1.ACO.get();
            ((AbstractActivityC33001d7) this).A06 = (C15550nR) r1.A45.get();
            ((AbstractActivityC33001d7) this).A02 = (C19850um) r1.A2v.get();
            ((AbstractActivityC33001d7) this).A08 = (AnonymousClass018) r1.ANb.get();
            ((AbstractActivityC33001d7) this).A0K = (C17070qD) r1.AFC.get();
            ((AbstractActivityC33001d7) this).A04 = (C243915i) r1.A37.get();
            ((AbstractActivityC33001d7) this).A0H = (C20710wC) r1.A8m.get();
            ((AbstractActivityC33001d7) this).A0D = (C20000v3) r1.AAG.get();
            ((AbstractActivityC33001d7) this).A0E = (C20050v8) r1.AAV.get();
            ((AbstractActivityC33001d7) this).A0F = (C15660nh) r1.ABE.get();
            ((AbstractActivityC33001d7) this).A0N = (C15860o1) r1.A3H.get();
            ((AbstractActivityC33001d7) this).A0I = (C17900ra) r1.AF5.get();
            ((AbstractActivityC33001d7) this).A03 = (AnonymousClass19Q) r1.A2u.get();
            ((AbstractActivityC33001d7) this).A07 = (C15890o4) r1.AN1.get();
            ((AbstractActivityC33001d7) this).A0B = (AnonymousClass1BB) r1.A68.get();
            ((AbstractActivityC33001d7) this).A0J = (C22710zW) r1.AF7.get();
            ((AbstractActivityC33001d7) this).A0O = (C255719x) r1.A5X.get();
            ((AbstractActivityC33001d7) this).A0C = (C15600nX) r1.A8x.get();
            ((AbstractActivityC33001d7) this).A0G = (AnonymousClass150) r1.A63.get();
            this.A0N = (C253719d) r1.A8V.get();
            this.A0V = (AnonymousClass12U) r1.AJd.get();
            this.A0M = (C16120oU) r1.ANE.get();
            this.A0K = (C231510o) r1.AHO.get();
            this.A0C = (C21270x9) r1.A4A.get();
            this.A0F = (C25621Ac) r1.A8n.get();
            this.A0A = (C15610nY) r1.AMe.get();
            this.A0R = (C22230yk) r1.ANT.get();
            this.A09 = (AnonymousClass10S) r1.A46.get();
            this.A0G = (AnonymousClass12H) r1.AC5.get();
            this.A0U = (AnonymousClass12F) r1.AJM.get();
            this.A0W = (AnonymousClass198) r1.A0J.get();
            this.A0X = (C254219i) r1.A0K.get();
            this.A05 = (C22330yu) r1.A3I.get();
            this.A0D = (C20730wE) r1.A4J.get();
            this.A0L = (AnonymousClass193) r1.A6S.get();
            this.A0H = (C242114q) r1.AJq.get();
            this.A0E = (C21320xE) r1.A4Y.get();
            this.A0Q = (C22410z2) r1.ANH.get();
            this.A0S = (C16630pM) r1.AIc.get();
            this.A0O = (C244215l) r1.A8y.get();
            this.A0T = (AnonymousClass1AT) r1.AG2.get();
        }
    }

    @Override // X.AbstractActivityC33001d7
    public void A2o(long j) {
        super.A2o(j);
        View findViewById = findViewById(R.id.actions_card);
        int i = 0;
        if (j == 0) {
            i = 8;
        }
        findViewById.setVisibility(i);
        A2w();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if (r4.isEmpty() != false) goto L_0x0016;
     */
    @Override // X.AbstractActivityC33001d7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2t(java.util.ArrayList r4) {
        /*
            r3 = this;
            super.A2t(r4)
            r0 = 2131363847(0x7f0a0807, float:1.8347514E38)
            android.view.View r2 = r3.findViewById(r0)
            if (r2 == 0) goto L_0x001a
            if (r4 == 0) goto L_0x0016
            boolean r1 = r4.isEmpty()
            r0 = 8
            if (r1 == 0) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            r2.setVisibility(r0)
        L_0x001a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.chatinfo.ListChatInfo.A2t(java.util.ArrayList):void");
    }

    public C29901Ve A2u() {
        Jid A0B = this.A0I.A0B(C29901Ve.class);
        StringBuilder sb = new StringBuilder("jid is not broadcast jid: ");
        sb.append(this.A0I.A0B(C29901Ve.class));
        AnonymousClass009.A06(A0B, sb.toString());
        return (C29901Ve) A0B;
    }

    public final void A2v() {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.A0d.iterator();
        while (it.hasNext()) {
            arrayList.add(((C15370n3) it.next()).A0B(UserJid.class));
        }
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.conversation.EditBroadcastRecipientsSelector");
        intent.putExtra("selected", C15380n4.A06(arrayList));
        startActivityForResult(intent, 12);
    }

    public final void A2w() {
        View findViewById = ((ActivityC13810kN) this).A00.findViewById(R.id.starred_messages_separator);
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
        AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.participants_search).setVisibility(8);
        AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.mute_layout).setVisibility(8);
        AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.notifications_layout).setVisibility(8);
        View findViewById2 = ((ActivityC13810kN) this).A00.findViewById(R.id.notifications_separator);
        if (findViewById2 != null) {
            findViewById2.setVisibility(8);
        }
        AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.media_visibility_layout).setVisibility(8);
        View findViewById3 = ((ActivityC13810kN) this).A00.findViewById(R.id.media_visibility_separator);
        if (findViewById3 != null) {
            findViewById3.setVisibility(8);
        }
    }

    public final void A2x() {
        AbstractC58392on r2 = (AbstractC58392on) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.encryption_info_view);
        r2.setDescription(getString(R.string.group_info_encrypted));
        r2.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 36));
        r2.setVisibility(0);
    }

    public final void A2y() {
        int i;
        View childAt = this.A01.getChildAt(0);
        if (childAt == null) {
            return;
        }
        if (this.A01.getWidth() > this.A01.getHeight()) {
            if (this.A01.getFirstVisiblePosition() == 0) {
                i = childAt.getTop();
            } else {
                i = (-this.A00.getHeight()) + 1;
            }
            View view = this.A00;
            view.offsetTopAndBottom(i - view.getTop());
        } else if (this.A00.getTop() != 0) {
            View view2 = this.A00;
            view2.offsetTopAndBottom(-view2.getTop());
        }
    }

    public final void A2z() {
        TextView textView;
        long A01 = C28421Nd.A01(this.A0I.A0P, Long.MIN_VALUE);
        if (A01 != Long.MIN_VALUE || (textView = this.A02) == null) {
            String A0B = C38131nZ.A0B(((AbstractActivityC33001d7) this).A08, new Object[0], R.string.group_creation_time_today, R.string.group_creation_time_yesterday, R.string.group_creation_time, A01, true);
            GroupDetailsCard groupDetailsCard = this.A0P;
            AnonymousClass009.A03(groupDetailsCard);
            groupDetailsCard.setSecondSubtitleText(A0B);
        } else {
            textView.setVisibility(8);
        }
        C60342wg r2 = this.A07;
        if (r2 != null) {
            r2.A03(true);
        }
        A2l();
        A1g(true);
        C14900mE r3 = ((ActivityC13810kN) this).A05;
        C17070qD r12 = ((AbstractActivityC33001d7) this).A0K;
        C60342wg r22 = new C60342wg(r3, this, ((AbstractActivityC33001d7) this).A0B, ((AbstractActivityC33001d7) this).A0D, ((AbstractActivityC33001d7) this).A0E, ((AbstractActivityC33001d7) this).A0F, this.A0H, this.A0I, ((AbstractActivityC33001d7) this).A0J, r12);
        this.A07 = r22;
        ((ActivityC13830kP) this).A05.Aaz(r22, new Void[0]);
    }

    public final void A30() {
        String str;
        int i;
        if (TextUtils.isEmpty(this.A0I.A0K)) {
            str = getString(R.string.untitled_broadcast_list);
            i = R.color.ui_refresh_contact_info_subtitle;
        } else {
            str = this.A0I.A0K;
            i = R.color.ui_refresh_contact_info_title;
        }
        int A00 = AnonymousClass00T.A00(this, i);
        this.A08.setTitleText(str);
        GroupDetailsCard groupDetailsCard = this.A0P;
        AnonymousClass009.A03(groupDetailsCard);
        groupDetailsCard.setTitleText(str);
        this.A0P.setTitleColor(A00);
        GroupDetailsCard groupDetailsCard2 = this.A0P;
        Resources resources = getResources();
        ArrayList arrayList = this.A0d;
        groupDetailsCard2.setSubtitleText(resources.getQuantityString(R.plurals.broadcast_list_subtitle, arrayList.size(), Integer.valueOf(arrayList.size())));
    }

    public final void A31() {
        TextView textView = this.A04;
        Resources resources = getResources();
        ArrayList arrayList = this.A0d;
        textView.setText(resources.getQuantityString(R.plurals.recipients_title, arrayList.size(), Integer.valueOf(arrayList.size())));
        A32();
        Collections.sort(arrayList, new C36071jH(((ActivityC13790kL) this).A01, this.A0A, true));
        this.A06.notifyDataSetChanged();
        A30();
    }

    public final void A32() {
        int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1I);
        ArrayList arrayList = this.A0d;
        if (arrayList.size() <= (A02 * 9) / 10 || A02 == 0) {
            this.A03.setVisibility(8);
            return;
        }
        this.A03.setVisibility(0);
        this.A03.setText(getString(R.string.participants_count, Integer.valueOf(arrayList.size()), Integer.valueOf(A02)));
    }

    public final void A33(boolean z) {
        String str;
        boolean z2;
        C15370n3 r1 = this.A0J;
        if (r1 == null) {
            ((ActivityC13810kN) this).A05.A07(R.string.group_add_contact_failed, 0);
            return;
        }
        C254219i r3 = this.A0X;
        String A01 = C248917h.A01(r1);
        if (r1.A0J()) {
            str = r1.A0D();
            z2 = true;
        } else {
            str = null;
            z2 = false;
        }
        try {
            startActivityForResult(r3.A01(A01, str, z, z2), 10);
            this.A0W.A02(z, 9);
        } catch (ActivityNotFoundException unused) {
            C36021jC.A01(this, 4);
        }
    }

    @Override // X.AbstractActivityC33001d7, android.app.Activity
    public void finishAfterTransition() {
        if (AbstractC454421p.A00) {
            this.A00.setTransitionName(null);
            TransitionSet transitionSet = new TransitionSet();
            Slide slide = new Slide(48);
            slide.addTarget(this.A00);
            transitionSet.addTransition(slide);
            Slide slide2 = new Slide(80);
            slide2.addTarget(this.A01);
            transitionSet.addTransition(slide2);
            getWindow().setReturnTransition(transitionSet);
        }
        super.finishAfterTransition();
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        C30461Xm A06;
        super.onActivityResult(i, i2, intent);
        switch (i) {
            case 10:
            case 11:
                this.A0D.A06();
                this.A0W.A00();
                return;
            case 12:
                if (i2 == -1) {
                    List A07 = C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("contacts"));
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    HashSet hashSet = new HashSet();
                    ArrayList arrayList3 = this.A0d;
                    Iterator it = arrayList3.iterator();
                    while (it.hasNext()) {
                        hashSet.add(((C15370n3) it.next()).A0B(UserJid.class));
                    }
                    for (Object obj : A07) {
                        if (!hashSet.contains(obj)) {
                            arrayList.add(obj);
                        }
                    }
                    Iterator it2 = arrayList3.iterator();
                    while (it2.hasNext()) {
                        Jid A0B = ((C15370n3) it2.next()).A0B(UserJid.class);
                        if (!A07.contains(A0B)) {
                            arrayList2.add(A0B);
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        C20710wC r8 = ((AbstractActivityC33001d7) this).A0H;
                        C29901Ve A2u = A2u();
                        AnonymousClass009.A09("", arrayList);
                        AnonymousClass1YM A02 = r8.A0X.A02(A2u);
                        ArrayList arrayList4 = new ArrayList(arrayList.size());
                        Iterator it3 = arrayList.iterator();
                        while (it3.hasNext()) {
                            UserJid userJid = (UserJid) it3.next();
                            arrayList4.add(new AnonymousClass1YO(userJid, AnonymousClass1YM.A01(r8.A0b.A0D(userJid)), 0, false));
                        }
                        A02.A0F(arrayList4);
                        r8.A0D.A0J(A2u);
                        int size = arrayList.size();
                        AnonymousClass11C r3 = r8.A0d;
                        if (size == 1) {
                            A06 = r8.A0s.A08(A2u, (UserJid) arrayList.get(0), null, 4, r8.A0I.A00(), 0);
                        } else {
                            A06 = r8.A0s.A06(A02, A2u, null, null, arrayList, 12, r8.A0I.A00(), 0);
                        }
                        r3.A00(A06, 2);
                        Iterator it4 = arrayList.iterator();
                        while (it4.hasNext()) {
                            arrayList3.add(((AbstractActivityC33001d7) this).A06.A0B((AbstractC14640lm) it4.next()));
                        }
                    }
                    if (!arrayList2.isEmpty()) {
                        ((AbstractActivityC33001d7) this).A0H.A0M(A2u(), arrayList2);
                        Iterator it5 = arrayList2.iterator();
                        while (it5.hasNext()) {
                            arrayList3.remove(((AbstractActivityC33001d7) this).A06.A0B((AbstractC14640lm) it5.next()));
                        }
                    }
                    this.A0R.A03(A2u(), false);
                    A31();
                    return;
                }
                return;
            default:
                return;
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        Intent intent;
        C15370n3 r3 = ((AnonymousClass4RZ) ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).targetView.getTag()).A03;
        this.A0J = r3;
        int itemId = menuItem.getItemId();
        if (itemId != 0) {
            if (itemId == 1) {
                ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, r3));
                return true;
            } else if (itemId == 2) {
                A33(true);
                return true;
            } else if (itemId == 3) {
                A33(false);
                return true;
            } else if (itemId == 5) {
                C36021jC.A01(this, 6);
                return true;
            } else if (itemId != 6) {
                return false;
            } else {
                intent = C14960mK.A0P(this, (UserJid) this.A0J.A0B(UserJid.class));
            }
        } else if (r3.A0C == null) {
            return true;
        } else {
            intent = new C14960mK().A0h(this, r3, 7);
        }
        startActivity(intent);
        return true;
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        View findViewById;
        UserJid nullable;
        A1b(5);
        super.onCreate(bundle);
        this.A0B = this.A0C.A04(this, "list-chat-info");
        A0c();
        setTitle(R.string.list_info);
        setContentView(R.layout.groupchat_info);
        this.A08 = (AnonymousClass2Ew) findViewById(R.id.content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.A07();
        A1e(toolbar);
        A1U().A0M(true);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_back_shadow), ((AbstractActivityC33001d7) this).A08));
        this.A01 = A2e();
        View inflate = getLayoutInflater().inflate(R.layout.groupchat_info_header, (ViewGroup) this.A01, false);
        AnonymousClass028.A0a(inflate, 2);
        this.A01.addHeaderView(inflate, null, false);
        this.A00 = findViewById(R.id.header);
        this.A0P = (GroupDetailsCard) findViewById(R.id.group_details_card);
        this.A08.A05();
        this.A08.setColor(AnonymousClass00T.A00(this, R.color.primary));
        this.A08.A09(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material), getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * 1);
        View inflate2 = getLayoutInflater().inflate(R.layout.groupchat_info_footer, (ViewGroup) this.A01, false);
        this.A01.addFooterView(inflate2, null, false);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setVisibility(4);
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        linearLayout.setPadding(0, 0, 0, point.y);
        this.A01.addFooterView(linearLayout, null, false);
        C29901Ve A02 = C29901Ve.A02(getIntent().getStringExtra("gid"));
        if (A02 == null) {
            Log.e("list_chat_info/on_create: exiting due to null listChat jid object");
            finish();
            return;
        }
        this.A0I = ((AbstractActivityC33001d7) this).A06.A0B(A02);
        ArrayList arrayList = this.A0d;
        this.A06 = new C36581k7(this, this, arrayList);
        this.A00 = findViewById(R.id.header);
        this.A01.setOnScrollListener(new C102204ol(this));
        this.A01.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nc
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                ListChatInfo.this.A2y();
            }
        });
        this.A01.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4ox
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                ListChatInfo listChatInfo = ListChatInfo.this;
                C15370n3 r0 = ((AnonymousClass4RZ) view.getTag()).A03;
                if (r0 != null) {
                    listChatInfo.A0J = r0;
                    view.showContextMenu();
                }
            }
        });
        this.A0I.toString();
        View findViewById2 = findViewById(R.id.add_participant_layout);
        ((TextView) findViewById2.findViewById(R.id.add_participant_text)).setText(R.string.edit_broadcast_recipients);
        findViewById2.findViewById(R.id.invite_via_link_button).setVisibility(8);
        findViewById2.setVisibility(0);
        findViewById2.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 16));
        A2w();
        this.A02 = (TextView) findViewById(R.id.conversation_contact_status);
        C1114559l r2 = new AbstractC116275Uu() { // from class: X.59l
            @Override // X.AbstractC116275Uu
            public final void AO9() {
                ListChatInfo listChatInfo = ListChatInfo.this;
                listChatInfo.startActivity(C14960mK.A0C(listChatInfo, listChatInfo.A2u()));
            }
        };
        AnonymousClass36H r0 = (AnonymousClass36H) findViewById(R.id.media_card_view);
        r0.setSeeMoreClickListener(r2);
        r0.setTopShadowVisibility(8);
        this.A01.setAdapter((ListAdapter) this.A06);
        registerForContextMenu(this.A01);
        this.A0I.toString();
        TextView textView = (TextView) findViewById(R.id.participants_title);
        this.A04 = textView;
        textView.setText(getResources().getQuantityString(R.plurals.recipients_title, arrayList.size(), Integer.valueOf(arrayList.size())));
        this.A03 = (TextView) findViewById(R.id.participants_info);
        A32();
        A2r(Integer.valueOf((int) R.drawable.avatar_broadcast));
        A2s(getString(R.string.delete_list), R.drawable.ic_action_delete);
        AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.report_group_btn).setVisibility(8);
        View findViewById3 = findViewById(R.id.exit_group_btn);
        findViewById3.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 17));
        AnonymousClass23N.A01(findViewById3);
        HashSet hashSet = new HashSet(((AbstractActivityC33001d7) this).A0C.A02(A2u()).A06().A00);
        C15570nT r02 = ((ActivityC13790kL) this).A01;
        r02.A08();
        hashSet.remove(r02.A05);
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            C15370n3 A0B = ((AbstractActivityC33001d7) this).A06.A0B((AbstractC14640lm) it.next());
            if (!arrayList.contains(A0B)) {
                arrayList.add(A0B);
            }
        }
        A30();
        A2z();
        A31();
        A2x();
        findViewById(R.id.starred_messages_layout).setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 15));
        this.A09.A03(this.A0a);
        this.A0G.A03(this.A0b);
        this.A05.A03(this.A0Z);
        this.A0O.A03(this.A0c);
        if (!(bundle == null || (nullable = UserJid.getNullable(bundle.getString("selected_jid"))) == null)) {
            this.A0J = ((AbstractActivityC33001d7) this).A06.A0B(nullable);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass2TT r22 = new AnonymousClass2TT(this);
            if (getIntent().getBooleanExtra("circular_transition", false)) {
                findViewById = this.A00;
            } else {
                findViewById = findViewById(R.id.picture);
            }
            findViewById.setTransitionName(r22.A00(R.string.transition_photo));
        }
        this.A08.A0B(inflate, inflate2, linearLayout, this.A06);
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        C15370n3 r2 = ((AnonymousClass4RZ) ((AdapterView.AdapterContextMenuInfo) contextMenuInfo).targetView.getTag()).A03;
        if (r2 != null) {
            String A0A = this.A0A.A0A(r2, -1);
            contextMenu.add(0, 1, 0, getString(R.string.message_contact_name, A0A));
            if (r2.A0C == null) {
                contextMenu.add(0, 2, 0, R.string.add_contact);
                contextMenu.add(0, 3, 0, R.string.add_exist);
            } else {
                contextMenu.add(0, 0, 0, getString(R.string.view_contact_name, A0A));
            }
            if (this.A0d.size() > 2) {
                contextMenu.add(0, 5, 0, getString(R.string.remove_contact_name_from_list, A0A));
            }
            contextMenu.add(0, 6, 0, R.string.verify_identity);
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        String string;
        C15370n3 r3;
        if (i == 2) {
            if (TextUtils.isEmpty(this.A0A.A04(this.A0I))) {
                string = getString(R.string.delete_list_unnamed_dialog_title);
            } else {
                string = getString(R.string.delete_list_dialog_title, this.A0A.A04(this.A0I));
            }
            return ((AbstractActivityC33001d7) this).A0O.A01(this, new C70063ag(this), string, 1).create();
        } else if (i == 3) {
            C68753Wp r7 = new AnonymousClass5U9() { // from class: X.3Wp
                @Override // X.AnonymousClass5U9
                public final void AZw(String str) {
                    ListChatInfo listChatInfo = ListChatInfo.this;
                    if (!listChatInfo.A0A.A04(listChatInfo.A0I).equals(str)) {
                        C15370n3 r1 = listChatInfo.A0I;
                        r1.A0K = str;
                        ((AbstractActivityC33001d7) listChatInfo).A06.A0L(r1);
                        listChatInfo.A0F.A00(listChatInfo.A2u(), str);
                        listChatInfo.A30();
                        listChatInfo.A0E.A07(listChatInfo.A2u());
                        listChatInfo.A0Q.A02(listChatInfo.A0I);
                    }
                }
            };
            C14830m7 r0 = ((ActivityC13790kL) this).A05;
            C14850m9 r13 = ((ActivityC13810kN) this).A0C;
            C14900mE r12 = ((ActivityC13810kN) this).A05;
            C252718t r11 = ((ActivityC13790kL) this).A0D;
            AbstractC15710nm r10 = ((ActivityC13810kN) this).A03;
            AnonymousClass19M r9 = ((ActivityC13810kN) this).A0B;
            C231510o r6 = this.A0K;
            AnonymousClass01d r5 = ((ActivityC13810kN) this).A08;
            AnonymousClass018 r4 = ((AbstractActivityC33001d7) this).A08;
            AnonymousClass193 r32 = this.A0L;
            C14820m6 r2 = ((ActivityC13810kN) this).A09;
            C16630pM r1 = this.A0S;
            C15370n3 A09 = ((AbstractActivityC33001d7) this).A06.A09(A2u());
            AnonymousClass009.A05(A09);
            return new DialogC58332oe(this, r10, r12, r5, r0, r2, r4, r7, r9, r6, r32, r13, r1, r11, A09.A0K, 3, R.string.edit_list_name_dialog_title, ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A2A), 0, 0, 16385);
        } else if (i == 4) {
            Log.w("listchatinfo/add existing contact: activity not found, probably tablet");
            C004802e r22 = new C004802e(this);
            r22.A06(R.string.activity_not_found);
            r22.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fU
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(ListChatInfo.this, 4);
                }
            });
            return r22.create();
        } else if (i != 6 || (r3 = this.A0J) == null) {
            return super.onCreateDialog(i);
        } else {
            String string2 = getString(R.string.remove_recipient_dialog_title, this.A0A.A04(r3));
            C004802e r23 = new C004802e(this);
            r23.A0A(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, string2));
            r23.A0B(true);
            r23.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fT
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(ListChatInfo.this, 6);
                }
            });
            r23.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.3K8
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ListChatInfo listChatInfo = ListChatInfo.this;
                    C36021jC.A00(listChatInfo, 6);
                    C15370n3 r33 = listChatInfo.A0J;
                    ((AbstractActivityC33001d7) listChatInfo).A0H.A0M(listChatInfo.A2u(), Collections.singletonList(C15370n3.A03(r33, UserJid.class)));
                    listChatInfo.A0d.remove(r33);
                    listChatInfo.A0R.A03(listChatInfo.A2u(), false);
                    listChatInfo.A2x();
                    listChatInfo.A31();
                }
            });
            return r23.create();
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, R.string.add_broadcast_recipient).setIcon(R.drawable.ic_action_add_person_shadow).setShowAsAction(0);
        menu.add(0, 3, 0, R.string.edit_list_name_action).setShowAsAction(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0B.A00();
        this.A09.A04(this.A0a);
        this.A0G.A04(this.A0b);
        this.A05.A04(this.A0Z);
        this.A0O.A04(this.A0c);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId != 1) {
            if (itemId != 2) {
                if (itemId == 3) {
                    C36021jC.A01(this, 3);
                    return true;
                } else if (itemId != 16908332) {
                    return super.onOptionsItemSelected(menuItem);
                } else {
                    AnonymousClass00T.A08(this);
                }
            }
            return true;
        }
        A2v();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 1, A2u()));
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        C15370n3 r0 = this.A0J;
        if (r0 != null) {
            bundle.putString("selected_jid", C15380n4.A03(r0.A0D));
        }
    }
}
