package com.whatsapp.chatinfo.view.custom;

import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass14X;
import X.AnonymousClass19Z;
import X.AnonymousClass1A2;
import X.AnonymousClass1IZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C14900mE;
import X.C15370n3;
import X.C15570nT;
import X.C15610nY;
import X.C17070qD;
import X.C17930rd;
import X.C22710zW;
import X.C28801Pb;
import X.C51442Ut;
import X.C74503iB;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.contact.view.custom.ContactDetailsActionIcon;

/* loaded from: classes2.dex */
public class ContactDetailsCard extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public TextView A06;
    public TextView A07;
    public AnonymousClass12P A08;
    public AbstractC15710nm A09;
    public TextEmojiLabel A0A;
    public C15610nY A0B;
    public ContactDetailsActionIcon A0C;
    public ContactDetailsActionIcon A0D;
    public C15370n3 A0E;
    public C51442Ut A0F;
    public AnonymousClass1IZ A0G;
    public AnonymousClass1A2 A0H;
    public AnonymousClass14X A0I;
    public AnonymousClass12F A0J;
    public AnonymousClass19Z A0K;
    public AnonymousClass2P7 A0L;
    public boolean A0M;

    public ContactDetailsCard(Context context) {
        super(context);
        A00();
    }

    public ContactDetailsCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public ContactDetailsCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public ContactDetailsCard(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A09 = C12970iu.A0Q(A00);
            this.A0K = (AnonymousClass19Z) A00.A2o.get();
            this.A08 = C12980iv.A0W(A00);
            this.A0I = (AnonymousClass14X) A00.AFM.get();
            this.A0B = C12960it.A0P(A00);
            this.A0J = C12990iw.A0f(A00);
            this.A0H = (AnonymousClass1A2) A00.AET.get();
        }
    }

    public void A01(boolean z) {
        C15370n3 r3 = this.A0E;
        if (r3 != null) {
            C51442Ut r1 = this.A0F;
            if (r1 != null) {
                r1.A0C = Boolean.valueOf(z);
                r1.A0D = Boolean.valueOf(!z);
            }
            this.A0K.A01(getContext(), r3, 6, z);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0L;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0L = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.A0A = C12970iu.A0T(this, R.id.contact_title);
        this.A0D = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_pay);
        this.A01 = AnonymousClass028.A0D(this, R.id.action_add_person);
        this.A02 = AnonymousClass028.A0D(this, R.id.action_call_plus);
        this.A0C = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_call);
        this.A04 = AnonymousClass028.A0D(this, R.id.action_message);
        this.A03 = AnonymousClass028.A0D(this, R.id.action_search_chat);
        this.A05 = AnonymousClass028.A0D(this, R.id.action_videocall);
        this.A07 = C12960it.A0I(this, R.id.contact_subtitle);
        this.A06 = C12960it.A0I(this, R.id.contact_chat_status);
        if (getContext() instanceof ActivityC13790kL) {
            ActivityC13810kN r4 = (ActivityC13810kN) AnonymousClass12P.A01(getContext(), ActivityC13790kL.class);
            AnonymousClass1A2 r2 = this.A0H;
            Context context = getContext();
            RunnableBRunnable0Shape15S0100000_I1_1 runnableBRunnable0Shape15S0100000_I1_1 = new RunnableBRunnable0Shape15S0100000_I1_1(this, 8);
            C14900mE r5 = r2.A00;
            C15570nT r6 = r2.A01;
            C17070qD r9 = r2.A04;
            C22710zW r8 = r2.A03;
            this.A0G = new AnonymousClass1IZ(context, r4, r5, r6, r2.A02, r8, r9, (C74503iB) C13000ix.A02(r4).A00(C74503iB.class), null, runnableBRunnable0Shape15S0100000_I1_1, false);
        }
        C12960it.A0y(this.A04, this, 6);
        C12960it.A0y(this.A03, this, 10);
        C12960it.A0y(this.A02, this, 8);
        C12960it.A0y(this.A0D, this, 9);
        C12960it.A0y(this.A0C, this, 7);
        C12960it.A0y(this.A05, this, 5);
    }

    public void setAddContactButtonListener(View.OnClickListener onClickListener) {
        this.A01.setOnClickListener(onClickListener);
    }

    public void setContact(C15370n3 r6) {
        this.A0E = r6;
        new C28801Pb(getContext(), this.A0A, this.A0B, this.A0J).A06(r6);
    }

    public void setContactChatStatus(String str) {
        this.A06.setText(str);
    }

    public void setContactChatStatusVisibility(int i) {
        this.A06.setVisibility(i);
    }

    public void setContactInfoLoggingEvent(C51442Ut r1) {
        this.A0F = r1;
    }

    public void setCurrencyIcon(C17930rd r5) {
        getContext();
        int A00 = AnonymousClass14X.A00(r5);
        if (A00 != 0) {
            this.A0D.A00(A00, getContext().getString(R.string.contact_info_action_icon_pay));
            return;
        }
        this.A0D.setVisibility(8);
        AbstractC15710nm r3 = this.A09;
        StringBuilder A0k = C12960it.A0k("Currency icon for country ");
        A0k.append(r5.A03);
        r3.AaV("ContactDetailsCard/PayButton", C12960it.A0d(" missing", A0k), true);
    }

    public void setPaymentEligibility(int i) {
        this.A00 = i;
    }

    public void setSubTitle(String str) {
        this.A07.setText(str);
    }

    public void setSubtitleOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.A07.setOnLongClickListener(onLongClickListener);
    }

    public void setTitleOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.A0A.setOnLongClickListener(onLongClickListener);
    }
}
