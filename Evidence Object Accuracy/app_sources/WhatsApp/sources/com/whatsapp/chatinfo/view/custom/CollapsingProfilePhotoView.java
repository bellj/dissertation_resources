package com.whatsapp.chatinfo.view.custom;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CollapsingProfilePhotoView extends FrameLayout implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public ImageView A07;
    public ImageView A08;
    public AnonymousClass018 A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public final int A0E;

    public CollapsingProfilePhotoView(Context context) {
        super(context);
        A00();
        this.A04 = -1;
        this.A00 = -1.0f;
        this.A0E = C12970iu.A05(this);
        this.A06 = -1;
        this.A01 = -1;
        this.A0B = false;
        A02(context);
    }

    public CollapsingProfilePhotoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A04 = -1;
        this.A00 = -1.0f;
        this.A0E = C12970iu.A05(this);
        this.A06 = -1;
        this.A01 = -1;
        this.A0B = false;
        A02(context);
    }

    public CollapsingProfilePhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A04 = -1;
        this.A00 = -1.0f;
        this.A0E = C12970iu.A05(this);
        this.A06 = -1;
        this.A01 = -1;
        this.A0B = false;
        A02(context);
    }

    public CollapsingProfilePhotoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0D) {
            this.A0D = true;
            this.A09 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public void A01(int i, int i2) {
        if (i != -1 && i2 != -1) {
            this.A06 = i;
            this.A01 = i2;
            if (this.A02 != 0) {
                int i3 = (i - i2) >> 1;
                if (!C28141Kv.A01(this.A09)) {
                    i = (this.A02 - i2) - ((int) this.A00);
                }
                this.A04 = i - i3;
            }
        }
    }

    public final void A02(Context context) {
        FrameLayout.inflate(context, R.layout.collapsing_profile_photo_layout, this);
        this.A08 = C12970iu.A0K(this, R.id.wds_profile_picture);
        this.A07 = C12970iu.A0K(this, R.id.profile_photo_halo);
        C12980iv.A1H(this.A08.getViewTreeObserver(), this, 2);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public ImageView getProfileImage() {
        return this.A08;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (!this.A0B) {
            int left = this.A08.getLeft();
            this.A05 = left;
            this.A03 = left;
        }
    }

    public void setAnimationValue(float f) {
        float f2;
        float f3;
        if (!this.A0B) {
            this.A0B = C12960it.A1U((f > 0.0f ? 1 : (f == 0.0f ? 0 : -1)));
        }
        boolean A01 = C28141Kv.A01(this.A09);
        int i = this.A04;
        if (i != -1) {
            float f4 = this.A00;
            if (f4 != -1.0f) {
                float A02 = 1.0f - ((1.0f - (f4 / C12990iw.A02(this.A08))) * f);
                float A022 = (C12990iw.A02(this.A08) - this.A00) / 2.0f;
                if (A01) {
                    f2 = -(((float) (this.A05 - this.A04)) + A022);
                } else {
                    f2 = ((float) (this.A04 - this.A05)) - A022;
                }
                this.A08.setTranslationX(f2 * f);
                this.A08.setScaleX(A02);
                this.A08.setScaleY(A02);
                if (this.A0C) {
                    float f5 = this.A00;
                    float A023 = 1.0f - ((1.0f - (f5 / C12990iw.A02(this.A07))) * f);
                    float A024 = (C12990iw.A02(this.A07) - f5) / 2.0f;
                    if (A01) {
                        f3 = -(((float) (this.A03 - this.A04)) + A024);
                    } else {
                        f3 = ((float) (this.A04 - this.A03)) - A024;
                    }
                    this.A07.setTranslationX(f3 * f);
                    this.A07.setScaleX(A023);
                    this.A07.setScaleY(A023);
                    return;
                }
                return;
            }
        }
        Object[] A1a = C12980iv.A1a();
        C12960it.A1P(A1a, i, 0);
        A1a[1] = Float.valueOf(this.A00);
        String.format("Required values not set: profilePhotoCollapsedX = %s , targetDimen = %s", A1a);
    }

    public void setCollapsedProfilePhotoDimen(float f) {
        this.A00 = f;
    }

    public void setHaloEnabled(boolean z) {
        if (this.A0C != z) {
            this.A0C = z;
            this.A07.setVisibility(C12960it.A02(z ? 1 : 0));
        }
    }
}
