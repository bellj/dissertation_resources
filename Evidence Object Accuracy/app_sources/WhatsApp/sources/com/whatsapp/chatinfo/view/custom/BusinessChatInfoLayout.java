package com.whatsapp.chatinfo.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class BusinessChatInfoLayout extends ChatInfoLayoutV2 {
    public boolean A00;

    public BusinessChatInfoLayout(Context context) {
        super(context);
        A00();
    }

    public BusinessChatInfoLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public BusinessChatInfoLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public BusinessChatInfoLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    @Override // com.whatsapp.chatinfo.view.custom.ChatInfoLayoutV2, X.AnonymousClass2Ew
    public int A03(int i) {
        ImageView imageView = ((ChatInfoLayoutV2) this).A05;
        if (imageView == null || imageView.getDrawable() == null) {
            return super.A03(i);
        }
        return (int) (((float) i) * 0.5625f);
    }
}
