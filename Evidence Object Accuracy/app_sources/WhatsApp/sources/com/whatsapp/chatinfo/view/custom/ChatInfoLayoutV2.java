package com.whatsapp.chatinfo.view.custom;

import X.AbstractC60352wl;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C92434Vw;
import X.EnumC87284Ax;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.ScalingFrameLayout;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.status.ScalingContactStatusThumbnail;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* loaded from: classes2.dex */
public class ChatInfoLayoutV2 extends AbstractC60352wl {
    public float A00;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 0;
    public View A04;
    public ImageView A05;
    public ImageView A06;
    public TextView A07;
    public CollapsingProfilePhotoView A08;
    public ScalingFrameLayout A09;
    public C92434Vw A0A;
    public boolean A0B = true;
    public boolean A0C = false;

    public ChatInfoLayoutV2(Context context) {
        super(context);
    }

    public ChatInfoLayoutV2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ChatInfoLayoutV2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // X.AnonymousClass2Ew
    public int A03(int i) {
        return getResources().getDimensionPixelSize(R.dimen.chat_info_profile_photo_max_size) + getResources().getDimensionPixelSize(R.dimen.space_loose);
    }

    @Override // X.AnonymousClass2Ew
    public void A04() {
        super.A04();
        this.A0L.setVisibility(0);
        A0D();
    }

    @Override // X.AnonymousClass2Ew
    public void A05() {
        super.A05();
        this.A03 = getResources().getDimensionPixelSize(R.dimen.business_profile_photo_bg_circle_padding);
        this.A01 = getResources().getDimensionPixelSize(R.dimen.business_profile_photo_toolbar_margins);
        this.A0C = this.A0Q.A07(1533);
        this.A02 = getResources().getConfiguration().orientation;
        this.A09 = (ScalingFrameLayout) AnonymousClass028.A0D(this, R.id.profile_picture_scaler);
        this.A07 = C12960it.A0I(this, R.id.conversation_contact_name);
        this.A04 = AnonymousClass028.A0D(this, R.id.profile_picture_circle);
        this.A05 = C12970iu.A0K(this, R.id.picture);
        this.A06 = getProfilePhotoImage();
    }

    @Override // X.AnonymousClass2Ew
    public void A09(int i, int i2) {
        super.A09(i, i2);
        if (this.A0C) {
            CollapsingProfilePhotoView collapsingProfilePhotoView = this.A08;
            collapsingProfilePhotoView.A00 = (float) (super.A07 - (this.A01 << 1));
            collapsingProfilePhotoView.A01(super.A04, super.A03);
        }
    }

    public final void A0D() {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.A0E.getLayoutParams();
        int i = 80;
        if (getWidth() >= getHeight()) {
            i = 17;
        }
        layoutParams.gravity = i;
        this.A0E.setLayoutParams(layoutParams);
    }

    public final void A0E() {
        EnumC87284Ax r0;
        C92434Vw r3 = this.A0A;
        this.A08.setHaloEnabled(this.A0B);
        if (r3 == null || !r3.A00() || !this.A0B) {
            ImageView imageView = this.A06;
            if (imageView instanceof ScalingContactStatusThumbnail) {
                ((ScalingContactStatusThumbnail) imageView).A00 = false;
            } else if (imageView instanceof WDSProfilePhoto) {
                ((WDSProfilePhoto) imageView).setStatusIndicatorEnabled(false);
            }
        } else {
            ImageView imageView2 = this.A06;
            if (imageView2 instanceof ScalingContactStatusThumbnail) {
                ScalingContactStatusThumbnail scalingContactStatusThumbnail = (ScalingContactStatusThumbnail) imageView2;
                scalingContactStatusThumbnail.A00 = true;
                scalingContactStatusThumbnail.A03(r3.A01, r3.A00);
            } else if (this.A0C) {
                WDSProfilePhoto wDSProfilePhoto = (WDSProfilePhoto) imageView2;
                wDSProfilePhoto.setStatusIndicatorEnabled(true);
                if (r3.A01 > 0) {
                    r0 = EnumC87284Ax.A01;
                } else {
                    r0 = EnumC87284Ax.A02;
                }
                wDSProfilePhoto.setStatusIndicatorState(r0);
            }
        }
    }

    private ImageView getProfilePhotoImage() {
        CollapsingProfilePhotoView collapsingProfilePhotoView = (CollapsingProfilePhotoView) AnonymousClass028.A0D(this, R.id.collapsing_profile_photo_view);
        this.A08 = collapsingProfilePhotoView;
        ImageView imageView = collapsingProfilePhotoView.A08;
        ImageView A0K = C12970iu.A0K(this, R.id.profile_picture_image);
        boolean z = this.A0C;
        CollapsingProfilePhotoView collapsingProfilePhotoView2 = this.A08;
        if (z) {
            collapsingProfilePhotoView2.setVisibility(0);
            if (this.A0C) {
                CollapsingProfilePhotoView collapsingProfilePhotoView3 = this.A08;
                collapsingProfilePhotoView3.A00 = (float) (super.A07 - (this.A01 << 1));
                collapsingProfilePhotoView3.A01(super.A04, super.A03);
            }
            A0K.setVisibility(8);
            return imageView;
        }
        collapsingProfilePhotoView2.setVisibility(8);
        A0K.setVisibility(0);
        return A0K;
    }

    @Override // X.AnonymousClass2Ew
    public int getToolbarColorResId() {
        return R.color.toolbar_icon_color_light_mode;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int i = this.A02;
        int i2 = configuration.orientation;
        if (i != i2) {
            this.A02 = i2;
            CollapsingProfilePhotoView collapsingProfilePhotoView = this.A08;
            C12980iv.A1H(collapsingProfilePhotoView.A08.getViewTreeObserver(), collapsingProfilePhotoView, 2);
            this.A0B = true;
            A0E();
        }
    }

    @Override // X.AnonymousClass2Ew
    public void setOnPhotoClickListener(View.OnClickListener onClickListener) {
        super.A0A = onClickListener;
    }

    @Override // X.AnonymousClass2Ew
    public void setRadius(float f) {
        Drawable drawable;
        this.A00 = f;
        ImageView imageView = this.A06;
        if (imageView instanceof ScalingContactStatusThumbnail) {
            ((ThumbnailButton) imageView).A02 = f;
        }
        boolean A1S = C12960it.A1S((f > -2.14748365E9f ? 1 : (f == -2.14748365E9f ? 0 : -1)));
        View view = this.A04;
        if (A1S) {
            drawable = AnonymousClass00T.A04(getContext(), R.drawable.business_profile_photo_bg);
        } else {
            drawable = null;
        }
        view.setBackground(drawable);
    }

    @Override // X.AnonymousClass2Ew
    public void setStatusData(C92434Vw r1) {
        this.A0A = r1;
        A0E();
    }
}
