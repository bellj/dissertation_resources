package com.whatsapp.chatinfo.view.custom;

import X.AnonymousClass2x5;
import X.AnonymousClass4TJ;
import X.C28141Kv;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.ui.media.MediaCard;

/* loaded from: classes2.dex */
public class ChatInfoMediaCardV2 extends MediaCard {
    public boolean A00;

    public ChatInfoMediaCardV2(Context context) {
        super(context, null);
        A01();
    }

    public ChatInfoMediaCardV2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public ChatInfoMediaCardV2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public ChatInfoMediaCardV2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A01();
    }

    @Override // X.AnonymousClass36H
    public AnonymousClass2x5 A03(ViewGroup.LayoutParams layoutParams, AnonymousClass4TJ r5, int i) {
        AnonymousClass2x5 A03 = super.A03(layoutParams, r5, i);
        ((ThumbnailButton) A03).A02 = getResources().getDimension(R.dimen.quick_contact_card_radius);
        return A03;
    }

    @Override // com.whatsapp.ui.media.MediaCard, X.AnonymousClass36H
    public void A08(AttributeSet attributeSet) {
        super.A08(attributeSet);
        ViewGroup.LayoutParams layoutParams = ((MediaCard) this).A00.getLayoutParams();
        layoutParams.height = getThumbnailPixelSize();
        ((MediaCard) this).A00.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.A06.getLayoutParams();
        layoutParams2.height = getThumbnailPixelSize();
        this.A06.setLayoutParams(layoutParams2);
    }

    @Override // X.AnonymousClass36H
    public int getThumbnailIconGravity() {
        return C28141Kv.A00(this.A0B) ? 5 : 3;
    }

    @Override // com.whatsapp.ui.media.MediaCard, X.AnonymousClass36H
    public int getThumbnailPixelSize() {
        return getResources().getDimensionPixelSize(R.dimen.large_thumbnail_size);
    }

    @Override // X.AnonymousClass36H
    public int getThumbnailTextGravity() {
        return C28141Kv.A00(this.A0B) ? 3 : 5;
    }
}
