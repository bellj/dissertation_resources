package com.whatsapp.contactinput.contactscreen;

import X.AbstractC16710pd;
import X.ActivityC13830kP;
import X.C10690f2;
import X.C113835Je;
import X.C113845Jf;
import X.C16700pc;
import X.C54172gI;
import X.C71583dA;
import X.C74453i2;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public final class NativeContactActivity extends ActivityC13830kP {
    public final AbstractC16710pd A00 = new C10690f2(new C113845Jf(this), new C113835Je(this), new C71583dA(C74453i2.class));

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_native_contact);
        List emptyList = Collections.emptyList();
        C16700pc.A0B(emptyList);
        C54172gI r1 = new C54172gI(emptyList);
        View findViewById = findViewById(R.id.form_recycler_view);
        C16700pc.A0B(findViewById);
        ((RecyclerView) findViewById).setAdapter(r1);
    }
}
