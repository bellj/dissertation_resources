package com.whatsapp;

import X.AbstractC58392on;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class ListItemWithRightIcon extends AbstractC58392on {
    public boolean A00;

    public ListItemWithRightIcon(Context context) {
        super(context, null);
        A00();
    }

    public ListItemWithRightIcon(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public ListItemWithRightIcon(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public ListItemWithRightIcon(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    @Override // X.AbstractC58392on
    public int getRootLayoutID() {
        return R.layout.list_item_with_right_icon;
    }
}
