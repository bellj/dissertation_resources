package com.whatsapp;

import X.AbstractC37201lg;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class CircleWaImageView extends AbstractC37201lg {
    public static final Bitmap.Config A0A = Bitmap.Config.ARGB_8888;
    public static final ImageView.ScaleType A0B = ImageView.ScaleType.CENTER_CROP;
    public float A00;
    public int A01;
    public int A02;
    public Bitmap A03;
    public BitmapShader A04;
    public Matrix A05;
    public Paint A06;
    public boolean A07;
    public boolean A08;
    public final RectF A09;

    public CircleWaImageView(Context context) {
        this(context, null);
    }

    public CircleWaImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CircleWaImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A09 = new RectF();
        this.A06 = new Paint();
        this.A05 = new Matrix();
        super.setScaleType(A0B);
        this.A07 = true;
        if (this.A08) {
            A03();
            this.A08 = false;
        }
    }

    public final void A02() {
        Drawable drawable = getDrawable();
        Bitmap bitmap = null;
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                bitmap = ((BitmapDrawable) drawable).getBitmap();
            } else {
                try {
                    Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), A0A);
                    Canvas canvas = new Canvas(createBitmap);
                    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    drawable.draw(canvas);
                    bitmap = createBitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        this.A03 = bitmap;
        A03();
    }

    public final void A03() {
        float width;
        float height;
        if (!this.A07) {
            this.A08 = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            Bitmap bitmap = this.A03;
            if (bitmap != null) {
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                this.A04 = new BitmapShader(bitmap, tileMode, tileMode);
                Paint paint = this.A06;
                paint.setAntiAlias(true);
                paint.setShader(this.A04);
                this.A01 = this.A03.getHeight();
                this.A02 = this.A03.getWidth();
                RectF rectF = this.A09;
                int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int min = Math.min(width2, height2);
                float paddingLeft = ((float) getPaddingLeft()) + (((float) (width2 - min)) / 2.0f);
                float paddingTop = ((float) getPaddingTop()) + (((float) (height2 - min)) / 2.0f);
                float f = (float) min;
                rectF.set(new RectF(paddingLeft, paddingTop, paddingLeft + f, f + paddingTop));
                this.A00 = Math.min(rectF.height() / 2.0f, rectF.width() / 2.0f);
                Matrix matrix = this.A05;
                matrix.set(null);
                float f2 = 0.0f;
                if (((float) this.A02) * rectF.height() > rectF.width() * ((float) this.A01)) {
                    width = rectF.height() / ((float) this.A01);
                    f2 = (rectF.width() - (((float) this.A02) * width)) * 0.5f;
                    height = 0.0f;
                } else {
                    width = rectF.width() / ((float) this.A02);
                    height = (rectF.height() - (((float) this.A01) * width)) * 0.5f;
                }
                matrix.setScale(width, width);
                matrix.postTranslate(((float) ((int) (f2 + 0.5f))) + rectF.left, ((float) ((int) (height + 0.5f))) + rectF.top);
                this.A04.setLocalMatrix(matrix);
            }
            invalidate();
        }
    }

    @Override // android.widget.ImageView
    public ImageView.ScaleType getScaleType() {
        return A0B;
    }

    @Override // com.whatsapp.WaImageView, android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03 != null) {
            RectF rectF = this.A09;
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), this.A00, this.A06);
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        A03();
    }

    @Override // android.widget.ImageView
    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        A02();
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        A02();
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageResource(int i) {
        super.setImageResource(i);
        A02();
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        A02();
    }

    @Override // android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
        super.setPadding(i, i2, i3, i4);
        A03();
    }

    @Override // android.view.View
    public void setPaddingRelative(int i, int i2, int i3, int i4) {
        super.setPaddingRelative(i, i2, i3, i4);
        A03();
    }

    @Override // android.widget.ImageView
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != A0B) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }
}
