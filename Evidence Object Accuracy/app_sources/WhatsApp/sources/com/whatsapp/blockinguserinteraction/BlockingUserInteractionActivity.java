package com.whatsapp.blockinguserinteraction;

import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.C15880o3;
import X.C249317l;
import android.os.Bundle;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class BlockingUserInteractionActivity extends ActivityC13810kN {
    public C15880o3 A00;
    public C249317l A01;
    public boolean A02;

    public BlockingUserInteractionActivity() {
        this(0);
    }

    public BlockingUserInteractionActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 21);
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            ActivityC13810kN.A10(A1M, this);
            this.A00 = (C15880o3) A1M.ACF.get();
            this.A01 = (C249317l) A1M.A8B.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        IDxObserverShape3S0100000_1_I1 iDxObserverShape3S0100000_1_I1;
        AnonymousClass016 r0;
        overridePendingTransition(0, 0);
        super.onCreate(bundle);
        int intExtra = getIntent().getIntExtra("blocking_type", 0);
        if (intExtra == 0) {
            setContentView(R.layout.activity_blocking_user_interactions);
            C15880o3 r1 = this.A00;
            iDxObserverShape3S0100000_1_I1 = new IDxObserverShape3S0100000_1_I1(this, 22);
            r0 = r1.A03;
        } else if (intExtra == 1) {
            setTitle(R.string.msg_store_migrate_title);
            setContentView(R.layout.activity_forced_migration_blocking_user_interactions);
            C249317l r12 = this.A01;
            iDxObserverShape3S0100000_1_I1 = new IDxObserverShape3S0100000_1_I1(this, 23);
            r0 = r12.A01;
        } else {
            return;
        }
        r0.A05(this, iDxObserverShape3S0100000_1_I1);
    }
}
