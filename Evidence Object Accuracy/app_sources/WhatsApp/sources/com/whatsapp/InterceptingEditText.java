package com.whatsapp;

import X.AnonymousClass1IW;
import X.AnonymousClass5TC;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

/* loaded from: classes2.dex */
public class InterceptingEditText extends AnonymousClass1IW {
    public AnonymousClass5TC A00;

    public InterceptingEditText(Context context) {
        super(context);
    }

    public InterceptingEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public InterceptingEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        AnonymousClass5TC r0;
        if (keyEvent.getKeyCode() != 4 || keyEvent.getAction() != 1 || (r0 = this.A00) == null) {
            return super.onKeyPreIme(i, keyEvent);
        }
        r0.AMn();
        return true;
    }

    public void setOnBackButtonListener(AnonymousClass5TC r1) {
        this.A00 = r1;
    }
}
