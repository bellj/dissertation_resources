package com.whatsapp.softenforcementsmb;

import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass316;
import X.AnonymousClass5ID;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C252818u;
import X.C26851Fb;
import X.C28391Mz;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101794o6;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.RoundedBottomSheetDialogFragment;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class SMBSoftEnforcementEducationFragment extends RoundedBottomSheetDialogFragment {
    public static final Map A08 = new AnonymousClass5ID();
    public Context A00;
    public View A01;
    public ScrollView A02;
    public Integer A03 = C12970iu.A0h();
    public final AnonymousClass01F A04;
    public final C252818u A05;
    public final C14830m7 A06;
    public final C26851Fb A07;

    public SMBSoftEnforcementEducationFragment(Context context, AnonymousClass01F r3, C252818u r4, C14830m7 r5, C26851Fb r6) {
        this.A00 = context;
        this.A05 = r4;
        this.A04 = r3;
        this.A07 = r6;
        this.A06 = r5;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.smb_softenforcement_warning);
        A0F.findViewById(R.id.smb_soft_enforcement_education_intro);
        this.A02 = (ScrollView) AnonymousClass028.A0D(A0F, R.id.smb_soft_enforcement_warning_scroller);
        this.A01 = AnonymousClass028.A0D(A0F, R.id.smb_soft_enforcement_accept_button_container);
        throw C12980iv.A0n("type");
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ScrollView scrollView = this.A02;
        View view = this.A01;
        if (C28391Mz.A02()) {
            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101794o6(view, scrollView, this));
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - 0);
        new AnonymousClass316();
        throw C12980iv.A0n("source");
    }
}
