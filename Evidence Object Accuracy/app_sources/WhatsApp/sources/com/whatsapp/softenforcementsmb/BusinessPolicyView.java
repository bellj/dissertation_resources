package com.whatsapp.softenforcementsmb;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass316;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13000ix;
import X.C26851Fb;
import X.C64143Ek;
import android.os.Bundle;
import com.whatsapp.WaInAppBrowsingActivity;
import com.whatsapp.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

/* loaded from: classes2.dex */
public class BusinessPolicyView extends WaInAppBrowsingActivity {
    public long A00;
    public C26851Fb A01;
    public boolean A02;

    public BusinessPolicyView() {
        this(0);
    }

    public BusinessPolicyView(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 122);
    }

    @Override // X.AbstractActivityC58452pV, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13790kL.A0f(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            this.A01 = (C26851Fb) A1M.AHq.get();
        }
    }

    @Override // com.whatsapp.WaInAppBrowsingActivity, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - this.A00);
        try {
            C64143Ek r5 = new C64143Ek(C13000ix.A05(getIntent().getStringExtra("notificationJSONObject")));
            C26851Fb r4 = this.A01;
            Integer A0h = C12970iu.A0h();
            Long valueOf = Long.valueOf(seconds);
            AnonymousClass316 r2 = new AnonymousClass316();
            r2.A06 = r5.A05;
            r2.A08 = r5.A07;
            r2.A05 = r5.A04;
            r2.A04 = C12980iv.A0l(r5.A00);
            r2.A07 = r5.A06;
            r2.A00 = C12960it.A0V();
            r2.A01 = A0h;
            r2.A02 = A0h;
            r2.A03 = valueOf;
            if (!r4.A01.A07(1730)) {
                r4.A02.A07(r2);
            }
        } catch (JSONException e) {
            Log.e("Error deserializing JSON String: notificationJSONObject", e);
        }
        super.onBackPressed();
    }

    @Override // com.whatsapp.WaInAppBrowsingActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00 = System.currentTimeMillis();
    }
}
