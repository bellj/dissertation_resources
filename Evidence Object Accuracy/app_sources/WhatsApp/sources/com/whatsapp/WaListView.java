package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.ListView;

/* loaded from: classes3.dex */
public class WaListView extends ListView implements AbsListView.OnScrollListener, AnonymousClass004 {
    public int A00;
    public AbsListView.OnScrollListener A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public WaListView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A00 = 0;
        super.setOnScrollListener(this);
    }

    public WaListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A00 = 0;
        super.setOnScrollListener(this);
    }

    public WaListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A00 = 0;
        super.setOnScrollListener(this);
    }

    public WaListView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public int getScrollState() {
        return this.A00;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        AbsListView.OnScrollListener onScrollListener = this.A01;
        if (onScrollListener != null) {
            onScrollListener.onScroll(absListView, i, i2, i3);
        }
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.A00 = i;
        AbsListView.OnScrollListener onScrollListener = this.A01;
        if (onScrollListener != null) {
            onScrollListener.onScrollStateChanged(absListView, i);
        }
    }

    @Override // android.widget.AbsListView
    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.A01 = onScrollListener;
    }
}
