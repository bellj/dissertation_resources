package com.whatsapp.jobqueue.requirement;

import X.AbstractC27291Gt;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17030q9;
import X.C43551xD;
import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Locale;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class HsmMessagePackRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C17030q9 A00;
    public final String elementName;
    public Locale[] locales;
    public final String namespace;

    public HsmMessagePackRequirement(String str, String str2, Locale[] localeArr) {
        AnonymousClass009.A05(localeArr);
        this.locales = localeArr;
        AnonymousClass009.A04(str);
        this.namespace = str;
        AnonymousClass009.A04(str2);
        this.elementName = str2;
    }

    public boolean A00() {
        C43551xD A02 = this.A00.A02(this.namespace, this.locales);
        return (A02 == null || A02.A02.size() <= 0 || C17030q9.A00(A02, this.elementName) == null) ? false : true;
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        Long l;
        C17030q9 r1 = this.A00;
        Locale[] localeArr = this.locales;
        String str = this.namespace;
        synchronized (r1.A03) {
            l = (Long) r1.A04.get(Pair.create(localeArr, str));
        }
        if (l != null) {
            long longValue = l.longValue();
            if (longValue > 0 && System.currentTimeMillis() - longValue < 3600000) {
                StringBuilder sb = new StringBuilder("satisfying hsm pack requirement due to recent response");
                StringBuilder sb2 = new StringBuilder("; locales=");
                sb2.append(AbstractC27291Gt.A08(this.locales));
                sb2.append("; namespace=");
                sb2.append(this.namespace);
                sb.append(sb2.toString());
                Log.i(sb.toString());
                return true;
            }
        }
        return A00();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C17030q9) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AAM.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        Locale[] localeArr = this.locales;
        if (localeArr == null || localeArr.length == 0) {
            throw new InvalidObjectException("locales[] must not be empty");
        } else if (TextUtils.isEmpty(this.namespace)) {
            throw new InvalidObjectException("namespace must not be empty");
        } else if (TextUtils.isEmpty(this.elementName)) {
            throw new InvalidObjectException("elementName must not be empty");
        }
    }
}
