package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1MW;
import X.C15940oB;
import X.C15990oG;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class AxolotlSessionRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15990oG A00;
    public transient DeviceJid A01;
    public final String jid;

    public AxolotlSessionRequirement(DeviceJid deviceJid) {
        this.A01 = deviceJid;
        this.jid = deviceJid.getRawString();
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        return this.A00.A0g(C15940oB.A02(this.A01));
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A2d();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A01 = DeviceJid.get(this.jid);
        } catch (AnonymousClass1MW unused) {
            StringBuilder sb = new StringBuilder("jid must be a valid user jid; jid=");
            sb.append(this.jid);
            throw new InvalidObjectException(sb.toString());
        }
    }
}
