package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C15570nT;
import X.C15960oD;
import X.C15990oG;
import X.C16030oK;
import android.content.Context;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class AxolotlFastRatchetSenderKeyRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15570nT A00;
    public transient C15990oG A01;
    public transient C16030oK A02;
    public String groupJid = C15960oD.A00.getRawString();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (r1 <= 0) goto L_0x004d;
     */
    @Override // org.whispersystems.jobqueue.requirements.Requirement
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AJu() {
        /*
            r6 = this;
            X.0oK r0 = r6.A02
            boolean r0 = r0.A0d()
            r5 = 1
            if (r0 == 0) goto L_0x0067
            X.0oK r0 = r6.A02
            java.util.List r0 = r0.A08()
            boolean r0 = r0.isEmpty()
            r4 = 0
            if (r0 != 0) goto L_0x001c
            X.0oK r0 = r6.A02
            r0.A0e()
            return r4
        L_0x001c:
            X.0nT r0 = r6.A00
            r0.A08()
            X.1Hq r0 = r0.A04
            X.0oC r1 = X.C15940oB.A02(r0)
            X.0oD r0 = X.C15960oD.A00
            java.lang.String r0 = r0.getRawString()
            X.0oF r3 = new X.0oF
            r3.<init>(r1, r0)
            X.0oG r1 = r6.A01
            X.16Z r0 = r1.A0J
            java.util.concurrent.locks.Lock r2 = r0.A01(r3)
            if (r2 == 0) goto L_0x003f
            r2.lock()     // Catch: all -> 0x0060
        L_0x003f:
            X.1Rl r0 = r1.A06     // Catch: all -> 0x0060
            X.1Rm r0 = r0.A00(r3)     // Catch: all -> 0x0060
            if (r0 == 0) goto L_0x004d
            byte[] r0 = r0.A01     // Catch: all -> 0x0060
            int r1 = r0.length     // Catch: all -> 0x0060
            r0 = 1
            if (r1 > 0) goto L_0x004e
        L_0x004d:
            r0 = 0
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.unlock()
        L_0x0053:
            if (r0 != 0) goto L_0x0067
            java.lang.String r0 = "AxolotlFastRatchetSenderKeyRequirement/empty sender key record; reset key"
            com.whatsapp.util.Log.i(r0)
            X.0oK r0 = r6.A02
            r0.A0H()
            return r4
        L_0x0060:
            r0 = move-exception
            if (r2 == 0) goto L_0x0066
            r2.unlock()
        L_0x0066:
            throw r0
        L_0x0067:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.requirement.AxolotlFastRatchetSenderKeyRequirement.AJu():boolean");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.A1w();
        this.A01 = r1.A2d();
        this.A02 = (C16030oK) r1.AAd.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        String rawString = C15960oD.A00.getRawString();
        String str = this.groupJid;
        if (!rawString.equals(str)) {
            StringBuilder sb = new StringBuilder("groupJid is not location Jid, only location Jid supported for now; groupJid=");
            sb.append(str);
            throw new InvalidObjectException(sb.toString());
        }
    }
}
