package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass01J;
import X.AnonymousClass1LJ;
import X.C12990iw;
import X.C14850m9;
import X.C16240og;
import android.content.Context;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class OfflineProcessingCompletedRequirement implements Requirement, AnonymousClass1LJ {
    public transient C16240og A00;
    public transient C14850m9 A01;

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        if (this.A01.A07(560)) {
            C16240og r2 = this.A00;
            if (r2.A04 == 2 && r2.A02) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J A0S = C12990iw.A0S(context);
        this.A00 = (C16240og) A0S.ANq.get();
        this.A01 = A0S.A3L();
    }
}
