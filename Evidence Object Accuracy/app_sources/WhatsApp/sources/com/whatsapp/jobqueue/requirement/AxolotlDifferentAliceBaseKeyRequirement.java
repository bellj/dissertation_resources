package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1MW;
import X.C15940oB;
import X.C15990oG;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class AxolotlDifferentAliceBaseKeyRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15990oG A00;
    public transient DeviceJid A01;
    public final String jid;
    public final byte[] oldAliceBaseKey;

    public AxolotlDifferentAliceBaseKeyRequirement(DeviceJid deviceJid, byte[] bArr) {
        this.A01 = deviceJid;
        this.jid = deviceJid.getRawString();
        AnonymousClass009.A0G(bArr);
        this.oldAliceBaseKey = bArr;
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        return !Arrays.equals(this.oldAliceBaseKey, this.A00.A0G(C15940oB.A02(this.A01)).A01.A00.A05.A04());
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A2d();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A01 = DeviceJid.get(this.jid);
            byte[] bArr = this.oldAliceBaseKey;
            if (bArr == null || bArr.length == 0) {
                throw new InvalidObjectException("oldAliceBaseKey must not be empty");
            }
        } catch (AnonymousClass1MW unused) {
            StringBuilder sb = new StringBuilder("jid must be a valid user jid; jid=");
            sb.append(this.jid);
            throw new InvalidObjectException(sb.toString());
        }
    }
}
