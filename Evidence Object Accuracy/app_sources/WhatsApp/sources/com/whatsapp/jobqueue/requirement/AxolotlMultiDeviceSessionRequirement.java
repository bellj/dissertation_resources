package com.whatsapp.jobqueue.requirement;

import X.AbstractC14640lm;
import X.AbstractC15590nW;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1IS;
import X.AnonymousClass1LJ;
import X.AnonymousClass1MW;
import X.AnonymousClass1RV;
import X.AnonymousClass2Ci;
import X.C14850m9;
import X.C15380n4;
import X.C15600nX;
import X.C15940oB;
import X.C15950oC;
import X.C15990oG;
import X.C21100wr;
import X.C22830zi;
import X.C242914y;
import android.content.Context;
import android.database.Cursor;
import com.whatsapp.jid.DeviceJid;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public class AxolotlMultiDeviceSessionRequirement implements Requirement, AnonymousClass1LJ {
    public transient int A00 = 100;
    public transient int A01;
    public transient C15990oG A02;
    public transient C242914y A03;
    public transient C22830zi A04;
    public transient AbstractC14640lm A05;
    public transient AnonymousClass2Ci A06;
    public transient Object A07 = new Object();
    public transient List A08;
    public transient boolean A09;
    public volatile transient int A0A;
    public final boolean forceSenderKeyDistribution;
    public final String messageKeyId;
    public final String remoteRawJid;
    public final HashSet targetDeviceRawJids;

    public AxolotlMultiDeviceSessionRequirement(AbstractC14640lm r2, String str, Set set, boolean z) {
        this.messageKeyId = str;
        this.A05 = r2;
        this.remoteRawJid = r2.getRawString();
        HashSet hashSet = new HashSet();
        C15380n4.A0D(set, hashSet);
        this.targetDeviceRawJids = hashSet;
        this.forceSenderKeyDistribution = z;
    }

    public final List A00() {
        Collection<DeviceJid> A00;
        AbstractC15590nW r2;
        String A03;
        List list;
        synchronized (this.A07) {
            if (!(this instanceof AxolotlMultiDeviceSenderKeyRequirement)) {
                A00 = this.A06.A00();
            } else {
                AxolotlMultiDeviceSenderKeyRequirement axolotlMultiDeviceSenderKeyRequirement = (AxolotlMultiDeviceSenderKeyRequirement) this;
                HashSet hashSet = axolotlMultiDeviceSenderKeyRequirement.A06.A03;
                if (hashSet == null || hashSet.isEmpty()) {
                    A00 = axolotlMultiDeviceSenderKeyRequirement.A02.A00(new AnonymousClass1IS(axolotlMultiDeviceSenderKeyRequirement.A05, axolotlMultiDeviceSenderKeyRequirement.messageKeyId, true));
                    AbstractC14640lm r22 = axolotlMultiDeviceSenderKeyRequirement.A05;
                    if (r22 instanceof AbstractC15590nW) {
                        r2 = (AbstractC15590nW) r22;
                    } else {
                        r2 = null;
                    }
                    AnonymousClass009.A05(r2);
                    String str = axolotlMultiDeviceSenderKeyRequirement.groupParticipantHash;
                    AnonymousClass009.A05(str);
                    boolean startsWith = str.startsWith("2");
                    C15600nX r0 = axolotlMultiDeviceSenderKeyRequirement.A01;
                    if (startsWith) {
                        A03 = r0.A02(r2).A08();
                    } else {
                        A03 = r0.A03(r2);
                    }
                    if (!(!A03.equals(axolotlMultiDeviceSenderKeyRequirement.groupParticipantHash))) {
                        Collection A0C = axolotlMultiDeviceSenderKeyRequirement.A01.A02(r2).A0C(axolotlMultiDeviceSenderKeyRequirement.A00);
                        A0C.retainAll(A00);
                        if (C15380n4.A0G(r2)) {
                            HashSet hashSet2 = new HashSet();
                            for (DeviceJid deviceJid : A00) {
                                if (axolotlMultiDeviceSenderKeyRequirement.A00.A0E(deviceJid)) {
                                    hashSet2.add(deviceJid);
                                }
                            }
                            A0C.addAll(hashSet2);
                        }
                        A00 = A0C;
                    }
                } else {
                    A00 = axolotlMultiDeviceSenderKeyRequirement.A06.A00();
                }
            }
            if (!this.A09 || this.A01 != A00.size()) {
                if (!A00.isEmpty()) {
                    ArrayList arrayList = new ArrayList(A00.size());
                    for (DeviceJid deviceJid2 : A00) {
                        arrayList.add(C15940oB.A02(deviceJid2));
                    }
                    this.A08 = new ArrayList();
                    int size = arrayList.size() / this.A00;
                    int size2 = arrayList.size() % this.A00;
                    int i = 0;
                    while (i < size) {
                        List list2 = this.A08;
                        int i2 = this.A00;
                        int i3 = i * i2;
                        i++;
                        list2.add(arrayList.subList(i3, i2 * i));
                    }
                    if (size2 > 0) {
                        this.A08.add(arrayList.subList(arrayList.size() - size2, arrayList.size()));
                    }
                } else {
                    this.A08 = null;
                }
                this.A09 = true;
                this.A01 = A00.size();
                this.A0A = 0;
            }
            list = this.A08;
        }
        return list;
    }

    public void A01() {
        C22830zi r4 = this.A04;
        this.A06 = new AnonymousClass2Ci(this.A03, r4, new AnonymousClass1IS(this.A05, this.messageKeyId, true), this.targetDeviceRawJids, this.forceSenderKeyDistribution);
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        int i;
        List A00 = A00();
        if (A00 == null || A00.isEmpty()) {
            return true;
        }
        int i2 = this.A0A;
        do {
            C15990oG r8 = this.A02;
            List list = (List) A00.get(this.A0A);
            HashSet hashSet = new HashSet();
            C21100wr r9 = r8.A0F;
            synchronized (r9) {
                i = 0;
                if (r9.A01(list).isEmpty()) {
                    Set A02 = r9.A02(list);
                    int size = A02.size();
                    if (size != 0) {
                        Cursor A002 = r8.A0B.A00(A02);
                        C14850m9 r5 = r9.A00;
                        if (r5.A07(1056) || A002.getCount() == size) {
                            int columnIndex = A002.getColumnIndex("record");
                            int columnIndex2 = A002.getColumnIndex("recipient_id");
                            int columnIndex3 = A002.getColumnIndex("recipient_type");
                            int columnIndex4 = A002.getColumnIndex("device_id");
                            while (A002.moveToNext()) {
                                byte[] blob = A002.getBlob(columnIndex);
                                C15950oC r12 = new C15950oC(A002.getInt(columnIndex3), String.valueOf(A002.getLong(columnIndex2)), A002.getInt(columnIndex4));
                                try {
                                    AnonymousClass1RV r0 = new AnonymousClass1RV(blob);
                                    C15990oG.A05(r0);
                                    r9.A03(r0, r12);
                                    i++;
                                } catch (IOException unused) {
                                    hashSet.add(r12);
                                    if (!r5.A07(1056)) {
                                        break;
                                    }
                                }
                            }
                            A002.close();
                            r9.A05(A02);
                            Iterator it = hashSet.iterator();
                            while (it.hasNext()) {
                                r8.A0H((C15950oC) it.next());
                            }
                            if (!(hashSet.size() == 0 && size == i)) {
                                i = 0;
                            }
                        } else {
                            A002.getCount();
                            A002.close();
                        }
                    }
                    i = 1;
                }
            }
            if (i == 0) {
                return false;
            }
            int i3 = this.A0A + 1;
            this.A0A = i3;
            if (i3 == A00.size()) {
                this.A0A = 0;
            }
        } while (this.A0A != i2);
        return true;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        if (!(this instanceof AxolotlMultiDeviceSenderKeyRequirement)) {
            AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
            this.A02 = r1.A2d();
            this.A04 = (C22830zi) r1.AHH.get();
            this.A03 = (C242914y) r1.ABu.get();
            A01();
            return;
        }
        AxolotlMultiDeviceSenderKeyRequirement axolotlMultiDeviceSenderKeyRequirement = (AxolotlMultiDeviceSenderKeyRequirement) this;
        AnonymousClass01J r12 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        axolotlMultiDeviceSenderKeyRequirement.A00 = r12.A1w();
        ((AxolotlMultiDeviceSessionRequirement) axolotlMultiDeviceSenderKeyRequirement).A02 = r12.A2d();
        axolotlMultiDeviceSenderKeyRequirement.A02 = (C22830zi) r12.AHH.get();
        axolotlMultiDeviceSenderKeyRequirement.A01 = (C15600nX) r12.A8x.get();
        axolotlMultiDeviceSenderKeyRequirement.A03 = (C242914y) r12.ABu.get();
        axolotlMultiDeviceSenderKeyRequirement.A01();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A05 = AbstractC14640lm.A00(this.remoteRawJid);
            this.A00 = 100;
            this.A07 = new Object();
        } catch (AnonymousClass1MW unused) {
            StringBuilder sb = new StringBuilder("invalid jid=");
            sb.append(this.remoteRawJid);
            throw new InvalidObjectException(sb.toString());
        }
    }
}
