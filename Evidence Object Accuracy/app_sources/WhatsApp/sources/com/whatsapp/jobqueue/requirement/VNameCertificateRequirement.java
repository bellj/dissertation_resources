package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1MW;
import X.C22700zV;
import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class VNameCertificateRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C22700zV A00;
    public transient UserJid A01;
    public final String jid;

    public VNameCertificateRequirement(UserJid userJid) {
        AnonymousClass009.A05(userJid);
        this.A01 = userJid;
        String rawString = userJid.getRawString();
        AnonymousClass009.A04(rawString);
        this.jid = rawString;
    }

    public UserJid A00() {
        UserJid userJid = this.A01;
        if (userJid != null) {
            return userJid;
        }
        try {
            UserJid userJid2 = UserJid.get(this.jid);
            this.A01 = userJid2;
            return userJid2;
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        long j;
        Number number;
        UserJid A00 = A00();
        if (A00 == null || (number = (Number) this.A00.A0D.get(A00)) == null) {
            j = 0;
        } else {
            j = number.longValue();
        }
        if (j <= 0 || System.currentTimeMillis() - j >= 3600000) {
            if (GetVNameCertificateJob.A02.containsKey(this.jid)) {
                if (this.A00.A00(A00()) == null) {
                    return false;
                }
                return true;
            }
        }
        A00();
        return true;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C22700zV) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AMN.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.jid)) {
            StringBuilder sb = new StringBuilder("jid must not be empty");
            StringBuilder sb2 = new StringBuilder("; jid=");
            sb2.append(A00());
            sb.append(sb2.toString());
            throw new InvalidObjectException(sb.toString());
        }
    }
}
