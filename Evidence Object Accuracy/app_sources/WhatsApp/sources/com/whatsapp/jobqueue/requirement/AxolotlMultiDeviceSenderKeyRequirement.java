package com.whatsapp.jobqueue.requirement;

import X.AbstractC15590nW;
import X.C15570nT;
import X.C15600nX;
import X.C22830zi;
import java.util.Set;

/* loaded from: classes2.dex */
public class AxolotlMultiDeviceSenderKeyRequirement extends AxolotlMultiDeviceSessionRequirement {
    public transient C15570nT A00;
    public transient C15600nX A01;
    public transient C22830zi A02;
    public final String groupParticipantHash;

    public AxolotlMultiDeviceSenderKeyRequirement(AbstractC15590nW r1, String str, String str2, Set set, boolean z) {
        super(r1, str, set, z);
        this.groupParticipantHash = str2;
    }
}
