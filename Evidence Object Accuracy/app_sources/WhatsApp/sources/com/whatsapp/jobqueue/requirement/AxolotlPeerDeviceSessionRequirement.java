package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C15940oB;
import X.C15990oG;
import X.C18770sz;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public class AxolotlPeerDeviceSessionRequirement implements Requirement, AnonymousClass1LJ {
    public transient C15990oG A00;
    public transient C18770sz A01;
    public final String targetJidRawString;

    public AxolotlPeerDeviceSessionRequirement(DeviceJid deviceJid) {
        this.targetJidRawString = deviceJid.getRawString();
    }

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        DeviceJid nullable = DeviceJid.getNullable(this.targetJidRawString);
        AnonymousClass009.A05(nullable);
        if (this.A01.A06().A00.contains(nullable)) {
            return this.A00.A0g(C15940oB.A02(nullable));
        }
        StringBuilder sb = new StringBuilder("AxolotlDeviceSessionRequirement/isPresent/warning: the specific device is not in db, handle the error in the job. jid=");
        sb.append(this.targetJidRawString);
        Log.w(sb.toString());
        return true;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = (C18770sz) r1.AM8.get();
        this.A00 = r1.A2d();
    }
}
