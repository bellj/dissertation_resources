package com.whatsapp.jobqueue.requirement;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C16240og;
import android.content.Context;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class ChatConnectionRequirement implements Requirement, AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C16240og A00;

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        return this.A00.A04 == 2;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C16240og) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).ANq.get();
    }
}
