package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C14830m7;
import X.C15650ng;
import X.C17030q9;
import X.C20320vZ;
import X.C20660w7;
import X.C20670w8;
import X.C21250x7;
import X.C22230yk;
import X.C27081Fy;
import android.content.Context;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.Locale;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class RehydrateHsmJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient Context A00;
    public transient AbstractC15710nm A01;
    public transient C20670w8 A02;
    public transient C17030q9 A03;
    public transient C14830m7 A04;
    public transient C15650ng A05;
    public transient C21250x7 A06;
    public transient C20660w7 A07;
    public transient C22230yk A08;
    public transient C27081Fy A09;
    public transient C20320vZ A0A;
    public final Long existingMessageRowId;
    public final int expiration;
    public final long expireTimeMs;
    public final String id;
    public final String jid;
    public final Locale[] locales;
    public final String participant;
    public final long timestamp;
    public final int verifiedLevel;
    public final Long verifiedSender;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RehydrateHsmJob(X.AnonymousClass018 r9, X.AbstractC14640lm r10, X.AbstractC14640lm r11, X.C27081Fy r12, X.C20320vZ r13, java.lang.Long r14, java.lang.Long r15, java.lang.String r16, int r17, int r18, long r19, long r21) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.RehydrateHsmJob.<init>(X.018, X.0lm, X.0lm, X.1Fy, X.0vZ, java.lang.Long, java.lang.Long, java.lang.String, int, int, long, long):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public boolean A02() {
        return this.A04.A00() >= this.expireTimeMs || super.A02();
    }

    public final String A04() {
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.jid);
        StringBuilder sb = new StringBuilder("; id=");
        sb.append(this.id);
        sb.append("; jid=");
        sb.append(A01);
        sb.append("; participant=");
        sb.append(this.participant);
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    public final void A05(Integer num) {
        this.A07.A0B(AbstractC14640lm.A01(this.jid), AbstractC14640lm.A01(this.participant), num, this.id, null, null);
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = context.getApplicationContext();
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A04 = r1.Aen();
        this.A01 = r1.A7p();
        this.A07 = (C20660w7) r1.AIB.get();
        this.A06 = (C21250x7) r1.AJh.get();
        this.A02 = (C20670w8) r1.AMw.get();
        this.A08 = (C22230yk) r1.ANT.get();
        this.A0A = (C20320vZ) r1.A7A.get();
        this.A05 = (C15650ng) r1.A4m.get();
        this.A03 = (C17030q9) r1.AAM.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A09 = C27081Fy.A01((byte[]) objectInputStream.readObject());
        } catch (OptionalDataException unused) {
            StringBuilder sb = new StringBuilder("RehydrateHsmJob/readObject/error: missing message bytes ");
            sb.append(A04());
            Log.e(sb.toString());
        }
        if (this.A09 == null) {
            StringBuilder sb2 = new StringBuilder("RehydrateHsmJob/readObject/error: message is null");
            sb2.append(A04());
            Log.e(sb2.toString());
        }
        C27081Fy r0 = this.A09;
        if (r0 != null && (r0.A00 & DefaultCrypto.BUFFER_SIZE) != 8192) {
            StringBuilder sb3 = new StringBuilder("message must contain an HSM");
            sb3.append(A04());
            throw new InvalidObjectException(sb3.toString());
        } else if (this.id == null) {
            StringBuilder sb4 = new StringBuilder("id must not be null");
            sb4.append(A04());
            throw new InvalidObjectException(sb4.toString());
        } else if (this.jid == null) {
            StringBuilder sb5 = new StringBuilder("jid must not be null");
            sb5.append(A04());
            throw new InvalidObjectException(sb5.toString());
        } else if (this.timestamp <= 0) {
            StringBuilder sb6 = new StringBuilder("timestamp must be valid");
            sb6.append(A04());
            throw new InvalidObjectException(sb6.toString());
        } else if (this.expireTimeMs > 0) {
            Locale[] localeArr = this.locales;
            if (localeArr == null || localeArr.length == 0) {
                StringBuilder sb7 = new StringBuilder("locales[] must not be empty");
                sb7.append(A04());
                throw new InvalidObjectException(sb7.toString());
            }
        } else {
            StringBuilder sb8 = new StringBuilder("expireTimeMs must be non-negative");
            sb8.append(A04());
            throw new InvalidObjectException(sb8.toString());
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.A09.A02());
    }
}
