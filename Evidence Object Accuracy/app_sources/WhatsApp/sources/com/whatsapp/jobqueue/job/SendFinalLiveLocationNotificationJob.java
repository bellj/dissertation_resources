package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass13N;
import X.AnonymousClass1LJ;
import X.C15570nT;
import X.C15990oG;
import X.C16030oK;
import X.C17220qS;
import X.C18240s8;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendFinalLiveLocationNotificationJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15570nT A00;
    public transient C15990oG A01;
    public transient C18240s8 A02;
    public transient AnonymousClass13N A03;
    public transient C16030oK A04;
    public transient C17220qS A05;
    public final double latitude;
    public final double longitude;
    public final String msgId;
    public final String rawJid;
    public final int timeOffset;
    public final long timestamp;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendFinalLiveLocationNotificationJob(X.AnonymousClass1IS r6, X.C30751Yr r7, int r8) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.lang.String r0 = "final-live-location-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.0lm r3 = r6.A00
            java.lang.String r0 = X.C15380n4.A03(r3)
            r1.append(r0)
            java.lang.String r2 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            com.whatsapp.jobqueue.requirement.AxolotlFastRatchetSenderKeyRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlFastRatchetSenderKeyRequirement
            r0.<init>()
            r4.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r4, r1)
            r5.<init>(r0)
            boolean r0 = r6.A02
            X.AnonymousClass009.A0F(r0)
            X.AnonymousClass009.A05(r3)
            java.lang.String r0 = r3.getRawString()
            r5.rawJid = r0
            java.lang.String r0 = r6.A01
            r5.msgId = r0
            double r0 = r7.A00
            r5.latitude = r0
            double r0 = r7.A01
            r5.longitude = r0
            long r0 = r7.A05
            r5.timestamp = r0
            r5.timeOffset = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendFinalLiveLocationNotificationJob.<init>(X.1IS, X.1Yr, int):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; persistentId=");
        sb.append(super.A01);
        sb.append("; jid=");
        sb.append(this.rawJid);
        sb.append("; msgId=");
        sb.append(this.msgId);
        sb.append("; location.timestamp=");
        sb.append(this.timestamp);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.A1w();
        this.A05 = r1.A3P();
        this.A02 = (C18240s8) r1.AIt.get();
        this.A03 = (AnonymousClass13N) r1.A1M.get();
        this.A01 = r1.A2d();
        this.A04 = (C16030oK) r1.AAd.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.rawJid)) {
            StringBuilder sb = new StringBuilder("jid must not be empty");
            sb.append(A04());
            throw new InvalidObjectException(sb.toString());
        } else if (TextUtils.isEmpty(this.msgId)) {
            StringBuilder sb2 = new StringBuilder("msgId must not be empty");
            sb2.append(A04());
            throw new InvalidObjectException(sb2.toString());
        } else if (this.timestamp == 0) {
            StringBuilder sb3 = new StringBuilder("location timestamp must not be 0");
            sb3.append(A04());
            throw new InvalidObjectException(sb3.toString());
        }
    }
}
