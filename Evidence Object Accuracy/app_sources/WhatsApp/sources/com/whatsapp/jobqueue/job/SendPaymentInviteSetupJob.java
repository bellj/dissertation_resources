package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1OT;
import X.AnonymousClass1VH;
import X.C17220qS;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SendPaymentInviteSetupJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C17220qS A00;
    public final boolean inviteUsed;
    public final String jidRawStr;
    public final int paymentService;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendPaymentInviteSetupJob(com.whatsapp.jid.UserJid r5, int r6, boolean r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = "SendPaymentInviteSetupJob"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            r4.jidRawStr = r0
            r4.paymentService = r6
            r4.inviteUsed = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendPaymentInviteSetupJob.<init>(com.whatsapp.jid.UserJid, int, boolean):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        StringBuilder sb = new StringBuilder("PAY: starting SendPaymentInviteSetupJob job");
        sb.append(A04());
        Log.i(sb.toString());
        String A02 = this.A00.A02();
        AnonymousClass1VH r1 = new AnonymousClass1VH();
        r1.A01 = UserJid.getNullable(this.jidRawStr);
        r1.A05 = "notification";
        r1.A08 = "pay";
        r1.A07 = A02;
        AnonymousClass1OT A00 = r1.A00();
        Message obtain = Message.obtain(null, 0, 272, 0);
        Bundle data = obtain.getData();
        data.putString("id", A02);
        data.putParcelable("jid", UserJid.getNullable(this.jidRawStr));
        data.putInt("paymentService", this.paymentService);
        data.putBoolean("inviteUsed", this.inviteUsed);
        this.A00.A03(obtain, A00).get();
        StringBuilder sb2 = new StringBuilder("PAY: done SendPaymentInviteSetupJob job");
        sb2.append(A04());
        Log.i(sb2.toString());
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(this.jidRawStr);
        sb.append("; service: ");
        sb.append(this.paymentService);
        sb.append("; inviteUsed: ");
        sb.append(this.inviteUsed);
        sb.append("; persistentId=");
        sb.append(this.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = ((AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class)).A3P();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.jidRawStr)) {
            throw new InvalidObjectException("jid must not be empty");
        } else if (this.paymentService == 0) {
            throw new InvalidObjectException("payment service must not be unknown");
        }
    }
}
