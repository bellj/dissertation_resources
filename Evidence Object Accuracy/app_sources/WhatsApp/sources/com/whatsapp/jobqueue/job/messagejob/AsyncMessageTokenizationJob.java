package com.whatsapp.jobqueue.job.messagejob;

import X.AbstractC15340mz;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.C15240mn;
import android.content.Context;

/* loaded from: classes2.dex */
public final class AsyncMessageTokenizationJob extends AsyncMessageJob {
    public transient C15240mn A00;

    public AsyncMessageTokenizationJob(AbstractC15340mz r5) {
        super(r5.A11, r5.A12);
    }

    @Override // com.whatsapp.jobqueue.job.messagejob.AsyncMessageJob, X.AnonymousClass1LJ
    public void Abz(Context context) {
        super.Abz(context);
        this.A00 = (C15240mn) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A8F.get();
    }
}
