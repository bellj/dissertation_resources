package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17220qS;
import X.C238213d;
import X.C248817g;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SendPlayedReceiptJobV2 extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C248817g A00;
    public transient C17220qS A01;
    public transient C238213d A02;
    public final String[] messageIds;
    public final Long[] messageRowIds;
    public final String participantRawJid;
    public final boolean playedSelfFromPeer;
    public final String toRawJid;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendPlayedReceiptJobV2(X.C32551cL r6, boolean r7) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.lang.String r0 = "played-receipt-v2-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.0lm r3 = r6.A01
            java.lang.String r0 = r3.getRawString()
            X.AnonymousClass009.A05(r0)
            r1.append(r0)
            java.lang.String r2 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r4, r1)
            r5.<init>(r0)
            java.lang.String r0 = r3.getRawString()
            X.AnonymousClass009.A05(r0)
            r5.toRawJid = r0
            X.0lm r0 = r6.A00
            if (r0 != 0) goto L_0x004e
            r0 = 0
        L_0x003b:
            r5.participantRawJid = r0
            java.lang.Long[] r0 = r6.A02
            X.AnonymousClass009.A0H(r0)
            r5.messageRowIds = r0
            java.lang.String[] r0 = r6.A03
            X.AnonymousClass009.A0H(r0)
            r5.messageIds = r0
            r5.playedSelfFromPeer = r7
            return
        L_0x004e:
            java.lang.String r0 = r0.getRawString()
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendPlayedReceiptJobV2.<init>(X.1cL, boolean):void");
    }

    public final String A04() {
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.toRawJid);
        AbstractC14640lm A012 = AbstractC14640lm.A01(this.participantRawJid);
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(A01);
        sb.append("; participant=");
        sb.append(A012);
        sb.append("; id=");
        String[] strArr = this.messageIds;
        sb.append(strArr[0]);
        sb.append("; count=");
        sb.append(strArr.length);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = r1.A3P();
        this.A02 = (C238213d) r1.AHF.get();
        this.A00 = (C248817g) r1.AFm.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (!TextUtils.isEmpty(this.toRawJid)) {
            String[] strArr = this.messageIds;
            if (strArr == null || strArr.length == 0) {
                throw new InvalidObjectException("messageIds must not be empty");
            }
            return;
        }
        throw new InvalidObjectException("toJid must not be empty");
    }
}
