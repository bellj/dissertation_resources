package com.whatsapp.jobqueue.job;

import X.AbstractC14440lR;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C244315m;
import android.content.Context;
import java.util.Random;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public final class DeleteAccountFromHsmServerJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C244315m A00;
    public transient AbstractC14440lR A01;
    public transient Random A02;

    public DeleteAccountFromHsmServerJob(JobParameters jobParameters) {
        super(jobParameters);
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = new Random();
        this.A01 = r1.Ag3();
        this.A00 = (C244315m) r1.A6W.get();
    }
}
