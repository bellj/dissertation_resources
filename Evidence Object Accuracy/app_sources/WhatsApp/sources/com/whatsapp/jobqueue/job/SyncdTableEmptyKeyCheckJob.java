package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C233511i;
import X.C233711k;
import android.content.Context;
import java.util.LinkedList;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public class SyncdTableEmptyKeyCheckJob extends Job implements AnonymousClass1LJ {
    public transient C233711k A00;
    public transient C233511i A01;

    public SyncdTableEmptyKeyCheckJob() {
        super(new JobParameters("syncd-table-empty-key-check", new LinkedList(), true));
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = (C233511i) r1.ALA.get();
        this.A00 = (C233711k) r1.ALC.get();
    }
}
