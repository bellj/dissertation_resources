package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1IS;
import X.AnonymousClass1LJ;
import X.C15380n4;
import X.C239513q;
import X.C32141bg;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import java.util.LinkedList;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public final class ReceiptProcessingJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C239513q A00;
    public final boolean[] keyFromMe;
    public final String[] keyId;
    public final String[] keyRemoteChatJidRawString;
    public final String participantDeviceJidRawString;
    public final C32141bg receiptPrivacyMode;
    public final String remoteJidRawString;
    public final int status;
    public final long timestamp;

    public ReceiptProcessingJob(DeviceJid deviceJid, Jid jid, C32141bg r10, AnonymousClass1IS[] r11, int i, long j) {
        super(new JobParameters("ReceiptProcessingGroup", new LinkedList(), true));
        int length = r11.length;
        String[] strArr = new String[length];
        this.keyId = strArr;
        boolean[] zArr = new boolean[length];
        this.keyFromMe = zArr;
        String[] strArr2 = new String[length];
        this.keyRemoteChatJidRawString = strArr2;
        for (int i2 = 0; i2 < length; i2++) {
            strArr[i2] = r11[i2].A01;
            AnonymousClass1IS r1 = r11[i2];
            zArr[i2] = r1.A02;
            strArr2[i2] = C15380n4.A03(r1.A00);
        }
        this.remoteJidRawString = jid.getRawString();
        this.participantDeviceJidRawString = C15380n4.A03(deviceJid);
        this.status = i;
        this.timestamp = j;
        this.receiptPrivacyMode = r10;
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; remoteJid=");
        sb.append(Jid.getNullable(this.remoteJidRawString));
        sb.append("; number of keys=");
        sb.append(this.keyId.length);
        sb.append("; receiptPrivacyMode=");
        sb.append(this.receiptPrivacyMode);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C239513q) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).ACV.get();
    }
}
