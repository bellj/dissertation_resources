package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1OT;
import X.AnonymousClass1VH;
import X.C16030oK;
import X.C17220qS;
import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SendDisableLiveLocationJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C16030oK A00;
    public transient C17220qS A01;
    public final String rawJid;
    public final long sequenceNumber;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendDisableLiveLocationJob(X.AbstractC14640lm r5, long r6) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = r5.getRawString()
            r1 = 1
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            X.AnonymousClass009.A0E(r1)
            java.lang.String r0 = r5.getRawString()
            r4.rawJid = r0
            r4.sequenceNumber = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendDisableLiveLocationJob.<init>(X.0lm, long):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        StringBuilder sb;
        String str;
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.rawJid);
        if (A01 == null) {
            StringBuilder sb2 = new StringBuilder("skip disable live location job; invalid jid: ");
            sb2.append(this.rawJid);
            Log.e(sb2.toString());
            return;
        }
        if (this.A00.A0f(A01)) {
            sb = new StringBuilder();
            str = "skip disable live location job; sharing is currently enabled";
        } else {
            StringBuilder sb3 = new StringBuilder("starting disable live location job");
            sb3.append(A04());
            Log.i(sb3.toString());
            String A02 = this.A01.A02();
            AnonymousClass1VH r1 = new AnonymousClass1VH();
            r1.A01 = A01;
            r1.A05 = "notification";
            r1.A08 = "location";
            r1.A07 = A02;
            AnonymousClass1OT A00 = r1.A00();
            C17220qS r6 = this.A01;
            AbstractC14640lm A012 = AbstractC14640lm.A01(this.rawJid);
            long j = this.sequenceNumber;
            Message obtain = Message.obtain(null, 0, 81, 0);
            obtain.getData().putString("id", A02);
            obtain.getData().putParcelable("jid", A012);
            obtain.getData().putLong("seq", j);
            r6.A03(obtain, A00).get();
            sb = new StringBuilder();
            str = "done disable live location job";
        }
        sb.append(str);
        sb.append(A04());
        Log.i(sb.toString());
    }

    public final String A04() {
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.rawJid);
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(A01);
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A01 = r1.A3P();
        this.A00 = (C16030oK) r1.AAd.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.rawJid)) {
            throw new InvalidObjectException("jid must not be empty");
        }
    }
}
