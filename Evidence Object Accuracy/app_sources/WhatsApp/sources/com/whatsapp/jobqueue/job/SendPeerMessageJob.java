package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass101;
import X.AnonymousClass16U;
import X.AnonymousClass1LJ;
import X.C14840m8;
import X.C14850m9;
import X.C15570nT;
import X.C15990oG;
import X.C17220qS;
import X.C18240s8;
import X.C18770sz;
import X.C22090yV;
import X.C22300yr;
import X.C26601Ec;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SendPeerMessageJob extends Job implements AnonymousClass1LJ {
    public static final DeviceJid[] A0D = new DeviceJid[0];
    public static final long serialVersionUID = 1;
    public transient C15570nT A00;
    public transient AnonymousClass101 A01;
    public transient AnonymousClass16U A02;
    public transient C15990oG A03;
    public transient C18240s8 A04;
    public transient C22300yr A05;
    public transient C18770sz A06;
    public transient C22090yV A07;
    public transient C14850m9 A08;
    public transient C17220qS A09;
    public transient C14840m8 A0A;
    public transient C26601Ec A0B;
    public final transient byte[] A0C;
    public final long peerMessageRowId;
    public final int retryCount;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendPeerMessageJob(com.whatsapp.jid.DeviceJid r5, X.AbstractC30271Wt r6, byte[] r7, int r8) {
        /*
            r4 = this;
            byte r2 = r6.A0y
            r0 = 35
            if (r2 == r0) goto L_0x006c
            r0 = 47
            if (r2 == r0) goto L_0x0068
            r0 = 50
            if (r2 == r0) goto L_0x0064
            r0 = 38
            if (r2 == r0) goto L_0x0060
            r0 = 39
            if (r2 != r0) goto L_0x006f
            java.lang.String r0 = "syncd-key-request"
        L_0x0019:
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r0)
            java.lang.String r0 = "-"
            r2.append(r0)
            long r0 = r6.A11
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            com.whatsapp.jobqueue.requirement.AxolotlPeerDeviceSessionRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlPeerDeviceSessionRequirement
            r0.<init>(r5)
            r3.add(r0)
            if (r7 == 0) goto L_0x004e
            com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement
            r0.<init>(r5, r7)
            r3.add(r0)
        L_0x004e:
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            long r0 = r6.A11
            r4.peerMessageRowId = r0
            r4.A0C = r7
            r4.retryCount = r8
            return
        L_0x0060:
            java.lang.String r0 = "syncd-key-share"
            goto L_0x0019
        L_0x0064:
            java.lang.String r0 = "syncd-fatal-exception-notification"
            goto L_0x0019
        L_0x0068:
            java.lang.String r0 = "sync-security-settings"
            goto L_0x0019
        L_0x006c:
            java.lang.String r0 = "device-history-sync-notification"
            goto L_0x0019
        L_0x006f:
            java.lang.String r1 = "Cannot send message of type "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendPeerMessageJob.<init>(com.whatsapp.jid.DeviceJid, X.1Wt, byte[], int):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; peer_msg_row_id=");
        sb.append(this.peerMessageRowId);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A08 = r1.A3L();
        this.A00 = r1.A1w();
        this.A09 = r1.A3P();
        this.A04 = (C18240s8) r1.AIt.get();
        this.A05 = (C22300yr) r1.AMv.get();
        this.A07 = (C22090yV) r1.AFV.get();
        this.A03 = r1.A2d();
        this.A06 = (C18770sz) r1.AM8.get();
        this.A0A = (C14840m8) r1.ACi.get();
        this.A01 = (AnonymousClass101) r1.AFt.get();
        this.A0B = (C26601Ec) r1.A7D.get();
        this.A02 = (AnonymousClass16U) r1.AFQ.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
    }
}
