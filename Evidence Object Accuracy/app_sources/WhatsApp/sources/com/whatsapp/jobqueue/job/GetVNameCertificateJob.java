package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass101;
import X.AnonymousClass1LJ;
import X.AnonymousClass1MW;
import X.C17220qS;
import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.concurrent.ConcurrentHashMap;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class GetVNameCertificateJob extends Job implements AnonymousClass1LJ {
    public static final ConcurrentHashMap A02 = new ConcurrentHashMap();
    public static final long serialVersionUID = 1;
    public transient AnonymousClass101 A00;
    public transient C17220qS A01;
    public final String jid;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GetVNameCertificateJob(com.whatsapp.jid.UserJid r5) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = r5.getRawString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r5)
            X.AnonymousClass009.A05(r1)
            com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement
            r0.<init>(r1)
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            X.AnonymousClass009.A04(r0)
            r4.jid = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.GetVNameCertificateJob.<init>(com.whatsapp.jid.UserJid):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(UserJid.getNullable(this.jid));
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = r1.A3P();
        this.A00 = (AnonymousClass101) r1.AFt.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (!TextUtils.isEmpty(this.jid)) {
            try {
                UserJid.get(this.jid);
                A02.put(this.jid, Boolean.TRUE);
            } catch (AnonymousClass1MW unused) {
                StringBuilder sb = new StringBuilder("jid must be an individual jid; jid=");
                sb.append(this.jid);
                throw new InvalidObjectException(sb.toString());
            }
        } else {
            throw new InvalidObjectException("jid must not be empty");
        }
    }
}
