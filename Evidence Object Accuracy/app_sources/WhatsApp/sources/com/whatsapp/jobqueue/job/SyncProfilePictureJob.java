package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C15380n4;
import X.C20730wE;
import android.content.Context;
import com.whatsapp.jid.UserJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SyncProfilePictureJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C20730wE A00;
    public final String[] jids;
    public final int type;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SyncProfilePictureJob(com.whatsapp.jid.UserJid[] r5, int r6) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            X.AnonymousClass009.A0H(r5)
            int r3 = r5.length
            r2 = 0
        L_0x001c:
            if (r2 >= r3) goto L_0x0028
            r1 = r5[r2]
            java.lang.String r0 = "an element of jids was empty."
            X.AnonymousClass009.A06(r1, r0)
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0028:
            java.util.List r0 = java.util.Arrays.asList(r5)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            r4.jids = r0
            r4.type = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SyncProfilePictureJob.<init>(com.whatsapp.jid.UserJid[], int):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; jids=");
        sb.append(C15380n4.A05(this.jids));
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C20730wE) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A4J.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        int length;
        objectInputStream.defaultReadObject();
        String[] strArr = this.jids;
        if (strArr == null || (length = strArr.length) == 0) {
            throw new InvalidObjectException("jids must not be empty");
        }
        int i = 0;
        while (UserJid.getNullable(strArr[i]) != null) {
            i++;
            if (i >= length) {
                return;
            }
        }
        throw new InvalidObjectException("an jid is not a UserJid");
    }
}
