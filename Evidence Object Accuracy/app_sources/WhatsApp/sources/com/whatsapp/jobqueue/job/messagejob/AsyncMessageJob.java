package com.whatsapp.jobqueue.job.messagejob;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C16370ot;
import X.C16490p7;
import android.content.Context;
import java.util.LinkedList;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public abstract class AsyncMessageJob extends Job implements AnonymousClass1LJ {
    public transient C16370ot A00;
    public transient C16490p7 A01;
    public final long rowId;
    public final long sortId;

    public AsyncMessageJob(long j, long j2) {
        super(new JobParameters("async-message", new LinkedList(), true));
        this.rowId = j;
        this.sortId = j2;
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = (C16370ot) r1.A2b.get();
        this.A01 = (C16490p7) r1.ACJ.get();
    }
}
