package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C14830m7;
import X.C17220qS;
import X.C17230qT;
import X.C238213d;
import android.content.Context;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendReadReceiptJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C14830m7 A00;
    public transient C17220qS A01;
    public transient C238213d A02;
    public transient C17230qT A03;
    public final String jid;
    public final long loggableStanzaId;
    public final String[] messageIds;
    public final long originalMessageTimestamp;
    public final String participant;
    public final String remoteSender;
    public final boolean shouldForceReadSelfReceipt;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendReadReceiptJob(X.AbstractC14640lm r6, X.AbstractC14640lm r7, com.whatsapp.jid.DeviceJid r8, java.lang.String[] r9, long r10, long r12, boolean r14) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.lang.String r0 = "read-receipt-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r6.getRawString()
            r1.append(r0)
            java.lang.String r0 = "-"
            r1.append(r0)
            java.lang.String r0 = X.C15380n4.A03(r7)
            r1.append(r0)
            java.lang.String r3 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            r2 = 1
            r1 = 0
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r3, r4, r2)
            r5.<init>(r0)
            java.lang.String r0 = r6.getRawString()
            X.AnonymousClass009.A05(r0)
            r5.jid = r0
            r0 = 0
            if (r7 == 0) goto L_0x0045
            java.lang.String r1 = r7.getRawString()
        L_0x0045:
            r5.participant = r1
            if (r8 == 0) goto L_0x004d
            java.lang.String r0 = r8.getRawString()
        L_0x004d:
            r5.remoteSender = r0
            X.AnonymousClass009.A0H(r9)
            r5.messageIds = r9
            r5.originalMessageTimestamp = r10
            r5.shouldForceReadSelfReceipt = r14
            r5.loggableStanzaId = r12
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendReadReceiptJob.<init>(X.0lm, X.0lm, com.whatsapp.jid.DeviceJid, java.lang.String[], long, long, boolean):void");
    }

    public final String A04() {
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.jid);
        AbstractC14640lm A012 = AbstractC14640lm.A01(this.participant);
        DeviceJid nullable = DeviceJid.getNullable(this.remoteSender);
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(A01);
        sb.append("; participant=");
        sb.append(A012);
        sb.append("; remoteSender=");
        sb.append(nullable);
        sb.append("; shouldForceReadSelfReceipt=");
        sb.append(this.shouldForceReadSelfReceipt);
        sb.append("; ids:");
        sb.append(Arrays.deepToString(this.messageIds));
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.Aen();
        this.A01 = r1.A3P();
        this.A02 = (C238213d) r1.AHF.get();
        this.A03 = (C17230qT) r1.AAh.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (Jid.getNullable(this.jid) != null) {
            String[] strArr = this.messageIds;
            if (strArr == null || strArr.length == 0) {
                throw new InvalidObjectException("messageIds must not be empty");
            }
            return;
        }
        throw new InvalidObjectException("jid must not be empty");
    }
}
