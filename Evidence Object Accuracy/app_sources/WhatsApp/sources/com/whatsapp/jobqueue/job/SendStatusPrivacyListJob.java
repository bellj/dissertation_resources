package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C14860mA;
import X.C15380n4;
import X.C17220qS;
import X.C862246h;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendStatusPrivacyListJob extends Job implements AnonymousClass1LJ {
    public static volatile long A02 = 0;
    public static final long serialVersionUID = 1;
    public transient C17220qS A00;
    public transient C14860mA A01;
    public final Collection jids;
    public final int statusDistribution;
    public final String webId;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendStatusPrivacyListJob(java.lang.String r5, java.util.Collection r6, int r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = "SendStatusPrivacyListJob"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            r4.statusDistribution = r7
            if (r6 != 0) goto L_0x0022
            r0 = 0
        L_0x001d:
            r4.jids = r0
            r4.webId = r5
            return
        L_0x0022:
            java.util.ArrayList r0 = X.C15380n4.A06(r6)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendStatusPrivacyListJob.<init>(java.lang.String, java.util.Collection, int):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        ArrayList<? extends Parcelable> arrayList;
        if (A02 != super.A01) {
            StringBuilder sb = new StringBuilder("skip send status privacy job");
            sb.append(A04());
            sb.append("; lastJobId=");
            sb.append(A02);
            Log.i(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder("run send status privacy job");
        sb2.append(A04());
        Log.i(sb2.toString());
        AtomicInteger atomicInteger = new AtomicInteger();
        String str = this.webId;
        if (str == null) {
            str = this.A00.A01();
        }
        C17220qS r6 = this.A00;
        int i = this.statusDistribution;
        Collection collection = this.jids;
        if (collection == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList<>();
            C15380n4.A0C(UserJid.class, collection, arrayList);
        }
        Message obtain = Message.obtain(null, 0, 120, 0, new C862246h(this, atomicInteger));
        Bundle data = obtain.getData();
        data.putString("id", str);
        data.putInt("statusDistributionMode", i);
        if (arrayList != null) {
            data.putParcelableArrayList("jids", arrayList);
        }
        r6.A04(obtain, str, false).get();
        int i2 = atomicInteger.get();
        if (i2 == 500) {
            StringBuilder sb3 = new StringBuilder("server 500 error during send status privacy job");
            sb3.append(A04());
            throw new Exception(sb3.toString());
        } else if (i2 != 0) {
            StringBuilder sb4 = new StringBuilder("server error code returned during send status privacy job; errorCode=");
            sb4.append(i2);
            sb4.append(A04());
            Log.w(sb4.toString());
        }
    }

    public final String A04() {
        String arrays;
        Jid nullable;
        StringBuilder sb = new StringBuilder("; statusDistribution=");
        sb.append(this.statusDistribution);
        sb.append("; jids=");
        Collection<String> collection = this.jids;
        if (collection == null) {
            arrays = "null";
        } else {
            ArrayList arrayList = new ArrayList(collection.size());
            for (String str : collection) {
                if (!(str == null || (nullable = Jid.getNullable(str)) == null)) {
                    arrayList.add(nullable);
                }
            }
            arrays = Arrays.toString(arrayList.toArray());
        }
        sb.append(arrays);
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A01 = (C14860mA) r1.ANU.get();
        this.A00 = r1.A3P();
    }
}
