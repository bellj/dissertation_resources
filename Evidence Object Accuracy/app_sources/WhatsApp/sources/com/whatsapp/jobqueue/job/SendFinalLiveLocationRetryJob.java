package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass101;
import X.AnonymousClass1LJ;
import X.C15570nT;
import X.C15990oG;
import X.C17220qS;
import X.C18240s8;
import X.C244415n;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendFinalLiveLocationRetryJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15570nT A00;
    public transient AnonymousClass101 A01;
    public transient C15990oG A02;
    public transient C18240s8 A03;
    public transient C244415n A04;
    public transient C17220qS A05;
    public final String contextRawJid;
    public final double latitude;
    public final double longitude;
    public final String msgId;
    public final String rawDeviceJid;
    public final int retryCount;
    public final int timeOffset;
    public final long timestamp;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendFinalLiveLocationRetryJob(com.whatsapp.jid.DeviceJid r5, X.AnonymousClass1IS r6, X.C30751Yr r7, byte[] r8, int r9, int r10) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            if (r8 == 0) goto L_0x0012
            int r0 = r8.length
            if (r0 != 0) goto L_0x0012
            java.lang.String r1 = "cannot use empty old alice base key"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0012:
            com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement
            r0.<init>(r5)
            r3.add(r0)
            if (r8 == 0) goto L_0x0024
            com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement
            r0.<init>(r5, r8)
            r3.add(r0)
        L_0x0024:
            java.lang.String r1 = "final-live-location-"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r5)
            java.lang.String r2 = r0.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            r0 = 0
            if (r10 <= 0) goto L_0x0047
            r0 = 1
        L_0x0047:
            X.AnonymousClass009.A0F(r0)
            java.lang.String r0 = r5.getRawString()
            r4.rawDeviceJid = r0
            X.0lm r1 = r6.A00
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = X.C15380n4.A03(r1)
        L_0x005c:
            r4.contextRawJid = r0
            java.lang.String r0 = r6.A01
            r4.msgId = r0
            double r0 = r7.A00
            r4.latitude = r0
            double r0 = r7.A01
            r4.longitude = r0
            long r0 = r7.A05
            r4.timestamp = r0
            r4.timeOffset = r9
            r4.retryCount = r10
            return
        L_0x0073:
            r0 = 0
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendFinalLiveLocationRetryJob.<init>(com.whatsapp.jid.DeviceJid, X.1IS, X.1Yr, byte[], int, int):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; persistentId=");
        sb.append(super.A01);
        sb.append("; jid=");
        sb.append(this.rawDeviceJid);
        sb.append("; msgId=");
        sb.append(this.msgId);
        sb.append("; location.timestamp=");
        sb.append(this.timestamp);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.A1w();
        this.A04 = (C244415n) r1.AAg.get();
        this.A05 = r1.A3P();
        this.A03 = (C18240s8) r1.AIt.get();
        this.A02 = r1.A2d();
        this.A01 = (AnonymousClass101) r1.AFt.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.rawDeviceJid)) {
            StringBuilder sb = new StringBuilder("jid must not be empty");
            sb.append(A04());
            throw new InvalidObjectException(sb.toString());
        } else if (TextUtils.isEmpty(this.msgId)) {
            StringBuilder sb2 = new StringBuilder("msgId must not be empty");
            sb2.append(A04());
            throw new InvalidObjectException(sb2.toString());
        } else if (this.timestamp == 0) {
            StringBuilder sb3 = new StringBuilder("location timestamp must not be 0");
            sb3.append(A04());
            throw new InvalidObjectException(sb3.toString());
        }
    }
}
