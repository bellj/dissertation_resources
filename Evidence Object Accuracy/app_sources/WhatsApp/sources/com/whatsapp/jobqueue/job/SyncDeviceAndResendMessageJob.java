package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1E7;
import X.AnonymousClass1IS;
import X.AnonymousClass1LJ;
import X.C14830m7;
import X.C15380n4;
import X.C16370ot;
import X.C20730wE;
import X.C20740wF;
import X.C20870wS;
import X.C21400xM;
import X.C22830zi;
import X.C22840zj;
import X.C242914y;
import android.content.Context;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.requirement.ChatConnectionRequirement;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public class SyncDeviceAndResendMessageJob extends Job implements AnonymousClass1LJ {
    public transient int A00;
    public transient AbstractC15710nm A01;
    public transient C20870wS A02;
    public transient C20730wE A03;
    public transient AnonymousClass1E7 A04;
    public transient C14830m7 A05;
    public transient C16370ot A06;
    public transient C242914y A07;
    public transient C22830zi A08;
    public transient C21400xM A09;
    public transient C22840zj A0A;
    public transient C20740wF A0B;
    public transient AnonymousClass1IS A0C;
    public transient Boolean A0D;
    public transient Set A0E = new HashSet();
    public final long expirationMs;
    public final String messageId;
    public final String messageRawChatJid;
    public final String[] rawUserJids;
    public final long startTimeMs;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SyncDeviceAndResendMessageJob(X.AnonymousClass1IS r6, com.whatsapp.jid.UserJid[] r7, long r8, long r10, boolean r12) {
        /*
            r5 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement r0 = new com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r5.<init>(r0)
            X.AnonymousClass009.A0H(r7)
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r5.A0E = r0
            int r4 = r7.length
            r3 = 0
        L_0x002b:
            if (r3 >= r4) goto L_0x003c
            r2 = r7[r3]
            java.util.Set r1 = r5.A0E
            java.lang.String r0 = "invalid jid"
            X.AnonymousClass009.A06(r2, r0)
            r1.add(r2)
            int r3 = r3 + 1
            goto L_0x002b
        L_0x003c:
            r5.A0C = r6
            java.util.List r0 = java.util.Arrays.asList(r7)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            r5.rawUserJids = r0
            java.lang.String r0 = r6.A01
            r5.messageId = r0
            X.0lm r0 = r6.A00
            X.AnonymousClass009.A05(r0)
            java.lang.String r0 = r0.getRawString()
            r5.messageRawChatJid = r0
            r5.expirationMs = r10
            r5.startTimeMs = r8
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r12)
            r5.A0D = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SyncDeviceAndResendMessageJob.<init>(X.1IS, com.whatsapp.jid.UserJid[], long, long, boolean):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public boolean A02() {
        for (Requirement requirement : this.parameters.requirements) {
            if (!requirement.AJu()) {
                StringBuilder sb = new StringBuilder("SyncDeviceAndResendMessageJob/isRequirementsMet/req ");
                sb.append(requirement);
                sb.append(" not present: ");
                sb.append(A04());
                Log.e(sb.toString());
                if (requirement instanceof ChatConnectionRequirement) {
                    this.A00 = 1;
                    return false;
                }
                this.A00 = 2;
                return false;
            }
        }
        return true;
    }

    public String A04() {
        StringBuilder sb = new StringBuilder("; key=");
        sb.append(this.A0C);
        sb.append("; timeoutMs=");
        sb.append(this.expirationMs);
        sb.append("; rawJids=");
        sb.append(this.A0E);
        sb.append("; offlineInProgressDuringMessageSend=");
        sb.append(this.A0D);
        return sb.toString();
    }

    public void A05(int i) {
        AbstractC15340mz A03 = this.A06.A03(this.A0C);
        if (A03 != null || (A03 = this.A09.A02(this.A0C)) != null) {
            Set A00 = this.A08.A00(this.A0C);
            this.A02.A0F(A03, i, 1, C15380n4.A09(this.A01, A00).size(), A00.size(), this.A05.A00() - this.startTimeMs, false, false, true);
        }
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A05 = r1.Aen();
        this.A01 = r1.A7p();
        this.A02 = (C20870wS) r1.AC2.get();
        this.A06 = (C16370ot) r1.A2b.get();
        this.A03 = (C20730wE) r1.A4J.get();
        this.A0B = (C20740wF) r1.AIA.get();
        this.A09 = (C21400xM) r1.ABd.get();
        this.A08 = (C22830zi) r1.AHH.get();
        this.A04 = (AnonymousClass1E7) r1.A5n.get();
        this.A0A = (C22840zj) r1.AG0.get();
        this.A07 = (C242914y) r1.ABu.get();
        this.A04.A01(this.A0C);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        String[] strArr = this.rawUserJids;
        if (strArr == null || (r5 = strArr.length) == 0) {
            throw new InvalidObjectException("rawJids must not be empty");
        }
        this.A0E = new HashSet();
        for (String str : strArr) {
            UserJid nullable = UserJid.getNullable(str);
            if (nullable != null) {
                this.A0E.add(nullable);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("invalid jid:");
                sb.append(str);
                throw new InvalidObjectException(sb.toString());
            }
        }
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.messageRawChatJid);
        if (A01 != null) {
            this.A0C = new AnonymousClass1IS(A01, this.messageId, true);
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("invalid jid:");
        sb2.append(this.messageRawChatJid);
        throw new InvalidObjectException(sb2.toString());
    }
}
