package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass101;
import X.AnonymousClass1LJ;
import X.C15570nT;
import X.C15990oG;
import X.C16030oK;
import X.C17220qS;
import X.C18240s8;
import android.content.Context;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendLiveLocationKeyJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15570nT A00;
    public transient AnonymousClass101 A01;
    public transient C15990oG A02;
    public transient C18240s8 A03;
    public transient C16030oK A04;
    public transient C17220qS A05;
    public final ArrayList rawJids;
    public final Integer retryCount;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendLiveLocationKeyJob(com.whatsapp.jid.DeviceJid r5, byte[] r6, int r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            if (r6 == 0) goto L_0x0012
            int r0 = r6.length
            if (r0 != 0) goto L_0x0012
            java.lang.String r1 = "cannot use empty old alice base key"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0012:
            com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement
            r0.<init>(r5)
            r3.add(r0)
            if (r6 == 0) goto L_0x0024
            com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement
            r0.<init>(r5, r6)
            r3.add(r0)
        L_0x0024:
            java.lang.String r2 = "SendLiveLocationKeyJob"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            if (r7 < 0) goto L_0x0052
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r4.rawJids = r1
            com.whatsapp.jid.UserJid r0 = r5.getUserJid()
            java.lang.String r0 = r0.getRawString()
            r1.add(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r4.retryCount = r0
            return
        L_0x0052:
            java.lang.String r0 = "retryCount cannot be negative"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r4.A04()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendLiveLocationKeyJob.<init>(com.whatsapp.jid.DeviceJid, byte[], int):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendLiveLocationKeyJob(java.util.List r6) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.util.Iterator r2 = r6.iterator()
        L_0x0009:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0027
            java.lang.Object r0 = r2.next()
            com.whatsapp.jid.Jid r0 = (com.whatsapp.jid.Jid) r0
            if (r0 == 0) goto L_0x0009
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r0)
            X.AnonymousClass009.A05(r1)
            com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement r0 = new com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement
            r0.<init>(r1)
            r4.add(r0)
            goto L_0x0009
        L_0x0027:
            java.lang.String r3 = "SendLiveLocationKeyJob"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            r2 = 1
            r1 = 0
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r3, r4, r2)
            r5.<init>(r0)
            java.lang.String r0 = ""
            X.AnonymousClass009.A09(r0, r6)
            java.util.ArrayList r0 = X.C15380n4.A06(r6)
            r5.rawJids = r0
            r5.retryCount = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendLiveLocationKeyJob.<init>(java.util.List):void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:39:0x0110 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r2v6, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r2v9, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.whispersystems.jobqueue.Job
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 627
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendLiveLocationKeyJob.A03():void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; persistentId=");
        sb.append(super.A01);
        sb.append("; jids.size()=");
        sb.append(this.rawJids.size());
        sb.append("; retryCount=");
        sb.append(this.retryCount);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.A1w();
        this.A05 = r1.A3P();
        this.A03 = (C18240s8) r1.AIt.get();
        this.A02 = r1.A2d();
        this.A01 = (AnonymousClass101) r1.AFt.get();
        this.A04 = (C16030oK) r1.AAd.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        ArrayList arrayList = this.rawJids;
        if (arrayList == null || arrayList.isEmpty()) {
            StringBuilder sb = new StringBuilder("jids must not be empty");
            sb.append(A04());
            throw new InvalidObjectException(sb.toString());
        }
        Integer num = this.retryCount;
        if (num != null && num.intValue() < 0) {
            StringBuilder sb2 = new StringBuilder("retryCount cannot be negative");
            sb2.append(A04());
            throw new InvalidObjectException(sb2.toString());
        }
    }
}
