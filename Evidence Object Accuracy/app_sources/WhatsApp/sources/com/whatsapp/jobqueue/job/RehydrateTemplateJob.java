package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1US;
import X.AnonymousClass43X;
import X.C14830m7;
import X.C15450nH;
import X.C15650ng;
import X.C16120oU;
import X.C17030q9;
import X.C18790t3;
import X.C18810t5;
import X.C20320vZ;
import X.C20660w7;
import X.C20670w8;
import X.C27081Fy;
import android.content.Context;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.Locale;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class RehydrateTemplateJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient Context A00;
    public transient AbstractC15710nm A01;
    public transient C15450nH A02;
    public transient C18790t3 A03;
    public transient C20670w8 A04;
    public transient C17030q9 A05;
    public transient C14830m7 A06;
    public transient C15650ng A07;
    public transient C16120oU A08;
    public transient C18810t5 A09;
    public transient C20660w7 A0A;
    public transient C27081Fy A0B;
    public final transient C20320vZ A0C;
    public final int expiration;
    public final long expireTimeMs;
    public final String id;
    public final String jid;
    public final Locale[] locales;
    public final String participant;
    public final long timestamp;
    public final int verifiedLevel;
    public final Long verifiedSender;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RehydrateTemplateJob(X.AnonymousClass018 r10, X.AbstractC14640lm r11, X.AbstractC14640lm r12, X.C27081Fy r13, X.C20320vZ r14, java.lang.Long r15, java.lang.String r16, int r17, int r18, long r19, long r21) {
        /*
        // Method dump skipped, instructions count: 348
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.RehydrateTemplateJob.<init>(X.018, X.0lm, X.0lm, X.1Fy, X.0vZ, java.lang.Long, java.lang.String, int, int, long, long):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public boolean A02() {
        return this.A06.A00() >= this.expireTimeMs || super.A02();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x03dc, code lost:
        if (r4 != 0) goto L_0x03de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0402, code lost:
        if (r6 != false) goto L_0x0404;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x0469, code lost:
        if (r1 != false) goto L_0x03dc;
     */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0265 A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x02a9 A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02bb A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x02d6 A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02ec A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02ef A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02f2 A[Catch: HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, TRY_LEAVE, TryCatch #3 {HSMRehydrationUtil$SendStructUnavailableException -> 0x0342, blocks: (B:95:0x01fb, B:97:0x0203, B:98:0x0209, B:106:0x0217, B:113:0x0224, B:115:0x0229, B:117:0x022d, B:120:0x0237, B:123:0x0241, B:127:0x024c, B:128:0x024e, B:130:0x0265, B:132:0x0269, B:134:0x026e, B:137:0x0278, B:141:0x0283, B:142:0x0285, B:146:0x029a, B:148:0x029e, B:149:0x02a4, B:151:0x02a9, B:152:0x02ad, B:154:0x02bb, B:155:0x02bf, B:157:0x02d6, B:158:0x02da, B:159:0x02ec, B:160:0x02ef, B:161:0x02f2, B:167:0x0320), top: B:264:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0579  */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x01c8 A[SYNTHETIC] */
    @Override // org.whispersystems.jobqueue.Job
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 1494
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.RehydrateTemplateJob.A03():void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; id=");
        sb.append(this.id);
        sb.append("; jid=");
        sb.append(this.jid);
        sb.append("; participant=");
        sb.append(this.participant);
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    public final String A05(String str, int i, int i2, int i3) {
        String A04 = AnonymousClass1US.A04(i, str);
        if (str != null && !str.equals(A04)) {
            AnonymousClass43X r2 = new AnonymousClass43X();
            r2.A02 = Long.valueOf((long) str.length());
            r2.A00 = Integer.valueOf(i2);
            if (i2 == 4) {
                r2.A01 = Long.valueOf((long) i3);
            }
            this.A08.A06(r2);
        }
        return A04;
    }

    public final void A06(Integer num, String str, String str2) {
        this.A0A.A0B(AbstractC14640lm.A01(this.jid), AbstractC14640lm.A01(this.participant), num, this.id, str, str2);
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = context.getApplicationContext();
        this.A06 = r1.Aen();
        this.A01 = r1.A7p();
        this.A03 = (C18790t3) r1.AJw.get();
        this.A08 = r1.Ag5();
        this.A0A = (C20660w7) r1.AIB.get();
        this.A02 = (C15450nH) r1.AII.get();
        this.A04 = (C20670w8) r1.AMw.get();
        this.A07 = (C15650ng) r1.A4m.get();
        this.A05 = (C17030q9) r1.AAM.get();
        this.A09 = (C18810t5) r1.AMu.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A0B = C27081Fy.A01((byte[]) objectInputStream.readObject());
        } catch (OptionalDataException unused) {
            StringBuilder sb = new StringBuilder("RehydrateTemplateJob/readObject/error hsm missing message bytes, loggableParam=");
            sb.append(A04());
            Log.e(sb.toString());
        }
        if (this.A0B == null) {
            StringBuilder sb2 = new StringBuilder("RehydrateTemplateJob/readObject/error message is null, loggableParam=");
            sb2.append(A04());
            Log.e(sb2.toString());
        }
        if (this.id == null) {
            StringBuilder sb3 = new StringBuilder("id must not be null");
            sb3.append(A04());
            throw new InvalidObjectException(sb3.toString());
        } else if (this.jid == null) {
            StringBuilder sb4 = new StringBuilder("jid must not be null");
            sb4.append(A04());
            throw new InvalidObjectException(sb4.toString());
        } else if (this.timestamp <= 0) {
            StringBuilder sb5 = new StringBuilder("timestamp must be valid");
            sb5.append(A04());
            throw new InvalidObjectException(sb5.toString());
        } else if (this.expireTimeMs <= 0) {
            StringBuilder sb6 = new StringBuilder("expireTimeMs must be non-negative");
            sb6.append(A04());
            throw new InvalidObjectException(sb6.toString());
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.A0B.A02());
    }
}
