package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17220qS;
import X.C18780t0;
import X.C22850zk;
import android.content.Context;
import com.whatsapp.jid.UserJid;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class GenerateTcTokenJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C18780t0 A00;
    public transient UserJid A01;
    public transient C17220qS A02;
    public transient C22850zk A03;
    public transient boolean A04 = false;
    public final Long timestamp;
    public final String toJid;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GenerateTcTokenJob(com.whatsapp.jid.UserJid r5, java.lang.Long r6) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r0 = "generate-tc-token-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            r1.append(r0)
            java.lang.String r2 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            r0 = 0
            r4.A04 = r0
            java.lang.String r0 = r5.getRawString()
            r4.toJid = r0
            r4.timestamp = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.GenerateTcTokenJob.<init>(com.whatsapp.jid.UserJid, java.lang.Long):void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = r1.A3P();
        this.A00 = (C18780t0) r1.ALp.get();
        this.A03 = (C22850zk) r1.AG8.get();
        UserJid nullable = UserJid.getNullable(this.toJid);
        this.A01 = nullable;
        if (this.A04 && nullable != null) {
            this.A03.A02(nullable);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A04 = true;
    }
}
