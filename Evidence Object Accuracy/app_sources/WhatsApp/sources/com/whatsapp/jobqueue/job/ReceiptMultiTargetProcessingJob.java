package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1IS;
import X.AnonymousClass1LJ;
import X.C15380n4;
import X.C239513q;
import X.C32141bg;
import android.content.Context;
import android.util.Pair;
import com.whatsapp.jid.Jid;
import java.util.LinkedList;
import java.util.List;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public final class ReceiptMultiTargetProcessingJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C239513q A00;
    public final boolean keyFromMe;
    public final String keyId;
    public final String keyRemoteChatJidRawString;
    public final String[] participantDeviceJidRawString;
    public final C32141bg receiptPrivacyMode;
    public final String remoteJidString;
    public final int status;
    public final long[] timestamp;

    public ReceiptMultiTargetProcessingJob(Jid jid, AnonymousClass1IS r8, C32141bg r9, List list, int i) {
        super(new JobParameters("ReceiptProcessingGroup", new LinkedList(), true));
        int size = list.size();
        this.keyId = r8.A01;
        this.keyFromMe = r8.A02;
        AbstractC14640lm r0 = r8.A00;
        AnonymousClass009.A05(r0);
        this.keyRemoteChatJidRawString = r0.getRawString();
        this.remoteJidString = jid.getRawString();
        this.status = i;
        this.participantDeviceJidRawString = new String[size];
        this.timestamp = new long[size];
        this.receiptPrivacyMode = r9;
        for (int i2 = 0; i2 < size; i2++) {
            Pair pair = (Pair) list.get(i2);
            this.participantDeviceJidRawString[i2] = C15380n4.A03((Jid) pair.first);
            long[] jArr = this.timestamp;
            Object obj = pair.second;
            AnonymousClass009.A05(obj);
            jArr[i2] = ((Number) obj).longValue();
        }
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; keyRemoteJid=");
        sb.append(Jid.getNullable(this.keyRemoteChatJidRawString));
        sb.append("; remoteJid=");
        sb.append(Jid.getNullable(this.remoteJidString));
        sb.append("; number of participants=");
        sb.append(this.participantDeviceJidRawString.length);
        sb.append("; recepitPrivacyMode=");
        sb.append(this.receiptPrivacyMode);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = (C239513q) ((AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class)).ACV.get();
    }
}
