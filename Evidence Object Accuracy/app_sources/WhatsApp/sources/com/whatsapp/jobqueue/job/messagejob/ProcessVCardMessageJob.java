package com.whatsapp.jobqueue.job.messagejob;

import X.AbstractC15340mz;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.C14650lo;
import X.C15240mn;
import X.C15550nR;
import X.C16590pI;
import X.C20510vs;
import X.C26501Ds;
import android.content.Context;

/* loaded from: classes2.dex */
public class ProcessVCardMessageJob extends AsyncMessageJob {
    public transient C14650lo A00;
    public transient C15550nR A01;
    public transient C16590pI A02;
    public transient AnonymousClass018 A03;
    public transient C15240mn A04;
    public transient C20510vs A05;
    public transient C26501Ds A06;

    public ProcessVCardMessageJob(AbstractC15340mz r5) {
        super(r5.A11, r5.A12);
    }

    @Override // com.whatsapp.jobqueue.job.messagejob.AsyncMessageJob, X.AnonymousClass1LJ
    public void Abz(Context context) {
        super.Abz(context);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = (C16590pI) r1.AMg.get();
        this.A06 = (C26501Ds) r1.AML.get();
        this.A01 = (C15550nR) r1.A45.get();
        this.A03 = r1.Ag8();
        this.A04 = (C15240mn) r1.A8F.get();
        this.A05 = (C20510vs) r1.AMJ.get();
        this.A00 = (C14650lo) r1.A2V.get();
    }
}
