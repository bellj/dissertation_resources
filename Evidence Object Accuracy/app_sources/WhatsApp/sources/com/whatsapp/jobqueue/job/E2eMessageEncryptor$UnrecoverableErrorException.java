package com.whatsapp.jobqueue.job;

/* loaded from: classes2.dex */
public class E2eMessageEncryptor$UnrecoverableErrorException extends Exception {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ E2eMessageEncryptor$UnrecoverableErrorException(com.whatsapp.jid.DeviceJid r3) {
        /*
            r2 = this;
            java.lang.String r1 = "Unable to encrypt message for "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.E2eMessageEncryptor$UnrecoverableErrorException.<init>(com.whatsapp.jid.DeviceJid):void");
    }
}
