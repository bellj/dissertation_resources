package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1E7;
import X.AnonymousClass1IS;
import X.AnonymousClass1LJ;
import X.C14830m7;
import X.C15650ng;
import X.C20320vZ;
import X.C20730wE;
import android.content.Context;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SyncDevicesAndSendInvisibleMessageJob extends Job implements AnonymousClass1LJ {
    public transient C20730wE A00;
    public transient AnonymousClass1E7 A01;
    public transient C14830m7 A02;
    public transient C15650ng A03;
    public transient AnonymousClass1IS A04;
    public transient C20320vZ A05;
    public transient Set A06 = new HashSet();
    public final String messageId;
    public final String rawGroupJid;
    public final String[] rawUserJids;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SyncDevicesAndSendInvisibleMessageJob(X.AnonymousClass1X5 r6, com.whatsapp.jid.UserJid[] r7) {
        /*
            r5 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement r0 = new com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r5.<init>(r0)
            X.AnonymousClass009.A0H(r7)
            X.1IS r3 = r6.A0z
            X.0lm r2 = r3.A00
            boolean r1 = r2 instanceof com.whatsapp.jid.GroupJid
            java.lang.String r0 = "Invalid message"
            X.AnonymousClass009.A0B(r0, r1)
            r5.A04 = r3
            X.AnonymousClass009.A05(r2)
            java.lang.String r0 = r2.getRawString()
            r5.rawGroupJid = r0
            java.lang.String r0 = r3.A01
            r5.messageId = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r5.A06 = r0
            int r4 = r7.length
            r3 = 0
        L_0x0045:
            if (r3 >= r4) goto L_0x0056
            r2 = r7[r3]
            java.util.Set r1 = r5.A06
            java.lang.String r0 = "invalid jid"
            X.AnonymousClass009.A06(r2, r0)
            r1.add(r2)
            int r3 = r3 + 1
            goto L_0x0045
        L_0x0056:
            java.util.List r0 = java.util.Arrays.asList(r7)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            r5.rawUserJids = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SyncDevicesAndSendInvisibleMessageJob.<init>(X.1X5, com.whatsapp.jid.UserJid[]):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; key=");
        sb.append(this.A04);
        sb.append("; rawJids=");
        sb.append(this.A06);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A02 = r1.Aen();
        this.A05 = (C20320vZ) r1.A7A.get();
        this.A03 = (C15650ng) r1.A4m.get();
        this.A00 = (C20730wE) r1.A4J.get();
        AnonymousClass1E7 r12 = (AnonymousClass1E7) r1.A5n.get();
        this.A01 = r12;
        r12.A01(this.A04);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        String[] strArr = this.rawUserJids;
        if (strArr == null || (r5 = strArr.length) == 0) {
            throw new InvalidObjectException("rawJids must not be empty");
        }
        this.A06 = new HashSet();
        for (String str : strArr) {
            UserJid nullable = UserJid.getNullable(str);
            if (nullable != null) {
                this.A06.add(nullable);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("invalid jid:");
                sb.append(str);
                throw new InvalidObjectException(sb.toString());
            }
        }
        GroupJid nullable2 = GroupJid.getNullable(this.rawGroupJid);
        if (nullable2 != null) {
            this.A04 = new AnonymousClass1IS(nullable2, this.messageId, true);
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("invalid jid:");
        sb2.append(this.rawGroupJid);
        throw new InvalidObjectException(sb2.toString());
    }
}
