package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1E7;
import X.AnonymousClass1LJ;
import X.C14840m8;
import X.C15380n4;
import X.C20730wE;
import android.content.Context;
import com.whatsapp.jid.UserJid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SyncDevicesJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C20730wE A00;
    public transient AnonymousClass1E7 A01;
    public transient C14840m8 A02;
    public final String[] jids;
    public final int syncType;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SyncDevicesJob(com.whatsapp.jid.UserJid[] r5, int r6) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement r0 = new com.whatsapp.jobqueue.requirement.OfflineProcessingCompletedRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            X.AnonymousClass009.A0H(r5)
            int r3 = r5.length
            r2 = 0
        L_0x0024:
            if (r2 >= r3) goto L_0x0030
            r1 = r5[r2]
            java.lang.String r0 = "an element of jids was empty."
            X.AnonymousClass009.A06(r1, r0)
            int r2 = r2 + 1
            goto L_0x0024
        L_0x0030:
            java.util.List r0 = java.util.Arrays.asList(r5)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            r4.jids = r0
            r4.syncType = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SyncDevicesJob.<init>(com.whatsapp.jid.UserJid[], int):void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; jids=");
        sb.append(C15380n4.A05(this.jids));
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        int length;
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = (C14840m8) r1.ACi.get();
        this.A00 = (C20730wE) r1.A4J.get();
        this.A01 = (AnonymousClass1E7) r1.A5n.get();
        String[] strArr = this.jids;
        if (strArr != null && (length = strArr.length) > 0) {
            HashSet hashSet = new HashSet();
            int i = 0;
            do {
                UserJid nullable = UserJid.getNullable(strArr[i]);
                if (nullable != null) {
                    hashSet.add(nullable);
                }
                i++;
            } while (i < length);
            AnonymousClass1E7 r7 = this.A01;
            Set set = r7.A03;
            synchronized (set) {
                set.addAll(hashSet);
                long A00 = r7.A00.A00();
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    r7.A01.put((UserJid) it.next(), Long.valueOf(A00));
                }
            }
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        int length;
        objectInputStream.defaultReadObject();
        String[] strArr = this.jids;
        if (strArr == null || (length = strArr.length) == 0) {
            throw new InvalidObjectException("jids must not be empty");
        }
        int i = 0;
        while (UserJid.getNullable(strArr[i]) != null) {
            i++;
            if (i >= length) {
                return;
            }
        }
        throw new InvalidObjectException("an jid is not a UserJid");
    }
}
