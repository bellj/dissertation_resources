package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17220qS;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

@Deprecated
/* loaded from: classes2.dex */
public final class SendPlayedReceiptJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C17220qS A00;
    public final int editVersion = 0;
    public final String messageId;
    public final String remoteJidRawJid;
    public final String remoteResourceRawJid;
    public final String webAttrString = null;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendPlayedReceiptJob(X.AbstractC15340mz r9) {
        /*
            r8 = this;
            java.util.LinkedList r7 = new java.util.LinkedList
            r7.<init>()
            java.lang.String r0 = "played-receipt-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.1IS r6 = r9.A0z
            X.0lm r5 = r6.A00
            X.AnonymousClass009.A05(r5)
            java.lang.String r0 = r5.getRawString()
            r1.append(r0)
            java.lang.String r4 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r7.add(r0)
            r3 = 1
            r2 = 0
            r1 = 0
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r4, r7, r3)
            r8.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            r8.remoteJidRawJid = r0
            X.0lm r0 = r9.A0B()
            java.lang.String r0 = X.C15380n4.A03(r0)
            r8.remoteResourceRawJid = r0
            java.lang.String r0 = r6.A01
            r8.messageId = r0
            r8.webAttrString = r2
            r8.editVersion = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendPlayedReceiptJob.<init>(X.0mz):void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A3P();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.remoteJidRawJid)) {
            throw new InvalidObjectException("remoteJid must not be empty");
        } else if (TextUtils.isEmpty(this.messageId)) {
            throw new InvalidObjectException("messageId must not be empty");
        }
    }
}
