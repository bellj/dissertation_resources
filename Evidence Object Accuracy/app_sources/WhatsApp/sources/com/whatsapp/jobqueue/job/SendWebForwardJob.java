package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C14890mD;
import X.C17220qS;
import X.C20670w8;
import android.content.Context;
import android.os.Message;
import java.util.LinkedList;
import java.util.concurrent.Future;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public final class SendWebForwardJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C20670w8 A00;
    public transient C17220qS A01;
    public transient C14890mD A02;
    public final transient Message A03;
    public final transient String A04;
    public final transient String A05;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendWebForwardJob(android.os.Message r5, java.lang.String r6, java.lang.String r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = "webSend"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 0
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            X.AnonymousClass009.A05(r6)
            r4.A04 = r6
            X.AnonymousClass009.A05(r5)
            r4.A03 = r5
            r4.A05 = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendWebForwardJob.<init>(android.os.Message, java.lang.String, java.lang.String):void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = (C14890mD) r1.ANL.get();
        this.A00 = (C20670w8) r1.AMw.get();
        this.A01 = r1.A3P();
    }

    /* loaded from: classes2.dex */
    public final class AckWebForwardJob extends Job implements AnonymousClass1LJ {
        public static final long serialVersionUID = 1;
        public transient C20670w8 A00;
        public transient C14890mD A01;
        public final transient Message A02;
        public final transient String A03;
        public final transient String A04;
        public final transient Future A05;

        public AckWebForwardJob(Message message, String str, String str2, Future future) {
            super(new JobParameters("webAck", new LinkedList(), false));
            this.A03 = str;
            this.A02 = message;
            this.A04 = str2;
            this.A05 = future;
        }

        @Override // X.AnonymousClass1LJ
        public void Abz(Context context) {
            AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
            this.A01 = (C14890mD) r1.ANL.get();
            this.A00 = (C20670w8) r1.AMw.get();
        }
    }
}
