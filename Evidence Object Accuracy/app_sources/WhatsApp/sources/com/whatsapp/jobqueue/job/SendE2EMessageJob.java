package com.whatsapp.jobqueue.job;

import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass100;
import X.AnonymousClass101;
import X.AnonymousClass12H;
import X.AnonymousClass1LJ;
import X.AnonymousClass2Ci;
import X.AnonymousClass3IO;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15380n4;
import X.C15510nN;
import X.C15570nT;
import X.C15600nX;
import X.C15650ng;
import X.C15990oG;
import X.C16030oK;
import X.C17070qD;
import X.C17220qS;
import X.C18240s8;
import X.C20320vZ;
import X.C20780wJ;
import X.C20870wS;
import X.C21400xM;
import X.C22130yZ;
import X.C22180yf;
import X.C22300yr;
import X.C22340yv;
import X.C22700zV;
import X.C22830zi;
import X.C22910zq;
import X.C231210l;
import X.C242914y;
import X.C27061Fw;
import X.C27081Fy;
import X.C63843Dd;
import X.EnumC35661iT;
import android.content.Context;
import android.os.SystemClock;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jobqueue.requirement.AxolotlDifferentAliceBaseKeyRequirement;
import com.whatsapp.jobqueue.requirement.AxolotlMultiDeviceSessionRequirement;
import com.whatsapp.jobqueue.requirement.AxolotlSessionRequirement;
import com.whatsapp.jobqueue.requirement.ChatConnectionRequirement;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public final class SendE2EMessageJob extends Job implements AnonymousClass1LJ {
    public static final ConcurrentHashMap A0j = new ConcurrentHashMap();
    public static final long serialVersionUID = 1;
    public transient int A00;
    public transient long A01;
    public transient long A02;
    public transient long A03;
    public transient long A04;
    public transient AbstractC15710nm A05;
    public transient C14900mE A06;
    public transient C15570nT A07;
    public transient C20870wS A08;
    public transient AnonymousClass101 A09;
    public transient C22700zV A0A;
    public transient C14830m7 A0B;
    public transient C15990oG A0C;
    public transient C18240s8 A0D;
    public transient C15650ng A0E;
    public transient C15600nX A0F;
    public transient C242914y A0G;
    public transient AnonymousClass12H A0H;
    public transient C22340yv A0I;
    public transient C22830zi A0J;
    public transient C22300yr A0K;
    public transient C21400xM A0L;
    public transient C22130yZ A0M;
    public transient C27061Fw A0N;
    public transient C14850m9 A0O;
    public transient DeviceJid A0P;
    public transient C63843Dd A0Q;
    public transient AnonymousClass3IO A0R;
    public transient AnonymousClass2Ci A0S;
    public transient C16030oK A0T;
    public transient C22910zq A0U;
    public transient C17220qS A0V;
    public transient C17070qD A0W;
    public transient C27081Fy A0X;
    public transient AbstractC15340mz A0Y;
    public transient C20320vZ A0Z;
    public transient C15510nN A0a;
    public transient C231210l A0b;
    public transient AnonymousClass100 A0c;
    public transient C20780wJ A0d;
    public transient boolean A0e;
    public transient boolean A0f;
    public transient boolean A0g;
    public transient boolean A0h;
    public final transient int A0i;
    public final HashMap broadcastParticipantEphemeralSettings;
    public boolean duplicate;
    public final int editVersion;
    public final HashMap encryptionRetryCounts;
    public final byte[] ephemeralSharedSecret;
    public final long expireTimeMs;
    public final boolean forceSenderKeyDistribution;
    public final String groupParticipantHash;
    public String groupParticipantHashToSend;
    public final String id;
    public final boolean includeSenderKeysInMessage;
    public final String jid;
    public final Integer liveLocationDuration;
    public final long messageSendStartTime;
    public final boolean multiDeviceFanOut;
    public final long originalTimestamp;
    public final int originationFlags;
    public final String participant;
    public final String recipientRawJid;
    public final int retryCount;
    public final HashSet targetDeviceRawJids;
    public final boolean useOneOneEncryptionOnPHashMismatch;
    public final EnumC35661iT webAttribute;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        if (r14 != null) goto L_0x0028;
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0148  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SendE2EMessageJob(com.whatsapp.jid.DeviceJid r14, com.whatsapp.jid.Jid r15, com.whatsapp.jid.UserJid r16, X.C27081Fy r17, X.EnumC35661iT r18, X.C20780wJ r19, java.lang.Integer r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, java.util.Map r24, java.util.Set r25, byte[] r26, byte[] r27, int r28, int r29, int r30, int r31, long r32, long r34, long r36, long r38, boolean r40, boolean r41, boolean r42, boolean r43, boolean r44, boolean r45) {
        /*
        // Method dump skipped, instructions count: 374
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendE2EMessageJob.<init>(com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid, com.whatsapp.jid.UserJid, X.1Fy, X.1iT, X.0wJ, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.util.Map, java.util.Set, byte[], byte[], int, int, int, int, long, long, long, long, boolean, boolean, boolean, boolean, boolean, boolean):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public boolean A02() {
        boolean z = false;
        if (this.A0B.A00() >= this.expireTimeMs) {
            z = true;
        }
        if (z) {
            return true;
        }
        boolean z2 = true;
        for (Requirement requirement : this.parameters.requirements) {
            if (!requirement.AJu()) {
                z2 = false;
                if (requirement instanceof ChatConnectionRequirement) {
                    this.A0h = true;
                }
            }
            if (!this.A0e && !this.A0g && ((requirement instanceof AxolotlSessionRequirement) || (requirement instanceof AxolotlDifferentAliceBaseKeyRequirement) || (requirement instanceof AxolotlMultiDeviceSessionRequirement))) {
                if (requirement.AJu()) {
                    this.A0g = true;
                    C14830m7 r2 = this.A0B;
                    this.A02 = SystemClock.uptimeMillis();
                    this.A01 = r2.A00();
                }
            }
        }
        return z2;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r10v13, types: [java.util.List, java.util.Map] */
    /* JADX WARN: Type inference failed for: r10v14 */
    /* JADX WARN: Type inference failed for: r10v16 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        /*
        // Method dump skipped, instructions count: 3601
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendE2EMessageJob.A03():void");
    }

    public final String A04() {
        String A04 = C15380n4.A04(this.jid);
        String A042 = C15380n4.A04(this.participant);
        StringBuilder sb = new StringBuilder("; id=");
        sb.append(this.id);
        sb.append("; jid=");
        sb.append(A04);
        sb.append("; participant=");
        sb.append(A042);
        sb.append("; retryCount=");
        sb.append(this.retryCount);
        sb.append("; targetDevices=");
        sb.append(this.targetDeviceRawJids);
        sb.append("; groupParticipantHash=");
        sb.append(this.groupParticipantHash);
        sb.append("; groupParticipantHashToSend=");
        sb.append(this.groupParticipantHashToSend);
        sb.append("; webAttribute=");
        sb.append(this.webAttribute);
        sb.append("; includeSenderKeysInMessage=");
        sb.append(this.includeSenderKeysInMessage);
        sb.append("; useOneOneEncryptionOnPHashMismatch=");
        sb.append(this.useOneOneEncryptionOnPHashMismatch);
        sb.append("; forceSenderKeyDistribution=");
        sb.append(this.forceSenderKeyDistribution);
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r8 != null) goto L_0x0007;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0213  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(com.whatsapp.jid.DeviceJid r8, com.whatsapp.jid.Jid r9) {
        /*
        // Method dump skipped, instructions count: 555
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendE2EMessageJob.A05(com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid):void");
    }

    public void A06(AbstractC15340mz r28, Collection collection, int i, int i2, int i3, int i4, boolean z) {
        long j;
        if (r28 != null && this.messageSendStartTime != 0 && this.A04 != 0) {
            C14830m7 r0 = this.A0B;
            long uptimeMillis = SystemClock.uptimeMillis();
            long A00 = r0.A00() - this.messageSendStartTime;
            if (i == 6) {
                j = this.A04;
            } else {
                j = r28.A16;
            }
            C20870wS r5 = this.A08;
            int i5 = this.retryCount;
            boolean z2 = this.A0h;
            boolean z3 = this.A0f;
            r5.A0G(r28, collection, i, i2, i5, this.A00, i4, 0, 0, i3, uptimeMillis - j, A00, A00, z2, z3, this.A0e, A08(), z);
        }
    }

    public final boolean A07() {
        HashSet hashSet = this.targetDeviceRawJids;
        return hashSet != null && !hashSet.isEmpty();
    }

    public final boolean A08() {
        return !this.forceSenderKeyDistribution && A07();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A0B = r1.Aen();
        this.A0O = r1.A3L();
        this.A06 = (C14900mE) r1.A8X.get();
        this.A05 = r1.A7p();
        this.A07 = r1.A1w();
        this.A0V = r1.A3P();
        this.A08 = (C20870wS) r1.AC2.get();
        this.A0D = (C18240s8) r1.AIt.get();
        this.A0Z = (C20320vZ) r1.A7A.get();
        this.A0W = (C17070qD) r1.AFC.get();
        this.A0E = (C15650ng) r1.A4m.get();
        this.A0U = (C22910zq) r1.A9O.get();
        this.A0K = (C22300yr) r1.AMv.get();
        this.A0H = (AnonymousClass12H) r1.AC5.get();
        this.A0C = r1.A2d();
        this.A0I = (C22340yv) r1.ACE.get();
        this.A0L = (C21400xM) r1.ABd.get();
        this.A0A = (C22700zV) r1.AMN.get();
        this.A0M = (C22130yZ) r1.A5d.get();
        this.A0J = (C22830zi) r1.AHH.get();
        this.A0c = (AnonymousClass100) r1.AJB.get();
        this.A09 = (AnonymousClass101) r1.AFt.get();
        this.A0T = (C16030oK) r1.AAd.get();
        this.A0N = (C27061Fw) r1.A6e.get();
        this.A0F = (C15600nX) r1.A8x.get();
        this.A0a = (C15510nN) r1.AHZ.get();
        this.A0G = (C242914y) r1.ABu.get();
        this.A0b = (C231210l) r1.ACx.get();
        this.A0R = new AnonymousClass3IO(this.A07, this.A0J, this.A0M, (C22180yf) r1.A5U.get());
        this.A0Q = new C63843Dd(this.encryptionRetryCounts);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        try {
            this.A0X = C27081Fy.A01((byte[]) objectInputStream.readObject());
        } catch (OptionalDataException unused) {
            StringBuilder sb = new StringBuilder("sende2emessagejob/e2e missing message bytes ");
            sb.append(A04());
            Log.e(sb.toString());
        }
        if (this.A0X == null) {
            StringBuilder sb2 = new StringBuilder("message must not be null");
            sb2.append(A04());
            throw new InvalidObjectException(sb2.toString());
        } else if (this.id != null) {
            Jid nullable = Jid.getNullable(this.jid);
            if (nullable != null) {
                this.A0P = DeviceJid.getNullable(this.jid);
                DeviceJid nullable2 = DeviceJid.getNullable(this.participant);
                if (this.groupParticipantHashToSend == null) {
                    this.groupParticipantHashToSend = this.groupParticipantHash;
                }
                this.A0e = true;
                this.A04 = SystemClock.uptimeMillis();
                A05(nullable2, nullable);
                return;
            }
            StringBuilder sb3 = new StringBuilder("jid must not be null");
            sb3.append(A04());
            throw new InvalidObjectException(sb3.toString());
        } else {
            StringBuilder sb4 = new StringBuilder("id must not be null");
            sb4.append(A04());
            throw new InvalidObjectException(sb4.toString());
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.A0X.A02());
    }
}
