package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass104;
import X.AnonymousClass1LJ;
import X.C15450nH;
import X.C17220qS;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendMediaErrorReceiptJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15450nH A00;
    public transient AnonymousClass104 A01;
    public transient C17220qS A02;
    public final String category;
    public final boolean mediaFromMe;
    public final byte[] mediaKey;
    public final String messageId;
    public final String myPrimaryJid = null;
    public final String remoteJidRawJid;
    public final String remoteResourceRawJid;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendMediaErrorReceiptJob(X.AbstractC15340mz r6, java.lang.String r7, byte[] r8) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.lang.String r3 = "media-error-receipt"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            r1 = 1
            r2 = 0
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r3, r4, r1)
            r5.<init>(r0)
            X.1IS r1 = r6.A0z
            X.0lm r0 = r1.A00
            X.AnonymousClass009.A05(r0)
            java.lang.String r0 = r0.getRawString()
            r5.remoteJidRawJid = r0
            X.0lm r0 = r6.A0B()
            java.lang.String r0 = X.C15380n4.A03(r0)
            r5.remoteResourceRawJid = r0
            r5.myPrimaryJid = r2
            java.lang.String r0 = r1.A01
            r5.messageId = r0
            r5.mediaKey = r8
            r5.category = r7
            boolean r0 = r1.A02
            r5.mediaFromMe = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendMediaErrorReceiptJob.<init>(X.0mz, java.lang.String, byte[]):void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = (C15450nH) r1.AII.get();
        this.A02 = r1.A3P();
        this.A01 = (AnonymousClass104) r1.AHf.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (TextUtils.isEmpty(this.remoteJidRawJid)) {
            throw new InvalidObjectException("remoteJid must not be empty");
        } else if (TextUtils.isEmpty(this.messageId)) {
            throw new InvalidObjectException("messageId must not be empty");
        }
    }
}
