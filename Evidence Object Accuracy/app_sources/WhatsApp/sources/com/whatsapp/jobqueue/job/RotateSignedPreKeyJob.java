package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass2NE;
import X.C15990oG;
import X.C16050oM;
import X.C17220qS;
import X.C18240s8;
import X.C22920zr;
import X.C29211Rh;
import X.C35931j1;
import android.content.Context;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.jobqueue.job.RotateSignedPreKeyJob;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class RotateSignedPreKeyJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C22920zr A00;
    public transient C15990oG A01;
    public transient C18240s8 A02;
    public transient C17220qS A03;
    public final byte[] data;
    public final byte[] id;
    public final byte[] signature;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RotateSignedPreKeyJob(byte[] r5, byte[] r6, byte[] r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r2 = "RotateSignedPreKeyJob"
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            X.AnonymousClass009.A0G(r5)
            r4.id = r5
            X.AnonymousClass009.A0G(r6)
            r4.data = r6
            X.AnonymousClass009.A0G(r7)
            r4.signature = r7
            int r2 = r5.length
            r0 = 3
            if (r2 != r0) goto L_0x005e
            int r2 = r6.length
            r0 = 32
            if (r2 != r0) goto L_0x004a
            int r2 = r7.length
            r0 = 64
            if (r2 != r0) goto L_0x0036
            return
        L_0x0036:
            java.lang.String r1 = "invalid signed pre-key signature length: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x004a:
            java.lang.String r1 = "invalid signed pre-key length: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x005e:
            java.lang.String r1 = "invalid signed pre-key id length: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.RotateSignedPreKeyJob.<init>(byte[], byte[], byte[]):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        StringBuilder sb;
        StringBuilder sb2 = new StringBuilder("starting rotate signed pre key job");
        sb2.append(A04());
        Log.i(sb2.toString());
        C18240s8 r0 = this.A02;
        if (!Arrays.equals(this.id, ((C29211Rh) r0.A00.submit(new Callable() { // from class: X.5Dd
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return RotateSignedPreKeyJob.this.A01.A0M();
            }
        }).get()).A01)) {
            sb = new StringBuilder("aborting rotate signed pre key job due to id mismatch with latest");
        } else {
            String A01 = this.A03.A01();
            AtomicInteger atomicInteger = new AtomicInteger();
            AtomicReference atomicReference = new AtomicReference();
            C17220qS r5 = this.A03;
            C29211Rh r8 = new C29211Rh(this.id, this.data, this.signature);
            Message obtain = Message.obtain(null, 0, 86, 0, new C35931j1(this, atomicInteger, atomicReference));
            obtain.getData().putString("iqId", A01);
            obtain.getData().putParcelable("signedPreKey", new AnonymousClass2NE(r8));
            r5.A04(obtain, A01, false).get();
            int i = atomicInteger.get();
            if (i == 503) {
                StringBuilder sb3 = new StringBuilder("server 503 error during rotate signed pre key job");
                sb3.append(A04());
                throw new Exception(sb3.toString());
            } else if (i == 409) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("server error code returned during rotate signed pre key job; errorCode=");
                sb4.append(i);
                sb4.append(A04());
                Log.w(sb4.toString());
                Object obj = atomicReference.get();
                if (obj != null) {
                    C18240s8 r2 = this.A02;
                    r2.A00.submit(new RunnableBRunnable0Shape5S0200000_I0_5(this, 26, obj));
                    return;
                }
                return;
            } else if (i != 0) {
                sb = new StringBuilder();
                sb.append("server error code returned during rotate signed pre key job; errorCode=");
                sb.append(i);
            } else {
                return;
            }
        }
        sb.append(A04());
        Log.w(sb.toString());
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; signedPreKeyId=");
        sb.append(C16050oM.A01(this.id));
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A03 = r1.A3P();
        this.A02 = (C18240s8) r1.AIt.get();
        this.A00 = (C22920zr) r1.ACq.get();
        this.A01 = r1.A2d();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        byte[] bArr = this.id;
        if (bArr != null) {
            byte[] bArr2 = this.data;
            if (bArr2 != null) {
                byte[] bArr3 = this.signature;
                if (bArr3 != null) {
                    int length = bArr.length;
                    if (length == 3) {
                        int length2 = bArr2.length;
                        if (length2 == 32) {
                            int length3 = bArr3.length;
                            if (length3 != 64) {
                                StringBuilder sb = new StringBuilder("invalid signed pre-key signature length: ");
                                sb.append(length3);
                                throw new InvalidObjectException(sb.toString());
                            }
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder("invalid signed pre-key length: ");
                        sb2.append(length2);
                        throw new InvalidObjectException(sb2.toString());
                    }
                    StringBuilder sb3 = new StringBuilder("invalid signed pre-key id length: ");
                    sb3.append(length);
                    throw new InvalidObjectException(sb3.toString());
                }
                throw new InvalidObjectException("signature cannot be null");
            }
            throw new InvalidObjectException("data cannot be null");
        }
        throw new InvalidObjectException("id cannot be null");
    }
}
