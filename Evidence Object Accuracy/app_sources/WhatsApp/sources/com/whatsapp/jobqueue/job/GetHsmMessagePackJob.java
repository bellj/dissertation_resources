package com.whatsapp.jobqueue.job;

import X.AbstractC27291Gt;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17030q9;
import X.C17220qS;
import android.content.Context;
import android.text.TextUtils;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class GetHsmMessagePackJob extends Job implements AnonymousClass1LJ {
    public static final HashSet A02 = new HashSet();
    public static final long serialVersionUID = 1;
    public transient C17030q9 A00;
    public transient C17220qS A01;
    public final String elementName;
    public final Locale[] locales;
    public final String namespace;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GetHsmMessagePackJob(java.lang.String r4, java.lang.String r5, java.util.Locale[] r6) {
        /*
            r3 = this;
            java.util.LinkedList r2 = new java.util.LinkedList
            r2.<init>()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r2.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r4, r2, r1)
            r3.<init>(r0)
            X.AnonymousClass009.A0H(r6)
            r3.locales = r6
            X.AnonymousClass009.A04(r4)
            r3.namespace = r4
            X.AnonymousClass009.A04(r5)
            r3.elementName = r5
            java.util.HashSet r1 = com.whatsapp.jobqueue.job.GetHsmMessagePackJob.A02
            monitor-enter(r1)
            r1.add(r3)     // Catch: all -> 0x002d
            monitor-exit(r1)     // Catch: all -> 0x002d
            return
        L_0x002d:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x002d
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.GetHsmMessagePackJob.<init>(java.lang.String, java.lang.String, java.util.Locale[]):void");
    }

    public static boolean A00(String str, String str2, Locale[] localeArr) {
        HashSet hashSet = A02;
        synchronized (hashSet) {
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                GetHsmMessagePackJob getHsmMessagePackJob = (GetHsmMessagePackJob) it.next();
                if (Arrays.equals(getHsmMessagePackJob.locales, localeArr) && TextUtils.equals(getHsmMessagePackJob.namespace, str) && TextUtils.equals(getHsmMessagePackJob.elementName, str2)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r7v2, types: [java.util.Locale[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r10v2, types: [java.util.Locale[], java.io.Serializable] */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x014b, code lost:
        if (r9 != null) goto L_0x014d;
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // org.whispersystems.jobqueue.Job
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 438
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.GetHsmMessagePackJob.A03():void");
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; namespace=");
        sb.append(this.namespace);
        sb.append("; element=");
        sb.append(this.elementName);
        sb.append("; locales=");
        sb.append(AbstractC27291Gt.A08(this.locales));
        sb.append("; persistentId=");
        sb.append(super.A01);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A01 = r1.A3P();
        this.A00 = (C17030q9) r1.AAM.get();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        Locale[] localeArr = this.locales;
        if (localeArr == null || localeArr.length == 0) {
            throw new InvalidObjectException("locales[] must not be empty");
        } else if (TextUtils.isEmpty(this.namespace)) {
            throw new InvalidObjectException("namespace must not be empty");
        } else if (!TextUtils.isEmpty(this.elementName)) {
            HashSet hashSet = A02;
            synchronized (hashSet) {
                hashSet.add(this);
            }
        } else {
            throw new InvalidObjectException("elementName must not be empty");
        }
    }
}
