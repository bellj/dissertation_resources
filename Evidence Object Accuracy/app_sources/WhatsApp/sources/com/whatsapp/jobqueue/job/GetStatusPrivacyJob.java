package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C17220qS;
import X.C18470sV;
import android.content.Context;
import com.whatsapp.jobqueue.requirement.ChatConnectionRequirement;
import java.util.LinkedList;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public final class GetStatusPrivacyJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C18470sV A00;
    public transient C17220qS A01;

    public GetStatusPrivacyJob(JobParameters jobParameters) {
        super(jobParameters);
    }

    public static GetStatusPrivacyJob A00() {
        LinkedList linkedList = new LinkedList();
        linkedList.add(new ChatConnectionRequirement());
        return new GetStatusPrivacyJob(new JobParameters("GetStatusPrivacyJob", linkedList, true));
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = (C18470sV) r1.AK8.get();
        this.A01 = r1.A3P();
    }
}
