package com.whatsapp.jobqueue.job;

/* loaded from: classes2.dex */
public class HSMRehydrationUtil$SendStructUnavailableException extends Exception {
    public final Integer errorCode;

    public HSMRehydrationUtil$SendStructUnavailableException(Integer num) {
        this.errorCode = num;
    }
}
