package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C15990oG;
import X.C17220qS;
import X.C18240s8;
import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendRetryReceiptJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C15990oG A00;
    public transient C18240s8 A01;
    public transient C17220qS A02;
    public final String category;
    public final int editVersion;
    public final String id;
    public final String jid;
    public final int localRegistrationId;
    public final long loggableStanzaId;
    public final String participant;
    public final String recipientJid;
    public final int retryCount;
    public final long timestamp;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendRetryReceiptJob(X.C28941Pp r6, int r7) {
        /*
            r5 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.lang.String r0 = "retry-receipt-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            com.whatsapp.jid.Jid r3 = r6.A0g
            X.0lm r0 = X.C15380n4.A00(r3)
            java.lang.String r0 = X.C15380n4.A03(r0)
            r1.append(r0)
            java.lang.String r2 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r4.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r4, r1)
            r5.<init>(r0)
            X.AnonymousClass009.A05(r3)
            java.lang.String r0 = r3.getRawString()
            r5.jid = r0
            java.lang.String r0 = r6.A0k
            X.AnonymousClass009.A05(r0)
            r5.id = r0
            com.whatsapp.jid.Jid r0 = r6.A08
            java.lang.String r0 = X.C15380n4.A03(r0)
            r5.participant = r0
            com.whatsapp.jid.UserJid r0 = r6.A0h
            java.lang.String r0 = X.C15380n4.A03(r0)
            r5.recipientJid = r0
            long r0 = r6.A0f
            r5.timestamp = r0
            int r0 = r6.A00()
            r5.retryCount = r0
            r5.localRegistrationId = r7
            int r0 = r6.A01
            r5.editVersion = r0
            long r0 = r6.A06
            r5.loggableStanzaId = r0
            java.lang.String r0 = r6.A0R
            r5.category = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendRetryReceiptJob.<init>(X.1Pp, int):void");
    }

    public String A04() {
        Jid nullable = Jid.getNullable(this.jid);
        Jid nullable2 = Jid.getNullable(this.participant);
        StringBuilder sb = new StringBuilder("; jid=");
        sb.append(nullable);
        sb.append("; id=");
        sb.append(this.id);
        sb.append("; participant=");
        sb.append(nullable2);
        sb.append("; retryCount=");
        sb.append(this.retryCount);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A02 = r1.A3P();
        this.A01 = (C18240s8) r1.AIt.get();
        this.A00 = r1.A2d();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (Jid.getNullable(this.jid) == null) {
            throw new InvalidObjectException("jid must not be empty");
        } else if (TextUtils.isEmpty(this.id)) {
            throw new InvalidObjectException("id must not be empty");
        }
    }
}
