package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass30S;
import X.C15380n4;
import X.C16120oU;
import X.C17220qS;
import android.content.Context;
import android.os.Message;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class BulkGetPreKeyJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C16120oU A00;
    public transient C17220qS A01;
    public final int context;
    public final String[] identityChangedJids;
    public final String[] jids;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BulkGetPreKeyJob(com.whatsapp.jid.DeviceJid[] r7, com.whatsapp.jid.DeviceJid[] r8, int r9) {
        /*
            r6 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r6.<init>(r0)
            java.util.List r0 = java.util.Arrays.asList(r7)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            X.AnonymousClass009.A0H(r0)
            r6.jids = r0
            if (r8 != 0) goto L_0x0045
            r0 = 0
        L_0x0027:
            r6.identityChangedJids = r0
            r6.context = r9
            int r5 = r7.length
            r4 = 0
            r2 = 0
        L_0x002e:
            java.lang.String r3 = "jid must be an individual jid; jid="
            if (r2 >= r5) goto L_0x006b
            r1 = r7[r2]
            if (r1 == 0) goto L_0x0063
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 != 0) goto L_0x004e
            boolean r0 = X.C15380n4.A0F(r1)
            if (r0 != 0) goto L_0x004e
            int r2 = r2 + 1
            goto L_0x002e
        L_0x0045:
            java.util.List r0 = java.util.Arrays.asList(r8)
            java.lang.String[] r0 = X.C15380n4.A0Q(r0)
            goto L_0x0027
        L_0x004e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0063:
            java.lang.String r1 = "an element of jids was empty"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x006b:
            if (r8 == 0) goto L_0x00a0
            int r2 = r8.length
        L_0x006e:
            if (r4 >= r2) goto L_0x00a0
            r1 = r8[r4]
            if (r1 == 0) goto L_0x0098
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 != 0) goto L_0x0083
            boolean r0 = X.C15380n4.A0F(r1)
            if (r0 != 0) goto L_0x0083
            int r4 = r4 + 1
            goto L_0x006e
        L_0x0083:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0098:
            java.lang.String r1 = "an element of identityChangedJids was empty"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x00a0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.BulkGetPreKeyJob.<init>(com.whatsapp.jid.DeviceJid[], com.whatsapp.jid.DeviceJid[], int):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        List arrayList;
        StringBuilder sb = new StringBuilder("starting bulk get pre key job");
        sb.append(A04());
        Log.i(sb.toString());
        String A01 = this.A01.A01();
        List A07 = C15380n4.A07(DeviceJid.class, Arrays.asList(this.jids));
        String[] strArr = this.identityChangedJids;
        if (strArr != null) {
            arrayList = C15380n4.A07(DeviceJid.class, Arrays.asList(strArr));
        } else {
            arrayList = new ArrayList();
        }
        if (this.context != 0) {
            AnonymousClass30S r4 = new AnonymousClass30S();
            r4.A00 = Boolean.valueOf(!arrayList.isEmpty());
            r4.A02 = Long.valueOf((long) A07.size());
            r4.A01 = Integer.valueOf(this.context);
            this.A00.A07(r4);
        }
        C17220qS r6 = this.A01;
        Message obtain = Message.obtain(null, 0, 87, 0);
        obtain.getData().putString("id", A01);
        obtain.getData().putParcelableArray("jids", (Jid[]) A07.toArray(new DeviceJid[0]));
        obtain.getData().putParcelableArray("identityJids", (Jid[]) arrayList.toArray(new DeviceJid[0]));
        r6.A04(obtain, A01, false).get();
    }

    public final String A04() {
        StringBuilder sb = new StringBuilder("; jids=");
        sb.append(C15380n4.A05(this.jids));
        sb.append("; context=");
        sb.append(this.context);
        return sb.toString();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = r1.Ag5();
        this.A01 = r1.A3P();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        String[] strArr = this.jids;
        if (strArr == null || (r3 = strArr.length) == 0) {
            throw new InvalidObjectException("jids must not be empty");
        }
        for (String str : strArr) {
            DeviceJid nullable = DeviceJid.getNullable(str);
            if (nullable == null) {
                throw new InvalidObjectException("an element of jids was empty");
            } else if (C15380n4.A0J(nullable) || C15380n4.A0F(nullable)) {
                StringBuilder sb = new StringBuilder();
                sb.append("jid must be an individual jid; jid=");
                sb.append(nullable);
                throw new InvalidObjectException(sb.toString());
            }
        }
        String[] strArr2 = this.identityChangedJids;
        if (strArr2 != null) {
            for (String str2 : strArr2) {
                DeviceJid nullable2 = DeviceJid.getNullable(str2);
                if (nullable2 == null) {
                    throw new InvalidObjectException("an element of identityChangedJids was empty");
                } else if (C15380n4.A0J(nullable2) || C15380n4.A0F(nullable2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("jid must be an individual jid; jid=");
                    sb2.append(nullable2);
                    throw new InvalidObjectException(sb2.toString());
                }
            }
        }
    }
}
