package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.C14820m6;
import X.C15380n4;
import X.C16240og;
import X.C18770sz;
import X.C20730wE;
import android.content.Context;
import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.LinkedList;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public class SyncDeviceForAdvValidationJob extends Job implements AnonymousClass1LJ {
    public static final long serialVersionUID = 1;
    public transient C16240og A00;
    public transient C20730wE A01;
    public transient C14820m6 A02;
    public transient C18770sz A03;
    public final String[] jids;

    public SyncDeviceForAdvValidationJob(UserJid[] userJidArr) {
        super(new JobParameters("SyncDeviceForAdvValidationJob", new LinkedList(), false));
        this.jids = C15380n4.A0Q(Arrays.asList(userJidArr));
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = (C16240og) r1.ANq.get();
        this.A03 = (C18770sz) r1.AM8.get();
        this.A01 = (C20730wE) r1.A4J.get();
        this.A02 = r1.Ag2();
    }
}
