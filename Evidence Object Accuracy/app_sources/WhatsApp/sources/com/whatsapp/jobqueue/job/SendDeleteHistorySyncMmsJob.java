package com.whatsapp.jobqueue.job;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass14D;
import X.AnonymousClass1LJ;
import X.C20110vE;
import X.C22600zL;
import android.content.Context;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public class SendDeleteHistorySyncMmsJob extends Job implements AnonymousClass1LJ {
    public transient C20110vE A00;
    public transient AnonymousClass14D A01;
    public transient C22600zL A02;
    public final String chunkId;
    public final String directPath;
    public final String mediaEncHash;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendDeleteHistorySyncMmsJob(java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            org.whispersystems.jobqueue.requirements.NetworkRequirement r0 = new org.whispersystems.jobqueue.requirements.NetworkRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            r4.chunkId = r5
            r4.directPath = r6
            r4.mediaEncHash = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendDeleteHistorySyncMmsJob.<init>(java.lang.String, java.lang.String, java.lang.String):void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A02 = (C22600zL) r1.AHm.get();
        this.A01 = (AnonymousClass14D) r1.AM2.get();
        this.A00 = r1.A3O();
    }
}
