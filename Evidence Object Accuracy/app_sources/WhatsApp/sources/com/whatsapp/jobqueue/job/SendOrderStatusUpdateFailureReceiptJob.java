package com.whatsapp.jobqueue.job;

import X.AbstractC14640lm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass1LJ;
import X.AnonymousClass1OT;
import X.AnonymousClass1VH;
import X.C17220qS;
import android.content.Context;
import android.os.Message;
import org.whispersystems.jobqueue.Job;

/* loaded from: classes2.dex */
public final class SendOrderStatusUpdateFailureReceiptJob extends Job implements AnonymousClass1LJ {
    public transient C17220qS A00;
    public final String jid;
    public final String messageKeyId;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SendOrderStatusUpdateFailureReceiptJob(X.AbstractC14640lm r5, java.lang.String r6) {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            java.lang.String r0 = "order-status-update-failure-"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            r1.append(r0)
            java.lang.String r2 = r1.toString()
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            java.lang.String r0 = r5.getRawString()
            r4.jid = r0
            r4.messageKeyId = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jobqueue.job.SendOrderStatusUpdateFailureReceiptJob.<init>(X.0lm, java.lang.String):void");
    }

    @Override // org.whispersystems.jobqueue.Job
    public void A03() {
        AbstractC14640lm A01 = AbstractC14640lm.A01(this.jid);
        AnonymousClass1VH r1 = new AnonymousClass1VH();
        r1.A01 = A01;
        r1.A07 = this.messageKeyId;
        r1.A08 = "error";
        r1.A05 = "receipt";
        AnonymousClass1OT A00 = r1.A00();
        C17220qS r4 = this.A00;
        String str = this.messageKeyId;
        Message obtain = Message.obtain(null, 0, 295, 0, A01);
        obtain.getData().putString("messageKeyId", str);
        r4.A03(obtain, A00).get();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).A3P();
    }
}
