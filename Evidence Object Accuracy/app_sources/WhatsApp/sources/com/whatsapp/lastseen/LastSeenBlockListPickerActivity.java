package com.whatsapp.lastseen;

import X.AbstractActivityC36551k4;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2I5;

/* loaded from: classes2.dex */
public class LastSeenBlockListPickerActivity extends AbstractActivityC36551k4 {
    public AnonymousClass2I5 A00;
    public boolean A01;

    public LastSeenBlockListPickerActivity() {
        this(0);
    }

    public LastSeenBlockListPickerActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 83);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0M(this, A1M, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            this.A00 = A1L.A0H();
        }
    }
}
