package com.whatsapp.lastseen;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.RadioButton;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class TrustedContactsPresencePrivacyActivity extends ActivityC13790kL {
    public int A00;
    public int A01;
    public RadioButton A02;
    public RadioButton A03;
    public RadioButton A04;
    public RadioButton A05;
    public RadioButton A06;
    public RadioButton A07;
    public boolean A08;

    public TrustedContactsPresencePrivacyActivity() {
        this(0);
    }

    public TrustedContactsPresencePrivacyActivity(int i) {
        this.A08 = false;
        ActivityC13830kP.A1P(this, 85);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    public final void A2e() {
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("last_seen", this.A00);
        A0A.putExtra("online", this.A01);
        C12960it.A0q(this, A0A);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            this.A00 = 3;
            A2e();
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.privacy_trusted_contacts_presence);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0A(R.string.settings_trusted_contacts_title);
        this.A03 = (RadioButton) findViewById(R.id.my_contacts_button);
        this.A02 = (RadioButton) findViewById(R.id.everyone_btn);
        this.A04 = (RadioButton) findViewById(R.id.my_contacts_except_button);
        this.A05 = (RadioButton) findViewById(R.id.nobody_btn);
        this.A06 = (RadioButton) findViewById(R.id.child_everyone_button);
        this.A07 = (RadioButton) findViewById(R.id.child_match_parent_button);
        C12970iu.A0M(this, R.id.reciprocity_description).setText(Html.fromHtml(getString(R.string.settings_trusted_contacts_reciprocity)));
        this.A03.setText(R.string.privacy_contacts);
        this.A02.setText(R.string.privacy_everyone);
        this.A04.setText(R.string.group_add_permission_blacklist);
        this.A05.setText(R.string.privacy_nobody);
        this.A06.setText(R.string.privacy_everyone);
        this.A07.setText(R.string.settings_trusted_contacts_same_as_last_seen);
        C12960it.A0x(this.A03, this, 37);
        C12960it.A0x(this.A02, this, 35);
        C12960it.A0x(this.A04, this, 36);
        C12960it.A0x(this.A05, this, 38);
        C12960it.A0x(this.A06, this, 34);
        C12960it.A0x(this.A07, this, 39);
        this.A00 = ((ActivityC13810kN) this).A09.A00.getInt("privacy_last_seen", 0);
        this.A01 = ((ActivityC13810kN) this).A09.A00.getInt("privacy_online", 0);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2e();
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        int i = this.A00;
        boolean z = false;
        this.A03.setChecked(C12970iu.A1W(i));
        this.A02.setChecked(C12960it.A1T(i));
        this.A05.setChecked(C12960it.A1V(i, 2));
        this.A04.setChecked(C12960it.A1V(i, 3));
        this.A06.setChecked(C12960it.A1T(this.A01));
        RadioButton radioButton = this.A07;
        if (this.A01 == 4) {
            z = true;
        }
        radioButton.setChecked(z);
    }
}
