package com.whatsapp;

import X.AnonymousClass2GZ;
import X.AnonymousClass3NL;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.TypedValue;

/* loaded from: classes2.dex */
public class WaRoundCornerImageView extends WaImageView {
    public float A00;
    public Path A01;
    public AnonymousClass3NL A02;
    public boolean A03;

    public WaRoundCornerImageView(Context context) {
        super(context);
        A00();
        A00(context, null);
    }

    public WaRoundCornerImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A00(context, attributeSet);
    }

    public WaRoundCornerImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A00(context, attributeSet);
    }

    public WaRoundCornerImageView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    private void A00(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0V);
            try {
                this.A00 = obtainStyledAttributes.getDimension(0, TypedValue.applyDimension(1, 4.0f, getResources().getDisplayMetrics()));
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.A02 = new AnonymousClass3NL(this);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalLayoutListener(this.A02);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        getViewTreeObserver().removeGlobalOnLayoutListener(this.A02);
        super.onDetachedFromWindow();
    }

    @Override // com.whatsapp.WaImageView, android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        Path path = this.A01;
        if (path != null) {
            canvas.clipPath(path);
        }
        super.onDraw(canvas);
    }
}
