package com.whatsapp.base;

import X.AnonymousClass04S;
import X.AnonymousClass0U5;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.C42941w9;
import X.C51122Sx;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.status.playback.fragment.OpenLinkConfirmationDialogFragment;

/* loaded from: classes2.dex */
public class WaDialogFragment extends Hilt_WaDialogFragment {
    public AnonymousClass182 A00;
    public AnonymousClass180 A01;

    @Override // X.AnonymousClass01E
    public void A0n(boolean z) {
        C51122Sx.A02(this, this.A00, this.A01, this.A0j, z);
        super.A0n(z);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        CharSequence text;
        CharSequence text2;
        CharSequence text3;
        super.A0s();
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog instanceof AnonymousClass04S) {
            AnonymousClass04S r3 = (AnonymousClass04S) dialog;
            Button button = r3.A00.A0G;
            if (!(button == null || (text3 = button.getText()) == null)) {
                button.setContentDescription(text3);
            }
            AnonymousClass0U5 r2 = r3.A00;
            Button button2 = r2.A0E;
            if (!(button2 == null || (text2 = button2.getText()) == null)) {
                button2.setContentDescription(text2);
            }
            Button button3 = r2.A0F;
            if (!(button3 == null || (text = button3.getText()) == null)) {
                button3.setContentDescription(text);
            }
            View findViewById = r3.findViewById(16908299);
            if (findViewById == null) {
                return;
            }
            if (!(this instanceof OpenLinkConfirmationDialogFragment)) {
                C42941w9.A04(findViewById);
            } else {
                C42941w9.A03(findViewById);
            }
        }
    }
}
