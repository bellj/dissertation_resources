package com.whatsapp.base;

import X.AbstractC13870kT;
import X.AbstractC33081dJ;
import X.AnonymousClass00E;
import X.AnonymousClass01E;
import X.AnonymousClass01V;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.C51122Sx;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;

/* loaded from: classes2.dex */
public abstract class WaFragment extends AnonymousClass01E implements AbstractC33081dJ, AbstractC13870kT {
    public AnonymousClass182 A00;
    public AnonymousClass180 A01;

    @Override // X.AnonymousClass01E
    public void A0n(boolean z) {
        C51122Sx.A02(this, this.A00, this.A01, this.A0j, z);
        super.A0n(z);
    }

    @Override // X.AbstractC33081dJ
    public /* synthetic */ AnonymousClass00E AGM() {
        if (!(this instanceof StatusPlaybackContactFragment)) {
            return AnonymousClass01V.A02;
        }
        return AnonymousClass01V.A01;
    }
}
