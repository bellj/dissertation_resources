package com.whatsapp.base;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC253919f;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10Y;
import X.AnonymousClass11B;
import X.AnonymousClass11G;
import X.AnonymousClass11I;
import X.AnonymousClass12G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass12V;
import X.AnonymousClass15H;
import X.AnonymousClass17R;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass18U;
import X.AnonymousClass190;
import X.AnonymousClass193;
import X.AnonymousClass19J;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass19Z;
import X.AnonymousClass1AO;
import X.AnonymousClass1AT;
import X.AnonymousClass1B9;
import X.AnonymousClass1BD;
import X.AnonymousClass1CP;
import X.AnonymousClass1E9;
import X.AnonymousClass1FO;
import X.C002701f;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C14920mG;
import X.C15220ml;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15670ni;
import X.C15860o1;
import X.C16120oU;
import X.C16170oZ;
import X.C16370ot;
import X.C16630pM;
import X.C17170qN;
import X.C18000rk;
import X.C18350sJ;
import X.C18470sV;
import X.C18640sm;
import X.C18750sx;
import X.C18770sz;
import X.C18790t3;
import X.C19990v2;
import X.C20220vP;
import X.C20230vQ;
import X.C20640w5;
import X.C20650w6;
import X.C20660w7;
import X.C20670w8;
import X.C20710wC;
import X.C20730wE;
import X.C21320xE;
import X.C21740xu;
import X.C22100yW;
import X.C22190yg;
import X.C22210yi;
import X.C22230yk;
import X.C22330yu;
import X.C22640zP;
import X.C22670zS;
import X.C22680zT;
import X.C22700zV;
import X.C231510o;
import X.C235512c;
import X.C238013b;
import X.C238813j;
import X.C245015t;
import X.C245716a;
import X.C250417w;
import X.C250918b;
import X.C252018m;
import X.C252718t;
import X.C253118x;
import X.C253719d;
import X.C254119h;
import X.C254219i;
import X.C255719x;
import X.C26671Ej;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.CallConfirmationFragment;
import com.whatsapp.DisplayExceptionDialogFactory$ClockWrongDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$ContactBlockedDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$LoginFailedDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment;
import com.whatsapp.Hilt_CallConfirmationFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment;
import com.whatsapp.Hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment;
import com.whatsapp.Hilt_MessageDialogFragment;
import com.whatsapp.Hilt_MuteDialogFragment;
import com.whatsapp.Hilt_PhoneHyperLinkDialogFragment;
import com.whatsapp.Hilt_RevokeLinkConfirmationDialogFragment;
import com.whatsapp.Hilt_SuspiciousLinkWarningDialogFragment;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.PhoneHyperLinkDialogFragment;
import com.whatsapp.RevokeLinkConfirmationDialogFragment;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import com.whatsapp.account.delete.Hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment;
import com.whatsapp.authentication.Hilt_SetupDeviceAuthDialog;
import com.whatsapp.authentication.Hilt_VerifyTwoFactorAuthCodeDialogFragment;
import com.whatsapp.authentication.SetupDeviceAuthDialog;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;
import com.whatsapp.backup.google.Hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment;
import com.whatsapp.backup.google.Hilt_PromptDialogFragment;
import com.whatsapp.backup.google.Hilt_SettingsGoogleDrive_AuthRequestDialogFragment;
import com.whatsapp.backup.google.Hilt_SingleChoiceListDialogFragment;
import com.whatsapp.backup.google.PromptDialogFragment;
import com.whatsapp.backup.google.SingleChoiceListDialogFragment;
import com.whatsapp.biz.product.view.fragment.Hilt_ProductReportReasonDialogFragment;
import com.whatsapp.biz.product.view.fragment.Hilt_ReportProductDialogFragment;
import com.whatsapp.biz.product.view.fragment.ProductReportReasonDialogFragment;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.blockbusiness.blockreasonlist.Hilt_BlockReasonListFragment;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.blocklist.Hilt_BlockConfirmationDialogFragment;
import com.whatsapp.businessdirectory.view.custom.ClearLocationDialogFragment;
import com.whatsapp.businessdirectory.view.custom.Hilt_ClearLocationDialogFragment;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.calling.callhistory.Hilt_CallsHistoryFragment_ClearCallLogDialogFragment;
import com.whatsapp.calling.spam.CallSpamActivity;
import com.whatsapp.calling.spam.Hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment;
import com.whatsapp.calling.videoparticipant.Hilt_MaximizedParticipantVideoDialogFragment;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.calling.views.Hilt_JoinableEducationDialogFragment;
import com.whatsapp.calling.views.Hilt_VoipContactPickerDialogFragment;
import com.whatsapp.calling.views.JoinableEducationDialogFragment;
import com.whatsapp.chatinfo.ChatInfoActivity$EncryptionExplanationDialogFragment;
import com.whatsapp.chatinfo.Hilt_ChatInfoActivity_EncryptionExplanationDialogFragment;
import com.whatsapp.chatinfo.Hilt_ViewPhotoOrStatusDialogFragment;
import com.whatsapp.community.CommunityDeleteDialogFragment;
import com.whatsapp.community.CommunitySpamReportDialogFragment;
import com.whatsapp.community.Hilt_CommunityDeleteDialogFragment;
import com.whatsapp.community.Hilt_CommunitySpamReportDialogFragment;
import com.whatsapp.community.deactivate.DeactivateCommunityConfirmationFragment;
import com.whatsapp.community.deactivate.Hilt_DeactivateCommunityConfirmationFragment;
import com.whatsapp.companiondevice.Hilt_LinkedDevicesDetailDialogFragment;
import com.whatsapp.companiondevice.LinkedDevicesDetailDialogFragment;
import com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.Hilt_BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.Hilt_SharedTextPreviewDialogFragment;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;
import com.whatsapp.contact.picker.invite.Hilt_InviteToGroupCallConfirmationFragment;
import com.whatsapp.contact.picker.invite.InviteToGroupCallConfirmationFragment;
import com.whatsapp.conversation.CapturePictureOrVideoDialogFragment;
import com.whatsapp.conversation.ChangeNumberNotificationDialogFragment;
import com.whatsapp.conversation.ChatMediaVisibilityDialog;
import com.whatsapp.conversation.Hilt_CapturePictureOrVideoDialogFragment;
import com.whatsapp.conversation.Hilt_ChangeNumberNotificationDialogFragment;
import com.whatsapp.conversation.Hilt_ChatMediaEphemeralVisibilityDialog;
import com.whatsapp.conversation.Hilt_ChatMediaVisibilityDialog;
import com.whatsapp.conversation.conversationrow.BusinessTransitionInfoDialogFragment;
import com.whatsapp.conversation.conversationrow.ChatWithBusinessInDirectoryDialogFragment;
import com.whatsapp.conversation.conversationrow.ConversationRow$ConversationRowDialogFragment;
import com.whatsapp.conversation.conversationrow.ConversationRowContact$MessageSharedContactDialogFragment;
import com.whatsapp.conversation.conversationrow.DeviceUpdateDialogFragment;
import com.whatsapp.conversation.conversationrow.EncryptionChangeDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_BusinessTransitionInfoDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_ChatWithBusinessInDirectoryDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_ConversationRowContact_MessageSharedContactDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_ConversationRow_ConversationRowDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_DeviceUpdateDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_EncryptionChangeDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_IdentityChangeDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_SecurityNotificationDialogFragment;
import com.whatsapp.conversation.conversationrow.Hilt_VerifiedBusinessInfoDialogFragment;
import com.whatsapp.conversation.conversationrow.IdentityChangeDialogFragment;
import com.whatsapp.conversation.conversationrow.SecurityNotificationDialogFragment;
import com.whatsapp.conversation.conversationrow.VerifiedBusinessInfoDialogFragment;
import com.whatsapp.conversation.conversationrow.message.Hilt_StarredMessagesActivity_UnstarAllDialogFragment;
import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.conversationslist.Hilt_ConversationsFragment_BulkDeleteConversationDialogFragment;
import com.whatsapp.conversationslist.Hilt_ConversationsFragment_DeleteBroadcastListDialogFragment;
import com.whatsapp.conversationslist.Hilt_ConversationsFragment_DeleteContactDialogFragment;
import com.whatsapp.conversationslist.Hilt_ConversationsFragment_DeleteGroupDialogFragment;
import com.whatsapp.conversationslist.Hilt_LeaveGroupsDialogFragment;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.dialogs.Hilt_CreateOrAddToContactsDialog;
import com.whatsapp.dialogs.Hilt_ProgressDialogFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.documentpicker.DocumentPickerActivity;
import com.whatsapp.documentpicker.Hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment;
import com.whatsapp.ephemeral.ChangeEphemeralSettingsDialog;
import com.whatsapp.ephemeral.EphemeralNUXDialog;
import com.whatsapp.ephemeral.Hilt_ChangeEphemeralSettingsDialog;
import com.whatsapp.ephemeral.Hilt_EphemeralNUXDialog;
import com.whatsapp.ephemeral.Hilt_ViewOnceNUXDialog;
import com.whatsapp.ephemeral.ViewOnceNUXDialog;
import com.whatsapp.groupsuspend.CreateGroupSuspendDialog;
import com.whatsapp.groupsuspend.Hilt_CreateGroupSuspendDialog;
import com.whatsapp.growthlock.Hilt_InviteLinkUnavailableDialogFragment;
import com.whatsapp.growthlock.InviteLinkUnavailableDialogFragment;
import com.whatsapp.http.GoogleSearchDialogFragment;
import com.whatsapp.http.Hilt_GoogleSearchDialogFragment;
import com.whatsapp.invites.Hilt_PromptSendGroupInviteDialogFragment;
import com.whatsapp.invites.Hilt_RevokeInviteDialogFragment;
import com.whatsapp.invites.PromptSendGroupInviteDialogFragment;
import com.whatsapp.invites.RevokeInviteDialogFragment;
import com.whatsapp.mediaview.DeleteMessagesDialogFragment;
import com.whatsapp.mediaview.Hilt_DeleteMessagesDialogFragment;
import com.whatsapp.mediaview.Hilt_RevokeNuxDialogFragment;
import com.whatsapp.mediaview.RevokeNuxDialogFragment;
import com.whatsapp.phonematching.Hilt_ConnectionProgressDialogFragment;
import com.whatsapp.picker.search.GifSearchDialogFragment;
import com.whatsapp.picker.search.Hilt_GifSearchDialogFragment;
import com.whatsapp.picker.search.Hilt_PickerSearchDialogFragment;
import com.whatsapp.picker.search.Hilt_StickerSearchDialogFragment;
import com.whatsapp.picker.search.StickerSearchDialogFragment;
import com.whatsapp.profile.Hilt_ResetGroupPhoto_ConfirmDialogFragment;
import com.whatsapp.profile.Hilt_ResetPhoto_ConfirmDialogFragment;
import com.whatsapp.profile.ResetPhoto;
import com.whatsapp.qrcode.Hilt_QrEducationDialogFragment;
import com.whatsapp.qrcode.contactqr.Hilt_ErrorDialogFragment;
import com.whatsapp.qrcode.contactqr.Hilt_WebCodeDialogFragment;
import com.whatsapp.qrcode.contactqr.WebCodeDialogFragment;
import com.whatsapp.registration.Hilt_SelectPhoneNumberDialog;
import com.whatsapp.registration.Hilt_VerifyTwoFactorAuth_ConfirmResetCode;
import com.whatsapp.registration.SelectPhoneNumberDialog;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.report.DeleteReportConfirmationDialogFragment;
import com.whatsapp.report.Hilt_DeleteReportConfirmationDialogFragment;
import com.whatsapp.settings.Hilt_MultiSelectionDialogFragment;
import com.whatsapp.settings.chat.wallpaper.Hilt_WallpaperDownloadFailedDialogFragment;
import com.whatsapp.status.Hilt_StatusConfirmMuteDialogFragment;
import com.whatsapp.status.Hilt_StatusConfirmUnmuteDialogFragment;
import com.whatsapp.status.Hilt_StatusDeleteDialogFragment;
import com.whatsapp.status.StatusConfirmMuteDialogFragment;
import com.whatsapp.status.StatusConfirmUnmuteDialogFragment;
import com.whatsapp.status.StatusDeleteDialogFragment;
import com.whatsapp.status.playback.fragment.Hilt_OpenLinkConfirmationDialogFragment;
import com.whatsapp.status.playback.fragment.OpenLinkConfirmationDialogFragment;
import com.whatsapp.status.posting.FirstStatusConfirmationDialogFragment;
import com.whatsapp.status.posting.Hilt_FirstStatusConfirmationDialogFragment;
import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;
import com.whatsapp.stickers.ConfirmPackDeleteDialogFragment;
import com.whatsapp.stickers.Hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment;
import com.whatsapp.stickers.Hilt_ConfirmPackDeleteDialogFragment;
import com.whatsapp.stickers.Hilt_RemoveStickerFromFavoritesDialogFragment;
import com.whatsapp.stickers.Hilt_StarOrRemoveFromRecentsStickerDialogFragment;
import com.whatsapp.stickers.Hilt_StarStickerFromPickerDialogFragment;
import com.whatsapp.stickers.Hilt_StickerInfoDialogFragment;
import com.whatsapp.stickers.RemoveStickerFromFavoritesDialogFragment;
import com.whatsapp.stickers.StarOrRemoveFromRecentsStickerDialogFragment;
import com.whatsapp.stickers.StarStickerFromPickerDialogFragment;
import com.whatsapp.stickers.StickerInfoDialogFragment;
import com.whatsapp.stickers.picker.pages.Hilt_ThirdPartyPackPage_StickerBlockedDialogFragment;
import com.whatsapp.storage.Hilt_StorageUsageDeleteCompleteDialogFragment;
import com.whatsapp.storage.Hilt_StorageUsageDeleteMessagesDialogFragment;
import com.whatsapp.storage.StorageUsageDeleteCompleteDialogFragment;
import com.whatsapp.storage.StorageUsageDeleteMessagesDialogFragment;
import com.whatsapp.twofactor.Hilt_SetEmailFragment_ConfirmSkipEmailDialog;
import com.whatsapp.twofactor.Hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;
import com.whatsapp.util.DocumentWarningDialogFragment;
import com.whatsapp.util.Hilt_DocumentWarningDialogFragment;
import com.whatsapp.voipcalling.Hilt_VoipActivityV2_E2EEInfoDialogFragment;
import com.whatsapp.voipcalling.Hilt_VoipActivityV2_EndCallConfirmationDialogFragment;
import com.whatsapp.voipcalling.Hilt_VoipActivityV2_NonActivityDismissDialogFragment;
import com.whatsapp.voipcalling.Hilt_VoipActivityV2_ReplyWithMessageDialogFragment;
import com.whatsapp.voipcalling.Hilt_VoipActivityV2_SwitchConfirmationFragment;
import com.whatsapp.voipcalling.Hilt_VoipErrorDialogFragment;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipErrorDialogFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_WaDialogFragment extends DialogFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A01() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A01();
        return this.A00;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A01()
            r3.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.base.Hilt_WaDialogFragment.A0u(android.app.Activity):void");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A01();
        A1I();
    }

    public void A1I() {
        if (this instanceof Hilt_VoipErrorDialogFragment) {
            Hilt_VoipErrorDialogFragment hilt_VoipErrorDialogFragment = (Hilt_VoipErrorDialogFragment) this;
            if (!hilt_VoipErrorDialogFragment.A02) {
                hilt_VoipErrorDialogFragment.A02 = true;
                VoipErrorDialogFragment voipErrorDialogFragment = (VoipErrorDialogFragment) hilt_VoipErrorDialogFragment;
                AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipErrorDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) voipErrorDialogFragment).A00 = (AnonymousClass182) r1.A94.get();
                ((WaDialogFragment) voipErrorDialogFragment).A01 = (AnonymousClass180) r1.ALt.get();
                voipErrorDialogFragment.A02 = (C15450nH) r1.AII.get();
                voipErrorDialogFragment.A06 = (AnonymousClass018) r1.ANb.get();
                voipErrorDialogFragment.A04 = (C15550nR) r1.A45.get();
                voipErrorDialogFragment.A05 = (C15610nY) r1.AMe.get();
            }
        } else if (this instanceof Hilt_VoipActivityV2_SwitchConfirmationFragment) {
            Hilt_VoipActivityV2_SwitchConfirmationFragment hilt_VoipActivityV2_SwitchConfirmationFragment = (Hilt_VoipActivityV2_SwitchConfirmationFragment) this;
            if (!hilt_VoipActivityV2_SwitchConfirmationFragment.A02) {
                hilt_VoipActivityV2_SwitchConfirmationFragment.A02 = true;
                VoipActivityV2.SwitchConfirmationFragment switchConfirmationFragment = (VoipActivityV2.SwitchConfirmationFragment) hilt_VoipActivityV2_SwitchConfirmationFragment;
                AnonymousClass01J r12 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipActivityV2_SwitchConfirmationFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) switchConfirmationFragment).A00 = (AnonymousClass182) r12.A94.get();
                ((WaDialogFragment) switchConfirmationFragment).A01 = (AnonymousClass180) r12.ALt.get();
                switchConfirmationFragment.A00 = (C14820m6) r12.AN3.get();
            }
        } else if (this instanceof Hilt_VoipActivityV2_ReplyWithMessageDialogFragment) {
            Hilt_VoipActivityV2_ReplyWithMessageDialogFragment hilt_VoipActivityV2_ReplyWithMessageDialogFragment = (Hilt_VoipActivityV2_ReplyWithMessageDialogFragment) this;
            if (!hilt_VoipActivityV2_ReplyWithMessageDialogFragment.A02) {
                hilt_VoipActivityV2_ReplyWithMessageDialogFragment.A02 = true;
                VoipActivityV2.ReplyWithMessageDialogFragment replyWithMessageDialogFragment = (VoipActivityV2.ReplyWithMessageDialogFragment) hilt_VoipActivityV2_ReplyWithMessageDialogFragment;
                AnonymousClass01J r13 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipActivityV2_ReplyWithMessageDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) replyWithMessageDialogFragment).A00 = (AnonymousClass182) r13.A94.get();
                ((WaDialogFragment) replyWithMessageDialogFragment).A01 = (AnonymousClass180) r13.ALt.get();
                replyWithMessageDialogFragment.A00 = (C14830m7) r13.ALb.get();
                replyWithMessageDialogFragment.A01 = (AnonymousClass018) r13.ANb.get();
            }
        } else if (this instanceof Hilt_VoipActivityV2_NonActivityDismissDialogFragment) {
            Hilt_VoipActivityV2_NonActivityDismissDialogFragment hilt_VoipActivityV2_NonActivityDismissDialogFragment = (Hilt_VoipActivityV2_NonActivityDismissDialogFragment) this;
            if (!hilt_VoipActivityV2_NonActivityDismissDialogFragment.A02) {
                hilt_VoipActivityV2_NonActivityDismissDialogFragment.A02 = true;
                AnonymousClass01J r14 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipActivityV2_NonActivityDismissDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_VoipActivityV2_NonActivityDismissDialogFragment).A00 = (AnonymousClass182) r14.A94.get();
                ((WaDialogFragment) hilt_VoipActivityV2_NonActivityDismissDialogFragment).A01 = (AnonymousClass180) r14.ALt.get();
            }
        } else if (this instanceof Hilt_VoipActivityV2_EndCallConfirmationDialogFragment) {
            Hilt_VoipActivityV2_EndCallConfirmationDialogFragment hilt_VoipActivityV2_EndCallConfirmationDialogFragment = (Hilt_VoipActivityV2_EndCallConfirmationDialogFragment) this;
            if (!hilt_VoipActivityV2_EndCallConfirmationDialogFragment.A02) {
                hilt_VoipActivityV2_EndCallConfirmationDialogFragment.A02 = true;
                AnonymousClass01J r15 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipActivityV2_EndCallConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_VoipActivityV2_EndCallConfirmationDialogFragment).A00 = (AnonymousClass182) r15.A94.get();
                ((WaDialogFragment) hilt_VoipActivityV2_EndCallConfirmationDialogFragment).A01 = (AnonymousClass180) r15.ALt.get();
            }
        } else if (this instanceof Hilt_VoipActivityV2_E2EEInfoDialogFragment) {
            Hilt_VoipActivityV2_E2EEInfoDialogFragment hilt_VoipActivityV2_E2EEInfoDialogFragment = (Hilt_VoipActivityV2_E2EEInfoDialogFragment) this;
            if (!hilt_VoipActivityV2_E2EEInfoDialogFragment.A02) {
                hilt_VoipActivityV2_E2EEInfoDialogFragment.A02 = true;
                VoipActivityV2.E2EEInfoDialogFragment e2EEInfoDialogFragment = (VoipActivityV2.E2EEInfoDialogFragment) hilt_VoipActivityV2_E2EEInfoDialogFragment;
                AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipActivityV2_E2EEInfoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) e2EEInfoDialogFragment).A00 = (AnonymousClass182) r2.A94.get();
                ((WaDialogFragment) e2EEInfoDialogFragment).A01 = (AnonymousClass180) r2.ALt.get();
                e2EEInfoDialogFragment.A02 = (C14850m9) r2.A04.get();
                e2EEInfoDialogFragment.A03 = (C16120oU) r2.ANE.get();
                e2EEInfoDialogFragment.A01 = (AnonymousClass12P) r2.A0H.get();
                e2EEInfoDialogFragment.A05 = (C252018m) r2.A7g.get();
                e2EEInfoDialogFragment.A04 = (AnonymousClass1AT) r2.AG2.get();
            }
        } else if (this instanceof Hilt_DocumentWarningDialogFragment) {
            Hilt_DocumentWarningDialogFragment hilt_DocumentWarningDialogFragment = (Hilt_DocumentWarningDialogFragment) this;
            if (!hilt_DocumentWarningDialogFragment.A02) {
                hilt_DocumentWarningDialogFragment.A02 = true;
                DocumentWarningDialogFragment documentWarningDialogFragment = (DocumentWarningDialogFragment) hilt_DocumentWarningDialogFragment;
                AnonymousClass01J r22 = ((C51112Sw) ((AbstractC51092Su) hilt_DocumentWarningDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) documentWarningDialogFragment).A00 = (AnonymousClass182) r22.A94.get();
                ((WaDialogFragment) documentWarningDialogFragment).A01 = (AnonymousClass180) r22.ALt.get();
                documentWarningDialogFragment.A02 = (C14900mE) r22.A8X.get();
                documentWarningDialogFragment.A01 = (AbstractC15710nm) r22.A4o.get();
                documentWarningDialogFragment.A06 = (AbstractC14440lR) r22.ANe.get();
                documentWarningDialogFragment.A03 = (C15450nH) r22.AII.get();
                documentWarningDialogFragment.A00 = (AnonymousClass12P) r22.A0H.get();
                documentWarningDialogFragment.A04 = (C15650ng) r22.A4m.get();
                documentWarningDialogFragment.A05 = (C15670ni) r22.AIb.get();
            }
        } else if (this instanceof Hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog) {
            Hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog = (Hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog) this;
            if (!hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog.A02) {
                hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog.A02 = true;
                SettingsTwoFactorAuthActivity.ConfirmDisableDialog confirmDisableDialog = (SettingsTwoFactorAuthActivity.ConfirmDisableDialog) hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog;
                AnonymousClass01J r16 = ((C51112Sw) ((AbstractC51092Su) hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) confirmDisableDialog).A00 = (AnonymousClass182) r16.A94.get();
                ((WaDialogFragment) confirmDisableDialog).A01 = (AnonymousClass180) r16.ALt.get();
                confirmDisableDialog.A00 = (AnonymousClass018) r16.ANb.get();
            }
        } else if (this instanceof Hilt_SetEmailFragment_ConfirmSkipEmailDialog) {
            Hilt_SetEmailFragment_ConfirmSkipEmailDialog hilt_SetEmailFragment_ConfirmSkipEmailDialog = (Hilt_SetEmailFragment_ConfirmSkipEmailDialog) this;
            if (!hilt_SetEmailFragment_ConfirmSkipEmailDialog.A02) {
                hilt_SetEmailFragment_ConfirmSkipEmailDialog.A02 = true;
                AnonymousClass01J r17 = ((C51112Sw) ((AbstractC51092Su) hilt_SetEmailFragment_ConfirmSkipEmailDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_SetEmailFragment_ConfirmSkipEmailDialog).A00 = (AnonymousClass182) r17.A94.get();
                ((WaDialogFragment) hilt_SetEmailFragment_ConfirmSkipEmailDialog).A01 = (AnonymousClass180) r17.ALt.get();
            }
        } else if (this instanceof Hilt_StorageUsageDeleteMessagesDialogFragment) {
            Hilt_StorageUsageDeleteMessagesDialogFragment hilt_StorageUsageDeleteMessagesDialogFragment = (Hilt_StorageUsageDeleteMessagesDialogFragment) this;
            if (!hilt_StorageUsageDeleteMessagesDialogFragment.A02) {
                hilt_StorageUsageDeleteMessagesDialogFragment.A02 = true;
                StorageUsageDeleteMessagesDialogFragment storageUsageDeleteMessagesDialogFragment = (StorageUsageDeleteMessagesDialogFragment) hilt_StorageUsageDeleteMessagesDialogFragment;
                AnonymousClass01J r18 = ((C51112Sw) ((AbstractC51092Su) hilt_StorageUsageDeleteMessagesDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) storageUsageDeleteMessagesDialogFragment).A00 = (AnonymousClass182) r18.A94.get();
                ((WaDialogFragment) storageUsageDeleteMessagesDialogFragment).A01 = (AnonymousClass180) r18.ALt.get();
                storageUsageDeleteMessagesDialogFragment.A03 = (AbstractC14440lR) r18.ANe.get();
                storageUsageDeleteMessagesDialogFragment.A00 = (C16170oZ) r18.AM4.get();
                storageUsageDeleteMessagesDialogFragment.A01 = (AnonymousClass018) r18.ANb.get();
            }
        } else if (this instanceof Hilt_StorageUsageDeleteCompleteDialogFragment) {
            Hilt_StorageUsageDeleteCompleteDialogFragment hilt_StorageUsageDeleteCompleteDialogFragment = (Hilt_StorageUsageDeleteCompleteDialogFragment) this;
            if (!hilt_StorageUsageDeleteCompleteDialogFragment.A02) {
                hilt_StorageUsageDeleteCompleteDialogFragment.A02 = true;
                StorageUsageDeleteCompleteDialogFragment storageUsageDeleteCompleteDialogFragment = (StorageUsageDeleteCompleteDialogFragment) hilt_StorageUsageDeleteCompleteDialogFragment;
                AnonymousClass01J r19 = ((C51112Sw) ((AbstractC51092Su) hilt_StorageUsageDeleteCompleteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) storageUsageDeleteCompleteDialogFragment).A00 = (AnonymousClass182) r19.A94.get();
                ((WaDialogFragment) storageUsageDeleteCompleteDialogFragment).A01 = (AnonymousClass180) r19.ALt.get();
                storageUsageDeleteCompleteDialogFragment.A00 = (C14900mE) r19.A8X.get();
                storageUsageDeleteCompleteDialogFragment.A01 = (AnonymousClass018) r19.ANb.get();
            }
        } else if (this instanceof Hilt_ThirdPartyPackPage_StickerBlockedDialogFragment) {
            Hilt_ThirdPartyPackPage_StickerBlockedDialogFragment hilt_ThirdPartyPackPage_StickerBlockedDialogFragment = (Hilt_ThirdPartyPackPage_StickerBlockedDialogFragment) this;
            if (!hilt_ThirdPartyPackPage_StickerBlockedDialogFragment.A02) {
                hilt_ThirdPartyPackPage_StickerBlockedDialogFragment.A02 = true;
                AnonymousClass01J r110 = ((C51112Sw) ((AbstractC51092Su) hilt_ThirdPartyPackPage_StickerBlockedDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ThirdPartyPackPage_StickerBlockedDialogFragment).A00 = (AnonymousClass182) r110.A94.get();
                ((WaDialogFragment) hilt_ThirdPartyPackPage_StickerBlockedDialogFragment).A01 = (AnonymousClass180) r110.ALt.get();
            }
        } else if (this instanceof Hilt_StickerInfoDialogFragment) {
            Hilt_StickerInfoDialogFragment hilt_StickerInfoDialogFragment = (Hilt_StickerInfoDialogFragment) this;
            if (!hilt_StickerInfoDialogFragment.A02) {
                hilt_StickerInfoDialogFragment.A02 = true;
                StickerInfoDialogFragment stickerInfoDialogFragment = (StickerInfoDialogFragment) hilt_StickerInfoDialogFragment;
                AnonymousClass01J r23 = ((C51112Sw) ((AbstractC51092Su) hilt_StickerInfoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) stickerInfoDialogFragment).A00 = (AnonymousClass182) r23.A94.get();
                ((WaDialogFragment) stickerInfoDialogFragment).A01 = (AnonymousClass180) r23.ALt.get();
                stickerInfoDialogFragment.A0A = (C14850m9) r23.A04.get();
                stickerInfoDialogFragment.A0K = (AbstractC14440lR) r23.ANe.get();
                stickerInfoDialogFragment.A09 = (AnonymousClass12P) r23.A0H.get();
                stickerInfoDialogFragment.A0H = (C235512c) r23.AKS.get();
                stickerInfoDialogFragment.A0B = (AnonymousClass12V) r23.A0p.get();
                stickerInfoDialogFragment.A0C = r23.A41();
                stickerInfoDialogFragment.A0D = r23.A42();
                stickerInfoDialogFragment.A0J = (C26671Ej) r23.AKR.get();
            }
        } else if (this instanceof Hilt_StarStickerFromPickerDialogFragment) {
            Hilt_StarStickerFromPickerDialogFragment hilt_StarStickerFromPickerDialogFragment = (Hilt_StarStickerFromPickerDialogFragment) this;
            if (!hilt_StarStickerFromPickerDialogFragment.A02) {
                hilt_StarStickerFromPickerDialogFragment.A02 = true;
                StarStickerFromPickerDialogFragment starStickerFromPickerDialogFragment = (StarStickerFromPickerDialogFragment) hilt_StarStickerFromPickerDialogFragment;
                AnonymousClass01J r111 = ((C51112Sw) ((AbstractC51092Su) hilt_StarStickerFromPickerDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) starStickerFromPickerDialogFragment).A00 = (AnonymousClass182) r111.A94.get();
                ((WaDialogFragment) starStickerFromPickerDialogFragment).A01 = (AnonymousClass180) r111.ALt.get();
                starStickerFromPickerDialogFragment.A04 = (AbstractC14440lR) r111.ANe.get();
                starStickerFromPickerDialogFragment.A03 = (C235512c) r111.AKS.get();
                starStickerFromPickerDialogFragment.A00 = (C002701f) r111.AHU.get();
            }
        } else if (this instanceof Hilt_StarOrRemoveFromRecentsStickerDialogFragment) {
            Hilt_StarOrRemoveFromRecentsStickerDialogFragment hilt_StarOrRemoveFromRecentsStickerDialogFragment = (Hilt_StarOrRemoveFromRecentsStickerDialogFragment) this;
            if (!hilt_StarOrRemoveFromRecentsStickerDialogFragment.A02) {
                hilt_StarOrRemoveFromRecentsStickerDialogFragment.A02 = true;
                StarOrRemoveFromRecentsStickerDialogFragment starOrRemoveFromRecentsStickerDialogFragment = (StarOrRemoveFromRecentsStickerDialogFragment) hilt_StarOrRemoveFromRecentsStickerDialogFragment;
                AnonymousClass01J r112 = ((C51112Sw) ((AbstractC51092Su) hilt_StarOrRemoveFromRecentsStickerDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) starOrRemoveFromRecentsStickerDialogFragment).A00 = (AnonymousClass182) r112.A94.get();
                ((WaDialogFragment) starOrRemoveFromRecentsStickerDialogFragment).A01 = (AnonymousClass180) r112.ALt.get();
                starOrRemoveFromRecentsStickerDialogFragment.A01 = (C22210yi) r112.AGm.get();
                starOrRemoveFromRecentsStickerDialogFragment.A00 = (C22210yi) r112.AGT.get();
                starOrRemoveFromRecentsStickerDialogFragment.A03 = (C235512c) r112.AKS.get();
            }
        } else if (this instanceof Hilt_RemoveStickerFromFavoritesDialogFragment) {
            Hilt_RemoveStickerFromFavoritesDialogFragment hilt_RemoveStickerFromFavoritesDialogFragment = (Hilt_RemoveStickerFromFavoritesDialogFragment) this;
            if (!hilt_RemoveStickerFromFavoritesDialogFragment.A02) {
                hilt_RemoveStickerFromFavoritesDialogFragment.A02 = true;
                RemoveStickerFromFavoritesDialogFragment removeStickerFromFavoritesDialogFragment = (RemoveStickerFromFavoritesDialogFragment) hilt_RemoveStickerFromFavoritesDialogFragment;
                AnonymousClass01J r113 = ((C51112Sw) ((AbstractC51092Su) hilt_RemoveStickerFromFavoritesDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) removeStickerFromFavoritesDialogFragment).A00 = (AnonymousClass182) r113.A94.get();
                ((WaDialogFragment) removeStickerFromFavoritesDialogFragment).A01 = (AnonymousClass180) r113.ALt.get();
                removeStickerFromFavoritesDialogFragment.A01 = (C235512c) r113.AKS.get();
            }
        } else if (this instanceof Hilt_ConfirmPackDeleteDialogFragment) {
            Hilt_ConfirmPackDeleteDialogFragment hilt_ConfirmPackDeleteDialogFragment = (Hilt_ConfirmPackDeleteDialogFragment) this;
            if (!hilt_ConfirmPackDeleteDialogFragment.A02) {
                hilt_ConfirmPackDeleteDialogFragment.A02 = true;
                ConfirmPackDeleteDialogFragment confirmPackDeleteDialogFragment = (ConfirmPackDeleteDialogFragment) hilt_ConfirmPackDeleteDialogFragment;
                AnonymousClass01J r114 = ((C51112Sw) ((AbstractC51092Su) hilt_ConfirmPackDeleteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) confirmPackDeleteDialogFragment).A00 = (AnonymousClass182) r114.A94.get();
                ((WaDialogFragment) confirmPackDeleteDialogFragment).A01 = (AnonymousClass180) r114.ALt.get();
                confirmPackDeleteDialogFragment.A00 = (C235512c) r114.AKS.get();
            }
        } else if (this instanceof Hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment) {
            Hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment = (Hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment) this;
            if (!hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment.A02) {
                hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment.A02 = true;
                AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment addStickerPackDialogFragment = (AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment) hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment;
                C51112Sw r24 = (C51112Sw) ((AbstractC51092Su) hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment.generatedComponent());
                AnonymousClass01J r115 = r24.A0Y;
                ((WaDialogFragment) addStickerPackDialogFragment).A00 = (AnonymousClass182) r115.A94.get();
                ((WaDialogFragment) addStickerPackDialogFragment).A01 = (AnonymousClass180) r115.ALt.get();
                addStickerPackDialogFragment.A00 = (C14900mE) r115.A8X.get();
                addStickerPackDialogFragment.A01 = (AnonymousClass018) r115.ANb.get();
                addStickerPackDialogFragment.A02 = r24.A0V.A0K();
            }
        } else if (this instanceof Hilt_FirstStatusConfirmationDialogFragment) {
            Hilt_FirstStatusConfirmationDialogFragment hilt_FirstStatusConfirmationDialogFragment = (Hilt_FirstStatusConfirmationDialogFragment) this;
            if (!hilt_FirstStatusConfirmationDialogFragment.A02) {
                hilt_FirstStatusConfirmationDialogFragment.A02 = true;
                FirstStatusConfirmationDialogFragment firstStatusConfirmationDialogFragment = (FirstStatusConfirmationDialogFragment) hilt_FirstStatusConfirmationDialogFragment;
                AnonymousClass01J r116 = ((C51112Sw) ((AbstractC51092Su) hilt_FirstStatusConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) firstStatusConfirmationDialogFragment).A00 = (AnonymousClass182) r116.A94.get();
                ((WaDialogFragment) firstStatusConfirmationDialogFragment).A01 = (AnonymousClass180) r116.ALt.get();
                firstStatusConfirmationDialogFragment.A03 = (C18470sV) r116.AK8.get();
                firstStatusConfirmationDialogFragment.A01 = (C20670w8) r116.AMw.get();
                firstStatusConfirmationDialogFragment.A02 = (AnonymousClass018) r116.ANb.get();
            }
        } else if (this instanceof Hilt_OpenLinkConfirmationDialogFragment) {
            Hilt_OpenLinkConfirmationDialogFragment hilt_OpenLinkConfirmationDialogFragment = (Hilt_OpenLinkConfirmationDialogFragment) this;
            if (!hilt_OpenLinkConfirmationDialogFragment.A02) {
                hilt_OpenLinkConfirmationDialogFragment.A02 = true;
                OpenLinkConfirmationDialogFragment openLinkConfirmationDialogFragment = (OpenLinkConfirmationDialogFragment) hilt_OpenLinkConfirmationDialogFragment;
                AnonymousClass01J r117 = ((C51112Sw) ((AbstractC51092Su) hilt_OpenLinkConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) openLinkConfirmationDialogFragment).A00 = (AnonymousClass182) r117.A94.get();
                ((WaDialogFragment) openLinkConfirmationDialogFragment).A01 = (AnonymousClass180) r117.ALt.get();
                openLinkConfirmationDialogFragment.A00 = (AnonymousClass1BD) r117.AKA.get();
            }
        } else if (this instanceof Hilt_StatusDeleteDialogFragment) {
            Hilt_StatusDeleteDialogFragment hilt_StatusDeleteDialogFragment = (Hilt_StatusDeleteDialogFragment) this;
            if (!hilt_StatusDeleteDialogFragment.A02) {
                hilt_StatusDeleteDialogFragment.A02 = true;
                StatusDeleteDialogFragment statusDeleteDialogFragment = (StatusDeleteDialogFragment) hilt_StatusDeleteDialogFragment;
                AnonymousClass01J r25 = ((C51112Sw) ((AbstractC51092Su) hilt_StatusDeleteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) statusDeleteDialogFragment).A00 = (AnonymousClass182) r25.A94.get();
                ((WaDialogFragment) statusDeleteDialogFragment).A01 = (AnonymousClass180) r25.ALt.get();
                statusDeleteDialogFragment.A00 = (C14900mE) r25.A8X.get();
                statusDeleteDialogFragment.A04 = (AnonymousClass19M) r25.A6R.get();
                statusDeleteDialogFragment.A01 = (C16170oZ) r25.AM4.get();
                statusDeleteDialogFragment.A02 = (AnonymousClass018) r25.ANb.get();
                statusDeleteDialogFragment.A03 = (C15650ng) r25.A4m.get();
                statusDeleteDialogFragment.A06 = C18000rk.A00(r25.A4v);
            }
        } else if (this instanceof Hilt_StatusConfirmUnmuteDialogFragment) {
            Hilt_StatusConfirmUnmuteDialogFragment hilt_StatusConfirmUnmuteDialogFragment = (Hilt_StatusConfirmUnmuteDialogFragment) this;
            if (!hilt_StatusConfirmUnmuteDialogFragment.A02) {
                hilt_StatusConfirmUnmuteDialogFragment.A02 = true;
                StatusConfirmUnmuteDialogFragment statusConfirmUnmuteDialogFragment = (StatusConfirmUnmuteDialogFragment) hilt_StatusConfirmUnmuteDialogFragment;
                AnonymousClass01J r26 = ((C51112Sw) ((AbstractC51092Su) hilt_StatusConfirmUnmuteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) statusConfirmUnmuteDialogFragment).A00 = (AnonymousClass182) r26.A94.get();
                ((WaDialogFragment) statusConfirmUnmuteDialogFragment).A01 = (AnonymousClass180) r26.ALt.get();
                statusConfirmUnmuteDialogFragment.A04 = (AnonymousClass1E9) r26.AJy.get();
                statusConfirmUnmuteDialogFragment.A00 = (C15550nR) r26.A45.get();
                statusConfirmUnmuteDialogFragment.A01 = (C15610nY) r26.AMe.get();
                statusConfirmUnmuteDialogFragment.A02 = (AnonymousClass018) r26.ANb.get();
                statusConfirmUnmuteDialogFragment.A05 = (AnonymousClass1BD) r26.AKA.get();
            }
        } else if (this instanceof Hilt_StatusConfirmMuteDialogFragment) {
            Hilt_StatusConfirmMuteDialogFragment hilt_StatusConfirmMuteDialogFragment = (Hilt_StatusConfirmMuteDialogFragment) this;
            if (!hilt_StatusConfirmMuteDialogFragment.A02) {
                hilt_StatusConfirmMuteDialogFragment.A02 = true;
                StatusConfirmMuteDialogFragment statusConfirmMuteDialogFragment = (StatusConfirmMuteDialogFragment) hilt_StatusConfirmMuteDialogFragment;
                AnonymousClass01J r118 = ((C51112Sw) ((AbstractC51092Su) hilt_StatusConfirmMuteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) statusConfirmMuteDialogFragment).A00 = (AnonymousClass182) r118.A94.get();
                ((WaDialogFragment) statusConfirmMuteDialogFragment).A01 = (AnonymousClass180) r118.ALt.get();
                statusConfirmMuteDialogFragment.A03 = (AnonymousClass1E9) r118.AJy.get();
                statusConfirmMuteDialogFragment.A00 = (C15550nR) r118.A45.get();
                statusConfirmMuteDialogFragment.A01 = (C15610nY) r118.AMe.get();
                statusConfirmMuteDialogFragment.A04 = (AnonymousClass1BD) r118.AKA.get();
            }
        } else if (this instanceof Hilt_WallpaperDownloadFailedDialogFragment) {
            Hilt_WallpaperDownloadFailedDialogFragment hilt_WallpaperDownloadFailedDialogFragment = (Hilt_WallpaperDownloadFailedDialogFragment) this;
            if (!hilt_WallpaperDownloadFailedDialogFragment.A02) {
                hilt_WallpaperDownloadFailedDialogFragment.A02 = true;
                AnonymousClass01J r119 = ((C51112Sw) ((AbstractC51092Su) hilt_WallpaperDownloadFailedDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_WallpaperDownloadFailedDialogFragment).A00 = (AnonymousClass182) r119.A94.get();
                ((WaDialogFragment) hilt_WallpaperDownloadFailedDialogFragment).A01 = (AnonymousClass180) r119.ALt.get();
            }
        } else if (this instanceof Hilt_MultiSelectionDialogFragment) {
            Hilt_MultiSelectionDialogFragment hilt_MultiSelectionDialogFragment = (Hilt_MultiSelectionDialogFragment) this;
            if (!hilt_MultiSelectionDialogFragment.A02) {
                hilt_MultiSelectionDialogFragment.A02 = true;
                AnonymousClass01J r120 = ((C51112Sw) ((AbstractC51092Su) hilt_MultiSelectionDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_MultiSelectionDialogFragment).A00 = (AnonymousClass182) r120.A94.get();
                ((WaDialogFragment) hilt_MultiSelectionDialogFragment).A01 = (AnonymousClass180) r120.ALt.get();
            }
        } else if (this instanceof Hilt_DeleteReportConfirmationDialogFragment) {
            Hilt_DeleteReportConfirmationDialogFragment hilt_DeleteReportConfirmationDialogFragment = (Hilt_DeleteReportConfirmationDialogFragment) this;
            if (!hilt_DeleteReportConfirmationDialogFragment.A02) {
                hilt_DeleteReportConfirmationDialogFragment.A02 = true;
                DeleteReportConfirmationDialogFragment deleteReportConfirmationDialogFragment = (DeleteReportConfirmationDialogFragment) hilt_DeleteReportConfirmationDialogFragment;
                AnonymousClass01J r121 = ((C51112Sw) ((AbstractC51092Su) hilt_DeleteReportConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deleteReportConfirmationDialogFragment).A00 = (AnonymousClass182) r121.A94.get();
                ((WaDialogFragment) deleteReportConfirmationDialogFragment).A01 = (AnonymousClass180) r121.ALt.get();
                deleteReportConfirmationDialogFragment.A00 = (AnonymousClass018) r121.ANb.get();
            }
        } else if (this instanceof Hilt_VerifyTwoFactorAuth_ConfirmResetCode) {
            Hilt_VerifyTwoFactorAuth_ConfirmResetCode hilt_VerifyTwoFactorAuth_ConfirmResetCode = (Hilt_VerifyTwoFactorAuth_ConfirmResetCode) this;
            if (!hilt_VerifyTwoFactorAuth_ConfirmResetCode.A02) {
                hilt_VerifyTwoFactorAuth_ConfirmResetCode.A02 = true;
                VerifyTwoFactorAuth.ConfirmResetCode confirmResetCode = (VerifyTwoFactorAuth.ConfirmResetCode) hilt_VerifyTwoFactorAuth_ConfirmResetCode;
                AnonymousClass01J r122 = ((C51112Sw) ((AbstractC51092Su) hilt_VerifyTwoFactorAuth_ConfirmResetCode.generatedComponent())).A0Y;
                ((WaDialogFragment) confirmResetCode).A00 = (AnonymousClass182) r122.A94.get();
                ((WaDialogFragment) confirmResetCode).A01 = (AnonymousClass180) r122.ALt.get();
                confirmResetCode.A00 = (AnonymousClass018) r122.ANb.get();
            }
        } else if (this instanceof Hilt_SelectPhoneNumberDialog) {
            Hilt_SelectPhoneNumberDialog hilt_SelectPhoneNumberDialog = (Hilt_SelectPhoneNumberDialog) this;
            if (!hilt_SelectPhoneNumberDialog.A02) {
                hilt_SelectPhoneNumberDialog.A02 = true;
                SelectPhoneNumberDialog selectPhoneNumberDialog = (SelectPhoneNumberDialog) hilt_SelectPhoneNumberDialog;
                AnonymousClass01J r123 = ((C51112Sw) ((AbstractC51092Su) hilt_SelectPhoneNumberDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) selectPhoneNumberDialog).A00 = (AnonymousClass182) r123.A94.get();
                ((WaDialogFragment) selectPhoneNumberDialog).A01 = (AnonymousClass180) r123.ALt.get();
                selectPhoneNumberDialog.A01 = (AnonymousClass018) r123.ANb.get();
                selectPhoneNumberDialog.A00 = (C22680zT) r123.AGW.get();
            }
        } else if (this instanceof Hilt_WebCodeDialogFragment) {
            Hilt_WebCodeDialogFragment hilt_WebCodeDialogFragment = (Hilt_WebCodeDialogFragment) this;
            if (!hilt_WebCodeDialogFragment.A02) {
                hilt_WebCodeDialogFragment.A02 = true;
                WebCodeDialogFragment webCodeDialogFragment = (WebCodeDialogFragment) hilt_WebCodeDialogFragment;
                AnonymousClass01J r124 = ((C51112Sw) ((AbstractC51092Su) hilt_WebCodeDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) webCodeDialogFragment).A00 = (AnonymousClass182) r124.A94.get();
                ((WaDialogFragment) webCodeDialogFragment).A01 = (AnonymousClass180) r124.ALt.get();
                webCodeDialogFragment.A00 = (C15450nH) r124.AII.get();
                webCodeDialogFragment.A04 = (AnonymousClass17R) r124.AIz.get();
                webCodeDialogFragment.A02 = (C14840m8) r124.ACi.get();
                webCodeDialogFragment.A01 = (C245716a) r124.AJR.get();
            }
        } else if (this instanceof Hilt_ErrorDialogFragment) {
            Hilt_ErrorDialogFragment hilt_ErrorDialogFragment = (Hilt_ErrorDialogFragment) this;
            if (!hilt_ErrorDialogFragment.A02) {
                hilt_ErrorDialogFragment.A02 = true;
                AnonymousClass01J r125 = ((C51112Sw) ((AbstractC51092Su) hilt_ErrorDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ErrorDialogFragment).A00 = (AnonymousClass182) r125.A94.get();
                ((WaDialogFragment) hilt_ErrorDialogFragment).A01 = (AnonymousClass180) r125.ALt.get();
            }
        } else if (this instanceof Hilt_QrEducationDialogFragment) {
            Hilt_QrEducationDialogFragment hilt_QrEducationDialogFragment = (Hilt_QrEducationDialogFragment) this;
            if (!hilt_QrEducationDialogFragment.A02) {
                hilt_QrEducationDialogFragment.A02 = true;
                AnonymousClass01J r126 = ((C51112Sw) ((AbstractC51092Su) hilt_QrEducationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_QrEducationDialogFragment).A00 = (AnonymousClass182) r126.A94.get();
                ((WaDialogFragment) hilt_QrEducationDialogFragment).A01 = (AnonymousClass180) r126.ALt.get();
            }
        } else if (this instanceof Hilt_ResetPhoto_ConfirmDialogFragment) {
            Hilt_ResetPhoto_ConfirmDialogFragment hilt_ResetPhoto_ConfirmDialogFragment = (Hilt_ResetPhoto_ConfirmDialogFragment) this;
            if (!hilt_ResetPhoto_ConfirmDialogFragment.A02) {
                hilt_ResetPhoto_ConfirmDialogFragment.A02 = true;
                ResetPhoto.ConfirmDialogFragment confirmDialogFragment = (ResetPhoto.ConfirmDialogFragment) hilt_ResetPhoto_ConfirmDialogFragment;
                AnonymousClass01J r127 = ((C51112Sw) ((AbstractC51092Su) hilt_ResetPhoto_ConfirmDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) confirmDialogFragment).A00 = (AnonymousClass182) r127.A94.get();
                ((WaDialogFragment) confirmDialogFragment).A01 = (AnonymousClass180) r127.ALt.get();
                confirmDialogFragment.A00 = (AnonymousClass12U) r127.AJd.get();
            }
        } else if (this instanceof Hilt_ResetGroupPhoto_ConfirmDialogFragment) {
            Hilt_ResetGroupPhoto_ConfirmDialogFragment hilt_ResetGroupPhoto_ConfirmDialogFragment = (Hilt_ResetGroupPhoto_ConfirmDialogFragment) this;
            if (!hilt_ResetGroupPhoto_ConfirmDialogFragment.A02) {
                hilt_ResetGroupPhoto_ConfirmDialogFragment.A02 = true;
                AnonymousClass01J r128 = ((C51112Sw) ((AbstractC51092Su) hilt_ResetGroupPhoto_ConfirmDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ResetGroupPhoto_ConfirmDialogFragment).A00 = (AnonymousClass182) r128.A94.get();
                ((WaDialogFragment) hilt_ResetGroupPhoto_ConfirmDialogFragment).A01 = (AnonymousClass180) r128.ALt.get();
            }
        } else if (this instanceof Hilt_PickerSearchDialogFragment) {
            Hilt_PickerSearchDialogFragment hilt_PickerSearchDialogFragment = (Hilt_PickerSearchDialogFragment) this;
            if (hilt_PickerSearchDialogFragment instanceof Hilt_StickerSearchDialogFragment) {
                Hilt_StickerSearchDialogFragment hilt_StickerSearchDialogFragment = (Hilt_StickerSearchDialogFragment) hilt_PickerSearchDialogFragment;
                if (!hilt_StickerSearchDialogFragment.A02) {
                    hilt_StickerSearchDialogFragment.A02 = true;
                    StickerSearchDialogFragment stickerSearchDialogFragment = (StickerSearchDialogFragment) hilt_StickerSearchDialogFragment;
                    AnonymousClass01J r27 = ((C51112Sw) ((AbstractC51092Su) hilt_StickerSearchDialogFragment.generatedComponent())).A0Y;
                    ((WaDialogFragment) stickerSearchDialogFragment).A00 = (AnonymousClass182) r27.A94.get();
                    ((WaDialogFragment) stickerSearchDialogFragment).A01 = (AnonymousClass180) r27.ALt.get();
                    stickerSearchDialogFragment.A08 = (C14850m9) r27.A04.get();
                    stickerSearchDialogFragment.A09 = (C16120oU) r27.ANE.get();
                    stickerSearchDialogFragment.A0C = (C22210yi) r27.AGm.get();
                    stickerSearchDialogFragment.A06 = (AnonymousClass018) r27.ANb.get();
                    stickerSearchDialogFragment.A07 = (AnonymousClass193) r27.A6S.get();
                    stickerSearchDialogFragment.A0E = (AnonymousClass15H) r27.AKC.get();
                }
            } else if (hilt_PickerSearchDialogFragment instanceof Hilt_GifSearchDialogFragment) {
                Hilt_GifSearchDialogFragment hilt_GifSearchDialogFragment = (Hilt_GifSearchDialogFragment) hilt_PickerSearchDialogFragment;
                if (!hilt_GifSearchDialogFragment.A02) {
                    hilt_GifSearchDialogFragment.A02 = true;
                    GifSearchDialogFragment gifSearchDialogFragment = (GifSearchDialogFragment) hilt_GifSearchDialogFragment;
                    AnonymousClass01J r28 = ((C51112Sw) ((AbstractC51092Su) hilt_GifSearchDialogFragment.generatedComponent())).A0Y;
                    ((WaDialogFragment) gifSearchDialogFragment).A00 = (AnonymousClass182) r28.A94.get();
                    ((WaDialogFragment) gifSearchDialogFragment).A01 = (AnonymousClass180) r28.ALt.get();
                    gifSearchDialogFragment.A02 = (C14850m9) r28.A04.get();
                    gifSearchDialogFragment.A04 = (C253719d) r28.A8V.get();
                    gifSearchDialogFragment.A07 = (C252718t) r28.A9K.get();
                    gifSearchDialogFragment.A03 = (C16120oU) r28.ANE.get();
                    gifSearchDialogFragment.A00 = (AnonymousClass01d) r28.ALI.get();
                    gifSearchDialogFragment.A01 = (C14820m6) r28.AN3.get();
                    gifSearchDialogFragment.A06 = (C16630pM) r28.AIc.get();
                    gifSearchDialogFragment.A05 = (AbstractC253919f) r28.AGb.get();
                }
            } else if (!hilt_PickerSearchDialogFragment.A02) {
                hilt_PickerSearchDialogFragment.A02 = true;
                AnonymousClass01J r29 = ((C51112Sw) ((AbstractC51092Su) hilt_PickerSearchDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_PickerSearchDialogFragment).A00 = (AnonymousClass182) r29.A94.get();
                ((WaDialogFragment) hilt_PickerSearchDialogFragment).A01 = (AnonymousClass180) r29.ALt.get();
            }
        } else if (this instanceof Hilt_ConnectionProgressDialogFragment) {
            Hilt_ConnectionProgressDialogFragment hilt_ConnectionProgressDialogFragment = (Hilt_ConnectionProgressDialogFragment) this;
            if (!hilt_ConnectionProgressDialogFragment.A02) {
                hilt_ConnectionProgressDialogFragment.A02 = true;
                AnonymousClass01J r129 = ((C51112Sw) ((AbstractC51092Su) hilt_ConnectionProgressDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ConnectionProgressDialogFragment).A00 = (AnonymousClass182) r129.A94.get();
                ((WaDialogFragment) hilt_ConnectionProgressDialogFragment).A01 = (AnonymousClass180) r129.ALt.get();
            }
        } else if (this instanceof Hilt_RevokeNuxDialogFragment) {
            Hilt_RevokeNuxDialogFragment hilt_RevokeNuxDialogFragment = (Hilt_RevokeNuxDialogFragment) this;
            if (!hilt_RevokeNuxDialogFragment.A02) {
                hilt_RevokeNuxDialogFragment.A02 = true;
                RevokeNuxDialogFragment revokeNuxDialogFragment = (RevokeNuxDialogFragment) hilt_RevokeNuxDialogFragment;
                AnonymousClass01J r130 = ((C51112Sw) ((AbstractC51092Su) hilt_RevokeNuxDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) revokeNuxDialogFragment).A00 = (AnonymousClass182) r130.A94.get();
                ((WaDialogFragment) revokeNuxDialogFragment).A01 = (AnonymousClass180) r130.ALt.get();
                revokeNuxDialogFragment.A02 = (C14850m9) r130.A04.get();
                revokeNuxDialogFragment.A00 = (AnonymousClass12P) r130.A0H.get();
                revokeNuxDialogFragment.A03 = (C252018m) r130.A7g.get();
                revokeNuxDialogFragment.A01 = (C14820m6) r130.AN3.get();
            }
        } else if (this instanceof Hilt_DeleteMessagesDialogFragment) {
            Hilt_DeleteMessagesDialogFragment hilt_DeleteMessagesDialogFragment = (Hilt_DeleteMessagesDialogFragment) this;
            if (!hilt_DeleteMessagesDialogFragment.A02) {
                hilt_DeleteMessagesDialogFragment.A02 = true;
                DeleteMessagesDialogFragment deleteMessagesDialogFragment = (DeleteMessagesDialogFragment) hilt_DeleteMessagesDialogFragment;
                AnonymousClass01J r210 = ((C51112Sw) ((AbstractC51092Su) hilt_DeleteMessagesDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deleteMessagesDialogFragment).A00 = (AnonymousClass182) r210.A94.get();
                ((WaDialogFragment) deleteMessagesDialogFragment).A01 = (AnonymousClass180) r210.ALt.get();
                deleteMessagesDialogFragment.A06 = (C14830m7) r210.ALb.get();
                deleteMessagesDialogFragment.A0C = (C14850m9) r210.A04.get();
                deleteMessagesDialogFragment.A02 = (C14900mE) r210.A8X.get();
                deleteMessagesDialogFragment.A0G = (AbstractC14440lR) r210.ANe.get();
                deleteMessagesDialogFragment.A0B = (AnonymousClass19M) r210.A6R.get();
                deleteMessagesDialogFragment.A03 = (C16170oZ) r210.AM4.get();
                deleteMessagesDialogFragment.A04 = (C15550nR) r210.A45.get();
                deleteMessagesDialogFragment.A05 = (C15610nY) r210.AMe.get();
                deleteMessagesDialogFragment.A08 = (AnonymousClass018) r210.ANb.get();
                deleteMessagesDialogFragment.A0D = (C20710wC) r210.A8m.get();
                deleteMessagesDialogFragment.A09 = (C16370ot) r210.A2b.get();
                deleteMessagesDialogFragment.A0E = (AnonymousClass11G) r210.AKq.get();
                deleteMessagesDialogFragment.A07 = (C14820m6) r210.AN3.get();
                deleteMessagesDialogFragment.A0A = (C15600nX) r210.A8x.get();
                deleteMessagesDialogFragment.A0F = (AnonymousClass19J) r210.ACA.get();
            }
        } else if (this instanceof Hilt_RevokeInviteDialogFragment) {
            Hilt_RevokeInviteDialogFragment hilt_RevokeInviteDialogFragment = (Hilt_RevokeInviteDialogFragment) this;
            if (!hilt_RevokeInviteDialogFragment.A02) {
                hilt_RevokeInviteDialogFragment.A02 = true;
                RevokeInviteDialogFragment revokeInviteDialogFragment = (RevokeInviteDialogFragment) hilt_RevokeInviteDialogFragment;
                AnonymousClass01J r131 = ((C51112Sw) ((AbstractC51092Su) hilt_RevokeInviteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) revokeInviteDialogFragment).A00 = (AnonymousClass182) r131.A94.get();
                ((WaDialogFragment) revokeInviteDialogFragment).A01 = (AnonymousClass180) r131.ALt.get();
                revokeInviteDialogFragment.A00 = (C15550nR) r131.A45.get();
                revokeInviteDialogFragment.A01 = (C15610nY) r131.AMe.get();
            }
        } else if (this instanceof Hilt_PromptSendGroupInviteDialogFragment) {
            Hilt_PromptSendGroupInviteDialogFragment hilt_PromptSendGroupInviteDialogFragment = (Hilt_PromptSendGroupInviteDialogFragment) this;
            if (!hilt_PromptSendGroupInviteDialogFragment.A02) {
                hilt_PromptSendGroupInviteDialogFragment.A02 = true;
                PromptSendGroupInviteDialogFragment promptSendGroupInviteDialogFragment = (PromptSendGroupInviteDialogFragment) hilt_PromptSendGroupInviteDialogFragment;
                AnonymousClass01J r132 = ((C51112Sw) ((AbstractC51092Su) hilt_PromptSendGroupInviteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) promptSendGroupInviteDialogFragment).A00 = (AnonymousClass182) r132.A94.get();
                ((WaDialogFragment) promptSendGroupInviteDialogFragment).A01 = (AnonymousClass180) r132.ALt.get();
                promptSendGroupInviteDialogFragment.A00 = (C15610nY) r132.AMe.get();
                promptSendGroupInviteDialogFragment.A01 = (AnonymousClass018) r132.ANb.get();
                promptSendGroupInviteDialogFragment.A02 = (C20710wC) r132.A8m.get();
            }
        } else if (this instanceof Hilt_GoogleSearchDialogFragment) {
            Hilt_GoogleSearchDialogFragment hilt_GoogleSearchDialogFragment = (Hilt_GoogleSearchDialogFragment) this;
            if (!hilt_GoogleSearchDialogFragment.A02) {
                hilt_GoogleSearchDialogFragment.A02 = true;
                GoogleSearchDialogFragment googleSearchDialogFragment = (GoogleSearchDialogFragment) hilt_GoogleSearchDialogFragment;
                AnonymousClass01J r211 = ((C51112Sw) ((AbstractC51092Su) hilt_GoogleSearchDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) googleSearchDialogFragment).A00 = (AnonymousClass182) r211.A94.get();
                ((WaDialogFragment) googleSearchDialogFragment).A01 = (AnonymousClass180) r211.ALt.get();
                googleSearchDialogFragment.A01 = (C14900mE) r211.A8X.get();
                googleSearchDialogFragment.A05 = (AbstractC14440lR) r211.ANe.get();
                googleSearchDialogFragment.A03 = (C18790t3) r211.AJw.get();
                googleSearchDialogFragment.A04 = (C16120oU) r211.ANE.get();
                googleSearchDialogFragment.A02 = (C15450nH) r211.AII.get();
                googleSearchDialogFragment.A00 = (AnonymousClass12P) r211.A0H.get();
            }
        } else if (this instanceof Hilt_InviteLinkUnavailableDialogFragment) {
            Hilt_InviteLinkUnavailableDialogFragment hilt_InviteLinkUnavailableDialogFragment = (Hilt_InviteLinkUnavailableDialogFragment) this;
            if (!hilt_InviteLinkUnavailableDialogFragment.A02) {
                hilt_InviteLinkUnavailableDialogFragment.A02 = true;
                InviteLinkUnavailableDialogFragment inviteLinkUnavailableDialogFragment = (InviteLinkUnavailableDialogFragment) hilt_InviteLinkUnavailableDialogFragment;
                AnonymousClass01J r133 = ((C51112Sw) ((AbstractC51092Su) hilt_InviteLinkUnavailableDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) inviteLinkUnavailableDialogFragment).A00 = (AnonymousClass182) r133.A94.get();
                ((WaDialogFragment) inviteLinkUnavailableDialogFragment).A01 = (AnonymousClass180) r133.ALt.get();
                inviteLinkUnavailableDialogFragment.A00 = (C15220ml) r133.A4V.get();
            }
        } else if (this instanceof Hilt_CreateGroupSuspendDialog) {
            Hilt_CreateGroupSuspendDialog hilt_CreateGroupSuspendDialog = (Hilt_CreateGroupSuspendDialog) this;
            if (!hilt_CreateGroupSuspendDialog.A02) {
                hilt_CreateGroupSuspendDialog.A02 = true;
                CreateGroupSuspendDialog createGroupSuspendDialog = (CreateGroupSuspendDialog) hilt_CreateGroupSuspendDialog;
                AnonymousClass01J r134 = ((C51112Sw) ((AbstractC51092Su) hilt_CreateGroupSuspendDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) createGroupSuspendDialog).A00 = (AnonymousClass182) r134.A94.get();
                ((WaDialogFragment) createGroupSuspendDialog).A01 = (AnonymousClass180) r134.ALt.get();
                createGroupSuspendDialog.A03 = (C253118x) r134.AAW.get();
                createGroupSuspendDialog.A00 = (AnonymousClass19Y) r134.AI6.get();
                createGroupSuspendDialog.A02 = (AnonymousClass11G) r134.AKq.get();
                createGroupSuspendDialog.A01 = (C15220ml) r134.A4V.get();
            }
        } else if (this instanceof Hilt_ViewOnceNUXDialog) {
            Hilt_ViewOnceNUXDialog hilt_ViewOnceNUXDialog = (Hilt_ViewOnceNUXDialog) this;
            if (!hilt_ViewOnceNUXDialog.A02) {
                hilt_ViewOnceNUXDialog.A02 = true;
                ViewOnceNUXDialog viewOnceNUXDialog = (ViewOnceNUXDialog) hilt_ViewOnceNUXDialog;
                AnonymousClass01J r135 = ((C51112Sw) ((AbstractC51092Su) hilt_ViewOnceNUXDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) viewOnceNUXDialog).A00 = (AnonymousClass182) r135.A94.get();
                ((WaDialogFragment) viewOnceNUXDialog).A01 = (AnonymousClass180) r135.ALt.get();
                viewOnceNUXDialog.A02 = (AnonymousClass12P) r135.A0H.get();
                viewOnceNUXDialog.A04 = (C252018m) r135.A7g.get();
                viewOnceNUXDialog.A03 = (C14820m6) r135.AN3.get();
            }
        } else if (this instanceof Hilt_EphemeralNUXDialog) {
            Hilt_EphemeralNUXDialog hilt_EphemeralNUXDialog = (Hilt_EphemeralNUXDialog) this;
            if (!hilt_EphemeralNUXDialog.A02) {
                hilt_EphemeralNUXDialog.A02 = true;
                EphemeralNUXDialog ephemeralNUXDialog = (EphemeralNUXDialog) hilt_EphemeralNUXDialog;
                AnonymousClass01J r136 = ((C51112Sw) ((AbstractC51092Su) hilt_EphemeralNUXDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) ephemeralNUXDialog).A00 = (AnonymousClass182) r136.A94.get();
                ((WaDialogFragment) ephemeralNUXDialog).A01 = (AnonymousClass180) r136.ALt.get();
                ephemeralNUXDialog.A04 = (C14850m9) r136.A04.get();
                ephemeralNUXDialog.A02 = (AnonymousClass12P) r136.A0H.get();
                ephemeralNUXDialog.A05 = (C252018m) r136.A7g.get();
                ephemeralNUXDialog.A03 = (C14820m6) r136.AN3.get();
            }
        } else if (this instanceof Hilt_ChangeEphemeralSettingsDialog) {
            Hilt_ChangeEphemeralSettingsDialog hilt_ChangeEphemeralSettingsDialog = (Hilt_ChangeEphemeralSettingsDialog) this;
            if (!hilt_ChangeEphemeralSettingsDialog.A02) {
                hilt_ChangeEphemeralSettingsDialog.A02 = true;
                ChangeEphemeralSettingsDialog changeEphemeralSettingsDialog = (ChangeEphemeralSettingsDialog) hilt_ChangeEphemeralSettingsDialog;
                AnonymousClass01J r137 = ((C51112Sw) ((AbstractC51092Su) hilt_ChangeEphemeralSettingsDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) changeEphemeralSettingsDialog).A00 = (AnonymousClass182) r137.A94.get();
                ((WaDialogFragment) changeEphemeralSettingsDialog).A01 = (AnonymousClass180) r137.ALt.get();
                changeEphemeralSettingsDialog.A01 = (C14850m9) r137.A04.get();
                changeEphemeralSettingsDialog.A00 = (C17170qN) r137.AMt.get();
            }
        } else if (this instanceof Hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment) {
            Hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment = (Hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment) this;
            if (!hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment.A02) {
                hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment.A02 = true;
                DocumentPickerActivity.SendDocumentsConfirmationDialogFragment sendDocumentsConfirmationDialogFragment = (DocumentPickerActivity.SendDocumentsConfirmationDialogFragment) hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment;
                AnonymousClass01J r212 = ((C51112Sw) ((AbstractC51092Su) hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) sendDocumentsConfirmationDialogFragment).A00 = (AnonymousClass182) r212.A94.get();
                ((WaDialogFragment) sendDocumentsConfirmationDialogFragment).A01 = (AnonymousClass180) r212.ALt.get();
                sendDocumentsConfirmationDialogFragment.A05 = (AnonymousClass19M) r212.A6R.get();
                sendDocumentsConfirmationDialogFragment.A00 = (C15550nR) r212.A45.get();
                sendDocumentsConfirmationDialogFragment.A03 = (AnonymousClass01d) r212.ALI.get();
                sendDocumentsConfirmationDialogFragment.A01 = (C15610nY) r212.AMe.get();
                sendDocumentsConfirmationDialogFragment.A04 = (AnonymousClass018) r212.ANb.get();
                sendDocumentsConfirmationDialogFragment.A02 = (C18640sm) r212.A3u.get();
            }
        } else if (this instanceof Hilt_ProgressDialogFragment) {
            Hilt_ProgressDialogFragment hilt_ProgressDialogFragment = (Hilt_ProgressDialogFragment) this;
            if (!hilt_ProgressDialogFragment.A02) {
                hilt_ProgressDialogFragment.A02 = true;
                ProgressDialogFragment progressDialogFragment = (ProgressDialogFragment) hilt_ProgressDialogFragment;
                AnonymousClass01J r138 = ((C51112Sw) ((AbstractC51092Su) hilt_ProgressDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) progressDialogFragment).A00 = (AnonymousClass182) r138.A94.get();
                ((WaDialogFragment) progressDialogFragment).A01 = (AnonymousClass180) r138.ALt.get();
                progressDialogFragment.A01 = (AnonymousClass018) r138.ANb.get();
            }
        } else if (this instanceof Hilt_CreateOrAddToContactsDialog) {
            Hilt_CreateOrAddToContactsDialog hilt_CreateOrAddToContactsDialog = (Hilt_CreateOrAddToContactsDialog) this;
            if (!hilt_CreateOrAddToContactsDialog.A02) {
                hilt_CreateOrAddToContactsDialog.A02 = true;
                AnonymousClass01J r139 = ((C51112Sw) ((AbstractC51092Su) hilt_CreateOrAddToContactsDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_CreateOrAddToContactsDialog).A00 = (AnonymousClass182) r139.A94.get();
                ((WaDialogFragment) hilt_CreateOrAddToContactsDialog).A01 = (AnonymousClass180) r139.ALt.get();
            }
        } else if (this instanceof Hilt_LeaveGroupsDialogFragment) {
            Hilt_LeaveGroupsDialogFragment hilt_LeaveGroupsDialogFragment = (Hilt_LeaveGroupsDialogFragment) this;
            if (!hilt_LeaveGroupsDialogFragment.A02) {
                hilt_LeaveGroupsDialogFragment.A02 = true;
                LeaveGroupsDialogFragment leaveGroupsDialogFragment = (LeaveGroupsDialogFragment) hilt_LeaveGroupsDialogFragment;
                AnonymousClass01J r213 = ((C51112Sw) ((AbstractC51092Su) hilt_LeaveGroupsDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) leaveGroupsDialogFragment).A00 = (AnonymousClass182) r213.A94.get();
                ((WaDialogFragment) leaveGroupsDialogFragment).A01 = (AnonymousClass180) r213.ALt.get();
                leaveGroupsDialogFragment.A00 = (C14900mE) r213.A8X.get();
                leaveGroupsDialogFragment.A0G = (AbstractC14440lR) r213.ANe.get();
                leaveGroupsDialogFragment.A08 = (C19990v2) r213.A3M.get();
                leaveGroupsDialogFragment.A0H = (C14860mA) r213.ANU.get();
                leaveGroupsDialogFragment.A07 = (C20650w6) r213.A3A.get();
                leaveGroupsDialogFragment.A0C = (AnonymousClass19M) r213.A6R.get();
                leaveGroupsDialogFragment.A01 = (C15450nH) r213.AII.get();
                leaveGroupsDialogFragment.A0E = (C20660w7) r213.AIB.get();
                leaveGroupsDialogFragment.A02 = (C15550nR) r213.A45.get();
                leaveGroupsDialogFragment.A03 = (C15610nY) r213.AMe.get();
                leaveGroupsDialogFragment.A0D = (C20710wC) r213.A8m.get();
                leaveGroupsDialogFragment.A0F = (C15860o1) r213.A3H.get();
                leaveGroupsDialogFragment.A04 = (C254119h) r213.AJi.get();
                leaveGroupsDialogFragment.A06 = (C14820m6) r213.AN3.get();
                leaveGroupsDialogFragment.A09 = (C21320xE) r213.A4Y.get();
                leaveGroupsDialogFragment.A0A = (C15600nX) r213.A8x.get();
                leaveGroupsDialogFragment.A0B = (AnonymousClass11B) r213.AE2.get();
                leaveGroupsDialogFragment.A05 = (C18640sm) r213.A3u.get();
            }
        } else if (this instanceof Hilt_ConversationsFragment_DeleteGroupDialogFragment) {
            Hilt_ConversationsFragment_DeleteGroupDialogFragment hilt_ConversationsFragment_DeleteGroupDialogFragment = (Hilt_ConversationsFragment_DeleteGroupDialogFragment) this;
            if (!hilt_ConversationsFragment_DeleteGroupDialogFragment.A02) {
                hilt_ConversationsFragment_DeleteGroupDialogFragment.A02 = true;
                ConversationsFragment.DeleteGroupDialogFragment deleteGroupDialogFragment = (ConversationsFragment.DeleteGroupDialogFragment) hilt_ConversationsFragment_DeleteGroupDialogFragment;
                AnonymousClass01J r214 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationsFragment_DeleteGroupDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deleteGroupDialogFragment).A00 = (AnonymousClass182) r214.A94.get();
                ((WaDialogFragment) deleteGroupDialogFragment).A01 = (AnonymousClass180) r214.ALt.get();
                deleteGroupDialogFragment.A08 = (AbstractC14440lR) r214.ANe.get();
                deleteGroupDialogFragment.A06 = (AnonymousClass19M) r214.A6R.get();
                deleteGroupDialogFragment.A00 = (C16170oZ) r214.AM4.get();
                deleteGroupDialogFragment.A01 = (C15550nR) r214.A45.get();
                deleteGroupDialogFragment.A02 = (C15610nY) r214.AMe.get();
                deleteGroupDialogFragment.A05 = (AnonymousClass10Y) r214.AAR.get();
                deleteGroupDialogFragment.A03 = (C14820m6) r214.AN3.get();
                deleteGroupDialogFragment.A04 = (C21320xE) r214.A4Y.get();
                deleteGroupDialogFragment.A07 = (C255719x) r214.A5X.get();
            }
        } else if (this instanceof Hilt_ConversationsFragment_DeleteContactDialogFragment) {
            Hilt_ConversationsFragment_DeleteContactDialogFragment hilt_ConversationsFragment_DeleteContactDialogFragment = (Hilt_ConversationsFragment_DeleteContactDialogFragment) this;
            if (!hilt_ConversationsFragment_DeleteContactDialogFragment.A02) {
                hilt_ConversationsFragment_DeleteContactDialogFragment.A02 = true;
                ConversationsFragment.DeleteContactDialogFragment deleteContactDialogFragment = (ConversationsFragment.DeleteContactDialogFragment) hilt_ConversationsFragment_DeleteContactDialogFragment;
                AnonymousClass01J r215 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationsFragment_DeleteContactDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deleteContactDialogFragment).A00 = (AnonymousClass182) r215.A94.get();
                ((WaDialogFragment) deleteContactDialogFragment).A01 = (AnonymousClass180) r215.ALt.get();
                deleteContactDialogFragment.A07 = (AbstractC14440lR) r215.ANe.get();
                deleteContactDialogFragment.A05 = (AnonymousClass19M) r215.A6R.get();
                deleteContactDialogFragment.A00 = (C16170oZ) r215.AM4.get();
                deleteContactDialogFragment.A01 = (C15550nR) r215.A45.get();
                deleteContactDialogFragment.A02 = (C15610nY) r215.AMe.get();
                deleteContactDialogFragment.A03 = (C14820m6) r215.AN3.get();
                deleteContactDialogFragment.A04 = (C21320xE) r215.A4Y.get();
                deleteContactDialogFragment.A06 = (C255719x) r215.A5X.get();
            }
        } else if (this instanceof Hilt_ConversationsFragment_DeleteBroadcastListDialogFragment) {
            Hilt_ConversationsFragment_DeleteBroadcastListDialogFragment hilt_ConversationsFragment_DeleteBroadcastListDialogFragment = (Hilt_ConversationsFragment_DeleteBroadcastListDialogFragment) this;
            if (!hilt_ConversationsFragment_DeleteBroadcastListDialogFragment.A02) {
                hilt_ConversationsFragment_DeleteBroadcastListDialogFragment.A02 = true;
                ConversationsFragment.DeleteBroadcastListDialogFragment deleteBroadcastListDialogFragment = (ConversationsFragment.DeleteBroadcastListDialogFragment) hilt_ConversationsFragment_DeleteBroadcastListDialogFragment;
                AnonymousClass01J r216 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationsFragment_DeleteBroadcastListDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deleteBroadcastListDialogFragment).A00 = (AnonymousClass182) r216.A94.get();
                ((WaDialogFragment) deleteBroadcastListDialogFragment).A01 = (AnonymousClass180) r216.ALt.get();
                deleteBroadcastListDialogFragment.A07 = (AbstractC14440lR) r216.ANe.get();
                deleteBroadcastListDialogFragment.A05 = (AnonymousClass19M) r216.A6R.get();
                deleteBroadcastListDialogFragment.A00 = (C16170oZ) r216.AM4.get();
                deleteBroadcastListDialogFragment.A01 = (C15550nR) r216.A45.get();
                deleteBroadcastListDialogFragment.A02 = (C15610nY) r216.AMe.get();
                deleteBroadcastListDialogFragment.A03 = (C14820m6) r216.AN3.get();
                deleteBroadcastListDialogFragment.A04 = (C21320xE) r216.A4Y.get();
                deleteBroadcastListDialogFragment.A06 = (C255719x) r216.A5X.get();
            }
        } else if (this instanceof Hilt_ConversationsFragment_BulkDeleteConversationDialogFragment) {
            Hilt_ConversationsFragment_BulkDeleteConversationDialogFragment hilt_ConversationsFragment_BulkDeleteConversationDialogFragment = (Hilt_ConversationsFragment_BulkDeleteConversationDialogFragment) this;
            if (!hilt_ConversationsFragment_BulkDeleteConversationDialogFragment.A02) {
                hilt_ConversationsFragment_BulkDeleteConversationDialogFragment.A02 = true;
                ConversationsFragment.BulkDeleteConversationDialogFragment bulkDeleteConversationDialogFragment = (ConversationsFragment.BulkDeleteConversationDialogFragment) hilt_ConversationsFragment_BulkDeleteConversationDialogFragment;
                AnonymousClass01J r217 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationsFragment_BulkDeleteConversationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) bulkDeleteConversationDialogFragment).A00 = (AnonymousClass182) r217.A94.get();
                ((WaDialogFragment) bulkDeleteConversationDialogFragment).A01 = (AnonymousClass180) r217.ALt.get();
                bulkDeleteConversationDialogFragment.A07 = (AbstractC14440lR) r217.ANe.get();
                bulkDeleteConversationDialogFragment.A05 = (AnonymousClass19M) r217.A6R.get();
                bulkDeleteConversationDialogFragment.A00 = (C16170oZ) r217.AM4.get();
                bulkDeleteConversationDialogFragment.A01 = (C15550nR) r217.A45.get();
                bulkDeleteConversationDialogFragment.A03 = (AnonymousClass018) r217.ANb.get();
                bulkDeleteConversationDialogFragment.A02 = (C14820m6) r217.AN3.get();
                bulkDeleteConversationDialogFragment.A04 = (C21320xE) r217.A4Y.get();
                bulkDeleteConversationDialogFragment.A06 = (C255719x) r217.A5X.get();
            }
        } else if (this instanceof Hilt_StarredMessagesActivity_UnstarAllDialogFragment) {
            Hilt_StarredMessagesActivity_UnstarAllDialogFragment hilt_StarredMessagesActivity_UnstarAllDialogFragment = (Hilt_StarredMessagesActivity_UnstarAllDialogFragment) this;
            if (!hilt_StarredMessagesActivity_UnstarAllDialogFragment.A02) {
                hilt_StarredMessagesActivity_UnstarAllDialogFragment.A02 = true;
                StarredMessagesActivity.UnstarAllDialogFragment unstarAllDialogFragment = (StarredMessagesActivity.UnstarAllDialogFragment) hilt_StarredMessagesActivity_UnstarAllDialogFragment;
                AnonymousClass01J r140 = ((C51112Sw) ((AbstractC51092Su) hilt_StarredMessagesActivity_UnstarAllDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) unstarAllDialogFragment).A00 = (AnonymousClass182) r140.A94.get();
                ((WaDialogFragment) unstarAllDialogFragment).A01 = (AnonymousClass180) r140.ALt.get();
                unstarAllDialogFragment.A00 = (AnonymousClass018) r140.ANb.get();
            }
        } else if (this instanceof Hilt_VerifiedBusinessInfoDialogFragment) {
            Hilt_VerifiedBusinessInfoDialogFragment hilt_VerifiedBusinessInfoDialogFragment = (Hilt_VerifiedBusinessInfoDialogFragment) this;
            if (!hilt_VerifiedBusinessInfoDialogFragment.A02) {
                hilt_VerifiedBusinessInfoDialogFragment.A02 = true;
                VerifiedBusinessInfoDialogFragment verifiedBusinessInfoDialogFragment = (VerifiedBusinessInfoDialogFragment) hilt_VerifiedBusinessInfoDialogFragment;
                AnonymousClass01J r218 = ((C51112Sw) ((AbstractC51092Su) hilt_VerifiedBusinessInfoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) verifiedBusinessInfoDialogFragment).A00 = (AnonymousClass182) r218.A94.get();
                ((WaDialogFragment) verifiedBusinessInfoDialogFragment).A01 = (AnonymousClass180) r218.ALt.get();
                verifiedBusinessInfoDialogFragment.A03 = (C16120oU) r218.ANE.get();
                verifiedBusinessInfoDialogFragment.A02 = (AnonymousClass19M) r218.A6R.get();
                verifiedBusinessInfoDialogFragment.A00 = (AnonymousClass12P) r218.A0H.get();
                verifiedBusinessInfoDialogFragment.A04 = (C252018m) r218.A7g.get();
                verifiedBusinessInfoDialogFragment.A01 = (AnonymousClass018) r218.ANb.get();
            }
        } else if (this instanceof Hilt_EncryptionChangeDialogFragment) {
            Hilt_EncryptionChangeDialogFragment hilt_EncryptionChangeDialogFragment = (Hilt_EncryptionChangeDialogFragment) this;
            if (!hilt_EncryptionChangeDialogFragment.A02) {
                hilt_EncryptionChangeDialogFragment.A02 = true;
                EncryptionChangeDialogFragment encryptionChangeDialogFragment = (EncryptionChangeDialogFragment) hilt_EncryptionChangeDialogFragment;
                AnonymousClass01J r219 = ((C51112Sw) ((AbstractC51092Su) hilt_EncryptionChangeDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) encryptionChangeDialogFragment).A00 = (AnonymousClass182) r219.A94.get();
                ((WaDialogFragment) encryptionChangeDialogFragment).A01 = (AnonymousClass180) r219.ALt.get();
                encryptionChangeDialogFragment.A08 = (C14850m9) r219.A04.get();
                encryptionChangeDialogFragment.A09 = (C16120oU) r219.ANE.get();
                encryptionChangeDialogFragment.A07 = (AnonymousClass19M) r219.A6R.get();
                encryptionChangeDialogFragment.A00 = (AnonymousClass12P) r219.A0H.get();
                encryptionChangeDialogFragment.A01 = (C15450nH) r219.AII.get();
                encryptionChangeDialogFragment.A02 = (C15550nR) r219.A45.get();
                encryptionChangeDialogFragment.A0C = (C252018m) r219.A7g.get();
                encryptionChangeDialogFragment.A04 = (C15610nY) r219.AMe.get();
                encryptionChangeDialogFragment.A05 = (AnonymousClass018) r219.ANb.get();
                encryptionChangeDialogFragment.A0B = (AnonymousClass11G) r219.AKq.get();
                encryptionChangeDialogFragment.A03 = (C22700zV) r219.AMN.get();
                encryptionChangeDialogFragment.A06 = (C15600nX) r219.A8x.get();
            }
        } else if (this instanceof Hilt_SecurityNotificationDialogFragment) {
            Hilt_SecurityNotificationDialogFragment hilt_SecurityNotificationDialogFragment = (Hilt_SecurityNotificationDialogFragment) this;
            if (hilt_SecurityNotificationDialogFragment instanceof Hilt_IdentityChangeDialogFragment) {
                Hilt_IdentityChangeDialogFragment hilt_IdentityChangeDialogFragment = (Hilt_IdentityChangeDialogFragment) hilt_SecurityNotificationDialogFragment;
                if (!hilt_IdentityChangeDialogFragment.A02) {
                    hilt_IdentityChangeDialogFragment.A02 = true;
                    IdentityChangeDialogFragment identityChangeDialogFragment = (IdentityChangeDialogFragment) hilt_IdentityChangeDialogFragment;
                    AnonymousClass01J r220 = ((C51112Sw) ((AbstractC51092Su) hilt_IdentityChangeDialogFragment.generatedComponent())).A0Y;
                    ((WaDialogFragment) identityChangeDialogFragment).A00 = (AnonymousClass182) r220.A94.get();
                    ((WaDialogFragment) identityChangeDialogFragment).A01 = (AnonymousClass180) r220.ALt.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A01 = (C15570nT) r220.AAr.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A06 = (AnonymousClass19M) r220.A6R.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A00 = (AnonymousClass12P) r220.A0H.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A02 = (C15550nR) r220.A45.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A07 = (C252018m) r220.A7g.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A03 = (C15610nY) r220.AMe.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A05 = (AnonymousClass018) r220.ANb.get();
                    ((SecurityNotificationDialogFragment) identityChangeDialogFragment).A04 = (C14820m6) r220.AN3.get();
                    identityChangeDialogFragment.A02 = (AbstractC14440lR) r220.ANe.get();
                    identityChangeDialogFragment.A01 = (C14840m8) r220.ACi.get();
                    identityChangeDialogFragment.A00 = (C18770sz) r220.AM8.get();
                }
            } else if (hilt_SecurityNotificationDialogFragment instanceof Hilt_DeviceUpdateDialogFragment) {
                Hilt_DeviceUpdateDialogFragment hilt_DeviceUpdateDialogFragment = (Hilt_DeviceUpdateDialogFragment) hilt_SecurityNotificationDialogFragment;
                if (!hilt_DeviceUpdateDialogFragment.A02) {
                    hilt_DeviceUpdateDialogFragment.A02 = true;
                    DeviceUpdateDialogFragment deviceUpdateDialogFragment = (DeviceUpdateDialogFragment) hilt_DeviceUpdateDialogFragment;
                    AnonymousClass01J r221 = ((C51112Sw) ((AbstractC51092Su) hilt_DeviceUpdateDialogFragment.generatedComponent())).A0Y;
                    ((WaDialogFragment) deviceUpdateDialogFragment).A00 = (AnonymousClass182) r221.A94.get();
                    ((WaDialogFragment) deviceUpdateDialogFragment).A01 = (AnonymousClass180) r221.ALt.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A01 = (C15570nT) r221.AAr.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A06 = (AnonymousClass19M) r221.A6R.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A00 = (AnonymousClass12P) r221.A0H.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A02 = (C15550nR) r221.A45.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A07 = (C252018m) r221.A7g.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A03 = (C15610nY) r221.AMe.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A05 = (AnonymousClass018) r221.ANb.get();
                    ((SecurityNotificationDialogFragment) deviceUpdateDialogFragment).A04 = (C14820m6) r221.AN3.get();
                    deviceUpdateDialogFragment.A02 = (AbstractC14440lR) r221.ANe.get();
                    deviceUpdateDialogFragment.A01 = (C20660w7) r221.AIB.get();
                    deviceUpdateDialogFragment.A00 = (C245015t) r221.A5h.get();
                }
            } else if (!hilt_SecurityNotificationDialogFragment.A02) {
                hilt_SecurityNotificationDialogFragment.A02 = true;
                SecurityNotificationDialogFragment securityNotificationDialogFragment = (SecurityNotificationDialogFragment) hilt_SecurityNotificationDialogFragment;
                AnonymousClass01J r222 = ((C51112Sw) ((AbstractC51092Su) hilt_SecurityNotificationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) securityNotificationDialogFragment).A00 = (AnonymousClass182) r222.A94.get();
                ((WaDialogFragment) securityNotificationDialogFragment).A01 = (AnonymousClass180) r222.ALt.get();
                securityNotificationDialogFragment.A01 = (C15570nT) r222.AAr.get();
                securityNotificationDialogFragment.A06 = (AnonymousClass19M) r222.A6R.get();
                securityNotificationDialogFragment.A00 = (AnonymousClass12P) r222.A0H.get();
                securityNotificationDialogFragment.A02 = (C15550nR) r222.A45.get();
                securityNotificationDialogFragment.A07 = (C252018m) r222.A7g.get();
                securityNotificationDialogFragment.A03 = (C15610nY) r222.AMe.get();
                securityNotificationDialogFragment.A05 = (AnonymousClass018) r222.ANb.get();
                securityNotificationDialogFragment.A04 = (C14820m6) r222.AN3.get();
            }
        } else if (this instanceof Hilt_ConversationRow_ConversationRowDialogFragment) {
            Hilt_ConversationRow_ConversationRowDialogFragment hilt_ConversationRow_ConversationRowDialogFragment = (Hilt_ConversationRow_ConversationRowDialogFragment) this;
            if (!hilt_ConversationRow_ConversationRowDialogFragment.A02) {
                hilt_ConversationRow_ConversationRowDialogFragment.A02 = true;
                ConversationRow$ConversationRowDialogFragment conversationRow$ConversationRowDialogFragment = (ConversationRow$ConversationRowDialogFragment) hilt_ConversationRow_ConversationRowDialogFragment;
                AnonymousClass01J r141 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationRow_ConversationRowDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) conversationRow$ConversationRowDialogFragment).A00 = (AnonymousClass182) r141.A94.get();
                ((WaDialogFragment) conversationRow$ConversationRowDialogFragment).A01 = (AnonymousClass180) r141.ALt.get();
                conversationRow$ConversationRowDialogFragment.A00 = (C15570nT) r141.AAr.get();
                conversationRow$ConversationRowDialogFragment.A01 = (C15550nR) r141.A45.get();
                conversationRow$ConversationRowDialogFragment.A02 = (C15610nY) r141.AMe.get();
                conversationRow$ConversationRowDialogFragment.A03 = (AnonymousClass018) r141.ANb.get();
            }
        } else if (this instanceof Hilt_ConversationRowContact_MessageSharedContactDialogFragment) {
            Hilt_ConversationRowContact_MessageSharedContactDialogFragment hilt_ConversationRowContact_MessageSharedContactDialogFragment = (Hilt_ConversationRowContact_MessageSharedContactDialogFragment) this;
            if (!hilt_ConversationRowContact_MessageSharedContactDialogFragment.A02) {
                hilt_ConversationRowContact_MessageSharedContactDialogFragment.A02 = true;
                ConversationRowContact$MessageSharedContactDialogFragment conversationRowContact$MessageSharedContactDialogFragment = (ConversationRowContact$MessageSharedContactDialogFragment) hilt_ConversationRowContact_MessageSharedContactDialogFragment;
                AnonymousClass01J r142 = ((C51112Sw) ((AbstractC51092Su) hilt_ConversationRowContact_MessageSharedContactDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) conversationRowContact$MessageSharedContactDialogFragment).A00 = (AnonymousClass182) r142.A94.get();
                ((WaDialogFragment) conversationRowContact$MessageSharedContactDialogFragment).A01 = (AnonymousClass180) r142.ALt.get();
                conversationRowContact$MessageSharedContactDialogFragment.A01 = (AnonymousClass018) r142.ANb.get();
                conversationRowContact$MessageSharedContactDialogFragment.A00 = (AnonymousClass190) r142.AIZ.get();
            }
        } else if (this instanceof Hilt_ChatWithBusinessInDirectoryDialogFragment) {
            Hilt_ChatWithBusinessInDirectoryDialogFragment hilt_ChatWithBusinessInDirectoryDialogFragment = (Hilt_ChatWithBusinessInDirectoryDialogFragment) this;
            if (!hilt_ChatWithBusinessInDirectoryDialogFragment.A02) {
                hilt_ChatWithBusinessInDirectoryDialogFragment.A02 = true;
                ChatWithBusinessInDirectoryDialogFragment chatWithBusinessInDirectoryDialogFragment = (ChatWithBusinessInDirectoryDialogFragment) hilt_ChatWithBusinessInDirectoryDialogFragment;
                AnonymousClass01J r223 = ((C51112Sw) ((AbstractC51092Su) hilt_ChatWithBusinessInDirectoryDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) chatWithBusinessInDirectoryDialogFragment).A00 = (AnonymousClass182) r223.A94.get();
                ((WaDialogFragment) chatWithBusinessInDirectoryDialogFragment).A01 = (AnonymousClass180) r223.ALt.get();
                chatWithBusinessInDirectoryDialogFragment.A03 = (AnonymousClass17R) r223.AIz.get();
                chatWithBusinessInDirectoryDialogFragment.A00 = (AnonymousClass12P) r223.A0H.get();
                chatWithBusinessInDirectoryDialogFragment.A04 = (C252018m) r223.A7g.get();
                chatWithBusinessInDirectoryDialogFragment.A02 = (AnonymousClass11I) r223.A2F.get();
                chatWithBusinessInDirectoryDialogFragment.A01 = (C250918b) r223.A2B.get();
            }
        } else if (this instanceof Hilt_BusinessTransitionInfoDialogFragment) {
            Hilt_BusinessTransitionInfoDialogFragment hilt_BusinessTransitionInfoDialogFragment = (Hilt_BusinessTransitionInfoDialogFragment) this;
            if (!hilt_BusinessTransitionInfoDialogFragment.A02) {
                hilt_BusinessTransitionInfoDialogFragment.A02 = true;
                BusinessTransitionInfoDialogFragment businessTransitionInfoDialogFragment = (BusinessTransitionInfoDialogFragment) hilt_BusinessTransitionInfoDialogFragment;
                AnonymousClass01J r224 = ((C51112Sw) ((AbstractC51092Su) hilt_BusinessTransitionInfoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) businessTransitionInfoDialogFragment).A00 = (AnonymousClass182) r224.A94.get();
                ((WaDialogFragment) businessTransitionInfoDialogFragment).A01 = (AnonymousClass180) r224.ALt.get();
                businessTransitionInfoDialogFragment.A05 = (C16120oU) r224.ANE.get();
                businessTransitionInfoDialogFragment.A04 = (AnonymousClass19M) r224.A6R.get();
                businessTransitionInfoDialogFragment.A00 = (AnonymousClass12P) r224.A0H.get();
                businessTransitionInfoDialogFragment.A07 = (C252018m) r224.A7g.get();
                businessTransitionInfoDialogFragment.A02 = (AnonymousClass018) r224.ANb.get();
                businessTransitionInfoDialogFragment.A01 = (C15550nR) r224.A45.get();
                businessTransitionInfoDialogFragment.A03 = (C15600nX) r224.A8x.get();
            }
        } else if (this instanceof Hilt_ChatMediaVisibilityDialog) {
            Hilt_ChatMediaVisibilityDialog hilt_ChatMediaVisibilityDialog = (Hilt_ChatMediaVisibilityDialog) this;
            if (!hilt_ChatMediaVisibilityDialog.A02) {
                hilt_ChatMediaVisibilityDialog.A02 = true;
                ChatMediaVisibilityDialog chatMediaVisibilityDialog = (ChatMediaVisibilityDialog) hilt_ChatMediaVisibilityDialog;
                AnonymousClass01J r143 = ((C51112Sw) ((AbstractC51092Su) hilt_ChatMediaVisibilityDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) chatMediaVisibilityDialog).A00 = (AnonymousClass182) r143.A94.get();
                ((WaDialogFragment) chatMediaVisibilityDialog).A01 = (AnonymousClass180) r143.ALt.get();
                chatMediaVisibilityDialog.A04 = (C15860o1) r143.A3H.get();
            }
        } else if (this instanceof Hilt_ChatMediaEphemeralVisibilityDialog) {
            Hilt_ChatMediaEphemeralVisibilityDialog hilt_ChatMediaEphemeralVisibilityDialog = (Hilt_ChatMediaEphemeralVisibilityDialog) this;
            if (!hilt_ChatMediaEphemeralVisibilityDialog.A02) {
                hilt_ChatMediaEphemeralVisibilityDialog.A02 = true;
                AnonymousClass01J r144 = ((C51112Sw) ((AbstractC51092Su) hilt_ChatMediaEphemeralVisibilityDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ChatMediaEphemeralVisibilityDialog).A00 = (AnonymousClass182) r144.A94.get();
                ((WaDialogFragment) hilt_ChatMediaEphemeralVisibilityDialog).A01 = (AnonymousClass180) r144.ALt.get();
            }
        } else if (this instanceof Hilt_ChangeNumberNotificationDialogFragment) {
            Hilt_ChangeNumberNotificationDialogFragment hilt_ChangeNumberNotificationDialogFragment = (Hilt_ChangeNumberNotificationDialogFragment) this;
            if (!hilt_ChangeNumberNotificationDialogFragment.A02) {
                hilt_ChangeNumberNotificationDialogFragment.A02 = true;
                ChangeNumberNotificationDialogFragment changeNumberNotificationDialogFragment = (ChangeNumberNotificationDialogFragment) hilt_ChangeNumberNotificationDialogFragment;
                AnonymousClass01J r145 = ((C51112Sw) ((AbstractC51092Su) hilt_ChangeNumberNotificationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) changeNumberNotificationDialogFragment).A00 = (AnonymousClass182) r145.A94.get();
                ((WaDialogFragment) changeNumberNotificationDialogFragment).A01 = (AnonymousClass180) r145.ALt.get();
                changeNumberNotificationDialogFragment.A02 = (C14830m7) r145.ALb.get();
                changeNumberNotificationDialogFragment.A00 = (C15550nR) r145.A45.get();
                changeNumberNotificationDialogFragment.A03 = (AnonymousClass018) r145.ANb.get();
            }
        } else if (this instanceof Hilt_CapturePictureOrVideoDialogFragment) {
            Hilt_CapturePictureOrVideoDialogFragment hilt_CapturePictureOrVideoDialogFragment = (Hilt_CapturePictureOrVideoDialogFragment) this;
            if (!hilt_CapturePictureOrVideoDialogFragment.A02) {
                hilt_CapturePictureOrVideoDialogFragment.A02 = true;
                CapturePictureOrVideoDialogFragment capturePictureOrVideoDialogFragment = (CapturePictureOrVideoDialogFragment) hilt_CapturePictureOrVideoDialogFragment;
                AnonymousClass01J r146 = ((C51112Sw) ((AbstractC51092Su) hilt_CapturePictureOrVideoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) capturePictureOrVideoDialogFragment).A00 = (AnonymousClass182) r146.A94.get();
                ((WaDialogFragment) capturePictureOrVideoDialogFragment).A01 = (AnonymousClass180) r146.ALt.get();
                capturePictureOrVideoDialogFragment.A01 = (AnonymousClass018) r146.ANb.get();
            }
        } else if (this instanceof Hilt_InviteToGroupCallConfirmationFragment) {
            Hilt_InviteToGroupCallConfirmationFragment hilt_InviteToGroupCallConfirmationFragment = (Hilt_InviteToGroupCallConfirmationFragment) this;
            if (!hilt_InviteToGroupCallConfirmationFragment.A02) {
                hilt_InviteToGroupCallConfirmationFragment.A02 = true;
                InviteToGroupCallConfirmationFragment inviteToGroupCallConfirmationFragment = (InviteToGroupCallConfirmationFragment) hilt_InviteToGroupCallConfirmationFragment;
                AnonymousClass01J r147 = ((C51112Sw) ((AbstractC51092Su) hilt_InviteToGroupCallConfirmationFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) inviteToGroupCallConfirmationFragment).A00 = (AnonymousClass182) r147.A94.get();
                ((WaDialogFragment) inviteToGroupCallConfirmationFragment).A01 = (AnonymousClass180) r147.ALt.get();
                inviteToGroupCallConfirmationFragment.A00 = (C15450nH) r147.AII.get();
                inviteToGroupCallConfirmationFragment.A01 = (C15550nR) r147.A45.get();
                inviteToGroupCallConfirmationFragment.A02 = (C15610nY) r147.AMe.get();
            }
        } else if (this instanceof Hilt_BaseSharedPreviewDialogFragment) {
            Hilt_BaseSharedPreviewDialogFragment hilt_BaseSharedPreviewDialogFragment = (Hilt_BaseSharedPreviewDialogFragment) this;
            if (hilt_BaseSharedPreviewDialogFragment instanceof Hilt_SharedTextPreviewDialogFragment) {
                Hilt_SharedTextPreviewDialogFragment hilt_SharedTextPreviewDialogFragment = (Hilt_SharedTextPreviewDialogFragment) hilt_BaseSharedPreviewDialogFragment;
                if (!hilt_SharedTextPreviewDialogFragment.A02) {
                    hilt_SharedTextPreviewDialogFragment.A02 = true;
                    SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = (SharedTextPreviewDialogFragment) hilt_SharedTextPreviewDialogFragment;
                    AnonymousClass01J r225 = ((C51112Sw) ((AbstractC51092Su) hilt_SharedTextPreviewDialogFragment.generatedComponent())).A0Y;
                    ((WaDialogFragment) sharedTextPreviewDialogFragment).A00 = (AnonymousClass182) r225.A94.get();
                    ((WaDialogFragment) sharedTextPreviewDialogFragment).A01 = (AnonymousClass180) r225.ALt.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A08 = (C14900mE) r225.A8X.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A07 = (C14330lG) r225.A7B.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A09 = (C15550nR) r225.A45.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0F = (C22190yg) r225.AB6.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0C = (AnonymousClass01d) r225.ALI.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0A = (C15610nY) r225.AMe.get();
                    ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0D = (AnonymousClass018) r225.ANb.get();
                    sharedTextPreviewDialogFragment.A0D = (C14850m9) r225.A04.get();
                    sharedTextPreviewDialogFragment.A0G = (C252718t) r225.A9K.get();
                    sharedTextPreviewDialogFragment.A03 = (AbstractC15710nm) r225.A4o.get();
                    sharedTextPreviewDialogFragment.A0H = (AbstractC14440lR) r225.ANe.get();
                    sharedTextPreviewDialogFragment.A04 = (C18790t3) r225.AJw.get();
                    sharedTextPreviewDialogFragment.A09 = (AnonymousClass19M) r225.A6R.get();
                    sharedTextPreviewDialogFragment.A0B = (C231510o) r225.AHO.get();
                    sharedTextPreviewDialogFragment.A02 = (AnonymousClass12P) r225.A0H.get();
                    sharedTextPreviewDialogFragment.A0C = (AnonymousClass193) r225.A6S.get();
                    sharedTextPreviewDialogFragment.A08 = (C14820m6) r225.AN3.get();
                    sharedTextPreviewDialogFragment.A06 = (C22640zP) r225.A3Z.get();
                    sharedTextPreviewDialogFragment.A0F = (C16630pM) r225.AIc.get();
                }
            } else if (!hilt_BaseSharedPreviewDialogFragment.A02) {
                hilt_BaseSharedPreviewDialogFragment.A02 = true;
                BaseSharedPreviewDialogFragment baseSharedPreviewDialogFragment = (BaseSharedPreviewDialogFragment) hilt_BaseSharedPreviewDialogFragment;
                AnonymousClass01J r226 = ((C51112Sw) ((AbstractC51092Su) hilt_BaseSharedPreviewDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) baseSharedPreviewDialogFragment).A00 = (AnonymousClass182) r226.A94.get();
                ((WaDialogFragment) baseSharedPreviewDialogFragment).A01 = (AnonymousClass180) r226.ALt.get();
                baseSharedPreviewDialogFragment.A08 = (C14900mE) r226.A8X.get();
                baseSharedPreviewDialogFragment.A07 = (C14330lG) r226.A7B.get();
                baseSharedPreviewDialogFragment.A09 = (C15550nR) r226.A45.get();
                baseSharedPreviewDialogFragment.A0F = (C22190yg) r226.AB6.get();
                baseSharedPreviewDialogFragment.A0C = (AnonymousClass01d) r226.ALI.get();
                baseSharedPreviewDialogFragment.A0A = (C15610nY) r226.AMe.get();
                baseSharedPreviewDialogFragment.A0D = (AnonymousClass018) r226.ANb.get();
            }
        } else if (this instanceof Hilt_LinkedDevicesDetailDialogFragment) {
            Hilt_LinkedDevicesDetailDialogFragment hilt_LinkedDevicesDetailDialogFragment = (Hilt_LinkedDevicesDetailDialogFragment) this;
            if (!hilt_LinkedDevicesDetailDialogFragment.A02) {
                hilt_LinkedDevicesDetailDialogFragment.A02 = true;
                LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment = (LinkedDevicesDetailDialogFragment) hilt_LinkedDevicesDetailDialogFragment;
                AnonymousClass01J r227 = ((C51112Sw) ((AbstractC51092Su) hilt_LinkedDevicesDetailDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) linkedDevicesDetailDialogFragment).A00 = (AnonymousClass182) r227.A94.get();
                ((WaDialogFragment) linkedDevicesDetailDialogFragment).A01 = (AnonymousClass180) r227.ALt.get();
                linkedDevicesDetailDialogFragment.A06 = (C14830m7) r227.ALb.get();
                linkedDevicesDetailDialogFragment.A03 = (C14900mE) r227.A8X.get();
                linkedDevicesDetailDialogFragment.A0D = (C14860mA) r227.ANU.get();
                linkedDevicesDetailDialogFragment.A02 = (AnonymousClass12P) r227.A0H.get();
                linkedDevicesDetailDialogFragment.A0C = (C252018m) r227.A7g.get();
                linkedDevicesDetailDialogFragment.A05 = (AnonymousClass01d) r227.ALI.get();
                linkedDevicesDetailDialogFragment.A07 = (AnonymousClass018) r227.ANb.get();
                linkedDevicesDetailDialogFragment.A0A = (C238813j) r227.ABy.get();
                linkedDevicesDetailDialogFragment.A09 = (C22100yW) r227.A3g.get();
            }
        } else if (this instanceof Hilt_DeactivateCommunityConfirmationFragment) {
            Hilt_DeactivateCommunityConfirmationFragment hilt_DeactivateCommunityConfirmationFragment = (Hilt_DeactivateCommunityConfirmationFragment) this;
            if (!hilt_DeactivateCommunityConfirmationFragment.A02) {
                hilt_DeactivateCommunityConfirmationFragment.A02 = true;
                DeactivateCommunityConfirmationFragment deactivateCommunityConfirmationFragment = (DeactivateCommunityConfirmationFragment) hilt_DeactivateCommunityConfirmationFragment;
                AnonymousClass01J r148 = ((C51112Sw) ((AbstractC51092Su) hilt_DeactivateCommunityConfirmationFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) deactivateCommunityConfirmationFragment).A00 = (AnonymousClass182) r148.A94.get();
                ((WaDialogFragment) deactivateCommunityConfirmationFragment).A01 = (AnonymousClass180) r148.ALt.get();
                deactivateCommunityConfirmationFragment.A01 = (C15550nR) r148.A45.get();
                deactivateCommunityConfirmationFragment.A02 = (C15610nY) r148.AMe.get();
            }
        } else if (this instanceof Hilt_CommunitySpamReportDialogFragment) {
            Hilt_CommunitySpamReportDialogFragment hilt_CommunitySpamReportDialogFragment = (Hilt_CommunitySpamReportDialogFragment) this;
            if (!hilt_CommunitySpamReportDialogFragment.A02) {
                hilt_CommunitySpamReportDialogFragment.A02 = true;
                CommunitySpamReportDialogFragment communitySpamReportDialogFragment = (CommunitySpamReportDialogFragment) hilt_CommunitySpamReportDialogFragment;
                AnonymousClass01J r149 = ((C51112Sw) ((AbstractC51092Su) hilt_CommunitySpamReportDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) communitySpamReportDialogFragment).A00 = (AnonymousClass182) r149.A94.get();
                ((WaDialogFragment) communitySpamReportDialogFragment).A01 = (AnonymousClass180) r149.ALt.get();
                communitySpamReportDialogFragment.A02 = (C254119h) r149.AJi.get();
                communitySpamReportDialogFragment.A00 = (C14900mE) r149.A8X.get();
                communitySpamReportDialogFragment.A03 = (AbstractC14440lR) r149.ANe.get();
                communitySpamReportDialogFragment.A01 = (C15550nR) r149.A45.get();
            }
        } else if (this instanceof Hilt_CommunityDeleteDialogFragment) {
            Hilt_CommunityDeleteDialogFragment hilt_CommunityDeleteDialogFragment = (Hilt_CommunityDeleteDialogFragment) this;
            if (!hilt_CommunityDeleteDialogFragment.A02) {
                hilt_CommunityDeleteDialogFragment.A02 = true;
                CommunityDeleteDialogFragment communityDeleteDialogFragment = (CommunityDeleteDialogFragment) hilt_CommunityDeleteDialogFragment;
                AnonymousClass01J r228 = ((C51112Sw) ((AbstractC51092Su) hilt_CommunityDeleteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) communityDeleteDialogFragment).A00 = (AnonymousClass182) r228.A94.get();
                ((WaDialogFragment) communityDeleteDialogFragment).A01 = (AnonymousClass180) r228.ALt.get();
                communityDeleteDialogFragment.A04 = (C14830m7) r228.ALb.get();
                communityDeleteDialogFragment.A05 = (AbstractC14440lR) r228.ANe.get();
                communityDeleteDialogFragment.A00 = (C15450nH) r228.AII.get();
                communityDeleteDialogFragment.A01 = (C16170oZ) r228.AM4.get();
                communityDeleteDialogFragment.A03 = (C15610nY) r228.AMe.get();
                communityDeleteDialogFragment.A02 = (C15550nR) r228.A45.get();
            }
        } else if (this instanceof Hilt_ViewPhotoOrStatusDialogFragment) {
            Hilt_ViewPhotoOrStatusDialogFragment hilt_ViewPhotoOrStatusDialogFragment = (Hilt_ViewPhotoOrStatusDialogFragment) this;
            if (!hilt_ViewPhotoOrStatusDialogFragment.A02) {
                hilt_ViewPhotoOrStatusDialogFragment.A02 = true;
                AnonymousClass01J r150 = ((C51112Sw) ((AbstractC51092Su) hilt_ViewPhotoOrStatusDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ViewPhotoOrStatusDialogFragment).A00 = (AnonymousClass182) r150.A94.get();
                ((WaDialogFragment) hilt_ViewPhotoOrStatusDialogFragment).A01 = (AnonymousClass180) r150.ALt.get();
            }
        } else if (this instanceof Hilt_ChatInfoActivity_EncryptionExplanationDialogFragment) {
            Hilt_ChatInfoActivity_EncryptionExplanationDialogFragment hilt_ChatInfoActivity_EncryptionExplanationDialogFragment = (Hilt_ChatInfoActivity_EncryptionExplanationDialogFragment) this;
            if (!hilt_ChatInfoActivity_EncryptionExplanationDialogFragment.A02) {
                hilt_ChatInfoActivity_EncryptionExplanationDialogFragment.A02 = true;
                ChatInfoActivity$EncryptionExplanationDialogFragment chatInfoActivity$EncryptionExplanationDialogFragment = (ChatInfoActivity$EncryptionExplanationDialogFragment) hilt_ChatInfoActivity_EncryptionExplanationDialogFragment;
                AnonymousClass01J r229 = ((C51112Sw) ((AbstractC51092Su) hilt_ChatInfoActivity_EncryptionExplanationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) chatInfoActivity$EncryptionExplanationDialogFragment).A00 = (AnonymousClass182) r229.A94.get();
                ((WaDialogFragment) chatInfoActivity$EncryptionExplanationDialogFragment).A01 = (AnonymousClass180) r229.ALt.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A05 = (C14850m9) r229.A04.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A06 = (C16120oU) r229.ANE.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A04 = (AnonymousClass19M) r229.A6R.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A00 = (AnonymousClass12P) r229.A0H.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A01 = (C15550nR) r229.A45.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A08 = (C252018m) r229.A7g.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A02 = (AnonymousClass018) r229.ANb.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A07 = (AnonymousClass11G) r229.AKq.get();
                chatInfoActivity$EncryptionExplanationDialogFragment.A03 = (C15600nX) r229.A8x.get();
            }
        } else if (this instanceof Hilt_VoipContactPickerDialogFragment) {
            Hilt_VoipContactPickerDialogFragment hilt_VoipContactPickerDialogFragment = (Hilt_VoipContactPickerDialogFragment) this;
            if (!hilt_VoipContactPickerDialogFragment.A02) {
                hilt_VoipContactPickerDialogFragment.A02 = true;
                AnonymousClass01J r151 = ((C51112Sw) ((AbstractC51092Su) hilt_VoipContactPickerDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_VoipContactPickerDialogFragment).A00 = (AnonymousClass182) r151.A94.get();
                ((WaDialogFragment) hilt_VoipContactPickerDialogFragment).A01 = (AnonymousClass180) r151.ALt.get();
            }
        } else if (this instanceof Hilt_JoinableEducationDialogFragment) {
            Hilt_JoinableEducationDialogFragment hilt_JoinableEducationDialogFragment = (Hilt_JoinableEducationDialogFragment) this;
            if (!hilt_JoinableEducationDialogFragment.A02) {
                hilt_JoinableEducationDialogFragment.A02 = true;
                JoinableEducationDialogFragment joinableEducationDialogFragment = (JoinableEducationDialogFragment) hilt_JoinableEducationDialogFragment;
                AnonymousClass01J r152 = ((C51112Sw) ((AbstractC51092Su) hilt_JoinableEducationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) joinableEducationDialogFragment).A00 = (AnonymousClass182) r152.A94.get();
                ((WaDialogFragment) joinableEducationDialogFragment).A01 = (AnonymousClass180) r152.ALt.get();
                joinableEducationDialogFragment.A00 = (AnonymousClass018) r152.ANb.get();
            }
        } else if (this instanceof Hilt_MaximizedParticipantVideoDialogFragment) {
            Hilt_MaximizedParticipantVideoDialogFragment hilt_MaximizedParticipantVideoDialogFragment = (Hilt_MaximizedParticipantVideoDialogFragment) this;
            if (!hilt_MaximizedParticipantVideoDialogFragment.A02) {
                hilt_MaximizedParticipantVideoDialogFragment.A02 = true;
                MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = (MaximizedParticipantVideoDialogFragment) hilt_MaximizedParticipantVideoDialogFragment;
                AnonymousClass01J r153 = ((C51112Sw) ((AbstractC51092Su) hilt_MaximizedParticipantVideoDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) maximizedParticipantVideoDialogFragment).A00 = (AnonymousClass182) r153.A94.get();
                ((WaDialogFragment) maximizedParticipantVideoDialogFragment).A01 = (AnonymousClass180) r153.ALt.get();
                maximizedParticipantVideoDialogFragment.A0A = (C15550nR) r153.A45.get();
                maximizedParticipantVideoDialogFragment.A09 = (AnonymousClass1CP) r153.AMO.get();
                maximizedParticipantVideoDialogFragment.A0B = (C15610nY) r153.AMe.get();
                maximizedParticipantVideoDialogFragment.A0C = (AnonymousClass018) r153.ANb.get();
            }
        } else if (this instanceof Hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment) {
            Hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment = (Hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment) this;
            if (!hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment.A02) {
                hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment.A02 = true;
                CallSpamActivity.ReportSpamOrBlockDialogFragment reportSpamOrBlockDialogFragment = (CallSpamActivity.ReportSpamOrBlockDialogFragment) hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment;
                AnonymousClass01J r230 = ((C51112Sw) ((AbstractC51092Su) hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) reportSpamOrBlockDialogFragment).A00 = (AnonymousClass182) r230.A94.get();
                ((WaDialogFragment) reportSpamOrBlockDialogFragment).A01 = (AnonymousClass180) r230.ALt.get();
                reportSpamOrBlockDialogFragment.A02 = (C14900mE) r230.A8X.get();
                reportSpamOrBlockDialogFragment.A0F = (AbstractC14440lR) r230.ANe.get();
                reportSpamOrBlockDialogFragment.A03 = (C16170oZ) r230.AM4.get();
                reportSpamOrBlockDialogFragment.A05 = (C15550nR) r230.A45.get();
                reportSpamOrBlockDialogFragment.A06 = (C15610nY) r230.AMe.get();
                reportSpamOrBlockDialogFragment.A0D = (C22230yk) r230.ANT.get();
                reportSpamOrBlockDialogFragment.A04 = (C238013b) r230.A1Z.get();
                reportSpamOrBlockDialogFragment.A09 = (C15650ng) r230.A4m.get();
                reportSpamOrBlockDialogFragment.A0E = (C20220vP) r230.AC3.get();
                reportSpamOrBlockDialogFragment.A07 = (C250417w) r230.A4b.get();
                reportSpamOrBlockDialogFragment.A08 = (C18640sm) r230.A3u.get();
            }
        } else if (this instanceof Hilt_CallsHistoryFragment_ClearCallLogDialogFragment) {
            Hilt_CallsHistoryFragment_ClearCallLogDialogFragment hilt_CallsHistoryFragment_ClearCallLogDialogFragment = (Hilt_CallsHistoryFragment_ClearCallLogDialogFragment) this;
            if (!hilt_CallsHistoryFragment_ClearCallLogDialogFragment.A02) {
                hilt_CallsHistoryFragment_ClearCallLogDialogFragment.A02 = true;
                CallsHistoryFragment.ClearCallLogDialogFragment clearCallLogDialogFragment = (CallsHistoryFragment.ClearCallLogDialogFragment) hilt_CallsHistoryFragment_ClearCallLogDialogFragment;
                AnonymousClass01J r231 = ((C51112Sw) ((AbstractC51092Su) hilt_CallsHistoryFragment_ClearCallLogDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) clearCallLogDialogFragment).A00 = (AnonymousClass182) r231.A94.get();
                ((WaDialogFragment) clearCallLogDialogFragment).A01 = (AnonymousClass180) r231.ALt.get();
                clearCallLogDialogFragment.A00 = (C14900mE) r231.A8X.get();
                clearCallLogDialogFragment.A04 = (AbstractC14440lR) r231.ANe.get();
                clearCallLogDialogFragment.A01 = (AnonymousClass018) r231.ANb.get();
                clearCallLogDialogFragment.A05 = (AnonymousClass12G) r231.A2h.get();
                clearCallLogDialogFragment.A02 = (C18750sx) r231.A2p.get();
                clearCallLogDialogFragment.A03 = (C20230vQ) r231.ACe.get();
            }
        } else if (this instanceof Hilt_ClearLocationDialogFragment) {
            Hilt_ClearLocationDialogFragment hilt_ClearLocationDialogFragment = (Hilt_ClearLocationDialogFragment) this;
            if (!hilt_ClearLocationDialogFragment.A02) {
                hilt_ClearLocationDialogFragment.A02 = true;
                ClearLocationDialogFragment clearLocationDialogFragment = (ClearLocationDialogFragment) hilt_ClearLocationDialogFragment;
                AnonymousClass01J r154 = ((C51112Sw) ((AbstractC51092Su) hilt_ClearLocationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) clearLocationDialogFragment).A00 = (AnonymousClass182) r154.A94.get();
                ((WaDialogFragment) clearLocationDialogFragment).A01 = (AnonymousClass180) r154.ALt.get();
                clearLocationDialogFragment.A00 = (AnonymousClass1B9) r154.A1P.get();
            }
        } else if (this instanceof Hilt_BlockConfirmationDialogFragment) {
            Hilt_BlockConfirmationDialogFragment hilt_BlockConfirmationDialogFragment = (Hilt_BlockConfirmationDialogFragment) this;
            if (!hilt_BlockConfirmationDialogFragment.A02) {
                hilt_BlockConfirmationDialogFragment.A02 = true;
                BlockConfirmationDialogFragment blockConfirmationDialogFragment = (BlockConfirmationDialogFragment) hilt_BlockConfirmationDialogFragment;
                AnonymousClass01J r232 = ((C51112Sw) ((AbstractC51092Su) hilt_BlockConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) blockConfirmationDialogFragment).A00 = (AnonymousClass182) r232.A94.get();
                ((WaDialogFragment) blockConfirmationDialogFragment).A01 = (AnonymousClass180) r232.ALt.get();
                blockConfirmationDialogFragment.A00 = (C14900mE) r232.A8X.get();
                blockConfirmationDialogFragment.A09 = (AbstractC14440lR) r232.ANe.get();
                blockConfirmationDialogFragment.A08 = (C16120oU) r232.ANE.get();
                blockConfirmationDialogFragment.A01 = (C15450nH) r232.AII.get();
                blockConfirmationDialogFragment.A02 = (C16170oZ) r232.AM4.get();
                blockConfirmationDialogFragment.A05 = (C15550nR) r232.A45.get();
                blockConfirmationDialogFragment.A06 = (C15610nY) r232.AMe.get();
                blockConfirmationDialogFragment.A04 = (C238013b) r232.A1Z.get();
                blockConfirmationDialogFragment.A07 = (C254119h) r232.AJi.get();
            }
        } else if (this instanceof Hilt_BlockReasonListFragment) {
            Hilt_BlockReasonListFragment hilt_BlockReasonListFragment = (Hilt_BlockReasonListFragment) this;
            if (!hilt_BlockReasonListFragment.A02) {
                hilt_BlockReasonListFragment.A02 = true;
                BlockReasonListFragment blockReasonListFragment = (BlockReasonListFragment) hilt_BlockReasonListFragment;
                AnonymousClass01J r233 = ((C51112Sw) ((AbstractC51092Su) hilt_BlockReasonListFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) blockReasonListFragment).A00 = (AnonymousClass182) r233.A94.get();
                ((WaDialogFragment) blockReasonListFragment).A01 = (AnonymousClass180) r233.ALt.get();
                blockReasonListFragment.A09 = (C14850m9) r233.A04.get();
                blockReasonListFragment.A08 = (AnonymousClass19M) r233.A6R.get();
                blockReasonListFragment.A04 = (C15550nR) r233.A45.get();
                blockReasonListFragment.A06 = (AnonymousClass01d) r233.ALI.get();
                blockReasonListFragment.A05 = (C15610nY) r233.AMe.get();
                blockReasonListFragment.A07 = (AnonymousClass018) r233.ANb.get();
                blockReasonListFragment.A0A = (C16630pM) r233.AIc.get();
            }
        } else if (this instanceof Hilt_ReportProductDialogFragment) {
            Hilt_ReportProductDialogFragment hilt_ReportProductDialogFragment = (Hilt_ReportProductDialogFragment) this;
            if (!hilt_ReportProductDialogFragment.A02) {
                hilt_ReportProductDialogFragment.A02 = true;
                AnonymousClass01J r155 = ((C51112Sw) ((AbstractC51092Su) hilt_ReportProductDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_ReportProductDialogFragment).A00 = (AnonymousClass182) r155.A94.get();
                ((WaDialogFragment) hilt_ReportProductDialogFragment).A01 = (AnonymousClass180) r155.ALt.get();
            }
        } else if (this instanceof Hilt_ProductReportReasonDialogFragment) {
            Hilt_ProductReportReasonDialogFragment hilt_ProductReportReasonDialogFragment = (Hilt_ProductReportReasonDialogFragment) this;
            if (!hilt_ProductReportReasonDialogFragment.A02) {
                hilt_ProductReportReasonDialogFragment.A02 = true;
                ProductReportReasonDialogFragment productReportReasonDialogFragment = (ProductReportReasonDialogFragment) hilt_ProductReportReasonDialogFragment;
                AnonymousClass01J r156 = ((C51112Sw) ((AbstractC51092Su) hilt_ProductReportReasonDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) productReportReasonDialogFragment).A00 = (AnonymousClass182) r156.A94.get();
                ((WaDialogFragment) productReportReasonDialogFragment).A01 = (AnonymousClass180) r156.ALt.get();
                productReportReasonDialogFragment.A01 = (C14900mE) r156.A8X.get();
            }
        } else if (this instanceof Hilt_SingleChoiceListDialogFragment) {
            Hilt_SingleChoiceListDialogFragment hilt_SingleChoiceListDialogFragment = (Hilt_SingleChoiceListDialogFragment) this;
            if (!hilt_SingleChoiceListDialogFragment.A02) {
                hilt_SingleChoiceListDialogFragment.A02 = true;
                SingleChoiceListDialogFragment singleChoiceListDialogFragment = (SingleChoiceListDialogFragment) hilt_SingleChoiceListDialogFragment;
                AnonymousClass01J r157 = ((C51112Sw) ((AbstractC51092Su) hilt_SingleChoiceListDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) singleChoiceListDialogFragment).A00 = (AnonymousClass182) r157.A94.get();
                ((WaDialogFragment) singleChoiceListDialogFragment).A01 = (AnonymousClass180) r157.ALt.get();
                singleChoiceListDialogFragment.A00 = (C14900mE) r157.A8X.get();
            }
        } else if (this instanceof Hilt_SettingsGoogleDrive_AuthRequestDialogFragment) {
            Hilt_SettingsGoogleDrive_AuthRequestDialogFragment hilt_SettingsGoogleDrive_AuthRequestDialogFragment = (Hilt_SettingsGoogleDrive_AuthRequestDialogFragment) this;
            if (!hilt_SettingsGoogleDrive_AuthRequestDialogFragment.A02) {
                hilt_SettingsGoogleDrive_AuthRequestDialogFragment.A02 = true;
                AnonymousClass01J r158 = ((C51112Sw) ((AbstractC51092Su) hilt_SettingsGoogleDrive_AuthRequestDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_SettingsGoogleDrive_AuthRequestDialogFragment).A00 = (AnonymousClass182) r158.A94.get();
                ((WaDialogFragment) hilt_SettingsGoogleDrive_AuthRequestDialogFragment).A01 = (AnonymousClass180) r158.ALt.get();
            }
        } else if (this instanceof Hilt_PromptDialogFragment) {
            Hilt_PromptDialogFragment hilt_PromptDialogFragment = (Hilt_PromptDialogFragment) this;
            if (!hilt_PromptDialogFragment.A02) {
                hilt_PromptDialogFragment.A02 = true;
                PromptDialogFragment promptDialogFragment = (PromptDialogFragment) hilt_PromptDialogFragment;
                AnonymousClass01J r159 = ((C51112Sw) ((AbstractC51092Su) hilt_PromptDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) promptDialogFragment).A00 = (AnonymousClass182) r159.A94.get();
                ((WaDialogFragment) promptDialogFragment).A01 = (AnonymousClass180) r159.ALt.get();
                promptDialogFragment.A01 = (AnonymousClass01d) r159.ALI.get();
            }
        } else if (this instanceof Hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment) {
            Hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment = (Hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment) this;
            if (!hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment.A02) {
                hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment.A02 = true;
                AnonymousClass01J r160 = ((C51112Sw) ((AbstractC51092Su) hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment).A00 = (AnonymousClass182) r160.A94.get();
                ((WaDialogFragment) hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment).A01 = (AnonymousClass180) r160.ALt.get();
            }
        } else if (this instanceof Hilt_VerifyTwoFactorAuthCodeDialogFragment) {
            Hilt_VerifyTwoFactorAuthCodeDialogFragment hilt_VerifyTwoFactorAuthCodeDialogFragment = (Hilt_VerifyTwoFactorAuthCodeDialogFragment) this;
            if (!hilt_VerifyTwoFactorAuthCodeDialogFragment.A02) {
                hilt_VerifyTwoFactorAuthCodeDialogFragment.A02 = true;
                VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment = (VerifyTwoFactorAuthCodeDialogFragment) hilt_VerifyTwoFactorAuthCodeDialogFragment;
                AnonymousClass01J r234 = ((C51112Sw) ((AbstractC51092Su) hilt_VerifyTwoFactorAuthCodeDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) verifyTwoFactorAuthCodeDialogFragment).A00 = (AnonymousClass182) r234.A94.get();
                ((WaDialogFragment) verifyTwoFactorAuthCodeDialogFragment).A01 = (AnonymousClass180) r234.ALt.get();
                verifyTwoFactorAuthCodeDialogFragment.A04 = (C14900mE) r234.A8X.get();
                verifyTwoFactorAuthCodeDialogFragment.A08 = (AbstractC14440lR) r234.ANe.get();
                verifyTwoFactorAuthCodeDialogFragment.A05 = (AnonymousClass01d) r234.ALI.get();
                verifyTwoFactorAuthCodeDialogFragment.A06 = (AnonymousClass018) r234.ANb.get();
                verifyTwoFactorAuthCodeDialogFragment.A07 = (C14920mG) r234.ALr.get();
            }
        } else if (this instanceof Hilt_SetupDeviceAuthDialog) {
            Hilt_SetupDeviceAuthDialog hilt_SetupDeviceAuthDialog = (Hilt_SetupDeviceAuthDialog) this;
            if (!hilt_SetupDeviceAuthDialog.A02) {
                hilt_SetupDeviceAuthDialog.A02 = true;
                SetupDeviceAuthDialog setupDeviceAuthDialog = (SetupDeviceAuthDialog) hilt_SetupDeviceAuthDialog;
                AnonymousClass01J r161 = ((C51112Sw) ((AbstractC51092Su) hilt_SetupDeviceAuthDialog.generatedComponent())).A0Y;
                ((WaDialogFragment) setupDeviceAuthDialog).A00 = (AnonymousClass182) r161.A94.get();
                ((WaDialogFragment) setupDeviceAuthDialog).A01 = (AnonymousClass180) r161.ALt.get();
                setupDeviceAuthDialog.A00 = (C22670zS) r161.A0V.get();
            }
        } else if (this instanceof Hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment) {
            Hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment = (Hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment) this;
            if (!hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment.A02) {
                hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment.A02 = true;
                AnonymousClass01J r162 = ((C51112Sw) ((AbstractC51092Su) hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment).A00 = (AnonymousClass182) r162.A94.get();
                ((WaDialogFragment) hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment).A01 = (AnonymousClass180) r162.ALt.get();
            }
        } else if (this instanceof Hilt_SuspiciousLinkWarningDialogFragment) {
            Hilt_SuspiciousLinkWarningDialogFragment hilt_SuspiciousLinkWarningDialogFragment = (Hilt_SuspiciousLinkWarningDialogFragment) this;
            if (!hilt_SuspiciousLinkWarningDialogFragment.A02) {
                hilt_SuspiciousLinkWarningDialogFragment.A02 = true;
                SuspiciousLinkWarningDialogFragment suspiciousLinkWarningDialogFragment = (SuspiciousLinkWarningDialogFragment) hilt_SuspiciousLinkWarningDialogFragment;
                AnonymousClass01J r235 = ((C51112Sw) ((AbstractC51092Su) hilt_SuspiciousLinkWarningDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) suspiciousLinkWarningDialogFragment).A00 = (AnonymousClass182) r235.A94.get();
                ((WaDialogFragment) suspiciousLinkWarningDialogFragment).A01 = (AnonymousClass180) r235.ALt.get();
                suspiciousLinkWarningDialogFragment.A00 = (C14900mE) r235.A8X.get();
                suspiciousLinkWarningDialogFragment.A06 = (C252018m) r235.A7g.get();
                suspiciousLinkWarningDialogFragment.A02 = (AnonymousClass01d) r235.ALI.get();
                suspiciousLinkWarningDialogFragment.A03 = (AnonymousClass018) r235.ANb.get();
                suspiciousLinkWarningDialogFragment.A01 = (AnonymousClass18U) r235.AAU.get();
                suspiciousLinkWarningDialogFragment.A04 = (AnonymousClass1BD) r235.AKA.get();
            }
        } else if (this instanceof Hilt_RevokeLinkConfirmationDialogFragment) {
            Hilt_RevokeLinkConfirmationDialogFragment hilt_RevokeLinkConfirmationDialogFragment = (Hilt_RevokeLinkConfirmationDialogFragment) this;
            if (!hilt_RevokeLinkConfirmationDialogFragment.A02) {
                hilt_RevokeLinkConfirmationDialogFragment.A02 = true;
                RevokeLinkConfirmationDialogFragment revokeLinkConfirmationDialogFragment = (RevokeLinkConfirmationDialogFragment) hilt_RevokeLinkConfirmationDialogFragment;
                AnonymousClass01J r163 = ((C51112Sw) ((AbstractC51092Su) hilt_RevokeLinkConfirmationDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) revokeLinkConfirmationDialogFragment).A00 = (AnonymousClass182) r163.A94.get();
                ((WaDialogFragment) revokeLinkConfirmationDialogFragment).A01 = (AnonymousClass180) r163.ALt.get();
                revokeLinkConfirmationDialogFragment.A00 = (C15550nR) r163.A45.get();
                revokeLinkConfirmationDialogFragment.A01 = (C15610nY) r163.AMe.get();
                revokeLinkConfirmationDialogFragment.A02 = (C20710wC) r163.A8m.get();
            }
        } else if (this instanceof Hilt_PhoneHyperLinkDialogFragment) {
            Hilt_PhoneHyperLinkDialogFragment hilt_PhoneHyperLinkDialogFragment = (Hilt_PhoneHyperLinkDialogFragment) this;
            if (!hilt_PhoneHyperLinkDialogFragment.A02) {
                hilt_PhoneHyperLinkDialogFragment.A02 = true;
                PhoneHyperLinkDialogFragment phoneHyperLinkDialogFragment = (PhoneHyperLinkDialogFragment) hilt_PhoneHyperLinkDialogFragment;
                AnonymousClass01J r236 = ((C51112Sw) ((AbstractC51092Su) hilt_PhoneHyperLinkDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) phoneHyperLinkDialogFragment).A00 = (AnonymousClass182) r236.A94.get();
                ((WaDialogFragment) phoneHyperLinkDialogFragment).A01 = (AnonymousClass180) r236.ALt.get();
                phoneHyperLinkDialogFragment.A01 = (AnonymousClass18U) r236.AAU.get();
                phoneHyperLinkDialogFragment.A00 = (AnonymousClass12P) r236.A0H.get();
                phoneHyperLinkDialogFragment.A02 = (C15550nR) r236.A45.get();
                phoneHyperLinkDialogFragment.A05 = (AnonymousClass018) r236.ANb.get();
                phoneHyperLinkDialogFragment.A07 = (C254219i) r236.A0K.get();
                phoneHyperLinkDialogFragment.A03 = (C20730wE) r236.A4J.get();
                phoneHyperLinkDialogFragment.A04 = (AnonymousClass1AO) r236.AFg.get();
            }
        } else if (this instanceof Hilt_MuteDialogFragment) {
            Hilt_MuteDialogFragment hilt_MuteDialogFragment = (Hilt_MuteDialogFragment) this;
            if (!hilt_MuteDialogFragment.A02) {
                hilt_MuteDialogFragment.A02 = true;
                MuteDialogFragment muteDialogFragment = (MuteDialogFragment) hilt_MuteDialogFragment;
                AnonymousClass01J r237 = ((C51112Sw) ((AbstractC51092Su) hilt_MuteDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) muteDialogFragment).A00 = (AnonymousClass182) r237.A94.get();
                ((WaDialogFragment) muteDialogFragment).A01 = (AnonymousClass180) r237.ALt.get();
                muteDialogFragment.A05 = (C14830m7) r237.ALb.get();
                muteDialogFragment.A00 = (C14900mE) r237.A8X.get();
                muteDialogFragment.A0A = (AbstractC14440lR) r237.ANe.get();
                muteDialogFragment.A02 = (C16170oZ) r237.AM4.get();
                muteDialogFragment.A01 = (C15450nH) r237.AII.get();
                muteDialogFragment.A04 = (C15550nR) r237.A45.get();
                muteDialogFragment.A07 = (AnonymousClass018) r237.ANb.get();
                muteDialogFragment.A09 = (C15860o1) r237.A3H.get();
                muteDialogFragment.A03 = (C22330yu) r237.A3I.get();
                muteDialogFragment.A06 = (C14820m6) r237.AN3.get();
                muteDialogFragment.A08 = (C21320xE) r237.A4Y.get();
            }
        } else if (this instanceof Hilt_MessageDialogFragment) {
            Hilt_MessageDialogFragment hilt_MessageDialogFragment = (Hilt_MessageDialogFragment) this;
            if (!hilt_MessageDialogFragment.A02) {
                hilt_MessageDialogFragment.A02 = true;
                MessageDialogFragment messageDialogFragment = (MessageDialogFragment) hilt_MessageDialogFragment;
                AnonymousClass01J r164 = ((C51112Sw) ((AbstractC51092Su) hilt_MessageDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) messageDialogFragment).A00 = (AnonymousClass182) r164.A94.get();
                ((WaDialogFragment) messageDialogFragment).A01 = (AnonymousClass180) r164.ALt.get();
                messageDialogFragment.A03 = (AnonymousClass19M) r164.A6R.get();
                messageDialogFragment.A02 = (AnonymousClass018) r164.ANb.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment = (Hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment.A02 = true;
                AnonymousClass01J r165 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment).A00 = (AnonymousClass182) r165.A94.get();
                ((WaDialogFragment) hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment).A01 = (AnonymousClass180) r165.ALt.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment = (Hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment displayExceptionDialogFactory$SoftwareExpiredDialogFragment = (DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment) hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment;
                AnonymousClass01J r238 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$SoftwareExpiredDialogFragment).A00 = (AnonymousClass182) r238.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$SoftwareExpiredDialogFragment).A01 = (AnonymousClass180) r238.ALt.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A04 = (C14830m7) r238.ALb.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A02 = (C21740xu) r238.AM1.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A01 = (C20640w5) r238.AHk.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A00 = (AnonymousClass12P) r238.A0H.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A05 = (AnonymousClass018) r238.ANb.get();
                displayExceptionDialogFactory$SoftwareExpiredDialogFragment.A03 = (AnonymousClass01d) r238.ALI.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment = (Hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment = (DisplayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment) hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment;
                AnonymousClass01J r166 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment).A00 = (AnonymousClass182) r166.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment).A01 = (AnonymousClass180) r166.ALt.get();
                displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment.A02 = (C21740xu) r166.AM1.get();
                displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment.A00 = (C20640w5) r166.AHk.get();
                displayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment.A01 = (AnonymousClass1FO) r166.AJf.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment = (Hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$LoginFailedDialogFragment displayExceptionDialogFactory$LoginFailedDialogFragment = (DisplayExceptionDialogFactory$LoginFailedDialogFragment) hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment;
                AnonymousClass01J r167 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$LoginFailedDialogFragment).A00 = (AnonymousClass182) r167.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$LoginFailedDialogFragment).A01 = (AnonymousClass180) r167.ALt.get();
                displayExceptionDialogFactory$LoginFailedDialogFragment.A00 = (C14830m7) r167.ALb.get();
                displayExceptionDialogFactory$LoginFailedDialogFragment.A01 = (C14820m6) r167.AN3.get();
                displayExceptionDialogFactory$LoginFailedDialogFragment.A02 = (C18350sJ) r167.AHX.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment = (Hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment displayExceptionDialogFactory$DoNotShareCodeDialogFragment = (DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment) hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment;
                AnonymousClass01J r168 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$DoNotShareCodeDialogFragment).A00 = (AnonymousClass182) r168.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$DoNotShareCodeDialogFragment).A01 = (AnonymousClass180) r168.ALt.get();
                displayExceptionDialogFactory$DoNotShareCodeDialogFragment.A00 = (AnonymousClass12P) r168.A0H.get();
                displayExceptionDialogFactory$DoNotShareCodeDialogFragment.A01 = (C252018m) r168.A7g.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment = (Hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$ContactBlockedDialogFragment displayExceptionDialogFactory$ContactBlockedDialogFragment = (DisplayExceptionDialogFactory$ContactBlockedDialogFragment) hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment;
                AnonymousClass01J r169 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$ContactBlockedDialogFragment).A00 = (AnonymousClass182) r169.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$ContactBlockedDialogFragment).A01 = (AnonymousClass180) r169.ALt.get();
                displayExceptionDialogFactory$ContactBlockedDialogFragment.A00 = (C238013b) r169.A1Z.get();
            }
        } else if (this instanceof Hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment) {
            Hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment = (Hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment) this;
            if (!hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment.A02) {
                hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment.A02 = true;
                DisplayExceptionDialogFactory$ClockWrongDialogFragment displayExceptionDialogFactory$ClockWrongDialogFragment = (DisplayExceptionDialogFactory$ClockWrongDialogFragment) hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment;
                AnonymousClass01J r170 = ((C51112Sw) ((AbstractC51092Su) hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) displayExceptionDialogFactory$ClockWrongDialogFragment).A00 = (AnonymousClass182) r170.A94.get();
                ((WaDialogFragment) displayExceptionDialogFactory$ClockWrongDialogFragment).A01 = (AnonymousClass180) r170.ALt.get();
                displayExceptionDialogFactory$ClockWrongDialogFragment.A02 = (C14830m7) r170.ALb.get();
                displayExceptionDialogFactory$ClockWrongDialogFragment.A00 = (C20640w5) r170.AHk.get();
                displayExceptionDialogFactory$ClockWrongDialogFragment.A03 = (AnonymousClass018) r170.ANb.get();
                displayExceptionDialogFactory$ClockWrongDialogFragment.A01 = (AnonymousClass01d) r170.ALI.get();
            }
        } else if (this instanceof Hilt_CallConfirmationFragment) {
            Hilt_CallConfirmationFragment hilt_CallConfirmationFragment = (Hilt_CallConfirmationFragment) this;
            if (!hilt_CallConfirmationFragment.A02) {
                hilt_CallConfirmationFragment.A02 = true;
                CallConfirmationFragment callConfirmationFragment = (CallConfirmationFragment) hilt_CallConfirmationFragment;
                AnonymousClass01J r239 = ((C51112Sw) ((AbstractC51092Su) hilt_CallConfirmationFragment.generatedComponent())).A0Y;
                ((WaDialogFragment) callConfirmationFragment).A00 = (AnonymousClass182) r239.A94.get();
                ((WaDialogFragment) callConfirmationFragment).A01 = (AnonymousClass180) r239.ALt.get();
                callConfirmationFragment.A00 = (C15570nT) r239.AAr.get();
                callConfirmationFragment.A05 = (AnonymousClass19Z) r239.A2o.get();
                callConfirmationFragment.A01 = (C15550nR) r239.A45.get();
                callConfirmationFragment.A03 = (AnonymousClass018) r239.ANb.get();
                callConfirmationFragment.A02 = (C14820m6) r239.AN3.get();
                callConfirmationFragment.A04 = (C15600nX) r239.A8x.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            WaDialogFragment waDialogFragment = (WaDialogFragment) this;
            AnonymousClass01J r171 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            waDialogFragment.A00 = (AnonymousClass182) r171.A94.get();
            waDialogFragment.A01 = (AnonymousClass180) r171.ALt.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
