package com.whatsapp.wamsys;

import X.AbstractC16010oI;
import X.AbstractC29081Qu;
import X.AbstractRunnableC14570le;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass1JS;
import X.AnonymousClass1MU;
import X.AnonymousClass1R0;
import X.AnonymousClass1R1;
import X.AnonymousClass1R2;
import X.AnonymousClass1R3;
import X.AnonymousClass1R4;
import X.AnonymousClass1R5;
import X.AnonymousClass1R6;
import X.AnonymousClass1R7;
import X.AnonymousClass1R8;
import X.AnonymousClass1R9;
import X.AnonymousClass1RA;
import X.AnonymousClass1RB;
import X.AnonymousClass1RC;
import X.AnonymousClass1RD;
import X.AnonymousClass1RF;
import X.AnonymousClass1RG;
import X.AnonymousClass1RH;
import X.AnonymousClass1RI;
import X.AnonymousClass1RJ;
import X.AnonymousClass1RK;
import X.AnonymousClass1RM;
import X.AnonymousClass1RQ;
import X.AnonymousClass1RR;
import X.AnonymousClass1RS;
import X.AnonymousClass1RT;
import X.AnonymousClass1RV;
import X.AnonymousClass1RW;
import X.AnonymousClass1RX;
import X.AnonymousClass1RY;
import X.AnonymousClass1RZ;
import X.C14370lK;
import X.C15660nh;
import X.C15940oB;
import X.C15950oC;
import X.C15980oF;
import X.C15990oG;
import X.C16120oU;
import X.C16150oX;
import X.C16310on;
import X.C16330op;
import X.C17200qQ;
import X.C19980v1;
import X.C21060wn;
import X.C21080wp;
import X.C21090wq;
import X.C21100wr;
import X.C21110ws;
import X.C21120wu;
import X.C21130wv;
import X.C21140ww;
import X.C21150wx;
import X.C21160wy;
import X.C21170wz;
import X.C21800y0;
import X.C251918l;
import X.C29091Qv;
import X.C29101Qw;
import X.C29131Qz;
import X.C29141Ra;
import X.C29151Rb;
import X.C29181Re;
import X.C29201Rg;
import X.C29221Ri;
import X.C29231Rj;
import X.C29241Rk;
import X.C29261Rm;
import X.C29271Rn;
import X.C29281Ro;
import X.C29301Rq;
import X.C29311Rr;
import X.C29321Rs;
import X.C29331Rt;
import X.C29341Ru;
import X.C29361Rw;
import X.EnumC29121Qy;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.msys.mcf.MsysError;
import com.facebook.redex.RunnableBRunnable0Shape0S0202000_I0;
import com.whatsapp.jid.UserJid;
import com.whatsapp.protocol.ProtocolJniHelper;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

/* loaded from: classes2.dex */
public class JniBridge {
    public static C19980v1 DEPENDENCIES;
    public static volatile JniBridge INSTANCE;
    public final C21170wz jniBridgeExceptionHandler;
    public C21080wp jniCallbacks;
    public final AtomicReference wajContext = new AtomicReference();

    public static native double jvidispatchDIO(int i, long j, Object obj);

    public static native long jvidispatchI();

    public static native long jvidispatchIIDO(int i, long j, double d, Object obj);

    public static native long jvidispatchIIIIIIOOOOOOO(long j, long j2, long j3, long j4, long j5, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7);

    public static native long jvidispatchIIIIIOOOO(long j, long j2, long j3, long j4, Object obj, Object obj2, Object obj3, Object obj4);

    public static native long jvidispatchIIIIOOOOOOOOO(long j, long j2, long j3, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9);

    public static native long jvidispatchIIIIOOOOOOOOOOOO(long j, long j2, long j3, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12);

    public static native long jvidispatchIIIO(int i, long j, long j2, Object obj);

    public static native long jvidispatchIIIOO(long j, long j2, Object obj, Object obj2);

    public static native long jvidispatchIIO(int i, long j, Object obj);

    public static native long jvidispatchIIOO(int i, long j, Object obj, Object obj2);

    public static native long jvidispatchIIOOOO(int i, long j, Object obj, Object obj2, Object obj3, Object obj4);

    public static native long jvidispatchIIOOOOO(long j, Object obj, Object obj2, Object obj3, Object obj4, Object obj5);

    public static native long jvidispatchIIOOOOOOOOOO(long j, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10);

    public static native long jvidispatchIO(int i, Object obj);

    public static native long jvidispatchIOO(Object obj, Object obj2);

    public static native long jvidispatchIOOO(Object obj, Object obj2, Object obj3);

    public static native long jvidispatchIOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4);

    public static native long jvidispatchIOOOOO(Object obj, Object obj2, Object obj3, Object obj4, Object obj5);

    public static native long jvidispatchIOOOOOOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9);

    public static native long jvidispatchIOOOOOOOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10);

    public static native long jvidispatchIOOOOOOOOOOOO(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12);

    public static native Object jvidispatchO(int i);

    public static native Object jvidispatchOII(long j, long j2);

    public static native Object jvidispatchOIIIIIIIOOOOO(long j, long j2, long j3, long j4, long j5, long j6, long j7, Object obj, Object obj2, Object obj3, Object obj4, Object obj5);

    public static native Object jvidispatchOIIIOOOO(long j, long j2, long j3, Object obj, Object obj2, Object obj3, Object obj4);

    public static native Object jvidispatchOIIIOOOOOO(long j, long j2, long j3, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6);

    public static native Object jvidispatchOIIO(int i, long j, long j2, Object obj);

    public static native Object jvidispatchOIIOO(long j, long j2, Object obj, Object obj2);

    public static native Object jvidispatchOIO(int i, long j, Object obj);

    public static native Object jvidispatchOIOO(int i, long j, Object obj, Object obj2);

    public static native Object jvidispatchOIOOO(int i, long j, Object obj, Object obj2, Object obj3);

    public static native Object jvidispatchOIOOOO(int i, long j, Object obj, Object obj2, Object obj3, Object obj4);

    public static native Object jvidispatchOIOOOOO(int i, long j, Object obj, Object obj2, Object obj3, Object obj4, Object obj5);

    public static native Object jvidispatchOO(int i, Object obj);

    public static native Object jvidispatchOOO(int i, Object obj, Object obj2);

    public static native Object jvidispatchOOOO(Object obj, Object obj2, Object obj3);

    public static native Object jvidispatchOOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4);

    public static native Object jvidispatchOOOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4, Object obj5);

    public static native Object jvidispatchOOOOOOO(int i, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6);

    public static native Object jvidispatchOOOOOOOO(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7);

    public JniBridge(C21080wp r2, C21170wz r3) {
        this.jniCallbacks = r2;
        this.jniBridgeExceptionHandler = r3;
    }

    public static C15950oC A00(String str, int i) {
        UserJid nullable = UserJid.getNullable(str);
        AnonymousClass009.A05(nullable);
        String str2 = nullable.user;
        int i2 = 0;
        if (nullable instanceof AnonymousClass1MU) {
            i2 = 1;
        }
        return new C15950oC(i2, str2, i);
    }

    public static JniBridge getInstance() {
        if (INSTANCE == null) {
            synchronized (JniBridge.class) {
                if (INSTANCE == null) {
                    C19980v1 r0 = DEPENDENCIES;
                    if (r0 != null) {
                        INSTANCE = new JniBridge((C21080wp) r0.A01.get(), (C21170wz) DEPENDENCIES.A00.get());
                    } else {
                        throw new IllegalStateException("Dependencies are not set. Call setDependencies() first.");
                    }
                }
            }
        }
        return INSTANCE;
    }

    public static long jnidispatchI(int i) {
        if (i != 0) {
            return 0;
        }
        try {
            return INSTANCE.jniCallbacks.A03.A00() / 1000;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIIIIIIIIIIOOOOOOOOOO(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, long j12, long j13, long j14, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        int i;
        EnumC29121Qy r1;
        C29131Qz r2;
        AnonymousClass1R0 r0;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj10;
            int i3 = (int) j2;
            String str = (String) obj;
            int i4 = (int) j3;
            int i5 = (int) j4;
            String str2 = (String) obj2;
            String str3 = (String) obj3;
            boolean z = false;
            if (0 != j13) {
                z = true;
            }
            int i6 = (int) j5;
            String str4 = (String) obj4;
            String str5 = (String) obj5;
            String str6 = (String) obj6;
            boolean z2 = false;
            if (0 != j14) {
                z2 = true;
            }
            String str7 = (String) obj7;
            int i7 = (int) j6;
            String str8 = (String) obj8;
            AbstractC29081Qu r22 = (AbstractC29081Qu) obj9;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (r22 instanceof C29091Qv) {
                C29101Qw r10 = ((C29091Qv) r22).A00;
                if (i2 == 1) {
                    r1 = EnumC29121Qy.OK;
                } else if (i2 != 3) {
                    r1 = EnumC29121Qy.ERROR_UNSPECIFIED;
                } else {
                    r1 = EnumC29121Qy.FAIL;
                }
                if (r1 == EnumC29121Qy.ERROR_UNSPECIFIED) {
                    StringBuilder sb = new StringBuilder("wamsys/registration/unknown-exist-status ");
                    sb.append(i2);
                    Log.e(sb.toString());
                }
                try {
                    boolean z3 = false;
                    boolean z4 = true;
                    if (r1 == EnumC29121Qy.OK) {
                        if (i3 == 1) {
                            z3 = true;
                        }
                        r2 = new C29131Qz(r1, str, z3);
                    } else {
                        if (i == 16) {
                            r0 = AnonymousClass1R0.ERROR_BAD_TOKEN;
                        } else if (i == 30) {
                            r0 = AnonymousClass1R0.DEVICE_CONFIRM_OR_SECOND_CODE;
                        } else if (i != 31) {
                            switch (i) {
                                case 1:
                                    r0 = AnonymousClass1R0.INCORRECT;
                                    break;
                                case 2:
                                    r0 = AnonymousClass1R0.BLOCKED;
                                    break;
                                case 3:
                                    r0 = AnonymousClass1R0.LENGTH_LONG;
                                    break;
                                case 4:
                                    r0 = AnonymousClass1R0.LENGTH_SHORT;
                                    break;
                                case 5:
                                    r0 = AnonymousClass1R0.FORMAT_WRONG;
                                    break;
                                case 6:
                                    r0 = AnonymousClass1R0.TEMPORARILY_UNAVAILABLE;
                                    break;
                                case 7:
                                    r0 = AnonymousClass1R0.OLD_VERSION;
                                    break;
                                default:
                                    switch (i) {
                                        case 23:
                                            r0 = AnonymousClass1R0.SECURITY_CODE;
                                            break;
                                        case 24:
                                            r0 = AnonymousClass1R0.INVALID_SKEY_SIGNATURE;
                                            break;
                                        case 25:
                                            r0 = AnonymousClass1R0.BIZ_NOT_ALLOWED;
                                            break;
                                        case 26:
                                            r0 = AnonymousClass1R0.LIMITED_RELEASE;
                                            break;
                                        default:
                                            throw new IOException("fail, unknown reason");
                                    }
                            }
                        } else {
                            r0 = AnonymousClass1R0.SECOND_CODE;
                        }
                        r2 = new C29131Qz(r0, r1);
                        r2.A07 = String.valueOf(j7);
                        r2.A02 = i4;
                        r2.A01 = i5;
                        r2.A09 = String.valueOf(j8);
                        r2.A0A = String.valueOf(j9);
                        r2.A04 = j10;
                        r2.A0C = str2;
                        r2.A0B = str3;
                        r2.A05 = j11;
                        r2.A03 = j12;
                        r2.A0D = z;
                        r2.A00 = i6;
                        if (i7 != 1) {
                            z4 = false;
                        }
                        r2.A0E = z4;
                        r2.A06 = new AnonymousClass1R1(str5, str6, str7, z2);
                        r2.A08 = str8;
                        if (str4 != null) {
                            r2.A0F = Base64.decode(str4, 0);
                        }
                    }
                    r10.A02(r2);
                    return 0;
                } catch (IOException unused) {
                    StringBuilder sb2 = new StringBuilder("wamsys/registration/unknown-exist-fail-reason ");
                    sb2.append(i);
                    Log.e(sb2.toString());
                    r10.A02(null);
                }
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIIIIIIIOOOOOOOOOO(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        int i;
        AnonymousClass1R4 r5;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj10;
            int i3 = (int) j2;
            String str = (String) obj;
            int i4 = (int) j3;
            String str2 = (String) obj2;
            String str3 = (String) obj3;
            boolean z = false;
            if (0 != j10) {
                z = true;
            }
            String str4 = (String) obj5;
            int i5 = (int) j5;
            AbstractC29081Qu r2 = (AbstractC29081Qu) obj9;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (r2 instanceof AnonymousClass1R2) {
                AnonymousClass1R3 r22 = ((AnonymousClass1R2) r2).A00;
                if (i2 == 1 || i2 == 2) {
                    r5 = AnonymousClass1R4.YES;
                } else {
                    if (i2 == 3) {
                        if (i == 1) {
                            r5 = AnonymousClass1R4.FAIL_INCORRECT;
                        } else if (i == 2) {
                            r5 = AnonymousClass1R4.FAIL_BLOCKED;
                        } else if (i == 6) {
                            r5 = AnonymousClass1R4.FAIL_TEMPORARILY_UNAVAILABLE;
                        } else if (i == 11) {
                            r5 = AnonymousClass1R4.FAIL_TOO_MANY_GUESSES;
                        } else if (i == 22) {
                            r5 = AnonymousClass1R4.FAIL_STALE;
                        } else if (i == 28) {
                            r5 = AnonymousClass1R4.FAIL_RESET_TOO_SOON;
                        } else if (i == 19) {
                            r5 = AnonymousClass1R4.FAIL_MISMATCH;
                        } else if (i == 20) {
                            r5 = AnonymousClass1R4.FAIL_GUESSED_TOO_FAST;
                        }
                    }
                    r5 = AnonymousClass1R4.ERROR_UNSPECIFIED;
                }
                if (r5 == AnonymousClass1R4.ERROR_UNSPECIFIED) {
                    StringBuilder sb = new StringBuilder("wamsys/registration/security-status-unspecified; response-status ");
                    sb.append(i2);
                    sb.append(" failure-reason ");
                    sb.append(i);
                    Log.e(sb.toString());
                }
                AnonymousClass1R5 r1 = new AnonymousClass1R5(r5);
                r1.A06 = str;
                boolean z2 = true;
                if (i3 != 1) {
                    z2 = false;
                }
                r1.A09 = z2;
                r1.A05 = String.valueOf(i4);
                r1.A01 = j6;
                r1.A08 = str2;
                r1.A07 = str3;
                r1.A03 = j7;
                r1.A02 = j8;
                r1.A0A = z;
                if (str4 != null) {
                    r1.A0B = Base64.decode(str4, 0);
                }
                r1.A00 = i5;
                r22.A02(r1);
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIIIIIIOOOOOOOOOO(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        int i;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj10;
            int i3 = (int) j2;
            String str = (String) obj;
            String str2 = (String) obj2;
            String str3 = (String) obj3;
            String str4 = (String) obj4;
            String str5 = (String) obj6;
            String str6 = (String) obj7;
            boolean z = false;
            if (0 != j10) {
                z = true;
            }
            String str7 = (String) obj8;
            int i4 = (int) j3;
            AbstractC29081Qu r2 = (AbstractC29081Qu) obj9;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (r2 instanceof AnonymousClass1R6) {
                AnonymousClass1R7 r22 = ((AnonymousClass1R6) r2).A00;
                AnonymousClass1R8 A01 = C251918l.A01(i2, i);
                if (A01 == AnonymousClass1R8.ERROR_UNSPECIFIED) {
                    StringBuilder sb = new StringBuilder("wamsys/registration/verify-code-status-unspecified; response-status ");
                    sb.append(i2);
                    sb.append(" failure-reason ");
                    sb.append(i);
                    Log.e(sb.toString());
                }
                AnonymousClass1R9 r1 = new AnonymousClass1R9(A01);
                r1.A08 = str;
                boolean z2 = true;
                if (i3 != 1) {
                    z2 = false;
                }
                r1.A0C = z2;
                r1.A09 = String.valueOf(j6);
                r1.A0B = str2;
                r1.A0A = str3;
                r1.A04 = j7;
                r1.A02 = j8;
                r1.A03 = j9;
                r1.A05 = new AnonymousClass1R1(str5, str6, str7, z);
                if (str4 != null) {
                    r1.A0D = Base64.decode(str4, 0);
                }
                r1.A00 = i4;
                r22.A02(r1);
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIIIIIOOOOOOOOOOOOO(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13) {
        int i;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj13;
            String str = (String) obj;
            int i3 = (int) j2;
            String str2 = (String) obj2;
            String str3 = (String) obj3;
            String str4 = (String) obj4;
            String str5 = (String) obj5;
            int i4 = (int) j3;
            String str6 = (String) obj6;
            String str7 = (String) obj7;
            String str8 = (String) obj8;
            boolean z = false;
            if (0 != j9) {
                z = true;
            }
            String str9 = (String) obj9;
            String str10 = (String) obj10;
            AbstractC29081Qu r2 = (AbstractC29081Qu) obj12;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (r2 instanceof AnonymousClass1RA) {
                AnonymousClass1RB r22 = ((AnonymousClass1RA) r2).A00;
                AnonymousClass1RC A00 = C251918l.A00(i2, i);
                if (A00 == AnonymousClass1RC.ERROR_UNSPECIFIED) {
                    StringBuilder sb = new StringBuilder("wamsys/registration/request-code-status-unspecified; response-status ");
                    sb.append(i2);
                    sb.append(" failure-reason ");
                    sb.append(i);
                    Log.e(sb.toString());
                }
                AnonymousClass1RD r5 = new AnonymousClass1RD(A00);
                r5.A02 = i;
                r5.A0A = str;
                boolean z2 = true;
                if (i3 != 1) {
                    z2 = false;
                }
                r5.A0I = z2;
                r5.A0D = String.valueOf(j4);
                r5.A08 = str2;
                r5.A0C = str3;
                r5.A0E = String.valueOf(j5);
                r5.A0F = String.valueOf(j6);
                r5.A0H = str4;
                r5.A0G = str5;
                r5.A03 = j7;
                r5.A00 = i4;
                r5.A07 = str6;
                r5.A09 = String.valueOf(j8);
                r5.A04 = new AnonymousClass1R1(str7, str8, str9, z);
                r5.A06 = str10;
                r22.A02(r5);
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIO(long j, long j2, long j3, long j4, long j5, Object obj) {
        try {
            C21080wp r4 = INSTANCE.jniCallbacks;
            int i = (int) j;
            byte[] bArr = (byte[]) obj;
            int i2 = (int) j2;
            AnonymousClass00E r0 = new AnonymousClass00E((int) j3, (int) j4, (int) j5);
            C16120oU r9 = r4.A04;
            Integer A00 = r9.A00(r0, i, false);
            if (A00 != null) {
                int intValue = A00.intValue();
                if (i2 != 2) {
                    r9.A0H.A01.execute(new Runnable(bArr, i2, i, intValue) { // from class: X.1RE
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ int A01;
                        public final /* synthetic */ int A02;
                        public final /* synthetic */ byte[] A04;

                        {
                            this.A00 = r3;
                            this.A01 = r4;
                            this.A04 = r2;
                            this.A02 = r5;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            C16120oU r5 = C16120oU.this;
                            int i3 = this.A00;
                            int i4 = this.A01;
                            byte[] bArr2 = this.A04;
                            int i5 = this.A02;
                            if (i3 == 1) {
                                if (r5.A0J()) {
                                    r5.A04.A04(bArr2, i4, i5);
                                    r5.A04.A01();
                                    r5.A03();
                                }
                            } else if (r5.A0H()) {
                                r5.A05.A04(bArr2, i4, i5);
                                r5.A05.A01();
                                r5.A0G(false);
                                r5.A00.A01();
                            }
                        }
                    });
                    return 0;
                }
                r9.A0H.A02.execute(new RunnableBRunnable0Shape0S0202000_I0(r9, i, bArr, intValue, 2));
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIIOOOO(long j, long j2, long j3, long j4, long j5, Object obj, Object obj2, Object obj3, Object obj4) {
        int i;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj4;
            int i3 = (int) j3;
            AbstractC29081Qu r1 = (AbstractC29081Qu) obj3;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (!(r1 instanceof AnonymousClass1RF)) {
                return 0;
            }
            AnonymousClass1RG r3 = ((AnonymousClass1RF) r1).A00;
            AnonymousClass1RC A00 = C251918l.A00(i2, i);
            if (A00 == AnonymousClass1RC.ERROR_UNSPECIFIED) {
                StringBuilder sb = new StringBuilder("wamsys/registration/request-code-status-standalone-unspecified; response-status ");
                sb.append(i2);
                sb.append(" failure-reason ");
                sb.append(i);
                Log.e(sb.toString());
            }
            AnonymousClass1RD r12 = new AnonymousClass1RD(A00);
            r12.A02 = i;
            r12.A0D = String.valueOf(j4);
            r12.A0E = String.valueOf(j5);
            r12.A00 = i3;
            r3.A02(r12);
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIIOOOOO(long j, long j2, long j3, long j4, Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        int i;
        try {
            int i2 = (int) j;
            MsysError msysError = (MsysError) obj5;
            int i3 = (int) j2;
            String str = (String) obj;
            String str2 = (String) obj2;
            String str3 = (String) obj3;
            int i4 = (int) j3;
            AbstractC29081Qu r3 = (AbstractC29081Qu) obj4;
            if (msysError != null) {
                i = msysError.getCode();
            } else {
                i = 0;
            }
            if (r3 instanceof AnonymousClass1RH) {
                AnonymousClass1RI r32 = ((AnonymousClass1RH) r3).A00;
                AnonymousClass1R8 A01 = C251918l.A01(i2, i);
                if (A01 == AnonymousClass1R8.ERROR_UNSPECIFIED) {
                    StringBuilder sb = new StringBuilder("wamsys/registration/verify-code-status-standalone-unspecified; response-status ");
                    sb.append(i2);
                    sb.append(" failure-reason ");
                    sb.append(i);
                    Log.e(sb.toString());
                }
                AnonymousClass1R9 r2 = new AnonymousClass1R9(A01);
                r2.A08 = str;
                boolean z = true;
                if (i3 != 1) {
                    z = false;
                }
                r2.A0C = z;
                r2.A09 = String.valueOf(j4);
                r2.A07 = str3;
                r2.A01 = i4;
                if (str2 != null) {
                    r2.A0D = Base64.decode(str2, 0);
                }
                r32.A02(r2);
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIIIO(int i, long j, long j2, long j3, Object obj) {
        try {
            if (i == 0) {
                ((AnonymousClass1RK) obj).A02.A09(j2);
            } else if (i == 1) {
                ((AnonymousClass1RJ) obj).A02.A09(j2);
                return 0;
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x016b, code lost:
        if (r1 == 7) goto L_0x016d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long jnidispatchIIO(int r13, long r14, java.lang.Object r16) {
        /*
        // Method dump skipped, instructions count: 396
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wamsys.JniBridge.jnidispatchIIO(int, long, java.lang.Object):long");
    }

    public static long jnidispatchIIOO(int i, long j, Object obj, Object obj2) {
        int i2;
        int i3;
        int i4;
        try {
            switch (i) {
                case 0:
                    int i5 = (int) j;
                    byte[] bArr = (byte[]) obj2;
                    C21140ww r1 = (C21140ww) INSTANCE.jniCallbacks.A0D.get();
                    if (bArr == null || bArr.length <= 0) {
                        return 0;
                    }
                    r1.A00.A0C.A00(i5, bArr);
                    return 1;
                case 1:
                    int i6 = (int) j;
                    Map map = (Map) obj2;
                    C15990oG r7 = (C15990oG) INSTANCE.jniCallbacks.A0C.get();
                    Lock A00 = r7.A0J.A00(r7.A0I());
                    if (A00 == null) {
                        r7.A0I.A00();
                    } else {
                        A00.lock();
                    }
                    int i7 = (i6 % 16777214) + 1;
                    ArrayList arrayList = new ArrayList();
                    for (Map.Entry entry : map.entrySet()) {
                        arrayList.add(new AnonymousClass1RW(((Integer) entry.getKey()).intValue(), (byte[]) entry.getValue()));
                    }
                    r7.A0e(arrayList, i7);
                    if (A00 == null) {
                        return 0;
                    }
                    A00.unlock();
                    return 0;
                case 2:
                    List list = (List) obj2;
                    C21090wq r3 = (C21090wq) INSTANCE.jniCallbacks.A08.get();
                    int[] iArr = new int[list.size()];
                    for (int i8 = 0; i8 < list.size(); i8++) {
                        iArr[i8] = ((Integer) list.get(i8)).intValue();
                    }
                    r3.A00.A09.A03(iArr);
                    return 1;
                case 3:
                    C21110ws r5 = (C21110ws) INSTANCE.jniCallbacks.A0B.get();
                    C15950oC A002 = A00((String) obj, (int) j);
                    C21100wr r32 = r5.A00;
                    synchronized (r32) {
                        AnonymousClass1RV A003 = r32.A00(A002);
                        boolean z = true;
                        if (A003 == null) {
                            byte[] A03 = r5.A01.A0B.A03(A002);
                            if (A03 == null) {
                                r32.A03(new AnonymousClass1RV(), A002);
                            } else {
                                try {
                                    AnonymousClass1RV r0 = new AnonymousClass1RV(A03);
                                    C15990oG.A05(r0);
                                    r32.A03(r0, A002);
                                } catch (IOException unused) {
                                    r5.A00(A002);
                                }
                            }
                            return 0;
                        } else if (A003.A00) {
                            z = false;
                        }
                        if (z) {
                            return 1;
                        }
                        return 0;
                    }
                case 4:
                    ((C21110ws) INSTANCE.jniCallbacks.A0B.get()).A00(A00((String) obj, (int) j));
                    return 1;
                case 6:
                    int i9 = (int) j;
                    MsysError msysError = (MsysError) obj2;
                    AbstractC29081Qu r13 = (AbstractC29081Qu) obj;
                    if (msysError != null) {
                        i3 = msysError.getCode();
                    } else {
                        i3 = 0;
                    }
                    if (r13 instanceof AnonymousClass1RQ) {
                        AnonymousClass1RR r33 = ((AnonymousClass1RQ) r13).A00;
                        if (i9 != 1) {
                            i4 = 0;
                            if (i9 != 3) {
                                i4 = -1;
                                StringBuilder sb = new StringBuilder("wamsys/registration/autoconf-verifier-request-status-unspecified; response-status ");
                                sb.append(i9);
                                sb.append(" failure-reason ");
                                sb.append(i3);
                                Log.e(sb.toString());
                            }
                        } else {
                            i4 = 1;
                        }
                        r33.A02(new AnonymousClass1RS(i4));
                        return 0;
                    }
                    break;
                case 7:
                    int i10 = (int) j;
                    MsysError msysError2 = (MsysError) obj2;
                    AbstractC29081Qu r132 = (AbstractC29081Qu) obj;
                    if (msysError2 != null) {
                        i2 = msysError2.getCode();
                    } else {
                        i2 = 0;
                    }
                    if (r132 instanceof AnonymousClass1RT) {
                        StringBuilder sb2 = new StringBuilder("wamsys/registration/send-funnel-log/status/");
                        sb2.append(i10);
                        sb2.append("/failureReason/");
                        sb2.append(i2);
                        Log.i(sb2.toString());
                        ((AnonymousClass1RT) r132).A00.A02(null);
                        return 0;
                    }
                    break;
            }
            return 0;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIIOOO(int i, long j, Object obj, Object obj2, Object obj3) {
        try {
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
        if (i != 0) {
            if (i == 1) {
                byte[] bArr = (byte[]) obj3;
                C21130wv r3 = (C21130wv) INSTANCE.jniCallbacks.A07.get();
                C15950oC A00 = A00((String) obj, (int) j);
                if (bArr == null) {
                    r3.A00.A0W(A00);
                    return 1;
                }
                try {
                    r3.A00.A0U(new AnonymousClass1JS(C15940oB.A01(bArr)), A00);
                    return 1;
                } catch (AnonymousClass1RY e2) {
                    Log.e("IdentityKeyStoreImpl/Could not save the identity key.", e2);
                    return 0;
                }
            } else if (i == 2) {
                INSTANCE.jniCallbacks.A07.get();
                C15950oC A002 = A00((String) obj, (int) j);
                StringBuilder sb = new StringBuilder("axolotl trusting ");
                sb.append(A002);
                sb.append(" key pair");
                Log.i(sb.toString());
                return 1;
            } else if (i != 3) {
                return 0;
            } else {
                String str = (String) obj;
                int i2 = (int) j;
                byte[] bArr2 = (byte[]) obj3;
                C21110ws r32 = (C21110ws) INSTANCE.jniCallbacks.A0B.get();
                try {
                    AnonymousClass1RV r1 = new AnonymousClass1RV(bArr2);
                    C15990oG r0 = r32.A01;
                    C15990oG.A05(r1);
                    C15950oC A003 = A00(str, i2);
                    C21100wr r12 = r32.A00;
                    synchronized (r12) {
                        r0.A0B.A02(A003, bArr2);
                        r12.A04(A003);
                    }
                    return 1;
                } catch (IOException e3) {
                    Log.e("sessionStoreImpl/invalid-session-record", e3);
                    return 0;
                }
            }
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
        INSTANCE.jniCallbacks.A07.get();
        return 0;
    }

    public static long jnidispatchIIOOOO(int i, long j, Object obj, Object obj2, Object obj3, Object obj4) {
        int i2;
        try {
            if (i == 0) {
                byte[] bArr = (byte[]) obj4;
                C15980oF r1 = new C15980oF(A00((String) obj2, (int) j), (String) obj);
                C15990oG r0 = ((C21120wu) INSTANCE.jniCallbacks.A0A.get()).A00;
                if (bArr == null) {
                    r0.A0j(r1);
                    return 1;
                }
                r0.A0d(r1, bArr);
                return 1;
            } else if (i != 1) {
                if (i == 2) {
                    MsysError msysError = (MsysError) obj4;
                    if (msysError != null) {
                        msysError.getCode();
                    }
                } else if (i == 3) {
                    int i3 = (int) j;
                    MsysError msysError2 = (MsysError) obj4;
                    String str = (String) obj;
                    AbstractC29081Qu r12 = (AbstractC29081Qu) obj3;
                    if (msysError2 != null) {
                        i2 = msysError2.getCode();
                    } else {
                        i2 = 0;
                    }
                    if (r12 instanceof AnonymousClass1RZ) {
                        C29141Ra r2 = ((AnonymousClass1RZ) r12).A00;
                        if (!(i3 == 2 || i3 == 3)) {
                            StringBuilder sb = new StringBuilder("wamsys/registration/autoconf-request-status-unspecified; response-status ");
                            sb.append(i3);
                            sb.append(" failure-reason ");
                            sb.append(i2);
                            Log.e(sb.toString());
                        }
                        C29151Rb r02 = new C29151Rb();
                        r02.A00 = str;
                        r2.A02(r02);
                        return 0;
                    }
                }
                return 0;
            } else {
                byte[] bArr2 = (byte[]) obj4;
                C15980oF r13 = new C15980oF(A00((String) obj2, (int) j), (String) obj);
                C15990oG r03 = ((C21150wx) INSTANCE.jniCallbacks.A06.get()).A00;
                if (bArr2 == null) {
                    r03.A0b(r13);
                    return 1;
                }
                r03.A0c(r13, bArr2);
                return 1;
            }
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r6 == 7) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long jnidispatchIIOOOOOOOOO(long r16, java.lang.Object r18, java.lang.Object r19, java.lang.Object r20, java.lang.Object r21, java.lang.Object r22, java.lang.Object r23, java.lang.Object r24, java.lang.Object r25, java.lang.Object r26) {
        /*
            r4 = r20
            r2 = r26
            r3 = r25
            r5 = r24
            r12 = r19
            r11 = r18
            r15 = r23
            r13 = r21
            r14 = r22
            r8 = 0
            r0 = r16
            int r6 = (int) r0     // Catch: Exception -> 0x00bb
            java.lang.String r11 = (java.lang.String) r11     // Catch: Exception -> 0x00bb
            java.lang.String r12 = (java.lang.String) r12     // Catch: Exception -> 0x00bb
            byte[] r13 = (byte[]) r13     // Catch: Exception -> 0x00bb
            byte[] r14 = (byte[]) r14     // Catch: Exception -> 0x00bb
            byte[] r15 = (byte[]) r15     // Catch: Exception -> 0x00bb
            byte[] r5 = (byte[]) r5     // Catch: Exception -> 0x00bb
            byte[] r3 = (byte[]) r3     // Catch: Exception -> 0x00bb
            byte[] r2 = (byte[]) r2     // Catch: Exception -> 0x00bb
            X.1RK r4 = (X.AnonymousClass1RK) r4     // Catch: Exception -> 0x00bb
            X.1Qx r0 = r4.A00     // Catch: Exception -> 0x00bb
            X.AnonymousClass009.A05(r0)     // Catch: Exception -> 0x00bb
            if (r6 == 0) goto L_0x004b
            r0 = 11
            if (r6 == r0) goto L_0x0048
            r0 = 4
            if (r6 == r0) goto L_0x0045
            r0 = 5
            if (r6 == r0) goto L_0x0042
            r0 = 6
            if (r6 == r0) goto L_0x0042
            r0 = 7
            r19 = 23
            if (r6 != r0) goto L_0x004d
        L_0x0042:
            r19 = 3
            goto L_0x004d
        L_0x0045:
            r19 = 17
            goto L_0x004d
        L_0x0048:
            r19 = 19
            goto L_0x004d
        L_0x004b:
            r19 = 0
        L_0x004d:
            r17 = r3
            r18 = r2
            r16 = r5
            X.1Rc r10 = new X.1Rc     // Catch: Exception -> 0x00bb
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19)     // Catch: Exception -> 0x00bb
            X.1RL r7 = r4.A02     // Catch: Exception -> 0x00bb
            int r0 = r10.A00     // Catch: Exception -> 0x00bb
            if (r0 != 0) goto L_0x00b1
            r5 = 1
            r7.A06 = r5     // Catch: Exception -> 0x00bb
            X.1Rd r6 = r7.A0X     // Catch: Exception -> 0x00bb
            java.lang.String r0 = r10.A01     // Catch: Exception -> 0x00bb
            r6.A07(r0)     // Catch: Exception -> 0x00bb
            java.lang.String r0 = r10.A02     // Catch: Exception -> 0x00bb
            r6.A0A(r0)     // Catch: Exception -> 0x00bb
            byte[] r0 = r10.A07     // Catch: Exception -> 0x00bb
            r1 = 2
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r1)     // Catch: Exception -> 0x00bb
            r6.A09(r0)     // Catch: Exception -> 0x00bb
        L_0x0079:
            byte[] r0 = r10.A06     // Catch: Exception -> 0x00bb
            if (r0 == 0) goto L_0x0084
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r1)     // Catch: Exception -> 0x00bb
            r6.A08(r0)     // Catch: Exception -> 0x00bb
        L_0x0084:
            byte[] r2 = r10.A08     // Catch: Exception -> 0x00bb
            if (r2 == 0) goto L_0x00ab
            long r0 = java.lang.System.currentTimeMillis()     // Catch: Exception -> 0x00bb
            X.0lM r3 = new X.0lM     // Catch: Exception -> 0x00bb
            r3.<init>(r2, r0)     // Catch: Exception -> 0x00bb
            byte[] r2 = r10.A03     // Catch: Exception -> 0x00bb
            if (r2 == 0) goto L_0x00ab
            byte[] r1 = r10.A05     // Catch: Exception -> 0x00bb
            if (r1 == 0) goto L_0x00ab
            byte[] r0 = r10.A04     // Catch: Exception -> 0x00bb
            if (r0 == 0) goto L_0x00ab
            monitor-enter(r6)     // Catch: Exception -> 0x00bb
            r6.A00 = r3     // Catch: all -> 0x00a7
            r6.A0I = r2     // Catch: all -> 0x00a7
            r6.A0K = r1     // Catch: all -> 0x00a7
            r6.A0L = r0     // Catch: all -> 0x00a7
            goto L_0x00aa
        L_0x00a7:
            r0 = move-exception
            monitor-exit(r6)     // Catch: Exception -> 0x00bb
            throw r0     // Catch: Exception -> 0x00bb
        L_0x00aa:
            monitor-exit(r6)     // Catch: Exception -> 0x00bb
        L_0x00ab:
            X.0lY r0 = r7.A0V     // Catch: Exception -> 0x00bb
            X.0lQ r0 = r0.A01     // Catch: Exception -> 0x00bb
            r0.A00 = r5     // Catch: Exception -> 0x00bb
        L_0x00b1:
            X.1Qx r1 = r4.A00     // Catch: Exception -> 0x00bb
            java.lang.Integer r0 = java.lang.Integer.valueOf(r19)     // Catch: Exception -> 0x00bb
            r1.A02(r0)     // Catch: Exception -> 0x00bb
            return r8
        L_0x00bb:
            r1 = move-exception
            com.whatsapp.wamsys.JniBridge r0 = com.whatsapp.wamsys.JniBridge.INSTANCE
            X.0wz r0 = r0.jniBridgeExceptionHandler
            r0.A00(r1)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wamsys.JniBridge.jnidispatchIIOOOOOOOOO(long, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object):long");
    }

    public static long jnidispatchIO(int i, Object obj) {
        int i2;
        try {
            switch (i) {
                case 1:
                    if (((AbstractRunnableC14570le) ((AnonymousClass1RK) obj).A02).A02.isCancelled()) {
                        return 1;
                    }
                    return 0;
                case 2:
                    return 0;
                case 3:
                    i2 = ((C21130wv) INSTANCE.jniCallbacks.A07.get()).A00.A07.A01();
                    break;
                case 4:
                    i2 = ((C21140ww) INSTANCE.jniCallbacks.A0D.get()).A00.A0K().A00.A01;
                    break;
                case 5:
                    i2 = ((C21090wq) INSTANCE.jniCallbacks.A08.get()).A00.A09.A00();
                    break;
                case 6:
                    i2 = ((C21090wq) INSTANCE.jniCallbacks.A08.get()).A00.A07.A00();
                    break;
                case 7:
                    i2 = ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getTypeFromKeyValue(obj);
                    break;
                default:
                    return 0;
            }
            return (long) i2;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIOO(int i, Object obj, Object obj2) {
        if (i == 0) {
            return 1;
        }
        if (i != 1) {
            return 0;
        }
        try {
            C21080wp r1 = INSTANCE.jniCallbacks;
            C15950oC A00 = A00((String) obj, 0);
            C21110ws r6 = (C21110ws) r1.A0B.get();
            synchronized (r6.A00) {
                AnonymousClass1RX r0 = r6.A01.A0B;
                String A01 = C29201Rg.A01(A00);
                C16310on A02 = r0.A01.A02();
                C15950oC A002 = A00(A01, 0);
                C16330op r10 = A02.A03;
                String[] strArr = {A002.A02, String.valueOf(A002.A01)};
                StringBuilder sb = new StringBuilder();
                sb.append("axolotl deleted ");
                sb.append((long) r10.A01("sessions", "recipient_id = ? AND recipient_type = ?", strArr));
                sb.append(" sessions with ");
                sb.append(A002.toString());
                Log.i(sb.toString());
                A02.close();
                r6.A00(A00);
            }
            return 1;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return 0;
        }
    }

    public static long jnidispatchIOOO(Object obj, Object obj2, Object obj3) {
        long j = 0;
        try {
            byte[] bArr = (byte[]) obj3;
            AbstractC16010oI r4 = (AbstractC16010oI) obj;
            ((C21160wy) INSTANCE.jniCallbacks.A05.get()).A00.A0I.A00();
            if (r4 == null) {
                return 0;
            }
            r4.AI3(bArr);
            j = 1;
            return 1;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return j;
        }
    }

    public static Object jnidispatchOI(long j) {
        try {
            try {
                byte[] bArr = new byte[(int) j];
                SecureRandom.getInstance("SHA1PRNG").nextBytes(bArr);
                return bArr;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e2) {
            INSTANCE.jniBridgeExceptionHandler.A00(e2);
            return null;
        }
    }

    public static Object jnidispatchOIO(int i, long j, Object obj) {
        try {
            if (i == 0) {
                int i2 = (int) j;
                byte[] A01 = ((C21140ww) INSTANCE.jniCallbacks.A0D.get()).A00.A0C.A01(i2);
                if (A01 == null) {
                    StringBuilder sb = new StringBuilder("no signed prekey available with id ");
                    sb.append(i2);
                    Log.e(sb.toString());
                    return null;
                }
                try {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("axolotl loaded a signed pre key with id ");
                    sb2.append(i2);
                    Log.i(sb2.toString());
                    new C29181Re(A01);
                    return A01;
                } catch (IOException e) {
                    StringBuilder sb3 = new StringBuilder("failed to parse signed pre key record during load for id ");
                    sb3.append(i2);
                    Log.e(sb3.toString(), e);
                    return null;
                }
            } else if (i == 1) {
                int i3 = (int) j;
                AnonymousClass1RM r3 = ((C21090wq) INSTANCE.jniCallbacks.A08.get()).A00.A09;
                byte[] A04 = r3.A04(i3);
                if (A04 == null) {
                    return null;
                }
                try {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("axolotl found a pre key with id ");
                    sb4.append(i3);
                    Log.i(sb4.toString());
                    C15990oG.A01(A04, i3);
                    return A04;
                } catch (IOException e2) {
                    StringBuilder sb5 = new StringBuilder("error reading prekey ");
                    sb5.append(i3);
                    sb5.append("; deleting");
                    Log.e(sb5.toString(), e2);
                    r3.A02(i3);
                    return null;
                }
            } else if (i != 2) {
                return null;
            } else {
                int i4 = (int) j;
                C21090wq r0 = (C21090wq) INSTANCE.jniCallbacks.A08.get();
                if (i4 <= 0) {
                    return null;
                }
                List<AnonymousClass1RW> A012 = r0.A00.A09.A01();
                HashMap hashMap = new HashMap();
                for (AnonymousClass1RW r2 : A012) {
                    hashMap.put(Integer.valueOf(r2.A00), r2.A01);
                    if (hashMap.size() == i4) {
                        return hashMap;
                    }
                }
                return hashMap;
            }
        } catch (Exception e3) {
            INSTANCE.jniBridgeExceptionHandler.A00(e3);
            return null;
        }
        INSTANCE.jniBridgeExceptionHandler.A00(e3);
        return null;
    }

    public static Object jnidispatchOIOO(int i, long j, Object obj, Object obj2) {
        C14370lK r0;
        byte b;
        C29241Rk r7;
        byte[] bArr;
        try {
            if (i == 0) {
                String str = (String) obj;
                C21800y0 r15 = (C21800y0) obj2;
                switch ((int) j) {
                    case 0:
                        r0 = C14370lK.A0B;
                        break;
                    case 1:
                        r0 = C14370lK.A0I;
                        break;
                    case 2:
                        r0 = C14370lK.A05;
                        break;
                    case 3:
                        r0 = C14370lK.A08;
                        break;
                    case 4:
                        r0 = C14370lK.A0X;
                        break;
                    case 5:
                        r0 = C14370lK.A04;
                        break;
                    case 6:
                        r0 = C14370lK.A0S;
                        break;
                    case 7:
                    default:
                        r0 = null;
                        break;
                    case 8:
                        r0 = C14370lK.A0J;
                        break;
                    case 9:
                        r0 = C14370lK.A0K;
                        break;
                }
                C15660nh r2 = r15.A00;
                if (r0 != null) {
                    b = r0.A00;
                } else {
                    b = 0;
                }
                C29221Ri A08 = r2.A08(str, b, false);
                if (A08 != null) {
                    byte[] decode = Base64.decode(str, 0);
                    byte[] decode2 = Base64.decode(A08.A03, 0);
                    int A00 = C29231Rj.A00(C14370lK.A01(A08.A00, 0));
                    C16150oX r22 = A08.A02;
                    r7 = new C29241Rk(r22.A0G, A08.A04, decode, decode2, r22.A0U, A00, r22.A0B / 1000);
                } else {
                    r7 = null;
                }
                if (r7 != null) {
                    return r7.A00;
                }
                return null;
            } else if (i == 1) {
                AnonymousClass1JS A0E = ((C21130wv) INSTANCE.jniCallbacks.A07.get()).A00.A0E(A00((String) obj, (int) j));
                if (A0E != null) {
                    return A0E.A00.A00();
                }
                return null;
            } else if (i != 2) {
                return null;
            } else {
                C21110ws r5 = (C21110ws) INSTANCE.jniCallbacks.A0B.get();
                C15950oC A002 = A00((String) obj, (int) j);
                C21100wr r3 = r5.A00;
                synchronized (r3) {
                    AnonymousClass1RV A003 = r3.A00(A002);
                    if (A003 != null) {
                        bArr = A003.A00();
                    } else {
                        byte[] A03 = r5.A01.A0B.A03(A002);
                        bArr = null;
                        if (A03 == null) {
                            r3.A03(new AnonymousClass1RV(), A002);
                        } else {
                            try {
                                AnonymousClass1RV r02 = new AnonymousClass1RV(A03);
                                C15990oG.A05(r02);
                                r3.A03(r02, A002);
                                return A03;
                            } catch (IOException unused) {
                                r5.A00(A002);
                            }
                        }
                    }
                    return bArr;
                }
            }
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return null;
        }
    }

    public static Object jnidispatchOIOOO(int i, long j, Object obj, Object obj2, Object obj3) {
        try {
            if (i == 0) {
                C29301Rq A01 = ((C21120wu) INSTANCE.jniCallbacks.A0A.get()).A00.A0A.A01(new C15980oF(A00((String) obj2, (int) j), (String) obj));
                C29321Rs r4 = null;
                if (A01 != null) {
                    try {
                        byte[] bArr = A01.A01;
                        long j2 = A01.A00;
                        new C29311Rr(bArr);
                        r4 = new C29321Rs(bArr, j2);
                    } catch (IOException e) {
                        Log.e("SenderKeyStoreImpl/loadSenderKeyImpl", e);
                    }
                }
                if (r4 != null) {
                    return r4.A00;
                }
                return null;
            } else if (i == 1) {
                C29261Rm A00 = ((C21150wx) INSTANCE.jniCallbacks.A06.get()).A00.A06.A00(new C15980oF(A00((String) obj2, (int) j), (String) obj));
                C29281Ro r3 = null;
                if (A00 != null) {
                    try {
                        byte[] bArr2 = A00.A01;
                        new C29271Rn(bArr2);
                        r3 = new C29281Ro(bArr2, A00.A00);
                    } catch (IOException e2) {
                        Log.e("FastRatchetSenderKeyStoreImpl/loadFastRatchetSenderKeyImpl", e2);
                    }
                }
                if (r3 != null) {
                    return r3.A00;
                }
                return null;
            } else if (i != 2) {
                return null;
            } else {
                byte b = (byte) ((int) j);
                return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).createKeyValue((String) obj, (String) obj2, obj3, b);
            }
        } catch (Exception e3) {
            INSTANCE.jniBridgeExceptionHandler.A00(e3);
            return null;
        }
        INSTANCE.jniBridgeExceptionHandler.A00(e3);
        return null;
    }

    public static Object jnidispatchOO(int i, Object obj) {
        C29361Rw r0;
        try {
            switch (i) {
                case 0:
                    String str = (String) obj;
                    C21060wn r1 = INSTANCE.jniCallbacks.A01;
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            InetAddress[] inetAddressArr = r1.A00.A00(str).A04;
                            ArrayList arrayList = new ArrayList();
                            for (InetAddress inetAddress : inetAddressArr) {
                                arrayList.add(inetAddress.getHostAddress());
                            }
                            return arrayList;
                        } catch (UnknownHostException unused) {
                        }
                    }
                    return null;
                case 1:
                    C17200qQ r2 = INSTANCE.jniCallbacks.A00.A00;
                    synchronized (r2) {
                        r0 = r2.A02().A01;
                        if (r0 == null) {
                            throw new RuntimeException("AuthKeyStore/failed to get client static key pair");
                        }
                    }
                    byte[] bArr = r0.A01.A01;
                    int length = bArr.length;
                    if (length == 32) {
                        return bArr;
                    }
                    StringBuilder sb = new StringBuilder("AuthKeyStoreImpl/the key length is not expected/privateLength=");
                    sb.append(length);
                    Log.w(sb.toString());
                    return null;
                case 2:
                    C29331Rt A02 = ((C21130wv) INSTANCE.jniCallbacks.A07.get()).A00.A07.A02();
                    return new C29341Ru(A02.A01, A02.A00).A00;
                case 3:
                    return ((C21140ww) INSTANCE.jniCallbacks.A0D.get()).A00.A0K().A00.A02();
                case 4:
                    return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getTagFromProtocolTreeNode(obj);
                case 5:
                    Object[] attributesFromProtocolTreeNode = ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getAttributesFromProtocolTreeNode(obj);
                    if (attributesFromProtocolTreeNode != null) {
                        return Arrays.asList(attributesFromProtocolTreeNode);
                    }
                    return null;
                case 6:
                    Object[] childrenFromProtocolTreeNode = ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getChildrenFromProtocolTreeNode(obj);
                    if (childrenFromProtocolTreeNode != null) {
                        return Arrays.asList(childrenFromProtocolTreeNode);
                    }
                    return null;
                case 7:
                    return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getDataFromProtocolTreeNode(obj);
                case 8:
                    return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getKeyFromKeyValue(obj);
                case 9:
                    return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).getValueStringFromKeyValue(obj);
                case 10:
                    return ((ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get()).createNewJid((String) obj);
                default:
                    return null;
            }
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return null;
        }
    }

    public static Object jnidispatchOOO(Object obj, Object obj2) {
        try {
            Map map = (Map) obj2;
            C21110ws r4 = (C21110ws) INSTANCE.jniCallbacks.A0B.get();
            if (map == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Integer num : map.keySet()) {
                int intValue = num.intValue();
                Object obj3 = map.get(Integer.valueOf(intValue));
                AnonymousClass009.A05(obj3);
                arrayList.add(A00((String) obj3, intValue));
            }
            Set<C15950oC> A0Q = r4.A01.A0Q(arrayList);
            HashMap hashMap = new HashMap();
            for (C15950oC r2 : A0Q) {
                hashMap.put(Integer.valueOf(r2.A00), r2.A02);
            }
            return hashMap;
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return null;
        }
    }

    public static Object jnidispatchOOOOO(Object obj, Object obj2, Object obj3, Object obj4) {
        try {
            String str = (String) obj;
            List list = (List) obj3;
            List list2 = (List) obj4;
            byte[] bArr = (byte[]) obj2;
            ProtocolJniHelper protocolJniHelper = (ProtocolJniHelper) INSTANCE.jniCallbacks.A09.get();
            Object[] objArr = null;
            Object[] array = list != null ? list.toArray() : null;
            if (list2 != null) {
                objArr = list2.toArray();
            }
            return protocolJniHelper.createProtocolTreeNode(str, array, objArr, bArr);
        } catch (Exception e) {
            INSTANCE.jniBridgeExceptionHandler.A00(e);
            return null;
        }
    }

    public static void setDependencies(C19980v1 r3) {
        synchronized (JniBridge.class) {
            if (DEPENDENCIES == null) {
                DEPENDENCIES = r3;
            } else {
                throw new IllegalStateException("JniBridgeDependencies are already set. Can't override them.");
            }
        }
    }
}
