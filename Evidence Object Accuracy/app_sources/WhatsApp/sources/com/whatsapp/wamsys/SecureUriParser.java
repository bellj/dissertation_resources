package com.whatsapp.wamsys;

import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.net.Uri;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

/* loaded from: classes2.dex */
public class SecureUriParser {
    public static Uri createHierAndroidUriFromJavaUri(URI uri) {
        return C12960it.A0C(uri);
    }

    public static URI createHierJavaUriFromAndroidUri(String str, Uri uri) {
        try {
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            URI createHierJavaUriFromAndroidUriSecondTry = createHierJavaUriFromAndroidUriSecondTry(uri);
            if (createHierJavaUriFromAndroidUriSecondTry != null) {
                return createHierJavaUriFromAndroidUriSecondTry;
            }
            throw createOnParsingJavaUriFailException(str, e);
        }
    }

    public static URI createHierJavaUriFromAndroidUriSecondTry(Uri uri) {
        try {
            URI uri2 = new URI(uri.toString());
            if (shouldSkipAuthority(uri2, uri)) {
                return uri2;
            }
            return null;
        } catch (URISyntaxException unused) {
            return null;
        }
    }

    public static SecurityException createOnParsingJavaUriFailException(String str, URISyntaxException uRISyntaxException) {
        Locale locale = Locale.US;
        Object[] A1a = C12980iv.A1a();
        A1a[0] = str;
        A1a[1] = uRISyntaxException.getMessage();
        return new SecurityException(String.format(locale, "Parsing url %s caused exception: %s.", A1a));
    }

    public static Uri createOpaqueAndroidUriFromJavaUri(URI uri) {
        return new Uri.Builder().scheme(uri.getScheme()).encodedOpaquePart(uri.getRawSchemeSpecificPart()).encodedFragment(uri.getRawFragment()).build();
    }

    public static URI createOpaqueJavaUriFromAndroidUri(String str, Uri uri) {
        try {
            return new URI(uri.getScheme(), uri.getSchemeSpecificPart(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw createOnParsingJavaUriFailException(str, e);
        }
    }

    public static Uri parseEncodedRFC2396(String str) {
        Uri parse = Uri.parse(str);
        if (!validateScheme(parse)) {
            return parseEncodedRFC2396Reverse(str);
        }
        validateMightThrow(str, parse);
        return parse;
    }

    public static Uri parseEncodedRFC2396Reverse(String str) {
        try {
            URI uri = new URI(str);
            if (uri.isOpaque()) {
                Uri createOpaqueAndroidUriFromJavaUri = createOpaqueAndroidUriFromJavaUri(uri);
                urisMatchMightThrowOnOpaque(str, uri, createOpaqueAndroidUriFromJavaUri);
                return createOpaqueAndroidUriFromJavaUri;
            }
            Uri A0C = C12960it.A0C(uri);
            urisMatchMightThrowOnHier(str, uri, A0C, false);
            return A0C;
        } catch (URISyntaxException e) {
            throw createOnParsingJavaUriFailException(str, e);
        }
    }

    public static boolean shouldSkipAuthority(URI uri, Uri uri2) {
        String host = uri2.getHost();
        return uri.getHost() == null && host != null && host.contains("_");
    }

    public static boolean stringEquals(String str, String str2) {
        if (str == null || str.equals("")) {
            return str2 == null || str2.equals("");
        }
        return str.equals(str2);
    }

    public static void urisMatchMightThrowOnHier(String str, URI uri, Uri uri2, boolean z) {
        boolean stringEquals = stringEquals(uri.getScheme(), uri2.getScheme());
        boolean stringEquals2 = stringEquals(uri.getAuthority(), uri2.getAuthority());
        boolean stringEquals3 = stringEquals(uri.getPath(), uri2.getPath());
        String str2 = "";
        if (!stringEquals) {
            str2 = C12960it.A0d(C12970iu.A0o(uri2, uri), C12960it.A0j(str2));
        }
        if (!z && !stringEquals2) {
            str2 = C12960it.A0d(String.format(Locale.US, "javaUri authority: \"%s\". androidUri authority: \"%s\".", uri.getAuthority(), uri2.getAuthority()), C12960it.A0j(str2));
        }
        if (!stringEquals3) {
            str2 = C12960it.A0d(String.format(Locale.US, "javaUri path: \"%s\". androidUri path: \"%s\".", uri.getPath(), uri2.getPath()), C12960it.A0j(str2));
        }
        if (!stringEquals || !stringEquals2 || !stringEquals3) {
            throw C12970iu.A0m(str2, str, Locale.US, C12980iv.A1b(uri, uri2));
        }
    }

    public static void urisMatchMightThrowOnOpaque(String str, URI uri, Uri uri2) {
        boolean stringEquals = stringEquals(uri.getScheme(), uri2.getScheme());
        boolean stringEquals2 = stringEquals(uri.getSchemeSpecificPart(), uri2.getSchemeSpecificPart());
        if (!stringEquals || !stringEquals2) {
            String str2 = "";
            if (!stringEquals) {
                str2 = C12960it.A0d(C12970iu.A0o(uri2, uri), C12960it.A0j(str2));
            }
            if (!stringEquals2) {
                str2 = C12960it.A0d(String.format(Locale.US, "javaUri opaque part: \"%s\". androidUri opaque part: \"%s\".", uri.getSchemeSpecificPart(), uri2.getSchemeSpecificPart()), C12960it.A0j(str2));
            }
            throw C12970iu.A0m(str2, str, Locale.US, C12980iv.A1b(uri, uri2));
        }
    }

    public static void validateMightThrow(String str, Uri uri) {
        if (uri.isOpaque()) {
            urisMatchMightThrowOnOpaque(str, createOpaqueJavaUriFromAndroidUri(str, uri), uri);
            return;
        }
        URI createHierJavaUriFromAndroidUri = createHierJavaUriFromAndroidUri(str, uri);
        urisMatchMightThrowOnHier(str, createHierJavaUriFromAndroidUri, uri, shouldSkipAuthority(createHierJavaUriFromAndroidUri, uri));
    }

    public static boolean validateScheme(Uri uri) {
        if (uri.getScheme() == null) {
            return true;
        }
        return uri.getScheme().matches("([a-zA-Z][a-zA-Z0-9+.-]*)?");
    }
}
