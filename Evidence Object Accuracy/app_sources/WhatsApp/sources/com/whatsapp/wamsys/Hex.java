package com.whatsapp.wamsys;

/* loaded from: classes3.dex */
public class Hex {
    public static final char[] FIRST_CHAR;
    public static final char[] HEX_DIGITS;
    public static final char[] SECOND_CHAR;

    static {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        HEX_DIGITS = cArr;
        char[] cArr2 = new char[256];
        FIRST_CHAR = cArr2;
        char[] cArr3 = new char[256];
        SECOND_CHAR = cArr3;
        int i = 0;
        do {
            cArr2[i] = cArr[(i >> 4) & 15];
            cArr3[i] = cArr[i & 15];
            i++;
        } while (i < 256);
    }

    public static String encodeHex(byte[] bArr, boolean z) {
        int length = bArr.length;
        char[] cArr = new char[length << 1];
        int i = 0;
        for (byte b : bArr) {
            int i2 = b & 255;
            if (i2 == 0 && z) {
                break;
            }
            int i3 = i + 1;
            cArr[i] = FIRST_CHAR[i2];
            i = i3 + 1;
            cArr[i3] = SECOND_CHAR[i2];
        }
        return new String(cArr, 0, i);
    }
}
