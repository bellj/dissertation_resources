package com.whatsapp.catalogcategory.view;

import X.AbstractC001200n;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass1J7;
import X.AnonymousClass1WK;
import X.AnonymousClass2E5;
import X.AnonymousClass5TN;
import X.AnonymousClass5TO;
import X.C16700pc;
import X.C37071lG;
import X.C44741zT;
import android.graphics.Bitmap;

/* loaded from: classes2.dex */
public final class CategoryThumbnailLoader implements AnonymousClass054 {
    public final AbstractC001200n A00;
    public final C37071lG A01;

    public CategoryThumbnailLoader(AbstractC001200n r2, C37071lG r3) {
        this.A01 = r3;
        this.A00 = r2;
        r2.ADr().A00(this);
    }

    public final void A00(C44741zT r8, AnonymousClass1WK r9, AnonymousClass1WK r10, AnonymousClass1J7 r11) {
        this.A01.A01(null, r8, new AnonymousClass5TN() { // from class: X.53g
            @Override // X.AnonymousClass5TN
            public final void AML(C68203Um r2) {
                AnonymousClass1WK.this.AJ3();
            }
        }, new AnonymousClass5TO() { // from class: X.53j
            @Override // X.AnonymousClass5TO
            public final void ARz(C68203Um r2) {
                AnonymousClass1WK.this.AJ3();
            }
        }, new AnonymousClass2E5() { // from class: X.53o
            @Override // X.AnonymousClass2E5
            public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                AnonymousClass1J7 r1 = AnonymousClass1J7.this;
                C16700pc.A0E(bitmap, 2);
                r1.AJ4(bitmap);
            }
        }, 2);
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
        C16700pc.A0E(r3, 1);
        if (r3.ordinal() == 5) {
            this.A01.A00();
            this.A00.ADr().A01(this);
        }
    }
}
