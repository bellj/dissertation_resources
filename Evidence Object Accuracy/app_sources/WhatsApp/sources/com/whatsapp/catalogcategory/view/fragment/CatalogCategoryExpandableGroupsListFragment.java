package com.whatsapp.catalogcategory.view.fragment;

import X.AbstractC16710pd;
import X.AnonymousClass009;
import X.AnonymousClass017;
import X.AnonymousClass12P;
import X.AnonymousClass1WL;
import X.AnonymousClass410;
import X.AnonymousClass4JB;
import X.C113795Ja;
import X.C12960it;
import X.C16700pc;
import X.C71933dk;
import X.C74093hI;
import X.C849740q;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.R;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;
import com.whatsapp.catalogcategory.view.fragment.CatalogCategoryExpandableGroupsListFragment;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public final class CatalogCategoryExpandableGroupsListFragment extends Hilt_CatalogCategoryExpandableGroupsListFragment {
    public int A00 = -1;
    public ExpandableListView A01;
    public AnonymousClass12P A02;
    public AnonymousClass4JB A03;
    public C74093hI A04;
    public UserJid A05;
    public String A06;
    public final AbstractC16710pd A07 = new AnonymousClass1WL(new C71933dk(this));
    public final AbstractC16710pd A08 = new AnonymousClass1WL(new C113795Ja(this));

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.fragment_catalog_category_expandable_groups, viewGroup, false);
        C16700pc.A0B(inflate);
        this.A01 = (ExpandableListView) C16700pc.A02(inflate, R.id.expandable_list_catalog_category);
        C74093hI r1 = new C74093hI((CategoryThumbnailLoader) this.A07.getValue());
        this.A04 = r1;
        ExpandableListView expandableListView = this.A01;
        if (expandableListView == null) {
            throw C16700pc.A06("expandableListView");
        }
        expandableListView.setAdapter(r1);
        ExpandableListView expandableListView2 = this.A01;
        if (expandableListView2 == null) {
            throw C16700pc.A06("expandableListView");
        }
        expandableListView2.setOnChildClickListener(new ExpandableListView.OnChildClickListener() { // from class: X.4pC
            @Override // android.widget.ExpandableListView.OnChildClickListener
            public final boolean onChildClick(ExpandableListView expandableListView3, View view, int i, int i2, long j) {
                AnonymousClass411 r3;
                CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = CatalogCategoryExpandableGroupsListFragment.this;
                C16700pc.A0E(catalogCategoryExpandableGroupsListFragment, 0);
                CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel = (CatalogCategoryGroupsViewModel) catalogCategoryExpandableGroupsListFragment.A08.getValue();
                Object A01 = catalogCategoryGroupsViewModel.A00.A01();
                if (!(A01 instanceof AnonymousClass411) || (r3 = (AnonymousClass411) A01) == null) {
                    return true;
                }
                String str = ((AnonymousClass2wS) r3.A00.get(i)).A00.A01;
                C16700pc.A0B(str);
                AnonymousClass2wR r12 = (AnonymousClass2wR) ((List) C17530qx.A00(r3.A01, str)).get(i2);
                AnonymousClass4SX r0 = r12.A00;
                UserJid userJid = r12.A01;
                C25811Ax r32 = catalogCategoryGroupsViewModel.A04;
                String str2 = r0.A01;
                r32.A01(userJid, str2, 3, 3, i2, r0.A04);
                C27691It r2 = catalogCategoryGroupsViewModel.A06;
                C16700pc.A0B(str2);
                String str3 = r0.A02;
                C16700pc.A0B(str3);
                r2.A0B(new AnonymousClass414(userJid, str2, str3, 3));
                return true;
            }
        });
        ExpandableListView expandableListView3 = this.A01;
        if (expandableListView3 == null) {
            throw C16700pc.A06("expandableListView");
        }
        expandableListView3.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() { // from class: X.4pD
            @Override // android.widget.ExpandableListView.OnGroupClickListener
            public final boolean onGroupClick(ExpandableListView expandableListView4, View view, int i, long j) {
                CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = CatalogCategoryExpandableGroupsListFragment.this;
                C16700pc.A0E(catalogCategoryExpandableGroupsListFragment, 0);
                C74093hI r0 = catalogCategoryExpandableGroupsListFragment.A04;
                if (r0 == null) {
                    throw C16700pc.A06("expandableListAdapter");
                } else if (r0.getGroupType(i) == 3) {
                    CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel = (CatalogCategoryGroupsViewModel) catalogCategoryExpandableGroupsListFragment.A08.getValue();
                    Object A01 = catalogCategoryGroupsViewModel.A00.A01();
                    if (A01 != null) {
                        AnonymousClass2wR r12 = (AnonymousClass2wR) ((AnonymousClass411) A01).A00.get(i);
                        AnonymousClass4SX r02 = r12.A00;
                        UserJid userJid = r12.A01;
                        C25811Ax r3 = catalogCategoryGroupsViewModel.A04;
                        String str = r02.A01;
                        r3.A01(userJid, str, 2, 3, i, r02.A04);
                        C27691It r2 = catalogCategoryGroupsViewModel.A06;
                        C16700pc.A0B(str);
                        String str2 = r02.A02;
                        C16700pc.A0B(str2);
                        r2.A0B(new AnonymousClass414(userJid, str, str2, 2));
                        return true;
                    }
                    throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupListState.CategoryGroupsWithChildItems");
                } else {
                    int i2 = catalogCategoryExpandableGroupsListFragment.A00;
                    if (i2 != i) {
                        if (i2 != -1) {
                            ExpandableListView expandableListView5 = catalogCategoryExpandableGroupsListFragment.A01;
                            if (expandableListView5 == null) {
                                throw C16700pc.A06("expandableListView");
                            }
                            expandableListView5.collapseGroup(i2);
                        }
                        AbstractC16710pd r32 = catalogCategoryExpandableGroupsListFragment.A08;
                        if (C16700pc.A0O(((CatalogCategoryGroupsViewModel) r32.getValue()).A02.A01(), Boolean.TRUE)) {
                            C53312dp r5 = new C53312dp(catalogCategoryExpandableGroupsListFragment.A01());
                            r5.A06(R.string.catalog_categories_no_network_dialog_message);
                            r5.A0E(catalogCategoryExpandableGroupsListFragment.A0G(), new IDxObserverShape4S0100000_2_I1(catalogCategoryExpandableGroupsListFragment, 24), R.string.catalog_categories_no_network_dialog_button);
                            r5.A05();
                            return true;
                        }
                        CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel2 = (CatalogCategoryGroupsViewModel) r32.getValue();
                        AnonymousClass017 r13 = catalogCategoryGroupsViewModel2.A00;
                        if (r13.A01() instanceof AnonymousClass411) {
                            Object A012 = r13.A01();
                            if (A012 != null) {
                                AnonymousClass2wS r14 = (AnonymousClass2wS) ((AnonymousClass411) A012).A00.get(i);
                                AnonymousClass4SX r03 = r14.A00;
                                catalogCategoryGroupsViewModel2.A04.A01(r14.A01, r03.A01, 2, 3, i, r03.A04);
                            } else {
                                throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupListState.CategoryGroupsWithChildItems");
                            }
                        }
                        ExpandableListView expandableListView6 = catalogCategoryExpandableGroupsListFragment.A01;
                        if (expandableListView6 == null) {
                            throw C16700pc.A06("expandableListView");
                        }
                        expandableListView6.smoothScrollToPosition(i);
                        ExpandableListView expandableListView7 = catalogCategoryExpandableGroupsListFragment.A01;
                        if (expandableListView7 == null) {
                            throw C16700pc.A06("expandableListView");
                        }
                        expandableListView7.expandGroup(i);
                        return true;
                    }
                    ExpandableListView expandableListView8 = catalogCategoryExpandableGroupsListFragment.A01;
                    if (expandableListView8 == null) {
                        throw C16700pc.A06("expandableListView");
                    }
                    expandableListView8.collapseGroup(i);
                    return true;
                }
            }
        });
        ExpandableListView expandableListView4 = this.A01;
        if (expandableListView4 == null) {
            throw C16700pc.A06("expandableListView");
        }
        expandableListView4.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() { // from class: X.4pF
            @Override // android.widget.ExpandableListView.OnGroupExpandListener
            public final void onGroupExpand(int i) {
                CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = CatalogCategoryExpandableGroupsListFragment.this;
                C16700pc.A0E(catalogCategoryExpandableGroupsListFragment, 0);
                catalogCategoryExpandableGroupsListFragment.A00 = i;
            }
        });
        ExpandableListView expandableListView5 = this.A01;
        if (expandableListView5 == null) {
            throw C16700pc.A06("expandableListView");
        }
        expandableListView5.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() { // from class: X.4pE
            @Override // android.widget.ExpandableListView.OnGroupCollapseListener
            public final void onGroupCollapse(int i) {
                CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = CatalogCategoryExpandableGroupsListFragment.this;
                C16700pc.A0E(catalogCategoryExpandableGroupsListFragment, 0);
                catalogCategoryExpandableGroupsListFragment.A00 = -1;
            }
        });
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        int i = this.A00;
        if (i != -1) {
            ExpandableListView expandableListView = this.A01;
            if (expandableListView == null) {
                throw C16700pc.A06("expandableListView");
            }
            expandableListView.expandGroup(i);
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        String string = A03().getString("parent_category_id");
        AnonymousClass009.A05(string);
        C16700pc.A0B(string);
        this.A06 = string;
        Parcelable parcelable = A03().getParcelable("category_biz_id");
        AnonymousClass009.A05(parcelable);
        C16700pc.A0B(parcelable);
        this.A05 = (UserJid) parcelable;
        CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel = (CatalogCategoryGroupsViewModel) this.A08.getValue();
        String str = this.A06;
        if (str == null) {
            throw C16700pc.A06("categoryParentId");
        }
        UserJid userJid = this.A05;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        AnonymousClass017 r3 = (AnonymousClass017) catalogCategoryGroupsViewModel.A08.getValue();
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        do {
            i++;
            A0l.add(new C849740q());
        } while (i < 5);
        r3.A0B(new AnonymousClass410(A0l));
        catalogCategoryGroupsViewModel.A07.Ab2(new RunnableBRunnable0Shape1S1200000_I1(userJid, catalogCategoryGroupsViewModel, str, 12));
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        AbstractC16710pd r4 = this.A08;
        ((CatalogCategoryGroupsViewModel) r4.getValue()).A00.A05(A0G(), new IDxObserverShape4S0100000_2_I1(this, 25));
        ((CatalogCategoryGroupsViewModel) r4.getValue()).A01.A05(A0G(), new IDxObserverShape3S0100000_1_I1(this, 52));
        ((CatalogCategoryGroupsViewModel) r4.getValue()).A02.A05(A0G(), new IDxObserverShape4S0100000_2_I1(this, 26));
    }
}
