package com.whatsapp.catalogcategory.view.activity;

import X.AbstractActivityC60272wQ;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass3AT;
import X.AnonymousClass4A0;
import X.C004902f;
import X.C12970iu;
import X.C12980iv;
import X.C16700pc;
import X.C21770xx;
import X.C48882Ih;
import android.os.Bundle;
import android.view.Menu;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class CatalogAllCategoryActivity extends AbstractActivityC60272wQ {
    public boolean A00;

    public CatalogAllCategoryActivity() {
        this(0);
    }

    public CatalogAllCategoryActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 40);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC60272wQ) this).A00 = (C48882Ih) A1L.A0Q.get();
            ((AbstractActivityC60272wQ) this).A01 = (C21770xx) A1M.A2s.get();
            ((AbstractActivityC60272wQ) this).A02 = C12980iv.A0Z(A1M);
        }
    }

    @Override // X.AbstractActivityC60272wQ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_catalog_category);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0I(getString(R.string.catalog_categories_all_category));
        }
        if (bundle == null) {
            String stringExtra = getIntent().getStringExtra("category_parent_id");
            AnonymousClass009.A05(stringExtra);
            C004902f A0P = C12970iu.A0P(this);
            C16700pc.A0B(stringExtra);
            A0P.A07(AnonymousClass3AT.A00(AnonymousClass4A0.A01, A2e(), stringExtra), R.id.container);
            A0P.A01();
        }
    }

    @Override // X.AbstractActivityC60272wQ, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C16700pc.A0E(menu, 0);
        getMenuInflater().inflate(R.menu.catalog_category_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
