package com.whatsapp.catalogcategory.view.viewmodel;

import X.AbstractC14440lR;
import X.AbstractC16710pd;
import X.AnonymousClass015;
import X.AnonymousClass017;
import X.AnonymousClass2ET;
import X.AnonymousClass4Yq;
import X.C113755Iw;
import X.C113765Ix;
import X.C12990iw;
import X.C13000ix;
import X.C16700pc;
import X.C25811Ax;
import X.C27691It;

/* loaded from: classes2.dex */
public final class CatalogAllCategoryViewModel extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass017 A01;
    public final AnonymousClass017 A02;
    public final C25811Ax A03;
    public final AnonymousClass2ET A04;
    public final C27691It A05;
    public final AbstractC14440lR A06;
    public final AbstractC16710pd A07;
    public final AbstractC16710pd A08;

    public CatalogAllCategoryViewModel(C25811Ax r2, AnonymousClass2ET r3, AbstractC14440lR r4) {
        C16700pc.A0E(r4, 1);
        C16700pc.A0E(r2, 3);
        this.A06 = r4;
        this.A04 = r3;
        this.A03 = r2;
        AbstractC16710pd A00 = AnonymousClass4Yq.A00(new C113765Ix());
        this.A08 = A00;
        this.A01 = C12990iw.A0P(A00);
        AbstractC16710pd A002 = AnonymousClass4Yq.A00(new C113755Iw());
        this.A07 = A002;
        this.A00 = C12990iw.A0P(A002);
        C27691It A03 = C13000ix.A03();
        this.A05 = A03;
        this.A02 = A03;
    }
}
