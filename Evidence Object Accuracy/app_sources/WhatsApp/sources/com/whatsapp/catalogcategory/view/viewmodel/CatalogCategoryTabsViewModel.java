package com.whatsapp.catalogcategory.view.viewmodel;

import X.AbstractC14440lR;
import X.AbstractC16710pd;
import X.AnonymousClass015;
import X.AnonymousClass017;
import X.AnonymousClass2ET;
import X.AnonymousClass4Yq;
import X.C113785Iz;
import X.C16700pc;
import X.C25811Ax;

/* loaded from: classes2.dex */
public final class CatalogCategoryTabsViewModel extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final C25811Ax A01;
    public final AnonymousClass2ET A02;
    public final AbstractC14440lR A03;
    public final AbstractC16710pd A04;

    public CatalogCategoryTabsViewModel(C25811Ax r2, AnonymousClass2ET r3, AbstractC14440lR r4) {
        C16700pc.A0E(r4, 1);
        C16700pc.A0E(r2, 3);
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        AbstractC16710pd A00 = AnonymousClass4Yq.A00(new C113785Iz());
        this.A04 = A00;
        this.A00 = (AnonymousClass017) A00.getValue();
    }
}
