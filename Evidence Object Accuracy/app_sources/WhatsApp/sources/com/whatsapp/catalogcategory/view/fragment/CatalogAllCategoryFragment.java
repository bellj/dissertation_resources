package com.whatsapp.catalogcategory.view.fragment;

import X.AbstractC16710pd;
import X.AnonymousClass017;
import X.AnonymousClass1J7;
import X.AnonymousClass4A0;
import X.AnonymousClass4JB;
import X.AnonymousClass4Yq;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C16700pc;
import X.C71913di;
import X.C71923dj;
import X.C74793in;
import X.C849740q;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape1S1300000_I1;
import com.whatsapp.R;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public final class CatalogAllCategoryFragment extends Hilt_CatalogAllCategoryFragment {
    public RecyclerView A00;
    public AnonymousClass4JB A01;
    public C74793in A02;
    public final AbstractC16710pd A03 = AnonymousClass4Yq.A00(new C71913di(this));
    public final AbstractC16710pd A04 = AnonymousClass4Yq.A00(new C71923dj(this));

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View A01 = C16700pc.A01(layoutInflater, viewGroup, R.layout.fragment_all_category_list);
        RecyclerView recyclerView = (RecyclerView) C16700pc.A02(A01, R.id.list_all_category);
        recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(1));
        recyclerView.A0h = true;
        this.A00 = recyclerView;
        C74793in r1 = new C74793in((CategoryThumbnailLoader) this.A03.getValue(), new AnonymousClass1J7(C16700pc.A05(this.A04)) { // from class: X.3da
            @Override // X.AnonymousClass1J7
            public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
                String str;
                UserJid userJid;
                C27691It r2;
                int i;
                AnonymousClass4K7 r12 = (AnonymousClass4K7) obj;
                C16700pc.A0E(r12, 0);
                CatalogAllCategoryViewModel catalogAllCategoryViewModel = (CatalogAllCategoryViewModel) this.receiver;
                if (r12 instanceof AnonymousClass2wU) {
                    AnonymousClass2wU r122 = (AnonymousClass2wU) r12;
                    AnonymousClass4SX r3 = r122.A00;
                    Iterable iterable = (Iterable) C12990iw.A0P(catalogAllCategoryViewModel.A07).A01();
                    if (iterable != null) {
                        i = 0;
                        for (Object obj2 : iterable) {
                            int i2 = i + 1;
                            if (i >= 0) {
                                AnonymousClass4K7 r13 = (AnonymousClass4K7) obj2;
                                if ((r13 instanceof AnonymousClass2wU) && C16700pc.A0O(((AnonymousClass2wU) r13).A00.A01, r3.A01)) {
                                    break;
                                }
                                i = i2;
                            } else {
                                throw new ArithmeticException("Index overflow has happened.");
                            }
                        }
                    }
                    i = -1;
                    C25811Ax r4 = catalogAllCategoryViewModel.A03;
                    userJid = r122.A01;
                    str = r3.A01;
                    boolean z = r3.A04;
                    r4.A01(userJid, str, 1, 2, i, z);
                    r2 = catalogAllCategoryViewModel.A05;
                    if (z) {
                        C16700pc.A0B(str);
                        String str2 = r3.A02;
                        C16700pc.A0B(str2);
                        r2.A0B(new AnonymousClass414(userJid, str, str2, 1));
                        return AnonymousClass1WZ.A00;
                    }
                    C16700pc.A0B(str);
                    r2.A0B(new AnonymousClass413(userJid, str));
                    return AnonymousClass1WZ.A00;
                }
                if (r12 instanceof AnonymousClass2wT) {
                    r2 = catalogAllCategoryViewModel.A05;
                    AnonymousClass2wT r123 = (AnonymousClass2wT) r12;
                    str = r123.A00.A01;
                    C16700pc.A0B(str);
                    userJid = r123.A01;
                    r2.A0B(new AnonymousClass413(userJid, str));
                }
                return AnonymousClass1WZ.A00;
            }
        });
        this.A02 = r1;
        RecyclerView recyclerView2 = this.A00;
        if (recyclerView2 == null) {
            throw C16700pc.A06("recyclerView");
        }
        recyclerView2.setAdapter(r1);
        return A01;
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        String string = A03().getString("parent_category_id");
        Parcelable parcelable = A03().getParcelable("category_biz_id");
        Bundle A03 = A03();
        AnonymousClass4A0 r2 = AnonymousClass4A0.A01;
        String string2 = A03.getString("category_display_context", "CATALOG_CATEGORY_FLOW");
        C16700pc.A0B(string2);
        AnonymousClass4A0 valueOf = AnonymousClass4A0.valueOf(string2);
        if (string == null || parcelable == null) {
            throw C12970iu.A0f("Required categoryParentId and bizJid not found");
        }
        CatalogAllCategoryViewModel catalogAllCategoryViewModel = (CatalogAllCategoryViewModel) this.A04.getValue();
        int i = 0;
        C16700pc.A0E(valueOf, 2);
        C12960it.A1A(C12990iw.A0P(catalogAllCategoryViewModel.A08), 0);
        if (valueOf == r2) {
            AnonymousClass017 A0P = C12990iw.A0P(catalogAllCategoryViewModel.A07);
            ArrayList A0l = C12960it.A0l();
            do {
                i++;
                A0l.add(new C849740q());
            } while (i < 5);
            A0P.A0B(A0l);
        }
        catalogAllCategoryViewModel.A06.Ab2(new RunnableBRunnable0Shape1S1300000_I1(catalogAllCategoryViewModel, parcelable, valueOf, string, 4));
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        AbstractC16710pd r3 = this.A04;
        C12970iu.A1P(A0G(), ((CatalogAllCategoryViewModel) r3.getValue()).A01, this, 23);
        C12960it.A19(A0G(), ((CatalogAllCategoryViewModel) r3.getValue()).A00, this, 51);
        C12960it.A19(A0G(), ((CatalogAllCategoryViewModel) r3.getValue()).A02, this, 50);
    }
}
