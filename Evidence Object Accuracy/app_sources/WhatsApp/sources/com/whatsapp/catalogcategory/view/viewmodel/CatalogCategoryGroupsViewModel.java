package com.whatsapp.catalogcategory.view.viewmodel;

import X.AbstractC14440lR;
import X.AbstractC16710pd;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass1WL;
import X.AnonymousClass2ET;
import X.C113775Iy;
import X.C12980iv;
import X.C16700pc;
import X.C25811Ax;
import X.C27691It;

/* loaded from: classes3.dex */
public final class CatalogCategoryGroupsViewModel extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass017 A01;
    public final AnonymousClass017 A02;
    public final AnonymousClass016 A03;
    public final C25811Ax A04;
    public final AnonymousClass2ET A05;
    public final C27691It A06;
    public final AbstractC14440lR A07;
    public final AbstractC16710pd A08;

    public CatalogCategoryGroupsViewModel(C25811Ax r3, AnonymousClass2ET r4, AbstractC14440lR r5) {
        C16700pc.A0E(r5, 1);
        C16700pc.A0E(r3, 3);
        this.A07 = r5;
        this.A05 = r4;
        this.A04 = r3;
        AnonymousClass1WL r0 = new AnonymousClass1WL(new C113775Iy());
        this.A08 = r0;
        this.A00 = (AnonymousClass017) r0.getValue();
        C27691It r02 = new C27691It();
        this.A06 = r02;
        this.A01 = r02;
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A03 = A0T;
        this.A02 = A0T;
    }
}
