package com.whatsapp.voicerecorder;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass0MU;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12980iv;
import X.C73603gV;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.SeekBar;
import androidx.appcompat.widget.AppCompatSeekBar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class VoiceNoteSeekBar extends AppCompatSeekBar implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public SeekBar.OnSeekBarChangeListener A06;
    public AnonymousClass0MU A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final Paint A0D;
    public final RectF A0E;

    public VoiceNoteSeekBar(Context context) {
        super(context);
        if (!this.A0B) {
            this.A0B = true;
            generatedComponent();
        }
        this.A0D = C12960it.A0A();
        this.A0E = C12980iv.A0K();
        this.A04 = 20;
        this.A05 = 10;
        A00(context, null);
    }

    public VoiceNoteSeekBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A0D = C12960it.A0A();
        this.A0E = C12980iv.A0K();
        this.A04 = 20;
        this.A05 = 10;
        A00(context, attributeSet);
    }

    public VoiceNoteSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0B) {
            this.A0B = true;
            generatedComponent();
        }
        this.A0D = C12960it.A0A();
        this.A0E = C12980iv.A0K();
        this.A04 = 20;
        this.A05 = 10;
        A00(context, attributeSet);
    }

    public VoiceNoteSeekBar(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A0B) {
            this.A0B = true;
            generatedComponent();
        }
    }

    public final void A00(Context context, AttributeSet attributeSet) {
        this.A03 = ViewConfiguration.get(context).getScaledTouchSlop();
        this.A02 = AnonymousClass00T.A00(context, R.color.voice_note_seekbar_progress);
        this.A01 = AnonymousClass00T.A00(context, R.color.voice_note_seekbar_background);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0Q);
            this.A04 = obtainStyledAttributes.getDimensionPixelSize(2, this.A04);
            this.A05 = obtainStyledAttributes.getDimensionPixelSize(3, this.A05);
            this.A02 = obtainStyledAttributes.getInteger(1, this.A02);
            this.A01 = obtainStyledAttributes.getInteger(0, this.A01);
            obtainStyledAttributes.recycle();
        }
        this.A07 = new AnonymousClass0MU(context, new C73603gV(this));
    }

    public final void A01(MotionEvent motionEvent) {
        float f;
        int width = getWidth();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int i = (width - paddingLeft) - paddingRight;
        int x = (int) motionEvent.getX();
        if (x < paddingLeft) {
            f = 0.0f;
        } else {
            int i2 = width - paddingRight;
            f = 1.0f;
            if (x <= i2) {
                f = ((float) (x - paddingLeft)) / ((float) i);
            }
        }
        int max = (int) ((f * ((float) getMax())) + 0.0f);
        setProgress(max);
        SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = this.A06;
        if (onSeekBarChangeListener != null) {
            onSeekBarChangeListener.onProgressChanged(null, max, true);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public int getProgressColor() {
        return this.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0079, code lost:
        if (r3 != 0) goto L_0x007b;
     */
    @Override // androidx.appcompat.widget.AppCompatSeekBar, android.widget.AbsSeekBar, android.widget.ProgressBar, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onDraw(android.graphics.Canvas r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            int r4 = r12.getWidth()     // Catch: all -> 0x00ab
            int r2 = r12.getPaddingLeft()     // Catch: all -> 0x00ab
            int r0 = r12.getPaddingRight()     // Catch: all -> 0x00ab
            int r4 = r4 - r2
            int r4 = r4 - r0
            boolean r0 = r12.isPressed()     // Catch: all -> 0x00ab
            if (r0 == 0) goto L_0x0028
            int r0 = r12.A04     // Catch: all -> 0x00ab
            int r0 = r0 * 3
            int r7 = r0 / 4
        L_0x001b:
            int r3 = r12.getProgress()     // Catch: all -> 0x00ab
            int r0 = r12.getMax()     // Catch: all -> 0x00ab
            r10 = 0
            r1 = 0
            if (r0 <= 0) goto L_0x0030
            goto L_0x002d
        L_0x0028:
            int r0 = r12.A04     // Catch: all -> 0x00ab
            int r7 = r0 / 2
            goto L_0x001b
        L_0x002d:
            float r1 = (float) r3     // Catch: all -> 0x00ab
            float r0 = (float) r0     // Catch: all -> 0x00ab
            float r1 = r1 / r0
        L_0x0030:
            float r0 = (float) r4     // Catch: all -> 0x00ab
            float r1 = r1 * r0
            int r9 = (int) r1     // Catch: all -> 0x00ab
            int r9 = r9 + r2
            int r8 = r12.getPaddingTop()     // Catch: all -> 0x00ab
            int r0 = X.C12960it.A03(r12)     // Catch: all -> 0x00ab
            int r0 = r0 / 2
            int r8 = r8 + r0
            android.graphics.Paint r6 = r12.A0D     // Catch: all -> 0x00ab
            int r0 = r12.A01     // Catch: all -> 0x00ab
            X.C12970iu.A16(r0, r6)     // Catch: all -> 0x00ab
            boolean r0 = r12.A09     // Catch: all -> 0x00ab
            r11 = 1073741824(0x40000000, float:2.0)
            if (r0 != 0) goto L_0x006f
            android.graphics.RectF r5 = r12.A0E     // Catch: all -> 0x00ab
            int r0 = r12.A05     // Catch: all -> 0x00ab
            int r0 = r0 / 2
            int r0 = r8 - r0
            float r4 = (float) r0     // Catch: all -> 0x00ab
            float r1 = X.C12990iw.A02(r12)     // Catch: all -> 0x00ab
            int r0 = r12.A05     // Catch: all -> 0x00ab
            int r0 = r0 / 2
            int r0 = r0 + r8
            float r0 = (float) r0     // Catch: all -> 0x00ab
            r5.set(r10, r4, r1, r0)     // Catch: all -> 0x00ab
            float r1 = r5.height()     // Catch: all -> 0x00ab
            float r1 = r1 / r11
            float r0 = r5.height()     // Catch: all -> 0x00ab
            float r0 = r0 / r11
            r13.drawRoundRect(r5, r1, r0, r6)     // Catch: all -> 0x00ab
        L_0x006f:
            int r0 = r12.A02     // Catch: all -> 0x00ab
            r6.setColor(r0)     // Catch: all -> 0x00ab
            boolean r0 = r12.A0A     // Catch: all -> 0x00ab
            if (r0 == 0) goto L_0x007b
            r1 = 1
            if (r3 == 0) goto L_0x007c
        L_0x007b:
            r1 = 0
        L_0x007c:
            boolean r0 = r12.A09     // Catch: all -> 0x00ab
            if (r0 != 0) goto L_0x0083
            if (r1 != 0) goto L_0x00a9
            goto L_0x0086
        L_0x0083:
            if (r1 != 0) goto L_0x00a9
            goto L_0x00a3
        L_0x0086:
            android.graphics.RectF r5 = r12.A0E     // Catch: all -> 0x00ab
            float r4 = (float) r2     // Catch: all -> 0x00ab
            int r0 = r12.A05     // Catch: all -> 0x00ab
            int r3 = r0 / 2
            int r0 = r8 - r3
            float r2 = (float) r0     // Catch: all -> 0x00ab
            float r1 = (float) r9     // Catch: all -> 0x00ab
            int r3 = r3 + r8
            float r0 = (float) r3     // Catch: all -> 0x00ab
            r5.set(r4, r2, r1, r0)     // Catch: all -> 0x00ab
            float r1 = r5.height()     // Catch: all -> 0x00ab
            float r1 = r1 / r11
            float r0 = r5.height()     // Catch: all -> 0x00ab
            float r0 = r0 / r11
            r13.drawRoundRect(r5, r1, r0, r6)     // Catch: all -> 0x00ab
        L_0x00a3:
            float r2 = (float) r9     // Catch: all -> 0x00ab
            float r1 = (float) r8     // Catch: all -> 0x00ab
            float r0 = (float) r7     // Catch: all -> 0x00ab
            r13.drawCircle(r2, r1, r0, r6)     // Catch: all -> 0x00ab
        L_0x00a9:
            monitor-exit(r12)
            return
        L_0x00ab:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voicerecorder.VoiceNoteSeekBar.onDraw(android.graphics.Canvas):void");
    }

    @Override // android.widget.AbsSeekBar, android.widget.ProgressBar, android.view.View
    public synchronized void onMeasure(int i, int i2) {
        setMeasuredDimension(SeekBar.resolveSizeAndState(this.A04 << 1, i, 0), SeekBar.resolveSizeAndState(this.A04 << 1, i2, 0));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        if (r4.A0C != false) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008a, code lost:
        if (java.lang.Math.abs(r5.getX() - r4.A00) > ((float) r4.A03)) goto L_0x008c;
     */
    @Override // android.widget.AbsSeekBar, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r5) {
        /*
            r4 = this;
            boolean r0 = r4.isEnabled()
            r3 = 0
            if (r0 != 0) goto L_0x0008
            return r3
        L_0x0008:
            X.0MU r0 = r4.A07
            if (r0 == 0) goto L_0x0011
            X.0hn r0 = r0.A00
            r0.AXZ(r5)
        L_0x0011:
            int r1 = r5.getAction()
            r2 = 1
            if (r1 == 0) goto L_0x005d
            if (r1 == r2) goto L_0x003d
            r0 = 2
            if (r1 == r0) goto L_0x0035
            r0 = 3
            if (r1 != r0) goto L_0x0034
            boolean r0 = r4.A0C
            if (r0 == 0) goto L_0x0031
        L_0x0024:
            r4.A0C = r3
            android.widget.SeekBar$OnSeekBarChangeListener r1 = r4.A06
            if (r1 == 0) goto L_0x002e
            r0 = 0
            r1.onStopTrackingTouch(r0)
        L_0x002e:
            r4.setPressed(r3)
        L_0x0031:
            r4.invalidate()
        L_0x0034:
            return r2
        L_0x0035:
            boolean r0 = r4.A0C
            if (r0 == 0) goto L_0x007a
            r4.A01(r5)
            return r2
        L_0x003d:
            boolean r0 = r4.A0C
            if (r0 == 0) goto L_0x0045
            r4.A01(r5)
            goto L_0x0024
        L_0x0045:
            r4.A0C = r2
            android.widget.SeekBar$OnSeekBarChangeListener r1 = r4.A06
            if (r1 == 0) goto L_0x004f
            r0 = 0
            r1.onStartTrackingTouch(r0)
        L_0x004f:
            r4.A01(r5)
            r4.A0C = r3
            android.widget.SeekBar$OnSeekBarChangeListener r1 = r4.A06
            if (r1 == 0) goto L_0x0031
            r0 = 0
            r1.onStopTrackingTouch(r0)
            goto L_0x0031
        L_0x005d:
            android.view.ViewParent r1 = r4.getParent()
        L_0x0061:
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x008c
            r0 = r1
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            boolean r0 = r0.shouldDelayChildPressedState()
            if (r0 == 0) goto L_0x0075
            float r0 = r5.getX()
            r4.A00 = r0
            return r2
        L_0x0075:
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x0061
        L_0x007a:
            float r1 = r5.getX()
            float r0 = r4.A00
            float r1 = r1 - r0
            float r1 = java.lang.Math.abs(r1)
            int r0 = r4.A03
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0034
        L_0x008c:
            r4.setPressed(r2)
            r4.invalidate()
            r4.A0C = r2
            android.widget.SeekBar$OnSeekBarChangeListener r1 = r4.A06
            if (r1 == 0) goto L_0x009c
            r0 = 0
            r1.onStartTrackingTouch(r0)
        L_0x009c:
            r4.A01(r5)
            android.view.ViewParent r0 = r4.getParent()
            if (r0 == 0) goto L_0x0034
            r0.requestDisallowInterceptTouchEvent(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voicerecorder.VoiceNoteSeekBar.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setHideProgressBar(boolean z) {
        this.A09 = z;
    }

    public void setHideProgressWhenPlaybackPositionAtStart(boolean z) {
        this.A0A = z;
    }

    @Override // android.widget.SeekBar
    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener onSeekBarChangeListener) {
        this.A06 = onSeekBarChangeListener;
        super.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }

    public void setProgressColor(int i) {
        this.A02 = i;
        invalidate();
    }
}
