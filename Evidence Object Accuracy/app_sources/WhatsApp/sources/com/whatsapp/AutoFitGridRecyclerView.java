package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass02H;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C54612h0;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes2.dex */
public class AutoFitGridRecyclerView extends RecyclerView implements AnonymousClass004 {
    public int A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public AutoFitGridRecyclerView(Context context) {
        super(context);
        A0y();
        A0z(context, null);
    }

    public AutoFitGridRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0y();
        A0z(context, attributeSet);
    }

    public AutoFitGridRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0y();
        A0z(context, attributeSet);
    }

    public AutoFitGridRecyclerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A0y();
    }

    public void A0y() {
        if (!this.A03) {
            this.A03 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A0z(Context context, AttributeSet attributeSet) {
        int i = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A01);
            this.A00 = obtainStyledAttributes.getDimensionPixelSize(0, this.A00);
            i = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            obtainStyledAttributes.recycle();
        }
        A0l(new C54612h0(this.A01, i));
        setLayoutManager(new GridLayoutManager(1));
        this.A0h = true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A00 > 0) {
            AnonymousClass02H layoutManager = getLayoutManager();
            if (layoutManager instanceof GridLayoutManager) {
                ((GridLayoutManager) layoutManager).A1h(Math.max(1, getMeasuredWidth() / this.A00));
            }
        }
    }
}
