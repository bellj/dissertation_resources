package com.whatsapp.dmsetting;

import X.AbstractC14640lm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass150;
import X.AnonymousClass1V8;
import X.AnonymousClass1VY;
import X.AnonymousClass1W9;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass65I;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119945fM;
import X.C119985fQ;
import X.C12960it;
import X.C14860mA;
import X.C14900mE;
import X.C15380n4;
import X.C15550nR;
import X.C15580nU;
import X.C15600nX;
import X.C16120oU;
import X.C16170oZ;
import X.C17170qN;
import X.C17220qS;
import X.C19990v2;
import X.C20020v5;
import X.C20660w7;
import X.C20710wC;
import X.C20790wK;
import X.C21320xE;
import X.C238013b;
import X.C252018m;
import X.C253118x;
import X.C32341c0;
import X.C34271fr;
import X.C42971wC;
import X.C457622z;
import X.C49372Km;
import X.C52162aM;
import X.C614330i;
import X.RunnableC32531cJ;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.dmsetting.ChangeDMSettingActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public class ChangeDMSettingActivity extends ActivityC13790kL {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public TextEmojiLabel A04;
    public TextEmojiLabel A05;
    public C16170oZ A06;
    public C238013b A07;
    public C15550nR A08;
    public C20020v5 A09;
    public C17170qN A0A;
    public C19990v2 A0B;
    public C21320xE A0C;
    public C15600nX A0D;
    public C20790wK A0E;
    public AnonymousClass150 A0F;
    public C16120oU A0G;
    public C20710wC A0H;
    public C20660w7 A0I;
    public C252018m A0J;
    public C253118x A0K;
    public C14860mA A0L;
    public boolean A0M;

    public ChangeDMSettingActivity() {
        this(0);
    }

    public ChangeDMSettingActivity(int i) {
        this.A0M = false;
        C117295Zj.A0p(this, 1);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0K = C117315Zl.A0H(A1M);
            this.A0B = (C19990v2) A1M.A3M.get();
            this.A0G = (C16120oU) A1M.ANE.get();
            this.A0L = (C14860mA) A1M.ANU.get();
            this.A0I = (C20660w7) A1M.AIB.get();
            this.A06 = (C16170oZ) A1M.AM4.get();
            this.A08 = C12960it.A0O(A1M);
            this.A0J = (C252018m) A1M.A7g.get();
            this.A07 = (C238013b) A1M.A1Z.get();
            this.A0H = (C20710wC) A1M.A8m.get();
            this.A09 = (C20020v5) A1M.A3B.get();
            this.A0C = (C21320xE) A1M.A4Y.get();
            this.A0E = (C20790wK) A1M.A61.get();
            this.A0D = (C15600nX) A1M.A8x.get();
            this.A0A = (C17170qN) A1M.AMt.get();
            this.A0F = (AnonymousClass150) A1M.A63.get();
        }
    }

    public final void A2e(int i) {
        if (i == -1) {
            A2f(3);
        } else if (i != this.A0F.A04().intValue()) {
            C20790wK r3 = this.A0E;
            int i2 = this.A01;
            if (!r3.A02.A0B()) {
                r3.A01.A07(R.string.coldsync_no_network, 0);
                r3.A00.A0B(r3.A04.A04());
                return;
            }
            C17220qS r11 = r3.A06;
            String A01 = r11.A01();
            AnonymousClass1V8 r6 = new AnonymousClass1V8("disappearing_mode", new AnonymousClass1W9[]{new AnonymousClass1W9("duration", (long) i)});
            AnonymousClass1W9[] r5 = new AnonymousClass1W9[4];
            r5[0] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
            C12960it.A1M("id", A01, r5, 1);
            C117295Zj.A1P("type", "set", r5);
            C117305Zk.A1Q("xmlns", "disappearing_mode", r5);
            r11.A0D(new C457622z(r3, i, i2), new AnonymousClass1V8(r6, "iq", r5), A01, 277, 20000);
        }
    }

    public final void A2f(int i) {
        if (((ActivityC13810kN) this).A0C.A07(1518)) {
            C119945fM r1 = new C119945fM();
            r1.A01 = Integer.valueOf(i);
            r1.A00 = Integer.valueOf(this.A01);
            this.A0G.A07(r1);
        }
    }

    public final void A2g(int i) {
        if (!((ActivityC13810kN) this).A0C.A07(1518)) {
            return;
        }
        if (i == 0) {
            this.A04.setVisibility(8);
            return;
        }
        this.A04.setVisibility(0);
        A2h(null, 0, i, 0);
    }

    public final void A2h(List list, int i, int i2, int i3) {
        long j;
        if (((ActivityC13810kN) this).A0C.A07(1518)) {
            C119985fQ r2 = new C119985fQ();
            int i4 = 0;
            r2.A00 = 0;
            r2.A01 = Integer.valueOf(i);
            if (i2 == -1) {
                j = 0;
            } else {
                j = (long) i2;
            }
            r2.A03 = Long.valueOf(j);
            if (list != null) {
                r2.A02 = Long.valueOf((long) list.size());
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (C15380n4.A0K((Jid) it.next())) {
                        i4++;
                    }
                }
                r2.A04 = Long.valueOf((long) i4);
                r2.A06 = Long.valueOf((long) this.A00);
                r2.A05 = Long.valueOf((long) i3);
            }
            this.A0G.A07(r2);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        View view;
        C14900mE r1;
        int i3;
        long j;
        super.onActivityResult(i, i2, intent);
        if (intent == null) {
            return;
        }
        if (i == 1 && i2 == -1) {
            List<AbstractC14640lm> A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
            this.A00 = intent.getIntExtra("all_contacts_count", 0);
            int i4 = this.A03;
            if (i4 == -1) {
                i4 = this.A0F.A04().intValue();
            }
            if (i4 != -1) {
                int i5 = this.A02;
                int i6 = 0;
                if (!((ActivityC13810kN) this).A07.A0B()) {
                    ((ActivityC13810kN) this).A05.A07(R.string.coldsync_no_network, 0);
                } else {
                    for (AbstractC14640lm r2 : A07) {
                        if (C32341c0.A00(this.A08, this.A0B, r2) == 0) {
                            i6++;
                        }
                        AnonymousClass009.A05(r2);
                        boolean z = r2 instanceof UserJid;
                        if (z && this.A07.A0I((UserJid) r2)) {
                            r1 = ((ActivityC13810kN) this).A05;
                            i3 = R.string.ephemeral_unblock_to_turn_setting_on;
                            if (i4 == 0) {
                                i3 = R.string.ephemeral_unblock_to_turn_setting_off;
                            }
                        } else if (i4 != -1) {
                            if (((ActivityC13810kN) this).A07.A0B()) {
                                boolean A0K = C15380n4.A0K(r2);
                                if (A0K) {
                                    C15580nU r12 = (C15580nU) r2;
                                    this.A0I.A07(new RunnableC32531cJ(this.A0C, this.A0H, r12, null, this.A0L, null, null, 224), r12, i4);
                                } else if (z) {
                                    this.A06.A0K((UserJid) r2, i4);
                                } else {
                                    Log.e(C12960it.A0f(C12960it.A0k("Ephemeral not supported for this type of jid, type="), r2.getType()));
                                }
                                if (((ActivityC13810kN) this).A0C.A07(1518)) {
                                    C614330i r122 = new C614330i();
                                    r122.A02 = Long.valueOf((long) i4);
                                    if (i5 == -1) {
                                        j = 0;
                                    } else {
                                        j = (long) i5;
                                    }
                                    r122.A03 = Long.valueOf(j);
                                    r122.A00 = 4;
                                    r122.A04 = this.A09.A06(r2.getRawString());
                                    if (A0K) {
                                        C15600nX r13 = this.A0D;
                                        C15580nU A02 = C15580nU.A02(r2);
                                        AnonymousClass009.A05(A02);
                                        r122.A01 = Integer.valueOf(C49372Km.A01(r13.A02(A02).A07()));
                                    }
                                    this.A0G.A07(r122);
                                }
                            } else {
                                r1 = ((ActivityC13810kN) this).A05;
                                i3 = R.string.ephemeral_setting_internet_needed;
                            }
                        }
                        r1.A07(i3, 1);
                    }
                    A2h(A07, 3, i4, i6);
                    if (A07.size() > 0) {
                        A2f(2);
                    }
                }
            }
            if (A07.size() > 0 && (view = ((ActivityC13810kN) this).A00) != null) {
                Object[] objArr = new Object[2];
                objArr[0] = C32341c0.A02(this, i4);
                C12960it.A1P(objArr, A07.size(), 1);
                C34271fr A00 = C34271fr.A00(view, ((ActivityC13830kP) this).A01.A0I(objArr, R.plurals.dm_multi_select_ddm_confirmation_toast_message, (long) A07.size()), -1);
                TextView A0J = C12960it.A0J(A00.A05, R.id.snackbar_text);
                if (A0J != null) {
                    A0J.setSingleLine(false);
                }
                A00.A03();
                return;
            }
            return;
        }
        List A072 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
        this.A00 = intent.getIntExtra("all_contacts_count", 0);
        int i7 = this.A03;
        if (i7 == -1) {
            i7 = this.A0F.A04().intValue();
        }
        A2h(A072, 2, i7, 0);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e(this.A03);
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int[] iArr;
        super.onCreate(bundle);
        setContentView(R.layout.preferences_disappearing_mode);
        this.A01 = getIntent().getIntExtra("entry_point", 1);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        toolbar.setNavigationIcon(AnonymousClass2GF.A00(this, ((ActivityC13830kP) this).A01, R.drawable.ic_back));
        toolbar.setTitle(getString(R.string.dm_setting_toolbar_title));
        toolbar.setBackgroundResource(R.color.primary);
        toolbar.setNavigationOnClickListener(C117305Zk.A0A(this, 1));
        toolbar.A0C(this, 2131952341);
        A1e(toolbar);
        this.A04 = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.dm_description);
        this.A05 = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.dm_learn_more);
        String A0X = C12960it.A0X(this, "by-selecting-them", new Object[1], 0, R.string.dm_setting_description_to_select_existing_chats);
        String string = getString(R.string.dm_learn_more_label);
        if (((ActivityC13810kN) this).A0C.A07(1518)) {
            this.A04.setText(this.A0K.A03(this, new Runnable() { // from class: X.6FN
                @Override // java.lang.Runnable
                public final void run() {
                    ChangeDMSettingActivity changeDMSettingActivity = ChangeDMSettingActivity.this;
                    int i = changeDMSettingActivity.A03;
                    if (i == -1) {
                        i = changeDMSettingActivity.A0F.A04().intValue();
                    }
                    changeDMSettingActivity.A2h(null, 1, i, 0);
                    C42641vY r2 = new C42641vY(changeDMSettingActivity);
                    r2.A0D = true;
                    r2.A0F = true;
                    r2.A0R = C12960it.A0l();
                    r2.A0A = true;
                    r2.A0I = Integer.valueOf(i);
                    changeDMSettingActivity.startActivityForResult(r2.A00(), 1);
                }
            }, A0X, "by-selecting-them", R.color.primary_light));
            this.A04.setMovementMethod(new C52162aM());
            this.A05.setVisibility(0);
            this.A05.setText(this.A0K.A02(this, new Runnable() { // from class: X.6FM
                @Override // java.lang.Runnable
                public final void run() {
                    ChangeDMSettingActivity changeDMSettingActivity = ChangeDMSettingActivity.this;
                    Intent intent = new Intent("android.intent.action.VIEW", changeDMSettingActivity.A0J.A04("chats", "about-disappearing-messages"));
                    intent.addFlags(268435456);
                    ((ActivityC13790kL) changeDMSettingActivity).A00.A06(changeDMSettingActivity, intent);
                    changeDMSettingActivity.A2f(4);
                }
            }, string, "learn-more"));
            this.A05.setMovementMethod(new C52162aM());
        } else {
            String string2 = getString(R.string.dm_setting_description_multi_durations);
            C14900mE r10 = ((ActivityC13810kN) this).A05;
            AnonymousClass12P r9 = ((ActivityC13790kL) this).A00;
            AnonymousClass01d r12 = ((ActivityC13810kN) this).A08;
            C42971wC.A08(this, this.A0J.A04("chats", "about-disappearing-messages"), r9, r10, this.A04, r12, string2, "learn-more");
        }
        this.A03 = -1;
        RadioGroup radioGroup = (RadioGroup) AnonymousClass00T.A05(this, R.id.dm_radio_group);
        int intValue = this.A0F.A04().intValue();
        this.A02 = intValue;
        C32341c0.A05(radioGroup, ((ActivityC13810kN) this).A0C, intValue, true);
        A2g(intValue);
        if (((ActivityC13810kN) this).A0C.A07(1397)) {
            iArr = AnonymousClass01V.A0E;
        } else {
            iArr = AnonymousClass01V.A0F;
        }
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            View childAt = radioGroup.getChildAt(i);
            if (childAt instanceof RadioButton) {
                A0l.add(childAt);
            }
        }
        AnonymousClass65I r3 = new RadioGroup.OnCheckedChangeListener() { // from class: X.65I
            @Override // android.widget.RadioGroup.OnCheckedChangeListener
            public final void onCheckedChanged(RadioGroup radioGroup2, int i2) {
                ChangeDMSettingActivity changeDMSettingActivity = ChangeDMSettingActivity.this;
                int A05 = C12960it.A05(AnonymousClass028.A0D(radioGroup2, i2).getTag());
                changeDMSettingActivity.A03 = A05;
                changeDMSettingActivity.A2g(A05);
            }
        };
        radioGroup.setOnCheckedChangeListener(r3);
        this.A0E.A04.A00.A05(this, new AnonymousClass02B(r3, radioGroup, A0l, iArr) { // from class: X.65k
            public final /* synthetic */ RadioGroup.OnCheckedChangeListener A00;
            public final /* synthetic */ RadioGroup A01;
            public final /* synthetic */ List A02;
            public final /* synthetic */ int[] A03;

            {
                this.A01 = r2;
                this.A03 = r4;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                RadioGroup radioGroup2 = this.A01;
                int[] iArr2 = this.A03;
                List list = this.A02;
                RadioGroup.OnCheckedChangeListener onCheckedChangeListener = this.A00;
                int A05 = C12960it.A05(AnonymousClass028.A0D(radioGroup2, radioGroup2.getCheckedRadioButtonId()).getTag());
                int intValue2 = ((Number) obj).intValue();
                if (intValue2 != A05) {
                    radioGroup2.setOnCheckedChangeListener(null);
                    for (int i2 = 0; i2 < iArr2.length; i2++) {
                        if (iArr2[i2] == intValue2) {
                            ((CompoundButton) list.get(i2)).setChecked(true);
                        }
                    }
                    radioGroup2.setOnCheckedChangeListener(onCheckedChangeListener);
                }
            }
        });
        A2f(1);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            A2e(this.A03);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
