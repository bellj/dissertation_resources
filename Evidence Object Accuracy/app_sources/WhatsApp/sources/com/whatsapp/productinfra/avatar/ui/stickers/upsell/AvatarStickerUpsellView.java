package com.whatsapp.productinfra.avatar.ui.stickers.upsell;

import X.AbstractC16710pd;
import X.ActivityC13810kN;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass12V;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3DF;
import X.AnonymousClass4EX;
import X.AnonymousClass4GN;
import X.AnonymousClass4Yq;
import X.C1111758j;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16700pc;
import X.C18120rw;
import X.C18160s0;
import X.C18170s1;
import X.C20980wd;
import X.C235812f;
import X.C51012Sk;
import X.C72083dz;
import X.C72253eG;
import X.C862046f;
import X.C862146g;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class AvatarStickerUpsellView extends ConstraintLayout implements AnonymousClass004 {
    public AnonymousClass12V A00;
    public C18170s1 A01;
    public C18160s0 A02;
    public C18120rw A03;
    public C20980wd A04;
    public C235812f A05;
    public AnonymousClass4EX A06;
    public AnonymousClass2P7 A07;
    public boolean A08;
    public final C1111758j A09;
    public final AbstractC16710pd A0A;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public AvatarStickerUpsellView(Context context) {
        this(context, null, 0, 6, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public AvatarStickerUpsellView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarStickerUpsellView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        AnonymousClass4EX r0;
        C16700pc.A0E(context, 1);
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A03 = A00.A42();
            this.A02 = (C18160s0) A00.A14.get();
            this.A00 = (AnonymousClass12V) A00.A0p.get();
            this.A01 = A00.A41();
            this.A04 = (C20980wd) A00.A0t.get();
            this.A05 = (C235812f) A00.A13.get();
        }
        this.A0A = AnonymousClass4Yq.A01(new C72083dz(context, this));
        this.A09 = new C1111758j(this);
        LayoutInflater.from(context).inflate(R.layout.view_stickers_upsell, (ViewGroup) this, true);
        setBackgroundResource(R.drawable.stickers_upsell_background);
        View A02 = C16700pc.A02(this, R.id.stickers_upsell_close);
        if (attributeSet != null) {
            int i2 = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4GN.A00, 0, 0);
            C16700pc.A0B(obtainStyledAttributes);
            A02.setVisibility(obtainStyledAttributes.getBoolean(0, true) ? 0 : 4);
            boolean z = obtainStyledAttributes.getBoolean(2, true);
            TextView A0I = C12960it.A0I(this, R.id.stickers_upsell_publisher);
            A0I.setVisibility(!z ? 8 : i2);
            A0I.setText("Meta");
            int i3 = obtainStyledAttributes.getInt(1, -1);
            if (i3 == 0) {
                r0 = C862046f.A00;
            } else if (i3 == 1) {
                r0 = C862146g.A00;
            } else {
                throw C12970iu.A0f("Avatar sticker upsell entry point must be set");
            }
            this.A06 = r0;
            obtainStyledAttributes.recycle();
        }
        setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 12));
        C12960it.A12(A02, this, 11);
        AnonymousClass3DF viewController = getViewController();
        AnonymousClass4EX r2 = this.A06;
        if (r2 == null) {
            throw C16700pc.A06("entryPoint");
        } else if (C12980iv.A1W((SharedPreferences) C16700pc.A05(viewController.A03.A01), "pref_has_dismissed_sticker_upsell")) {
            viewController.A01.setVisibility(8);
        } else {
            viewController.A02.A00(new C72253eG(r2, viewController));
        }
    }

    public /* synthetic */ AvatarStickerUpsellView(Context context, AttributeSet attributeSet, int i, int i2, C51012Sk r6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    public static final void A01(AvatarStickerUpsellView avatarStickerUpsellView) {
        C16700pc.A0E(avatarStickerUpsellView, 0);
        avatarStickerUpsellView.getAvatarLogger().A02(10);
        AnonymousClass3DF viewController = avatarStickerUpsellView.getViewController();
        C18120rw.A00((ActivityC13810kN) viewController.A00, viewController.A04);
    }

    public static final void A02(AvatarStickerUpsellView avatarStickerUpsellView) {
        C16700pc.A0E(avatarStickerUpsellView, 0);
        avatarStickerUpsellView.getAvatarLogger().A02(11);
        avatarStickerUpsellView.getViewController().A00();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    public final AnonymousClass12V getAvatarConfigRepository() {
        AnonymousClass12V r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarConfigRepository");
    }

    public final C20980wd getAvatarEditorEventObservers() {
        C20980wd r0 = this.A04;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarEditorEventObservers");
    }

    public final C18120rw getAvatarEditorLauncherProxy() {
        C18120rw r0 = this.A03;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarEditorLauncherProxy");
    }

    public final C235812f getAvatarLogger() {
        C235812f r0 = this.A05;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarLogger");
    }

    public final C18170s1 getAvatarRepository() {
        C18170s1 r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarRepository");
    }

    public final C18160s0 getAvatarSharedPreferences() {
        C18160s0 r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("avatarSharedPreferences");
    }

    /* access modifiers changed from: private */
    public final AnonymousClass3DF getViewController() {
        return (AnonymousClass3DF) this.A0A.getValue();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getAvatarEditorEventObservers().A03(this.A09);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getAvatarEditorEventObservers().A04(this.A09);
    }

    public final void setAvatarConfigRepository(AnonymousClass12V r2) {
        C16700pc.A0E(r2, 0);
        this.A00 = r2;
    }

    public final void setAvatarEditorEventObservers(C20980wd r2) {
        C16700pc.A0E(r2, 0);
        this.A04 = r2;
    }

    public final void setAvatarEditorLauncherProxy(C18120rw r2) {
        C16700pc.A0E(r2, 0);
        this.A03 = r2;
    }

    public final void setAvatarLogger(C235812f r2) {
        C16700pc.A0E(r2, 0);
        this.A05 = r2;
    }

    public final void setAvatarRepository(C18170s1 r2) {
        C16700pc.A0E(r2, 0);
        this.A01 = r2;
    }

    public final void setAvatarSharedPreferences(C18160s0 r2) {
        C16700pc.A0E(r2, 0);
        this.A02 = r2;
    }
}
