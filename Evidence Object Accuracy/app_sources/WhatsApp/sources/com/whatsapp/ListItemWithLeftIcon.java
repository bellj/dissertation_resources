package com.whatsapp;

import X.AbstractC58392on;
import X.AnonymousClass028;
import X.C12960it;
import android.content.Context;
import android.content.res.Resources;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/* loaded from: classes2.dex */
public class ListItemWithLeftIcon extends AbstractC58392on {
    public View A00;

    public ListItemWithLeftIcon(Context context) {
        super(context, null);
    }

    public ListItemWithLeftIcon(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ListItemWithLeftIcon(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // X.AbstractC58392on
    public void A01(AttributeSet attributeSet) {
        super.A01(attributeSet);
        this.A00 = AnonymousClass028.A0D(this, R.id.list_item_with_left_icon);
        if (TextUtils.isEmpty(((AbstractC58392on) this).A00.getText())) {
            ((AbstractC58392on) this).A00.setVisibility(8);
        }
    }

    @Override // X.AbstractC58392on
    public int getRootLayoutID() {
        return R.layout.list_item_with_left_icon;
    }

    @Override // X.AbstractC58392on
    public void setDescription(Spanned spanned) {
        if (TextUtils.isEmpty(spanned)) {
            setDescriptionVisibility(8);
            return;
        }
        setDescriptionVisibility(0);
        super.setDescription(spanned);
    }

    @Override // X.AbstractC58392on
    public void setDescription(String str) {
        if (TextUtils.isEmpty(str)) {
            setDescriptionVisibility(8);
            return;
        }
        setDescriptionVisibility(0);
        super.setDescription(str);
    }

    public void setDescriptionVisibility(int i) {
        int i2;
        if (((AbstractC58392on) this).A00.getVisibility() != i) {
            ((AbstractC58392on) this).A00.setVisibility(i);
            boolean A1T = C12960it.A1T(i);
            Resources resources = getResources();
            int i3 = R.dimen.info_screen_padding;
            int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.info_screen_padding);
            Resources resources2 = getResources();
            if (A1T) {
                i3 = R.dimen.space_base_halfStep;
            }
            int dimensionPixelSize2 = resources2.getDimensionPixelSize(i3);
            this.A00.setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ((AbstractC58392on) this).A01.getLayoutParams();
            int i4 = 3;
            if (A1T) {
                i4 = 51;
            }
            layoutParams.gravity = i4;
            ((AbstractC58392on) this).A01.setLayoutParams(layoutParams);
            WaImageView waImageView = ((AbstractC58392on) this).A01;
            if (A1T) {
                i2 = getResources().getDimensionPixelSize(R.dimen.space_tight);
            } else {
                i2 = 0;
            }
            waImageView.setPadding(0, i2, 0, 0);
        }
    }

    public void setTitleTextColor(int i) {
        this.A02.setTextColor(i);
    }
}
