package com.whatsapp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* loaded from: classes2.dex */
public class InteractiveAnnotation implements Serializable {
    public static final long serialVersionUID = -3211751283609597L;
    public SerializablePoint[] polygonVertices;
    public SerializableLocation serializableLocation;

    public InteractiveAnnotation(SerializableLocation serializableLocation, SerializablePoint[] serializablePointArr) {
        this.polygonVertices = serializablePointArr;
        this.serializableLocation = serializableLocation;
    }

    public InteractiveAnnotation(String str, SerializablePoint[] serializablePointArr, double d, double d2) {
        this.polygonVertices = serializablePointArr;
        this.serializableLocation = new SerializableLocation(str, d, d2);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.polygonVertices = (SerializablePoint[]) objectInputStream.readObject();
        this.serializableLocation = (SerializableLocation) objectInputStream.readObject();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(this.polygonVertices);
        objectOutputStream.writeObject(this.serializableLocation);
    }
}
