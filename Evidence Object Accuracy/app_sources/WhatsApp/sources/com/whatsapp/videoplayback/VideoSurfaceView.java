package com.whatsapp.videoplayback;

import X.AbstractC73673gc;
import X.AnonymousClass009;
import X.C12960it;
import X.C12970iu;
import X.C14350lI;
import X.C98024i1;
import X.C98054i4;
import X.C98094i8;
import X.C98144iD;
import X.SurfaceHolder$CallbackC100894me;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.MediaController;
import com.whatsapp.GifHelper;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes3.dex */
public class VideoSurfaceView extends AbstractC73673gc implements MediaController.MediaPlayerControl {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public MediaPlayer.OnCompletionListener A09;
    public MediaPlayer.OnErrorListener A0A;
    public MediaPlayer.OnPreparedListener A0B;
    public MediaPlayer A0C;
    public Uri A0D;
    public SurfaceHolder A0E;
    public Map A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public final MediaPlayer.OnBufferingUpdateListener A0L = new C98024i1(this);
    public final MediaPlayer.OnCompletionListener A0M = new C98054i4(this);
    public final MediaPlayer.OnErrorListener A0N = new C98094i8(this);
    public final MediaPlayer.OnPreparedListener A0O = new C98144iD(this);
    public final MediaPlayer.OnVideoSizeChangedListener A0P = AbstractC73673gc.A00(this);
    public final SurfaceHolder.Callback A0Q = new SurfaceHolder$CallbackC100894me(this);

    public VideoSurfaceView(Context context) {
        super(context);
        A01();
    }

    public VideoSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public VideoSurfaceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public void A00() {
        MediaPlayer mediaPlayer = this.A0C;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.A0C.release();
            this.A0C = null;
            this.A02 = 0;
            this.A06 = 0;
        }
    }

    public final void A01() {
        this.A08 = 0;
        this.A07 = 0;
        getHolder().addCallback(this.A0Q);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.A02 = 0;
        this.A06 = 0;
    }

    public final void A02() {
        Uri uri = this.A0D;
        if (uri != null && this.A0E != null) {
            File A03 = C14350lI.A03(uri);
            if (A03 == null || !A03.exists() || !GifHelper.A01(A03)) {
                Intent intent = new Intent("com.android.music.musicservicecommand");
                intent.putExtra("command", "pause");
                getContext().sendBroadcast(intent);
            }
            A04(false);
            try {
                MediaPlayer mediaPlayer = new MediaPlayer();
                this.A0C = mediaPlayer;
                int i = this.A00;
                if (i != 0) {
                    mediaPlayer.setAudioSessionId(i);
                } else {
                    this.A00 = mediaPlayer.getAudioSessionId();
                }
                if (this.A0K) {
                    this.A0C.setVolume(0.0f, 0.0f);
                }
                if (this.A0J) {
                    this.A0C.setLooping(true);
                }
                this.A0C.setOnPreparedListener(this.A0O);
                this.A0C.setOnVideoSizeChangedListener(this.A0P);
                this.A0C.setOnCompletionListener(this.A0M);
                this.A0C.setOnErrorListener(this.A0N);
                this.A0C.setOnBufferingUpdateListener(this.A0L);
                this.A01 = 0;
                this.A0C.setDataSource(getContext(), this.A0D, this.A0F);
                this.A0C.setDisplay(this.A0E);
                this.A0C.setAudioStreamType(3);
                this.A0C.setScreenOnWhilePlaying(true);
                this.A0C.prepareAsync();
                this.A02 = 1;
            } catch (IOException | IllegalArgumentException e) {
                Log.w(C12970iu.A0s(this.A0D, C12960it.A0k("videoview/ Unable to open content: ")), e);
                this.A02 = -1;
                this.A06 = -1;
                this.A0N.onError(this.A0C, 1, 0);
            }
        }
    }

    public void A03(int i, int i2) {
        StringBuilder A0k = C12960it.A0k("videoview/setVideoDimensions: ");
        A0k.append(i);
        Log.i(C12960it.A0e("x", A0k, i2));
        this.A08 = i;
        this.A07 = i2;
    }

    public final void A04(boolean z) {
        MediaPlayer mediaPlayer = this.A0C;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.A0C.release();
            this.A0C = null;
            this.A02 = 0;
            if (z) {
                this.A06 = 0;
            }
        }
    }

    public boolean A05() {
        int i;
        return (this.A0C == null || (i = this.A02) == -1 || i == 0 || i == 1) ? false : true;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canPause() {
        return this.A0G;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canSeekBackward() {
        return this.A0H;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canSeekForward() {
        return this.A0I;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getAudioSessionId() {
        if (this.A00 == 0) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            this.A00 = mediaPlayer.getAudioSessionId();
            mediaPlayer.release();
        }
        return this.A00;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getBufferPercentage() {
        if (this.A0C != null) {
            return this.A01;
        }
        return 0;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getCurrentPosition() {
        if (!A05()) {
            return 0;
        }
        MediaPlayer mediaPlayer = this.A0C;
        AnonymousClass009.A05(mediaPlayer);
        return mediaPlayer.getCurrentPosition();
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getDuration() {
        if (!A05()) {
            return -1;
        }
        MediaPlayer mediaPlayer = this.A0C;
        AnonymousClass009.A05(mediaPlayer);
        return mediaPlayer.getDuration();
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean isPlaying() {
        if (A05()) {
            MediaPlayer mediaPlayer = this.A0C;
            AnonymousClass009.A05(mediaPlayer);
            if (mediaPlayer.isPlaying()) {
                return true;
            }
        }
        return false;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(VideoSurfaceView.class.getName());
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(VideoSurfaceView.class.getName());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0066, code lost:
        if (r2 > r5) goto L_0x0083;
     */
    @Override // android.view.SurfaceView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            int r0 = r7.A08
            int r2 = android.view.SurfaceView.getDefaultSize(r0, r8)
            int r0 = r7.A07
            int r4 = android.view.SurfaceView.getDefaultSize(r0, r9)
            int r0 = r7.A08
            if (r0 <= 0) goto L_0x0035
            int r0 = r7.A07
            if (r0 <= 0) goto L_0x0035
            int r6 = android.view.View.MeasureSpec.getMode(r8)
            int r5 = android.view.View.MeasureSpec.getSize(r8)
            int r2 = android.view.View.MeasureSpec.getMode(r9)
            int r4 = android.view.View.MeasureSpec.getSize(r9)
            r0 = 1073741824(0x40000000, float:2.0)
            if (r6 != r0) goto L_0x004b
            if (r2 != r0) goto L_0x004b
            int r3 = r7.A08
            int r2 = r3 * r4
            int r1 = r7.A07
            int r0 = r5 * r1
            if (r2 >= r0) goto L_0x007f
            int r2 = r2 / r1
        L_0x0035:
            java.lang.String r0 = "videoview/setMeasuredDimension: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r2)
            java.lang.String r0 = "x"
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r4)
            com.whatsapp.util.Log.i(r0)
            r7.setMeasuredDimension(r2, r4)
            return
        L_0x004b:
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r6 != r0) goto L_0x005c
            int r1 = r7.A07
            int r1 = r1 * r5
            int r0 = r7.A08
            int r1 = r1 / r0
            if (r2 != r3) goto L_0x005a
            if (r1 <= r4) goto L_0x005a
            goto L_0x0083
        L_0x005a:
            r4 = r1
            goto L_0x0083
        L_0x005c:
            if (r2 != r0) goto L_0x0069
            int r2 = r7.A08
            int r2 = r2 * r4
            int r0 = r7.A07
            int r2 = r2 / r0
            if (r6 != r3) goto L_0x0035
            if (r2 <= r5) goto L_0x0035
            goto L_0x0083
        L_0x0069:
            int r1 = r7.A08
            int r0 = r7.A07
            if (r2 != r3) goto L_0x007c
            if (r0 <= r4) goto L_0x007c
            int r2 = r4 * r1
            int r2 = r2 / r0
        L_0x0074:
            if (r6 != r3) goto L_0x0035
            if (r2 <= r5) goto L_0x0035
            int r0 = r0 * r5
            int r4 = r0 / r1
            goto L_0x0083
        L_0x007c:
            r2 = r1
            r4 = r0
            goto L_0x0074
        L_0x007f:
            if (r2 <= r0) goto L_0x0083
            int r4 = r0 / r3
        L_0x0083:
            r2 = r5
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.videoplayback.VideoSurfaceView.onMeasure(int, int):void");
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void pause() {
        if (A05()) {
            MediaPlayer mediaPlayer = this.A0C;
            AnonymousClass009.A05(mediaPlayer);
            if (mediaPlayer.isPlaying()) {
                this.A0C.pause();
                this.A02 = 4;
            }
        }
        this.A06 = 4;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void seekTo(int i) {
        if (A05()) {
            MediaPlayer mediaPlayer = this.A0C;
            AnonymousClass009.A05(mediaPlayer);
            mediaPlayer.seekTo(i);
            i = -1;
        }
        this.A03 = i;
    }

    public void setLooping(boolean z) {
        this.A0J = z;
        MediaPlayer mediaPlayer = this.A0C;
        if (mediaPlayer != null) {
            mediaPlayer.setLooping(z);
        }
    }

    public void setMute(boolean z) {
        this.A0K = z;
        MediaPlayer mediaPlayer = this.A0C;
        if (mediaPlayer != null) {
            float f = 1.0f;
            if (z) {
                f = 0.0f;
            }
            mediaPlayer.setVolume(f, f);
        }
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.A09 = onCompletionListener;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.A0A = onErrorListener;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.A0B = onPreparedListener;
    }

    public void setVideoPath(String str) {
        setVideoURI(Uri.parse(str), null);
    }

    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    public void setVideoURI(Uri uri, Map map) {
        this.A0D = uri;
        this.A0F = map;
        this.A03 = -1;
        A02();
        requestLayout();
        invalidate();
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void start() {
        if (A05()) {
            MediaPlayer mediaPlayer = this.A0C;
            AnonymousClass009.A05(mediaPlayer);
            mediaPlayer.start();
            this.A02 = 3;
        }
        this.A06 = 3;
    }
}
