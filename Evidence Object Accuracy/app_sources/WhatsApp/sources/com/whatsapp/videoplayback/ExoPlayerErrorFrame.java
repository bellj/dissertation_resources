package com.whatsapp.videoplayback;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class ExoPlayerErrorFrame extends FrameLayout implements AnonymousClass004 {
    public View.OnClickListener A00;
    public View A01;
    public FrameLayout A02;
    public TextView A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public final FrameLayout A06;
    public final CircularProgressBar A07;

    public ExoPlayerErrorFrame(Context context) {
        this(context, null);
    }

    public ExoPlayerErrorFrame(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ExoPlayerErrorFrame(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A06 = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.wa_exoplayer_error_frame, this);
        this.A07 = (CircularProgressBar) findViewById(R.id.loading);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    public int getErrorScreenVisibility() {
        FrameLayout frameLayout = this.A02;
        if (frameLayout != null) {
            return frameLayout.getVisibility();
        }
        return 8;
    }

    public void setLoadingViewVisibility(int i) {
        this.A07.setVisibility(i);
    }

    public void setOnRetryListener(View.OnClickListener onClickListener) {
        this.A00 = onClickListener;
        View view = this.A01;
        if (view != null) {
            view.setOnClickListener(onClickListener);
        }
    }
}
