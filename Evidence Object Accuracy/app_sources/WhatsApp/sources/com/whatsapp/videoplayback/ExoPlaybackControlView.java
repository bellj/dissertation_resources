package com.whatsapp.videoplayback;

import X.AbstractC115455Rp;
import X.AbstractC116315Uy;
import X.AbstractC116325Uz;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2B0;
import X.AnonymousClass2Bd;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3MV;
import X.C83323x3;
import X.C94404bl;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.google.android.exoplayer2.Timeline;
import com.whatsapp.R;
import java.util.Formatter;
import java.util.Locale;

/* loaded from: classes2.dex */
public class ExoPlaybackControlView extends FrameLayout implements AnonymousClass004 {
    public AlphaAnimation A00;
    public AnonymousClass2B0 A01;
    public AnonymousClass018 A02;
    public AbstractC115455Rp A03;
    public AbstractC116315Uy A04;
    public AbstractC116325Uz A05;
    public AnonymousClass2P7 A06;
    public Long A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final View A0D;
    public final FrameLayout A0E;
    public final FrameLayout A0F;
    public final ImageButton A0G;
    public final ImageView A0H;
    public final LinearLayout A0I;
    public final SeekBar A0J;
    public final TextView A0K;
    public final TextView A0L;
    public final C94404bl A0M;
    public final AnonymousClass3MV A0N;
    public final Runnable A0O;
    public final Runnable A0P;
    public final StringBuilder A0Q;
    public final Formatter A0R;

    public ExoPlaybackControlView(Context context) {
        this(context, null);
    }

    public ExoPlaybackControlView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ExoPlaybackControlView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0B) {
            this.A0B = true;
            this.A02 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
        this.A0C = true;
        this.A09 = true;
        this.A08 = true;
        this.A0P = new RunnableBRunnable0Shape13S0100000_I0_13(this, 20);
        this.A0O = new RunnableBRunnable0Shape13S0100000_I0_13(this, 19);
        this.A0M = new C94404bl();
        StringBuilder sb = new StringBuilder();
        this.A0Q = sb;
        this.A0R = new Formatter(sb, Locale.getDefault());
        AnonymousClass3MV r2 = new AnonymousClass3MV(this);
        this.A0N = r2;
        LayoutInflater.from(context).inflate(R.layout.wa_exoplayer_playback_control_view, this);
        this.A0D = AnonymousClass028.A0D(this, R.id.exo_player_navbar_padding_view);
        this.A0F = (FrameLayout) findViewById(R.id.main_controls);
        this.A0K = (TextView) findViewById(R.id.time);
        this.A0L = (TextView) findViewById(R.id.time_current);
        SeekBar seekBar = (SeekBar) findViewById(R.id.mediacontroller_progress);
        this.A0J = seekBar;
        ImageView imageView = (ImageView) findViewById(R.id.back);
        this.A0H = imageView;
        this.A0I = (LinearLayout) findViewById(R.id.footerView);
        seekBar.setOnSeekBarChangeListener(r2);
        seekBar.setMax(1000);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.control_frame);
        this.A0E = frameLayout;
        this.A0G = (ImageButton) findViewById(R.id.play);
        frameLayout.setOnClickListener(r2);
        A04();
        A03();
        A05();
        if (this.A02.A04().A06) {
            if (!isInEditMode()) {
                imageView.setRotationY(180.0f);
            }
            if (Build.VERSION.SDK_INT < 17) {
                seekBar.setRotationY(180.0f);
            }
        }
        onConfigurationChanged(getResources().getConfiguration());
    }

    public void A00() {
        AnonymousClass2B0 r0;
        if (this.A08 && this.A00 == null) {
            AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator(1.5f);
            AlphaAnimation alphaAnimation = new AlphaAnimation(getAlpha(), 0.0f);
            this.A00 = alphaAnimation;
            alphaAnimation.setDuration(250);
            this.A00.setInterpolator(accelerateInterpolator);
            this.A00.setAnimationListener(new C83323x3(this));
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.exo_player_controls_down);
            loadAnimation.setDuration(250);
            loadAnimation.setInterpolator(accelerateInterpolator);
            if (A07()) {
                FrameLayout frameLayout = this.A0F;
                frameLayout.setVisibility(4);
                AbstractC116325Uz r1 = this.A05;
                if (r1 != null) {
                    r1.AYP(frameLayout.getVisibility());
                }
                Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), R.anim.exo_player_back_button_up);
                loadAnimation2.setDuration(250);
                loadAnimation2.setInterpolator(accelerateInterpolator);
                this.A0H.startAnimation(loadAnimation2);
                frameLayout.startAnimation(this.A00);
                this.A0I.startAnimation(loadAnimation);
            }
            if (this.A09) {
                FrameLayout frameLayout2 = this.A0E;
                if (frameLayout2.getVisibility() == 0 && (r0 = this.A01) != null && r0.AFj()) {
                    if (this.A01.AFl() == 3 || this.A01.AFl() == 2) {
                        frameLayout2.setVisibility(4);
                        frameLayout2.startAnimation(this.A00);
                        View view = this.A0D;
                        view.setVisibility(4);
                        view.startAnimation(loadAnimation);
                    }
                }
            }
        }
    }

    public void A01() {
        if (this.A08) {
            FrameLayout frameLayout = this.A0F;
            frameLayout.setVisibility(0);
            AbstractC116325Uz r1 = this.A05;
            if (r1 != null) {
                r1.AYP(frameLayout.getVisibility());
            }
            DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(1.5f);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, getAlpha());
            alphaAnimation.setDuration(250);
            alphaAnimation.setInterpolator(decelerateInterpolator);
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.exo_player_controls_up);
            loadAnimation.setDuration(250);
            loadAnimation.setInterpolator(decelerateInterpolator);
            Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), R.anim.exo_player_back_button_down);
            loadAnimation2.setDuration(250);
            loadAnimation2.setInterpolator(decelerateInterpolator);
            FrameLayout frameLayout2 = this.A0E;
            if (frameLayout2.getVisibility() == 4 && this.A09) {
                frameLayout2.setVisibility(0);
                frameLayout2.startAnimation(alphaAnimation);
                this.A0G.sendAccessibilityEvent(8);
            }
            frameLayout.startAnimation(alphaAnimation);
            this.A0I.startAnimation(loadAnimation);
            this.A0H.startAnimation(loadAnimation2);
            View view = this.A0D;
            view.setVisibility(0);
            view.startAnimation(loadAnimation);
            A04();
            A03();
            A05();
        }
    }

    public void A02() {
        if (this.A09) {
            this.A0E.setVisibility(0);
            this.A0D.setVisibility(0);
        }
        this.A0F.setVisibility(4);
        A04();
        A03();
        A05();
    }

    public final void A03() {
        Timeline timeline;
        if (A07()) {
            boolean z = true;
            if (this.A07 == null) {
                AnonymousClass2B0 r0 = this.A01;
                if (r0 != null) {
                    timeline = r0.ACE();
                } else {
                    timeline = null;
                }
                z = false;
                if (timeline != null && !timeline.A0D()) {
                    int ACF = this.A01.ACF();
                    C94404bl r2 = this.A0M;
                    timeline.A0B(r2, ACF, 0);
                    z = r2.A0D;
                }
            }
            this.A0J.setEnabled(z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r0.AFj() == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04() {
        /*
            r4 = this;
            android.widget.FrameLayout r0 = r4.A0E
            int r1 = r0.getVisibility()
            r0 = 4
            if (r1 == r0) goto L_0x0039
            X.2B0 r0 = r4.A01
            if (r0 == 0) goto L_0x0014
            boolean r0 = r0.AFj()
            r3 = 1
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            android.widget.ImageButton r2 = r4.A0G
            r0 = 2131232207(0x7f0805cf, float:1.8080517E38)
            if (r3 == 0) goto L_0x001f
            r0 = 2131232205(0x7f0805cd, float:1.8080513E38)
        L_0x001f:
            r2.setImageResource(r0)
            X.018 r0 = r4.A02
            r1 = 2131888146(0x7f120812, float:1.941092E38)
            if (r3 == 0) goto L_0x002c
            r1 = 2131888145(0x7f120811, float:1.9410917E38)
        L_0x002c:
            android.content.Context r0 = r0.A00
            android.content.res.Resources r0 = r0.getResources()
            java.lang.String r0 = r0.getString(r1)
            r2.setContentDescription(r0)
        L_0x0039:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.videoplayback.ExoPlaybackControlView.A04():void");
    }

    public final void A05() {
        SeekBar seekBar;
        int i;
        int AFl;
        int i2;
        long AB1;
        long ACb;
        if (A07()) {
            long j = 0;
            if (this.A07 == null) {
                AnonymousClass2B0 r0 = this.A01;
                if (r0 == null) {
                    ACb = 0;
                } else {
                    ACb = r0.ACb();
                }
                String A01 = AnonymousClass2Bd.A01(this.A0Q, this.A0R, ACb);
                TextView textView = this.A0K;
                if (textView.getText() == null || !A01.equals(textView.getText().toString())) {
                    textView.setText(A01);
                }
            }
            if (this.A0C) {
                AnonymousClass2B0 r02 = this.A01;
                if (r02 == null) {
                    AB1 = 0;
                } else {
                    AB1 = r02.AB1();
                }
                seekBar = this.A0J;
                long duration = getDuration();
                if (duration == -9223372036854775807L || duration == 0) {
                    i = 0;
                } else {
                    i = (int) ((AB1 * 1000) / duration);
                }
            } else {
                seekBar = this.A0J;
                i = 1000;
            }
            seekBar.setSecondaryProgress(i);
            AnonymousClass2B0 r03 = this.A01;
            if (r03 != null) {
                j = r03.AC9();
            }
            if (!this.A0A) {
                String A012 = AnonymousClass2Bd.A01(this.A0Q, this.A0R, j);
                TextView textView2 = this.A0L;
                if (textView2.getText() == null || !A012.equals(textView2.getText().toString())) {
                    textView2.setText(A012);
                }
            }
            if (!this.A0A) {
                long duration2 = getDuration();
                if (duration2 == -9223372036854775807L || duration2 == 0) {
                    i2 = 0;
                } else {
                    i2 = (int) ((j * 1000) / duration2);
                }
                seekBar.setProgress(i2);
            }
            Runnable runnable = this.A0P;
            removeCallbacks(runnable);
            AnonymousClass2B0 r3 = this.A01;
            if (r3 != null && (AFl = r3.AFl()) != 1 && AFl != 4) {
                long j2 = 1000;
                if (this.A01.AFj() && AFl == 3) {
                    long j3 = 1000 - (j % 1000);
                    j2 = j3 < 200 ? 1000 + j3 : j3;
                }
                postDelayed(runnable, j2);
            }
        }
    }

    public void A06(int i) {
        Runnable runnable = this.A0O;
        removeCallbacks(runnable);
        AnonymousClass2B0 r0 = this.A01;
        if (r0 != null && r0.AFj()) {
            postDelayed(runnable, (long) i);
        }
        if (this.A00 != null) {
            clearAnimation();
            this.A00 = null;
        }
    }

    public boolean A07() {
        return this.A0F.getVisibility() == 0;
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        AnonymousClass2B0 r4;
        long j;
        AnonymousClass2B0 r3;
        int i;
        if (this.A01 == null || keyEvent.getAction() != 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 21) {
            if (keyCode != 22) {
                if (keyCode == 85) {
                    AnonymousClass2B0 r1 = this.A01;
                    r1.AcW(!r1.AFj());
                } else if (keyCode == 126) {
                    this.A01.AcW(true);
                } else if (keyCode != 127) {
                    switch (keyCode) {
                        case 87:
                            AnonymousClass2B0 r0 = this.A01;
                            AnonymousClass009.A05(r0);
                            Timeline ACE = r0.ACE();
                            if (ACE != null) {
                                int ACF = this.A01.ACF();
                                if (ACF < ACE.A01() - 1) {
                                    r3 = this.A01;
                                    i = ACF + 1;
                                } else if (ACE.A0B(this.A0M, ACF, 0).A0A) {
                                    r3 = this.A01;
                                    i = r3.ACF();
                                }
                                r3.AbS(i, -9223372036854775807L);
                                break;
                            }
                            break;
                        case 88:
                            AnonymousClass2B0 r02 = this.A01;
                            AnonymousClass009.A05(r02);
                            Timeline ACE2 = r02.ACE();
                            if (ACE2 != null) {
                                int ACF2 = this.A01.ACF();
                                C94404bl r12 = this.A0M;
                                ACE2.A0B(r12, ACF2, 0);
                                if (ACF2 > 0 && (this.A01.AC9() <= 3000 || (r12.A0A && !r12.A0D))) {
                                    r3 = this.A01;
                                    i = ACF2 - 1;
                                    r3.AbS(i, -9223372036854775807L);
                                    break;
                                } else {
                                    AnonymousClass2B0 r13 = this.A01;
                                    r13.AbS(r13.ACF(), 0);
                                    break;
                                }
                            }
                            break;
                        case 89:
                            break;
                        case 90:
                            break;
                        default:
                            return false;
                    }
                } else {
                    this.A01.AcW(false);
                }
                A01();
                return true;
            }
            r4 = this.A01;
            AnonymousClass009.A05(r4);
            j = Math.min(r4.AC9() + 15000, this.A01.ACb());
            r4.AbS(r4.ACF(), j);
            A01();
            return true;
        }
        r4 = this.A01;
        AnonymousClass009.A05(r4);
        j = Math.max(r4.AC9() - 5000, 0L);
        r4.AbS(r4.ACF(), j);
        A01();
        return true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public long getDuration() {
        Long l = this.A07;
        if (l != null) {
            return l.longValue();
        }
        AnonymousClass2B0 r0 = this.A01;
        if (r0 == null) {
            return -9223372036854775807L;
        }
        return r0.ACb();
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int i = configuration.orientation;
        Context context = getContext();
        float f = 30.0f;
        if (i == 2) {
            f = 20.0f;
        }
        int i2 = (int) ((f * context.getResources().getDisplayMetrics().density) + 0.5f);
        TextView textView = this.A0L;
        textView.setPadding(textView.getPaddingLeft(), i2, textView.getPaddingRight(), i2);
        SeekBar seekBar = this.A0J;
        seekBar.setPadding(seekBar.getPaddingLeft(), i2, seekBar.getPaddingRight(), i2);
        TextView textView2 = this.A0K;
        textView2.setPadding(textView2.getPaddingLeft(), i2, textView2.getPaddingRight(), i2);
    }

    public void setAllowControlFrameVisibilityChanges(boolean z) {
        this.A08 = z;
    }

    public void setDuration(long j) {
        Long valueOf = Long.valueOf(j);
        this.A07 = valueOf;
        this.A0K.setText(AnonymousClass2Bd.A01(this.A0Q, this.A0R, valueOf.longValue()));
        A05();
        A03();
    }

    public void setPlayButtonClickListener(AbstractC115455Rp r1) {
        this.A03 = r1;
    }

    public void setPlayControlVisibility(int i) {
        boolean z = false;
        if (i == 0) {
            z = true;
        }
        this.A09 = z;
        this.A0E.setVisibility(i);
        this.A0D.setVisibility(i);
    }

    public void setPlayer(AnonymousClass2B0 r3) {
        AnonymousClass2B0 r1 = this.A01;
        if (r1 != null) {
            r1.AaK(this.A0N);
        }
        this.A01 = r3;
        if (r3 != null) {
            r3.A5h(this.A0N);
        }
        A04();
        A03();
        A05();
    }

    public void setSeekbarStartTrackingTouchListener(AbstractC116315Uy r1) {
        this.A04 = r1;
    }

    public void setStreaming(boolean z) {
        this.A0C = z;
    }

    public void setVisibilityListener(AbstractC116325Uz r1) {
        this.A05 = r1;
    }
}
