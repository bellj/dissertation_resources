package com.whatsapp.videoplayback;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.AnonymousClass39B;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/* loaded from: classes2.dex */
public class YoutubePlayerTouchOverlay extends RelativeLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass39B A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public YoutubePlayerTouchOverlay(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A00 = 400;
        A00();
    }

    public YoutubePlayerTouchOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A00 = 400;
        A00();
    }

    public YoutubePlayerTouchOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A00 = 400;
        A00();
    }

    public YoutubePlayerTouchOverlay(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams.setMargins(0, 0, 0, 0);
        setLayoutParams(layoutParams);
        requestLayout();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2) {
            setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.A00);
            layoutParams.addRule(13);
            setLayoutParams(layoutParams);
        }
        requestLayout();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        View childAt = getChildAt(0);
        if (childAt == null) {
            return super.onTouchEvent(motionEvent);
        }
        if (action == 3) {
            motionEvent.setAction(1);
            childAt.onTouchEvent(motionEvent);
        } else if (action != 2) {
            childAt.onTouchEvent(motionEvent);
            AnonymousClass39B r0 = this.A01;
            if (r0 != null) {
                r0.A01();
                return true;
            }
        }
        return true;
    }

    @Override // android.view.View
    public boolean performClick() {
        return super.performClick();
    }

    public void setInlineVideoPlaybackControlView(AnonymousClass39B r1) {
        this.A01 = r1;
    }

    public void setVideoHeight(int i) {
        this.A00 = i;
    }
}
