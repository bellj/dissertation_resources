package com.whatsapp.status;

import X.AbstractC33401dy;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass1BD;
import X.AnonymousClass1E9;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.RunnableC55712jB;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class StatusConfirmUnmuteDialogFragment extends Hilt_StatusConfirmUnmuteDialogFragment {
    public C15550nR A00;
    public C15610nY A01;
    public AnonymousClass018 A02;
    public AbstractC33401dy A03;
    public AnonymousClass1E9 A04;
    public AnonymousClass1BD A05;

    public static StatusConfirmUnmuteDialogFragment A00(UserJid userJid, Long l, String str, String str2, String str3) {
        long j;
        StatusConfirmUnmuteDialogFragment statusConfirmUnmuteDialogFragment = new StatusConfirmUnmuteDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", userJid.getRawString());
        A0D.putString("message_id", str);
        if (l != null) {
            j = l.longValue();
        } else {
            j = 0;
        }
        A0D.putLong("status_item_index", j);
        A0D.putString("psa_campaign_id", str2);
        A0D.putString("psa_campaign_ids", str3);
        statusConfirmUnmuteDialogFragment.A0U(A0D);
        return statusConfirmUnmuteDialogFragment;
    }

    public static /* synthetic */ void A01(UserJid userJid, StatusConfirmUnmuteDialogFragment statusConfirmUnmuteDialogFragment) {
        StringBuilder A0k = C12960it.A0k("statusesfragment/unmute status for ");
        A0k.append(userJid);
        C12960it.A1F(A0k);
        statusConfirmUnmuteDialogFragment.A04.A01(userJid, true);
        AnonymousClass1BD r3 = statusConfirmUnmuteDialogFragment.A05;
        String string = statusConfirmUnmuteDialogFragment.A03().getString("message_id");
        Long valueOf = Long.valueOf(statusConfirmUnmuteDialogFragment.A03().getLong("status_item_index"));
        String string2 = statusConfirmUnmuteDialogFragment.A03().getString("psa_campaign_id");
        String string3 = statusConfirmUnmuteDialogFragment.A03().getString("psa_campaign_ids");
        r3.A0D.Ab2(new RunnableC55712jB(userJid, r3, C12970iu.A0g(), valueOf, string2, string, string3));
        statusConfirmUnmuteDialogFragment.A1B();
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        try {
            this.A03 = (AbstractC33401dy) A09();
        } catch (ClassCastException unused) {
            throw new ClassCastException("Calling fragment must implement Host interface");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        this.A03.AP7(this, true);
        UserJid nullable = UserJid.getNullable(A03().getString("jid"));
        AnonymousClass009.A05(nullable);
        C15370n3 A0B = this.A00.A0B(nullable);
        C004802e A0K = C12960it.A0K(this);
        A0K.setTitle(C12970iu.A0q(this, C15610nY.A01(this.A01, A0B), new Object[1], 0, R.string.unmute_status_confirmation_title));
        A0K.A0A(C12970iu.A0q(this, this.A01.A04(A0B), new Object[1], 0, R.string.unmute_status_confirmation_message));
        C12970iu.A1K(A0K, this, 67, R.string.cancel);
        A0K.setPositiveButton(R.string.unmute_status, new IDxCListenerShape4S0200000_2_I1(nullable, 15, this));
        return A0K.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.A03.AP7(this, false);
    }
}
