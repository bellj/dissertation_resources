package com.whatsapp.status;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC27571Ib;
import X.AbstractC32491cF;
import X.AbstractC34031fT;
import X.AbstractC49562Lg;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass47X;
import X.C004802e;
import X.C103524qt;
import X.C1111958l;
import X.C1112358p;
import X.C1113258y;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C237913a;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C36021jC;
import X.C36391jo;
import X.C36681kM;
import X.C41691tw;
import X.C49552Lf;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.emoji.EmojiEditTextBottomSheetDialogFragment;
import com.whatsapp.status.SetStatus;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class SetStatus extends ActivityC13790kL implements AbstractC27571Ib {
    public static ArrayList A09;
    public View A00;
    public C237913a A01;
    public TextEmojiLabel A02;
    public AnonymousClass10S A03;
    public C36681kM A04;
    public boolean A05;
    public boolean A06;
    public final Handler A07;
    public final C27131Gd A08;

    @Override // X.AbstractC27571Ib
    public void ANI(String str) {
    }

    public SetStatus() {
        this(0);
        this.A07 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.4iP
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                SetStatus setStatus = SetStatus.this;
                if (message.what == 1) {
                    setStatus.A01.A02((String) message.obj, null);
                } else {
                    ((ActivityC13810kN) setStatus).A05.A07(R.string.info_update_failed, 0);
                }
                C36021jC.A00(setStatus, 2);
                return true;
            }
        });
        this.A08 = new C36391jo(this);
    }

    public SetStatus(int i) {
        this.A05 = false;
        A0R(new C103524qt(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A01 = (C237913a) r1.ACt.get();
            this.A03 = (AnonymousClass10S) r1.A46.get();
        }
    }

    public final void A2e() {
        Adm(EmojiEditTextBottomSheetDialogFragment.A00(this.A01.A00(), null, 4, R.string.add_info, R.string.no_empty_info, 139, 16385));
    }

    public final void A2f() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(openFileOutput("status", 0));
            StringBuilder sb = new StringBuilder();
            Iterator it = A09.iterator();
            while (it.hasNext()) {
                sb.append((String) it.next());
                sb.append("\n");
            }
            if (sb.length() > 1) {
                sb.deleteCharAt(sb.length() - 1);
            }
            objectOutputStream.writeObject(sb.toString());
            objectOutputStream.close();
        } catch (IOException e) {
            Log.e("SetStatus/writeStatusListString", e);
        }
    }

    public void A2g(String str) {
        if (!A2S(R.string.about_update_no_network)) {
            C36021jC.A01(this, 2);
            C237913a r1 = this.A01;
            C1113258y r6 = new AbstractC49562Lg() { // from class: X.58y
                @Override // X.AbstractC49562Lg
                public final void Ab3(String str2) {
                    SetStatus setStatus = SetStatus.this;
                    setStatus.A06 = true;
                    Handler handler = setStatus.A07;
                    handler.removeMessages(0);
                    handler.sendMessage(Message.obtain(handler, 1, str2));
                }
            };
            C1112358p r5 = new AbstractC32491cF() { // from class: X.58p
                @Override // X.AbstractC32491cF
                public final void Aaw(int i) {
                    Handler handler = SetStatus.this.A07;
                    handler.removeMessages(0);
                    handler.sendEmptyMessage(0);
                }
            };
            C1111958l r4 = new AbstractC34031fT() { // from class: X.58l
                @Override // X.AbstractC34031fT
                public final void Ab0(Exception exc) {
                    Handler handler = SetStatus.this.A07;
                    handler.removeMessages(0);
                    handler.sendEmptyMessage(0);
                }
            };
            if (r1.A06.A06) {
                r1.A09.A08(Message.obtain(null, 0, 29, 0, new C49552Lf(r4, r5, r6, null, str)), false);
            } else {
                Handler handler = this.A07;
                handler.removeMessages(0);
                handler.sendEmptyMessage(0);
            }
            this.A07.sendEmptyMessageDelayed(0, 32000);
        }
    }

    @Override // X.AbstractC27571Ib
    public void APb(int i, String str) {
        if (i == 4 && str.length() > 0 && !str.equals(this.A02.getText().toString())) {
            this.A00.setOnClickListener(null);
            A2g(str);
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        if (menuItem.getItemId() == 1) {
            A09.remove(adapterContextMenuInfo.position);
            this.A04.notifyDataSetChanged();
            A2f();
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        TypedArray obtainTypedArray;
        super.onCreate(bundle);
        setTitle(R.string.my_info);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.setstatus);
        View findViewById = findViewById(R.id.status_layout);
        this.A00 = findViewById;
        findViewById.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 34));
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.status_tv);
        this.A02 = textEmojiLabel;
        textEmojiLabel.A0G(null, this.A01.A00());
        try {
            if (new File(getFilesDir(), "status").exists()) {
                try {
                    ObjectInputStream objectInputStream = new ObjectInputStream(openFileInput("status"));
                    try {
                        ArrayList arrayList = new ArrayList();
                        A09 = arrayList;
                        String[] split = ((String) objectInputStream.readObject()).split("\n");
                        for (String str : split) {
                            if (str.length() > 0) {
                                arrayList.add(str);
                            }
                        }
                        objectInputStream.close();
                    } catch (Throwable th) {
                        try {
                            objectInputStream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IOException e) {
                    Log.e(e);
                } catch (ClassNotFoundException e2) {
                    Log.w("create/status/serialization_error", e2);
                }
                AbsListView absListView = (AbsListView) findViewById(R.id.list);
                absListView.setEmptyView(findViewById(R.id.list_empty));
                C36681kM r0 = new C36681kM(this, this, A09);
                this.A04 = r0;
                absListView.setAdapter((ListAdapter) r0);
                absListView.setOnItemClickListener(new AnonymousClass47X(this));
                registerForContextMenu(absListView);
                this.A03.A03(this.A08);
                AnonymousClass2GE.A07((ImageView) findViewById(R.id.status_tv_edit_icon), C41691tw.A00(this, R.attr.settingsGrayButtonColor, R.color.settings_edit_btn));
                return;
            }
            ArrayList arrayList2 = new ArrayList();
            for (int i = 0; i < obtainTypedArray.length(); i++) {
                arrayList2.add(obtainTypedArray.getString(i));
            }
            obtainTypedArray.recycle();
            A09 = arrayList2;
            AbsListView absListView = (AbsListView) findViewById(R.id.list);
            absListView.setEmptyView(findViewById(R.id.list_empty));
            C36681kM r0 = new C36681kM(this, this, A09);
            this.A04 = r0;
            absListView.setAdapter((ListAdapter) r0);
            absListView.setOnItemClickListener(new AnonymousClass47X(this));
            registerForContextMenu(absListView);
            this.A03.A03(this.A08);
            AnonymousClass2GE.A07((ImageView) findViewById(R.id.status_tv_edit_icon), C41691tw.A00(this, R.attr.settingsGrayButtonColor, R.color.settings_edit_btn));
            return;
        } catch (Throwable th2) {
            obtainTypedArray.recycle();
            throw th2;
        }
        obtainTypedArray = getResources().obtainTypedArray(R.array.default_statuses);
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        contextMenu.add(0, 1, 0, R.string.delete_info);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        String str;
        int i2;
        if (i == 0) {
            str = getString(R.string.info_update_dialog_title);
            i2 = R.string.info_update_dialog_message;
        } else if (i == 1) {
            str = getString(R.string.info_retrieve_dialog_title);
            i2 = R.string.info_retrieve_dialog_message;
        } else if (i == 2) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.info_update_dialog_title));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            return progressDialog;
        } else if (i != 3) {
            return super.onCreateDialog(i);
        } else {
            C004802e r2 = new C004802e(this);
            r2.A06(R.string.delete_all_confirm);
            r2.setPositiveButton(R.string.delete_all, new DialogInterface.OnClickListener() { // from class: X.4gC
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    SetStatus setStatus = SetStatus.this;
                    C36021jC.A00(setStatus, 3);
                    SetStatus.A09.clear();
                    setStatus.A2f();
                    setStatus.A04.notifyDataSetChanged();
                }
            });
            r2.setNegativeButton(R.string.cancel, null);
            return r2.create();
        }
        return ProgressDialog.show(this, str, getString(i2), true, false);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.delete_all);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A03.A04(this.A08);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId != 0) {
            if (itemId != 16908332) {
                return super.onOptionsItemSelected(menuItem);
            }
            finish();
            return true;
        } else if (A09.size() == 0) {
            Ado(R.string.no_info_to_delete);
            return true;
        } else {
            C36021jC.A01(this, 3);
            return true;
        }
    }
}
