package com.whatsapp.status;

import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1BD;
import X.AnonymousClass2FL;
import X.C18470sV;
import X.C20670w8;
import X.C32731ce;

/* loaded from: classes2.dex */
public class StatusTemporalRecipientsActivity extends StatusRecipientsActivity {
    public C32731ce A00;
    public boolean A01;

    public StatusTemporalRecipientsActivity() {
        this(0);
    }

    public StatusTemporalRecipientsActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 126);
    }

    @Override // X.AbstractActivityC58282oS, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0M(this, A1M, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ((StatusRecipientsActivity) this).A01 = (C18470sV) A1M.AK8.get();
            ((StatusRecipientsActivity) this).A00 = (C20670w8) A1M.AMw.get();
            ((StatusRecipientsActivity) this).A02 = (AnonymousClass1BD) A1M.AKA.get();
        }
    }
}
