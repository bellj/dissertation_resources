package com.whatsapp.status;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1BD;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103534qu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20670w8;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C626337z;
import X.C87934Dp;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.ScrollView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;

/* loaded from: classes2.dex */
public class StatusPrivacyActivity extends ActivityC13790kL {
    public RadioButton A00;
    public RadioButton A01;
    public RadioButton A02;
    public ScrollView A03;
    public C20670w8 A04;
    public C18470sV A05;
    public AnonymousClass1BD A06;
    public AnonymousClass01H A07;
    public boolean A08;

    public StatusPrivacyActivity() {
        this(0);
    }

    public StatusPrivacyActivity(int i) {
        this.A08 = false;
        A0R(new C103534qu(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A05 = (C18470sV) r1.AK8.get();
            this.A04 = (C20670w8) r1.AMw.get();
            this.A06 = (AnonymousClass1BD) r1.AKA.get();
            this.A07 = C18000rk.A00(r1.A4v);
        }
    }

    public final void A2e() {
        if (this.A01.isChecked()) {
            Ady(R.string.processing, R.string.register_wait_message);
            ((ActivityC13830kP) this).A05.Aaz(new C626337z(((ActivityC13810kN) this).A05, this.A04, this.A05, this, this.A06), new Void[0]);
            return;
        }
        setResult(-1, C87934Dp.A00(getIntent()));
        finish();
    }

    public final void A2f() {
        RadioButton radioButton;
        int A00 = this.A05.A03.A00("status_distribution", 0);
        if (A00 == 0) {
            radioButton = this.A01;
        } else if (A00 == 1) {
            radioButton = this.A02;
        } else if (A00 == 2) {
            radioButton = this.A00;
        } else {
            throw new IllegalStateException("unknown status distribution mode");
        }
        radioButton.setChecked(true);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 0) {
            A2f();
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.status_privacy);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0A(R.string.status_privacy);
        this.A03 = (ScrollView) findViewById(R.id.scroll_view);
        this.A01 = (RadioButton) findViewById(R.id.my_contacts_button);
        this.A00 = (RadioButton) findViewById(R.id.my_contacts_except_button);
        this.A02 = (RadioButton) findViewById(R.id.only_share_with_button);
        A2f();
        this.A01.setText(R.string.select_status_recipients_my_contacts);
        this.A00.setText(R.string.select_status_recipients_black_list);
        this.A02.setText(R.string.select_status_recipients_white_list);
        this.A01.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 29));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 30));
        this.A02.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 31));
        if (!this.A05.A0F()) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 3));
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2e();
        return false;
    }
}
