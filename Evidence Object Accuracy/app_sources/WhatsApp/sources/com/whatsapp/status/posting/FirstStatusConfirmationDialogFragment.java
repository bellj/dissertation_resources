package com.whatsapp.status.posting;

import X.AnonymousClass018;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C18470sV;
import X.C20670w8;
import X.C52232aT;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class FirstStatusConfirmationDialogFragment extends Hilt_FirstStatusConfirmationDialogFragment {
    public TextView A00;
    public C20670w8 A01;
    public AnonymousClass018 A02;
    public C18470sV A03;

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 0) {
            this.A00.setText(A1K());
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        View A0F = C12960it.A0F(A0B().getLayoutInflater(), null, R.layout.first_status_confirmation);
        TextView A0J = C12960it.A0J(A0F, R.id.text);
        this.A00 = A0J;
        A0J.setText(A1K());
        C12990iw.A1F(this.A00);
        C004802e A0O = C12970iu.A0O(this);
        A0O.setView(A0F);
        A0O.A0B(true);
        C12970iu.A1M(A0O, this, 32, R.string.send);
        C12970iu.A1K(A0O, this, 69, R.string.cancel);
        return A0O.create();
    }

    public final Spanned A1K() {
        String A0I;
        int size;
        AnonymousClass018 r4;
        int i;
        int A00 = this.A03.A03.A00("status_distribution", 0);
        if (A00 != 0) {
            if (A00 == 1) {
                size = this.A03.A06().size();
                r4 = this.A02;
                i = R.plurals.first_status_selected_contacts;
            } else if (A00 == 2) {
                size = this.A03.A07().size();
                if (size != 0) {
                    r4 = this.A02;
                    i = R.plurals.first_status_excluded_contacts;
                }
            } else {
                throw C12960it.A0U("unknown status distribution mode");
            }
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, size, 0);
            A0I = r4.A0I(objArr, i, (long) size);
            SpannableStringBuilder A0J = C12990iw.A0J(A0I);
            SpannableStringBuilder A0J2 = C12990iw.A0J(A0I(R.string.change_privacy_settings));
            A0J2.setSpan(new C52232aT(this), 0, A0J2.length(), 33);
            A0J.append((CharSequence) " ");
            A0J.append((CharSequence) A0J2);
            return A0J;
        }
        A0I = A0I(R.string.first_status_all_contacts);
        SpannableStringBuilder A0J = C12990iw.A0J(A0I);
        SpannableStringBuilder A0J2 = C12990iw.A0J(A0I(R.string.change_privacy_settings));
        A0J2.setSpan(new C52232aT(this), 0, A0J2.length(), 33);
        A0J.append((CharSequence) " ");
        A0J.append((CharSequence) A0J2);
        return A0J;
    }
}
