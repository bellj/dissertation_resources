package com.whatsapp.status;

import X.AbstractC51382Uh;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.whatsapp.components.button.ThumbnailButton;

/* loaded from: classes3.dex */
public class ScalingContactStatusThumbnail extends AbstractC51382Uh {
    public boolean A00 = false;

    public ScalingContactStatusThumbnail(Context context) {
        super(context);
    }

    public ScalingContactStatusThumbnail(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ScalingContactStatusThumbnail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // com.whatsapp.status.ContactStatusThumbnail, com.whatsapp.components.button.ThumbnailButton
    public void A02(Canvas canvas) {
        if (this.A00) {
            super.A02(canvas);
        }
    }

    @Override // com.whatsapp.status.ContactStatusThumbnail
    public void A03(int i, int i2) {
        if (((ContactStatusThumbnail) this).A02 == 0) {
            requestLayout();
        }
        super.A03(i, i2);
    }

    @Override // com.whatsapp.components.button.ThumbnailButton
    public int getBorderSizeAdjustment() {
        return (int) ((ThumbnailButton) this).A01;
    }

    @Override // com.whatsapp.components.button.ThumbnailButton, android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (((ContactStatusThumbnail) this).A02 > 0) {
            ((ThumbnailButton) this).A01 = ((float) getMeasuredWidth()) * 0.04f;
        }
    }

    public void setShowRing(boolean z) {
        this.A00 = z;
    }
}
