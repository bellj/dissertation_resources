package com.whatsapp.status;

import X.AbstractC116195Um;
import X.AbstractC15340mz;
import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass19M;
import X.C004802e;
import X.C12970iu;
import X.C14900mE;
import X.C15650ng;
import X.C16170oZ;
import X.C38211ni;
import X.C63203Ar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import java.util.Collections;
import java.util.Set;

/* loaded from: classes2.dex */
public class StatusDeleteDialogFragment extends Hilt_StatusDeleteDialogFragment {
    public C14900mE A00;
    public C16170oZ A01;
    public AnonymousClass018 A02;
    public C15650ng A03;
    public AnonymousClass19M A04;
    public StatusPlaybackContactFragment A05;
    public AnonymousClass01H A06;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        try {
            this.A05 = (StatusPlaybackContactFragment) A09();
        } catch (ClassCastException unused) {
            throw new ClassCastException("Calling fragment must implement Host interface");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        this.A05.AP7(this, true);
        AbstractC15340mz A03 = this.A03.A0K.A03(C38211ni.A03(A03(), ""));
        Set set = null;
        ActivityC000900k A0B = A0B();
        C14900mE r2 = this.A00;
        AnonymousClass19M r4 = this.A04;
        C16170oZ r3 = this.A01;
        if (A03 != null) {
            set = Collections.singleton(A03);
        }
        Dialog A00 = C63203Ar.A00(A0B, r2, r3, r4, new AbstractC116195Um() { // from class: X.59K
            @Override // X.AbstractC116195Um
            public final void AOu() {
            }
        }, set);
        if (A00 != null) {
            return A00;
        }
        C004802e A0O = C12970iu.A0O(this);
        A0O.A06(R.string.status_deleted);
        return A0O.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.A05.AP7(this, false);
    }
}
