package com.whatsapp.status;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10U;
import X.AnonymousClass116;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass187;
import X.AnonymousClass1AR;
import X.AnonymousClass1BD;
import X.AnonymousClass1CW;
import X.AnonymousClass1m4;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16590pI;
import X.C17050qB;
import X.C18000rk;
import X.C18470sV;
import X.C21270x9;
import X.C22330yu;
import X.C242714w;
import X.C244015j;
import X.C244215l;
import X.C248016x;
import X.C248116y;
import X.C252518r;
import X.C253018w;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaListFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_StatusesFragment extends WaListFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A01() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A01();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A01()
            r3.A19()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.Hilt_StatusesFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A01();
        A19();
    }

    public void A19() {
        if (!this.A02) {
            this.A02 = true;
            StatusesFragment statusesFragment = (StatusesFragment) this;
            C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r2 = r3.A0Y;
            ((WaListFragment) statusesFragment).A00 = (AnonymousClass182) r2.A94.get();
            ((WaListFragment) statusesFragment).A01 = (AnonymousClass180) r2.ALt.get();
            statusesFragment.A0L = (C14830m7) r2.ALb.get();
            statusesFragment.A0W = (C14850m9) r2.A04.get();
            statusesFragment.A06 = (C14900mE) r2.A8X.get();
            statusesFragment.A0b = (C253018w) r2.AJS.get();
            statusesFragment.A0M = (C16590pI) r2.AMg.get();
            statusesFragment.A07 = (C15570nT) r2.AAr.get();
            statusesFragment.A0r = (AbstractC14440lR) r2.ANe.get();
            statusesFragment.A0c = (AnonymousClass12U) r2.AJd.get();
            statusesFragment.A0X = (C16120oU) r2.ANE.get();
            statusesFragment.A08 = (C15450nH) r2.AII.get();
            statusesFragment.A0V = (C18470sV) r2.AK8.get();
            statusesFragment.A09 = (AnonymousClass1AR) r2.ALM.get();
            statusesFragment.A0A = (C16170oZ) r2.AM4.get();
            statusesFragment.A0Q = (C14950mJ) r2.AKf.get();
            statusesFragment.A05 = (AnonymousClass12P) r2.A0H.get();
            statusesFragment.A0H = (C21270x9) r2.A4A.get();
            statusesFragment.A0a = (AnonymousClass17R) r2.AIz.get();
            statusesFragment.A0D = (C15550nR) r2.A45.get();
            statusesFragment.A0K = (AnonymousClass01d) r2.ALI.get();
            statusesFragment.A0F = (C15610nY) r2.AMe.get();
            statusesFragment.A0P = (AnonymousClass018) r2.ANb.get();
            statusesFragment.A0R = (C15650ng) r2.A4m.get();
            statusesFragment.A0E = (AnonymousClass10S) r2.A46.get();
            statusesFragment.A0S = (AnonymousClass12H) r2.AC5.get();
            statusesFragment.A0I = r3.A0V.A05();
            statusesFragment.A0Z = (C15860o1) r2.A3H.get();
            statusesFragment.A0J = (C17050qB) r2.ABL.get();
            AnonymousClass1m4 r32 = new AnonymousClass1m4((C16170oZ) r2.AM4.get());
            r32.A00 = (AbstractC14440lR) r2.ANe.get();
            r32.A01 = C18000rk.A00(r2.A4v);
            statusesFragment.A0n = r32;
            statusesFragment.A0B = (C22330yu) r2.A3I.get();
            statusesFragment.A0T = (AnonymousClass10U) r2.AJz.get();
            statusesFragment.A0C = (AnonymousClass116) r2.A3z.get();
            statusesFragment.A0i = (C248116y) r2.AHR.get();
            statusesFragment.A0N = (C15890o4) r2.AN1.get();
            statusesFragment.A0O = (C14820m6) r2.AN3.get();
            statusesFragment.A0f = (C244015j) r2.AK0.get();
            statusesFragment.A0j = (C248016x) r2.AK7.get();
            statusesFragment.A0k = (AnonymousClass1BD) r2.AKA.get();
            statusesFragment.A0l = (AnonymousClass1CW) r2.AJx.get();
            statusesFragment.A0U = (C242714w) r2.AK6.get();
            statusesFragment.A04 = (AnonymousClass187) r2.A0G.get();
            statusesFragment.A0Y = (C244215l) r2.A8y.get();
            statusesFragment.A0m = (C252518r) r2.AK9.get();
            statusesFragment.A0s = C18000rk.A00(r2.A4v);
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
