package com.whatsapp.status;

import X.AbstractActivityC36551k4;
import X.AbstractActivityC58282oS;
import X.ActivityC13810kN;
import X.AnonymousClass1BD;
import X.C18470sV;
import X.C20670w8;
import X.C63223At;
import java.util.Set;

/* loaded from: classes2.dex */
public class StatusRecipientsActivity extends AbstractActivityC58282oS {
    public C20670w8 A00;
    public C18470sV A01;
    public AnonymousClass1BD A02;

    @Override // X.AbstractActivityC36551k4
    public void A2h() {
        super.A2h();
        if (((ActivityC13810kN) this).A0C.A07(1267) && !((AbstractActivityC36551k4) this).A0L) {
            Set set = this.A0U;
            if (set.size() == 0 && ((AbstractActivityC36551k4) this).A02.getVisibility() == 0) {
                C63223At.A00(((AbstractActivityC36551k4) this).A02, false, true);
            } else if (set.size() != 0 && ((AbstractActivityC36551k4) this).A02.getVisibility() == 4) {
                C63223At.A00(((AbstractActivityC36551k4) this).A02, true, true);
            }
        }
    }
}
