package com.whatsapp.status;

import X.AbstractC001200n;
import X.AbstractC116185Ul;
import X.AbstractC13860kS;
import X.AbstractC14440lR;
import X.AbstractC14750lz;
import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC32851cq;
import X.AbstractC33021d9;
import X.AbstractC33331dp;
import X.AbstractC33381dw;
import X.AbstractC33401dy;
import X.AbstractC41661tt;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass02O;
import X.AnonymousClass0R2;
import X.AnonymousClass10S;
import X.AnonymousClass10U;
import X.AnonymousClass116;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass187;
import X.AnonymousClass1AR;
import X.AnonymousClass1BD;
import X.AnonymousClass1CW;
import X.AnonymousClass1J1;
import X.AnonymousClass1US;
import X.AnonymousClass1V2;
import X.AnonymousClass1VX;
import X.AnonymousClass1m4;
import X.AnonymousClass2Dn;
import X.AnonymousClass2GE;
import X.AnonymousClass2I2;
import X.AnonymousClass2LB;
import X.AnonymousClass36R;
import X.AnonymousClass4LB;
import X.AnonymousClass4ON;
import X.C1104655q;
import X.C14810m5;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16590pI;
import X.C17050qB;
import X.C18470sV;
import X.C21230x5;
import X.C21270x9;
import X.C21390xL;
import X.C22330yu;
import X.C22370yy;
import X.C242714w;
import X.C244015j;
import X.C244215l;
import X.C248016x;
import X.C248116y;
import X.C252518r;
import X.C253018w;
import X.C27131Gd;
import X.C28391Mz;
import X.C33471e8;
import X.C33681eu;
import X.C33701ew;
import X.C34271fr;
import X.C35131hI;
import X.C36841kj;
import X.C37371mG;
import X.C38121nY;
import X.C458223h;
import X.C52252aV;
import X.C628238s;
import X.C64153El;
import X.C69643a0;
import X.C72603es;
import X.C850941d;
import X.C858844n;
import X.ViewTreeObserver$OnGlobalLayoutListenerC33691ev;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Space;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.ListFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.EmptyTellAFriendView;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.StatusConfirmMuteDialogFragment;
import com.whatsapp.status.StatusConfirmUnmuteDialogFragment;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class StatusesFragment extends Hilt_StatusesFragment implements AbstractC001200n, AbstractC14940mI, AbstractC14770m1, AbstractC14750lz, AbstractC33021d9, AbstractC33381dw, AbstractC33401dy {
    public AnimatorSet A00;
    public AnimatorSet A01;
    public View A02;
    public View A03;
    public AnonymousClass187 A04;
    public AnonymousClass12P A05;
    public C14900mE A06;
    public C15570nT A07;
    public C15450nH A08;
    public AnonymousClass1AR A09;
    public C16170oZ A0A;
    public C22330yu A0B;
    public AnonymousClass116 A0C;
    public C15550nR A0D;
    public AnonymousClass10S A0E;
    public C15610nY A0F;
    public AnonymousClass1J1 A0G;
    public C21270x9 A0H;
    public AnonymousClass2I2 A0I;
    public C17050qB A0J;
    public AnonymousClass01d A0K;
    public C14830m7 A0L;
    public C16590pI A0M;
    public C15890o4 A0N;
    public C14820m6 A0O;
    public AnonymousClass018 A0P;
    public C14950mJ A0Q;
    public C15650ng A0R;
    public AnonymousClass12H A0S;
    public AnonymousClass10U A0T;
    public C242714w A0U;
    public C18470sV A0V;
    public C14850m9 A0W;
    public C16120oU A0X;
    public C244215l A0Y;
    public C15860o1 A0Z;
    public AnonymousClass17R A0a;
    public C253018w A0b;
    public AnonymousClass12U A0c;
    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev A0d;
    public StatusExpirationLifecycleOwner A0e;
    public C244015j A0f;
    public C33681eu A0g;
    public C64153El A0h;
    public C248116y A0i;
    public C248016x A0j;
    public AnonymousClass1BD A0k;
    public AnonymousClass1CW A0l;
    public C252518r A0m;
    public AnonymousClass1m4 A0n;
    public C37371mG A0o;
    public C628238s A0p;
    public C33701ew A0q = new C33701ew();
    public AbstractC14440lR A0r;
    public AnonymousClass01H A0s;
    public CharSequence A0t;
    public List A0u;
    public List A0v = new ArrayList();
    public List A0w = new ArrayList();
    public boolean A0x;
    public boolean A0y = true;
    public boolean A0z;
    public boolean A10 = false;
    public boolean A11;
    public boolean A12 = false;
    public final AnonymousClass2Dn A13 = new C850941d(this);
    public final C27131Gd A14 = new C36841kj(this);
    public final AbstractC32851cq A15 = new C1104655q(this);
    public final AbstractC18860tB A16 = new C35131hI(this);
    public final AbstractC33331dp A17 = new C858844n(this);
    public final AbstractC41661tt A18 = new AbstractC41661tt() { // from class: X.59I
        @Override // X.AbstractC41661tt
        public final void AWW(AbstractC14640lm r2) {
            StatusesFragment.this.A1B();
        }
    };
    public final Runnable A19 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 6);
    public final List A1A = new ArrayList();
    public final List A1B = new ArrayList();
    public final List A1C = new ArrayList();
    public final List A1D = new ArrayList();
    public final Set A1E = new HashSet();

    @Override // X.AbstractC14940mI
    public String AE5() {
        return null;
    }

    @Override // X.AbstractC33391dx
    public void AP7(DialogFragment dialogFragment, boolean z) {
    }

    @Override // X.AbstractC14770m1
    public boolean Aed() {
        return true;
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        boolean z;
        String str;
        Log.i("statusesFragment/onActivityCreated");
        this.A0k.A0C.ALC(453128091, "CREATE_ACTIVITY_START", 1);
        super.A0m(bundle);
        A0M();
        ListFragment.A00(this);
        ListView listView = ((ListFragment) this).A04;
        listView.setFastScrollEnabled(false);
        listView.setScrollbarFadingEnabled(true);
        listView.setOnItemClickListener(new AnonymousClass36R(this));
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: X.3OJ
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                UserJid userJid;
                StatusesFragment statusesFragment = StatusesFragment.this;
                C55172hu r4 = (C55172hu) view.getTag();
                ActivityC000900k A01 = C13010iy.A01(statusesFragment);
                if (r4 == null || (userJid = r4.A01) == C29831Uv.A00 || A01.A0V().A0m()) {
                    return false;
                }
                C15860o1 r0 = statusesFragment.A0Z;
                AnonymousClass009.A05(userJid);
                boolean z2 = C15860o1.A00(userJid, r0).A0H;
                UserJid userJid2 = r4.A01;
                String A1A = statusesFragment.A1A();
                if (z2) {
                    C51122Sx.A01(StatusConfirmUnmuteDialogFragment.A00(userJid2, null, null, null, A1A), statusesFragment);
                    return true;
                }
                C51122Sx.A01(StatusConfirmMuteDialogFragment.A00(userJid2, null, null, null, A1A), statusesFragment);
                return true;
            }
        });
        if (this.A0O.A00.getBoolean("show_statuses_education", true)) {
            C21390xL r7 = this.A0V.A03;
            AnonymousClass016 r4 = new AnonymousClass016();
            synchronized (r7.A04) {
                Map map = r7.A05;
                if (map.containsKey("status_distribution")) {
                    str = (String) map.get("status_distribution");
                    z = true;
                } else {
                    z = false;
                    str = null;
                }
            }
            if (z) {
                r4.A0A(str);
            } else {
                r7.A03.Ab2(new RunnableBRunnable0Shape0S1200000_I0(r4, r7));
            }
            AnonymousClass0R2.A00(new AnonymousClass02O() { // from class: X.4rR
                @Override // X.AnonymousClass02O
                public final Object apply(Object obj) {
                    return Boolean.valueOf(C12960it.A1W(obj));
                }
            }, r4).A05(A0G(), new AnonymousClass02B(listView, this) { // from class: X.3RQ
                public final /* synthetic */ ListView A00;
                public final /* synthetic */ StatusesFragment A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    StatusesFragment statusesFragment = this.A01;
                    ListView listView2 = this.A00;
                    if (!C12970iu.A1Y(obj)) {
                        if (statusesFragment.A03 == null) {
                            ListFragment.A00(statusesFragment);
                            ListView listView3 = ((ListFragment) statusesFragment).A04;
                            View inflate = statusesFragment.A0C().getLayoutInflater().inflate(R.layout.status_education_row, (ViewGroup) listView3, false);
                            statusesFragment.A03 = inflate;
                            TextView A0J = C12960it.A0J(inflate, R.id.text);
                            Object[] objArr = new Object[1];
                            C12960it.A1P(objArr, 24, 0);
                            A0J.setText(statusesFragment.A0J(R.string.status_education_with_placeholder, objArr));
                            statusesFragment.A03.findViewById(R.id.cancel).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(statusesFragment, 38));
                            statusesFragment.A03.findViewById(R.id.privacy_settings).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(statusesFragment, 37));
                            FrameLayout frameLayout = new FrameLayout(statusesFragment.A01());
                            frameLayout.addView(statusesFragment.A03);
                            listView3.addHeaderView(frameLayout, null, true);
                        }
                        statusesFragment.A03.setVisibility(0);
                        statusesFragment.A1I(listView2);
                    }
                }
            });
        } else {
            A1I(listView);
        }
        A05().findViewById(R.id.init_statuses_progress).setVisibility(0);
        C33681eu r0 = new C33681eu(this);
        this.A0g = r0;
        A18(r0);
        this.A0E.A03(this.A14);
        this.A0S.A03(this.A16);
        this.A0B.A03(this.A13);
        this.A0f.A03(this.A18);
        this.A0Y.A03(this.A17);
        A1B();
        C21230x5 r3 = this.A0k.A0C;
        r3.ALC(453128091, "CREATE_ACTIVITY_END", 1);
        r3.AL6(453128091, 1, 2);
    }

    @Override // com.whatsapp.base.WaListFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        super.A0n(z);
        if (((AnonymousClass01E) this).A03 >= 7 && z) {
            this.A0I.A01(this);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        Log.i("statusesFragment/onPause");
        this.A0j.A04.A04(this);
        ViewTreeObserver$OnGlobalLayoutListenerC33691ev r0 = this.A0d;
        if (r0 != null) {
            r0.A00();
        }
        super.A0r();
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        Log.i("statusesFragment/onStart");
        super.A0s();
        if (this.A04.A02) {
            A1D();
        }
        A1G();
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        C458223h r0;
        if (i != 33) {
            if (i == 35) {
                if (this.A0x) {
                    C64153El r4 = this.A0h;
                    C248116y r02 = r4.A05;
                    r02.A00.post(new RunnableBRunnable0Shape12S0100000_I0_12(r02.A03, 7));
                    AnonymousClass4ON r1 = r4.A04;
                    r1.A01 = true;
                    r1.A00 = false;
                    r4.A00();
                }
                this.A0k.A03();
                if (this.A0x) {
                    this.A0j.A00(intent);
                }
            } else if (i != 151) {
            } else {
                if (i2 == -1) {
                    A1K(this.A0x);
                } else if (i2 == 0 && (r0 = this.A0k.A00) != null) {
                    r0.A01 = 4;
                }
            }
        } else if (i2 == -1) {
            A1F();
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putBoolean("WAS_FB_SHARE_BUTTON_ATTEMPTED_SI_KEY", this.A0x);
        bundle.putBoolean("SHARE_CTA_VISIBILITY_SI_KEY", this.A0h.A04.A00);
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(2, R.id.menuitem_status_privacy, 0, R.string.status_privacy);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        Intent intent;
        if (menuItem.getItemId() == R.id.menuitem_new_status) {
            A1F();
            return true;
        }
        if (menuItem.getItemId() == R.id.menuitem_status_privacy) {
            Context A0p = A0p();
            intent = new Intent();
            intent.setClassName(A0p.getPackageName(), "com.whatsapp.status.StatusPrivacyActivity");
        } else if (menuItem.getItemId() != R.id.menuitem_new_text_status) {
            return false;
        } else {
            Context A0p2 = A0p();
            intent = new Intent();
            intent.setClassName(A0p2.getPackageName(), "com.whatsapp.textstatuscomposer.TextStatusComposerActivity");
            intent.putExtra("origin", 4);
        }
        A0v(intent);
        return true;
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.statuses, viewGroup, false);
        this.A0k.A0C.ALC(453128091, "CREATE_VIEW_START", 1);
        ListView listView = (ListView) AnonymousClass028.A0D(inflate, 16908298);
        if (!C28391Mz.A01()) {
            this.A0I.A00(listView, this);
        }
        HomeActivity.A0A(inflate, this);
        if (!this.A0W.A07(1071)) {
            HomeActivity.A0B(inflate, this, A02().getDimensionPixelSize(R.dimen.statuses_fragment_empty_footer_height));
        }
        this.A0k.A0C.ALC(453128091, "CREATE_VIEW_END", 1);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Log.i("statusesFragment/onDestroy");
        super.A11();
        C64153El r0 = this.A0h;
        AnonymousClass4LB r1 = r0.A01;
        if (r1 != null) {
            r0.A05.A01.A04(r1);
        }
        this.A0G.A00();
        this.A0E.A04(this.A14);
        this.A0S.A04(this.A16);
        this.A0B.A04(this.A13);
        this.A0f.A04(this.A18);
        this.A0Y.A04(this.A17);
        this.A06.A0G(this.A19);
        C628238s r12 = this.A0p;
        if (r12 != null) {
            r12.A03(true);
        }
        C37371mG r13 = this.A0o;
        if (r13 != null) {
            r13.A03(true);
        }
        A1E();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        Log.i("statusesFragment/onResume");
        super.A13();
        this.A0j.A04.A05(this, new AnonymousClass02B() { // from class: X.3Qw
            /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
                if (r4 != 2) goto L_0x0038;
             */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x003f  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0046  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0055  */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r6) {
                /*
                    r5 = this;
                    com.whatsapp.status.StatusesFragment r2 = com.whatsapp.status.StatusesFragment.this
                    X.1Pr r6 = (X.C28961Pr) r6
                    java.lang.String r0 = "statusesFragment/onStatusSharingInfoChanged"
                    com.whatsapp.util.Log.i(r0)
                    if (r6 == 0) goto L_0x002d
                    android.content.Intent r0 = r6.A01
                    if (r0 == 0) goto L_0x002f
                    r2.A1E()
                    java.util.List r1 = r2.A1D
                    java.util.List r0 = r6.A02
                    r1.addAll(r0)
                    boolean r0 = r2.A0x
                    int r1 = X.C12990iw.A04(r0)
                    X.3El r0 = r2.A0h
                    r0.A01(r1)
                    android.content.Intent r1 = r6.A01
                    r0 = 35
                    r2.startActivityForResult(r1, r0)
                    return
                L_0x002d:
                    r4 = 0
                    goto L_0x0038
                L_0x002f:
                    int r4 = r6.A00
                    r0 = 1
                    if (r4 == r0) goto L_0x0053
                    r0 = 2
                    r1 = 2
                    if (r4 == r0) goto L_0x0039
                L_0x0038:
                    r1 = 5
                L_0x0039:
                    X.1BD r0 = r2.A0k
                    X.23h r0 = r0.A00
                    if (r0 == 0) goto L_0x0041
                    r0.A01 = r1
                L_0x0041:
                    r3 = 1
                    X.0mE r2 = r2.A06
                    if (r4 == r3) goto L_0x0055
                    r1 = 2
                    r0 = 2131891845(0x7f121685, float:1.9418422E38)
                    if (r4 == r1) goto L_0x0058
                    r0 = 2131891843(0x7f121683, float:1.9418417E38)
                    r2.A07(r0, r3)
                    return
                L_0x0053:
                    r1 = 3
                    goto L_0x0039
                L_0x0055:
                    r0 = 2131891844(0x7f121684, float:1.941842E38)
                L_0x0058:
                    r2.A05(r0, r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C67273Qw.ANq(java.lang.Object):void");
            }
        });
        this.A0I.A01(this);
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        Log.i("statusesFragment/onStop");
        super.A14();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x004e, code lost:
        if (r13.getBoolean("SHARE_CTA_VISIBILITY_SI_KEY", false) == false) goto L_0x0050;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A16(android.os.Bundle r13) {
        /*
            r12 = this;
            java.lang.String r0 = "statusesFragment/onCreate"
            com.whatsapp.util.Log.i(r0)
            r7 = r12
            X.1BD r0 = r12.A0k
            X.0x5 r2 = r0.A0C
            r1 = 453128091(0x1b022f9b, float:1.076873E-22)
            r3 = 1
            r2.ALF(r1, r3)
            java.lang.String r0 = "CREATE_START"
            r2.ALC(r1, r0, r3)
            X.0x9 r2 = r12.A0H
            android.content.Context r1 = r12.A0p()
            java.lang.String r0 = "status-fragment"
            X.1J1 r0 = r2.A04(r1, r0)
            r12.A0G = r0
            super.A16(r13)
            X.0m9 r1 = r12.A0W
            r0 = 1874(0x752, float:2.626E-42)
            boolean r0 = r1.A07(r0)
            r12.A12 = r0
            X.0mE r8 = r12.A06
            X.0lR r11 = r12.A0r
            X.0sV r10 = r12.A0V
            X.10U r9 = r12.A0T
            com.whatsapp.status.StatusExpirationLifecycleOwner r6 = new com.whatsapp.status.StatusExpirationLifecycleOwner
            r6.<init>(r7, r8, r9, r10, r11)
            r12.A0e = r6
            r12.A11 = r3
            r5 = 0
            if (r13 == 0) goto L_0x0050
            java.lang.String r0 = "SHARE_CTA_VISIBILITY_SI_KEY"
            boolean r0 = r13.getBoolean(r0, r5)
            r6 = 1
            if (r0 != 0) goto L_0x0051
        L_0x0050:
            r6 = 0
        L_0x0051:
            android.content.Context r1 = r12.A01()
            X.16y r2 = r12.A0i
            X.1BD r3 = r12.A0k
            X.01H r4 = r12.A0s
            X.3El r0 = new X.3El
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r12.A0h = r0
            X.4LB r1 = r0.A01
            if (r1 == 0) goto L_0x006d
            X.16y r0 = r0.A05
            X.16w r0 = r0.A01
            r0.A03(r1)
        L_0x006d:
            if (r13 == 0) goto L_0x0077
            java.lang.String r0 = "WAS_FB_SHARE_BUTTON_ATTEMPTED_SI_KEY"
            boolean r0 = r13.getBoolean(r0, r5)
            r12.A0x = r0
        L_0x0077:
            X.1BD r0 = r12.A0k
            X.0x5 r3 = r0.A0C
            r2 = 453128091(0x1b022f9b, float:1.076873E-22)
            r1 = 1
            java.lang.String r0 = "CREATE_END"
            r3.ALC(r2, r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.StatusesFragment.A16(android.os.Bundle):void");
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        if (C28391Mz.A01()) {
            AnonymousClass2I2 r1 = this.A0I;
            ListFragment.A00(this);
            r1.A00(((ListFragment) this).A04, this);
        }
        if (this.A0W.A07(1071)) {
            HomeActivity.A0B(view, this, A02().getDimensionPixelSize(R.dimen.statuses_fragment_empty_footer_height));
        }
    }

    public final String A1A() {
        C33701ew r0 = this.A0q;
        if (r0 == null || r0.A05.isEmpty()) {
            return null;
        }
        return AnonymousClass1US.A09(",", (String[]) this.A0q.A05.keySet().toArray(new String[0]));
    }

    public void A1B() {
        C628238s r1 = this.A0p;
        if (r1 != null) {
            r1.A03(true);
        }
        C18470sV r5 = this.A0V;
        C15860o1 r6 = this.A0Z;
        C628238s r2 = new C628238s(this.A0T, this.A0U, r5, r6, this.A0k, this);
        this.A0p = r2;
        this.A0r.Aaz(r2, new Void[0]);
    }

    public final void A1C() {
        int i;
        int i2;
        View view = super.A0A;
        if (view != null) {
            if (this.A0q.A04()) {
                if (this.A0p != null) {
                    view.findViewById(R.id.init_statuses_progress).setVisibility(0);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                } else if (this.A0D.A04() > 0) {
                    view.findViewById(R.id.init_statuses_progress).setVisibility(8);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                    view.findViewById(R.id.welcome_statuses_message).setVisibility(0);
                    view.findViewById(R.id.statuses_empty_no_contacts).setVisibility(8);
                    view.findViewById(R.id.contacts_empty_permission_denied).setVisibility(8);
                    TextView textView = (TextView) view.findViewById(R.id.welcome_statuses_message);
                    Context context = textView.getContext();
                    String A0I = A0I(R.string.welcome_statuses_message);
                    Drawable A04 = AnonymousClass00T.A04(context, R.drawable.ic_new_status_tip);
                    AnonymousClass009.A05(A04);
                    textView.setText(C52252aV.A00(textView.getPaint(), AnonymousClass2GE.A04(A04, AnonymousClass00T.A00(context, R.color.secondary_text)), A0I));
                    return;
                } else {
                    if (this.A0C.A00()) {
                        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(view, R.id.statuses_empty_no_contacts);
                        if (viewGroup.getChildCount() == 0) {
                            EmptyTellAFriendView emptyTellAFriendView = new EmptyTellAFriendView(A0p());
                            viewGroup.addView(emptyTellAFriendView);
                            emptyTellAFriendView.setInviteButtonClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 36));
                        }
                        viewGroup.setVisibility(0);
                        i2 = R.id.contacts_empty_permission_denied;
                    } else {
                        ViewGroup viewGroup2 = (ViewGroup) view.findViewById(R.id.contacts_empty_permission_denied);
                        if (viewGroup2.getChildCount() == 0) {
                            A0C().getLayoutInflater().inflate(R.layout.empty_contacts_permissions_needed, viewGroup2, true);
                            viewGroup2.findViewById(R.id.button_open_permission_settings).setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 32));
                        }
                        viewGroup2.setVisibility(0);
                        i2 = R.id.statuses_empty_no_contacts;
                    }
                    view.findViewById(i2).setVisibility(8);
                    view.findViewById(R.id.init_statuses_progress).setVisibility(8);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                    i = R.id.welcome_statuses_message;
                    view.findViewById(i).setVisibility(8);
                }
            } else if (!TextUtils.isEmpty(this.A0t)) {
                view.findViewById(R.id.init_statuses_progress).setVisibility(8);
                view.findViewById(R.id.search_no_matches).setVisibility(0);
                ((TextView) view.findViewById(R.id.search_no_matches)).setText(A0J(R.string.search_no_results, this.A0t));
            } else {
                return;
            }
            view.findViewById(R.id.welcome_statuses_message).setVisibility(8);
            view.findViewById(R.id.statuses_empty_no_contacts).setVisibility(8);
            i = R.id.contacts_empty_permission_denied;
            view.findViewById(i).setVisibility(8);
        }
    }

    public final void A1D() {
        if (this.A10) {
            this.A0k.A06(Boolean.TRUE);
            if (this.A0p == null) {
                this.A0k.A07(this.A0q.A05, this.A0q.A02.size());
            }
        }
    }

    public final void A1E() {
        List<Uri> list = this.A1D;
        for (Uri uri : list) {
            this.A0M.A00.revokeUriPermission(uri, 1);
        }
        list.clear();
    }

    public final void A1F() {
        Intent A09 = RequestPermissionActivity.A09(A0p(), this.A0N, 33);
        if (A09 != null) {
            startActivityForResult(A09, 33);
        } else if (!this.A0J.A04(this.A15)) {
        } else {
            if (this.A0Q.A01() < ((long) ((this.A08.A02(AbstractC15460nI.A1p) << 10) << 10))) {
                C16120oU r3 = this.A0X;
                ActivityC000900k A0B = A0B();
                AnonymousClass009.A05(A0B);
                C33471e8.A04(A0B, (AbstractC13860kS) A0B, r3, 5);
                return;
            }
            if (this.A03 != null) {
                this.A0O.A00.edit().putBoolean("show_statuses_education", false).apply();
                this.A03.setVisibility(8);
            }
            Context A0p = A0p();
            String rawString = AnonymousClass1VX.A00.getRawString();
            Intent intent = new Intent();
            intent.setClassName(A0p.getPackageName(), "com.whatsapp.camera.CameraActivity");
            intent.putExtra("jid", rawString);
            intent.putExtra("origin", 4);
            A0v(intent);
        }
    }

    public final void A1G() {
        C14900mE r0 = this.A06;
        Runnable runnable = this.A19;
        r0.A0G(runnable);
        if (!(this.A0q.A04() || A0B() == null)) {
            C33701ew r6 = this.A0q;
            long j = 0;
            for (AnonymousClass1V2 r3 : r6.A02) {
                if (r3.A04() > j) {
                    j = r3.A04();
                }
            }
            for (AnonymousClass1V2 r32 : r6.A03) {
                if (r32.A04() > j) {
                    j = r32.A04();
                }
            }
            for (AnonymousClass1V2 r33 : r6.A01) {
                if (r33.A04() > j) {
                    j = r33.A04();
                }
            }
            AnonymousClass1V2 r34 = r6.A00;
            if (r34 != null && r34.A04() > j) {
                j = r34.A04();
            }
            this.A06.A0J(runnable, (C38121nY.A01(j) - System.currentTimeMillis()) + 1000);
        }
    }

    public final void A1H(Animator.AnimatorListener animatorListener, boolean z) {
        View view;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.A0g.getCount(); i++) {
            if (this.A0g.getItemViewType(i) == 0) {
                C69643a0 r4 = (C69643a0) ((AbstractC116185Ul) this.A0g.A03.A0v.get(i));
                C15860o1 r2 = this.A0Z;
                AnonymousClass1V2 r1 = r4.A01;
                if (r2.A08(r1.A0A.getRawString()).A0H && !r1.A0C() && (view = r4.A00) != null) {
                    if (z) {
                        view.measure(0, 0);
                    }
                    int measuredHeight = view.getMeasuredHeight();
                    int[] iArr = new int[2];
                    if (z) {
                        iArr[0] = 0;
                        iArr[1] = measuredHeight;
                    } else {
                        iArr[0] = measuredHeight;
                        iArr[1] = 0;
                    }
                    ValueAnimator ofInt = ValueAnimator.ofInt(iArr);
                    ofInt.addListener(new C72603es(view, measuredHeight));
                    ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view, z) { // from class: X.3Jp
                        public final /* synthetic */ View A00;
                        public final /* synthetic */ boolean A01;

                        {
                            this.A00 = r1;
                            this.A01 = r2;
                        }

                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                            float animatedFraction;
                            View view2 = this.A00;
                            boolean z2 = this.A01;
                            int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
                            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                            if (layoutParams != null) {
                                layoutParams.height = A05;
                                view2.setLayoutParams(layoutParams);
                            }
                            if (z2) {
                                animatedFraction = valueAnimator.getAnimatedFraction();
                            } else {
                                animatedFraction = 1.0f - valueAnimator.getAnimatedFraction();
                            }
                            view2.setAlpha(animatedFraction);
                        }
                    });
                    arrayList.add(ofInt);
                }
            }
        }
        AnimatorSet animatorSet = new AnimatorSet();
        this.A00 = animatorSet;
        animatorSet.setInterpolator(new DecelerateInterpolator());
        this.A00.setDuration(250L);
        this.A00.addListener(animatorListener);
        this.A00.playTogether(arrayList);
        this.A00.start();
    }

    public final void A1I(ListView listView) {
        if (this.A02 == null) {
            this.A02 = new Space(A0p());
            this.A02.setLayoutParams(new AbsListView.LayoutParams(-1, A02().getDimensionPixelSize(R.dimen.conversation_list_padding_top)));
        }
        listView.removeHeaderView(this.A02);
        listView.addHeaderView(this.A02);
    }

    public final void A1J(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A0m.A00(this.A0T.A00(((AnonymousClass1V2) it.next()).A0A));
        }
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            for (AbstractC15340mz r1 : this.A0T.A01(((AnonymousClass1V2) it2.next()).A0A)) {
                this.A0m.A00(r1);
            }
        }
    }

    public final void A1K(boolean z) {
        boolean A02;
        C458223h r0;
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            this.A0x = z;
            C248016x r2 = this.A0j;
            ArrayList arrayList = new ArrayList(this.A0h.A05.A03.values());
            if (z) {
                A02 = r2.A02(A0B, this, r2.A02, arrayList);
            } else {
                A02 = r2.A02(A0B, this, r2.A03, arrayList);
            }
            if (!A02 && !this.A0j.A00.A07() && (r0 = this.A0k.A00) != null) {
                r0.A01 = 4;
            }
        }
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void A5j(AbstractC14780m2 r1) {
        r1.AM3();
    }

    @Override // X.AbstractC14770m1
    public void A68(AnonymousClass2LB r3) {
        this.A0t = r3.A01;
        this.A0g.getFilter().filter(this.A0t);
    }

    @Override // X.AbstractC14940mI
    public String AE3() {
        this.A07.A08();
        return A0I(R.string.menuitem_new_status);
    }

    @Override // X.AbstractC14940mI
    public Drawable AE4() {
        this.A07.A08();
        return AnonymousClass2GE.A01(A01(), R.drawable.ic_camera, R.color.white);
    }

    @Override // X.AbstractC14940mI
    public String AGV() {
        this.A07.A08();
        return A0I(R.string.menuitem_new_text_status);
    }

    @Override // X.AbstractC14940mI
    public Drawable AGW() {
        this.A07.A08();
        return AnonymousClass2GE.A01(A01(), R.drawable.ic_text_status_compose, R.color.fabSecondaryContent);
    }

    @Override // X.AbstractC14750lz
    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev AGo(int i, int i2, boolean z) {
        View findViewById = A0C().findViewById(R.id.pager_holder);
        ArrayList arrayList = new ArrayList();
        arrayList.add(A0C().findViewById(R.id.fab));
        arrayList.add(A0C().findViewById(R.id.fab_second));
        this.A07.A08();
        if (this.A12) {
            arrayList.add(A0C().findViewById(R.id.fab_third));
        }
        ViewTreeObserver$OnGlobalLayoutListenerC33691ev r2 = new ViewTreeObserver$OnGlobalLayoutListenerC33691ev(this, C34271fr.A00(findViewById, findViewById.getResources().getText(i), i2), this.A0K, arrayList, z);
        this.A0d = r2;
        r2.A03(new RunnableBRunnable0Shape12S0100000_I0_12(this, 5));
        return this.A0d;
    }

    @Override // X.AbstractC14940mI
    public String AHA() {
        this.A07.A08();
        if (this.A12) {
            return A0I(R.string.menuitem_new_voice_status);
        }
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AHB() {
        this.A07.A08();
        if (this.A12) {
            return AnonymousClass2GE.A01(A01(), R.drawable.input_mic_white, R.color.fabSecondaryContent);
        }
        return null;
    }

    @Override // X.AbstractC14940mI
    public void ASK() {
        A1F();
    }

    @Override // X.AbstractC14940mI
    public void AVh() {
        Context A0p = A0p();
        Intent intent = new Intent();
        intent.setClassName(A0p.getPackageName(), "com.whatsapp.textstatuscomposer.TextStatusComposerActivity");
        intent.putExtra("origin", 4);
        A0v(intent);
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r10) {
        this.A0p = null;
        this.A0q = r10;
        AnonymousClass1BD r0 = this.A0k;
        Map map = r10.A04;
        Map map2 = r0.A0F;
        map2.clear();
        map2.putAll(map);
        if (r10.A01.size() == 0) {
            this.A0y = true;
        }
        this.A0g.getFilter().filter(this.A0t);
        long j = 0;
        int i = 0;
        for (AnonymousClass1V2 r1 : this.A0q.A02) {
            i++;
            if (r1.A03() > j) {
                j = r1.A03();
            }
        }
        ActivityC000900k A0B = A0B();
        if (A0B instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) A0B;
            if (j != 0) {
                homeActivity.A04 = j;
            }
            ((ActivityC13810kN) homeActivity).A05.A0G(homeActivity.A22);
            if (homeActivity.A03 == 300) {
                homeActivity.A2k();
            } else {
                long j2 = ((ActivityC13810kN) homeActivity).A09.A00.getLong("last_notified_status_row_id", 0);
                C14810m5 A0H = homeActivity.A0N.A0H(HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 300));
                if (j2 < j) {
                    A0H.A00 = i;
                } else if (A0H.A00 != 0) {
                    A0H.A00 = 0;
                }
                homeActivity.A2s();
            }
        }
        AnonymousClass1BD r2 = this.A0k;
        C458223h r12 = r2.A00;
        if (r12 != null && !r12.A04 && r12.A07) {
            r2.A07(r10.A05, this.A0q.A02.size());
        }
        A1C();
        A1G();
        this.A0e.A00();
        C37371mG r13 = this.A0o;
        if (r13 != null) {
            r13.A03(true);
        }
        C37371mG r22 = new C37371mG(this.A0T, this);
        this.A0o = r22;
        this.A0r.Aaz(r22, new Void[0]);
    }

    @Override // X.AbstractC14770m1
    public void Acn(boolean z) {
        this.A0z = z;
        this.A0g.getFilter().filter(this.A0t);
    }

    @Override // X.AbstractC14770m1
    public void Aco(boolean z) {
        this.A10 = z;
        if (z) {
            C14820m6 r1 = this.A0O;
            r1.A00.edit().putLong("status_tab_last_opened_time", this.A0L.A00()).apply();
            A1D();
            if (this.A0W.A07(249)) {
                this.A0r.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 4));
                return;
            }
            return;
        }
        C64153El r5 = this.A0h;
        AnonymousClass4ON r4 = r5.A04;
        if (r4.A00) {
            C248116y r0 = r5.A05;
            r0.A00.post(new RunnableBRunnable0Shape12S0100000_I0_12(r0.A03, 7));
            C458223h r02 = r5.A06.A00;
            if (r02 != null) {
                r02.A00 = 1;
            }
            r4.A01 = false;
            r4.A00 = false;
            r5.A00();
        }
        this.A0k.A03();
        if (this.A11) {
            this.A0y = true;
            this.A0g.getFilter().filter(this.A0t);
        }
        AnonymousClass1CW r42 = this.A0l;
        Log.i("statusdownload/cancel-all-status-downloads");
        C22370yy r3 = r42.A02;
        for (AbstractC16130oV r12 : r3.A04()) {
            if (C15380n4.A0N(r12.A0z.A00)) {
                r3.A0C(r12, false, false);
            }
        }
        r42.A03.clear();
        r42.A00 = null;
        r42.A01 = null;
    }
}
