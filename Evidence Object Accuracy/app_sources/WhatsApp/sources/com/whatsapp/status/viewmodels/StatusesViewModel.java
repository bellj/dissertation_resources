package com.whatsapp.status.viewmodels;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC16350or;
import X.AbstractC33021d9;
import X.AbstractC41661tt;
import X.AbstractCallableC112595Dz;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass02O;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass0R2;
import X.AnonymousClass12H;
import X.AnonymousClass1BD;
import X.AnonymousClass1BE;
import X.AnonymousClass1US;
import X.AnonymousClass1V2;
import X.AnonymousClass5U2;
import X.C18470sV;
import X.C244015j;
import X.C33701ew;
import X.C35121hH;
import X.C61212zb;
import X.C628238s;
import X.C63563Cb;
import X.C92434Vw;
import X.ExecutorC27271Gr;
import androidx.lifecycle.OnLifecycleEvent;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes.dex */
public class StatusesViewModel extends AnonymousClass015 implements AnonymousClass03H {
    public AbstractC33021d9 A00;
    public C628238s A01;
    public C33701ew A02 = null;
    public C61212zb A03;
    public Set A04 = new HashSet();
    public final AnonymousClass017 A05;
    public final AnonymousClass016 A06;
    public final C63563Cb A07;
    public final AnonymousClass12H A08;
    public final C18470sV A09;
    public final AbstractC41661tt A0A = new AbstractC41661tt() { // from class: X.59J
        @Override // X.AbstractC41661tt
        public final void AWW(AbstractC14640lm r4) {
            StatusesViewModel statusesViewModel = StatusesViewModel.this;
            UserJid of = UserJid.of(r4);
            Set set = statusesViewModel.A0G;
            synchronized (set) {
                set.add(of);
            }
            statusesViewModel.A07();
        }
    };
    public final C244015j A0B;
    public final AnonymousClass1BD A0C;
    public final AnonymousClass1BE A0D;
    public final C35121hH A0E = new C35121hH(this);
    public final AbstractC14440lR A0F;
    public final Set A0G;
    public final AtomicBoolean A0H;
    public final boolean A0I;

    public StatusesViewModel(AnonymousClass12H r3, C18470sV r4, C244015j r5, AnonymousClass1BD r6, AnonymousClass1BE r7, AbstractC14440lR r8, boolean z) {
        AnonymousClass016 r1 = new AnonymousClass016(new HashMap());
        this.A06 = r1;
        this.A05 = AnonymousClass0R2.A00(new AnonymousClass02O() { // from class: X.3P3
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                Set set = StatusesViewModel.this.A04;
                HashMap A11 = C12970iu.A11();
                Iterator A0n = C12960it.A0n((Map) obj);
                while (A0n.hasNext()) {
                    Map.Entry A15 = C12970iu.A15(A0n);
                    Object key = A15.getKey();
                    A11.put(key, new C92434Vw((AnonymousClass1V2) A15.getValue(), set.contains(key)));
                }
                return A11;
            }
        }, r1);
        this.A0G = new HashSet();
        this.A0H = new AtomicBoolean(false);
        this.A0C = r6;
        this.A0B = r5;
        this.A08 = r3;
        this.A0F = r8;
        this.A0D = r7;
        this.A09 = r4;
        this.A07 = new C63563Cb(new ExecutorC27271Gr(r8, true));
        this.A0I = z;
    }

    public static final void A00(AbstractCallableC112595Dz r0) {
        if (r0 != null) {
            r0.A00();
        }
    }

    public static /* synthetic */ void A01(UserJid userJid, StatusesViewModel statusesViewModel) {
        Set set = statusesViewModel.A0G;
        synchronized (set) {
            set.add(userJid);
        }
        statusesViewModel.A07();
    }

    public static final void A02(AbstractC16350or r1) {
        if (r1 != null) {
            r1.A03(true);
        }
    }

    public C92434Vw A04(UserJid userJid) {
        Map map = (Map) this.A05.A01();
        if (map != null) {
            return (C92434Vw) map.get(userJid);
        }
        return null;
    }

    public final String A05() {
        C33701ew r0 = this.A02;
        if (r0 == null || r0.A03().isEmpty()) {
            return null;
        }
        return AnonymousClass1US.A09(",", (String[]) this.A02.A03().keySet().toArray(new String[0]));
    }

    public final void A06() {
        this.A04 = new HashSet();
        C33701ew r0 = this.A02;
        if (r0 != null) {
            for (AnonymousClass1V2 r02 : r0.A00()) {
                this.A04.add(r02.A07());
            }
        }
    }

    public final void A07() {
        A02(this.A01);
        AbstractC33021d9 r1 = this.A00;
        if (r1 != null) {
            C628238s A00 = this.A0D.A00(r1);
            this.A01 = A00;
            this.A0F.Aaz(A00, new Void[0]);
        }
    }

    public void A08(AbstractC14640lm r13, Integer num, Integer num2) {
        String str;
        UserJid of = UserJid.of(r13);
        if (of != null && this.A02 != null) {
            AnonymousClass1BD r3 = this.A0C;
            boolean z = true;
            int intValue = num.intValue();
            if (!(intValue == 4 || intValue == 1 || intValue == 3 || intValue == 2)) {
                z = false;
                r3.A06(Boolean.FALSE);
            }
            C33701ew r0 = this.A02;
            List A01 = r0.A01();
            List A02 = r0.A02();
            List A00 = r0.A00();
            Map map = null;
            if (z) {
                map = r0.A03();
                str = A05();
            } else {
                str = null;
            }
            r3.A04(of, num, num2, str, A01, A02, A00, map);
        }
    }

    public void A09(C33701ew r5) {
        this.A02 = r5;
        A06();
        A00((AbstractCallableC112595Dz) this.A03);
        C61212zb r3 = new C61212zb(this);
        this.A03 = r3;
        this.A07.A00(new AnonymousClass5U2() { // from class: X.55T
            @Override // X.AnonymousClass5U2
            public final void AOO(Object obj) {
                AnonymousClass016.this.A0A(obj);
            }
        }, r3);
    }

    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    public void onLifecycleDestroy() {
        this.A00 = null;
    }

    @OnLifecycleEvent(AnonymousClass074.ON_PAUSE)
    public void onLifecyclePause() {
        A02(this.A01);
        A00((AbstractCallableC112595Dz) this.A03);
        if (this.A0I) {
            this.A08.A04(this.A0E);
            this.A0B.A04(this.A0A);
        }
    }

    @OnLifecycleEvent(AnonymousClass074.ON_RESUME)
    public void onLifecycleResume() {
        if (this.A0I) {
            this.A08.A03(this.A0E);
            this.A0B.A03(this.A0A);
        }
        this.A0H.set(false);
        A07();
    }
}
