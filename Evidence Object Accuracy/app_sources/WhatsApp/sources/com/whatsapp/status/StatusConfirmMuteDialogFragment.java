package com.whatsapp.status;

import X.AbstractC33381dw;
import X.AnonymousClass009;
import X.AnonymousClass1BD;
import X.AnonymousClass1E9;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.RunnableC55712jB;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class StatusConfirmMuteDialogFragment extends Hilt_StatusConfirmMuteDialogFragment {
    public C15550nR A00;
    public C15610nY A01;
    public AbstractC33381dw A02;
    public AnonymousClass1E9 A03;
    public AnonymousClass1BD A04;

    public static StatusConfirmMuteDialogFragment A00(UserJid userJid, Long l, String str, String str2, String str3) {
        long j;
        StatusConfirmMuteDialogFragment statusConfirmMuteDialogFragment = new StatusConfirmMuteDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", userJid.getRawString());
        A0D.putString("message_id", str);
        if (l != null) {
            j = l.longValue();
        } else {
            j = 0;
        }
        A0D.putLong("status_item_index", j);
        A0D.putString("psa_campaign_id", str2);
        A0D.putString("psa_campaign_ids", str3);
        statusConfirmMuteDialogFragment.A0U(A0D);
        return statusConfirmMuteDialogFragment;
    }

    public static /* synthetic */ void A01(UserJid userJid, StatusConfirmMuteDialogFragment statusConfirmMuteDialogFragment) {
        StringBuilder A0k = C12960it.A0k("statusesfragment/mute status for ");
        A0k.append(userJid);
        C12960it.A1F(A0k);
        statusConfirmMuteDialogFragment.A03.A00(userJid, true);
        AnonymousClass1BD r4 = statusConfirmMuteDialogFragment.A04;
        String string = statusConfirmMuteDialogFragment.A03().getString("message_id");
        r4.A0D.Ab2(new RunnableC55712jB(userJid, r4, 1, Long.valueOf(statusConfirmMuteDialogFragment.A03().getLong("status_item_index")), statusConfirmMuteDialogFragment.A03().getString("psa_campaign_id"), string, statusConfirmMuteDialogFragment.A03().getString("psa_campaign_ids")));
        statusConfirmMuteDialogFragment.A1B();
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        try {
            this.A02 = (AbstractC33381dw) A09();
        } catch (ClassCastException unused) {
            throw new ClassCastException("Calling fragment must implement Host interface");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        this.A02.AP7(this, true);
        UserJid nullable = UserJid.getNullable(A03().getString("jid"));
        AnonymousClass009.A05(nullable);
        C15370n3 A0B = this.A00.A0B(nullable);
        C004802e A0K = C12960it.A0K(this);
        A0K.setTitle(C12970iu.A0q(this, C15610nY.A01(this.A01, A0B), new Object[1], 0, R.string.mute_status_confirmation_title));
        A0K.A0A(C12970iu.A0q(this, this.A01.A04(A0B), new Object[1], 0, R.string.mute_status_confirmation_message));
        C12970iu.A1K(A0K, this, 66, R.string.cancel);
        A0K.setPositiveButton(R.string.mute_status, new IDxCListenerShape4S0200000_2_I1(nullable, 14, this));
        return A0K.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.A02.AP7(this, false);
    }
}
