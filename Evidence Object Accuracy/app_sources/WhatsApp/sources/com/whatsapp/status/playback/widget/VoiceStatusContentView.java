package com.whatsapp.status.playback.widget;

import X.AbstractC115415Rl;
import X.AbstractC14640lm;
import X.AbstractC16130oV;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass11F;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3WG;
import X.AnonymousClass51L;
import X.AnonymousClass59Q;
import X.C12960it;
import X.C12980iv;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C18000rk;
import X.C20710wC;
import X.C30421Xi;
import X.C38131nZ;
import X.C51502Vc;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.status.playback.content.BlurFrameLayout;

/* loaded from: classes2.dex */
public class VoiceStatusContentView extends LinearLayout implements AnonymousClass004 {
    public TextView A00;
    public C30421Xi A01;
    public AnonymousClass3WG A02;
    public AbstractC115415Rl A03;
    public VoiceStatusProfileAvatarView A04;
    public AnonymousClass01H A05;
    public AnonymousClass01H A06;
    public AnonymousClass01H A07;
    public AnonymousClass01H A08;
    public AnonymousClass01H A09;
    public AnonymousClass01H A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;

    public VoiceStatusContentView(Context context) {
        super(context);
        A00();
        A01(context);
    }

    public VoiceStatusContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(context);
    }

    public VoiceStatusContentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(context);
    }

    public VoiceStatusContentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        A01(context);
    }

    public VoiceStatusContentView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A08 = C18000rk.A00(A00.AAr);
            this.A06 = C18000rk.A00(A00.A45);
            this.A0A = C18000rk.A00(A00.ANb);
            this.A07 = C18000rk.A00(A00.A8m);
            this.A05 = C18000rk.A00(A00.A41);
            this.A09 = C18000rk.A00(A00.AE3);
        }
    }

    public final void A01(Context context) {
        LinearLayout.inflate(context, R.layout.voice_status_content_view, this);
        this.A04 = (VoiceStatusProfileAvatarView) AnonymousClass028.A0D(this, R.id.voice_status_profile_avatar);
        this.A00 = C12960it.A0I(this, R.id.voice_duration);
        setBackgroundResource(R.drawable.voice_status_content_view_background);
        setOrientation(0);
        setGravity(16);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.status_voice_content_padding);
        setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnonymousClass3WG r0 = this.A02;
        if (r0 != null) {
            r0.A01.clear();
        }
    }

    private void setBackgroundColorFromMessage(C30421Xi r3) {
        AnonymousClass028.A0M(ColorStateList.valueOf(-9467188), this);
        this.A04.setMicrophoneStrokeColor(-9467188);
    }

    public void setContentUpdatedListener(AbstractC115415Rl r1) {
        this.A03 = r1;
    }

    public void setDuration(int i) {
        this.A00.setText(C38131nZ.A04((AnonymousClass018) this.A0A.get(), (long) i));
    }

    public void setVoiceMessage(C30421Xi r7, AnonymousClass1J1 r8) {
        C15370n3 A0B;
        this.A01 = r7;
        setBackgroundColorFromMessage(r7);
        ImageView imageView = this.A04.A01;
        AnonymousClass11F r5 = (AnonymousClass11F) this.A09.get();
        imageView.setImageDrawable(r5.A00(C12980iv.A0I(this), getResources(), AnonymousClass51L.A00, R.drawable.avatar_contact));
        C51502Vc r3 = new C51502Vc((AnonymousClass130) this.A05.get(), null, r5, (C20710wC) this.A07.get());
        this.A02 = new AnonymousClass3WG(r3, this);
        if (r7.A0z.A02) {
            C15570nT r0 = (C15570nT) this.A08.get();
            r0.A08();
            A0B = r0.A01;
            if (A0B != null) {
                AnonymousClass3WG r02 = this.A02;
                if (r02 != null) {
                    r02.A01.clear();
                }
                r8.A02(imageView, r3, A0B, true);
            }
        } else {
            AbstractC14640lm A0B2 = r7.A0B();
            if (A0B2 != null) {
                A0B = ((C15550nR) this.A06.get()).A0B(A0B2);
                r8.A02(imageView, r3, A0B, true);
            }
        }
        setDuration(((AbstractC16130oV) r7).A00);
        AbstractC115415Rl r03 = this.A03;
        if (r03 != null) {
            BlurFrameLayout blurFrameLayout = ((AnonymousClass59Q) r03).A00.A01;
            blurFrameLayout.A09 = true;
            blurFrameLayout.invalidate();
        }
    }
}
