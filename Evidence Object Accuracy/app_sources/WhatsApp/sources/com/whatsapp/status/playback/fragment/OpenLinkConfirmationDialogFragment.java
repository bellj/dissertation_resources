package com.whatsapp.status.playback.fragment;

import X.AnonymousClass12Q;
import X.AnonymousClass1BD;
import X.AnonymousClass35V;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class OpenLinkConfirmationDialogFragment extends Hilt_OpenLinkConfirmationDialogFragment {
    public AnonymousClass1BD A00;
    public String A01;
    public final AnonymousClass12Q A02;
    public final AnonymousClass35V A03;
    public final String A04;

    public OpenLinkConfirmationDialogFragment(AnonymousClass12Q r1, AnonymousClass35V r2, String str, String str2) {
        this.A04 = str;
        this.A02 = r1;
        this.A03 = r2;
        this.A01 = str2;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0K = C12960it.A0K(this);
        A0K.A07(R.string.text_status_viewer_open_link_dialog_title);
        A0K.A0A(this.A04);
        C12970iu.A1K(A0K, this, 68, R.string.cancel);
        C12970iu.A1M(A0K, this, 31, R.string.text_status_viewer_open_link_dialog_open_button);
        return A0K.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.A03.A05();
    }
}
