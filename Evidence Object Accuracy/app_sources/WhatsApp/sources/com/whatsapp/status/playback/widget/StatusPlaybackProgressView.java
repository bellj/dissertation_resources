package com.whatsapp.status.playback.widget;

import X.AbstractC115405Rk;
import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
public class StatusPlaybackProgressView extends View implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public AbstractC115405Rk A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public final Paint A06;
    public final RectF A07;
    public final Set A08;

    public StatusPlaybackProgressView(Context context) {
        super(context);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A08 = new HashSet();
        this.A07 = new RectF();
        this.A06 = new Paint(1);
    }

    public StatusPlaybackProgressView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A08 = new HashSet();
        this.A07 = new RectF();
        this.A06 = new Paint(1);
    }

    public StatusPlaybackProgressView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A08 = new HashSet();
        this.A07 = new RectF();
        this.A06 = new Paint(1);
    }

    public StatusPlaybackProgressView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A08 = new HashSet();
        this.A07 = new RectF();
        this.A06 = new Paint(1);
    }

    public StatusPlaybackProgressView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0125, code lost:
        if (((long) r4.A0C()) < (((long) r4.A0B.A02(X.AbstractC15460nI.A28)) * 1000)) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01e2, code lost:
        if (r15 == 0) goto L_0x01e4;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r19) {
        /*
        // Method dump skipped, instructions count: 685
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.widget.StatusPlaybackProgressView.onDraw(android.graphics.Canvas):void");
    }

    public void setCount(int i) {
        this.A01 = i;
        invalidate();
    }

    public void setPosition(int i) {
        if (this.A02 != i) {
            this.A02 = i;
            this.A00 = 0.0f;
            invalidate();
        }
    }

    public void setProgressProvider(AbstractC115405Rk r1) {
        this.A03 = r1;
        invalidate();
    }
}
