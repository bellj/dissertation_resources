package com.whatsapp.status.playback.widget;

import X.AnonymousClass1US;
import X.AnonymousClass24D;
import X.AnonymousClass4GR;
import X.C64583Gc;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import com.facebook.redex.RunnableBRunnable0Shape0S0102000_I0;
import com.whatsapp.mentions.MentionableEntry;

/* loaded from: classes2.dex */
public class StatusEditText extends MentionableEntry {
    public float A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;

    public StatusEditText(Context context) {
        super(context);
        A02();
        this.A03 = false;
        this.A02 = false;
    }

    public StatusEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A03 = false;
        this.A02 = false;
    }

    public StatusEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A03 = false;
        this.A02 = false;
    }

    public StatusEditText(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    public void A0F() {
        int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        if (measuredWidth > 0 && measuredHeight > 0) {
            if (!this.A03) {
                if (this.A00 == 0.0f) {
                    this.A00 = getPaint().getTextSize();
                }
                Editable text = getText();
                float f = this.A00;
                TextPaint textPaint = new TextPaint(getPaint());
                float f2 = f;
                float f3 = 2.0f;
                while (true) {
                    if (f - f3 <= 2.0f) {
                        f2 = f3;
                        break;
                    }
                    textPaint.setTextSize(f2);
                    C64583Gc.A00(textPaint, text);
                    if (AnonymousClass4GR.A00.A8K(textPaint, this, AnonymousClass1US.A01(text), measuredWidth).getHeight() <= measuredHeight) {
                        if (this.A00 == f2) {
                            break;
                        }
                        f3 = f2;
                    } else {
                        f = f2;
                    }
                    f2 = (f3 + f) / 2.0f;
                }
                super.setTextSize(0, f2);
            } else if (getText() == null) {
                super.setTextSize(2, 32.0f);
            } else {
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float f4 = ((float) displayMetrics.heightPixels) / displayMetrics.density;
                int A02 = AnonymousClass24D.A02(getText(), 0, getText().length());
                super.setTextSize(2, (float) AnonymousClass24D.A00(A02, (int) f4, this.A02));
                AnonymousClass24D.A08(this, A02);
            }
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3 || i2 != i4) {
            A0F();
            int selectionStart = getSelectionStart();
            int selectionEnd = getSelectionEnd();
            if (selectionStart >= 0 && selectionEnd >= 0) {
                if (!this.A03) {
                    setSelection(0);
                }
                post(new RunnableBRunnable0Shape0S0102000_I0(this, selectionStart, selectionEnd));
            }
        }
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        A0F();
    }

    public void setLinkPreviewPresent(boolean z) {
        this.A02 = z;
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        this.A00 = getPaint().getTextSize();
        A0F();
    }

    @Override // android.widget.TextView
    public void setTypeface(Typeface typeface) {
        super.setTypeface(typeface);
        A0F();
    }

    public void setUseNewTextSizeLimits(boolean z) {
        this.A03 = z;
    }
}
