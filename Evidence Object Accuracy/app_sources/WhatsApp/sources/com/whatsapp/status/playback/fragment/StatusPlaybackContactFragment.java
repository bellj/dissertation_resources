package com.whatsapp.status.playback.fragment;

import X.AbstractC116205Un;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14750lz;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC33081dJ;
import X.AbstractC33331dp;
import X.AbstractC33381dw;
import X.AbstractC33391dx;
import X.AbstractC33401dy;
import X.AbstractC33621eg;
import X.AbstractC33631eh;
import X.AbstractC454421p;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass10S;
import X.AnonymousClass10U;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass18U;
import X.AnonymousClass19Z;
import X.AnonymousClass1BD;
import X.AnonymousClass1CH;
import X.AnonymousClass1CW;
import X.AnonymousClass1CY;
import X.AnonymousClass1E9;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass1V1;
import X.AnonymousClass1X2;
import X.AnonymousClass1X4;
import X.AnonymousClass2Dn;
import X.AnonymousClass2TT;
import X.AnonymousClass35V;
import X.AnonymousClass389;
import X.AnonymousClass423;
import X.AnonymousClass4ZH;
import X.C006202y;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15860o1;
import X.C16150oX;
import X.C16170oZ;
import X.C16590pI;
import X.C17020q8;
import X.C18470sV;
import X.C18790t3;
import X.C20020v5;
import X.C21270x9;
import X.C22330yu;
import X.C22810zg;
import X.C239613r;
import X.C242714w;
import X.C244215l;
import X.C244415n;
import X.C26101Ca;
import X.C27131Gd;
import X.C28801Pb;
import X.C29831Uv;
import X.C30041Vv;
import X.C32731ce;
import X.C33651en;
import X.C35271hY;
import X.C35551iF;
import X.C35681iV;
import X.C37381mH;
import X.C38131nZ;
import X.C38211ni;
import X.C48252Fe;
import X.C51122Sx;
import X.C53402eD;
import X.C851041e;
import X.C858944o;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1200000_I0;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.widget.StatusPlaybackProgressView;
import com.whatsapp.util.Log;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class StatusPlaybackContactFragment extends Hilt_StatusPlaybackContactFragment implements AbstractC33391dx, AbstractC33081dJ, AbstractC14750lz, AbstractC33381dw, AbstractC33401dy {
    public int A00 = 0;
    public int A01;
    public AbstractC15710nm A02;
    public AnonymousClass18U A03;
    public C15570nT A04;
    public Mp4Ops A05;
    public C239613r A06;
    public C15450nH A07;
    public C16170oZ A08;
    public C22330yu A09;
    public C15550nR A0A;
    public AnonymousClass10S A0B;
    public C15610nY A0C;
    public AnonymousClass1J1 A0D;
    public C21270x9 A0E;
    public C20020v5 A0F;
    public C14830m7 A0G;
    public C16590pI A0H;
    public C15650ng A0I;
    public AnonymousClass12H A0J;
    public AnonymousClass10U A0K;
    public C242714w A0L;
    public C18470sV A0M;
    public C14850m9 A0N;
    public C244215l A0O;
    public UserJid A0P;
    public AbstractC15340mz A0Q;
    public C15860o1 A0R;
    public AnonymousClass12F A0S;
    public AnonymousClass1E9 A0T;
    public AnonymousClass1BD A0U;
    public AnonymousClass389 A0V;
    public C26101Ca A0W;
    public AbstractC14440lR A0X;
    public AnonymousClass19Z A0Y;
    public AnonymousClass01H A0Z;
    public String A0a;
    public List A0b;
    public Map A0c;
    public boolean A0d;
    public boolean A0e;
    public final C006202y A0f = new C53402eD(this);
    public final AnonymousClass2Dn A0g = new C851041e(this);
    public final C27131Gd A0h = new AnonymousClass423(this);
    public final AbstractC18860tB A0i = new C35271hY(this);
    public final AbstractC33331dp A0j = new C858944o(this);

    public static StatusPlaybackContactFragment A00(UserJid userJid, AnonymousClass1IS r5) {
        StatusPlaybackContactFragment statusPlaybackContactFragment = new StatusPlaybackContactFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", userJid.getRawString());
        C38211ni.A08(bundle, r5, "");
        statusPlaybackContactFragment.A0U(bundle);
        return statusPlaybackContactFragment;
    }

    public static StatusPlaybackContactFragment A01(UserJid userJid, boolean z) {
        StatusPlaybackContactFragment statusPlaybackContactFragment = new StatusPlaybackContactFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", userJid.getRawString());
        bundle.putBoolean("unseen_only", z);
        statusPlaybackContactFragment.A0U(bundle);
        return statusPlaybackContactFragment;
    }

    public static /* synthetic */ boolean A02(StatusPlaybackContactFragment statusPlaybackContactFragment, int i, int i2) {
        List list = statusPlaybackContactFragment.A0b;
        if (list == null) {
            return false;
        }
        if (statusPlaybackContactFragment.A00 < list.size() - 1) {
            statusPlaybackContactFragment.A1M(statusPlaybackContactFragment.A00 + 1);
            statusPlaybackContactFragment.A1P(statusPlaybackContactFragment.A1I(), i, i2);
            return true;
        }
        AbstractC116205Un r1 = (AbstractC116205Un) statusPlaybackContactFragment.A0B();
        if (r1 == null) {
            return false;
        }
        UserJid userJid = statusPlaybackContactFragment.A0P;
        AnonymousClass009.A05(userJid);
        return r1.ARa(userJid.getRawString(), i, i2, true);
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment, X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        this.A0B.A03(this.A0h);
        this.A0J.A03(this.A0i);
        this.A09.A03(this.A0g);
        this.A0O.A03(this.A0j);
        this.A0X.Aaz(this.A0V, new Void[0]);
        UserJid userJid = this.A0P;
        if (userJid != C29831Uv.A00) {
            C15370n3 A0B = this.A0A.A0B(userJid);
            if (A0B.A0j) {
                A0B.A0j = false;
                this.A0X.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(this, 48, A0B));
            }
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment, com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        for (AbstractC33631eh r1 : this.A0f.A05().values()) {
            if (r1 != null && r1.A03) {
                r1.A02();
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i != 2) {
            super.A0t(i, i2, intent);
        } else if (i2 == -1 && intent != null) {
            List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
            C32731ce r3 = null;
            if (AnonymousClass4ZH.A00(this.A0N, A07)) {
                r3 = (C32731ce) intent.getParcelableExtra("status_distribution");
            }
            this.A08.A06(this.A06, r3, this.A0Q, A07);
            if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                ((ActivityC13790kL) A0B()).A2a(A07);
            } else {
                C51122Sx.A00(new C14960mK().A0i(A0p(), (AbstractC14640lm) A07.get(0)), this);
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        AbstractC15340mz r0 = this.A0Q;
        if (r0 != null) {
            C38211ni.A08(bundle, r0.A0z, "");
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A0B.A04(this.A0h);
        this.A0J.A04(this.A0i);
        this.A09.A04(this.A0g);
        this.A0O.A04(this.A0j);
        AnonymousClass389 r1 = this.A0V;
        if (r1 != null) {
            r1.A03(true);
        }
        AnonymousClass1J1 r0 = this.A0D;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A0f.A06(-1);
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment, com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
        for (AbstractC33631eh r1 : this.A0f.A05().values()) {
            if (r1 != null && !r1.A03) {
                r1.A03();
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        AnonymousClass1IS A03;
        super.A16(bundle);
        this.A0P = C15380n4.A02(A03().getString("jid"));
        this.A0e = ((AnonymousClass01E) this).A05.getBoolean("unseen_only");
        if (bundle != null && (A03 = C38211ni.A03(bundle, "")) != null) {
            this.A0Q = this.A0I.A0K.A03(A03);
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        C33651en r3 = ((StatusPlaybackBaseFragment) this).A03;
        AnonymousClass009.A06(r3, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        boolean z = false;
        if (this.A0P == C29831Uv.A00) {
            z = true;
        }
        View view2 = r3.A03;
        int i = 0;
        if (z) {
            i = 8;
        }
        view2.setVisibility(i);
        this.A0D = this.A0E.A04(A01(), "status-playback-contact-fragment");
        A1K();
        AnonymousClass1IS A03 = C38211ni.A03(A03(), "");
        C18470sV r4 = this.A0M;
        this.A0V = new AnonymousClass389(this.A0I, this.A0K, this.A0L, r4, this.A0P, A03, this, this.A0Z, this.A0e);
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment
    public void A1A() {
        super.A1A();
        if (this.A0b != null) {
            int i = this.A00;
            this.A00 = -1;
            if (i == -1) {
                i = 0;
            }
            A1M(i);
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment
    public void A1B() {
        super.A1B();
        AbstractC33631eh A1I = A1I();
        if (A1I != null && A1I.A04) {
            A1I.A05();
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment
    public void A1H(boolean z) {
        super.A1H(z);
        AbstractC33631eh A1I = A1I();
        if (A1I != null) {
            ((AbstractC33621eg) A1I).A0A().A09(z);
        }
    }

    public final AbstractC33631eh A1I() {
        List list;
        int i = this.A00;
        if (i < 0 || (list = this.A0b) == null || i >= list.size()) {
            return null;
        }
        return (AbstractC33631eh) this.A0f.A04(((AbstractC15340mz) this.A0b.get(this.A00)).A0z);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v6, types: [X.1iV] */
    public final AbstractC33631eh A1J(AbstractC15340mz r60) {
        C35551iF r2;
        C33651en r5 = ((StatusPlaybackBaseFragment) this).A03;
        AnonymousClass009.A06(r5, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        C006202y r4 = this.A0f;
        AnonymousClass1IS r3 = r60.A0z;
        AbstractC33631eh r22 = (AbstractC33631eh) r4.A04(r3);
        C35551iF r23 = r22;
        if (r22 == null) {
            C26101Ca r24 = this.A0W;
            C48252Fe r1 = new C48252Fe(r60, this);
            AnonymousClass1J1 r0 = this.A0D;
            if (r3.A02) {
                C14830m7 r02 = r24.A0C;
                Mp4Ops mp4Ops = r24.A03;
                C14850m9 r03 = r24.A0K;
                AnonymousClass1CY r04 = r24.A0T;
                C14900mE r05 = r24.A02;
                C239613r r06 = r24.A04;
                AbstractC14440lR r07 = r24.A0U;
                C16590pI r08 = r24.A0D;
                AbstractC15710nm r09 = r24.A01;
                C18790t3 r010 = r24.A05;
                C18470sV r011 = r24.A0J;
                C16170oZ r012 = r24.A06;
                AnonymousClass12P r013 = r24.A00;
                C21270x9 r014 = r24.A0B;
                C244415n r015 = r24.A0M;
                C15550nR r016 = r24.A08;
                C22810zg r017 = r24.A0I;
                C15610nY r018 = r24.A0A;
                AnonymousClass018 r019 = r24.A0F;
                AnonymousClass1CH r020 = r24.A0O;
                AnonymousClass10S r021 = r24.A09;
                C15650ng r022 = r24.A0G;
                AnonymousClass12H r023 = r24.A0H;
                C15860o1 r024 = r24.A0P;
                C22330yu r14 = r24.A07;
                C14820m6 r13 = r24.A0E;
                C17020q8 r12 = r24.A0V;
                AnonymousClass1BD r11 = r24.A0Q;
                AnonymousClass1CW r10 = r24.A0R;
                r2 = new C35681iV(r013, r09, r05, mp4Ops, r06, r010, r012, r14, r016, r021, r018, r0, r014, r02, r08, r13, r019, r022, r023, r017, r011, r03, r24.A0L, r015, r24.A0N, r020, r60, r024, r11, r10, r24.A0S, r1, r04, r07, r12, r24.A0W);
            } else {
                Mp4Ops mp4Ops2 = r24.A03;
                C14850m9 r025 = r24.A0K;
                AnonymousClass1CY r026 = r24.A0T;
                C14900mE r027 = r24.A02;
                C239613r r028 = r24.A04;
                AbstractC14440lR r029 = r24.A0U;
                C16590pI r030 = r24.A0D;
                AbstractC15710nm r031 = r24.A01;
                C18790t3 r032 = r24.A05;
                C18470sV r033 = r24.A0J;
                C16170oZ r034 = r24.A06;
                AnonymousClass12P r035 = r24.A00;
                C244415n r036 = r24.A0M;
                C22810zg r037 = r24.A0I;
                AnonymousClass018 r038 = r24.A0F;
                AnonymousClass1CH r15 = r24.A0O;
                C15650ng r142 = r24.A0G;
                AnonymousClass12H r132 = r24.A0H;
                C15860o1 r122 = r24.A0P;
                C17020q8 r112 = r24.A0V;
                r2 = new C35551iF(r035, r031, r027, mp4Ops2, r028, r032, r034, r0, r030, r038, r142, r132, r037, r033, r025, r036, r24.A0N, r15, r60, r122, r24.A0Q, r24.A0R, r24.A0S, r1, r026, r029, r112);
            }
            ViewGroup viewGroup = r5.A07;
            boolean z = false;
            if (((AnonymousClass01E) this).A03 >= 7) {
                z = true;
            }
            Rect rect = ((StatusPlaybackFragment) this).A01;
            if (!((AbstractC33631eh) r2).A01) {
                ((AbstractC33631eh) r2).A01 = true;
                StringBuilder sb = new StringBuilder("playbackPage/onCreate page=");
                sb.append(r2);
                sb.append("; host=");
                sb.append(r2.A0L.A01);
                Log.i(sb.toString());
                View A09 = r2.A09(LayoutInflater.from(viewGroup.getContext()), viewGroup);
                ((AbstractC33631eh) r2).A00 = A09;
                r2.A0K(A09);
                r2.A0A().A06();
                r2.A0M(r2.A0O());
                r2.A08(rect);
                if (z && !((AbstractC33631eh) r2).A03) {
                    r2.A03();
                }
            }
            r4.A08(r3, r2);
            r23 = r2;
        }
        return r23;
    }

    public final void A1K() {
        C33651en r3 = ((StatusPlaybackBaseFragment) this).A03;
        AnonymousClass009.A06(r3, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        C15550nR r1 = this.A0A;
        AbstractC14640lm r0 = this.A0P;
        C29831Uv r6 = C29831Uv.A00;
        if (r0 == r6) {
            C15570nT r02 = this.A04;
            r02.A08();
            r0 = r02.A05;
            AnonymousClass009.A05(r0);
        }
        C15370n3 A0B = r1.A0B(r0);
        AnonymousClass1J1 r12 = this.A0D;
        if (r12 != null) {
            r12.A06(r3.A0B, A0B);
        }
        C28801Pb r2 = new C28801Pb(r3.A09, this.A0C, this.A0S, (int) R.id.name);
        if (this.A0P == r6) {
            r2.A03();
        } else {
            r2.A09(null, this.A0C.A07(A0B));
            r2.A05(C15380n4.A0M(this.A0P) ? 1 : 0);
        }
        UserJid userJid = this.A0P;
        if (!C15380n4.A0L(userJid) || userJid == r6) {
            r3.A0B.setClickable(false);
            r3.A04.setClickable(false);
            return;
        }
        r3.A0B.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(this, r3, A0B, 15));
        r3.A04.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(this, r3, A0B, 14));
    }

    public final void A1L() {
        C16150oX r1;
        C33651en r12 = ((StatusPlaybackBaseFragment) this).A03;
        AnonymousClass009.A06(r12, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        StatusPlaybackProgressView statusPlaybackProgressView = r12.A0E;
        statusPlaybackProgressView.setCount(this.A0b.size());
        Set set = statusPlaybackProgressView.A08;
        set.clear();
        if (this.A0P == C29831Uv.A00) {
            int i = 0;
            for (AbstractC15340mz r2 : this.A0b) {
                if ((r2 instanceof AbstractC16130oV) && (r1 = ((AbstractC16130oV) r2).A02) != null && !r1.A0P && !r1.A0a && (!(r2 instanceof AnonymousClass1X2) || !C30041Vv.A15((AnonymousClass1X4) r2))) {
                    set.add(Integer.valueOf(i));
                }
                i++;
            }
        }
    }

    public final void A1M(int i) {
        List list;
        int i2;
        AnonymousClass1V1 r7;
        if (!(this.A00 == i || (list = this.A0b) == null)) {
            if (list.isEmpty()) {
                StringBuilder sb = new StringBuilder("playbackFragment/setPageActive no-messages ");
                sb.append(this);
                Log.w(sb.toString());
                return;
            }
            this.A00 = i;
            C33651en r6 = ((StatusPlaybackBaseFragment) this).A03;
            AnonymousClass009.A06(r6, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
            StatusPlaybackProgressView statusPlaybackProgressView = r6.A0E;
            statusPlaybackProgressView.setPosition(i);
            statusPlaybackProgressView.setProgressProvider(null);
            AbstractC15340mz r3 = (AbstractC15340mz) this.A0b.get(i);
            if (C15380n4.A0M(r3.A0B()) && (r7 = (AnonymousClass1V1) this.A0c.get(Long.valueOf(r3.A11))) != null) {
                this.A0U.A0G.put(r3.A0z.A01, Boolean.FALSE);
                String str = r7.A03;
                String str2 = r7.A02;
                if (str == null || str2 == null) {
                    r6.A08.setVisibility(8);
                } else {
                    Button button = r6.A00;
                    if (button == null) {
                        button = (Button) r6.A08.inflate();
                        r6.A00 = button;
                    }
                    button.setText(r7.A02);
                    button.setOnClickListener(new ViewOnClickCListenerShape0S1200000_I0(this, r3, str, 2));
                    button.setVisibility(0);
                }
                this.A0a = r7.A04;
            }
            AbstractC33631eh A1J = A1J(r3);
            View view = r6.A05;
            if (!(((AbstractC33621eg) A1J).A0A() instanceof AnonymousClass35V)) {
                i2 = 0;
            } else {
                i2 = 4;
            }
            view.setVisibility(i2);
            View view2 = A1J.A00;
            ViewGroup viewGroup = r6.A07;
            if (viewGroup.getChildCount() == 0 || viewGroup.getChildAt(0) != view2) {
                viewGroup.removeAllViews();
                viewGroup.addView(view2);
            }
            for (AbstractC33631eh r1 : this.A0f.A05().values()) {
                if (!(r1 == A1J || r1 == null || !r1.A04)) {
                    r1.A05();
                }
            }
            A1O(r3);
            if (!A1J.A04) {
                A1J.A04();
            }
            if (i < this.A0b.size() - 1) {
                A1J((AbstractC15340mz) this.A0b.get(i + 1));
            }
            if (i > 0) {
                A1J((AbstractC15340mz) this.A0b.get(i - 1));
            }
            this.A0F.A09(this.A0P, 9);
        }
    }

    public final void A1N(C15370n3 r5, C33651en r6) {
        ActivityC000900k A0C = A0C();
        A0C.startActivity(new C14960mK().A0h(A0C, r5, 5), AbstractC454421p.A05(A0C, r6.A0B, new AnonymousClass2TT(A0C).A00(R.string.transition_photo)));
    }

    public final void A1O(AbstractC15340mz r7) {
        C14830m7 r4;
        AnonymousClass018 r2;
        long j;
        int i;
        C16150oX r1;
        C33651en r12 = ((StatusPlaybackBaseFragment) this).A03;
        AnonymousClass009.A06(r12, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        if (!C15380n4.A0M(this.A0P)) {
            TextView textView = r12.A0C;
            textView.setVisibility(0);
            if (!r7.A0z.A02) {
                r4 = this.A0G;
                r2 = ((StatusPlaybackBaseFragment) this).A02;
                j = r7.A0I;
            } else if (C37381mH.A00(r7.A0C, 4) >= 0) {
                j = r7.A0H;
                if (j <= 0) {
                    j = r7.A0I;
                }
                r4 = this.A0G;
                r2 = ((StatusPlaybackBaseFragment) this).A02;
            } else {
                if (!(r7 instanceof AbstractC16130oV) || (r1 = ((AbstractC16130oV) r7).A02) == null || r1.A0P || r1.A0a) {
                    boolean A0m = C30041Vv.A0m(r7);
                    i = R.string.sending_status_progress;
                    if (A0m) {
                        i = R.string.deleting_status_progress;
                    }
                } else {
                    i = R.string.sending_status_failed;
                }
                textView.setText(i);
                return;
            }
            textView.setText(C38131nZ.A01(r2, r4.A02(j)));
            return;
        }
        r12.A0C.setVisibility(8);
    }

    public final void A1P(AbstractC33631eh r4, int i, int i2) {
        for (AbstractC33631eh r1 : this.A0f.A05().values()) {
            if (!(r1 == r4 || r1 == null || !r1.A05)) {
                r1.A06(i);
            }
        }
        if (!(r4 == null || r4.A05)) {
            r4.A07(i2);
        }
    }

    @Override // X.AbstractC33391dx
    public void AP7(DialogFragment dialogFragment, boolean z) {
        this.A0d = z;
        A19();
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AbstractC33631eh A1I = A1I();
        if (A1I != null) {
            A1I.A00();
        }
    }

    @Override // X.AnonymousClass01E, java.lang.Object
    public String toString() {
        UserJid userJid = this.A0P;
        if (userJid != null) {
            return userJid.toString();
        }
        String string = A03().getString("jid");
        AnonymousClass009.A05(string);
        return string;
    }
}
