package com.whatsapp.status.playback;

import X.AbstractActivityC33661er;
import X.AbstractC116455Vm;
import X.AbstractC13910kX;
import X.AbstractC13950kb;
import X.AbstractC13970kd;
import X.AbstractC14640lm;
import X.AbstractC14670lq;
import X.AbstractC15340mz;
import X.AbstractC253919f;
import X.AbstractC32741cf;
import X.AbstractC32851cq;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass028;
import X.AnonymousClass0L1;
import X.AnonymousClass116;
import X.AnonymousClass11P;
import X.AnonymousClass146;
import X.AnonymousClass193;
import X.AnonymousClass19N;
import X.AnonymousClass19O;
import X.AnonymousClass19P;
import X.AnonymousClass19X;
import X.AnonymousClass1A2;
import X.AnonymousClass1A4;
import X.AnonymousClass1AA;
import X.AnonymousClass1AB;
import X.AnonymousClass1AD;
import X.AnonymousClass1BD;
import X.AnonymousClass1BP;
import X.AnonymousClass1BQ;
import X.AnonymousClass1J1;
import X.AnonymousClass1KT;
import X.AnonymousClass1Ly;
import X.AnonymousClass1Y6;
import X.AnonymousClass2IC;
import X.AnonymousClass3JF;
import X.AnonymousClass3UI;
import X.AnonymousClass4UK;
import X.AnonymousClass5V5;
import X.C004802e;
import X.C1104755r;
import X.C14410lO;
import X.C14650lo;
import X.C14960mK;
import X.C15260mp;
import X.C15320mw;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C15610nY;
import X.C15650ng;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C17020q8;
import X.C17050qB;
import X.C21200x2;
import X.C21270x9;
import X.C22190yg;
import X.C22210yi;
import X.C231510o;
import X.C235512c;
import X.C238013b;
import X.C239613r;
import X.C244415n;
import X.C252718t;
import X.C253619c;
import X.C253719d;
import X.C254719n;
import X.C255519v;
import X.C36021jC;
import X.C37471mS;
import X.C39511q1;
import X.C42531vM;
import X.C458223h;
import X.C458423j;
import X.C53502eY;
import X.C624537h;
import X.C63413Bm;
import X.C66003Lx;
import X.C66013Ly;
import X.C69893aP;
import X.C70843bw;
import X.C74503iB;
import android.animation.AnimatorSet;
import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0202000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0500000_I0;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.picker.search.PickerSearchDialogFragment;
import com.whatsapp.status.playback.MessageReplyActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class MessageReplyActivity extends AbstractActivityC33661er implements AbstractC13910kX, AbstractC13950kb, AbstractC13970kd {
    public static final Interpolator A1D = AnonymousClass0L1.A00(0.32f, 0.0f, 0.67f, 0.0f);
    public static final Interpolator A1E = AnonymousClass0L1.A00(0.33f, 1.0f, 0.68f, 1.0f);
    public static final HashMap A1F = new HashMap();
    public static final HashMap A1G = new HashMap();
    public AnimatorSet A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public ViewGroup A05;
    public FrameLayout A06;
    public ImageButton A07;
    public ImageButton A08;
    public ImageButton A09;
    public ImageButton A0A;
    public ImageButton A0B;
    public C239613r A0C;
    public C16170oZ A0D;
    public C254719n A0E;
    public C14650lo A0F;
    public AnonymousClass19N A0G;
    public AnonymousClass19X A0H;
    public AnonymousClass1AA A0I;
    public C253619c A0J;
    public C238013b A0K;
    public AnonymousClass116 A0L;
    public C15550nR A0M;
    public C15610nY A0N;
    public AnonymousClass1J1 A0O;
    public C21270x9 A0P;
    public C42531vM A0Q;
    public AnonymousClass11P A0R;
    public AnonymousClass19P A0S;
    public C17050qB A0T;
    public C15890o4 A0U;
    public AnonymousClass1AD A0V;
    public C15650ng A0W;
    public C231510o A0X;
    public AnonymousClass193 A0Y;
    public C16120oU A0Z;
    public AnonymousClass1BP A0a;
    public C15260mp A0b;
    public C15320mw A0c;
    public C253719d A0d;
    public AbstractC253919f A0e;
    public AbstractC14640lm A0f;
    public C244415n A0g;
    public C14410lO A0h;
    public MentionableEntry A0i;
    public C74503iB A0j;
    public AnonymousClass1A2 A0k;
    public C21200x2 A0l;
    public C69893aP A0m;
    public C16630pM A0n;
    public AbstractC15340mz A0o;
    public C22210yi A0p;
    public AnonymousClass1AB A0q;
    public AnonymousClass146 A0r;
    public C235512c A0s;
    public C255519v A0t;
    public AnonymousClass1BQ A0u;
    public AnonymousClass1Ly A0v;
    public AnonymousClass1A4 A0w;
    public AnonymousClass1KT A0x;
    public C22190yg A0y;
    public AnonymousClass19O A0z;
    public C624537h A10;
    public C17020q8 A11;
    public AbstractC14670lq A12;
    public AnonymousClass2IC A13;
    public boolean A14 = false;
    public boolean A15;
    public boolean A16;
    public final AbstractC116455Vm A17 = new AnonymousClass3UI(this);
    public final AbstractC32851cq A18 = new C1104755r(this);
    public final Set A19 = new HashSet();
    public final Set A1A = new HashSet();
    public final Set A1B = new HashSet();
    public final int[] A1C = new int[2];

    @Override // X.AbstractC13910kX
    public void A6G() {
    }

    @Override // X.AbstractC13910kX
    public void Aai() {
    }

    public static /* synthetic */ void A02(Resources resources, View view, ViewGroup viewGroup, ImageView imageView, WaTextView waTextView, MessageReplyActivity messageReplyActivity, int[] iArr, boolean z) {
        C37471mS r1 = new C37471mS(iArr);
        C16630pM r0 = messageReplyActivity.A0n;
        if (z) {
            AnonymousClass3JF.A01(r0, iArr);
        } else {
            AnonymousClass3JF.A02(r0, iArr);
        }
        messageReplyActivity.A2i(resources, view, viewGroup, imageView, waTextView, (int[]) r1.A00.clone());
    }

    public void A2e() {
        if (this instanceof StatusReplyActivity) {
            StatusReplyActivity statusReplyActivity = (StatusReplyActivity) this;
            AnonymousClass1BD r2 = statusReplyActivity.A02;
            r2.A0D.Ab2(new RunnableBRunnable0Shape0S0202000_I0(r2, 2, statusReplyActivity.A0o, 1, 5));
        }
    }

    public final void A2f() {
        this.A01.setVisibility(8);
        MentionableEntry mentionableEntry = this.A0i;
        if (mentionableEntry != null && C252718t.A00(mentionableEntry)) {
            ((ActivityC13790kL) this).A0D.A01(this.A0i);
        }
        finish();
    }

    public final void A2g() {
        A1G.put(this.A0o.A0z, AbstractC32741cf.A04(this.A0i.getStringText()));
        A1F.put(this.A0o.A0z, AnonymousClass1Y6.A00(this.A0i.getMentions()));
    }

    public void A2h(int i) {
        int i2;
        C63413Bm r0;
        AnonymousClass4UK r1;
        if (this instanceof StatusReplyActivity) {
            StatusReplyActivity statusReplyActivity = (StatusReplyActivity) this;
            AnonymousClass1BD r2 = statusReplyActivity.A02;
            AbstractC15340mz r4 = statusReplyActivity.A0o;
            switch (i) {
                case 1:
                    i2 = 1;
                    break;
                case 2:
                    i2 = 2;
                    break;
                case 3:
                    i2 = 3;
                    break;
                case 4:
                    i2 = 4;
                    break;
                case 5:
                    i2 = 5;
                    break;
                case 6:
                    i2 = 6;
                    break;
                case 7:
                    i2 = 7;
                    break;
                case 8:
                    i2 = 8;
                    break;
                case 9:
                    i2 = 9;
                    break;
                case 10:
                    i2 = 10;
                    break;
                case 11:
                    i2 = 11;
                    break;
                case 12:
                    i2 = 12;
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    i2 = 13;
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    i2 = 14;
                    break;
                case 15:
                    i2 = 15;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            C458223h r12 = r2.A00;
            if (r12 != null) {
                r12.A02++;
            }
            C458423j r02 = r2.A01;
            if (!(r02 == null || (r0 = (C63413Bm) r02.A0F.get(AnonymousClass1BD.A01(r4))) == null || (r1 = (AnonymousClass4UK) r0.A08.get(r4.A0z)) == null)) {
                r1.A00++;
            }
            r2.A0D.Ab2(new RunnableBRunnable0Shape0S0202000_I0(r2, 1, r4, i2, 5));
        }
        if (this.A15) {
            String string = getString(R.string.sending_reply);
            View inflate = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) null);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.toast_text)).setText(string);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(87, 0, 0);
            toast.setDuration(1);
            toast.setView(inflate);
            toast.show();
        } else {
            ((ActivityC13810kN) this).A05.A07(R.string.sending_reply, 0);
        }
        A1G.remove(this.A0o.A0z);
        A1F.remove(this.A0o.A0z);
        A2f();
    }

    public final void A2i(Resources resources, View view, ViewGroup viewGroup, ImageView imageView, WaTextView waTextView, int[] iArr) {
        imageView.setImageDrawable(((ActivityC13810kN) this).A0B.A04(resources, new C39511q1(iArr), 1.0f, -1));
        AnonymousClass028.A0g(view, new C53502eY(this));
        view.setOnClickListener(new ViewOnClickCListenerShape0S0500000_I0(this, view, viewGroup, waTextView, iArr, 2));
    }

    public void A2j(C66013Ly r15) {
        if (this.A0K.A0I(UserJid.of(this.A0f))) {
            C36021jC.A01(this, 106);
            return;
        }
        ArrayList arrayList = new ArrayList();
        AbstractC14640lm r4 = this.A0f;
        arrayList.add(r4);
        C66003Lx r1 = r15.A01;
        int i = r1.A01;
        if (i <= 0) {
            i = r15.A02.A01;
        }
        int i2 = r1.A00;
        if (i2 <= 0) {
            i2 = r15.A02.A00;
        }
        String str = r15.A02.A02;
        String str2 = r1.A02;
        String str3 = r15.A03.A02;
        int i3 = r15.A00;
        int i4 = 22;
        if (C15380n4.A0N(r4)) {
            i4 = 24;
        }
        startActivityForResult(C14960mK.A0Z(this, str, str2, str3, arrayList, i3, i4, false, false).putExtra("jid", this.A0f.getRawString()).putExtra("media_width", i).putExtra("media_height", i2).putExtra("mentions", C15380n4.A06(this.A0i.getMentions())).putExtra("caption", AbstractC32741cf.A04(this.A0i.getStringText())).putExtra("usage_quote", true), 25);
    }

    public final void A2k(String str, int i) {
        setResult(-1);
        C16170oZ r1 = this.A0D;
        List singletonList = Collections.singletonList(this.A0f);
        r1.A08(null, null, this.A0o, AbstractC32741cf.A04(str), singletonList, this.A0i.getMentions(), false, false);
        A2h(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0070, code lost:
        if (r2.codePointCount(0, r2.length()) <= 65536) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2l(boolean r6) {
        /*
            r5 = this;
            X.13b r1 = r5.A0K
            X.0lm r0 = r5.A0f
            com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.of(r0)
            boolean r0 = r1.A0I(r0)
            if (r0 == 0) goto L_0x0014
            r3 = 106(0x6a, float:1.49E-43)
        L_0x0010:
            X.C36021jC.A01(r5, r3)
            return
        L_0x0014:
            X.0lq r2 = r5.A12
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0037
            X.0ls r1 = r2.A0P
            r0 = 0
            if (r1 == 0) goto L_0x0020
            r0 = 1
        L_0x0020:
            r1 = 4
            if (r0 == 0) goto L_0x002a
            r2.A0T(r3, r4)
        L_0x0026:
            r5.A2h(r1)
            return
        L_0x002a:
            java.io.File r0 = r2.A0Q
            if (r0 == 0) goto L_0x0037
            r2.A0Q(r4)
            X.0lq r0 = r5.A12
            r0.A0N(r3)
            goto L_0x0026
        L_0x0037:
            android.widget.ImageButton r0 = r5.A0A
            r0.setEnabled(r4)
            com.whatsapp.mentions.MentionableEntry r0 = r5.A0i
            java.lang.String r0 = r0.getStringText()
            java.lang.String r2 = r0.trim()
            X.01d r1 = r5.A08
            X.0pM r0 = r5.A0n
            boolean r0 = X.C42971wC.A0C(r1, r0, r2)
            if (r0 != 0) goto L_0x0059
            X.0mE r1 = r5.A05
            r0 = 2131886933(0x7f120355, float:1.9408459E38)
            r1.A07(r0, r3)
            return
        L_0x0059:
            r1 = 65536(0x10000, float:9.18355E-41)
            if (r6 == 0) goto L_0x0066
            java.lang.String r2 = X.AnonymousClass1US.A03(r1, r2)
        L_0x0061:
            r0 = 2
            r5.A2k(r2, r0)
            return
        L_0x0066:
            int r0 = r2.length()
            int r0 = r2.codePointCount(r4, r0)
            r3 = 17
            if (r0 > r1) goto L_0x0010
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.MessageReplyActivity.A2l(boolean):void");
    }

    public final boolean A2m() {
        Rect rect = new Rect();
        this.A05.getWindowVisibleDisplayFrame(rect);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int height = this.A05.getRootView().getHeight() - (rect.bottom - rect.top);
        if (((int) (((float) height) / displayMetrics.density)) <= 100) {
            return false;
        }
        int dimensionPixelSize = (displayMetrics.heightPixels - height) + getResources().getDimensionPixelSize(R.dimen.conversation_entry_input_height);
        ViewGroup.LayoutParams layoutParams = this.A06.getLayoutParams();
        layoutParams.height = dimensionPixelSize;
        this.A06.setLayoutParams(layoutParams);
        this.A00.start();
        return true;
    }

    @Override // X.AbstractC13910kX
    public void A6H(int i) {
        A2h(i);
    }

    @Override // X.AbstractC13910kX
    public AbstractC15340mz AG1() {
        return this.A0o;
    }

    @Override // X.AbstractC13950kb
    public void ATj(PickerSearchDialogFragment pickerSearchDialogFragment) {
        this.A0m.A01(pickerSearchDialogFragment);
    }

    @Override // X.AbstractC13970kd
    public void AUl() {
        this.A0Q.A01();
    }

    @Override // X.AbstractC13970kd
    public void AXQ() {
        this.A0Q.A00();
    }

    @Override // X.AbstractC13950kb
    public void Adk(DialogFragment dialogFragment) {
        Adm(dialogFragment);
        getWindow().setSoftInputMode(1);
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.A0c.A01()) {
            View view = this.A02;
            int[] iArr = this.A1C;
            view.getLocationOnScreen(iArr);
            if (motionEvent.getRawY() >= ((float) iArr[1]) && motionEvent.getRawY() < ((float) (iArr[1] + this.A02.getHeight()))) {
                if (motionEvent.getAction() == 0) {
                    this.A16 = true;
                } else if (motionEvent.getAction() == 1) {
                    if (this.A16) {
                        this.A0c.A00(true);
                        this.A02.requestFocus();
                    }
                    this.A16 = false;
                }
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a0, code lost:
        if (r9.A0F != null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a2, code lost:
        r5 = r24.getIntExtra("provider", 0);
        r0 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00aa, code lost:
        if (r5 == 1) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ac, code lost:
        r0 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ad, code lost:
        if (r5 == 2) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00af, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00b0, code lost:
        r9.A05 = r0;
        r21.A0D.A05(new X.C38421o4(java.util.Collections.singletonList(r21.A0h.A03(r8, r9, null, r21.A0f, r21.A0o, r24.getStringExtra("caption"), null, X.C15380n4.A07(com.whatsapp.jid.UserJid.class, r24.getStringArrayListExtra("mentions")), null, (byte) 13, 0, 0, false))), r3, false, false);
        r21.A0c.A00(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ed, code lost:
        A2h(14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x011b, code lost:
        if (r8 == null) goto L_0x009e;
     */
    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r22, int r23, android.content.Intent r24) {
        /*
        // Method dump skipped, instructions count: 286
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.MessageReplyActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.A15) {
            int i = 0;
            boolean z = false;
            if (configuration.orientation == 2) {
                z = true;
            }
            FrameLayout frameLayout = this.A06;
            if (z) {
                i = 8;
            }
            frameLayout.setVisibility(i);
            if (!z) {
                A2m();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        if (((X.ActivityC13810kN) r60).A0C.A07(1455) == false) goto L_0x0022;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r61) {
        /*
        // Method dump skipped, instructions count: 1540
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.MessageReplyActivity.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        if (i == 17) {
            r3 = new C004802e(this);
            r3.A0A(getString(R.string.cant_send_message_too_long_with_placeholder, 65536));
            r3.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() { // from class: X.4gF
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    MessageReplyActivity messageReplyActivity = MessageReplyActivity.this;
                    messageReplyActivity.A2l(true);
                    C36021jC.A00(messageReplyActivity, 17);
                }
            });
            r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4gE
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(MessageReplyActivity.this, 17);
                }
            });
        } else if (i != 106) {
            return super.onCreateDialog(i);
        } else {
            C15370n3 A0B = this.A0M.A0B(this.A0f);
            r3 = new C004802e(this);
            r3.A0A(getString(R.string.cannot_send_to_blocked_contact_1, this.A0N.A04(A0B)));
            r3.setPositiveButton(R.string.unblock, new DialogInterface.OnClickListener(A0B, this) { // from class: X.3KY
                public final /* synthetic */ C15370n3 A00;
                public final /* synthetic */ MessageReplyActivity A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    MessageReplyActivity messageReplyActivity = this.A01;
                    messageReplyActivity.A0K.A0C(messageReplyActivity, C15370n3.A04(this.A00));
                    C36021jC.A00(messageReplyActivity, 106);
                    messageReplyActivity.A0i.A04(true);
                }
            });
            r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4gD
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    MessageReplyActivity messageReplyActivity = MessageReplyActivity.this;
                    C36021jC.A00(messageReplyActivity, 106);
                    messageReplyActivity.A2f();
                }
            });
            r3.A08(new DialogInterface.OnCancelListener() { // from class: X.4fA
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    MessageReplyActivity.this.A2f();
                }
            });
        }
        return r3.create();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0m.A00();
        AbstractC14670lq r0 = this.A12;
        if (r0 != null) {
            r0.A02();
        }
        C15260mp r02 = this.A0b;
        if (r02 != null) {
            r02.A0G();
        }
        AnonymousClass1AB r03 = this.A0q;
        if (r03 != null) {
            r03.A03();
        }
        AnonymousClass1J1 r04 = this.A0O;
        if (r04 != null) {
            r04.A00();
            this.A0O = null;
        }
        C624537h r1 = this.A10;
        if (r1 != null) {
            r1.A03(true);
            this.A10 = null;
        }
        Set<Application.ActivityLifecycleCallbacks> set = this.A1A;
        for (Application.ActivityLifecycleCallbacks activityLifecycleCallbacks : set) {
            activityLifecycleCallbacks.onActivityDestroyed(this);
        }
        set.clear();
        this.A19.clear();
        this.A1B.clear();
        this.A0x.A03();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Iterator it = this.A1B.iterator();
        while (it.hasNext()) {
            it.next();
        }
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        AbstractC14670lq r1 = this.A12;
        if (r1 != null && r1.A0P != null) {
            r1.A03();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        for (Application.ActivityLifecycleCallbacks activityLifecycleCallbacks : this.A1A) {
            activityLifecycleCallbacks.onActivityResumed(this);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        Window window = getWindow();
        int i = 4;
        if (this.A0b.isShowing()) {
            i = 2;
        }
        window.setSoftInputMode(i | 1);
        if (!this.A0b.isShowing()) {
            this.A0i.A04(true);
        }
        C624537h r1 = this.A10;
        if (r1 != null) {
            r1.A03(true);
            this.A10 = null;
        }
        C70843bw r4 = new AnonymousClass5V5() { // from class: X.3bw
            @Override // X.AnonymousClass5V5
            public final void AVG(C90494Oc r6) {
                AnonymousClass1IS r12;
                MessageReplyActivity messageReplyActivity = MessageReplyActivity.this;
                C91054Qg r2 = r6.A01;
                if (r2 != null && (r12 = r2.A01) != null && r12.equals(messageReplyActivity.A0o.A0z)) {
                    messageReplyActivity.A12.A0M(r2.A02, null, true, false);
                }
            }
        };
        C624537h r2 = new C624537h(this.A0W, this.A0f, r4, this.A11);
        this.A10 = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00c0, code lost:
        if (r7 != false) goto L_0x00c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setStatusReactionsEmojiLayout(android.view.View r20) {
        /*
        // Method dump skipped, instructions count: 329
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.MessageReplyActivity.setStatusReactionsEmojiLayout(android.view.View):void");
    }
}
