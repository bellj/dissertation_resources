package com.whatsapp.status.playback.widget;

import X.AnonymousClass1US;
import X.AnonymousClass4GR;
import X.C64583Gc;
import android.content.Context;
import android.text.Layout;
import android.text.TextPaint;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes2.dex */
public class TextStatusContentView extends TextEmojiLabel {
    public float A00;
    public boolean A01;

    public TextStatusContentView(Context context) {
        super(context);
        A08();
    }

    public TextStatusContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
    }

    public TextStatusContentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
    }

    public TextStatusContentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A08();
    }

    public final void A0H() {
        if (this.A00 == 0.0f) {
            this.A00 = getPaint().getTextSize();
        }
        int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        if (measuredWidth > 0 && measuredHeight > 0) {
            CharSequence text = getText();
            TransformationMethod transformationMethod = getTransformationMethod();
            if (transformationMethod != null) {
                text = transformationMethod.getTransformation(text, this);
            }
            float f = this.A00;
            TextPaint textPaint = new TextPaint(getPaint());
            float f2 = f;
            float f3 = 2.0f;
            while (true) {
                if (f - f3 <= 2.0f) {
                    f2 = f3;
                    break;
                }
                textPaint.setTextSize(f2);
                C64583Gc.A00(textPaint, text);
                if (AnonymousClass4GR.A00.A8K(textPaint, this, AnonymousClass1US.A01(text), measuredWidth).getHeight() <= measuredHeight) {
                    if (this.A00 == f2) {
                        break;
                    }
                    f3 = f2;
                } else {
                    f = f2;
                }
                f2 = (f3 + f) / 2.0f;
            }
            textPaint.setTextSize(f2);
            while (textPaint.getTextSize() > 2.0f) {
                C64583Gc.A00(textPaint, text);
                Layout A8K = AnonymousClass4GR.A00.A8K(textPaint, this, AnonymousClass1US.A01(text), measuredWidth);
                if (A8K.getHeight() <= measuredHeight) {
                    int lineCount = A8K.getLineCount();
                    boolean z = true;
                    for (int i = 0; i < lineCount; i++) {
                        if (A8K.getLineMax(i) > ((float) measuredWidth)) {
                            z = false;
                        }
                    }
                    if (z) {
                        break;
                    }
                }
                textPaint.setTextSize(textPaint.getTextSize() - 1.0f);
            }
            super.setTextSize(0, textPaint.getTextSize());
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3 || i2 != i4) {
            A0H();
        }
    }

    @Override // X.C004602b, android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        A0H();
    }

    @Override // com.whatsapp.TextEmojiLabel, com.whatsapp.WaTextView, android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
        A0H();
    }

    @Override // X.C004602b, android.widget.TextView
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        this.A00 = getPaint().getTextSize();
        A0H();
    }
}
