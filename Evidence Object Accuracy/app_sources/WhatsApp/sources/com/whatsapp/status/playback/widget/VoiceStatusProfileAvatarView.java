package com.whatsapp.status.playback.widget;

import X.AnonymousClass028;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes2.dex */
public class VoiceStatusProfileAvatarView extends FrameLayout {
    public WaImageView A00;
    public WaImageView A01;

    public VoiceStatusProfileAvatarView(Context context) {
        super(context);
        A00(context);
    }

    public VoiceStatusProfileAvatarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context);
    }

    public VoiceStatusProfileAvatarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context);
    }

    public VoiceStatusProfileAvatarView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00(context);
    }

    public final void A00(Context context) {
        FrameLayout.inflate(context, R.layout.voice_status_profile_avatar, this);
        this.A01 = C12980iv.A0X(this, R.id.profile_avatar);
        this.A00 = C12980iv.A0X(this, R.id.mic_overlay);
    }

    public WaImageView getProfileAvatarImageView() {
        return this.A01;
    }

    public void setMicrophoneStrokeColor(int i) {
        this.A00.setBackground(C12970iu.A0C(getContext(), R.drawable.mic_background_incoming_normal).mutate());
        AnonymousClass028.A0M(ColorStateList.valueOf(i), this.A00);
    }
}
