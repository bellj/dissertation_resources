package com.whatsapp.status.playback;

import X.AbstractC005102i;
import X.AbstractC009504t;
import X.AbstractC116195Um;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14750lz;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.AbstractC18030rn;
import X.AbstractC18860tB;
import X.AbstractC32851cq;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass02B;
import X.AnonymousClass02Q;
import X.AnonymousClass07N;
import X.AnonymousClass109;
import X.AnonymousClass10U;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.AnonymousClass13H;
import X.AnonymousClass17R;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass1A6;
import X.AnonymousClass1BD;
import X.AnonymousClass1IS;
import X.AnonymousClass1VX;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2JG;
import X.AnonymousClass2z6;
import X.AnonymousClass36T;
import X.AnonymousClass375;
import X.AnonymousClass4LC;
import X.AnonymousClass4LL;
import X.C103554qw;
import X.C1104855s;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C17050qB;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C18850tA;
import X.C20710wC;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22180yf;
import X.C22370yy;
import X.C22670zS;
import X.C22700zV;
import X.C22810zg;
import X.C23000zz;
import X.C231510o;
import X.C239613r;
import X.C242114q;
import X.C248016x;
import X.C248116y;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253018w;
import X.C253218y;
import X.C255419u;
import X.C25611Ab;
import X.C28391Mz;
import X.C33471e8;
import X.C33671et;
import X.C35281hZ;
import X.C35541iC;
import X.C38121nY;
import X.C38211ni;
import X.C41691tw;
import X.C45041zy;
import X.C458223h;
import X.C52252aV;
import X.C63203Ar;
import X.C64153El;
import X.C70433bH;
import X.C88054Ec;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.status.StatusExpirationLifecycleOwner;
import com.whatsapp.status.playback.MyStatusesActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class MyStatusesActivity extends ActivityC13770kJ implements AbstractC14750lz {
    public AnonymousClass02Q A00;
    public AbstractC009504t A01;
    public AnonymousClass07N A02;
    public C239613r A03;
    public C16170oZ A04;
    public AnonymousClass2JG A05;
    public AnonymousClass12T A06;
    public C18850tA A07;
    public C15550nR A08;
    public C22700zV A09;
    public C15610nY A0A;
    public AnonymousClass1A6 A0B;
    public C255419u A0C;
    public C17050qB A0D;
    public C15890o4 A0E;
    public AnonymousClass018 A0F;
    public C15650ng A0G;
    public C15600nX A0H;
    public C253218y A0I;
    public AnonymousClass12H A0J;
    public C22810zg A0K;
    public C242114q A0L;
    public AnonymousClass10U A0M;
    public C18470sV A0N;
    public C22100yW A0O;
    public C22180yf A0P;
    public C231510o A0Q;
    public AnonymousClass193 A0R;
    public C16120oU A0S;
    public C20710wC A0T;
    public AnonymousClass109 A0U;
    public C22370yy A0V;
    public AnonymousClass13H A0W;
    public C16630pM A0X;
    public AbstractC15340mz A0Y;
    public AnonymousClass17R A0Z;
    public C25611Ab A0a;
    public C88054Ec A0b;
    public AnonymousClass12F A0c;
    public C253018w A0d;
    public AnonymousClass12U A0e;
    public StatusExpirationLifecycleOwner A0f;
    public C64153El A0g;
    public C248116y A0h;
    public C248016x A0i;
    public AnonymousClass1BD A0j;
    public C35541iC A0k;
    public C33671et A0l;
    public C70433bH A0m;
    public C23000zz A0n;
    public AnonymousClass4LL A0o;
    public AnonymousClass19O A0p;
    public AnonymousClass01H A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u;
    public final Handler A0v;
    public final View.OnClickListener A0w;
    public final View.OnClickListener A0x;
    public final AbstractC32851cq A0y;
    public final AbstractC18860tB A0z;
    public final AnonymousClass4LC A10;
    public final Runnable A11;
    public final Runnable A12;
    public final HashMap A13;
    public final HashMap A14;
    public final List A15;
    public final Map A16;
    public final Set A17;

    public MyStatusesActivity() {
        this(0);
        this.A16 = new LinkedHashMap();
        HashSet hashSet = new HashSet();
        this.A17 = hashSet;
        this.A11 = new RunnableBRunnable0Shape0S0100000_I0(hashSet, 21);
        this.A0v = new Handler(Looper.getMainLooper());
        this.A13 = new HashMap();
        this.A14 = new HashMap();
        this.A10 = new AnonymousClass4LC(this);
        this.A0z = new C35281hZ(this);
        this.A12 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 8);
        this.A15 = new ArrayList();
        this.A0t = false;
        this.A0u = false;
        this.A0y = new C1104855s(this);
        this.A0x = new ViewOnClickCListenerShape15S0100000_I0_2(this, 39);
        this.A0w = new ViewOnClickCListenerShape15S0100000_I0_2(this, 35);
    }

    public MyStatusesActivity(int i) {
        this.A0s = false;
        A0R(new C103554qw(this));
    }

    public static /* synthetic */ void A02(AbstractC15340mz r5, MyStatusesActivity myStatusesActivity, boolean z) {
        HashMap hashMap = myStatusesActivity.A14;
        AnonymousClass1IS r3 = r5.A0z;
        AbstractC16350or r1 = (AbstractC16350or) hashMap.remove(r3);
        if (r1 != null) {
            if (z) {
                r1.A03(true);
            } else {
                return;
            }
        }
        AnonymousClass375 r2 = new AnonymousClass375(r5, myStatusesActivity);
        hashMap.put(r3, r2);
        ((ActivityC13830kP) myStatusesActivity).A05.Aaz(r2, new Void[0]);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0s) {
            this.A0s = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0W = (AnonymousClass13H) r1.ABY.get();
            this.A0d = (C253018w) r1.AJS.get();
            this.A03 = (C239613r) r1.AI9.get();
            this.A0e = (AnonymousClass12U) r1.AJd.get();
            this.A0S = (C16120oU) r1.ANE.get();
            this.A0N = (C18470sV) r1.AK8.get();
            this.A07 = (C18850tA) r1.AKx.get();
            this.A04 = (C16170oZ) r1.AM4.get();
            this.A0Q = (C231510o) r1.AHO.get();
            this.A0b = new C88054Ec();
            this.A0Z = (AnonymousClass17R) r1.AIz.get();
            this.A08 = (C15550nR) r1.A45.get();
            this.A0P = (C22180yf) r1.A5U.get();
            this.A0K = (C22810zg) r1.AHI.get();
            this.A0A = (C15610nY) r1.AMe.get();
            this.A0F = (AnonymousClass018) r1.ANb.get();
            this.A05 = (AnonymousClass2JG) r2.A11.get();
            this.A0G = (C15650ng) r1.A4m.get();
            this.A0J = (AnonymousClass12H) r1.AC5.get();
            this.A0T = (C20710wC) r1.A8m.get();
            this.A0c = (AnonymousClass12F) r1.AJM.get();
            this.A0I = (C253218y) r1.AAF.get();
            this.A0D = (C17050qB) r1.ABL.get();
            this.A0p = (AnonymousClass19O) r1.ACO.get();
            this.A0o = new AnonymousClass4LL(AbstractC18030rn.A00(r1.AO3));
            this.A06 = (AnonymousClass12T) r1.AJZ.get();
            this.A0M = (AnonymousClass10U) r1.AJz.get();
            this.A0h = (C248116y) r1.AHR.get();
            this.A0L = (C242114q) r1.AJq.get();
            this.A0n = (C23000zz) r1.ALg.get();
            this.A09 = (C22700zV) r1.AMN.get();
            this.A0R = (AnonymousClass193) r1.A6S.get();
            this.A0E = (C15890o4) r1.AN1.get();
            this.A0V = (C22370yy) r1.AB0.get();
            this.A0i = (C248016x) r1.AK7.get();
            this.A0j = (AnonymousClass1BD) r1.AKA.get();
            this.A0O = (C22100yW) r1.A3g.get();
            this.A0C = (C255419u) r1.AJp.get();
            this.A0U = (AnonymousClass109) r1.AI8.get();
            this.A0H = (C15600nX) r1.A8x.get();
            this.A0X = (C16630pM) r1.AIc.get();
            C25611Ab r0 = (C25611Ab) r1.AJ0.get();
            if (r0 != null) {
                this.A0a = r0;
                this.A0q = C18000rk.A00(r1.A4v);
                this.A0B = (AnonymousClass1A6) r1.A4u.get();
                return;
            }
            throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
        }
    }

    public final void A2g() {
        if (RequestPermissionActivity.A0T(this, this.A0E, 33) && this.A0D.A04(this.A0y)) {
            if (((ActivityC13790kL) this).A06.A01() < ((long) ((((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1p) << 10) << 10))) {
                C33471e8.A04(this, this, this.A0S, 5);
                return;
            }
            String rawString = AnonymousClass1VX.A00.getRawString();
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.camera.CameraActivity");
            intent.putExtra("jid", rawString);
            intent.putExtra("origin", 4);
            startActivity(intent);
        }
    }

    public final void A2h() {
        long j;
        C14900mE r0 = ((ActivityC13810kN) this).A05;
        Runnable runnable = this.A12;
        r0.A0G(runnable);
        if (!this.A0l.isEmpty()) {
            C33671et r1 = this.A0l;
            if (r1.A00.isEmpty()) {
                j = 0;
            } else {
                j = ((AbstractC15340mz) r1.A00.get(0)).A0I;
            }
            ((ActivityC13810kN) this).A05.A0J(runnable, (C38121nY.A01(j) - System.currentTimeMillis()) + 1000);
        }
    }

    public void A2i(View view, AbstractC15340mz r7) {
        int i;
        Map map = this.A16;
        AnonymousClass1IS r4 = r7.A0z;
        if (map.containsKey(r4)) {
            map.remove(r4);
            i = 0;
        } else {
            map.put(r4, r7);
            i = R.color.home_row_selection;
        }
        view.setBackgroundResource(i);
        boolean isEmpty = map.isEmpty();
        AbstractC009504t r3 = this.A01;
        if (!isEmpty) {
            if (r3 == null) {
                r3 = A1W(this.A00);
                this.A01 = r3;
            }
            r3.A0B(this.A0F.A0J().format((long) map.size()));
            this.A01.A06();
        } else if (r3 != null) {
            r3.A05();
        }
        this.A17.add(r4);
        Handler handler = this.A0v;
        Runnable runnable = this.A11;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 200);
        this.A0l.notifyDataSetChanged();
    }

    public final void A2j(List list, boolean z) {
        boolean A02;
        C458223h r0;
        this.A0r = z;
        C248016x r1 = this.A0i;
        if (z) {
            A02 = r1.A02(this, null, r1.A02, list);
        } else {
            A02 = r1.A02(this, null, r1.A03, list);
        }
        if (!A02 && !this.A0i.A00.A07() && (r0 = this.A0j.A00) != null) {
            r0.A01 = 4;
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A02(this, R.color.primary);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        super.AXD(r2);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        AbstractC009504t r0;
        C458223h r02;
        super.onActivityResult(i, i2, intent);
        if (i != 2) {
            if (i != 33) {
                if (i == 35) {
                    if (i2 == -1 && (r0 = this.A01) != null) {
                        r0.A05();
                    }
                    if (this.A0r) {
                        this.A0i.A00(intent);
                    }
                } else if (i != 151) {
                } else {
                    if (i2 == -1) {
                        Map map = this.A16;
                        if (!map.isEmpty()) {
                            ArrayList arrayList = new ArrayList(map.values());
                            map.clear();
                            A2j(arrayList, this.A0r);
                        }
                    } else if (i2 == 0 && (r02 = this.A0j.A00) != null) {
                        r02.A01 = 4;
                    }
                }
            } else if (i2 == -1) {
                A2g();
            }
        } else if (i2 == -1) {
            ArrayList arrayList2 = new ArrayList();
            Map map2 = this.A16;
            if (!map2.isEmpty()) {
                arrayList2.addAll(map2.values());
            } else {
                AbstractC15340mz r03 = this.A0Y;
                if (r03 != null) {
                    arrayList2.add(r03);
                }
            }
            if (!arrayList2.isEmpty()) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                ArrayList arrayList3 = new ArrayList(arrayList2);
                Collections.sort(arrayList3, new C45041zy());
                Iterator it = arrayList3.iterator();
                while (it.hasNext()) {
                    this.A04.A06(this.A03, null, (AbstractC15340mz) it.next(), A07);
                }
                if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                    A2a(A07);
                } else {
                    ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, this.A08.A0B((AbstractC14640lm) A07.get(0))));
                }
            } else {
                Log.w("mystatuses/forward/failed");
                ((ActivityC13810kN) this).A05.A07(R.string.message_forward_failed, 0);
            }
            AbstractC009504t r04 = this.A01;
            if (r04 != null) {
                r04.A05();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Log.i("myStatusesActivity/onCreate");
        if (C28391Mz.A02()) {
            getWindow().addFlags(Integer.MIN_VALUE);
        }
        super.onCreate(bundle);
        this.A0t = ((ActivityC13810kN) this).A0C.A07(1874);
        this.A0u = ((ActivityC13810kN) this).A0C.A07(1875);
        setTitle(R.string.my_status);
        A29();
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        setContentView(R.layout.my_statuses);
        findViewById(R.id.root_view).setSystemUiVisibility(1280);
        ((ActivityC13810kN) this).A01.setFitsSystemWindows(true);
        this.A0f = new StatusExpirationLifecycleOwner(this, ((ActivityC13810kN) this).A05, this.A0M, this.A0N, ((ActivityC13830kP) this).A05);
        this.A0g = new C64153El(this, this.A0h, this.A0j, this.A0q, 1, false);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        C252718t r04 = ((ActivityC13790kL) this).A0D;
        AnonymousClass13H r05 = this.A0W;
        C253018w r06 = this.A0d;
        AbstractC15710nm r07 = ((ActivityC13810kN) this).A03;
        C15570nT r08 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r09 = ((ActivityC13830kP) this).A05;
        AnonymousClass12U r010 = this.A0e;
        C16120oU r011 = this.A0S;
        AnonymousClass19M r012 = ((ActivityC13810kN) this).A0B;
        C15450nH r013 = ((ActivityC13810kN) this).A06;
        C18850tA r014 = this.A07;
        C16170oZ r015 = this.A04;
        C231510o r016 = this.A0Q;
        C88054Ec r017 = this.A0b;
        AnonymousClass12P r018 = ((ActivityC13790kL) this).A00;
        C15550nR r019 = this.A08;
        C22180yf r020 = this.A0P;
        AnonymousClass01d r021 = ((ActivityC13810kN) this).A08;
        C15610nY r022 = this.A0A;
        AnonymousClass018 r023 = this.A0F;
        C20710wC r024 = this.A0T;
        AnonymousClass12F r025 = this.A0c;
        C253218y r026 = this.A0I;
        AnonymousClass12T r027 = this.A06;
        AnonymousClass193 r15 = this.A0R;
        C242114q r14 = this.A0L;
        C23000zz r13 = this.A0n;
        C22700zV r12 = this.A09;
        C14820m6 r11 = ((ActivityC13810kN) this).A09;
        C22370yy r10 = this.A0V;
        C22100yW r9 = this.A0O;
        this.A00 = new AnonymousClass2z6(r018, r07, r03, r08, r013, r015, this, r027, r014, r019, r12, r022, this.A0B, this.A0C, r021, r0, r11, r023, this.A0H, r026, r14, r9, r020, r012, r016, r15, r02, r011, r024, this.A0U, r10, r05, this.A0X, r017, r025, r06, r010, this, r13, r04, r09, this.A0q);
        this.A0m = new C70433bH(this);
        this.A0l = new C33671et(this);
        ListView A2e = A2e();
        A2e.setFastScrollEnabled(false);
        View inflate = getLayoutInflater().inflate(R.layout.conversations_tip_row, (ViewGroup) A2e, false);
        inflate.setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.conversations_row_height));
        A2e.addFooterView(inflate, null, false);
        ((TextView) inflate.findViewById(R.id.conversations_row_tip_tv)).setText(getString(R.string.status_expire_explanation_with_placeholder, 24));
        A2e.setAdapter((ListAdapter) this.A0l);
        A2e.setOnItemClickListener(new AnonymousClass36T(this));
        A2e.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: X.4p4
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                MyStatusesActivity myStatusesActivity = MyStatusesActivity.this;
                if (i >= myStatusesActivity.A0l.getCount()) {
                    return false;
                }
                myStatusesActivity.A2i(view, (AbstractC15340mz) myStatusesActivity.A0l.A00.get(i));
                return true;
            }
        });
        TextView textView = (TextView) findViewById(16908292);
        textView.setText(C52252aV.A00(textView.getPaint(), AnonymousClass00T.A04(this, R.drawable.ic_new_status_tip), getString(R.string.welcome_statuses_message)));
        View findViewById = findViewById(R.id.fab);
        View findViewById2 = findViewById(R.id.fab_second);
        ((ActivityC13790kL) this).A01.A08();
        findViewById.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 36));
        findViewById2.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 37));
        View findViewById3 = findViewById(R.id.fab_third);
        ((ActivityC13790kL) this).A01.A08();
        if (this.A0t) {
            findViewById3.setVisibility(0);
            findViewById3.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 38));
        } else {
            findViewById3.setVisibility(8);
            findViewById3.setOnClickListener(null);
        }
        AnonymousClass00T.A05(this, R.id.progress).setVisibility(0);
        C35541iC r1 = this.A0k;
        if (r1 != null) {
            r1.A03(true);
        }
        C35541iC r3 = new C35541iC(this);
        this.A0k = r3;
        ((ActivityC13830kP) this).A05.Aaz(r3, new Void[0]);
        this.A0J.A03(this.A0z);
        this.A0q.get();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 13) {
            Map map = this.A16;
            if (map.isEmpty()) {
                Log.e("mediagallery/dialog/delete no messages");
            } else {
                StringBuilder sb = new StringBuilder("mediagallery/dialog/delete/");
                sb.append(map.size());
                Log.i(sb.toString());
                HashSet hashSet = new HashSet(map.values());
                return C63203Ar.A00(this, ((ActivityC13810kN) this).A05, this.A04, ((ActivityC13810kN) this).A0B, new AbstractC116195Um(hashSet) { // from class: X.59L
                    public final /* synthetic */ Set A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC116195Um
                    public final void AOu() {
                        MyStatusesActivity myStatusesActivity = MyStatusesActivity.this;
                        myStatusesActivity.A16.clear();
                        AbstractC009504t r0 = myStatusesActivity.A01;
                        if (r0 != null) {
                            r0.A05();
                        }
                    }
                }, hashSet);
            }
        } else if (i == 26 || i == 27) {
            AnonymousClass009.A07("MyStatusActivity/invalid dialog invoke");
            return null;
        }
        return super.onCreateDialog(i);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        Log.i("myStatusesActivity/onDestroy");
        super.onDestroy();
        this.A0J.A04(this.A0z);
        C35541iC r0 = this.A0k;
        if (r0 != null) {
            r0.A03(true);
        }
        HashMap hashMap = this.A14;
        for (AbstractC16350or r02 : hashMap.values()) {
            r02.A03(true);
        }
        List<Uri> list = this.A15;
        for (Uri uri : list) {
            revokeUriPermission(uri, 1);
        }
        list.clear();
        hashMap.clear();
        ((ActivityC13810kN) this).A05.A0G(this.A12);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        Log.i("myStatusesActivity/onPause");
        this.A0i.A04.A04(this);
        super.onPause();
    }

    @Override // X.ActivityC13770kJ, android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle != null) {
            List<AnonymousClass1IS> A04 = C38211ni.A04(bundle);
            if (A04 != null) {
                Map map = this.A16;
                map.clear();
                for (AnonymousClass1IS r1 : A04) {
                    map.put(r1, this.A0G.A0K.A03(r1));
                }
                AbstractC009504t r3 = this.A01;
                if (r3 == null) {
                    r3 = A1W(this.A00);
                    this.A01 = r3;
                }
                r3.A0B(this.A0F.A0J().format((long) map.size()));
                this.A01.A06();
                this.A0l.notifyDataSetChanged();
            }
            AnonymousClass1IS A03 = C38211ni.A03(bundle, "");
            if (A03 != null) {
                this.A0Y = this.A0G.A0K.A03(A03);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        Log.i("myStatusesActivity/onResume");
        super.onResume();
        this.A0i.A04.A05(this, new AnonymousClass02B() { // from class: X.3Qx
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
                if (r4 != 2) goto L_0x004c;
             */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0053  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x005a  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0069  */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r6) {
                /*
                    r5 = this;
                    com.whatsapp.status.playback.MyStatusesActivity r2 = com.whatsapp.status.playback.MyStatusesActivity.this
                    X.1Pr r6 = (X.C28961Pr) r6
                    java.lang.String r0 = "myStatuessActivity/onStatusSharingInfoChanged"
                    com.whatsapp.util.Log.i(r0)
                    if (r6 == 0) goto L_0x0041
                    android.content.Intent r0 = r6.A01
                    if (r0 == 0) goto L_0x0043
                    java.util.List r4 = r2.A15
                    java.util.Iterator r3 = r4.iterator()
                L_0x0015:
                    boolean r0 = r3.hasNext()
                    if (r0 == 0) goto L_0x0026
                    java.lang.Object r1 = r3.next()
                    android.net.Uri r1 = (android.net.Uri) r1
                    r0 = 1
                    r2.revokeUriPermission(r1, r0)
                    goto L_0x0015
                L_0x0026:
                    r4.clear()
                    java.util.List r0 = r6.A02
                    r4.addAll(r0)
                    boolean r0 = r2.A0r
                    int r1 = X.C12990iw.A04(r0)
                    X.3El r0 = r2.A0g
                    r0.A01(r1)
                    android.content.Intent r1 = r6.A01
                    r0 = 35
                    r2.startActivityForResult(r1, r0)
                    return
                L_0x0041:
                    r4 = 0
                    goto L_0x004c
                L_0x0043:
                    int r4 = r6.A00
                    r0 = 1
                    if (r4 == r0) goto L_0x0067
                    r0 = 2
                    r1 = 2
                    if (r4 == r0) goto L_0x004d
                L_0x004c:
                    r1 = 5
                L_0x004d:
                    X.1BD r0 = r2.A0j
                    X.23h r0 = r0.A00
                    if (r0 == 0) goto L_0x0055
                    r0.A01 = r1
                L_0x0055:
                    r3 = 1
                    X.0mE r2 = r2.A05
                    if (r4 == r3) goto L_0x0069
                    r1 = 2
                    r0 = 2131891845(0x7f121685, float:1.9418422E38)
                    if (r4 == r1) goto L_0x006c
                    r0 = 2131891843(0x7f121683, float:1.9418417E38)
                    r2.A07(r0, r3)
                    return
                L_0x0067:
                    r1 = 3
                    goto L_0x004d
                L_0x0069:
                    r0 = 2131891844(0x7f121684, float:1.941842E38)
                L_0x006c:
                    r2.A05(r0, r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C67283Qx.ANq(java.lang.Object):void");
            }
        });
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Map map = this.A16;
        if (!map.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AbstractC15340mz r0 : map.values()) {
                arrayList.add(r0.A0z);
            }
            C38211ni.A09(bundle, arrayList);
        }
        AbstractC15340mz r02 = this.A0Y;
        if (r02 != null) {
            C38211ni.A08(bundle, r02.A0z, "");
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        Log.i("myStatusesActivity/onStart");
        super.onStart();
        A2h();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        Log.i("myStatusesActivity/onStop");
        super.onStop();
    }
}
