package com.whatsapp.status.playback.fragment;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10U;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass18U;
import X.AnonymousClass19Z;
import X.AnonymousClass1BD;
import X.AnonymousClass1CJ;
import X.AnonymousClass1E9;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15860o1;
import X.C16170oZ;
import X.C16590pI;
import X.C18000rk;
import X.C18470sV;
import X.C20020v5;
import X.C21270x9;
import X.C22330yu;
import X.C239613r;
import X.C242714w;
import X.C244215l;
import X.C26101Ca;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.Mp4Ops;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_StatusPlaybackFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A01() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A01();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A01()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.fragment.Hilt_StatusPlaybackFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A01();
        A18();
    }

    public void A18() {
        if (this instanceof Hilt_StatusPlaybackBaseFragment) {
            Hilt_StatusPlaybackBaseFragment hilt_StatusPlaybackBaseFragment = (Hilt_StatusPlaybackBaseFragment) this;
            if (hilt_StatusPlaybackBaseFragment instanceof Hilt_StatusPlaybackContactFragment) {
                Hilt_StatusPlaybackContactFragment hilt_StatusPlaybackContactFragment = (Hilt_StatusPlaybackContactFragment) hilt_StatusPlaybackBaseFragment;
                if (!hilt_StatusPlaybackContactFragment.A02) {
                    hilt_StatusPlaybackContactFragment.A02 = true;
                    StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) hilt_StatusPlaybackContactFragment;
                    AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_StatusPlaybackContactFragment.generatedComponent())).A0Y;
                    ((WaFragment) statusPlaybackContactFragment).A00 = (AnonymousClass182) r2.A94.get();
                    ((WaFragment) statusPlaybackContactFragment).A01 = (AnonymousClass180) r2.ALt.get();
                    ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A00 = (C14900mE) r2.A8X.get();
                    ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A01 = (AnonymousClass01d) r2.ALI.get();
                    ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A02 = (AnonymousClass018) r2.ANb.get();
                    ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A05 = (AnonymousClass1CJ) r2.AK3.get();
                    statusPlaybackContactFragment.A0G = (C14830m7) r2.ALb.get();
                    statusPlaybackContactFragment.A05 = (Mp4Ops) r2.ACh.get();
                    statusPlaybackContactFragment.A0N = (C14850m9) r2.A04.get();
                    statusPlaybackContactFragment.A04 = (C15570nT) r2.AAr.get();
                    statusPlaybackContactFragment.A06 = (C239613r) r2.AI9.get();
                    statusPlaybackContactFragment.A0X = (AbstractC14440lR) r2.ANe.get();
                    statusPlaybackContactFragment.A0H = (C16590pI) r2.AMg.get();
                    statusPlaybackContactFragment.A02 = (AbstractC15710nm) r2.A4o.get();
                    statusPlaybackContactFragment.A07 = (C15450nH) r2.AII.get();
                    statusPlaybackContactFragment.A0M = (C18470sV) r2.AK8.get();
                    statusPlaybackContactFragment.A08 = (C16170oZ) r2.AM4.get();
                    statusPlaybackContactFragment.A0Y = (AnonymousClass19Z) r2.A2o.get();
                    statusPlaybackContactFragment.A03 = (AnonymousClass18U) r2.AAU.get();
                    statusPlaybackContactFragment.A0E = (C21270x9) r2.A4A.get();
                    statusPlaybackContactFragment.A0T = (AnonymousClass1E9) r2.AJy.get();
                    statusPlaybackContactFragment.A0A = (C15550nR) r2.A45.get();
                    statusPlaybackContactFragment.A0C = (C15610nY) r2.AMe.get();
                    statusPlaybackContactFragment.A0B = (AnonymousClass10S) r2.A46.get();
                    statusPlaybackContactFragment.A0I = (C15650ng) r2.A4m.get();
                    statusPlaybackContactFragment.A0J = (AnonymousClass12H) r2.AC5.get();
                    statusPlaybackContactFragment.A0S = (AnonymousClass12F) r2.AJM.get();
                    statusPlaybackContactFragment.A0R = (C15860o1) r2.A3H.get();
                    statusPlaybackContactFragment.A0F = (C20020v5) r2.A3B.get();
                    statusPlaybackContactFragment.A09 = (C22330yu) r2.A3I.get();
                    statusPlaybackContactFragment.A0K = (AnonymousClass10U) r2.AJz.get();
                    statusPlaybackContactFragment.A0U = (AnonymousClass1BD) r2.AKA.get();
                    statusPlaybackContactFragment.A0L = (C242714w) r2.AK6.get();
                    statusPlaybackContactFragment.A0W = (C26101Ca) r2.AK5.get();
                    statusPlaybackContactFragment.A0O = (C244215l) r2.A8y.get();
                    statusPlaybackContactFragment.A0Z = C18000rk.A00(r2.A4v);
                }
            } else if (!hilt_StatusPlaybackBaseFragment.A02) {
                hilt_StatusPlaybackBaseFragment.A02 = true;
                StatusPlaybackBaseFragment statusPlaybackBaseFragment = (StatusPlaybackBaseFragment) hilt_StatusPlaybackBaseFragment;
                AnonymousClass01J r22 = ((C51112Sw) ((AbstractC51092Su) hilt_StatusPlaybackBaseFragment.generatedComponent())).A0Y;
                ((WaFragment) statusPlaybackBaseFragment).A00 = (AnonymousClass182) r22.A94.get();
                ((WaFragment) statusPlaybackBaseFragment).A01 = (AnonymousClass180) r22.ALt.get();
                statusPlaybackBaseFragment.A00 = (C14900mE) r22.A8X.get();
                statusPlaybackBaseFragment.A01 = (AnonymousClass01d) r22.ALI.get();
                statusPlaybackBaseFragment.A02 = (AnonymousClass018) r22.ANb.get();
                statusPlaybackBaseFragment.A05 = (AnonymousClass1CJ) r22.AK3.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            ((WaFragment) this).A00 = (AnonymousClass182) r1.A94.get();
            ((WaFragment) this).A01 = (AnonymousClass180) r1.ALt.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
