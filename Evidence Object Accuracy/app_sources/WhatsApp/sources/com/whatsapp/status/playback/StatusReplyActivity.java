package com.whatsapp.status.playback;

import X.AbstractC14440lR;
import X.AbstractC15280mr;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.AbstractC253919f;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00E;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass116;
import X.AnonymousClass11P;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass146;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass19N;
import X.AnonymousClass19O;
import X.AnonymousClass19P;
import X.AnonymousClass19X;
import X.AnonymousClass1A2;
import X.AnonymousClass1A4;
import X.AnonymousClass1AA;
import X.AnonymousClass1AB;
import X.AnonymousClass1AD;
import X.AnonymousClass1BD;
import X.AnonymousClass1EB;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103564qx;
import X.C14330lG;
import X.C14410lO;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15260mp;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C17020q8;
import X.C17050qB;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21200x2;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22190yg;
import X.C22210yi;
import X.C22670zS;
import X.C231510o;
import X.C235512c;
import X.C238013b;
import X.C239613r;
import X.C244415n;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253619c;
import X.C253719d;
import X.C254719n;
import X.C255519v;
import X.C37311lz;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101654ns;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;

/* loaded from: classes2.dex */
public class StatusReplyActivity extends MessageReplyActivity {
    public AnonymousClass1EB A00;
    public AnonymousClass12H A01;
    public AnonymousClass1BD A02;
    public boolean A03;
    public final Rect A04;
    public final ViewTreeObserver.OnGlobalLayoutListener A05;
    public final AbstractC18860tB A06;
    public final Runnable A07;

    public StatusReplyActivity() {
        this(0);
        this.A04 = new Rect();
        this.A07 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 9);
        this.A06 = new C37311lz(this);
        this.A05 = new ViewTreeObserver$OnGlobalLayoutListenerC101654ns(this);
    }

    public StatusReplyActivity(int i) {
        this.A03 = false;
        A0R(new C103564qx(this));
    }

    @Override // X.AbstractActivityC33661er, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0d = (C253719d) r1.A8V.get();
            this.A0S = (AnonymousClass19P) r1.ACQ.get();
            ((MessageReplyActivity) this).A0C = (C239613r) r1.AI9.get();
            this.A0Z = (C16120oU) r1.ANE.get();
            ((MessageReplyActivity) this).A0H = (AnonymousClass19X) r1.AIe.get();
            ((MessageReplyActivity) this).A0D = (C16170oZ) r1.AM4.get();
            ((MessageReplyActivity) this).A0G = (AnonymousClass19N) r1.A32.get();
            this.A0h = (C14410lO) r1.AB3.get();
            this.A0X = (C231510o) r1.AHO.get();
            this.A0P = (C21270x9) r1.A4A.get();
            this.A0g = (C244415n) r1.AAg.get();
            ((MessageReplyActivity) this).A0M = (C15550nR) r1.A45.get();
            this.A0p = (C22210yi) r1.AGm.get();
            this.A0y = (C22190yg) r1.AB6.get();
            ((MessageReplyActivity) this).A0J = (C253619c) r1.AId.get();
            ((MessageReplyActivity) this).A0N = (C15610nY) r1.AMe.get();
            ((MessageReplyActivity) this).A0K = (C238013b) r1.A1Z.get();
            this.A0W = (C15650ng) r1.A4m.get();
            this.A0r = (AnonymousClass146) r1.AKM.get();
            this.A0l = (C21200x2) r1.A2q.get();
            this.A0e = (AbstractC253919f) r1.AGb.get();
            this.A0z = (AnonymousClass19O) r1.ACO.get();
            this.A0T = (C17050qB) r1.ABL.get();
            this.A0s = (C235512c) r1.AKS.get();
            ((MessageReplyActivity) this).A0L = (AnonymousClass116) r1.A3z.get();
            this.A0Y = (AnonymousClass193) r1.A6S.get();
            this.A0U = (C15890o4) r1.AN1.get();
            this.A11 = (C17020q8) r1.A6G.get();
            ((MessageReplyActivity) this).A0E = (C254719n) r1.A2U.get();
            ((MessageReplyActivity) this).A0F = (C14650lo) r1.A2V.get();
            this.A0q = (AnonymousClass1AB) r1.AKI.get();
            this.A0n = (C16630pM) r1.AIc.get();
            this.A0R = (AnonymousClass11P) r1.ABp.get();
            this.A13 = r2.A0N();
            this.A0a = r2.A07();
            this.A0k = (AnonymousClass1A2) r1.AET.get();
            this.A0w = (AnonymousClass1A4) r1.AKX.get();
            this.A0t = (C255519v) r1.AKF.get();
            ((MessageReplyActivity) this).A0I = (AnonymousClass1AA) r1.ADr.get();
            this.A0V = (AnonymousClass1AD) r1.A54.get();
            this.A01 = (AnonymousClass12H) r1.AC5.get();
            this.A02 = (AnonymousClass1BD) r1.AKA.get();
            this.A00 = (AnonymousClass1EB) r1.ACN.get();
        }
    }

    public final void A2n() {
        int i;
        int identifier;
        C15260mp r0;
        View view = ((MessageReplyActivity) this).A04;
        Rect rect = this.A04;
        view.getWindowVisibleDisplayFrame(rect);
        int[] iArr = new int[2];
        ((MessageReplyActivity) this).A04.getLocationOnScreen(iArr);
        int measuredHeight = rect.bottom - ((MessageReplyActivity) this).A03.getMeasuredHeight();
        if (C252718t.A00(((MessageReplyActivity) this).A04) || (r0 = this.A0b) == null || !r0.isShowing()) {
            i = 0;
        } else {
            i = ((AbstractC15280mr) this.A0b).A01;
        }
        int i2 = (measuredHeight - i) - iArr[1];
        if (Build.MANUFACTURER.equalsIgnoreCase("Essential Products") && Build.MODEL.equalsIgnoreCase("PH-1") && (identifier = getResources().getIdentifier("status_bar_height", "dimen", "android")) > 0) {
            i2 -= getResources().getDimensionPixelSize(identifier);
        }
        View view2 = ((MessageReplyActivity) this).A03;
        AnonymousClass028.A0Y(view2, i2 - view2.getTop());
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // com.whatsapp.status.playback.MessageReplyActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        getWindow().setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(134217728, 134217728);
            getWindow().setFlags(67108864, 67108864);
        }
        super.onCreate(bundle);
        View view = ((MessageReplyActivity) this).A04;
        if (view != null) {
            view.setSystemUiVisibility(4);
            ((MessageReplyActivity) this).A04.getViewTreeObserver().addOnGlobalLayoutListener(this.A05);
            if (((ActivityC13810kN) this).A0C.A07(1022)) {
                this.A01.A03(this.A06);
            }
        }
    }

    @Override // com.whatsapp.status.playback.MessageReplyActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A01.A04(this.A06);
        View view = ((MessageReplyActivity) this).A04;
        if (view != null) {
            view.getViewTreeObserver().removeGlobalOnLayoutListener(this.A05);
            ((MessageReplyActivity) this).A04.removeCallbacks(this.A07);
        }
    }
}
