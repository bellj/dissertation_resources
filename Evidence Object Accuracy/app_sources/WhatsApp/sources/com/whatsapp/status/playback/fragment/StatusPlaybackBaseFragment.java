package com.whatsapp.status.playback.fragment;

import X.AbstractC116205Un;
import X.AbstractC33631eh;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass1CJ;
import X.AnonymousClass2GF;
import X.AnonymousClass4LD;
import X.AnonymousClass59N;
import X.AnonymousClass5WI;
import X.C14900mE;
import X.C33651en;
import X.View$OnClickListenerC55932ju;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class StatusPlaybackBaseFragment extends Hilt_StatusPlaybackBaseFragment {
    public C14900mE A00;
    public AnonymousClass01d A01;
    public AnonymousClass018 A02;
    public C33651en A03;
    public AnonymousClass5WI A04 = new AnonymousClass59N(this);
    public AnonymousClass1CJ A05;
    public Runnable A06 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 10);
    public boolean A07;

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        StatusPlaybackFragment A2f;
        super.A0m(bundle);
        A1G(((StatusPlaybackFragment) this).A01);
        AbstractC116205Un r3 = (AbstractC116205Un) A0B();
        if (r3 != null) {
            UserJid userJid = ((StatusPlaybackContactFragment) this).A0P;
            AnonymousClass009.A05(userJid);
            String rawString = userJid.getRawString();
            StatusPlaybackActivity statusPlaybackActivity = (StatusPlaybackActivity) r3;
            AnonymousClass4LD r1 = (AnonymousClass4LD) statusPlaybackActivity.A0D.A00.get(statusPlaybackActivity.A07.getCurrentItem());
            if (r1.A00.A0A.getRawString().equals(rawString) && (A2f = statusPlaybackActivity.A2f(r1)) != null) {
                A2f.A1A();
                A2f.A1C(1);
            }
        }
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        AnonymousClass1CJ r0 = this.A05;
        AnonymousClass5WI r1 = this.A04;
        List list = r0.A04;
        if (list != null) {
            list.remove(r1);
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.status_playback_fragment, viewGroup, false);
        this.A03 = new C33651en(inflate);
        return inflate;
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
        AnonymousClass1CJ r2 = this.A05;
        AnonymousClass5WI r1 = this.A04;
        List list = r2.A04;
        if (list == null) {
            list = new ArrayList();
            r2.A04 = list;
        }
        list.add(r1);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        ActivityC000900k A0C = A0C();
        C33651en r5 = this.A03;
        AnonymousClass009.A06(r5, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        ViewOnClickCListenerShape15S0100000_I0_2 viewOnClickCListenerShape15S0100000_I0_2 = new ViewOnClickCListenerShape15S0100000_I0_2(this, 40);
        ImageView imageView = r5.A0A;
        imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(A0C, R.drawable.ic_cam_back), this.A02));
        imageView.setOnClickListener(viewOnClickCListenerShape15S0100000_I0_2);
        View view2 = r5.A03;
        view2.setOnClickListener(new View$OnClickListenerC55932ju(A0C, view2, this.A02, this));
    }

    @Override // com.whatsapp.status.playback.fragment.StatusPlaybackFragment
    public void A1D(Rect rect) {
        super.A1D(rect);
        A1G(rect);
        Rect rect2 = ((StatusPlaybackFragment) this).A01;
        for (AbstractC33631eh r0 : ((StatusPlaybackContactFragment) this).A0f.A05().values()) {
            r0.A08(rect2);
        }
    }

    public final C33651en A1F() {
        C33651en r1 = this.A03;
        AnonymousClass009.A06(r1, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0176, code lost:
        if (r0 != false) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1G(android.graphics.Rect r8) {
        /*
        // Method dump skipped, instructions count: 379
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment.A1G(android.graphics.Rect):void");
    }

    public void A1H(boolean z) {
        StringBuilder sb = new StringBuilder("playbackFragment/onDragChanged dragging=");
        sb.append(z);
        sb.append("; ");
        sb.append(this);
        Log.i(sb.toString());
    }
}
