package com.whatsapp.status.playback.widget;

import X.C105764ua;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.whatsapp.collections.MarginCorrectedViewPager;

/* loaded from: classes2.dex */
public class StatusPlaybackPager extends MarginCorrectedViewPager {
    public boolean A00;
    public boolean A01;

    public StatusPlaybackPager(Context context) {
        super(context, null);
        A0O();
        this.A01 = true;
        A0H(new C105764ua(), true);
    }

    public StatusPlaybackPager(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A01 = true;
        A0H(new C105764ua(), true);
    }

    public StatusPlaybackPager(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A0O();
    }

    @Override // X.AbstractC75813kV
    public void A0O() {
        if (!this.A00) {
            this.A00 = true;
            generatedComponent();
        }
    }

    @Override // com.whatsapp.collections.MarginCorrectedViewPager, androidx.viewpager.widget.ViewPager, android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.A01 || (motionEvent.getPointerCount() > 1 && motionEvent.getActionMasked() == 2)) {
            return false;
        }
        try {
            return super.onInterceptTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    @Override // com.whatsapp.collections.MarginCorrectedViewPager, androidx.viewpager.widget.ViewPager, android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (!(i == i3 && i2 == i4)) {
            if (!(i3 == 0 && i4 == 0)) {
                int childCount = getChildCount();
                for (int i5 = 0; i5 < childCount; i5++) {
                    getChildAt(i5).setRotation(0.0f);
                }
            }
        }
    }

    @Override // com.whatsapp.collections.MarginCorrectedViewPager, androidx.viewpager.widget.ViewPager, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (!this.A01) {
            return false;
        }
        try {
            z = super.onTouchEvent(motionEvent);
            return z;
        } catch (IllegalArgumentException unused) {
            return z;
        }
    }

    @Override // com.whatsapp.collections.MarginCorrectedViewPager
    public void setScrollEnabled(boolean z) {
        this.A01 = z;
    }
}
