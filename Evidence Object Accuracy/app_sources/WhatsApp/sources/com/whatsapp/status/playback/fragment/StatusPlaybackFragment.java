package com.whatsapp.status.playback.fragment;

import X.AbstractC33621eg;
import X.AbstractC33631eh;
import android.content.res.Configuration;
import android.graphics.Rect;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public abstract class StatusPlaybackFragment extends Hilt_StatusPlaybackFragment {
    public boolean A00;
    public final Rect A01 = new Rect();

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        StringBuilder sb = new StringBuilder("playbackFragment/onPause ");
        sb.append(this);
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        StringBuilder sb = new StringBuilder("playbackFragment/onDestroy ");
        sb.append(this);
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        StringBuilder sb = new StringBuilder("playbackFragment/onResume ");
        sb.append(this);
        Log.i(sb.toString());
    }

    public void A19() {
        StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) this;
        for (AbstractC33631eh r1 : statusPlaybackContactFragment.A0f.A05().values()) {
            r1.A02 = statusPlaybackContactFragment.A1E();
            AbstractC33621eg r12 = (AbstractC33621eg) r1;
            if (((AbstractC33631eh) r12).A02) {
                r12.A0D();
            } else {
                r12.A0E();
            }
        }
    }

    public void A1A() {
        this.A00 = true;
        StringBuilder sb = new StringBuilder("playbackFragment/onViewActive ");
        sb.append(this);
        Log.i(sb.toString());
    }

    public void A1B() {
        this.A00 = false;
        StringBuilder sb = new StringBuilder("playbackFragment/onViewInactive ");
        sb.append(this);
        Log.i(sb.toString());
    }

    public void A1C(int i) {
        StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) this;
        if (statusPlaybackContactFragment.A0b == null) {
            statusPlaybackContactFragment.A01 = i;
            return;
        }
        AbstractC33631eh A1I = statusPlaybackContactFragment.A1I();
        if (A1I != null && !A1I.A05) {
            A1I.A07(i);
        }
    }

    public void A1D(Rect rect) {
        this.A01.set(rect);
    }

    public boolean A1E() {
        if (!(this instanceof StatusPlaybackBaseFragment)) {
            return false;
        }
        StatusPlaybackBaseFragment statusPlaybackBaseFragment = (StatusPlaybackBaseFragment) this;
        if (!(statusPlaybackBaseFragment instanceof StatusPlaybackContactFragment)) {
            return statusPlaybackBaseFragment.A07;
        }
        StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) statusPlaybackBaseFragment;
        return ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A07 || statusPlaybackContactFragment.A0d;
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        StringBuilder sb = new StringBuilder("playbackFragment/onConfigurationChanged ");
        sb.append(this);
        Log.i(sb.toString());
    }
}
