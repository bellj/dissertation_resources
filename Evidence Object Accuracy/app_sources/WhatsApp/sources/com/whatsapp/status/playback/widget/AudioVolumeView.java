package com.whatsapp.status.playback.widget;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class AudioVolumeView extends View implements AnonymousClass004 {
    public float A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final Paint A03;
    public final Path A04;
    public final RectF A05;

    public AudioVolumeView(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A03 = new Paint(1);
        this.A05 = new RectF();
        this.A04 = new Path();
        A00(context);
    }

    public AudioVolumeView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A03 = new Paint(1);
        this.A05 = new RectF();
        this.A04 = new Path();
        A00(context);
    }

    public AudioVolumeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A03 = new Paint(1);
        this.A05 = new RectF();
        this.A04 = new Path();
        A00(context);
    }

    public AudioVolumeView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A03 = new Paint(1);
        this.A05 = new RectF();
        this.A04 = new Path();
        A00(context);
    }

    public AudioVolumeView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        Paint paint = this.A03;
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-1);
        paint.setStrokeWidth(context.getResources().getDimension(R.dimen.audio_volume_segment));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float height = (float) ((getHeight() - getPaddingTop()) - getPaddingBottom());
        float f = 1.5f * height;
        RectF rectF = this.A05;
        rectF.set(0.0f, (height - f) / 2.0f, f, (height + f) / 2.0f);
        canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
        Paint paint = this.A03;
        float strokeWidth = paint.getStrokeWidth();
        Path path = this.A04;
        path.reset();
        float f2 = height / 3.0f;
        path.moveTo(strokeWidth, f2);
        path.lineTo(f2, f2);
        float f3 = (height * 2.0f) / 3.0f;
        path.lineTo(f3, strokeWidth + 0.0f);
        path.lineTo(f3, height - strokeWidth);
        path.lineTo(f2, f3);
        path.lineTo(strokeWidth, f3);
        path.lineTo(strokeWidth, f2);
        paint.setColor(-1);
        canvas.drawPath(path, paint);
        canvas.translate(((-height) / 2.0f) + strokeWidth, 0.0f);
        int i = 0;
        do {
            float f4 = this.A00;
            float f5 = (((float) i) * 1.0f) / 8.0f;
            int i2 = 51;
            if (f4 >= f5) {
                i2 = f4 > (((float) (i + 1)) * 1.0f) / 8.0f ? 255 : 51 + ((int) ((f4 - f5) * 8.0f * 204.0f));
            }
            paint.setColor((i2 << 24) | 16777215);
            canvas.drawArc(rectF, -33.0f, 66.0f, false, paint);
            canvas.translate(paint.getStrokeWidth() * 3.0f, 0.0f);
            i++;
        } while (i < 8);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(((int) (((float) ((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom())) + (this.A03.getStrokeWidth() * 3.0f * 8.0f) + ((float) getPaddingLeft()) + ((float) getPaddingRight()))) + 1, getMeasuredHeight());
    }

    public void setVolume(float f) {
        this.A00 = f;
        invalidate();
    }
}
