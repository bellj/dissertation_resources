package com.whatsapp.status.playback;

import X.AbstractActivityC58512pb;
import X.AbstractC116205Un;
import X.AbstractC33621eg;
import X.AbstractC33631eh;
import X.AbstractC33641ei;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass01A;
import X.AnonymousClass01E;
import X.AnonymousClass01V;
import X.AnonymousClass1BD;
import X.AnonymousClass1CI;
import X.AnonymousClass1CJ;
import X.AnonymousClass1IS;
import X.AnonymousClass21S;
import X.AnonymousClass314;
import X.AnonymousClass35V;
import X.AnonymousClass3EE;
import X.AnonymousClass4LD;
import X.AnonymousClass4ZH;
import X.AnonymousClass5WI;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C18470sV;
import X.C35571iH;
import X.C458223h;
import X.C458423j;
import X.C63413Bm;
import X.C74173hV;
import android.content.Intent;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class StatusPlaybackActivity extends AbstractActivityC58512pb implements AbstractC116205Un {
    public static final Interpolator A0O = new Interpolator() { // from class: X.4oi
        @Override // android.animation.TimeInterpolator
        public final float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };
    public float A00 = 3.5f;
    public int A01;
    public int A02 = 0;
    public int A03 = 0;
    public int A04;
    public int A05 = -1;
    public long A06;
    public ViewPager A07;
    public C15890o4 A08;
    public C18470sV A09;
    public AnonymousClass1IS A0A;
    public C15860o1 A0B;
    public AnonymousClass1BD A0C;
    public AnonymousClass3EE A0D;
    public C74173hV A0E;
    public AnonymousClass1CJ A0F;
    public AnonymousClass1CI A0G;
    public Runnable A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K = false;
    public boolean A0L;
    public boolean A0M;
    public final Rect A0N = C12980iv.A0J();

    public StatusPlaybackFragment A2e() {
        int currentItem = this.A07.getCurrentItem();
        AnonymousClass3EE r0 = this.A0D;
        if (r0 == null || currentItem < 0 || currentItem >= r0.A00.size()) {
            return null;
        }
        return A2f((AnonymousClass4LD) this.A0D.A00.get(currentItem));
    }

    public final StatusPlaybackFragment A2f(AnonymousClass4LD r6) {
        String rawString;
        if (r6 == null || (rawString = r6.A00.A0A.getRawString()) == null) {
            return null;
        }
        Iterator A0o = ActivityC13810kN.A0o(this);
        while (A0o.hasNext()) {
            AnonymousClass01E r1 = (AnonymousClass01E) A0o.next();
            if (r1 instanceof StatusPlaybackFragment) {
                StatusPlaybackFragment statusPlaybackFragment = (StatusPlaybackFragment) r1;
                UserJid userJid = ((StatusPlaybackContactFragment) statusPlaybackFragment).A0P;
                AnonymousClass009.A05(userJid);
                if (rawString.equals(userJid.getRawString())) {
                    return statusPlaybackFragment;
                }
            }
        }
        return null;
    }

    public final void A2g(int i, String str, int i2) {
        int A00 = this.A0D.A00(str);
        if (A00 >= 0 && A00 < this.A0D.A00.size()) {
            if (A00 != this.A07.getCurrentItem()) {
                this.A0D.A00.remove(A00);
                int i3 = this.A01;
                if (A00 <= i3) {
                    this.A01 = i3 - 1;
                }
                int i4 = this.A05;
                if (A00 <= i4) {
                    this.A05 = i4 - 1;
                }
                this.A07.A0V.A06();
            } else if (this.A0I || A00 == C12990iw.A09(this.A0D.A00, 1)) {
                finish();
            } else {
                this.A0H = new Runnable(str, i, i2) { // from class: X.3lF
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ int A01;
                    public final /* synthetic */ String A03;

                    {
                        this.A03 = r2;
                        this.A00 = r3;
                        this.A01 = r4;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        StatusPlaybackActivity.this.A2g(this.A00, this.A03, this.A01);
                    }
                };
                ARa(str, i, i2, true);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A01;
    }

    @Override // X.AbstractC116205Un
    public boolean ARa(String str, int i, int i2, boolean z) {
        ViewPager viewPager;
        int i3;
        int A00 = this.A0D.A00(str);
        if (z) {
            this.A03 = i;
            this.A02 = i2;
            if (A00 >= C12990iw.A09(this.A0D.A00, 1) || this.A0I) {
                finish();
                return true;
            }
            this.A0E.A00 = this.A00;
            this.A00 = 3.5f;
            viewPager = this.A07;
            i3 = A00 + 1;
        } else if (A00 <= 0 || this.A0I) {
            return false;
        } else {
            this.A0E.A00 = this.A00;
            this.A00 = 3.5f;
            this.A03 = i;
            this.A02 = i2;
            viewPager = this.A07;
            i3 = A00 - 1;
        }
        viewPager.A0F(i3, true);
        this.A0E.A00 = 0.0f;
        return true;
    }

    @Override // X.ActivityC000800j, X.AbstractActivityC001100m, android.app.Activity, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int i;
        int i2;
        int keyCode = keyEvent.getKeyCode();
        if (!(keyEvent.getAction() == 0 && (keyCode == 24 || keyCode == 25))) {
            return super.dispatchKeyEvent(keyEvent);
        }
        AnonymousClass1CJ r8 = this.A0F;
        boolean A1V = C12960it.A1V(keyCode, 24);
        AudioManager A0G = r8.A06.A0G();
        if (A0G != null) {
            int streamVolume = A0G.getStreamVolume(3);
            int streamMaxVolume = A0G.getStreamMaxVolume(3);
            if (A1V) {
                if (streamVolume < streamMaxVolume) {
                    i = streamVolume + 1;
                    i2 = 1;
                    A0G.adjustSuggestedStreamVolume(i2, 3, 16);
                }
                i = streamVolume;
            } else {
                if (streamVolume > 0) {
                    i = streamVolume - 1;
                    i2 = -1;
                    A0G.adjustSuggestedStreamVolume(i2, 3, 16);
                }
                i = streamVolume;
            }
            List<AnonymousClass5WI> list = r8.A04;
            if (list != null) {
                for (AnonymousClass5WI r0 : list) {
                    r0.AMa(streamVolume, i, streamMaxVolume);
                }
            }
        }
        AnonymousClass1CJ r1 = this.A0F;
        if (r1.A05) {
            r1.A05 = false;
            List<AnonymousClass5WI> list2 = r1.A04;
            if (list2 != null) {
                for (AnonymousClass5WI r02 : list2) {
                    r02.AMW(false);
                }
            }
        }
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        float f;
        if (!this.A0E.isFinished() && this.A0E.timePassed() < (this.A0E.getDuration() >> 1)) {
            return false;
        }
        if (motionEvent.getActionMasked() == 0) {
            long eventTime = motionEvent.getEventTime() - this.A06;
            if (eventTime == 0 || eventTime > 1000) {
                f = 3.5f;
            } else {
                f = ((((float) eventTime) * 2.5f) / 1000.0f) + 1.0f;
            }
            this.A00 = f;
            this.A06 = motionEvent.getEventTime();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        boolean z = true;
        if (i == 10) {
            if (i2 != -1) {
                z = false;
            }
            this.A0L = z;
        } else if (i != 151) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            this.A0J = true;
            AnonymousClass01A r0 = this.A07.A0V;
            AnonymousClass009.A05(r0);
            r0.A06();
            this.A07.setCurrentItem(this.A04);
        } else {
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AbstractC33631eh A1I;
        View view;
        StatusPlaybackFragment A2e = A2e();
        if (!(A2e == null || !(A2e instanceof StatusPlaybackContactFragment) || (A1I = ((StatusPlaybackContactFragment) A2e).A1I()) == null)) {
            AbstractC33621eg r3 = (AbstractC33621eg) A1I;
            BottomSheetBehavior bottomSheetBehavior = r3.A00;
            if (bottomSheetBehavior.A0B == 3) {
                bottomSheetBehavior.A0M(4);
                return;
            }
            C35571iH A0B = r3.A0B();
            if (A0B.A0F.A0I()) {
                A0B.A0F.setExpanded(false);
                A0B.A02.setVisibility(A0B.A0F.getVisibility());
                r3.A0E();
                return;
            }
            AbstractC33641ei A0A = r3.A0A();
            if (A0A instanceof AnonymousClass35V) {
                AnonymousClass35V r1 = (AnonymousClass35V) A0A;
                if (!r1.A0G && (view = r1.A00) != null && view.getVisibility() == 0) {
                    r1.A00.performClick();
                    return;
                }
            }
        }
        this.A03 = 3;
        super.onBackPressed();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0234, code lost:
        if (r2 == 2) goto L_0x0236;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01c7  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 575
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.status.playback.StatusPlaybackActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1CJ r2 = this.A0F;
        Handler handler = r2.A02;
        if (handler != null) {
            handler.removeCallbacks(r2.A07);
        }
        r2.A01();
        if (r2.A04 != null) {
            r2.A04 = null;
        }
        AnonymousClass1BD r5 = this.A0C;
        C458223h r0 = r5.A00;
        C458423j r4 = r5.A01;
        if (!(r0 == null || r4 == null)) {
            ArrayList A0l = C12960it.A0l();
            Iterator A0o = C12960it.A0o(r4.A0F);
            while (A0o.hasNext()) {
                C63413Bm r7 = (C63413Bm) A0o.next();
                AnonymousClass314 r6 = new AnonymousClass314();
                r6.A05 = Long.valueOf(r7.A05);
                r6.A06 = Long.valueOf(r7.A06);
                r6.A01 = Integer.valueOf(r7.A02);
                r6.A02 = C12980iv.A0l(r7.A01);
                r6.A00 = Integer.valueOf(r7.A00);
                r6.A04 = C12980iv.A0l(r7.A04);
                r6.A03 = C12980iv.A0l(r7.A03);
                String str = r7.A07;
                r6.A07 = str;
                boolean isEmpty = TextUtils.isEmpty(str);
                C16120oU r22 = r5.A0A;
                if (isEmpty) {
                    r22.A06(r6);
                } else {
                    r22.A0B(r6, AnonymousClass4ZH.A00, true);
                }
                A0l.addAll(r7.A08.values());
            }
            r5.A0D.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r5, r4, A0l, 45));
            r5.A01 = null;
        }
        AnonymousClass1CI r1 = this.A0G;
        AnonymousClass21S r02 = r1.A00;
        if (r02 != null) {
            r02.A08();
            r1.A00 = null;
        }
    }
}
