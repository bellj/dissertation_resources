package com.whatsapp.status.playback.content;

import X.AbstractC14440lR;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C16590pI;
import X.C28521Nt;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class BlurFrameLayout extends FrameLayout implements AnonymousClass004 {
    public Bitmap A00;
    public Bitmap A01;
    public ViewPropertyAnimator A02;
    public C16590pI A03;
    public C28521Nt A04;
    public AbstractC14440lR A05;
    public AnonymousClass2P7 A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;

    public BlurFrameLayout(Context context) {
        super(context);
        A00();
        this.A00 = null;
        this.A09 = true;
        this.A07 = true;
        this.A01 = null;
        this.A04 = null;
        this.A02 = null;
    }

    public BlurFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A00 = null;
        this.A09 = true;
        this.A07 = true;
        this.A01 = null;
        this.A04 = null;
        this.A02 = null;
    }

    public BlurFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A00 = null;
        this.A09 = true;
        this.A07 = true;
        this.A01 = null;
        this.A04 = null;
        this.A02 = null;
    }

    public BlurFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A00 = null;
        this.A09 = true;
        this.A07 = true;
        this.A01 = null;
        this.A04 = null;
        this.A02 = null;
    }

    public BlurFrameLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A05 = (AbstractC14440lR) r1.ANe.get();
            this.A03 = (C16590pI) r1.AMg.get();
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        if (this.A07) {
            if (this.A09) {
                Bitmap bitmap = this.A00;
                if (bitmap == null) {
                    bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                    this.A00 = bitmap;
                }
                Canvas canvas2 = new Canvas(bitmap);
                Drawable background = getBackground();
                if (background != null) {
                    background.draw(canvas2);
                }
                super.dispatchDraw(canvas2);
                C28521Nt r0 = this.A04;
                if (r0 != null) {
                    r0.A02.clear();
                    this.A04.A03(true);
                }
                C28521Nt r4 = new C28521Nt(this.A03, this);
                this.A04 = r4;
                this.A05.Aaz(r4, this.A00);
                this.A09 = false;
            }
            Bitmap bitmap2 = this.A01;
            if (bitmap2 != null) {
                canvas.drawBitmap(bitmap2, 0.0f, 0.0f, (Paint) null);
                return;
            }
            return;
        }
        super.dispatchDraw(canvas);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C28521Nt r0 = this.A04;
        if (r0 != null) {
            r0.A02.clear();
            this.A04.A03(true);
        }
        ViewPropertyAnimator viewPropertyAnimator = this.A02;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
        Bitmap bitmap = this.A01;
        if (bitmap != null) {
            bitmap.recycle();
        }
        Bitmap bitmap2 = this.A00;
        if (bitmap2 != null) {
            bitmap2.recycle();
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.A00 = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        this.A09 = true;
        invalidate();
    }

    public void setBlurEnabled(boolean z) {
        this.A07 = z;
        if (z) {
            this.A09 = true;
        }
        invalidate();
    }
}
