package com.whatsapp.status;

import X.AbstractC001200n;
import X.AbstractC14440lR;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass10U;
import X.C14900mE;
import X.C18470sV;
import androidx.lifecycle.OnLifecycleEvent;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;

/* loaded from: classes.dex */
public class StatusExpirationLifecycleOwner implements AnonymousClass03H {
    public final C14900mE A00;
    public final AnonymousClass10U A01;
    public final C18470sV A02;
    public final AbstractC14440lR A03;
    public final Runnable A04 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 1);

    public StatusExpirationLifecycleOwner(AbstractC001200n r3, C14900mE r4, AnonymousClass10U r5, C18470sV r6, AbstractC14440lR r7) {
        this.A00 = r4;
        this.A03 = r7;
        this.A02 = r6;
        this.A01 = r5;
        r3.ADr().A00(this);
    }

    public void A00() {
        this.A00.A0G(this.A04);
        this.A03.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 2));
    }

    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    public void onDestroy() {
        this.A00.A0G(this.A04);
    }

    @OnLifecycleEvent(AnonymousClass074.ON_START)
    public void onStart() {
        A00();
    }
}
