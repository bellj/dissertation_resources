package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;

/* loaded from: classes3.dex */
public class EnhancedConstraintLayoutGroup extends Group implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public EnhancedConstraintLayoutGroup(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public EnhancedConstraintLayoutGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }

    public EnhancedConstraintLayoutGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public EnhancedConstraintLayoutGroup(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass067
    public void A06(ConstraintLayout constraintLayout) {
        super.A06(constraintLayout);
        for (int i : getReferencedIds()) {
            constraintLayout.findViewById(i).setAlpha(getAlpha());
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void setAlpha(float f) {
        super.setAlpha(f);
        A01();
    }
}
