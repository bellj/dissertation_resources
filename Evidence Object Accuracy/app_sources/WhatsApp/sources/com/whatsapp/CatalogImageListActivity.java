package com.whatsapp;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass07F;
import X.AnonymousClass19Q;
import X.AnonymousClass2FL;
import X.AnonymousClass2TT;
import X.AnonymousClass3I4;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C252918v;
import X.C37071lG;
import X.C41691tw;
import X.C44691zO;
import X.C54192gK;
import X.C54772hG;
import X.C74953j3;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.CatalogImageListActivity;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CatalogImageListActivity extends ActivityC13790kL {
    public static final boolean A0B = C12990iw.A1X(Build.VERSION.SDK_INT, 21);
    public int A00;
    public int A01;
    public LinearLayoutManager A02;
    public RecyclerView A03;
    public C74953j3 A04;
    public C44691zO A05;
    public AnonymousClass19Q A06;
    public C252918v A07;
    public C37071lG A08;
    public UserJid A09;
    public boolean A0A;

    public CatalogImageListActivity() {
        this(0);
    }

    public CatalogImageListActivity(int i) {
        this.A0A = false;
        ActivityC13830kP.A1P(this, 0);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A07 = C12990iw.A0V(A1M);
            this.A06 = C12980iv.A0Z(A1M);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ActivityC13790kL.A0a(this);
        super.onCreate(bundle);
        AnonymousClass3I4.A01(bundle, this, new AnonymousClass2TT(this));
        if (A0B) {
            C12970iu.A0G(this).setSystemUiVisibility(1792);
            C41691tw.A02(this, R.color.primary);
        }
        this.A09 = ActivityC13790kL.A0V(getIntent(), "cached_jid");
        this.A05 = (C44691zO) getIntent().getParcelableExtra("product");
        this.A00 = getIntent().getIntExtra("image_index", 0);
        setContentView(R.layout.business_product_catalog_image_list);
        this.A03 = (RecyclerView) findViewById(R.id.catalog_image_list);
        A1e((Toolbar) findViewById(R.id.catalog_image_list_toolbar));
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0I(this.A05.A04);
        this.A08 = new C37071lG(this.A07);
        C54192gK r1 = new C54192gK(this, new AnonymousClass2TT(this));
        this.A02 = new LinearLayoutManager();
        this.A03.setAdapter(r1);
        this.A03.setLayoutManager(this.A02);
        C74953j3 r12 = new C74953j3(this.A05.A06.size(), getResources().getDimensionPixelSize(R.dimen.actionbar_height));
        this.A04 = r12;
        this.A03.A0l(r12);
        AnonymousClass028.A0h(this.A03, new AnonymousClass07F() { // from class: X.3PK
            @Override // X.AnonymousClass07F
            public final C018408o AMH(View view, C018408o r6) {
                CatalogImageListActivity catalogImageListActivity = CatalogImageListActivity.this;
                catalogImageListActivity.A01 = r6.A06() + catalogImageListActivity.getResources().getDimensionPixelSize(R.dimen.actionbar_height);
                int A03 = r6.A03();
                C74953j3 r0 = catalogImageListActivity.A04;
                int i = catalogImageListActivity.A01;
                r0.A01 = i;
                r0.A00 = A03;
                int i2 = catalogImageListActivity.A00;
                if (i2 > 0) {
                    catalogImageListActivity.A02.A1R(i2, i);
                }
                return r6;
            }
        });
        int A00 = AnonymousClass00T.A00(this, R.color.primary);
        int A002 = AnonymousClass00T.A00(this, R.color.primary);
        this.A03.A0n(new C54772hG(A0N, this, A00, AnonymousClass00T.A00(this, R.color.catalog_image_list_transparent_color), A002));
        if (bundle == null) {
            this.A06.A03(this.A09, 27, null, 8);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A08.A00();
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }
}
