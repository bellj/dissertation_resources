package com.whatsapp.gifvideopreview;

import X.AbstractC16350or;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass19O;
import X.AnonymousClass1O6;
import X.AnonymousClass2FL;
import X.AnonymousClass2GL;
import X.AnonymousClass31y;
import X.AnonymousClass3XP;
import X.C016307r;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14410lO;
import X.C16120oU;
import X.C16170oZ;
import X.C22050yP;
import X.C253719d;
import X.C33421e0;
import X.C38721ob;
import X.C39391pp;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.videoplayback.VideoSurfaceView;
import java.io.File;

/* loaded from: classes2.dex */
public class GifVideoPreviewActivity extends AnonymousClass2GL {
    public int A00;
    public View A01;
    public C16170oZ A02;
    public C22050yP A03;
    public C16120oU A04;
    public C253719d A05;
    public C14410lO A06;
    public VideoSurfaceView A07;
    public boolean A08;

    public GifVideoPreviewActivity() {
        this(0);
    }

    public GifVideoPreviewActivity(int i) {
        this.A08 = false;
        ActivityC13830kP.A1P(this, 70);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13790kL.A0e(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            this.A05 = (C253719d) A1M.A8V.get();
            this.A04 = C12970iu.A0b(A1M);
            this.A02 = (C16170oZ) A1M.AM4.get();
            this.A06 = (C14410lO) A1M.AB3.get();
            this.A03 = (C22050yP) A1M.A7v.get();
        }
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass2GL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2e() {
        /*
        // Method dump skipped, instructions count: 561
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gifvideopreview.GifVideoPreviewActivity.A2e():void");
    }

    @Override // X.AnonymousClass2GL, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        byte[] A02;
        super.onCreate(bundle);
        setTitle(R.string.send_gif);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.view_once_toggle);
        View A05 = AnonymousClass00T.A05(this, R.id.view_once_toggle_spacer);
        C12990iw.A0x(this, imageView, R.drawable.view_once_selector);
        C016307r.A00(AnonymousClass00T.A03(this, R.color.selector_media_preview_button), imageView);
        imageView.setEnabled(false);
        imageView.setVisibility(0);
        A05.setVisibility(8);
        View view = new View(this);
        this.A01 = view;
        view.setId(R.id.gif_preview_shutter);
        C12970iu.A18(this, this.A01, R.color.black);
        C12960it.A0r(this, this.A01, R.string.gif_preview_description);
        this.A01.setLayoutParams(C12990iw.A0M());
        ((AnonymousClass2GL) this).A01.addView(this.A01, 0);
        VideoSurfaceView videoSurfaceView = new VideoSurfaceView(this);
        this.A07 = videoSurfaceView;
        videoSurfaceView.setId(R.id.gif_preview_video);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1, 17);
        layoutParams.setMargins(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.gif_vide_view_bottom_margin));
        this.A07.setLayoutParams(layoutParams);
        VideoSurfaceView videoSurfaceView2 = this.A07;
        videoSurfaceView2.A0B = new MediaPlayer.OnPreparedListener() { // from class: X.4iB
            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
                mediaPlayer.setVolume(0.0f, 0.0f);
            }
        };
        ((AnonymousClass2GL) this).A01.addView(videoSurfaceView2, 0);
        int intExtra = getIntent().getIntExtra("provider", 0);
        int i = 1;
        if (intExtra != 1) {
            i = 2;
            if (intExtra != 2) {
                i = 0;
            }
        }
        this.A00 = i;
        AnonymousClass028.A0a(this.A07, 2);
        if (TextUtils.isEmpty(((AnonymousClass2GL) this).A0D)) {
            String stringExtra = getIntent().getStringExtra("preview_media_url");
            if (stringExtra == null || (A02 = this.A05.A02(stringExtra)) == null) {
                this.A05.A01(((AnonymousClass2GL) this).A02, getIntent().getStringExtra("static_preview_url"));
            } else {
                ((AnonymousClass2GL) this).A02.setImageBitmap(BitmapFactory.decodeByteArray(A02, 0, A02.length, AnonymousClass19O.A07));
            }
            C253719d r2 = this.A05;
            String stringExtra2 = getIntent().getStringExtra("media_url");
            AnonymousClass3XP r11 = new AnonymousClass3XP(this);
            AnonymousClass009.A01();
            AnonymousClass1O6 A052 = r2.A07.A05();
            C39391pp A00 = A052.A00(stringExtra2);
            if (A00 != null) {
                String str = A00.A00;
                if (new File(str).exists() && A00.A02 != null) {
                    r11.AQX(new File(str), stringExtra2, A00.A02);
                }
            }
            ((AbstractC16350or) new AnonymousClass31y(r2.A02, r2.A03, r2.A05, r2.A06, r2.A08, r2.A09, A052, r11, stringExtra2)).A02.executeOnExecutor(r2.A00(), new Void[0]);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C33421e0 r2 = ((AnonymousClass2GL) this).A0B;
        if (r2 != null) {
            r2.A00.getViewTreeObserver().removeGlobalOnLayoutListener(r2.A01);
            r2.A05.A08();
            r2.A03.dismiss();
            ((AnonymousClass2GL) this).A0B = null;
        }
        C253719d r22 = this.A05;
        C38721ob r0 = r22.A00;
        if (r0 != null) {
            r0.A02.A02(false);
            r22.A00 = null;
        }
    }

    @Override // X.AnonymousClass2GL, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (!TextUtils.isEmpty(((AnonymousClass2GL) this).A0D)) {
            this.A07.setVideoPath(((AnonymousClass2GL) this).A0D);
            this.A07.start();
            this.A01.setVisibility(8);
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A07.A00();
    }
}
