package com.whatsapp;

import X.AnonymousClass018;
import X.C14850m9;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

/* loaded from: classes2.dex */
public abstract class BaseViewStubFragment extends Hilt_BaseViewStubFragment {
    public ViewStub A00;
    public AnonymousClass018 A01;
    public C14850m9 A02;
    public boolean A03 = false;

    @Override // X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A03 = false;
    }

    @Override // com.whatsapp.base.WaFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        ViewStub viewStub;
        super.A0n(z);
        if (z && (viewStub = this.A00) != null && !this.A03) {
            viewStub.inflate();
            this.A03 = true;
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.fragment_viewstub, viewGroup, false);
        ViewStub viewStub = (ViewStub) inflate.findViewById(R.id.fragmentViewStub);
        this.A00 = viewStub;
        boolean A07 = this.A02.A07(1857);
        int i = R.layout.camera;
        if (A07) {
            i = R.layout.camera_new;
        }
        viewStub.setLayoutResource(i);
        if (!this.A03 && (this.A0j || !this.A02.A07(128))) {
            this.A00.inflate();
            this.A03 = true;
        }
        return inflate;
    }
}
