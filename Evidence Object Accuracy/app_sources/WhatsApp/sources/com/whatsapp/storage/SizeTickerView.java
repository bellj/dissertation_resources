package com.whatsapp.storage;

import X.AnonymousClass2QN;
import X.C44891zj;
import X.C72673ez;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import com.whatsapp.WaTextView;
import com.whatsapp.storage.SizeTickerView;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class SizeTickerView extends WaTextView {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public AnimatorSet A06;
    public boolean A07;

    public SizeTickerView(Context context) {
        this(context, null);
    }

    public SizeTickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SizeTickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0K);
            this.A04 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            this.A03 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
            obtainStyledAttributes.recycle();
        }
        this.A01 = 1000;
        this.A00 = 300;
        setTextSize(0, (float) this.A04);
    }

    public final void A0A() {
        String str;
        String[] split = C44891zj.A03(((WaTextView) this).A01, this.A05).split(" ");
        int length = split.length;
        if (length == 0) {
            str = "";
        } else if (length == 1) {
            str = split[0];
        } else {
            SpannableString spannableString = new SpannableString(split[1]);
            spannableString.setSpan(new AbsoluteSizeSpan(this.A03), 0, split[1].length(), 18);
            str = TextUtils.concat(split[0], " ", spannableString);
        }
        setText(str);
    }

    public void A0B(long j, int i, boolean z) {
        if (z) {
            ArrayList arrayList = new ArrayList();
            AnimatorSet animatorSet = this.A06;
            if (animatorSet != null) {
                animatorSet.cancel();
            }
            this.A06 = new AnimatorSet();
            long j2 = this.A05;
            if (j2 != j) {
                ValueAnimator ofInt = ValueAnimator.ofInt(0, 10);
                ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(j2, j) { // from class: X.4ej
                    public final /* synthetic */ long A00;
                    public final /* synthetic */ long A01;

                    {
                        this.A00 = r2;
                        this.A01 = r4;
                    }

                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                        SizeTickerView sizeTickerView = SizeTickerView.this;
                        long j3 = this.A00;
                        sizeTickerView.A05 = (long) (((float) j3) + (((float) ((this.A01 - j3) * ((long) C12960it.A05(valueAnimator.getAnimatedValue())))) / 10.0f));
                        sizeTickerView.A0A();
                    }
                });
                arrayList.add(ofInt);
            }
            if (this.A02 != i) {
                ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(this.A02), Integer.valueOf(i));
                ofObject.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eU
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                        SizeTickerView sizeTickerView = SizeTickerView.this;
                        int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
                        sizeTickerView.A02 = A05;
                        sizeTickerView.setTextColor(A05);
                    }
                });
                arrayList.add(ofObject);
            }
            this.A06.addListener(new C72673ez(this, i, j));
            this.A06.setInterpolator(new LinearInterpolator());
            this.A06.setDuration((long) this.A01);
            this.A06.setStartDelay((long) this.A00);
            this.A06.playTogether(arrayList);
            this.A06.start();
            return;
        }
        AnimatorSet animatorSet2 = this.A06;
        if (animatorSet2 != null) {
            animatorSet2.cancel();
        }
        this.A05 = j;
        this.A02 = i;
        setTextColor(i);
        A0A();
    }
}
