package com.whatsapp.storage;

import X.AbstractC14440lR;
import X.AbstractC15340mz;
import X.AnonymousClass018;
import X.AnonymousClass01F;
import X.AnonymousClass3KH;
import X.AnonymousClass5U8;
import X.C004802e;
import X.C004902f;
import X.C16170oZ;
import X.C28031Kg;
import X.C53102cu;
import X.C90234Nc;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.storage.StorageUsageDeleteMessagesDialogFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* loaded from: classes2.dex */
public class StorageUsageDeleteMessagesDialogFragment extends Hilt_StorageUsageDeleteMessagesDialogFragment {
    public C16170oZ A00;
    public AnonymousClass018 A01;
    public C28031Kg A02;
    public AbstractC14440lR A03;
    public Collection A04;
    public Collection A05;
    public boolean A06;
    public boolean A07;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        boolean z;
        boolean z2;
        AnonymousClass018 r3;
        int i;
        String A0D;
        Iterator it = this.A05.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((AbstractC15340mz) it.next()).A0v) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        Iterator it2 = this.A04.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (((AbstractC15340mz) it2.next()).A0v) {
                    z2 = true;
                    break;
                }
            } else {
                z2 = false;
                break;
            }
        }
        if (z) {
            int size = this.A05.size();
            int i2 = R.string.storage_usage_delete_dialog_starred_item_multiple_subtitle;
            if (size == 1) {
                i2 = R.string.storage_usage_delete_dialog_starred_item_single_subtitle;
            }
            A0D = A0I(i2);
        } else {
            if (z2 || this.A04.size() <= this.A05.size()) {
                r3 = this.A01;
                i = R.plurals.storage_usage_delete_dialog_subtitle;
            } else {
                r3 = this.A01;
                i = R.plurals.storage_usage_delete_dialog_duplicate_item_subtitle;
            }
            A0D = r3.A0D((long) this.A05.size(), i);
        }
        Context A0p = A0p();
        ArrayList arrayList = new ArrayList();
        String A0D2 = this.A01.A0D((long) this.A05.size(), R.plurals.storage_usage_delete_messages_title);
        if (z) {
            if (this.A05.size() == 1) {
                this.A07 = true;
            } else {
                arrayList.add(new C90234Nc(new AnonymousClass5U8() { // from class: X.56R
                    @Override // X.AnonymousClass5U8
                    public final void ANu(boolean z3) {
                        StorageUsageDeleteMessagesDialogFragment.this.A07 = z3;
                    }
                }, A0I(R.string.storage_usage_delete_dialog_starred_checkbox_text)));
            }
        } else if (!z2 && this.A04.size() > this.A05.size()) {
            arrayList.add(new C90234Nc(new AnonymousClass5U8() { // from class: X.56S
                @Override // X.AnonymousClass5U8
                public final void ANu(boolean z3) {
                    StorageUsageDeleteMessagesDialogFragment.this.A06 = z3;
                }
            }, A0I(R.string.storage_usage_delete_dialog_duplicate_checkbox_text)));
        }
        AnonymousClass3KH r1 = new DialogInterface.OnClickListener() { // from class: X.3KH
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                Collection collection;
                StorageUsageDeleteMessagesDialogFragment storageUsageDeleteMessagesDialogFragment = StorageUsageDeleteMessagesDialogFragment.this;
                if (storageUsageDeleteMessagesDialogFragment.A06) {
                    collection = storageUsageDeleteMessagesDialogFragment.A04;
                } else {
                    collection = storageUsageDeleteMessagesDialogFragment.A05;
                }
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                if (!storageUsageDeleteMessagesDialogFragment.A07) {
                    Iterator it3 = collection.iterator();
                    while (it3.hasNext()) {
                        AbstractC15340mz A0f = C12980iv.A0f(it3);
                        if (!A0f.A0v) {
                            linkedHashSet.add(A0f);
                        }
                    }
                } else {
                    linkedHashSet.addAll(collection);
                }
                storageUsageDeleteMessagesDialogFragment.A03.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(storageUsageDeleteMessagesDialogFragment, 20, linkedHashSet));
            }
        };
        C004802e r2 = new C004802e(A0p());
        r2.setView(new C53102cu(A0p, A0D2, A0D, arrayList));
        r2.setPositiveButton(R.string.delete, r1);
        r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4gG
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                StorageUsageDeleteMessagesDialogFragment.this.A1C();
            }
        });
        r2.A0B(true);
        return r2.create();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }
}
