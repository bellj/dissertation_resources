package com.whatsapp.storage;

import X.AbstractC35601iM;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass23D;
import X.AnonymousClass2GE;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2T3;
import X.AnonymousClass3XG;
import X.C1107756v;
import X.C18720su;
import X.C457522x;
import X.C52982cA;
import X.C616631i;
import X.View$OnLayoutChangeListenerC101084mx;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public class StorageUsageMediaPreviewView extends LinearLayout implements AnonymousClass004 {
    public static final Bitmap A0B = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
    public int A00;
    public C18720su A01;
    public AnonymousClass2P7 A02;
    public String A03;
    public List A04;
    public boolean A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final Drawable A09;
    public final C457522x A0A;

    public StorageUsageMediaPreviewView(Context context) {
        this(context, null);
    }

    public StorageUsageMediaPreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public StorageUsageMediaPreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A05) {
            this.A05 = true;
            this.A01 = (C18720su) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.A2c.get();
        }
        setOrientation(0);
        this.A08 = getResources().getDimensionPixelSize(R.dimen.storage_preview_item_thumb_space);
        this.A06 = getResources().getDimensionPixelSize(R.dimen.storage_preview_item_thumb_size);
        int A00 = AnonymousClass00T.A00(getContext(), R.color.gallery_cell);
        this.A07 = A00;
        this.A09 = new ColorDrawable(A00);
        this.A0A = new C457522x(context.getContentResolver(), new Handler(Looper.getMainLooper()), this.A01, "storage-usage-media-preview");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.A04 != null && this.A03 != null) {
            post(new RunnableBRunnable0Shape12S0100000_I0_12(this, 36));
        }
    }

    public void setPreviewMediaItems(List list, int i, String str) {
        this.A04 = list;
        this.A00 = i;
        this.A03 = str;
        removeAllViews();
        if (list.size() == 0) {
            return;
        }
        if (getMeasuredWidth() == 0) {
            addOnLayoutChangeListener(new View$OnLayoutChangeListenerC101084mx(this, str, list, i));
        } else {
            setPreviewMediaItemsInternal(list, i, str);
        }
    }

    public final void setPreviewMediaItemsInternal(List list, int i, String str) {
        ViewGroup.LayoutParams layoutParams;
        AnonymousClass2T3 r3;
        int measuredWidth = getMeasuredWidth();
        int i2 = this.A06;
        int i3 = (measuredWidth + (i2 >> 1)) / i2;
        int measuredWidth2 = getMeasuredWidth();
        int i4 = this.A08;
        int i5 = (measuredWidth2 - ((i3 - 1) * i4)) / i3;
        int min = Math.min(list.size(), i3);
        Drawable A04 = AnonymousClass00T.A04(getContext(), R.drawable.balloon_incoming_frame);
        int A00 = AnonymousClass00T.A00(getContext(), R.color.primary_surface);
        AnonymousClass009.A05(A04);
        Drawable A042 = AnonymousClass2GE.A04(A04, A00);
        for (int i6 = 0; i6 < min; i6++) {
            AbstractC35601iM r4 = (AbstractC35601iM) list.get(i6);
            if (i6 != min - 1 || i <= min) {
                C616631i r32 = new C616631i(getContext());
                r32.A00 = 3;
                r32.setFrameDrawable(A042);
                addView(r32);
                layoutParams = r32.getLayoutParams();
                r3 = r32;
            } else {
                AnonymousClass2T3 r33 = new AnonymousClass2T3(getContext());
                C52982cA r14 = new C52982cA(getContext());
                int i7 = i - min;
                View view = r14.A00;
                if (view != null) {
                    r14.removeView(view);
                }
                r14.addView(r33, 0);
                r14.A00 = r33;
                r14.A03.setText(r14.getContext().getString(R.string.storage_usage_preview_overlay_text, Integer.valueOf(i7)));
                r14.setFrameDrawable(A042);
                addView(r14);
                layoutParams = r14.getLayoutParams();
                r3 = r33;
            }
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            if (i6 != 0) {
                marginLayoutParams.leftMargin = i4;
            }
            marginLayoutParams.width = i5;
            marginLayoutParams.height = i5;
            r3.setMediaItem(r4);
            r3.setScaleType(ImageView.ScaleType.CENTER_CROP);
            r3.setSelector(null);
            C457522x r2 = this.A0A;
            r2.A01((AnonymousClass23D) r3.getTag());
            C1107756v r1 = new C1107756v(r4, this, str, i5);
            r3.setTag(r1);
            r2.A02(r1, new AnonymousClass3XG(r4, r3, r1, this));
        }
    }
}
