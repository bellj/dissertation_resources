package com.whatsapp.storage;

import X.AbstractC005102i;
import X.AbstractC009504t;
import X.AbstractC13890kV;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.AbstractC28051Kk;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02N;
import X.AnonymousClass02Q;
import X.AnonymousClass109;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.AnonymousClass13H;
import X.AnonymousClass193;
import X.AnonymousClass19I;
import X.AnonymousClass19M;
import X.AnonymousClass1A6;
import X.AnonymousClass1AB;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass1Kj;
import X.AnonymousClass1MC;
import X.AnonymousClass1X3;
import X.AnonymousClass23N;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3DI;
import X.AnonymousClass55P;
import X.C004902f;
import X.C009604u;
import X.C103604r1;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C18850tA;
import X.C20710wC;
import X.C21270x9;
import X.C21600xg;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22180yf;
import X.C22370yy;
import X.C22670zS;
import X.C22700zV;
import X.C23000zz;
import X.C231510o;
import X.C242114q;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253018w;
import X.C253218y;
import X.C255419u;
import X.C28031Kg;
import X.C33471e8;
import X.C35251hW;
import X.C35451ht;
import X.C38211ni;
import X.C44891zj;
import X.C623837a;
import X.C64533Fx;
import X.C88054Ec;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class StorageUsageGalleryActivity extends ActivityC13790kL implements AbstractC13890kV {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public ViewGroup A04;
    public AnonymousClass02Q A05;
    public AbstractC009504t A06;
    public AnonymousClass02N A07;
    public C16170oZ A08;
    public AnonymousClass12T A09;
    public C18850tA A0A;
    public C15550nR A0B;
    public C22700zV A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public C21270x9 A0F;
    public AnonymousClass19I A0G;
    public C35451ht A0H;
    public AnonymousClass1A6 A0I;
    public C255419u A0J;
    public C15650ng A0K;
    public C15600nX A0L;
    public C253218y A0M;
    public C623837a A0N;
    public C15660nh A0O;
    public AnonymousClass12H A0P;
    public C242114q A0Q;
    public C21600xg A0R;
    public C15370n3 A0S;
    public C22100yW A0T;
    public C22180yf A0U;
    public ProgressDialogFragment A0V;
    public C231510o A0W;
    public AnonymousClass193 A0X;
    public C16120oU A0Y;
    public C20710wC A0Z;
    public AbstractC14640lm A0a;
    public AnonymousClass109 A0b;
    public C22370yy A0c;
    public AnonymousClass13H A0d;
    public C16630pM A0e;
    public C88054Ec A0f;
    public AnonymousClass12F A0g;
    public C253018w A0h;
    public AnonymousClass12U A0i;
    public StorageUsageMediaGalleryFragment A0j;
    public C23000zz A0k;
    public AnonymousClass01H A0l;
    public Runnable A0m;
    public Runnable A0n;
    public String A0o;
    public boolean A0p;
    public final Handler A0q;
    public final AbstractC18860tB A0r;
    public final AbstractC28051Kk A0s;
    public final Runnable A0t;
    public final Runnable A0u;

    @Override // X.AbstractC13890kV
    public /* synthetic */ void A5t(AnonymousClass1IS r1) {
    }

    @Override // X.AbstractC13890kV
    public void A5u(Drawable drawable, View view) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void A8w(AnonymousClass1IS r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AAC(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AUy(AbstractC15340mz r1, boolean z) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AbN(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void Ach(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void Ad0(AbstractC15340mz r1, int i) {
    }

    @Override // X.AbstractC13890kV
    public boolean AdM(AnonymousClass1IS r2) {
        return true;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AfW(AnonymousClass1X3 r1, long j) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AfZ(AbstractC15340mz r1) {
    }

    public StorageUsageGalleryActivity() {
        this(0);
        this.A0q = new Handler(Looper.getMainLooper());
        this.A0t = new RunnableBRunnable0Shape12S0100000_I0_12(this, 34);
        this.A0r = new C35251hW(this);
        this.A0s = new AnonymousClass1Kj(this);
        this.A0u = new RunnableBRunnable0Shape12S0100000_I0_12(this, 35);
    }

    public StorageUsageGalleryActivity(int i) {
        this.A0p = false;
        A0R(new C103604r1(this));
    }

    public static /* synthetic */ void A02(StorageUsageGalleryActivity storageUsageGalleryActivity, Collection collection, Collection collection2) {
        C28031Kg r0 = new C28031Kg(storageUsageGalleryActivity);
        StorageUsageDeleteMessagesDialogFragment storageUsageDeleteMessagesDialogFragment = new StorageUsageDeleteMessagesDialogFragment();
        storageUsageDeleteMessagesDialogFragment.A05 = collection2;
        storageUsageDeleteMessagesDialogFragment.A04 = collection;
        storageUsageDeleteMessagesDialogFragment.A02 = r0;
        storageUsageDeleteMessagesDialogFragment.A1F(storageUsageGalleryActivity.A0V(), null);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0p) {
            this.A0p = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0d = (AnonymousClass13H) r1.ABY.get();
            this.A0h = (C253018w) r1.AJS.get();
            this.A0i = (AnonymousClass12U) r1.AJd.get();
            this.A0Y = (C16120oU) r1.ANE.get();
            this.A0A = (C18850tA) r1.AKx.get();
            this.A08 = (C16170oZ) r1.AM4.get();
            this.A0W = (C231510o) r1.AHO.get();
            this.A0f = new C88054Ec();
            this.A0F = (C21270x9) r1.A4A.get();
            this.A0U = (C22180yf) r1.A5U.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A0M = (C253218y) r1.AAF.get();
            this.A0K = (C15650ng) r1.A4m.get();
            this.A0P = (AnonymousClass12H) r1.AC5.get();
            this.A0Z = (C20710wC) r1.A8m.get();
            this.A0g = (AnonymousClass12F) r1.AJM.get();
            this.A0O = (C15660nh) r1.ABE.get();
            this.A09 = (AnonymousClass12T) r1.AJZ.get();
            this.A0X = (AnonymousClass193) r1.A6S.get();
            this.A0Q = (C242114q) r1.AJq.get();
            this.A0k = (C23000zz) r1.ALg.get();
            this.A0C = (C22700zV) r1.AMN.get();
            this.A0R = (C21600xg) r1.AKc.get();
            this.A0c = (C22370yy) r1.AB0.get();
            this.A0T = (C22100yW) r1.A3g.get();
            this.A0J = (C255419u) r1.AJp.get();
            this.A0b = (AnonymousClass109) r1.AI8.get();
            this.A0L = (C15600nX) r1.A8x.get();
            this.A0e = (C16630pM) r1.AIc.get();
            this.A0G = (AnonymousClass19I) r1.A4a.get();
            this.A0l = C18000rk.A00(r1.A4v);
            this.A0I = (AnonymousClass1A6) r1.A4u.get();
        }
    }

    public final void A2e() {
        Handler handler = this.A0q;
        handler.removeCallbacks(this.A0u);
        Runnable runnable = this.A0n;
        if (runnable != null) {
            handler.removeCallbacks(runnable);
            this.A0n = null;
        }
        ProgressDialogFragment progressDialogFragment = this.A0V;
        if (progressDialogFragment != null) {
            progressDialogFragment.A1C();
            this.A0V = null;
        }
        C623837a r1 = this.A0N;
        if (r1 != null) {
            r1.A03(true);
            this.A0N = null;
        }
        AnonymousClass02N r0 = this.A07;
        if (r0 != null) {
            r0.A01();
            this.A07 = null;
        }
    }

    public final void A2f() {
        int i;
        TextView textView = (TextView) AnonymousClass028.A0D(this.A04, R.id.storage_usage_detail_all_size);
        long j = this.A03;
        if (j >= 0) {
            textView.setText(C44891zj.A04(((ActivityC13830kP) this).A01, Math.max(j - this.A02, 0L)));
            i = 0;
        } else {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public final void A2g() {
        C35451ht r1;
        AbstractC009504t r2 = this.A06;
        if (r2 != null && (r1 = this.A0H) != null) {
            if (r1.A04.isEmpty()) {
                r2.A05();
                return;
            }
            AnonymousClass01d r8 = ((ActivityC13810kN) this).A08;
            AnonymousClass018 r7 = ((ActivityC13830kP) this).A01;
            HashMap hashMap = r1.A04;
            AnonymousClass23N.A00(this, r8, r7.A0I(new Object[]{Integer.valueOf(hashMap.size())}, R.plurals.n_items_selected, (long) hashMap.size()));
            this.A06.A06();
        }
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ AnonymousClass3DI AAl() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int ABe() {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public C64533Fx ABj() {
        return this.A0G.A01;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int ACM(AnonymousClass1X3 r2) {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ PollVoterViewModel AFm() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ ArrayList AGT() {
        return null;
    }

    @Override // X.AbstractC13900kW
    public /* synthetic */ AnonymousClass1AB AGx() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int AH8(AbstractC15340mz r2) {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public boolean AIM() {
        return this.A0H != null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJl() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public boolean AJm(AbstractC15340mz r3) {
        C35451ht r0 = this.A0H;
        if (r0 != null) {
            if (r0.A04.containsKey(r3.A0z)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJv() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AKC(AbstractC15340mz r2) {
        return false;
    }

    @Override // X.AbstractC13890kV
    public void Acq(List list, boolean z) {
        if (this.A0H == null) {
            this.A0H = new C35451ht(((ActivityC13810kN) this).A05, new AnonymousClass55P(this), null, this.A0P);
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            C35451ht r0 = this.A0H;
            AnonymousClass1IS r1 = r2.A0z;
            HashMap hashMap = r0.A04;
            if (z) {
                hashMap.put(r1, r2);
            } else {
                hashMap.remove(r1);
            }
        }
        A2g();
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AdX() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean Adn() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public void AeE(AbstractC15340mz r9) {
        C35451ht r0 = new C35451ht(((ActivityC13810kN) this).A05, new AnonymousClass55P(this), this.A0H, this.A0P);
        this.A0H = r0;
        r0.A04.put(r9.A0z, r9);
        this.A06 = A1W(this.A05);
        AnonymousClass01d r7 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r6 = ((ActivityC13830kP) this).A01;
        C35451ht r1 = this.A0H;
        AnonymousClass23N.A00(this, r7, r6.A0I(new Object[]{Integer.valueOf(r1.A04.size())}, R.plurals.n_items_selected, (long) r1.A04.size()));
    }

    @Override // X.AbstractC13890kV
    public boolean Af1(AbstractC15340mz r6) {
        C35451ht r4 = this.A0H;
        if (r4 == null) {
            r4 = new C35451ht(((ActivityC13810kN) this).A05, new AnonymousClass55P(this), null, this.A0P);
            this.A0H = r4;
        }
        AnonymousClass1IS r2 = r6.A0z;
        boolean containsKey = r4.A04.containsKey(r2);
        HashMap hashMap = this.A0H.A04;
        if (containsKey) {
            hashMap.remove(r2);
        } else {
            hashMap.put(r2, r6);
        }
        A2g();
        return !containsKey;
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        long j = this.A02;
        if (j >= 0) {
            if (j > this.A03) {
                Log.e("Deleted media size is greater than the total media size");
            }
            Intent intent = new Intent();
            AbstractC14640lm r0 = this.A0a;
            if (r0 != null) {
                intent.putExtra("jid", r0.getRawString());
            }
            intent.putExtra("gallery_type", this.A01);
            intent.putExtra("memory_size", Math.max(this.A03 - this.A02, 0L));
            intent.putExtra("deleted_size", this.A02);
            setResult(1, intent);
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        A29();
        setContentView(R.layout.activity_storage_usage_gallery);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        C252718t r04 = ((ActivityC13790kL) this).A0D;
        AnonymousClass13H r05 = this.A0d;
        C253018w r06 = this.A0h;
        AbstractC15710nm r07 = ((ActivityC13810kN) this).A03;
        C15570nT r08 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r09 = ((ActivityC13830kP) this).A05;
        AnonymousClass12U r010 = this.A0i;
        C16120oU r011 = this.A0Y;
        AnonymousClass19M r012 = ((ActivityC13810kN) this).A0B;
        C15450nH r013 = ((ActivityC13810kN) this).A06;
        C18850tA r014 = this.A0A;
        C16170oZ r015 = this.A08;
        C231510o r016 = this.A0W;
        C88054Ec r017 = this.A0f;
        AnonymousClass12P r018 = ((ActivityC13790kL) this).A00;
        C15550nR r019 = this.A0B;
        C22180yf r020 = this.A0U;
        AnonymousClass01d r021 = ((ActivityC13810kN) this).A08;
        C15610nY r022 = this.A0D;
        AnonymousClass018 r023 = ((ActivityC13830kP) this).A01;
        C20710wC r024 = this.A0Z;
        AnonymousClass12F r025 = this.A0g;
        C253218y r026 = this.A0M;
        AnonymousClass12T r027 = this.A09;
        AnonymousClass193 r15 = this.A0X;
        C242114q r14 = this.A0Q;
        C23000zz r13 = this.A0k;
        C22700zV r12 = this.A0C;
        C14820m6 r11 = ((ActivityC13810kN) this).A09;
        C22370yy r10 = this.A0c;
        C22100yW r9 = this.A0T;
        this.A05 = new AnonymousClass1MC(r018, r07, r03, r08, r013, r015, this, r027, r014, r019, r12, r022, this.A0I, this.A0J, r021, r0, r11, r023, this.A0L, r026, r14, r9, r020, r012, r016, r15, r02, r011, r024, this.A0b, r10, r05, this.A0e, r017, r025, r06, r010, this, r13, r04, r09, this.A0l);
        this.A0E = this.A0F.A04(this, "storage-usage-gallery-activity");
        int intExtra = getIntent().getIntExtra("gallery_type", 0);
        this.A01 = intExtra;
        if (intExtra == 0) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
            AnonymousClass009.A05(A01);
            this.A0a = A01;
            this.A0S = this.A0B.A0A(A01);
        }
        this.A03 = getIntent().getLongExtra("memory_size", 0);
        this.A0o = getIntent().getStringExtra("session_id");
        this.A00 = getIntent().getIntExtra("entry_point", 0);
        String str = null;
        if (bundle == null) {
            int i = this.A01;
            AbstractC14640lm r028 = this.A0a;
            if (r028 != null) {
                str = r028.getRawString();
            }
            this.A0j = StorageUsageMediaGalleryFragment.A00(str, i);
            C004902f r5 = new C004902f(A0V());
            r5.A0A(this.A0j, "storage_usage_gallery_fragment_tag", R.id.storage_usage_gallery_container);
            r5.A01();
            this.A02 = 0;
        } else {
            this.A0j = (StorageUsageMediaGalleryFragment) A0V().A0A("storage_usage_gallery_fragment_tag");
            List<AnonymousClass1IS> A04 = C38211ni.A04(bundle);
            if (A04 != null) {
                for (AnonymousClass1IS r7 : A04) {
                    AbstractC15340mz A03 = this.A0K.A0K.A03(r7);
                    if (A03 != null) {
                        C35451ht r52 = this.A0H;
                        if (r52 == null) {
                            r52 = new C35451ht(((ActivityC13810kN) this).A05, new AnonymousClass55P(this), null, this.A0P);
                            this.A0H = r52;
                        }
                        r52.A04.put(r7, A03);
                    }
                }
                if (this.A0H != null) {
                    this.A06 = A1W(this.A05);
                }
            }
            this.A02 = bundle.getLong("deleted_size");
        }
        this.A0R.A08.add(this.A0s);
        this.A0P.A03(this.A0r);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(false);
        A1U.A0P(false);
        ((Toolbar) AnonymousClass00T.A05(this, R.id.toolbar)).A07();
        View inflate = LayoutInflater.from(this).inflate(R.layout.storage_usage_detail_toolbar, (ViewGroup) null, false);
        AnonymousClass009.A03(inflate);
        ViewGroup viewGroup = (ViewGroup) inflate;
        this.A04 = viewGroup;
        ImageView imageView = (ImageView) AnonymousClass028.A0D(viewGroup, R.id.storage_usage_back_button);
        imageView.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 0));
        boolean z = !((ActivityC13830kP) this).A01.A04().A06;
        int i2 = R.drawable.ic_back_rtl;
        if (z) {
            i2 = R.drawable.ic_back;
        }
        imageView.setImageResource(i2);
        View A0D = AnonymousClass028.A0D(this.A04, R.id.storage_usage_sort_button);
        A0D.setVisibility(0);
        A0D.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 1));
        A1U.A0N(true);
        A1U.A0G(this.A04, new C009604u(-1, -1));
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(this.A04, R.id.storage_usage_detail_name);
        View A0D2 = AnonymousClass028.A0D(this.A04, R.id.storage_usage_contact_photo_container);
        ImageView imageView2 = (ImageView) AnonymousClass028.A0D(this.A04, R.id.storage_usage_contact_photo);
        int i3 = this.A01;
        if (i3 == 2) {
            textEmojiLabel.setText(C33471e8.A03(this, ((ActivityC13830kP) this).A01));
        } else if (i3 == 1) {
            textEmojiLabel.setText(R.string.storage_usage_forwarded_files_title);
        } else {
            if (i3 == 0) {
                C15610nY r2 = this.A0D;
                C15370n3 r029 = this.A0S;
                AnonymousClass009.A05(r029);
                textEmojiLabel.A0G(null, r2.A04(r029));
                A0D2.setVisibility(0);
                this.A0E.A06(imageView2, this.A0S);
            }
            textEmojiLabel.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            textEmojiLabel.setMarqueeRepeatLimit(1);
            textEmojiLabel.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(textEmojiLabel, 49));
            ((ActivityC13810kN) this).A05.A0J(new RunnableBRunnable0Shape8S0200000_I0_8(this, 23, textEmojiLabel), 1000);
            A2f();
        }
        A0D2.setVisibility(8);
        textEmojiLabel.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textEmojiLabel.setMarqueeRepeatLimit(1);
        textEmojiLabel.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(textEmojiLabel, 49));
        ((ActivityC13810kN) this).A05.A0J(new RunnableBRunnable0Shape8S0200000_I0_8(this, 23, textEmojiLabel), 1000);
        A2f();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C35451ht r0 = this.A0H;
        if (r0 != null) {
            r0.A00();
            this.A0H = null;
        }
        this.A0j = null;
        C21600xg r02 = this.A0R;
        r02.A08.remove(this.A0s);
        this.A0q.removeCallbacks(null);
        A2e();
        this.A0P.A04(this.A0r);
        AnonymousClass1J1 r03 = this.A0E;
        if (r03 != null) {
            r03.A00();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        C35451ht r0 = this.A0H;
        if (r0 != null) {
            ArrayList arrayList = new ArrayList();
            for (AbstractC15340mz r02 : r0.A04.values()) {
                arrayList.add(r02.A0z);
            }
            C38211ni.A09(bundle, arrayList);
        }
        bundle.putLong("deleted_size", this.A02);
    }
}
