package com.whatsapp.storage;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class StorageUsageMediaPreviewOverflowOverlayView extends View implements AnonymousClass004 {
    public Drawable A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public StorageUsageMediaPreviewOverflowOverlayView(Context context) {
        this(context, null);
    }

    public StorageUsageMediaPreviewOverflowOverlayView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public StorageUsageMediaPreviewOverflowOverlayView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        C12970iu.A18(context, this, R.color.black_alpha_40);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Drawable drawable = this.A00;
        if (drawable != null) {
            C12980iv.A17(drawable, this);
            this.A00.draw(canvas);
        }
    }

    public void setFrameDrawable(Drawable drawable) {
        this.A00 = drawable;
        invalidate();
    }
}
