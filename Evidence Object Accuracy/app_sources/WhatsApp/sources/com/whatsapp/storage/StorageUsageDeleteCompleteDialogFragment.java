package com.whatsapp.storage;

import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass07M;
import X.C004802e;
import X.C004902f;
import X.C14900mE;
import X.C33581ea;
import X.C44891zj;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class StorageUsageDeleteCompleteDialogFragment extends Hilt_StorageUsageDeleteCompleteDialogFragment {
    public C14900mE A00;
    public AnonymousClass018 A01;

    public static StorageUsageDeleteCompleteDialogFragment A00(long j) {
        StorageUsageDeleteCompleteDialogFragment storageUsageDeleteCompleteDialogFragment = new StorageUsageDeleteCompleteDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("deleted_disk_size", j);
        storageUsageDeleteCompleteDialogFragment.A0U(bundle);
        return storageUsageDeleteCompleteDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        ((DialogFragment) this).A03.getWindow().setLayout(A02().getDimensionPixelSize(R.dimen.storage_usage_delete_complete_dialog_width), -2);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Context A0p = A0p();
        Bundle A03 = A03();
        View inflate = LayoutInflater.from(A0p).inflate(R.layout.storage_usage_delete_complete_dialog, (ViewGroup) null, false);
        AnonymousClass07M A04 = AnonymousClass07M.A04(A0p, R.drawable.storage_usage_check_mark_icon);
        AnonymousClass009.A05(A04);
        ((ImageView) AnonymousClass028.A0D(inflate, R.id.check_mark_image_view)).setImageDrawable(A04);
        A04.start();
        A04.A08(new C33581ea(this));
        ((TextView) AnonymousClass028.A0D(inflate, R.id.title_text_view)).setText(C44891zj.A02(this.A01, R.plurals.storage_usage_delete_completed_text, A03.getLong("deleted_disk_size"), true));
        C004802e r0 = new C004802e(A0p);
        r0.setView(inflate);
        r0.A0B(true);
        return r0.create();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }
}
