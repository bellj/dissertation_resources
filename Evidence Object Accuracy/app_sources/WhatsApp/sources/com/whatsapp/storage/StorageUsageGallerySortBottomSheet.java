package com.whatsapp.storage;

import X.AnonymousClass2Ul;
import X.C51392Um;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class StorageUsageGallerySortBottomSheet extends Hilt_StorageUsageGallerySortBottomSheet {
    public C51392Um A00;
    public AnonymousClass2Ul A01;
    public AnonymousClass2Ul A02;
    public AnonymousClass2Ul A03;
    public AnonymousClass2Ul A04;

    public static StorageUsageGallerySortBottomSheet A00(int i, boolean z) {
        StorageUsageGallerySortBottomSheet storageUsageGallerySortBottomSheet = new StorageUsageGallerySortBottomSheet();
        Bundle bundle = new Bundle();
        bundle.putInt("storage_usage_gallery_sort_bottom_sheet_selected_sort_row", i);
        bundle.putBoolean("storage_usage_gallery_sort_bottom_sheet_show_forwarding_score", z);
        storageUsageGallerySortBottomSheet.A0U(bundle);
        return storageUsageGallerySortBottomSheet;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x00e3  */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A10(android.os.Bundle r10, android.view.LayoutInflater r11, android.view.ViewGroup r12) {
        /*
        // Method dump skipped, instructions count: 271
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.storage.StorageUsageGallerySortBottomSheet.A10(android.os.Bundle, android.view.LayoutInflater, android.view.ViewGroup):android.view.View");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A00 = null;
        this.A02 = null;
        this.A03 = null;
        this.A04 = null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, 2131952347);
    }
}
