package com.whatsapp.storage;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC28051Kk;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02M;
import X.AnonymousClass0QS;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass1J1;
import X.AnonymousClass1MA;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C05480Ps;
import X.C103594r0;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16490p7;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20120vF;
import X.C21270x9;
import X.C21560xc;
import X.C21600xg;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27691It;
import X.C28021Kd;
import X.C33461e7;
import X.C33481e9;
import X.C33591ed;
import X.C48232Fc;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.MenuItem;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public class StorageUsageActivity extends ActivityC13790kL {
    public static final long A0R = TimeUnit.MINUTES.toMillis(1);
    public int A00;
    public RecyclerView A01;
    public RunnableBRunnable0Shape8S0200000_I0_8 A02;
    public C48232Fc A03;
    public C15550nR A04;
    public C15610nY A05;
    public AnonymousClass1J1 A06;
    public C21270x9 A07;
    public C15650ng A08;
    public C20120vF A09;
    public C15660nh A0A;
    public C16490p7 A0B;
    public C21600xg A0C;
    public C16120oU A0D;
    public AnonymousClass12F A0E;
    public AnonymousClass1MA A0F;
    public C33461e7 A0G;
    public C33481e9 A0H;
    public AnonymousClass19O A0I;
    public C21560xc A0J;
    public String A0K;
    public ArrayList A0L;
    public List A0M;
    public boolean A0N;
    public final AbstractC28051Kk A0O;
    public final C27691It A0P;
    public final Set A0Q;

    public StorageUsageActivity() {
        this(0);
        this.A0P = new C27691It();
        this.A0Q = new HashSet();
        this.A0L = new ArrayList();
        this.A0O = new C33591ed(this);
    }

    public StorageUsageActivity(int i) {
        this.A0N = false;
        A0R(new C103594r0(this));
    }

    public static /* synthetic */ void A02(StorageUsageActivity storageUsageActivity, List list, List list2, boolean z) {
        C48232Fc r0;
        synchronized (storageUsageActivity) {
            char c = 2;
            if (storageUsageActivity.A0K != null && list != null && !list.isEmpty() && list2 != null && !list2.isEmpty()) {
                Iterator it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        c = 1;
                        break;
                    }
                    AbstractC14640lm A01 = ((C28021Kd) list.get(((Integer) it.next()).intValue())).A01();
                    C15550nR r02 = storageUsageActivity.A04;
                    AnonymousClass009.A05(A01);
                    C15370n3 A0A = r02.A0A(A01);
                    if (A0A != null && storageUsageActivity.A05.A0M(A0A, storageUsageActivity.A0M, true)) {
                        break;
                    }
                }
            } else if (!z) {
                c = 0;
            }
            if (list2 == null || ((r0 = storageUsageActivity.A03) != null && r0.A05() && c == 2)) {
                if (list == null) {
                    list = new ArrayList();
                } else if (!TextUtils.isEmpty(storageUsageActivity.A0K)) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < list.size(); i++) {
                        AbstractC14640lm A012 = ((C28021Kd) list.get(i)).A01();
                        C15550nR r03 = storageUsageActivity.A04;
                        AnonymousClass009.A05(A012);
                        C15370n3 A0A2 = r03.A0A(A012);
                        if (A0A2 != null && storageUsageActivity.A05.A0M(A0A2, storageUsageActivity.A0M, true)) {
                            arrayList.add(list.get(i));
                        }
                    }
                    list = arrayList;
                }
            }
            if (c != 1) {
                ((ActivityC13810kN) storageUsageActivity).A05.A0H(new RunnableBRunnable0Shape1S0300000_I0_1(storageUsageActivity, list, list2, 47));
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0N) {
            this.A0N = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0D = (C16120oU) r1.ANE.get();
            this.A07 = (C21270x9) r1.A4A.get();
            this.A0J = (C21560xc) r1.AAI.get();
            this.A04 = (C15550nR) r1.A45.get();
            this.A05 = (C15610nY) r1.AMe.get();
            this.A08 = (C15650ng) r1.A4m.get();
            this.A0E = (AnonymousClass12F) r1.AJM.get();
            this.A0A = (C15660nh) r1.ABE.get();
            this.A0I = (AnonymousClass19O) r1.ACO.get();
            this.A0B = (C16490p7) r1.ACJ.get();
            this.A0C = (C21600xg) r1.AKc.get();
            this.A09 = (C20120vF) r1.AAv.get();
        }
    }

    public final void A2e(int i) {
        this.A0Q.add(Integer.valueOf(i));
        AnonymousClass1MA r0 = this.A0F;
        C14900mE r3 = r0.A0D;
        Runnable runnable = r0.A0N;
        r3.A0G(runnable);
        r3.A0J(runnable, 1000);
    }

    public final void A2f(int i) {
        Set set = this.A0Q;
        set.remove(Integer.valueOf(i));
        AnonymousClass1MA r4 = this.A0F;
        boolean z = false;
        if (set.size() != 0) {
            z = true;
        }
        C14900mE r3 = r4.A0D;
        Runnable runnable = r4.A0N;
        r3.A0G(runnable);
        if (z) {
            r3.A0J(runnable, 1000);
        } else {
            r4.A0I(2, false);
        }
    }

    public final void A2g(Runnable runnable) {
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 15, runnable));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 0 && i2 == 1) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(intent.getStringExtra("jid"));
            int intExtra = intent.getIntExtra("gallery_type", -1);
            long longExtra = intent.getLongExtra("memory_size", -1);
            long longExtra2 = intent.getLongExtra("deleted_size", -1);
            if (longExtra >= 0) {
                if (longExtra2 > 0) {
                    ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 28));
                    ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 29));
                    ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 30));
                }
                if (intExtra == 0 && A01 != null) {
                    AnonymousClass1MA r5 = this.A0F;
                    for (C28021Kd r3 : r5.A05) {
                        if (r3.A01().equals(A01)) {
                            r3.A00.A0G = longExtra;
                            Collections.sort(r5.A05);
                            r5.A02();
                            return;
                        }
                    }
                }
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C48232Fc r0 = this.A03;
        if (r0 == null || !r0.A05()) {
            super.onBackPressed();
            return;
        }
        this.A0K = null;
        this.A0M = null;
        this.A03.A04(true);
        AnonymousClass1MA r1 = this.A0F;
        r1.A08 = false;
        int A0F = r1.A0F();
        r1.A0I(1, true);
        r1.A0H();
        r1.A0I(4, true);
        ((AnonymousClass02M) r1).A01.A04(null, r1.A0D() - A0F, A0F);
        this.A01.A0Y(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x015f, code lost:
        if (r19.getBoolean("LIST_IS_NOT_FULL", false) != false) goto L_0x0161;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r19) {
        /*
        // Method dump skipped, instructions count: 397
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.storage.StorageUsageActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A01 = null;
        this.A06.A00();
        C21600xg r0 = this.A0C;
        r0.A08.remove(this.A0O);
        this.A0Q.clear();
        RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = this.A02;
        if (runnableBRunnable0Shape8S0200000_I0_8 != null) {
            ((AtomicBoolean) runnableBRunnable0Shape8S0200000_I0_8.A00).set(true);
        }
        AnonymousClass1MA r2 = this.A0F;
        r2.A0D.A0G(r2.A0N);
        r2.A0I(2, false);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_search) {
            return super.onOptionsItemSelected(menuItem);
        }
        onSearchRequested();
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!this.A0L.isEmpty()) {
            bundle.putLong("SAVED_AT_TIMESTAMP", System.currentTimeMillis());
            ArrayList<? extends Parcelable> arrayList = this.A0L;
            if (arrayList.size() > 200) {
                bundle.putParcelableArrayList("LIST_OF_CONTACTS", new ArrayList<>(arrayList.subList(0, 200)));
                bundle.putBoolean("LIST_IS_NOT_FULL", true);
                return;
            }
            bundle.putParcelableArrayList("LIST_OF_CONTACTS", arrayList);
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        C48232Fc r0 = this.A03;
        if (r0 == null) {
            return false;
        }
        r0.A01();
        AnonymousClass1MA r5 = this.A0F;
        r5.A08 = true;
        int A0F = r5.A0F();
        r5.A0I(1, false);
        r5.A0I(3, false);
        r5.A0I(4, false);
        ((AnonymousClass02M) r5).A01.A04(null, r5.A0D() - 1, A0F + 1);
        this.A03.A06.findViewById(R.id.search_back).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 48));
        return false;
    }

    /* loaded from: classes3.dex */
    public class WrappedLinearLayoutManager extends LinearLayoutManager {
        public WrappedLinearLayoutManager() {
            super(1);
        }

        @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
        public void A0u(AnonymousClass0QS r3, C05480Ps r4) {
            try {
                super.A0u(r3, r4);
            } catch (IndexOutOfBoundsException e) {
                Log.e("WrappedLinearLayoutManager", e);
            }
        }
    }
}
