package com.whatsapp.storage;

import X.AbstractC13890kV;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC35601iM;
import X.AbstractC35611iN;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass19O;
import X.AnonymousClass1CY;
import X.AnonymousClass2T3;
import X.C15450nH;
import X.C15650ng;
import X.C15660nh;
import X.C15670ni;
import X.C35241hV;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* loaded from: classes2.dex */
public class StorageUsageMediaGalleryFragment extends Hilt_StorageUsageMediaGalleryFragment {
    public int A00;
    public AnonymousClass12P A01;
    public AbstractC15710nm A02;
    public C15450nH A03;
    public C15650ng A04;
    public C15660nh A05;
    public AnonymousClass12H A06;
    public C15670ni A07;
    public AbstractC14640lm A08;
    public AnonymousClass1CY A09;
    public AnonymousClass19O A0A;
    public final AbstractC18860tB A0B = new C35241hV(this);

    public static StorageUsageMediaGalleryFragment A00(String str, int i) {
        StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = new StorageUsageMediaGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("storage_media_gallery_fragment_gallery_type", i);
        bundle.putString("storage_media_gallery_fragment_jid", str);
        bundle.putInt("sort_type", 2);
        storageUsageMediaGalleryFragment.A0U(bundle);
        return storageUsageMediaGalleryFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            int i = bundle2.getInt("storage_media_gallery_fragment_gallery_type", 0);
            this.A00 = i;
            if (i == 0) {
                AbstractC14640lm A01 = AbstractC14640lm.A01(bundle2.getString("storage_media_gallery_fragment_jid"));
                AnonymousClass009.A05(A01);
                this.A08 = A01;
            } else {
                AnonymousClass028.A0D(((AnonymousClass01E) this).A0A, R.id.no_media_text).setVisibility(8);
            }
        }
        AnonymousClass028.A0m(((MediaGalleryFragmentBase) this).A08, true);
        AnonymousClass028.A0m(A05().findViewById(R.id.no_media), true);
        A1H(false);
        this.A06.A03(this.A0B);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.storage_usage_gallery, viewGroup, false);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A06.A04(this.A0B);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r5, AnonymousClass2T3 r6) {
        AbstractC16130oV r3 = ((AbstractC35601iM) r5).A03;
        boolean A1J = A1J();
        AbstractC13890kV r0 = (AbstractC13890kV) A0C();
        if (A1J) {
            r6.setChecked(r0.Af1(r3));
            return true;
        }
        r0.AeE(r3);
        r6.setChecked(true);
        return true;
    }
}
