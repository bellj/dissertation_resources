package com.whatsapp.growthlock;

import X.ActivityC000900k;
import X.AnonymousClass04S;
import X.C004802e;
import X.C12970iu;
import X.C12980iv;
import X.C15220ml;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class InviteLinkUnavailableDialogFragment extends Hilt_InviteLinkUnavailableDialogFragment {
    public C15220ml A00;

    public static InviteLinkUnavailableDialogFragment A00(boolean z, boolean z2) {
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("finishCurrentActivity", z);
        A0D.putBoolean("isGroupStillLocked", z2);
        InviteLinkUnavailableDialogFragment inviteLinkUnavailableDialogFragment = new InviteLinkUnavailableDialogFragment();
        inviteLinkUnavailableDialogFragment.A0U(A0D);
        return inviteLinkUnavailableDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0B = A0B();
        boolean z = A03().getBoolean("isGroupStillLocked");
        IDxCListenerShape4S0200000_2_I1 iDxCListenerShape4S0200000_2_I1 = new IDxCListenerShape4S0200000_2_I1(A0B, 7, this);
        TextView textView = (TextView) A04().inflate(R.layout.custom_dialog_title, (ViewGroup) null);
        int i = R.string.invite_via_link_was_unavailable_dialog_title;
        if (z) {
            i = R.string.invite_via_link_unavailable_title;
        }
        textView.setText(i);
        C004802e A0S = C12980iv.A0S(A0B);
        A0S.A01.A0B = textView;
        int i2 = R.string.invite_via_link_was_unavailable_dialog_text;
        if (z) {
            i2 = R.string.invite_via_link_unavailable_text;
        }
        A0S.A06(i2);
        A0S.A0B(true);
        A0S.setNegativeButton(R.string.learn_more, iDxCListenerShape4S0200000_2_I1);
        A0S.setPositiveButton(R.string.ok, null);
        AnonymousClass04S create = A0S.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        ActivityC000900k A0B;
        super.onDismiss(dialogInterface);
        if (A03().getBoolean("finishCurrentActivity") && (A0B = A0B()) != null) {
            A0B.finish();
        }
    }
}
