package com.whatsapp;

import X.AbstractActivityC457823d;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass116;
import X.C14820m6;
import X.C14900mE;
import X.C15890o4;
import X.C20730wE;
import X.C21200x2;
import X.C244415n;
import X.C249417m;
import X.C25661Ag;
import X.C35751ig;
import X.C37871n9;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import com.whatsapp.registration.directmigration.RequestPermissionFromSisterAppActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class RequestPermissionActivity extends AbstractActivityC457823d {
    public static final Map A0A;
    public C249417m A00;
    public C20730wE A01;
    public C15890o4 A02;
    public C14820m6 A03;
    public C21200x2 A04;
    public C25661Ag A05;
    public String A06;
    public Set A07;
    public boolean A08;
    public boolean A09;

    static {
        HashMap hashMap = new HashMap(4);
        int[] iArr = new int[6];
        iArr[0] = R.string.permission_storage_cam_on_camera_access_request;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_cam_on_camera_access_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_cam_on_camera_access;
        }
        iArr[1] = i2;
        iArr[2] = R.string.permission_storage_need_write_access_on_camera_access_request;
        int i3 = R.string.permission_storage_need_write_access_on_camera_access_v30;
        if (i < 30) {
            i3 = R.string.permission_storage_need_write_access_on_camera_access;
        }
        iArr[3] = i3;
        iArr[4] = R.string.permission_cam_access_request;
        iArr[5] = R.string.permission_cam_access;
        hashMap.put(30, iArr);
        int[] iArr2 = new int[6];
        iArr2[0] = R.string.permission_storage_cam_on_attaching_photo_request;
        int i4 = R.string.permission_storage_cam_on_attaching_photo_v30;
        if (i < 30) {
            i4 = R.string.permission_storage_cam_on_attaching_photo;
        }
        iArr2[1] = i4;
        iArr2[2] = R.string.permission_storage_need_write_access_on_attaching_photo_request;
        int i5 = R.string.permission_storage_need_write_access_on_attaching_photo_v30;
        if (i < 30) {
            i5 = R.string.permission_storage_need_write_access_on_attaching_photo;
        }
        iArr2[3] = i5;
        iArr2[4] = R.string.permission_cam_access_on_attaching_photo_request;
        iArr2[5] = R.string.permission_cam_access_on_attaching_photo;
        hashMap.put(31, iArr2);
        int[] iArr3 = new int[6];
        iArr3[0] = R.string.permission_storage_cam_on_attaching_video_request;
        int i6 = R.string.permission_storage_cam_on_attaching_video_v30;
        if (i < 30) {
            i6 = R.string.permission_storage_cam_on_attaching_video;
        }
        iArr3[1] = i6;
        iArr3[2] = R.string.permission_storage_need_write_access_on_attaching_video_request;
        int i7 = R.string.permission_storage_need_write_access_on_attaching_video_v30;
        if (i < 30) {
            i7 = R.string.permission_storage_need_write_access_on_attaching_video;
        }
        iArr3[3] = i7;
        iArr3[4] = R.string.permission_cam_access_on_attaching_video_request;
        iArr3[5] = R.string.permission_cam_access_on_attaching_video;
        hashMap.put(32, iArr3);
        int[] iArr4 = new int[6];
        iArr4[0] = R.string.permission_storage_cam_on_post_status_request;
        int i8 = R.string.permission_storage_cam_on_post_status_v30;
        if (i < 30) {
            i8 = R.string.permission_storage_cam_on_post_status;
        }
        iArr4[1] = i8;
        iArr4[2] = R.string.permission_storage_need_write_access_on_post_status_request;
        int i9 = R.string.permission_storage_need_write_access_on_post_status_v30;
        if (i < 30) {
            i9 = R.string.permission_storage_need_write_access_on_post_status;
        }
        iArr4[3] = i9;
        iArr4[4] = R.string.permission_cam_on_post_status_request;
        iArr4[5] = R.string.permission_cam_on_post_status;
        hashMap.put(33, iArr4);
        A0A = Collections.unmodifiableMap(hashMap);
    }

    public static Intent A02(Context context, int i, int i2, boolean z) {
        C35751ig r3 = new C35751ig(context);
        r3.A01 = R.drawable.permission_contacts_small;
        r3.A0C = new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS"};
        r3.A02 = i;
        r3.A0A = null;
        r3.A03 = i2;
        r3.A08 = null;
        r3.A06 = z;
        return r3.A00();
    }

    public static Intent A03(Context context, int i, int i2, boolean z) {
        C35751ig r3 = new C35751ig(context);
        r3.A01 = R.drawable.permission_storage;
        r3.A0C = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        r3.A02 = i;
        r3.A03 = i2;
        r3.A06 = z;
        return r3.A00();
    }

    public static Intent A09(Context context, C15890o4 r3, int i) {
        return A0A(context, r3, (int[]) A0A.get(Integer.valueOf(i)), i, false);
    }

    public static Intent A0A(Context context, C15890o4 r10, int[] iArr, int i, boolean z) {
        C35751ig r2;
        boolean z2 = !r10.A07();
        boolean z3 = false;
        if (r10.A02("android.permission.CAMERA") != 0) {
            z3 = true;
        }
        if (iArr == null) {
            StringBuilder sb = new StringBuilder("conversation/check/camera/storage/permissions/unexpected request code ");
            sb.append(i);
            Log.e(sb.toString());
        } else if (z3) {
            if (z2) {
                r2 = new C35751ig(context);
                r2.A09 = new int[]{R.drawable.permission_storage, R.drawable.permission_plus, R.drawable.permission_cam};
                r2.A0C = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
                r2.A02 = iArr[0];
                r2.A03 = iArr[1];
            } else {
                r2 = new C35751ig(context);
                r2.A01 = R.drawable.permission_cam;
                r2.A02 = iArr[4];
                r2.A03 = iArr[5];
                r2.A0C = new String[]{"android.permission.CAMERA"};
            }
            r2.A06 = z;
            return r2.A00();
        } else if (z2) {
            return A03(context, iArr[2], iArr[3], z);
        }
        return null;
    }

    public static void A0B(Activity activity) {
        if (!activity.isFinishing()) {
            C35751ig r2 = new C35751ig(activity);
            r2.A01 = R.drawable.permission_call;
            r2.A0C = (String[]) C15890o4.A00().toArray(new String[0]);
            r2.A02 = R.string.permission_read_phone_number_request;
            r2.A03 = R.string.permission_read_phone_number_permission_needed;
            r2.A06 = true;
            activity.startActivityForResult(r2.A00(), 155);
        }
    }

    public static void A0D(Activity activity, int i, int i2) {
        if (!activity.isFinishing()) {
            activity.startActivityForResult(A02(activity, i, i2, false), 150);
        }
    }

    public static void A0K(Activity activity, int i, int i2) {
        if (!activity.isFinishing()) {
            activity.startActivityForResult(A03(activity, i, i2, false), 151);
        }
    }

    public static void A0L(Activity activity, int i, int i2, int i3) {
        if (!activity.isFinishing()) {
            activity.startActivityForResult(A03(activity, i, i2, false), i3);
        }
    }

    public static void A0M(Activity activity, C14900mE r12, C15890o4 r13, boolean z) {
        boolean z2;
        boolean z3;
        int i;
        C35751ig r4;
        String[] strArr;
        int i2 = Build.VERSION.SDK_INT;
        if ((i2 >= 23 || r13.A00.A00.checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") == 0) && (i2 < 23 || r13.A02("android.permission.RECORD_AUDIO") == 0)) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (!z || ((i2 >= 23 || r13.A00.A00.checkCallingOrSelfPermission("android.permission.CAMERA") == 0) && (i2 < 23 || r13.A02("android.permission.CAMERA") == 0))) {
            z3 = false;
        } else {
            z3 = true;
        }
        StringBuilder sb = new StringBuilder("request/permission/checkCameraAndMicPermissionsForVoipCall needMicPerm = ");
        sb.append(z2);
        sb.append(", needCameraPerm = ");
        sb.append(z3);
        Log.i(sb.toString());
        if (i2 >= 23) {
            if (z3) {
                if (z2) {
                    r4 = new C35751ig(activity);
                    r4.A09 = new int[]{R.drawable.permission_mic, R.drawable.permission_plus, R.drawable.permission_cam};
                    r4.A0C = new String[]{"android.permission.CAMERA", "android.permission.RECORD_AUDIO"};
                    r4.A02 = R.string.permission_mic_and_cam_on_video_call_request;
                    r4.A03 = R.string.permission_mic_and_cam_on_video_call;
                    r4.A06 = true;
                    activity.startActivityForResult(r4.A00(), 152);
                    return;
                }
                r4 = new C35751ig(activity);
                r4.A01 = R.drawable.permission_cam;
                r4.A02 = R.string.permission_cam_access_on_video_call_request;
                r4.A03 = R.string.permission_cam_access_on_video_call;
                strArr = new String[]{"android.permission.CAMERA"};
            } else if (z2) {
                r4 = new C35751ig(activity);
                r4.A01 = R.drawable.permission_mic;
                r4.A02 = R.string.permission_mic_access_request;
                r4.A03 = R.string.permission_mic_access;
                strArr = new String[]{"android.permission.RECORD_AUDIO"};
            } else {
                return;
            }
            r4.A0C = strArr;
            r4.A06 = true;
            activity.startActivityForResult(r4.A00(), 152);
            return;
        }
        if (z3) {
            i = R.string.can_not_start_video_call_without_camera_permission;
            if (z2) {
                i = R.string.can_not_start_video_call_without_mic_and_camera_permission;
            }
        } else if (z2) {
            i = R.string.can_not_start_voip_call_without_record_permission;
        } else {
            return;
        }
        r12.A05(i, 1);
    }

    public static void A0N(Activity activity, C15890o4 r7, int i, boolean z) {
        int i2;
        if (!activity.isFinishing() && (!r7.A06())) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(C15890o4.A00());
            C35751ig r1 = new C35751ig(activity);
            if (Build.VERSION.SDK_INT >= 28) {
                arrayList.add("android.permission.READ_CALL_LOG");
                arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                r1.A0C = (String[]) arrayList.toArray(new String[0]);
                r1.A02 = R.string.permission_flash_call_read_call_log_telephone_request;
                i2 = R.string.permission_flash_call_read_call_log_telephone_permission_needed;
            } else {
                arrayList.add("android.permission.CALL_PHONE");
                r1.A0C = (String[]) arrayList.toArray(new String[0]);
                r1.A02 = R.string.permission_flash_call_telephone_request;
                i2 = R.string.permission_flash_call_telephone_permission_needed;
            }
            r1.A03 = i2;
            r1.A04 = R.string.permission_flash_call_permission_title;
            r1.A06 = true;
            r1.A06 = true;
            r1.A07 = z;
            activity.startActivityForResult(r1.A00(), i);
        }
    }

    public static void A0O(AnonymousClass01E r2, int i, int i2) {
        if (r2.A0p() != null) {
            r2.startActivityForResult(A02(r2.A0p(), i, i2, false), 150);
        }
    }

    public static void A0P(C14820m6 r6, String[] strArr) {
        for (String str : strArr) {
            SharedPreferences sharedPreferences = r6.A00;
            sharedPreferences.edit().putBoolean(str, true).apply();
            if (C37871n9.A01(str, C244415n.A02)) {
                r6.A13(true);
                sharedPreferences.edit().putBoolean("nearby_location_new_user", true).apply();
            }
        }
    }

    public static boolean A0Q(Activity activity, AnonymousClass116 r12, C15890o4 r13, String str, int[] iArr, int i, int i2, int i3, int i4) {
        String[] strArr;
        boolean z = !r13.A07();
        boolean z2 = !r12.A00();
        if (z) {
            strArr = z2 ? new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"} : new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        } else if (!z2) {
            return true;
        } else {
            strArr = new String[]{"android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.GET_ACCOUNTS"};
        }
        int length = iArr.length;
        C35751ig r1 = new C35751ig(activity);
        if (length == 1) {
            r1.A01 = iArr[0];
        } else {
            r1.A09 = iArr;
        }
        r1.A0C = strArr;
        r1.A02 = i3;
        r1.A04 = i2;
        r1.A00 = i4;
        r1.A06 = true;
        Intent A00 = r1.A00();
        A00.putExtra("permission_requester_screen", str);
        activity.startActivityForResult(A00, i);
        return false;
    }

    public static boolean A0R(Activity activity, C15890o4 r6) {
        if (!(!r6.A07())) {
            return true;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_read_on_viewing_media_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_read_on_viewing_media;
        }
        activity.startActivityForResult(A03(activity, R.string.permission_storage_need_read_on_viewing_media_request, i2, false), 34);
        return false;
    }

    public static boolean A0S(Activity activity, C15890o4 r6) {
        if (r6.A07()) {
            return true;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_cam_on_share_status_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_cam_on_share_status;
        }
        activity.startActivityForResult(A03(activity, R.string.permission_storage_cam_on_share_status_request, i2, false), 151);
        return false;
    }

    public static boolean A0T(Activity activity, C15890o4 r2, int i) {
        Intent A09 = A09(activity, r2, i);
        if (A09 == null) {
            return true;
        }
        activity.startActivityForResult(A09, i);
        return false;
    }

    public static boolean A0U(Activity activity, C15890o4 r4, int i, int i2, int i3) {
        String[] strArr = C244415n.A02;
        if (r4.A03()) {
            return true;
        }
        C35751ig r1 = new C35751ig(activity);
        r1.A01 = R.drawable.permission_location;
        r1.A0C = strArr;
        r1.A03 = i2;
        r1.A02 = i;
        activity.startActivityForResult(r1.A00(), i3);
        return false;
    }

    public static boolean A0V(Activity activity, String[] strArr) {
        for (String str : strArr) {
            if (!AnonymousClass00T.A0G(activity, str)) {
                return false;
            }
        }
        return true;
    }

    public static boolean A0W(Context context, C15890o4 r5) {
        if (!(!r5.A07())) {
            return true;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_read_on_viewing_media_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_read_on_viewing_media;
        }
        context.startActivity(A03(context, R.string.permission_storage_need_read_on_viewing_media_request, i2, false));
        return false;
    }

    public static boolean A0X(AnonymousClass01E r6, C15890o4 r7) {
        if (r7.A07()) {
            return true;
        }
        Context A0p = r6.A0p();
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_cam_on_share_status_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_cam_on_share_status;
        }
        r6.startActivityForResult(A03(A0p, R.string.permission_storage_cam_on_share_status_request, i2, false), 151);
        return false;
    }

    public static boolean A0Y(C14820m6 r5, String[] strArr) {
        for (String str : strArr) {
            if (r5.A00.getBoolean(str, false)) {
                return false;
            }
        }
        return true;
    }

    public final String A22(Bundle bundle, boolean z) {
        String str;
        String str2;
        if (z) {
            str = "perm_denial_message_id";
        } else {
            str = "message_id";
        }
        int i = bundle.getInt(str);
        if (i == 0) {
            return null;
        }
        if (z) {
            str2 = "perm_denial_message_params_id";
        } else {
            str2 = "message_params_id";
        }
        int[] intArray = bundle.getIntArray(str2);
        if (intArray == null) {
            return getString(i);
        }
        int length = intArray.length;
        String[] strArr = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            strArr[i2] = getString(intArray[i2]);
        }
        return getString(i, strArr);
    }

    public void A23(String str, Bundle bundle) {
        if (str != null) {
            ((TextView) AnonymousClass00T.A05(this, R.id.permission_message)).setText(str);
            return;
        }
        StringBuilder sb = new StringBuilder("request/permission/activity/there is no message id for ");
        sb.append(Arrays.toString(bundle.getStringArray("permissions")));
        Log.e(sb.toString());
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f7, code lost:
        if (r1 != false) goto L_0x00f9;
     */
    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 479
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.RequestPermissionActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity, X.AbstractC000300e
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        String str;
        String str2;
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 0) {
            setResult(-1);
            for (int i2 = 0; i2 < strArr.length; i2++) {
                if (iArr[i2] == 0) {
                    C14820m6 r0 = this.A03;
                    r0.A00.edit().remove(strArr[i2]).apply();
                    if ("android.permission.WRITE_CONTACTS".equals(strArr[i2])) {
                        this.A00.A00(getApplicationContext());
                        this.A01.A04();
                    }
                    Set set = this.A07;
                    if (set != null) {
                        set.remove(strArr[i2]);
                    }
                } else {
                    StringBuilder sb = new StringBuilder("request/permission/activity/");
                    sb.append(strArr[i2]);
                    sb.append(" denied");
                    Log.i(sb.toString());
                    if (this.A07 == null) {
                        setResult(0);
                    }
                }
                if (this.A06 != null) {
                    String str3 = strArr[i2];
                    boolean z = false;
                    if (iArr[i2] == 0) {
                        z = true;
                    }
                    if ("android.permission.WRITE_CONTACTS".equals(str3)) {
                        str = "access_to_contacts";
                    } else if ("android.permission.WRITE_EXTERNAL_STORAGE".equals(str3)) {
                        str = "access_to_files";
                    }
                    if (z) {
                        str2 = "allow";
                    } else {
                        str2 = "not_now";
                    }
                    this.A05.A01(str, str2);
                }
            }
            Set set2 = this.A07;
            if (set2 != null && !set2.isEmpty()) {
                setResult(0);
            }
            finish();
        }
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        AnonymousClass009.A05(extras);
        String[] stringArray = extras.getStringArray("permissions");
        if (stringArray != null) {
            if (!(this instanceof RequestPermissionFromSisterAppActivity)) {
                for (String str : stringArray) {
                    if (this.A02.A02(str) == 0) {
                        this.A03.A00.edit().remove(str).apply();
                    } else {
                        return;
                    }
                }
            } else {
                for (String str2 : stringArray) {
                    if (!C15890o4.A01(this, str2)) {
                        return;
                    }
                }
            }
        }
        if (!this.A08) {
            Log.i("request/permission/activity/permissions has been granted while we were paused");
            setResult(-1);
            finish();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("permissions_requested", this.A09);
    }
}
