package com.whatsapp;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.C14850m9;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_BaseViewStubFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A01() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A01();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A01()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.Hilt_BaseViewStubFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A01();
        A18();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4, types: [com.whatsapp.BaseViewStubFragment] */
    public void A18() {
        AbstractC51092Su r0;
        Hilt_CameraHomeFragment hilt_CameraHomeFragment;
        if (this instanceof Hilt_CameraHomeFragment) {
            Hilt_CameraHomeFragment hilt_CameraHomeFragment2 = (Hilt_CameraHomeFragment) this;
            if (!hilt_CameraHomeFragment2.A02) {
                hilt_CameraHomeFragment2.A02 = true;
                r0 = (AbstractC51092Su) hilt_CameraHomeFragment2.generatedComponent();
                hilt_CameraHomeFragment = hilt_CameraHomeFragment2;
            } else {
                return;
            }
        } else if (!this.A02) {
            this.A02 = true;
            r0 = (AbstractC51092Su) generatedComponent();
            hilt_CameraHomeFragment = (BaseViewStubFragment) this;
        } else {
            return;
        }
        AnonymousClass01J r1 = ((C51112Sw) r0).A0Y;
        ((WaFragment) hilt_CameraHomeFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) hilt_CameraHomeFragment).A01 = (AnonymousClass180) r1.ALt.get();
        ((BaseViewStubFragment) hilt_CameraHomeFragment).A01 = (AnonymousClass018) r1.ANb.get();
        ((BaseViewStubFragment) hilt_CameraHomeFragment).A02 = (C14850m9) r1.A04.get();
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
