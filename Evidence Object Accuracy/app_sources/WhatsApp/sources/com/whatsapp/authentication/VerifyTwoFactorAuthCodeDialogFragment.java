package com.whatsapp.authentication;

import X.AbstractC14440lR;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass1e1;
import X.AnonymousClass23M;
import X.AnonymousClass2a3;
import X.AnonymousClass2eq;
import X.AnonymousClass3U0;
import X.AnonymousClass3U4;
import X.C004902f;
import X.C14900mE;
import X.C14920mG;
import X.C52162aM;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes2.dex */
public class VerifyTwoFactorAuthCodeDialogFragment extends Hilt_VerifyTwoFactorAuthCodeDialogFragment implements AnonymousClass1e1 {
    public int A00 = 0;
    public ProgressBar A01;
    public TextView A02;
    public CodeInputField A03;
    public C14900mE A04;
    public AnonymousClass01d A05;
    public AnonymousClass018 A06;
    public C14920mG A07;
    public AbstractC14440lR A08;
    public final Handler A09 = new AnonymousClass2a3(Looper.getMainLooper(), this);
    public final Runnable A0A = new RunnableBRunnable0Shape2S0100000_I0_2(this, 2);

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        List list = this.A07.A0B;
        AnonymousClass009.A0F(list.contains(this));
        list.remove(this);
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        List list = this.A07.A0B;
        AnonymousClass009.A0F(!list.contains(this));
        list.add(this);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog dialog = new Dialog(A0B());
        dialog.requestWindowFeature(1);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.setContentView(R.layout.fragment_two_factor_auth_nag);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) dialog.findViewById(R.id.nag_text);
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, this.A05));
        textEmojiLabel.setText(AnonymousClass23M.A08(new RunnableBRunnable0Shape2S0100000_I0_2(this, 3), A0I(R.string.two_factor_auth_code_nag_explanation), "forgot-pin"));
        this.A02 = (TextView) dialog.findViewById(R.id.error);
        this.A03 = (CodeInputField) dialog.findViewById(R.id.code);
        String A0J = A0J(R.string.accessibility_two_factor_auth_code_entry, 6);
        CodeInputField codeInputField = this.A03;
        codeInputField.A08(new AnonymousClass3U0(this), new AnonymousClass3U4(codeInputField.getContext()), null, A0J, '*', '*', 6);
        this.A03.setPasswordTransformationEnabled(true);
        this.A01 = (ProgressBar) dialog.findViewById(R.id.progress_bar_code_input_blocked);
        this.A03.setEnabled(true);
        this.A01.setProgress(100);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.4hU
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment = VerifyTwoFactorAuthCodeDialogFragment.this;
                verifyTwoFactorAuthCodeDialogFragment.A04.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(verifyTwoFactorAuthCodeDialogFragment, 5));
            }
        });
        dialog.getWindow().addFlags(DefaultCrypto.BUFFER_SIZE);
        return dialog;
    }

    public void A1K() {
        this.A00 = 1;
        this.A04.A06(0, R.string.two_factor_auth_disabling);
        this.A04.A0J(this.A0A, 5000);
        C14920mG r2 = this.A07;
        Log.i("twofactorauthmanager/disable-two-factor-auth");
        r2.A03("", null);
    }

    public final void A1L() {
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            C004902f r1 = new C004902f(A0B.A0V());
            r1.A05(this);
            r1.A07 = 8194;
            r1.A02();
        }
    }

    @Override // X.AnonymousClass1e1
    public void AXu() {
        if (this.A00 == 1) {
            this.A00 = 0;
            this.A04.A0G(this.A0A);
            this.A04.A0J(new RunnableBRunnable0Shape2S0100000_I0_2(this, 4), 500);
        }
    }

    @Override // X.AnonymousClass1e1
    public void AXv() {
        if (this.A00 == 1) {
            this.A00 = 3;
            this.A04.A0G(this.A0A);
            this.A04.A0J(new RunnableBRunnable0Shape2S0100000_I0_2(this, 6), 500);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        ActivityC000900k A0B;
        super.onDismiss(dialogInterface);
        int i = this.A00;
        if (i != 2 && i != 4 && (A0B = A0B()) != null) {
            A0B.finish();
        }
    }
}
