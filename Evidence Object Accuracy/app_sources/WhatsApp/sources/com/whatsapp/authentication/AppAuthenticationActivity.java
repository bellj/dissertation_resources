package com.whatsapp.authentication;

import X.AbstractActivityC461624u;
import X.AbstractC13880kU;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass01V;
import X.AnonymousClass02N;
import X.AnonymousClass21K;
import X.C05000Nw;
import X.C05130Oj;
import X.C05500Pu;
import X.C21290xB;
import X.C22670zS;
import X.C26061Bw;
import X.C53382eB;
import X.C83833xy;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.security.Signature;

/* loaded from: classes2.dex */
public class AppAuthenticationActivity extends AbstractActivityC461624u implements AnonymousClass21K, AbstractC13880kU {
    public int A00;
    public int A01 = 1;
    public C05000Nw A02;
    public C05500Pu A03;
    public AnonymousClass02N A04;
    public C21290xB A05;
    public C22670zS A06;
    public FingerprintView A07;
    public Runnable A08;
    public boolean A09;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public static /* synthetic */ void A03(AppAuthenticationActivity appAuthenticationActivity) {
        appAuthenticationActivity.A01 = 2;
        appAuthenticationActivity.A03.A03(appAuthenticationActivity.A02);
    }

    public final void A2T() {
        if (this.A00 != 0) {
            this.A05.A01();
            Intent intent = new Intent();
            intent.putExtra("appWidgetId", this.A00);
            setResult(-1, intent);
            return;
        }
        setResult(-1);
    }

    public final void A2U() {
        Log.i("AuthenticationActivity/start-listening");
        this.A07.removeCallbacks(this.A08);
        AnonymousClass02N r2 = new AnonymousClass02N();
        this.A04 = r2;
        C22670zS r1 = this.A06;
        AnonymousClass009.A0F(r1.A04());
        r1.A01.A6I(r2, this);
        FingerprintView fingerprintView = this.A07;
        fingerprintView.A02(fingerprintView.A06);
    }

    @Override // X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        Log.i("AppAuthenticationActivity/fingerprint-error");
        this.A06.A01(true);
        if (i == 7) {
            Log.i("AppAuthenticationActivity/fingerprint-error-too-many-attempts");
            charSequence = getString(R.string.fingerprint_lockout_error, 30);
            this.A07.removeCallbacks(this.A08);
            this.A07.postDelayed(this.A08, C26061Bw.A0L);
        }
        this.A07.A03(charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        Log.i("AppAuthenticationActivity/fingerprint-failed");
        FingerprintView fingerprintView = this.A07;
        fingerprintView.A04(fingerprintView.getContext().getString(R.string.fingerprint_not_recognized));
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        Log.i("AppAuthenticationActivity/fingerprint-help");
        this.A07.A04(charSequence.toString());
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        Log.i("AppAuthenticationActivity/fingerprint-success");
        this.A06.A01(false);
        this.A07.A01();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        ActivityManager A03 = ((ActivityC13810kN) this).A08.A03();
        if (A03 == null || A03.getLockTaskModeState() != 2) {
            setResult(0);
            finishAffinity();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.A00 = extras.getInt("appWidgetId", 0);
        }
        if (!this.A06.A03()) {
            Log.i("AppAuthenticationActivity/onCreate: setting not enabled");
            A2T();
            finish();
            overridePendingTransition(0, 17432577);
            return;
        }
        this.A09 = this.A06.A04.A07(266);
        setContentView(R.layout.activity_authentication);
        ((TextView) findViewById(R.id.auth_title)).setText(R.string.app_auth_locked_title);
        View findViewById = findViewById(R.id.app_unlock);
        this.A07 = (FingerprintView) findViewById(R.id.fingerprint_view);
        if (this.A09) {
            findViewById.setVisibility(0);
            this.A07.setVisibility(8);
            this.A03 = new C05500Pu(new C53382eB(this), this, AnonymousClass00T.A07(this));
            C05130Oj r1 = new C05130Oj();
            r1.A03 = getString(R.string.app_locked_biometric_prompt_title);
            r1.A05 = true;
            r1.A04 = false;
            this.A02 = r1.A00();
            findViewById.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 18));
            return;
        }
        findViewById.setVisibility(8);
        this.A07.setVisibility(0);
        this.A07.A00 = new C83833xy(this);
        this.A08 = new RunnableBRunnable0Shape2S0100000_I0_2(this, 0);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        FingerprintView fingerprintView = this.A07;
        if (fingerprintView != null) {
            fingerprintView.A00 = null;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:19:0x0017 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: X.02N */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1, types: [com.whatsapp.authentication.FingerprintView, android.view.View] */
    /* JADX WARN: Type inference failed for: r1v3, types: [X.02N] */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (!this.A09) {
            Log.i("AuthenticationActivity/stop-listening");
            AnonymousClass02N r1 = this.A07;
            r1.removeCallbacks(this.A08);
            AnonymousClass02N r0 = this.A04;
            if (r0 != null) {
                try {
                    r1 = 0;
                    r1 = 0;
                    r1 = 0;
                    try {
                        r0.A01();
                    } catch (NullPointerException e) {
                        e.getMessage();
                    }
                } finally {
                    this.A04 = r1;
                }
            }
        } else if (this.A01 == 3) {
            this.A01 = 1;
            this.A03.A00();
        }
    }

    @Override // X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (!this.A09) {
            A2U();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (!this.A06.A02()) {
            Log.i("AppAuthenticationActivity/not-enrolled");
            setResult(-1);
            finish();
        } else if (this.A09 && this.A01 == 1) {
            this.A01 = 2;
            this.A03.A03(this.A02);
        }
    }
}
