package com.whatsapp.authentication;

import X.C004802e;
import X.C12960it;
import X.C22670zS;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SetupDeviceAuthDialog extends Hilt_SetupDeviceAuthDialog {
    public C22670zS A00;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        boolean A07 = this.A00.A04.A07(266);
        C004802e A0K = C12960it.A0K(this);
        int i = R.string.fingerprint_setup_dialog_title;
        if (A07) {
            i = R.string.app_auth_setup_dialog_title;
        }
        A0K.setTitle(A0I(i));
        int i2 = R.string.fingerprint_setup_dialog_message;
        if (A07) {
            i2 = R.string.app_auth_setup_dialog_message;
        }
        A0K.A0A(A0I(i2));
        A0K.A03(null, A0I(R.string.ok));
        return A0K.create();
    }
}
