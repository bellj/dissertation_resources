package com.whatsapp.authentication;

import X.AbstractC58672rA;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass02N;
import X.AnonymousClass21K;
import X.AnonymousClass23N;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14830m7;
import X.C27531Hw;
import X.C80623sb;
import X.CountDownTimerC51972Zw;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import java.security.Signature;

/* loaded from: classes2.dex */
public class FingerprintBottomSheet extends Hilt_FingerprintBottomSheet implements AnonymousClass21K {
    public long A00 = 0;
    public CountDownTimer A01;
    public TextView A02;
    public TextView A03;
    public AnonymousClass02N A04;
    public AbstractC58672rA A05;
    public FingerprintView A06;
    public AnonymousClass01d A07;
    public C14830m7 A08;
    public AnonymousClass018 A09;
    public boolean A0A = false;

    public static FingerprintBottomSheet A00(int i, int i2, int i3, int i4) {
        return A01(i, i2, i3, i4, 0, R.style.FingerprintView);
    }

    public static FingerprintBottomSheet A01(int i, int i2, int i3, int i4, int i5, int i6) {
        FingerprintBottomSheet fingerprintBottomSheet = new FingerprintBottomSheet();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("title", i);
        A0D.putInt("negative_button_text", i2);
        A0D.putInt("positive_button_text", i3);
        if (i4 != 0) {
            A0D.putInt("header_layout_id", i4);
        }
        if (i5 != 0) {
            A0D.putInt("custom_layout_id", i5);
        }
        A0D.putInt("fingerprint_view_style_id", i6);
        A0D.putBoolean("full_screen", false);
        fingerprintBottomSheet.A0U(A0D);
        return fingerprintBottomSheet;
    }

    public static /* synthetic */ void A02(DialogInterface dialogInterface, Bundle bundle, FingerprintBottomSheet fingerprintBottomSheet) {
        View findViewById = ((Dialog) dialogInterface).findViewById(R.id.design_bottom_sheet);
        AnonymousClass009.A03(findViewById);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(findViewById);
        if (bundle.getBoolean("full_screen")) {
            ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
            layoutParams.height = Resources.getSystem().getDisplayMetrics().heightPixels - C27531Hw.A02(fingerprintBottomSheet.A0p(), fingerprintBottomSheet.A07.A0O());
            findViewById.setLayoutParams(layoutParams);
        }
        A00.A0M(3);
        A00.A0E = new C80623sb(fingerprintBottomSheet);
    }

    public static /* synthetic */ void A03(FingerprintBottomSheet fingerprintBottomSheet) {
        fingerprintBottomSheet.A1C();
        AbstractC58672rA r0 = fingerprintBottomSheet.A05;
        if (r0 != null) {
            r0.A02();
        }
    }

    public static /* synthetic */ void A04(FingerprintBottomSheet fingerprintBottomSheet) {
        fingerprintBottomSheet.A1C();
        AbstractC58672rA r0 = fingerprintBottomSheet.A05;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A05 = null;
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        A1K();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Bundle A03 = A03();
        int i = A03.getInt("custom_layout_id");
        if (i == 0) {
            i = R.layout.fingerprint_bottom_sheet;
        }
        View inflate = layoutInflater.inflate(i, viewGroup, false);
        int i2 = A03.getInt("header_layout_id");
        if (i2 != 0) {
            ViewGroup A0P = C12980iv.A0P(inflate, R.id.header_container);
            layoutInflater.inflate(i2, A0P);
            A0P.setVisibility(0);
        }
        ViewGroup A0P2 = C12980iv.A0P(inflate, R.id.fingerprint_view_wrapper);
        if (A0P2 != null) {
            FingerprintView fingerprintView = new FingerprintView(inflate.getContext(), null, 0, A03.getInt("fingerprint_view_style_id"));
            this.A06 = fingerprintView;
            A0P2.addView(fingerprintView);
        } else {
            this.A06 = (FingerprintView) inflate.findViewById(R.id.fingerprint_view);
        }
        C12990iw.A1H(C12960it.A0J(inflate, R.id.fingerprint_bottomsheet_title), this, A03.getInt("title", R.string.fingerprint_bottom_sheet_title));
        if (A03.getInt("positive_button_text") != 0) {
            String A0I = A0I(A03.getInt("positive_button_text"));
            TextView A0J = C12960it.A0J(inflate, R.id.fingerprint_bottomsheet_positive_button);
            this.A03 = A0J;
            A0J.setText(A0I);
            C12960it.A10(this.A03, this, 17);
        }
        if (A03.getInt("negative_button_text") != 0) {
            String A0I2 = A0I(A03.getInt("negative_button_text"));
            TextView A0J2 = C12960it.A0J(inflate, R.id.fingerprint_bottomsheet_negative_button);
            this.A02 = A0J2;
            AnonymousClass23N.A01(A0J2);
            this.A02.setText(A0I2);
            C12960it.A10(this.A02, this, 18);
        }
        this.A06.A00 = this.A05;
        Window window = ((DialogFragment) this).A03.getWindow();
        AnonymousClass009.A05(window);
        if (Build.VERSION.SDK_INT >= 21) {
            window.getDecorView().setSystemUiVisibility(1280);
            window.setStatusBarColor(0);
        }
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = -1;
        attributes.gravity = 48;
        window.setAttributes(attributes);
        ((DialogFragment) this).A03.setOnShowListener(new DialogInterface.OnShowListener(A03, this) { // from class: X.4ha
            public final /* synthetic */ Bundle A00;
            public final /* synthetic */ FingerprintBottomSheet A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                FingerprintBottomSheet.A02(dialogInterface, this.A00, this.A01);
            }
        });
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        FingerprintView fingerprintView = this.A06;
        if (fingerprintView != null) {
            fingerprintView.A00 = null;
            this.A06 = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (this.A00 <= this.A08.A00() && !this.A0A) {
            FingerprintView fingerprintView = this.A06;
            if (fingerprintView != null) {
                fingerprintView.A02(fingerprintView.A06);
            }
            A1J();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, 2131952347);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1B() {
        A1K();
        super.A1B();
    }

    public void A1J() {
        AnonymousClass02N r1 = new AnonymousClass02N();
        this.A04 = r1;
        AbstractC58672rA r0 = this.A05;
        if (r0 != null) {
            r0.A04(r1, this);
        }
    }

    public final void A1K() {
        AnonymousClass02N r0 = this.A04;
        if (r0 != null) {
            r0.A01();
            this.A04 = null;
        }
    }

    public void A1L(long j) {
        CountDownTimer countDownTimer = this.A01;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A01 = null;
        }
        if (j > this.A08.A00()) {
            this.A00 = j;
            A1K();
            this.A01 = new CountDownTimerC51972Zw(this, j - this.A08.A00(), j).start();
        }
    }

    public void A1M(AbstractC58672rA r1) {
        this.A05 = r1;
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        AbstractC58672rA r0 = this.A05;
        if (r0 != null) {
            r0.A03(i);
        }
        if (this.A06 != null) {
            if (i == 7) {
                Object[] A1b = C12970iu.A1b();
                C12960it.A1P(A1b, 30, 0);
                charSequence = A0J(R.string.app_auth_lockout_error_short, A1b);
            }
            this.A06.A03(charSequence);
        }
        A1K();
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        FingerprintView fingerprintView = this.A06;
        if (fingerprintView != null) {
            fingerprintView.A04(fingerprintView.getContext().getString(R.string.fingerprint_not_recognized));
        }
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        FingerprintView fingerprintView = this.A06;
        if (fingerprintView != null) {
            fingerprintView.A04(charSequence.toString());
        }
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        AbstractC58672rA r0 = this.A05;
        if (r0 != null) {
            r0.A06(bArr);
        }
        FingerprintView fingerprintView = this.A06;
        if (fingerprintView != null) {
            fingerprintView.A01();
        }
    }

    @Override // X.AnonymousClass21K
    public void AMg(Signature signature) {
        AbstractC58672rA r0 = this.A05;
        if (r0 != null) {
            r0.A05(signature);
        }
        FingerprintView fingerprintView = this.A06;
        if (fingerprintView != null) {
            fingerprintView.A01();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        A1K();
        super.onCancel(dialogInterface);
    }
}
