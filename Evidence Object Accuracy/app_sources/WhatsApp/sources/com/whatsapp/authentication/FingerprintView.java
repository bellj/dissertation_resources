package com.whatsapp.authentication;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass07M;
import X.AnonymousClass2P7;
import X.AnonymousClass4UT;
import X.C75773kR;
import X.C75783kS;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class FingerprintView extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass4UT A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final ImageView A03;
    public final TextView A04;
    public final AnonymousClass07M A05;
    public final AnonymousClass07M A06;
    public final AnonymousClass07M A07;
    public final AnonymousClass07M A08;
    public final Runnable A09;

    public FingerprintView(Context context) {
        this(context, null, 0, R.style.FingerprintView);
    }

    public FingerprintView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, R.style.FingerprintView);
    }

    public FingerprintView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, R.style.FingerprintView);
    }

    public FingerprintView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i2);
        setOrientation(1);
        LayoutInflater A01 = AnonymousClass01d.A01(context);
        AnonymousClass009.A05(A01);
        A01.inflate(R.layout.fingerprint_view, (ViewGroup) this, true);
        this.A04 = (TextView) findViewById(R.id.fingerprint_prompt);
        ImageView imageView = (ImageView) findViewById(R.id.fingerprint_icon);
        this.A03 = imageView;
        AnonymousClass07M A04 = AnonymousClass07M.A04(contextThemeWrapper, R.drawable.fingerprint_icon);
        AnonymousClass009.A05(A04);
        this.A06 = A04;
        imageView.setImageDrawable(A04);
        A04.start();
        AnonymousClass07M A042 = AnonymousClass07M.A04(contextThemeWrapper, R.drawable.fingerprint_icon_to_success);
        AnonymousClass009.A05(A042);
        this.A08 = A042;
        AnonymousClass07M A043 = AnonymousClass07M.A04(contextThemeWrapper, R.drawable.fingerprint_icon_to_error);
        AnonymousClass009.A05(A043);
        this.A07 = A043;
        AnonymousClass07M A044 = AnonymousClass07M.A04(contextThemeWrapper, R.drawable.error_to_fingerprint_icon);
        AnonymousClass009.A05(A044);
        this.A05 = A044;
        this.A09 = new RunnableBRunnable0Shape2S0100000_I0_2(this, 1);
    }

    public void A01() {
        this.A04.setText("");
        ImageView imageView = this.A03;
        imageView.removeCallbacks(this.A09);
        AnonymousClass07M r1 = this.A08;
        imageView.setImageDrawable(r1);
        r1.start();
        r1.A08(new C75783kS(this));
    }

    public final void A02(AnonymousClass07M r5) {
        String string = getContext().getString(R.string.fingerprint_locked_fingerprint_prompt);
        if (getContext() != null) {
            TextView textView = this.A04;
            textView.setText(string);
            textView.setTextColor(AnonymousClass00T.A00(getContext(), R.color.settings_item_subtitle_text));
            textView.announceForAccessibility(string);
        }
        this.A03.setImageDrawable(r5);
        r5.start();
    }

    public void A03(CharSequence charSequence) {
        setError(charSequence.toString());
        ImageView imageView = this.A03;
        imageView.removeCallbacks(this.A09);
        Drawable drawable = imageView.getDrawable();
        AnonymousClass07M r1 = this.A07;
        if (!drawable.equals(r1)) {
            imageView.setImageDrawable(r1);
            r1.start();
            r1.A08(new C75773kR(this));
        }
    }

    public void A04(String str) {
        setError(str);
        ImageView imageView = this.A03;
        Drawable drawable = imageView.getDrawable();
        AnonymousClass07M r1 = this.A07;
        if (!drawable.equals(r1)) {
            imageView.setImageDrawable(r1);
            r1.start();
        }
        Runnable runnable = this.A09;
        imageView.removeCallbacks(runnable);
        imageView.postDelayed(runnable, 1000);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    private void setError(String str) {
        if (getContext() != null) {
            TextView textView = this.A04;
            textView.setText(str);
            textView.setTextColor(AnonymousClass00T.A00(getContext(), R.color.settings_item_subtitle_text));
            textView.announceForAccessibility(str);
        }
    }

    public void setListener(AnonymousClass4UT r1) {
        this.A00 = r1;
    }
}
