package com.whatsapp.authentication;

import X.AbstractC58672rA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C05000Nw;
import X.C05130Oj;
import X.C05500Pu;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C18360sK;
import X.C20220vP;
import X.C21290xB;
import X.C53372eA;
import X.C58662r9;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;
import com.whatsapp.authentication.AppAuthSettingsActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class AppAuthSettingsActivity extends ActivityC13790kL {
    public View A00;
    public View A01;
    public RadioButton A02;
    public RadioButton A03;
    public RadioButton A04;
    public SwitchCompat A05;
    public SwitchCompat A06;
    public C05000Nw A07;
    public C05500Pu A08;
    public C21290xB A09;
    public FingerprintBottomSheet A0A;
    public C18360sK A0B;
    public C20220vP A0C;
    public boolean A0D;
    public final AbstractC58672rA A0E;

    public AppAuthSettingsActivity() {
        this(0);
        this.A0E = new C58662r9(this);
    }

    public AppAuthSettingsActivity(int i) {
        this.A0D = false;
        ActivityC13830kP.A1P(this, 12);
    }

    public static /* synthetic */ void A02(AppAuthSettingsActivity appAuthSettingsActivity) {
        boolean z = !appAuthSettingsActivity.A06.isChecked();
        C12960it.A0t(C12960it.A08(((ActivityC13810kN) appAuthSettingsActivity).A09), "privacy_fingerprint_show_notification_content", z);
        appAuthSettingsActivity.A06.setChecked(z);
        appAuthSettingsActivity.A0B.A04(1, null);
        appAuthSettingsActivity.A0C.A07();
        appAuthSettingsActivity.A09.A01();
    }

    public static /* synthetic */ void A03(AppAuthSettingsActivity appAuthSettingsActivity) {
        if (!(!appAuthSettingsActivity.A05.isChecked())) {
            appAuthSettingsActivity.A2e();
        } else if (((ActivityC13790kL) appAuthSettingsActivity).A03.A02()) {
            Log.i("AppAuthSettingsActivity/show-bottom-sheet");
            if (((ActivityC13790kL) appAuthSettingsActivity).A03.A04.A07(266)) {
                appAuthSettingsActivity.A08.A03(appAuthSettingsActivity.A07);
                return;
            }
            FingerprintBottomSheet A00 = FingerprintBottomSheet.A00(R.string.fingerprint_bottom_sheet_title, R.string.fingerprint_bottom_sheet_negative_button, 0, 0);
            appAuthSettingsActivity.A0A = A00;
            A00.A05 = appAuthSettingsActivity.A0E;
            appAuthSettingsActivity.Adm(A00);
        } else {
            Log.i("AppAuthSettingsActivity/setup");
            appAuthSettingsActivity.Adm(new SetupDeviceAuthDialog());
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0D) {
            this.A0D = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A09 = (C21290xB) A1M.ANf.get();
            this.A0C = (C20220vP) A1M.AC3.get();
            this.A0B = (C18360sK) A1M.AN0.get();
        }
    }

    public final void A2e() {
        Log.i("AppAuthSettingsActivity/disable-setting");
        ((ActivityC13790kL) this).A03.A01(true);
        ((ActivityC13810kN) this).A09.A17(false);
        this.A0C.A07();
        A2f(false);
        this.A05.setChecked(false);
        this.A09.A01();
        ((ActivityC13790kL) this).A03.A00(this);
    }

    public final void A2f(boolean z) {
        Log.i("AppAuthSettingsActivity/update-dependent-views");
        int i = 0;
        this.A01.setVisibility(C12960it.A02(z ? 1 : 0));
        View view = this.A00;
        if (!z) {
            i = 8;
        }
        view.setVisibility(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.app_auth_settings);
        C12970iu.A0N(this).A0M(true);
        TextView A0M = C12970iu.A0M(this, R.id.security_settings_title);
        TextView A0M2 = C12970iu.A0M(this, R.id.security_settings_desc);
        if (((ActivityC13790kL) this).A03.A04.A07(266)) {
            setTitle(R.string.settings_privacy_security_section_biometric_title);
            A0M.setText(R.string.settings_privacy_biometric);
            A0M2.setText(R.string.settings_privacy_biometric_message);
            this.A08 = new C05500Pu(new C53372eA(this), this, AnonymousClass00T.A07(this));
            C05130Oj r1 = new C05130Oj();
            r1.A01 = getString(R.string.biometric_prompt_negative_button);
            r1.A03 = getString(R.string.biometric_prompt_title);
            r1.A05 = false;
            r1.A04 = false;
            this.A07 = r1.A00();
        } else {
            setTitle(R.string.settings_privacy_security_section_title);
            A0M.setText(R.string.settings_privacy_fingerprint);
            A0M2.setText(R.string.settings_privacy_fingerprint_message);
            if (bundle != null) {
                FingerprintBottomSheet fingerprintBottomSheet = (FingerprintBottomSheet) A0V().A0A(FingerprintBottomSheet.class.getName());
                this.A0A = fingerprintBottomSheet;
                if (fingerprintBottomSheet != null) {
                    fingerprintBottomSheet.A05 = this.A0E;
                }
            }
        }
        this.A01 = findViewById(R.id.timeout);
        this.A05 = (SwitchCompat) findViewById(R.id.app_auth_settings_switch);
        this.A00 = findViewById(R.id.notification_preference);
        this.A06 = (SwitchCompat) findViewById(R.id.notification_content_switch);
        C12960it.A10(findViewById(R.id.app_auth_settings_preference), this, 16);
        C12960it.A10(this.A00, this, 15);
        this.A02 = (RadioButton) findViewById(R.id.timeout_immediately);
        this.A03 = (RadioButton) findViewById(R.id.timeout_one_min);
        this.A04 = (RadioButton) findViewById(R.id.timeout_thirty_min);
        this.A02.setText(R.string.app_auth_timeout_immediately);
        RadioButton radioButton = this.A03;
        AnonymousClass018 r3 = ((ActivityC13830kP) this).A01;
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, 1, 0);
        radioButton.setText(r3.A0I(objArr, R.plurals.app_auth_timeout_values, 1));
        RadioButton radioButton2 = this.A04;
        AnonymousClass018 r4 = ((ActivityC13830kP) this).A01;
        Object[] objArr2 = new Object[1];
        C12960it.A1P(objArr2, 30, 0);
        radioButton2.setText(r4.A0I(objArr2, R.plurals.app_auth_timeout_values, 30));
        this.A02.setOnClickListener(new View.OnClickListener(0) { // from class: X.3lY
            public final /* synthetic */ long A00;

            {
                this.A00 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C12970iu.A1C(C12960it.A08(((ActivityC13810kN) AppAuthSettingsActivity.this).A09), "privacy_fingerprint_timeout", this.A00);
            }
        });
        this.A03.setOnClickListener(new View.OnClickListener(60000) { // from class: X.3lY
            public final /* synthetic */ long A00;

            {
                this.A00 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C12970iu.A1C(C12960it.A08(((ActivityC13810kN) AppAuthSettingsActivity.this).A09), "privacy_fingerprint_timeout", this.A00);
            }
        });
        this.A04.setOnClickListener(new View.OnClickListener(1800000) { // from class: X.3lY
            public final /* synthetic */ long A00;

            {
                this.A00 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C12970iu.A1C(C12960it.A08(((ActivityC13810kN) AppAuthSettingsActivity.this).A09), "privacy_fingerprint_timeout", this.A00);
            }
        });
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C05500Pu r0 = this.A08;
        if (r0 != null) {
            r0.A00();
            this.A08 = null;
        }
        FingerprintBottomSheet fingerprintBottomSheet = this.A0A;
        if (fingerprintBottomSheet != null) {
            fingerprintBottomSheet.A05 = null;
            this.A0A = null;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        Log.i("AppAuthSettingsActivity/update-ui");
        boolean A1W = C12980iv.A1W(((ActivityC13810kN) this).A09.A00, "privacy_fingerprint_enabled");
        long j = ((ActivityC13810kN) this).A09.A00.getLong("privacy_fingerprint_timeout", 60000);
        boolean z = ((ActivityC13810kN) this).A09.A00.getBoolean("privacy_fingerprint_show_notification_content", true);
        A2f(A1W);
        StringBuilder A0k = C12960it.A0k("AppAuthSettingsActivity/update-timeout: ");
        A0k.append(j);
        C12960it.A1F(A0k);
        boolean z2 = true;
        this.A02.setChecked(C12960it.A1T((j > 0 ? 1 : (j == 0 ? 0 : -1))));
        this.A03.setChecked(C12960it.A1T((j > 60000 ? 1 : (j == 60000 ? 0 : -1))));
        RadioButton radioButton = this.A04;
        if (j != 1800000) {
            z2 = false;
        }
        radioButton.setChecked(z2);
        this.A05.setChecked(A1W);
        this.A06.setChecked(z);
    }
}
