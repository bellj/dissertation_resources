package com.whatsapp;

import X.C004802e;
import X.C12970iu;
import X.C14950mJ;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;

/* loaded from: classes3.dex */
public class SimpleExternalStorageStateCallback$SDCardUnavailableDialogFragment extends Hilt_SimpleExternalStorageStateCallback_SDCardUnavailableDialogFragment {
    public C14950mJ A00;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        boolean A00 = C14950mJ.A00();
        int i = R.string.record_need_sd_card_title_shared_storage;
        if (A00) {
            i = R.string.record_need_sd_card_title;
        }
        A0O.A07(i);
        int i2 = R.string.record_need_sd_card_message_shared_storage;
        if (A00) {
            i2 = R.string.record_need_sd_card_message;
        }
        A0O.A06(i2);
        A0O.setPositiveButton(R.string.ok, new IDxCListenerShape4S0000000_2_I1(2));
        return A0O.create();
    }
}
