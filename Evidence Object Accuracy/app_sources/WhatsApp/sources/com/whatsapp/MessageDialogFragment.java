package com.whatsapp;

import X.AbstractC36671kL;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass0OC;
import X.AnonymousClass19M;
import X.AnonymousClass2AC;
import X.C004802e;
import X.C004902f;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.MediaViewFragment;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class MessageDialogFragment extends Hilt_MessageDialogFragment {
    public DialogInterface.OnClickListener A00;
    public DialogInterface.OnClickListener A01;
    public AnonymousClass018 A02;
    public AnonymousClass19M A03;

    public static AnonymousClass2AC A00(String str) {
        AnonymousClass2AC r0 = new AnonymousClass2AC();
        r0.A08 = str;
        return r0;
    }

    public static AnonymousClass2AC A01(Object[] objArr, int i) {
        AnonymousClass2AC r0 = new AnonymousClass2AC();
        r0.A01 = i;
        r0.A0A = objArr;
        return r0;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        DialogInterface.OnClickListener onClickListener;
        DialogInterface.OnClickListener onClickListener2;
        C004802e r4 = new C004802e(A0C());
        r4.A0B(true);
        r4.setTitle(A1K("title", "title_id", "title_params_values", "title_params_types"));
        int i = A03().getInt("message_view_id");
        if (i != 0) {
            AnonymousClass0OC r1 = r4.A01;
            r1.A0C = null;
            r1.A01 = i;
        } else {
            r4.A0A(AbstractC36671kL.A03(A0p(), null, this.A03, A1K("message", "message_id", "message_params_values", "message_params_types")));
        }
        int i2 = A03().getInt("primary_action_text_id");
        if (i2 == 0 || (onClickListener = this.A00) == null) {
            r4.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fM
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    MessageDialogFragment.this.A1C();
                }
            });
        } else {
            r4.setPositiveButton(i2, onClickListener);
            int i3 = A03().getInt("secondary_action_text_id");
            if (!(i3 == 0 || (onClickListener2 = this.A01) == null)) {
                r4.setNegativeButton(i3, onClickListener2);
            }
        }
        return r4.create();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }

    public final String A1K(String str, String str2, String str3, String str4) {
        String string = A03().getString(str);
        if (string != null) {
            return string;
        }
        int i = ((AnonymousClass01E) this).A05.getInt(str2);
        if (i == 0) {
            return null;
        }
        ArrayList<String> stringArrayList = ((AnonymousClass01E) this).A05.getStringArrayList(str3);
        if (stringArrayList == null) {
            return this.A02.A09(i);
        }
        ArrayList<Integer> integerArrayList = ((AnonymousClass01E) this).A05.getIntegerArrayList(str4);
        if (integerArrayList == null || integerArrayList.size() != stringArrayList.size()) {
            throw new IllegalArgumentException();
        }
        Object[] objArr = new Object[stringArrayList.size()];
        for (int i2 = 0; i2 < stringArrayList.size(); i2++) {
            int intValue = integerArrayList.get(i2).intValue();
            String str5 = stringArrayList.get(i2);
            if (intValue == 1) {
                objArr[i2] = Long.valueOf(Long.parseLong(str5));
            } else {
                objArr[i2] = str5;
            }
        }
        return this.A02.A0B(i, objArr);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AnonymousClass01E r3 = ((AnonymousClass01E) this).A0D;
        if (r3 != null && (r3 instanceof MediaViewFragment)) {
            MediaViewBaseFragment mediaViewBaseFragment = (MediaViewBaseFragment) r3;
            if (A03().getInt("id") == 101) {
                mediaViewBaseFragment.A1E();
                return;
            }
        }
        ActivityC000900k A0B = A0B();
        if (A0B instanceof ActivityC13810kN) {
            ((ActivityC13810kN) A0B).A2A(A03().getInt("id"));
        }
    }
}
