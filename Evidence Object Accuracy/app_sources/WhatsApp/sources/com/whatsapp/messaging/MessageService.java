package com.whatsapp.messaging;

import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass5B7;
import X.C20640w5;
import X.C231210l;
import X.C58182oH;
import X.C71083cM;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class MessageService extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public C20640w5 A00;
    public boolean A01;
    public final Object A02;
    public volatile C71083cM A03;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public MessageService() {
        this(0);
    }

    public MessageService(int i) {
        this.A02 = new Object();
        this.A01 = false;
    }

    public static void A00(Context context) {
        try {
            context.startService(new Intent(context, MessageService.class).setAction("com.whatsapp.messaging.MessageService.START"));
            Log.i("messageservice/startService success");
        } catch (Exception e) {
            if (Build.VERSION.SDK_INT >= 26) {
                e.getMessage();
                return;
            }
            throw e;
        }
    }

    public static void A01(Context context, C231210l r3) {
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                context.startService(new Intent(context, MessageService.class).setAction("com.whatsapp.messaging.MessageService.START"));
                Log.i("messageservice/startOnForeground success");
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder("messageservice/startOnForeground failed:");
                sb.append(e.getMessage());
                Log.e(sb.toString());
                r3.A00();
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    this.A03 = new C71083cM(this);
                }
            }
        }
        return this.A03.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        Log.i("messageservice/onCreate");
        if (!this.A01) {
            this.A01 = true;
            this.A00 = (C20640w5) ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01.AHk.get();
        }
        super.onCreate();
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.i("messageservice/onDestroy");
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (!C20640w5.A00() && intent != null) {
            intent.getAction();
        }
        return 1;
    }
}
