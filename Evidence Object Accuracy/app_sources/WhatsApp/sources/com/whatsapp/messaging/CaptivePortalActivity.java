package com.whatsapp.messaging;

import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.C12960it;
import X.C21840y4;

/* loaded from: classes2.dex */
public class CaptivePortalActivity extends ActivityC13830kP {
    public AnonymousClass01d A00;
    public C21840y4 A01;
    public boolean A02;

    public CaptivePortalActivity() {
        this(0);
    }

    public CaptivePortalActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 87);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            this.A00 = C12960it.A0Q(A1M);
            this.A01 = (C21840y4) A1M.ACr.get();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            super.onCreate(r10)
            r0 = 1
            r9.requestWindowFeature(r0)
            X.01d r2 = r9.A00
            android.net.wifi.WifiManager r6 = r2.A0E
            if (r6 != 0) goto L_0x00ba
            r1 = 0
            java.lang.String r0 = "wifi"
            java.lang.Object r6 = r2.A0R(r0, r1)
            android.net.wifi.WifiManager r6 = (android.net.wifi.WifiManager) r6
            r2.A0E = r6
            if (r6 != 0) goto L_0x00ba
            java.lang.String r0 = "captiveportalactivity/create wm=null"
            com.whatsapp.util.Log.w(r0)
            r3 = 0
        L_0x0021:
            X.02e r4 = X.C12980iv.A0S(r9)
            r7 = 0
            r4.A0B(r7)
            r0 = 2131889595(0x7f120dbb, float:1.9413858E38)
            r4.A07(r0)
            r2 = 2131890036(0x7f120f74, float:1.9414752E38)
            r1 = 41
            com.facebook.redex.IDxCListenerShape9S0100000_2_I1 r0 = new com.facebook.redex.IDxCListenerShape9S0100000_2_I1
            r0.<init>(r9, r1)
            r4.A00(r2, r0)
            r2 = 2131887782(0x7f1206a6, float:1.941018E38)
            r1 = 9
            com.facebook.redex.IDxCListenerShape4S0200000_2_I1 r0 = new com.facebook.redex.IDxCListenerShape4S0200000_2_I1
            r0.<init>(r6, r1, r9)
            r4.setNegativeButton(r2, r0)
            if (r3 == 0) goto L_0x00b3
            int r5 = r3.getNetworkId()
            java.lang.String r3 = r3.getSSID()
            if (r3 == 0) goto L_0x007e
            int r8 = r3.length()
            r0 = 2
            if (r8 < r0) goto L_0x007e
            java.lang.String r2 = "\""
            boolean r0 = r3.startsWith(r2)
            java.lang.String r1 = "'"
            if (r0 != 0) goto L_0x006c
            boolean r0 = r3.startsWith(r1)
            if (r0 == 0) goto L_0x007e
        L_0x006c:
            boolean r0 = r3.endsWith(r2)
            if (r0 != 0) goto L_0x0078
            boolean r0 = r3.endsWith(r1)
            if (r0 == 0) goto L_0x007e
        L_0x0078:
            r0 = 1
            int r8 = r8 - r0
            java.lang.String r3 = r3.substring(r0, r8)
        L_0x007e:
            java.lang.String r0 = "wifi network name is "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r3, r0)
            com.whatsapp.util.Log.i(r0)
            r1 = 2131893167(0x7f121baf, float:1.9421103E38)
            r2 = 1
            java.lang.Object[] r0 = new java.lang.Object[r2]
            java.lang.String r0 = X.C12960it.A0X(r9, r3, r0, r7, r1)
            r4.A0A(r0)
            r1 = 2131888326(0x7f1208c6, float:1.9411284E38)
            java.lang.Object[] r0 = new java.lang.Object[r2]
            java.lang.String r1 = X.C12960it.A0X(r9, r3, r0, r7, r1)
            X.3Ks r0 = new X.3Ks
            r0.<init>(r6, r9, r3, r5)
            r4.A03(r0, r1)
        L_0x00aa:
            java.lang.String r0 = "captive portal dialog created"
            com.whatsapp.util.Log.i(r0)
            r4.A05()
            return
        L_0x00b3:
            r0 = 2131893166(0x7f121bae, float:1.94211E38)
            r4.A06(r0)
            goto L_0x00aa
        L_0x00ba:
            android.net.wifi.WifiInfo r3 = r6.getConnectionInfo()
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.messaging.CaptivePortalActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A01.A00.removeMessages(1);
        this.A01.A00();
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A01.A00.sendEmptyMessageDelayed(1, 3000);
    }
}
