package com.whatsapp;

import X.AbstractActivityC28171Kz;
import X.AbstractActivityC461524s;
import X.AbstractC44141yJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01H;
import X.AnonymousClass2F9;
import X.C003401m;
import X.C004802e;
import X.C14960mK;
import X.C15570nT;
import X.C16490p7;
import X.C18990tO;
import X.C20640w5;
import X.C20690wA;
import X.C21740xu;
import X.C22690zU;
import X.C231010j;
import X.C237412v;
import X.C36021jC;
import X.C48212Fa;
import X.C623436t;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.Main;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;
import java.net.URISyntaxException;

/* loaded from: classes2.dex */
public class Main extends AbstractActivityC461524s {
    public Uri A00;
    public C22690zU A01;
    public C48212Fa A02;
    public C623436t A03;
    public C20640w5 A04;
    public C21740xu A05;
    public C20690wA A06;
    public C237412v A07;
    public C18990tO A08;
    public C231010j A09;
    public C16490p7 A0A;
    public WhatsAppLibLoader A0B;
    public AnonymousClass01H A0C;
    public boolean A0D;

    public final void A2h() {
        C48212Fa r0 = this.A02;
        if (r0 == null || r0.A00() != 1) {
            C48212Fa r2 = new C48212Fa(this);
            this.A02 = r2;
            ((ActivityC13830kP) this).A05.Ab5(r2, new Void[0]);
            return;
        }
        Log.i("main/show dialog sync");
        if (this.A0D) {
            C36021jC.A01(this, 104);
        }
    }

    public final void A2i() {
        if (!isFinishing()) {
            Intent intent = getIntent();
            if (intent != null && !"android.intent.action.MAIN".equals(intent.getAction()) && (intent.getFlags() & 67108864) != 0 && ((ActivityC13810kN) this).A09.A00.getInt("shortcut_version", 0) == 0) {
                Log.i("main/recreate_shortcut");
                String string = getString(R.string.app_name);
                Intent A04 = C14960mK.A04(this);
                A04.addFlags(268435456);
                A04.addFlags(67108864);
                Intent intent2 = new Intent();
                try {
                    intent2.putExtra("android.intent.extra.shortcut.INTENT", Intent.parseUri(A04.toUri(0), 0));
                } catch (URISyntaxException e) {
                    StringBuilder sb = new StringBuilder("registername/remove-shortcut cannot parse shortcut uri ");
                    sb.append(e.getMessage());
                    Log.e(sb.toString(), e);
                }
                intent2.putExtra("android.intent.extra.shortcut.NAME", string);
                intent2.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
                sendBroadcast(intent2);
                RegisterName.A02(this, getString(R.string.app_name));
                ((ActivityC13810kN) this).A09.A00.edit().putInt("shortcut_version", 1).apply();
            }
            if (this.A0D && !isFinishing()) {
                startActivity(C14960mK.A02(this));
                overridePendingTransition(0, 0);
            }
            finish();
        }
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Intent intent;
        C003401m.A01("Main/onCreate");
        try {
            ((ActivityC13830kP) this).A02.A09("Main");
            ((ActivityC13830kP) this).A02.A08("main_onCreate");
            super.onCreate(bundle);
            setTitle(R.string.launcher_app_name);
            if (!this.A0B.A03()) {
                Log.i("aborting due to native libraries missing");
                intent = new Intent();
                intent.setClassName(getPackageName(), "com.whatsapp.corruptinstallation.CorruptInstallationActivity");
            } else {
                if (C20640w5.A00()) {
                    Log.w("main/device-not-supported");
                    setTheme(2131952364);
                    Adm(new DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment());
                } else {
                    int A00 = ((ActivityC13790kL) this).A0B.A00();
                    C15570nT r0 = ((ActivityC13790kL) this).A01;
                    r0.A08();
                    Me me = r0.A00;
                    if (me == null && A00 == 0) {
                        if (!isFinishing()) {
                            boolean booleanExtra = getIntent().getBooleanExtra("show_registration_first_dlg", false);
                            Intent intent2 = new Intent();
                            intent2.setClassName(getPackageName(), "com.whatsapp.registration.EULA");
                            intent2.putExtra("show_registration_first_dlg", booleanExtra);
                            startActivity(intent2);
                            finishAffinity();
                        }
                    } else if (A00 != 6) {
                        ((ActivityC13790kL) this).A01.A08();
                        if (me != null) {
                            C16490p7 r02 = this.A0A;
                            r02.A04();
                            if (!r02.A01) {
                                AnonymousClass2F9 r03 = ((AbstractActivityC28171Kz) this).A01;
                                if (((AbstractC44141yJ) r03).A03.A03(r03.A06)) {
                                    int A04 = ((ActivityC13790kL) this).A07.A04();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("main/create/backupfilesfound ");
                                    sb.append(A04);
                                    Log.i(sb.toString());
                                    if (A04 > 0) {
                                        C36021jC.A01(this, 105);
                                    } else {
                                        A2g(false);
                                    }
                                }
                                ((ActivityC13830kP) this).A02.A0A("Main created");
                            }
                        }
                        this.A0D = true;
                        A2e();
                        ((ActivityC13830kP) this).A02.A0A("Main created");
                    } else if (!isFinishing()) {
                        intent = new Intent();
                        intent.setClassName(getPackageName(), "com.whatsapp.account.delete.DeleteAccountConfirmation");
                    }
                }
            }
            startActivity(intent);
            finish();
        } finally {
            ((ActivityC13830kP) this).A02.A07("main_onCreate");
            C003401m.A00();
        }
    }

    @Override // X.AbstractActivityC28171Kz, android.app.Activity
    public Dialog onCreateDialog(int i) {
        setTheme(2131952364);
        if (i != 0) {
            return super.onCreateDialog(i);
        }
        Log.i("main/dialog/upgrade");
        ((ActivityC13830kP) this).A02.A06("upgrade");
        C004802e r2 = new C004802e(this);
        r2.A07(R.string.upgrade_question);
        r2.A06(R.string.upgrade_message);
        r2.A0B(false);
        r2.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() { // from class: X.2FA
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                Main main = Main.this;
                main.startActivity(new Intent("android.intent.action.VIEW").setDataAndType(main.A00, "application/vnd.android.package-archive").setFlags(1));
                C36021jC.A00(main, 0);
                main.finish();
            }
        });
        r2.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() { // from class: X.2FB
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                Main main = Main.this;
                main.A05.A04();
                C36021jC.A00(main, 0);
                main.A2i();
            }
        });
        return r2.create();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0D = true;
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0D = false;
    }
}
