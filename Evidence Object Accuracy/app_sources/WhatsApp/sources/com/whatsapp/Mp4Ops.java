package com.whatsapp;

import X.AbstractC15710nm;
import X.AnonymousClass009;
import X.AnonymousClass152;
import X.AnonymousClass16A;
import X.C14350lI;
import X.C17050qB;
import X.C22200yh;
import X.C28411Nc;
import X.C39361pl;
import android.content.Context;
import android.os.Environment;
import android.os.SystemClock;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class Mp4Ops {
    public final C17050qB A00;

    public static native LibMp4OperationResult mp4check(String str, boolean z);

    public static native LibMp4CheckAndRepairResult mp4checkAndRepair(String str, String str2);

    public static native boolean mp4forensic(int i, String str, String str2);

    public static native LibMp4OperationResult mp4mux(String str, String str2, String str3, float f, float f2, float f3, float f4, int i, String str4, float f5);

    public static native LibMp4OperationResult mp4removeDolbyEAC3Track(String str, String str2);

    public static native LibMp4StreamCheckResult mp4streamcheck(String str, boolean z, long j);

    public static native LibMp4OperationResult removeAudioTracks(String str, String str2);

    public Mp4Ops(C17050qB r1) {
        this.A00 = r1;
    }

    public static void A00(Context context, AbstractC15710nm r8, File file, Exception exc, String str) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory().getPath());
        sb.append("/WhatsApp/Media/WhatsApp Video/video.fos");
        File file2 = new File(sb.toString());
        try {
            z = mp4forensic(400, file2.getAbsolutePath(), file.getAbsolutePath());
            StringBuilder sb2 = new StringBuilder("mp4ops/forensic ret=");
            sb2.append(z);
            Log.e(sb2.toString());
        } catch (Error e) {
            Log.e("videotranscodder/forensic fail/", e);
            z = false;
        }
        StringBuilder sb3 = new StringBuilder("mp4ops/forensic-upload/create result=");
        sb3.append(z);
        Log.i(sb3.toString());
        if (z) {
            try {
                String name = file2.getName();
                if (TextUtils.isEmpty(name)) {
                    name = "source";
                }
                File A04 = C14350lI.A04(file2, context.getFilesDir(), name);
                Log.e("Mp4Ops/uploadMp4FailureLogs", exc);
                HashMap hashMap = new HashMap();
                if (A04 != null) {
                    hashMap.put("attachment", A04.getPath());
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append("LibMp4Operations ");
                sb4.append(str);
                sb4.append(" failed (file): ");
                sb4.append(exc.getMessage());
                ((AnonymousClass16A) r8).A08(new C28411Nc("Mp4Ops/uploadMp4FailureLogs"), "Mp4Ops/uploadMp4FailureLogs", sb4.toString(), hashMap, true);
            } catch (IOException e2) {
                Log.e("mp4ops/forensic-upload/", e2);
            }
        }
        file2.delete();
    }

    public static void A01(C17050qB r6, File file) {
        try {
            if (AnonymousClass152.A04(file, false).A00 != 0) {
                File A01 = r6.A01(file);
                LibMp4OperationResult removeAudioTracks = removeAudioTracks(file.getAbsolutePath(), A01.getAbsolutePath());
                if (!removeAudioTracks.success) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("mp4ops/remove-audio-tracks");
                    sb.append(removeAudioTracks.errorMessage);
                    Log.e(sb.toString());
                    int i = removeAudioTracks.errorCode;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("invalid result, error_code: ");
                    sb2.append(i);
                    throw new C39361pl(i, sb2.toString());
                } else if (!C22200yh.A0S(r6, A01, file)) {
                    Log.e("mp4ops/remove-audio-tracks failed to apply tag properly.  Renaming marked file to original filepath unsuccessful");
                    throw new C39361pl(0, "mp4ops/remove-audio-tracks failed to apply tag properly.  Renaming marked file to original filepath unsuccessful");
                }
            }
        } catch (IOException e) {
            Log.e("Could not access file or failed to move files properly", e);
            throw new C39361pl(0, "Could not access file or failed to move files properly");
        }
    }

    public static void A02(File file, File file2) {
        try {
            Log.i("mp4ops/removeExifData/start");
            LibMp4OperationResult mp4mux = mp4mux(file.getAbsolutePath(), file.getAbsolutePath(), file2.getAbsolutePath(), -1.0f, 0.0f, -1.0f, -1.0f, -1, file.getAbsolutePath(), 0.0f);
            StringBuilder sb = new StringBuilder();
            sb.append("mp4ops/removeExifData/finished success=");
            sb.append(mp4mux.success);
            Log.i(sb.toString());
            if (mp4mux.success) {
                return;
            }
            if (mp4mux.ioException) {
                throw new IOException("mp4ops/removeExifData/No space");
            }
            int i = mp4mux.errorCode;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("mp4ops/removeExifData failed, error_code: ");
            sb2.append(i);
            throw new C39361pl(i, sb2.toString());
        } catch (Error e) {
            Log.e("mp4ops/removeExifData/failed: mp4mux error, exiting", e);
            throw new C39361pl(0, e.getMessage());
        }
    }

    public static void A03(File file, File file2, File file3, File file4, int i, long j, long j2, long j3, long j4) {
        String absolutePath;
        Log.i("mp4ops/mux/start");
        float f = ((float) j) / 1000.0f;
        float f2 = ((float) j4) / 1000.0f;
        float f3 = ((float) j3) / 1000.0f;
        float f4 = ((float) j2) / 1000.0f;
        String str = "";
        if (file2 == null) {
            absolutePath = str;
        } else {
            try {
                absolutePath = file2.getAbsolutePath();
            } catch (Error e) {
                Log.e("mp4ops/mux/failed: mp4mux error, exiting", e);
                throw new C39361pl(0, e.getMessage());
            }
        }
        if (file3 != null) {
            str = file3.getAbsolutePath();
        }
        LibMp4OperationResult mp4mux = mp4mux(absolutePath, str, file.getAbsolutePath(), f, 0.0f, f3, f4, i, file4.getAbsolutePath(), f2);
        StringBuilder sb = new StringBuilder("mp4ops/mux/result: ");
        sb.append(mp4mux.success);
        Log.i(sb.toString());
        if (!mp4mux.success) {
            StringBuilder sb2 = new StringBuilder("mp4ops/mux/error_message/");
            sb2.append(mp4mux.errorMessage);
            Log.e(sb2.toString());
            if (mp4mux.ioException) {
                throw new IOException("No space");
            }
            StringBuilder sb3 = new StringBuilder("invalid result, error_code: ");
            int i2 = mp4mux.errorCode;
            sb3.append(i2);
            throw new C39361pl(i2, sb3.toString());
        }
        StringBuilder sb4 = new StringBuilder("mp4ops/mux/finished, size:");
        sb4.append(file.length());
        Log.i(sb4.toString());
    }

    public static void A04(File file, boolean z) {
        LibMp4OperationResult mp4check;
        Log.i("mp4ops/check/start");
        int i = 0;
        do {
            try {
                mp4check = mp4check(file.getAbsolutePath(), z);
                if (mp4check.success || !mp4check.ioException) {
                    break;
                }
                SystemClock.sleep(100);
                i++;
            } catch (Error e) {
                Log.e("mp4ops/integration fail/", e);
                throw new C39361pl(0, "integrity check error");
            }
        } while (i < 5);
        AnonymousClass009.A05(mp4check);
        if (mp4check.success) {
            Log.i("mp4ops/check/finished");
            return;
        }
        StringBuilder sb = new StringBuilder("mp4ops/check/error_message/");
        sb.append(mp4check.errorMessage);
        Log.e(sb.toString());
        int i2 = mp4check.errorCode;
        StringBuilder sb2 = new StringBuilder("integrity check failed, error_code: ");
        sb2.append(i2);
        throw new C39361pl(i2, sb2.toString());
    }

    public boolean A05(File file) {
        Log.i("mp4ops/checkAndRepair/start");
        C17050qB r4 = this.A00;
        File A01 = r4.A01(file);
        StringBuilder sb = new StringBuilder("mp4ops/checkAndRepair/repairFileName.exists");
        sb.append(A01.exists());
        Log.i(sb.toString());
        try {
            LibMp4CheckAndRepairResult mp4checkAndRepair = mp4checkAndRepair(file.getAbsolutePath(), A01.getAbsolutePath());
            if (!mp4checkAndRepair.success) {
                if (mp4checkAndRepair.repaired) {
                    A01.delete();
                }
                StringBuilder sb2 = new StringBuilder("mp4ops/checkAndRepair/error_message/");
                sb2.append(mp4checkAndRepair.errorMessage);
                Log.e(sb2.toString());
                if (mp4checkAndRepair.ioException) {
                    throw new IOException("No space");
                }
                int i = mp4checkAndRepair.errorCode;
                StringBuilder sb3 = new StringBuilder("integrity check/repair failed, error_code: ");
                sb3.append(i);
                throw new C39361pl(i, sb3.toString());
            }
            Log.i("mp4ops/checkAndRepair/finished");
            if (mp4checkAndRepair.repaired) {
                StringBuilder sb4 = new StringBuilder("mp4ops/checkAndRepair/file_is_repaired, new file created and renamed: ");
                sb4.append(A01.getAbsolutePath());
                Log.i(sb4.toString());
                A02(A01, file);
                return true;
            }
            Log.i("mp4ops/checkAndRepair/file_repair_not_needed but will remove exif data");
            File A012 = r4.A01(file);
            A02(file, A012);
            if (file.length() == A012.length()) {
                return false;
            }
            if (A012.renameTo(file)) {
                return true;
            }
            Log.i("mp4ops/checkAndRepair/rename_failed");
            throw new IOException("unable to rename file");
        } catch (Error e) {
            Log.e("mp4ops/integration fail/", e);
            throw new C39361pl(0, "integrity check error");
        }
    }

    /* loaded from: classes2.dex */
    public class LibMp4OperationResult {
        public final int errorCode;
        public final String errorMessage;
        public final boolean ioException;
        public final int rotationDegrees;
        public final boolean success;

        public LibMp4OperationResult(boolean z, boolean z2, int i, int i2, String str) {
            this.success = z;
            this.ioException = z2;
            this.errorCode = i;
            this.errorMessage = str;
            this.rotationDegrees = i2;
        }
    }

    /* loaded from: classes2.dex */
    public class LibMp4CheckAndRepairResult {
        public final int errorCode;
        public final String errorMessage;
        public final boolean ioException;
        public final long newMajorVersion;
        public final long newMinorVersion;
        public final int newOriginator;
        public final long newReleaseVersion;
        public final long oldMajorVersion;
        public final long oldMinorVersion;
        public final int oldOriginator;
        public final long oldReleaseVersion;
        public final boolean repairRequired;
        public final boolean repaired;
        public final boolean success;

        public LibMp4CheckAndRepairResult(boolean z, boolean z2, boolean z3, int i, String str, boolean z4, long j, long j2, long j3, int i2, long j4, long j5, long j6, int i3) {
            this.success = z;
            this.repaired = z2;
            this.repairRequired = z3;
            this.errorCode = i;
            this.errorMessage = str;
            this.ioException = z4;
            this.oldMajorVersion = j;
            this.oldMinorVersion = j2;
            this.oldReleaseVersion = j3;
            this.oldOriginator = i2;
            this.newMajorVersion = j4;
            this.newMinorVersion = j5;
            this.newReleaseVersion = j6;
            this.newOriginator = i3;
        }
    }

    /* loaded from: classes3.dex */
    public class LibMp4StreamCheckResult {
        public final long bytesRequiredToExtractThumbnail;
        public final int errorCode;
        public final String errorMessage;
        public final boolean ioException;
        public final boolean success;

        public LibMp4StreamCheckResult(boolean z, boolean z2, int i, String str, long j) {
            this.success = z;
            this.ioException = z2;
            this.errorCode = i;
            this.errorMessage = str;
            this.bytesRequiredToExtractThumbnail = j;
        }
    }
}
