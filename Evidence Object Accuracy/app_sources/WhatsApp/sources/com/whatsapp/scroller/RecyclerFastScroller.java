package com.whatsapp.scroller;

import X.AbstractC117155Ys;
import X.AbstractC51142Ta;
import X.AnonymousClass004;
import X.AnonymousClass02M;
import X.AnonymousClass0QE;
import X.AnonymousClass2P7;
import X.AnonymousClass3NF;
import X.C74853it;
import X.C75043jD;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.google.android.material.appbar.AppBarLayout;
import com.whatsapp.scroller.RecyclerFastScroller;

/* loaded from: classes2.dex */
public class RecyclerFastScroller extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public View A02;
    public View A03;
    public CoordinatorLayout A04;
    public AnonymousClass02M A05;
    public RecyclerView A06;
    public AppBarLayout A07;
    public AbstractC51142Ta A08;
    public AnonymousClass2P7 A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final AnonymousClass0QE A0D;
    public final Runnable A0E;

    public RecyclerFastScroller(Context context) {
        this(context, null, 0);
    }

    public RecyclerFastScroller(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RecyclerFastScroller(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public RecyclerFastScroller(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        if (!this.A0B) {
            this.A0B = true;
            generatedComponent();
        }
        this.A0D = new C74853it(this);
        this.A0E = new RunnableBRunnable0Shape11S0100000_I0_11(this, 22);
        this.A01 = 1500;
        this.A0A = true;
    }

    public void A01() {
        RecyclerView recyclerView = this.A06;
        if (recyclerView != null && this.A0A) {
            Runnable runnable = this.A0E;
            recyclerView.removeCallbacks(runnable);
            this.A06.postDelayed(runnable, (long) this.A01);
        }
    }

    public final void A02() {
        requestLayout();
        if (this.A03.getVisibility() != 0) {
            this.A03.setVisibility(0);
            float f = 1.0f;
            if (this.A0C) {
                f = -1.0f;
            }
            TranslateAnimation translateAnimation = new TranslateAnimation(1, f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
            translateAnimation.setDuration(200);
            this.A03.startAnimation(translateAnimation);
        }
        A01();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A09;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A09 = r0;
        }
        return r0.generatedComponent();
    }

    public int getHideDelay() {
        return this.A01;
    }

    /* access modifiers changed from: private */
    public int getVisibleHeight() {
        if (this.A04 == null || this.A07 == null) {
            return getHeight();
        }
        return Math.min(getHeight(), (this.A04.getHeight() - this.A07.getHeight()) + this.A00);
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int totalScrollRange;
        int i5;
        int right;
        int left;
        int top;
        int left2;
        super.onLayout(z, i, i2, i3, i4);
        RecyclerView recyclerView = this.A06;
        if (recyclerView != null) {
            int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() + this.A00;
            int computeVerticalScrollRange = this.A06.computeVerticalScrollRange();
            AppBarLayout appBarLayout = this.A07;
            int i6 = 0;
            if (appBarLayout == null) {
                totalScrollRange = 0;
            } else {
                totalScrollRange = appBarLayout.getTotalScrollRange();
            }
            int visibleHeight = getVisibleHeight();
            float paddingBottom = ((float) computeVerticalScrollOffset) / ((float) (((computeVerticalScrollRange + totalScrollRange) + this.A06.getPaddingBottom()) - visibleHeight));
            View view = this.A03;
            if (view != null) {
                float height = paddingBottom * ((float) (visibleHeight - view.getHeight()));
                boolean z2 = this.A0C;
                View view2 = this.A03;
                if (z2) {
                    i5 = (int) height;
                    right = view2.getWidth();
                } else {
                    i6 = getRight() - this.A03.getWidth();
                    i5 = (int) height;
                    right = getRight();
                }
                view2.layout(i6, i5, right, this.A03.getHeight() + i5);
                View view3 = this.A02;
                if (view3 != null) {
                    boolean z3 = this.A0C;
                    View view4 = this.A03;
                    if (z3) {
                        left = view4.getRight();
                        top = ((this.A03.getTop() + this.A03.getBottom()) - this.A02.getHeight()) >> 1;
                        left2 = this.A03.getRight() + this.A02.getWidth();
                    } else {
                        left = view4.getLeft() - this.A02.getWidth();
                        top = ((this.A03.getTop() + this.A03.getBottom()) - this.A02.getHeight()) >> 1;
                        left2 = this.A03.getLeft();
                    }
                    view3.layout(left, top, left2, ((this.A03.getTop() + this.A03.getBottom()) + this.A02.getHeight()) >> 1);
                }
            }
        }
    }

    public void setAdapter(AnonymousClass02M r3) {
        AnonymousClass02M r0 = this.A05;
        if (r0 != r3) {
            if (r0 != null) {
                r0.A01.unregisterObserver(this.A0D);
            }
            if (r3 != null) {
                r3.A01.registerObserver(this.A0D);
            }
            this.A05 = r3;
        }
    }

    public void setAppBarLayout(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
        this.A04 = coordinatorLayout;
        this.A07 = appBarLayout;
        appBarLayout.A01(new AbstractC117155Ys() { // from class: X.518
            @Override // X.AbstractC115785Sx
            public final void ATD(AppBarLayout appBarLayout2, int i) {
                RecyclerFastScroller recyclerFastScroller = RecyclerFastScroller.this;
                int i2 = -i;
                if (recyclerFastScroller.A00 != i2) {
                    recyclerFastScroller.A02();
                    recyclerFastScroller.A00 = i2;
                }
            }
        });
    }

    public void setBubbleView(View view, AbstractC51142Ta r4) {
        this.A08 = r4;
        this.A02 = view;
        view.setVisibility(4);
        addView(this.A02, -2, -2);
    }

    public void setHideDelay(int i) {
        this.A01 = i;
    }

    public void setHidingEnabled(boolean z) {
        this.A0A = z;
        if (z) {
            A01();
        }
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.A06 = recyclerView;
        recyclerView.A0n(new C75043jD(this));
        AnonymousClass02M r0 = recyclerView.A0N;
        if (r0 != null) {
            setAdapter(r0);
        }
    }

    public void setRtl(boolean z) {
        this.A0C = z;
    }

    public void setThumbView(View view) {
        View view2 = this.A03;
        if (view2 != null) {
            removeView(view2);
        }
        this.A03 = view;
        view.setVisibility(4);
        this.A03.setOnTouchListener(new AnonymousClass3NF(this));
        addView(this.A03, -2, -2);
    }
}
