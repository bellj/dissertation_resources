package com.whatsapp.greenalert;

import X.AbstractC11770gq;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12O;
import X.AnonymousClass12P;
import X.AnonymousClass12S;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C17170qN;
import X.C22650zQ;
import X.C23000zz;
import X.C252018m;
import X.C252818u;
import X.C43901xo;
import X.C55362iH;
import X.C55372iI;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101574nk;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101704nx;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import com.facebook.redex.ViewOnClickCListenerShape1S0110000_I1;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaTabLayout;
import com.whatsapp.WaViewPager;
import com.whatsapp.components.Button;
import com.whatsapp.greenalert.GreenAlertActivity;

/* loaded from: classes2.dex */
public class GreenAlertActivity extends ActivityC13790kL {
    public static final int[] A0I = {R.string.green_alert_tos_opening_paragraph, R.string.green_alert_tos_eu_opening_paragraph};
    public static final int[] A0J = {R.string.green_alert_tos_bullets_intro, R.string.green_alert_tos_eu_bullets_intro};
    public static final int[] A0K = {R.string.green_alert_tos_bullet_1, R.string.green_alert_tos_eu_bullet_1};
    public static final int[] A0L = {R.string.green_alert_tos_bullet_2, R.string.green_alert_tos_eu_bullet_2};
    public static final int[] A0M = {R.string.green_alert_tos_bullet_3, R.string.green_alert_tos_bullet_3};
    public static final int[] A0N = {R.string.green_alert_tos_footer_no_date, R.string.green_alert_tos_eu_footer_no_date};
    public static final int[] A0O = {R.string.green_alert_tos_title, R.string.green_alert_tos_eu_title};
    public View A00;
    public View A01;
    public WaImageButton A02;
    public WaImageButton A03;
    public WaImageButton A04;
    public WaTabLayout A05;
    public WaViewPager A06;
    public Button A07;
    public C17170qN A08;
    public AnonymousClass018 A09;
    public C55362iH A0A;
    public C252018m A0B;
    public C23000zz A0C;
    public AnonymousClass12S A0D;
    public AnonymousClass12O A0E;
    public C22650zQ A0F;
    public boolean A0G;
    public final AbstractC11770gq A0H;

    public GreenAlertActivity() {
        this(0);
        this.A0H = new AbstractC11770gq() { // from class: X.4ru
            @Override // X.AbstractC11770gq
            public final void AVa(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                GreenAlertActivity greenAlertActivity = GreenAlertActivity.this;
                greenAlertActivity.A2h(greenAlertActivity.A06.getCurrentLogicalItem());
            }
        };
    }

    public GreenAlertActivity(int i) {
        this.A0G = false;
        ActivityC13830kP.A1P(this, 71);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0G) {
            this.A0G = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0F = (C22650zQ) A1M.A4n.get();
            this.A0B = C12980iv.A0g(A1M);
            this.A09 = C12960it.A0R(A1M);
            this.A0D = (AnonymousClass12S) A1M.AMF.get();
            this.A0E = (AnonymousClass12O) A1M.AMG.get();
            this.A0C = (C23000zz) A1M.ALg.get();
            this.A08 = C12990iw.A0a(A1M);
        }
    }

    public final void A2e() {
        int currentLogicalItem = this.A06.getCurrentLogicalItem();
        if (C43901xo.A02(this.A0E)) {
            AnonymousClass12S r2 = this.A0D;
            int i = 12;
            if (currentLogicalItem == 1) {
                i = 4;
            }
            r2.A01(Integer.valueOf(i));
            finish();
            return;
        }
        AnonymousClass12P.A03(this);
    }

    public final void A2f() {
        WaViewPager waViewPager = this.A06;
        NestedScrollView nestedScrollView = (NestedScrollView) waViewPager.findViewWithTag(Integer.valueOf(waViewPager.getCurrentLogicalItem()));
        if (nestedScrollView != null) {
            nestedScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101704nx(nestedScrollView, this));
        }
    }

    public final void A2g(int i) {
        WaImageButton waImageButton = this.A02;
        int i2 = 0;
        if (i == 0) {
            i2 = 8;
        }
        waImageButton.setVisibility(i2);
        Button button = this.A07;
        int i3 = R.string.green_alert_interstitial_continue_button;
        if (i == 1) {
            i3 = R.string.green_alert_interstitial_agree;
        }
        button.setText(i3);
    }

    public final void A2h(int i) {
        WaImageButton waImageButton;
        View findViewWithTag = this.A06.findViewWithTag(Integer.valueOf(i));
        if (findViewWithTag != null) {
            int i2 = 0;
            if (i != 1 || !this.A06.findViewWithTag(1).canScrollVertically(1)) {
                this.A07.setVisibility(0);
                waImageButton = this.A04;
                i2 = 8;
            } else {
                this.A07.setVisibility(4);
                waImageButton = this.A04;
            }
            waImageButton.setVisibility(i2);
            float dimension = getResources().getDimension(R.dimen.green_alert_sticky_top_panel_elevation);
            View view = this.A01;
            float f = 0.0f;
            if (findViewWithTag.getScrollY() <= 0) {
                dimension = 0.0f;
            }
            AnonymousClass028.A0V(view, dimension);
            float dimension2 = getResources().getDimension(R.dimen.green_alert_sticky_bottom_panel_elevation);
            boolean canScrollVertically = findViewWithTag.canScrollVertically(1);
            View view2 = this.A00;
            if (canScrollVertically) {
                f = dimension2;
            }
            AnonymousClass028.A0V(view2, f);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        int max = Math.max(-1, this.A06.getCurrentLogicalItem() - 1);
        if (max < 0) {
            A2e();
            return;
        }
        this.A06.setCurrentLogicalItem(max);
        A2g(max);
        A2h(max);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A2f();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_green_alert);
        this.A02 = (WaImageButton) AnonymousClass00T.A05(this, R.id.green_alert_back_button);
        this.A03 = (WaImageButton) AnonymousClass00T.A05(this, R.id.green_alert_dismiss_button);
        this.A07 = (Button) AnonymousClass00T.A05(this, R.id.green_alert_continue_button);
        this.A04 = (WaImageButton) AnonymousClass00T.A05(this, R.id.green_alert_scroll_tos_button);
        this.A05 = (WaTabLayout) AnonymousClass00T.A05(this, R.id.green_alert_tab_layout);
        this.A01 = AnonymousClass00T.A05(this, R.id.green_alert_sticky_top_panel);
        this.A00 = AnonymousClass00T.A05(this, R.id.green_alert_sticky_bottom_panel);
        this.A06 = (WaViewPager) AnonymousClass00T.A05(this, R.id.green_alert_viewpager);
        boolean A02 = C43901xo.A02(this.A0E);
        C14900mE r7 = ((ActivityC13810kN) this).A05;
        C22650zQ r13 = this.A0F;
        AnonymousClass12P r6 = ((ActivityC13790kL) this).A00;
        C252818u r8 = ((ActivityC13790kL) this).A02;
        C252018m r12 = this.A0B;
        AnonymousClass01d r9 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r11 = this.A09;
        C55362iH r4 = new C55362iH(this.A0H, r6, r7, r8, r9, this.A08, r11, r12, r13);
        this.A0A = r4;
        this.A06.setAdapter(r4);
        this.A06.A0G(new C55372iI(this));
        this.A06.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101574nk(this));
        this.A05.setupWithViewPager(this.A06);
        this.A05.setupTabsForAccessibility(this.A07);
        this.A05.setTabsClickable(false);
        this.A02.setOnClickListener(new ViewOnClickCListenerShape1S0110000_I1(this, 2, A02));
        C12960it.A0x(this.A03, this, 22);
        this.A07.setOnClickListener(new ViewOnClickCListenerShape1S0110000_I1(this, 3, A02));
        C12960it.A0x(this.A04, this, 21);
        int intExtra = getIntent().getIntExtra("page", 0);
        this.A06.setCurrentLogicalItem(intExtra);
        A2g(intExtra);
        A2h(intExtra);
        this.A0D.A01(11);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        boolean A02 = C43901xo.A02(this.A0E);
        WaImageButton waImageButton = this.A03;
        int i = 0;
        if (!A02) {
            i = 8;
        }
        waImageButton.setVisibility(i);
    }
}
