package com.whatsapp.tosgating.viewmodel;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass10Y;
import X.AnonymousClass163;
import X.C14850m9;
import X.C15570nT;
import X.C22700zV;
import X.C23000zz;
import X.C28081Kn;
import X.C33431e2;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ToSGatingViewModel extends AnonymousClass015 {
    public boolean A00;
    public final AnonymousClass016 A01 = new AnonymousClass016();
    public final C15570nT A02;
    public final C22700zV A03;
    public final AnonymousClass10Y A04;
    public final C14850m9 A05;
    public final AnonymousClass163 A06;
    public final C23000zz A07;
    public final C33431e2 A08;

    public ToSGatingViewModel(C15570nT r2, C22700zV r3, AnonymousClass10Y r4, C14850m9 r5, AnonymousClass163 r6, C23000zz r7) {
        C33431e2 r0 = new C33431e2(this);
        this.A08 = r0;
        this.A05 = r5;
        this.A02 = r2;
        this.A04 = r4;
        this.A06 = r6;
        this.A07 = r7;
        this.A03 = r3;
        r6.A03(r0);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A06.A04(this.A08);
    }

    public boolean A04(UserJid userJid) {
        return C28081Kn.A01(this.A03, this.A05, userJid, this.A07);
    }
}
