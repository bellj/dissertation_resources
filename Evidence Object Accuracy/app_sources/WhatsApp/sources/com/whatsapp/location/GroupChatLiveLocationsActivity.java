package com.whatsapp.location;

import X.AbstractC12160hT;
import X.AbstractC12170hU;
import X.AbstractC12180hV;
import X.AbstractC12190hW;
import X.AbstractC12200hX;
import X.AbstractC12500i1;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC36671kL;
import X.AbstractView$OnCreateContextMenuListenerC35851ir;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass03R;
import X.AnonymousClass03T;
import X.AnonymousClass04Q;
import X.AnonymousClass0I9;
import X.AnonymousClass0O0;
import X.AnonymousClass0PM;
import X.AnonymousClass0R9;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass13N;
import X.AnonymousClass13O;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass294;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3SC;
import X.AnonymousClass3SD;
import X.C05110Oh;
import X.C05200Oq;
import X.C05430Pn;
import X.C06860Vj;
import X.C06870Vk;
import X.C103154qI;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16240og;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20830wO;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22330yu;
import X.C22670zS;
import X.C244215l;
import X.C244415n;
import X.C244615p;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C30751Yr;
import X.C35991j8;
import X.C36301jf;
import X.C618032p;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101594nm;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.R;
import com.whatsapp.location.GroupChatLiveLocationsActivity;
import com.whatsapp.quickcontact.QuickContactActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class GroupChatLiveLocationsActivity extends ActivityC13790kL {
    public float A00;
    public int A01;
    public Bundle A02;
    public ImageView A03;
    public AbstractC12500i1 A04;
    public AnonymousClass04Q A05;
    public C244615p A06;
    public C16240og A07;
    public C22330yu A08;
    public AnonymousClass130 A09;
    public C15550nR A0A;
    public AnonymousClass10S A0B;
    public C15610nY A0C;
    public C21270x9 A0D;
    public AnonymousClass131 A0E;
    public C15890o4 A0F;
    public C20830wO A0G;
    public C15600nX A0H;
    public AnonymousClass12H A0I;
    public C244215l A0J;
    public AnonymousClass294 A0K;
    public AbstractView$OnCreateContextMenuListenerC35851ir A0L;
    public C16030oK A0M;
    public AnonymousClass13O A0N;
    public C244415n A0O;
    public C16630pM A0P;
    public AnonymousClass12F A0Q;
    public AnonymousClass19Z A0R;
    public Map A0S;
    public Set A0T;
    public boolean A0U;
    public final AbstractC12200hX A0V;
    public volatile boolean A0W;
    public volatile boolean A0X;

    public GroupChatLiveLocationsActivity() {
        this(0);
        this.A0T = new HashSet();
        this.A0S = new HashMap();
        this.A01 = 0;
        this.A0V = new AbstractC12200hX() { // from class: X.4uh
            @Override // X.AbstractC12200hX
            public final void ASO(AnonymousClass04Q r2) {
                GroupChatLiveLocationsActivity.A03(r2, GroupChatLiveLocationsActivity.this);
            }
        };
        this.A00 = -1.0f;
        this.A0X = false;
        this.A04 = new AnonymousClass3SC(this);
    }

    public GroupChatLiveLocationsActivity(int i) {
        this.A0U = false;
        A0R(new C103154qI(this));
    }

    public static /* synthetic */ float A02(GroupChatLiveLocationsActivity groupChatLiveLocationsActivity, float f, float f2) {
        if (f <= 0.0f) {
            return f2;
        }
        AnonymousClass04Q r0 = groupChatLiveLocationsActivity.A05;
        AnonymousClass009.A05(r0);
        C05430Pn A06 = r0.A0R.A06();
        Location location = new Location("");
        AnonymousClass03T r2 = A06.A02;
        location.setLatitude(r2.A00);
        location.setLongitude(r2.A01);
        Location location2 = new Location("");
        AnonymousClass03T r22 = A06.A03;
        location2.setLatitude(r22.A00);
        location2.setLongitude(r22.A01);
        double distanceTo = (double) location2.distanceTo(location);
        if (distanceTo <= 0.0d) {
            return f2;
        }
        float log = (float) (((double) groupChatLiveLocationsActivity.A05.A02().A02) + (Math.log((distanceTo / ((double) f)) / 30.0d) / Math.log(2.0d)));
        if (log > 16.0f) {
            return 16.0f;
        }
        return log;
    }

    public static /* synthetic */ void A03(AnonymousClass04Q r6, GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        if (groupChatLiveLocationsActivity.A05 == null) {
            groupChatLiveLocationsActivity.A05 = r6;
            if (r6 != null) {
                r6.A08(0, 0, groupChatLiveLocationsActivity.A01);
                groupChatLiveLocationsActivity.A01 = 0;
                AnonymousClass04Q r0 = groupChatLiveLocationsActivity.A05;
                AnonymousClass009.A05(r0);
                AnonymousClass04Q r1 = r0.A0S.A00;
                if (r1.A0F == null) {
                    AnonymousClass0I9 r02 = new AnonymousClass0I9(r1);
                    r1.A0F = r02;
                    r1.A0C(r02);
                }
                C05110Oh r03 = groupChatLiveLocationsActivity.A05.A0S;
                r03.A01 = false;
                r03.A00();
                groupChatLiveLocationsActivity.A05.A08 = new AnonymousClass3SD(groupChatLiveLocationsActivity);
                AnonymousClass04Q r12 = groupChatLiveLocationsActivity.A05;
                r12.A0C = new AbstractC12190hW() { // from class: X.3SI
                    @Override // X.AbstractC12190hW
                    public final boolean ASQ(AnonymousClass03R r7) {
                        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity2 = GroupChatLiveLocationsActivity.this;
                        AbstractView$OnCreateContextMenuListenerC35851ir r04 = groupChatLiveLocationsActivity2.A0L;
                        r04.A0u = true;
                        r04.A0s = false;
                        View view = r04.A0U;
                        C30751Yr r13 = r04.A0m;
                        int i = 8;
                        if (r13 == null) {
                            i = 0;
                        }
                        view.setVisibility(i);
                        Object obj = r7.A0L;
                        if (obj instanceof C35991j8) {
                            C35991j8 r2 = (C35991j8) obj;
                            if (!((AnonymousClass03S) r7).A04) {
                                r2 = groupChatLiveLocationsActivity2.A0L.A08((C30751Yr) r2.A04.get(0));
                                if (r2 != null) {
                                    r7 = (AnonymousClass03R) groupChatLiveLocationsActivity2.A0S.get(r2.A03);
                                }
                            }
                            if (r2.A00 != 1) {
                                List list = r2.A04;
                                if (list.size() == 1) {
                                    groupChatLiveLocationsActivity2.A0L.A0R(r2, true);
                                    r7.A0B();
                                    return true;
                                }
                                AnonymousClass04Q r05 = groupChatLiveLocationsActivity2.A05;
                                AnonymousClass009.A05(r05);
                                if (r05.A02().A02 >= 16.0f) {
                                    groupChatLiveLocationsActivity2.A0L.A0R(r2, true);
                                    return true;
                                }
                                groupChatLiveLocationsActivity2.A2h(list, true);
                                groupChatLiveLocationsActivity2.A0L.A0j = new C90294Ni(list, groupChatLiveLocationsActivity2.A05.A02().A02);
                                return true;
                            }
                        }
                        groupChatLiveLocationsActivity2.A0L.A0C();
                        return true;
                    }
                };
                r12.A09 = new AbstractC12160hT() { // from class: X.4ud
                    @Override // X.AbstractC12160hT
                    public final void ANZ(C06860Vj r62) {
                        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity2 = GroupChatLiveLocationsActivity.this;
                        AnonymousClass04Q r3 = groupChatLiveLocationsActivity2.A05;
                        AnonymousClass009.A05(r3);
                        if (((int) (groupChatLiveLocationsActivity2.A00 * 5.0f)) != ((int) (r3.A02().A02 * 5.0f))) {
                            groupChatLiveLocationsActivity2.A00 = groupChatLiveLocationsActivity2.A05.A02().A02;
                            groupChatLiveLocationsActivity2.A2f();
                        }
                    }
                };
                r12.A0B = new AbstractC12180hV() { // from class: X.3SG
                    @Override // X.AbstractC12180hV
                    public final void ASL(AnonymousClass03T r8) {
                        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity2 = GroupChatLiveLocationsActivity.this;
                        AbstractView$OnCreateContextMenuListenerC35851ir r62 = groupChatLiveLocationsActivity2.A0L;
                        if (r62.A0l != null) {
                            r62.A0C();
                            return;
                        }
                        C35991j8 A07 = r62.A07(new LatLng(r8.A00, r8.A01));
                        if (A07 != null) {
                            List list = A07.A04;
                            if (list.size() == 1) {
                                groupChatLiveLocationsActivity2.A0L.A0R(A07, true);
                                ((AnonymousClass03R) groupChatLiveLocationsActivity2.A0S.get(A07.A03)).A0B();
                            } else if (groupChatLiveLocationsActivity2.A05.A02().A02 >= 16.0f) {
                                groupChatLiveLocationsActivity2.A0L.A0R(A07, true);
                            } else {
                                groupChatLiveLocationsActivity2.A2h(list, true);
                                groupChatLiveLocationsActivity2.A0L.A0j = new C90294Ni(list, groupChatLiveLocationsActivity2.A05.A02().A02);
                            }
                        }
                    }
                };
                r12.A0A = new AbstractC12170hU() { // from class: X.3SF
                    @Override // X.AbstractC12170hU
                    public final void ARN(AnonymousClass03R r7) {
                        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity2 = GroupChatLiveLocationsActivity.this;
                        C35991j8 r5 = (C35991j8) r7.A0L;
                        if (r5 != null && !((ActivityC13790kL) groupChatLiveLocationsActivity2).A01.A0F(r5.A02.A06)) {
                            Intent A0D = C12990iw.A0D(groupChatLiveLocationsActivity2, QuickContactActivity.class);
                            AnonymousClass03T r13 = r7.A0K;
                            AnonymousClass04Q r04 = groupChatLiveLocationsActivity2.A05;
                            AnonymousClass009.A05(r04);
                            Point A04 = r04.A0R.A04(r13);
                            Rect A0J = C12980iv.A0J();
                            int i = A04.x;
                            A0J.left = i;
                            int i2 = A04.y;
                            A0J.top = i2;
                            A0J.right = i;
                            A0J.bottom = i2;
                            A0D.setSourceBounds(A0J);
                            C12980iv.A13(A0D, r5.A02.A06);
                            A0D.putExtra("gjid", groupChatLiveLocationsActivity2.A0L.A0c.getRawString());
                            A0D.putExtra("show_get_direction", true);
                            A0D.putExtra("profile_entry_point", 16);
                            C30751Yr r05 = groupChatLiveLocationsActivity2.A0L.A0m;
                            if (r05 != null) {
                                A0D.putExtra("location_latitude", r05.A00);
                                A0D.putExtra("location_longitude", groupChatLiveLocationsActivity2.A0L.A0m.A01);
                            }
                            groupChatLiveLocationsActivity2.startActivity(A0D);
                        }
                    }
                };
                groupChatLiveLocationsActivity.A2f();
                Bundle bundle = groupChatLiveLocationsActivity.A02;
                if (bundle != null) {
                    groupChatLiveLocationsActivity.A0K.setLocationMode(bundle.getInt("map_location_mode", 2));
                    if (groupChatLiveLocationsActivity.A02.containsKey("camera_zoom")) {
                        groupChatLiveLocationsActivity.A05.A0A(AnonymousClass0R9.A01(new AnonymousClass03T(groupChatLiveLocationsActivity.A02.getDouble("camera_lat"), groupChatLiveLocationsActivity.A02.getDouble("camera_lng")), groupChatLiveLocationsActivity.A02.getFloat("camera_zoom")));
                    }
                    groupChatLiveLocationsActivity.A02 = null;
                } else if (groupChatLiveLocationsActivity.A0T.isEmpty()) {
                    SharedPreferences A01 = groupChatLiveLocationsActivity.A0P.A01(AnonymousClass01V.A07);
                    AnonymousClass03T r2 = new AnonymousClass03T((double) A01.getFloat("live_location_lat", 37.389805f), (double) A01.getFloat("live_location_lng", -122.08141f));
                    AnonymousClass04Q r13 = groupChatLiveLocationsActivity.A05;
                    C05200Oq r04 = new C05200Oq();
                    r04.A06 = r2;
                    r13.A0A(r04);
                    AnonymousClass04Q r22 = groupChatLiveLocationsActivity.A05;
                    C05200Oq r05 = new C05200Oq();
                    r05.A01 = A01.getFloat("live_location_zoom", 16.0f) - 0.2f;
                    r22.A0A(r05);
                } else {
                    groupChatLiveLocationsActivity.A2i(false);
                }
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0U) {
            this.A0U = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0R = (AnonymousClass19Z) r1.A2o.get();
            this.A0D = (C21270x9) r1.A4A.get();
            this.A0O = (C244415n) r1.AAg.get();
            this.A09 = (AnonymousClass130) r1.A41.get();
            this.A0A = (C15550nR) r1.A45.get();
            this.A0C = (C15610nY) r1.AMe.get();
            this.A0B = (AnonymousClass10S) r1.A46.get();
            this.A0I = (AnonymousClass12H) r1.AC5.get();
            this.A0Q = (AnonymousClass12F) r1.AJM.get();
            this.A07 = (C16240og) r1.ANq.get();
            this.A08 = (C22330yu) r1.A3I.get();
            this.A0F = (C15890o4) r1.AN1.get();
            this.A06 = (C244615p) r1.A8H.get();
            this.A0M = (C16030oK) r1.AAd.get();
            this.A0H = (C15600nX) r1.A8x.get();
            this.A0P = (C16630pM) r1.AIc.get();
            this.A0G = (C20830wO) r1.A4W.get();
            this.A0E = (AnonymousClass131) r1.A49.get();
            this.A0J = (C244215l) r1.A8y.get();
            this.A0N = (AnonymousClass13O) r1.AAf.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        if (r3.A0F.A03() == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2e() {
        /*
            r3 = this;
            X.AnonymousClass009.A01()
            X.04Q r0 = r3.A05
            if (r0 != 0) goto L_0x0011
            X.294 r1 = r3.A0K
            X.0hX r0 = r3.A0V
            X.04Q r0 = r1.A0J(r0)
            r3.A05 = r0
        L_0x0011:
            android.widget.ImageView r2 = r3.A03
            X.1ir r0 = r3.A0L
            X.1Yr r0 = r0.A0m
            if (r0 != 0) goto L_0x0022
            X.0o4 r0 = r3.A0F
            boolean r1 = r0.A03()
            r0 = 0
            if (r1 != 0) goto L_0x0024
        L_0x0022:
            r0 = 8
        L_0x0024:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.GroupChatLiveLocationsActivity.A2e():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00fc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2f() {
        /*
        // Method dump skipped, instructions count: 415
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.GroupChatLiveLocationsActivity.A2f():void");
    }

    public final void A2g(AnonymousClass0PM r15, boolean z) {
        C05200Oq r2;
        AnonymousClass009.A05(this.A05);
        C06870Vk A00 = r15.A00();
        AnonymousClass03T A002 = A00.A00();
        int width = this.A0K.getWidth();
        int height = this.A0K.getHeight();
        AnonymousClass03T r0 = A00.A01;
        LatLng latLng = new LatLng(r0.A00, r0.A01);
        AnonymousClass03T r02 = A00.A00;
        LatLngBounds latLngBounds = new LatLngBounds(latLng, new LatLng(r02.A00, r02.A01));
        LatLng latLng2 = latLngBounds.A00;
        double A003 = AbstractView$OnCreateContextMenuListenerC35851ir.A00(latLng2.A00);
        LatLng latLng3 = latLngBounds.A01;
        double A004 = (A003 - AbstractView$OnCreateContextMenuListenerC35851ir.A00(latLng3.A00)) / 3.141592653589793d;
        double d = latLng2.A01 - latLng3.A01;
        if (d < 0.0d) {
            d += 360.0d;
        }
        float min = (float) Math.min(Math.log((((double) height) / 256.0d) / A004) / 0.6931471805599453d, Math.log((((double) width) / 256.0d) / (d / 360.0d)) / 0.6931471805599453d);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.group_chat_live_location_map_view_bound_size);
        int i = dimensionPixelSize << 1;
        if (this.A0K.getHeight() > i && this.A0K.getWidth() > i) {
            if (z) {
                this.A0W = true;
                int i2 = (min > 21.0f ? 1 : (min == 21.0f ? 0 : -1));
                AnonymousClass04Q r3 = this.A05;
                if (i2 > 0) {
                    r2 = AnonymousClass0R9.A01(A002, 19.0f);
                } else {
                    r2 = new C05200Oq();
                    r2.A07 = A00;
                    r2.A05 = dimensionPixelSize;
                }
                r3.A0B(r2, this.A04, 1500);
                return;
            }
            this.A05.A0A(AnonymousClass0R9.A01(A002, Math.min(19.0f, min)));
        }
    }

    public final void A2h(List list, boolean z) {
        AnonymousClass009.A05(this.A05);
        if (list.size() != 1) {
            AnonymousClass0PM r6 = new AnonymousClass0PM();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C30751Yr r0 = (C30751Yr) it.next();
                r6.A01(new AnonymousClass03T(r0.A00, r0.A01));
            }
            A2g(r6, z);
        } else if (z) {
            this.A0W = true;
            this.A05.A09(AnonymousClass0R9.A01(new AnonymousClass03T(((C30751Yr) list.get(0)).A00, ((C30751Yr) list.get(0)).A01), 16.0f));
        } else {
            this.A05.A0A(AnonymousClass0R9.A01(new AnonymousClass03T(((C30751Yr) list.get(0)).A00, ((C30751Yr) list.get(0)).A01), 16.0f));
        }
    }

    public final void A2i(boolean z) {
        if (this.A05 != null && !this.A0L.A0u) {
            Set set = this.A0T;
            if (set.isEmpty()) {
                return;
            }
            if (this.A0K.getWidth() <= 0 || this.A0K.getHeight() <= 0) {
                this.A0K.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101594nm(this));
            } else if (!z || !this.A0W) {
                ArrayList arrayList = new ArrayList(set);
                AnonymousClass009.A05(this.A05);
                if (this.A0L.A06() != null) {
                    LatLng A06 = this.A0L.A06();
                    AnonymousClass03T r4 = new AnonymousClass03T(A06.A00, A06.A01);
                    Collections.sort(arrayList, new Comparator(r4.A00, r4.A01) { // from class: X.5Cn
                        public final /* synthetic */ double A00;
                        public final /* synthetic */ double A01;

                        {
                            this.A00 = r1;
                            this.A01 = r3;
                        }

                        @Override // java.util.Comparator
                        public final int compare(Object obj, Object obj2) {
                            double d = this.A00;
                            double d2 = this.A01;
                            AnonymousClass03T r6 = ((AnonymousClass03R) obj).A0K;
                            double d3 = r6.A00 - d;
                            double d4 = r6.A01 - d2;
                            double d5 = (d3 * d3) + (d4 * d4);
                            AnonymousClass03T r8 = ((AnonymousClass03R) obj2).A0K;
                            double d6 = r8.A00 - d;
                            double d7 = r8.A01 - d2;
                            return Double.compare(d5, (d6 * d6) + (d7 * d7));
                        }
                    });
                }
                AnonymousClass0PM r8 = new AnonymousClass0PM();
                AnonymousClass0PM r10 = new AnonymousClass0PM();
                int i = 0;
                while (i < arrayList.size()) {
                    AnonymousClass03R r5 = (AnonymousClass03R) arrayList.get(i);
                    r10.A01(r5.A0K);
                    C06870Vk A00 = r10.A00();
                    AnonymousClass03T r0 = A00.A01;
                    LatLng latLng = new LatLng(r0.A00, r0.A01);
                    AnonymousClass03T r02 = A00.A00;
                    if (!AbstractView$OnCreateContextMenuListenerC35851ir.A03(new LatLngBounds(latLng, new LatLng(r02.A00, r02.A01)))) {
                        break;
                    }
                    r8.A01(r5.A0K);
                    i++;
                }
                if (i == 1) {
                    A2h(((C35991j8) ((AnonymousClass03R) arrayList.get(0)).A0L).A04, z);
                } else {
                    A2g(r8, z);
                }
            } else {
                this.A0X = true;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.A0L.A0Y(i, i2)) {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C15570nT r03 = ((ActivityC13790kL) this).A01;
        AnonymousClass19Z r04 = this.A0R;
        AnonymousClass12P r05 = ((ActivityC13790kL) this).A00;
        C21270x9 r06 = this.A0D;
        C244415n r07 = this.A0O;
        AnonymousClass130 r08 = this.A09;
        C15550nR r15 = this.A0A;
        C15610nY r14 = this.A0C;
        AnonymousClass018 r13 = ((ActivityC13830kP) this).A01;
        AnonymousClass10S r12 = this.A0B;
        AnonymousClass12H r11 = this.A0I;
        C16240og r10 = this.A07;
        C22330yu r9 = this.A08;
        C15890o4 r8 = this.A0F;
        this.A0L = new C36301jf(r05, this.A06, r02, r03, r10, r9, r08, r15, r12, r14, r06, this.A0E, r0, r8, r13, r11, this.A0J, this, this.A0M, this.A0N, r07, r04);
        A1U().A0M(true);
        setContentView(R.layout.groupchat_live_locations);
        C20830wO r2 = this.A0G;
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        C15370n3 A012 = r2.A01(A01);
        A1U().A0I(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, this.A0C.A04(A012)));
        this.A0L.A0O(this, bundle);
        AnonymousClass13N.A00(this);
        AnonymousClass0O0 r1 = new AnonymousClass0O0();
        r1.A00 = 1;
        r1.A05 = true;
        r1.A02 = true;
        r1.A03 = true;
        this.A0K = new C618032p(this, r1, this);
        ((ViewGroup) AnonymousClass00T.A05(this, R.id.map_holder)).addView(this.A0K);
        this.A0K.A0E(bundle);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.my_location);
        this.A03 = imageView;
        imageView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 27));
        this.A02 = bundle;
        A2e();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A04 = this.A0L.A04(i);
        if (A04 == null) {
            return super.onCreateDialog(i);
        }
        return A04;
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_layers, menu);
        menu.removeGroup(R.id.map_setting);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0L.A0D();
        if (this.A05 != null) {
            SharedPreferences.Editor edit = this.A0P.A01(AnonymousClass01V.A07).edit();
            C06860Vj A02 = this.A05.A02();
            AnonymousClass03T r4 = A02.A03;
            edit.putFloat("live_location_lat", (float) r4.A00);
            edit.putFloat("live_location_lng", (float) r4.A01);
            edit.putFloat("live_location_zoom", A02.A02);
            edit.apply();
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        this.A0K.A05();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        AnonymousClass009.A05(this.A05);
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        AnonymousClass294 r0 = this.A0K;
        SensorManager sensorManager = r0.A04;
        if (sensorManager != null) {
            sensorManager.unregisterListener(r0.A09);
        }
        this.A0L.A0E();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0K.A0K();
        this.A0L.A0F();
        A2e();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass04Q r0 = this.A05;
        if (r0 != null) {
            C06860Vj A02 = r0.A02();
            bundle.putFloat("camera_zoom", A02.A02);
            AnonymousClass03T r3 = A02.A03;
            bundle.putDouble("camera_lat", r3.A00);
            bundle.putDouble("camera_lng", r3.A01);
            bundle.putInt("map_location_mode", this.A0K.A02);
        }
        this.A0K.A0F(bundle);
        this.A0L.A0P(bundle);
        super.onSaveInstanceState(bundle);
    }
}
