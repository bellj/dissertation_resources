package com.whatsapp.location;

import X.AbstractC115755Su;
import X.AbstractC12200hX;
import X.AnonymousClass004;
import X.AnonymousClass04L;
import X.AnonymousClass1XP;
import X.AnonymousClass226;
import X.AnonymousClass2P7;
import X.C04640Mm;
import X.C244415n;
import X.C30341Xa;
import X.C30751Yr;
import X.C36331ji;
import X.C56462kv;
import android.content.Context;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.R;
import com.whatsapp.location.WaMapView;

/* loaded from: classes2.dex */
public class WaMapView extends FrameLayout implements AnonymousClass004 {
    public static C04640Mm A04;
    public static C36331ji A05;
    public AnonymousClass04L A00;
    public AnonymousClass226 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public WaMapView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public WaMapView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
    }

    public WaMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public WaMapView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public WaMapView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public void A00(LatLng latLng) {
        String string = getContext().getString(R.string.location_marker_content_description);
        AnonymousClass226 r1 = this.A01;
        if (r1 != null) {
            r1.A06(new AbstractC115755Su(string) { // from class: X.3TK
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AbstractC115755Su
                public final void ASP(C35961j4 r6) {
                    LatLng latLng2 = LatLng.this;
                    String str = this.A01;
                    C36331ji r12 = WaMapView.A05;
                    if (r12 == null) {
                        try {
                            AnonymousClass5YB r2 = C36321jh.A00;
                            C13020j0.A02(r2, "IBitmapDescriptorFactory is not initialized");
                            C65873Li r22 = (C65873Li) r2;
                            Parcel A01 = r22.A01();
                            A01.writeInt(R.drawable.ic_map_pin);
                            r12 = new C36331ji(C65873Li.A00(A01, r22, 1));
                            WaMapView.A05 = r12;
                        } catch (RemoteException e) {
                            throw new C113245Gt(e);
                        }
                    }
                    C56482kx r0 = new C56482kx();
                    r0.A08 = latLng2;
                    r0.A07 = r12;
                    r0.A09 = str;
                    r6.A06();
                    r6.A03(r0);
                }
            });
            return;
        }
        AnonymousClass04L r12 = this.A00;
        if (r12 != null) {
            r12.A0G(new AbstractC12200hX(string) { // from class: X.3SK
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AbstractC12200hX
                public final void ASO(AnonymousClass04Q r8) {
                    C04640Mm A01;
                    LatLng latLng2 = LatLng.this;
                    String str = this.A01;
                    if (WaMapView.A04 == null) {
                        if (AnonymousClass03Q.A02 == null) {
                            A01 = null;
                        } else {
                            A01 = AnonymousClass03Q.A01(new C08580bR(), C12960it.A0W(R.drawable.ic_map_pin, "resource_"));
                        }
                        WaMapView.A04 = A01;
                    }
                    C06050Rz r5 = new C06050Rz();
                    r5.A01 = new AnonymousClass03T(latLng2.A00, latLng2.A01);
                    r5.A00 = WaMapView.A04;
                    r5.A03 = str;
                    r8.A06();
                    r8.A03(r5);
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r10.A00 != 0.0d) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.google.android.gms.maps.model.LatLng r10, X.C56462kv r11, X.C244415n r12) {
        /*
        // Method dump skipped, instructions count: 228
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.WaMapView.A01(com.google.android.gms.maps.model.LatLng, X.2kv, X.15n):void");
    }

    public void A02(C244415n r6, C30341Xa r7, boolean z) {
        double d;
        double d2;
        C56462kv A00;
        C30751Yr r2;
        if (z || (r2 = r7.A02) == null) {
            d = ((AnonymousClass1XP) r7).A00;
            d2 = ((AnonymousClass1XP) r7).A01;
        } else {
            d = r2.A00;
            d2 = r2.A01;
        }
        LatLng latLng = new LatLng(d, d2);
        if (z) {
            A00 = null;
        } else {
            A00 = C56462kv.A00(getContext(), R.raw.expired_map_style_json);
        }
        A01(latLng, A00, r6);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }
}
