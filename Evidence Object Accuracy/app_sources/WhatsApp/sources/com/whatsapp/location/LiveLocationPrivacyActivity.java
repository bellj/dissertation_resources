package com.whatsapp.location;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass04S;
import X.AnonymousClass12P;
import X.AnonymousClass13Q;
import X.AnonymousClass19M;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C102284ot;
import X.C103174qK;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C35211hS;
import X.C36281jd;
import X.C52792bh;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.location.LiveLocationPrivacyActivity;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class LiveLocationPrivacyActivity extends ActivityC13790kL {
    public View A00;
    public View A01;
    public View A02;
    public Button A03;
    public ListView A04;
    public ScrollView A05;
    public TextView A06;
    public C15610nY A07;
    public AnonymousClass1J1 A08;
    public C21270x9 A09;
    public C15890o4 A0A;
    public C15650ng A0B;
    public C52792bh A0C;
    public C16030oK A0D;
    public boolean A0E;
    public final AnonymousClass13Q A0F;
    public final List A0G;

    public LiveLocationPrivacyActivity() {
        this(0);
        this.A0G = new ArrayList();
        this.A0F = new C36281jd(this);
    }

    public LiveLocationPrivacyActivity(int i) {
        this.A0E = false;
        A0R(new C103174qK(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0E) {
            this.A0E = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A09 = (C21270x9) r1.A4A.get();
            this.A07 = (C15610nY) r1.AMe.get();
            this.A0B = (C15650ng) r1.A4m.get();
            this.A0A = (C15890o4) r1.AN1.get();
            this.A0D = (C16030oK) r1.AAd.get();
        }
    }

    public final void A2e() {
        this.A04.setOnScrollListener(new C102284ot(this, getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation)));
    }

    public final void A2f() {
        ArrayList arrayList;
        List list = this.A0G;
        list.clear();
        C16030oK r7 = this.A0D;
        synchronized (r7.A0X) {
            Map A0B = r7.A0B();
            arrayList = new ArrayList(A0B.size());
            long A00 = r7.A0K.A00();
            for (C35211hS r4 : A0B.values()) {
                if (C16030oK.A01(r4.A01, A00)) {
                    C15550nR r1 = r7.A0G;
                    AnonymousClass1IS r42 = r4.A02;
                    AbstractC14640lm r0 = r42.A00;
                    AnonymousClass009.A05(r0);
                    arrayList.add(new Pair(r1.A0A(r0), r42));
                }
            }
        }
        list.addAll(arrayList);
        this.A0C.notifyDataSetChanged();
        boolean isEmpty = list.isEmpty();
        TextView textView = this.A06;
        if (isEmpty) {
            textView.setVisibility(8);
            this.A01.setVisibility(8);
            this.A02.setVisibility(8);
            this.A04.setVisibility(8);
            this.A05.setVisibility(0);
            this.A03.setVisibility(8);
            return;
        }
        textView.setText(((ActivityC13830kP) this).A01.A0I(new Object[]{Integer.valueOf(list.size())}, R.plurals.live_location_currently_sharing, (long) list.size()));
        this.A06.setVisibility(0);
        this.A01.setVisibility(0);
        this.A05.setVisibility(8);
        this.A02.setVisibility(0);
        this.A04.setVisibility(0);
        this.A03.setVisibility(0);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        RequestPermissionActivity.A0U(this, this.A0A, R.string.permission_location_access_on_updating_location_request, R.string.permission_location_access_on_updating_location, 0);
        setContentView(R.layout.live_location_privacy);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0A(R.string.settings_privacy_live_location);
        this.A08 = this.A09.A04(this, "live-location-privacy-activity");
        this.A0C = new C52792bh(this);
        this.A02 = findViewById(R.id.list_view_container);
        this.A04 = (ListView) findViewById(R.id.list_view);
        View inflate = getLayoutInflater().inflate(R.layout.live_location_list_header, (ViewGroup) null, false);
        AnonymousClass028.A0a(inflate, 2);
        this.A06 = (TextView) inflate.findViewById(R.id.title);
        this.A05 = (ScrollView) findViewById(R.id.live_location_not_sharing);
        this.A00 = findViewById(R.id.bottom_button_container);
        this.A03 = (Button) findViewById(R.id.stop_sharing_btn);
        this.A04.addHeaderView(inflate);
        View inflate2 = getLayoutInflater().inflate(R.layout.live_location_privacy_footer, (ViewGroup) null, false);
        this.A01 = inflate2;
        this.A04.addFooterView(inflate2);
        this.A04.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.3OA
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                LiveLocationPrivacyActivity liveLocationPrivacyActivity = LiveLocationPrivacyActivity.this;
                int i2 = i - 1;
                if (i2 >= 0 && i2 < liveLocationPrivacyActivity.A0C.getCount()) {
                    AbstractC15340mz A03 = liveLocationPrivacyActivity.A0B.A0K.A03((AnonymousClass1IS) ((Pair) liveLocationPrivacyActivity.A0C.A00.A0G.get(i2)).second);
                    C14960mK r1 = new C14960mK();
                    AnonymousClass1IS r5 = A03.A0z;
                    Intent putExtra = r1.A0i(liveLocationPrivacyActivity, r5.A00).putExtra("start_t", SystemClock.uptimeMillis()).putExtra("row_id", A03.A11).putExtra("sort_id", A03.A12);
                    C38211ni.A00(putExtra, r5);
                    ((ActivityC13790kL) liveLocationPrivacyActivity).A00.A07(liveLocationPrivacyActivity, putExtra);
                }
            }
        });
        this.A04.setAdapter((ListAdapter) this.A0C);
        if (Build.VERSION.SDK_INT >= 21) {
            A2e();
        }
        this.A03.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 7));
        A2f();
        this.A0D.A0W(this.A0F);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 0) {
            return super.onCreateDialog(i);
        }
        C004802e r3 = new C004802e(this);
        r3.A06(R.string.live_location_stop_sharing_dialog);
        r3.A0B(true);
        r3.setNegativeButton(R.string.cancel, null);
        r3.setPositiveButton(R.string.live_location_stop, new DialogInterface.OnClickListener() { // from class: X.3KD
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                LiveLocationPrivacyActivity liveLocationPrivacyActivity = LiveLocationPrivacyActivity.this;
                ((ActivityC13810kN) liveLocationPrivacyActivity).A09.A13(true);
                ((ActivityC13830kP) liveLocationPrivacyActivity).A05.Ab2(new RunnableBRunnable0Shape7S0100000_I0_7(liveLocationPrivacyActivity, 33));
            }
        });
        AnonymousClass04S create = r3.create();
        create.requestWindowFeature(1);
        return create;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C16030oK r0 = this.A0D;
        r0.A0b.remove(this.A0F);
        AnonymousClass1J1 r02 = this.A08;
        if (r02 != null) {
            r02.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        LocationSharingService.A01(getApplicationContext(), this.A0D);
    }
}
