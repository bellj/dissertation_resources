package com.whatsapp.location;

import X.AbstractC12160hT;
import X.AbstractC12170hU;
import X.AbstractC12180hV;
import X.AbstractC12190hW;
import X.AbstractC12200hX;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC36001jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass03R;
import X.AnonymousClass03T;
import X.AnonymousClass04Q;
import X.AnonymousClass0O0;
import X.AnonymousClass0R9;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass13N;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass294;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3DJ;
import X.AnonymousClass3SE;
import X.C04640Mm;
import X.C05110Oh;
import X.C06050Rz;
import X.C06860Vj;
import X.C103184qL;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16170oZ;
import X.C16590pI;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22670zS;
import X.C22700zV;
import X.C231510o;
import X.C244415n;
import X.C244615p;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C253719d;
import X.C36011jB;
import X.C618132q;
import X.C618732w;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.PlaceInfo;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* loaded from: classes2.dex */
public class LocationPicker extends ActivityC13790kL {
    public float A00;
    public float A01;
    public Bundle A02;
    public AnonymousClass04Q A03;
    public C04640Mm A04;
    public C04640Mm A05;
    public C04640Mm A06;
    public AnonymousClass03R A07;
    public C244615p A08;
    public C18790t3 A09;
    public C16170oZ A0A;
    public AnonymousClass130 A0B;
    public C22700zV A0C;
    public AnonymousClass131 A0D;
    public C16590pI A0E;
    public C15890o4 A0F;
    public C15650ng A0G;
    public C231510o A0H;
    public AnonymousClass193 A0I;
    public C22050yP A0J;
    public C253719d A0K;
    public AnonymousClass294 A0L;
    public AbstractC36001jA A0M;
    public C16030oK A0N;
    public C244415n A0O;
    public WhatsAppLibLoader A0P;
    public C16630pM A0Q;
    public C252018m A0R;
    public boolean A0S;
    public boolean A0T;
    public final AbstractC12200hX A0U;

    public LocationPicker() {
        this(0);
        this.A0U = new AbstractC12200hX() { // from class: X.4ui
            @Override // X.AbstractC12200hX
            public final void ASO(AnonymousClass04Q r2) {
                LocationPicker.A02(r2, LocationPicker.this);
            }
        };
    }

    public LocationPicker(int i) {
        this.A0T = false;
        A0R(new C103184qL(this));
    }

    public static /* synthetic */ void A02(AnonymousClass04Q r7, LocationPicker locationPicker) {
        if (locationPicker.A03 == null) {
            locationPicker.A03 = r7;
            if (r7 != null) {
                AnonymousClass009.A05(r7);
                if (locationPicker.A0F.A03() && !locationPicker.A0M.A0s) {
                    locationPicker.A03.A0E(true);
                }
                AnonymousClass04Q r3 = locationPicker.A03;
                AbstractC36001jA r0 = locationPicker.A0M;
                r3.A08(0, 0, Math.max(r0.A00, r0.A02));
                C05110Oh r02 = locationPicker.A03.A0S;
                r02.A01 = false;
                r02.A00();
                locationPicker.A03.A08 = new AnonymousClass3SE(locationPicker);
                AnonymousClass04Q r1 = locationPicker.A03;
                r1.A0C = new AbstractC12190hW() { // from class: X.3SJ
                    @Override // X.AbstractC12190hW
                    public final boolean ASQ(AnonymousClass03R r4) {
                        Object obj;
                        LocationPicker locationPicker2 = LocationPicker.this;
                        AbstractC36001jA r12 = locationPicker2.A0M;
                        if (r12.A0s) {
                            return true;
                        }
                        PlaceInfo placeInfo = r12.A0f;
                        if (!(placeInfo == null || (obj = placeInfo.A0D) == null)) {
                            AnonymousClass03R r13 = (AnonymousClass03R) obj;
                            r13.A0E(locationPicker2.A05);
                            r13.A0A();
                        }
                        r4.A0E(locationPicker2.A06);
                        locationPicker2.A0M.A0Q(r4);
                        locationPicker2.A0M.A0B.setVisibility(8);
                        locationPicker2.A0M.A0E.setVisibility(8);
                        if (!locationPicker2.A0M.A0n && locationPicker2.A0F.A03()) {
                            return true;
                        }
                        r4.A0B();
                        return true;
                    }
                };
                r1.A0A = new AbstractC12170hU() { // from class: X.4uf
                    @Override // X.AbstractC12170hU
                    public final void ARN(AnonymousClass03R r32) {
                        LocationPicker.this.A0M.A0R(String.valueOf(((AnonymousClass03S) r32).A06), r32);
                    }
                };
                r1.A0B = new AbstractC12180hV() { // from class: X.3SH
                    @Override // X.AbstractC12180hV
                    public final void ASL(AnonymousClass03T r4) {
                        LocationPicker locationPicker2 = LocationPicker.this;
                        PlaceInfo placeInfo = locationPicker2.A0M.A0f;
                        if (placeInfo != null) {
                            Object obj = placeInfo.A0D;
                            if (obj != null) {
                                ((AnonymousClass03R) obj).A0E(locationPicker2.A05);
                            }
                            AbstractC36001jA r12 = locationPicker2.A0M;
                            r12.A0f = null;
                            r12.A0D();
                        }
                        AbstractC36001jA r13 = locationPicker2.A0M;
                        if (r13.A0n) {
                            r13.A0E.setVisibility(0);
                        }
                        locationPicker2.A0M.A0B.setVisibility(8);
                    }
                };
                r1.A09 = new AbstractC12160hT() { // from class: X.4ue
                    @Override // X.AbstractC12160hT
                    public final void ANZ(C06860Vj r6) {
                        AbstractC36001jA r4 = LocationPicker.this.A0M;
                        AnonymousClass03T r03 = r6.A03;
                        r4.A0E(r03.A00, r03.A01);
                    }
                };
                locationPicker.A0M.A0O(null, false);
                AbstractC36001jA r12 = locationPicker.A0M;
                C36011jB r03 = r12.A0g;
                if (r03 != null && !r03.A08.isEmpty()) {
                    r12.A05();
                }
                Bundle bundle = locationPicker.A02;
                if (bundle != null) {
                    locationPicker.A0L.setLocationMode(bundle.getInt("map_location_mode", 2));
                    if (locationPicker.A02.containsKey("camera_zoom")) {
                        locationPicker.A03.A0A(AnonymousClass0R9.A01(new AnonymousClass03T(locationPicker.A02.getDouble("camera_lat"), locationPicker.A02.getDouble("camera_lng")), locationPicker.A02.getFloat("camera_zoom")));
                    }
                    locationPicker.A02 = null;
                    return;
                }
                SharedPreferences A01 = locationPicker.A0Q.A01(AnonymousClass01V.A07);
                locationPicker.A03.A0A(AnonymousClass0R9.A01(new AnonymousClass03T((double) A01.getFloat("share_location_lat", 37.389805f), (double) A01.getFloat("share_location_lon", -122.08141f)), A01.getFloat("share_location_zoom", 15.0f) - 0.2f));
            }
        }
    }

    public static /* synthetic */ void A03(AnonymousClass03T r2, LocationPicker locationPicker) {
        AnonymousClass009.A05(locationPicker.A03);
        AnonymousClass03R r0 = locationPicker.A07;
        if (r0 == null) {
            C06050Rz r1 = new C06050Rz();
            r1.A01 = r2;
            r1.A00 = locationPicker.A04;
            locationPicker.A07 = locationPicker.A03.A03(r1);
            return;
        }
        r0.A0F(r2);
        locationPicker.A07.A09(true);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0K = (C253719d) r1.A8V.get();
            this.A0E = (C16590pI) r1.AMg.get();
            this.A09 = (C18790t3) r1.AJw.get();
            this.A0A = (C16170oZ) r1.AM4.get();
            this.A0H = (C231510o) r1.AHO.get();
            this.A0O = (C244415n) r1.AAg.get();
            this.A0B = (AnonymousClass130) r1.A41.get();
            this.A0R = (C252018m) r1.A7g.get();
            this.A0G = (C15650ng) r1.A4m.get();
            this.A0J = (C22050yP) r1.A7v.get();
            this.A0P = (WhatsAppLibLoader) r1.ANa.get();
            this.A0I = (AnonymousClass193) r1.A6S.get();
            this.A0C = (C22700zV) r1.AMN.get();
            this.A0F = (C15890o4) r1.AN1.get();
            this.A08 = (C244615p) r1.A8H.get();
            this.A0N = (C16030oK) r1.AAd.get();
            this.A0Q = (C16630pM) r1.AIc.get();
            this.A0D = (AnonymousClass131) r1.A49.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AbstractC36001jA r2 = this.A0M;
        if (r2.A0Y.A05()) {
            r2.A0Y.A04(true);
            return;
        }
        r2.A0a.A05.dismiss();
        if (r2.A0s) {
            r2.A08();
        } else {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.send_location);
        AnonymousClass3DJ r1 = new AnonymousClass3DJ(this.A09, this.A0J, ((ActivityC13810kN) this).A0D);
        C16590pI r0 = this.A0E;
        C14830m7 r02 = ((ActivityC13790kL) this).A05;
        C14850m9 r03 = ((ActivityC13810kN) this).A0C;
        C253719d r04 = this.A0K;
        C14900mE r05 = ((ActivityC13810kN) this).A05;
        C252718t r06 = ((ActivityC13790kL) this).A0D;
        AbstractC15710nm r07 = ((ActivityC13810kN) this).A03;
        C15570nT r08 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r09 = ((ActivityC13830kP) this).A05;
        C18790t3 r010 = this.A09;
        AnonymousClass19M r011 = ((ActivityC13810kN) this).A0B;
        C16170oZ r012 = this.A0A;
        C231510o r013 = this.A0H;
        AnonymousClass12P r014 = ((ActivityC13790kL) this).A00;
        C244415n r015 = this.A0O;
        AnonymousClass130 r016 = this.A0B;
        AnonymousClass01d r017 = ((ActivityC13810kN) this).A08;
        C252018m r018 = this.A0R;
        AnonymousClass018 r15 = ((ActivityC13830kP) this).A01;
        C15650ng r14 = this.A0G;
        WhatsAppLibLoader whatsAppLibLoader = this.A0P;
        AnonymousClass193 r12 = this.A0I;
        C22700zV r11 = this.A0C;
        C18810t5 r10 = ((ActivityC13810kN) this).A0D;
        C15890o4 r9 = this.A0F;
        C14820m6 r8 = ((ActivityC13810kN) this).A09;
        C618732w r019 = new C618732w(r014, r07, this.A08, r05, r08, r010, r012, r016, r11, this.A0D, r017, r02, r0, r9, r8, r15, r14, r011, r013, r12, r03, r04, r10, this, this.A0N, r015, r1, whatsAppLibLoader, this.A0Q, r018, r06, r09);
        this.A0M = r019;
        r019.A0L(bundle, this);
        this.A0M.A0D.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 29));
        AnonymousClass13N.A00(this);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.pin_location_green);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(getResources(), R.drawable.pin_location_red);
        this.A05 = new C04640Mm(decodeResource.copy(decodeResource.getConfig(), false));
        this.A06 = new C04640Mm(decodeResource2.copy(decodeResource2.getConfig(), false));
        Bitmap bitmap = this.A0M.A05;
        this.A04 = new C04640Mm(bitmap.copy(bitmap.getConfig(), false));
        AnonymousClass0O0 r13 = new AnonymousClass0O0();
        r13.A00 = 1;
        r13.A05 = true;
        r13.A02 = false;
        r13.A03 = true;
        this.A0L = new C618132q(this, r13, this);
        ((ViewGroup) AnonymousClass00T.A05(this, R.id.map_holder)).addView(this.A0L);
        this.A0L.A0E(bundle);
        this.A02 = bundle;
        if (this.A03 == null) {
            this.A03 = this.A0L.A0J(this.A0U);
        }
        this.A0M.A0S = (ImageView) AnonymousClass00T.A05(this, R.id.my_location);
        this.A0M.A0S.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 30));
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A02 = this.A0M.A02(i);
        if (A02 == null) {
            return super.onCreateDialog(i);
        }
        return A02;
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.search).setIcon(R.drawable.ic_action_search).setShowAsAction(2);
        menu.add(0, 1, 0, R.string.refresh).setIcon(R.drawable.ic_action_refresh).setShowAsAction(1);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0M.A06();
        if (this.A03 != null) {
            SharedPreferences.Editor edit = this.A0Q.A01(AnonymousClass01V.A07).edit();
            C06860Vj A02 = this.A03.A02();
            AnonymousClass03T r4 = A02.A03;
            edit.putFloat("share_location_lat", (float) r4.A00);
            edit.putFloat("share_location_lon", (float) r4.A01);
            edit.putFloat("share_location_zoom", A02.A02);
            edit.apply();
        }
        super.onDestroy();
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        this.A0L.A05();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.A0M.A0H(intent);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return this.A0M.A0U(menuItem) || super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        AnonymousClass294 r0 = this.A0L;
        SensorManager sensorManager = r0.A04;
        if (sensorManager != null) {
            sensorManager.unregisterListener(r0.A09);
        }
        AbstractC36001jA r1 = this.A0M;
        r1.A0p = r1.A18.A03();
        r1.A0x.A04(r1);
        super.onPause();
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem findItem;
        if (this.A0M.A0s) {
            menu.findItem(0).setVisible(false);
            findItem = menu.findItem(1);
        } else {
            if (!this.A0F.A03()) {
                findItem = menu.findItem(0);
            }
            return true;
        }
        findItem.setVisible(false);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        AnonymousClass04Q r1;
        super.onResume();
        if (this.A0F.A03() != this.A0M.A0p) {
            invalidateOptionsMenu();
            if (this.A0F.A03() && (r1 = this.A03) != null && !this.A0M.A0s) {
                r1.A0E(true);
            }
        }
        this.A0L.A0K();
        if (this.A03 == null) {
            this.A03 = this.A0L.A0J(this.A0U);
        }
        this.A0M.A07();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass04Q r0 = this.A03;
        if (r0 != null) {
            C06860Vj A02 = r0.A02();
            bundle.putFloat("camera_zoom", A02.A02);
            AnonymousClass03T r3 = A02.A03;
            bundle.putDouble("camera_lat", r3.A00);
            bundle.putDouble("camera_lng", r3.A01);
            bundle.putInt("map_location_mode", this.A0L.A02);
        }
        this.A0L.A0F(bundle);
        this.A0M.A0K(bundle);
        super.onSaveInstanceState(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A0M.A0Y.A01();
        return false;
    }
}
