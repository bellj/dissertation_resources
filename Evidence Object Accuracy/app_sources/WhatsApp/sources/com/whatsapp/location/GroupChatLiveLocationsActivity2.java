package com.whatsapp.location;

import X.AbstractBinderC79573qo;
import X.AbstractC115695So;
import X.AbstractC115705Sp;
import X.AbstractC115715Sq;
import X.AbstractC115725Sr;
import X.AbstractC115745St;
import X.AbstractC115755Su;
import X.AbstractC116405Vh;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC36671kL;
import X.AbstractView$OnCreateContextMenuListenerC35851ir;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass13O;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3EP;
import X.AnonymousClass3F0;
import X.AnonymousClass3T6;
import X.AnonymousClass3T7;
import X.AnonymousClass4IX;
import X.C103164qJ;
import X.C113245Gt;
import X.C13020j0;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16240og;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20830wO;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22330yu;
import X.C22670zS;
import X.C244215l;
import X.C244415n;
import X.C244615p;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C30751Yr;
import X.C35781ij;
import X.C35961j4;
import X.C35991j8;
import X.C36291je;
import X.C36311jg;
import X.C41691tw;
import X.C56442kt;
import X.C56462kv;
import X.C618232r;
import X.C618632v;
import X.C65193Io;
import X.C65873Li;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101604nn;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;
import com.whatsapp.quickcontact.QuickContactActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class GroupChatLiveLocationsActivity2 extends ActivityC13790kL {
    public float A00;
    public int A01;
    public Bundle A02;
    public MenuItem A03;
    public ImageView A04;
    public AbstractC116405Vh A05;
    public C35961j4 A06;
    public C244615p A07;
    public C16240og A08;
    public C22330yu A09;
    public AnonymousClass130 A0A;
    public C15550nR A0B;
    public AnonymousClass10S A0C;
    public C15610nY A0D;
    public C21270x9 A0E;
    public AnonymousClass131 A0F;
    public C15890o4 A0G;
    public C20830wO A0H;
    public C15600nX A0I;
    public AnonymousClass12H A0J;
    public C244215l A0K;
    public C618632v A0L;
    public AbstractView$OnCreateContextMenuListenerC35851ir A0M;
    public C16030oK A0N;
    public AnonymousClass13O A0O;
    public C244415n A0P;
    public C16630pM A0Q;
    public AnonymousClass12F A0R;
    public AnonymousClass19Z A0S;
    public Map A0T;
    public Set A0U;
    public boolean A0V;
    public final AbstractC115755Su A0W;
    public volatile boolean A0X;
    public volatile boolean A0Y;

    public GroupChatLiveLocationsActivity2() {
        this(0);
        this.A0U = new HashSet();
        this.A0T = new HashMap();
        this.A01 = 0;
        this.A00 = -1.0f;
        this.A0Y = false;
        this.A05 = new AnonymousClass3T6(this);
        this.A0W = new AbstractC115755Su() { // from class: X.50j
            @Override // X.AbstractC115755Su
            public final void ASP(C35961j4 r2) {
                GroupChatLiveLocationsActivity2.A03(r2, GroupChatLiveLocationsActivity2.this);
            }
        };
    }

    public GroupChatLiveLocationsActivity2(int i) {
        this.A0V = false;
        A0R(new C103164qJ(this));
    }

    public static /* synthetic */ float A02(GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2, float f, float f2) {
        if (f <= 0.0f) {
            return f2;
        }
        C35961j4 r0 = groupChatLiveLocationsActivity2.A06;
        AnonymousClass009.A05(r0);
        C56442kt A02 = r0.A00().A02();
        Location location = new Location("");
        LatLng latLng = A02.A02;
        location.setLatitude(latLng.A00);
        location.setLongitude(latLng.A01);
        Location location2 = new Location("");
        LatLng latLng2 = A02.A03;
        location2.setLatitude(latLng2.A00);
        location2.setLongitude(latLng2.A01);
        double distanceTo = (double) location2.distanceTo(location);
        if (distanceTo <= 0.0d) {
            return f2;
        }
        float log = (float) (((double) groupChatLiveLocationsActivity2.A06.A02().A02) + (Math.log((distanceTo / ((double) f)) / 30.0d) / Math.log(2.0d)));
        if (log > 16.0f) {
            return 16.0f;
        }
        return log;
    }

    public static /* synthetic */ void A03(C35961j4 r6, GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        if (groupChatLiveLocationsActivity2.A06 == null) {
            groupChatLiveLocationsActivity2.A06 = r6;
            if (r6 != null) {
                r6.A08(0, 0, 0, groupChatLiveLocationsActivity2.A01);
                groupChatLiveLocationsActivity2.A01 = 0;
                AnonymousClass009.A05(groupChatLiveLocationsActivity2.A06);
                C16630pM r0 = groupChatLiveLocationsActivity2.A0Q;
                String str = AnonymousClass01V.A07;
                int i = 0;
                boolean z = r0.A01(str).getBoolean("live_location_show_traffic", false);
                groupChatLiveLocationsActivity2.A06.A0M(z);
                MenuItem menuItem = groupChatLiveLocationsActivity2.A03;
                if (menuItem != null) {
                    menuItem.setChecked(z);
                }
                groupChatLiveLocationsActivity2.A06.A07(groupChatLiveLocationsActivity2.A0Q.A01(str).getInt("live_location_map_type", 1));
                groupChatLiveLocationsActivity2.A06.A0K(true);
                try {
                    C65873Li r5 = (C65873Li) groupChatLiveLocationsActivity2.A06.A01().A00;
                    Parcel A01 = r5.A01();
                    A01.writeInt(1);
                    r5.A03(2, A01);
                    try {
                        C65873Li r1 = (C65873Li) groupChatLiveLocationsActivity2.A06.A01().A00;
                        Parcel A012 = r1.A01();
                        A012.writeInt(0);
                        r1.A03(1, A012);
                        groupChatLiveLocationsActivity2.A06.A01().A00();
                        groupChatLiveLocationsActivity2.A06.A0D(new AnonymousClass3T7(groupChatLiveLocationsActivity2));
                        groupChatLiveLocationsActivity2.A06.A0I(new AbstractC115745St() { // from class: X.3TI
                            @Override // X.AbstractC115745St
                            public final boolean ASR(C36311jg r7) {
                                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = GroupChatLiveLocationsActivity2.this;
                                AnonymousClass009.A05(groupChatLiveLocationsActivity22.A06);
                                AbstractView$OnCreateContextMenuListenerC35851ir r02 = groupChatLiveLocationsActivity22.A0M;
                                r02.A0u = true;
                                r02.A0s = false;
                                View view = r02.A0U;
                                C30751Yr r12 = r02.A0m;
                                int i2 = 8;
                                if (r12 == null) {
                                    i2 = 0;
                                }
                                view.setVisibility(i2);
                                if (r7.A01() instanceof C35991j8) {
                                    C35991j8 r2 = (C35991j8) r7.A01();
                                    if (!r7.A0A()) {
                                        r2 = groupChatLiveLocationsActivity22.A0M.A08((C30751Yr) r2.A04.get(0));
                                        if (r2 != null) {
                                            r7 = (C36311jg) groupChatLiveLocationsActivity22.A0T.get(r2.A03);
                                        }
                                    }
                                    if (r2.A00 != 1) {
                                        List list = r2.A04;
                                        if (list.size() == 1) {
                                            groupChatLiveLocationsActivity22.A0M.A0R(r2, true);
                                            r7.A04();
                                            return true;
                                        } else if (groupChatLiveLocationsActivity22.A06.A02().A02 >= 16.0f) {
                                            groupChatLiveLocationsActivity22.A0M.A0R(r2, true);
                                            return true;
                                        } else {
                                            groupChatLiveLocationsActivity22.A2h(list, true);
                                            groupChatLiveLocationsActivity22.A0M.A0j = new C90294Ni(list, groupChatLiveLocationsActivity22.A06.A02().A02);
                                            return true;
                                        }
                                    }
                                }
                                groupChatLiveLocationsActivity22.A0M.A0C();
                                return true;
                            }
                        });
                        groupChatLiveLocationsActivity2.A06.A0F(new AbstractC115705Sp() { // from class: X.3TD
                            @Override // X.AbstractC115705Sp
                            public final void ANd(int i2) {
                                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = GroupChatLiveLocationsActivity2.this;
                                if (i2 == 1) {
                                    AnonymousClass009.A05(groupChatLiveLocationsActivity22.A06);
                                    AbstractView$OnCreateContextMenuListenerC35851ir r02 = groupChatLiveLocationsActivity22.A0M;
                                    r02.A0u = true;
                                    int i3 = 0;
                                    r02.A0s = false;
                                    View view = r02.A0U;
                                    if (r02.A0m != null) {
                                        i3 = 8;
                                    }
                                    view.setVisibility(i3);
                                    groupChatLiveLocationsActivity22.A06.A04();
                                    groupChatLiveLocationsActivity22.A0M.A0t = true;
                                }
                            }
                        });
                        groupChatLiveLocationsActivity2.A06.A0E(new AbstractC115695So() { // from class: X.3TA
                            @Override // X.AbstractC115695So
                            public final void ANc() {
                                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = GroupChatLiveLocationsActivity2.this;
                                C35961j4 r02 = groupChatLiveLocationsActivity22.A06;
                                AnonymousClass009.A05(r02);
                                CameraPosition A02 = r02.A02();
                                if (A02 != null) {
                                    int i2 = (int) (groupChatLiveLocationsActivity22.A00 * 5.0f);
                                    float f = A02.A02;
                                    if (i2 != ((int) (5.0f * f))) {
                                        groupChatLiveLocationsActivity22.A00 = f;
                                        groupChatLiveLocationsActivity22.A2f();
                                    }
                                    AbstractView$OnCreateContextMenuListenerC35851ir r12 = groupChatLiveLocationsActivity22.A0M;
                                    if (r12.A0j != null) {
                                        r12.A0V(null);
                                    }
                                    AbstractView$OnCreateContextMenuListenerC35851ir r03 = groupChatLiveLocationsActivity22.A0M;
                                    C35991j8 r13 = r03.A0l;
                                    if (r13 != null && r03.A0t && groupChatLiveLocationsActivity22.A2j(r13.A00())) {
                                        groupChatLiveLocationsActivity22.A0M.A0C();
                                    }
                                }
                            }
                        });
                        groupChatLiveLocationsActivity2.A06.A0H(new AbstractC115725Sr() { // from class: X.3TG
                            @Override // X.AbstractC115725Sr
                            public final void ASM(LatLng latLng) {
                                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = GroupChatLiveLocationsActivity2.this;
                                AbstractView$OnCreateContextMenuListenerC35851ir r12 = groupChatLiveLocationsActivity22.A0M;
                                if (r12.A0l != null) {
                                    r12.A0C();
                                    return;
                                }
                                C35991j8 A07 = r12.A07(latLng);
                                if (A07 != null) {
                                    List list = A07.A04;
                                    if (list.size() == 1) {
                                        groupChatLiveLocationsActivity22.A0M.A0R(A07, true);
                                        ((C36311jg) groupChatLiveLocationsActivity22.A0T.get(A07.A03)).A04();
                                    } else if (groupChatLiveLocationsActivity22.A06.A02().A02 >= 16.0f) {
                                        groupChatLiveLocationsActivity22.A0M.A0R(A07, true);
                                    } else {
                                        groupChatLiveLocationsActivity22.A2h(list, true);
                                        groupChatLiveLocationsActivity22.A0M.A0j = new C90294Ni(list, groupChatLiveLocationsActivity22.A06.A02().A02);
                                    }
                                }
                            }
                        });
                        groupChatLiveLocationsActivity2.A06.A0G(new AbstractC115715Sq() { // from class: X.3TF
                            @Override // X.AbstractC115715Sq
                            public final void ARO(C36311jg r7) {
                                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = GroupChatLiveLocationsActivity2.this;
                                C35991j8 r02 = (C35991j8) r7.A01();
                                if (r02 != null) {
                                    C15570nT r12 = ((ActivityC13790kL) groupChatLiveLocationsActivity22).A01;
                                    UserJid userJid = r02.A02.A06;
                                    if (!r12.A0F(userJid)) {
                                        Intent A0D = C12990iw.A0D(groupChatLiveLocationsActivity22, QuickContactActivity.class);
                                        groupChatLiveLocationsActivity22.A0L.getLocationOnScreen(C13000ix.A07());
                                        LatLng A00 = r7.A00();
                                        C35961j4 r03 = groupChatLiveLocationsActivity22.A06;
                                        AnonymousClass009.A05(r03);
                                        Point A002 = r03.A00().A00(A00);
                                        Rect A0J = C12980iv.A0J();
                                        int i2 = A002.x;
                                        A0J.left = i2;
                                        int i3 = A002.y;
                                        A0J.top = i3;
                                        A0J.right = i2;
                                        A0J.bottom = i3;
                                        A0D.setSourceBounds(A0J);
                                        C12980iv.A13(A0D, userJid);
                                        A0D.putExtra("gjid", groupChatLiveLocationsActivity22.A0M.A0c.getRawString());
                                        A0D.putExtra("show_get_direction", true);
                                        C30751Yr r04 = groupChatLiveLocationsActivity22.A0M.A0m;
                                        if (r04 != null) {
                                            A0D.putExtra("location_latitude", r04.A00);
                                            A0D.putExtra("location_longitude", groupChatLiveLocationsActivity22.A0M.A0m.A01);
                                        }
                                        groupChatLiveLocationsActivity22.startActivity(A0D);
                                    }
                                }
                            }
                        });
                        groupChatLiveLocationsActivity2.A2f();
                        if (groupChatLiveLocationsActivity2.A02 != null) {
                            AbstractView$OnCreateContextMenuListenerC35851ir r2 = groupChatLiveLocationsActivity2.A0M;
                            View view = r2.A0U;
                            if (!r2.A0u || r2.A0m != null) {
                                i = 8;
                            }
                            view.setVisibility(i);
                            groupChatLiveLocationsActivity2.A0L.setLocationMode(groupChatLiveLocationsActivity2.A02.getInt("map_location_mode", 2));
                            if (groupChatLiveLocationsActivity2.A02.containsKey("camera_zoom")) {
                                groupChatLiveLocationsActivity2.A06.A0A(C65193Io.A02(new LatLng(groupChatLiveLocationsActivity2.A02.getDouble("camera_lat"), groupChatLiveLocationsActivity2.A02.getDouble("camera_lng")), groupChatLiveLocationsActivity2.A02.getFloat("camera_zoom")));
                            }
                            groupChatLiveLocationsActivity2.A02 = null;
                        } else if (groupChatLiveLocationsActivity2.A0U.isEmpty()) {
                            SharedPreferences A013 = groupChatLiveLocationsActivity2.A0Q.A01(str);
                            groupChatLiveLocationsActivity2.A06.A0A(C65193Io.A01(new LatLng((double) A013.getFloat("live_location_lat", 37.389805f), (double) A013.getFloat("live_location_lng", -122.08141f))));
                            C35961j4 r4 = groupChatLiveLocationsActivity2.A06;
                            float f = A013.getFloat("live_location_zoom", 16.0f) - 0.2f;
                            try {
                                ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate = C65193Io.A00;
                                C13020j0.A02(iCameraUpdateFactoryDelegate, "CameraUpdateFactory is not initialized");
                                C65873Li r22 = (C65873Li) iCameraUpdateFactoryDelegate;
                                Parcel A014 = r22.A01();
                                A014.writeFloat(f);
                                Parcel A02 = r22.A02(4, A014);
                                IObjectWrapper A015 = AbstractBinderC79573qo.A01(A02.readStrongBinder());
                                A02.recycle();
                                r4.A0A(new AnonymousClass4IX(A015));
                            } catch (RemoteException e) {
                                throw new C113245Gt(e);
                            }
                        } else {
                            groupChatLiveLocationsActivity2.A2i(false);
                        }
                        if (C41691tw.A08(groupChatLiveLocationsActivity2)) {
                            groupChatLiveLocationsActivity2.A06.A0J(C56462kv.A00(groupChatLiveLocationsActivity2, R.raw.night_map_style_json));
                        }
                    } catch (RemoteException e2) {
                        throw new C113245Gt(e2);
                    }
                } catch (RemoteException e3) {
                    throw new C113245Gt(e3);
                }
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0V) {
            this.A0V = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0S = (AnonymousClass19Z) r1.A2o.get();
            this.A0E = (C21270x9) r1.A4A.get();
            this.A0P = (C244415n) r1.AAg.get();
            this.A0A = (AnonymousClass130) r1.A41.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A0C = (AnonymousClass10S) r1.A46.get();
            this.A0J = (AnonymousClass12H) r1.AC5.get();
            this.A0R = (AnonymousClass12F) r1.AJM.get();
            this.A09 = (C22330yu) r1.A3I.get();
            this.A0G = (C15890o4) r1.AN1.get();
            this.A07 = (C244615p) r1.A8H.get();
            this.A0N = (C16030oK) r1.AAd.get();
            this.A0I = (C15600nX) r1.A8x.get();
            this.A0Q = (C16630pM) r1.AIc.get();
            this.A0H = (C20830wO) r1.A4W.get();
            this.A0F = (AnonymousClass131) r1.A49.get();
            this.A0K = (C244215l) r1.A8y.get();
            this.A0O = (AnonymousClass13O) r1.AAf.get();
            this.A08 = (C16240og) r1.ANq.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        if (r3.A0G.A03() == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2e() {
        /*
            r3 = this;
            X.AnonymousClass009.A01()
            X.1j4 r0 = r3.A06
            if (r0 != 0) goto L_0x0011
            X.32v r1 = r3.A0L
            X.5Su r0 = r3.A0W
            X.1j4 r0 = r1.A07(r0)
            r3.A06 = r0
        L_0x0011:
            android.widget.ImageView r2 = r3.A04
            X.1ir r0 = r3.A0M
            X.1Yr r0 = r0.A0m
            if (r0 != 0) goto L_0x0022
            X.0o4 r0 = r3.A0G
            boolean r1 = r0.A03()
            r0 = 0
            if (r1 != 0) goto L_0x0024
        L_0x0022:
            r0 = 8
        L_0x0024:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.GroupChatLiveLocationsActivity2.A2e():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2f() {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.GroupChatLiveLocationsActivity2.A2f():void");
    }

    public final void A2g(AnonymousClass3EP r6, boolean z) {
        AnonymousClass009.A05(this.A06);
        LatLngBounds A00 = r6.A00();
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.group_chat_live_location_map_view_bound_size);
        int i = dimensionPixelSize << 1;
        if (this.A0L.getHeight() > i && this.A0L.getWidth() > i) {
            if (!z) {
                this.A06.A05();
                this.A06.A0A(C65193Io.A03(A00, dimensionPixelSize));
                this.A0L.postDelayed(new RunnableBRunnable0Shape7S0100000_I0_7(this, 27), 500);
            } else if (!this.A0X) {
                this.A0X = true;
                this.A06.A05();
                this.A06.A0B(C65193Io.A03(A00, dimensionPixelSize), this.A05);
            }
        }
    }

    public final void A2h(List list, boolean z) {
        AnonymousClass009.A05(this.A06);
        if (list.size() != 1) {
            AnonymousClass3EP r6 = new AnonymousClass3EP();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C30751Yr r0 = (C30751Yr) it.next();
                r6.A01(new LatLng(r0.A00, r0.A01));
            }
            A2g(r6, z);
        } else if (!z) {
            this.A06.A0A(C65193Io.A02(new LatLng(((C30751Yr) list.get(0)).A00, ((C30751Yr) list.get(0)).A01), 16.0f));
        } else if (!this.A0X) {
            this.A0X = true;
            this.A06.A0B(C65193Io.A02(new LatLng(((C30751Yr) list.get(0)).A00, ((C30751Yr) list.get(0)).A01), 16.0f), this.A05);
        }
    }

    public final void A2i(boolean z) {
        if (this.A06 != null && !this.A0M.A0u) {
            Set set = this.A0U;
            if (set.isEmpty()) {
                return;
            }
            if (this.A0L.getWidth() <= 0 || this.A0L.getHeight() <= 0) {
                this.A0L.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101604nn(this));
            } else if (!z || !this.A0X) {
                ArrayList arrayList = new ArrayList(set);
                AnonymousClass009.A05(this.A06);
                if (!arrayList.isEmpty()) {
                    LatLng A06 = this.A0M.A06();
                    if (A06 != null) {
                        Collections.sort(arrayList, new Comparator(A06.A00, A06.A01) { // from class: X.5Co
                            public final /* synthetic */ double A00;
                            public final /* synthetic */ double A01;

                            {
                                this.A00 = r1;
                                this.A01 = r3;
                            }

                            public static double A00(C36311jg r6, double d, double d2) {
                                return ((r6.A00().A00 - d) * (r6.A00().A00 - d)) + ((r6.A00().A01 - d2) * (r6.A00().A01 - d2));
                            }

                            @Override // java.util.Comparator
                            public final int compare(Object obj, Object obj2) {
                                double d = this.A00;
                                double d2 = this.A01;
                                return Double.compare(A00((C36311jg) obj, d, d2), A00((C36311jg) obj2, d, d2));
                            }
                        });
                    }
                    AnonymousClass3EP r6 = new AnonymousClass3EP();
                    AnonymousClass3EP r5 = new AnonymousClass3EP();
                    r5.A01(((C36311jg) arrayList.get(0)).A00());
                    r6.A01(((C36311jg) arrayList.get(0)).A00());
                    int i = 1;
                    while (i < arrayList.size()) {
                        C36311jg r1 = (C36311jg) arrayList.get(i);
                        r5.A01(r1.A00());
                        if (!AbstractView$OnCreateContextMenuListenerC35851ir.A03(r5.A00())) {
                            break;
                        }
                        r6.A01(r1.A00());
                        i++;
                    }
                    if (i == 1) {
                        Object A01 = ((C36311jg) arrayList.get(0)).A01();
                        AnonymousClass009.A05(A01);
                        A2h(((C35991j8) A01).A04, z);
                        return;
                    }
                    A2g(r6, z);
                }
            } else {
                this.A0Y = true;
            }
        }
    }

    public final boolean A2j(LatLng latLng) {
        C35961j4 r0 = this.A06;
        AnonymousClass009.A05(r0);
        AnonymousClass3F0 A00 = r0.A00();
        if (A00.A02().A04.A00(latLng)) {
            return false;
        }
        if (latLng.A00 >= A00.A02().A04.A01.A00) {
            return true;
        }
        Point A002 = A00.A00(A00.A02().A04.A01);
        A002.offset(0, this.A0M.A0A);
        return !new LatLngBounds(A00.A01(A002), A00.A02().A04.A00).A00(latLng);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.A0M.A0Y(i, i2)) {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C15570nT r03 = ((ActivityC13790kL) this).A01;
        AnonymousClass19Z r04 = this.A0S;
        AnonymousClass12P r05 = ((ActivityC13790kL) this).A00;
        C21270x9 r06 = this.A0E;
        C244415n r07 = this.A0P;
        AnonymousClass130 r08 = this.A0A;
        C15550nR r15 = this.A0B;
        C15610nY r14 = this.A0D;
        AnonymousClass018 r13 = ((ActivityC13830kP) this).A01;
        AnonymousClass10S r12 = this.A0C;
        AnonymousClass12H r11 = this.A0J;
        C16240og r10 = this.A08;
        C22330yu r9 = this.A09;
        C15890o4 r8 = this.A0G;
        this.A0M = new C36291je(r05, this.A07, r02, r03, r10, r9, r08, r15, r12, r14, r06, this.A0F, r0, r8, r13, r11, this.A0K, this, this.A0N, this.A0O, r07, r04);
        A1U().A0M(true);
        setContentView(R.layout.groupchat_live_locations);
        C20830wO r2 = this.A0H;
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        C15370n3 A012 = r2.A01(A01);
        A1U().A0I(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, this.A0D.A04(A012)));
        this.A0M.A0O(this, bundle);
        C35781ij.A00(this);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        googleMapOptions.A00 = 1;
        googleMapOptions.A05 = false;
        googleMapOptions.A08 = true;
        googleMapOptions.A06 = true;
        googleMapOptions.A0A = true;
        googleMapOptions.A09 = true;
        this.A0L = new C618232r(this, googleMapOptions, this);
        ((ViewGroup) AnonymousClass00T.A05(this, R.id.map_holder)).addView(this.A0L);
        this.A0L.A04(bundle);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.my_location);
        this.A04 = imageView;
        imageView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 28));
        this.A02 = bundle;
        A2e();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A04 = this.A0M.A04(i);
        if (A04 == null) {
            return super.onCreateDialog(i);
        }
        return A04;
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C35961j4 r0;
        getMenuInflater().inflate(R.menu.map_layers, menu);
        MenuItem findItem = menu.findItem(R.id.map_traffic);
        this.A03 = findItem;
        if (findItem == null || (r0 = this.A06) == null) {
            return true;
        }
        findItem.setChecked(r0.A0N());
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0L.A00();
        this.A0M.A0D();
        if (this.A06 != null) {
            SharedPreferences.Editor edit = this.A0Q.A01(AnonymousClass01V.A07).edit();
            CameraPosition A02 = this.A06.A02();
            LatLng latLng = A02.A03;
            edit.putFloat("live_location_lat", (float) latLng.A00);
            edit.putFloat("live_location_lng", (float) latLng.A01);
            edit.putFloat("live_location_zoom", A02.A02);
            edit.apply();
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        this.A0L.A01();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        SharedPreferences.Editor putBoolean;
        C35961j4 r0;
        int i;
        if (this.A06 != null) {
            int itemId = menuItem.getItemId();
            if (itemId == R.id.map_type_normal) {
                this.A06.A07(1);
                putBoolean = this.A0Q.A01(AnonymousClass01V.A07).edit().putInt("live_location_map_type", 1);
            } else {
                if (itemId == R.id.map_type_satellite) {
                    r0 = this.A06;
                    i = 4;
                } else if (itemId == R.id.map_type_terrain) {
                    r0 = this.A06;
                    i = 3;
                } else if (itemId == R.id.map_traffic) {
                    boolean z = !this.A06.A0N();
                    this.A06.A0M(z);
                    this.A03.setChecked(z);
                    putBoolean = this.A0Q.A01(AnonymousClass01V.A07).edit().putBoolean("live_location_show_traffic", z);
                } else if (itemId == 16908332) {
                    finish();
                    return true;
                }
                r0.A07(i);
                putBoolean = this.A0Q.A01(AnonymousClass01V.A07).edit().putInt("live_location_map_type", i);
            }
            putBoolean.apply();
            return true;
        }
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A0L.A02();
        C618632v r0 = this.A0L;
        SensorManager sensorManager = r0.A05;
        if (sensorManager != null) {
            sensorManager.unregisterListener(r0.A0C);
        }
        this.A0M.A0E();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0L.A03();
        this.A0L.A08();
        this.A0M.A0F();
        A2e();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        C35961j4 r0 = this.A06;
        if (r0 != null) {
            CameraPosition A02 = r0.A02();
            bundle.putFloat("camera_zoom", A02.A02);
            LatLng latLng = A02.A03;
            bundle.putDouble("camera_lat", latLng.A00);
            bundle.putDouble("camera_lng", latLng.A01);
            bundle.putInt("map_location_mode", this.A0L.A03);
        }
        this.A0L.A05(bundle);
        this.A0M.A0P(bundle);
        super.onSaveInstanceState(bundle);
    }
}
