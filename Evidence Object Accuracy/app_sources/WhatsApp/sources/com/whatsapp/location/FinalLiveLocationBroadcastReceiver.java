package com.whatsapp.location;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C16030oK;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class FinalLiveLocationBroadcastReceiver extends BroadcastReceiver {
    public C16030oK A00;
    public final Object A01;
    public volatile boolean A02;

    public FinalLiveLocationBroadcastReceiver() {
        this(0);
    }

    public FinalLiveLocationBroadcastReceiver(int i) {
        this.A02 = false;
        this.A01 = new Object();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = (C16030oK) ((AnonymousClass01J) AnonymousClass22D.A00(context)).AAd.get();
                    this.A02 = true;
                }
            }
        }
        Log.i("FinalLiveLocationBroadcastReceiver/onReceive");
        this.A00.A0E();
    }
}
