package com.whatsapp.location;

import X.AbstractC115695So;
import X.AbstractC115705Sp;
import X.AbstractC115715Sq;
import X.AbstractC115725Sr;
import X.AbstractC115745St;
import X.AbstractC115755Su;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC36001jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass11P;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass193;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1A1;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass32x;
import X.AnonymousClass3DJ;
import X.AnonymousClass3FV;
import X.AnonymousClass3T8;
import X.AnonymousClass454;
import X.C103194qM;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16170oZ;
import X.C16590pI;
import X.C16630pM;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22670zS;
import X.C22700zV;
import X.C231510o;
import X.C244415n;
import X.C244615p;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C253719d;
import X.C35781ij;
import X.C35961j4;
import X.C36011jB;
import X.C36311jg;
import X.C36321jh;
import X.C36331ji;
import X.C40691s6;
import X.C41691tw;
import X.C56462kv;
import X.C56482kx;
import X.C618632v;
import X.C65193Io;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.R;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.location.PlaceInfo;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* loaded from: classes2.dex */
public class LocationPicker2 extends ActivityC13790kL {
    public Bundle A00;
    public View A01;
    public C35961j4 A02;
    public C36331ji A03;
    public C36331ji A04;
    public C36331ji A05;
    public C36311jg A06;
    public C244615p A07;
    public C18790t3 A08;
    public C16170oZ A09;
    public AnonymousClass130 A0A;
    public C15550nR A0B;
    public C22700zV A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public C21270x9 A0F;
    public AnonymousClass131 A0G;
    public AnonymousClass19D A0H;
    public AnonymousClass11P A0I;
    public C16590pI A0J;
    public C15890o4 A0K;
    public C15650ng A0L;
    public C231510o A0M;
    public AnonymousClass193 A0N;
    public C22050yP A0O;
    public C253719d A0P;
    public AnonymousClass3FV A0Q;
    public C618632v A0R;
    public AbstractC36001jA A0S;
    public C16030oK A0T;
    public C244415n A0U;
    public WhatsAppLibLoader A0V;
    public C16630pM A0W;
    public C252018m A0X;
    public AnonymousClass01H A0Y;
    public AnonymousClass01H A0Z;
    public boolean A0a;
    public final AbstractC115755Su A0b;

    public LocationPicker2() {
        this(0);
        this.A0b = new AbstractC115755Su() { // from class: X.50k
            @Override // X.AbstractC115755Su
            public final void ASP(C35961j4 r2) {
                LocationPicker2.A02(r2, LocationPicker2.this);
            }
        };
    }

    public LocationPicker2(int i) {
        this.A0a = false;
        A0R(new C103194qM(this));
    }

    public static /* synthetic */ void A02(C35961j4 r7, LocationPicker2 locationPicker2) {
        if (locationPicker2.A02 == null) {
            locationPicker2.A02 = r7;
            if (r7 != null) {
                AnonymousClass009.A05(r7);
                locationPicker2.A0Q = new AnonymousClass3FV(r7);
                r7.A0M(false);
                locationPicker2.A02.A0K(true);
                if (locationPicker2.A0K.A03() && !locationPicker2.A0S.A0s) {
                    locationPicker2.A02.A0L(true);
                }
                C35961j4 r3 = locationPicker2.A02;
                AbstractC36001jA r0 = locationPicker2.A0S;
                r3.A08(0, 0, 0, Math.max(r0.A00, r0.A02));
                locationPicker2.A02.A01().A00();
                locationPicker2.A02.A0D(new AnonymousClass3T8(locationPicker2));
                locationPicker2.A02.A0I(new AbstractC115745St() { // from class: X.3TJ
                    @Override // X.AbstractC115745St
                    public final boolean ASR(C36311jg r4) {
                        Object obj;
                        LocationPicker2 locationPicker22 = LocationPicker2.this;
                        if (locationPicker22.A0S.A0s) {
                            return true;
                        }
                        if (r4.A02() == null) {
                            return false;
                        }
                        PlaceInfo placeInfo = locationPicker22.A0S.A0f;
                        if (!(placeInfo == null || (obj = placeInfo.A0D) == null)) {
                            C36311jg r1 = (C36311jg) obj;
                            r1.A05(locationPicker22.A04);
                            r1.A03();
                        }
                        r4.A05(locationPicker22.A05);
                        locationPicker22.A0S.A0Q(r4);
                        locationPicker22.A0S.A0B.setVisibility(8);
                        locationPicker22.A0S.A0E.setVisibility(8);
                        if (!locationPicker22.A0S.A0n && locationPicker22.A0K.A03()) {
                            return true;
                        }
                        r4.A04();
                        return true;
                    }
                });
                locationPicker2.A02.A0G(new AbstractC115715Sq() { // from class: X.50f
                    @Override // X.AbstractC115715Sq
                    public final void ARO(C36311jg r32) {
                        LocationPicker2.this.A0S.A0R(r32.A02(), r32);
                    }
                });
                locationPicker2.A02.A0H(new AbstractC115725Sr() { // from class: X.3TH
                    @Override // X.AbstractC115725Sr
                    public final void ASM(LatLng latLng) {
                        LocationPicker2 locationPicker22 = LocationPicker2.this;
                        PlaceInfo placeInfo = locationPicker22.A0S.A0f;
                        if (placeInfo != null) {
                            Object obj = placeInfo.A0D;
                            if (obj != null) {
                                ((C36311jg) obj).A05(locationPicker22.A04);
                            }
                            AbstractC36001jA r1 = locationPicker22.A0S;
                            r1.A0f = null;
                            r1.A0D();
                        }
                        AbstractC36001jA r12 = locationPicker22.A0S;
                        if (r12.A0n) {
                            r12.A0E.setVisibility(0);
                        }
                        locationPicker22.A0S.A0B.setVisibility(8);
                    }
                });
                locationPicker2.A02.A0F(new AbstractC115705Sp() { // from class: X.3TE
                    @Override // X.AbstractC115705Sp
                    public final void ANd(int i) {
                        LocationPicker2 locationPicker22 = LocationPicker2.this;
                        if (i == 1) {
                            AbstractC36001jA r1 = locationPicker22.A0S;
                            if (r1.A0s) {
                                r1.A0S.setImageResource(R.drawable.btn_myl);
                                locationPicker22.A0S.A0r = false;
                            } else {
                                PlaceInfo placeInfo = r1.A0f;
                                if (placeInfo != null) {
                                    Object obj = placeInfo.A0D;
                                    if (obj != null) {
                                        C36311jg r12 = (C36311jg) obj;
                                        r12.A05(locationPicker22.A04);
                                        r12.A03();
                                    }
                                    AbstractC36001jA r13 = locationPicker22.A0S;
                                    r13.A0f = null;
                                    r13.A0D();
                                }
                                AbstractC36001jA r14 = locationPicker22.A0S;
                                if (r14.A0n) {
                                    r14.A0C.setVisibility(0);
                                    locationPicker22.A0S.A0D.startAnimation(C12960it.A0H(locationPicker22.A0S.A0C.getHeight()));
                                    locationPicker22.A0S.A0E.setVisibility(0);
                                    locationPicker22.A0S.A0B.setVisibility(8);
                                }
                            }
                        }
                        AbstractC36001jA r15 = locationPicker22.A0S;
                        if (r15.A0r) {
                            r15.A0B.setVisibility(8);
                        }
                        View findViewById = locationPicker22.findViewById(R.id.map_center_address);
                        View findViewById2 = locationPicker22.findViewById(R.id.location_description);
                        if (findViewById != null) {
                            findViewById.setVisibility(8);
                        }
                        if (findViewById2 != null && locationPicker22.A0S.A0n) {
                            findViewById2.setVisibility(8);
                        }
                    }
                });
                locationPicker2.A02.A0E(new AbstractC115695So() { // from class: X.3TB
                    @Override // X.AbstractC115695So
                    public final void ANc() {
                        LatLng latLng;
                        LocationPicker2 locationPicker22 = LocationPicker2.this;
                        if (locationPicker22.A0S.A0C.getVisibility() == 0) {
                            locationPicker22.A0S.A0C.setVisibility(8);
                            locationPicker22.A0S.A0D.startAnimation(C12960it.A0H(-locationPicker22.A0S.A0C.getHeight()));
                        }
                        C35961j4 r02 = locationPicker22.A02;
                        AnonymousClass009.A05(r02);
                        CameraPosition A02 = r02.A02();
                        if (A02 != null && (latLng = A02.A03) != null) {
                            locationPicker22.A0S.A0E(latLng.A00, latLng.A01);
                        }
                    }
                });
                locationPicker2.A0S.A0O(null, false);
                AbstractC36001jA r1 = locationPicker2.A0S;
                C36011jB r02 = r1.A0g;
                if (r02 != null && !r02.A08.isEmpty()) {
                    r1.A05();
                }
                Bundle bundle = locationPicker2.A00;
                if (bundle != null) {
                    locationPicker2.A0R.setLocationMode(bundle.getInt("map_location_mode", 2));
                    if (locationPicker2.A00.containsKey("camera_zoom")) {
                        locationPicker2.A02.A0A(C65193Io.A02(new LatLng(locationPicker2.A00.getDouble("camera_lat"), locationPicker2.A00.getDouble("camera_lng")), locationPicker2.A00.getFloat("camera_zoom")));
                    }
                    locationPicker2.A00 = null;
                } else {
                    SharedPreferences A01 = locationPicker2.A0W.A01(AnonymousClass01V.A07);
                    locationPicker2.A02.A0A(C65193Io.A02(new LatLng((double) A01.getFloat("share_location_lat", 37.389805f), (double) A01.getFloat("share_location_lon", -122.08141f)), A01.getFloat("share_location_zoom", 15.0f) - 0.2f));
                }
                if (C41691tw.A08(locationPicker2)) {
                    locationPicker2.A02.A0J(C56462kv.A00(locationPicker2, R.raw.night_map_style_json));
                }
            }
        }
    }

    public static /* synthetic */ void A03(LatLng latLng, LocationPicker2 locationPicker2) {
        C35961j4 r2 = locationPicker2.A02;
        AnonymousClass009.A05(r2);
        C36311jg r0 = locationPicker2.A06;
        if (r0 == null) {
            C56482kx r1 = new C56482kx();
            r1.A08 = latLng;
            r1.A07 = locationPicker2.A03;
            locationPicker2.A06 = r2.A03(r1);
            return;
        }
        r0.A06(latLng);
        locationPicker2.A06.A09(true);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0a) {
            this.A0a = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0P = (C253719d) r1.A8V.get();
            this.A0J = (C16590pI) r1.AMg.get();
            this.A08 = (C18790t3) r1.AJw.get();
            this.A09 = (C16170oZ) r1.AM4.get();
            this.A0M = (C231510o) r1.AHO.get();
            this.A0F = (C21270x9) r1.A4A.get();
            this.A0U = (C244415n) r1.AAg.get();
            this.A0A = (AnonymousClass130) r1.A41.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0X = (C252018m) r1.A7g.get();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A0L = (C15650ng) r1.A4m.get();
            this.A0O = (C22050yP) r1.A7v.get();
            this.A0V = (WhatsAppLibLoader) r1.ANa.get();
            this.A0N = (AnonymousClass193) r1.A6S.get();
            this.A0C = (C22700zV) r1.AMN.get();
            this.A0K = (C15890o4) r1.AN1.get();
            this.A07 = (C244615p) r1.A8H.get();
            this.A0T = (C16030oK) r1.AAd.get();
            this.A0W = (C16630pM) r1.AIc.get();
            this.A0H = (AnonymousClass19D) r1.ABo.get();
            this.A0G = (AnonymousClass131) r1.A49.get();
            this.A0I = (AnonymousClass11P) r1.ABp.get();
            this.A0Y = C18000rk.A00(r1.ADw);
            this.A0Z = C18000rk.A00(r1.AID);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            this.A0Y.get();
        }
        AbstractC36001jA r2 = this.A0S;
        if (r2.A0Y.A05()) {
            r2.A0Y.A04(true);
            return;
        }
        r2.A0a.A05.dismiss();
        if (r2.A0s) {
            r2.A08();
        } else {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.send_location);
        AnonymousClass3DJ r1 = new AnonymousClass3DJ(this.A08, this.A0O, ((ActivityC13810kN) this).A0D);
        C16590pI r0 = this.A0J;
        C14830m7 r02 = ((ActivityC13790kL) this).A05;
        C14850m9 r03 = ((ActivityC13810kN) this).A0C;
        C253719d r04 = this.A0P;
        C14900mE r05 = ((ActivityC13810kN) this).A05;
        C252718t r06 = ((ActivityC13790kL) this).A0D;
        AbstractC15710nm r07 = ((ActivityC13810kN) this).A03;
        C15570nT r08 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r09 = ((ActivityC13830kP) this).A05;
        C18790t3 r010 = this.A08;
        AnonymousClass19M r011 = ((ActivityC13810kN) this).A0B;
        C16170oZ r012 = this.A09;
        C231510o r013 = this.A0M;
        AnonymousClass12P r014 = ((ActivityC13790kL) this).A00;
        C244415n r015 = this.A0U;
        AnonymousClass130 r016 = this.A0A;
        AnonymousClass01d r017 = ((ActivityC13810kN) this).A08;
        C252018m r018 = this.A0X;
        AnonymousClass018 r019 = ((ActivityC13830kP) this).A01;
        C15650ng r15 = this.A0L;
        WhatsAppLibLoader whatsAppLibLoader = this.A0V;
        AnonymousClass193 r13 = this.A0N;
        C22700zV r12 = this.A0C;
        C18810t5 r11 = ((ActivityC13810kN) this).A0D;
        C15890o4 r10 = this.A0K;
        C14820m6 r9 = ((ActivityC13810kN) this).A09;
        AnonymousClass32x r020 = new AnonymousClass32x(r014, r07, this.A07, r05, r08, r010, r012, r016, r12, this.A0G, r017, r02, r0, r10, r9, r019, r15, r011, r013, r13, r03, r04, r11, this, this.A0T, r015, r1, whatsAppLibLoader, this.A0W, r018, r06, r09);
        this.A0S = r020;
        r020.A0L(bundle, this);
        this.A0S.A0D.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 32));
        C35781ij.A00(this);
        this.A04 = C36321jh.A00(BitmapFactory.decodeResource(getResources(), R.drawable.pin_location_green));
        this.A05 = C36321jh.A00(BitmapFactory.decodeResource(getResources(), R.drawable.pin_location_red));
        this.A03 = C36321jh.A00(this.A0S.A05);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        googleMapOptions.A00 = 1;
        googleMapOptions.A0C = false;
        googleMapOptions.A05 = false;
        googleMapOptions.A08 = true;
        googleMapOptions.A06 = false;
        googleMapOptions.A0A = true;
        googleMapOptions.A09 = true;
        this.A0R = new AnonymousClass454(this, googleMapOptions, this);
        ((ViewGroup) AnonymousClass00T.A05(this, R.id.map_holder)).addView(this.A0R);
        this.A0R.A04(bundle);
        this.A00 = bundle;
        if (this.A02 == null) {
            this.A02 = this.A0R.A07(this.A0b);
        }
        this.A0S.A0S = (ImageView) AnonymousClass00T.A05(this, R.id.my_location);
        this.A0S.A0S.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 31));
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A02 = this.A0S.A02(i);
        if (A02 == null) {
            return super.onCreateDialog(i);
        }
        return A02;
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.search).setIcon(R.drawable.ic_action_search).setShowAsAction(2);
        menu.add(0, 1, 0, R.string.refresh).setIcon(R.drawable.ic_action_refresh).setShowAsAction(1);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0R.A00();
        this.A0S.A06();
        if (this.A02 != null) {
            SharedPreferences.Editor edit = this.A0W.A01(AnonymousClass01V.A07).edit();
            CameraPosition A02 = this.A02.A02();
            LatLng latLng = A02.A03;
            edit.putFloat("share_location_lat", (float) latLng.A00);
            edit.putFloat("share_location_lon", (float) latLng.A01);
            edit.putFloat("share_location_zoom", A02.A02);
            edit.apply();
        }
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A02(this.A01, this.A0I);
            AnonymousClass1J1 r0 = this.A0E;
            if (r0 != null) {
                r0.A00();
                this.A0E = null;
            }
        }
        super.onDestroy();
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        this.A0R.A01();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.A0S.A0H(intent);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return this.A0S.A0U(menuItem) || super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        this.A0R.A02();
        C618632v r0 = this.A0R;
        SensorManager sensorManager = r0.A05;
        if (sensorManager != null) {
            sensorManager.unregisterListener(r0.A0C);
        }
        AbstractC36001jA r1 = this.A0S;
        r1.A0p = r1.A18.A03();
        r1.A0x.A04(r1);
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A07(this.A0I);
            ((AnonymousClass1A1) this.A0Y.get()).A02(((ActivityC13810kN) this).A00);
        }
        super.onPause();
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem findItem;
        if (this.A0S.A0s) {
            menu.findItem(0).setVisible(false);
            findItem = menu.findItem(1);
        } else {
            if (!this.A0K.A03()) {
                findItem = menu.findItem(0);
            }
            return true;
        }
        findItem.setVisible(false);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        C35961j4 r1;
        super.onResume();
        if (this.A0K.A03() != this.A0S.A0p) {
            invalidateOptionsMenu();
            if (this.A0K.A03() && (r1 = this.A02) != null && !this.A0S.A0s) {
                r1.A0L(true);
            }
        }
        this.A0R.A03();
        this.A0R.A08();
        if (this.A02 == null) {
            this.A02 = this.A0R.A07(this.A0b);
        }
        this.A0S.A07();
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            boolean z = ((AnonymousClass1A1) this.A0Y.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r15 = ((ActivityC13810kN) this).A0C;
                C14900mE r13 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
                C21270x9 r10 = this.A0F;
                Pair A00 = C40691s6.A00(this, view, this.A01, r13, r12, this.A0B, this.A0D, this.A0E, r10, this.A0H, this.A0I, ((ActivityC13810kN) this).A09, ((ActivityC13830kP) this).A01, r15, r11, this.A0Y, this.A0Z, "location-picker-activity");
                this.A01 = (View) A00.first;
                this.A0E = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A0I, this.A0Y);
            }
            ((AnonymousClass1A1) this.A0Y.get()).A01();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        C35961j4 r0 = this.A02;
        if (r0 != null) {
            CameraPosition A02 = r0.A02();
            bundle.putFloat("camera_zoom", A02.A02);
            LatLng latLng = A02.A03;
            bundle.putDouble("camera_lat", latLng.A00);
            bundle.putDouble("camera_lng", latLng.A01);
            bundle.putInt("map_location_mode", this.A0R.A03);
        }
        this.A0R.A05(bundle);
        this.A0S.A0K(bundle);
        super.onSaveInstanceState(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A0S.A0Y.A01();
        return false;
    }
}
