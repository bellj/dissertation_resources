package com.whatsapp.location;

import X.AnonymousClass2QN;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* loaded from: classes2.dex */
public class ContactLiveLocationThumbnail extends ThumbnailButton {
    public float A00;
    public int A01;
    public int A02;
    public boolean A03;
    public boolean A04;
    public final RectF A05;

    public ContactLiveLocationThumbnail(Context context) {
        super(context);
        A00();
        this.A05 = new RectF();
    }

    public ContactLiveLocationThumbnail(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A05 = new RectF();
        A00(context, attributeSet);
    }

    public ContactLiveLocationThumbnail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A05 = new RectF();
        A00(context, attributeSet);
    }

    public ContactLiveLocationThumbnail(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    private void A00(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A07);
            this.A00 = obtainStyledAttributes.getDimension(1, this.A00);
            this.A01 = obtainStyledAttributes.getInteger(0, this.A01);
            this.A02 = obtainStyledAttributes.getInteger(2, this.A02);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // com.whatsapp.components.button.ThumbnailButton
    public void A02(Canvas canvas) {
        RectF rectF = this.A05;
        rectF.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        float f = this.A00;
        if (f > 0.0f && this.A01 != 0) {
            float f2 = f / 2.0f;
            rectF.inset(f2, f2);
            super.A05.setColor(this.A01);
            super.A05.setStrokeWidth(this.A00);
            super.A05.setStyle(Paint.Style.STROKE);
            float f3 = super.A02;
            int i = (f3 > 0.0f ? 1 : (f3 == 0.0f ? 0 : -1));
            Paint paint = super.A05;
            if (i >= 0) {
                canvas.drawRoundRect(rectF, f3, f3, paint);
            } else {
                canvas.drawOval(rectF, paint);
            }
            float f4 = this.A00 / 2.0f;
            rectF.inset(f4, f4);
        }
        float f5 = ((ThumbnailButton) this).A01;
        if (f5 > 0.0f && super.A03 != 0) {
            float f6 = f5 / 2.0f;
            rectF.inset(f6, f6);
            super.A05.setColor(super.A03);
            super.A05.setStrokeWidth(((ThumbnailButton) this).A01);
            super.A05.setStyle(Paint.Style.STROKE);
            float f7 = super.A02;
            int i2 = (f7 > 0.0f ? 1 : (f7 == 0.0f ? 0 : -1));
            Paint paint2 = super.A05;
            if (i2 >= 0) {
                canvas.drawRoundRect(rectF, f7, f7, paint2);
            } else {
                canvas.drawOval(rectF, paint2);
            }
            float f8 = ((ThumbnailButton) this).A01 / 2.0f;
            rectF.inset(f8, f8);
        }
        if (this.A03) {
            super.A05.setColor(getResources().getColor(R.color.live_location_no_avatar_overlay));
            super.A05.setStyle(Paint.Style.FILL);
            float f9 = super.A02;
            int i3 = (f9 > 0.0f ? 1 : (f9 == 0.0f ? 0 : -1));
            Paint paint3 = super.A05;
            if (i3 >= 0) {
                canvas.drawRoundRect(rectF, f9, f9, paint3);
            } else {
                canvas.drawOval(rectF, paint3);
            }
        }
        if (this.A02 > 1) {
            super.A05.setColor(1107296256);
            super.A05.setStyle(Paint.Style.FILL);
            float f10 = super.A02;
            int i4 = (f10 > 0.0f ? 1 : (f10 == 0.0f ? 0 : -1));
            Paint paint4 = super.A05;
            if (i4 >= 0) {
                canvas.drawRoundRect(rectF, f10, f10, paint4);
            } else {
                canvas.drawOval(rectF, paint4);
            }
            super.A05.setTextAlign(Paint.Align.CENTER);
            super.A05.setColor(-1);
            super.A05.setTextSize((float) getResources().getDimensionPixelSize(R.dimen.title_text_size));
            StringBuilder sb = new StringBuilder();
            sb.append(this.A02);
            canvas.drawText(sb.toString(), rectF.centerX(), rectF.centerY() - ((super.A05.ascent() + super.A05.descent()) / 2.0f), super.A05);
        }
    }

    public void setGlowColor(int i) {
        this.A01 = i;
    }

    public void setGlowSize(float f) {
        this.A00 = f;
    }

    public void setGreyOverlay(boolean z) {
        this.A03 = z;
    }

    public void setStackSize(int i) {
        this.A02 = i;
    }
}
