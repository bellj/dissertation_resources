package com.whatsapp.location;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.C004802e;
import X.C12970iu;
import X.C16030oK;
import X.C72463ee;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.location.StopLiveLocationDialogFragment;

/* loaded from: classes3.dex */
public class StopLiveLocationDialogFragment extends Hilt_StopLiveLocationDialogFragment {
    public AnonymousClass018 A00;
    public C16030oK A01;
    public AbstractC14440lR A02;

    public static StopLiveLocationDialogFragment A00(AbstractC14640lm r4, String str) {
        StopLiveLocationDialogFragment stopLiveLocationDialogFragment = new StopLiveLocationDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r4.getRawString());
        A0D.putString("id", str);
        stopLiveLocationDialogFragment.A0U(A0D);
        return stopLiveLocationDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String string = A03().getString("id");
        AnonymousClass009.A05(string);
        String string2 = A03().getString("jid");
        AnonymousClass009.A05(string2);
        C004802e A0O = C12970iu.A0O(this);
        A0O.A06(R.string.live_location_stop_sharing_dialog);
        A0O.setPositiveButton(R.string.live_location_stop, new DialogInterface.OnClickListener(string, string2) { // from class: X.4gz
            public final /* synthetic */ String A01;
            public final /* synthetic */ String A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                StopLiveLocationDialogFragment stopLiveLocationDialogFragment = StopLiveLocationDialogFragment.this;
                stopLiveLocationDialogFragment.A02.Ab2(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000d: INVOKE  
                      (wrap: X.0lR : 0x0006: IGET  (r1v0 X.0lR A[REMOVE]) = (r4v0 'stopLiveLocationDialogFragment' com.whatsapp.location.StopLiveLocationDialogFragment) com.whatsapp.location.StopLiveLocationDialogFragment.A02 X.0lR)
                      (wrap: X.3l7 : 0x000a: CONSTRUCTOR  (r0v0 X.3l7 A[REMOVE]) = 
                      (r4v0 'stopLiveLocationDialogFragment' com.whatsapp.location.StopLiveLocationDialogFragment)
                      (wrap: java.lang.String : 0x0002: IGET  (r3v0 java.lang.String A[REMOVE]) = (r5v0 'this' X.4gz A[IMMUTABLE_TYPE, THIS]) X.4gz.A01 java.lang.String)
                      (wrap: java.lang.String : 0x0004: IGET  (r2v0 java.lang.String A[REMOVE]) = (r5v0 'this' X.4gz A[IMMUTABLE_TYPE, THIS]) X.4gz.A02 java.lang.String)
                     call: X.3l7.<init>(com.whatsapp.location.StopLiveLocationDialogFragment, java.lang.String, java.lang.String):void type: CONSTRUCTOR)
                     type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.4gz.onClick(android.content.DialogInterface, int):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000a: CONSTRUCTOR  (r0v0 X.3l7 A[REMOVE]) = 
                      (r4v0 'stopLiveLocationDialogFragment' com.whatsapp.location.StopLiveLocationDialogFragment)
                      (wrap: java.lang.String : 0x0002: IGET  (r3v0 java.lang.String A[REMOVE]) = (r5v0 'this' X.4gz A[IMMUTABLE_TYPE, THIS]) X.4gz.A01 java.lang.String)
                      (wrap: java.lang.String : 0x0004: IGET  (r2v0 java.lang.String A[REMOVE]) = (r5v0 'this' X.4gz A[IMMUTABLE_TYPE, THIS]) X.4gz.A02 java.lang.String)
                     call: X.3l7.<init>(com.whatsapp.location.StopLiveLocationDialogFragment, java.lang.String, java.lang.String):void type: CONSTRUCTOR in method: X.4gz.onClick(android.content.DialogInterface, int):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3l7, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    com.whatsapp.location.StopLiveLocationDialogFragment r4 = com.whatsapp.location.StopLiveLocationDialogFragment.this
                    java.lang.String r3 = r5.A01
                    java.lang.String r2 = r5.A02
                    X.0lR r1 = r4.A02
                    X.3l7 r0 = new X.3l7
                    r0.<init>(r4, r3, r2)
                    r1.Ab2(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.DialogInterface$OnClickListenerC97394gz.onClick(android.content.DialogInterface, int):void");
            }
        });
        C72463ee.A0S(A0O);
        return A0O.create();
    }
}
