package com.whatsapp.location;

import X.AbstractC27501Ht;
import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass13O;
import X.AnonymousClass143;
import X.AnonymousClass1Tv;
import X.AnonymousClass1UY;
import X.AnonymousClass3LT;
import X.C005602s;
import X.C14820m6;
import X.C14830m7;
import X.C15890o4;
import X.C16030oK;
import X.C18280sC;
import X.C18360sK;
import X.C22630zO;
import X.C244615p;
import X.C244715q;
import X.C71083cM;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class LocationSharingService extends AbstractServiceC27491Hs implements AbstractC27501Ht, AnonymousClass004 {
    public static volatile boolean A0K;
    public long A00;
    public C244615p A01;
    public C18280sC A02;
    public AnonymousClass143 A03;
    public AnonymousClass01d A04;
    public C14830m7 A05;
    public C15890o4 A06;
    public C14820m6 A07;
    public C16030oK A08;
    public AnonymousClass13O A09;
    public AnonymousClass3LT A0A;
    public C244715q A0B;
    public boolean A0C;
    public final Handler A0D;
    public final Object A0E;
    public final Runnable A0F;
    public final Runnable A0G;
    public volatile C71083cM A0H;
    public volatile boolean A0I;
    public volatile boolean A0J;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public LocationSharingService() {
        this(0);
        this.A0D = new Handler(Looper.getMainLooper());
        this.A0F = new RunnableBRunnable0Shape7S0100000_I0_7(this, 40);
        this.A0G = new RunnableBRunnable0Shape7S0100000_I0_7(this, 41);
    }

    public LocationSharingService(int i) {
        this.A0E = new Object();
        this.A0C = false;
    }

    public static void A00(Context context, Intent intent) {
        if (!AnonymousClass1Tv.A00(context, intent)) {
            C005602s A00 = C22630zO.A00(context);
            A00.A0J = "other_notifications@1";
            A00.A0A(context.getString(R.string.notification_title_live_location_stopped));
            A00.A09(context.getString(R.string.notification_text_live_location_stopped));
            Intent intent2 = new Intent();
            intent2.setClassName(context.getPackageName(), "com.whatsapp.location.LiveLocationPrivacyActivity");
            A00.A09 = AnonymousClass1UY.A00(context, 0, intent2, 0);
            int i = -2;
            if (Build.VERSION.SDK_INT >= 26) {
                i = -1;
            }
            A00.A03 = i;
            C18360sK.A01(A00, R.drawable.notifybar);
            ((NotificationManager) context.getSystemService("notification")).notify(12, A00.A01());
        }
    }

    public static void A01(Context context, C16030oK r3) {
        if (Build.VERSION.SDK_INT < 29) {
            return;
        }
        if (r3.A0d()) {
            A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.START_PERSISTENT_LOCATION_REPORTING"));
        } else if (A0K) {
            AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.STOP_LOCATION_REPORTING"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        if (r3.A08.A0d() == false) goto L_0x003f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
            r3 = this;
            boolean r0 = r3.A0I
            if (r0 != 0) goto L_0x0016
            boolean r0 = r3.A0J
            if (r0 != 0) goto L_0x0016
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 29
            if (r1 < r0) goto L_0x004b
            X.0oK r0 = r3.A08
            boolean r0 = r0.A0d()
            if (r0 == 0) goto L_0x004b
        L_0x0016:
            java.lang.String r0 = "LocationSharingService/stopSelfIfNeeded/service not stopped: "
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            boolean r0 = r3.A0I
            r2.append(r0)
            java.lang.String r1 = "|"
            r2.append(r1)
            boolean r0 = r3.A0J
            r2.append(r0)
            r2.append(r1)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 29
            if (r1 < r0) goto L_0x003f
            X.0oK r0 = r3.A08
            boolean r1 = r0.A0d()
            r0 = 1
            if (r1 != 0) goto L_0x0040
        L_0x003f:
            r0 = 0
        L_0x0040:
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.whatsapp.util.Log.i(r0)
            return
        L_0x004b:
            r3.stopSelf()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.LocationSharingService.A02():void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A0H == null) {
            synchronized (this.A0E) {
                if (this.A0H == null) {
                    this.A0H = new C71083cM(this);
                }
            }
        }
        return this.A0H.generatedComponent();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00b7, code lost:
        if (r0 != null) goto L_0x00b9;
     */
    @Override // android.app.Service
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate() {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.LocationSharingService.onCreate():void");
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.i("LocationSharingService/onDestroy");
        C16030oK r0 = this.A08;
        synchronized (r0.A0V) {
            r0.A00 = 0;
        }
        stopForeground(true);
        A0K = false;
        this.A01.A08 = false;
        Handler handler = this.A0D;
        handler.removeCallbacks(this.A0F);
        handler.removeCallbacks(this.A0G);
        AnonymousClass3LT r1 = this.A0A;
        r1.A05.A04(r1);
        r1.A00();
        PowerManager.WakeLock wakeLock = r1.A03;
        if (wakeLock != null && wakeLock.isHeld()) {
            r1.A03.release();
            r1.A03 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0231, code lost:
        if (r8 == Integer.MIN_VALUE) goto L_0x0233;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0212 A[LOOP:1: B:57:0x020c->B:59:0x0212, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x023e  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0208 A[EDGE_INSN: B:95:0x0208->B:56:0x0208 ?: BREAK  , SYNTHETIC] */
    @Override // android.app.Service
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int onStartCommand(android.content.Intent r20, int r21, int r22) {
        /*
        // Method dump skipped, instructions count: 699
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.location.LocationSharingService.onStartCommand(android.content.Intent, int, int):int");
    }
}
