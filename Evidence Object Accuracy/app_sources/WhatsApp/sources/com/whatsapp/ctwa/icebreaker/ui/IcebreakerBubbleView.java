package com.whatsapp.ctwa.icebreaker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class IcebreakerBubbleView extends FrameLayout {
    public IcebreakerBubbleView(Context context) {
        super(context);
    }

    public IcebreakerBubbleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public IcebreakerBubbleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public IcebreakerBubbleView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
    }

    public final void A00() {
        setBackgroundResource(R.drawable.floating_spam_banner_background);
        int dimension = (int) getResources().getDimension(R.dimen.icebreaker_bubble_content_padding);
        setPadding(dimension, dimension, dimension, 0);
    }
}
