package com.whatsapp.ctwa.bizpreview;

import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass10S;
import X.AnonymousClass1J1;
import X.AnonymousClass232;
import X.AnonymousClass233;
import X.AnonymousClass23G;
import X.AnonymousClass3GB;
import X.AnonymousClass4KW;
import X.C14830m7;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C20830wO;
import X.C21270x9;
import X.C248917h;
import X.C254719n;
import X.C255919z;
import X.C27131Gd;
import X.C30141Wg;
import X.C30211Wn;
import X.C37071lG;
import X.C37121lW;
import X.C80673sg;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.ctwa.bizpreview.BusinessPreviewFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes2.dex */
public class BusinessPreviewFragment extends Hilt_BusinessPreviewFragment implements View.OnClickListener {
    public View A00;
    public ImageView A01;
    public ScrollView A02;
    public C14900mE A03;
    public AnonymousClass23G A04;
    public C254719n A05;
    public CatalogMediaCard A06;
    public C15550nR A07;
    public AnonymousClass10S A08;
    public C15610nY A09;
    public AnonymousClass1J1 A0A;
    public C21270x9 A0B;
    public C14830m7 A0C;
    public AnonymousClass018 A0D;
    public C255919z A0E;
    public AnonymousClass233 A0F;
    public C20830wO A0G;
    public C15370n3 A0H;
    public UserJid A0I;
    public boolean A0J = false;
    public final C27131Gd A0K = new C37121lW(this);

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        CatalogMediaCard catalogMediaCard = this.A06;
        if (catalogMediaCard != null && !this.A0J) {
            this.A0J = true;
            catalogMediaCard.A02();
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.business_preview, viewGroup, false);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        CatalogMediaCard catalogMediaCard = this.A06;
        if (catalogMediaCard != null && !this.A0J) {
            this.A0J = true;
            catalogMediaCard.A02();
        }
        this.A0A.A00();
        this.A0E.A00(5);
        this.A00 = null;
        this.A06 = null;
        this.A02 = null;
        this.A08.A04(this.A0K);
        this.A01 = null;
        this.A0F.A04.A04(this);
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        CatalogMediaCard catalogMediaCard = this.A06;
        if (catalogMediaCard != null) {
            C37071lG r1 = catalogMediaCard.A0A;
            if (r1.A00) {
                r1.A00 = false;
                r1.A01.A00();
            }
        }
        this.A0J = false;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A0A = this.A0B.A05("business-preview", -1.0f, A01().getResources().getDimensionPixelSize(R.dimen.business_preview_avatar_size));
        Parcelable parcelable = A03().getParcelable("arg_user_jid");
        AnonymousClass009.A05(parcelable);
        this.A0I = (UserJid) parcelable;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A00 = view;
        this.A02 = (ScrollView) AnonymousClass028.A0D(view, R.id.business_preview_scrollview);
        AnonymousClass028.A0D(this.A00, R.id.close_button).setOnClickListener(this);
        AnonymousClass233 r3 = this.A0F;
        AnonymousClass232 r2 = r3.A02;
        boolean z = r2.A05;
        AnonymousClass016 r1 = r2.A06;
        if (z) {
            r3.A04((C30141Wg) r1.A01());
        } else {
            r1.A08(new AnonymousClass02B(r2, r3) { // from class: X.4u5
                public final /* synthetic */ AnonymousClass232 A00;
                public final /* synthetic */ AnonymousClass233 A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    this.A01.A04((C30141Wg) this.A00.A06.A01());
                }
            });
        }
        AnonymousClass009.A05(this.A0F.A04.A01());
        int i = ((AnonymousClass4KW) this.A0F.A04.A01()).A00;
        AnonymousClass233 r0 = this.A0F;
        if (i != 1) {
            int i2 = ((AnonymousClass4KW) r0.A04.A01()).A00;
            if (i2 != 2 && i2 == 4) {
                AnonymousClass028.A0D(this.A00, R.id.spinner_container).setVisibility(8);
                View A0D = AnonymousClass028.A0D(this.A00, R.id.business_fields_container);
                A1M();
                A0D.setVisibility(0);
            }
        } else {
            AnonymousClass016 r22 = r0.A04;
            AnonymousClass009.A05(r22.A01());
            if (((AnonymousClass4KW) r22.A01()).A00 == 1) {
                r22.A0B(new AnonymousClass4KW(3));
            }
        }
        this.A0F.A04.A05(this, new AnonymousClass02B() { // from class: X.3QL
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                BusinessPreviewFragment businessPreviewFragment = BusinessPreviewFragment.this;
                int i3 = ((AnonymousClass4KW) obj).A00;
                if (i3 != 2 && i3 == 4) {
                    businessPreviewFragment.A04 = new AnonymousClass23G(businessPreviewFragment.A00, AnonymousClass028.A0D(businessPreviewFragment.A00, R.id.spinner_container), AnonymousClass028.A0D(businessPreviewFragment.A00, R.id.business_fields_container));
                    businessPreviewFragment.A1M();
                    C14900mE r23 = businessPreviewFragment.A03;
                    r23.A02.post(new RunnableBRunnable0Shape5S0100000_I0_5(businessPreviewFragment, 45));
                }
            }
        });
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        super.A1L(view);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        A00.A0L(-1);
        A00.A0E = new C80673sg(A00, this);
    }

    public final void A1M() {
        int i;
        C15370n3 r0 = this.A0H;
        if (r0 == null) {
            UserJid userJid = this.A0I;
            if (userJid != null) {
                C20830wO r02 = this.A0G;
                AnonymousClass009.A05(userJid);
                r0 = r02.A01(userJid);
                this.A0H = r0;
            }
            A1B();
            Log.w("BusinessPreviewFragment/populateBusinessProfileFields/Business preview used on non-business number");
        }
        if (r0.A0J()) {
            AnonymousClass009.A05(this.A0H);
            ImageView imageView = (ImageView) AnonymousClass028.A0D(this.A00, R.id.picture);
            this.A01 = imageView;
            C15370n3 r1 = this.A0H;
            if (r1.A0X) {
                this.A0A.A07(imageView, r1, false);
            }
            try {
                this.A08.A03(this.A0K);
            } catch (IllegalStateException unused) {
            }
            AnonymousClass028.A0D(this.A00, R.id.header).setVisibility(0);
            AnonymousClass009.A05(this.A0H);
            TextView textView = (TextView) AnonymousClass028.A0D(this.A00, R.id.contact_title);
            String A0D = this.A09.A0D(this.A0H, false, false);
            if (A0D == null) {
                A0D = this.A0H.A0D();
            }
            textView.setText(A0D);
            AnonymousClass009.A05(this.A0H);
            ((TextView) AnonymousClass028.A0D(this.A00, R.id.contact_subtitle)).setText(this.A0D.A0G(C248917h.A01(this.A0H)));
            C30141Wg r2 = this.A0F.A01;
            if (r2 != null) {
                TextView textView2 = (TextView) AnonymousClass028.A0D(this.A00, R.id.business_categories);
                List list = r2.A0E;
                if (list.isEmpty()) {
                    i = 8;
                } else {
                    StringBuilder sb = new StringBuilder(" ");
                    sb.append(A0I(R.string.bullet_char));
                    sb.append(" ");
                    textView2.setText(C30211Wn.A00(sb.toString(), list), (TextView.BufferType) null);
                    i = 0;
                }
                textView2.setVisibility(i);
                TextView textView3 = (TextView) AnonymousClass028.A0D(this.A00, R.id.business_hours_status);
                C30141Wg r03 = this.A0F.A01;
                if (r03 == null || r03.A00 == null) {
                    this.A0E.A01 = false;
                    textView3.setVisibility(8);
                } else {
                    this.A0E.A01 = true;
                    AnonymousClass018 r4 = this.A0D;
                    textView3.setText(AnonymousClass3GB.A00(A01(), this.A0F.A01.A00, r4, this.A0C.A00()), TextView.BufferType.SPANNABLE);
                    textView3.setVisibility(0);
                }
                View A0D2 = AnonymousClass028.A0D(this.A00, R.id.business_catalog_shop_info_card);
                this.A06 = (CatalogMediaCard) AnonymousClass028.A0D(this.A00, R.id.business_catalog_media_card);
                C30141Wg r12 = this.A0F.A01;
                if ((r12 == null || !r12.A0I) && !this.A05.A01(r12)) {
                    this.A0E.A00 = false;
                    A0D2.setVisibility(8);
                } else {
                    this.A0E.A00 = true;
                    this.A06.setVisibility(0);
                    A0D2.setVisibility(0);
                    this.A06.A03(this.A0F.A01, this.A0I, null, false, false);
                }
                View findViewById = this.A00.findViewById(R.id.media_card_grid);
                if (findViewById != null) {
                    findViewById.setPadding(findViewById.getPaddingLeft(), findViewById.getPaddingTop(), findViewById.getPaddingRight(), 0);
                }
            }
            AnonymousClass028.A0D(this.A00, R.id.info_card_top).setVisibility(0);
            AnonymousClass028.A0D(this.A00, R.id.button_chat).setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 48));
            AnonymousClass028.A0D(this.A00, R.id.button_view_profile).setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 47));
            AnonymousClass028.A0D(this.A00, R.id.business_preview_buttons).setVisibility(0);
            this.A0E.A00(0);
            return;
        }
        A1B();
        Log.w("BusinessPreviewFragment/populateBusinessProfileFields/Business preview used on non-business number");
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.close_button) {
            A1B();
        }
    }
}
