package com.whatsapp.ctwa.bizpreview;

import X.AnonymousClass028;
import X.AnonymousClass36H;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;

/* loaded from: classes3.dex */
public class BusinessPreviewCatalogMediaCard extends CatalogMediaCard {
    public BusinessPreviewCatalogMediaCard(Context context) {
        this(context, null);
    }

    public BusinessPreviewCatalogMediaCard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BusinessPreviewCatalogMediaCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // com.whatsapp.biz.catalog.view.CatalogMediaCard
    public AnonymousClass36H A00(boolean z) {
        return (AnonymousClass36H) AnonymousClass028.A0D(C12960it.A0E(this).inflate(R.layout.business_preview_catalog_card_grid, (ViewGroup) this, true), R.id.product_catalog_media_card_view);
    }
}
