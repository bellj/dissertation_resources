package com.whatsapp.ctwa.bizpreview;

import X.AbstractC53212dH;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2QN;
import X.AnonymousClass36H;
import X.C12960it;
import X.C12980iv;
import X.C255919z;
import X.C74083hH;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewStub;
import android.widget.GridView;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class BusinessPreviewMediaCardGrid extends AnonymousClass36H {
    public int A00;
    public GridView A01;
    public AnonymousClass01d A02;
    public C255919z A03;
    public C74083hH A04;
    public ArrayList A05;
    public boolean A06;

    public BusinessPreviewMediaCardGrid(Context context) {
        this(context, null);
    }

    public BusinessPreviewMediaCardGrid(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BusinessPreviewMediaCardGrid(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    @Override // X.AbstractC53212dH
    public void A01() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A0B = C12960it.A0R(A00);
            this.A02 = C12960it.A0Q(A00);
            this.A03 = (C255919z) A00.A2Q.get();
        }
    }

    @Override // X.AnonymousClass36H
    public void A04() {
        super.A04();
        this.A01.setVisibility(8);
    }

    @Override // X.AnonymousClass36H
    public void A05() {
        super.A05();
        this.A01.setVisibility(0);
    }

    @Override // X.AnonymousClass36H
    public void A08(AttributeSet attributeSet) {
        super.A08(attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(attributeSet, AnonymousClass2QN.A03, 0, 0);
            try {
                this.A00 = obtainStyledAttributes.getInt(0, 4);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        ViewStub viewStub = (ViewStub) AnonymousClass028.A0D(this, R.id.media_card_grid_stub);
        viewStub.setLayoutResource(R.layout.media_card_grid);
        GridView gridView = (GridView) viewStub.inflate();
        this.A01 = gridView;
        gridView.setNumColumns(this.A00);
    }

    @Override // X.AnonymousClass36H
    public int getThumbnailPixelSize() {
        return AbstractC53212dH.A00(new DisplayMetrics(), this, this.A02.A0O()) / this.A00;
    }

    @Override // X.AnonymousClass36H
    public void setError(String str) {
        super.setError(str);
        this.A01.setVisibility(8);
    }
}
