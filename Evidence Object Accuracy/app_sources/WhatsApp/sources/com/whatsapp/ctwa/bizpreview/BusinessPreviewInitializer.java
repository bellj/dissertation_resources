package com.whatsapp.ctwa.bizpreview;

import X.AbstractC14440lR;
import X.AnonymousClass016;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass232;
import X.C14900mE;
import androidx.lifecycle.OnLifecycleEvent;

/* loaded from: classes.dex */
public class BusinessPreviewInitializer implements AnonymousClass03H {
    public C14900mE A00;
    public AnonymousClass232 A01;
    public AbstractC14440lR A02;
    public Runnable A03;
    public final AnonymousClass016 A04 = new AnonymousClass016();

    public BusinessPreviewInitializer(C14900mE r2, AnonymousClass232 r3, AbstractC14440lR r4) {
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
    }

    @OnLifecycleEvent(AnonymousClass074.ON_STOP)
    public void onStop() {
        Runnable runnable = this.A03;
        if (runnable != null) {
            this.A02.AaP(runnable);
        }
    }
}
