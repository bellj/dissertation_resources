package com.whatsapp;

import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;

/* loaded from: classes3.dex */
public class SimpleExternalStorageStateCallback$PermissionDeniedDialogFragment extends Hilt_SimpleExternalStorageStateCallback_PermissionDeniedDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.alert);
        A0O.A06(R.string.permission_storage_need_access);
        A0O.setPositiveButton(R.string.ok, new IDxCListenerShape4S0000000_2_I1(1));
        return A0O.create();
    }
}
