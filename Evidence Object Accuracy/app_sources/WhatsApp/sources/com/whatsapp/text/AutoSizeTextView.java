package com.whatsapp.text;

import android.content.Context;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class AutoSizeTextView extends WaTextView {
    public float A00;
    public float A01;
    public int A02;
    public boolean A03;
    public int[] A04;

    public AutoSizeTextView(Context context) {
        super(context);
        A08();
        this.A00 = 0.0f;
        this.A01 = 0.0f;
        this.A02 = 0;
        this.A04 = null;
    }

    public AutoSizeTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
        this.A00 = 0.0f;
        this.A01 = 0.0f;
        this.A02 = 0;
        this.A04 = null;
    }

    public AutoSizeTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        this.A00 = 0.0f;
        this.A01 = 0.0f;
        this.A02 = 0;
        this.A04 = null;
    }

    public AutoSizeTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A08();
    }

    public final void A0A() {
        int i;
        int compoundPaddingLeft = (this.A02 - getCompoundPaddingLeft()) - getCompoundPaddingRight();
        int[] iArr = this.A04;
        if (iArr != null && iArr.length != 0) {
            TextPaint textPaint = new TextPaint(getPaint());
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.A04;
                int length = iArr2.length;
                if (i2 >= length) {
                    i = iArr2[length - 1];
                    break;
                }
                textPaint.setTextSize((float) iArr2[i2]);
                if (textPaint.measureText(getText().toString()) <= ((float) compoundPaddingLeft)) {
                    i = this.A04[i2];
                    break;
                }
                i2++;
            }
            if (i >= 0) {
                post(new RunnableBRunnable0Shape0S0101000_I0(this, i, 25));
            }
        }
    }

    public final void A0B() {
        int round = Math.round(this.A00 / getResources().getDisplayMetrics().scaledDensity);
        int round2 = (round - Math.round(this.A01 / getResources().getDisplayMetrics().scaledDensity)) + 1;
        this.A04 = new int[round2];
        for (int i = 0; i < round2; i++) {
            this.A04[i] = Math.round(TypedValue.applyDimension(2, (float) (round - i), getResources().getDisplayMetrics()));
        }
    }

    public void A0C(int i) {
        this.A01 = TypedValue.applyDimension(2, (float) 11, getResources().getDisplayMetrics());
        this.A02 = Math.round(TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics()));
        if (this.A00 == 0.0f) {
            this.A00 = getTextSize();
        }
        A0B();
        A0A();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i > 0 && i != i3 && i2 != i4) {
            A0A();
        }
    }

    @Override // X.C004602b, android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        A0A();
    }

    @Override // X.C004602b, android.widget.TextView
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        this.A00 = getPaint().getTextSize();
        A0B();
        A0A();
    }
}
