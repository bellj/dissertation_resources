package com.whatsapp.text;

import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class SeeMoreTextView extends WaTextView {
    public String A00;
    public String A01;
    public String A02;
    public boolean A03 = false;
    public final Paint A04 = C12990iw.A0F();

    public SeeMoreTextView(Context context) {
        super(context);
        A0A();
    }

    public SeeMoreTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0A();
    }

    public SeeMoreTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0A();
    }

    public static void A02(Paint paint, String str, ArrayList arrayList, ArrayList arrayList2, int i) {
        arrayList2.add(str);
        if (paint.measureText(TextUtils.join(" ", arrayList2)) >= ((float) i)) {
            arrayList2.remove(arrayList2.size() - 1);
            arrayList.add(TextUtils.join(" ", arrayList2));
            arrayList2.clear();
            arrayList2.add(str);
        }
    }

    public final void A0A() {
        this.A00 = getContext().getString(R.string.see_more_ellipses);
        this.A02 = getContext().getString(R.string.see_more_expand_text);
        this.A01 = C12980iv.A0q(this);
        Paint paint = this.A04;
        paint.setTextSize(getTextSize());
        paint.setTypeface(getTypeface());
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:56:0x0085 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r17v0, types: [android.widget.TextView, com.whatsapp.text.SeeMoreTextView, android.view.View, com.whatsapp.WaTextView] */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.whatsapp.WaTextView, X.C004602b, android.widget.TextView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 320
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.text.SeeMoreTextView.onMeasure(int, int):void");
    }

    @Override // android.widget.TextView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.A03 = bundle.getBoolean("is_expanded");
            String string = bundle.getString("original_text");
            if (string == null) {
                string = "";
            }
            this.A01 = string;
            parcelable = bundle.getParcelable("super_state");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.widget.TextView, android.view.View
    public Parcelable onSaveInstanceState() {
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("super_state", super.onSaveInstanceState());
        A0D.putBoolean("is_expanded", this.A03);
        A0D.putString("original_text", this.A01);
        return A0D;
    }

    public void setText(String str) {
        this.A01 = str;
        super.setText((CharSequence) str);
    }
}
