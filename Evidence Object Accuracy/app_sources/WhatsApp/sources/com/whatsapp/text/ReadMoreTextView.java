package com.whatsapp.text;

import X.AbstractC116255Us;
import X.AbstractC50882Rn;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass2GZ;
import X.AnonymousClass5WJ;
import X.C52162aM;
import X.C69933aT;
import X.C69943aU;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0102000_I0;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class ReadMoreTextView extends AbstractC50882Rn {
    public static final AnonymousClass5WJ A0B;
    public int A00;
    public int A01;
    public AbstractC116255Us A02;
    public CharSequence A03;
    public CharSequence A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final Handler A08 = new Handler(Looper.getMainLooper());
    public final AnonymousClass016 A09 = new AnonymousClass016(Boolean.FALSE);
    public final Runnable A0A = new RunnableBRunnable0Shape0S0102000_I0(this);

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            A0B = new C69933aT();
        } else {
            A0B = new C69943aU();
        }
    }

    public ReadMoreTextView(Context context) {
        super(context);
        A0H(context, null);
    }

    public ReadMoreTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0H(context, attributeSet);
    }

    public ReadMoreTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0H(context, attributeSet);
    }

    public final void A0H(Context context, AttributeSet attributeSet) {
        A0B.Acs(this);
        super.A07 = new C52162aM();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0F);
            int resourceId = obtainStyledAttributes.getResourceId(3, 0);
            if (resourceId != 0) {
                this.A03 = ((WaTextView) this).A01.A09(resourceId);
            }
            this.A01 = obtainStyledAttributes.getResourceId(2, R.color.link_color);
            this.A06 = obtainStyledAttributes.getBoolean(1, false);
            int i = obtainStyledAttributes.getInt(0, 0);
            this.A00 = i;
            if (i > 0) {
                setMaxLines(i);
            }
            obtainStyledAttributes.recycle();
        }
    }

    public boolean A0I() {
        AnonymousClass016 r1 = this.A09;
        return r1.A01() != null && ((Boolean) r1.A01()).booleanValue();
    }

    public AnonymousClass017 getExpanded() {
        return this.A09;
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A08.removeCallbacks(this.A0A);
    }

    @Override // X.C004602b, android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        Handler handler = this.A08;
        Runnable runnable = this.A0A;
        handler.removeCallbacks(runnable);
        if (this.A00 != 0) {
            handler.post(runnable);
        }
    }

    public void setExpanded(boolean z) {
        int i;
        Boolean valueOf = Boolean.valueOf(z);
        AnonymousClass016 r1 = this.A09;
        if (!valueOf.equals(r1.A01())) {
            r1.A0B(valueOf);
            if (z || (i = this.A00) == 0) {
                i = Integer.MAX_VALUE;
            }
            setMaxLines(i);
            setText(this.A04);
        }
    }

    public void setLinesLimit(int i) {
        int i2;
        this.A00 = i;
        if (A0I() || (i2 = this.A00) == 0) {
            i2 = Integer.MAX_VALUE;
        }
        setMaxLines(i2);
        requestLayout();
        invalidate();
    }

    public void setReadMoreClickListener(AbstractC116255Us r1) {
        this.A02 = r1;
    }

    @Override // com.whatsapp.TextEmojiLabel, com.whatsapp.WaTextView, android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
        if (!this.A07) {
            this.A04 = charSequence;
        }
    }

    /* access modifiers changed from: private */
    public void setVisibleText(CharSequence charSequence) {
        this.A07 = true;
        setText(charSequence);
        this.A07 = false;
    }
}
