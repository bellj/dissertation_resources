package com.whatsapp.text;

import X.AnonymousClass1l9;
import X.AnonymousClass362;
import X.AnonymousClass4L6;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.TextView;
import com.whatsapp.search.SearchViewModel;
import java.util.List;

/* loaded from: classes2.dex */
public class FinalBackspaceAwareEntry extends AnonymousClass1l9 {
    public static final char A04 = "​".charAt(0);
    public TextView.BufferType A00;
    public List A01;
    public boolean A02;
    public boolean A03;

    public FinalBackspaceAwareEntry(Context context) {
        super(context);
        A02();
        this.A02 = false;
        this.A00 = TextView.BufferType.EDITABLE;
        A09();
    }

    public FinalBackspaceAwareEntry(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A02 = false;
        this.A00 = TextView.BufferType.EDITABLE;
        A09();
    }

    public FinalBackspaceAwareEntry(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A02 = false;
        this.A00 = TextView.BufferType.EDITABLE;
        A09();
    }

    public FinalBackspaceAwareEntry(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    public static /* synthetic */ void A00(FinalBackspaceAwareEntry finalBackspaceAwareEntry, String str) {
        Editable A08 = finalBackspaceAwareEntry.A08(finalBackspaceAwareEntry.getText());
        if (str != null && A08 != null && !str.equals(A08.toString())) {
            finalBackspaceAwareEntry.setTextWithBsPrefix(str);
            finalBackspaceAwareEntry.setSelection(str.length());
        }
    }

    public final int A06(int i) {
        int length;
        if (TextUtils.isEmpty(getText())) {
            length = 0;
        } else {
            length = getText().length();
        }
        return Math.max(0, Math.min(i, length));
    }

    public final int A07(CharSequence charSequence) {
        return (TextUtils.isEmpty(charSequence) || charSequence.charAt(0) != A04) ? 0 : 1;
    }

    public final Editable A08(Editable editable) {
        if (TextUtils.isEmpty(editable) || editable.charAt(0) != A04) {
            return editable;
        }
        Editable newEditable = Editable.Factory.getInstance().newEditable(editable);
        newEditable.replace(0, A07(newEditable), "", 0, 0);
        return newEditable;
    }

    public final void A09() {
        Editable text = getText();
        if (TextUtils.isEmpty(text) || text.charAt(0) != A04) {
            A0A(getText());
        }
        addTextChangedListener(new AnonymousClass362(this));
    }

    public final void A0A(Editable editable) {
        Editable editable2;
        this.A02 = true;
        if (editable != null) {
            editable2 = Editable.Factory.getInstance().newEditable(editable);
            editable2.replace(0, 0, "​", 0, 1);
        } else {
            editable2 = null;
        }
        setText(editable2, this.A00);
        this.A02 = false;
    }

    @Override // android.widget.TextView
    public int getSelectionEnd() {
        Editable text = getText();
        if (TextUtils.isEmpty(A08(text))) {
            return A07(text);
        }
        return super.getSelectionEnd();
    }

    @Override // android.widget.TextView
    public int getSelectionStart() {
        return Math.max(super.getSelectionStart(), A07(getText()));
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        SearchViewModel searchViewModel;
        List<AnonymousClass4L6> list = this.A01;
        if (list != null) {
            for (AnonymousClass4L6 r2 : list) {
                if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1 && (searchViewModel = r2.A00.A0C) != null) {
                    searchViewModel.A0X(false);
                }
            }
        }
        return super.onKeyPreIme(i, keyEvent);
    }

    @Override // android.widget.TextView
    public void onSelectionChanged(int i, int i2) {
        super.onSelectionChanged(i, i2);
        int min = Math.min(i, i2);
        int max = Math.max(i, i2);
        int A07 = A07(getText());
        if (min < A07) {
            setSelection(A06(Math.max(min, A07)), A06(max));
        }
    }

    @Override // android.widget.EditText
    public void setSelection(int i) {
        super.setSelection(A06(i + A07(getText())));
    }

    private void setTextWithBsPrefix(String str) {
        StringBuilder sb = new StringBuilder("​");
        sb.append(str);
        setText(sb.toString());
    }
}
