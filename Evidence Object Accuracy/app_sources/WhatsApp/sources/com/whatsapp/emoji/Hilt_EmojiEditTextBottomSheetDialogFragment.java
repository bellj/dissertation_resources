package com.whatsapp.emoji;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C231510o;
import X.C252718t;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_EmojiEditTextBottomSheetDialogFragment extends BottomSheetDialogFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1J();
        return this.A00;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1J()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.emoji.Hilt_EmojiEditTextBottomSheetDialogFragment.A0u(android.app.Activity):void");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1J();
        A1I();
    }

    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment = (EmojiEditTextBottomSheetDialogFragment) this;
            AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            emojiEditTextBottomSheetDialogFragment.A0J = C12960it.A0S(r2);
            emojiEditTextBottomSheetDialogFragment.A08 = C12970iu.A0R(r2);
            emojiEditTextBottomSheetDialogFragment.A0L = (C252718t) r2.A9K.get();
            emojiEditTextBottomSheetDialogFragment.A07 = C12970iu.A0Q(r2);
            emojiEditTextBottomSheetDialogFragment.A0F = (AnonymousClass19M) r2.A6R.get();
            emojiEditTextBottomSheetDialogFragment.A0H = (C231510o) r2.AHO.get();
            emojiEditTextBottomSheetDialogFragment.A0B = C12960it.A0Q(r2);
            emojiEditTextBottomSheetDialogFragment.A0D = C12960it.A0R(r2);
            emojiEditTextBottomSheetDialogFragment.A0I = (AnonymousClass193) r2.A6S.get();
            emojiEditTextBottomSheetDialogFragment.A0C = C12970iu.A0Z(r2);
            emojiEditTextBottomSheetDialogFragment.A0K = C12990iw.A0e(r2);
        }
    }

    public final void A1J() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
