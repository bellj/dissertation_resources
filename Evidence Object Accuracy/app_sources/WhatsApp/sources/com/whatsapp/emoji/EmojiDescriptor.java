package com.whatsapp.emoji;

import X.AbstractC32691cZ;
import X.AnonymousClass4GD;
import X.AnonymousClass4GE;
import X.AnonymousClass4GF;
import X.AnonymousClass4GG;
import X.AnonymousClass4GH;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class EmojiDescriptor {
    public static long A00(AbstractC32691cZ r9, boolean z) {
        long j = 0;
        do {
            int A00 = r9.A00();
            if (A00 == 0) {
                return AnonymousClass4GE.A00[(int) j];
            }
            int i = (int) j;
            int binarySearch = Arrays.binarySearch(AnonymousClass4GD.A00, (int) AnonymousClass4GH.A00[i], (int) AnonymousClass4GF.A00[i], A00);
            if (binarySearch >= 0) {
                j = AnonymousClass4GG.A00[binarySearch];
            } else if (!z) {
                return AnonymousClass4GE.A00[i];
            } else {
                return -1;
            }
        } while (j >= 0);
        if (z) {
            if (r9.A00() != 0 || j == -1) {
                return -1;
            }
        } else if (j == -1) {
            return -1;
        }
        return -j;
    }

    public static long getDescriptor(AbstractC32691cZ r1) {
        return A00(r1, false);
    }
}
