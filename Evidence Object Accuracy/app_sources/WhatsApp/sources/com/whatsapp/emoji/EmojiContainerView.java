package com.whatsapp.emoji;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12990iw;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class EmojiContainerView extends FrameLayout implements AnonymousClass004 {
    public Paint A00;
    public Path A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public boolean A04;

    public EmojiContainerView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        setWillNotDraw(false);
    }

    public EmojiContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        setWillNotDraw(false);
    }

    public EmojiContainerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        setWillNotDraw(false);
    }

    public EmojiContainerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.A04) {
            if (this.A01 == null) {
                this.A01 = new Path();
            }
            Paint paint = this.A00;
            if (paint == null) {
                paint = C12990iw.A0F();
                this.A00 = paint;
            }
            paint.setColor(285212672);
            this.A01.reset();
            this.A01.moveTo((float) ((getWidth() * 9) / 10), (float) ((getHeight() * 9) / 10));
            this.A01.lineTo((float) ((getWidth() * 9) / 10), (float) ((getHeight() * 3) >> 2));
            C12990iw.A15(this.A01, this, (getWidth() * 3) >> 2);
            C12990iw.A15(this.A01, this, (getWidth() * 9) / 10);
            this.A01.setFillType(Path.FillType.WINDING);
            canvas.drawPath(this.A01, this.A00);
        }
    }

    public void setIsSkinTone(boolean z) {
        this.A04 = z;
    }
}
