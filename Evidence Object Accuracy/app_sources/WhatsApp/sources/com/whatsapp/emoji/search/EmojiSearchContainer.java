package com.whatsapp.emoji.search;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass2P7;
import X.AnonymousClass4VM;
import X.AnonymousClass5UC;
import X.C16630pM;
import X.C231510o;
import X.C37471mS;
import X.C54542gt;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.InterceptingEditText;

/* loaded from: classes2.dex */
public class EmojiSearchContainer extends FrameLayout implements AnonymousClass004 {
    public Activity A00;
    public View A01;
    public View A02;
    public RecyclerView A03;
    public InterceptingEditText A04;
    public AnonymousClass018 A05;
    public AnonymousClass19M A06;
    public C231510o A07;
    public C54542gt A08;
    public AnonymousClass193 A09;
    public AnonymousClass5UC A0A;
    public C16630pM A0B;
    public AnonymousClass2P7 A0C;
    public String A0D;
    public boolean A0E;
    public boolean A0F;
    public C37471mS[] A0G;

    public EmojiSearchContainer(Context context) {
        super(context);
        if (!this.A0F) {
            this.A0F = true;
            generatedComponent();
        }
    }

    public EmojiSearchContainer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
    }

    public EmojiSearchContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0F) {
            this.A0F = true;
            generatedComponent();
        }
    }

    public EmojiSearchContainer(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A0F) {
            this.A0F = true;
            generatedComponent();
        }
    }

    public EmojiSearchContainer(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A0F) {
            this.A0F = true;
            generatedComponent();
        }
    }

    public final void A00(String str) {
        AnonymousClass193 r0 = this.A09;
        if (r0 != null && r0.A02) {
            this.A02.setVisibility(8);
            this.A01.setVisibility(0);
            C54542gt r3 = this.A08;
            AnonymousClass4VM A00 = this.A09.A00(str, this.A0G, true);
            synchronized (r3) {
                AnonymousClass4VM r1 = r3.A00;
                if (r1 != null) {
                    r1.A00 = null;
                }
                r3.A00 = A00;
                A00.A00(r3);
                r3.A02();
            }
            this.A0D = str;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0C;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0C = r0;
        }
        return r0.generatedComponent();
    }

    public void setExcludedEmojis(C37471mS[] r1) {
        this.A0G = r1;
    }
}
