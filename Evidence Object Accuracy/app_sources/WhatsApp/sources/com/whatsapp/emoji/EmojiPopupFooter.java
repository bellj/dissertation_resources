package com.whatsapp.emoji;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class EmojiPopupFooter extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public EmojiPopupFooter(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public EmojiPopupFooter(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }

    public EmojiPopupFooter(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public EmojiPopupFooter(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public int getTopOffset() {
        return this.A00;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass028.A0Y(this, this.A00);
    }

    public void setTopOffset(int i) {
        AnonymousClass028.A0Y(this, Math.max(Math.min(getHeight(), i), 0) - getTop());
        this.A00 = getTop();
    }
}
