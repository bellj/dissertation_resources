package com.whatsapp.emoji;

import X.AbstractC116455Vm;
import X.AbstractC14020ki;
import X.AbstractC15710nm;
import X.AbstractC27571Ib;
import X.AbstractC36671kL;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01d;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass367;
import X.AnonymousClass3UD;
import X.C100654mG;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15270mq;
import X.C15330mx;
import X.C16630pM;
import X.C231510o;
import X.C252718t;
import X.C42941w9;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.EmojiEditTextBottomSheetDialogFragment;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class EmojiEditTextBottomSheetDialogFragment extends Hilt_EmojiEditTextBottomSheetDialogFragment {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ImageButton A06;
    public AbstractC15710nm A07;
    public C14900mE A08;
    public WaButton A09;
    public WaEditText A0A;
    public AnonymousClass01d A0B;
    public C14820m6 A0C;
    public AnonymousClass018 A0D;
    public AbstractC27571Ib A0E;
    public AnonymousClass19M A0F;
    public C15270mq A0G;
    public C231510o A0H;
    public AnonymousClass193 A0I;
    public C14850m9 A0J;
    public C16630pM A0K;
    public C252718t A0L;
    public String A0M;
    public boolean A0N;
    public boolean A0O;
    public String[] A0P;
    public final AbstractC116455Vm A0Q = new AnonymousClass3UD(this);

    public static EmojiEditTextBottomSheetDialogFragment A00(String str, String[] strArr, int i, int i2, int i3, int i4, int i5) {
        EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment = new EmojiEditTextBottomSheetDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogId", i);
        A0D.putInt("titleResId", i2);
        A0D.putInt("hintResId", 0);
        A0D.putInt("emptyErrorResId", i3);
        A0D.putString("defaultStr", str);
        A0D.putInt("maxLength", i4);
        A0D.putInt("inputType", i5);
        A0D.putStringArray("codepointBlacklist", strArr);
        A0D.putBoolean("shouldHideEmojiBtn", false);
        emojiEditTextBottomSheetDialogFragment.A0U(A0D);
        return emojiEditTextBottomSheetDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A0E = null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        boolean A00 = C252718t.A00(this.A0A);
        this.A0N = A00;
        bundle.putBoolean("is_keyboard_showing", A00);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        boolean z;
        View inflate = A0C().getLayoutInflater().inflate(R.layout.emoji_editext_bottomsheet_dialog, (ViewGroup) null, false);
        TextView A0J = C12960it.A0J(inflate, R.id.dialog_title_tv);
        int i = this.A05;
        if (i != 0) {
            A0J.setText(i);
        }
        this.A0A = (WaEditText) inflate.findViewById(R.id.edit_text);
        TextView A0J2 = C12960it.A0J(inflate, R.id.counter_tv);
        C42941w9.A0C(this.A0A, this.A0D);
        if (this.A04 > 0) {
            A0J2.setVisibility(0);
        }
        ArrayList A0l = C12960it.A0l();
        int i2 = this.A04;
        if (i2 > 0) {
            A0l.add(new C100654mG(i2));
        }
        if (!A0l.isEmpty()) {
            this.A0A.setFilters((InputFilter[]) A0l.toArray(new InputFilter[0]));
        }
        WaEditText waEditText = this.A0A;
        waEditText.addTextChangedListener(new AnonymousClass367(waEditText, A0J2, this.A0B, this.A0D, this.A0F, this.A0K, this.A04, 0, false));
        this.A09 = (WaButton) inflate.findViewById(R.id.save_button);
        this.A0A.setInputType(this.A03);
        this.A0A.A04(true);
        Window window = ((DialogFragment) this).A03.getWindow();
        if (Build.VERSION.SDK_INT >= 21) {
            window.getDecorView().setSystemUiVisibility(1280);
            window.setStatusBarColor(0);
        }
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = -1;
        attributes.gravity = 48;
        ((DialogFragment) this).A03.getWindow().setAttributes(attributes);
        C12960it.A0x(this.A09, this, 8);
        C12960it.A0x(inflate.findViewById(R.id.cancel_button), this, 7);
        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) inflate.findViewById(R.id.emoji_edit_text_layout);
        keyboardPopupLayout.A07 = true;
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.emoji_btn);
        this.A06 = imageButton;
        ActivityC000900k A0B = A0B();
        C252718t r14 = this.A0L;
        AbstractC15710nm r11 = this.A07;
        AnonymousClass19M r10 = this.A0F;
        C231510o r9 = this.A0H;
        AnonymousClass01d r8 = this.A0B;
        AnonymousClass018 r7 = this.A0D;
        AnonymousClass193 r6 = this.A0I;
        this.A0G = new C15270mq(A0B, imageButton, r11, keyboardPopupLayout, this.A0A, r8, this.A0C, r7, r10, r9, r6, this.A0K, r14);
        C15270mq r102 = this.A0G;
        ActivityC000900k A0B2 = A0B();
        AnonymousClass19M r92 = this.A0F;
        C231510o r112 = this.A0H;
        new C15330mx(A0B2, this.A0D, r92, r102, r112, (EmojiSearchContainer) inflate.findViewById(R.id.emoji_search_container), this.A0K).A00 = new AbstractC14020ki() { // from class: X.56g
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                EmojiEditTextBottomSheetDialogFragment.this.A0Q.APc(r3.A00);
            }
        };
        C15270mq r4 = this.A0G;
        r4.A0C(this.A0Q);
        r4.A0E = new RunnableBRunnable0Shape15S0100000_I1_1(this, 33);
        int i3 = this.A02;
        if (i3 != 0) {
            this.A0A.setHint(A0I(i3));
        }
        this.A0A.setText(AbstractC36671kL.A05(A0B(), this.A0F, this.A0M));
        if (!TextUtils.isEmpty(this.A0M)) {
            this.A0A.selectAll();
        }
        ((DialogFragment) this).A03.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.3LD
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment = EmojiEditTextBottomSheetDialogFragment.this;
                BottomSheetBehavior A00 = BottomSheetBehavior.A00(((Dialog) dialogInterface).findViewById(R.id.design_bottom_sheet));
                A00.A0M(3);
                A00.A0E = new C80633sc(emojiEditTextBottomSheetDialogFragment);
            }
        });
        if (bundle == null) {
            z = true;
        } else {
            z = bundle.getBoolean("is_keyboard_showing");
        }
        this.A0N = z;
        if (this.A0O) {
            ImageButton imageButton2 = this.A06;
            AnonymousClass009.A03(imageButton2);
            imageButton2.setVisibility(8);
        }
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        this.A0A.requestFocus();
        if (this.A0N) {
            this.A0A.A04(false);
        }
    }

    @Override // com.whatsapp.emoji.Hilt_EmojiEditTextBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        AnonymousClass01E r1 = ((AnonymousClass01E) this).A0D;
        if (r1 instanceof AbstractC27571Ib) {
            this.A0E = (AbstractC27571Ib) r1;
        } else if (context instanceof AbstractC27571Ib) {
            this.A0E = (AbstractC27571Ib) context;
        } else {
            throw C12960it.A0U(C12960it.A0d("EmojiEditTextDialogListener", C12960it.A0k("Activity/Fragment must implement ")));
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, 2131952347);
        Bundle A03 = A03();
        this.A00 = A03.getInt("dialogId");
        this.A05 = A03.getInt("titleResId");
        this.A02 = A03.getInt("hintResId");
        this.A01 = A03.getInt("emptyErrorResId");
        this.A0M = A03.getString("defaultStr");
        this.A04 = A03.getInt("maxLength");
        this.A03 = A03.getInt("inputType");
        this.A0P = A03.getStringArray("codepointBlacklist");
        this.A0O = A03.getBoolean("shouldHideEmojiBtn");
    }
}
