package com.whatsapp;

import X.AnonymousClass1FO;
import X.C20640w5;
import X.C21740xu;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment extends Hilt_DisplayExceptionDialogFactory_SoftwareAboutToExpireDialogFragment {
    public C20640w5 A00;
    public AnonymousClass1FO A01;
    public C21740xu A02;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Log.i("home/dialog software-about-to-expire");
        return this.A01.A03(A0C(), this.A00, this.A02);
    }
}
