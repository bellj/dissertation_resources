package com.whatsapp.acceptinvitelink;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass11F;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass38A;
import X.AnonymousClass3ZT;
import X.C102764pf;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15220ml;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18770sz;
import X.C18810t5;
import X.C19990v2;
import X.C20660w7;
import X.C20710wC;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C25691Aj;
import X.C27151Gf;
import X.C32981d3;
import X.C469728k;
import X.C63773Cw;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101754o2;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;

/* loaded from: classes2.dex */
public class AcceptInviteLinkActivity extends ActivityC13790kL {
    public int A00;
    public C15550nR A01;
    public C15610nY A02;
    public AnonymousClass1J1 A03;
    public C21270x9 A04;
    public C15220ml A05;
    public AnonymousClass018 A06;
    public C19990v2 A07;
    public C21320xE A08;
    public C15600nX A09;
    public C25691Aj A0A;
    public C18770sz A0B;
    public AnonymousClass11F A0C;
    public C16120oU A0D;
    public C20710wC A0E;
    public C469728k A0F;
    public C15580nU A0G;
    public C17220qS A0H;
    public C20660w7 A0I;
    public Runnable A0J;
    public boolean A0K;
    public final C27151Gf A0L;

    public AcceptInviteLinkActivity() {
        this(0);
        this.A0L = new C32981d3(this);
    }

    public AcceptInviteLinkActivity(int i) {
        this.A0K = false;
        A0R(new C102764pf(this));
    }

    public static String A02(Uri uri) {
        if (uri == null) {
            return null;
        }
        if ("whatsapp".equals(uri.getScheme())) {
            if (!"chat".equals(uri.getHost())) {
                return null;
            }
            String queryParameter = uri.getQueryParameter("code");
            if (queryParameter != null) {
                return queryParameter;
            }
        } else if (!"http".equals(uri.getScheme()) && !"https".equals(uri.getScheme())) {
            return null;
        } else {
            if (!"chat.whatsapp.com".equals(uri.getHost())) {
                if (!"whatsapp.com".equals(uri.getHost()) || !"chat".equals(uri.getLastPathSegment())) {
                    return null;
                }
                return uri.getQueryParameter("code");
            }
        }
        return uri.getLastPathSegment();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0K) {
            this.A0K = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A07 = (C19990v2) r1.A3M.get();
            this.A0D = (C16120oU) r1.ANE.get();
            this.A0I = (C20660w7) r1.AIB.get();
            this.A04 = (C21270x9) r1.A4A.get();
            this.A01 = (C15550nR) r1.A45.get();
            this.A02 = (C15610nY) r1.AMe.get();
            this.A06 = (AnonymousClass018) r1.ANb.get();
            this.A0E = (C20710wC) r1.A8m.get();
            this.A0B = (C18770sz) r1.AM8.get();
            this.A0C = (AnonymousClass11F) r1.AE3.get();
            this.A0A = (C25691Aj) r1.AKj.get();
            this.A05 = (C15220ml) r1.A4V.get();
            this.A08 = (C21320xE) r1.A4Y.get();
            this.A09 = (C15600nX) r1.A8x.get();
            this.A0H = (C17220qS) r1.ABt.get();
        }
    }

    public final void A2e() {
        findViewById(R.id.invite_ignore).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 14));
        findViewById(R.id.progress).setVisibility(8);
        findViewById(R.id.group_info).setVisibility(0);
    }

    public final void A2f(int i) {
        findViewById(R.id.progress).setVisibility(4);
        findViewById(R.id.group_info).setVisibility(4);
        findViewById(R.id.error).setVisibility(0);
        findViewById(R.id.learn_more).setVisibility(4);
        ((TextView) findViewById(R.id.error_text)).setText(i);
        findViewById(R.id.ok).setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 5));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.app_name);
        setContentView(R.layout.view_group_invite);
        View findViewById = findViewById(R.id.invite_container);
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101754o2(findViewById, findViewById(R.id.background), this));
        this.A03 = this.A04.A04(this, "accept-invite-link-activity");
        findViewById(R.id.filler).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 15));
        this.A00 = getIntent().getIntExtra("display_type", 0);
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.progress_text);
        int i = this.A00;
        if (i == 0) {
            textView.setText(R.string.verifying_group_invite);
            String stringExtra = getIntent().getStringExtra("code");
            if (TextUtils.isEmpty(stringExtra)) {
                ((ActivityC13810kN) this).A05.A07(R.string.failed_accept_bad_invite_link, 1);
                finish();
            } else {
                StringBuilder sb = new StringBuilder("acceptlink/processcode/");
                sb.append(stringExtra);
                Log.i(sb.toString());
                AbstractC14440lR r3 = ((ActivityC13830kP) this).A05;
                C15570nT r7 = ((ActivityC13790kL) this).A01;
                C20660w7 r13 = this.A0I;
                r3.Aaz(new AnonymousClass38A(r7, this, this.A01, this.A02, this.A0E, this.A0H, r13, stringExtra), new Void[0]);
            }
        } else if (i == 1) {
            textView.setText(R.string.loading_subgroup);
            String stringExtra2 = getIntent().getStringExtra("subgroup_jid");
            String stringExtra3 = getIntent().getStringExtra("parent_group_jid");
            C15580nU A04 = C15580nU.A04(stringExtra2);
            C15580nU A042 = C15580nU.A04(stringExtra3);
            if (A04 == null || A042 == null) {
                AbstractC15710nm r5 = ((ActivityC13810kN) this).A03;
                StringBuilder sb2 = new StringBuilder("subgroup jid is null = ");
                boolean z = true;
                boolean z2 = false;
                if (A04 == null) {
                    z2 = true;
                }
                sb2.append(z2);
                sb2.append("parent group jid is null = ");
                if (A042 != null) {
                    z = false;
                }
                sb2.append(z);
                r5.AaV("parent-group-error", sb2.toString(), false);
            } else {
                this.A0G = A04;
                new C63773Cw(((ActivityC13810kN) this).A03, A042, this.A0H, new AnonymousClass3ZT(this, stringExtra3)).A00(A04);
            }
        }
        C14830m7 r132 = ((ActivityC13790kL) this).A05;
        C19990v2 r15 = this.A07;
        C15550nR r10 = this.A01;
        C15610nY r11 = this.A02;
        AnonymousClass018 r14 = this.A06;
        C20710wC r32 = this.A0E;
        C469728k r72 = new C469728k(this, (ViewGroup) findViewById(R.id.invite_root), r10, r11, this.A03, r132, r14, r15, r32);
        this.A0F = r72;
        r72.A0G = true;
        this.A08.A03(this.A0L);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(0);
            getWindow().setNavigationBarColor(AnonymousClass00T.A00(this, R.color.black));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A08.A04(this.A0L);
        Runnable runnable = this.A0J;
        if (runnable != null) {
            ((ActivityC13810kN) this).A05.A0G(runnable);
        }
        this.A03.A00();
    }
}
