package com.whatsapp.crop;

import X.AnonymousClass289;
import X.C47932Di;
import X.RunnableC55742jF;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class CropImageView extends AnonymousClass289 {
    public float A00;
    public float A01;
    public int A02;
    public C47932Di A03;
    public boolean A04;
    public boolean A05;
    public final ArrayList A06;

    public CropImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A06 = new ArrayList();
        this.A03 = null;
    }

    public CropImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass289
    public void A01(float f, float f2) {
        super.A01(f, f2);
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A06;
            if (i < arrayList.size()) {
                C47932Di r1 = (C47932Di) arrayList.get(i);
                r1.A03.postTranslate(f, f2);
                r1.A04 = r1.A02();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass289
    public void A02(float f, float f2, float f3) {
        super.A02(f, f2, f3);
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            C47932Di r2 = (C47932Di) it.next();
            r2.A03.set(getImageMatrix());
            r2.A04 = r2.A02();
        }
    }

    public void A05(C47932Di r8) {
        Rect rect = r8.A04;
        int max = Math.max(0, -rect.left);
        int min = Math.min(0, getWidth() - rect.right);
        int max2 = Math.max(0, -rect.top);
        int min2 = Math.min(0, getHeight() - rect.bottom);
        if (max == 0 && rect.width() <= getWidth()) {
            max = min;
        }
        if (max2 == 0 && rect.height() <= getHeight()) {
            max2 = min2;
        }
        if (max != 0 || max2 != 0) {
            float f = (float) max;
            float f2 = (float) max2;
            if (f != 0.0f || f2 != 0.0f) {
                A01(f, f2);
                setImageMatrix(getImageViewMatrix());
            }
        }
    }

    public final void A06(C47932Di r14) {
        Rect rect = r14.A04;
        float max = Math.max(1.0f, Math.min((((float) getWidth()) / ((float) rect.width())) * 0.6f, (((float) getHeight()) / ((float) rect.height())) * 0.6f) * getScale());
        if (((double) (Math.abs(max - getScale()) / max)) > 0.1d) {
            float[] fArr = {r14.A05.centerX(), r14.A05.centerY()};
            getImageMatrix().mapPoints(fArr);
            this.A09.post(new RunnableC55742jF(this, new RunnableBRunnable0Shape3S0200000_I0_3(this, 33, r14), getScale(), (max - getScale()) / 300.0f, fArr[0], fArr[1], System.currentTimeMillis()));
        }
    }

    @Override // android.view.View
    public void clearFocus() {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A06;
            if (i < arrayList.size()) {
                C47932Di r1 = (C47932Di) arrayList.get(i);
                r1.A09 = false;
                r1.A04 = r1.A02();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        Paint paint;
        Paint paint2;
        Paint paint3;
        Paint paint4;
        if (!this.A05) {
            super.onDraw(canvas);
        }
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A06;
            if (i < arrayList.size()) {
                C47932Di r8 = (C47932Di) arrayList.get(i);
                Path path = new Path();
                View view = r8.A07;
                float f = view.getResources().getDisplayMetrics().density;
                Paint paint5 = r8.A0D;
                paint5.setStrokeWidth(0.5f + f);
                Rect rect = new Rect();
                view.getDrawingRect(rect);
                boolean z = r8.A08;
                Rect rect2 = r8.A04;
                if (z) {
                    float width = (float) rect2.width();
                    float height = (float) r8.A04.height();
                    Rect rect3 = r8.A04;
                    float f2 = width / 2.0f;
                    path.addCircle(((float) rect3.left) + f2, ((float) rect3.top) + (height / 2.0f), f2, Path.Direction.CW);
                    paint5.setColor(-1112874);
                } else {
                    path.addRect(new RectF(rect2), Path.Direction.CW);
                    paint5.setColor(1728053247);
                    Rect rect4 = new Rect();
                    rect4.set(rect);
                    rect4.right = r8.A04.left;
                    if (r8.A09) {
                        paint = r8.A0B;
                    } else {
                        paint = r8.A0C;
                    }
                    canvas.drawRect(rect4, paint);
                    rect4.set(rect);
                    Rect rect5 = r8.A04;
                    rect4.right = rect5.right;
                    rect4.left = rect5.left;
                    rect4.bottom = rect5.top;
                    if (r8.A09) {
                        paint2 = r8.A0B;
                    } else {
                        paint2 = r8.A0C;
                    }
                    canvas.drawRect(rect4, paint2);
                    rect4.set(rect);
                    Rect rect6 = r8.A04;
                    rect4.right = rect6.right;
                    rect4.left = rect6.left;
                    rect4.top = rect6.bottom;
                    if (r8.A09) {
                        paint3 = r8.A0B;
                    } else {
                        paint3 = r8.A0C;
                    }
                    canvas.drawRect(rect4, paint3);
                    rect4.set(rect);
                    rect4.left = r8.A04.right;
                    if (r8.A09) {
                        paint4 = r8.A0B;
                    } else {
                        paint4 = r8.A0C;
                    }
                    canvas.drawRect(rect4, paint4);
                    int round = Math.round(f);
                    Rect rect7 = r8.A04;
                    int i2 = rect7.left + round;
                    int i3 = rect7.right - round;
                    int i4 = rect7.top;
                    int i5 = i4 + round;
                    int i6 = rect7.bottom;
                    int i7 = i6 - round;
                    float f3 = (float) i2;
                    float f4 = (float) (((i6 - i4) / 3) + i4);
                    float f5 = (float) i3;
                    canvas.drawLine(f3, f4, f5, f4, paint5);
                    Rect rect8 = r8.A04;
                    int i8 = rect8.bottom;
                    float f6 = (float) (i8 - ((i8 - rect8.top) / 3));
                    canvas.drawLine(f3, f6, f5, f6, paint5);
                    Rect rect9 = r8.A04;
                    int i9 = rect9.left;
                    float f7 = (float) (((rect9.right - i9) / 3) + i9);
                    float f8 = (float) i5;
                    float f9 = (float) i7;
                    canvas.drawLine(f7, f8, f7, f9, paint5);
                    Rect rect10 = r8.A04;
                    int i10 = rect10.right;
                    float f10 = (float) (i10 - ((i10 - rect10.left) / 3));
                    canvas.drawLine(f10, f8, f10, f9, paint5);
                }
                canvas.drawPath(path, paint5);
                float f11 = 2.0f * f;
                int round2 = Math.round(f11);
                Rect rect11 = r8.A04;
                int i11 = rect11.left + round2;
                int i12 = rect11.right - round2;
                int i13 = rect11.top + round2;
                int i14 = rect11.bottom - round2;
                int i15 = (int) (f * 24.0f);
                int min = Math.min(i15, rect11.width() >> 2);
                int min2 = Math.min(i15, r8.A04.height() >> 2);
                Rect rect12 = r8.A04;
                int i16 = rect12.left;
                int i17 = i16 + ((rect12.right - i16) >> 1);
                int i18 = rect12.top;
                int i19 = i18 + ((rect12.bottom - i18) >> 1);
                paint5.setStrokeWidth(f11);
                paint5.setColor(-1);
                paint5.setStrokeCap(Paint.Cap.SQUARE);
                int i20 = min >> 1;
                float f12 = (float) (i17 - i20);
                float f13 = (float) i13;
                float f14 = (float) (i17 + i20);
                canvas.drawLine(f12, f13, f14, f13, paint5);
                float f15 = (float) i14;
                canvas.drawLine(f12, f15, f14, f15, paint5);
                float f16 = (float) i11;
                int i21 = min2 >> 1;
                float f17 = (float) (i19 - i21);
                float f18 = (float) (i19 + i21);
                canvas.drawLine(f16, f17, f16, f18, paint5);
                float f19 = (float) i12;
                canvas.drawLine(f19, f17, f19, f18, paint5);
                float f20 = (float) (i11 + min);
                canvas.drawLine(f16, f13, f20, f13, paint5);
                float f21 = (float) (i13 + min2);
                canvas.drawLine(f16, f13, f16, f21, paint5);
                float f22 = (float) (i12 - min);
                canvas.drawLine(f19, f13, f22, f13, paint5);
                canvas.drawLine(f19, f13, f19, f21, paint5);
                canvas.drawLine(f19, f15, f22, f15, paint5);
                float f23 = (float) (i14 - min2);
                canvas.drawLine(f19, f15, f19, f23, paint5);
                canvas.drawLine(f16, f15, f20, f15, paint5);
                canvas.drawLine(f16, f15, f16, f23, paint5);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass289, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.A0A.A00 != null) {
            Iterator it = this.A06.iterator();
            while (it.hasNext()) {
                C47932Di r2 = (C47932Di) it.next();
                r2.A03.set(getImageMatrix());
                r2.A04 = r2.A02();
                if (r2.A09) {
                    A06(r2);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:216:0x03cf, code lost:
        if (r10[1] != 0.0f) goto L_0x03d1;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r18) {
        /*
        // Method dump skipped, instructions count: 1174
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.crop.CropImageView.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
