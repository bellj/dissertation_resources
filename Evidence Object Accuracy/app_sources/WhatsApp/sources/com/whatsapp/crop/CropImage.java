package com.whatsapp.crop;

import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass18U;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1BI;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14900mE;
import X.C14950mJ;
import X.C15410nB;
import X.C22190yg;
import X.C47932Di;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CropImage extends ActivityC13830kP {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public Bitmap.CompressFormat A0A;
    public Bitmap A0B;
    public Matrix A0C;
    public Matrix A0D;
    public Rect A0E;
    public Uri A0F;
    public C14900mE A0G;
    public AnonymousClass18U A0H;
    public AnonymousClass01d A0I;
    public C15410nB A0J;
    public C14950mJ A0K;
    public CropImageView A0L;
    public C47932Di A0M;
    public AnonymousClass1BI A0N;
    public AnonymousClass19M A0O;
    public AnonymousClass1AB A0P;
    public C22190yg A0Q;
    public String A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;

    public CropImage() {
        this(0);
        this.A0A = Bitmap.CompressFormat.JPEG;
        this.A0Z = true;
        this.A09 = 1;
    }

    public CropImage(int i) {
        this.A0V = false;
        ActivityC13830kP.A1P(this, 65);
    }

    public static final Intent A02() {
        Log.e("profileinfo/activityres/fail/load-image");
        return C12970iu.A0A().putExtra("io-error", true).putExtra("error_message_id", R.string.error_load_image);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0V) {
            this.A0V = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            this.A0G = C12970iu.A0R(A1M);
            this.A0O = (AnonymousClass19M) A1M.A6R.get();
            this.A0H = C12990iw.A0T(A1M);
            this.A0K = (C14950mJ) A1M.AKf.get();
            this.A0N = (AnonymousClass1BI) A1M.A83.get();
            this.A0Q = (C22190yg) A1M.AB6.get();
            this.A0I = C12960it.A0Q(A1M);
            this.A0J = (C15410nB) A1M.ALJ.get();
            this.A0P = (AnonymousClass1AB) A1M.AKI.get();
        }
    }

    public final void A22(Rect rect) {
        int i = rect.left;
        int i2 = this.A09;
        rect.left = i * i2;
        rect.right *= i2;
        rect.top *= i2;
        rect.bottom *= i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:82:0x03e2, code lost:
        if (r19.A01 == 0) goto L_0x03e4;
     */
    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r20) {
        /*
        // Method dump skipped, instructions count: 1702
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.crop.CropImage.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        Log.i("crop/ondestroy");
        super.onDestroy();
        Bitmap bitmap = this.A0B;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.A0L.A05 = true;
            this.A0B.recycle();
            this.A0B = null;
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0J, 37));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("rotate", this.A08);
        Rect A01 = this.A0M.A01();
        A22(A01);
        bundle.putParcelable("initialRect", A01);
    }
}
