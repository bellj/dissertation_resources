package com.whatsapp;

import X.AbstractActivityC58452pV;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13830kP;
import X.AnonymousClass01V;
import X.AnonymousClass04S;
import X.AnonymousClass18U;
import X.AnonymousClass2GF;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C22180yf;
import X.C52672bU;
import X.C74033hC;
import X.C74043hD;
import X.C88134Ek;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.AlphaAnimation;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1;
import com.whatsapp.WaInAppBrowsingActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class WaInAppBrowsingActivity extends AbstractActivityC58452pV {
    public static final String A06 = AnonymousClass01V.A08;
    public WebView A00;
    public ProgressBar A01;
    public AnonymousClass04S A02;
    public AnonymousClass18U A03;
    public C22180yf A04;
    public String A05;

    public static /* synthetic */ void A02(WaInAppBrowsingActivity waInAppBrowsingActivity, String str) {
        if (waInAppBrowsingActivity.A1U() != null && str != null && !"about:blank".equals(str) && !waInAppBrowsingActivity.getIntent().getBooleanExtra("webview_hide_url", false)) {
            TextView A0M = C12970iu.A0M(waInAppBrowsingActivity, R.id.website_url);
            A0M.setText(str);
            TextView A0M2 = C12970iu.A0M(waInAppBrowsingActivity, R.id.website_title);
            if (TextUtils.isEmpty(str)) {
                C12960it.A0s(waInAppBrowsingActivity, A0M2, R.color.secondary_text);
                A0M2.setTypeface(null, 0);
                A0M.setVisibility(8);
                return;
            }
            C12960it.A0s(waInAppBrowsingActivity, A0M2, R.color.primary_text);
            A0M2.setTypeface(null, 1);
            AlphaAnimation A0J = C12970iu.A0J();
            A0M.setVisibility(0);
            C12990iw.A1B(A0M, A0J);
        }
    }

    public final void A2e(String str) {
        if (A1U() != null) {
            String stringExtra = getIntent().getStringExtra("webview_title");
            TextView A0M = C12970iu.A0M(this, R.id.website_title);
            if (!TextUtils.isEmpty(stringExtra)) {
                A0M.setText(stringExtra);
            } else if (!TextUtils.isEmpty(str)) {
                A0M.setText(str);
            }
        }
    }

    public void A2f(String str, boolean z) {
        if (this.A02 == null) {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A0A(str);
            A0S.A0B(false);
            A0S.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(z) { // from class: X.3KL
                public final /* synthetic */ boolean A01;

                {
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    WaInAppBrowsingActivity waInAppBrowsingActivity = WaInAppBrowsingActivity.this;
                    boolean z2 = this.A01;
                    dialogInterface.dismiss();
                    if (z2) {
                        Intent A0A = C12970iu.A0A();
                        String stringExtra = waInAppBrowsingActivity.getIntent().getStringExtra("webview_callback");
                        if (stringExtra != null) {
                            A0A.putExtra("webview_callback", stringExtra);
                        }
                        waInAppBrowsingActivity.setResult(0, A0A);
                        waInAppBrowsingActivity.finish();
                    }
                }
            });
            this.A02 = A0S.A05();
        }
    }

    public boolean A2g(String str) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(this.A05) || !str.contains(this.A05)) {
            return false;
        }
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("webview_callback", str);
        C12960it.A0q(this, A0A);
        return true;
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        Intent A0A = C12970iu.A0A();
        String stringExtra = getIntent().getStringExtra("webview_callback");
        if (stringExtra != null) {
            A0A.putExtra("webview_callback", stringExtra);
        }
        setResult(0, A0A);
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.inapp_browsing);
        this.A05 = getIntent().getStringExtra("webview_callback");
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            AnonymousClass2GF A00 = AnonymousClass2GF.A00(this, ((ActivityC13830kP) this).A01, R.drawable.ic_back);
            A00.setColorFilter(getResources().getColor(R.color.dark_gray), PorterDuff.Mode.SRC_ATOP);
            A0Q.setNavigationIcon(A00);
            A0Q.setNavigationOnClickListener(new ViewOnClickCListenerShape6S0100000_I1(this, 9));
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_page_progress);
            this.A01 = progressBar;
            C88134Ek.A00(progressBar, R.color.webview_progress_foreground);
        }
        WebView webView = (WebView) findViewById(R.id.web_view);
        this.A00 = webView;
        boolean booleanExtra = getIntent().getBooleanExtra("webview_javascript_enabled", false);
        webView.getSettings().setAllowContentAccess(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setJavaScriptEnabled(booleanExtra);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setGeolocationEnabled(false);
        webView.clearCache(true);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.setWebChromeClient(new C74033hC());
        CookieManager.getInstance().setAcceptCookie(false);
        if (Build.VERSION.SDK_INT < 18) {
            webView.getSettings().setPluginState(WebSettings.PluginState.OFF);
            webView.getSettings().setSavePassword(false);
        }
        webView.getSettings().setAllowFileAccessFromFileURLs(false);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(false);
        this.A00.setWebViewClient(new C52672bU(this));
        this.A00.setWebChromeClient(new C74043hD(this));
        getWindow().setFlags(DefaultCrypto.BUFFER_SIZE, DefaultCrypto.BUFFER_SIZE);
        if (Build.VERSION.SDK_INT >= 27) {
            WebView.startSafeBrowsing(this, new ValueCallback() { // from class: X.4oj
                @Override // android.webkit.ValueCallback
                public final void onReceiveValue(Object obj) {
                    Log.e("SecuredWebViewUtil/enableSafeBrowsing: Safe browsing is not allowed");
                }
            });
        }
        A2e(getString(R.string.webview_loading));
        String stringExtra = getIntent().getStringExtra("webview_url");
        if (A2g(stringExtra)) {
            return;
        }
        if (getIntent().getBooleanExtra("webview_post_on_initial_request", false)) {
            String stringExtra2 = getIntent().getStringExtra("webview_initial_body_params");
            if (stringExtra2 == null) {
                stringExtra2 = "";
            }
            this.A00.postUrl(stringExtra, stringExtra2.getBytes());
            return;
        }
        this.A00.loadUrl(stringExtra);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        WebView webView = this.A00;
        if (webView != null) {
            webView.onPause();
            webView.loadUrl("about:blank");
            webView.clearHistory();
            webView.clearCache(true);
            webView.removeAllViews();
            webView.destroyDrawingCache();
        }
        this.A00 = null;
    }
}
