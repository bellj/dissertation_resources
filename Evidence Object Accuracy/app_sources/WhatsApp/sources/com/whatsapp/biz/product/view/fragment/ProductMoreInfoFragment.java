package com.whatsapp.biz.product.view.fragment;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass4TA;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C22650zQ;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.Group;
import com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.product.viewmodel.ComplianceInfoViewModel;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ProductMoreInfoFragment extends Hilt_ProductMoreInfoFragment {
    public ProgressBar A00;
    public Group A01;
    public Group A02;
    public Group A03;
    public TextEmojiLabel A04;
    public TextEmojiLabel A05;
    public TextEmojiLabel A06;
    public WaTextView A07;
    public ComplianceInfoViewModel A08;
    public AnonymousClass018 A09;
    public C22650zQ A0A;

    public static ProductMoreInfoFragment A00(UserJid userJid, String str) {
        ProductMoreInfoFragment productMoreInfoFragment = new ProductMoreInfoFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("product_owner_jid", userJid);
        A0D.putString("product_id", str);
        productMoreInfoFragment.A0U(A0D);
        return productMoreInfoFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.product_info_fragment, viewGroup, false);
        View A0D = AnonymousClass028.A0D(inflate, R.id.close_button);
        C12960it.A0r(A01(), A0D, R.string.close);
        AbstractView$OnClickListenerC34281fs.A00(A0D, this, 23);
        this.A00 = (ProgressBar) AnonymousClass028.A0D(inflate, R.id.more_info_progress);
        this.A04 = C12970iu.A0T(inflate, R.id.more_info_country_description);
        this.A06 = C12970iu.A0T(inflate, R.id.more_info_name_description);
        this.A05 = C12970iu.A0T(inflate, R.id.more_info_address_description);
        this.A02 = (Group) AnonymousClass028.A0D(inflate, R.id.importer_country_group);
        this.A03 = (Group) AnonymousClass028.A0D(inflate, R.id.importer_name_group);
        this.A01 = (Group) AnonymousClass028.A0D(inflate, R.id.importer_address_group);
        this.A07 = C12960it.A0N(inflate, R.id.compliance_network_error_info);
        String string = A03().getString("product_id");
        ComplianceInfoViewModel complianceInfoViewModel = this.A08;
        AnonymousClass016 r2 = complianceInfoViewModel.A01;
        r2.A0B(0);
        if (!complianceInfoViewModel.A04.A07(new AnonymousClass4TA((UserJid) A03().getParcelable("product_owner_jid"), 0, 0, string, complianceInfoViewModel.A03.A00, true))) {
            C12960it.A1A(r2, 3);
        } else {
            complianceInfoViewModel.A05.Ab2(new RunnableBRunnable0Shape1S1100000_I1(2, string, complianceInfoViewModel));
        }
        C12960it.A19(A0G(), this.A08.A00, this, 20);
        C12960it.A19(A0G(), this.A08.A01, this, 21);
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A08 = (ComplianceInfoViewModel) C13000ix.A02(this).A00(ComplianceInfoViewModel.class);
    }
}
