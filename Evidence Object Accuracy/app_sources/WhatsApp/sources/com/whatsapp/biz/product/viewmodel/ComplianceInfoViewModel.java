package com.whatsapp.biz.product.viewmodel;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass19Q;
import X.AnonymousClass19T;
import X.C12980iv;
import X.C19850um;

/* loaded from: classes2.dex */
public class ComplianceInfoViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C19850um A02;
    public final AnonymousClass19Q A03;
    public final AnonymousClass19T A04;
    public final AbstractC14440lR A05;

    public ComplianceInfoViewModel(C19850um r2, AnonymousClass19Q r3, AnonymousClass19T r4, AbstractC14440lR r5) {
        this.A05 = r5;
        this.A04 = r4;
        this.A03 = r3;
        this.A02 = r2;
    }
}
