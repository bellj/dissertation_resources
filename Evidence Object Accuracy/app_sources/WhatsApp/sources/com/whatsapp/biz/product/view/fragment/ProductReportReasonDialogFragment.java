package com.whatsapp.biz.product.view.fragment;

import X.AnonymousClass04S;
import X.C004802e;
import X.C12960it;
import X.C14900mE;
import X.C90874Po;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.biz.product.view.fragment.ProductReportReasonDialogFragment;

/* loaded from: classes2.dex */
public class ProductReportReasonDialogFragment extends Hilt_ProductReportReasonDialogFragment {
    public int A00 = -1;
    public C14900mE A01;
    public final C90874Po[] A02 = {new C90874Po(this, "no-match", R.string.catalog_product_report_reason_no_match), new C90874Po(this, "spam", R.string.catalog_product_report_reason_spam), new C90874Po(this, "illegal", R.string.catalog_product_report_reason_illegal), new C90874Po(this, "scam", R.string.catalog_product_report_reason_scam), new C90874Po(this, "knockoff", R.string.catalog_product_report_reason_knockoff), new C90874Po(this, "other", R.string.catalog_product_report_reason_other)};

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0K = C12960it.A0K(this);
        C90874Po[] r4 = this.A02;
        int length = r4.length;
        CharSequence[] charSequenceArr = new CharSequence[length];
        for (int i = 0; i < length; i++) {
            charSequenceArr[i] = A0I(r4[i].A00);
        }
        A0K.A09(new IDxCListenerShape9S0100000_2_I1(this, 16), charSequenceArr, this.A00);
        A0K.A07(R.string.catalog_product_report_details_title);
        A0K.setPositiveButton(R.string.submit, null);
        AnonymousClass04S create = A0K.create();
        create.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.3LC
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                AbstractView$OnClickListenerC34281fs.A00(((AnonymousClass04S) dialogInterface).A00.A0G, ProductReportReasonDialogFragment.this, 24);
            }
        });
        return create;
    }
}
