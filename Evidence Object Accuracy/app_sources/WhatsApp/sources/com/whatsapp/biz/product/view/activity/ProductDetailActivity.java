package com.whatsapp.biz.product.view.activity;

import X.AbstractActivityC59292uP;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.AnonymousClass00E;
import X.AnonymousClass024;
import X.AnonymousClass02B;
import X.AnonymousClass10A;
import X.AnonymousClass19O;
import X.AnonymousClass19Q;
import X.AnonymousClass19T;
import X.AnonymousClass1J1;
import X.AnonymousClass1M2;
import X.AnonymousClass1US;
import X.AnonymousClass1XV;
import X.AnonymousClass281;
import X.AnonymousClass282;
import X.AnonymousClass283;
import X.AnonymousClass2Cu;
import X.AnonymousClass2SH;
import X.AnonymousClass31O;
import X.AnonymousClass31Q;
import X.AnonymousClass4TA;
import X.C103994re;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14960mK;
import X.C15370n3;
import X.C15610nY;
import X.C16120oU;
import X.C19840ul;
import X.C19850um;
import X.C21270x9;
import X.C34271fr;
import X.C44691zO;
import X.C53912fi;
import X.C59002to;
import X.C59152u4;
import X.C65003Ht;
import X.C68263Us;
import X.C70403bE;
import X.C84353zA;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.InfoCard;
import com.whatsapp.R;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.biz.product.view.fragment.ReportProductDialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.Collections;

/* loaded from: classes2.dex */
public class ProductDetailActivity extends AbstractActivityC59292uP implements AnonymousClass282 {
    public C34271fr A00;
    public AnonymousClass10A A01;
    public C15610nY A02;
    public AnonymousClass1J1 A03;
    public C21270x9 A04;
    public final AnonymousClass2Cu A05 = new C84353zA(this);
    public final AnonymousClass2SH A06 = new C59152u4(this);

    public static void A09(Context context, View view, C19850um r13, AnonymousClass1XV r14, AnonymousClass19O r15, int i, boolean z, boolean z2) {
        String str = r14.A06;
        UserJid userJid = r14.A01;
        C44691zO A05 = r13.A05(null, str);
        if (A05 == null) {
            C70403bE r0 = new C70403bE(context, view, r13, r14, r15, i, z2);
            if (z) {
                r15.A08(view, r14, r0);
            } else {
                r15.A07(view, r14, r0);
            }
        } else {
            AnonymousClass283.A02(context, C14960mK.A0c(context, false), userJid, null, null, A05.A0D, i, z2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0178, code lost:
        if (r6.A0H != false) goto L_0x017a;
     */
    @Override // X.AnonymousClass283
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2e() {
        /*
        // Method dump skipped, instructions count: 749
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.biz.product.view.activity.ProductDetailActivity.A2e():void");
    }

    public final void A2g() {
        int dimension = (int) getResources().getDimension(R.dimen.medium_thumbnail_size);
        this.A0S.A07(new AnonymousClass4TA(this.A0h, Integer.valueOf(getIntent().getIntExtra("thumb_width", dimension)), Integer.valueOf(getIntent().getIntExtra("thumb_height", dimension)), this.A0l, this.A0R.A00, false));
    }

    public void A2h(String str) {
        C44691zO r0 = this.A0Q;
        if (r0 != null) {
            AnonymousClass19Q r1 = this.A0R;
            String str2 = r0.A0D;
            UserJid userJid = this.A0h;
            AnonymousClass00E r4 = r1.A05;
            boolean A01 = r4.A01(r1.A00);
            if (r1.A06.contains(13) || A01) {
                int i = 1;
                if (r1.A03.A07(904)) {
                    AnonymousClass31Q r2 = new AnonymousClass31Q();
                    r2.A08 = C12980iv.A0l(r1.A08.getAndIncrement());
                    r2.A05 = 13;
                    r2.A0A = str;
                    r2.A0B = r1.A00;
                    r2.A0E = str2;
                    r2.A09 = userJid.getRawString();
                    int i2 = r1.A07.get();
                    if (i2 != 0) {
                        r2.A04 = Integer.valueOf(i2);
                    }
                    if (!A01) {
                        r2.A01 = Boolean.TRUE;
                    }
                    r2.A03 = Integer.valueOf(C65003Ht.A00(r1.A02.A00(userJid)));
                    C16120oU r12 = r1.A04;
                    if (A01) {
                        i = r4.A03 * 1;
                    }
                    r12.A08(r2, i);
                } else {
                    AnonymousClass31O r22 = new AnonymousClass31O();
                    r22.A05 = 13;
                    r22.A09 = str;
                    r22.A0A = r1.A00;
                    r22.A0D = str2;
                    r22.A08 = userJid.getRawString();
                    int i3 = r1.A07.get();
                    if (i3 != 0) {
                        r22.A04 = Integer.valueOf(i3);
                    }
                    if (!A01) {
                        r22.A01 = Boolean.TRUE;
                    }
                    r22.A03 = Integer.valueOf(C65003Ht.A00(r1.A02.A00(userJid)));
                    r22.A0D = null;
                    r22.A08 = null;
                    r22.A0C = null;
                    C16120oU r13 = r1.A04;
                    if (A01) {
                        i = r4.A03 * 1;
                    }
                    r13.A08(r22, i);
                }
            }
            AnonymousClass281 r42 = new AnonymousClass281(this.A0h, this.A0Q.A0D, str, this.A0R.A00);
            AnonymousClass19T r3 = this.A0S;
            C19840ul r7 = r3.A0L;
            r7.A01(774782053, "report_product_tag", "CatalogManager");
            if (new C59002to(r3.A0A, r3, r42, r3.A0J, r3.A0K, r7).A02()) {
                A2C(R.string.catalog_product_report_sending);
            } else {
                this.A0S.A02(r42, false);
            }
        }
    }

    @Override // X.AnonymousClass283, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 55) {
            ((AnonymousClass283) this).A0N.A02(this, this.A0U, this.A0h, this.A0h, Collections.singletonList(this.A0Q), 2, 0, 0);
        }
    }

    @Override // X.AnonymousClass283, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        this.A01.A03(this.A05);
        InfoCard infoCard = (InfoCard) findViewById(R.id.product_business_info_container);
        C12960it.A17(this, this.A0a.A05, 3);
        this.A0a.A03.A05(this, new AnonymousClass02B(bundle, infoCard, this) { // from class: X.3RU
            public final /* synthetic */ Bundle A00;
            public final /* synthetic */ InfoCard A01;
            public final /* synthetic */ ProductDetailActivity A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e0, code lost:
                if (r9 != null) goto L_0x009c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e7, code lost:
                if (r9 != null) goto L_0x00e9;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 243
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3RU.ANq(java.lang.Object):void");
            }
        });
        C12960it.A18(this, this.A0a.A07, 18);
        ((AnonymousClass283) this).A0O.A03(this.A06);
        this.A0S.A0N.add(this);
        if (infoCard != null && !((ActivityC13790kL) this).A01.A0F(this.A0h)) {
            C12970iu.A1N(this, R.id.divider_bizinfo, 0);
            infoCard.setVisibility(0);
            View findViewById = findViewById(R.id.contact_info_container);
            TextView A0M = C12970iu.A0M(this, R.id.contact_name);
            ImageView imageView = (ImageView) findViewById(R.id.contact_photo);
            AnonymousClass1M2 A00 = this.A0e.A00(this.A0h);
            if (A00 == null) {
                str = null;
            } else {
                str = A00.A08;
            }
            C15370n3 A0B = this.A0c.A0B(this.A0h);
            if (A0M != null) {
                if (AnonymousClass1US.A0C(str)) {
                    str = this.A02.A04(A0B);
                }
                A0M.setText(str);
            }
            AnonymousClass1J1 A04 = this.A04.A04(this, "product-detail-activity");
            this.A03 = A04;
            A04.A06(imageView, A0B);
            AbstractView$OnClickListenerC34281fs.A00(findViewById, this, 20);
        }
        C68263Us r2 = this.A0a.A0D;
        C12990iw.A1O(r2.A0A, r2, 37);
        ((AnonymousClass283) this).A0M.A05();
        this.A0f.A08(new AnonymousClass024() { // from class: X.4rW
            @Override // X.AnonymousClass024
            public final void accept(Object obj) {
                C40591rq r4 = (C40591rq) obj;
                r4.A06 = Long.valueOf(C40601rr.A00(r4.A06, 1));
            }
        }, this.A0h);
        this.A0f.A08(new C103994re(0), this.A0h);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        if (r2 == false) goto L_0x0022;
     */
    @Override // X.AnonymousClass283, X.ActivityC13790kL, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onCreateOptionsMenu(android.view.Menu r4) {
        /*
            r3 = this;
            android.view.MenuInflater r1 = r3.getMenuInflater()
            r0 = 2131623953(0x7f0e0011, float:1.8875072E38)
            r1.inflate(r0, r4)
            X.2fi r2 = r3.A0a
            int r1 = r3.A00
            X.1zO r0 = r3.A0Q
            boolean r2 = r2.A06(r0, r1)
            r0 = 2131364357(0x7f0a0a05, float:1.8348549E38)
            android.view.MenuItem r1 = r4.findItem(r0)
            boolean r0 = r3.A0o
            if (r0 != 0) goto L_0x0022
            r0 = 1
            if (r2 != 0) goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            r1.setVisible(r0)
            boolean r0 = super.onCreateOptionsMenu(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.biz.product.view.activity.ProductDetailActivity.onCreateOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.AnonymousClass283, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0S.A0N.remove(this);
        ((AnonymousClass283) this).A0O.A04(this.A06);
        this.A01.A04(this.A05);
        super.onDestroy();
        AnonymousClass1J1 r0 = this.A03;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass283, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (R.id.menu_report == itemId) {
            Adl(new ReportProductDialogFragment(), null);
            return true;
        } else if (16908332 != itemId) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            this.A0a.A05(this);
            return true;
        }
    }

    @Override // X.AnonymousClass283, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (getIntent().getBooleanExtra("partial_loaded", false)) {
            C53912fi r2 = this.A0a;
            C12990iw.A1J(r2.A07, r2.A0F.A0B());
        }
    }
}
