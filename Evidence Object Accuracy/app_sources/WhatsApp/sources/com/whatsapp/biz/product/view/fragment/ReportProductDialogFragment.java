package com.whatsapp.biz.product.view.fragment;

import X.C004802e;
import X.C12960it;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ReportProductDialogFragment extends Hilt_ReportProductDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0K = C12960it.A0K(this);
        A0K.A07(R.string.catalog_product_report_dialog_title);
        A0K.A06(R.string.catalog_product_report_content);
        A0K.A00(R.string.catalog_product_report_title, new IDxCListenerShape9S0100000_2_I1(this, 18));
        C12970iu.A1M(A0K, this, 8, R.string.catalog_product_report_details_title);
        C12970iu.A1K(A0K, this, 17, R.string.cancel);
        return A0K.create();
    }
}
