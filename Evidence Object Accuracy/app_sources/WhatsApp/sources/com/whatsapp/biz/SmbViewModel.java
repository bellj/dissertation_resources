package com.whatsapp.biz;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.C14820m6;
import X.C14850m9;
import X.C20540vv;
import X.C27691It;

/* loaded from: classes3.dex */
public class SmbViewModel extends AnonymousClass015 {
    public final C14820m6 A00;
    public final C20540vv A01;
    public final C14850m9 A02;
    public final C27691It A03;
    public final AbstractC14440lR A04;

    public SmbViewModel(C14820m6 r3, C20540vv r4, C14850m9 r5, AbstractC14440lR r6) {
        C27691It r1 = new C27691It();
        this.A03 = r1;
        this.A02 = r5;
        this.A04 = r6;
        this.A01 = r4;
        this.A00 = r3;
        r1.A0A(Boolean.FALSE);
    }
}
