package com.whatsapp.biz.cart.view.fragment;

import X.AbstractC008704k;
import X.AbstractC116455Vm;
import X.AbstractC14020ki;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC30111Wd;
import X.AbstractC32741cf;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass04Y;
import X.AnonymousClass10S;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass19Q;
import X.AnonymousClass1FZ;
import X.AnonymousClass1M2;
import X.AnonymousClass1Y6;
import X.AnonymousClass283;
import X.AnonymousClass2E4;
import X.AnonymousClass2EH;
import X.AnonymousClass2EM;
import X.AnonymousClass2EN;
import X.AnonymousClass2GF;
import X.AnonymousClass3C7;
import X.AnonymousClass3HN;
import X.AnonymousClass3M0;
import X.AnonymousClass3M5;
import X.AnonymousClass3M7;
import X.AnonymousClass4J6;
import X.AnonymousClass4JV;
import X.AnonymousClass4UV;
import X.C1096352l;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15270mq;
import X.C15330mx;
import X.C15370n3;
import X.C15570nT;
import X.C15610nY;
import X.C16170oZ;
import X.C16630pM;
import X.C17220qS;
import X.C19840ul;
import X.C19850um;
import X.C19870uo;
import X.C20670w8;
import X.C21770xx;
import X.C22190yg;
import X.C22700zV;
import X.C231510o;
import X.C238013b;
import X.C252718t;
import X.C252918v;
import X.C25791Av;
import X.C25831Az;
import X.C27131Gd;
import X.C30711Yn;
import X.C34271fr;
import X.C36981kz;
import X.C37041lD;
import X.C37051lE;
import X.C37071lG;
import X.C44691zO;
import X.C44741zT;
import X.C48202Ev;
import X.C67433Rm;
import X.C84503zP;
import X.C84513zQ;
import X.C88014Dx;
import X.C89224Jd;
import X.C90824Pj;
import X.C92584Wm;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.biz.order.viewmodel.OrderInfoViewModel;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class CartFragment extends Hilt_CartFragment {
    public static final HashMap A0y = new HashMap();
    public static final HashMap A0z = new HashMap();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public LinearLayout A08;
    public TextView A09;
    public RecyclerView A0A;
    public AbstractC15710nm A0B;
    public AnonymousClass4J6 A0C;
    public C14330lG A0D;
    public C14900mE A0E;
    public KeyboardPopupLayout A0F;
    public C15570nT A0G;
    public C16170oZ A0H;
    public C20670w8 A0I;
    public WaTextView A0J;
    public WaTextView A0K;
    public C14650lo A0L;
    public C25831Az A0M;
    public C25791Av A0N;
    public C21770xx A0O;
    public C48202Ev A0P;
    public C37051lE A0Q;
    public C37041lD A0R;
    public AnonymousClass1FZ A0S;
    public C19850um A0T;
    public AnonymousClass19Q A0U;
    public C252918v A0V;
    public C37071lG A0W;
    public OrderInfoViewModel A0X;
    public AnonymousClass3HN A0Y;
    public C238013b A0Z;
    public AnonymousClass10S A0a;
    public C22700zV A0b;
    public C15610nY A0c;
    public AnonymousClass01d A0d;
    public C14830m7 A0e;
    public C14820m6 A0f;
    public AnonymousClass018 A0g;
    public AnonymousClass19M A0h;
    public C15270mq A0i;
    public C231510o A0j;
    public AnonymousClass193 A0k;
    public C14850m9 A0l;
    public UserJid A0m;
    public MentionableEntry A0n;
    public C19870uo A0o;
    public C17220qS A0p;
    public C19840ul A0q;
    public C16630pM A0r;
    public C252718t A0s;
    public C22190yg A0t;
    public AbstractC14440lR A0u;
    public boolean A0v = false;
    public final AbstractC116455Vm A0w = new C1096352l(this);
    public final C27131Gd A0x = new C36981kz(this);

    public static CartFragment A00(UserJid userJid, String str, int i) {
        CartFragment cartFragment = new CartFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("extra_business_id", userJid);
        bundle.putInt("extra_entry_point", i);
        bundle.putString("extra_product_id", str);
        cartFragment.A0U(bundle);
        return cartFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        MentionableEntry mentionableEntry;
        super.A0r();
        UserJid userJid = this.A0m;
        if (!(userJid == null || (mentionableEntry = this.A0n) == null)) {
            A0z.put(userJid, AbstractC32741cf.A04(mentionableEntry.getStringText()));
            A0y.put(this.A0m, AnonymousClass1Y6.A00(this.A0n.getMentions()));
        }
        if (this.A00 == 1) {
            A0C().setRequestedOrientation(1);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        int i;
        super.A0w(bundle);
        if (this.A0i.isShowing()) {
            i = 1;
        } else {
            i = 2;
            if (C252718t.A00(this.A0F)) {
                i = 0;
            }
        }
        this.A01 = i;
        bundle.putInt("extra_input_method", i);
        bundle.putBoolean("extra_is_sending_order", this.A0v);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        Parcelable parcelable = A03().getParcelable("extra_business_id");
        AnonymousClass009.A05(parcelable);
        this.A0m = (UserJid) parcelable;
        int i = A03().getInt("extra_entry_point");
        this.A00 = i;
        this.A0q.A05("cart_view_tag", "IsConsumer", !this.A0G.A0F(this.A0m));
        if (i == 0) {
            str = "Catalog";
        } else if (i == 1) {
            str = "Product";
        } else if (i == 2) {
            str = "Collection";
        } else if (i == 3) {
            str = "PLM";
        } else if (i == 4) {
            str = "Conversation";
        } else if (i == 5) {
            str = "CatalogSearch";
        } else {
            throw new IllegalStateException("CartFragment/logQplCartViewAnnotations/unhandled entry point");
        }
        this.A0q.A04("cart_view_tag", "EntryPoint", str);
        View inflate = layoutInflater.inflate(R.layout.fragment_cart, viewGroup, false);
        this.A06 = inflate;
        this.A0F = (KeyboardPopupLayout) AnonymousClass028.A0D(inflate, R.id.cart);
        this.A0n = (MentionableEntry) AnonymousClass028.A0D(this.A06, R.id.entry);
        ImageButton imageButton = (ImageButton) AnonymousClass028.A0D(this.A06, R.id.emoji_picker_btn);
        this.A05 = AnonymousClass028.A0D(this.A06, R.id.footer);
        this.A04 = AnonymousClass028.A0D(this.A06, R.id.cart_empty_container);
        this.A0K = (WaTextView) AnonymousClass028.A0D(this.A06, R.id.empty_cart_title);
        this.A0J = (WaTextView) AnonymousClass028.A0D(this.A06, R.id.empty_cart_description);
        this.A07 = AnonymousClass028.A0D(this.A06, R.id.cart_empty_view_catalog_btn);
        this.A0A = (RecyclerView) AnonymousClass028.A0D(this.A06, R.id.cart_items_recycler_view);
        this.A08 = (LinearLayout) AnonymousClass028.A0D(this.A06, R.id.send_cart_cta);
        if (this.A0P.A01.A07(1867)) {
            this.A09 = (TextView) AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_message);
        }
        View A0D = AnonymousClass028.A0D(this.A06, R.id.send);
        View A0D2 = AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_btn);
        View A0D3 = AnonymousClass028.A0D(this.A06, R.id.cart_close_btn);
        this.A06.setMinimumHeight(A1K());
        View A0D4 = AnonymousClass028.A0D(this.A06, R.id.text_entry_layout);
        int max = Math.max(A0D4.getPaddingLeft(), A0D4.getPaddingRight());
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) A0D4.getLayoutParams();
        if (!this.A0g.A04().A06) {
            layoutParams.rightMargin = max;
        } else {
            layoutParams.leftMargin = max;
        }
        A0D4.setLayoutParams(layoutParams);
        this.A03 = A02().getDimensionPixelSize(R.dimen.order_message_thumbnail_width);
        this.A02 = A02().getDimensionPixelSize(R.dimen.order_message_thumbnail_height);
        C14900mE r1 = this.A0E;
        C19840ul r13 = this.A0q;
        C17220qS r11 = this.A0p;
        C14650lo r15 = this.A0L;
        C89224Jd r4 = new C89224Jd(new AnonymousClass3C7());
        C19870uo r5 = this.A0o;
        C14820m6 r3 = this.A0f;
        C48202Ev r12 = this.A0P;
        AbstractC14440lR r122 = this.A0u;
        AnonymousClass2EH r24 = new AnonymousClass2EH(r1, r15, r12, r4, r3, r5, r11, r13, r122);
        AnonymousClass3C7 r14 = new AnonymousClass3C7();
        AnonymousClass2EM r16 = new AnonymousClass2EM(r1, r15, this.A0O, new AnonymousClass2EN(r1, r15, new C90824Pj(new C88014Dx(), this.A0Y, r14), r5, r11, r13), this.A0S, this.A0T, this.A0U, r24, this.A0m, r122);
        Dialog dialog = ((DialogFragment) this).A03;
        if (!(dialog == null || dialog.getWindow() == null)) {
            ((DialogFragment) this).A03.getWindow().setSoftInputMode(16);
        }
        this.A0n.setHint(A01().getString(R.string.message));
        C14850m9 r32 = this.A0l;
        this.A0Q = new C37051lE(this.A0C, r16, this, this, this.A0W, r32);
        ViewOnClickCListenerShape13S0100000_I0 viewOnClickCListenerShape13S0100000_I0 = new ViewOnClickCListenerShape13S0100000_I0(this, 14);
        if (this.A0l.A07(1660)) {
            A0D2.setOnClickListener(viewOnClickCListenerShape13S0100000_I0);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.A0A.getLayoutParams();
            layoutParams2.setMargins(0, 0, 0, 0);
            layoutParams2.addRule(2, R.id.send_cart_cta);
            this.A0A.setLayoutParams(layoutParams2);
            this.A08.setVisibility(0);
            this.A05.setVisibility(8);
        } else {
            A0D.setOnClickListener(viewOnClickCListenerShape13S0100000_I0);
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.A0A.getLayoutParams();
            layoutParams3.setMargins(0, 0, 0, A02().getDimensionPixelSize(R.dimen.send_cart_cta_btn_margin_vertical));
            layoutParams3.addRule(2, R.id.footer);
            this.A0A.setLayoutParams(layoutParams3);
            this.A08.setVisibility(8);
            this.A05.setVisibility(0);
        }
        A0D3.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 15));
        this.A07.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 16));
        RecyclerView recyclerView = this.A0A;
        recyclerView.A0h = true;
        recyclerView.setAdapter(this.A0Q);
        AnonymousClass04Y r33 = this.A0A.A0R;
        if (r33 instanceof AbstractC008704k) {
            ((AbstractC008704k) r33).A00 = false;
        }
        this.A0X = (OrderInfoViewModel) new AnonymousClass02A(this).A00(OrderInfoViewModel.class);
        C14830m7 r9 = this.A0e;
        UserJid userJid = this.A0m;
        C16170oZ r112 = this.A0H;
        C20670w8 r123 = this.A0I;
        C19840ul r7 = this.A0q;
        C15610nY r6 = this.A0c;
        AbstractC14440lR r52 = this.A0u;
        AnonymousClass2E4 r42 = new AnonymousClass2E4(this.A0D, this.A0t, r52);
        C25831Az r142 = this.A0M;
        C22700zV r34 = this.A0b;
        C37041lD r17 = (C37041lD) new AnonymousClass02A(new C67433Rm(r112, r123, this.A0L, r142, this.A0P, r16, this.A0U, r42, r34, r6, r9, userJid, r7, r52), this).A00(C37041lD.class);
        this.A0R = r17;
        r17.A07.A05(A0G(), new AnonymousClass02B() { // from class: X.4sb
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment.this.A1Q(C12970iu.A1Y(obj));
            }
        });
        this.A0R.A0B.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                String str2;
                CartFragment cartFragment = CartFragment.this;
                String trim = cartFragment.A0n.getStringText().trim();
                cartFragment.A0n.setText("");
                C37041lD r35 = cartFragment.A0R;
                ActivityC000800j r43 = (ActivityC000800j) cartFragment.A0C();
                C37051lE r53 = cartFragment.A0Q;
                C37071lG r62 = cartFragment.A0W;
                C22700zV r0 = r35.A0L;
                UserJid userJid2 = r35.A0O;
                AnonymousClass1M2 A00 = r0.A00(userJid2);
                if (A00 == null || (str2 = A00.A08) == null) {
                    r35.A0E.A00(new GetVNameCertificateJob(userJid2));
                } else {
                    r35.A04(r43, r53, r62, trim, str2);
                }
            }
        });
        this.A0R.A02.A05(A0G(), new AnonymousClass02B() { // from class: X.3Po
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                boolean booleanValue;
                CartFragment cartFragment = CartFragment.this;
                List<C92584Wm> list = (List) obj;
                ((ActivityC13810kN) cartFragment.A0C()).AaN();
                C37051lE r62 = cartFragment.A0Q;
                Boolean bool = (Boolean) cartFragment.A0R.A06.A01();
                r62.A00 = new Date();
                List list2 = r62.A07;
                list2.clear();
                if (bool == null) {
                    booleanValue = false;
                } else {
                    booleanValue = bool.booleanValue();
                }
                list2.add(new C84513zQ(booleanValue));
                for (C92584Wm r2 : list) {
                    if (r2.A01.A00 == 0) {
                        list2.add(new C84493zO(r2, r62.A00));
                    }
                }
                if (!r62.A06.A07(1660)) {
                    list2.add(new C84503zP());
                }
                r62.A02();
                cartFragment.A1M();
                C37041lD r22 = cartFragment.A0R;
                int i2 = cartFragment.A03;
                int i3 = cartFragment.A02;
                List<C92584Wm> A0F = cartFragment.A0Q.A0F();
                if (r22.A00 || A0F.isEmpty()) {
                    cartFragment.A0q.A04("cart_view_tag", "ProductsCount", String.valueOf(cartFragment.A0Q.A0E()));
                    cartFragment.A0q.A06("cart_view_tag", true);
                    return;
                }
                r22.A00 = true;
                ArrayList A0y2 = C12980iv.A0y(A0F);
                for (C92584Wm r0 : A0F) {
                    A0y2.add(r0.A01.A0D);
                }
                AnonymousClass2EM r35 = r22.A0I;
                r35.A0D.A02(new AnonymousClass4RN(r22.A0O, A0y2, i2, i3), r35.A0K);
            }
        });
        this.A0R.A09.A05(A0G(), new AnonymousClass02B() { // from class: X.4sa
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                if (C12970iu.A1Y(obj)) {
                    cartFragment.A0v = false;
                    cartFragment.A1Q(true);
                    cartFragment.A0q.A06("order_creates_tag", false);
                }
            }
        });
        this.A0R.A06.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pq
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                boolean A1Y = C12970iu.A1Y(obj);
                cartFragment.A07.setVisibility(C12960it.A02(A1Y ? 1 : 0));
                C37051lE r35 = cartFragment.A0Q;
                int i2 = 0;
                while (true) {
                    List list = r35.A07;
                    if (i2 < list.size()) {
                        AnonymousClass4JV r18 = (AnonymousClass4JV) list.get(i2);
                        if (r18 instanceof C84513zQ) {
                            ((C84513zQ) r18).A02 = A1Y;
                            r35.A03(i2);
                            return;
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        });
        this.A0R.A05.A05(A0G(), new AnonymousClass02B() { // from class: X.3Ps
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                if (C12970iu.A1Y(obj)) {
                    C37041lD r18 = cartFragment.A0R;
                    r18.A00 = false;
                    r18.A01 = false;
                    AnonymousClass2EM r35 = r18.A0I;
                    r35.A0L.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r35, 6));
                }
            }
        });
        this.A0R.A08.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pr
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                if (C12970iu.A1Y(obj)) {
                    C37041lD r18 = cartFragment.A0R;
                    if (!r18.A01) {
                        r18.A07.A0A(Boolean.TRUE);
                    }
                    cartFragment.A0q.A06("cart_view_tag", false);
                }
            }
        });
        this.A0R.A04.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pu
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                String str2 = (String) obj;
                C37051lE r43 = cartFragment.A0Q;
                int i2 = 0;
                while (true) {
                    List list = r43.A07;
                    if (i2 >= list.size()) {
                        break;
                    }
                    AnonymousClass4JV r18 = (AnonymousClass4JV) list.get(i2);
                    if ((r18 instanceof C84493zO) && str2.equals(((C84493zO) r18).A00.A01.A0D)) {
                        list.remove(i2);
                        r43.A05(i2);
                        break;
                    }
                    i2++;
                }
                cartFragment.A1M();
            }
        });
        this.A0R.A0C.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pn
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                Pair pair = (Pair) obj;
                C37051lE r43 = cartFragment.A0Q;
                String str2 = (String) pair.first;
                Number number = (Number) pair.second;
                if (str2 != null && number != null) {
                    int i2 = 0;
                    while (true) {
                        List list = r43.A07;
                        if (i2 >= list.size()) {
                            break;
                        }
                        AnonymousClass4JV r18 = (AnonymousClass4JV) list.get(i2);
                        if (r18 instanceof C84493zO) {
                            C92584Wm r2 = ((C84493zO) r18).A00;
                            if (str2.equals(r2.A01.A0D)) {
                                r2.A00 = (long) number.intValue();
                                r43.A03(i2);
                                break;
                            }
                        }
                        i2++;
                    }
                }
                cartFragment.A1M();
            }
        });
        this.A0R.A03.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pt
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CartFragment cartFragment = CartFragment.this;
                int A05 = C12960it.A05(obj);
                C004802e A0K = C12960it.A0K(cartFragment);
                A0K.A0B(false);
                A0K.A06(A05);
                C12970iu.A1I(A0K);
                C12970iu.A1J(A0K);
            }
        });
        if (this.A0P.A01.A07(1867) && this.A09 != null) {
            this.A0R.A0A.A05(A0G(), new AnonymousClass02B() { // from class: X.4sc
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    CartFragment cartFragment = CartFragment.this;
                    if (C12970iu.A1Y(obj)) {
                        String A0I = cartFragment.A0I(R.string.send_cart_cta_message_share_phone_number_disclaimer);
                        TextView textView = cartFragment.A09;
                        if (textView != null) {
                            textView.setText(A0I);
                        }
                    }
                }
            });
            C37041lD r62 = this.A0R;
            r62.A0Q.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(r62, 11, this.A0m));
        }
        C37041lD r35 = this.A0R;
        r35.A00 = false;
        r35.A01 = true;
        AnonymousClass2EM r53 = r35.A0I;
        r53.A0L.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r53, 6));
        AnonymousClass2EM r54 = this.A0R.A0I;
        r54.A0B.A03(new AbstractC30111Wd() { // from class: X.53N
            @Override // X.AbstractC30111Wd
            public final void ANP(C30141Wg r36) {
                AnonymousClass2EM.this.A04.A0A(Boolean.valueOf(r36.A0I));
            }
        }, r54.A0I);
        A1O();
        ActivityC000900k A0C = A0C();
        C252718t r124 = this.A0s;
        AbstractC15710nm r113 = this.A0B;
        AnonymousClass19M r10 = this.A0h;
        C231510o r92 = this.A0j;
        AnonymousClass01d r8 = this.A0d;
        AnonymousClass018 r72 = this.A0g;
        AnonymousClass193 r63 = this.A0k;
        this.A0i = new C15270mq(A0C, imageButton, r113, this.A0F, this.A0n, r8, this.A0f, r72, r10, r92, r63, this.A0r, r124);
        C15270mq r64 = this.A0i;
        ActivityC000900k A0C2 = A0C();
        AnonymousClass19M r55 = this.A0h;
        C231510o r73 = this.A0j;
        new C15330mx(A0C2, this.A0g, r55, r64, r73, (EmojiSearchContainer) this.A0F.findViewById(R.id.emoji_search_container), this.A0r).A00 = new AbstractC14020ki() { // from class: X.56c
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r36) {
                CartFragment.this.A0w.APc(r36.A00);
            }
        };
        C15270mq r36 = this.A0i;
        r36.A0C(this.A0w);
        r36.A0E = new RunnableBRunnable0Shape3S0100000_I0_3(this, 7);
        String str2 = (String) A0z.get(this.A0m);
        if (!TextUtils.isEmpty(str2)) {
            this.A0n.setMentionableText(str2, AnonymousClass1Y6.A01((String) A0y.get(this.A0m)));
        }
        this.A0U.A03(this.A0m, 52, null, 37);
        A1N();
        return this.A06;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A0W.A00();
        this.A0a.A04(this.A0x);
        this.A0q.A06("cart_view_tag", false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
        if (r1 != 2) goto L_0x0011;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A13() {
        /*
            r5 = this;
            super.A13()
            X.00k r4 = r5.A0C()
            int r1 = r5.A01
            if (r1 == 0) goto L_0x0048
            r0 = 1
            r3 = 2
            if (r1 == r0) goto L_0x002c
            if (r1 == r3) goto L_0x0040
        L_0x0011:
            X.1lD r1 = r5.A0R
            r0 = 0
            r1.A00 = r0
            r0 = 1
            r1.A01 = r0
            X.2EM r3 = r1.A0I
            X.0lR r2 = r3.A0L
            r1 = 6
            com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3 r0 = new com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3
            r0.<init>(r3, r1)
            r2.Ab2(r0)
            r0 = 10
            r4.setRequestedOrientation(r0)
            return
        L_0x002c:
            X.0mq r0 = r5.A0i
            boolean r0 = r0.isShowing()
            if (r0 != 0) goto L_0x0040
            com.whatsapp.KeyboardPopupLayout r2 = r5.A0F
            r1 = 8
            com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3 r0 = new com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3
            r0.<init>(r5, r1)
            r2.post(r0)
        L_0x0040:
            android.view.Window r0 = r4.getWindow()
            r0.setSoftInputMode(r3)
            goto L_0x0011
        L_0x0048:
            android.view.Window r1 = r4.getWindow()
            r0 = 4
            r1.setSoftInputMode(r0)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.biz.cart.view.fragment.CartFragment.A13():void");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        this.A0q.A01(774774619, "cart_view_tag", "CartFragment");
        super.A16(bundle);
        this.A0a.A03(this.A0x);
        this.A0W = new C37071lG(this.A0V);
        if (bundle == null) {
            this.A01 = 2;
            return;
        }
        this.A01 = bundle.getInt("extra_input_method");
        this.A0v = bundle.getBoolean("extra_is_sending_order");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass028.A0m(view, true);
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        super.A1L(view);
        BottomSheetBehavior.A00(view).A0J = false;
    }

    public final void A1M() {
        BigDecimal bigDecimal;
        View view;
        C30711Yn r1;
        AnonymousClass3M0 r14;
        int A0E = this.A0Q.A0E();
        List<C92584Wm> A0F = this.A0Q.A0F();
        Date date = this.A0Q.A00;
        ArrayList arrayList = new ArrayList();
        for (C92584Wm r3 : A0F) {
            C44691zO r4 = r3.A01;
            List list = r4.A06;
            if (!list.isEmpty()) {
                r14 = new AnonymousClass3M0(((C44741zT) list.get(0)).A04, ((C44741zT) list.get(0)).A00);
            } else {
                r14 = null;
            }
            BigDecimal bigDecimal2 = r4.A05;
            AnonymousClass3M7 r12 = r4.A02;
            if (bigDecimal2 == null) {
                bigDecimal2 = null;
            } else if (r12 != null && r12.A00(date)) {
                bigDecimal2 = r12.A01;
            }
            arrayList.add(new AnonymousClass3M5(r14, r4.A03, r4.A0D, r4.A04, bigDecimal2, (int) r3.A00, 0));
        }
        String A04 = this.A0X.A04(arrayList);
        OrderInfoViewModel orderInfoViewModel = this.A0X;
        HashMap hashMap = new HashMap();
        Iterator it = arrayList.iterator();
        String str = null;
        C30711Yn r7 = null;
        while (true) {
            if (it.hasNext()) {
                AnonymousClass3M5 r42 = (AnonymousClass3M5) it.next();
                BigDecimal bigDecimal3 = r42.A03;
                if (bigDecimal3 == null || (r1 = r42.A02) == null || !(r7 == null || r1.equals(r7))) {
                    break;
                }
                hashMap.put(r42.A06, bigDecimal3);
                r7 = r1;
            } else if (r7 != null) {
                BigDecimal bigDecimal4 = new BigDecimal(0);
                for (C92584Wm r43 : A0F) {
                    C44691zO r122 = r43.A01;
                    BigDecimal bigDecimal5 = (BigDecimal) hashMap.get(r122.A0D);
                    if (!(bigDecimal5 == null || (bigDecimal = r122.A05) == null || bigDecimal.subtract(bigDecimal5).compareTo(BigDecimal.ZERO) <= 0)) {
                        bigDecimal4 = bigDecimal4.add(r122.A05.subtract(bigDecimal5).multiply(new BigDecimal(r43.A00)));
                    }
                }
                if (bigDecimal4.compareTo(BigDecimal.ZERO) > 0) {
                    str = r7.A03(orderInfoViewModel.A01, bigDecimal4, true);
                }
            }
        }
        if (this.A0l.A07(1660)) {
            View A0D = AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_save_label);
            TextView textView = (TextView) AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_save_amount);
            TextView textView2 = (TextView) AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_subtotal_label);
            TextView textView3 = (TextView) AnonymousClass028.A0D(this.A06, R.id.send_cart_cta_subtotal_amount);
            if (TextUtils.isEmpty(str)) {
                A0D.setVisibility(8);
                textView.setVisibility(8);
            } else {
                A0D.setVisibility(0);
                textView.setVisibility(0);
                textView.setText(str);
            }
            boolean isEmpty = TextUtils.isEmpty(A04);
            Context A01 = A01();
            if (isEmpty) {
                textView2.setText(A01.getString(R.string.cart_business_will_provide_prices));
                textView3.setVisibility(8);
            } else {
                textView2.setText(A01.getString(R.string.cart_item_subtotal));
                textView3.setText(A04);
                textView3.setVisibility(0);
            }
        }
        C37051lE r6 = this.A0Q;
        int i = 0;
        while (true) {
            List list2 = r6.A07;
            if (i >= list2.size()) {
                break;
            }
            AnonymousClass4JV r32 = (AnonymousClass4JV) list2.get(i);
            if (r32 instanceof C84513zQ) {
                ((C84513zQ) r32).A00 = A0E;
                r6.A03(i);
            }
            if ((r32 instanceof C84503zP) && !r6.A06.A07(1660)) {
                C84503zP r33 = (C84503zP) r32;
                r33.A01 = A04;
                r33.A00 = str;
                r6.A03(i);
            }
            i++;
        }
        if (this.A0Q.A0E() == 0) {
            this.A0U.A03(this.A0m, 55, null, 31);
            this.A04.setVisibility(0);
            this.A0A.setVisibility(8);
            this.A05.setVisibility(8);
            this.A08.setVisibility(8);
        } else {
            this.A0A.setVisibility(0);
            if (this.A0l.A07(1660)) {
                view = this.A08;
            } else {
                view = this.A05;
            }
            view.setVisibility(0);
            this.A04.setVisibility(8);
        }
        for (AnonymousClass4UV r0 : this.A0N.A01()) {
            r0.A00();
        }
    }

    public final void A1N() {
        int dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.horizontal_padding);
        int i = dimensionPixelSize << 1;
        if (A02().getConfiguration().orientation != 1) {
            dimensionPixelSize >>= 1;
            i = dimensionPixelSize;
        }
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.A0K.getLayoutParams();
        layoutParams.topMargin = i;
        this.A0K.setLayoutParams(layoutParams);
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.A0J.getLayoutParams();
        layoutParams2.topMargin = dimensionPixelSize;
        this.A0J.setLayoutParams(layoutParams2);
    }

    public final void A1O() {
        String A04;
        C37041lD r1 = this.A0R;
        C22700zV r0 = r1.A0L;
        UserJid userJid = r1.A0O;
        AnonymousClass1M2 A00 = r0.A00(userJid);
        if (A00 != null) {
            A04 = A00.A08;
        } else {
            A04 = r1.A0M.A04(new C15370n3(userJid));
        }
        if (!TextUtils.isEmpty(A04)) {
            AnonymousClass028.A0D(this.A06, R.id.recipient_name_layout).setVisibility(0);
            ((ImageView) AnonymousClass028.A0D(this.A06, R.id.recipient_name_prompt_icon)).setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(A01(), R.drawable.chevron), this.A0g));
            ((TextEmojiLabel) AnonymousClass028.A0D(this.A06, R.id.recipient_name_text)).A0G(null, A04);
        }
    }

    public void A1P(String str) {
        String string = A03().getString("extra_product_id");
        C37041lD r1 = this.A0R;
        if (str.equals(string)) {
            A1B();
            return;
        }
        UserJid userJid = r1.A0O;
        A1B();
        Context A01 = A01();
        AnonymousClass283.A02(A01, C14960mK.A0c(A01, false), userJid, null, null, str, 8, false);
    }

    public final void A1Q(boolean z) {
        if (z) {
            ((ActivityC13810kN) A0C()).AaN();
            C34271fr.A00(A05(), A01().getString(R.string.catalog_something_went_wrong_error), 0).A03();
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A1N();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B instanceof ActivityC13810kN) {
            ((ActivityC13810kN) A0B).A2A(0);
        }
    }
}
