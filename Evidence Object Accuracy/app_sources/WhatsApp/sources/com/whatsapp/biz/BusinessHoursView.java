package com.whatsapp.biz;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14830m7;
import X.C25781Au;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class BusinessHoursView extends LinearLayout implements AnonymousClass004 {
    public ImageView A00;
    public BusinessHoursContentView A01;
    public C25781Au A02;
    public C14830m7 A03;
    public AnonymousClass018 A04;
    public AnonymousClass2P7 A05;
    public boolean A06;
    public boolean A07;

    public BusinessHoursView(Context context) {
        super(context);
        A00();
        this.A06 = false;
        A01();
    }

    public BusinessHoursView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A06 = false;
        A01();
    }

    public BusinessHoursView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A06 = false;
        A01();
    }

    public BusinessHoursView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A06 = false;
        A01();
    }

    public BusinessHoursView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A03 = C12980iv.A0b(A00);
            this.A04 = C12960it.A0R(A00);
            this.A02 = (C25781Au) A00.A2S.get();
        }
    }

    public final void A01() {
        View inflate = C12960it.A0E(this).inflate(R.layout.business_hours_layout, (ViewGroup) this, true);
        setFocusable(true);
        this.A01 = (BusinessHoursContentView) inflate.findViewById(R.id.business_hours_content_view);
        this.A00 = C12970iu.A0L(inflate, R.id.business_hours_chevron_icon);
    }

    public final void A02() {
        this.A01.setFullView(this.A06);
        ImageView imageView = this.A00;
        Context context = getContext();
        boolean z = this.A06;
        int i = R.drawable.ic_chevron_down;
        if (z) {
            i = R.drawable.ic_chevron_up;
        }
        C12990iw.A0x(context, imageView, i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }
}
