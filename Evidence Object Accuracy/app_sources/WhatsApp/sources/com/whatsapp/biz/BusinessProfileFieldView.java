package com.whatsapp.biz;

import X.AbstractC36671kL;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass19M;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.biz.catalog.view.EllipsizedTextEmojiLabel;

/* loaded from: classes2.dex */
public class BusinessProfileFieldView extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public ColorStateList A01;
    public ColorStateList A02;
    public Drawable A03;
    public ImageView A04;
    public TextView A05;
    public TextView A06;
    public AnonymousClass018 A07;
    public AnonymousClass19M A08;
    public C14850m9 A09;
    public AnonymousClass2P7 A0A;
    public String A0B;
    public boolean A0C;
    public boolean A0D;

    public BusinessProfileFieldView(Context context) {
        super(context);
        A00();
        A01(null);
    }

    public BusinessProfileFieldView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(attributeSet);
    }

    public BusinessProfileFieldView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(attributeSet);
    }

    public BusinessProfileFieldView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        A01(attributeSet);
    }

    public BusinessProfileFieldView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A07 = C12960it.A0R(A00);
            this.A09 = C12960it.A0S(A00);
            this.A08 = (AnonymousClass19M) A00.A6R.get();
        }
    }

    public void A01(AttributeSet attributeSet) {
        TextUtils.TruncateAt truncateAt;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(attributeSet, AnonymousClass2QN.A04, 0, 0);
            try {
                this.A03 = obtainStyledAttributes.getDrawable(2);
                this.A00 = obtainStyledAttributes.getInteger(0, 0);
                this.A0D = obtainStyledAttributes.getBoolean(3, false);
                ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(4);
                this.A01 = colorStateList;
                if (colorStateList == null) {
                    this.A01 = new ColorStateList(new int[][]{new int[0]}, new int[]{AnonymousClass00T.A00(getContext(), R.color.primary_text)});
                }
                ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(5);
                this.A02 = colorStateList2;
                if (colorStateList2 == null) {
                    this.A02 = this.A01;
                }
                this.A0B = this.A07.A0E(obtainStyledAttributes, 1);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        View inflate = C12960it.A0E(this).inflate(R.layout.business_profile_field_layout, (ViewGroup) this, true);
        this.A04 = C12970iu.A0L(inflate, R.id.field_icon);
        setIcon(this.A03);
        this.A06 = C12960it.A0J(inflate, R.id.field_textview);
        this.A05 = C12960it.A0J(inflate, R.id.sub_field_textview);
        this.A06.setSingleLine(this.A0D);
        this.A05.setSingleLine(this.A0D);
        int i = this.A00;
        if (i == 1) {
            truncateAt = TextUtils.TruncateAt.START;
        } else if (i == 2) {
            truncateAt = TextUtils.TruncateAt.MIDDLE;
        } else if (i == 3) {
            truncateAt = TextUtils.TruncateAt.END;
        } else if (i != 4) {
            truncateAt = null;
        } else {
            truncateAt = TextUtils.TruncateAt.MARQUEE;
        }
        this.A06.setEllipsize(truncateAt);
        this.A05.setEllipsize(truncateAt);
        TextView textView = this.A06;
        EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel = (EllipsizedTextEmojiLabel) textView;
        ellipsizedTextEmojiLabel.A00 = 180;
        ellipsizedTextEmojiLabel.A02 = R.color.primary_light;
        EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel2 = (EllipsizedTextEmojiLabel) this.A05;
        ellipsizedTextEmojiLabel2.A00 = 180;
        ellipsizedTextEmojiLabel2.A02 = R.color.primary_light;
        textView.setTextColor(this.A01);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public int getLayoutRes() {
        return R.layout.business_profile_field_layout;
    }

    public TextView getSubTextView() {
        return this.A05;
    }

    public String getText() {
        TextView textView = this.A06;
        if (textView != null) {
            return C12980iv.A0q(textView);
        }
        return null;
    }

    public TextView getTextView() {
        return this.A06;
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.A06.setEnabled(z);
        this.A05.setEnabled(z);
    }

    public void setIcon(int i) {
        setIcon(AnonymousClass00T.A04(getContext(), i));
    }

    private void setIcon(Drawable drawable) {
        if (drawable != null) {
            setPadding(C12960it.A09(this).getDimensionPixelSize(R.dimen.info_screen_padding), getPaddingTop(), C12960it.A09(this).getDimensionPixelSize(R.dimen.info_screen_padding), getPaddingBottom());
        }
    }

    public void setInputType(int i) {
        TextView textView = this.A06;
        if (textView != null) {
            textView.setInputType(i);
        }
    }

    public void setSubText(CharSequence charSequence) {
        TextView textView;
        int i;
        if (TextUtils.isEmpty(charSequence)) {
            C12990iw.A1G(this.A05);
            textView = this.A05;
            i = 8;
        } else {
            ((TextEmojiLabel) this.A05).A0G(null, AbstractC36671kL.A03(getContext(), this.A06.getPaint(), this.A08, charSequence));
            textView = this.A05;
            i = 0;
        }
        textView.setVisibility(i);
    }

    public void setText(CharSequence charSequence, View.OnClickListener onClickListener) {
        int i;
        if (!TextUtils.isEmpty(charSequence) || !TextUtils.isEmpty(this.A0B)) {
            this.A06.setTextColor(this.A01);
            if (TextUtils.isEmpty(charSequence)) {
                charSequence = this.A0B;
                this.A06.setTextColor(this.A02);
            }
            EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel = (EllipsizedTextEmojiLabel) this.A06;
            ellipsizedTextEmojiLabel.A03 = onClickListener;
            ellipsizedTextEmojiLabel.A0G(null, AbstractC36671kL.A03(getContext(), this.A06.getPaint(), this.A08, charSequence));
            i = 0;
        } else {
            C12990iw.A1G(this.A06);
            i = 8;
        }
        setVisibility(i);
    }

    public void setTextColor(int i) {
        this.A06.setTextColor(i);
        this.A01 = new ColorStateList(new int[][]{new int[0]}, new int[]{i});
    }
}
