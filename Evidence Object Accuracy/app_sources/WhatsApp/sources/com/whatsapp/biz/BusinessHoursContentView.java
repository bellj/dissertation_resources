package com.whatsapp.biz;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass1US;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C30191Wl;
import X.C30201Wm;
import android.content.Context;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/* loaded from: classes2.dex */
public class BusinessHoursContentView extends FrameLayout implements AnonymousClass004 {
    public static final int[] A04 = {R.id.business_hours_day_layout_0, R.id.business_hours_day_layout_1, R.id.business_hours_day_layout_2, R.id.business_hours_day_layout_3, R.id.business_hours_day_layout_4, R.id.business_hours_day_layout_5, R.id.business_hours_day_layout_6};
    public AnonymousClass2P7 A00;
    public List A01;
    public List A02;
    public boolean A03;

    public BusinessHoursContentView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00();
    }

    public BusinessHoursContentView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        A00();
    }

    public BusinessHoursContentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00();
    }

    public BusinessHoursContentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00();
    }

    public BusinessHoursContentView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        View inflate = C12960it.A0E(this).inflate(R.layout.business_hours_content_layout, (ViewGroup) this, true);
        int[] iArr = A04;
        int length = iArr.length;
        this.A02 = C12980iv.A0w(length);
        this.A01 = C12980iv.A0w(length);
        for (int i : iArr) {
            View findViewById = inflate.findViewById(i);
            View findViewById2 = findViewById.findViewById(R.id.business_hours_day_layout_title);
            View findViewById3 = findViewById.findViewById(R.id.business_hours_day_layout_description);
            this.A02.add(findViewById);
            this.A01.add(C12990iw.A0L(findViewById2, findViewById3));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    public int getLayout() {
        return R.layout.business_hours_content_layout;
    }

    public void setFullView(boolean z) {
        for (int i = 0; i < this.A02.size(); i++) {
            if (i != 0) {
                ((View) this.A02.get(i)).setVisibility(C12960it.A02(z ? 1 : 0));
            }
        }
    }

    public void setup(List list) {
        for (int i = 0; i < list.size(); i++) {
            ((TextView) C12990iw.A0k(this.A01, i)).setText((CharSequence) C12990iw.A0k(list, i));
            ((TextView) ((Pair) this.A01.get(i)).second).setText((CharSequence) ((Pair) list.get(i)).second);
        }
    }

    public void setupWithOpenNow(List list, long j, C30201Wm r12) {
        TimeZone timeZone;
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                String str = r12.A01;
                if (AnonymousClass1US.A0C(str)) {
                    timeZone = TimeZone.getDefault();
                } else {
                    timeZone = TimeZone.getTimeZone(str);
                }
                Calendar instance = Calendar.getInstance(timeZone);
                instance.setTimeInMillis(j);
                int i2 = instance.get(7);
                int i3 = (instance.get(11) * 60) + instance.get(12);
                for (C30191Wl r6 : r12.A02) {
                    if (r6.A00 == i2) {
                        int i4 = r6.A01;
                        if (i4 != 0) {
                            if (!(i4 == 1 || i4 == 2)) {
                            }
                            ForegroundColorSpan A0M = C12980iv.A0M(getContext(), R.color.business_hours_open);
                            String string = getContext().getString(R.string.business_hours_status_open_now);
                            SpannableString spannableString = new SpannableString(string);
                            spannableString.setSpan(A0M, 0, string.length(), 33);
                            ((TextView) C12990iw.A0k(this.A01, i)).setText(spannableString);
                            break;
                        }
                        Integer num = r6.A03;
                        AnonymousClass009.A05(num);
                        if (i3 >= num.intValue()) {
                            Integer num2 = r6.A02;
                            AnonymousClass009.A05(num2);
                            if (i3 <= num2.intValue()) {
                                ForegroundColorSpan A0M = C12980iv.A0M(getContext(), R.color.business_hours_open);
                                String string = getContext().getString(R.string.business_hours_status_open_now);
                                SpannableString spannableString = new SpannableString(string);
                                spannableString.setSpan(A0M, 0, string.length(), 33);
                                ((TextView) C12990iw.A0k(this.A01, i)).setText(spannableString);
                                break;
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            }
            ((TextView) C12990iw.A0k(this.A01, i)).setText((CharSequence) C12990iw.A0k(list, i));
            ((TextView) ((Pair) this.A01.get(i)).second).setText((CharSequence) ((Pair) list.get(i)).second);
        }
    }
}
