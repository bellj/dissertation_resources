package com.whatsapp.biz.profile;

import X.AnonymousClass00T;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C30221Wo;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import java.text.NumberFormat;

/* loaded from: classes2.dex */
public class TrustSignalItem extends LinearLayout {
    public int A00;
    public WaImageView A01;
    public WaImageView A02;
    public WaTextView A03;
    public WaTextView A04;

    public TrustSignalItem(Context context) {
        this(context, null);
    }

    public TrustSignalItem(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TrustSignalItem(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOrientation(0);
        setClickable(true);
        setFocusable(true);
        LinearLayout.inflate(context, R.layout.trust_signal_item, this);
        this.A02 = C12980iv.A0X(this, R.id.linked_account_icon);
        this.A04 = C12960it.A0N(this, R.id.linked_account_name);
        this.A03 = C12960it.A0N(this, R.id.linked_account_info);
        this.A01 = C12980iv.A0X(this, R.id.linked_account_edit_button);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0N);
        try {
            int integer = obtainStyledAttributes.getInteger(0, 0);
            this.A00 = integer;
            if (integer == 0) {
                setIcon(AnonymousClass00T.A04(getContext(), R.drawable.ic_settings_fb));
                this.A02.setColorFilter(AnonymousClass00T.A00(getContext(), R.color.connected_accounts_facebook_icon_tint));
            } else if (integer == 1) {
                setIcon(AnonymousClass00T.A04(getContext(), R.drawable.ic_business_instagram));
            }
            setEditable(obtainStyledAttributes.getBoolean(2, false));
            int resourceId = obtainStyledAttributes.getResourceId(1, 0);
            if (resourceId != 0) {
                this.A01.setColorFilter(AnonymousClass00T.A00(getContext(), resourceId));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public String getAccountName() {
        return C12980iv.A0q(this.A04);
    }

    public int getAccountType() {
        return this.A00;
    }

    private void setAccountInfo(String str) {
        this.A03.setText(str);
    }

    private void setAccountName(String str) {
        if (this.A00 == 1) {
            str = C12960it.A0d(str, C12960it.A0k("@"));
        }
        this.A04.setText(str);
    }

    private void setEditable(boolean z) {
        WaImageView waImageView;
        int i;
        WaTextView waTextView = this.A04;
        Context context = getContext();
        if (z) {
            C12960it.A0s(context, waTextView, R.color.primary_text);
            waImageView = this.A01;
            i = 0;
        } else {
            C12960it.A0s(context, waTextView, R.color.primary_light);
            waImageView = this.A01;
            i = 8;
        }
        waImageView.setVisibility(i);
    }

    private void setIcon(Drawable drawable) {
        this.A02.setImageDrawable(drawable);
    }

    public void setUpFromAccount(C30221Wo r7) {
        String string;
        int i;
        if (r7 == null) {
            i = 8;
        } else {
            setAccountName(r7.A01);
            int i2 = r7.A00;
            if (i2 > 0) {
                int i3 = this.A00;
                int i4 = R.plurals.instagram_trust_signal_description;
                if (i3 == 0) {
                    i4 = R.plurals.facebook_trust_signal_description;
                }
                String format = NumberFormat.getIntegerInstance().format((long) i2);
                Resources A09 = C12960it.A09(this);
                Object[] A1b = C12970iu.A1b();
                A1b[0] = format;
                string = A09.getQuantityString(i4, i2, A1b);
            } else {
                Resources resources = getResources();
                int i5 = this.A00;
                int i6 = R.string.instagram_trust_signal_description_zero_followers;
                if (i5 == 0) {
                    i6 = R.string.facebook_trust_signal_description_zero_likes;
                }
                string = resources.getString(i6);
            }
            setAccountInfo(string);
            i = 0;
        }
        setVisibility(i);
    }
}
