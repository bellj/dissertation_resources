package com.whatsapp.biz.order.view.fragment;

import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass17R;
import X.AnonymousClass19Q;
import X.AnonymousClass1CK;
import X.AnonymousClass1CL;
import X.AnonymousClass1CM;
import X.AnonymousClass1IS;
import X.AnonymousClass2EE;
import X.AnonymousClass3C7;
import X.AnonymousClass4JH;
import X.AnonymousClass4JI;
import X.AnonymousClass4SY;
import X.C103994re;
import X.C14650lo;
import X.C14830m7;
import X.C15570nT;
import X.C16170oZ;
import X.C16590pI;
import X.C17220qS;
import X.C19840ul;
import X.C20020v5;
import X.C252918v;
import X.C37071lG;
import X.C38211ni;
import X.C53882fY;
import X.C54522gr;
import X.C59012tp;
import X.C67373Rg;
import X.C89234Je;
import android.app.Application;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.biz.order.view.fragment.OrderDetailFragment;
import com.whatsapp.biz.order.viewmodel.OrderInfoViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import com.whatsapp.util.ViewOnClickCListenerShape2S1100000_I0;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class OrderDetailFragment extends Hilt_OrderDetailFragment {
    public ProgressBar A00;
    public AnonymousClass4JH A01;
    public AnonymousClass4JI A02;
    public C15570nT A03;
    public C16170oZ A04;
    public C14650lo A05;
    public AnonymousClass19Q A06;
    public C252918v A07;
    public C37071lG A08;
    public AnonymousClass1CK A09;
    public AnonymousClass1CL A0A;
    public AnonymousClass2EE A0B;
    public C54522gr A0C;
    public C53882fY A0D;
    public OrderInfoViewModel A0E;
    public C20020v5 A0F;
    public C14830m7 A0G;
    public C16590pI A0H;
    public AnonymousClass018 A0I;
    public UserJid A0J;
    public UserJid A0K;
    public C17220qS A0L;
    public AnonymousClass1CM A0M;
    public C19840ul A0N;
    public AnonymousClass17R A0O;
    public AbstractC14440lR A0P;
    public String A0Q;

    public static OrderDetailFragment A00(UserJid userJid, UserJid userJid2, AnonymousClass1IS r6, String str, String str2) {
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        Bundle bundle = new Bundle();
        C38211ni.A08(bundle, r6, "");
        bundle.putParcelable("extra_key_seller_jid", userJid);
        bundle.putParcelable("extra_key_buyer_jid", userJid2);
        bundle.putString("extra_key_order_id", str);
        bundle.putString("extra_key_token", str2);
        bundle.putBoolean("extra_key_enable_create_order", false);
        orderDetailFragment.A0U(bundle);
        return orderDetailFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Object obj;
        View inflate = layoutInflater.inflate(R.layout.fragment_order_detail, viewGroup, false);
        inflate.findViewById(R.id.order_detail_close_btn).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 40));
        this.A00 = (ProgressBar) AnonymousClass028.A0D(inflate, R.id.order_detail_loading_spinner);
        RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(inflate, R.id.order_detail_recycler_view);
        recyclerView.A0h = true;
        C54522gr r0 = new C54522gr(this.A02, this.A08, this);
        this.A0C = r0;
        recyclerView.setAdapter(r0);
        AnonymousClass028.A0m(recyclerView, false);
        inflate.setMinimumHeight(A1K());
        Parcelable parcelable = A03().getParcelable("extra_key_seller_jid");
        AnonymousClass009.A05(parcelable);
        this.A0K = (UserJid) parcelable;
        Parcelable parcelable2 = A03().getParcelable("extra_key_buyer_jid");
        AnonymousClass009.A05(parcelable2);
        this.A0J = (UserJid) parcelable2;
        String string = A03().getString("extra_key_order_id");
        AnonymousClass009.A05(string);
        this.A0Q = string;
        String string2 = A03().getString("extra_key_token");
        AnonymousClass009.A05(string2);
        String str = this.A0Q;
        C53882fY r02 = (C53882fY) new AnonymousClass02A(new C67373Rg(this.A01, this.A0K, string2, str), this).A00(C53882fY.class);
        this.A0D = r02;
        r02.A02.A05(A0G(), new AnonymousClass02B() { // from class: X.3Px
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj2) {
                String A0X;
                OrderDetailFragment orderDetailFragment = OrderDetailFragment.this;
                AnonymousClass2EG r9 = (AnonymousClass2EG) obj2;
                orderDetailFragment.A00.setVisibility(8);
                List<AnonymousClass3M5> list = r9.A03;
                C54522gr r5 = orderDetailFragment.A0C;
                int i = 0;
                for (AnonymousClass3M5 r03 : list) {
                    i += r03.A00;
                }
                OrderInfoViewModel orderInfoViewModel = orderDetailFragment.A0E;
                UserJid userJid = orderDetailFragment.A0K;
                String A04 = orderInfoViewModel.A04(list);
                if (A04 == null) {
                    boolean A0F = orderInfoViewModel.A00.A0F(userJid);
                    Application application = ((AnonymousClass014) orderInfoViewModel).A00;
                    int i2 = R.string.no_price_cart_receiver_view;
                    if (A0F) {
                        i2 = R.string.no_price_entered;
                    }
                    A0X = application.getString(i2);
                } else if (TextUtils.isEmpty(A04)) {
                    A0X = "";
                } else {
                    A0X = C12960it.A0X(((AnonymousClass014) orderInfoViewModel).A00, A04, C12970iu.A1b(), 0, R.string.estimated);
                }
                C53882fY r3 = orderDetailFragment.A0D;
                long A02 = r3.A05.A02(TimeUnit.SECONDS.toMillis(r9.A00));
                AnonymousClass018 r12 = r3.A07;
                String format = AnonymousClass1MY.A07(r12, 1).format(new Date(A02));
                String A00 = AnonymousClass3JK.A00(r12, A02);
                Resources A002 = C16590pI.A00(r3.A06);
                Object[] A1a = C12980iv.A1a();
                A1a[0] = format;
                String A01 = AnonymousClass3JK.A01(r12, C12990iw.A0o(A002, A00, A1a, 1, R.string.order_sent_date_and_time), A02);
                List list2 = r5.A03;
                list2.clear();
                C84513zQ r04 = new C84513zQ(false);
                r04.A00 = i;
                r04.A01 = A0X;
                list2.add(r04);
                for (AnonymousClass3M5 r1 : list) {
                    list2.add(new C84483zN(r1));
                }
                list2.add(new C84473zM(A01));
                r5.A02();
                orderDetailFragment.A0N.A04("order_view_tag", "ProductsCount", String.valueOf(list.size()));
                orderDetailFragment.A0N.A06("order_view_tag", true);
            }
        });
        this.A0D.A01.A05(A0G(), new AnonymousClass02B() { // from class: X.3Pw
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj2) {
                OrderDetailFragment orderDetailFragment = OrderDetailFragment.this;
                orderDetailFragment.A00.setVisibility(8);
                int A05 = C12960it.A05(((Pair) obj2).first);
                int i = R.string.order_deleted_error;
                if (A05 != 404) {
                    i = R.string.catalog_something_went_wrong_error;
                }
                C34271fr.A00(orderDetailFragment.A05(), orderDetailFragment.A01().getString(i), 0).A03();
                orderDetailFragment.A0N.A06("order_view_tag", false);
            }
        });
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.order_detail_title);
        C53882fY r8 = this.A0D;
        Resources resources = r8.A06.A00.getResources();
        boolean A0F = r8.A03.A0F(r8.A08);
        int i = R.string.your_sent_cart;
        if (A0F) {
            i = R.string.received_cart;
        }
        textView.setText(resources.getString(i));
        this.A0E = (OrderInfoViewModel) new AnonymousClass02A(this).A00(OrderInfoViewModel.class);
        C53882fY r03 = this.A0D;
        AnonymousClass2EE r82 = r03.A04;
        UserJid userJid = r03.A08;
        String str2 = r03.A09;
        String str3 = r03.A0A;
        Object obj2 = r82.A05.A00.get(str2);
        if (obj2 != null) {
            AnonymousClass016 r04 = r82.A00;
            if (r04 != null) {
                r04.A0A(obj2);
            }
        } else {
            AnonymousClass4SY r15 = new AnonymousClass4SY(userJid, str2, str3, r82.A03, r82.A02);
            C19840ul r10 = r82.A09;
            C59012tp r13 = new C59012tp(r82.A04, r15, new C89234Je(new AnonymousClass3C7()), r82.A07, r82.A08, r10);
            AnonymousClass1CL r102 = r82.A06;
            synchronized (r102) {
                Hashtable hashtable = r102.A00;
                obj = (Future) hashtable.get(str2);
                if (obj == null) {
                    String A01 = r13.A03.A01();
                    r13.A04.A03("order_view_tag");
                    r13.A02.A02(r13, r13.A02(A01), A01, 248);
                    StringBuilder sb = new StringBuilder("GetOrderProtocol/sendGetOrderRequest/jid=");
                    sb.append(r13.A00.A02);
                    Log.i(sb.toString());
                    obj = r13.A05;
                    hashtable.put(str2, obj);
                    r102.A01.Ab2(new RunnableBRunnable0Shape0S1200000_I0(r102, obj, str2, 11));
                }
            }
            r82.A0A.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(r82, 16, obj));
        }
        this.A06.A01(this.A0K, null, null, 45, null, null, null, this.A0Q, null, null, 35);
        if (A03().getBoolean("extra_key_enable_create_order")) {
            View A0D = AnonymousClass028.A0D(inflate, R.id.create_order);
            this.A0D.A00.A05(A0G(), new AnonymousClass02B(A0D) { // from class: X.4sf
                public final /* synthetic */ View A00;

                {
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj3) {
                    this.A00.setEnabled(Boolean.TRUE.equals(obj3));
                }
            });
            A0D.setVisibility(0);
            A0D.setOnClickListener(new ViewOnClickCListenerShape2S1100000_I0(1, string2, this));
            View A0D2 = AnonymousClass028.A0D(inflate, R.id.decline_order);
            A0D2.setVisibility(0);
            A0D2.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 18));
        }
        this.A0F.A08(new C103994re(0), this.A0K);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A08.A00();
        this.A0N.A06("order_view_tag", false);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        this.A0N.A01(774769843, "order_view_tag", "OrderDetailFragment");
        super.A16(bundle);
        this.A08 = new C37071lG(this.A07);
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        super.A1L(view);
        BottomSheetBehavior.A00(view).A0J = false;
    }
}
