package com.whatsapp.biz.order.viewmodel;

import X.AnonymousClass014;
import X.AnonymousClass018;
import X.AnonymousClass3M5;
import X.C15570nT;
import X.C30711Yn;
import android.app.Application;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class OrderInfoViewModel extends AnonymousClass014 {
    public final C15570nT A00;
    public final AnonymousClass018 A01;

    public OrderInfoViewModel(Application application, C15570nT r2, AnonymousClass018 r3) {
        super(application);
        this.A01 = r3;
        this.A00 = r2;
    }

    public String A04(List list) {
        C30711Yn r1;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        Iterator it = list.iterator();
        C30711Yn r4 = null;
        while (true) {
            if (it.hasNext()) {
                AnonymousClass3M5 r3 = (AnonymousClass3M5) it.next();
                BigDecimal bigDecimal2 = r3.A03;
                if (bigDecimal2 == null || (r1 = r3.A02) == null || (r4 != null && !r1.equals(r4))) {
                    break;
                }
                r4 = r1;
                bigDecimal = bigDecimal.add(bigDecimal2.multiply(new BigDecimal(r3.A00)));
            } else if (r4 != null && !bigDecimal.equals(BigDecimal.ZERO)) {
                return r4.A03(this.A01, bigDecimal, true);
            }
        }
        return null;
    }
}
