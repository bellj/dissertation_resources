package com.whatsapp.biz;

import X.AbstractC005102i;
import X.AbstractC33331dp;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass12P;
import X.AnonymousClass2Cu;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FL;
import X.AnonymousClass3IU;
import X.AnonymousClass41O;
import X.AnonymousClass44S;
import X.AnonymousClass53L;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C14650lo;
import X.C15370n3;
import X.C15570nT;
import X.C15610nY;
import X.C20830wO;
import X.C22330yu;
import X.C244215l;
import X.C244415n;
import X.C246716k;
import X.C25781Au;
import X.C27131Gd;
import X.C58972tk;
import X.C852741x;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class BusinessProfileExtraFieldsActivity extends ActivityC13790kL {
    public AnonymousClass3IU A00;
    public C14650lo A01;
    public AnonymousClass10A A02;
    public C246716k A03;
    public C25781Au A04;
    public C22330yu A05;
    public AnonymousClass10S A06;
    public C15610nY A07;
    public AnonymousClass018 A08;
    public C20830wO A09;
    public C15370n3 A0A;
    public C244215l A0B;
    public UserJid A0C;
    public C244415n A0D;
    public Integer A0E;
    public boolean A0F;
    public final AnonymousClass2Cu A0G;
    public final AnonymousClass2Dn A0H;
    public final C27131Gd A0I;
    public final AbstractC33331dp A0J;

    public BusinessProfileExtraFieldsActivity() {
        this(0);
        this.A0I = new C852741x(this);
        this.A0H = new AnonymousClass41O(this);
        this.A0J = new AnonymousClass44S(this);
        this.A0G = new C58972tk(this);
    }

    public BusinessProfileExtraFieldsActivity(int i) {
        this.A0F = false;
        ActivityC13830kP.A1P(this, 16);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0D = (C244415n) A1M.AAg.get();
            this.A07 = C12960it.A0P(A1M);
            this.A08 = C12960it.A0R(A1M);
            this.A06 = C12990iw.A0Z(A1M);
            this.A05 = (C22330yu) A1M.A3I.get();
            this.A03 = (C246716k) A1M.A2X.get();
            this.A01 = C12980iv.A0Y(A1M);
            this.A02 = (AnonymousClass10A) A1M.A2W.get();
            this.A09 = (C20830wO) A1M.A4W.get();
            this.A0B = (C244215l) A1M.A8y.get();
            this.A04 = (C25781Au) A1M.A2S.get();
        }
    }

    public void A2e() {
        C15370n3 A01 = this.A09.A01(this.A0C);
        this.A0A = A01;
        setTitle(this.A07.A04(A01));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0C = ActivityC13790kL.A0V(getIntent(), "jid");
        Integer valueOf = Integer.valueOf(getIntent().getIntExtra("profile_entry_point", -1));
        this.A0E = valueOf;
        if (valueOf.intValue() == -1) {
            valueOf = null;
        }
        this.A0E = valueOf;
        A2e();
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.smb_extra_business_profile_activity_view);
        C15570nT r4 = ((ActivityC13790kL) this).A01;
        AnonymousClass12P r2 = ((ActivityC13790kL) this).A00;
        C244415n r11 = this.A0D;
        C15610nY r8 = this.A07;
        AnonymousClass018 r9 = this.A08;
        this.A00 = new AnonymousClass3IU(((ActivityC13810kN) this).A00, r2, this, r4, this.A03, this.A04, null, r8, r9, this.A0A, r11, this.A0E, true, false);
        this.A01.A03(new AnonymousClass53L(this), this.A0C);
        this.A06.A03(this.A0I);
        this.A05.A03(this.A0H);
        this.A02.A03(this.A0G);
        this.A0B.A03(this.A0J);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.A04(this.A0I);
        this.A05.A04(this.A0H);
        this.A02.A04(this.A0G);
        this.A0B.A04(this.A0J);
    }
}
