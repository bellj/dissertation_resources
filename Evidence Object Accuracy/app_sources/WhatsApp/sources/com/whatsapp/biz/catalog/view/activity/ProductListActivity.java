package com.whatsapp.biz.catalog.view.activity;

import X.AbstractC005102i;
import X.AbstractC008704k;
import X.AbstractC11880h1;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass04S;
import X.AnonymousClass04Y;
import X.AnonymousClass19Q;
import X.AnonymousClass1FB;
import X.AnonymousClass1ZC;
import X.AnonymousClass2FL;
import X.AnonymousClass3EX;
import X.AnonymousClass3RX;
import X.AnonymousClass3VX;
import X.AnonymousClass4UV;
import X.C004802e;
import X.C103994re;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15570nT;
import X.C19840ul;
import X.C20020v5;
import X.C21770xx;
import X.C252918v;
import X.C25791Av;
import X.C34271fr;
import X.C37071lG;
import X.C48882Ih;
import X.C53832fK;
import X.C53852fO;
import X.C54532gs;
import X.C54562gv;
import X.C54712hA;
import X.C67383Rh;
import X.C84433zI;
import android.app.Application;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.activity.ProductListActivity;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ProductListActivity extends ActivityC13790kL {
    public View A00;
    public AnonymousClass04S A01;
    public AnonymousClass04S A02;
    public RecyclerView A03;
    public C34271fr A04;
    public C48882Ih A05;
    public C25791Av A06;
    public C21770xx A07;
    public C53852fO A08;
    public AnonymousClass19Q A09;
    public AnonymousClass1FB A0A;
    public C252918v A0B;
    public C54532gs A0C;
    public C53832fK A0D;
    public Button A0E;
    public C20020v5 A0F;
    public UserJid A0G;
    public C19840ul A0H;
    public String A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public final AnonymousClass4UV A0N;

    public ProductListActivity() {
        this(0);
        this.A0K = true;
        this.A0N = new C84433zI(this);
    }

    public ProductListActivity(int i) {
        this.A0J = false;
        ActivityC13830kP.A1P(this, 19);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0J) {
            this.A0J = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0H = (C19840ul) A1M.A1Q.get();
            this.A07 = (C21770xx) A1M.A2s.get();
            this.A06 = (C25791Av) A1M.A2t.get();
            this.A0F = (C20020v5) A1M.A3B.get();
            this.A0B = C12990iw.A0V(A1M);
            this.A0A = (AnonymousClass1FB) A1M.AGH.get();
            this.A09 = C12980iv.A0Z(A1M);
            this.A05 = (C48882Ih) A1L.A0Q.get();
        }
    }

    public final void A2e() {
        View findViewById;
        int i;
        if (this.A0K) {
            findViewById = findViewById(R.id.shadow_bottom);
            i = 8;
        } else {
            boolean canScrollVertically = this.A03.canScrollVertically(1);
            findViewById = findViewById(R.id.shadow_bottom);
            i = 4;
            if (canScrollVertically) {
                i = 0;
            }
        }
        findViewById.setVisibility(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0H.A01(774777097, "plm_details_view_tag", "ProductListActivity");
        setContentView(R.layout.activity_product_list);
        String stringExtra = getIntent().getStringExtra("message_title");
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0I(stringExtra);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0B(false);
        A0S.A06(R.string.something_went_wrong);
        C12970iu.A1L(A0S, this, 15, R.string.ok);
        this.A01 = A0S.create();
        C004802e A0S2 = C12980iv.A0S(this);
        A0S2.A0B(false);
        A0S2.A06(R.string.items_no_longer_available);
        C12970iu.A1L(A0S2, this, 14, R.string.ok);
        this.A02 = A0S2.create();
        this.A06.A03(this.A0N);
        AnonymousClass1ZC r11 = (AnonymousClass1ZC) getIntent().getParcelableExtra("message_content");
        this.A0G = r11.A00;
        Application application = getApplication();
        UserJid userJid = this.A0G;
        C53832fK r3 = (C53832fK) new AnonymousClass02A(new C67383Rh(application, this.A0A, new AnonymousClass3EX(this.A07, this.A09, userJid, ((ActivityC13830kP) this).A05), ((ActivityC13810kN) this).A07, userJid, r11), this).A00(C53832fK.class);
        this.A0D = r3;
        C12960it.A18(this, r3.A02, 16);
        this.A08 = (C53852fO) AnonymousClass3RX.A00(this, this.A05, this.A0G);
        this.A00 = findViewById(R.id.no_internet_container);
        int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.product_list_no_internet_horizontal_padding);
        this.A00.setPadding(dimensionPixelOffset, getResources().getDimensionPixelOffset(R.dimen.product_list_no_internet_top_padding), dimensionPixelOffset, 0);
        C12960it.A10(findViewById(R.id.no_internet_retry_button), this, 48);
        Button button = (Button) findViewById(R.id.view_cart);
        this.A0E = button;
        C12960it.A10(button, this, 49);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.product_list);
        this.A03 = recyclerView;
        AnonymousClass04Y r4 = recyclerView.A0R;
        if (r4 instanceof AbstractC008704k) {
            ((AbstractC008704k) r4).A00 = false;
        }
        recyclerView.A0l(new C54562gv());
        UserJid userJid2 = this.A0G;
        C15570nT r5 = ((ActivityC13790kL) this).A01;
        AnonymousClass018 r9 = ((ActivityC13830kP) this).A01;
        C37071lG r7 = new C37071lG(this.A0B);
        C54532gs r42 = new C54532gs(r5, this.A09, r7, new AnonymousClass3VX(this), r9, ((ActivityC13810kN) this).A0C, userJid2);
        this.A0C = r42;
        this.A03.setAdapter(r42);
        this.A03.A0W = new AbstractC11880h1() { // from class: X.4uT
            @Override // X.AbstractC11880h1
            public final void AYO(AnonymousClass03U r2) {
                if (r2 instanceof C59302uQ) {
                    ((C59302uQ) r2).A0A();
                }
            }
        };
        C12960it.A18(this, this.A0D.A01, 15);
        C12960it.A18(this, this.A0D.A00, 14);
        this.A03.A0n(new C54712hA(this));
        this.A03.setOnTouchListener(new View.OnTouchListener() { // from class: X.3N2
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                ProductListActivity productListActivity = ProductListActivity.this;
                int action = motionEvent.getAction();
                if (action != 1) {
                    if (action == 2 && !productListActivity.A03.canScrollVertically(-1) && productListActivity.A0L) {
                        C54532gs r2 = productListActivity.A0C;
                        if (!r2.A0E()) {
                            r2.A09.add(0, new AnonymousClass53Y());
                            r2.A04(0);
                        }
                    }
                } else if (productListActivity.A0L && productListActivity.A0C.A0E()) {
                    C54532gs r1 = productListActivity.A0C;
                    if (r1.A0E()) {
                        r1.A09.remove(0);
                        r1.A05(0);
                    }
                    if (((ActivityC13810kN) productListActivity).A07.A0B()) {
                        productListActivity.A0D.A05();
                        productListActivity.A0L = false;
                        return false;
                    }
                }
                return false;
            }
        });
        this.A0L = false;
        this.A0F.A08(new C103994re(0), this.A0G);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.catalog, menu);
        MenuItem findItem = menu.findItem(R.id.menu_edit);
        menu.findItem(R.id.menu_share).setVisible(false);
        findItem.setVisible(false);
        MenuItem A0P = ActivityC13790kL.A0P(menu);
        AbstractView$OnClickListenerC34281fs.A00(A0P.getActionView(), this, 17);
        TextView A0J = C12960it.A0J(A0P.getActionView(), R.id.cart_total_quantity);
        String str = this.A0I;
        if (str != null) {
            A0J.setText(str);
        }
        this.A08.A00.A05(this, new AnonymousClass02B(A0P, this) { // from class: X.4u2
            public final /* synthetic */ MenuItem A00;
            public final /* synthetic */ ProductListActivity A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
                if (r1.A0I == null) goto L_0x000f;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r4) {
                /*
                    r3 = this;
                    com.whatsapp.biz.catalog.view.activity.ProductListActivity r1 = r3.A01
                    android.view.MenuItem r2 = r3.A00
                    boolean r0 = X.C12970iu.A1Y(r4)
                    if (r0 == 0) goto L_0x000f
                    java.lang.String r1 = r1.A0I
                    r0 = 1
                    if (r1 != 0) goto L_0x0010
                L_0x000f:
                    r0 = 0
                L_0x0010:
                    r2.setVisible(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C105424u2.ANq(java.lang.Object):void");
            }
        });
        this.A08.A05();
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.A04(this.A0N);
        this.A0H.A06("plm_details_view_tag", false);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        this.A0D.A04();
        this.A0D.A06.A00();
        if (!this.A0M) {
            this.A0M = true;
            this.A09.A01(this.A0G, null, (Boolean) this.A08.A00.A01(), 23, null, null, null, null, null, null, 4);
        }
        super.onResume();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0M = false;
        this.A0L = false;
    }
}
