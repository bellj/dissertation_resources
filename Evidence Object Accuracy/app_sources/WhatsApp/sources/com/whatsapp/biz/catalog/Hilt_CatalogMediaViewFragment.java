package com.whatsapp.biz.catalog;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_CatalogMediaViewFragment extends MediaViewBaseFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1P();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1P()
            r2.A1O()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.biz.catalog.Hilt_CatalogMediaViewFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1P();
        A1O();
    }

    public void A1O() {
        if (!this.A02) {
            this.A02 = true;
            CatalogMediaViewFragment catalogMediaViewFragment = (CatalogMediaViewFragment) this;
            AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            C12960it.A1B(r1, catalogMediaViewFragment);
            catalogMediaViewFragment.A01 = C12980iv.A0W(r1);
            catalogMediaViewFragment.A04 = C12990iw.A0V(r1);
            catalogMediaViewFragment.A06 = C12970iu.A0Y(r1);
            catalogMediaViewFragment.A03 = C12980iv.A0Z(r1);
        }
    }

    public final void A1P() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
