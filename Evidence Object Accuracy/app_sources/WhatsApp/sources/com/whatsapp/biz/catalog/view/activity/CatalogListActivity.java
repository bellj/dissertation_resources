package com.whatsapp.biz.catalog.view.activity;

import X.AbstractActivityC37081lH;
import X.AbstractC116515Vt;
import X.AbstractC30111Wd;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass10A;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass19T;
import X.AnonymousClass1CQ;
import X.AnonymousClass1FZ;
import X.AnonymousClass1lK;
import X.AnonymousClass1lL;
import X.AnonymousClass23N;
import X.AnonymousClass2FL;
import X.AnonymousClass3CW;
import X.AnonymousClass3V3;
import X.AnonymousClass5TP;
import X.AnonymousClass5TQ;
import X.AnonymousClass5TS;
import X.C004802e;
import X.C103994re;
import X.C1099553r;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14650lo;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C16700pc;
import X.C19840ul;
import X.C19850um;
import X.C20020v5;
import X.C21770xx;
import X.C238013b;
import X.C25791Av;
import X.C25801Aw;
import X.C25821Ay;
import X.C26311Cv;
import X.C37101lJ;
import X.C42791vs;
import X.C48882Ih;
import X.C48892Ii;
import X.C48902Ij;
import X.C53842fM;
import X.C74983j6;
import X.C84633zd;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.catalog.view.PostcodeChangeBottomSheet;
import com.whatsapp.biz.catalog.view.activity.CatalogListActivity;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* loaded from: classes2.dex */
public class CatalogListActivity extends AbstractActivityC37081lH implements AnonymousClass5TQ, AbstractC116515Vt, AnonymousClass5TS {
    public C48902Ij A00;
    public WaTextView A01;
    public WaTextView A02;
    public C14650lo A03;
    public AnonymousClass1CQ A04;
    public PostcodeChangeBottomSheet A05;
    public AnonymousClass3CW A06;
    public C238013b A07;
    public C25801Aw A08;
    public Button A09;
    public C15550nR A0A;
    public C15610nY A0B;
    public C20020v5 A0C;
    public AnonymousClass17R A0D;
    public AnonymousClass12U A0E;
    public boolean A0F;

    public CatalogListActivity() {
        this(0);
    }

    public CatalogListActivity(int i) {
        this.A0F = false;
        ActivityC13830kP.A1P(this, 18);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC37081lH) this).A0K = (C19840ul) A1M.A1Q.get();
            ((AbstractActivityC37081lH) this).A05 = (C21770xx) A1M.A2s.get();
            ((AbstractActivityC37081lH) this).A04 = (C25791Av) A1M.A2t.get();
            ((AbstractActivityC37081lH) this).A0B = (AnonymousClass19T) A1M.A2y.get();
            ((AbstractActivityC37081lH) this).A0C = C12990iw.A0V(A1M);
            ((AbstractActivityC37081lH) this).A07 = (C25821Ay) A1M.A31.get();
            ((AbstractActivityC37081lH) this).A0H = C12990iw.A0Z(A1M);
            ((AbstractActivityC37081lH) this).A08 = (AnonymousClass1FZ) A1M.AGJ.get();
            ((AbstractActivityC37081lH) this).A09 = (C19850um) A1M.A2v.get();
            ((AbstractActivityC37081lH) this).A02 = (C48892Ii) A1L.A0R.get();
            ((AbstractActivityC37081lH) this).A01 = (C48882Ih) A1L.A0Q.get();
            ((AbstractActivityC37081lH) this).A03 = (AnonymousClass10A) A1M.A2W.get();
            ((AbstractActivityC37081lH) this).A0I = (C26311Cv) A1M.AAQ.get();
            ((AbstractActivityC37081lH) this).A0A = C12980iv.A0Z(A1M);
            this.A0E = ActivityC13830kP.A1N(A1M);
            this.A0D = (AnonymousClass17R) A1M.AIz.get();
            this.A0A = C12960it.A0O(A1M);
            this.A0B = C12960it.A0P(A1M);
            this.A07 = (C238013b) A1M.A1Z.get();
            this.A00 = (C48902Ij) A1L.A0T.get();
            this.A0C = (C20020v5) A1M.A3B.get();
            this.A03 = C12980iv.A0Y(A1M);
            this.A08 = (C25801Aw) A1M.A33.get();
            this.A04 = A1L.A02();
        }
    }

    @Override // X.AbstractActivityC37081lH
    public void A2f(List list) {
        super.A2f(list);
        this.A09.setText(C12960it.A0X(this, ((AbstractActivityC37081lH) this).A0L, C12970iu.A1b(), 0, R.string.product_list_view_cart));
        C12990iw.A19(this.A09, ((AnonymousClass1lK) ((AbstractActivityC37081lH) this).A0E).A05.isEmpty() ? 1 : 0);
        A2h();
    }

    public final void A2g() {
        if (((AbstractActivityC37081lH) this).A09.A0H(((AbstractActivityC37081lH) this).A0J)) {
            ((AbstractActivityC37081lH) this).A09.A0D(((AbstractActivityC37081lH) this).A0J);
        }
        C25801Aw r2 = this.A08;
        UserJid userJid = ((AbstractActivityC37081lH) this).A0J;
        C16700pc.A0E(userJid, 0);
        synchronized (r2) {
            r2.A00.remove(userJid);
        }
        if (((AnonymousClass1lL) ((AbstractActivityC37081lH) this).A0E).A00.size() > 0) {
            ((AnonymousClass1lL) ((AbstractActivityC37081lH) this).A0E).A00.clear();
            ((AbstractActivityC37081lH) this).A0E.A02();
            ((AbstractActivityC37081lH) this).A0E.A0K();
        }
        C37101lJ r4 = ((AbstractActivityC37081lH) this).A0E;
        int i = 0;
        do {
            List list = ((AnonymousClass1lL) r4).A00;
            list.add(new C84633zd(9));
            r4.A04(C12980iv.A0C(list));
            i++;
        } while (i < 3);
        ((AbstractActivityC37081lH) this).A0F.A06(((AbstractActivityC37081lH) this).A0J);
        ((AbstractActivityC37081lH) this).A0F.A05(((AbstractActivityC37081lH) this).A0J);
        ((AbstractActivityC37081lH) this).A0F.A0H.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (((X.AbstractActivityC37081lH) r3).A00.canScrollVertically(1) == false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2h() {
        /*
            r3 = this;
            r0 = 2131365962(0x7f0a104a, float:1.8351804E38)
            android.view.View r2 = r3.findViewById(r0)
            X.1lJ r0 = r3.A0E
            java.util.List r0 = r0.A05
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x001b
            androidx.recyclerview.widget.RecyclerView r1 = r3.A00
            r0 = 1
            boolean r1 = r1.canScrollVertically(r0)
            r0 = 0
            if (r1 != 0) goto L_0x001d
        L_0x001b:
            r0 = 8
        L_0x001d:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.biz.catalog.view.activity.CatalogListActivity.A2h():void");
    }

    public final void A2i(boolean z) {
        PostcodeChangeBottomSheet postcodeChangeBottomSheet = new PostcodeChangeBottomSheet(this, z);
        this.A05 = postcodeChangeBottomSheet;
        String str = (String) ((AbstractActivityC37081lH) this).A0F.A0A.A01();
        postcodeChangeBottomSheet.A08 = str;
        WaEditText waEditText = postcodeChangeBottomSheet.A04;
        if (waEditText != null) {
            waEditText.setText(str);
        }
        C42791vs.A01(this.A05, A0V());
    }

    @Override // X.AnonymousClass5TQ
    public void ANm() {
        ((AbstractActivityC37081lH) this).A0F.A0H.A00();
    }

    @Override // X.AbstractC116515Vt
    public void AU0() {
        this.A05 = null;
    }

    @Override // X.AbstractC116515Vt
    public void AU1(String str) {
        A2C(R.string.pincode_verification_progress_spinner);
        C53842fM r0 = ((AbstractActivityC37081lH) this).A0F;
        AnonymousClass19T r4 = r0.A0G;
        r4.A06.A00(new C1099553r(new AnonymousClass3V3(r0, str), r4), r0.A0M, str).A06();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass01E A0A = A0V().A0A("CatalogSearchFragmentTag");
        if (A0A == null || !(A0A instanceof CatalogSearchFragment) || !((CatalogSearchFragment) A0A).A1F()) {
            super.onBackPressed();
        }
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        AnonymousClass3CW r0;
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2 && (r0 = this.A06) != null) {
            r0.A00();
            this.A06 = null;
        }
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Button button = (Button) findViewById(R.id.view_cart);
        this.A09 = button;
        C12960it.A10(button, this, 47);
        this.A0C.A08(new C103994re(0), ((AbstractActivityC37081lH) this).A0J);
        if (this.A03.A08()) {
            C12960it.A18(this, ((AbstractActivityC37081lH) this).A0F.A0A, 13);
            C12960it.A17(this, ((AbstractActivityC37081lH) this).A0F.A09, 2);
            this.A03.A03(new AbstractC30111Wd() { // from class: X.3Un
                /* JADX WARNING: Removed duplicated region for block: B:20:0x006e  */
                /* JADX WARNING: Removed duplicated region for block: B:25:0x00a3  */
                /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
                @Override // X.AbstractC30111Wd
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void ANP(X.C30141Wg r8) {
                    /*
                    // Method dump skipped, instructions count: 253
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C68213Un.ANP(X.1Wg):void");
                }
            }, ((AbstractActivityC37081lH) this).A0J);
        }
        ((AbstractActivityC37081lH) this).A00.A0n(new C74983j6(this));
        C12960it.A18(this, ((AbstractActivityC37081lH) this).A0F.A0P, 12);
        C12960it.A18(this, ((AbstractActivityC37081lH) this).A0F.A06, 11);
        this.A04.A00(new AnonymousClass5TP() { // from class: X.3VI
            @Override // X.AnonymousClass5TP
            public final void AQG(UserJid userJid) {
                C004902f A0P = C12970iu.A0P(CatalogListActivity.this);
                A0P.A0B(AnonymousClass3AU.A00(userJid, 0), "CatalogSearchFragmentTag", R.id.catalog_search_host);
                A0P.A01();
            }
        }, ((AbstractActivityC37081lH) this).A0J);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 106) {
            return super.onCreateDialog(i);
        }
        C15370n3 A0B = this.A0A.A0B(((AbstractActivityC37081lH) this).A0J);
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0A(C12960it.A0X(this, this.A0B.A04(A0B), C12970iu.A1b(), 0, R.string.cannot_send_to_blocked_contact_1));
        A0S.setPositiveButton(R.string.unblock, new IDxCListenerShape3S0200000_1_I1(A0B, 1, this));
        C12970iu.A1K(A0S, this, 13, R.string.cancel);
        return A0S.create();
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.catalog_with_search, menu);
        MenuItem findItem = menu.findItem(R.id.menu_edit);
        MenuItem findItem2 = menu.findItem(R.id.menu_share);
        findItem2.setActionView(R.layout.menu_item_share);
        C12960it.A0r(this, findItem2.getActionView(), R.string.catalog_product_share_title);
        findItem2.setVisible(((AbstractActivityC37081lH) this).A0M);
        AnonymousClass23N.A01(findItem2.getActionView());
        AbstractView$OnClickListenerC34281fs.A00(findItem2.getActionView(), this, 16);
        findItem.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        ((AbstractActivityC37081lH) this).A0F.A0A.A04(this);
        ((AbstractActivityC37081lH) this).A0F.A09.A04(this);
        ((AbstractActivityC37081lH) this).A0F.A0P.A04(this);
        PostcodeChangeBottomSheet postcodeChangeBottomSheet = this.A05;
        if (postcodeChangeBottomSheet != null) {
            postcodeChangeBottomSheet.A1B();
        }
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (16908332 != menuItem.getItemId()) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    @Override // X.AbstractActivityC37081lH, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        String str = (String) ((AbstractActivityC37081lH) this).A0F.A0A.A01();
        String A0D = ((ActivityC13810kN) this).A09.A0D(((AbstractActivityC37081lH) this).A0J.getRawString());
        if (str != null && A0D != null && !str.equals(A0D)) {
            ((AbstractActivityC37081lH) this).A0F.A0A.A0A(A0D);
            String A0p = C12980iv.A0p(((ActivityC13810kN) this).A09.A00, C12960it.A0d(((AbstractActivityC37081lH) this).A0J.getRawString(), C12960it.A0k("dc_location_name_")));
            if (A0p != null) {
                ((AbstractActivityC37081lH) this).A0F.A09.A0A(A0p);
            }
            A2g();
        }
    }

    @Override // X.AnonymousClass5TS
    public void setPostcodeAndLocationViews(View view) {
        this.A02 = C12960it.A0N(view, R.id.postcode_item_text);
        this.A01 = C12960it.A0N(view, R.id.postcode_item_location_name);
    }
}
