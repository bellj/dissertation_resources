package com.whatsapp.biz.catalog.view;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass2P7;
import X.AnonymousClass2Zc;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes2.dex */
public class CarouselScrollbarView extends View implements AnonymousClass004 {
    public RecyclerView A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;
    public final AnonymousClass2Zc A04;

    public CarouselScrollbarView(Context context) {
        this(context, null);
    }

    public CarouselScrollbarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CarouselScrollbarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        AnonymousClass2Zc r0 = new AnonymousClass2Zc(context);
        this.A04 = r0;
        r0.setCallback(this);
        this.A03 = false;
    }

    public final void A00() {
        RecyclerView recyclerView = this.A00;
        AnonymousClass009.A03(recyclerView);
        int computeHorizontalScrollExtent = recyclerView.computeHorizontalScrollExtent();
        int computeHorizontalScrollOffset = this.A00.computeHorizontalScrollOffset();
        int computeHorizontalScrollRange = this.A00.computeHorizontalScrollRange();
        if (computeHorizontalScrollExtent < computeHorizontalScrollRange) {
            int width = (getWidth() * computeHorizontalScrollExtent) / computeHorizontalScrollRange;
            int width2 = ((getWidth() - width) * computeHorizontalScrollOffset) / (computeHorizontalScrollRange - computeHorizontalScrollExtent);
            AnonymousClass2Zc r1 = this.A04;
            if (!(r1.A01 == width2 && r1.A00 == width)) {
                r1.A00 = width;
                r1.A01 = width2;
                r1.A00();
            }
            this.A03 = true;
            return;
        }
        AnonymousClass2Zc r2 = this.A04;
        if (!(r2.A01 == 0 && r2.A00 == 0)) {
            r2.A00 = 0;
            r2.A01 = 0;
            r2.A00();
        }
        this.A03 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03) {
            this.A04.draw(canvas);
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.A04.setBounds(0, 0, i, i2);
        A00();
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || this.A04 == drawable;
    }
}
