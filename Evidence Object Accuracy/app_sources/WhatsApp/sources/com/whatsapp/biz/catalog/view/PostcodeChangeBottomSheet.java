package com.whatsapp.biz.catalog.view;

import X.AbstractC116515Vt;
import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass18U;
import X.AnonymousClass3MB;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14900mE;
import X.C252718t;
import X.C42971wC;
import X.C58272oQ;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class PostcodeChangeBottomSheet extends Hilt_PostcodeChangeBottomSheet {
    public LinearLayout A00;
    public C14900mE A01;
    public AnonymousClass18U A02;
    public TextEmojiLabel A03;
    public WaEditText A04;
    public WaTextView A05;
    public AnonymousClass01d A06;
    public C252718t A07;
    public String A08 = "";
    public final AbstractC116515Vt A09;
    public final boolean A0A;

    public PostcodeChangeBottomSheet(AbstractC116515Vt r2, boolean z) {
        this.A09 = r2;
        this.A0A = z;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.dc_catalog_postcode_bottom_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        this.A09.AU0();
        super.A11();
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Dialog dialog = ((DialogFragment) this).A03;
        if (!(!this.A0A || dialog == null || dialog.getWindow() == null)) {
            dialog.getWindow().setSoftInputMode(21);
        }
        this.A00 = (LinearLayout) AnonymousClass028.A0D(view, R.id.dc_postcode_bottom_sheet);
        this.A04 = (WaEditText) AnonymousClass028.A0D(view, R.id.change_postcode_edit_text);
        this.A03 = C12970iu.A0T(view, R.id.change_postcode_privacy_message);
        this.A05 = C12960it.A0N(view, R.id.change_postcode_invalid_message);
        String A0I = A0I(R.string.change_postcode_privacy_message);
        Context A0p = A0p();
        String A0I2 = A0I(R.string.learn_more);
        SpannableStringBuilder A0J = C12990iw.A0J(A0I2);
        A0J.setSpan(new C58272oQ(A0p, this.A02, this.A01, this.A06, "https://faq.whatsapp.com/general/security-and-privacy/about-sharing-your-information-with-businesses-on-whatsapp"), 0, A0I2.length(), 33);
        SpannableStringBuilder A02 = C42971wC.A02(A0I, A0J);
        AbstractC28491Nn.A03(this.A03);
        AbstractC28491Nn.A04(this.A03, this.A06);
        this.A03.setLinksClickable(true);
        this.A03.setFocusable(false);
        this.A03.setText(A02);
        this.A04.addTextChangedListener(new AnonymousClass3MB(this));
        this.A04.setText(this.A08);
        this.A04.requestFocus();
        this.A04.selectAll();
        C12960it.A10(AnonymousClass028.A0D(view, R.id.postcode_button_cancel), this, 45);
        C12960it.A10(AnonymousClass028.A0D(view, R.id.postcode_button_enter), this, 46);
    }

    public void A1M() {
        WaEditText waEditText = this.A04;
        if (waEditText != null) {
            waEditText.clearFocus();
        }
        LinearLayout linearLayout = this.A00;
        if (linearLayout != null && C252718t.A00(linearLayout)) {
            this.A07.A01(this.A00);
        }
        A1B();
    }

    public void A1N() {
        this.A03.setVisibility(8);
        this.A05.setVisibility(0);
        this.A04.getBackground().setColorFilter(A02().getColor(R.color.catalog_error_color), PorterDuff.Mode.SRC_ATOP);
    }
}
