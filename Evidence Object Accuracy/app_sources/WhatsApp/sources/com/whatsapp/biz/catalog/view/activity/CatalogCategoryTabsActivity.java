package com.whatsapp.biz.catalog.view.activity;

import X.AbstractActivityC60272wQ;
import X.AbstractC005102i;
import X.AbstractC16710pd;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass02B;
import X.AnonymousClass1CQ;
import X.AnonymousClass2FL;
import X.AnonymousClass3F7;
import X.AnonymousClass4Yq;
import X.AnonymousClass5TP;
import X.AnonymousClass5TQ;
import X.C12980iv;
import X.C16700pc;
import X.C21770xx;
import X.C48882Ih;
import X.C53872fS;
import X.C71863dd;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.activity.CatalogCategoryTabsActivity;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public final class CatalogCategoryTabsActivity extends AbstractActivityC60272wQ implements AnonymousClass5TQ {
    public ViewPager A00;
    public AnonymousClass1CQ A01;
    public boolean A02;
    public final AbstractC16710pd A03;

    public CatalogCategoryTabsActivity() {
        this(0);
        this.A03 = AnonymousClass4Yq.A00(new C71863dd(this));
    }

    public CatalogCategoryTabsActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 17);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC60272wQ) this).A00 = (C48882Ih) A1L.A0Q.get();
            ((AbstractActivityC60272wQ) this).A01 = (C21770xx) A1M.A2s.get();
            ((AbstractActivityC60272wQ) this).A02 = C12980iv.A0Z(A1M);
            this.A01 = A1L.A02();
        }
    }

    @Override // X.AnonymousClass5TQ
    public void ANm() {
        ((C53872fS) ((AbstractActivityC60272wQ) this).A06.getValue()).A03.A00();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        CatalogSearchFragment catalogSearchFragment;
        AnonymousClass01E A0A = A0V().A0A("CategoryTabsSearchFragmentTag");
        if (A0A == null || !(A0A instanceof CatalogSearchFragment) || (catalogSearchFragment = (CatalogSearchFragment) A0A) == null || !catalogSearchFragment.A1F()) {
            super.onBackPressed();
        }
    }

    @Override // X.AbstractActivityC60272wQ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_catalog_category_host);
        ((ViewStub) findViewById(R.id.stub_toolbar_search)).inflate();
        View findViewById = findViewById(R.id.toolbar);
        C16700pc.A0B(findViewById);
        A1e((Toolbar) findViewById);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0A(R.string.catalog_categories_host_page);
        }
        AnonymousClass1CQ r2 = this.A01;
        if (r2 != null) {
            r2.A00(new AnonymousClass5TP() { // from class: X.3VH
                @Override // X.AnonymousClass5TP
                public final void AQG(UserJid userJid) {
                    CatalogCategoryTabsActivity catalogCategoryTabsActivity = CatalogCategoryTabsActivity.this;
                    C16700pc.A0E(catalogCategoryTabsActivity, 0);
                    C004902f A0P = C12970iu.A0P(catalogCategoryTabsActivity);
                    A0P.A0B(AnonymousClass3AU.A00(catalogCategoryTabsActivity.A2e(), 1), "CategoryTabsSearchFragmentTag", R.id.catalog_search_host);
                    A0P.A01();
                }
            }, A2e());
            String stringExtra = getIntent().getStringExtra("selected_category_parent_id");
            AnonymousClass009.A05(stringExtra);
            C16700pc.A0B(stringExtra);
            AbstractC16710pd r22 = this.A03;
            ((CatalogCategoryTabsViewModel) r22.getValue()).A00.A05(this, new AnonymousClass02B(stringExtra) { // from class: X.3RB
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    C114075Kc r1;
                    CatalogCategoryTabsActivity catalogCategoryTabsActivity = CatalogCategoryTabsActivity.this;
                    String str = this.A01;
                    List list = (List) obj;
                    C16700pc.A0I(catalogCategoryTabsActivity, str);
                    AnonymousClass01F A0V = catalogCategoryTabsActivity.A0V();
                    C16700pc.A0B(A0V);
                    C53752fA r4 = new C53752fA(A0V);
                    C16700pc.A0B(list);
                    r4.A00 = list;
                    ViewPager viewPager = (ViewPager) C16700pc.A00(catalogCategoryTabsActivity, R.id.view_pager);
                    catalogCategoryTabsActivity.A03.getValue();
                    Iterator it = list.iterator();
                    int i = 0;
                    while (true) {
                        if (it.hasNext()) {
                            if (C16700pc.A0O(((AnonymousClass3F7) it.next()).A01, str)) {
                                break;
                            }
                            i++;
                        } else {
                            i = -1;
                            break;
                        }
                    }
                    viewPager.setAdapter(r4);
                    viewPager.setCurrentItem(i);
                    catalogCategoryTabsActivity.A00 = viewPager;
                    TabLayout tabLayout = (TabLayout) AnonymousClass00T.A05(catalogCategoryTabsActivity, R.id.tabs);
                    ViewPager viewPager2 = catalogCategoryTabsActivity.A00;
                    if (viewPager2 == null) {
                        throw C16700pc.A06("viewPager");
                    }
                    tabLayout.setupWithViewPager(viewPager2);
                    tabLayout.A0D(new AnonymousClass3TW(tabLayout, catalogCategoryTabsActivity, list));
                    int size = tabLayout.A0c.size();
                    if (size <= Integer.MIN_VALUE) {
                        r1 = C114075Kc.A00;
                    } else {
                        r1 = new C114075Kc(0, size - 1);
                    }
                    Iterator it2 = r1.iterator();
                    while (it2.hasNext()) {
                        int A00 = ((AnonymousClass5DO) it2).A00();
                        View childAt = tabLayout.getChildAt(0);
                        if (childAt != null) {
                            View childAt2 = ((ViewGroup) childAt).getChildAt(A00);
                            ViewGroup.LayoutParams layoutParams = childAt2.getLayoutParams();
                            if (layoutParams != null) {
                                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                                int dimensionPixelSize = C12960it.A09(tabLayout).getDimensionPixelSize(R.dimen.chips_tab_pills_margin);
                                int dimensionPixelSize2 = C12960it.A09(tabLayout).getDimensionPixelSize(R.dimen.chips_tab_pills_margin_vertical);
                                if (A00 == 0) {
                                    int dimensionPixelSize3 = C12960it.A09(tabLayout).getDimensionPixelSize(R.dimen.chips_tab_pills_starting_margin);
                                    if (C28141Kv.A00(((ActivityC13830kP) catalogCategoryTabsActivity).A01)) {
                                        marginLayoutParams.setMargins(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize3, dimensionPixelSize2);
                                    } else {
                                        marginLayoutParams.setMargins(dimensionPixelSize3, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2);
                                    }
                                } else {
                                    marginLayoutParams.setMargins(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2);
                                }
                                childAt2.requestLayout();
                            } else {
                                throw C12980iv.A0n("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                            }
                        } else {
                            throw C12980iv.A0n("null cannot be cast to non-null type android.view.ViewGroup");
                        }
                    }
                }
            });
            CatalogCategoryTabsViewModel catalogCategoryTabsViewModel = (CatalogCategoryTabsViewModel) r22.getValue();
            catalogCategoryTabsViewModel.A03.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(catalogCategoryTabsViewModel, 46, A2e()));
            return;
        }
        throw C16700pc.A06("catalogSearchManager");
    }

    @Override // X.AbstractActivityC60272wQ, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C16700pc.A0E(menu, 0);
        getMenuInflater().inflate(R.menu.catalog_category_menu_with_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        CatalogSearchFragment catalogSearchFragment;
        C16700pc.A0E(intent, 0);
        super.onNewIntent(intent);
        String stringExtra = intent.getStringExtra("selected_category_parent_id");
        if (stringExtra != null) {
            AbstractC16710pd r1 = this.A03;
            List A0z = C12980iv.A0z(((CatalogCategoryTabsViewModel) r1.getValue()).A00);
            if (A0z != null) {
                r1.getValue();
                Iterator it = A0z.iterator();
                int i = 0;
                while (true) {
                    if (it.hasNext()) {
                        if (C16700pc.A0O(((AnonymousClass3F7) it.next()).A01, stringExtra)) {
                            break;
                        }
                        i++;
                    } else {
                        i = -1;
                        break;
                    }
                }
                ViewPager viewPager = this.A00;
                if (viewPager == null) {
                    throw C16700pc.A06("viewPager");
                }
                viewPager.setCurrentItem(i);
            }
            AnonymousClass01E A0A = A0V().A0A("CategoryTabsSearchFragmentTag");
            if (A0A != null && (A0A instanceof CatalogSearchFragment) && (catalogSearchFragment = (CatalogSearchFragment) A0A) != null) {
                catalogSearchFragment.A1E(false);
            }
        }
    }
}
