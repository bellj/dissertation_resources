package com.whatsapp.biz.catalog.view;

import X.C52162aM;
import X.C83573xS;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* loaded from: classes2.dex */
public class EllipsizedTextEmojiLabel extends TextEmojiLabel {
    public int A00;
    public int A01;
    public int A02;
    public View.OnClickListener A03;
    public CharSequence A04;
    public List A05;
    public boolean A06;

    public EllipsizedTextEmojiLabel(Context context) {
        super(context);
        A08();
        this.A07 = new C52162aM();
        this.A02 = R.color.link_color;
    }

    public EllipsizedTextEmojiLabel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
        this.A07 = new C52162aM();
        this.A02 = R.color.link_color;
    }

    public EllipsizedTextEmojiLabel(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        this.A07 = new C52162aM();
        this.A02 = R.color.link_color;
    }

    public EllipsizedTextEmojiLabel(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A08();
    }

    @Override // com.whatsapp.TextEmojiLabel
    public void A0F(CharSequence charSequence, List list, int i, boolean z) {
        this.A04 = charSequence;
        this.A05 = list;
        this.A01 = i;
        int codePointCount = Character.codePointCount(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        if (codePointCount > this.A00) {
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(getContext().getString(R.string.read_more));
            spannableStringBuilder2.setSpan(new C83573xS(getContext(), this, this.A02), 0, spannableStringBuilder2.length(), 18);
            int i2 = 0;
            for (int i3 = 0; i3 < this.A00; i3++) {
                i2 += Character.charCount(Character.codePointAt(charSequence, i2));
            }
            spannableStringBuilder = spannableStringBuilder.delete(i2, spannableStringBuilder.length()).append((CharSequence) "... ").append((CharSequence) spannableStringBuilder2);
        }
        super.A0F(spannableStringBuilder, list, i, true);
    }

    public void setEllipsizeLength(int i) {
        this.A00 = i;
    }

    public void setOnTextExpandClickListener(View.OnClickListener onClickListener) {
        this.A03 = onClickListener;
    }

    public void setReadMoreColor(int i) {
        this.A02 = i;
    }
}
