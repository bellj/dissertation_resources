package com.whatsapp.biz.catalog.view;

import X.AbstractC116295Uw;
import X.AbstractC14440lR;
import X.AbstractC42671vd;
import X.AbstractC53012cD;
import X.AbstractC72383eV;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass17R;
import X.AnonymousClass19N;
import X.AnonymousClass19Q;
import X.AnonymousClass19S;
import X.AnonymousClass19T;
import X.AnonymousClass2GZ;
import X.AnonymousClass36H;
import X.AnonymousClass3C2;
import X.AnonymousClass3VN;
import X.AnonymousClass3VO;
import X.AnonymousClass4TJ;
import X.AnonymousClass5RI;
import X.C12960it;
import X.C12990iw;
import X.C14650lo;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C16120oU;
import X.C252918v;
import X.C253619c;
import X.C254719n;
import X.C25821Ay;
import X.C30141Wg;
import X.C37071lG;
import X.C44691zO;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class CatalogMediaCard extends AbstractC53012cD {
    public int A00;
    public int A01;
    public AnonymousClass12P A02;
    public C14900mE A03;
    public C15570nT A04;
    public C14650lo A05;
    public C25821Ay A06;
    public AnonymousClass19S A07;
    public AnonymousClass19Q A08;
    public C252918v A09;
    public C37071lG A0A;
    public AnonymousClass5RI A0B;
    public AnonymousClass3C2 A0C;
    public AbstractC72383eV A0D;
    public AnonymousClass018 A0E;
    public C14850m9 A0F;
    public UserJid A0G;
    public AnonymousClass17R A0H;
    public AnonymousClass36H A0I;
    public AbstractC14440lR A0J;
    public String A0K;
    public boolean A0L;

    public CatalogMediaCard(Context context) {
        this(context, null);
    }

    public CatalogMediaCard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CatalogMediaCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        boolean z;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A05);
            z = obtainStyledAttributes.getBoolean(0, false);
            obtainStyledAttributes.recycle();
        } else {
            z = false;
        }
        AnonymousClass36H A00 = A00(z);
        this.A0I = A00;
        A00.setTopShadowVisibility(0);
        this.A0I.setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        this.A0A = new C37071lG(this.A09);
        int thumbnailPixelSize = this.A0I.getThumbnailPixelSize();
        this.A01 = thumbnailPixelSize;
        this.A09.A01 = thumbnailPixelSize;
    }

    public AnonymousClass36H A00(boolean z) {
        LayoutInflater A0E = C12960it.A0E(this);
        int i = R.layout.business_product_catalog_card;
        if (z) {
            i = R.layout.business_product_catalog_card_grid;
        }
        return (AnonymousClass36H) AnonymousClass028.A0D(A0E.inflate(i, (ViewGroup) this, true), R.id.product_catalog_media_card_view);
    }

    public List A01(UserJid userJid, String str, List list, boolean z) {
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        for (int i2 = 0; i2 < list.size() && i < 6; i2++) {
            C44691zO r5 = (C44691zO) list.get(i2);
            if (r5.A01() && !r5.A0D.equals(this.A0K)) {
                i++;
                A0l.add(new AnonymousClass4TJ(null, this.A0D.AHC(r5, userJid, z), new AbstractC116295Uw(r5, this) { // from class: X.3ad
                    public final /* synthetic */ C44691zO A00;
                    public final /* synthetic */ CatalogMediaCard A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC116295Uw
                    public final void AQS(AnonymousClass2x5 r9, int i3) {
                        CatalogMediaCard catalogMediaCard = this.A01;
                        C44691zO r1 = this.A00;
                        if (!r1.A02()) {
                            r9.setTag(r1.A0D);
                            catalogMediaCard.A0A.A02(r9, (C44741zT) C12980iv.A0o(r1.A06), 
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0025: INVOKE  
                                  (wrap: X.1lG : 0x0010: IGET  (r2v1 X.1lG A[REMOVE]) = (r2v0 'catalogMediaCard' com.whatsapp.biz.catalog.view.CatalogMediaCard) com.whatsapp.biz.catalog.view.CatalogMediaCard.A0A X.1lG)
                                  (r9v0 'r9' X.2x5)
                                  (wrap: X.1zT : 0x0018: CHECK_CAST (r4v1 X.1zT A[REMOVE]) = (X.1zT) (wrap: java.lang.Object : 0x0014: INVOKE  (r4v0 java.lang.Object A[REMOVE]) = (wrap: java.util.List : 0x0012: IGET  (r0v2 java.util.List A[REMOVE]) = (r1v0 'r1' X.1zO) X.1zO.A06 java.util.List) type: STATIC call: X.0iv.A0o(java.util.List):java.lang.Object))
                                  (wrap: X.53e : 0x0022: CONSTRUCTOR  (r5v0 X.53e A[REMOVE]) = (r9v0 'r9' X.2x5) call: X.53e.<init>(X.2x5):void type: CONSTRUCTOR)
                                  (wrap: X.53m : 0x001d: CONSTRUCTOR  (r6v0 X.53m A[REMOVE]) = (r9v0 'r9' X.2x5) call: X.53m.<init>(X.2x5):void type: CONSTRUCTOR)
                                  (2 int)
                                 type: VIRTUAL call: X.1lG.A02(android.widget.ImageView, X.1zT, X.5TN, X.2E5, int):void in method: X.3ad.AQS(X.2x5, int):void, file: classes2.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0022: CONSTRUCTOR  (r5v0 X.53e A[REMOVE]) = (r9v0 'r9' X.2x5) call: X.53e.<init>(X.2x5):void type: CONSTRUCTOR in method: X.3ad.AQS(X.2x5, int):void, file: classes2.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 23 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.53e, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 29 more
                                */
                            /*
                                this = this;
                                com.whatsapp.biz.catalog.view.CatalogMediaCard r2 = r8.A01
                                X.1zO r1 = r8.A00
                                boolean r0 = r1.A02()
                                r3 = r9
                                if (r0 != 0) goto L_0x0029
                                java.lang.String r0 = r1.A0D
                                r9.setTag(r0)
                                X.1lG r2 = r2.A0A
                                java.util.List r0 = r1.A06
                                java.lang.Object r4 = X.C12980iv.A0o(r0)
                                X.1zT r4 = (X.C44741zT) r4
                                r7 = 2
                                X.53m r6 = new X.53m
                                r6.<init>(r9)
                                X.53e r5 = new X.53e
                                r5.<init>(r9)
                                r2.A02(r3, r4, r5, r6, r7)
                                return
                            L_0x0029:
                                X.AnonymousClass4Dz.A00(r9)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.C70033ad.AQS(X.2x5, int):void");
                        }
                    }, null, str, AbstractC42671vd.A0Z(AnonymousClass19N.A00(0, r5.A0D))));
                }
            }
            return A0l;
        }

        public void A02() {
            this.A0A.A00();
            AnonymousClass3C2 r4 = this.A0C;
            AbstractC72383eV[] r2 = {r4.A01, r4.A00};
            int i = 0;
            do {
                AbstractC72383eV r0 = r2[i];
                if (r0 != null) {
                    r0.A7A();
                }
                i++;
            } while (i < 2);
            r4.A00 = null;
            r4.A01 = null;
        }

        public void A03(C30141Wg r30, UserJid userJid, String str, boolean z, boolean z2) {
            AnonymousClass3VN r9;
            this.A0G = userJid;
            this.A0L = z2;
            this.A0K = str;
            AnonymousClass3C2 r10 = this.A0C;
            C254719n r6 = r10.A06;
            if (r6.A01(r30)) {
                AnonymousClass3VN r92 = r10.A01;
                AnonymousClass3VN r93 = r92;
                if (r92 == null) {
                    C16120oU r5 = r10.A0F;
                    AnonymousClass3VN r94 = new AnonymousClass3VN(r10.A04, r6, r10.A09, r10.A0D, this, r10.A0E, r5, r10.A0I);
                    r10.A01 = r94;
                    r93 = r94;
                }
                AnonymousClass009.A05(r30);
                r93.A00 = r30;
                r9 = r93;
            } else {
                AnonymousClass3VO r95 = r10.A00;
                AnonymousClass3VO r96 = r95;
                if (r95 == null) {
                    C14900mE r0 = r10.A03;
                    C15570nT r15 = r10.A05;
                    AnonymousClass12P r8 = r10.A02;
                    AbstractC14440lR r7 = r10.A0H;
                    AnonymousClass17R r62 = r10.A0G;
                    AnonymousClass19T r52 = r10.A0C;
                    C253619c r4 = r10.A0E;
                    AnonymousClass3VO r97 = new AnonymousClass3VO(r8, r0, r15, r10.A07, r10.A08, r10.A0A, r10.A0B, r52, this, r4, r62, r7, z2);
                    r10.A00 = r97;
                    r96 = r97;
                }
                r96.A01 = str;
                r96.A00 = r30;
                r9 = r96;
            }
            this.A0D = r9;
            if (z && r9.AIA(userJid)) {
                this.A0D.AQR(userJid);
            } else if (this.A0D.AdS()) {
                setVisibility(8);
            } else {
                this.A0D.AIo(userJid);
                this.A0D.A5n();
                this.A0D.A9s(userJid, this.A01);
            }
        }

        public AnonymousClass5RI getCatalogPreviewItemClickListener() {
            return this.A0B;
        }

        public String getMediaCardViewErrorText() {
            return this.A0I.getError();
        }

        public AbstractC72383eV getMediaCardViewPresenter() {
            return this.A0D;
        }

        public void setCatalogPreviewItemClickListener(AnonymousClass5RI r1) {
            this.A0B = r1;
        }

        public void setError(int i) {
            this.A0I.setError(C12990iw.A0q(this, i));
        }

        public void setupThumbnails(UserJid userJid, int i, List list) {
            AbstractC72383eV r1 = this.A0D;
            UserJid userJid2 = this.A0G;
            AnonymousClass009.A05(userJid2);
            int AFx = r1.AFx(userJid2);
            if (AFx != this.A00) {
                this.A0I.A09(A01(userJid, C12990iw.A0q(this, i), list, this.A0L), 5);
                this.A00 = AFx;
            }
        }
    }
