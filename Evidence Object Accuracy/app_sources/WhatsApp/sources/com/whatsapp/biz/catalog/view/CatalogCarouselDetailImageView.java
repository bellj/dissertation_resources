package com.whatsapp.biz.catalog.view;

import X.AbstractC14440lR;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass19Q;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C13000ix;
import X.C25831Az;
import X.C37071lG;
import X.C44691zO;
import X.C54292gU;
import X.C90144Mt;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CatalogCarouselDetailImageView extends FrameLayout implements AnonymousClass004 {
    public RecyclerView A00;
    public C25831Az A01;
    public C44691zO A02;
    public AnonymousClass19Q A03;
    public C37071lG A04;
    public CarouselScrollbarView A05;
    public C54292gU A06;
    public AnonymousClass01d A07;
    public UserJid A08;
    public AbstractC14440lR A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;

    public CatalogCarouselDetailImageView(Context context) {
        this(context, null);
    }

    public CatalogCarouselDetailImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CatalogCarouselDetailImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A09 = C12960it.A0T(A00);
            this.A07 = C12960it.A0Q(A00);
            this.A03 = C12980iv.A0Z(A00);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public final void setImageAndGradient(C90144Mt r4, boolean z, ThumbnailButton thumbnailButton, Bitmap bitmap, View view) {
        GradientDrawable.Orientation orientation;
        int[] A07 = C13000ix.A07();
        A07[0] = r4.A01;
        A07[1] = r4.A00;
        if (z) {
            orientation = GradientDrawable.Orientation.LEFT_RIGHT;
        } else {
            orientation = GradientDrawable.Orientation.TOP_BOTTOM;
        }
        view.setBackground(new GradientDrawable(orientation, A07));
        thumbnailButton.setImageBitmap(bitmap);
    }
}
