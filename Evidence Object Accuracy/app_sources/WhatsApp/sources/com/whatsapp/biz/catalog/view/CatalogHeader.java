package com.whatsapp.biz.catalog.view;

import X.AbstractC13940ka;
import X.AbstractC14440lR;
import X.AbstractC30111Wd;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass10S;
import X.AnonymousClass131;
import X.AnonymousClass1M2;
import X.AnonymousClass1US;
import X.AnonymousClass2GE;
import X.AnonymousClass2P6;
import X.AnonymousClass37B;
import X.AnonymousClass3G9;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14650lo;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C17220qS;
import X.C20670w8;
import X.C22700zV;
import X.C246716k;
import X.C42941w9;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.biz.catalog.view.CatalogHeader;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;

/* loaded from: classes2.dex */
public class CatalogHeader extends AspectRatioFrameLayout implements AbstractC13940ka {
    public ImageView A00;
    public TextView A01;
    public C15570nT A02;
    public C15450nH A03;
    public TextEmojiLabel A04;
    public C20670w8 A05;
    public C14650lo A06;
    public C246716k A07;
    public C15550nR A08;
    public AnonymousClass10S A09;
    public C22700zV A0A;
    public C15610nY A0B;
    public AnonymousClass131 A0C;
    public AnonymousClass018 A0D;
    public GetVNameCertificateJob A0E;
    public C17220qS A0F;
    public AbstractC14440lR A0G;
    public boolean A0H;
    public boolean A0I;

    @Override // X.AbstractC13940ka
    public void AR0() {
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
    }

    public CatalogHeader(Context context) {
        this(context, null);
    }

    public CatalogHeader(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CatalogHeader(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        A02(context, attributeSet);
    }

    @Override // X.AbstractC74163hQ
    public void A01() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A02 = C12970iu.A0S(A00);
            this.A0G = C12960it.A0T(A00);
            this.A03 = (C15450nH) A00.AII.get();
            this.A05 = (C20670w8) A00.AMw.get();
            this.A0F = C12990iw.A0c(A00);
            this.A08 = C12960it.A0O(A00);
            this.A0B = C12960it.A0P(A00);
            this.A0D = C12960it.A0R(A00);
            this.A09 = C12990iw.A0Z(A00);
            this.A0A = C12980iv.A0a(A00);
            this.A07 = (C246716k) A00.A2X.get();
            this.A06 = C12980iv.A0Y(A00);
            this.A0C = (AnonymousClass131) A00.A49.get();
        }
    }

    public float getAspectRatio() {
        return ((AspectRatioFrameLayout) this).A00;
    }

    public void setOnTextClickListener(AbstractView$OnClickListenerC34281fs r2) {
        TextView textView = this.A01;
        if (textView != null && !TextUtils.isEmpty(textView.getText())) {
            this.A01.setOnClickListener(r2);
        }
        TextEmojiLabel textEmojiLabel = this.A04;
        if (textEmojiLabel != null && !TextUtils.isEmpty(textEmojiLabel.getText())) {
            this.A04.setOnClickListener(r2);
        }
    }

    public void setUp(UserJid userJid) {
        String str;
        this.A00 = C12970iu.A0L(this, R.id.catalog_list_header_image);
        TextView A0J = C12960it.A0J(this, R.id.catalog_list_header_business_name);
        this.A01 = A0J;
        AnonymousClass028.A0l(A0J, true);
        if (!this.A02.A0F(userJid)) {
            AnonymousClass2GE.A04(AnonymousClass00T.A04(getContext(), R.drawable.chevron_right), -1);
            C42941w9.A0F(this.A01, this.A0D);
            TextView textView = this.A01;
            if (textView != null) {
                textView.setCompoundDrawablePadding(AnonymousClass3G9.A01(getContext(), 8.0f));
            }
        }
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.catalog_list_header_business_description);
        this.A04 = A0U;
        AnonymousClass028.A0l(A0U, true);
        AnonymousClass1M2 A00 = this.A0A.A00(userJid);
        if (A00 == null) {
            if (this.A0E == null) {
                GetVNameCertificateJob getVNameCertificateJob = new GetVNameCertificateJob(userJid);
                this.A0E = getVNameCertificateJob;
                this.A05.A00(getVNameCertificateJob);
            }
            str = null;
        } else {
            str = A00.A08;
        }
        C15370n3 A0B = this.A08.A0B(userJid);
        TextView textView2 = this.A01;
        if (textView2 != null) {
            if (AnonymousClass1US.A0C(str)) {
                str = this.A0B.A04(A0B);
            }
            textView2.setText(str);
        }
        this.A06.A03(new AbstractC30111Wd(userJid) { // from class: X.3Uo
            public final /* synthetic */ UserJid A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC30111Wd
            public final void ANP(C30141Wg r5) {
                CatalogHeader catalogHeader = CatalogHeader.this;
                UserJid userJid2 = this.A01;
                if (!catalogHeader.A0I) {
                    if (r5 == null) {
                        catalogHeader.A06.A04(catalogHeader, userJid2, null);
                        catalogHeader.A0I = true;
                        return;
                    }
                } else if (r5 == null) {
                    return;
                }
                TextEmojiLabel textEmojiLabel = catalogHeader.A04;
                if (textEmojiLabel != null) {
                    textEmojiLabel.A0G(null, r5.A09);
                }
            }
        }, userJid);
        C12960it.A1E(new AnonymousClass37B(this, this.A0C, A0B), this.A0G);
    }
}
