package com.whatsapp.biz.catalog.view.widgets;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass23N;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass5TT;
import X.AnonymousClass5TU;
import X.C016907y;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import X.C52452ap;
import X.EnumC870449z;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.biz.catalog.view.widgets.QuantitySelector;

/* loaded from: classes2.dex */
public class QuantitySelector extends FrameLayout implements AnonymousClass004 {
    public long A00;
    public long A01;
    public ValueAnimator A02;
    public ColorStateList A03;
    public AnonymousClass5TT A04;
    public AnonymousClass5TU A05;
    public EnumC870449z A06;
    public AnonymousClass01d A07;
    public AnonymousClass018 A08;
    public AnonymousClass2P7 A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final Handler A0D;
    public final TextView A0E;
    public final WaImageButton A0F;
    public final WaImageButton A0G;

    public QuantitySelector(Context context) {
        this(context, null);
    }

    public QuantitySelector(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public QuantitySelector(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A08 = C12960it.A0R(A00);
            this.A07 = C12960it.A0Q(A00);
        }
        this.A0D = C12970iu.A0E();
        this.A0A = false;
        this.A06 = EnumC870449z.COLLAPSED;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0I, 0, 0);
        try {
            setCollapsible(obtainStyledAttributes.getBoolean(0, false));
            obtainStyledAttributes.recycle();
            if (Build.VERSION.SDK_INT < 21) {
                int A002 = AnonymousClass00T.A00(getContext(), R.color.primary_button_color_selector);
                int A05 = C016907y.A05(AnonymousClass00T.A00(getContext(), R.color.buttonPressOverlay), A002);
                this.A03 = new ColorStateList(new int[][]{new int[]{16842919}, new int[]{16842910}, new int[]{16842908, 16842919}, new int[0]}, new int[]{A05, A002, A05, A002});
            }
            View inflate = FrameLayout.inflate(getContext(), R.layout.quantity_selector, this);
            this.A0E = C12960it.A0I(inflate, R.id.quantity_count);
            this.A0F = (WaImageButton) AnonymousClass028.A0D(inflate, R.id.minus_button);
            this.A0G = (WaImageButton) AnonymousClass028.A0D(inflate, R.id.plus_button);
            A04(0, 99);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final void A00() {
        int measuredWidth = this.A0G.getMeasuredWidth();
        int measuredWidth2 = (measuredWidth << 1) + this.A0E.getMeasuredWidth();
        ValueAnimator ofInt = ValueAnimator.ofInt(C12970iu.A1a(measuredWidth, measuredWidth2));
        this.A02 = ofInt;
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(measuredWidth, measuredWidth2) { // from class: X.3Js
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;

            {
                this.A00 = r2;
                this.A01 = r3;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                QuantitySelector quantitySelector = QuantitySelector.this;
                int i = this.A00;
                int i2 = this.A01;
                int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
                if (A05 == i) {
                    quantitySelector.A0A = true;
                }
                ViewGroup.LayoutParams layoutParams = quantitySelector.getLayoutParams();
                layoutParams.width = A05;
                quantitySelector.setLayoutParams(layoutParams);
                int measuredWidth3 = quantitySelector.A0G.getMeasuredWidth();
                TextView textView = quantitySelector.A0E;
                if (A05 > i + (textView.getMeasuredWidth() << 1) + (((measuredWidth3 - textView.getMeasuredWidth()) >> 1) >> 1)) {
                    quantitySelector.A02();
                }
                if (A05 == i2) {
                    quantitySelector.A0A = false;
                    quantitySelector.A02.removeAllUpdateListeners();
                }
            }
        });
        this.A02.setInterpolator(new DecelerateInterpolator());
        this.A02.setDuration(250L);
        this.A02.start();
    }

    public final void A01() {
        ColorStateList colorStateList;
        this.A06 = EnumC870449z.COLLAPSED;
        WaImageButton waImageButton = this.A0G;
        waImageButton.setImageResource(0);
        waImageButton.setBackgroundResource(R.drawable.btn_default);
        if (Build.VERSION.SDK_INT < 21 && (colorStateList = this.A03) != null) {
            AnonymousClass028.A0M(colorStateList, waImageButton);
        }
        C12960it.A0z(waImageButton, this, 2);
        C12980iv.A14(getResources(), this.A0E, R.color.primary_button_text_color_selector);
        A03();
    }

    public final void A02() {
        this.A06 = EnumC870449z.EXPANDED;
        A03();
        if (Build.VERSION.SDK_INT < 21) {
            AnonymousClass028.A0M(null, this.A0G);
        }
        C12980iv.A14(getResources(), this.A0E, R.color.text_color_primary);
        WaImageButton waImageButton = this.A0G;
        waImageButton.setImageResource(R.drawable.ic_add_control);
        waImageButton.setBackgroundResource(R.drawable.quantity_button_selector);
        C12960it.A0z(waImageButton, this, 1);
        C12960it.A0z(this.A0F, this, 3);
    }

    public final void A03() {
        TextView textView = this.A0E;
        textView.setClickable(false);
        if (this.A01 > 0) {
            textView.setText(this.A08.A0J().format(this.A01));
            if (this.A06 == EnumC870449z.EXPANDED) {
                textView.setClickable(true);
                return;
            }
            return;
        }
        C12990iw.A1G(textView);
    }

    public void A04(long j, long j2) {
        long j3 = this.A01;
        this.A00 = j2;
        this.A01 = j;
        if (this.A0B) {
            if (this.A06 == EnumC870449z.COLLAPSED || j3 == 0) {
                if (j > 0) {
                    A01();
                    return;
                }
            } else if (this.A0A) {
                return;
            }
        }
        A02();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A09;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A09 = r0;
        }
        return r0.generatedComponent();
    }

    public long getQuantity() {
        return this.A01;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int left;
        int top;
        int left2;
        int left3;
        super.onLayout(z, i, i2, i3, i4);
        WaImageButton waImageButton = this.A0G;
        int measuredWidth = waImageButton.getMeasuredWidth();
        TextView textView = this.A0E;
        int measuredWidth2 = (measuredWidth - textView.getMeasuredWidth()) >> 1;
        if (C28141Kv.A01(this.A08)) {
            int left4 = waImageButton.getLeft();
            WaImageButton waImageButton2 = this.A0F;
            if (left4 >= waImageButton2.getMeasuredWidth() - measuredWidth2) {
                left = waImageButton2.getRight();
                top = textView.getTop();
                left2 = waImageButton2.getRight();
                left3 = left2 + textView.getMeasuredWidth();
                textView.layout(left, top, left3, textView.getBottom());
            }
        } else {
            WaImageButton waImageButton3 = this.A0F;
            if (waImageButton3.getLeft() >= waImageButton3.getMeasuredWidth() - measuredWidth2) {
                left = waImageButton3.getLeft() - textView.getMeasuredWidth();
                top = textView.getTop();
                left3 = waImageButton3.getLeft();
                textView.layout(left, top, left3, textView.getBottom());
            }
        }
        left = waImageButton.getLeft() + measuredWidth2;
        top = textView.getTop();
        left2 = waImageButton.getLeft() + measuredWidth2;
        left3 = left2 + textView.getMeasuredWidth();
        textView.layout(left, top, left3, textView.getBottom());
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        WaImageButton waImageButton = this.A0G;
        ViewGroup.LayoutParams layoutParams = waImageButton.getLayoutParams();
        layoutParams.width = getMeasuredHeight();
        waImageButton.setLayoutParams(layoutParams);
        WaImageButton waImageButton2 = this.A0F;
        ViewGroup.LayoutParams layoutParams2 = waImageButton2.getLayoutParams();
        layoutParams2.width = getMeasuredHeight();
        waImageButton2.setLayoutParams(layoutParams2);
        super.onMeasure(i, i2);
        int measuredWidth = waImageButton.getMeasuredWidth();
        int measuredWidth2 = (measuredWidth << 1) + this.A0E.getMeasuredWidth();
        if (!this.A0A) {
            EnumC870449z r4 = this.A06;
            EnumC870449z r3 = EnumC870449z.EXPANDED;
            if (r4 == r3 && this.A01 > 0) {
                setMeasuredDimension(measuredWidth2, getMeasuredHeight());
                return;
            } else if ((r4 == r3 && this.A01 == 0) || r4 == EnumC870449z.COLLAPSED) {
                setMeasuredDimension(measuredWidth, getMeasuredHeight());
                return;
            }
        }
        super.onMeasure(i, i2);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C52452ap r5 = (C52452ap) parcelable;
        super.onRestoreInstanceState(r5.getSuperState());
        this.A0B = r5.A02;
        this.A06 = EnumC870449z.COLLAPSED;
        A04(r5.A01, r5.A00);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        C52452ap r2 = new C52452ap(super.onSaveInstanceState());
        r2.A01 = this.A01;
        r2.A00 = this.A00;
        r2.A02 = this.A0B;
        return r2;
    }

    public void setCollapsible(boolean z) {
        this.A0B = z;
        if (z && AnonymousClass23N.A05(this.A07.A0P())) {
            this.A0B = false;
        }
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.A0E.setEnabled(z);
        this.A0G.setEnabled(z);
        this.A0F.setEnabled(z);
    }

    public void setLimit(int i) {
        this.A00 = (long) i;
    }

    public void setOnLimitReachedListener(AnonymousClass5TT r1) {
        this.A04 = r1;
    }

    public void setOnQuantityChanged(AnonymousClass5TU r1) {
        this.A05 = r1;
    }

    public void setQuantity(long j) {
        A04(j, this.A00);
    }
}
