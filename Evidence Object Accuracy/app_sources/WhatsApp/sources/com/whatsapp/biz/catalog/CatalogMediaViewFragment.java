package com.whatsapp.biz.catalog;

import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass19N;
import X.AnonymousClass19Q;
import X.AnonymousClass3S3;
import X.AnonymousClass3YN;
import X.C15890o4;
import X.C252918v;
import X.C37071lG;
import X.C44691zO;
import X.C58362oh;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* loaded from: classes2.dex */
public class CatalogMediaViewFragment extends Hilt_CatalogMediaViewFragment {
    public int A00;
    public AnonymousClass12P A01;
    public C44691zO A02;
    public AnonymousClass19Q A03;
    public C252918v A04;
    public C37071lG A05;
    public C15890o4 A06;
    public UserJid A07;
    public String A08;

    @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
    public void A11() {
        this.A05.A00();
        super.A11();
    }

    @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            this.A05 = new C37071lG(this.A04);
            this.A07 = UserJid.getNullable(bundle2.getString("cached_jid"));
            this.A02 = (C44691zO) bundle2.getParcelable("product");
            this.A00 = bundle2.getInt("target_image_index", 0);
            C58362oh r1 = new C58362oh(new AnonymousClass3YN(this), this);
            ((MediaViewBaseFragment) this).A08 = r1;
            ((MediaViewBaseFragment) this).A09.setAdapter(r1);
            ((MediaViewBaseFragment) this).A09.A0F(0, false);
            ((MediaViewBaseFragment) this).A09.A0F(this.A00, false);
            ((MediaViewBaseFragment) this).A09.A0G(new AnonymousClass3S3(this));
        }
    }

    @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        if (bundle == null) {
            this.A08 = AnonymousClass19N.A00(this.A00, this.A02.A0D);
            Bundle bundle2 = ((MediaViewBaseFragment) this).A01;
            if (bundle2 != null) {
                ((MediaViewBaseFragment) this).A0E = true;
                ((MediaViewBaseFragment) this).A0B.A0C(bundle2, this);
            }
            this.A03.A03(this.A07, 29, this.A02.A0D, 10);
        }
        ((MediaViewBaseFragment) this).A02.setVisibility(8);
        AnonymousClass028.A0D(view, R.id.title_holder).setClickable(false);
    }
}
