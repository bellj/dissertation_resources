package com.whatsapp.biz.catalog.view;

import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2E5;
import X.AnonymousClass4RR;
import X.AnonymousClass4SX;
import X.AnonymousClass5TN;
import X.C12960it;
import X.C12990iw;
import X.C42941w9;
import X.C53272db;
import X.C59352uY;
import X.C90154Mu;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.InfoCard;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class CategoryMediaCard extends InfoCard {
    public HorizontalScrollView A00;
    public LinearLayout A01;
    public AnonymousClass018 A02;
    public boolean A03;

    public CategoryMediaCard(Context context) {
        this(context, null);
    }

    public CategoryMediaCard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CategoryMediaCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        C12960it.A0E(this).inflate(R.layout.category_media_card, (ViewGroup) this, true);
        this.A01 = (LinearLayout) AnonymousClass028.A0D(this, R.id.media_card_thumbs);
        this.A00 = (HorizontalScrollView) AnonymousClass028.A0D(this, R.id.media_card_scroller);
    }

    public final C53272db A03(AnonymousClass4RR r11) {
        C53272db r3 = new C53272db(getContext());
        ThumbnailButton thumbnailButton = (ThumbnailButton) AnonymousClass028.A0D(r3, R.id.category_thumbnail_image);
        C12990iw.A1E(thumbnailButton);
        thumbnailButton.A02 = getResources().getDimension(R.dimen.catalog_item_image_radius);
        AnonymousClass028.A0k(thumbnailButton, null);
        String str = r11.A03;
        if (str != null) {
            r3.setText(str);
        }
        Drawable drawable = r11.A00;
        if (drawable != null) {
            thumbnailButton.setImageDrawable(drawable);
        }
        C12960it.A10(r3, r11, 44);
        C90154Mu r0 = r11.A02;
        if (r0 != null) {
            C59352uY r2 = r0.A01;
            AnonymousClass4SX r1 = r0.A00;
            thumbnailButton.setTag(r1.A01);
            r2.A02.A02(thumbnailButton, r1.A00, new AnonymousClass5TN() { // from class: X.53f
                @Override // X.AnonymousClass5TN
                public final void AML(C68203Um r22) {
                    AnonymousClass4Dz.A00(ThumbnailButton.this);
                }
            }, new AnonymousClass2E5() { // from class: X.53n
                @Override // X.AnonymousClass2E5
                public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                    ThumbnailButton thumbnailButton2 = ThumbnailButton.this;
                    thumbnailButton2.setBackgroundColor(0);
                    thumbnailButton2.setImageBitmap(bitmap);
                    thumbnailButton2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            }, 2);
        }
        return r3;
    }

    public void setError(String str) {
        this.A01.setVisibility(8);
    }

    public void setup(List list, AnonymousClass4RR r6) {
        HorizontalScrollView horizontalScrollView;
        int i;
        if (list.size() == 0) {
            horizontalScrollView = this.A00;
            i = 8;
        } else {
            LinearLayout linearLayout = this.A01;
            linearLayout.removeAllViews();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                linearLayout.addView(A03((AnonymousClass4RR) it.next()));
            }
            if (r6 != null) {
                C53272db A03 = A03(r6);
                AnonymousClass028.A0D(A03, R.id.category_thumbnail_text_bg).setVisibility(8);
                linearLayout.addView(A03);
            }
            C42941w9.A0E(linearLayout, this.A02);
            AnonymousClass018 r0 = this.A02;
            horizontalScrollView = this.A00;
            C42941w9.A0D(horizontalScrollView, r0);
            i = 0;
        }
        horizontalScrollView.setVisibility(i);
    }
}
