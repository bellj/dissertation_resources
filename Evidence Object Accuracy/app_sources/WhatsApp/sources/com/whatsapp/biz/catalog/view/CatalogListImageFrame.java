package com.whatsapp.biz.catalog.view;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12980iv;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CatalogListImageFrame extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final Drawable A02;
    public final Drawable A03;

    public CatalogListImageFrame(Context context) {
        this(context, null);
    }

    public CatalogListImageFrame(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CatalogListImageFrame(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = getResources().getDrawable(R.drawable.album_card_top);
        this.A02 = getResources().getDrawable(R.drawable.album_card_bottom);
        setWillNotDraw(false);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        Drawable drawable = this.A03;
        drawable.setBounds(0, getPaddingTop() - drawable.getIntrinsicHeight(), getWidth(), getPaddingTop());
        drawable.draw(canvas);
        Drawable drawable2 = this.A02;
        drawable2.setBounds(0, C12980iv.A07(this), getWidth(), C12980iv.A07(this) + drawable2.getIntrinsicHeight());
        drawable2.draw(canvas);
        super.onDraw(canvas);
    }
}
