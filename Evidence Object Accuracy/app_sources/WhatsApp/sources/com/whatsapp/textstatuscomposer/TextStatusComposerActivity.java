package com.whatsapp.textstatuscomposer;

import X.AbstractC116455Vm;
import X.AbstractC13940ka;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC253919f;
import X.AbstractC33441e5;
import X.AbstractC33451e6;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1AD;
import X.AnonymousClass1BP;
import X.AnonymousClass1VX;
import X.AnonymousClass24D;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2G4;
import X.AnonymousClass2GF;
import X.AnonymousClass2GS;
import X.AnonymousClass2IU;
import X.C103634r4;
import X.C1097052s;
import X.C14310lE;
import X.C14320lF;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15260mp;
import X.C15320mw;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22640zP;
import X.C22670zS;
import X.C231510o;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253719d;
import X.C32731ce;
import X.C42641vY;
import X.C49042Ix;
import X.C63763Cv;
import X.C63893Di;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.status.playback.widget.StatusEditText;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public class TextStatusComposerActivity extends ActivityC13790kL implements AbstractC13940ka, AbstractC33441e5, AbstractC33451e6 {
    public static final int[] A0k = {R.string.color_name_medium_red_violet, R.string.color_name_wasabi, R.string.color_name_sundance, R.string.color_name_scarlett, R.string.color_name_brandy_rose, R.string.color_name_bright_sun, R.string.color_name_earls_green, R.string.color_name_prelude, R.string.color_name_ce_soir, R.string.color_name_mona_lisa, R.string.color_name_emerald, R.string.color_name_bittersweet, R.string.color_name_summer_sky, R.string.color_name_maya_blue, R.string.color_name_scorpion, R.string.color_name_bali_hai, R.string.color_name_maya_blue_1, R.string.color_name_vivid_violet, R.string.color_name_monte_carlo, R.string.color_name_elephant, R.string.color_name_polo_blue};
    public static final int[] A0l = {R.string.font_name_sans_serif, R.string.font_name_serif, R.string.font_name_bryndan_write, R.string.font_name_norican, R.string.font_name_oswald};
    public int A00;
    public int A01;
    public DisplayMetrics A02;
    public View A03;
    public ViewGroup A04;
    public ImageButton A05;
    public ImageButton A06;
    public ScrollView A07;
    public TextView A08;
    public AnonymousClass016 A09;
    public AnonymousClass2IU A0A;
    public C49042Ix A0B;
    public C16170oZ A0C;
    public C14320lF A0D;
    public C22640zP A0E;
    public C15610nY A0F;
    public C14310lE A0G;
    public AnonymousClass1AD A0H;
    public C32731ce A0I;
    public C18470sV A0J;
    public C231510o A0K;
    public C16120oU A0L;
    public AnonymousClass1BP A0M;
    public C15260mp A0N;
    public C15320mw A0O;
    public C253719d A0P;
    public AbstractC253919f A0Q;
    public C63763Cv A0R;
    public C16630pM A0S;
    public StatusEditText A0T;
    public AnonymousClass2GS A0U;
    public WebPagePreviewView A0V;
    public AnonymousClass01H A0W;
    public String A0X;
    public List A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public final Handler A0g;
    public final AbstractC116455Vm A0h;
    public final C63893Di A0i;
    public final int[] A0j;

    @Override // X.AbstractC13940ka
    public void AR0() {
    }

    public TextStatusComposerActivity() {
        this(0);
        int[] iArr = AnonymousClass24D.A01;
        this.A00 = iArr[Math.abs(AnonymousClass24D.A00.nextInt()) % iArr.length];
        this.A01 = 0;
        this.A0g = new Handler(Looper.getMainLooper());
        this.A0Y = Collections.singletonList(AnonymousClass1VX.A00);
        this.A09 = new AnonymousClass016(0);
        this.A0h = new C1097052s(this);
        this.A0i = new C63893Di(this);
        this.A0j = new int[2];
    }

    public TextStatusComposerActivity(int i) {
        this.A0Z = false;
        A0R(new C103634r4(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0Z) {
            this.A0Z = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A0P = (C253719d) r2.A8V.get();
            this.A0L = (C16120oU) r2.ANE.get();
            this.A0J = (C18470sV) r2.AK8.get();
            this.A0C = (C16170oZ) r2.AM4.get();
            this.A0K = (C231510o) r2.AHO.get();
            this.A0F = (C15610nY) r2.AMe.get();
            this.A0Q = (AbstractC253919f) r2.AGb.get();
            this.A0E = (C22640zP) r2.A3Z.get();
            this.A0H = (AnonymousClass1AD) r2.A54.get();
            this.A0S = (C16630pM) r2.AIc.get();
            this.A0M = r1.A07();
            this.A0A = (AnonymousClass2IU) r1.A1A.get();
            this.A0W = C18000rk.A00(r2.A4v);
            this.A0B = (C49042Ix) r1.A0i.get();
        }
    }

    public final void A2e() {
        if (this.A0T.getText() == null || this.A0T.getText().length() <= 0) {
            super.onBackPressed();
        } else {
            Adl(new DiscardTextWarningDialogFragment(), null);
        }
    }

    public final void A2f() {
        if (this.A04.getVisibility() == 0) {
            boolean z = this.A0c;
            ViewGroup viewGroup = this.A04;
            if (!z) {
                viewGroup.setVisibility(8);
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 2.0f);
                translateAnimation.setDuration(160);
                this.A04.startAnimation(translateAnimation);
                return;
            }
            this.A04.animate().translationY((float) viewGroup.getHeight()).alpha(0.0f).setDuration((long) 200).setListener(new AnonymousClass2G4(this, 8));
        }
    }

    public final void A2g() {
        int length;
        int i = this.A00;
        int[] iArr = AnonymousClass24D.A01;
        int i2 = 0;
        while (true) {
            length = iArr.length;
            if (i2 < length) {
                if (i == iArr[i2]) {
                    break;
                }
                i2++;
            } else {
                i2 = -1;
                break;
            }
        }
        this.A00 = iArr[(i2 + 1) % length];
        getWindow().setBackgroundDrawable(new ColorDrawable(this.A00));
        A2i();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007c, code lost:
        if (X.AnonymousClass24D.A09(r26.A0D, X.AnonymousClass24D.A01(r4)) == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0119, code lost:
        if (r4 != false) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011f, code lost:
        if (r4 == false) goto L_0x0121;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2h() {
        /*
        // Method dump skipped, instructions count: 328
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.textstatuscomposer.TextStatusComposerActivity.A2h():void");
    }

    public final void A2i() {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{(this.A00 & 16777215) | -436207616, 0});
        gradientDrawable.setCornerRadius(0.0f);
        View view = this.A03;
        if (view != null) {
            view.setBackground(gradientDrawable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d5, code lost:
        if (r12 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f4, code lost:
        if (r3 == null) goto L_0x00f6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2j(X.C14320lF r12, int r13) {
        /*
        // Method dump skipped, instructions count: 588
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.textstatuscomposer.TextStatusComposerActivity.A2j(X.0lF, int):void");
    }

    public final void A2k(boolean z) {
        C42641vY r2 = new C42641vY(this);
        r2.A0D = true;
        r2.A0F = true;
        r2.A0S = this.A0Y;
        Byte b = (byte) 0;
        r2.A0R = new ArrayList(Collections.singleton(Integer.valueOf(b.intValue())));
        r2.A0G = Boolean.valueOf(z);
        r2.A01 = this.A0I;
        startActivityForResult(r2.A00(), 2);
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(this, 42));
    }

    @Override // X.AbstractC33441e5
    public void AUk(boolean z) {
        this.A0a = true;
        A2k(z);
    }

    @Override // X.AbstractC33451e6
    public void AWY() {
        A2h();
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.A0O.A01()) {
            View view = this.A03;
            int[] iArr = this.A0j;
            view.getLocationOnScreen(iArr);
            if (motionEvent.getRawY() >= ((float) iArr[1]) && motionEvent.getRawY() < ((float) (iArr[1] + this.A03.getHeight()))) {
                if (motionEvent.getAction() == 0) {
                    this.A0f = true;
                } else if (motionEvent.getAction() == 1 && this.A0f) {
                    this.A0O.A00(true);
                    this.A0f = false;
                    return false;
                }
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        boolean z = true;
        if (i != 1) {
            if (i == 2) {
                if (this.A0d) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("status_distribution");
                    AnonymousClass009.A05(parcelableExtra);
                    this.A0I = (C32731ce) parcelableExtra;
                }
                if (this.A0b) {
                    List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                    this.A0Y = A07;
                    this.A0R.A00(this.A0F, this.A0I, A07, C15380n4.A0P(A07), false);
                    int size = this.A0Y.size();
                    int i3 = R.drawable.input_send;
                    if (size <= 0) {
                        z = false;
                        i3 = R.drawable.ic_done;
                    }
                    this.A06.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, i3), ((ActivityC13830kP) this).A01));
                    int i4 = R.string.done;
                    if (z) {
                        i4 = R.string.send;
                    }
                    this.A06.setContentDescription(getString(i4));
                }
                if (i2 == -1) {
                    A2h();
                }
            }
        } else if (i2 == -1) {
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!this.A0O.A02()) {
            A2e();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0059, code lost:
        if (((X.ActivityC13810kN) r31).A0C.A07(1267) == false) goto L_0x005b;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r32) {
        /*
        // Method dump skipped, instructions count: 976
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.textstatuscomposer.TextStatusComposerActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C15260mp r0 = this.A0N;
        if (r0 != null) {
            r0.A0G();
        }
        AnonymousClass2GS r2 = this.A0U;
        StatusEditText statusEditText = r2.A0G;
        statusEditText.removeTextChangedListener(r2.A02);
        statusEditText.setFilters(new InputFilter[0]);
        r2.A02 = null;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!keyEvent.isPrintingKey() || !this.A0T.isShown() || this.A0T.hasFocus()) {
            return super.onKeyDown(i, keyEvent);
        }
        this.A0T.requestFocus();
        dispatchKeyEvent(keyEvent);
        return true;
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if ((i != 82 && i != 4) || !this.A0N.isShowing()) {
            return super.onKeyUp(i, keyEvent);
        }
        this.A0N.dismiss();
        return false;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("background_color", this.A00);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (this.A0e) {
            Window window = getWindow();
            int i = 4;
            if (this.A0N.isShowing()) {
                i = 2;
            }
            window.setSoftInputMode(i | 1);
            if (!this.A0N.isShowing()) {
                this.A0T.A04(true);
            }
        }
    }
}
