package com.whatsapp.textstatuscomposer;

import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class DiscardTextWarningDialogFragment extends Hilt_DiscardTextWarningDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e r2 = new C004802e(A01());
        r2.A06(R.string.text_status_composer_exit_dialog_description);
        C12970iu.A1K(r2, this, 72, R.string.cancel);
        C12970iu.A1L(r2, this, 73, R.string.text_status_composer_exit_dialog_discard);
        return r2.create();
    }
}
