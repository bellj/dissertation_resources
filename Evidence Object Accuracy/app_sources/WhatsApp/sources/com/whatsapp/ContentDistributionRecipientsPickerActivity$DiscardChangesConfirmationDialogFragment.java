package com.whatsapp;

import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;

/* loaded from: classes3.dex */
public class ContentDistributionRecipientsPickerActivity$DiscardChangesConfirmationDialogFragment extends Hilt_ContentDistributionRecipientsPickerActivity_DiscardChangesConfirmationDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(this, 0);
        C004802e A0O = C12970iu.A0O(this);
        A0O.A06(R.string.discard_changes);
        A0O.setPositiveButton(R.string.discard_status_privacy_changes, iDxCListenerShape9S0100000_2_I1);
        A0O.setNegativeButton(R.string.cancel_discarding_status_privacy_changes, null);
        return A0O.create();
    }
}
