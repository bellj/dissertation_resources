package com.whatsapp.userban.ui.fragment;

import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.AnonymousClass18U;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14900mE;
import X.C252818u;
import X.C58272oQ;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;

/* loaded from: classes2.dex */
public class BanAppealFormSubmittedFragment extends Hilt_BanAppealFormSubmittedFragment {
    public C14900mE A00;
    public AnonymousClass18U A01;
    public C252818u A02;
    public AnonymousClass01d A03;
    public BanAppealViewModel A04;

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(0, 1, 0, R.string.register_new_number).setShowAsAction(0);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 1) {
            this.A04.A07(A0C(), false);
            return true;
        } else if (itemId != 16908332) {
            return false;
        } else {
            this.A04.A0A.A0B(Boolean.TRUE);
            return true;
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        A0M();
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.ban_appeal_form_submitted_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A04 = C12970iu.A0e(this);
        BanAppealViewModel.A00(A0C(), true);
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.heading);
        AbstractC28491Nn.A03(A0T);
        AbstractC28491Nn.A04(A0T, this.A03);
        SpannableStringBuilder A0J = C12990iw.A0J(Html.fromHtml(C12970iu.A0q(this, this.A02.A00("https://www.whatsapp.com/legal/terms-of-service#terms-of-service-acceptable-use-of-our-services").toString(), new Object[1], 0, R.string.ban_appeal_form_message)));
        URLSpan[] uRLSpanArr = (URLSpan[]) A0J.getSpans(0, A0J.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                A0J.setSpan(new C58272oQ(A0p(), this.A01, this.A00, this.A03, uRLSpan.getURL()), A0J.getSpanStart(uRLSpan), A0J.getSpanEnd(uRLSpan), A0J.getSpanFlags(uRLSpan));
                A0J.removeSpan(uRLSpan);
            }
        }
        A0T.setText(A0J);
    }
}
