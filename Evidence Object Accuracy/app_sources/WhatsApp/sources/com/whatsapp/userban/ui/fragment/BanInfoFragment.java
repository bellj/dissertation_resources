package com.whatsapp.userban.ui.fragment;

import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C88104Eh;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BanInfoFragment extends Hilt_BanInfoFragment {
    public Button A00;
    public BanAppealViewModel A01;

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(0, 1, 0, R.string.register_new_number).setShowAsAction(0);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != 1) {
            return false;
        }
        this.A01.A07(A0C(), false);
        return true;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        A0M();
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.ban_info_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A01 = C12970iu.A0e(this);
        BanAppealViewModel.A00(A0C(), false);
        C12970iu.A0K(view, R.id.ban_icon).setImageDrawable(A02().getDrawable(R.drawable.icon_banned));
        TextView A0I = C12960it.A0I(view, R.id.heading);
        int i = this.A01.A09.A04.A00.getInt("support_ban_appeal_violation_type", 0);
        Log.i(C12960it.A0W(i, "BanAppealRepository/getBanViolationType "));
        int i2 = R.string.ban_info_heading_other;
        if (i == 15) {
            i2 = R.string.ban_info_heading_spam;
        }
        A0I.setText(i2);
        C12960it.A0I(view, R.id.sub_heading).setText(R.string.ban_info_message_chats_on_device);
        this.A00 = (Button) AnonymousClass028.A0D(view, R.id.action_button);
        boolean equals = C88104Eh.A00(C12980iv.A0p(this.A01.A09.A04.A00, "support_ban_appeal_state")).equals("IN_REVIEW");
        Button button = this.A00;
        int i3 = R.string.ban_info_request_review_button;
        if (equals) {
            i3 = R.string.ban_info_see_review_button;
        }
        button.setText(i3);
        C12960it.A11(this.A00, this, 43);
    }
}
