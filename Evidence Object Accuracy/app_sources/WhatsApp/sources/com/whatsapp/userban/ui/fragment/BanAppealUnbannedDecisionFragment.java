package com.whatsapp.userban.ui.fragment;

import X.AnonymousClass01d;
import X.C12960it;
import X.C253118x;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;

/* loaded from: classes2.dex */
public class BanAppealUnbannedDecisionFragment extends Hilt_BanAppealUnbannedDecisionFragment {
    public AnonymousClass01d A00;
    public BanAppealViewModel A01;
    public C253118x A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.ban_info_fragment);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:21:0x007e */
    /* JADX DEBUG: Multi-variable search result rejected for r5v2, resolved type: X.18m */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v10, types: [android.util.Pair, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00e3  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r14, android.view.View r15) {
        /*
        // Method dump skipped, instructions count: 287
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.userban.ui.fragment.BanAppealUnbannedDecisionFragment.A17(android.os.Bundle, android.view.View):void");
    }
}
