package com.whatsapp.userban.ui.fragment;

import X.AbstractC28491Nn;
import X.ActivityC001000l;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass18U;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C18340sI;
import X.C252718t;
import X.C252818u;
import X.C58272oQ;
import X.C74193hX;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BanAppealFormFragment extends Hilt_BanAppealFormFragment {
    public EditText A00;
    public C14900mE A01;
    public AnonymousClass18U A02;
    public C252818u A03;
    public AnonymousClass01d A04;
    public BanAppealViewModel A05;
    public C252718t A06;

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        String A0p = C12970iu.A0p(this.A00);
        C18340sI r1 = this.A05.A09;
        Log.i("BanAppealRepository/storeFormReviewDraft");
        C12970iu.A1D(C12960it.A08(r1.A04), "support_ban_appeal_form_review_draft", A0p);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        this.A05.A06();
        return true;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        A0M();
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.ban_appeal_form_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        C18340sI r1 = this.A05.A09;
        Log.i("BanAppealRepository/getFormReviewDraft");
        String A0p = C12980iv.A0p(r1.A04.A00, "support_ban_appeal_form_review_draft");
        if (A0p != null) {
            this.A00.setText(A0p);
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A05 = C12970iu.A0e(this);
        BanAppealViewModel.A00(A0C(), true);
        this.A00 = (EditText) AnonymousClass028.A0D(view, R.id.form_appeal_reason);
        C12960it.A11(AnonymousClass028.A0D(view, R.id.submit_button), this, 41);
        C12970iu.A1P(A0C(), this.A05.A02, this, 61);
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.heading);
        AbstractC28491Nn.A03(A0T);
        AbstractC28491Nn.A04(A0T, this.A04);
        SpannableStringBuilder A0J = C12990iw.A0J(Html.fromHtml(C12970iu.A0q(this, this.A03.A00("https://www.whatsapp.com/legal/terms-of-service#terms-of-service-acceptable-use-of-our-services").toString(), new Object[1], 0, R.string.ban_appeal_form_message)));
        URLSpan[] uRLSpanArr = (URLSpan[]) A0J.getSpans(0, A0J.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                A0J.setSpan(new C58272oQ(A0p(), this.A02, this.A01, this.A04, uRLSpan.getURL()), A0J.getSpanStart(uRLSpan), A0J.getSpanEnd(uRLSpan), A0J.getSpanFlags(uRLSpan));
                A0J.removeSpan(uRLSpan);
            }
        }
        A0T.setText(A0J);
        ((ActivityC001000l) A0C()).A04.A01(new C74193hX(this), A0G());
    }
}
