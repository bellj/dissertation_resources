package com.whatsapp.userban.ui.viewmodel;

import X.AbstractC005102i;
import X.ActivityC000800j;
import X.AnonymousClass009;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass18L;
import X.AnonymousClass19Y;
import X.AnonymousClass32H;
import X.C14820m6;
import X.C14850m9;
import X.C14960mK;
import X.C15510nN;
import X.C18000rk;
import X.C18340sI;
import X.C18350sJ;
import X.C18360sK;
import X.C18790t3;
import X.C252018m;
import X.C252818u;
import X.C27691It;
import X.C70043ae;
import X.C88104Eh;
import android.app.Activity;
import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BanAppealViewModel extends AnonymousClass015 {
    public int A00;
    public final AnonymousClass016 A01 = new AnonymousClass016();
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final AnonymousClass19Y A03;
    public final C252818u A04;
    public final C18360sK A05;
    public final C18350sJ A06;
    public final C15510nN A07;
    public final C252018m A08;
    public final C18340sI A09;
    public final C27691It A0A = new C27691It();
    public final C27691It A0B = new C27691It();

    public BanAppealViewModel(AnonymousClass19Y r2, C252818u r3, C18360sK r4, C18350sJ r5, C15510nN r6, C252018m r7, C18340sI r8) {
        this.A03 = r2;
        this.A04 = r3;
        this.A08 = r7;
        this.A09 = r8;
        this.A06 = r5;
        this.A05 = r4;
        this.A07 = r6;
    }

    public static void A00(Activity activity, boolean z) {
        AnonymousClass009.A05(activity);
        AbstractC005102i A1U = ((ActivityC000800j) activity).A1U();
        if (A1U != null) {
            A1U.A0M(z);
            int i = R.string.localized_app_name;
            if (z) {
                i = R.string.ban_appeal_review_screen_title;
            }
            A1U.A0A(i);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A04(java.lang.String r4, boolean r5) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            r1 = 4
            switch(r0) {
                case -358171056: goto L_0x0034;
                case 272787191: goto L_0x002b;
                case 527514546: goto L_0x001f;
                case 1166090011: goto L_0x001c;
                case 1951953694: goto L_0x0054;
                default: goto L_0x0008;
            }
        L_0x0008:
            java.lang.String r1 = "Invalid BanAppealState: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r4)
            java.lang.String r1 = r0.toString()
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>(r1)
            throw r0
        L_0x001c:
            java.lang.String r0 = "NO_APPEAL_OPENED"
            goto L_0x0036
        L_0x001f:
            java.lang.String r0 = "IN_REVIEW"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0008
            r1 = 3
            if (r5 != 0) goto L_0x005d
            goto L_0x0052
        L_0x002b:
            java.lang.String r0 = "UNBANNED"
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x005d
            goto L_0x0008
        L_0x0034:
            java.lang.String r0 = "UNKNOWN_IN_CLIENT"
        L_0x0036:
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0008
            int r1 = r3.A00
            r0 = 2
            if (r1 != r0) goto L_0x0052
            X.0sI r0 = r3.A09
            X.0m6 r0 = r0.A04
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "support_ban_appeal_user_banned_from_chat_disconnect"
            r0 = 0
            boolean r0 = r2.getBoolean(r1, r0)
            r1 = 2
            if (r0 != 0) goto L_0x005d
        L_0x0052:
            r1 = 1
            return r1
        L_0x0054:
            java.lang.String r0 = "BANNED"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0008
            r1 = 5
        L_0x005d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.userban.ui.viewmodel.BanAppealViewModel.A04(java.lang.String, boolean):int");
    }

    public void A05() {
        Log.i("BanAppealViewModel/fetchBanAppealStatus");
        C18340sI r4 = this.A09;
        SharedPreferences sharedPreferences = r4.A04.A00;
        this.A0B.A0B(Integer.valueOf(A04(C88104Eh.A00(sharedPreferences.getString("support_ban_appeal_state", null)), false)));
        int A00 = this.A07.A00();
        StringBuilder sb = new StringBuilder("BanAppealViewModel/verifyIfPhoneNumberIsVerified reg_state: ");
        sb.append(A00);
        Log.i(sb.toString());
        if (A00 == 10) {
            Log.i("BanAppealViewModel/fetchBanAppealStatus trying to fetch ban appeal status");
            C70043ae r3 = new C70043ae(this);
            String string = sharedPreferences.getString("support_ban_appeal_token", null);
            if (string == null) {
                r3.AQB(3);
                return;
            }
            AnonymousClass01J r1 = r4.A01.A00.A01;
            r4.A06.Ab2(new RunnableBRunnable0Shape2S0300000_I0_2(r4, new AnonymousClass32H((C18790t3) r1.AJw.get(), (C14820m6) r1.AN3.get(), (C14850m9) r1.A04.get(), (AnonymousClass18L) r1.A89.get(), C18000rk.A00(r1.AMu), string, r1.A7q, r1.A1J), r3, 1));
            return;
        }
        Log.i("BanAppealViewModel/fetchBanAppealStatus returning since phone number not verified yet");
    }

    public void A06() {
        if (this.A00 != 2 || !this.A09.A04.A00.getBoolean("support_ban_appeal_user_banned_from_chat_disconnect", false)) {
            this.A0A.A0B(Boolean.TRUE);
        } else {
            this.A0B.A0B(1);
        }
    }

    public void A07(Activity activity, boolean z) {
        this.A05.A04(42, null);
        this.A06.A01();
        SharedPreferences sharedPreferences = this.A09.A04.A00;
        sharedPreferences.edit().remove("support_ban_appeal_state").apply();
        sharedPreferences.edit().remove("support_ban_appeal_token").apply();
        sharedPreferences.edit().remove("support_ban_appeal_violation_type").apply();
        sharedPreferences.edit().remove("support_ban_appeal_unban_reason").apply();
        sharedPreferences.edit().remove("support_ban_appeal_unban_reason_url").apply();
        if (!z) {
            sharedPreferences.edit().remove("support_ban_appeal_user_banned_from_chat_disconnect").apply();
        }
        Log.i("BanAppealRepository/clearFormReviewDraft");
        sharedPreferences.edit().remove("support_ban_appeal_form_review_draft").apply();
        activity.startActivity(C14960mK.A01(activity));
        activity.finishAffinity();
    }
}
