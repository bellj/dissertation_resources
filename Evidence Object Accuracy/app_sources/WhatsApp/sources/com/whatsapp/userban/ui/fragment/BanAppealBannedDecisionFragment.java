package com.whatsapp.userban.ui.fragment;

import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.C12960it;
import X.C12970iu;
import X.C253118x;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0000000_I1;
import com.whatsapp.R;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;

/* loaded from: classes2.dex */
public class BanAppealBannedDecisionFragment extends Hilt_BanAppealBannedDecisionFragment {
    public AnonymousClass01d A00;
    public BanAppealViewModel A01;
    public C253118x A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.ban_info_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A01 = C12970iu.A0e(this);
        BanAppealViewModel.A00(A0C(), false);
        C12970iu.A0K(view, R.id.ban_icon).setImageDrawable(A02().getDrawable(R.drawable.icon_banned));
        C12960it.A0I(view, R.id.heading).setText(R.string.ban_decision_heading);
        AbstractC28491Nn.A05(C12970iu.A0T(view, R.id.sub_heading), this.A00, this.A02.A01(A01(), A0I(R.string.ban_decision_message), new Runnable[]{new RunnableBRunnable0Shape1S0000000_I1()}, new String[]{"terms-of-service-link"}, new String[]{"https://www.whatsapp.com/legal/updates/terms-of-service"}));
        TextView A0I = C12960it.A0I(view, R.id.action_button);
        A0I.setText(R.string.ban_decision_register_button);
        C12960it.A11(A0I, this, 40);
    }
}
