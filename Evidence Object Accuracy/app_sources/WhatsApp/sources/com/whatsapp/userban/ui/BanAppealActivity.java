package com.whatsapp.userban.ui;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C18340sI;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BanAppealActivity extends ActivityC13790kL {
    public BanAppealViewModel A00;
    public String A01;
    public boolean A02;

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public BanAppealActivity() {
        this(0);
    }

    public BanAppealActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 131);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.ban_appeal_activity);
        this.A00 = (BanAppealViewModel) C13000ix.A02(this).A00(BanAppealViewModel.class);
        String stringExtra = getIntent().getStringExtra("appeal_request_token");
        int intExtra = getIntent().getIntExtra("ban_violation_type", -1);
        int intExtra2 = getIntent().getIntExtra("launch_source", 0);
        BanAppealViewModel banAppealViewModel = this.A00;
        if (stringExtra != null) {
            C12970iu.A1D(C12960it.A08(banAppealViewModel.A09.A04), "support_ban_appeal_token", stringExtra);
        }
        if (intExtra >= 0) {
            C18340sI r1 = banAppealViewModel.A09;
            Log.i(C12960it.A0W(intExtra, "BanAppealRepository/storeBanViolationType "));
            C12970iu.A1B(C12960it.A08(r1.A04), "support_ban_appeal_violation_type", intExtra);
        }
        banAppealViewModel.A00 = intExtra2;
        if (bundle == null) {
            this.A00.A05();
        } else {
            this.A01 = bundle.getString("first_fragment_tag_save_instance_state");
        }
        C12960it.A18(this, this.A00.A0B, 95);
        C12960it.A18(this, this.A00.A01, 94);
        C12960it.A17(this, this.A00.A0A, 60);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("BanAppealActivity/onNewIntent");
        BanAppealViewModel banAppealViewModel = this.A00;
        if (banAppealViewModel.A00 == 4) {
            banAppealViewModel.A05();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("first_fragment_tag_save_instance_state", this.A01);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A00.A05.A04(42, null);
    }
}
