package com.whatsapp.crash.upload;

import X.AbstractServiceC003701q;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass180;
import X.AnonymousClass1DR;
import X.AnonymousClass1DS;
import X.AnonymousClass1DT;
import X.AnonymousClass1DU;
import X.AnonymousClass1DV;
import X.AnonymousClass5B7;
import X.C58182oH;
import X.C71083cM;

/* loaded from: classes2.dex */
public class ExceptionsUploadService extends AbstractServiceC003701q implements AnonymousClass004 {
    public AnonymousClass1DT A00;
    public AnonymousClass180 A01;
    public AnonymousClass1DR A02;
    public AnonymousClass1DS A03;
    public AnonymousClass1DV A04;
    public AnonymousClass1DU A05;
    public boolean A06;
    public final Object A07;
    public volatile C71083cM A08;

    public ExceptionsUploadService() {
        this(0);
    }

    public ExceptionsUploadService(int i) {
        this.A07 = new Object();
        this.A06 = false;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't wrap try/catch for region: R(24:2|(41:247|4|262|5|6|256|7|9|(2:11|(2:13|(2:258|15)))|16|17|18|23|(1:27)|28|250|29|30|31|37|(1:39)|40|41|260|42|(1:47)(1:46)|48|(2:(7:254|52|53|54|57|(1:59)|60)|61)|62|(3:64|(1:66)|268)|67|(9:71|(1:73)|74|(3:76|(2:80|(2:82|270)(1:272))(1:271)|83)|269|84|(2:86|(1:88))|89|(2:91|(1:95))(4:160|(3:162|(1:166)|167)|168|(2:170|(1:174))))|96|(4:99|(2:101|274)(3:(1:104)(2:106|(4:108|(2:110|(6:245|114|115|(2:116|(3:118|(3:281|120|(1:283)(1:286))(1:285)|284)(2:282|156))|159|280))(0)|123|(1:155)(7:127|(3:129|(1:133)|134)|(1:136)(1:154)|137|(4:139|(1:143)|144|(1:152))|153|279))(1:276))|105|275)|102|97)|273|175|(2:179|(3:181|(5:182|(1:184)|185|(1:187)|188)|287)(14:190|(1:192)|288|193|244|194|(1:196)|197|248|198|199|200|(4:202|253|203|(1:205)(10:206|266|207|208|209|(2:210|(1:212)(1:289))|213|214|215|216))|225))|228|(4:230|(3:231|(1:233)|234)|290|236)|237|(2:239|240)(1:291))|34|41|260|42|(1:44)|47|48|(0)|62|(0)|67|(10:69|71|(0)|74|(0)|269|84|(0)|89|(0)(0))|96|(1:97)|273|175|(3:177|179|(0)(0))|228|(0)|237|(0)(0)|(2:(1:252)|(0))) */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03c8, code lost:
        r6.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x049b  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x051a  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0566  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x0757  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x0789  */
    /* JADX WARNING: Removed duplicated region for block: B:291:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0284  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x02b6  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02ba  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02dd  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02ff  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0324  */
    @Override // X.AbstractServiceC003801r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.content.Intent r28) {
        /*
        // Method dump skipped, instructions count: 1949
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.crash.upload.ExceptionsUploadService.A05(android.content.Intent):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A08 == null) {
            synchronized (this.A07) {
                if (this.A08 == null) {
                    this.A08 = new C71083cM(this);
                }
            }
        }
        return this.A08.generatedComponent();
    }

    @Override // X.AbstractServiceC003801r, android.app.Service
    public void onCreate() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A02 = (AnonymousClass1DR) r1.A6j.get();
            this.A03 = (AnonymousClass1DS) r1.AA8.get();
            this.A00 = (AnonymousClass1DT) r1.A0A.get();
            this.A05 = (AnonymousClass1DU) r1.ACu.get();
            this.A04 = (AnonymousClass1DV) r1.ABU.get();
            this.A01 = (AnonymousClass180) r1.ALt.get();
        }
        super.onCreate();
    }
}
