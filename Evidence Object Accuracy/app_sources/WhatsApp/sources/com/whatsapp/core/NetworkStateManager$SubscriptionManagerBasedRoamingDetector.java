package com.whatsapp.core;

import X.AnonymousClass01d;
import android.telephony.SubscriptionManager;
import android.util.Pair;

/* loaded from: classes2.dex */
public class NetworkStateManager$SubscriptionManagerBasedRoamingDetector {
    public static Pair determineNetworkStateUsingSubscriptionManager(AnonymousClass01d r1, boolean z) {
        Boolean bool;
        int i;
        int defaultDataSubscriptionId;
        SubscriptionManager A0M = r1.A0M();
        if (A0M == null || (defaultDataSubscriptionId = SubscriptionManager.getDefaultDataSubscriptionId()) == -1) {
            bool = Boolean.FALSE;
            i = 0;
        } else {
            boolean isNetworkRoaming = A0M.isNetworkRoaming(defaultDataSubscriptionId);
            bool = Boolean.TRUE;
            i = 2;
            if (isNetworkRoaming) {
                i = 3;
            }
        }
        return new Pair(bool, Integer.valueOf(i));
    }
}
