package com.whatsapp.systemstatus;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.AnonymousClass2FL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class SystemStatusActivity extends ActivityC13790kL {
    public int A00;
    public AnonymousClass19Y A01;
    public AnonymousClass11G A02;
    public String A03;
    public String A04;
    public ArrayList A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public final List A0A;

    public SystemStatusActivity() {
        this(0);
        this.A0A = Arrays.asList("broadcast", "registration", "sync", "status");
    }

    public SystemStatusActivity(int i) {
        this.A08 = false;
        ActivityC13830kP.A1P(this, 130);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (AnonymousClass19Y) A1M.AI6.get();
            this.A02 = (AnonymousClass11G) A1M.AKq.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0084, code lost:
        if (r1 == null) goto L_0x0086;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0351  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0204 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0182 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x024c  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r22) {
        /*
        // Method dump skipped, instructions count: 1004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.systemstatus.SystemStatusActivity.onCreate(android.os.Bundle):void");
    }
}
