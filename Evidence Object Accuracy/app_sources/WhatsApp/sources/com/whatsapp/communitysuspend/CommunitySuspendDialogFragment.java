package com.whatsapp.communitysuspend;

import X.ActivityC000900k;
import X.C004802e;
import X.C15220ml;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class CommunitySuspendDialogFragment extends Hilt_CommunitySuspendDialogFragment {
    public C15220ml A00;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        C004802e r2 = new C004802e(A0C);
        IDxCListenerShape4S0200000_2_I1 iDxCListenerShape4S0200000_2_I1 = new IDxCListenerShape4S0200000_2_I1(A0C, 2, this);
        r2.A06(R.string.community_dialog_heading);
        r2.setNegativeButton(R.string.learn_more, iDxCListenerShape4S0200000_2_I1);
        r2.setPositiveButton(R.string.group_suspend_dialog_dismiss, null);
        return r2.create();
    }
}
