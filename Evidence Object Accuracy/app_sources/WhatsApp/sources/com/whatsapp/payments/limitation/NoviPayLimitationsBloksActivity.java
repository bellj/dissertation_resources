package com.whatsapp.payments.limitation;

import X.AbstractActivityC119225dN;
import X.AbstractActivityC123635nW;
import X.AbstractC128525wF;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass3FE;
import X.AnonymousClass610;
import X.AnonymousClass619;
import X.C004802e;
import X.C117295Zj;
import X.C117315Zl;
import X.C121115hH;
import X.C121125hI;
import X.C125065qd;
import X.C126915te;
import X.C128365vz;
import X.C12960it;
import X.C129675y7;
import X.C129685y8;
import X.C12970iu;
import X.C12980iv;
import X.C129895yT;
import X.C12990iw;
import X.C1316663q;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.redex.IDxCListenerShape5S0000000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity;
import com.whatsapp.payments.ui.NoviPayHubActivity;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public class NoviPayLimitationsBloksActivity extends AbstractActivityC123635nW {
    public int A00;
    public AnonymousClass3FE A01;
    public C129895yT A02;
    public C129685y8 A03;
    public C129675y7 A04;
    public boolean A05;
    public boolean A06;
    public final List A07;

    public NoviPayLimitationsBloksActivity() {
        this(0);
        this.A06 = true;
        this.A07 = C12960it.A0l();
    }

    public NoviPayLimitationsBloksActivity(int i) {
        this.A05 = false;
        C117295Zj.A0p(this, 3);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119225dN.A0B(A09, A1M, this, AbstractActivityC119225dN.A0A(A1M, this, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this))));
            AbstractActivityC119225dN.A0K(A1M, this, AbstractActivityC119225dN.A09(A1M, this));
            AbstractActivityC119225dN.A0D(A1M, AbstractActivityC119225dN.A02(A1M, this), this);
            this.A04 = (C129675y7) A1M.AKB.get();
            this.A03 = (C129685y8) A1M.ADa.get();
        }
    }

    @Override // X.AbstractActivityC123635nW
    public void A2o() {
        A30("BACK_CLICK", null);
        super.A2o();
    }

    public final String A2t() {
        AnonymousClass009.A05(this.A02);
        if (this.A00 == 6 || !((AbstractActivityC123635nW) this).A08.A0E()) {
            return "ACCT_RESTRICTION";
        }
        C129895yT r0 = this.A02;
        AnonymousClass009.A05(r0);
        if (r0.A04.contains("READ_DISABLED")) {
            return "READ_LIMITATION";
        }
        C129895yT r02 = this.A02;
        AnonymousClass009.A05(r02);
        if (r02.A04.contains("WRITE_DISABLED")) {
            return "WRITE_LIMITATION";
        }
        C129895yT r03 = this.A02;
        AnonymousClass009.A05(r03);
        return r03.A00() ? "WITHDRAW_LIMITATION" : "UNKNOWN";
    }

    public final void A2u() {
        String str;
        if (this.A02 != null) {
            List list = this.A07;
            list.clear();
            for (AbstractC128525wF r2 : this.A02.A03) {
                if (r2 instanceof C121125hI) {
                    for (C126915te r0 : ((C121125hI) r2).A00.A05) {
                        String str2 = r0.A01;
                        if (str2 != null) {
                            switch (str2.hashCode()) {
                                case -1852691096:
                                    if (str2.equals("SELFIE")) {
                                        str = "STEP_UP_SELFIE";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case -1504126555:
                                    if (str2.equals("DOCUMENT_UPLOAD")) {
                                        str = "STEP_UP_DOC_UPLOAD";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case -1362600187:
                                    if (str2.equals("SMS_OTP")) {
                                        str = "STEP_UP_SMS";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case -708597224:
                                    if (str2.equals("TEXT_INPUT")) {
                                        str = "STEP_UP_TEXT_INPUT";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 74901:
                                    if (str2.equals("KYC")) {
                                        str = "STEP_UP_KYC";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 82915:
                                    if (str2.equals("TDS")) {
                                        str = "STEP_UP_3DS";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 77859202:
                                    if (str2.equals("REKYC")) {
                                        str = "STEP_UP_REKYC";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 504547704:
                                    if (str2.equals("MANUAL_REVIEW__AUTO_TRIGGERED")) {
                                        str = "STEP_UP_MANUAL";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 618937991:
                                    if (str2.equals("MANUAL_REVIEW__SELFIE_ALTERNATIVE")) {
                                        str = "STEP_UP_MANUAL_SELFIE_ALT";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                                case 1793934804:
                                    if (str2.equals("PASSWORD_CHANGE")) {
                                        str = "STEP_UP_PWD_CHANGE";
                                        list.add(str);
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
        }
    }

    public final void A2v(AnonymousClass3FE r9) {
        AbstractC128525wF r6;
        AbstractC128525wF r5;
        C129895yT A03 = ((AbstractActivityC123635nW) this).A0A.A03();
        HashMap A11 = C12970iu.A11();
        if (A03 == null) {
            A11.put("account_limitation_applied", "false");
        } else {
            List list = A03.A03;
            if (list.size() > 0) {
                r6 = (AbstractC128525wF) C12980iv.A0o(list);
            } else {
                r6 = null;
            }
            if (list.size() > 1) {
                r5 = (AbstractC128525wF) list.get(1);
            } else {
                r5 = null;
            }
            A11.put("account_limitation_applied", "true");
            if (r6 != null) {
                String str = r6.A00;
                if (!TextUtils.isEmpty(str)) {
                    A11.put("primary_cta", str.toUpperCase(C12970iu.A14(((ActivityC13830kP) this).A01)));
                }
            }
            if (r5 != null) {
                String str2 = r5.A00;
                if (!TextUtils.isEmpty(str2)) {
                    A11.put("secondary_cta", str2.toUpperCase(C12970iu.A14(((ActivityC13830kP) this).A01)));
                }
            }
            A11.put("title_text", A03.A01.A00);
            AnonymousClass619.A04(A03.A00, A11);
        }
        r9.A02("on_success", A11);
    }

    public final void A2w(AnonymousClass3FE r7, boolean z) {
        AbstractC128525wF r5;
        Object obj;
        C129895yT A03 = ((AbstractActivityC123635nW) this).A0A.A03();
        if (A03 == null) {
            r7.A00("on_failure");
            return;
        }
        if (z) {
            List list = A03.A03;
            if (list.size() > 0) {
                obj = C12980iv.A0o(list);
                r5 = (AbstractC128525wF) obj;
            }
            r5 = null;
        } else {
            List list2 = A03.A03;
            if (list2.size() > 1) {
                obj = list2.get(1);
                r5 = (AbstractC128525wF) obj;
            }
            r5 = null;
        }
        if (r5 instanceof C121125hI) {
            A30("ACCT_RESTRICTION_ACTION_CLICK", r5.A00);
            C129675y7 r4 = this.A04;
            C1316663q r3 = ((C121125hI) r5).A00;
            int i = 12;
            if (this.A00 == 6) {
                i = 23;
            }
            r4.A01(r3, i);
        } else if (r5 instanceof C121115hH) {
            C121115hH r52 = (C121115hH) r5;
            try {
                String str = r52.A00;
                if (str.startsWith("novi://")) {
                    A30("GO_TO_NOVI_CLICK", ((AbstractC128525wF) r52).A00);
                    A2z(str);
                } else {
                    AnonymousClass610 r2 = new AnonymousClass610("HELP_LINK_CLICK", A2t(), "BUTTON");
                    String str2 = ((AbstractC128525wF) r52).A00;
                    C128365vz r0 = r2.A00;
                    r0.A0L = str2;
                    r0.A0R = str;
                    A2x(r2);
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(str));
                    startActivity(intent);
                }
            } catch (ActivityNotFoundException e) {
                Log.e("PAY: NoviPayLimitationsBloksActivity Couldn't process link uri", e);
            }
        } else {
            Log.e("PAY: can't launch the call to action");
            r7.A00("on_failure");
        }
        C117315Zl.A0T(r7);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2x(X.AnonymousClass610 r4) {
        /*
            r3 = this;
            X.5yT r0 = r3.A02
            X.AnonymousClass009.A05(r0)
            int r1 = r3.A00
            r0 = 6
            if (r1 != r0) goto L_0x0044
            java.lang.String r1 = "ONBOARDING"
        L_0x000c:
            X.5vz r0 = r4.A00
            r0.A0F = r1
        L_0x0010:
            java.util.List r1 = r3.A07
            int r0 = r1.size()
            if (r0 <= 0) goto L_0x0022
            java.lang.String r0 = ","
            java.lang.String r1 = android.text.TextUtils.join(r0, r1)
            X.5vz r0 = r4.A00
            r0.A0g = r1
        L_0x0022:
            X.60Y r2 = r3.A07
            X.5yT r0 = r3.A02
            X.AnonymousClass009.A05(r0)
            java.util.List r0 = r0.A04
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0042
            X.5yT r0 = r3.A02
            java.util.List r1 = r0.A04
            r0 = 0
            java.lang.String r1 = X.C12960it.A0g(r1, r0)
        L_0x003a:
            X.5vz r0 = r4.A00
            r0.A0D = r1
            r2.A05(r0)
            return
        L_0x0042:
            r1 = 0
            goto L_0x003a
        L_0x0044:
            r0 = 1
            if (r1 != r0) goto L_0x0010
            java.lang.String r1 = "LOGIN"
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity.A2x(X.610):void");
    }

    public final void A2y(String str) {
        if (this.A02 != null) {
            A2x(new AnonymousClass610(str, A2t(), "SCREEN"));
        }
    }

    public final void A2z(String str) {
        boolean A1W = C12960it.A1W(getPackageManager().getLaunchIntentForPackage("com.novi.wallet"));
        C004802e A0S = C12980iv.A0S(this);
        int i = R.string.redirect_to_novi_app_install_novi_title;
        if (A1W) {
            i = R.string.redirect_to_novi_app_open_novi_title;
        }
        A0S.A07(i);
        A0S.A06(R.string.redirect_to_novi_app_dialog_message);
        int i2 = R.string.redirect_to_novi_app_install_button_text;
        if (A1W) {
            i2 = R.string.open;
        }
        A0S.setPositiveButton(i2, new DialogInterface.OnClickListener(str, A1W) { // from class: X.62e
            public final /* synthetic */ String A01;
            public final /* synthetic */ boolean A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                Intent intent;
                NoviPayLimitationsBloksActivity noviPayLimitationsBloksActivity = NoviPayLimitationsBloksActivity.this;
                boolean z = this.A02;
                String str2 = this.A01;
                try {
                    if (z) {
                        noviPayLimitationsBloksActivity.A30("GO_TO_NOVI_CLICK", "Open");
                        if (str2 == null) {
                            intent = noviPayLimitationsBloksActivity.getPackageManager().getLaunchIntentForPackage("com.novi.wallet");
                            if (intent == null) {
                                return;
                            }
                        } else {
                            C1310561a.A05(noviPayLimitationsBloksActivity, str2);
                            return;
                        }
                    } else {
                        noviPayLimitationsBloksActivity.A30("INSTALL_NOVI_CLICK", "Install");
                        intent = C117295Zj.A04("market://details?id=com.novi.wallet");
                    }
                    noviPayLimitationsBloksActivity.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("PAY: NoviPayLimitationsBloksActivity Couldn't start Novi app", e);
                }
            }
        });
        A0S.setNegativeButton(R.string.cancel, new IDxCListenerShape5S0000000_3_I1(0));
        C12970iu.A1J(A0S);
    }

    public final void A30(String str, String str2) {
        AnonymousClass610 r1 = new AnonymousClass610(str, A2t(), "BUTTON");
        r1.A00.A0L = str2;
        A2x(r1);
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractC136496Mt
    public void AZC(AnonymousClass3FE r4, String str, Map map) {
        String str2;
        AbstractActivityC119225dN.A0M(r4, str);
        switch (str.hashCode()) {
            case -1868132857:
                if (str.equals("trigger_limitation_secondary_cta")) {
                    A2w(r4, false);
                    return;
                }
                break;
            case -1610316083:
                if (str.equals("trigger_limitation_body_uri")) {
                    String A0t = C12970iu.A0t("uri", map);
                    if (!TextUtils.isEmpty(A0t)) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(A0t));
                        startActivity(intent);
                        str2 = "on_success";
                    } else {
                        str2 = "on_failure";
                    }
                    r4.A00(str2);
                    return;
                }
                break;
            case -604595179:
                if (str.equals("trigger_limitation_primary_cta")) {
                    A2w(r4, true);
                    return;
                }
                break;
            case 1412172490:
                if (str.equals("get_limitation_info")) {
                    this.A01 = r4;
                    A2v(r4);
                    A2y("ACCT_RESTRICTION_VPV");
                    return;
                }
                break;
        }
        super.AZC(r4, str, map);
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC119645em, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C129895yT r2 = this.A02;
        if (r2 == null || this.A00 != 1 || r2.A04.contains("READ_DISABLED")) {
            super.onBackPressed();
            return;
        }
        startActivity(C12990iw.A0D(this, NoviPayHubActivity.class));
        finish();
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C125065qd.A00(this);
        if (getIntent() != null) {
            getIntent().putExtra("screen_name", "novipay_p_account_limitation");
            this.A00 = getIntent().getIntExtra("limitation_origin", 0);
            A2i();
        }
        C117295Zj.A0r(this, this.A04.A04, 2);
        this.A02 = ((AbstractActivityC123635nW) this).A0A.A03();
        A2u();
        C117295Zj.A0r(this, ((AbstractActivityC123635nW) this).A08.A0I, 0);
    }

    @Override // X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        A2y("NAVIGATION_END");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        if (((X.AbstractActivityC123635nW) r5).A08.A0H() == false) goto L_0x0036;
     */
    @Override // X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r5 = this;
            super.onResume()
            boolean r0 = r5.A06
            if (r0 == 0) goto L_0x0010
            r0 = 0
            r5.A06 = r0
        L_0x000a:
            java.lang.String r0 = "NAVIGATION_START"
            r5.A2y(r0)
            return
        L_0x0010:
            boolean r0 = r5.isFinishing()
            if (r0 == 0) goto L_0x001c
            java.lang.String r0 = "[PAY] : NoviPayLimitationsBloksActivity skip fetching limitation data, as screen is dismissed"
            com.whatsapp.util.Log.i(r0)
            goto L_0x000a
        L_0x001c:
            X.5y8 r4 = r5.A03
            X.5yT r0 = r5.A02
            X.AnonymousClass009.A05(r0)
            java.util.List r1 = r0.A04
            java.lang.String r0 = "READ_DISABLED"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0036
            X.61F r0 = r5.A08
            boolean r0 = r0.A0H()
            r3 = 5
            if (r0 != 0) goto L_0x0037
        L_0x0036:
            r3 = 6
        L_0x0037:
            X.016 r2 = X.C12980iv.A0T()
            X.0lR r1 = r4.A05
            X.6JD r0 = new X.6JD
            r0.<init>(r2, r4, r3)
            r1.Ab2(r0)
            r0 = 1
            X.C117295Zj.A0r(r5, r2, r0)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity.onResume():void");
    }
}
