package com.whatsapp.payments.ui.widget;

import X.AbstractC117675aQ;
import X.AnonymousClass004;
import X.AnonymousClass1In;
import X.AnonymousClass2P7;
import X.C128955ww;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes4.dex */
public class TransactionsExpandableView extends AbstractC117675aQ implements AnonymousClass004 {
    public C128955ww A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public TransactionsExpandableView(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A00 = new C128955ww(context);
    }

    public TransactionsExpandableView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A00 = new C128955ww(context);
    }

    public TransactionsExpandableView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A00 = new C128955ww(context);
    }

    public TransactionsExpandableView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setAdapter(C128955ww r1) {
        this.A00 = r1;
    }

    public void setPaymentRequestActionCallback(AnonymousClass1In r2) {
        this.A00.A02 = r2;
    }
}
