package com.whatsapp.payments.ui;

import X.AbstractActivityC121795k0;
import X.AbstractC005102i;
import X.AbstractC13900kW;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC28901Pl;
import X.AbstractC35651iS;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass03U;
import X.AnonymousClass109;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass17T;
import X.AnonymousClass18P;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.AnonymousClass18U;
import X.AnonymousClass19O;
import X.AnonymousClass19Y;
import X.AnonymousClass1AA;
import X.AnonymousClass1AB;
import X.AnonymousClass1IR;
import X.AnonymousClass1IS;
import X.AnonymousClass1P3;
import X.AnonymousClass1ZY;
import X.AnonymousClass2I7;
import X.AnonymousClass398;
import X.AnonymousClass3DI;
import X.AnonymousClass5X2;
import X.AnonymousClass5m0;
import X.AnonymousClass5m1;
import X.AnonymousClass5m3;
import X.AnonymousClass5m4;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C118185bP;
import X.C122285lD;
import X.C122565lf;
import X.C122575lg;
import X.C122585lh;
import X.C122605lj;
import X.C122615lk;
import X.C122625ll;
import X.C122675lq;
import X.C122685lr;
import X.C122695ls;
import X.C122735lw;
import X.C122745lx;
import X.C122755ly;
import X.C124295ov;
import X.C127915vG;
import X.C128315vu;
import X.C129075x8;
import X.C129395xe;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C130345zG;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15890o4;
import X.C16170oZ;
import X.C17070qD;
import X.C17900ra;
import X.C18790t3;
import X.C21270x9;
import X.C22370yy;
import X.C22460z7;
import X.C22710zW;
import X.C239613r;
import X.C241414j;
import X.C243515e;
import X.C253118x;
import X.C30041Vv;
import X.C30061Vy;
import X.C30921Zi;
import X.C30931Zj;
import X.C38211ni;
import X.C43951xu;
import X.C63593Ce;
import X.C64853Hd;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.List;
import org.chromium.net.UrlRequest;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class PaymentTransactionDetailsListActivity extends AbstractActivityC121795k0 implements AbstractC13900kW {
    public AnonymousClass18U A00;
    public AnonymousClass19Y A01;
    public C239613r A02;
    public C18790t3 A03;
    public C16170oZ A04;
    public AnonymousClass1AA A05;
    public AnonymousClass130 A06;
    public C15550nR A07;
    public C15610nY A08;
    public C21270x9 A09;
    public AnonymousClass3DI A0A;
    public C15890o4 A0B;
    public AnonymousClass018 A0C;
    public C241414j A0D;
    public AnonymousClass109 A0E;
    public C22370yy A0F;
    public AnonymousClass13H A0G;
    public AnonymousClass18T A0H;
    public AnonymousClass18P A0I;
    public AnonymousClass18S A0J;
    public C17900ra A0K;
    public C22710zW A0L;
    public C17070qD A0M;
    public C22460z7 A0N;
    public C118185bP A0O;
    public C129395xe A0P;
    public AnonymousClass14X A0Q;
    public AnonymousClass1AB A0R;
    public C253118x A0S;
    public AnonymousClass19O A0T;
    public AnonymousClass2I7 A0U;
    public String A0V;
    public final C30931Zj A0W = C117305Zk.A0V("PaymentTransactionDetailsListActivity", "payment-settings");

    @Override // X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 306) {
            return new C122605lj(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.order_detail_view));
        }
        switch (i) {
            case 200:
                return new C122735lw(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_amount_view), ((ActivityC13810kN) this).A0C, this.A0N);
            case 201:
                return new C122745lx(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_action_button_view), this.A0I);
            case 202:
                return new AnonymousClass5m1(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_status_view), ((ActivityC13810kN) this).A08);
            case 203:
                C21270x9 r8 = this.A09;
                AnonymousClass130 r7 = this.A06;
                C253118x r10 = this.A0S;
                return new AnonymousClass5m3(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_payee_payer_detail_view), ((ActivityC13790kL) this).A02, r7, r8, ((ActivityC13810kN) this).A08, r10);
            case 204:
                return new C122685lr(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_help_view));
            case 205:
                C14900mE r6 = ((ActivityC13810kN) this).A05;
                AnonymousClass13H r102 = this.A0G;
                return new AnonymousClass5m4(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_note_row), r6, this.A00, this.A0A, ((ActivityC13810kN) this).A08, r102);
            case 206:
                return new C122625ll(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_row));
            case 207:
                return new C122285lD(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_detail_item_row_divider));
            case 208:
                return new C122575lg(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_send_again_view));
            case 209:
                C14850m9 r62 = ((ActivityC13810kN) this).A0C;
                C239613r r2 = this.A02;
                C16170oZ r3 = this.A04;
                AnonymousClass018 r5 = this.A0C;
                AnonymousClass19O r103 = this.A0T;
                C15890o4 r4 = this.A0B;
                C22370yy r82 = this.A0F;
                AnonymousClass1AB r9 = this.A0R;
                AnonymousClass109 r72 = this.A0E;
                View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_sticker_note_row);
                return new C122585lh(A0F, new C64853Hd(A0F, r2, r3, r4, r5, r62, r72, r82, r9, r103));
            case 210:
                return new C122565lf(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_short_desc_view));
            case 211:
                return new C122675lq(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_status_blurb));
            case 212:
                return new C122755ly(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_status_timeline), ((ActivityC13810kN) this).A08, C117305Zk.A0U(this.A0M));
            case 213:
                return new C122615lk(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_report_transaction_view));
            case 214:
                return new AnonymousClass5m0(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_complaint_row), ((ActivityC13810kN) this).A08);
            case 215:
                return new C122695ls(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_description_with_asset_row));
            default:
                return super.A2e(viewGroup, i);
        }
    }

    public void A2f(C128315vu r19) {
        Intent intent;
        Intent intent2;
        AbstractC28901Pl r0;
        String str;
        AbstractC15340mz r4;
        String A03;
        C30921Zi A01;
        String str2;
        Boolean A02;
        UserJid userJid;
        switch (r19.A00) {
            case 0:
                int i = r19.A02.getInt("action_bar_title_res_id");
                AbstractC005102i A1U = A1U();
                if (A1U != null) {
                    A1U.A0M(true);
                    A1U.A0A(i);
                    if (getIntent().getBooleanExtra("extra_action_bar_display_close", false)) {
                        A1U.A08(R.drawable.ic_pip_close);
                        return;
                    }
                    return;
                }
                return;
            case 1:
                if (r19.A0H) {
                    A2C(R.string.payments_loading);
                    return;
                } else {
                    AaN();
                    return;
                }
            case 2:
                finish();
                return;
            case 3:
                invalidateOptionsMenu();
                return;
            case 4:
                C15370n3 r2 = r19.A03;
                AnonymousClass009.A05(r2);
                intent = new C14960mK().A0h(this, r2, 18);
                startActivity(intent);
                return;
            case 5:
                Intent A0D = C12990iw.A0D(this, this.A0M.A02().AFR());
                A0D.putExtra("extra_payment_handle", C117305Zk.A0I(C117305Zk.A0J(), String.class, r19.A0E, "paymentHandle"));
                A0D.putExtra("extra_payment_handle_id", r19.A0D);
                A0D.putExtra("extra_payee_name", r19.A07);
                A2D(A0D);
                return;
            case 6:
                Adr(new Object[]{getString(this.A0M.A02().AFH())}, 0, R.string.payment_id_cannot_verify_error_text_default);
                return;
            case 7:
                intent2 = C12990iw.A0D(this, r19.A0A);
                AbstractC28901Pl r02 = r19.A04;
                AnonymousClass009.A05(r02);
                intent2.putExtra("extra_bank_account", r02);
                intent2.putExtra("event_screen", "forgot_pin");
                startActivity(intent2);
                return;
            case 8:
                A2Q(r19.A0F, r19.A0B);
                return;
            case 9:
                intent2 = C12990iw.A0D(this, this.A0M.A02().AAV());
                r0 = r19.A04;
                AnonymousClass009.A05(r0);
                intent2.putExtra("extra_bank_account", r0);
                startActivity(intent2);
                return;
            case 10:
                AnonymousClass1IR r14 = r19.A05;
                AnonymousClass009.A05(r14);
                AbstractC28901Pl r13 = r19.A04;
                String str3 = r14.A0F() ? "payments:request" : "payments:transaction";
                try {
                    JSONObject put = C117295Zj.A0a().put("lg", this.A0C.A06()).put("lc", this.A0C.A05()).put("platform", "android").put("context", str3).put("type", "p2p");
                    String str4 = r14.A0J;
                    if (str4 != null) {
                        put.put("error_code", str4);
                    }
                    if (r13 != null && !TextUtils.isEmpty(r13.A0B)) {
                        put.put("bank_name", r13.A0B);
                    }
                } catch (Exception e) {
                    this.A0W.A0A("debugInfoData fields", e);
                }
                Bundle A0D2 = C12970iu.A0D();
                if (!r14.A0F()) {
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentFBTxnId", r14.A0K);
                }
                String str5 = r14.A0F;
                if (str5 != null) {
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentBankTxnId", str5);
                }
                if (r13 != null) {
                    A0D2.putParcelable("com.whatsapp.support.DescribeProblemActivity.paymentMethod", r13);
                    AnonymousClass1ZY r03 = r13.A08;
                    if (r03 != null) {
                        A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentBankPhone", r03.A08());
                    } else {
                        this.A0W.A05("payment method missing country fields");
                    }
                }
                String str6 = r14.A0J;
                if (str6 != null) {
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentErrorCode", str6);
                }
                if (r14.A02 == 409) {
                    A0D2.putInt("com.whatsapp.support.DescribeProblemActivity.type", 2);
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentStatus", "RF");
                }
                AnonymousClass5X2 AFE = this.A0M.A02().AFE();
                if (AFE != null && AFE.AIL()) {
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.uri", A22().toString());
                }
                A0D2.putString("com.whatsapp.support.DescribeProblemActivity.from", str3);
                C12990iw.A1N(new C124295ov(A0D2, this, this.A01, ((ActivityC13810kN) this).A06, this.A03, this.A0C, r13, r14, ((ActivityC13810kN) this).A0D, this.A0K, str3), ((ActivityC13830kP) this).A05);
                return;
            case 11:
                Context applicationContext = getApplicationContext();
                String str7 = r19.A0G;
                AnonymousClass009.A05(str7);
                intent = C14960mK.A0a(applicationContext, str7, null, false, true);
                startActivity(intent);
                return;
            case 12:
                C127915vG r04 = this.A0O.A06;
                if (r04 != null) {
                    r4 = r04.A02;
                } else {
                    r4 = null;
                }
                Intent A00 = this.A0H.A00(this, true, false);
                A00.putExtra("extra_payment_preset_amount", this.A0K.A00().AA8(this.A0C, r4.A0L.A08));
                AbstractC14640lm r3 = r4.A0z.A00;
                String str8 = "extra_jid";
                if (r3 instanceof GroupJid) {
                    A00.putExtra(str8, r3.getRawString());
                    A03 = C15380n4.A03(r4.A0L.A0D);
                    str8 = "extra_receiver_jid";
                } else {
                    A03 = C15380n4.A03(r4.A0L.A0D);
                }
                A00.putExtra(str8, A03);
                A00.putExtra("extra_payment_note", r4.A0I());
                A00.putExtra("extra_conversation_message_type", 1);
                if (r4.A0z()) {
                    List list = r4.A0o;
                    AnonymousClass009.A05(list);
                    A00.putStringArrayListExtra("extra_mentioned_jids", C12980iv.A0x(C15380n4.A06(list)));
                }
                AnonymousClass1IR r05 = r4.A0L;
                if (!(r05 == null || (A01 = r05.A01()) == null)) {
                    A00.putExtra("extra_payment_background", A01);
                }
                if ((((ActivityC13810kN) this).A0C.A07(812) || ((ActivityC13810kN) this).A0C.A07(811)) && (r4 instanceof C30061Vy)) {
                    C30061Vy r42 = (C30061Vy) r4;
                    A00.putExtra("extra_payment_sticker", r42.A1C());
                    A00.putExtra("extra_payment_sticker_send_origin", r42.A02);
                }
                startActivity(A00);
                finish();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A0J.A00(this, new AnonymousClass1P3(r19) { // from class: X.66v
                    public final /* synthetic */ C128315vu A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AnonymousClass1P3
                    public final void AVM(boolean z) {
                        PaymentTransactionDetailsListActivity paymentTransactionDetailsListActivity = PaymentTransactionDetailsListActivity.this;
                        C128315vu r06 = this.A01;
                        C118185bP r6 = paymentTransactionDetailsListActivity.A0O;
                        String str9 = r06.A0E;
                        C128315vu A002 = C128315vu.A00(8);
                        Context context = r6.A0O.A00;
                        int i2 = R.string.unblock_payment_id_error_default;
                        if (z) {
                            i2 = R.string.unblock_confirmation;
                        }
                        A002.A0B = C12960it.A0X(context, str9, C12970iu.A1b(), 0, i2);
                        C118185bP.A01(r6, A002);
                    }
                }, r19.A06, C117305Zk.A0I(C117305Zk.A0J(), String.class, r19.A0E, "paymentHandle"), false, false);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case 24:
            default:
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                intent2 = C12990iw.A0D(this, r19.A09);
                r0 = r19.A04;
                intent2.putExtra("extra_bank_account", r0);
                startActivity(intent2);
                return;
            case 17:
                if (r19.A05 != null) {
                    C130345zG.A01(this, r19.A05, this.A0M.A02(), null, "wa_payment_settings", 0);
                    return;
                }
                return;
            case 18:
                if (this instanceof NoviPaymentTransactionDetailsActivity) {
                    NoviPaymentTransactionDetailsActivity noviPaymentTransactionDetailsActivity = (NoviPaymentTransactionDetailsActivity) this;
                    noviPaymentTransactionDetailsActivity.A06.A0B(noviPaymentTransactionDetailsActivity);
                    return;
                } else if (!(this instanceof IndiaUpiPaymentTransactionDetailsActivity)) {
                    if (this instanceof BrazilPaymentTransactionDetailActivity) {
                        this.A0O.A0B(this);
                        return;
                    }
                    finish();
                    return;
                } else {
                    IndiaUpiPaymentTransactionDetailsActivity indiaUpiPaymentTransactionDetailsActivity = (IndiaUpiPaymentTransactionDetailsActivity) this;
                    indiaUpiPaymentTransactionDetailsActivity.A01.A0B(indiaUpiPaymentTransactionDetailsActivity);
                    return;
                }
            case 19:
                super.onBackPressed();
                return;
            case C43951xu.A01:
                this.A0O.A0F(this.A0V, 141);
                ((ActivityC13790kL) this).A00.A06(this, this.A0U.A00("smb_transaction_details"));
                return;
            case 21:
                this.A0O.A0F(this.A0V, 87);
                AnonymousClass1IS r10 = r19.A08;
                if (r10 == null) {
                    Log.e("PAY : PaymentTransactionDetailsListActivity/EVENT_OPEN_ORDER_DETAILS_PAGE : orderMessageKey is null");
                    AnonymousClass1IR r1 = r19.A05;
                    if (r1 == null || r1.A0D == null || (A02 = r1.A02()) == null) {
                        str2 = null;
                    } else {
                        C15370n3 A0B = this.A07.A0B(r19.A05.A0D);
                        str2 = A02.booleanValue() ? A0B.A0K : A0B.A0U;
                    }
                    String string = getString(R.string.order_details_order_details_not_available_title);
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = str2;
                    AnonymousClass61M.A03(this, null, string, C12960it.A0X(this, r19.A0C, A1a, 1, R.string.order_details_order_details_not_available_content)).show();
                    return;
                } else if (!r10.A02) {
                    C63593Ce r7 = new C63593Ce(this.A0H, this.A0L);
                    AbstractC14640lm r9 = r10.A00;
                    AnonymousClass009.A05(r9);
                    String str9 = r19.A0C;
                    AnonymousClass009.A05(str9);
                    r7.A00(this, r9, r10, null, null, str9, null, r19.A01, false);
                    return;
                } else {
                    return;
                }
            case 22:
                if (r19.A05.A03 == 200) {
                    str = "wa_smb_p2m_payment_details";
                } else {
                    str = "wa_p2m_receipt_support";
                }
                AnonymousClass1IR r22 = r19.A05;
                AnonymousClass009.A05(r22);
                C129075x8 r12 = new C129075x8();
                r12.A04 = str;
                r12.A01 = this.A0C;
                r12.A02 = r22;
                r12.A03 = this.A0Q;
                r12.A00 = this.A07;
                r12.A00(this);
                return;
            case 23:
                str = "wa_p2m_receipt_report_transaction";
                AnonymousClass1IR r22 = r19.A05;
                AnonymousClass009.A05(r22);
                C129075x8 r12 = new C129075x8();
                r12.A04 = str;
                r12.A01 = this.A0C;
                r12.A02 = r22;
                r12.A03 = this.A0Q;
                r12.A00 = this.A07;
                r12.A00(this);
                return;
            case 25:
                this.A0O.A0F(this.A0V, 142);
                AnonymousClass17T AEE = this.A0M.A02().AEE();
                if (AEE != null) {
                    AEE.A00(this, "payment_transaction_details");
                    return;
                }
                return;
            case 26:
                this.A0O.A0F(this.A0V, 143);
                C15570nT r32 = ((ActivityC13790kL) this).A01;
                AnonymousClass1IR r06 = r19.A05;
                AnonymousClass009.A05(r06);
                boolean A0F = r32.A0F(r06.A0E);
                AnonymousClass1IR r07 = r19.A05;
                if (!A0F) {
                    userJid = r07.A0E;
                } else {
                    userJid = r07.A0D;
                }
                AnonymousClass009.A05(userJid);
                r32.A08();
                if (r32.A05 != null) {
                    r32.A08();
                    throw new UnsupportedOperationException();
                }
                return;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0044, code lost:
        if (r2.A03 != 40) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0047, code lost:
        if (r2 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0049, code lost:
        r12 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004e, code lost:
        if (X.AnonymousClass604.A00(r2) != false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
        r12 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0051, code lost:
        r3.AKj(r4, r14, r15, "payment_transaction_details", r8, null, null, r11, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2g(java.lang.Integer r14, java.lang.Integer r15) {
        /*
            r13 = this;
            X.5bP r0 = r13.A0O
            X.5vG r0 = r0.A06
            r4 = 0
            if (r0 != 0) goto L_0x006f
            r2 = r4
        L_0x0008:
            X.0qD r0 = r13.A0M
            X.0pt r3 = X.C117305Zk.A0U(r0)
            if (r3 == 0) goto L_0x005a
            if (r2 == 0) goto L_0x003b
            int r1 = r2.A03
            r0 = 9
            if (r1 != r0) goto L_0x0061
            java.lang.String r1 = "cashback"
        L_0x001a:
            java.lang.String r0 = "cashback"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x005b
            java.lang.String r0 = "incentive"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x005b
            java.lang.String r0 = "purchase"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x003b
            X.3FW r4 = X.C117305Zk.A0S()
        L_0x0036:
            java.lang.String r0 = "transaction_type"
            r4.A01(r0, r1)
        L_0x003b:
            java.lang.String r8 = r13.A0V
            if (r2 == 0) goto L_0x0046
            int r1 = r2.A03
            r0 = 40
            r11 = 1
            if (r1 == r0) goto L_0x0049
        L_0x0046:
            r11 = 0
            if (r2 == 0) goto L_0x0050
        L_0x0049:
            boolean r0 = X.AnonymousClass604.A00(r2)
            r12 = 1
            if (r0 != 0) goto L_0x0051
        L_0x0050:
            r12 = 0
        L_0x0051:
            r9 = 0
            java.lang.String r7 = "payment_transaction_details"
            r5 = r14
            r6 = r15
            r10 = r9
            r3.AKj(r4, r5, r6, r7, r8, r9, r10, r11, r12)
        L_0x005a:
            return
        L_0x005b:
            X.5hA r4 = new X.5hA
            r4.<init>()
            goto L_0x0036
        L_0x0061:
            X.1Zf r0 = r2.A0A
            if (r0 == 0) goto L_0x006c
            X.1Zn r0 = r0.A00
            if (r0 == 0) goto L_0x006c
            java.lang.String r1 = "incentive"
            goto L_0x001a
        L_0x006c:
            java.lang.String r1 = "none"
            goto L_0x001a
        L_0x006f:
            X.1IR r2 = r0.A01
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity.A2g(java.lang.Integer, java.lang.Integer):void");
    }

    @Override // X.AbstractC13900kW
    public AnonymousClass1AB AGx() {
        return this.A0R;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v7, types: [X.5nN] */
    /* JADX WARN: Type inference failed for: r2v9, types: [X.5bP] */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        if (X.C12990iw.A0H(r5) != null) goto L_0x0037;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            super.onCreate(r6)
            X.0zW r0 = r5.A0L
            boolean r0 = r0.A03()
            X.AnonymousClass009.A0F(r0)
            android.content.Intent r1 = r5.getIntent()
            java.lang.String r0 = "referral_screen"
            java.lang.String r0 = r1.getStringExtra(r0)
            r5.A0V = r0
            X.14j r0 = r5.A0D
            boolean r0 = r0.A07
            if (r0 == 0) goto L_0x0026
            if (r6 != 0) goto L_0x0042
            android.os.Bundle r0 = X.C12990iw.A0H(r5)
            if (r0 != 0) goto L_0x0042
        L_0x0026:
            X.1Zj r3 = r5.A0W
            java.lang.String r0 = "PaymentStore uninitialized or no valid bundle: "
            java.lang.StringBuilder r2 = X.C12960it.A0k(r0)
            if (r6 != 0) goto L_0x0037
            android.os.Bundle r1 = X.C12990iw.A0H(r5)
            r0 = 1
            if (r1 == 0) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            r2.append(r0)
            X.C117295Zj.A1F(r3, r2)
            r5.finish()
            return
        L_0x0042:
            r3 = r5
            boolean r0 = r5 instanceof com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity
            if (r0 != 0) goto L_0x00d5
            boolean r0 = r5 instanceof com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity
            if (r0 != 0) goto L_0x00b7
            boolean r0 = r5 instanceof com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity
            if (r0 != 0) goto L_0x00a1
            X.5xe r2 = r5.A0P
            X.0qD r0 = r5.A0M
            X.0pt r1 = X.C117305Zk.A0U(r0)
            if (r6 != 0) goto L_0x005d
            android.os.Bundle r6 = X.C12990iw.A0H(r5)
        L_0x005d:
            X.5br r0 = new X.5br
            r0.<init>(r6, r1, r2)
            X.02A r1 = X.C117315Zl.A06(r0, r5)
            java.lang.Class<X.5bP> r0 = X.C118185bP.class
        L_0x0068:
            X.015 r2 = r1.A00(r0)
            X.5bP r2 = (X.C118185bP) r2
        L_0x006e:
            r5.A0O = r2
            r0 = 123(0x7b, float:1.72E-43)
            com.facebook.redex.IDxObserverShape5S0100000_3_I1 r1 = X.C117305Zk.A0B(r5, r0)
            X.016 r0 = r2.A02
            r0.A05(r5, r1)
            r0 = 122(0x7a, float:1.71E-43)
            com.facebook.redex.IDxObserverShape5S0100000_3_I1 r1 = X.C117305Zk.A0B(r5, r0)
            X.1It r0 = r2.A08
            r0.A05(r5, r1)
            X.5bP r2 = r5.A0O
            r1 = 2
            X.5sI r0 = new X.5sI
            r0.<init>(r1)
            r2.A0E(r0)
            X.0mE r4 = r5.A05
            X.0qD r3 = r5.A0M
            X.0m6 r2 = r5.A09
            X.0zW r1 = r5.A0L
            X.3DI r0 = new X.3DI
            r0.<init>(r4, r2, r1, r3)
            r5.A0A = r0
            return
        L_0x00a1:
            com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity r3 = (com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity) r3
            if (r6 != 0) goto L_0x00a9
            android.os.Bundle r6 = X.C12990iw.A0H(r3)
        L_0x00a9:
            X.5vx r1 = r3.A02
            X.5bZ r0 = new X.5bZ
            r0.<init>(r6, r1)
            X.02A r1 = X.C117315Zl.A06(r0, r3)
            java.lang.Class<X.5nL> r0 = X.C123555nL.class
            goto L_0x0068
        L_0x00b7:
            com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity r3 = (com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity) r3
            X.5vy r1 = r3.A02
            if (r6 != 0) goto L_0x00c1
            android.os.Bundle r6 = X.C12990iw.A0H(r3)
        L_0x00c1:
            X.5ba r0 = new X.5ba
            r0.<init>(r6, r1)
            X.02A r1 = X.C117315Zl.A06(r0, r3)
            java.lang.Class<X.5nN> r0 = X.C123575nN.class
            X.015 r2 = r1.A00(r0)
            X.5nN r2 = (X.C123575nN) r2
            r3.A01 = r2
            goto L_0x006e
        L_0x00d5:
            com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity r3 = (com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity) r3
            X.5w0 r1 = r3.A07
            if (r6 != 0) goto L_0x00df
            android.os.Bundle r6 = X.C12990iw.A0H(r3)
        L_0x00df:
            X.5bd r0 = new X.5bd
            r0.<init>(r6, r1)
            X.02A r1 = X.C117315Zl.A06(r0, r3)
            java.lang.Class<X.5nM> r0 = X.C123565nM.class
            X.015 r2 = r1.A00(r0)
            X.5nM r2 = (X.C123565nM) r2
            r3.A06 = r2
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C127915vG r0 = this.A0O.A06;
        if (!(r0 == null || r0.A02 == null)) {
            menu.add(0, R.id.menuitem_view_in_chat, 0, R.string.payment_view_in_chat);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        AnonymousClass398 r1;
        C243515e r12;
        AbstractC35651iS r0;
        super.onDestroy();
        C118185bP r02 = this.A0O;
        if (!(r02 == null || (r12 = r02.A0Y) == null || (r0 = r02.A04) == null)) {
            r12.A04(r0);
        }
        AnonymousClass3DI r2 = this.A0A;
        if (r2 != null && (r1 = r2.A00) != null) {
            r1.A04 = true;
            r1.interrupt();
            r2.A00 = null;
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        AbstractC15340mz r4;
        Intent A0A;
        Class cls;
        C127915vG r0 = this.A0O.A06;
        if (r0 != null) {
            r4 = r0.A02;
        } else {
            r4 = null;
        }
        if (menuItem.getItemId() == 16908332) {
            finish();
            if (isTaskRoot()) {
                if (!(this instanceof IndiaUpiPaymentTransactionDetailsActivity)) {
                    cls = PaymentTransactionHistoryActivity.class;
                } else {
                    cls = IndiaPaymentTransactionHistoryActivity.class;
                }
                A0A = C12990iw.A0D(this, cls);
                A0A.putExtra("extra_show_requests", this.A0O.A09);
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                }
            }
            return true;
        }
        if (r4 != null) {
            if (menuItem.getItemId() == R.id.menuitem_view_in_chat) {
                long A01 = C30041Vv.A01(r4);
                long A02 = C30041Vv.A02(r4);
                AnonymousClass12P r7 = ((ActivityC13790kL) this).A00;
                C14960mK r1 = new C14960mK();
                AnonymousClass009.A05(r4);
                AnonymousClass1IS r42 = r4.A0z;
                r7.A07(this, C38211ni.A00(r1.A0i(this, r42.A00).putExtra("row_id", A01).putExtra("sort_id", A02), r42));
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_debug) {
                AnonymousClass009.A0F(this.A0L.A03());
                A0A = C12970iu.A0A();
                String AF9 = this.A0M.A02().AF9();
                if (TextUtils.isEmpty(AF9)) {
                    return false;
                }
                A0A.setClassName(this, AF9);
                A0A.putExtra("extra_transaction_id", r4.A0m);
                AnonymousClass1IS r02 = r4.A0z;
                if (r02 != null) {
                    C38211ni.A00(A0A, r02);
                }
            }
        }
        return super.onOptionsItemSelected(menuItem);
        startActivity(A0A);
        return true;
    }
}
