package com.whatsapp.payments.ui;

import X.AbstractActivityC119345dZ;
import X.AbstractC005102i;
import X.AbstractC16710pd;
import X.AbstractC28901Pl;
import X.AnonymousClass00T;
import X.AnonymousClass1ZX;
import X.AnonymousClass1ZY;
import X.AnonymousClass2DI;
import X.AnonymousClass2GE;
import X.AnonymousClass4Yq;
import X.AnonymousClass617;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14960mK;
import X.C16700pc;
import X.C17070qD;
import X.C25901Bg;
import X.C43951xu;
import X.C72023dt;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BusinessHubActivity;
import com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel;

/* loaded from: classes2.dex */
public final class BusinessHubActivity extends AbstractActivityC119345dZ {
    public ViewGroup A00;
    public ViewGroup A01;
    public ImageView A02;
    public ImageView A03;
    public TextView A04;
    public TextView A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public TextView A09;
    public C17070qD A0A;
    public C25901Bg A0B;
    public final AbstractC16710pd A0C = AnonymousClass4Yq.A00(new C72023dt(this));

    public static /* synthetic */ void A02(AnonymousClass2DI r11, BusinessHubActivity businessHubActivity) {
        AbstractC28901Pl r0;
        AnonymousClass1ZY r1;
        AnonymousClass1ZX r12;
        boolean A0N = C16700pc.A0N(r11, businessHubActivity);
        int i = 110;
        if (!TextUtils.isEmpty(r11.A01())) {
            i = 109;
        }
        AbstractC16710pd r13 = businessHubActivity.A0C;
        ((BusinessHubViewModel) r13.getValue()).A06.AKi(null, Integer.valueOf(A0N ? 1 : 0), Integer.valueOf(i), "business_hub", null);
        r13.getValue();
        String A01 = r11.A01();
        if (A01 != null || ((r0 = r11.A00) != null && (r1 = r0.A08) != null && (r1 instanceof AnonymousClass1ZX) && (r12 = (AnonymousClass1ZX) r1) != null && (A01 = r12.A0A) != null)) {
            businessHubActivity.startActivity(C14960mK.A0a(businessHubActivity, A01, null, false, false));
        }
    }

    public static /* synthetic */ void A03(AnonymousClass2DI r4, BusinessHubActivity businessHubActivity) {
        C16700pc.A0F(businessHubActivity, r4);
        BusinessHubViewModel businessHubViewModel = (BusinessHubViewModel) businessHubActivity.A0C.getValue();
        if (C16700pc.A0O(r4.A00(), "EXTERNALLY_DISABLED")) {
            businessHubViewModel.A08.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(r4, 47, businessHubViewModel));
            return;
        }
        C12990iw.A0P(businessHubViewModel.A0A).A0B(AnonymousClass617.A00(null));
        businessHubViewModel.A08.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(businessHubViewModel, 21));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f6, code lost:
        if (r0 == false) goto L_0x00af;
     */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A09(X.AnonymousClass2DI r7, com.whatsapp.payments.ui.BusinessHubActivity r8) {
        /*
        // Method dump skipped, instructions count: 522
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.BusinessHubActivity.A09(X.2DI, com.whatsapp.payments.ui.BusinessHubActivity):void");
    }

    public static /* synthetic */ void A0A(AnonymousClass2DI r12, BusinessHubActivity businessHubActivity, String str) {
        String A0X;
        int A0N = C16700pc.A0N(businessHubActivity, str);
        C16700pc.A0E(r12, 2);
        AbstractC16710pd r2 = businessHubActivity.A0C;
        ((BusinessHubViewModel) r2.getValue()).A06.AKi(null, Integer.valueOf(A0N), Integer.valueOf((int) C43951xu.A03), "business_hub", null);
        String A0X2 = C12960it.A0X(businessHubActivity, str, new Object[A0N], 0, R.string.remove_partner_account_confirmation_dialog_title);
        C16700pc.A0B(A0X2);
        if (C16700pc.A0O(r12.A00(), "EXTERNALLY_DISABLED")) {
            A0X = businessHubActivity.getString(R.string.remove_revoked_partner_account_confirmation_dialog_msg);
        } else {
            A0X = C12960it.A0X(businessHubActivity, str, new Object[A0N], 0, R.string.remove_partner_account_confirmation_dialog_msg);
        }
        C16700pc.A0B(A0X);
        int ACY = ((BusinessHubViewModel) r2.getValue()).A05.A02().ACY();
        String string = businessHubActivity.getString(R.string.remove);
        IDxCListenerShape4S0200000_2_I1 iDxCListenerShape4S0200000_2_I1 = new IDxCListenerShape4S0200000_2_I1(r12, 10, businessHubActivity);
        String string2 = businessHubActivity.getString(R.string.cancel);
        AlertDialog.Builder builder = new AlertDialog.Builder(businessHubActivity, ACY);
        builder.setMessage(A0X);
        builder.setTitle(A0X2);
        if (string != null) {
            builder.setPositiveButton(string, iDxCListenerShape4S0200000_2_I1);
        }
        if (string2 != null) {
            builder.setNegativeButton(string2, (DialogInterface.OnClickListener) null);
        }
        builder.create().show();
    }

    public static /* synthetic */ void A0B(BusinessHubActivity businessHubActivity) {
        C16700pc.A0E(businessHubActivity, 0);
        ((BusinessHubViewModel) businessHubActivity.A0C.getValue()).A06.AKi(null, C12960it.A0V(), 107, "business_hub", null);
        C17070qD r0 = businessHubActivity.A0A;
        if (r0 != null) {
            Intent ACd = r0.A02().ACd(businessHubActivity, "business", null);
            if (ACd != null) {
                businessHubActivity.startActivity(ACd);
                return;
            }
            return;
        }
        throw C16700pc.A06("paymentsManager");
    }

    public static /* synthetic */ void A0D(BusinessHubActivity businessHubActivity, AnonymousClass617 r6) {
        String str;
        C16700pc.A0E(businessHubActivity, 0);
        int ACY = ((BusinessHubViewModel) businessHubActivity.A0C.getValue()).A05.A02().ACY();
        if (r6 != null) {
            int i = r6.A00;
            if (i == 0) {
                businessHubActivity.AaN();
                businessHubActivity.finish();
            } else if (i == 1) {
                businessHubActivity.AaN();
                Throwable th = r6.A02;
                if (th == null || (str = th.getMessage()) == null) {
                    str = businessHubActivity.getString(R.string.payments_generic_error);
                    C16700pc.A0B(str);
                }
                String string = businessHubActivity.getString(R.string.ok);
                AlertDialog.Builder builder = new AlertDialog.Builder(businessHubActivity, ACY);
                builder.setMessage(str);
                if (string != null) {
                    builder.setPositiveButton(string, (DialogInterface.OnClickListener) null);
                }
                builder.create().show();
            } else if (i == 2) {
                businessHubActivity.A2C(R.string.register_wait_message);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.business_hub);
        A1e((Toolbar) findViewById(R.id.pay_service_toolbar));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I(null);
            A1U.A0M(true);
            int A00 = AnonymousClass00T.A00(this, R.color.fb_pay_hub_icon_tint);
            Drawable A04 = AnonymousClass00T.A04(this, R.drawable.ic_close);
            if (A04 != null) {
                A1U.A0D(AnonymousClass2GE.A04(A04, A00));
            }
        }
        View findViewById = findViewById(R.id.payment_partner_container);
        this.A02 = (ImageView) C16700pc.A02(findViewById, R.id.payment_business_icon);
        this.A04 = (TextView) C16700pc.A02(findViewById, R.id.business_account_name);
        this.A05 = (TextView) C16700pc.A02(findViewById, R.id.business_account_status);
        this.A01 = (ViewGroup) C16700pc.A02(findViewById, R.id.view_dashboard_row);
        this.A06 = (TextView) C16700pc.A02(findViewById, R.id.payment_partner_dashboard);
        View findViewById2 = findViewById(R.id.payout_method_container);
        this.A03 = (ImageView) C16700pc.A02(findViewById2, R.id.payout_bank_icon);
        this.A07 = (TextView) C16700pc.A02(findViewById2, R.id.payout_bank_name);
        this.A08 = (TextView) C16700pc.A02(findViewById2, R.id.payout_bank_status);
        C16700pc.A02(findViewById2, R.id.warning_container).setVisibility(8);
        View A02 = C16700pc.A02(findViewById(R.id.partner_support_container), R.id.request_dyi_report_action);
        C12970iu.A0M(this, R.id.request_payment_account_info_text).setText(R.string.download_your_info);
        A02.setOnClickListener(new View.OnClickListener() { // from class: X.3lU
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BusinessHubActivity.A0B(BusinessHubActivity.this);
            }
        });
        int A002 = AnonymousClass00T.A00(this, R.color.icon_secondary);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.request_payment_account_info_icon), A002);
        View findViewById3 = findViewById(R.id.delete_payments_account_action);
        C16700pc.A0B(findViewById3);
        ViewGroup viewGroup = (ViewGroup) findViewById3;
        this.A00 = viewGroup;
        if (viewGroup == null) {
            throw C16700pc.A06("removeAccountRow");
        }
        AnonymousClass2GE.A07(C12970iu.A0L(viewGroup, R.id.delete_payments_account_icon), A002);
        ViewGroup viewGroup2 = this.A00;
        if (viewGroup2 == null) {
            throw C16700pc.A06("removeAccountRow");
        }
        this.A09 = (TextView) C16700pc.A02(viewGroup2, R.id.delete_payments_account_label);
        IDxObserverShape4S0100000_2_I1 iDxObserverShape4S0100000_2_I1 = new IDxObserverShape4S0100000_2_I1(this, 51);
        AbstractC16710pd r2 = this.A0C;
        C12990iw.A0P(((BusinessHubViewModel) r2.getValue()).A09).A05(this, iDxObserverShape4S0100000_2_I1);
        C12960it.A17(this, C12990iw.A0P(((BusinessHubViewModel) r2.getValue()).A0A), 52);
        ((BusinessHubViewModel) r2.getValue()).A04(true);
    }
}
