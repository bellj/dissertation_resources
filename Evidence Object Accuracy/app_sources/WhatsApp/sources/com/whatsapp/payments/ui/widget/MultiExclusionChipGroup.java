package com.whatsapp.payments.ui.widget;

import X.AbstractC136116Lb;
import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12970iu;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.widget.MultiExclusionChip;
import com.whatsapp.payments.ui.widget.MultiExclusionChipGroup;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes4.dex */
public class MultiExclusionChipGroup extends ChipGroup implements AnonymousClass004 {
    public AbstractC136116Lb A00;
    public AnonymousClass2P7 A01;
    public Map A02;
    public Set A03;
    public boolean A04;

    public MultiExclusionChipGroup(Context context) {
        super(context);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        this.A03 = C12970iu.A12();
        this.A02 = C12970iu.A11();
    }

    public MultiExclusionChipGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A03 = C12970iu.A12();
        this.A02 = C12970iu.A11();
    }

    public MultiExclusionChipGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        this.A03 = C12970iu.A12();
        this.A02 = C12970iu.A11();
    }

    public MultiExclusionChipGroup(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
    }

    public void A01(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            MultiExclusionChip multiExclusionChip = (MultiExclusionChip) it.next();
            this.A02.put(multiExclusionChip, list);
            multiExclusionChip.setCheckable(true);
            multiExclusionChip.setClickable(true);
            super.addView(multiExclusionChip);
            multiExclusionChip.A00 = new CompoundButton.OnCheckedChangeListener(multiExclusionChip, this) { // from class: X.65H
                public final /* synthetic */ MultiExclusionChip A00;
                public final /* synthetic */ MultiExclusionChipGroup A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    MultiExclusionChipGroup multiExclusionChipGroup = this.A01;
                    MultiExclusionChip multiExclusionChip2 = this.A00;
                    Map map = multiExclusionChipGroup.A02;
                    if (map.containsKey(multiExclusionChip2)) {
                        boolean isChecked = multiExclusionChip2.isChecked();
                        int i = R.dimen.payment_filter_unchecked_stroke_width;
                        if (isChecked) {
                            i = R.dimen.payment_filter_checked_stroke_width;
                        }
                        multiExclusionChip2.setChipStrokeWidthResource(i);
                        boolean isChecked2 = multiExclusionChip2.isChecked();
                        int i2 = R.dimen.payment_filter_unchecked_text_start_padding;
                        if (isChecked2) {
                            i2 = R.dimen.payment_filter_checked_text_start_padding;
                        }
                        multiExclusionChip2.setTextStartPaddingResource(i2);
                        List list2 = (List) map.get(multiExclusionChip2);
                        if (list2 != null) {
                            for (int i3 = 0; i3 < list2.size(); i3++) {
                                Chip chip = (Chip) list2.get(i3);
                                if (chip != multiExclusionChip2) {
                                    chip.setClickable(!multiExclusionChip2.isChecked());
                                    chip.setCheckable(!multiExclusionChip2.isChecked());
                                    chip.setVisibility(C13010iy.A00(multiExclusionChip2.isChecked() ? 1 : 0));
                                }
                            }
                        }
                    }
                    Set set = multiExclusionChipGroup.A03;
                    if (z) {
                        set.add(multiExclusionChip2);
                    } else {
                        set.remove(multiExclusionChip2);
                    }
                    AbstractC136116Lb r0 = multiExclusionChipGroup.A00;
                    if (r0 != null) {
                        C133886Cm r02 = (C133886Cm) r0;
                        PaymentTransactionHistoryActivity paymentTransactionHistoryActivity = r02.A00;
                        MultiExclusionChip multiExclusionChip3 = r02.A01;
                        MultiExclusionChip multiExclusionChip4 = r02.A02;
                        MultiExclusionChip multiExclusionChip5 = r02.A03;
                        MultiExclusionChip multiExclusionChip6 = r02.A04;
                        if (paymentTransactionHistoryActivity.A0S) {
                            C30941Zk r1 = paymentTransactionHistoryActivity.A0V;
                            r1.A06 = set.contains(multiExclusionChip3);
                            if (set.contains(multiExclusionChip4)) {
                                r1.A00 = new C30951Zl();
                            }
                        }
                        if (paymentTransactionHistoryActivity.A0N) {
                            C30941Zk r12 = paymentTransactionHistoryActivity.A0V;
                            r12.A02 = set.contains(multiExclusionChip5);
                            r12.A03 = set.contains(multiExclusionChip6);
                        }
                        paymentTransactionHistoryActivity.A2f();
                    }
                }
            };
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setOnSelectionChangedListener(AbstractC136116Lb r1) {
        this.A00 = r1;
    }
}
