package com.whatsapp.payments.ui.fragment;

import X.AnonymousClass015;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass4OZ;
import X.AnonymousClass60Y;
import X.C117295Zj;
import X.C117305Zk;
import X.C123475nD;
import X.C127445uV;
import X.C128375w0;
import X.C12960it;
import X.C134156Dn;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviAddDebitCardSheet extends Hilt_NoviAddDebitCardSheet {
    public AnonymousClass60Y A00;
    public C134156Dn A01;
    public C127445uV A02;
    public C128375w0 A03;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_add_card_description);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        AnonymousClass60Y.A02(this.A00, "NAVIGATION_END", "ADD_MONEY", "ADD_DC_INFO", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass015 A00 = new AnonymousClass02A(A0C()).A00(C123475nD.class);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.send_money_review_header_close), this, 121);
        C134156Dn r2 = new C134156Dn();
        this.A01 = r2;
        r2.AYL(C117305Zk.A05(view, r2, R.id.novi_withdraw_review_confirm, R.id.novi_withdraw_review_confirm_inflated));
        C127445uV r3 = new C127445uV(new IDxCListenerShape2S0200000_3_I1(this, 28, A00), A0I(R.string.novi_deposit_add_debit_card_label), true);
        this.A02 = r3;
        this.A01.A6Q(new AnonymousClass4OZ(2, r3));
        AnonymousClass60Y.A02(this.A00, "NAVIGATION_START", "ADD_MONEY", "ADD_DC_INFO", "SCREEN");
    }
}
