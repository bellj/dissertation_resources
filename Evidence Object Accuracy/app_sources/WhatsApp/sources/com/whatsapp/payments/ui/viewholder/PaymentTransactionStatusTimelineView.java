package com.whatsapp.payments.ui.viewholder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/* loaded from: classes4.dex */
public class PaymentTransactionStatusTimelineView extends ListView {
    public PaymentTransactionStatusTimelineView(Context context) {
        super(context);
    }

    public PaymentTransactionStatusTimelineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PaymentTransactionStatusTimelineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.widget.ListView, android.widget.AbsListView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }
}
