package com.whatsapp.payments.ui;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviSelfieCameraOverlay extends View implements AnonymousClass004 {
    public int A00;
    public int A01;
    public Bitmap A02;
    public Paint A03;
    public RectF A04;
    public AnonymousClass2P7 A05;
    public boolean A06;

    public NoviSelfieCameraOverlay(Context context) {
        super(context);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
        A00();
    }

    public NoviSelfieCameraOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        A00();
    }

    public NoviSelfieCameraOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
        A00();
    }

    public NoviSelfieCameraOverlay(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
        A00();
    }

    public NoviSelfieCameraOverlay(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        this.A03 = new Paint(1);
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        int i3 = (int) (((float) i) * 0.7f);
        this.A03.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.A03.setAntiAlias(true);
        int dimension = (int) getResources().getDimension(R.dimen.novi_selfie_camera_overlay_top_margin);
        this.A01 = dimension;
        int i4 = (i - i3) >> 1;
        this.A00 = dimension + i3;
        this.A02 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        this.A04 = new RectF((float) i4, (float) this.A01, (float) (i4 + i3), (float) this.A00);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        this.A02.eraseColor(0);
        canvas.drawColor(getResources().getColor(R.color.novi_selfie_overlay_color));
        this.A03.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(this.A04, (float) ((int) getResources().getDimension(R.dimen.novi_selfie_camera_overlay_radius)), (float) ((int) getResources().getDimension(R.dimen.novi_selfie_camera_overlay_radius)), this.A03);
        canvas.drawBitmap(this.A02, 0.0f, 0.0f, (Paint) null);
        super.onDraw(canvas);
    }
}
