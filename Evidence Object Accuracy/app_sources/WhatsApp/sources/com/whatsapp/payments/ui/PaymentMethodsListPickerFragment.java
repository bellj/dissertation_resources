package com.whatsapp.payments.ui;

import X.AbstractC1311261j;
import X.AbstractC1311861p;
import X.AbstractC28901Pl;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass102;
import X.AnonymousClass1ZY;
import X.AnonymousClass4UZ;
import X.AnonymousClass6M1;
import X.C117295Zj;
import X.C117315Zl;
import X.C117615aH;
import X.C120115fe;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1311161i;
import X.C14900mE;
import X.C17070qD;
import X.C248217a;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public class PaymentMethodsListPickerFragment extends Hilt_PaymentMethodsListPickerFragment implements AbstractC1311261j {
    public C14900mE A00;
    public AnonymousClass018 A01;
    public AnonymousClass102 A02;
    public AnonymousClass4UZ A03 = new C120115fe(this);
    public C248217a A04;
    public C17070qD A05;
    public AnonymousClass6M1 A06;
    public C117615aH A07;
    public AbstractC1311861p A08;

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return true;
    }

    public static PaymentMethodsListPickerFragment A00(List list) {
        PaymentMethodsListPickerFragment paymentMethodsListPickerFragment = new PaymentMethodsListPickerFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelableArrayList("arg_methods", C12980iv.A0x(list));
        paymentMethodsListPickerFragment.A0U(A0D);
        return paymentMethodsListPickerFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_method_picker_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A04.A04(this.A03);
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            r0.onDestroy();
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A04.A03(this.A03);
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            r0.onCreate();
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        View view2;
        View AAd;
        ArrayList parcelableArrayList = A03().getParcelableArrayList("arg_methods");
        AnonymousClass009.A05(parcelableArrayList);
        ListView listView = (ListView) view.findViewById(R.id.methods_list);
        AbstractC1311861p r1 = this.A08;
        if (r1 != null) {
            r1.AFP(A04(), null);
        }
        C117615aH r0 = new C117615aH(view.getContext(), this.A01, this.A05, this);
        this.A07 = r0;
        r0.A02 = parcelableArrayList;
        r0.notifyDataSetChanged();
        listView.setAdapter((ListAdapter) this.A07);
        if (this.A08 != null) {
            view2 = A04().inflate(R.layout.add_payment_method_row, (ViewGroup) null);
            C117295Zj.A0m(view2, R.id.add_new_account_icon, AnonymousClass00T.A00(view.getContext(), R.color.settings_icon));
            C12970iu.A19(view.getContext(), C12960it.A0I(view2, R.id.add_new_account_text), this.A08.AAc());
            listView.addFooterView(view2);
        } else {
            view2 = null;
        }
        ViewGroup A04 = C117315Zl.A04(view, R.id.additional_bottom_row);
        AbstractC1311861p r12 = this.A08;
        if (!(r12 == null || (AAd = r12.AAd(A04(), null)) == null)) {
            A04.addView(AAd);
            C117295Zj.A0n(A04, this, 102);
        }
        if (this.A08 != null) {
            FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(view, R.id.footer_view);
            View AD5 = this.A08.AD5(A04(), frameLayout);
            if (AD5 != null) {
                frameLayout.setVisibility(0);
                frameLayout.addView(AD5);
            } else {
                frameLayout.setVisibility(8);
            }
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(view2, listView, this) { // from class: X.65E
            public final /* synthetic */ View A00;
            public final /* synthetic */ ListView A01;
            public final /* synthetic */ PaymentMethodsListPickerFragment A02;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view3, int i, long j) {
                PaymentMethodsListPickerFragment paymentMethodsListPickerFragment = this.A02;
                ListView listView2 = this.A01;
                View view4 = this.A00;
                if (view4 == null || i != listView2.getPositionForView(view4)) {
                    AnonymousClass01E A08 = paymentMethodsListPickerFragment.A08();
                    PaymentBottomSheet paymentBottomSheet = (PaymentBottomSheet) paymentMethodsListPickerFragment.A0D;
                    AbstractC28901Pl A082 = C117315Zl.A08(paymentMethodsListPickerFragment.A07.A02, i - listView2.getHeaderViewsCount());
                    AbstractC1311861p r02 = paymentMethodsListPickerFragment.A08;
                    if (r02 != null && !r02.AdN(A082)) {
                        if (A08 instanceof AnonymousClass6M1) {
                            ((AnonymousClass6M1) A08).ATY(A082);
                            if (paymentBottomSheet != null) {
                                paymentBottomSheet.A1L(A08);
                                return;
                            }
                            return;
                        }
                        AnonymousClass6M1 r03 = paymentMethodsListPickerFragment.A06;
                        if (r03 != null) {
                            r03.ATY(A082);
                            if (paymentBottomSheet != null) {
                                paymentBottomSheet.A1K();
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                AbstractC1311861p r04 = paymentMethodsListPickerFragment.A08;
                if (r04 != null) {
                    r04.ALx();
                }
            }
        });
        View findViewById = view.findViewById(R.id.back);
        findViewById.setVisibility(0);
        C117295Zj.A0n(findViewById, this, 101);
        View findViewById2 = view.findViewById(R.id.icon_lock);
        AbstractC1311861p r02 = this.A08;
        if (r02 == null || r02.AdZ()) {
            findViewById2.setVisibility(0);
        } else {
            findViewById2.setVisibility(4);
        }
    }

    @Override // X.AbstractC1311261j
    public int AEM(AbstractC28901Pl r2) {
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            return r0.AEM(r2);
        }
        return 0;
    }

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEP(AbstractC28901Pl r4) {
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            String AEP = r0.AEP(r4);
            if (!TextUtils.isEmpty(AEP)) {
                return AEP;
            }
        }
        AnonymousClass1ZY r02 = r4.A08;
        AnonymousClass009.A05(r02);
        if (!r02.A0A()) {
            return A0I(R.string.payment_method_unverified);
        }
        if (C1311161i.A06(A01(), r4) != null) {
            return C1311161i.A06(A01(), r4);
        }
        return "";
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r2) {
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            return r0.AEQ(r2);
        }
        return null;
    }

    @Override // X.AbstractC1311261j
    public boolean AdN(AbstractC28901Pl r3) {
        AbstractC1311861p r0 = this.A08;
        return r0 == null || r0.AdN(r3);
    }

    @Override // X.AbstractC1311261j
    public boolean AdV() {
        AbstractC1311861p r0 = this.A08;
        return r0 != null && r0.AdV();
    }

    @Override // X.AbstractC1311261j
    public void Adj(AbstractC28901Pl r2, PaymentMethodRow paymentMethodRow) {
        AbstractC1311861p r0 = this.A08;
        if (r0 != null) {
            r0.Adj(r2, paymentMethodRow);
        }
    }
}
