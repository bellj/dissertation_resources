package com.whatsapp.payments.ui.mapper.register;

import X.AbstractActivityC119355da;
import X.ActivityC13790kL;
import X.AnonymousClass1WK;
import X.AnonymousClass3FW;
import X.AnonymousClass46W;
import X.AnonymousClass46X;
import X.AnonymousClass46Y;
import X.AnonymousClass4EU;
import X.AnonymousClass6BE;
import X.C004802e;
import X.C124935qP;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C1329668y;
import X.C16700pc;
import X.C620533w;
import X.C72033du;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class IndiaUpiMapperLinkActivity extends AbstractActivityC119355da {
    public TextView A00;
    public C1329668y A01;
    public AnonymousClass6BE A02;
    public IndiaUpiMapperLinkViewModel A03;
    public boolean A04;
    public final AnonymousClass1WK A05 = new C72033du(this);

    public static /* synthetic */ void A02(IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity) {
        C16700pc.A0E(indiaUpiMapperLinkActivity, 0);
        indiaUpiMapperLinkActivity.A2e().AKg(C12960it.A0V(), C12970iu.A0h(), "error", ActivityC13790kL.A0W(indiaUpiMapperLinkActivity));
        indiaUpiMapperLinkActivity.finish();
    }

    public static /* synthetic */ void A03(IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity) {
        C16700pc.A0E(indiaUpiMapperLinkActivity, 0);
        indiaUpiMapperLinkActivity.A04 = true;
        TextView textView = indiaUpiMapperLinkActivity.A00;
        if (textView != null) {
            textView.setText(indiaUpiMapperLinkActivity.getResources().getString(R.string.mapper_adding_upi_number_port_title));
            indiaUpiMapperLinkActivity.A2e().AKg(1, C12980iv.A0k(), "alias_switch_confirm_dialog", ActivityC13790kL.A0W(indiaUpiMapperLinkActivity));
            IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel = indiaUpiMapperLinkActivity.A03;
            if (indiaUpiMapperLinkViewModel != null) {
                indiaUpiMapperLinkViewModel.A04(true);
                return;
            }
            throw C16700pc.A06("indiaUpiMapperLinkViewModel");
        }
        throw C16700pc.A06("titleTextView");
    }

    public static /* synthetic */ void A09(IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity) {
        C16700pc.A0E(indiaUpiMapperLinkActivity, 0);
        indiaUpiMapperLinkActivity.A2e().AKg(C12960it.A0V(), C12970iu.A0h(), "alias_switch_confirm_dialog", ActivityC13790kL.A0W(indiaUpiMapperLinkActivity));
        indiaUpiMapperLinkActivity.finish();
    }

    public static /* synthetic */ void A0A(IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity, AnonymousClass4EU r10) {
        Intent A0D;
        String str;
        String str2;
        String stringExtra;
        String str3;
        C16700pc.A0E(indiaUpiMapperLinkActivity, 0);
        String str4 = null;
        if (r10 instanceof C620533w) {
            C004802e A0S = C12980iv.A0S(indiaUpiMapperLinkActivity);
            A0S.A0B(false);
            C620533w r102 = (C620533w) r10;
            String str5 = r102.A02;
            String str6 = "";
            if (str5 == null) {
                str5 = str6;
            }
            A0S.setTitle(str5);
            String str7 = r102.A01;
            if (str7 != null) {
                str6 = str7;
            }
            A0S.A0A(str6);
            C12970iu.A1K(A0S, indiaUpiMapperLinkActivity, 45, R.string.close);
            C12970iu.A1J(A0S);
            AnonymousClass3FW r4 = new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
            r4.A01("payments_error_code", String.valueOf(r102.A00));
            r4.A01("payments_error_text", str5);
            AnonymousClass6BE A2e = indiaUpiMapperLinkActivity.A2e();
            Integer A0j = C12980iv.A0j();
            if (indiaUpiMapperLinkActivity.A04) {
                str3 = "alias_switch_in_progress";
            } else {
                str3 = "alias_in_progress";
            }
            Intent intent = indiaUpiMapperLinkActivity.getIntent();
            if (intent != null) {
                str4 = intent.getStringExtra("extra_referral_screen");
            }
            A2e.AKi(r4, A0j, 51, str3, str4);
        } else if (r10 instanceof AnonymousClass46W) {
            C004802e A0S2 = C12980iv.A0S(indiaUpiMapperLinkActivity);
            A0S2.A0B(false);
            A0S2.A07(R.string.mapper_porting_dialog_title);
            A0S2.A06(R.string.mapper_porting_dialog_desc);
            C12970iu.A1L(A0S2, indiaUpiMapperLinkActivity, 46, R.string.permission_continue);
            C12970iu.A1K(A0S2, indiaUpiMapperLinkActivity, 47, R.string.cancel);
            C12970iu.A1J(A0S2);
            AnonymousClass6BE A2e2 = indiaUpiMapperLinkActivity.A2e();
            Intent intent2 = indiaUpiMapperLinkActivity.getIntent();
            if (intent2 == null) {
                stringExtra = null;
            } else {
                stringExtra = intent2.getStringExtra("extra_referral_screen");
            }
            A2e2.AKg(0, null, "alias_switch_confirm_dialog", stringExtra);
        } else {
            if (r10 instanceof AnonymousClass46X) {
                A0D = C12990iw.A0D(indiaUpiMapperLinkActivity, IndiaUpiMapperConfirmationActivity.class);
                A0D.putExtra("extra_payment_name", indiaUpiMapperLinkActivity.getIntent().getParcelableExtra("extra_payment_name"));
                if (indiaUpiMapperLinkActivity.A04) {
                    str2 = "alias_switch_in_progress";
                } else {
                    str2 = "alias_in_progress";
                }
                A0D.putExtra("extra_referral_screen", str2);
                A0D.addFlags(33554432);
            } else if (r10 instanceof AnonymousClass46Y) {
                A0D = C12990iw.A0D(indiaUpiMapperLinkActivity, IndiaUpiMapperPendingActivity.class);
                A0D.addFlags(33554432);
                if (indiaUpiMapperLinkActivity.A04) {
                    str = "alias_switch_in_progress";
                } else {
                    str = "alias_in_progress";
                }
                A0D.putExtra("extra_referral_screen", str);
            } else {
                throw C12960it.A0U("Unexpected value for indiaUpiMapperLinkEvent");
            }
            indiaUpiMapperLinkActivity.A2G(A0D, true);
        }
    }

    public final AnonymousClass6BE A2e() {
        AnonymousClass6BE r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        String str;
        super.onBackPressed();
        AnonymousClass6BE A2e = A2e();
        Integer A0V = C12960it.A0V();
        if (this.A04) {
            str = "alias_switch_in_progress";
        } else {
            str = "alias_in_progress";
        }
        A2e.AKg(A0V, A0V, str, ActivityC13790kL.A0W(this));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        String stringExtra;
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
        setContentView(R.layout.india_upi_mapper_link_activity);
        View findViewById = findViewById(R.id.mapper_link_title);
        C16700pc.A0B(findViewById);
        TextView textView = (TextView) findViewById;
        C16700pc.A0E(textView, 0);
        this.A00 = textView;
        Object AJ3 = this.A05.AJ3();
        C16700pc.A0B(AJ3);
        IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel = (IndiaUpiMapperLinkViewModel) AJ3;
        C16700pc.A0E(indiaUpiMapperLinkViewModel, 0);
        this.A03 = indiaUpiMapperLinkViewModel;
        if (bundle == null) {
            this.A04 = false;
            TextView textView2 = this.A00;
            if (textView2 != null) {
                textView2.setText(getResources().getString(R.string.mapper_adding_upi_number_add_title));
                IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel2 = this.A03;
                if (indiaUpiMapperLinkViewModel2 != null) {
                    indiaUpiMapperLinkViewModel2.A04(false);
                } else {
                    throw C16700pc.A06("indiaUpiMapperLinkViewModel");
                }
            } else {
                throw C16700pc.A06("titleTextView");
            }
        }
        C124935qP.A00(this);
        IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel3 = this.A03;
        if (indiaUpiMapperLinkViewModel3 != null) {
            indiaUpiMapperLinkViewModel3.A05.A05(this, new IDxObserverShape4S0100000_2_I1(this, 53));
            onConfigurationChanged(C12980iv.A0H(this));
            AnonymousClass6BE A2e = A2e();
            if (this.A04) {
                str = "alias_switch_in_progress";
            } else {
                str = "alias_in_progress";
            }
            Intent intent = getIntent();
            if (intent == null) {
                stringExtra = null;
            } else {
                stringExtra = intent.getStringExtra("extra_referral_screen");
            }
            A2e.AKg(0, null, str, stringExtra);
            return;
        }
        throw C16700pc.A06("indiaUpiMapperLinkViewModel");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        C16700pc.A0E(menuItem, 0);
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass6BE A2e = A2e();
            Integer A0V = C12960it.A0V();
            Integer A0h = C12970iu.A0h();
            if (this.A04) {
                str = "alias_switch_in_progress";
            } else {
                str = "alias_in_progress";
            }
            A2e.AKg(A0V, A0h, str, ActivityC13790kL.A0W(this));
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
