package com.whatsapp.payments.ui.widget;

import X.AnonymousClass028;
import X.C117305Zk;
import X.C12960it;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class PaymentDescriptionRow extends LinearLayout {
    public View A00;
    public TextView A01;
    public TextView A02;

    public PaymentDescriptionRow(Context context) {
        super(context);
        A00();
    }

    public PaymentDescriptionRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public PaymentDescriptionRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public PaymentDescriptionRow(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
    }

    public void A00() {
        C117305Zk.A14(C12960it.A0E(this), this, R.layout.payment_description_row);
        this.A00 = AnonymousClass028.A0D(this, R.id.payment_description_row_container);
        this.A01 = C12960it.A0I(this, R.id.payment_description_hint);
        this.A02 = C12960it.A0I(this, R.id.payment_description_text);
    }

    public void A01(String str) {
        Context context;
        int i;
        boolean isEmpty = TextUtils.isEmpty(str);
        TextView textView = this.A02;
        if (isEmpty) {
            textView.setVisibility(8);
            this.A02.setText(str);
            context = getContext();
            i = R.string.p2p_description_hint_without_description;
        } else {
            textView.setVisibility(0);
            this.A02.setText(str);
            context = getContext();
            i = R.string.p2p_description_hint;
        }
        this.A01.setText(context.getString(i));
    }

    public int getLayoutRes() {
        return R.layout.payment_description_row;
    }
}
