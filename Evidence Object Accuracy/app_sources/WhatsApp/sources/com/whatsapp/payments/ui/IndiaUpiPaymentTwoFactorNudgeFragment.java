package com.whatsapp.payments.ui;

import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass6BE;
import X.AnonymousClass6LV;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14900mE;
import X.C18600si;
import X.C30931Zj;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentTwoFactorNudgeFragment extends Hilt_IndiaUpiPaymentTwoFactorNudgeFragment {
    public AnonymousClass12P A00;
    public C14900mE A01;
    public AnonymousClass01d A02;
    public C18600si A03;
    public AnonymousClass6BE A04;
    public AnonymousClass6LV A05;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A05 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_payment_two_factor_nudge_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.continue_button), this, 54);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.close), this, 53);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.later_button), this, 52);
        C18600si r4 = this.A03;
        long A00 = r4.A01.A00();
        C12970iu.A1C(C117295Zj.A05(r4), "payments_last_two_factor_nudge_time", A00);
        C30931Zj r1 = r4.A02;
        StringBuilder A0k = C12960it.A0k("updateLastTwoFactorNudgeTimeMilli to: ");
        A0k.append(A00);
        C117295Zj.A1F(r1, A0k);
        C18600si r3 = this.A03;
        int A01 = C12970iu.A01(r3.A01(), "payments_two_factor_nudge_count") + 1;
        C12970iu.A1B(C117295Zj.A05(r3), "payments_two_factor_nudge_count", A01);
        r3.A02.A06(C12960it.A0W(A01, "updateTwoFactorNudgeCount to: "));
        this.A04.AKg(C12980iv.A0i(), null, "two_factor_nudge_prompt", null);
    }
}
