package com.whatsapp.payments.ui;

import X.AbstractC136366Mg;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass19M;
import X.AnonymousClass367;
import X.AnonymousClass6BE;
import X.C100654mG;
import X.C117295Zj;
import X.C117325Zm;
import X.C117475a1;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1317163v;
import X.C14900mE;
import X.C16630pM;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;
import com.whatsapp.components.Button;

/* loaded from: classes4.dex */
public class IndiaUpiEditTransactionDescriptionFragment extends Hilt_IndiaUpiEditTransactionDescriptionFragment {
    public C14900mE A00;
    public WaEditText A01;
    public WaTextView A02;
    public Button A03;
    public AnonymousClass01d A04;
    public AnonymousClass018 A05;
    public AnonymousClass19M A06;
    public AnonymousClass6BE A07;
    public AbstractC136366Mg A08;
    public C16630pM A09;
    public String A0A;

    public static IndiaUpiEditTransactionDescriptionFragment A00(String str) {
        IndiaUpiEditTransactionDescriptionFragment indiaUpiEditTransactionDescriptionFragment = new IndiaUpiEditTransactionDescriptionFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("arg_payment_description", str);
        indiaUpiEditTransactionDescriptionFragment.A0U(A0D);
        return indiaUpiEditTransactionDescriptionFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_edit_payment_description);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String string = A03().getString("arg_payment_description");
        AnonymousClass009.A05(string);
        this.A0A = string;
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.common_action_bar_header_back), this, 35);
        this.A03 = (Button) AnonymousClass028.A0D(view, R.id.save_description_button);
        this.A02 = C12960it.A0N(view, R.id.payment_description_error);
        WaEditText waEditText = (WaEditText) AnonymousClass028.A0D(view, R.id.payment_description_text);
        this.A01 = waEditText;
        waEditText.requestFocus();
        this.A01.addTextChangedListener(new C1317163v(this));
        AnonymousClass367 r3 = new AnonymousClass367(this.A01, C12960it.A0I(view, R.id.counter), this.A04, this.A05, this.A06, this.A09, 50, 0, true);
        this.A01.setFilters(new InputFilter[]{new C100654mG(50)});
        this.A01.addTextChangedListener(r3);
        if (!TextUtils.isEmpty(this.A0A) && this.A01.getText() != null) {
            this.A01.setText(this.A0A);
            WaEditText waEditText2 = this.A01;
            waEditText2.setSelection(waEditText2.getText().length());
        }
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.save_description_button), this, 34);
        TextView A0I = C12960it.A0I(view, R.id.payment_description_disclaimer_text);
        String A0I2 = A0I(R.string.upi_payment_description_learn_more_link);
        String A0q = C12970iu.A0q(this, A0I2, new Object[1], 0, R.string.upi_payment_description_disclaimer);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A0q);
        C117325Zm.A04(spannableStringBuilder, new C117475a1(this), A0q, A0I2);
        A0I.setText(spannableStringBuilder);
        A0I.setLinksClickable(true);
        C12990iw.A1F(A0I);
        this.A07.AKi(null, 0, null, "payment_description", null);
    }
}
