package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121515iQ;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121665jA;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2SP;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1311161i;
import X.C1317463y;
import X.C27531Hw;
import X.C30861Zc;
import X.C30931Zj;
import X.View$OnKeyListenerC1318964n;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.payments.ui.IndiaUpiDebitCardVerificationActivity;
import java.util.Calendar;

/* loaded from: classes4.dex */
public class IndiaUpiDebitCardVerificationActivity extends AbstractActivityC121515iQ {
    public int A00;
    public int A01;
    public EditText A02;
    public EditText A03;
    public EditText A04;
    public EditText A05;
    public TextView A06;
    public Button A07;
    public C30861Zc A08;
    public Integer A09;
    public boolean A0A;
    public final C30931Zj A0B;

    public IndiaUpiDebitCardVerificationActivity() {
        this(0);
        this.A0B = C117315Zl.A0D("IndiaUpiDebitCardVerificationActivity");
    }

    public IndiaUpiDebitCardVerificationActivity(int i) {
        this.A0A = false;
        C117295Zj.A0p(this, 44);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
        }
    }

    public final int A3I() {
        return (TextUtils.isEmpty(C117295Zj.A0T(this.A02)) || C117295Zj.A0T(this.A02).length() != 2 || TextUtils.isEmpty(C117295Zj.A0T(this.A03)) || C117295Zj.A0T(this.A03).length() != 4) ? 1 : 0;
    }

    public final void A3J() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(250);
        this.A06.startAnimation(alphaAnimation);
        this.A06.setVisibility(0);
    }

    public final void A3K() {
        if (A3L(this.A00, this.A01, true)) {
            C30861Zc r4 = this.A08;
            StringBuilder A0h = C12960it.A0h();
            A0h.append(C117295Zj.A0T(this.A02));
            String A0d = C12960it.A0d(C117295Zj.A0T(this.A03), A0h);
            String A0T = C117295Zj.A0T(this.A04);
            String A0T2 = C117295Zj.A0T(this.A05);
            ((AbstractActivityC121515iQ) this).A00 = r4;
            ((AbstractActivityC121515iQ) this).A04 = A0d;
            ((AbstractActivityC121515iQ) this).A02 = A0T;
            ((AbstractActivityC121515iQ) this).A03 = A0T2;
            A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
            C30931Zj r3 = ((AbstractActivityC121515iQ) this).A07;
            StringBuilder A0k = C12960it.A0k("onResume with states: ");
            A0k.append(((AbstractActivityC121545iU) this).A06);
            C117295Zj.A1F(r3, A0k);
            if (!((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge") && ((AbstractActivityC121665jA) this).A0B.A05().A00 == null) {
                ((AbstractActivityC121545iU) this).A06.A03("upi-get-challenge");
                A36();
            } else if (!((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge")) {
                A3A();
            }
        }
        AnonymousClass2SP A02 = ((AbstractActivityC121665jA) this).A0D.A02(1, C12980iv.A0k(), "enter_debit_card", null);
        A02.A0E = this.A09;
        ((AbstractActivityC121665jA) this).A05.A07(A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0112, code lost:
        if (r7 > 12) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0121, code lost:
        if (r5 >= r10) goto L_0x0123;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x012c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A3L(int r9, int r10, boolean r11) {
        /*
        // Method dump skipped, instructions count: 328
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiDebitCardVerificationActivity.A3L(int, int, boolean):boolean");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "enter_debit_card", null);
    }

    @Override // X.AbstractActivityC121515iQ, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        setContentView(R.layout.india_upi_payment_bank_card_verification);
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            A0K.A0A(R.string.payments_activity_title);
            A0K.A0M(true);
        }
        C30861Zc r0 = (C30861Zc) getIntent().getParcelableExtra("extra_bank_account");
        if (r0 == null) {
            this.A0B.A05("Bank account info is null, finishing");
            finish();
            return;
        }
        this.A08 = r0;
        C12970iu.A0M(this, R.id.add_card_number_label).setText(C12960it.A0X(this, C1311161i.A07(r0), new Object[1], 0, R.string.payments_debit_card_verif_title));
        ImageView A06 = C117305Zk.A06(this, R.id.issuer_bank_logo);
        Bitmap A05 = this.A08.A05();
        if (A05 != null) {
            A06.setImageBitmap(A05);
        } else {
            A06.setImageResource(R.drawable.bank_logo_placeholder);
        }
        Button button = (Button) findViewById(R.id.confirm_button);
        this.A07 = button;
        button.setEnabled(false);
        C117295Zj.A0n(this.A07, this, 33);
        EditText editText = (EditText) findViewById(R.id.add_card_number1);
        this.A02 = editText;
        C27531Hw.A06(editText);
        EditText editText2 = (EditText) findViewById(R.id.add_card_number2);
        this.A03 = editText2;
        C27531Hw.A06(editText2);
        this.A04 = (EditText) findViewById(R.id.add_card_month);
        this.A05 = (EditText) findViewById(R.id.add_card_year);
        C27531Hw.A06(this.A04);
        C27531Hw.A06(this.A05);
        this.A06 = C12970iu.A0M(this, R.id.payments_send_payment_error_text);
        Calendar instance = Calendar.getInstance();
        this.A00 = instance.get(2) + 1;
        this.A01 = instance.get(1) % 100;
        this.A02.addTextChangedListener(new C1317463y(this.A03, this, 2));
        this.A02.setOnKeyListener(new View$OnKeyListenerC1318964n(null, this.A03));
        this.A03.addTextChangedListener(new C1317463y(this.A04, this, 4));
        this.A03.setOnKeyListener(new View$OnKeyListenerC1318964n(this.A02, this.A04));
        this.A04.addTextChangedListener(new C1317463y(this.A05, this, 2));
        this.A04.setOnKeyListener(new View$OnKeyListenerC1318964n(this.A03, this.A05));
        this.A05.addTextChangedListener(new C1317463y(null, this, 2));
        this.A05.setOnKeyListener(new View$OnKeyListenerC1318964n(this.A04, null));
        this.A05.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.65J
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                IndiaUpiDebitCardVerificationActivity indiaUpiDebitCardVerificationActivity = IndiaUpiDebitCardVerificationActivity.this;
                if (i != 6) {
                    return false;
                }
                indiaUpiDebitCardVerificationActivity.A3K();
                return true;
            }
        });
        this.A02.requestFocus();
        ((AbstractActivityC121665jA) this).A0D.AKg(0, null, "enter_debit_card", null);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        A2w(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_help) {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.context_help_debit_card);
            A2x(A0S, "enter_debit_card");
            return true;
        }
        if (menuItem.getItemId() == 16908332) {
            ((AbstractActivityC121665jA) this).A0D.AKg(1, C12970iu.A0h(), "enter_debit_card", null);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        ((ActivityC13790kL) this).A0D.A01(findViewById(R.id.add_card_year));
    }
}
