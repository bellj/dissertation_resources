package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121495iO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC28491Nn;
import X.AbstractC38191ng;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2SP;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12970iu;
import X.C253118x;
import X.C50942Ry;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.IndiaUpiIncentivesValuePropsActivity;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class IndiaUpiIncentivesValuePropsActivity extends AbstractActivityC121495iO {
    public C253118x A00;
    public boolean A01;

    public IndiaUpiIncentivesValuePropsActivity() {
        this(0);
    }

    public IndiaUpiIncentivesValuePropsActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 46);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1U(A09, A1M, this);
            this.A00 = C117315Zl.A0H(A1M);
        }
    }

    @Override // X.AbstractActivityC121495iO
    public void A30() {
        ((AbstractActivityC121665jA) this).A03 = 1;
        super.A30();
    }

    @Override // X.AbstractActivityC121495iO, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        int i;
        super.onCreate(bundle);
        setContentView(R.layout.incentives_value_props);
        A2u(R.string.payments_activity_title, R.color.reg_title_color, R.id.payments_value_props_title_and_description_section);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.payments_activity_title);
            A1U.A0M(true);
        }
        C50942Ry A02 = this.A0T.A02();
        if (A02 == null || (str = A02.A0F) == null || (str2 = A02.A0B) == null) {
            Log.e("PAY: IndiaUpiIncentivesValuePropsActivity/PaymentIncentiveOfferInfo is null or has null items in it");
            finish();
            return;
        }
        TextView A0M = C12970iu.A0M(this, R.id.incentives_value_props_title);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.incentives_value_props_desc);
        A0M.setText(str);
        String str3 = A02.A0C;
        if (!TextUtils.isEmpty(str3)) {
            String[] strArr = new String[1];
            C117295Zj.A1A(((ActivityC13790kL) this).A02, str3, strArr, 0);
            AbstractC28491Nn.A05(textEmojiLabel, ((ActivityC13810kN) this).A08, this.A00.A01(this, C12960it.A0X(this, str2, new Object[1], 0, R.string.incentives_value_props_description_text), new Runnable[]{new Runnable() { // from class: X.6G2
                @Override // java.lang.Runnable
                public final void run() {
                    IndiaUpiIncentivesValuePropsActivity indiaUpiIncentivesValuePropsActivity = IndiaUpiIncentivesValuePropsActivity.this;
                    AnonymousClass2SP A022 = ((AbstractActivityC121665jA) indiaUpiIncentivesValuePropsActivity).A0D.A02(C12960it.A0V(), 9, "incentive_value_prop", null);
                    A022.A02 = Boolean.valueOf(AbstractActivityC119235dO.A1l(indiaUpiIncentivesValuePropsActivity));
                    AbstractActivityC119235dO.A1b(A022, indiaUpiIncentivesValuePropsActivity);
                }
            }}, new String[]{"incentive-blurb-cashback-terms"}, strArr));
        } else {
            textEmojiLabel.setText(str2);
        }
        View findViewById = findViewById(R.id.incentive_security_blurb_view);
        View findViewById2 = findViewById(R.id.payment_processor_logo);
        TextView A0M2 = C12970iu.A0M(this, R.id.incentives_value_props_continue);
        AbstractC38191ng A0K = C117305Zk.A0K(((AbstractActivityC121685jC) this).A0P);
        if (A0K == null || !A0K.A07.A07(979)) {
            if (AbstractActivityC119235dO.A1l(this)) {
                findViewById.setVisibility(8);
                findViewById2.setVisibility(8);
                A0M2.setText(R.string.payments_send_payment_text);
                i = 39;
            } else {
                findViewById.setVisibility(0);
                AnonymousClass2GE.A05(this, C117305Zk.A06(this, R.id.incentive_security_icon_view), R.color.payment_privacy_avatar_tint);
                findViewById2.setVisibility(0);
                A0M2.setText(R.string.incentives_value_props_unreg_cta);
                i = 40;
            }
            C117295Zj.A0n(A0M2, this, i);
        } else {
            C117295Zj.A0o(A0M2, this, A0K, 11);
        }
        AnonymousClass2SP A022 = ((AbstractActivityC121665jA) this).A0D.A02(0, null, "incentive_value_prop", ((AbstractActivityC121495iO) this).A02);
        A022.A02 = Boolean.valueOf(AbstractActivityC119235dO.A1l(this));
        AbstractActivityC119235dO.A1b(A022, this);
        ((AbstractActivityC121665jA) this).A0C.A09();
    }
}
