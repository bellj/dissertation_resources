package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121655j9;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC136226Ls;
import X.AbstractC136236Lt;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass02A;
import X.AnonymousClass04S;
import X.AnonymousClass1IR;
import X.AnonymousClass1V8;
import X.AnonymousClass1ZO;
import X.AnonymousClass1ZR;
import X.AnonymousClass1ZS;
import X.AnonymousClass2FL;
import X.AnonymousClass60O;
import X.AnonymousClass60R;
import X.AnonymousClass6AX;
import X.AnonymousClass6AY;
import X.AnonymousClass6AZ;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117895av;
import X.C118005b7;
import X.C118495bu;
import X.C119755f3;
import X.C119835fB;
import X.C120475gF;
import X.C120525gK;
import X.C120555gN;
import X.C120565gO;
import X.C120715gd;
import X.C120725ge;
import X.C120745gg;
import X.C124085oa;
import X.C126705tJ;
import X.C127115ty;
import X.C128285vr;
import X.C128355vy;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C1310060v;
import X.C14900mE;
import X.C18590sh;
import X.C18610sj;
import X.C18650sn;
import X.C30931Zj;
import X.C452120p;
import X.C64513Fv;
import X.RunnableC135796Jv;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class IndiaUpiMandatePaymentActivity extends AbstractActivityC121655j9 {
    public PaymentBottomSheet A00;
    public C118005b7 A01;
    public C128355vy A02;
    public C1310060v A03;
    public String A04;
    public boolean A05;
    public final C30931Zj A06;

    @Override // X.AbstractC124835qC
    public boolean Adt() {
        return true;
    }

    public IndiaUpiMandatePaymentActivity() {
        this(0);
        this.A06 = C117295Zj.A0G("IndiaUpiMandatePaymentActivity");
    }

    public IndiaUpiMandatePaymentActivity(int i) {
        this.A05 = false;
        C117295Zj.A0p(this, 49);
    }

    public static Intent A02(Context context, AnonymousClass1IR r2, String str, int i) {
        Intent A0D = C12990iw.A0D(context, IndiaUpiMandatePaymentActivity.class);
        A0D.putExtra("payment_transaction_info", r2);
        A0D.putExtra("user_action", i);
        A0D.putExtra("extra_referral_screen", str);
        return A0D;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
            AbstractActivityC119235dO.A1V(A09, A1M, this);
            this.A03 = (C1310060v) A1M.A9R.get();
            this.A02 = (C128355vy) A1M.A9f.get();
        }
    }

    @Override // X.AbstractActivityC121655j9
    public void A3J(AbstractC28901Pl r20, HashMap hashMap) {
        String str;
        String str2;
        AbstractC28901Pl r6 = r20;
        C118005b7 r0 = this.A01;
        if (r20 == null) {
            r6 = r0.A05;
        }
        r0.A0I.A06("handleCredentialBlob");
        AnonymousClass016 r8 = r0.A02;
        Context context = r0.A04.A00;
        C127115ty.A00(context, r8, R.string.register_wait_message);
        AnonymousClass1IR r3 = r0.A07;
        C119835fB r5 = (C119835fB) r3.A0A;
        int i = r0.A00;
        if (1 == i || 4 == i) {
            AnonymousClass60O r52 = r5.A0B.A0C;
            C120565gO r82 = r0.A08;
            AnonymousClass6AZ r7 = new AbstractC136236Lt(r6, r52, r0) { // from class: X.6AZ
                public final /* synthetic */ AbstractC28901Pl A00;
                public final /* synthetic */ AnonymousClass60O A01;
                public final /* synthetic */ C118005b7 A02;

                {
                    this.A02 = r3;
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC136236Lt
                public final void AVD(C452120p r62) {
                    C118005b7 r4 = this.A02;
                    AnonymousClass60O r32 = this.A01;
                    AbstractC28901Pl r2 = this.A00;
                    if (r62 == null) {
                        C127115ty.A01(r4.A02);
                        r4.A0J.Ab2(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0014: INVOKE  
                              (wrap: X.0lR : 0x000d: IGET  (r1v0 X.0lR A[REMOVE]) = (r4v0 'r4' X.5b7) X.5b7.A0J X.0lR)
                              (wrap: X.6Jd : 0x0011: CONSTRUCTOR  (r0v1 X.6Jd A[REMOVE]) = (r2v0 'r2' X.1Pl), (r3v0 'r32' X.60O), (r4v0 'r4' X.5b7) call: X.6Jd.<init>(X.1Pl, X.60O, X.5b7):void type: CONSTRUCTOR)
                             type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6AZ.AVD(X.20p):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: CONSTRUCTOR  (r0v1 X.6Jd A[REMOVE]) = (r2v0 'r2' X.1Pl), (r3v0 'r32' X.60O), (r4v0 'r4' X.5b7) call: X.6Jd.<init>(X.1Pl, X.60O, X.5b7):void type: CONSTRUCTOR in method: X.6AZ.AVD(X.20p):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 23 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Jd, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 29 more
                            */
                        /*
                            this = this;
                            X.5b7 r4 = r5.A02
                            X.60O r3 = r5.A01
                            X.1Pl r2 = r5.A00
                            if (r6 != 0) goto L_0x0018
                            X.016 r0 = r4.A02
                            X.C127115ty.A01(r0)
                            X.0lR r1 = r4.A0J
                            X.6Jd r0 = new X.6Jd
                            r0.<init>(r2, r3, r4)
                            r1.Ab2(r0)
                            return
                        L_0x0018:
                            X.C118005b7.A00(r6, r4)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AZ.AVD(X.20p):void");
                    }
                };
                Log.i("PAY: acceptPayeeMandate called");
                ArrayList A0l = C12960it.A0l();
                C117295Zj.A1M("action", "upi-accept-mandate-request", A0l);
                r82.A02(r3, A0l);
                C120565gO.A00(r6, hashMap, A0l);
                C119835fB r62 = (C119835fB) r3.A0A;
                AnonymousClass60R r02 = r62.A0B;
                AnonymousClass009.A05(r02);
                AnonymousClass1ZR r1 = r02.A06;
                if (!AnonymousClass1ZS.A03(r1)) {
                    C117295Zj.A1M("mandate-info", (String) C117295Zj.A0R(r1), A0l);
                }
                C120565gO.A01(r52, r62, null, A0l, false);
                C120475gF r12 = r82.A03;
                if (r12 != null) {
                    r12.A00("U66", A0l);
                }
                C64513Fv r63 = ((C126705tJ) r82).A00;
                if (r63 != null) {
                    r63.A04("upi-accept-mandate-request");
                }
                C117305Zk.A1H(((C126705tJ) r82).A01, new C120715gd(r82.A00, r82.A01, r82.A02, r63, r7, r82), C117295Zj.A0L(A0l, r82.A03(r3)));
            } else if (3 == i) {
                C120565gO r10 = r0.A08;
                String str3 = r0.A0A;
                AnonymousClass6AX r9 = new AbstractC136236Lt() { // from class: X.6AX
                    @Override // X.AbstractC136236Lt
                    public final void AVD(C452120p r4) {
                        C118005b7 r2 = C118005b7.this;
                        if (r4 == null) {
                            C127115ty.A01(r2.A02);
                            r2.A0J.Ab2(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0010: INVOKE  
                                  (wrap: X.0lR : 0x0009: IGET  (r1v0 X.0lR A[REMOVE]) = (r2v0 'r2' X.5b7) X.5b7.A0J X.0lR)
                                  (wrap: X.6HK : 0x000d: CONSTRUCTOR  (r0v1 X.6HK A[REMOVE]) = (r2v0 'r2' X.5b7) call: X.6HK.<init>(X.5b7):void type: CONSTRUCTOR)
                                 type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6AX.AVD(X.20p):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000d: CONSTRUCTOR  (r0v1 X.6HK A[REMOVE]) = (r2v0 'r2' X.5b7) call: X.6HK.<init>(X.5b7):void type: CONSTRUCTOR in method: X.6AX.AVD(X.20p):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 23 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6HK, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 29 more
                                */
                            /*
                                this = this;
                                X.5b7 r2 = X.C118005b7.this
                                if (r4 != 0) goto L_0x0014
                                X.016 r0 = r2.A02
                                X.C127115ty.A01(r0)
                                X.0lR r1 = r2.A0J
                                X.6HK r0 = new X.6HK
                                r0.<init>(r2)
                                r1.Ab2(r0)
                                return
                            L_0x0014:
                                X.C118005b7.A00(r4, r2)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AX.AVD(X.20p):void");
                        }
                    };
                    Log.i("PAY: revokePayerMandate called");
                    ArrayList A0l2 = C12960it.A0l();
                    C117295Zj.A1M("action", "upi-revoke-mandate", A0l2);
                    r10.A02(r3, A0l2);
                    C120565gO.A01(null, (C119835fB) r3.A0A, str3, A0l2, true);
                    C120565gO.A00(r6, hashMap, A0l2);
                    C64513Fv r83 = ((C126705tJ) r10).A00;
                    if (r83 != null) {
                        r83.A04("upi-revoke-mandate");
                    }
                    C120475gF r13 = r10.A03;
                    if (r13 != null) {
                        r13.A00("U66", A0l2);
                    }
                    C117305Zk.A1H(((C126705tJ) r10).A01, new C120725ge(r10.A00, r10.A01, r10.A02, r83, r9, r10), C117295Zj.A0L(A0l2, r10.A03(r3)));
                } else if (6 == i) {
                    C120565gO r102 = r0.A08;
                    String str4 = r0.A0A;
                    AnonymousClass6AY r92 = new AbstractC136236Lt(r5, r0) { // from class: X.6AY
                        public final /* synthetic */ C119835fB A00;
                        public final /* synthetic */ C118005b7 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC136236Lt
                        public final void AVD(C452120p r53) {
                            C118005b7 r32 = this.A01;
                            C119835fB r2 = this.A00;
                            if (r53 == null) {
                                r32.A0J.Ab2(
                                /*  JADX ERROR: Method code generation error
                                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000d: INVOKE  
                                      (wrap: X.0lR : 0x0006: IGET  (r1v0 X.0lR A[REMOVE]) = (r3v0 'r32' X.5b7) X.5b7.A0J X.0lR)
                                      (wrap: X.6Ih : 0x000a: CONSTRUCTOR  (r0v0 X.6Ih A[REMOVE]) = (r2v0 'r2' X.5fB), (r3v0 'r32' X.5b7) call: X.6Ih.<init>(X.5fB, X.5b7):void type: CONSTRUCTOR)
                                     type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6AY.AVD(X.20p):void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000a: CONSTRUCTOR  (r0v0 X.6Ih A[REMOVE]) = (r2v0 'r2' X.5fB), (r3v0 'r32' X.5b7) call: X.6Ih.<init>(X.5fB, X.5b7):void type: CONSTRUCTOR in method: X.6AY.AVD(X.20p):void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                    	... 21 more
                                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Ih, state: NOT_LOADED
                                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                    	... 27 more
                                    */
                                /*
                                    this = this;
                                    X.5b7 r3 = r4.A01
                                    X.5fB r2 = r4.A00
                                    if (r5 != 0) goto L_0x0011
                                    X.0lR r1 = r3.A0J
                                    X.6Ih r0 = new X.6Ih
                                    r0.<init>(r2, r3)
                                    r1.Ab2(r0)
                                    return
                                L_0x0011:
                                    X.C118005b7.A00(r5, r3)
                                    return
                                */
                                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AY.AVD(X.20p):void");
                            }
                        };
                        Log.i("PAY: resumePayeeMandate called");
                        ArrayList A0l3 = C12960it.A0l();
                        C117295Zj.A1M("action", "upi-resume-mandate", A0l3);
                        r102.A02(r3, A0l3);
                        C120565gO.A01(null, (C119835fB) r3.A0A, str4, A0l3, true);
                        C120565gO.A00(r6, hashMap, A0l3);
                        AnonymousClass1V8[] A03 = r102.A03(r3);
                        C120475gF r14 = r102.A03;
                        if (r14 != null) {
                            r14.A00("U66", A0l3);
                        }
                        C64513Fv r84 = ((C126705tJ) r102).A00;
                        if (r84 != null) {
                            r84.A04("upi-resume-mandate");
                        }
                        C117305Zk.A1H(((C126705tJ) r102).A01, new C120745gg(r102.A00, r102.A01, r102.A02, r84, r92, r102), C117295Zj.A0L(A0l3, A03));
                    } else if (7 == i) {
                        C127115ty.A00(context, r8, R.string.register_wait_message);
                        AnonymousClass1IR r2 = r0.A06;
                        AnonymousClass1ZR r85 = null;
                        if (r2 != null) {
                            str = (String) C117295Zj.A0R(((C119835fB) r2.A0A).A0B.A08);
                        } else {
                            str = null;
                        }
                        if (r6 != null) {
                            C119755f3 r22 = (C119755f3) r6.A08;
                            if (r22 != null) {
                                r85 = r22.A06;
                            }
                            str2 = r6.A0A;
                        } else {
                            str2 = null;
                        }
                        String str5 = r3.A0K;
                        r0.A0F.A01(r3.A08, r85, new AbstractC136226Ls(str5) { // from class: X.6AM
                            public final /* synthetic */ String A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // X.AbstractC136226Ls
                            public final void ALo(C452120p r53) {
                                C118005b7 r32 = C118005b7.this;
                                String str6 = this.A01;
                                if (r53 == null) {
                                    r32.A0J.Ab2(
                                    /*  JADX ERROR: Method code generation error
                                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000d: INVOKE  
                                          (wrap: X.0lR : 0x0006: IGET  (r1v0 X.0lR A[REMOVE]) = (r3v0 'r32' X.5b7) X.5b7.A0J X.0lR)
                                          (wrap: X.6Ii : 0x000a: CONSTRUCTOR  (r0v0 X.6Ii A[REMOVE]) = (r3v0 'r32' X.5b7), (r2v0 'str6' java.lang.String) call: X.6Ii.<init>(X.5b7, java.lang.String):void type: CONSTRUCTOR)
                                         type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6AM.ALo(X.20p):void, file: classes4.dex
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000a: CONSTRUCTOR  (r0v0 X.6Ii A[REMOVE]) = (r3v0 'r32' X.5b7), (r2v0 'str6' java.lang.String) call: X.6Ii.<init>(X.5b7, java.lang.String):void type: CONSTRUCTOR in method: X.6AM.ALo(X.20p):void, file: classes4.dex
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                        	... 21 more
                                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Ii, state: NOT_LOADED
                                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                        	... 27 more
                                        */
                                    /*
                                        this = this;
                                        X.5b7 r3 = X.C118005b7.this
                                        java.lang.String r2 = r4.A01
                                        if (r5 != 0) goto L_0x0011
                                        X.0lR r1 = r3.A0J
                                        X.6Ii r0 = new X.6Ii
                                        r0.<init>(r3, r2)
                                        r1.Ab2(r0)
                                        return
                                    L_0x0011:
                                        X.C118005b7.A00(r5, r3)
                                        return
                                    */
                                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AM.ALo(X.20p):void");
                                }
                            }, r0.A0G, str5, r5.A0L, r5.A0M, r5.A0J, r5.A0K, str2, str, hashMap);
                        }
                    }

                    @Override // X.AbstractActivityC121655j9
                    public void A3L(PaymentBottomSheet paymentBottomSheet) {
                        super.A3L(paymentBottomSheet);
                        paymentBottomSheet.A00 = new IDxDListenerShape14S0100000_3_I1(this, 11);
                        ((AbstractActivityC121665jA) this).A0D.AKh(C12980iv.A0i(), null, "approve_mandate_prompt", this.A04, true);
                    }

                    @Override // X.AbstractActivityC121655j9
                    public void A3M(PaymentBottomSheet paymentBottomSheet) {
                        super.A3M(paymentBottomSheet);
                        paymentBottomSheet.A00 = new IDxDListenerShape14S0100000_3_I1(this, 13);
                    }

                    @Override // X.AbstractActivityC121655j9
                    public void A3N(PaymentBottomSheet paymentBottomSheet) {
                        super.A3N(paymentBottomSheet);
                        paymentBottomSheet.A00 = new IDxDListenerShape14S0100000_3_I1(this, 14);
                    }

                    public void A3P(int i) {
                        C004802e A0S = C12980iv.A0S(this);
                        A0S.A06(i);
                        A0S.A0B(true);
                        A0S.setPositiveButton(R.string.payments_decline_request, null);
                        A0S.setNegativeButton(R.string.cancel, null);
                        A0S.A04(new IDxDListenerShape14S0100000_3_I1(this, 12));
                        AnonymousClass04S create = A0S.create();
                        create.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.634
                            @Override // android.content.DialogInterface.OnShowListener
                            public final void onShow(DialogInterface dialogInterface) {
                                IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity = IndiaUpiMandatePaymentActivity.this;
                                C117295Zj.A0o(((AnonymousClass04S) dialogInterface).A00.A0G, indiaUpiMandatePaymentActivity, dialogInterface, 13);
                                ((AbstractActivityC121665jA) indiaUpiMandatePaymentActivity).A0D.AKh(C12980iv.A0i(), null, "decline_mandate_dialogue", indiaUpiMandatePaymentActivity.A04, true);
                            }
                        });
                        create.show();
                    }

                    @Override // X.AbstractActivityC121655j9, X.AbstractC124835qC
                    public void AMN(ViewGroup viewGroup) {
                        super.AMN(viewGroup);
                        C12960it.A0I(viewGroup, R.id.text).setText(R.string.upi_mandate_bottom_sheet_pay_title);
                    }

                    @Override // X.AbstractActivityC121655j9, X.AnonymousClass6N0
                    public void AOW(View view, View view2, AnonymousClass1ZO r11, AbstractC28901Pl r12, PaymentBottomSheet paymentBottomSheet) {
                        super.AOW(view, view2, r11, r12, paymentBottomSheet);
                        ((AbstractActivityC121665jA) this).A0D.AKh(C12960it.A0V(), 104, "approve_mandate_prompt", this.A04, true);
                    }

                    @Override // X.AnonymousClass6MS
                    public void AVu(C452120p r3) {
                        throw C12980iv.A0u(this.A06.A02("onSetPin unsupported"));
                    }

                    @Override // X.AbstractActivityC121655j9, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
                    public void onActivityResult(int i, int i2, Intent intent) {
                        if (i == 1 || (i == 155 && i2 != -1)) {
                            finish();
                        } else {
                            super.onActivityResult(i, i2, intent);
                        }
                    }

                    @Override // X.AbstractActivityC121655j9, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
                    public void onCreate(Bundle bundle) {
                        int i;
                        int i2;
                        C128285vr r1;
                        int i3;
                        super.onCreate(bundle);
                        this.A04 = AbstractActivityC119235dO.A0N(this);
                        this.A00 = new PaymentBottomSheet();
                        C14900mE r10 = ((ActivityC13810kN) this).A05;
                        C64513Fv r12 = ((AbstractActivityC121545iU) this).A06;
                        C18590sh r4 = ((AbstractActivityC121545iU) this).A0C;
                        C18610sj r3 = ((AbstractActivityC121685jC) this).A0M;
                        C120475gF r0 = ((AbstractActivityC121545iU) this).A08;
                        C18650sn r2 = ((AbstractActivityC121685jC) this).A0K;
                        C120565gO r7 = new C120565gO(this, r10, r2, r12, r3, r0, r4);
                        C120555gN r5 = new C120555gN(this, r10, ((ActivityC13810kN) this).A0C, ((AbstractActivityC121685jC) this).A0H, ((AbstractActivityC121665jA) this).A0A, r2, r3, r4);
                        C128355vy r102 = this.A02;
                        C120525gK r8 = ((AbstractActivityC121545iU) this).A09;
                        C118005b7 r32 = (C118005b7) C117315Zl.A06(new C118495bu((AnonymousClass1IR) getIntent().getParcelableExtra("payment_transaction_info"), r5, ((AbstractActivityC121545iU) this).A08, r7, r8, this, r102, AbstractActivityC119235dO.A0O(this), getIntent().getIntExtra("user_action", 0)), this).A00(C118005b7.class);
                        this.A01 = r32;
                        r32.A02.A05(r32.A01, C117305Zk.A0B(this, 42));
                        C118005b7 r33 = this.A01;
                        r33.A09.A05(r33.A01, C117305Zk.A0B(this, 41));
                        C117295Zj.A0r(this, ((C117895av) new AnonymousClass02A(this).A00(C117895av.class)).A00, 40);
                        C118005b7 r42 = this.A01;
                        AnonymousClass1IR r22 = r42.A07;
                        C119835fB r13 = (C119835fB) r22.A0A;
                        switch (r42.A00) {
                            case 1:
                                i3 = 6;
                                r1 = new C128285vr(i3);
                                r1.A03 = r22;
                                r42.A09.A0B(r1);
                                return;
                            case 2:
                                AnonymousClass60O r02 = r13.A0B.A0C;
                                int i4 = R.string.upi_mandate_update_decline_confirm_message;
                                if (r02 == null) {
                                    i4 = R.string.upi_mandate_decline_confirm_message;
                                }
                                r1 = new C128285vr(5);
                                r1.A00 = i4;
                                r42.A09.A0B(r1);
                                return;
                            case 3:
                                i = 4;
                                i2 = R.string.upi_mandate_revoke_missing_payment_method;
                                r42.A0J.Ab2(new RunnableC135796Jv(r42, i2, i));
                                return;
                            case 4:
                                i = 7;
                                i2 = R.string.upi_mandate_missing_payment_method_message;
                                r42.A0J.Ab2(new RunnableC135796Jv(r42, i2, i));
                                return;
                            case 5:
                                i3 = 9;
                                r1 = new C128285vr(i3);
                                r1.A03 = r22;
                                r42.A09.A0B(r1);
                                return;
                            case 6:
                                i = 10;
                                i2 = R.string.upi_mandate_resume_missing_payment_method;
                                r42.A0J.Ab2(new RunnableC135796Jv(r42, i2, i));
                                return;
                            case 7:
                                AnonymousClass009.A05(r13);
                                C12990iw.A1N(new C124085oa(r42, r13.A0G), r42.A0J);
                                return;
                            default:
                                return;
                        }
                    }
                }
