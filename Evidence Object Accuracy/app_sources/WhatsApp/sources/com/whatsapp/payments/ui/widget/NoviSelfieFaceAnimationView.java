package com.whatsapp.payments.ui.widget;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass07M;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes4.dex */
public class NoviSelfieFaceAnimationView extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public Context A01;
    public Handler A02;
    public TextToSpeech A03;
    public ImageView A04;
    public AnonymousClass07M A05;
    public AnonymousClass07M A06;
    public AnonymousClass07M A07;
    public AnonymousClass07M A08;
    public WaTextView A09;
    public AnonymousClass018 A0A;
    public AnonymousClass2P7 A0B;
    public Runnable A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;

    public NoviSelfieFaceAnimationView(Context context) {
        super(context);
        C117295Zj.A0i(context, this);
    }

    public NoviSelfieFaceAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C117295Zj.A0i(context, this);
    }

    public NoviSelfieFaceAnimationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C117295Zj.A0i(context, this);
    }

    public NoviSelfieFaceAnimationView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        C117295Zj.A0i(context, this);
    }

    public NoviSelfieFaceAnimationView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A01();
    }

    public void A00() {
        A02();
        if (this.A02 != null) {
            this.A02 = null;
        }
    }

    public void A01() {
        if (!this.A0D) {
            this.A0D = true;
            this.A0A = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public void A02() {
        Runnable runnable;
        Handler handler = this.A02;
        if (!(handler == null || (runnable = this.A0C) == null)) {
            handler.removeCallbacks(runnable);
            this.A0C = null;
        }
        TextToSpeech textToSpeech = this.A03;
        if (textToSpeech != null) {
            textToSpeech.stop();
            this.A03.shutdown();
        }
        this.A00 = 0;
        this.A06.stop();
        this.A08.stop();
        this.A07.stop();
        this.A05.stop();
        C12970iu.A19(this.A01, this.A09, R.string.novi_selfie_follow_face_instruction);
    }

    public final void A03(Context context) {
        this.A01 = context;
        LayoutInflater.from(context).inflate(R.layout.novi_selfie_animation_view, (ViewGroup) this, true);
        this.A04 = C12970iu.A0K(this, R.id.novi_selfie_animation_face);
        this.A09 = C12960it.A0N(this, R.id.selfie_head_direction_tv);
        AnonymousClass07M A04 = AnonymousClass07M.A04(context, R.drawable.novi_selfie_face_animation_left);
        AnonymousClass009.A05(A04);
        this.A06 = A04;
        AnonymousClass07M A042 = AnonymousClass07M.A04(context, R.drawable.novi_selfie_face_animation_right);
        AnonymousClass009.A05(A042);
        this.A07 = A042;
        AnonymousClass07M A043 = AnonymousClass07M.A04(context, R.drawable.novi_selfie_face_animation_up);
        AnonymousClass009.A05(A043);
        this.A08 = A043;
        AnonymousClass07M A044 = AnonymousClass07M.A04(context, R.drawable.novi_selfie_face_animation_down);
        AnonymousClass009.A05(A044);
        this.A05 = A044;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(java.util.List r5) {
        /*
            r4 = this;
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x00f6
            int r1 = r4.A00
            int r0 = r1 + 1
            r4.A00 = r0
            java.lang.String r1 = X.C12960it.A0g(r5, r1)
            int r0 = r1.hashCode()
            switch(r0) {
                case 2715: goto L_0x0026;
                case 2104482: goto L_0x0050;
                case 2332679: goto L_0x007a;
                case 77974012: goto L_0x00a4;
                default: goto L_0x0017;
            }
        L_0x0017:
            java.lang.String r0 = "Invalid direction found: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r1, r0)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0026:
            java.lang.String r0 = "UP"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0017
            android.widget.ImageView r1 = r4.A04
            X.07M r0 = r4.A08
            r1.setImageDrawable(r0)
            com.whatsapp.WaTextView r2 = r4.A09
            android.content.res.Resources r1 = r4.getResources()
            r0 = 2131889918(0x7f120efe, float:1.9414513E38)
            java.lang.String r0 = r1.getString(r0)
            r2.setText(r0)
            X.07M r0 = r4.A08
            r0.start()
            android.content.Context r1 = r4.A01
            r0 = 2131889929(0x7f120f09, float:1.9414535E38)
            goto L_0x00cd
        L_0x0050:
            java.lang.String r0 = "DOWN"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0017
            android.widget.ImageView r1 = r4.A04
            X.07M r0 = r4.A05
            r1.setImageDrawable(r0)
            com.whatsapp.WaTextView r2 = r4.A09
            android.content.res.Resources r1 = r4.getResources()
            r0 = 2131889917(0x7f120efd, float:1.9414511E38)
            java.lang.String r0 = r1.getString(r0)
            r2.setText(r0)
            X.07M r0 = r4.A05
            r0.start()
            android.content.Context r1 = r4.A01
            r0 = 2131889928(0x7f120f08, float:1.9414533E38)
            goto L_0x00cd
        L_0x007a:
            java.lang.String r0 = "LEFT"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0017
            android.widget.ImageView r1 = r4.A04
            X.07M r0 = r4.A06
            r1.setImageDrawable(r0)
            com.whatsapp.WaTextView r2 = r4.A09
            android.content.res.Resources r1 = r4.getResources()
            r0 = 2131889919(0x7f120eff, float:1.9414515E38)
            java.lang.String r0 = r1.getString(r0)
            r2.setText(r0)
            X.07M r0 = r4.A06
            r0.start()
            android.content.Context r1 = r4.A01
            r0 = 2131889930(0x7f120f0a, float:1.9414537E38)
            goto L_0x00cd
        L_0x00a4:
            java.lang.String r0 = "RIGHT"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0017
            android.widget.ImageView r1 = r4.A04
            X.07M r0 = r4.A07
            r1.setImageDrawable(r0)
            com.whatsapp.WaTextView r2 = r4.A09
            android.content.res.Resources r1 = r4.getResources()
            r0 = 2131889920(0x7f120f00, float:1.9414517E38)
            java.lang.String r0 = r1.getString(r0)
            r2.setText(r0)
            X.07M r0 = r4.A07
            r0.start()
            android.content.Context r1 = r4.A01
            r0 = 2131889931(0x7f120f0b, float:1.941454E38)
        L_0x00cd:
            java.lang.String r3 = r1.getString(r0)
            boolean r0 = r4.A0F
            if (r0 == 0) goto L_0x00e7
            boolean r0 = r4.A0E
            if (r0 == 0) goto L_0x00e7
            android.content.Context r2 = r4.A01
            X.63t r1 = new X.63t
            r1.<init>(r4, r3)
            android.speech.tts.TextToSpeech r0 = new android.speech.tts.TextToSpeech
            r0.<init>(r2, r1)
            r4.A03 = r0
        L_0x00e7:
            X.6Iu r3 = new X.6Iu
            r3.<init>(r5)
            r4.A0C = r3
            android.os.Handler r2 = r4.A02
            r0 = 4000(0xfa0, double:1.9763E-320)
            r2.postDelayed(r3, r0)
            return
        L_0x00f6:
            java.lang.String r0 = "No direction found"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView.A04(java.util.List):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }
}
