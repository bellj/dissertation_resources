package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass69M;
import X.AnonymousClass6LW;
import X.C117295Zj;
import X.C30931Zj;
import android.content.DialogInterface;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentLauncherActivity extends AbstractActivityC121665jA implements DialogInterface.OnDismissListener, AnonymousClass6LW {
    public AnonymousClass69M A00;
    public boolean A01;
    public final C30931Zj A02;

    public IndiaUpiPaymentLauncherActivity() {
        this(0);
        this.A02 = C117295Zj.A0G("IndiaUpiPaymentLauncherActivity");
    }

    public IndiaUpiPaymentLauncherActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 54);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0041, code lost:
        if (X.C12990iw.A0H(r12).getBoolean("intent_source") == false) goto L_0x0043;
     */
    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            r7 = r12
            super.onCreate(r13)
            X.0zW r0 = r12.A0O
            boolean r0 = r0.A03()
            r8 = 0
            if (r0 != 0) goto L_0x0018
            X.1Zj r1 = r12.A02
            java.lang.String r0 = "payment feature is not enabled."
            r1.A0A(r0, r8)
        L_0x0014:
            r12.finish()
            return
        L_0x0018:
            android.content.Intent r0 = r12.getIntent()
            if (r0 == 0) goto L_0x0014
            android.content.Intent r0 = r12.getIntent()
            android.net.Uri r0 = r0.getData()
            if (r0 == 0) goto L_0x0014
            android.content.Intent r0 = r12.getIntent()
            android.net.Uri r5 = r0.getData()
            android.os.Bundle r0 = X.C12990iw.A0H(r12)
            if (r0 == 0) goto L_0x0043
            android.os.Bundle r1 = X.C12990iw.A0H(r12)
            java.lang.String r0 = "intent_source"
            boolean r0 = r1.getBoolean(r0)
            r4 = 1
            if (r0 != 0) goto L_0x0044
        L_0x0043:
            r4 = 0
        L_0x0044:
            X.1Zj r1 = r12.A02
            java.lang.String r0 = "received payment intent: isFromQrCode "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            r0.append(r4)
            X.C117295Zj.A1F(r1, r0)
            if (r4 == 0) goto L_0x007b
            java.lang.String r10 = "SCANNED_QR_CODE"
            java.lang.String r11 = "payments_camera"
        L_0x0058:
            X.68y r3 = r12.A0B
            X.0nH r2 = r12.A06
            X.6BE r1 = r12.A0D
            X.69M r0 = new X.69M
            r0.<init>(r2, r3, r1)
            r12.A00 = r0
            if (r4 == 0) goto L_0x0080
            X.0m9 r1 = r12.A0C
            r0 = 1354(0x54a, float:1.897E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0080
            X.69M r6 = r12.A00
            java.lang.String r9 = r5.toString()
            r6.A00(r7, r8, r9, r10, r11)
            goto L_0x0014
        L_0x007b:
            java.lang.String r10 = "DEEP_LINK"
            java.lang.String r11 = "deeplink"
            goto L_0x0058
        L_0x0080:
            java.lang.String r0 = r5.toString()
            com.whatsapp.payments.ui.PaymentBottomSheet r1 = new com.whatsapp.payments.ui.PaymentBottomSheet
            r1.<init>()
            r1.A00 = r12
            com.whatsapp.payments.ui.IndiaUpiQrCodeScannedDialogFragment r0 = com.whatsapp.payments.ui.IndiaUpiQrCodeScannedDialogFragment.A00(r0, r10, r11)
            r1.A01 = r0
            r12.Adl(r1, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiPaymentLauncherActivity.onCreate(android.os.Bundle):void");
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        finish();
    }
}
