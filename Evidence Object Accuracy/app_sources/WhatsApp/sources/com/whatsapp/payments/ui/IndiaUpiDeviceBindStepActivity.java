package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121595il;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC136436Mn;
import X.AbstractC14440lR;
import X.AbstractC30851Zb;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass102;
import X.AnonymousClass162;
import X.AnonymousClass17V;
import X.AnonymousClass1UY;
import X.AnonymousClass1ZR;
import X.AnonymousClass2SP;
import X.AnonymousClass5oU;
import X.AnonymousClass60I;
import X.AnonymousClass60V;
import X.AnonymousClass60W;
import X.AnonymousClass68W;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.AnonymousClass6MR;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117385Zs;
import X.C119715ez;
import X.C119755f3;
import X.C120475gF;
import X.C122195l4;
import X.C122205l5;
import X.C123985oG;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C129925yW;
import X.C130165yu;
import X.C1308460e;
import X.C1309060l;
import X.C1329668y;
import X.C14900mE;
import X.C15890o4;
import X.C16590pI;
import X.C17070qD;
import X.C17220qS;
import X.C18590sh;
import X.C18600si;
import X.C18610sj;
import X.C22120yY;
import X.C30931Zj;
import X.C32641cU;
import X.C452120p;
import X.C64513Fv;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class IndiaUpiDeviceBindStepActivity extends AbstractActivityC121595il implements AnonymousClass6MR {
    public static final AbstractC136436Mn A0X = new AnonymousClass68W();
    public int A00;
    public int A01 = -1;
    public PendingIntent A02;
    public View A03;
    public View A04;
    public View A05;
    public WaImageView A06;
    public C15890o4 A07;
    public AnonymousClass102 A08;
    public C129925yW A09;
    public C119755f3 A0A;
    public C452120p A0B;
    public C64513Fv A0C;
    public AnonymousClass162 A0D;
    public AnonymousClass60I A0E;
    public AnonymousClass17V A0F;
    public C123985oG A0G;
    public C117385Zs A0H;
    public AnonymousClass69E A0I;
    public C122195l4 A0J;
    public C122205l5 A0K;
    public AnonymousClass60W A0L;
    public C18590sh A0M;
    public C22120yY A0N;
    public Runnable A0O;
    public String A0P;
    public ArrayList A0Q;
    public C130165yu A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public final AnonymousClass2SP A0V = new AnonymousClass2SP();
    public final C30931Zj A0W = C117315Zl.A0D("IndiaUpiDeviceBindActivity");

    public static /* synthetic */ void A02(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity) {
        ArrayList<String> arrayList;
        ArrayList A0l;
        int i;
        Bundle bundle;
        AnonymousClass009.A00();
        String A05 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A05(indiaUpiDeviceBindStepActivity.A0A);
        C30931Zj r6 = indiaUpiDeviceBindStepActivity.A0W;
        StringBuilder A0k = C12960it.A0k("IndiaUpiDeviceBindActivity sendDeviceBindingSms called for psp: ");
        A0k.append(A05);
        A0k.append(" with ordering: ");
        C1308460e r3 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A;
        C119755f3 r0 = indiaUpiDeviceBindStepActivity.A0A;
        ArrayList<String> arrayList2 = null;
        if (r0 == null || (arrayList = r0.A0G) == null || arrayList.isEmpty()) {
            C119715ez r02 = r3.A03;
            if (!(r02 == null || (bundle = r02.A00) == null)) {
                arrayList2 = bundle.getStringArrayList("pspRouting");
            }
            arrayList = arrayList2;
        }
        A0k.append(arrayList);
        C117295Zj.A1F(r6, A0k);
        try {
            if (Build.VERSION.SDK_INT < 22 || (i = indiaUpiDeviceBindStepActivity.A01) < 0) {
                r6.A06("sending sms from default sim");
                A0l = C12960it.A0l();
                A0l.add(SmsManager.getDefault());
            } else {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("sending sms from sim subscription id: ");
                A0h.append(i);
                C117295Zj.A1F(r6, A0h);
                A0l = C12960it.A0l();
                A0l.add(indiaUpiDeviceBindStepActivity.A0L.A02(indiaUpiDeviceBindStepActivity.A01));
            }
            if (indiaUpiDeviceBindStepActivity.A0H == null) {
                indiaUpiDeviceBindStepActivity.A02 = AnonymousClass1UY.A01(indiaUpiDeviceBindStepActivity, 0, new Intent("SMS_SENT").setPackage("com.whatsapp"), 0);
                C117385Zs r1 = new C117385Zs(indiaUpiDeviceBindStepActivity);
                indiaUpiDeviceBindStepActivity.A0H = r1;
                indiaUpiDeviceBindStepActivity.registerReceiver(r1, new IntentFilter("SMS_SENT"));
            }
            AnonymousClass1ZR A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, indiaUpiDeviceBindStepActivity.A2o(""), "smsVerificationDataGen");
            C1329668y r8 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B;
            synchronized (r8) {
                try {
                    C18600si r12 = r8.A03;
                    JSONObject A0b = C117295Zj.A0b(r12);
                    A0b.put("v", "2");
                    Object obj = A0I.A00;
                    AnonymousClass009.A05(obj);
                    A0b.put("smsVerifDataGen", obj);
                    C117295Zj.A1E(r12, A0b);
                } catch (JSONException e) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs storeSmsVerificationDataGenerated threw: ", e);
                }
            }
            String A2p = indiaUpiDeviceBindStepActivity.A2p(((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A06(indiaUpiDeviceBindStepActivity.A0A), C117315Zl.A0K(A0I));
            String A04 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A04(indiaUpiDeviceBindStepActivity.A0A);
            C1329668y r82 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B;
            synchronized (r82) {
                try {
                    C18600si r122 = r82.A03;
                    JSONObject A0b2 = C117295Zj.A0b(r122);
                    A0b2.put("v", "2");
                    A0b2.put("smsVerifDataGateway", A04);
                    C117295Zj.A1E(r122, A0b2);
                } catch (JSONException e2) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs storeSmsGateway threw: ", e2);
                }
            }
            C119715ez A01 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A01(indiaUpiDeviceBindStepActivity.A0A);
            String A0A = (A01 == null || TextUtils.isEmpty(A01.A0A())) ? "TRL WHA" : A01.A0A();
            for (int i2 = 0; i2 < A0l.size(); i2++) {
                SmsManager smsManager = (SmsManager) A0l.get(i2);
                StringBuilder A0j = C12960it.A0j(A0A);
                A0j.append(" ");
                ArrayList<String> divideMessage = smsManager.divideMessage(C12960it.A0d(A2p, A0j));
                ArrayList<PendingIntent> A0l2 = C12960it.A0l();
                for (int i3 = 0; i3 < divideMessage.size(); i3++) {
                    A0l2.add(indiaUpiDeviceBindStepActivity.A02);
                }
                try {
                    smsManager.sendMultipartTextMessage(A04, null, divideMessage, A0l2, null);
                    indiaUpiDeviceBindStepActivity.A0T = true;
                } catch (IllegalArgumentException | NullPointerException | SecurityException | UnsupportedOperationException e3) {
                    r6.A0A("IndiaUpiPaymentSetup sendDeviceBindingSms failed. Manual SMS no longer available. More details: ", e3);
                    AbstractActivityC119235dO.A1j(indiaUpiDeviceBindStepActivity.A0K, 3);
                    AbstractActivityC119235dO.A1j(indiaUpiDeviceBindStepActivity.A0J, 3);
                    indiaUpiDeviceBindStepActivity.finish();
                }
                C64513Fv r03 = indiaUpiDeviceBindStepActivity.A0C;
                if (r03 != null) {
                    r03.A04("device-binding-sms");
                }
            }
            StringBuilder A0h2 = C12960it.A0h();
            A0h2.append("IndiaUpiDeviceBindActivity sendDeviceBindingSms sent sms to psp: ");
            A0h2.append(A05);
            A0h2.append(" smsNumber: ");
            A0h2.append(A04);
            A0h2.append(" smsPrefix: ");
            A0h2.append(A0A);
            A0h2.append(" verificationData:");
            r6.A06(C12960it.A0d(C1309060l.A00(A2p), A0h2));
        } catch (IllegalArgumentException | IllegalStateException e4) {
            r6.A0A("IndiaUpiDeviceBindActivity showSmsErrorAndFinish after sendDeviceBindingSms threw: ", e4);
            C64513Fv r13 = indiaUpiDeviceBindStepActivity.A0C;
            if (r13 != null) {
                r13.A06("device-binding-sms", -1);
            }
            indiaUpiDeviceBindStepActivity.A3C(false);
            AbstractActivityC119235dO.A1h(indiaUpiDeviceBindStepActivity, R.string.payments_error_sms, true);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void A30() {
        WaImageView waImageView;
        int i;
        String str = this.A0P;
        switch (str.hashCode()) {
            case 49:
                if (str.equals("1")) {
                    this.A0P = "2";
                    markStepDone(this.A05);
                    markStepProcessing(this.A03);
                    markStepDisabled(this.A04);
                    waImageView = this.A06;
                    i = R.drawable.ic_verify_bank;
                    break;
                }
                this.A0P = "1";
                markStepProcessing(this.A05);
                markStepDisabled(this.A03);
                markStepDisabled(this.A04);
                waImageView = this.A06;
                i = R.drawable.ic_send_sms;
                break;
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS:
                if (str.equals("2")) {
                    A37(this.A0B);
                    return;
                }
                this.A0P = "1";
                markStepProcessing(this.A05);
                markStepDisabled(this.A03);
                markStepDisabled(this.A04);
                waImageView = this.A06;
                i = R.drawable.ic_send_sms;
                break;
            case 51:
                if (str.equals("3")) {
                    A38(this.A0B, this.A0Q);
                    return;
                }
                this.A0P = "1";
                markStepProcessing(this.A05);
                markStepDisabled(this.A03);
                markStepDisabled(this.A04);
                waImageView = this.A06;
                i = R.drawable.ic_send_sms;
                break;
            case 52:
                if (str.equals("4")) {
                    A31();
                    return;
                }
                this.A0P = "1";
                markStepProcessing(this.A05);
                markStepDisabled(this.A03);
                markStepDisabled(this.A04);
                waImageView = this.A06;
                i = R.drawable.ic_send_sms;
                break;
            default:
                this.A0P = "1";
                markStepProcessing(this.A05);
                markStepDisabled(this.A03);
                markStepDisabled(this.A04);
                waImageView = this.A06;
                i = R.drawable.ic_send_sms;
                break;
        }
        C12990iw.A0x(this, waImageView, i);
    }

    public final void A31() {
        this.A0W.A06("PAY: continueOnFinishDeviceBind called");
        C32641cU A01 = ((AbstractActivityC121685jC) this).A0I.A01("add_bank");
        C32641cU A012 = ((AbstractActivityC121685jC) this).A0I.A01("2fa");
        ((AbstractActivityC121685jC) this).A0I.A06(A01);
        ((AbstractActivityC121685jC) this).A0I.A06(A012);
        AbstractActivityC119235dO.A0n(C12990iw.A0D(this, IndiaUpiAccountRecoveryFinishActivity.class), this);
    }

    public final void A32() {
        if (this.A07.A02("android.permission.RECEIVE_SMS") != 0 && this.A07.A02("android.permission.SEND_SMS") == 0) {
            AnonymousClass00T.A0E(this, new String[]{"android.permission.RECEIVE_SMS"}, 100);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x003b, code lost:
        if (r0.getSimState() != 5) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A33() {
        /*
            r3 = this;
            X.5l4 r0 = r3.A0J
            java.lang.String r1 = "smsSend"
            X.1Q5 r0 = r0.A00
            r0.A08(r1)
            java.lang.String r0 = "1"
            r3.A0P = r0
            android.view.View r0 = r3.A05
            r3.markStepProcessing(r0)
            android.view.View r0 = r3.A03
            r3.markStepDisabled(r0)
            android.view.View r0 = r3.A04
            r3.markStepDisabled(r0)
            com.whatsapp.WaImageView r1 = r3.A06
            r0 = 2131232091(0x7f08055b, float:1.8080281E38)
            X.C12990iw.A0x(r3, r1, r0)
            X.5l5 r0 = r3.A0K
            java.lang.String r1 = "deviceBindingStarted"
            X.1Q5 r0 = r0.A00
            r0.A09(r1)
            X.01d r0 = r3.A08
            android.telephony.TelephonyManager r0 = r0.A0N()
            if (r0 == 0) goto L_0x003d
            int r2 = r0.getSimState()
            r0 = 5
            r1 = 1
            if (r2 == r0) goto L_0x003e
        L_0x003d:
            r1 = 0
        L_0x003e:
            boolean r0 = X.C18640sm.A03(r3)
            if (r0 == 0) goto L_0x004c
            r1 = 2131890436(0x7f121104, float:1.9415564E38)
        L_0x0047:
            r0 = 1
            X.AbstractActivityC119235dO.A1h(r3, r1, r0)
            return
        L_0x004c:
            if (r1 != 0) goto L_0x0052
            r1 = 2131890438(0x7f121106, float:1.9415568E38)
            goto L_0x0047
        L_0x0052:
            X.5oG r1 = new X.5oG
            r1.<init>(r3)
            r3.A0G = r1
            X.0lR r0 = r3.A05
            X.C12990iw.A1N(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity.A33():void");
    }

    public final void A34() {
        String str;
        String str2 = this.A0P;
        switch (str2.hashCode()) {
            case 49:
                if (str2.equals("1")) {
                    markStepDisabled(this.A05);
                    markStepDisabled(this.A03);
                    markStepDisabled(this.A04);
                }
                return;
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS:
                if (str2.equals("2")) {
                    markStepDone(this.A05);
                    markStepDisabled(this.A03);
                    markStepDisabled(this.A04);
                }
                return;
            case 51:
                str = "3";
                break;
            case 52:
                str = "4";
                break;
            default:
                return;
        }
        if (str2.equals(str)) {
            markStepDone(this.A05);
            markStepDone(this.A03);
            markStepDisabled(this.A04);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002a, code lost:
        if (r2 == com.whatsapp.R.string.no_internet_message) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A35(int r6) {
        /*
            r5 = this;
            X.69E r1 = r5.A0I
            X.3Fv r0 = r5.A0C
            X.60V r4 = r1.A04(r0, r6)
            X.1Zj r3 = r5.A0W
            java.lang.String r0 = "onDeviceBinding failure. showErrorAndFinish: "
            java.lang.StringBuilder r2 = X.C12960it.A0k(r0)
            X.3Fv r1 = r5.A0C
            java.lang.String r0 = "upi-bind-device"
            X.C117315Zl.A0V(r1, r0, r2)
            X.C117295Zj.A1F(r3, r2)
            int r2 = r4.A00
            r0 = 2131890382(0x7f1210ce, float:1.9415454E38)
            if (r2 == r0) goto L_0x002c
            r0 = 2131890446(0x7f12110e, float:1.9415584E38)
            if (r2 == r0) goto L_0x002c
            r1 = 2131889594(0x7f120dba, float:1.9413856E38)
            r0 = 1
            if (r2 != r1) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r5.A39(r4, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity.A35(int):void");
    }

    public final void A36(View view, String str, String str2) {
        TextView A0J = C12960it.A0J(view, R.id.bind_step_number);
        if (A0J != null) {
            A0J.setText(str);
        }
        TextView A0J2 = C12960it.A0J(view, R.id.bind_step_desc);
        if (A0J2 != null) {
            A0J2.setText(str2);
        }
        markStepDisabled(view);
    }

    public final void A37(C452120p r11) {
        int i;
        int i2;
        long j;
        this.A0T = false;
        String A0Z = AbstractActivityC119235dO.A0Z(this);
        AnonymousClass6BE r1 = ((AbstractActivityC121665jA) this).A0D;
        r1.A05(A0Z);
        AnonymousClass2SP A01 = r1.A01(r11, 20);
        A01.A0O = this.A0A.A0C;
        C122205l5 r0 = this.A0K;
        if (r11 != null) {
            AbstractActivityC119235dO.A1j(r0, 3);
            AbstractActivityC119235dO.A1j(this.A0J, 3);
        } else {
            r0.A00.A09("deviceBindingEnded");
            this.A0J.A00.A07("deviceBind");
        }
        int i3 = this.A0E.A00;
        int i4 = i3 - 1;
        if (i3 == 0) {
            i4 = 0;
        }
        A01.A0K = Long.valueOf((long) i4);
        int i5 = i3 - 1;
        if (i3 == 0) {
            i5 = 0;
        }
        long j2 = 0;
        for (int i6 = 0; i6 <= i5; i6++) {
            long[] jArr = AnonymousClass60I.A0J;
            if (i6 < jArr.length) {
                j = jArr[i6];
            } else {
                j = ((long) i6) * 5;
            }
            j2 += j;
        }
        A01.A0L = Long.valueOf(j2);
        C1308460e r3 = ((AbstractActivityC121665jA) this).A0A;
        A01.A0M = Long.valueOf((long) r3.A02);
        A01.A0N = r3.A04(this.A0A);
        A01.A0Z = "device_binding";
        C30931Zj r32 = this.A0W;
        r32.A06(C12960it.A0d(A01.toString(), C12960it.A0k("PaymentUserActionEvent devicebind event:")));
        AbstractActivityC119235dO.A1b(A01, this);
        StringBuilder A0k = C12960it.A0k("IndiaUpiDeviceBindActivity: onDeviceBinding: ");
        A0k.append(C12980iv.A1X(r11));
        C117295Zj.A1F(r32, A0k);
        if (r11 == null || (i = r11.A00) == 11453) {
            this.A0J.A00.A08("getAccounts");
            C12990iw.A1N(new AnonymousClass5oU(this, ((AbstractActivityC121665jA) this).A0B.A07()), ((ActivityC13830kP) this).A05);
        } else if (!AnonymousClass69E.A02(this, "upi-bind-device", i, true)) {
            int i7 = r11.A00;
            if (i7 != 476) {
                if (i7 != 11452) {
                    if (!(i7 == 11477 || i7 == 11544)) {
                        if (i7 != 11469) {
                            if (i7 != 11470) {
                                StringBuilder A0k2 = C12960it.A0k("onDeviceBinding failure. showErrorAndFinish at error: ");
                                C117315Zl.A0V(this.A0C, "upi-bind-device", A0k2);
                                C117295Zj.A1F(r32, A0k2);
                                i2 = 2;
                            }
                        } else if (this.A0C.A07("upi-bind-device")) {
                            this.A0T = true;
                            StringBuilder A0k3 = C12960it.A0k("onDeviceBinding failure. Retry delayedDeviceVerifIqHandlerMessage at error: ");
                            C117315Zl.A0V(this.A0C, "upi-bind-device", A0k3);
                            C117295Zj.A1F(r32, A0k3);
                            this.A0E.A00();
                            return;
                        } else if (this.A0C.A00("upi-bind-device") >= 3) {
                            AbstractActivityC119235dO.A1g(this);
                            this.A00 = 4;
                            A35(this.A0C.A00);
                            ((AbstractActivityC121665jA) this).A0A.A08();
                            return;
                        } else {
                            return;
                        }
                    }
                    AbstractActivityC119235dO.A1g(this);
                    i2 = 3;
                } else {
                    AbstractActivityC119235dO.A1g(this);
                    i2 = 4;
                }
                this.A00 = i2;
                A35(r11.A00);
                return;
            }
            AbstractActivityC119235dO.A1g(this);
            AbstractActivityC119235dO.A1h(this, R.string.upi_twenty_four_hour_multiple_phone_number_device_bind_error, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0034, code lost:
        if (r9.size() <= 0) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A38(X.C452120p r8, java.util.ArrayList r9) {
        /*
        // Method dump skipped, instructions count: 331
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity.A38(X.20p, java.util.ArrayList):void");
    }

    public final void A39(AnonymousClass60V r6, boolean z) {
        int i = r6.A00;
        C30931Zj r3 = this.A0W;
        r3.A06(C12960it.A0W(i, "IndiaUpiDeviceBindActivity showErrorAndFinish: "));
        A34();
        if (i == 0) {
            i = R.string.payments_setup_error;
            String str = this.A0C.A04;
            if ("upi-bind-device".equalsIgnoreCase(str)) {
                i = R.string.device_binding_failure_reasons_bullet_list_title;
            }
            if ("upi-get-accounts".equalsIgnoreCase(str)) {
                this.A00 = 1;
                i = R.string.get_accounts_failure_reason;
            }
        }
        if (z) {
            C64513Fv r0 = this.A0C;
            if (r0 != null) {
                r0.A01();
                StringBuilder A0k = C12960it.A0k("clearStates: ");
                A0k.append(this.A0C);
                C117295Zj.A1F(r3, A0k);
            }
            ((AbstractActivityC121665jA) this).A0A.A04 = new C64513Fv();
            Intent A0D = C12990iw.A0D(this, IndiaUpiOnboardingErrorEducationActivity.class);
            if (r6.A01 != null) {
                A0D.putExtra("error_text", r6.A01(this));
            }
            A0D.putExtra("error", i);
            A0D.putExtra("error_type", this.A00);
            int i2 = this.A00;
            if (i2 >= 1 && i2 <= 6) {
                C117315Zl.A0M(A0D, this.A0A);
            }
            if (!((AbstractActivityC121665jA) this).A0N) {
                A0D.putExtra("try_again", 1);
            }
            A0D.addFlags(335544320);
            A2v(A0D);
            A2G(A0D, true);
        } else {
            AbstractActivityC119235dO.A1J(this, r6);
        }
        AbstractActivityC119235dO.A1j(this.A0K, 3);
        AbstractActivityC119235dO.A1j(this.A0J, 3);
    }

    public final void A3A(Integer num) {
        AnonymousClass2SP r1 = this.A0V;
        r1.A08 = num;
        r1.A09 = C12960it.A0V();
        r1.A0Z = "device_binding";
        AbstractActivityC119235dO.A1b(r1, this);
    }

    public final void A3B(String str) {
        if (this.A0S) {
            this.A0W.A06("PAY: getAccountsOrFinishAfterDeviceBinding called");
            this.A0P = "4";
            markStepDone(this.A05);
            markStepDone(this.A03);
            markStepProcessing(this.A04);
            C12990iw.A0x(this, this.A06, R.drawable.ic_account_search);
            A31();
            return;
        }
        this.A0P = "3";
        markStepDone(this.A05);
        markStepDone(this.A03);
        markStepProcessing(this.A04);
        C12990iw.A0x(this, this.A06, R.drawable.ic_account_search);
        C30931Zj r2 = this.A0W;
        StringBuilder A0k = C12960it.A0k("getAccountsAfterDeviceBinding: bank picked and calling sendGetBankAccounts for: ");
        A0k.append(((AbstractC30851Zb) this.A0A).A01);
        A0k.append(" accountProvider:");
        A0k.append(this.A0A.A0A);
        A0k.append(" psp: ");
        r2.A06(C12960it.A0d(str, A0k));
        this.A0E.A01(this.A0A);
        ((AbstractActivityC121665jA) this).A0D.AeG();
    }

    public final void A3C(boolean z) {
        String A0Z = AbstractActivityC119235dO.A0Z(this);
        AnonymousClass6BE r0 = ((AbstractActivityC121665jA) this).A0D;
        r0.A05(A0Z);
        AnonymousClass2SP A8H = r0.A8H();
        A8H.A0O = this.A0A.A0C;
        A8H.A0Z = "db_sms_sent";
        int i = 28;
        if (z) {
            i = 27;
        }
        A8H.A08 = Integer.valueOf(i);
        this.A0W.A06(C12960it.A0d(A8H.toString(), C12960it.A0k("PaymentUserActionEvent smsSent event: ")));
        AbstractActivityC119235dO.A1b(A8H, this);
    }

    @Override // X.AnonymousClass6MR
    public void AN8(C452120p r2, ArrayList arrayList) {
        if (((AbstractActivityC121665jA) this).A0P) {
            this.A0Q = arrayList;
            this.A0B = r2;
            return;
        }
        A38(r2, arrayList);
    }

    @Override // X.AnonymousClass6MR
    public void AP4(C452120p r2) {
        if (((AbstractActivityC121665jA) this).A0P) {
            this.A0B = r2;
        } else {
            A37(r2);
        }
    }

    public final void markStepDisabled(View view) {
        View findViewById = view.findViewById(R.id.bind_step_number);
        if (findViewById != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.step_circle);
            drawable.setColorFilter(AnonymousClass00T.A00(this, R.color.step_disabled), PorterDuff.Mode.SRC_OVER);
            findViewById.setBackground(drawable);
        }
        View findViewById2 = view.findViewById(R.id.bind_step_number_progress);
        if (findViewById2 != null) {
            findViewById2.setVisibility(4);
        }
        TextView A0J = C12960it.A0J(view, R.id.bind_step_desc);
        if (A0J != null) {
            C12960it.A0s(this, A0J, R.color.step_text_disabled);
        }
    }

    public final void markStepDone(View view) {
        View findViewById = view.findViewById(R.id.bind_step_number);
        if (findViewById != null) {
            findViewById.setVisibility(4);
        }
        View findViewById2 = view.findViewById(R.id.bind_step_number_progress);
        if (findViewById2 != null) {
            findViewById2.setVisibility(4);
        }
        View findViewById3 = view.findViewById(R.id.bind_step_check);
        if (findViewById3 != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.step_circle);
            drawable.setColorFilter(AnonymousClass00T.A00(this, R.color.step_completed), PorterDuff.Mode.SRC_OVER);
            findViewById3.setBackground(drawable);
            findViewById3.setVisibility(0);
        }
        TextView A0J = C12960it.A0J(view, R.id.bind_step_desc);
        if (A0J != null) {
            C12960it.A0s(this, A0J, R.color.step_text_disabled);
        }
    }

    public final void markStepProcessing(View view) {
        View findViewById = view.findViewById(R.id.bind_step_number);
        if (findViewById != null) {
            findViewById.setVisibility(0);
            Drawable drawable = getResources().getDrawable(R.drawable.step_circle);
            drawable.setColorFilter(AnonymousClass00T.A00(this, R.color.step_processing), PorterDuff.Mode.SRC_OVER);
            findViewById.setBackground(drawable);
        }
        View findViewById2 = view.findViewById(R.id.bind_step_number_progress);
        if (findViewById2 != null) {
            findViewById2.setVisibility(0);
        }
        View findViewById3 = view.findViewById(R.id.bind_step_check);
        if (findViewById3 != null) {
            findViewById3.setVisibility(4);
        }
        TextView A0J = C12960it.A0J(view, R.id.bind_step_desc);
        if (A0J != null) {
            C12960it.A0s(this, A0J, R.color.primary_text);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 154) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            A32();
            A33();
        } else {
            Ado(R.string.payments_sms_permission_msg);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C30931Zj r2 = this.A0W;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this);
        r2.A06(C12960it.A0d(" onBackPressed", A0h));
        A3A(C12960it.A0V());
        A2s();
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0S = ((AbstractActivityC121665jA) this).A0B.A0L();
        this.A0J.A00(getIntent());
        this.A0J.A00.A08("onCreate");
        setContentView(R.layout.india_upi_device_bind_steps);
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            C117295Zj.A0g(this, A0K, R.string.payments_device_bind_actionbar_title_text);
        }
        this.A05 = findViewById(R.id.bind_step_1);
        this.A03 = findViewById(R.id.bind_step_2);
        this.A04 = findViewById(R.id.bind_step_3);
        A36(this.A05, getString(R.string.payments_device_bind_step_1), getString(R.string.payments_device_bind_sms_step));
        A36(this.A03, getString(R.string.payments_device_bind_step_2), getString(R.string.payments_device_bind_verification_step));
        boolean z = this.A0S;
        int i = R.string.payments_device_bind_get_account_step;
        if (z) {
            i = R.string.payments_device_bind_finish_step;
        }
        A36(this.A04, getString(R.string.payments_device_bind_step_3), getString(i));
        this.A06 = (WaImageView) findViewById(R.id.ic_bind_top);
        this.A0C = ((AbstractActivityC121665jA) this).A0A.A04;
        C119755f3 r6 = (C119755f3) getIntent().getParcelableExtra("extra_selected_bank");
        this.A0A = r6;
        C14900mE r2 = ((ActivityC13810kN) this).A05;
        C16590pI r3 = ((AbstractActivityC121685jC) this).A07;
        AbstractC14440lR r15 = ((ActivityC13830kP) this).A05;
        C17220qS r5 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r14 = this.A0M;
        C17070qD r11 = ((AbstractActivityC121685jC) this).A0P;
        C1308460e r7 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r10 = ((AbstractActivityC121685jC) this).A0M;
        this.A0E = new AnonymousClass60I(r2, r3, this.A08, r5, r6, r7, ((AbstractActivityC121665jA) this).A0B, ((AbstractActivityC121685jC) this).A0K, r10, r11, this, this.A0L, r14, r15);
        this.A0W.A06(C12960it.A0d(((AbstractActivityC121665jA) this).A0B.toString(), C12960it.A0k("IndiaUpiDeviceBindActivity onCreate: device binding status: ")));
        String A07 = ((AbstractActivityC121665jA) this).A0B.A07();
        if (((AbstractActivityC121665jA) this).A0B.A0M(this.A0A, ((AbstractActivityC121665jA) this).A0D, A07)) {
            try {
                JSONObject A0a = C117295Zj.A0a();
                AbstractActivityC119235dO.A1c(this, "DeviceBindingStep", A0a);
                A0a.put("pspForDeviceBinding", A07);
                A0a.put("isDeviceBindingDone", ((AbstractActivityC121665jA) this).A0B.A0M(this.A0A, ((AbstractActivityC121665jA) this).A0D, A07));
                C120475gF r13 = new C120475gF(((ActivityC13790kL) this).A05, ((ActivityC13810kN) this).A0C, ((AbstractActivityC121685jC) this).A0H, ((AbstractActivityC121665jA) this).A0A, ((AbstractActivityC121685jC) this).A0M);
                r13.A00 = A0a;
                r13.A00("SKIPPED_DEVICE_BINDING", null);
            } catch (Exception unused) {
            }
            A3B(A07);
        } else {
            this.A0C.A02("upi-educate-sms");
            this.A01 = ((AbstractActivityC121665jA) this).A0B.A03();
            A33();
        }
        onConfigurationChanged(C12980iv.A0H(this));
        this.A0J.A00.A07("onCreate");
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60I r2 = this.A0E;
        r2.A02 = null;
        r2.A03.removeCallbacksAndMessages(null);
        r2.A01.quit();
        C117385Zs r0 = this.A0H;
        if (r0 != null) {
            unregisterReceiver(r0);
            this.A0H = null;
        }
        PendingIntent pendingIntent = this.A02;
        if (pendingIntent != null) {
            pendingIntent.cancel();
            this.A02 = null;
        }
        C123985oG r1 = this.A0G;
        if (r1 != null) {
            r1.A03(false);
        }
        Runnable runnable = this.A0O;
        if (runnable != null) {
            ((ActivityC13830kP) this).A05.AaP(runnable);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        C30931Zj r2 = this.A0W;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this);
        r2.A06(C12960it.A0d(" action bar home", A0h));
        A3A(1);
        A2s();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (this.A0U) {
            AbstractActivityC119235dO.A1h(this, R.string.payments_error_sms_backgrounded, true);
            return;
        }
        Runnable runnable = this.A0O;
        if (runnable != null) {
            ((ActivityC13830kP) this).A05.AaP(runnable);
            this.A0O = null;
            A30();
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        if (this.A0T && this.A0O == null) {
            this.A0O = ((ActivityC13830kP) this).A05.AbK(new Runnable() { // from class: X.6G0
                @Override // java.lang.Runnable
                public final void run() {
                    IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = IndiaUpiDeviceBindStepActivity.this;
                    if (!indiaUpiDeviceBindStepActivity.A0U) {
                        indiaUpiDeviceBindStepActivity.A0W.A06("IndiaUpiDeviceBindActivity: device binding canceled");
                        indiaUpiDeviceBindStepActivity.A0U = true;
                        indiaUpiDeviceBindStepActivity.A0E.A02 = null;
                        AbstractActivityC119235dO.A1g(indiaUpiDeviceBindStepActivity);
                        ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0P = false;
                    }
                }
            }, "IndiaUpiDeviceBindSetupActivity/onStop", ((long) ((ActivityC13810kN) this).A0C.A02(924)) * 1000);
        }
    }
}
