package com.whatsapp.payments.ui;

import X.AbstractActivityC119275dS;
import X.AbstractActivityC121755jw;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass60Y;
import X.AnonymousClass61F;
import X.C117295Zj;
import X.C117305Zk;
import X.C129235xO;
import X.C12970iu;
import android.os.Bundle;
import android.view.Menu;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviPaymentCardDetailsActivity extends AbstractActivityC121755jw {
    public AnonymousClass60Y A00;
    public AnonymousClass61F A01;
    public C129235xO A02;
    public boolean A03;

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public NoviPaymentCardDetailsActivity() {
        this(0);
    }

    public NoviPaymentCardDetailsActivity(int i) {
        this.A03 = false;
        C117295Zj.A0p(this, 90);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119275dS.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            AbstractActivityC119275dS.A03(A1M, this);
            this.A00 = C117305Zk.A0W(A1M);
            this.A01 = C117305Zk.A0X(A1M);
            this.A02 = (C129235xO) A1M.ADR.get();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00c2, code lost:
        if (r1.equals("SUSPENDED") == false) goto L_0x0055;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e1  */
    @Override // X.AbstractActivityC121755jw, X.AbstractView$OnClickListenerC121765jx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2i(X.AbstractC28901Pl r9, boolean r10) {
        /*
            r8 = this;
            super.A2i(r9, r10)
            if (r10 == 0) goto L_0x0008
            r8.A2k()
        L_0x0008:
            android.content.Intent r0 = r8.getIntent()
            r5 = 0
            if (r0 == 0) goto L_0x0019
            android.content.Intent r1 = r8.getIntent()
            java.lang.String r0 = "extra_number_of_payment_methods"
            int r5 = r1.getIntExtra(r0, r5)
        L_0x0019:
            X.1ZY r4 = r9.A08
            boolean r0 = r4 instanceof X.C119765f4
            r3 = 0
            if (r0 == 0) goto L_0x005f
            r0 = r4
            X.1Zd r0 = (X.AbstractC30871Zd) r0
            java.lang.String r1 = r0.A0I
            android.widget.FrameLayout r0 = r8.A00
            r0.removeAllViews()
            android.view.LayoutInflater r2 = r8.getLayoutInflater()
            r0 = 2131559373(0x7f0d03cd, float:1.8744088E38)
            android.view.View r7 = r2.inflate(r0, r3)
            r0 = 2131364653(0x7f0a0b2d, float:1.834915E38)
            android.widget.ImageView r6 = X.C12970iu.A0K(r7, r0)
            r0 = 2131364654(0x7f0a0b2e, float:1.8349151E38)
            android.widget.TextView r2 = X.C12960it.A0I(r7, r0)
            android.widget.FrameLayout r0 = r8.A00
            r0.addView(r7)
            X.5yh r0 = r8.A0F
            r0.A00()
            int r0 = r1.hashCode()
            r7 = -1
            switch(r0) {
                case -591252731: goto L_0x0098;
                case 1124965819: goto L_0x00bc;
                case 1925346054: goto L_0x00ec;
                default: goto L_0x0055;
            }
        L_0x0055:
            r1 = 8
            switch(r7) {
                case 1: goto L_0x00c5;
                case 2: goto L_0x00e1;
                default: goto L_0x005a;
            }
        L_0x005a:
            android.widget.FrameLayout r0 = r8.A00
            r0.setVisibility(r1)
        L_0x005f:
            android.view.LayoutInflater r1 = r8.getLayoutInflater()
            r0 = 2131559374(0x7f0d03ce, float:1.874409E38)
            android.view.View r2 = r1.inflate(r0, r3)
            r0 = 2131100178(0x7f060212, float:1.781273E38)
            int r0 = X.AnonymousClass00T.A00(r8, r0)
            r8.A00 = r0
            r0 = 2131364658(0x7f0a0b32, float:1.834916E38)
            android.widget.ImageView r1 = X.C12970iu.A0K(r2, r0)
            int r0 = r8.A00
            X.AnonymousClass2GE.A07(r1, r0)
            r0 = 2131366813(0x7f0a139d, float:1.835353E38)
            android.view.View r0 = r8.findViewById(r0)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            r0.addView(r2)
            X.64a r0 = new X.64a
            r0.<init>(r9, r4, r8, r5)
            r2.setOnClickListener(r0)
            r0 = 1
            r8.setResult(r0)
            return
        L_0x0098:
            java.lang.String r0 = "EXPIRED"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0055
            android.content.res.Resources r1 = r8.getResources()
            r0 = 2131231878(0x7f080486, float:1.807985E38)
            android.graphics.drawable.Drawable r0 = r1.getDrawable(r0)
            r6.setImageDrawable(r0)
            android.content.Context r1 = r6.getContext()
            r0 = 2131100621(0x7f0603cd, float:1.7813629E38)
            X.AnonymousClass2GE.A05(r1, r6, r0)
            r0 = 2131889804(0x7f120e8c, float:1.9414282E38)
            goto L_0x00e7
        L_0x00bc:
            java.lang.String r0 = "SUSPENDED"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00c5
            goto L_0x0055
        L_0x00c5:
            android.content.res.Resources r1 = r8.getResources()
            r0 = 2131231878(0x7f080486, float:1.807985E38)
            android.graphics.drawable.Drawable r0 = r1.getDrawable(r0)
            r6.setImageDrawable(r0)
            android.content.Context r1 = r6.getContext()
            r0 = 2131100621(0x7f0603cd, float:1.7813629E38)
            X.AnonymousClass2GE.A05(r1, r6, r0)
            r0 = 2131889803(0x7f120e8b, float:1.941428E38)
            goto L_0x00e7
        L_0x00e1:
            r6.setVisibility(r1)
            r0 = 2131889802(0x7f120e8a, float:1.9414278E38)
        L_0x00e7:
            r2.setText(r0)
            goto L_0x005f
        L_0x00ec:
            java.lang.String r0 = "ACTIVE"
            short r7 = X.C117305Zk.A0v(r0, r1)
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPaymentCardDetailsActivity.A2i(X.1Pl, boolean):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass60Y.A02(this.A00, "BACK_CLICK", "NOVI_HUB", "FI_INFO", "ARROW");
    }

    @Override // X.AbstractActivityC121755jw, X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117305Zk.A06(this, R.id.help_icon).setImageResource(R.drawable.ic_help_novi);
        C12970iu.A0M(this, R.id.help_label).setText(R.string.novi_help_center);
        C117295Zj.A0r(this, this.A01.A0G, 97);
        AnonymousClass60Y.A02(this.A00, "NAVIGATION_START", "NOVI_HUB", "FI_INFO", "SCREEN");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(this.A00, "NAVIGATION_END", "NOVI_HUB", "FI_INFO", "SCREEN");
    }
}
