package com.whatsapp.payments.ui.invites;

import X.AnonymousClass102;
import X.AnonymousClass17Z;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.AnonymousClass61M;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C117315Zl;
import X.C117905ax;
import X.C118215bS;
import X.C121265hX;
import X.C129925yW;
import X.C1329668y;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C18600si;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C21860y6;
import X.C22710zW;
import android.os.Bundle;
import android.view.View;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentInviteFragment extends Hilt_IndiaUpiPaymentInviteFragment {
    public C14900mE A00;
    public C15570nT A01;
    public C18640sm A02;
    public C14830m7 A03;
    public AnonymousClass102 A04;
    public C14850m9 A05;
    public C17220qS A06;
    public C129925yW A07;
    public C1329668y A08;
    public C21860y6 A09;
    public AnonymousClass18T A0A;
    public C18650sn A0B;
    public C18600si A0C;
    public AnonymousClass18S A0D;
    public C18610sj A0E;
    public AnonymousClass61M A0F;
    public C22710zW A0G;
    public C17070qD A0H;
    public AnonymousClass6BE A0I;
    public AnonymousClass17Z A0J;
    public C117905ax A0K;
    public C121265hX A0L;

    @Override // com.whatsapp.payments.ui.invites.PaymentInviteFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C117905ax r0 = (C117905ax) C117315Zl.A06(new C118215bS(this), this).A00(C117905ax.class);
        this.A0K = r0;
        C117295Zj.A0s(A0G(), r0.A00, this, 126);
        C117295Zj.A0s(A0G(), this.A0K.A01, this, 125);
        super.A17(bundle, view);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        if (((X.C133766Ca) r1).A0F == false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1D(X.AnonymousClass2SP r7) {
        /*
            r6 = this;
            X.17Z r0 = r6.A0J
            X.2S0 r2 = r0.A00()
            X.0m7 r0 = r6.A03
            long r0 = X.C117295Zj.A03(r0)
            int r3 = r2.A00(r0)
            r1 = 4
            r5 = 1
            if (r3 == r5) goto L_0x001a
            r0 = 2
            if (r3 == r0) goto L_0x001a
            if (r3 == r1) goto L_0x001a
            return
        L_0x001a:
            X.5hA r4 = new X.5hA
            r4.<init>()
            r2 = 0
            boolean r1 = X.C12960it.A1V(r3, r1)
            java.lang.String r0 = "is_ended_early"
            r4.A02(r0, r1)
            X.61d r1 = r6.A06
            if (r1 == 0) goto L_0x0038
            boolean r0 = r1 instanceof X.C133766Ca
            if (r0 == 0) goto L_0x0038
            X.6Ca r1 = (X.C133766Ca) r1
            boolean r0 = r1.A0F
            r3 = 1
            if (r0 != 0) goto L_0x0039
        L_0x0038:
            r3 = 0
        L_0x0039:
            X.3FW[] r1 = new X.AnonymousClass3FW[r5]
            r1[r2] = r4
            r0 = 0
            X.3FW r2 = new X.3FW
            r2.<init>(r0, r1)
            if (r3 == 0) goto L_0x004c
            java.lang.String r1 = "section"
            java.lang.String r0 = "incentive_banner"
            r2.A01(r1, r0)
        L_0x004c:
            java.lang.String r0 = r2.toString()
            r7.A0X = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.invites.IndiaUpiPaymentInviteFragment.A1D(X.2SP):void");
    }
}
