package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121685jC;
import X.AbstractC118095bG;
import X.AbstractC128555wI;
import X.AbstractC1310961f;
import X.AbstractC136516Mv;
import X.AbstractC14640lm;
import X.AbstractC30791Yv;
import X.ActivityC000800j;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1KS;
import X.AnonymousClass2FL;
import X.AnonymousClass60Y;
import X.AnonymousClass63Y;
import X.AnonymousClass6D0;
import X.AnonymousClass6F2;
import X.AnonymousClass6M9;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118385bj;
import X.C118395bk;
import X.C121415ht;
import X.C121425hu;
import X.C123465nC;
import X.C123475nD;
import X.C126095sK;
import X.C127135u0;
import X.C127145u1;
import X.C127155u2;
import X.C127505ub;
import X.C127785v3;
import X.C128295vs;
import X.C128305vt;
import X.C128375w0;
import X.C129295xU;
import X.C1315463e;
import X.C134106Di;
import X.C14580lf;
import X.C22460z7;
import X.C22540zF;
import X.C30821Yy;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.math.BigDecimal;

/* loaded from: classes4.dex */
public class NoviAmountEntryActivity extends AbstractActivityC121685jC implements AbstractC136516Mv, AbstractC1310961f, AnonymousClass6M9 {
    public C22540zF A00;
    public C22460z7 A01;
    public AnonymousClass60Y A02;
    public AbstractC128555wI A03;
    public C134106Di A04;
    public AbstractC118095bG A05;
    public C128375w0 A06;
    public PaymentView A07;
    public C129295xU A08;
    public String A09;
    public String A0A;
    public boolean A0B;

    @Override // X.AbstractC136516Mv
    public ActivityC000800j AAa() {
        return this;
    }

    @Override // X.AbstractC136516Mv
    public String AFG() {
        return null;
    }

    @Override // X.AbstractC136516Mv
    public boolean AJr() {
        return true;
    }

    @Override // X.AbstractC136516Mv
    public boolean AK3() {
        return false;
    }

    @Override // X.AbstractC1310961f
    public void ALy() {
    }

    @Override // X.AbstractC136486Ms
    public void APq(String str) {
    }

    @Override // X.AbstractC136486Ms
    public void AQf(String str, boolean z) {
    }

    @Override // X.AbstractC1310961f
    public void AR5() {
    }

    @Override // X.AbstractC1310961f
    public void ATT() {
    }

    @Override // X.AbstractC1310961f
    public void ATV() {
    }

    @Override // X.AbstractC1310961f
    public /* synthetic */ void ATa() {
    }

    @Override // X.AbstractC1310961f
    public void AV5(C30821Yy r1, String str) {
    }

    @Override // X.AbstractC1310961f
    public void AVp(C30821Yy r1) {
    }

    @Override // X.AbstractC1310961f
    public void AVq() {
    }

    @Override // X.AbstractC1310961f
    public void AVs() {
    }

    @Override // X.AbstractC1310961f
    public void AXP(boolean z) {
    }

    @Override // android.view.Window.Callback
    public void onPointerCaptureChanged(boolean z) {
    }

    public NoviAmountEntryActivity() {
        this(0);
    }

    public NoviAmountEntryActivity(int i) {
        this.A0B = false;
        C117295Zj.A0p(this, 79);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            this.A02 = C117305Zk.A0W(A1M);
            this.A08 = (C129295xU) A1M.A0I.get();
            this.A01 = (C22460z7) A1M.AEF.get();
            this.A00 = (C22540zF) A1M.AEC.get();
            this.A06 = C117315Zl.A0E(A1M);
        }
    }

    @Override // X.AbstractC136486Ms
    public void AMA(String str) {
        BigDecimal bigDecimal;
        AbstractC118095bG r3 = this.A05;
        if (r3.A01 != null) {
            if (TextUtils.isEmpty(str) || (bigDecimal = r3.A01.AAD(r3.A0G, str)) == null) {
                bigDecimal = new BigDecimal(0);
            }
            AnonymousClass6F2 r1 = new AnonymousClass6F2(r3.A01, C117295Zj.A0D(r3.A01, bigDecimal));
            r3.A02 = r1;
            r3.A0D.A0B(r1);
        }
    }

    @Override // X.AnonymousClass6M9
    public /* bridge */ /* synthetic */ Object AZb() {
        Parcelable parcelableExtra = getIntent().getParcelableExtra("account_info");
        AnonymousClass009.A06(parcelableExtra, "novi account is null");
        AnonymousClass63Y r0 = ((C1315463e) parcelableExtra).A00;
        AnonymousClass009.A05(r0);
        AbstractC30791Yv r11 = r0.A00;
        AbstractC14640lm r02 = ((AbstractActivityC121685jC) this).A0E;
        String str = this.A0h;
        AnonymousClass1KS r03 = this.A0c;
        Integer num = this.A0e;
        String str2 = this.A0n;
        C127155u2 r8 = new C127155u2(0, 0);
        C126095sK r4 = new C126095sK(false);
        C127135u0 r3 = new C127135u0(NumberEntryKeyboard.A00(((ActivityC13830kP) this).A01), this.A0q);
        String str3 = this.A0l;
        String str4 = this.A0i;
        String str5 = this.A0k;
        C127505ub r6 = new C127505ub(r11, null, 0);
        Pair pair = new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolAfterAmount), new int[]{12, 0, 4, 0});
        Pair pair2 = new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolBeforeAmount), new int[]{4, 0, 0, 0});
        C134106Di r9 = this.A04;
        AnonymousClass018 r10 = ((ActivityC13830kP) this).A01;
        C30821Yy AEA = r11.AEA();
        C128295vs r16 = new C128295vs(pair, pair2, r6, new AnonymousClass6D0(this, r10, r11, AEA, r11.AEV(), AEA, null), r9, str3, str4, str5, R.style.NoviSendPaymentAmountInput, true, true, true);
        C127145u1 r102 = new C127145u1(null, false);
        C22460z7 r92 = this.A01;
        return new C128305vt(r02, null, this, this, r16, new C127785v3(((AbstractActivityC121685jC) this).A0C, this.A00, r92, false), r3, r4, r102, r8, r03, num, str, str2, false);
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 120) {
            if (i2 == -1) {
                AbstractC118095bG r1 = this.A05;
                C14580lf r0 = r1.A00;
                if (r0 != null) {
                    r0.A04();
                }
                r1.A00 = C117305Zk.A0C(r1.A0H);
                this.A05.A08(this);
            }
        } else if (i == 125) {
            AbstractC118095bG r12 = this.A05;
            C14580lf r02 = r12.A00;
            if (r02 != null) {
                r02.A04();
            }
            r12.A00 = C117305Zk.A0C(r12.A0H);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass60Y.A02(this.A02, "FLOW_SESSION_END", this.A09, "ENTER_AMOUNT", "SCREEN");
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra("amount_entry_type");
        AnonymousClass009.A06(stringExtra, "novi amount entry type is null");
        this.A04 = new C134106Di(stringExtra);
        if (stringExtra.equals("withdraw")) {
            this.A0A = getString(R.string.novi_withdraw_money_title);
            this.A03 = new C121425hu(getIntent(), this.A02);
            this.A05 = (AbstractC118095bG) C117315Zl.A06(new C118385bj(this, this.A06), this).A00(C123465nC.class);
            this.A09 = "WITHDRAW_MONEY";
        } else if (stringExtra.equals("deposit")) {
            this.A03 = new C121415ht();
            this.A0A = getString(R.string.novi_add_money_title);
            this.A05 = (AbstractC118095bG) C117315Zl.A06(new C118395bk(this, this.A06), this).A00(C123475nD.class);
            this.A09 = "ADD_MONEY";
            AnonymousClass60Y.A01(this.A02, "FLOW_SESSION_START", "ADD_MONEY", "ENTER_AMOUNT", "SCREEN");
        }
        setContentView(R.layout.novi_withdraw_amount_entry);
        A2h(bundle);
        AnonymousClass60Y.A02(this.A02, "NAVIGATION_START", this.A09, "ENTER_AMOUNT", "SCREEN");
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(this.A02, "NAVIGATION_END", this.A09, "ENTER_AMOUNT", "SCREEN");
    }
}
