package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass130;
import X.AnonymousClass18T;
import X.AnonymousClass1ZR;
import X.AnonymousClass68Y;
import X.AnonymousClass68Z;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13010iy;
import X.C18610sj;
import X.C30931Zj;
import X.C36021jC;
import X.C72463ee;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiVpaContactInfoActivity extends ActivityC13810kN implements View.OnClickListener {
    public View A00;
    public LinearLayout A01;
    public AnonymousClass130 A02;
    public AnonymousClass1ZR A03;
    public AnonymousClass1ZR A04;
    public AnonymousClass68Z A05;
    public AnonymousClass18T A06;
    public C18610sj A07;
    public String A08;
    public boolean A09;
    public boolean A0A;
    public final C30931Zj A0B;

    public IndiaUpiVpaContactInfoActivity() {
        this(0);
        this.A0B = C117295Zj.A0G("IndiaUpiVpaContactInfoActivity");
    }

    public IndiaUpiVpaContactInfoActivity(int i) {
        this.A09 = false;
        C117295Zj.A0p(this, 76);
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(C117295Zj.A09(this), this);
            ActivityC13810kN.A10(A1M, this);
            this.A02 = (AnonymousClass130) A1M.A41.get();
            this.A07 = C117305Zk.A0M(A1M);
            this.A06 = (AnonymousClass18T) A1M.AE9.get();
            this.A05 = (AnonymousClass68Z) A1M.A9T.get();
        }
    }

    public final void A2T(boolean z) {
        int i;
        this.A0A = z;
        ImageView A06 = C117305Zk.A06(this, R.id.block_vpa_icon);
        TextView A0M = C12970iu.A0M(this, R.id.block_vpa_text);
        int i2 = 8;
        this.A00.setVisibility(C13010iy.A00(z ? 1 : 0));
        LinearLayout linearLayout = this.A01;
        if (!z) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (z) {
            A06.setColorFilter(AnonymousClass00T.A00(this, R.color.dark_gray));
            C12960it.A0s(this, A0M, R.color.dark_gray);
            i = R.string.unblock;
        } else {
            A06.setColorFilter(AnonymousClass00T.A00(this, R.color.red_button_text));
            C12960it.A0s(this, A0M, R.color.red_button_text);
            i = R.string.block;
        }
        A0M.setText(i);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        Intent A00;
        String str;
        int i;
        if (view.getId() == R.id.send_payment_container) {
            C30931Zj r2 = this.A0B;
            StringBuilder A0k = C12960it.A0k("send payment to vpa: ");
            A0k.append(this.A03);
            C117295Zj.A1F(r2, A0k);
            A00 = this.A06.A00(this, false, true);
            A00.putExtra("extra_payment_handle", this.A03);
            A00.putExtra("extra_payment_handle_id", this.A08);
            A00.putExtra("extra_payee_name", this.A04);
            str = "extra_transfer_direction";
            i = 0;
        } else if (view.getId() == R.id.request_payment_container) {
            C30931Zj r22 = this.A0B;
            StringBuilder A0k2 = C12960it.A0k("request payment from vpa: ");
            A0k2.append(this.A03);
            C117295Zj.A1F(r22, A0k2);
            A00 = this.A06.A00(this, false, true);
            A00.putExtra("extra_payment_handle", this.A03);
            A00.putExtra("extra_payment_handle_id", this.A08);
            A00.putExtra("extra_payee_name", this.A04);
            str = "extra_transfer_direction";
            i = 1;
        } else if (view.getId() == R.id.block_vpa_btn) {
            boolean z = this.A0A;
            C30931Zj r23 = this.A0B;
            if (z) {
                StringBuilder A0k3 = C12960it.A0k("unblock vpa: ");
                A0k3.append(this.A03);
                C117295Zj.A1F(r23, A0k3);
                this.A05.Afe(this, new AnonymousClass68Y(this, false), this.A07, (String) C117295Zj.A0R(this.A03), false);
                return;
            }
            StringBuilder A0k4 = C12960it.A0k("block vpa: ");
            A0k4.append(this.A03);
            C117295Zj.A1F(r23, A0k4);
            C36021jC.A01(this, 1);
            return;
        } else {
            return;
        }
        A00.putExtra(str, i);
        startActivity(A00);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_vpa_contact);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0A(R.string.upi_id_info);
        }
        this.A03 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payment_handle");
        this.A08 = getIntent().getStringExtra("extra_payment_handle_id");
        this.A04 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payee_name");
        this.A00 = findViewById(R.id.payment_separator);
        findViewById(R.id.send_payment_container).setOnClickListener(this);
        findViewById(R.id.request_payment_container).setOnClickListener(this);
        this.A01 = (LinearLayout) findViewById(R.id.send_and_request_payment_container);
        CopyableTextView copyableTextView = (CopyableTextView) findViewById(R.id.account_id_handle);
        copyableTextView.setText(C12960it.A0X(this, C117295Zj.A0R(this.A03), new Object[1], 0, R.string.vpa_prefix));
        copyableTextView.A02 = (String) C117295Zj.A0R(this.A03);
        C117315Zl.A0N(C12970iu.A0M(this, R.id.vpa_name), C117295Zj.A0R(this.A04));
        this.A02.A05(C117305Zk.A06(this, R.id.avatar), R.drawable.avatar_contact);
        View findViewById = findViewById(R.id.block_vpa_btn);
        findViewById.setVisibility(0);
        findViewById.setOnClickListener(this);
        A2T(this.A05.AJE(this.A03));
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 1) {
            return super.onCreateDialog(i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0A(C12960it.A0X(this, C117295Zj.A0R(this.A04), new Object[1], 0, R.string.block_upi_id_confirmation));
        C117295Zj.A0q(A0S, this, 66, R.string.block);
        C72463ee.A0S(A0S);
        return A0S.create();
    }
}
