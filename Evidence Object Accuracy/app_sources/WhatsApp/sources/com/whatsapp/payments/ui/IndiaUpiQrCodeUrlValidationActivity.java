package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass61M;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118135bK;
import X.C118435bo;
import X.C128255vo;
import X.C12960it;
import X.C12980iv;
import X.C129925yW;
import X.C1309360o;
import X.C25911Bh;
import X.C36021jC;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.util.Linkify;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiQrCodeUrlValidationActivity extends AbstractActivityC121665jA {
    public AnonymousClass018 A00;
    public C129925yW A01;
    public AnonymousClass61M A02;
    public C128255vo A03;
    public C1309360o A04;
    public C118135bK A05;
    public C25911Bh A06;
    public String A07;
    public boolean A08;

    public IndiaUpiQrCodeUrlValidationActivity() {
        this(0);
    }

    public IndiaUpiQrCodeUrlValidationActivity(int i) {
        this.A08 = false;
        C117295Zj.A0p(this, 70);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A04 = (C1309360o) A1M.A9h.get();
            this.A00 = C12960it.A0R(A1M);
            this.A06 = (C25911Bh) A1M.A77.get();
            this.A01 = (C129925yW) A1M.AEW.get();
            this.A03 = (C128255vo) A1M.A9S.get();
            this.A02 = C117305Zk.A0N(A1M);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 100) {
            C36021jC.A01(this, 25);
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A05 = (C118135bK) C117315Zl.A06(new C118435bo(this, getIntent().getStringExtra("ARG_URL"), getIntent().getStringExtra("external_payment_source")), this).A00(C118135bK.class);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        int i2;
        int i3;
        switch (i) {
            case 21:
                r3 = C12980iv.A0S(this);
                r3.A0A(C12960it.A0X(this, getString(R.string.india_upi_payment_id_name), new Object[1], 0, R.string.payment_id_cannot_verify_error_text_default));
                i2 = R.string.ok;
                i3 = 52;
                C117295Zj.A0q(r3, this, i3, i2);
                r3.A0B(false);
                break;
            case 22:
                r3 = C12980iv.A0S(this);
                r3.A0A(C12960it.A0X(this, getString(R.string.india_upi_payment_id_name), new Object[1], 0, R.string.unblock_payment_id_error_default));
                i2 = R.string.ok;
                i3 = 58;
                C117295Zj.A0q(r3, this, i3, i2);
                r3.A0B(false);
                break;
            case 23:
            default:
                return super.onCreateDialog(i);
            case 24:
                r3 = C12980iv.A0S(this);
                r3.A07(R.string.payments_qr_dialog_unsafe_code_warning_title);
                r3.A06(R.string.payments_qr_dialog_unsafe_code_warning);
                C117295Zj.A0q(r3, this, 55, R.string.payments_qr_dialog_unsafe_code_cta_continue);
                C117305Zk.A18(r3, this, 56, R.string.cancel);
                r3.A0B(true);
                break;
            case 25:
                Uri parse = Uri.parse(this.A05.A04().A08);
                String string = getString(R.string.upi_invoice_link_dialog_title);
                SpannableString spannableString = new SpannableString(C25911Bh.A00(parse.toString()));
                Linkify.addLinks(spannableString, 1);
                r3 = new C004802e(this, R.style.AlertDialogExternalLink);
                r3.setTitle(string);
                r3.A0A(spannableString);
                C117305Zk.A18(r3, this, 54, R.string.payments_send_money);
                C117295Zj.A0q(r3, this, 53, R.string.upi_invoice_link_dialog_cta);
                r3.A0B(true);
                r3.A04(new IDxDListenerShape14S0100000_3_I1(this, 20));
                break;
            case 26:
                r3 = C12980iv.A0S(this);
                r3.A0A(C12960it.A0X(this, this.A07, new Object[1], 0, R.string.payments_qr_dialog_payment_from_non_verified_merchant_exceeded_limit));
                i2 = R.string.ok;
                i3 = 57;
                C117295Zj.A0q(r3, this, i3, i2);
                r3.A0B(false);
                break;
        }
        return r3.create();
    }
}
