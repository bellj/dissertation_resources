package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1FK;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12980iv;
import X.C129965ya;
import X.C14900mE;
import X.C15650ng;
import X.C17070qD;
import X.C18590sh;
import X.C18600si;
import X.C18610sj;
import X.C18620sk;
import X.C18650sn;
import X.C30931Zj;
import X.C452120p;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class PaymentDeleteAccountActivity extends ActivityC13790kL implements AnonymousClass1FK {
    public int A00;
    public C15650ng A01;
    public C18650sn A02;
    public C18600si A03;
    public C18610sj A04;
    public C18620sk A05;
    public C17070qD A06;
    public C18590sh A07;
    public boolean A08;
    public final C30931Zj A09;

    public PaymentDeleteAccountActivity() {
        this(0);
        this.A09 = C117305Zk.A0V("PaymentDeleteAccountActivity", "payment-settings");
    }

    public PaymentDeleteAccountActivity(int i) {
        this.A08 = false;
        C117295Zj.A0p(this, 101);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A07 = C117315Zl.A0F(A1M);
            this.A06 = C117305Zk.A0P(A1M);
            this.A01 = (C15650ng) A1M.A4m.get();
            this.A03 = C117315Zl.A0A(A1M);
            this.A04 = C117305Zk.A0M(A1M);
            this.A05 = (C18620sk) A1M.AFB.get();
            this.A02 = (C18650sn) A1M.AEe.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        C117305Zk.A0x(this);
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r2) {
        Ado(R.string.payment_account_not_unlinked);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        int ACm = this.A06.A02().ABq().ACm(null, r4.A00);
        if (ACm == 0) {
            ACm = R.string.payment_account_not_unlinked;
        }
        Ado(ACm);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0049  */
    @Override // X.AnonymousClass1FK
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AVB(X.C452220q r5) {
        /*
            r4 = this;
            X.1Zj r2 = r4.A09
            java.lang.String r0 = "onDeleteAccount successful: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            boolean r0 = r5.A02
            r1.append(r0)
            java.lang.String r0 = " remove type: "
            r1.append(r0)
            int r0 = r4.A00
            r1.append(r0)
            X.C117295Zj.A1F(r2, r1)
            r0 = 2131365345(0x7f0a0de1, float:1.8350553E38)
            android.view.View r0 = r4.findViewById(r0)
            r3 = 8
            r0.setVisibility(r3)
            boolean r0 = r5.A02
            r2 = 1
            if (r0 == 0) goto L_0x006a
            int r0 = r4.A00
            if (r0 != r2) goto L_0x0045
            r1 = 2131890212(0x7f121024, float:1.941511E38)
        L_0x0032:
            r0 = 2131366528(0x7f0a1280, float:1.8352952E38)
            android.widget.TextView r0 = X.C12970iu.A0M(r4, r0)
            r0.setText(r1)
            r0 = 2131366527(0x7f0a127f, float:1.835295E38)
            X.C12970iu.A1N(r4, r0, r3)
            r4.Ado(r1)
        L_0x0045:
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x004e
            X.0qD r0 = r4.A06
            r0.A05(r2, r2)
        L_0x004e:
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x0069
            int r1 = r4.A00
            r0 = 2
            if (r1 != r0) goto L_0x0069
            android.content.Intent r2 = X.C12970iu.A0A()
            int r1 = r4.A00
            java.lang.String r0 = "extra_remove_payment_account"
            r2.putExtra(r0, r1)
            r0 = -1
            r4.setResult(r0, r2)
            r4.finish()
        L_0x0069:
            return
        L_0x006a:
            r1 = 2131890211(0x7f121023, float:1.9415107E38)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.PaymentDeleteAccountActivity.AVB(X.20q):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        View findViewById = findViewById(R.id.hero_payments);
        int i = 0;
        if (configuration.orientation == 2) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.payment_unlink_payment_accounts);
        AbstractC005102i A1U = A1U();
        int i = 1;
        if (A1U != null) {
            A1U.A0A(R.string.payments_unlink_payment_accounts);
            A1U.A0M(true);
        }
        if (getIntent() != null) {
            i = getIntent().getIntExtra("extra_remove_payment_account", 1);
        }
        this.A00 = i;
        C14900mE r4 = ((ActivityC13810kN) this).A05;
        AbstractC14440lR r13 = ((ActivityC13830kP) this).A05;
        C18590sh r12 = this.A07;
        C17070qD r11 = this.A06;
        new C129965ya(this, r4, ((ActivityC13810kN) this).A07, this.A01, this.A02, this.A03, this.A04, this.A05, r11, r12, r13).A00(this);
        this.A09.A06("deleted payments store and sending delete account request");
        onConfigurationChanged(C12980iv.A0H(this));
    }
}
