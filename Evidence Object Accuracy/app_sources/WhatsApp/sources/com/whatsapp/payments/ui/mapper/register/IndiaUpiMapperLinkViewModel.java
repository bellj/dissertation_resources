package com.whatsapp.payments.ui.mapper.register;

import X.AnonymousClass014;
import X.AnonymousClass18O;
import X.AnonymousClass1ZR;
import X.AnonymousClass2SM;
import X.AnonymousClass3CQ;
import X.C120535gL;
import X.C13000ix;
import X.C1329668y;
import X.C15570nT;
import X.C16700pc;
import X.C27691It;
import android.app.Application;
import com.whatsapp.Me;

/* loaded from: classes2.dex */
public final class IndiaUpiMapperLinkViewModel extends AnonymousClass014 {
    public C15570nT A00;
    public C1329668y A01;
    public final Application A02;
    public final C120535gL A03;
    public final AnonymousClass18O A04;
    public final C27691It A05 = C13000ix.A03();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public IndiaUpiMapperLinkViewModel(Application application, C15570nT r3, C1329668y r4, C120535gL r5, AnonymousClass18O r6) {
        super(application);
        C16700pc.A0H(r4, r3);
        C16700pc.A0E(r6, 5);
        this.A02 = application;
        this.A01 = r4;
        this.A00 = r3;
        this.A03 = r5;
        this.A04 = r6;
    }

    public final void A04(boolean z) {
        String str;
        String str2;
        C120535gL r4 = this.A03;
        C1329668y r0 = this.A01;
        String A0B = r0.A0B();
        if (A0B == null) {
            A0B = "";
        }
        AnonymousClass1ZR A04 = r0.A04();
        AnonymousClass2SM r3 = new AnonymousClass2SM();
        C15570nT r02 = this.A00;
        r02.A08();
        Me me = r02.A00;
        if (me == null) {
            str = null;
        } else {
            str = me.number;
        }
        AnonymousClass1ZR r6 = new AnonymousClass1ZR(r3, String.class, str, "upiAlias");
        if (z) {
            str2 = "port";
        } else {
            str2 = "add";
        }
        r4.A01(A04, r6, new AnonymousClass3CQ(this), A0B, str2);
    }
}
