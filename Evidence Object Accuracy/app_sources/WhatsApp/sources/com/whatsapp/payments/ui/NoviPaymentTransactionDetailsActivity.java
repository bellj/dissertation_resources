package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractC121025h8;
import X.AbstractC130195yx;
import X.AbstractC1316063k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass1IR;
import X.AnonymousClass1IS;
import X.AnonymousClass2FL;
import X.AnonymousClass5m2;
import X.AnonymousClass608;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61F;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119825fA;
import X.C122365lL;
import X.C122515la;
import X.C122525lb;
import X.C122535lc;
import X.C122545ld;
import X.C122555le;
import X.C122645ln;
import X.C122655lo;
import X.C122665lp;
import X.C122705lt;
import X.C122715lu;
import X.C122725lv;
import X.C122765lz;
import X.C122775m5;
import X.C122785m6;
import X.C123565nM;
import X.C123755no;
import X.C123765np;
import X.C125655rb;
import X.C126025sD;
import X.C126995tm;
import X.C128315vu;
import X.C128365vz;
import X.C128375w0;
import X.C12960it;
import X.C12970iu;
import X.C129865yQ;
import X.C12990iw;
import X.C130295zB;
import X.C1310561a;
import X.C14900mE;
import X.C18790t3;
import X.C21270x9;
import X.C244415n;
import X.C38211ni;
import X.C452120p;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviPaymentTransactionDetailsActivity extends PaymentTransactionDetailsListActivity {
    public C244415n A00;
    public AnonymousClass61M A01;
    public C129865yQ A02;
    public C125655rb A03;
    public AnonymousClass60Y A04;
    public AnonymousClass61F A05;
    public C123565nM A06;
    public C128375w0 A07;
    public String A08;
    public String A09;
    public boolean A0A;

    public NoviPaymentTransactionDetailsActivity() {
        this(0);
    }

    public NoviPaymentTransactionDetailsActivity(int i) {
        this.A0A = false;
        C117295Zj.A0p(this, 91);
    }

    @Override // X.AbstractActivityC121795k0, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A09(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            AbstractActivityC119265dR.A0A(A1M, this);
            ((PaymentTransactionDetailsListActivity) this).A0I = AbstractActivityC119265dR.A02(A09, A1M, this, A1M.AEw);
            this.A00 = (C244415n) A1M.AAg.get();
            this.A03 = (C125655rb) A1M.ADF.get();
            this.A04 = C117305Zk.A0W(A1M);
            this.A05 = C117305Zk.A0X(A1M);
            this.A01 = C117305Zk.A0N(A1M);
            this.A07 = C117315Zl.A0E(A1M);
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i != 203) {
            switch (i) {
                case 1000:
                    return new C122365lL(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_icon_text_row_item));
                case 1001:
                    return new C122545ld(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_view_link_row));
                case 1002:
                    return new C122535lc(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_solo_description_row));
                case 1003:
                    return new C122525lb(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_help_view));
                case 1004:
                    return new C122705lt(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_amount_view));
                case 1005:
                    return new C122665lp(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_key_name_value_inline_row));
                case 1006:
                    return new C122515la(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_breakdown_section_header_row));
                case 1007:
                    return new C122775m5(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_divider));
                case 1008:
                    return new C122715lu(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_icon_title_subtitle_row));
                case 1009:
                    return new C122555le(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_exchange_rate_row));
                case 1010:
                    return new C122725lv(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_withdraw_code_row));
                case 1011:
                    C14900mE r3 = ((ActivityC13810kN) this).A05;
                    C18790t3 r4 = ((PaymentTransactionDetailsListActivity) this).A03;
                    C244415n r6 = this.A00;
                    return new C122765lz(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_location_details_row), r3, r4, ((ActivityC13810kN) this).A0D, r6);
                case 1012:
                    return new C122645ln(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_icon_title_two_subtitles_row));
                case 1013:
                    return new C122655lo(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_instructions_row));
                case 1014:
                    return new C122785m6(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_details_with_asset_row));
                default:
                    return super.A2e(viewGroup, i);
            }
        } else {
            C21270x9 r32 = ((PaymentTransactionDetailsListActivity) this).A09;
            return new AnonymousClass5m2(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_payment_transaction_details_icon_title_subtitle_progressbar_row), ((PaymentTransactionDetailsListActivity) this).A06, r32);
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity
    public void A2f(C128315vu r15) {
        StringBuilder sb;
        String str;
        C119825fA r0;
        boolean z;
        String str2;
        String str3;
        C126995tm r10;
        C126995tm r9;
        int i = r15.A00;
        if (i != 10) {
            if (i != 14) {
                if (i != 15) {
                    switch (i) {
                        case 501:
                            AnonymousClass1IR r5 = r15.A05;
                            AnonymousClass009.A05(r5);
                            Intent A0D = C12990iw.A0D(this, getClass());
                            A0D.putExtra("extra_transaction_id", r5.A0K);
                            A0D.putExtra("extra_transaction_detail_data", r5);
                            if (r5.A0D != null) {
                                C38211ni.A00(A0D, new AnonymousClass1IS(r5.A0C, r5.A0L, r5.A0Q));
                            }
                            startActivity(A0D);
                            return;
                        case 502:
                            this.A06.A0V(this);
                            return;
                        case 503:
                            str2 = r15.A0B;
                            if (str2 == null) {
                                str2 = "";
                            }
                            z = false;
                            r9 = C126995tm.A00(new Runnable() { // from class: X.6Gk
                                @Override // java.lang.Runnable
                                public final void run() {
                                    NoviPaymentTransactionDetailsActivity noviPaymentTransactionDetailsActivity = NoviPaymentTransactionDetailsActivity.this;
                                    noviPaymentTransactionDetailsActivity.A06.A0V(noviPaymentTransactionDetailsActivity);
                                }
                            }, R.string.wallpaper_thumbnails_reload);
                            r10 = C126995tm.A00(null, R.string.close);
                            str3 = null;
                            break;
                        case 504:
                            str3 = getString(R.string.novi_payment_transaction_details_cannot_cancel_transaction_dialog_title);
                            str2 = getString(R.string.novi_payment_transaction_details_cannot_cancel_transaction_dialog_description);
                            z = false;
                            r9 = C126995tm.A00(new Runnable() { // from class: X.6Gl
                                @Override // java.lang.Runnable
                                public final void run() {
                                    NoviPaymentTransactionDetailsActivity.this.A06.A0U();
                                }
                            }, R.string.novi_get_help);
                            r10 = C126995tm.A00(null, R.string.ok);
                            break;
                        case 505:
                            C1310561a.A06(this, new C126025sD("loginScreen"));
                            break;
                        case 506:
                            C452120p A0L = C117305Zk.A0L();
                            A0L.A08 = r15.A0F;
                            A0L.A07 = r15.A0B;
                            this.A02.A02(A0L, new Runnable() { // from class: X.6Gj
                                @Override // java.lang.Runnable
                                public final void run() {
                                    NoviPaymentTransactionDetailsActivity.this.finish();
                                }
                            }, null);
                            break;
                        case 507:
                            ((ActivityC13790kL) this).A00.A06(this, new Intent("android.intent.action.VIEW", new AnonymousClass608(((PaymentTransactionDetailsListActivity) this).A0C, "594558031688041").A01()));
                            break;
                    }
                } else {
                    AnonymousClass1IR r4 = r15.A05;
                    AnonymousClass009.A05(r4);
                    Intent A0D2 = C12990iw.A0D(this, NoviPayBloksActivity.class);
                    A0D2.putExtra("screen_name", "novipay_p_report_transaction");
                    HashMap A11 = C12970iu.A11();
                    A11.put("claim_edu_origin", "transaction_detail");
                    A11.put("novi_claims_transaction_id", r4.A0K);
                    C117305Zk.A12(A0D2, "logging_disabled", Boolean.toString(!this.A05.A0H()), A11);
                    startActivity(A0D2);
                }
                super.A2f(r15);
                return;
            }
            str3 = getString(R.string.novi_payment_transaction_details_cancel_transaction_dialog_title);
            C123565nM r02 = this.A06;
            AnonymousClass1IR r2 = r15.A05;
            AbstractC130195yx A00 = r02.A0D.A00(r2.A03);
            A00.A06(r2);
            if (A00 instanceof C123765np) {
                str2 = ((C123765np) A00).A02.getString(R.string.novi_payment_transaction_details_cancel_transaction_dialog_description_withdrawal);
            } else if (!(A00 instanceof C123755no)) {
                str2 = null;
            } else {
                C123755no r1 = (C123755no) A00;
                str2 = C12960it.A0X(r1.A03, r1.A02, C12970iu.A1b(), 0, R.string.novi_payment_transaction_details_cancel_transaction_dialog_description);
            }
            z = false;
            r9 = C126995tm.A00(new Runnable(r15) { // from class: X.6IM
                public final /* synthetic */ C128315vu A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    NoviPaymentTransactionDetailsActivity noviPaymentTransactionDetailsActivity = NoviPaymentTransactionDetailsActivity.this;
                    C128315vu r42 = this.A01;
                    AnonymousClass610 A03 = AnonymousClass610.A03("CANCEL_TRANSACTION_MODAL_CLICK", "PAYMENT_HISTORY", "CANCEL_TRANSACTION_MODAL");
                    String str4 = r42.A05.A0K;
                    C128365vz r22 = A03.A00;
                    r22.A0m = str4;
                    r22.A0L = noviPaymentTransactionDetailsActivity.getString(R.string.novi_payment_transaction_details_button_title_cancel_transaction);
                    AnonymousClass1IR r03 = r42.A05;
                    r22.A0Q = C31001Zq.A05(r03.A03, r03.A02);
                    noviPaymentTransactionDetailsActivity.A2h(r22);
                    C123565nM r6 = noviPaymentTransactionDetailsActivity.A06;
                    String str5 = r42.A05.A0K;
                    r6.A0O(true);
                    C130095yn r3 = r6.A0B;
                    AnonymousClass016 A0T = C12980iv.A0T();
                    r3.A0A.Ab2(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004b: INVOKE  
                          (wrap: X.0lR : 0x0044: IGET  (r1v3 X.0lR A[REMOVE]) = (r3v1 'r3' X.5yn) X.5yn.A0A X.0lR)
                          (wrap: X.6JP : 0x0048: CONSTRUCTOR  (r0v10 X.6JP A[REMOVE]) = (r2v2 'A0T' X.016), (r3v1 'r3' X.5yn), (r5v0 'str5' java.lang.String) call: X.6JP.<init>(X.016, X.5yn, java.lang.String):void type: CONSTRUCTOR)
                         type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6IM.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0048: CONSTRUCTOR  (r0v10 X.6JP A[REMOVE]) = (r2v2 'A0T' X.016), (r3v1 'r3' X.5yn), (r5v0 'str5' java.lang.String) call: X.6JP.<init>(X.016, X.5yn, java.lang.String):void type: CONSTRUCTOR in method: X.6IM.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6JP, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity r3 = com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity.this
                        X.5vu r4 = r7.A01
                        java.lang.String r2 = "CANCEL_TRANSACTION_MODAL_CLICK"
                        java.lang.String r1 = "PAYMENT_HISTORY"
                        java.lang.String r0 = "CANCEL_TRANSACTION_MODAL"
                        X.610 r1 = X.AnonymousClass610.A03(r2, r1, r0)
                        X.1IR r0 = r4.A05
                        java.lang.String r0 = r0.A0K
                        X.5vz r2 = r1.A00
                        r2.A0m = r0
                        r0 = 2131889851(0x7f120ebb, float:1.9414377E38)
                        java.lang.String r0 = r3.getString(r0)
                        r2.A0L = r0
                        X.1IR r0 = r4.A05
                        int r1 = r0.A03
                        int r0 = r0.A02
                        java.lang.String r0 = X.C31001Zq.A05(r1, r0)
                        r2.A0Q = r0
                        r3.A2h(r2)
                        X.5nM r6 = r3.A06
                        X.1IR r0 = r4.A05
                        java.lang.String r5 = r0.A0K
                        android.app.Activity r4 = X.AbstractC35731ia.A00(r3)
                        X.00n r4 = (X.AbstractC001200n) r4
                        r0 = 1
                        r6.A0O(r0)
                        X.5yn r3 = r6.A0B
                        X.016 r2 = X.C12980iv.A0T()
                        X.0lR r1 = r3.A0A
                        X.6JP r0 = new X.6JP
                        r0.<init>(r2, r3, r5)
                        r1.Ab2(r0)
                        r0 = 147(0x93, float:2.06E-43)
                        X.C117295Zj.A0s(r4, r2, r6, r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6IM.run():void");
                }
            }, R.string.novi_payment_transaction_details_button_title_cancel_transaction);
            r10 = C126995tm.A00(new Runnable(r15) { // from class: X.6IN
                public final /* synthetic */ C128315vu A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    NoviPaymentTransactionDetailsActivity noviPaymentTransactionDetailsActivity = NoviPaymentTransactionDetailsActivity.this;
                    C128315vu r3 = this.A01;
                    AnonymousClass610 A03 = AnonymousClass610.A03("CANCEL_TRANSACTION_MODAL_VPV", "PAYMENT_HISTORY", "CANCEL_TRANSACTION_MODAL");
                    AnonymousClass1IR r32 = r3.A05;
                    String str4 = r32.A0K;
                    C128365vz r22 = A03.A00;
                    r22.A0m = str4;
                    r22.A0Q = C31001Zq.A05(r32.A03, r32.A02);
                    noviPaymentTransactionDetailsActivity.A2h(r22);
                }
            }, R.string.close);
            C130295zB.A00(this, r9, r10, str3, str2, z).show();
            super.A2f(r15);
            return;
        }
        AnonymousClass608 r22 = new AnonymousClass608(((PaymentTransactionDetailsListActivity) this).A0C);
        AnonymousClass1IR r52 = r15.A05;
        int i2 = r52.A03;
        if (i2 == 1) {
            int i3 = r52.A02;
            if (i3 != 405) {
                sb = r22.A00;
                if (i3 != 406) {
                    switch (i3) {
                        case 419:
                        case 420:
                            str = "TRANSACTION_SEND_PENDING_OR_IN_REVIEW";
                            break;
                        case 421:
                            str = "TRANSACTION_SEND_CANCELED";
                            break;
                        default:
                            sb.append("WA");
                            break;
                    }
                } else {
                    str = "TRANSACTION_SEND_FAILED";
                }
            } else {
                sb = r22.A00;
                str = "TRANSACTION_CARD";
            }
            sb.append(str);
        } else if (i2 != 2) {
            if (i2 == 6) {
                sb = r22.A00;
                str = "DEPOSIT_METHOD";
            } else if (i2 != 7) {
                if (i2 == 8 && (r0 = (C119825fA) r52.A0A) != null) {
                    AbstractC1316063k r12 = r0.A01;
                    if (r12 instanceof AbstractC121025h8) {
                        int i4 = ((AbstractC121025h8) r12).A02;
                        sb = r22.A00;
                        if (i4 == 1) {
                            str = "WITHDRAW_CASH_INSTRUCTIONS";
                        } else {
                            if (i4 == 2) {
                                str = "WITHDRAW_TO_BANK_RECEIPT";
                            }
                            sb.append("WA");
                        }
                    }
                }
                sb = r22.A00;
                sb.append("WA");
            } else {
                int i5 = r52.A02;
                if (i5 != 408) {
                    if (i5 == 409 || i5 == 411) {
                        sb = r22.A00;
                        str = "TRANSACTION_CLAIM_DECLINE";
                    }
                    sb = r22.A00;
                    sb.append("WA");
                } else {
                    sb = r22.A00;
                    str = "TRANSACTION_CLAIM_APPROVE";
                }
            }
            sb.append(str);
        } else {
            int i6 = r52.A02;
            if (i6 == 103) {
                sb = r22.A00;
                str = "TRANSACTION_RECEIVE_PENDING";
            } else if (i6 == 112 || i6 == 105) {
                sb = r22.A00;
                str = "TRANSACTION_RECEIVE_FAILED_OR_CANCELED";
            } else {
                sb = r22.A00;
                if (i6 == 106) {
                    str = "TRANSACTION_RECEIVE_COMPLETE";
                }
                sb.append("WA");
            }
            sb.append(str);
        }
        ((ActivityC13790kL) this).A00.A06(this, new Intent("android.intent.action.VIEW", r22.A01()));
    }

    public final void A2h(C128365vz r3) {
        if ("NOVI_HUB_HOMEPAGE".equals(this.A08)) {
            this.A04.A05(r3);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        A2h(new AnonymousClass610("BACK_CLICK", "PAYMENT_HISTORY", "REVIEW_TRANSACTION", "ARROW").A00);
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        this.A02 = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A01);
        if (getIntent() != null) {
            this.A08 = getIntent().getStringExtra("extra_origin_screen");
            this.A09 = getIntent().getStringExtra("referral_screen");
        }
        boolean equals = "chat".equals(this.A09);
        AnonymousClass60Y r1 = this.A04;
        if (equals) {
            str = "CHAT_BUBBLE";
        } else {
            str = "PAYMENTS";
        }
        r1.A04 = str;
        C123565nM r3 = this.A06;
        r3.A03 = this.A08;
        AnonymousClass61F r2 = r3.A0A;
        C117295Zj.A0s(this, r2.A0G, r3, 149);
        C117295Zj.A0s(this, r2.A03(), r3, 148);
        C117295Zj.A0r(this, this.A03.A00, 98);
        A2h(AnonymousClass610.A02("NAVIGATION_START", "PAYMENT_HISTORY").A00);
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        A2h(AnonymousClass610.A02("NAVIGATION_END", "PAYMENT_HISTORY").A00);
    }
}
