package com.whatsapp.payments.ui.widget;

import X.AbstractActivityC121685jC;
import X.AbstractC001200n;
import X.AbstractC116245Ur;
import X.AbstractC1310861e;
import X.AbstractC1310961f;
import X.AbstractC136126Lc;
import X.AbstractC136516Mv;
import X.AbstractC14020ki;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC253919f;
import X.AbstractC30781Yu;
import X.AbstractC30791Yv;
import X.AbstractC467627l;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass04D;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass0L1;
import X.AnonymousClass130;
import X.AnonymousClass146;
import X.AnonymousClass15H;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass1A4;
import X.AnonymousClass1AB;
import X.AnonymousClass1AD;
import X.AnonymousClass1BP;
import X.AnonymousClass1BQ;
import X.AnonymousClass1IR;
import X.AnonymousClass1J1;
import X.AnonymousClass1KS;
import X.AnonymousClass1KT;
import X.AnonymousClass1Ly;
import X.AnonymousClass1US;
import X.AnonymousClass2GE;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass3FN;
import X.AnonymousClass5Wu;
import X.AnonymousClass63Y;
import X.AnonymousClass6D0;
import X.AnonymousClass6HY;
import X.AnonymousClass6M9;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118145bL;
import X.C119155d0;
import X.C119875fF;
import X.C123825o0;
import X.C123945oC;
import X.C125905s0;
import X.C127145u1;
import X.C127165u3;
import X.C127505ub;
import X.C128295vs;
import X.C128305vt;
import X.C12960it;
import X.C12970iu;
import X.C129745yE;
import X.C12980iv;
import X.C12990iw;
import X.C1322366c;
import X.C1322466d;
import X.C134146Dm;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15260mp;
import X.C15270mq;
import X.C15320mw;
import X.C15330mx;
import X.C16120oU;
import X.C16630pM;
import X.C18600si;
import X.C20370ve;
import X.C21270x9;
import X.C22210yi;
import X.C22540zF;
import X.C22710zW;
import X.C231510o;
import X.C235512c;
import X.C252718t;
import X.C253719d;
import X.C255519v;
import X.C255619w;
import X.C30821Yy;
import X.C30921Zi;
import X.C69893aP;
import X.C88094Eg;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import androidx.constraintlayout.widget.Group;
import com.facebook.redex.IDxCListenerShape5S0000000_3_I1;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.components.FloatingActionButton;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.gifsearch.GifSearchContainer;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public class PaymentView extends KeyboardPopupLayout implements View.OnClickListener, AbstractC116245Ur, AbstractC467627l {
    public int A00;
    public int A01;
    public int A02;
    public AutoTransition A03;
    public View A04;
    public Animation A05;
    public FrameLayout A06;
    public ImageView A07;
    public ImageView A08;
    public ImageView A09;
    public LinearLayout A0A;
    public LinearLayout A0B;
    public LinearLayout A0C;
    public LinearLayout A0D;
    public LinearLayout A0E;
    public LinearLayout A0F;
    public TextSwitcher A0G;
    public TextSwitcher A0H;
    public TextView A0I;
    public TextView A0J;
    public TextView A0K;
    public TextView A0L;
    public TextView A0M;
    public Group A0N;
    public ShimmerFrameLayout A0O;
    public ShimmerFrameLayout A0P;
    public TabLayout A0Q;
    public AbstractC15710nm A0R;
    public C14900mE A0S;
    public KeyboardPopupLayout A0T;
    public FloatingActionButton A0U;
    public ThumbnailButton A0V;
    public ThumbnailButton A0W;
    public AnonymousClass130 A0X;
    public AnonymousClass1J1 A0Y;
    public C21270x9 A0Z;
    public AnonymousClass01d A0a;
    public C14820m6 A0b;
    public AnonymousClass018 A0c;
    public AnonymousClass1AD A0d;
    public C20370ve A0e;
    public AbstractC30791Yv A0f;
    public AnonymousClass19M A0g;
    public C231510o A0h;
    public AnonymousClass193 A0i;
    public C14850m9 A0j;
    public C16120oU A0k;
    public AnonymousClass1BP A0l;
    public C253719d A0m;
    public AbstractC253919f A0n;
    public AbstractC14640lm A0o;
    public C18600si A0p;
    public C22710zW A0q;
    public C22540zF A0r;
    public AnonymousClass6M9 A0s;
    public PaymentAmountInputField A0t;
    public C134146Dm A0u;
    public AbstractC1310961f A0v;
    public AbstractC136516Mv A0w;
    public C127145u1 A0x;
    public AbstractC136126Lc A0y;
    public C129745yE A0z;
    public C16630pM A10;
    public C22210yi A11;
    public AnonymousClass1KS A12;
    public AnonymousClass15H A13;
    public AnonymousClass1AB A14;
    public AnonymousClass146 A15;
    public C235512c A16;
    public C255519v A17;
    public AnonymousClass1BQ A18;
    public C255619w A19;
    public AnonymousClass1Ly A1A;
    public AnonymousClass1A4 A1B;
    public AnonymousClass1KT A1C;
    public AbstractC14440lR A1D;
    public Integer A1E;
    public String A1F;
    public String A1G;
    public String A1H;
    public String A1I;
    public String A1J;
    public String A1K;
    public List A1L;
    public boolean A1M;
    public boolean A1N;
    public boolean A1O;
    public final Runnable A1P;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public PaymentView(Context context) {
        super(context);
        A01();
        this.A01 = 0;
        this.A1P = new AnonymousClass6HY(this);
        A07();
    }

    public PaymentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
        this.A01 = 0;
        this.A1P = new AnonymousClass6HY(this);
        A07();
    }

    public PaymentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        this.A01 = 0;
        this.A1P = new AnonymousClass6HY(this);
        A07();
    }

    public PaymentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
        this.A01 = 0;
        this.A1P = new AnonymousClass6HY(this);
        A07();
    }

    public PaymentView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A01();
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x03a5  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x03f5  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x040a  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x04e4  */
    /* JADX WARNING: Removed duplicated region for block: B:163:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0244  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x02d0  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x034a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x035e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.AnonymousClass074 r34, com.whatsapp.payments.ui.widget.PaymentView r35) {
        /*
        // Method dump skipped, instructions count: 1283
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.PaymentView.A00(X.074, com.whatsapp.payments.ui.widget.PaymentView):void");
    }

    @Override // X.AbstractC49832My
    public void A01() {
        if (!this.A1M) {
            this.A1M = true;
            AnonymousClass2P6 r2 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r1 = r2.A06;
            super.A05 = (C252718t) r1.A9K.get();
            this.A0j = C12960it.A0S(r1);
            this.A0m = (C253719d) r1.A8V.get();
            this.A0S = C12970iu.A0R(r1);
            this.A0R = (AbstractC15710nm) r1.A4o.get();
            this.A1D = (AbstractC14440lR) r1.ANe.get();
            this.A0k = (C16120oU) r1.ANE.get();
            this.A0g = (AnonymousClass19M) r1.A6R.get();
            this.A0h = (C231510o) r1.AHO.get();
            this.A0Z = C12970iu.A0W(r1);
            this.A0X = (AnonymousClass130) r1.A41.get();
            this.A11 = (C22210yi) r1.AGm.get();
            this.A0a = C12960it.A0Q(r1);
            this.A15 = (AnonymousClass146) r1.AKM.get();
            this.A0n = (AbstractC253919f) r1.AGb.get();
            this.A16 = (C235512c) r1.AKS.get();
            this.A0p = C117315Zl.A0A(r1);
            this.A0c = C12960it.A0R(r1);
            this.A0i = (AnonymousClass193) r1.A6S.get();
            this.A0b = (C14820m6) r1.AN3.get();
            this.A0q = C117305Zk.A0O(r1);
            this.A14 = (AnonymousClass1AB) r1.AKI.get();
            this.A0e = (C20370ve) r1.AEu.get();
            this.A13 = (AnonymousClass15H) r1.AKC.get();
            this.A19 = (C255619w) r1.AKW.get();
            this.A10 = (C16630pM) r1.AIc.get();
            this.A0r = (C22540zF) r1.AEC.get();
            this.A0l = r2.A03.A07();
            this.A1B = (AnonymousClass1A4) r1.AKX.get();
            this.A17 = (C255519v) r1.AKF.get();
            this.A0d = (AnonymousClass1AD) r1.A54.get();
        }
    }

    public final SpannableStringBuilder A02(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String string = this.A0w.AAa().getString(i);
        Object[] A1a = C12980iv.A1a();
        A1a[0] = string;
        A1a[1] = str;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(String.format("%s %s", A1a));
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(this.A0w.AAa().getResources().getColor(R.color.list_item_sub_title_v2));
        int length = string.length();
        int i2 = length + 1;
        spannableStringBuilder.setSpan(foregroundColorSpan, 0, i2, 0);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(this.A0w.AAa().getResources().getColor(R.color.list_item_title)), i2, length + str.length() + 1, 0);
        return spannableStringBuilder;
    }

    public void A03() {
        int i;
        String str;
        C127165u3 r5;
        String str2;
        AbstractC30791Yv r0;
        AbstractC30791Yv r1;
        Editable text = this.A0t.getText();
        AnonymousClass009.A05(text);
        String obj = text.toString();
        int i2 = 1;
        if (this.A00 != 1) {
            this.A0q.A06.A01();
            i2 = 0;
        }
        AnonymousClass1IR A0N = this.A0e.A0N(this.A1I, this.A1K);
        if (A0N == null || A0N.A02 != 18) {
            BigDecimal AAD = this.A0f.AAD(this.A0c, obj);
            AnonymousClass6D0 r10 = (AnonymousClass6D0) this.A0y;
            C125905s0 r02 = r10.A06;
            if (r02 != null) {
                C118145bL r12 = r02.A00.A0H;
                Object A01 = r12.A0F.A01();
                AnonymousClass009.A05(A01);
                C30821Yy r52 = (C30821Yy) A01;
                AnonymousClass63Y r13 = (AnonymousClass63Y) r12.A0E.A01();
                if (r13 != null) {
                    r0 = r13.A02;
                } else {
                    r0 = r12.A01;
                    AnonymousClass009.A05(r0);
                }
                C30821Yy AEV = r0.AEV();
                i = 0;
                if (AEV.A00.compareTo(r52.A00) > 0) {
                    Context context = r12.A12;
                    Object[] A1b = C12970iu.A1b();
                    if (r13 != null) {
                        r1 = r13.A02;
                    } else {
                        r1 = r12.A01;
                        AnonymousClass009.A05(r1);
                    }
                    r5 = new C127165u3(2, C12960it.A0X(context, r1.AAA(r12.A0K, AEV, 0), A1b, 0, R.string.payments_send_payment_min_amount));
                } else {
                    str = "";
                    r5 = new C127165u3(i, str);
                }
            } else if (AAD == null || r10.A05.A00.compareTo(AAD) > 0) {
                i = 2;
                str = C12960it.A0X(r10.A01, r10.A03.AAA(r10.A02, r10.A05, 0), C12970iu.A1b(), 0, R.string.payments_send_payment_min_amount);
                r5 = new C127165u3(i, str);
            } else {
                r5 = new C127165u3(0, "");
            }
            if (r5.A00 == 0) {
                r5 = r10.A00("", AAD, i2, false);
            }
            int i3 = r5.A00;
            if ((i3 == 2 || i3 == 3) && (str2 = r5.A01) != null) {
                this.A0t.A09();
                this.A0v.APq(str2);
                A0F(str2);
                this.A0z.A01(1);
                return;
            }
            this.A1G = obj;
            C134146Dm r03 = this.A0u;
            if (r03 != null) {
                this.A1H = r03.A09.getStringText();
                this.A1L = this.A0u.A09.getMentions();
            }
            AbstractC1310961f r14 = this.A0v;
            C30821Yy A0D = C117295Zj.A0D(this.A0f, AAD);
            if (i2 != 0) {
                r14.AV5(A0D, obj);
            } else {
                r14.AVp(A0D);
            }
        } else {
            this.A0v.AVs();
        }
    }

    public void A04() {
        if (this.A0N.getVisibility() == 0) {
            this.A09.setTag(R.id.selected_expressive_background_theme, null);
            this.A09.setImageResource(R.drawable.payment_default_background);
            AnonymousClass6M9 r0 = this.A0s;
            if (r0 != null) {
                A0D(((C128305vt) r0.AZb()).A04);
            }
        }
    }

    public void A05() {
        C134146Dm r2 = this.A0u;
        if (r2 != null) {
            r2.A06.setVisibility(8);
            r2.A0B = null;
            r2.A0D = null;
            r2.A09.setVisibility(0);
            r2.A05.setVisibility(0);
        }
    }

    public void A06() {
        int i;
        if (this.A00 == 1) {
            this.A0G.setVisibility(0);
            this.A0G.setText(this.A0w.AAa().getString(R.string.payments_send_payment_to));
            if (this.A1N) {
                this.A0H.setText(this.A1F);
                A0H(this.A1O);
            }
            if (this.A0w.AK3()) {
                this.A0I.setText(this.A0w.AFG());
                this.A0I.setVisibility(0);
                A09();
            } else {
                A08();
            }
            C134146Dm r1 = this.A0u;
            if (r1 != null) {
                r1.A0A.A00(2);
            }
            this.A0t.A03 = 1;
            i = 6;
        } else {
            boolean z = this.A1N;
            TextSwitcher textSwitcher = this.A0G;
            if (z) {
                textSwitcher.setVisibility(8);
                this.A0H.setText(A02(this.A1F, R.string.payments_send_payment_to));
                A08();
                this.A0I.setVisibility(8);
                A0H(this.A1O);
            } else {
                textSwitcher.setVisibility(0);
                this.A0G.setText(this.A0w.AAa().getString(R.string.payments_send_payment_to));
                this.A0I.setVisibility(8);
                A0G(true);
            }
            C134146Dm r0 = this.A0u;
            if (r0 != null) {
                r0.A0A.A00(1);
            }
            this.A0t.A03 = 0;
            i = this.A02;
        }
        FrameLayout frameLayout = this.A06;
        if (i != 0) {
            frameLayout.setVisibility(8);
        } else {
            frameLayout.setVisibility(0);
            C12960it.A0t(C117295Zj.A05(this.A0p), "payment_incentive_tooltip_viewed", true);
        }
        if (this.A0u != null) {
            boolean AK3 = this.A0w.AK3();
            C134146Dm r02 = this.A0u;
            if (AK3) {
                r02.A02.setVisibility(8);
                return;
            }
            r02.A02.setVisibility(0);
            if (this.A0x.A01) {
                MentionableEntry mentionableEntry = this.A0u.A09;
                mentionableEntry.addTextChangedListener(new C123825o0(this));
                this.A1C.A03();
                C129745yE r5 = this.A0z;
                C134146Dm r12 = this.A0u;
                ImageButton imageButton = r12.A04;
                GifSearchContainer gifSearchContainer = r12.A08;
                EmojiSearchContainer emojiSearchContainer = r12.A07;
                AbstractC1310861e r11 = this.A0x.A00;
                AnonymousClass009.A05(r11);
                AnonymousClass1KT r8 = this.A1C;
                C69893aP r9 = new C69893aP(r8);
                ((AbstractActivityC121685jC) r11).A0a = r9;
                AnonymousClass1BP r4 = r5.A0C;
                Activity activity = r5.A00;
                r4.A00 = activity;
                AnonymousClass1AD r13 = r5.A06;
                r4.A05 = r13.A00();
                r4.A07 = r13.A01(r5.A0G, r8);
                r4.A02 = r5.A02;
                r4.A01 = imageButton;
                r4.A03 = mentionableEntry;
                C15260mp A00 = r4.A00();
                C1322466d r7 = new C1322466d(mentionableEntry, r5);
                C14850m9 r03 = r5.A0A;
                C253719d r04 = r5.A0D;
                C252718t r05 = r5.A0H;
                C16120oU r06 = r5.A0B;
                AnonymousClass01d r15 = r5.A03;
                AbstractC253919f r14 = r5.A0E;
                C15320mw r16 = new C15320mw(activity, r15, r5.A04, r5.A05, r5.A07, r5.A08, emojiSearchContainer, r03, r06, A00, r04, gifSearchContainer, r14, r5.A0F, r05);
                r9.A02 = r11;
                r9.A00 = A00;
                A00.A03 = r9;
                A00.A0C(r7);
                ((C15270mq) A00).A0E = new Runnable(r16, r5) { // from class: X.6Iw
                    public final /* synthetic */ C15320mw A00;
                    public final /* synthetic */ C129745yE A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        C129745yE r07 = this.A01;
                        C15320mw r2 = this.A00;
                        r07.A00();
                        r07.A00.getWindow().setSoftInputMode(1);
                        if (r2.A01()) {
                            r2.A00(true);
                        }
                    }
                };
                A00.A0K(this);
                ((C15330mx) r16).A00 = new AbstractC14020ki() { // from class: X.68L
                    @Override // X.AbstractC14020ki
                    public final void APd(C37471mS r3) {
                        AbstractC116455Vm.this.APc(r3.A00);
                    }
                };
                r9.A04 = this;
                r8.A0B.A03(r8.A0A);
                r5.A0I.put(C12970iu.A0h(), A00);
                return;
            }
            C129745yE r92 = this.A0z;
            C134146Dm r17 = this.A0u;
            MentionableEntry mentionableEntry2 = r17.A09;
            ImageButton imageButton2 = r17.A04;
            EmojiSearchContainer emojiSearchContainer2 = r17.A07;
            Activity activity2 = r92.A00;
            C252718t r152 = r92.A0H;
            AbstractC15710nm r142 = r92.A01;
            AnonymousClass19M r10 = r92.A07;
            C231510o r82 = r92.A08;
            AnonymousClass01d r72 = r92.A03;
            AnonymousClass018 r6 = r92.A05;
            AnonymousClass193 r2 = r92.A09;
            C14820m6 r18 = r92.A04;
            C16630pM r52 = r92.A0F;
            C119875fF r3 = new C119875fF(activity2, imageButton2, r142, r92.A02, mentionableEntry2, r72, r18, r6, r10, r82, emojiSearchContainer2, r2, r92, r52, r152);
            C1322366c r22 = new C1322366c(mentionableEntry2, r92);
            C15330mx r19 = new C15330mx(activity2, r6, r10, r3, r82, emojiSearchContainer2, r52);
            r19.A00 = new AbstractC14020ki() { // from class: X.68K
                @Override // X.AbstractC14020ki
                public final void APd(C37471mS r32) {
                    AbstractC116455Vm.this.APc(r32.A00);
                }
            };
            r3.A0C(r22);
            r3.A0E = new Runnable(r19, r92) { // from class: X.6Iv
                public final /* synthetic */ C15330mx A00;
                public final /* synthetic */ C129745yE A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C129745yE r07 = this.A01;
                    C15330mx r23 = this.A00;
                    r07.A00();
                    r07.A00.getWindow().setSoftInputMode(1);
                    if (r23.A01()) {
                        r23.A00(true);
                    }
                }
            };
            C12960it.A1J(r3, r92.A0I, 0);
        }
    }

    public final void A07() {
        View inflate = C12960it.A0E(this).inflate(R.layout.payment_view, (ViewGroup) this, true);
        this.A0K = C12960it.A0I(inflate, R.id.payment_currency_symbol_prefix);
        this.A0L = C12960it.A0I(inflate, R.id.payment_currency_symbol_suffix);
        this.A0H = (TextSwitcher) AnonymousClass028.A0D(inflate, R.id.contact_name);
        ImageView A0K = C12970iu.A0K(inflate, R.id.expand_contact_details_button);
        this.A07 = A0K;
        A0K.setColorFilter(getResources().getColor(R.color.black_alpha_40));
        this.A0I = C12960it.A0I(inflate, R.id.contact_aux_info);
        this.A0W = (ThumbnailButton) AnonymousClass028.A0D(inflate, R.id.contact_photo);
        this.A0V = (ThumbnailButton) AnonymousClass028.A0D(inflate, R.id.bank_logo);
        ImageView A0K2 = C12970iu.A0K(inflate, R.id.expand_details_button);
        this.A08 = A0K2;
        A0K2.setColorFilter(getResources().getColor(R.color.black_alpha_40));
        this.A0G = (TextSwitcher) AnonymousClass028.A0D(inflate, R.id.payment_contact_label);
        this.A0D = C117305Zk.A07(inflate, R.id.payment_method_container);
        this.A0C = C117305Zk.A07(inflate, R.id.payment_contact_container_shimmer);
        this.A0E = C117305Zk.A07(inflate, R.id.payment_method_container_shimmer);
        this.A0O = (ShimmerFrameLayout) AnonymousClass028.A0D(this.A0C, R.id.payment_method_name_shimmer);
        this.A0P = (ShimmerFrameLayout) AnonymousClass028.A0D(this.A0E, R.id.payment_method_name_shimmer);
        this.A0A = C117305Zk.A07(inflate, R.id.add_payment_method_container);
        this.A06 = (FrameLayout) AnonymousClass028.A0D(inflate, R.id.gift_details);
        this.A0t = (PaymentAmountInputField) AnonymousClass028.A0D(inflate, R.id.send_payment_amount);
        this.A0M = C12960it.A0I(inflate, R.id.bank_account_name);
        this.A0J = C12960it.A0I(inflate, R.id.payments_send_payment_error_text);
        this.A0T = (KeyboardPopupLayout) AnonymousClass028.A0D(inflate, R.id.send_payment_keyboard_popup_layout);
        AnonymousClass028.A0D(inflate, R.id.send_payment_amount_error_text_container).setOnClickListener(this);
        this.A0F = C117305Zk.A07(inflate, R.id.send_payment_amount_container);
        this.A0B = C117305Zk.A07(inflate, R.id.payment_contact_container);
        this.A0Q = (TabLayout) AnonymousClass028.A0D(inflate, R.id.payment_tabs);
        int A00 = AnonymousClass00T.A00(getContext(), R.color.settings_icon);
        AnonymousClass2GE.A07(this.A08, A00);
        this.A0Y = this.A0Z.A04(getContext(), "payment-view");
        AnonymousClass2GE.A07(C12970iu.A0K(inflate, R.id.add_payment_method_logo), A00);
        this.A0T.setKeyboardPopupBackgroundColor(AnonymousClass00T.A00(getContext(), R.color.emoji_popup_body));
        if (Build.VERSION.SDK_INT >= 19) {
            AutoTransition autoTransition = new AutoTransition();
            this.A03 = autoTransition;
            autoTransition.setDuration(100L);
        }
        this.A0N = (Group) AnonymousClass028.A0D(inflate, R.id.expressive_payment_widget_group);
        this.A09 = C12970iu.A0K(inflate, R.id.expressive_theme_background);
        FloatingActionButton floatingActionButton = (FloatingActionButton) AnonymousClass028.A0D(inflate, R.id.expression_theme_selection);
        this.A0U = floatingActionButton;
        floatingActionButton.setOnClickListener(new C123945oC(this));
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        this.A05 = alphaAnimation;
        alphaAnimation.setDuration(500);
        this.A05.setAnimationListener(new C119155d0(this));
        Interpolator A002 = AnonymousClass0L1.A00(0.16f, 1.0f, 0.3f, 1.0f);
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), 17432577);
        loadAnimation.setInterpolator(A002);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), 17432576);
        loadAnimation2.setInterpolator(A002);
        loadAnimation2.setDuration((long) getResources().getInteger(17694720));
        this.A0H.setOutAnimation(loadAnimation);
        this.A0H.setInAnimation(loadAnimation2);
    }

    public final void A08() {
        this.A0H.setPadding(0, getResources().getDimensionPixelSize(R.dimen.payment_detail_text_extra_padding_top_without_payment_id), 0, 0);
    }

    public final void A09() {
        this.A0H.setPadding(getResources().getDimensionPixelSize(R.dimen.payment_collapsed_detail_text_extra_padding_left), getResources().getDimensionPixelSize(R.dimen.payment_detail_text_extra_padding_top_with_payment_id), 0, 0);
        this.A0I.setPadding(getResources().getDimensionPixelSize(R.dimen.payment_collapsed_detail_text_extra_padding_left), getResources().getDimensionPixelSize(R.dimen.payment_detail_text_extra_padding_top_with_payment_id), 0, 0);
    }

    public void A0A(Bundle bundle) {
        this.A1J = bundle.getString("extra_payment_preset_amount");
    }

    public void A0B(Bundle bundle) {
        String A0p = C12970iu.A0p(this.A0t);
        this.A1J = A0p;
        this.A1G = A0p;
        bundle.putString("extra_payment_preset_amount", A0p);
    }

    public void A0C(AbstractC001200n r3) {
        AnonymousClass6M9 r32 = (AnonymousClass6M9) r3;
        this.A0s = r32;
        ((AbstractC001200n) r32).ADr().A00(new AnonymousClass054() { // from class: com.whatsapp.payments.ui.widget.PaymentView$$ExternalSyntheticLambda7
            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r2, AbstractC001200n r33) {
                PaymentView.A00(r2, PaymentView.this);
            }
        });
    }

    public final void A0D(C128295vs r10) {
        AnonymousClass04D.A08(this.A0t, r10.A00);
        Pair pair = r10.A01;
        AnonymousClass04D.A08(this.A0L, C12960it.A05(pair.first));
        TextView textView = this.A0L;
        int[] iArr = (int[]) pair.second;
        textView.setPadding(iArr[0], iArr[1], iArr[2], iArr[3]);
        Pair pair2 = r10.A02;
        AnonymousClass04D.A08(this.A0K, C12960it.A05(pair2.first));
        TextView textView2 = this.A0K;
        int[] iArr2 = (int[]) pair2.second;
        textView2.setPadding(iArr2[0], iArr2[1], iArr2[2], iArr2[3]);
    }

    public void A0E(AnonymousClass5Wu r2, int i, int i2) {
        if (r2 != null) {
            ViewStub viewStub = (ViewStub) findViewById(i);
            if (viewStub != null) {
                C88094Eg.A00(viewStub, r2);
            } else {
                r2.AYL(findViewById(i2));
            }
        }
    }

    public void A0F(CharSequence charSequence) {
        if (this.A0J != null) {
            int i = !TextUtils.isEmpty(charSequence) ? 1 : 0;
            this.A0J.setVisibility(C12960it.A02(i));
            this.A0J.setText(charSequence);
            this.A05.cancel();
            this.A05.reset();
            Handler handler = getHandler();
            if (handler != null) {
                Runnable runnable = this.A1P;
                handler.removeCallbacks(runnable);
                if (i != 0) {
                    this.A0J.announceForAccessibility(charSequence);
                    getHandler().postDelayed(runnable, 4000);
                }
            }
        }
    }

    public void A0G(boolean z) {
        if (!z) {
            this.A0D.setVisibility(8);
            this.A0A.setVisibility(8);
            this.A0G.setVisibility(8);
            if (this.A1N) {
                this.A0H.setText(this.A1F);
            }
            this.A0I.setVisibility(8);
        } else if (this.A1N) {
            this.A0H.setText(A02(this.A1F, R.string.payments_send_payment_to));
            A0H(this.A1O);
            this.A0G.setVisibility(8);
        } else {
            this.A0G.setVisibility(0);
            this.A0D.setVisibility(8);
            this.A0A.setVisibility(8);
            if (this.A0w.AK3()) {
                this.A0I.setVisibility(0);
                A09();
                return;
            }
            A08();
        }
    }

    public void A0H(boolean z) {
        this.A1O = z;
        LinearLayout linearLayout = this.A0D;
        if (z) {
            linearLayout.setVisibility(8);
            this.A0A.setVisibility(0);
            return;
        }
        linearLayout.setVisibility(0);
        this.A0A.setVisibility(8);
    }

    public boolean A0I() {
        HashMap hashMap = this.A0z.A0I;
        Iterator A0s = C12990iw.A0s(hashMap);
        while (A0s.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0s);
            PopupWindow popupWindow = (PopupWindow) hashMap.get(A15.getKey());
            if (popupWindow.isShowing()) {
                popupWindow.dismiss();
                int A05 = C12960it.A05(A15.getKey());
                if (A05 != 0) {
                    if (A05 != 1) {
                        if (!(A05 == 2 || A05 == 3)) {
                            return false;
                        }
                    }
                    return true;
                }
                this.A0z.A01(1);
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r5, Integer num, int i) {
        AbstractC1310861e r0 = this.A0x.A00;
        if (r0 != null) {
            ((AbstractActivityC121685jC) r0).A0a.A02(true);
        }
        C134146Dm r1 = this.A0u;
        if (r1 == null) {
            return;
        }
        if (r1.A0B != null || AnonymousClass1US.A0C(r1.A09.getStringText())) {
            C134146Dm r02 = this.A0u;
            if (r02 != null) {
                r02.A00(r5, num);
                return;
            }
            return;
        }
        C004802e A0S = C12980iv.A0S(getContext());
        A0S.A07(R.string.payment_sticker_replace_note_alert_dialog_title);
        A0S.A06(R.string.payment_sticker_replace_note_alert_dialog_message);
        A0S.setPositiveButton(R.string.payment_sticker_replace_note_alert_dialog_replace_action, new DialogInterface.OnClickListener(r5, num) { // from class: X.62n
            public final /* synthetic */ AnonymousClass1KS A01;
            public final /* synthetic */ Integer A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                PaymentView paymentView = PaymentView.this;
                AnonymousClass1KS r2 = this.A01;
                Integer num2 = this.A02;
                C134146Dm r03 = paymentView.A0u;
                if (r03 != null) {
                    r03.A00(r2, num2);
                }
            }
        });
        A0S.setNegativeButton(R.string.payment_sticker_replace_note_alert_dialog_cancel_replace_action, new IDxCListenerShape5S0000000_3_I1(9));
        C12970iu.A1J(A0S);
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r4) {
        boolean z = true;
        if (this.A00 != r4.A00) {
            this.A0z.A01(1);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            TransitionManager.beginDelayedTransition(C12980iv.A0P(this, R.id.send_payment_details), this.A03);
        }
        int i = r4.A00;
        this.A00 = i;
        AbstractC1310961f r0 = this.A0v;
        if (i != 1) {
            z = false;
        }
        r0.AXP(z);
        A06();
    }

    public List getMentionedJids() {
        C134146Dm r0 = this.A0u;
        if (r0 != null) {
            return r0.A09.getMentions();
        }
        return C12960it.A0l();
    }

    public String getPaymentAmountString() {
        Editable text = this.A0t.getText();
        if (text != null) {
            return text.toString();
        }
        return null;
    }

    public C30921Zi getPaymentBackground() {
        if (this.A0N.getVisibility() != 0) {
            return null;
        }
        return (C30921Zi) this.A09.getTag(R.id.selected_expressive_background_theme);
    }

    public String getPaymentNote() {
        C134146Dm r0 = this.A0u;
        if (r0 != null) {
            return r0.A09.getStringText();
        }
        return "";
    }

    public View.OnClickListener getSendPaymentClickListener() {
        return C117305Zk.A0A(this, 191);
    }

    public AnonymousClass1KS getStickerIfSelected() {
        C134146Dm r0 = this.A0u;
        if (r0 != null) {
            return r0.A0B;
        }
        return null;
    }

    public Integer getStickerSendOrigin() {
        C134146Dm r0 = this.A0u;
        if (r0 != null) {
            return r0.A0D;
        }
        return null;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.expand_details_button || view.getId() == R.id.payment_method_container) {
            this.A0v.ATV();
        } else if (view.getId() == R.id.payment_contact_container) {
            if (this.A00 == 1 || this.A0D.getVisibility() == 0 || !this.A1N) {
                this.A0v.ATT();
                return;
            }
            if (Build.VERSION.SDK_INT >= 19) {
                TransitionManager.beginDelayedTransition(C12980iv.A0P(this, R.id.send_payment_details), this.A03);
            }
            A0G(true);
        } else if (view.getId() == R.id.send_payment_amount || view.getId() == R.id.send_payment_note) {
            this.A0z.A00();
        } else if (view.getId() == R.id.send_payment_amount_error_text_container) {
            this.A0t.callOnClick();
        } else if (view.getId() == R.id.add_payment_method_container) {
            this.A0v.ALy();
        } else if (view.getId() == R.id.gift_icon) {
            this.A0v.AR5();
            TextView A0J = C12960it.A0J(this, R.id.gift_tool_tip);
            C12980iv.A1W(this.A0p.A01(), "payment_incentive_tooltip_viewed");
            A0J.setVisibility(8);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A0Y.A00();
    }

    public void setAmountInputData(C127505ub r6) {
        TextView textView;
        AbstractC30791Yv r1 = r6.A01;
        this.A0f = r1;
        this.A01 = r6.A00;
        this.A0t.A0E = r1;
        C30821Yy r3 = r6.A02;
        if (r3 != null) {
            if (r3.A02()) {
                this.A0t.setText(this.A0f.AA8(this.A0c, r3));
            } else {
                this.A0t.setText((CharSequence) null);
            }
        }
        AbstractC30791Yv r4 = this.A0f;
        AbstractC30781Yu r2 = (AbstractC30781Yu) r4;
        CharSequence charSequence = "";
        if (r2.A00 == 0) {
            int i = this.A01;
            if (i == 0) {
                int AH1 = r4.AH1(this.A0c);
                TextView textView2 = this.A0K;
                if (AH1 == 2) {
                    textView2.setText(charSequence);
                    textView = this.A0L;
                    charSequence = this.A0f.AC1(this.A0c);
                } else {
                    textView2.setText(this.A0f.AC1(this.A0c));
                    textView = this.A0L;
                }
            } else if (i == 1) {
                this.A0K.setText(r2.A05);
                textView = this.A0L;
                charSequence = ((AbstractC30781Yu) this.A0f).A04;
            } else {
                return;
            }
        } else {
            this.A0K.setText(charSequence);
            textView = this.A0L;
            charSequence = this.A0f.AA7(getContext(), this.A0f.AC1(this.A0c));
        }
        textView.setText(charSequence);
    }

    public void setBankLogo(Bitmap bitmap) {
        if (bitmap != null) {
            this.A0V.setImageBitmap(bitmap);
        } else {
            this.A0V.setImageResource(R.drawable.bank_logo_placeholder);
        }
    }

    private void setInitialTabConfiguration(C128305vt r3) {
        int i = 1;
        if (r3.A09.A01 == 0) {
            i = 0;
        }
        this.A00 = i;
        AnonymousClass3FN A04 = this.A0Q.A04(i);
        if (A04 != null) {
            A04.A00();
        }
    }

    public void setPaymentAmount(String str) {
        this.A1G = str;
    }

    public void setPaymentContactContainerVisibility(int i) {
        this.A0B.setVisibility(i);
    }

    public void setPaymentMethodText(String str) {
        this.A0M.setText(A02(str, R.string.payments_send_payment_using));
    }
}
