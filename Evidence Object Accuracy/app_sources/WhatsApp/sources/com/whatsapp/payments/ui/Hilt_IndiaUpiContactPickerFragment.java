package com.whatsapp.payments.ui;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.AnonymousClass18S;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C121265hX;
import X.C1329668y;
import X.C18650sn;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;

/* loaded from: classes4.dex */
public abstract class Hilt_IndiaUpiContactPickerFragment extends PaymentContactPickerFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    private void A04() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A04();
        return this.A00;
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A04()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.Hilt_IndiaUpiContactPickerFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment, com.whatsapp.contact.picker.ContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A04();
        A18();
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment
    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            IndiaUpiContactPickerFragment indiaUpiContactPickerFragment = (IndiaUpiContactPickerFragment) this;
            C51112Sw r0 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J A0B = C117295Zj.A0B(r0, indiaUpiContactPickerFragment);
            C117295Zj.A11(A0B, indiaUpiContactPickerFragment);
            C117295Zj.A13(A0B, indiaUpiContactPickerFragment);
            C117295Zj.A10(A0B, indiaUpiContactPickerFragment);
            C117295Zj.A0z(A0B, indiaUpiContactPickerFragment);
            C117295Zj.A12(A0B, indiaUpiContactPickerFragment);
            C117295Zj.A0v(r0, A0B, indiaUpiContactPickerFragment);
            C117295Zj.A14(A0B, indiaUpiContactPickerFragment);
            indiaUpiContactPickerFragment.A03 = C117315Zl.A0A(A0B);
            indiaUpiContactPickerFragment.A04 = (AnonymousClass18S) A0B.AEw.get();
            indiaUpiContactPickerFragment.A05 = C117305Zk.A0M(A0B);
            indiaUpiContactPickerFragment.A00 = C117305Zk.A0G(A0B);
            indiaUpiContactPickerFragment.A06 = C117305Zk.A0T(A0B);
            indiaUpiContactPickerFragment.A02 = (C18650sn) A0B.AEe.get();
            indiaUpiContactPickerFragment.A01 = (C1329668y) A0B.A9d.get();
            indiaUpiContactPickerFragment.A07 = (C121265hX) A0B.A9a.get();
        }
    }
}
