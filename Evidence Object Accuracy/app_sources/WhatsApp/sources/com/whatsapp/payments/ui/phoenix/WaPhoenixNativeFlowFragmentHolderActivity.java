package com.whatsapp.payments.ui.phoenix;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass6EA;
import X.C004902f;
import X.C117295Zj;
import X.C127895vE;
import X.C17120qI;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;

/* loaded from: classes4.dex */
public class WaPhoenixNativeFlowFragmentHolderActivity extends ActivityC13790kL {
    public C127895vE A00;
    public C17120qI A01;
    public FdsContentFragmentManager A02;
    public boolean A03;

    public WaPhoenixNativeFlowFragmentHolderActivity() {
        this(0);
    }

    public WaPhoenixNativeFlowFragmentHolderActivity(int i) {
        this.A03 = false;
        C117295Zj.A0p(this, 114);
    }

    @Override // X.ActivityC000900k
    public void A0Y() {
        FdsContentFragmentManager fdsContentFragmentManager = this.A02;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = true;
            Runnable runnable = fdsContentFragmentManager.A02;
            if (runnable != null) {
                runnable.run();
                fdsContentFragmentManager.A02 = null;
            }
        }
        super.A0Y();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (C17120qI) A1M.ALs.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C127895vE r2 = this.A00;
        r2.A03.A02(r2.A01).A01(new AnonymousClass6EA(r2.A00, false));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_phoenix_nativeflow_container);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("fds_on_back");
        String stringExtra2 = intent.getStringExtra("fds_state_name");
        String stringExtra3 = intent.getStringExtra("fds_observer_id");
        intent.getStringExtra("fds_resource_id");
        this.A00 = new C127895vE(this, this.A01, stringExtra2, stringExtra3, stringExtra);
        AnonymousClass01F A0V = A0V();
        this.A02 = FdsContentFragmentManager.A00(intent.getStringExtra("fds_observer_id"));
        if (A0V.A03() == 0) {
            C004902f r2 = new C004902f(A0V);
            r2.A06(this.A02, R.id.nativeflow_fragment_container);
            r2.A0F(stringExtra2);
            r2.A02();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C127895vE r3 = this.A00;
        String str = r3.A01;
        if (str != null) {
            try {
                C17120qI r0 = r3.A03;
                synchronized (r0.A02(str)) {
                    r0.A02(str).A03(r3);
                }
            } catch (Exception unused) {
            }
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        FdsContentFragmentManager fdsContentFragmentManager = this.A02;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = false;
        }
        super.onPause();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        FdsContentFragmentManager fdsContentFragmentManager = this.A02;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = false;
        }
        super.onSaveInstanceState(bundle);
    }
}
