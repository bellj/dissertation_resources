package com.whatsapp.payments.ui;

import X.AbstractActivityC119225dN;
import X.AbstractActivityC121705jc;
import X.AbstractC129485xn;
import X.AbstractC129955yZ;
import X.AbstractC136206Lq;
import X.AbstractC14440lR;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01T;
import X.AnonymousClass102;
import X.AnonymousClass1BY;
import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import X.AnonymousClass2FL;
import X.AnonymousClass3CS;
import X.AnonymousClass3CT;
import X.AnonymousClass3FE;
import X.AnonymousClass5TZ;
import X.AnonymousClass5g0;
import X.AnonymousClass60T;
import X.AnonymousClass60Z;
import X.AnonymousClass61O;
import X.AnonymousClass69D;
import X.AnonymousClass6AK;
import X.AnonymousClass6B7;
import X.AnonymousClass6Lp;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119775f5;
import X.C120045fX;
import X.C120205fn;
import X.C120245fr;
import X.C120305fx;
import X.C120315fy;
import X.C120325fz;
import X.C120335g1;
import X.C120355g3;
import X.C120365g4;
import X.C120915gx;
import X.C120925gy;
import X.C121875kP;
import X.C124705py;
import X.C124785q7;
import X.C125065qd;
import X.C125625rY;
import X.C125785ro;
import X.C126235sY;
import X.C126245sZ;
import X.C126255sa;
import X.C126275sc;
import X.C126305sf;
import X.C126315sg;
import X.C126725tL;
import X.C126735tM;
import X.C126745tN;
import X.C126875ta;
import X.C126925tf;
import X.C126935tg;
import X.C127605ul;
import X.C127825v7;
import X.C127835v8;
import X.C127965vL;
import X.C128015vQ;
import X.C128065vV;
import X.C128075vW;
import X.C128115va;
import X.C128125vb;
import X.C128165vf;
import X.C128175vg;
import X.C128245vn;
import X.C128265vp;
import X.C128325vv;
import X.C128485wB;
import X.C128545wH;
import X.C128565wJ;
import X.C128575wK;
import X.C128585wL;
import X.C128755wc;
import X.C128765wd;
import X.C128775we;
import X.C128785wf;
import X.C128905wr;
import X.C128915ws;
import X.C128925wt;
import X.C128935wu;
import X.C129055x6;
import X.C129135xE;
import X.C129195xK;
import X.C129215xM;
import X.C129225xN;
import X.C129265xR;
import X.C129335xY;
import X.C129355xa;
import X.C129365xb;
import X.C129385xd;
import X.C129395xe;
import X.C129565xv;
import X.C12960it;
import X.C12970iu;
import X.C129725yC;
import X.C129735yD;
import X.C12980iv;
import X.C12990iw;
import X.C129945yY;
import X.C130005ye;
import X.C13000ix;
import X.C130065yk;
import X.C130255z3;
import X.C130335zF;
import X.C130705zq;
import X.C130765zw;
import X.C1309660r;
import X.C1311161i;
import X.C1330369f;
import X.C133276Ad;
import X.C133286Ae;
import X.C133366Am;
import X.C133376An;
import X.C133386Ao;
import X.C133456Av;
import X.C133476Ax;
import X.C14370lK;
import X.C14830m7;
import X.C14900mE;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C18590sh;
import X.C18600si;
import X.C18610sj;
import X.C18650sn;
import X.C22120yY;
import X.C22710zW;
import X.C241414j;
import X.C25871Bd;
import X.C25891Bf;
import X.C26061Bw;
import X.C28421Nd;
import X.C30881Ze;
import X.C36021jC;
import X.C43951xu;
import X.C93954b1;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.JsonReader;
import android.util.JsonWriter;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.chromium.net.UrlRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class BrazilPayBloksActivity extends AbstractActivityC121705jc {
    public C129055x6 A00;
    public AnonymousClass3FE A01;
    public C128485wB A02;
    public AnonymousClass102 A03;
    public AbstractC28901Pl A04;
    public C241414j A05;
    public C17220qS A06;
    public C1309660r A07;
    public C120045fX A08;
    public AnonymousClass69D A09;
    public AnonymousClass60Z A0A;
    public C22710zW A0B;
    public C128175vg A0C;
    public C129265xR A0D;
    public C129565xv A0E;
    public C128325vv A0F;
    public C129385xd A0G;
    public C25891Bf A0H;
    public C25871Bd A0I;
    public C129945yY A0J;
    public C121875kP A0K;
    public C130065yk A0L;
    public C129735yD A0M;
    public String A0N;
    public boolean A0O;

    public BrazilPayBloksActivity() {
        this(0);
        this.A01 = null;
        this.A0N = null;
        this.A04 = null;
    }

    public BrazilPayBloksActivity(int i) {
        this.A0O = false;
        C117295Zj.A0p(this, 12);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0O) {
            this.A0O = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119225dN.A0B(A09, A1M, this, AbstractActivityC119225dN.A0A(A1M, this, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this))));
            AbstractActivityC119225dN.A0K(A1M, this, AbstractActivityC119225dN.A09(A1M, this));
            ((AbstractActivityC121705jc) this).A0B = AbstractActivityC119225dN.A02(A1M, this);
            this.A0S = (C129395xe) A1M.AEy.get();
            this.A05 = (C241414j) A1M.AEr.get();
            this.A06 = C12990iw.A0c(A1M);
            this.A02 = (C128485wB) A1M.A1h.get();
            this.A07 = (C1309660r) A1M.A1p.get();
            this.A0K = (C121875kP) A1M.A1t.get();
            this.A00 = (C129055x6) A1M.A1e.get();
            this.A0E = (C129565xv) A1M.AF6.get();
            this.A0G = (C129385xd) A1M.A1v.get();
            this.A0J = (C129945yY) A1M.A1q.get();
            this.A0B = C117305Zk.A0O(A1M);
            this.A0C = (C128175vg) A1M.ACm.get();
            this.A03 = C117305Zk.A0G(A1M);
            this.A0A = (AnonymousClass60Z) A1M.A24.get();
            this.A09 = A09.A08();
            this.A0I = (C25871Bd) A1M.ABZ.get();
            this.A0F = (C128325vv) A1M.A1u.get();
            this.A0L = (C130065yk) A1M.A1z.get();
            this.A0M = A09.A0C();
            this.A0D = (C129265xR) A1M.AF3.get();
            this.A08 = (C120045fX) A1M.A1s.get();
            this.A0H = (C25891Bf) A1M.A1w.get();
        }
    }

    @Override // X.AbstractActivityC119645em
    public AnonymousClass5TZ A2e() {
        return new AnonymousClass5TZ() { // from class: X.671
            @Override // X.AnonymousClass5TZ
            public final AbstractC17450qp AAO() {
                BrazilPayBloksActivity brazilPayBloksActivity = BrazilPayBloksActivity.this;
                return new C119495eX(brazilPayBloksActivity.A2h(), new C124895qI(), new C125795rp(brazilPayBloksActivity.A0H.A00));
            }
        };
    }

    public final void A2n(AnonymousClass3FE r4) {
        C004802e A0S = C12980iv.A0S(this);
        A0S.setTitle(getString(R.string.doc_upload_failure_title));
        A0S.A0A(getString(R.string.doc_upload_failure_message));
        C117305Zk.A17(A0S, getString(R.string.payments_try_again), r4, 7);
    }

    public void A2o(AnonymousClass3FE r10, C30881Ze r11, String str, List list, boolean z) {
        String str2;
        HashMap A11 = C12970iu.A11();
        if (!TextUtils.isEmpty(str)) {
            A11.put("callback_url", str);
        }
        A11.put("credential_id", r11.A0A);
        A11.put("readable_name", C1311161i.A05(this, r11));
        C119775f5 r8 = (C119775f5) r11.A08;
        if (r8 != null && ((str2 = r8.A0N) == null || !(!"DISABLED".equals(str2)))) {
            A11.put("p2p_ineligible", "1");
        }
        A11.put("network_name", C30881Ze.A07(r11.A01));
        if (r8 != null && !TextUtils.isEmpty(r8.A0E)) {
            A11.put("card_image_url", r8.A0E);
        }
        if (z) {
            A11.put("verified_state", "1");
        } else if (list == null || list.isEmpty()) {
            if (r8 != null && r8.A07) {
                A11.put("verified_state", "0");
                A11.put("card_need_device_binding", "1");
            }
            AbstractActivityC121705jc.A0l(r10, null, -233);
            return;
        } else {
            if (!C1309660r.A01(list)) {
                JSONArray A02 = this.A07.A02(list);
                if (A02 != null) {
                    A11.put("verify_methods", A02.toString());
                }
                A11.put("verified_state", "0");
            }
            AbstractActivityC121705jc.A0l(r10, null, -233);
            return;
        }
        r10.A01("on_success", A11);
    }

    public final void A2p(AnonymousClass3FE r10, String str) {
        C14900mE r3 = ((ActivityC13810kN) this).A05;
        new C129225xN(this, ((ActivityC13810kN) this).A03, r3, this.A03, this.A06, ((AbstractActivityC121705jc) this).A0A, new AbstractC136206Lq(r10, this) { // from class: X.6AF
            public final /* synthetic */ AnonymousClass3FE A00;
            public final /* synthetic */ BrazilPayBloksActivity A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136206Lq
            public final void AQz(C452120p r5, List list) {
                JSONArray A00;
                AnonymousClass3FE r32 = this.A00;
                if (r5 != null || (A00 = C1309660r.A00(list)) == null) {
                    r32.A00("on_failure");
                } else {
                    C117305Zk.A1F(r32, "banks", A00.toString(), C12970iu.A11());
                }
            }
        }, str).A00();
    }

    public final void A2q(AnonymousClass3FE r14, String str, List list, List list2, int i) {
        if (i >= list.size()) {
            C117315Zl.A0S(r14);
            return;
        }
        C129265xR r3 = this.A0D;
        File file = (File) list.get(i);
        C14370lK r1 = C14370lK.A0Q;
        C128015vQ r5 = new C128015vQ(r14, this, str, list2, list, i);
        Context context = r3.A01;
        C14900mE r8 = r3.A02;
        C18610sj r10 = r3.A06;
        C18650sn r9 = r3.A05;
        AnonymousClass60T r11 = r3.A07;
        C129215xM r6 = new C129215xM(context, r8, r9, r10, r11, "DOC-UPLOAD");
        AnonymousClass6B7 A0C = C117315Zl.A0C(r11, "FB", "DOC-UPLOAD");
        if (A0C != null) {
            r3.A00(A0C, r5, r1, file);
        } else {
            r6.A00(new C133456Av(r3, r5, r1, file), "FB");
        }
    }

    public final void A2r(AnonymousClass3FE r22, Map map, int i) {
        String A0X = C117295Zj.A0X("full_name", map);
        String A0t = C12970iu.A0t("tax_id", map);
        AnonymousClass009.A04(A0t);
        String replaceAll = A0t.replaceAll("[^\\d]", "");
        String replaceAll2 = C117305Zk.A0h(((ActivityC13790kL) this).A01).replaceAll("[^\\d]", "");
        String replaceAll3 = C117295Zj.A0X("address_postal_code", map).replaceAll("[^\\d]", "");
        String A0t2 = C12970iu.A0t("address_street_name", map);
        String A0t3 = C12970iu.A0t("address_city", map);
        String A0t4 = C12970iu.A0t("address_state", map);
        String A0t5 = C12970iu.A0t("address_houe_number", map);
        String A0t6 = C12970iu.A0t("address_extra_line", map);
        String A0t7 = C12970iu.A0t("address_neighborhood", map);
        C14900mE r6 = ((ActivityC13810kN) this).A05;
        C18590sh r10 = this.A0T;
        C129195xK r4 = new C129195xK(this, r6, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0J, r10, A0X, replaceAll, replaceAll2, A0t2, A0t5, A0t6, A0t7, A0t3, A0t4, replaceAll3);
        C133286Ae r3 = new C133286Ae(r22, this, map, i);
        AnonymousClass60T r102 = r4.A05;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r102, "FB", "KYC");
        if (A0C == null || !A0C.A05.equalsIgnoreCase("FB")) {
            new C129215xM(r4.A01, r4.A02, r4.A03, r4.A04, r102, "KYC").A00(new C133366Am(r4, r3), "FB");
            return;
        }
        r4.A00(r3, A0C);
    }

    @Override // X.AbstractActivityC121705jc, X.AbstractC136496Mt
    public boolean AHz(int i) {
        if (i != 442) {
            return super.AHz(i);
        }
        BrazilReTosFragment brazilReTosFragment = new BrazilReTosFragment();
        brazilReTosFragment.A1M();
        Adm(brazilReTosFragment);
        return true;
    }

    @Override // X.AbstractActivityC121705jc, X.AbstractC136496Mt
    public void AZC(AnonymousClass3FE r36, String str, Map map) {
        IDxRCallbackShape0S0200000_3_I1 iDxRCallbackShape0S0200000_3_I1;
        long j;
        int i;
        AnonymousClass1V8 r5;
        C17220qS r1;
        String str2;
        C119775f5 r0;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        Boolean bool;
        int i2;
        C130765zw r12;
        C130705zq r8;
        C130005ye r02;
        KeyPair keyPair;
        JSONObject optJSONObject;
        AbstractC129485xn r10;
        JSONObject optJSONObject2;
        int i3;
        Boolean bool2;
        C126315sg r52;
        if (TextUtils.isEmpty(str)) {
            r36.A00("");
        }
        short s = -1;
        switch (str.hashCode()) {
            case -1828362259:
                s = C117305Zk.A0t("get_compliance_status", str);
                break;
            case -1789788977:
                s = C117305Zk.A0u("show_account_removal_dialog", str);
                break;
            case -1579572125:
                s = C117305Zk.A0v("get_merchant_reg_data", str);
                break;
            case -1427341776:
                s = C117305Zk.A0w("get_autofill_address", str);
                break;
            case -1326006358:
                if (str.equals("get_filtered_card_verification_options")) {
                    s = 4;
                    break;
                }
                break;
            case -1290918873:
                if (str.equals("get_payout_banks")) {
                    s = 5;
                    break;
                }
                break;
            case -1265267765:
                if (str.equals("send_kyc_data")) {
                    s = 6;
                    break;
                }
                break;
            case -1264881022:
                if (str.equals("verify_card_otp")) {
                    s = 7;
                    break;
                }
                break;
            case -1236338706:
                if (str.equals("add_card")) {
                    s = 8;
                    break;
                }
                break;
            case -1157449815:
                if (str.equals("dial_phone_number")) {
                    s = 9;
                    break;
                }
                break;
            case -1017011091:
                if (str.equals("link_merchant")) {
                    s = 10;
                    break;
                }
                break;
            case -981053487:
                if (str.equals("pre_link_merchant")) {
                    s = 11;
                    break;
                }
                break;
            case -857462632:
                if (str.equals("bind_device")) {
                    s = 12;
                    break;
                }
                break;
            case -641808715:
                if (str.equals("add_credential")) {
                    s = 13;
                    break;
                }
                break;
            case -524241064:
                if (str.equals("show_confirm_cancel_add_card_alert_dialog")) {
                    s = 14;
                    break;
                }
                break;
            case -491008410:
                if (str.equals("refetch_verification_methods")) {
                    s = 15;
                    break;
                }
                break;
            case -438460728:
                if (str.equals("document_upload_submit_document")) {
                    s = 16;
                    break;
                }
                break;
            case 159220715:
                if (str.equals("dismiss_confirm_cancel_add_card_alert_dialog")) {
                    s = 17;
                    break;
                }
                break;
            case 243254635:
                if (str.equals("reset_pin_from_card")) {
                    s = 18;
                    break;
                }
                break;
            case 580608584:
                if (str.equals("get_card_network")) {
                    s = 19;
                    break;
                }
                break;
            case 828045967:
                if (str.equals("send_taxid")) {
                    s = 20;
                    break;
                }
                break;
            case 1410504463:
                if (str.equals("show_contact_picker")) {
                    s = 21;
                    break;
                }
                break;
            case 1546234131:
                if (str.equals("reg_merchant")) {
                    s = 22;
                    break;
                }
                break;
            case 1684922085:
                if (str.equals("handle_error_native")) {
                    s = 23;
                    break;
                }
                break;
            case 1760388972:
                if (str.equals("update_merchant_account")) {
                    s = 24;
                    break;
                }
                break;
            case 2069491034:
                if (str.equals("set_onboarding_started")) {
                    s = 25;
                    break;
                }
                break;
            case 2124929861:
                if (str.equals("get_kyc_status")) {
                    s = 26;
                    break;
                }
                break;
            case 2146747614:
                if (str.equals("submit_verification_method")) {
                    s = 27;
                    break;
                }
                break;
        }
        switch (s) {
            case 0:
                C128175vg r6 = this.A0C;
                C128785wf r4 = new C128785wf(r36, this);
                C126745tN r2 = r6.A06;
                try {
                    String A04 = r2.A00.A04();
                    if (!TextUtils.isEmpty(A04) && (optJSONObject2 = C13000ix.A05(A04).optJSONObject("td")) != null) {
                        if (optJSONObject2.optBoolean("td_is_committed", false)) {
                            r4.A01.A2l(r4.A00);
                            return;
                        }
                    }
                } catch (JSONException e) {
                    Log.e("PAY: TrustedDeviceKeyStore isCommitted failed", e);
                }
                try {
                    try {
                        r8 = r2.A01;
                        r02 = (C130005ye) r8.A00.get();
                    } catch (C124785q7 | NoSuchAlgorithmException | JSONException e2) {
                        Log.e("PAY: MFAFactors/registerTD/", e2);
                        r4.A00(new C130765zw(6));
                        return;
                    }
                } catch (InvalidAlgorithmParameterException unused) {
                    r12 = new C130765zw(8);
                }
                if (r02 == null || (keyPair = r02.A01("alias-payments-br-trusted-device-key")) == null) {
                    try {
                        String A042 = r2.A00.A04();
                        if (!TextUtils.isEmpty(A042) && (optJSONObject = C13000ix.A05(A042).optJSONObject("td")) != null) {
                            byte[] decode = Base64.decode(optJSONObject.getString("td_public_key_bytes"), 11);
                            byte[] decode2 = Base64.decode(optJSONObject.getString("td_private_key_bytes"), 11);
                            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(decode);
                            PKCS8EncodedKeySpec pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(decode2);
                            try {
                                KeyFactory instance = KeyFactory.getInstance("EC");
                                keyPair = new KeyPair(instance.generatePublic(x509EncodedKeySpec), instance.generatePrivate(pKCS8EncodedKeySpec));
                            } catch (NoSuchAlgorithmException | InvalidKeySpecException e3) {
                                Log.w("PAY: TrustedDeviceKeyStore recreateKeyPair failed", e3);
                            }
                        }
                    } catch (JSONException e4) {
                        Log.w("PAY: TrustedDeviceKeyStore retrieveKeyPair failed", e4);
                    }
                    AnonymousClass01T A01 = r8.A01();
                    Object obj = A01.A00;
                    if (obj != null) {
                        Object obj2 = A01.A01;
                        AnonymousClass009.A05(obj2);
                        if (!C12970iu.A1Y(obj2)) {
                            KeyPair keyPair2 = (KeyPair) obj;
                            try {
                                C18600si r82 = r2.A00;
                                String A043 = r82.A04();
                                if (!TextUtils.isEmpty(A043)) {
                                    JSONObject A05 = C13000ix.A05(A043);
                                    JSONObject optJSONObject3 = A05.optJSONObject("td");
                                    if (optJSONObject3 == null) {
                                        optJSONObject3 = C117295Zj.A0a();
                                    }
                                    optJSONObject3.put("td_public_key_bytes", Base64.encodeToString(keyPair2.getPublic().getEncoded(), 11));
                                    optJSONObject3.put("td_private_key_bytes", Base64.encodeToString(keyPair2.getPrivate().getEncoded(), 11));
                                    A05.put("td", optJSONObject3);
                                    C117295Zj.A1E(r82, A05);
                                }
                            } catch (JSONException e5) {
                                Log.w("PAY: TrustedDeviceKeyStore store failed", e5);
                                throw e5;
                            }
                        }
                        keyPair = (KeyPair) obj;
                        if (keyPair == null) {
                        }
                    }
                    r12 = new C130765zw(8);
                    r4.A00(r12);
                    return;
                }
                C127605ul r83 = r6.A01;
                Context context = r6.A00.A00;
                String A0n = C117305Zk.A0n(keyPair.getPublic().getEncoded());
                ArrayList A0l = C12960it.A0l();
                A0l.add(new C126735tM("auth_ticket_type", "TRUSTED_DEVICE"));
                A0l.add(new C126735tM("public_key", A0n));
                C128125vb r15 = new C128125vb(context, r83.A00, r83.A01, r83.A02, r83.A03, A0l, Collections.emptyList());
                C127835v8 r7 = r6.A05;
                JSONObject A0a = C117295Zj.A0a();
                A0a.put("ver", 1);
                A0a.put("op", r15.A05);
                A0a.put("nonce", C117295Zj.A0U(r7.A00, r7.A01));
                JSONObject A0a2 = C117295Zj.A0a();
                JSONObject A0a3 = C117295Zj.A0a();
                for (C126735tM r03 : r15.A06) {
                    A0a2.put(r03.A00, r03.A01);
                }
                List<Object> list = r15.A07;
                if (!list.isEmpty()) {
                    JSONArray A0L = C117315Zl.A0L();
                    for (Object obj3 : list) {
                        A0L.put(obj3);
                    }
                    A0a2.put("caps", A0L);
                }
                A0a2.put("app_id", "com.whatsapp");
                A0a2.put("device_id", r7.A03.A01());
                A0a.put("data", A0a2);
                if (A0a3.length() > 0) {
                    AnonymousClass009.A06(null, "server key was never set, its null");
                    r10 = new C120925gy(r7.A02, r7.A04, A0a, A0a3);
                } else {
                    r10 = new C120915gx(A0a);
                }
                r10.A00(keyPair.getPublic());
                try {
                    List list2 = r10.A00;
                    AnonymousClass009.A05(list2);
                    if (list2.contains(C130255z3.A00(keyPair.getPublic()))) {
                        PublicKey publicKey = keyPair.getPublic();
                        try {
                            String A012 = r10.A01();
                            AnonymousClass009.A05(A012);
                            JSONObject A0a4 = C117295Zj.A0a();
                            A0a4.put("alg", "ES256");
                            byte[] A0a5 = C117315Zl.A0a(TextUtils.join(".", new String[]{Base64.encodeToString(C117315Zl.A0a(C117305Zk.A0l(C130255z3.A00(publicKey), "kid", A0a4)), 11), A012}));
                            try {
                                Signature instance2 = Signature.getInstance("SHA256withECDSA");
                                instance2.initSign(keyPair.getPrivate());
                                instance2.update(A0a5);
                                byte[] sign = instance2.sign();
                                if (sign != null) {
                                    PublicKey publicKey2 = keyPair.getPublic();
                                    try {
                                        List list3 = r10.A00;
                                        AnonymousClass009.A05(list3);
                                        if (list3.contains(C130255z3.A00(publicKey2))) {
                                            String encodeToString = Base64.encodeToString(AnonymousClass61O.A01(sign), 11);
                                            JSONObject A0a6 = C117295Zj.A0a();
                                            A0a6.put("signature", encodeToString);
                                            JSONObject A0a7 = C117295Zj.A0a();
                                            A0a7.put("alg", "ES256");
                                            A0a6.put("protected", Base64.encodeToString(C117315Zl.A0a(C117305Zk.A0l(C130255z3.A00(publicKey2), "kid", A0a7)), 11));
                                            JSONArray jSONArray = r10.A01;
                                            jSONArray.put(A0a6);
                                            try {
                                                String A013 = r10.A01();
                                                AnonymousClass009.A05(A013);
                                                JSONObject A0a8 = C117295Zj.A0a();
                                                A0a8.put("payload", A013);
                                                AnonymousClass1V8 r53 = new AnonymousClass1V8("trust-token", Base64.encodeToString(C117315Zl.A0a(C117305Zk.A0l(jSONArray, "signatures", A0a8)), 11), (AnonymousClass1W9[]) null);
                                                C126725tL r72 = new C126725tL(r6, r4);
                                                C18610sj r62 = r15.A04;
                                                ArrayList A0l2 = C12960it.A0l();
                                                C117295Zj.A1M("action", "mfa-create-auth-ticket-based-factor", A0l2);
                                                r62.A0E(new IDxRCallbackShape0S0200000_3_I1(r15.A00, r15.A01, r15.A03, r72, r15, 7), new AnonymousClass1V8("account", C117305Zk.A1b(A0l2), new AnonymousClass1V8[]{r53}), "set");
                                                return;
                                            } catch (UnsupportedEncodingException | NoSuchAlgorithmException | JSONException e6) {
                                                Log.w("PAY: DefaultTrustTokenBuilder/build", e6);
                                                throw new C124785q7(e6);
                                            }
                                        } else {
                                            throw C12960it.A0U("cannot sign with public key that has not been declared");
                                        }
                                    } catch (UnsupportedEncodingException | NoSuchAlgorithmException | JSONException e7) {
                                        Log.w("PAY: DefaultTrustTokenBuilder/addSignature", e7);
                                        throw new C124785q7(e7);
                                    }
                                }
                            } catch (Exception unused2) {
                            }
                            throw new C124705py();
                        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | JSONException e8) {
                            Log.e("PAY: DefaultTrustTokenBuilder/constructInputForSigning", e8);
                            throw new C124785q7(e8);
                        }
                    } else {
                        throw C12960it.A0U("cannot sign with public key that has not been declared");
                    }
                } catch (C124705py | NoSuchAlgorithmException e9) {
                    Log.w("PAY: DefaultTrustTokenBuilder/signWith", e9);
                    throw new C124785q7(e9);
                }
            case 1:
                i3 = 102;
                C36021jC.A01(this, i3);
                return;
            case 2:
                r36.A01("on_success", this.A0F.A0J);
                return;
            case 3:
                String A0t = C12970iu.A0t("cep", map);
                if (A0t == null) {
                    C117315Zl.A0S(r36);
                    return;
                }
                String replace = A0t.replace("-", "");
                C127825v7 r9 = new C127825v7(this, ((ActivityC13810kN) this).A05, this.A06, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D);
                C126925tf r84 = new C126925tf(r36, this);
                C17220qS r3 = r9.A02;
                String A014 = r3.A01();
                C117295Zj.A1B(r3, new IDxRCallbackShape0S0200000_3_I1(r9.A00, r9.A01, r9.A03, r84, r9, 2), new C126255sa(new AnonymousClass3CS(A014), replace).A00, A014);
                return;
            case 4:
                String A0t2 = C12970iu.A0t("verify_methods", map);
                String A0t3 = C12970iu.A0t("keys", map);
                if (!(A0t2 == null || A0t3 == null)) {
                    List asList = Arrays.asList(A0t3.split(","));
                    StringWriter stringWriter = new StringWriter();
                    JsonWriter jsonWriter = new JsonWriter(stringWriter);
                    try {
                        JsonReader jsonReader = new JsonReader(new StringReader(A0t2));
                        jsonWriter.beginObject();
                        jsonWriter.name("verification_options");
                        while (jsonReader.hasNext()) {
                            jsonReader.beginArray();
                            jsonWriter.beginArray();
                            while (jsonReader.hasNext()) {
                                jsonReader.beginObject();
                                jsonWriter.beginObject();
                                while (jsonReader.hasNext()) {
                                    String nextName = jsonReader.nextName();
                                    if (asList.contains(nextName)) {
                                        jsonWriter.name(nextName).value(jsonReader.nextString());
                                    } else {
                                        jsonReader.skipValue();
                                    }
                                }
                                jsonReader.endObject();
                                jsonWriter.endObject();
                            }
                            jsonWriter.endArray();
                        }
                        jsonWriter.endObject();
                        jsonReader.close();
                        HashMap A11 = C12970iu.A11();
                        A11.put("payload", stringWriter.toString());
                        r36.A02("on_success", A11);
                        return;
                    } catch (IOException unused3) {
                    }
                }
                C117315Zl.A0S(r36);
                return;
            case 5:
                AnonymousClass6B7 A015 = ((AbstractActivityC121705jc) this).A0J.A01("FB", "KYC", true);
                if (A015 != null) {
                    String str8 = A015.A05;
                    if (str8.equalsIgnoreCase("FB")) {
                        A2p(r36, str8);
                        return;
                    }
                }
                new C129215xM(this, ((ActivityC13810kN) this).A05, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0J, "KYC").A00(new C133386Ao(r36, this), "FB");
                return;
            case 6:
                A2r(r36, map, 0);
                return;
            case 7:
                String A0t4 = C12970iu.A0t("otp", map);
                AnonymousClass009.A04(A0t4);
                String replace2 = A0t4.replace(" ", "");
                map.get("card_verify_type");
                String A0t5 = C12970iu.A0t("credential_id", map);
                String A0t6 = C12970iu.A0t("card_verify_identifier", map);
                String A0t7 = C12970iu.A0t("card_verify_otp_resend_interval_sec", map);
                String A0U = C117295Zj.A0U(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05);
                C120365g4 r13 = new C120365g4(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05, this.A03, this.A05, this.A06, this.A0A, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, ((AbstractActivityC121705jc) this).A0J, this.A0G, ((AbstractActivityC121705jc) this).A0M, new C128915ws(r36, this, A0t7), replace2, A0U, A0t5, A0t6);
                String str9 = r13.A06;
                C241414j r42 = ((AbstractC129955yZ) r13).A04;
                AbstractC28901Pl A08 = r42.A08(str9);
                if (A08 == null || (r0 = (C119775f5) A08.A08) == null || !"VISA".equals(r0.A03)) {
                    C30881Ze r04 = (C30881Ze) r42.A08(str9);
                    if (r04 == null || r04.A01 != 5) {
                        Log.i("PAY: BrazilVerifyCardOTPSendAction sendRequestCardVerification without encrypted value");
                        r13.A03(r13.A09);
                        return;
                    }
                    Log.i("PAY: BrazilStepUpVerificationBase getProviderEncryptionKeyAsync");
                    C128075vW r43 = ((AbstractC129955yZ) r13).A09.A00;
                    if (r43 != null) {
                        String str10 = r43.A02;
                        r43.A02 = null;
                        if (str10 != null) {
                            r13.A02(null, str10);
                            return;
                        }
                    }
                    C127965vL r92 = new C127965vL(((AbstractC129955yZ) r13).A00, ((AbstractC129955yZ) r13).A01, ((AbstractC129955yZ) r13).A02, ((AbstractC129955yZ) r13).A03, ((AbstractC129955yZ) r13).A05, ((AbstractC129955yZ) r13).A06);
                    C125625rY r14 = new C125625rY(r13);
                    String A0j = C117305Zk.A0j(r92.A02, r92.A03);
                    C18610sj r16 = r92.A05;
                    AnonymousClass1W9[] r44 = new AnonymousClass1W9[4];
                    C12960it.A1M("action", "br-request-new-challenge", r44, 0);
                    C12960it.A1M("network_type", "elo".toUpperCase(Locale.ROOT), r44, 1);
                    C117295Zj.A1P("scope", "ADD_CARD", r44);
                    C117305Zk.A1Q("nonce", A0j, r44);
                    r16.A0F(new IDxRCallbackShape0S0200000_3_I1(r92.A00, r92.A01, r92.A04, r14, r92, 5), C117315Zl.A0G(r44), "set", C26061Bw.A0L);
                    return;
                }
                Log.i("PAY: BrazilVerifyCardOTPSendAction sendRequestCardVerification with encrypted value");
                r13.A00();
                return;
            case 8:
                String A0t8 = C12970iu.A0t("cvv", map);
                String[] split = C117295Zj.A0X("expiry_date", map).split("/");
                int[] iArr = {C28421Nd.A00(split[0], 0), C28421Nd.A00(split[1], -2000) + 2000};
                String A0t9 = C12970iu.A0t("card_number", map);
                if (((ActivityC13810kN) this).A0C.A07(1482)) {
                    bool2 = Boolean.valueOf(!this.A0L.A03.A0E("add_card"));
                } else {
                    bool2 = null;
                }
                C14830m7 r17 = ((ActivityC13790kL) this).A05;
                C14900mE r18 = ((ActivityC13810kN) this).A05;
                C15570nT r19 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r110 = ((ActivityC13830kP) this).A05;
                C17220qS r111 = this.A06;
                AnonymousClass1BY r112 = this.A0U;
                C18590sh r152 = this.A0T;
                C17070qD r142 = ((AbstractActivityC121705jc) this).A0G;
                C18600si r132 = ((AbstractActivityC121705jc) this).A0C;
                C22120yY r122 = this.A0V;
                C129945yY r102 = this.A0J;
                C120335g1 r22 = new C120335g1(this, r18, r19, ((ActivityC13810kN) this).A07, r17, this.A03, r111, this.A0A, ((AbstractActivityC121705jc) this).A0A, r132, ((AbstractActivityC121705jc) this).A0D, r142, this.A0G, r102, new C128775we(r36, this), r152, r112, r122, r110, bool2, A0t9, A0t8, iArr[0], iArr[1]);
                C129385xd r54 = this.A0G;
                C126875ta r45 = new C126875ta(r22, r54);
                C120045fX r113 = r54.A0B;
                r113.A04(r45);
                if (r54.A03) {
                    r113.A03(r45);
                    return;
                }
                C128075vW r114 = r54.A00;
                if (!r54.A02) {
                    r54.A02 = true;
                    r54.A00 = r114;
                    r22.A0D.A06("sendAddCard");
                    C12960it.A1E(r22, r22.A0I);
                    return;
                }
                return;
            case 9:
                Object obj4 = map.get("phone_number");
                String str11 = "";
                if (obj4 != null) {
                    str11 = obj4.toString();
                }
                startActivity(new Intent("android.intent.action.DIAL", Uri.fromParts("tel", str11.replaceAll("[^\\d+]", ""), null)));
                return;
            case 10:
                String A0t10 = C12970iu.A0t("credential_id", map);
                AnonymousClass009.A04(A0t10);
                C128245vn r103 = new C128245vn(this, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A07, this.A03, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, new C128565wJ(r36), this.A0T, A0t10);
                Log.i("PAY: BrazilMerchantLinkAction linkMerchant");
                String str12 = r103.A09;
                ArrayList A0l3 = C12960it.A0l();
                C117295Zj.A1M("action", "br-link-merchant", A0l3);
                if (!TextUtils.isEmpty(str12)) {
                    C117295Zj.A1M("credential-id", str12, A0l3);
                    C117295Zj.A1M("device-id", r103.A08.A01(), A0l3);
                    C117305Zk.A1J(r103.A05, new IDxRCallbackShape2S0100000_3_I1(r103.A00, r103.A01, r103.A04, r103, 3), C117295Zj.A0K(A0l3));
                    return;
                }
                throw C12960it.A0U("Credential id missing");
            case 11:
                String A0t11 = C12970iu.A0t("verify_type", map);
                AnonymousClass009.A04(A0t11);
                String A0t12 = C12970iu.A0t("verify_id", map);
                AnonymousClass009.A04(A0t12);
                String str13 = "BANK";
                if (A0t11.equals(str13)) {
                    str3 = C12970iu.A0t("bank_code", map);
                    AnonymousClass009.A04(str3);
                    str4 = C12970iu.A0t("branch_name", map);
                    AnonymousClass009.A04(str4);
                    str5 = C12970iu.A0t("bank_account_type", map);
                    AnonymousClass009.A04(str5);
                    str6 = C12970iu.A0t("bank_account_number", map);
                    AnonymousClass009.A04(str6);
                    str7 = null;
                } else if (A0t11.equals("PREPAID")) {
                    str7 = C12970iu.A0t("serial_number", map);
                    AnonymousClass009.A04(str7);
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    str6 = null;
                } else {
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    str6 = null;
                    str7 = null;
                }
                C14830m7 r153 = ((ActivityC13790kL) this).A05;
                C14900mE r143 = ((ActivityC13810kN) this).A05;
                C15570nT r133 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r123 = ((ActivityC13830kP) this).A05;
                AnonymousClass5g0 r23 = new AnonymousClass5g0(this, r143, r133, ((ActivityC13810kN) this).A07, r153, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0C, ((AbstractActivityC121705jc) this).A0D, new C128575wK(r36), this.A0T, this.A0U, this.A0V, r123, A0t11, A0t12, str3, str4, str5, str6, str7);
                StringBuilder A0k = C12960it.A0k("PAY: BrazilMerchantLinkAction preLinkMerchant: ");
                String str14 = r23.A0G;
                Log.i(C12960it.A0d(str14, A0k));
                int hashCode = str14.hashCode();
                if (hashCode != -1136784465) {
                    if (hashCode != 2031164) {
                        if (hashCode == 399611855) {
                            str13 = "PREPAID";
                        }
                    }
                    if (str14.equals(str13)) {
                        C12990iw.A1N(r23, r23.A09);
                        return;
                    }
                }
                throw C12970iu.A0f(C12960it.A0d(str14, C12960it.A0k("Invalid Verify Type: ")));
            case 12:
                String A0t13 = C12970iu.A0t("credential_id", map);
                AnonymousClass009.A04(A0t13);
                C17070qD r115 = ((AbstractActivityC121705jc) this).A0G;
                r115.A03();
                AbstractC28901Pl A082 = r115.A09.A08(A0t13);
                AnonymousClass009.A05(A082);
                new C129725yC(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13810kN) this).A07, ((ActivityC13790kL) this).A05, this.A03, this.A06, this.A0A, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, new AnonymousClass6Lp(r36, this) { // from class: X.6AE
                    public final /* synthetic */ AnonymousClass3FE A00;
                    public final /* synthetic */ BrazilPayBloksActivity A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6Lp
                    public final void AP5(C30881Ze r63, C452120p r73, ArrayList arrayList) {
                        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
                        AnonymousClass3FE r32 = this.A00;
                        HashMap A112 = C12970iu.A11();
                        if (r73 != null) {
                            C117295Zj.A1D(r73, A112);
                            r32.A01("on_failure", A112);
                        } else if (r63 == null || arrayList == null || arrayList.isEmpty()) {
                            AbstractActivityC121705jc.A0l(r32, null, -233);
                        } else {
                            ((AbstractActivityC121705jc) brazilPayBloksActivity).A0G.A00().A03(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0028: INVOKE  
                                  (wrap: X.1nR : 0x001f: INVOKE  (r1v1 X.1nR A[REMOVE]) = 
                                  (wrap: X.0qD : 0x001d: IGET  (r0v2 X.0qD A[REMOVE]) = (wrap: ?? : ?: CAST (X.5jc) (r4v0 'brazilPayBloksActivity' com.whatsapp.payments.ui.BrazilPayBloksActivity)) X.5jc.A0G X.0qD)
                                 type: VIRTUAL call: X.0qD.A00():X.1nR)
                                  (wrap: X.684 : 0x0025: CONSTRUCTOR  (r0v3 X.684 A[REMOVE]) = 
                                  (r3v0 'r32' X.3FE)
                                  (r4v0 'brazilPayBloksActivity' com.whatsapp.payments.ui.BrazilPayBloksActivity)
                                  (r8v0 'arrayList' java.util.ArrayList)
                                  (r2v0 'A112' java.util.HashMap)
                                 call: X.684.<init>(X.3FE, com.whatsapp.payments.ui.BrazilPayBloksActivity, java.util.ArrayList, java.util.HashMap):void type: CONSTRUCTOR)
                                  (r6v0 'r63' X.1Ze)
                                 type: VIRTUAL call: X.1nR.A03(X.20l, X.1Pl):void in method: X.6AE.AP5(X.1Ze, X.20p, java.util.ArrayList):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                                	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0025: CONSTRUCTOR  (r0v3 X.684 A[REMOVE]) = 
                                  (r3v0 'r32' X.3FE)
                                  (r4v0 'brazilPayBloksActivity' com.whatsapp.payments.ui.BrazilPayBloksActivity)
                                  (r8v0 'arrayList' java.util.ArrayList)
                                  (r2v0 'A112' java.util.HashMap)
                                 call: X.684.<init>(X.3FE, com.whatsapp.payments.ui.BrazilPayBloksActivity, java.util.ArrayList, java.util.HashMap):void type: CONSTRUCTOR in method: X.6AE.AP5(X.1Ze, X.20p, java.util.ArrayList):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 23 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.684, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 29 more
                                */
                            /*
                                this = this;
                                com.whatsapp.payments.ui.BrazilPayBloksActivity r4 = r5.A01
                                X.3FE r3 = r5.A00
                                java.util.HashMap r2 = X.C12970iu.A11()
                                if (r7 == 0) goto L_0x0013
                                X.C117295Zj.A1D(r7, r2)
                                java.lang.String r0 = "on_failure"
                                r3.A01(r0, r2)
                                return
                            L_0x0013:
                                if (r6 == 0) goto L_0x002c
                                if (r8 == 0) goto L_0x002c
                                boolean r0 = r8.isEmpty()
                                if (r0 != 0) goto L_0x002c
                                X.0qD r0 = r4.A0G
                                X.1nR r1 = r0.A00()
                                X.684 r0 = new X.684
                                r0.<init>(r3, r4, r8, r2)
                                r1.A03(r0, r6)
                                return
                            L_0x002c:
                                r1 = 0
                                r0 = -233(0xffffffffffffff17, float:NaN)
                                X.AbstractActivityC121705jc.A0l(r3, r1, r0)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AE.AP5(X.1Ze, X.20p, java.util.ArrayList):void");
                        }
                    }, ((AbstractActivityC121705jc) this).A0M).A00((C30881Ze) A082);
                    return;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    Log.i("Java received startAddCredential call");
                    String A0t14 = C12970iu.A0t("credential_push_data", map);
                    String replaceAll = C117305Zk.A0h(((ActivityC13790kL) this).A01).replaceAll("[^\\d]", "");
                    if (((ActivityC13810kN) this).A0C.A07(1482)) {
                        bool = Boolean.valueOf(!this.A0L.A03.A0E("add_card"));
                    } else {
                        bool = null;
                    }
                    C129335xY r144 = new C129335xY(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13810kN) this).A07, ((ActivityC13790kL) this).A05, this.A03, this.A06, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, ((AbstractActivityC121705jc) this).A0J, this.A0J);
                    String A016 = this.A0T.A01();
                    C126935tg r46 = new C126935tg(r36, this);
                    AnonymousClass60T r85 = r144.A0A;
                    AnonymousClass6B7 A017 = r85.A01("FB", "KYC", true);
                    if (A017 == null || !A017.A05.equalsIgnoreCase("FB")) {
                        new C129215xM(r144.A00, r144.A01, r144.A07, r144.A08, r85, "KYC").A00(new C133476Ax(r144, r46, bool, A0t14, replaceAll, A016), "FB");
                        return;
                    } else {
                        r144.A00(A017, r46, bool, A0t14, replaceAll, A016);
                        return;
                    }
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    i3 = R.string.brazil_payments_confirm_cancel_add_card_dialog_title;
                    C36021jC.A01(this, i3);
                    return;
                case 15:
                    String A0t15 = C12970iu.A0t("credential_id", map);
                    C14830m7 r145 = ((ActivityC13790kL) this).A05;
                    new C129355xa(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13810kN) this).A07, r145, this.A03, this.A06, this.A0A, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, ((AbstractActivityC121705jc) this).A0M, this.A0T, A0t15).A00(new AnonymousClass6AK(r36, this, A0t15));
                    return;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    String A0X = C117295Zj.A0X("document_type", map);
                    String A0X2 = C117295Zj.A0X("doc_file_name_list", map);
                    ArrayList A0l4 = C12960it.A0l();
                    try {
                        JSONObject A052 = C13000ix.A05(A0X2);
                        File A00 = this.A02.A00(A052.getString("front"));
                        File A002 = this.A02.A00(A052.getString("back"));
                        if (A00 == null || A002 == null || !A00.exists() || !A002.exists()) {
                            Log.e("PAY: BrazilPayBloksActivity performDocumentUpload file not found");
                            r36.A00("on_failure");
                            return;
                        }
                        A0l4.add(A00);
                        A0l4.add(A002);
                        if (A0l4.size() == 0) {
                            Log.e("PAY: BrazilPayBloksActivity performDocumentUpload no file to upload");
                            r36.A00("on_failure");
                            return;
                        }
                        A2q(r36, A0X, A0l4, C12960it.A0l(), 0);
                        return;
                    } catch (JSONException e10) {
                        Log.e("PAY: BrazilPayBloksActivity performDocumentUpload threw exception ", e10);
                        r36.A00("on_failure");
                        return;
                    }
                case 17:
                    C36021jC.A00(this, R.string.brazil_payments_confirm_cancel_add_card_dialog_title);
                    return;
                case 18:
                    String A0X3 = C117295Zj.A0X("credential_id", map);
                    ArrayList A0l5 = C12960it.A0l();
                    C12970iu.A1T("creditCardNumber", C117295Zj.A0X("card_num", map).replace("\\s", ""), A0l5);
                    C129365xb r146 = new C129365xb(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13810kN) this).A07, ((ActivityC13790kL) this).A05, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0H, ((AbstractActivityC121705jc) this).A0J, this.A0P, this.A0T, A0X3, C12970iu.A0t("pin", map), C12970iu.A0t("provider", map), A0l5);
                    C128925wt r32 = new C128925wt(r36, this, A0X3);
                    String str15 = r146.A0C;
                    if (!TextUtils.isEmpty(str15)) {
                        String str16 = r146.A0D;
                        AnonymousClass009.A05(str16);
                        AnonymousClass6B7 A0C = C117315Zl.A0C(r146.A07, str16, "PIN");
                        if (A0C == null) {
                            r146.A06.A00(new C133376An(r146, r32), str16);
                            return;
                        } else {
                            r146.A00(new C128545wH(A0C), r32, str15);
                            return;
                        }
                    } else {
                        C129135xE r116 = r146.A05;
                        List list4 = r146.A0E;
                        C12960it.A1E(new C120305fx(r116.A00, r116.A01, r116.A02, null, new C1330369f(r146, r32), r116.A03, r116.A04, list4, -1), r116.A05);
                        return;
                    }
                case 19:
                    String replaceAll2 = C12970iu.A0t("card_number", map).replaceAll("\\s", "");
                    HashMap A112 = C12970iu.A11();
                    if (replaceAll2.length() < 6) {
                        A112.put("network_name", "unknown");
                    } else {
                        String substring = replaceAll2.substring(0, 6);
                        C129385xd r47 = this.A0G;
                        if (!r47.A03) {
                            r47.A03 = true;
                            C128165vf r104 = new C128165vf(this, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05, this.A06, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0J, ((AbstractActivityC121705jc) this).A0M, substring);
                            C128935wu r63 = new C128935wu(r36, this, A112);
                            String A003 = r104.A07.A00(5);
                            String A0j2 = C117305Zk.A0j(r104.A02, r104.A03);
                            r1 = r104.A04;
                            str2 = r1.A01();
                            r5 = new C126235sY(new C126245sZ(A003, A0j2), new AnonymousClass3CS(str2), r104.A08).A00;
                            iDxRCallbackShape0S0200000_3_I1 = new IDxRCallbackShape0S0200000_3_I1(r104.A00, r104.A01, r104.A05, r63, r104, 1);
                            i = 204;
                            j = C26061Bw.A0L;
                            r1.A09(iDxRCallbackShape0S0200000_3_I1, r5, str2, i, j);
                            return;
                        }
                    }
                    r36.A01("on_success", A112);
                    return;
                case C43951xu.A01:
                    String A0t16 = C12970iu.A0t("tax_id", map);
                    AnonymousClass009.A04(A0t16);
                    C128115va r86 = new C128115va(this, ((ActivityC13810kN) this).A03, ((ActivityC13810kN) this).A05, this.A06, ((AbstractActivityC121705jc) this).A0A, new C125785ro(r36), this.A0T, A0t16.replaceAll("[^\\d]", ""));
                    C17220qS r48 = r86.A03;
                    String A018 = r48.A01();
                    C126275sc r147 = new C126275sc(new AnonymousClass3CS(A018), r86.A06.A01(), r86.A07);
                    r48.A09(new C120205fn(r86.A00, r86.A02, r86.A04, r86, r147, C12970iu.A11()), r147.A00, A018, 204, C26061Bw.A0L);
                    return;
                case 21:
                    Intent A0D = C12990iw.A0D(this, PaymentContactPicker.class);
                    A0D.putExtra("for_payments", true);
                    startActivity(A0D);
                    return;
                case 22:
                    C14830m7 r117 = ((ActivityC13790kL) this).A05;
                    C14900mE r118 = ((ActivityC13810kN) this).A05;
                    C15570nT r154 = ((ActivityC13790kL) this).A01;
                    AbstractC14440lR r148 = ((ActivityC13830kP) this).A05;
                    AnonymousClass1BY r134 = this.A0U;
                    C18590sh r124 = this.A0T;
                    C120315fy r24 = new C120315fy(this, r118, r154, ((ActivityC13810kN) this).A07, r117, this.A03, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0C, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0G, this.A0E, this.A0F, new C128755wc(r36, this), r124, r134, this.A0V, r148);
                    Log.i("PAY: BrazilMerchantRegAction regMerchant");
                    C12960it.A1E(r24, r24.A0D);
                    return;
                case 23:
                    String A0t17 = C12970iu.A0t("error_code", map);
                    if (!TextUtils.isEmpty(A0t17)) {
                        i2 = Integer.parseInt(A0t17);
                        if (AHz(i2)) {
                            return;
                        }
                    } else {
                        i2 = -1;
                    }
                    this.A09.A01(this, ((ActivityC13810kN) this).A0C, ((AbstractActivityC121705jc) this).A07, i2, R.string.payments_generic_error).show();
                    return;
                case 24:
                    String A0t18 = C12970iu.A0t("bank_account_number", map);
                    AnonymousClass009.A04(A0t18);
                    String A0t19 = C12970iu.A0t("bank_code", map);
                    AnonymousClass009.A04(A0t19);
                    String A0t20 = C12970iu.A0t("bank_branch_number", map);
                    AnonymousClass009.A04(A0t20);
                    String A0t21 = C12970iu.A0t("bank_account_type", map);
                    AnonymousClass009.A04(A0t21);
                    String A0U2 = C117295Zj.A0U(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05);
                    C14900mE r135 = ((ActivityC13810kN) this).A05;
                    AbstractC14440lR r125 = ((ActivityC13830kP) this).A05;
                    AnonymousClass1BY r11 = this.A0U;
                    C18590sh r105 = this.A0T;
                    C17070qD r93 = ((AbstractActivityC121705jc) this).A0G;
                    C120325fz r149 = new C120325fz(this, r135, ((ActivityC13810kN) this).A07, this.A03, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0C, ((AbstractActivityC121705jc) this).A0D, r93, new C128585wL(r36), r105, r11, this.A0V, r125, A0t18, A0t19, A0t20, A0U2, Integer.parseInt(A0t21));
                    Log.i("PAY: BrazilUpdateMerchantAccountAction updateMerchant");
                    C12960it.A1E(r149, r149.A0A);
                    return;
                case 25:
                    C12960it.A0t(((ActivityC13810kN) this).A09.A00.edit(), "payments_onboarding_banner_registration_started", true);
                    return;
                case 26:
                    C128065vV r87 = new C128065vV(this, ((ActivityC13810kN) this).A05, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, ((AbstractActivityC121705jc) this).A0J, this.A0T);
                    C133276Ad r73 = new C133276Ad(r36, this);
                    C18610sj r25 = r87.A03;
                    AnonymousClass1W9[] r119 = new AnonymousClass1W9[2];
                    C117305Zk.A1O("action", "get-kyc-state", r119);
                    C12960it.A1M("provider", "FB", r119, 1);
                    C117305Zk.A1I(r25, new C120245fr(r87.A00, r87.A01, r87.A02, r73, r87, "get-kyc-state", "FB"), C117315Zl.A0G(r119));
                    return;
                case 27:
                    String A0t22 = C12970iu.A0t("card_verify_type", map);
                    AnonymousClass009.A04(A0t22);
                    if (A0t22.equals("app-to-app")) {
                        this.A01 = r36;
                        String A0t23 = C12970iu.A0t("credential_id", map);
                        AnonymousClass009.A04(A0t23);
                        this.A0N = A0t23;
                        String A0t24 = C12970iu.A0t("app_to_app_partner_app_package", map);
                        String A0t25 = C12970iu.A0t("app_to_app_partner_intent_action", map);
                        String A0t26 = C12970iu.A0t("app_to_app_request_payload", map);
                        AnonymousClass01T A004 = C130335zF.A00(A0t24, A0t25);
                        if (TextUtils.isEmpty(A0t24) || TextUtils.isEmpty(A0t26) || A004 == null) {
                            r36.A01("on_failure", C12970iu.A11());
                            return;
                        }
                        String str17 = (String) A004.A00;
                        String str18 = (String) A004.A01;
                        Intent A0A = C12970iu.A0A();
                        A0A.putExtra("android.intent.extra.TEXT", A0t26);
                        A0A.setPackage(str17);
                        A0A.setAction(str18);
                        StringBuilder A0k2 = C12960it.A0k("PAY: BrazilPayBloksActivity appToApp package: ");
                        A0k2.append(str17);
                        A0k2.append(" action ");
                        Log.i(C12960it.A0d(str18, A0k2));
                        if (A0A.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(A0A, 100);
                            return;
                        }
                        return;
                    } else if (A0t22.equals("otp")) {
                        String A0t27 = C12970iu.A0t("credential_id", map);
                        AnonymousClass009.A04(A0t27);
                        String A0t28 = C12970iu.A0t("card_verify_identifier", map);
                        AnonymousClass009.A04(A0t28);
                        String A0U3 = C117295Zj.A0U(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05);
                        String A0t29 = C12970iu.A0t("card_verify_otp_resend_interval_sec", map);
                        C128265vp r106 = new C128265vp(this, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A07, this.A03, this.A06, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, this.A0G, A0U3, A0t27, A0t28);
                        C128905wr r33 = new C128905wr(r36, this, A0t29);
                        r1 = r106.A04;
                        str2 = r1.A01();
                        String str19 = r106.A08;
                        String str20 = r106.A09;
                        String str21 = r106.A0A;
                        C129385xd r55 = r106.A07;
                        C30881Ze r26 = (C30881Ze) r55.A09.A08(str19);
                        if (r26 == null || r26.A01 != 5) {
                            r52 = null;
                        } else {
                            r52 = new C126315sg(r55.A0H.A00(5));
                        }
                        r5 = new C126305sf(r52, new AnonymousClass3CT(str2), str19, str20, str21).A00;
                        iDxRCallbackShape0S0200000_3_I1 = new IDxRCallbackShape0S0200000_3_I1(r106.A00, r106.A01, r106.A05, r33, r106, 6);
                        j = C26061Bw.A0L;
                        i = 204;
                        r1.A09(iDxRCallbackShape0S0200000_3_I1, r5, str2, i, j);
                        return;
                    } else {
                        return;
                    }
                default:
                    super.AZC(r36, str, map);
                    return;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:132:0x028e A[RETURN] */
        @Override // X.AbstractActivityC121705jc, X.AbstractC136496Mt
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String AZE(java.lang.String r12, java.util.Map r13) {
            /*
            // Method dump skipped, instructions count: 840
            */
            throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.BrazilPayBloksActivity.AZE(java.lang.String, java.util.Map):java.lang.String");
        }

        @Override // X.AbstractActivityC121705jc, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
        public void onActivityResult(int i, int i2, Intent intent) {
            AnonymousClass3FE r1;
            String str;
            C119775f5 r12;
            super.onActivityResult(i, i2, intent);
            if (i == 100 && (r1 = this.A01) != null) {
                if (i2 == -1) {
                    if (intent != null) {
                        Bundle extras = intent.getExtras();
                        String string = extras.getString("STEP_UP_RESPONSE", null);
                        if (string == null && (string = extras.getString("issuerMobileAppAuthResponse", null)) == null) {
                            Log.e("PAY: BrazilPayBloksActivity onActivityResult - response is NULL");
                            return;
                        }
                        String lowerCase = string.toLowerCase(Locale.US);
                        switch (lowerCase.hashCode()) {
                            case -1086574198:
                                str = "failure";
                                break;
                            case 568196142:
                                str = "declined";
                                break;
                            case 1185244855:
                                if (lowerCase.equals("approved")) {
                                    String str2 = "STEP_UP_AUTH_CODE";
                                    if (!intent.hasExtra(str2)) {
                                        str2 = "TAV";
                                    }
                                    String stringExtra = intent.getStringExtra(str2);
                                    if (TextUtils.isEmpty(stringExtra)) {
                                        r1 = this.A01;
                                        break;
                                    } else {
                                        String A0U = C117295Zj.A0U(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05);
                                        C14830m7 r0 = ((ActivityC13790kL) this).A05;
                                        C14900mE r15 = ((ActivityC13810kN) this).A05;
                                        C15570nT r122 = ((ActivityC13790kL) this).A01;
                                        C241414j r11 = this.A05;
                                        C17220qS r10 = this.A06;
                                        C17070qD r9 = ((AbstractActivityC121705jc) this).A0G;
                                        C129385xd r8 = this.A0G;
                                        C120355g3 r02 = new C120355g3(this, r15, r122, r0, this.A03, r11, r10, this.A0A, ((AbstractActivityC121705jc) this).A0A, ((AbstractActivityC121705jc) this).A0D, r9, ((AbstractActivityC121705jc) this).A0J, r8, new C128765wd(this, stringExtra), stringExtra, A0U, this.A0N);
                                        AbstractC28901Pl A08 = ((AbstractC129955yZ) r02).A04.A08(r02.A06);
                                        if (A08 == null || (r12 = (C119775f5) A08.A08) == null || !"VISA".equals(r12.A03)) {
                                            Log.i("PAY: BrazilVerifyCardSendAuthCodeAction sendRequestCardVerification without encrypted value");
                                            r02.A03(r02.A05);
                                            return;
                                        }
                                        Log.i("PAY: BrazilVerifyCardSendAuthCodeAction sendRequestCardVerification with encrypted value");
                                        r02.A00();
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            default:
                                return;
                        }
                        if (lowerCase.equals(str)) {
                            r1 = this.A01;
                        } else {
                            return;
                        }
                    }
                    r1.A00("on_success");
                    return;
                }
                AbstractActivityC121705jc.A0l(r1, null, -232);
            }
        }

        @Override // X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            C125065qd.A00(this);
            if (getIntent().getIntExtra("extra_setup_mode", 0) != 0) {
                getIntent().putExtra("screen_name", this.A0L.A01());
            }
            String stringExtra = getIntent().getStringExtra("screen_name");
            AnonymousClass009.A04(stringExtra);
            if ("brpay_p_tos".equalsIgnoreCase(stringExtra) || "brpay_p_compliance_kyc_next_screen_router".equalsIgnoreCase(stringExtra)) {
                C93954b1 r6 = this.A0H.A00;
                Intent intent = getIntent();
                if (intent == null || !intent.hasExtra("perf_origin")) {
                    r6.A01.A0D("unknown", -1);
                } else {
                    long longExtra = intent.getLongExtra("perf_start_time_ns", -1);
                    String stringExtra2 = intent.getStringExtra("perf_origin");
                    if (stringExtra2 != null) {
                        r6.A01.A0D(stringExtra2, longExtra);
                    }
                }
            }
            this.A00.A00 = this.A0K.A04;
            A2i();
            Toolbar A08 = C117305Zk.A08(this);
            if (A08 != null) {
                A08.setLogo((Drawable) null);
                A08.setTitle((CharSequence) null);
            }
        }

        @Override // android.app.Activity
        public Dialog onCreateDialog(int i) {
            if (i == R.string.brazil_payments_confirm_cancel_add_card_dialog_title) {
                C004802e A0S = C12980iv.A0S(this);
                A0S.A07(R.string.brazil_payments_confirm_cancel_add_card_dialog_title);
                A0S.A06(R.string.brazil_payments_confirm_cancel_add_card_dialog_message);
                C117295Zj.A0q(A0S, this, 8, R.string.brazil_payments_confirm_cancel_add_card_dialog_positive_btn);
                A0S.setNegativeButton(R.string.brazil_payments_confirm_cancel_add_card_dialog_negative_btn, null);
                return A0S.create();
            }
            Dialog A01 = this.A0M.A01(null, this, i);
            if (A01 == null) {
                return super.onCreateDialog(i);
            }
            return A01;
        }

        @Override // X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
        public void onDestroy() {
            super.onDestroy();
            C129385xd r1 = this.A0G;
            r1.A00 = null;
            r1.A03 = false;
            r1.A02 = false;
        }
    }
