package com.whatsapp.payments.ui;

import X.AnonymousClass028;
import X.C117295Zj;
import X.C126055sG;
import X.C12960it;
import X.C14850m9;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviGetStartedFragment extends Hilt_NoviGetStartedFragment {
    public C14850m9 A00;
    public C126055sG A01;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_education_fragment);
        C117295Zj.A0n(AnonymousClass028.A0D(A0F, R.id.send_money_review_header_close), this, 82);
        TextView A0I = C12960it.A0I(A0F, R.id.novi_education_description);
        boolean A07 = this.A00.A07(1230);
        int i = R.string.novi_get_started_description_without_cross_country;
        if (A07) {
            i = R.string.novi_get_started_description;
        }
        A0I.setText(i);
        TextView A0I2 = C12960it.A0I(A0F, R.id.novi_education_action_button);
        A0I2.setText(R.string.novi_get_started_label);
        C117295Zj.A0n(A0I2, this, 83);
        return A0F;
    }
}
