package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass18O;
import X.AnonymousClass1ZR;
import X.AnonymousClass2FL;
import X.AnonymousClass2SO;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117955b2;
import X.C120415g9;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C1321565o;
import X.C1329668y;
import X.C14900mE;
import X.C18590sh;
import X.C21270x9;
import X.C30931Zj;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiProfileDetailsActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class IndiaUpiProfileDetailsActivity extends AbstractActivityC121665jA {
    public ImageView A00;
    public LinearLayout A01;
    public LinearLayout A02;
    public LinearLayout A03;
    public TextView A04;
    public TextView A05;
    public TextView A06;
    public TextView A07;
    public ConstraintLayout A08;
    public C21270x9 A09;
    public AnonymousClass1ZR A0A;
    public C120415g9 A0B;
    public C117955b2 A0C;
    public AnonymousClass18O A0D;
    public C18590sh A0E;
    public String A0F;
    public boolean A0G;
    public final C30931Zj A0H;

    public IndiaUpiProfileDetailsActivity() {
        this(0);
        this.A0H = C117315Zl.A0D("IndiaUpiProfileDetailsActivity");
    }

    public IndiaUpiProfileDetailsActivity(int i) {
        this.A0G = false;
        C117295Zj.A0p(this, 66);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0G) {
            this.A0G = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A09 = C12970iu.A0W(A1M);
            this.A0E = C117315Zl.A0F(A1M);
            this.A0D = (AnonymousClass18O) A1M.A9Y.get();
        }
    }

    public void A30(boolean z) {
        int i;
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        if (z) {
            this.A08.setVisibility(0);
            this.A01.setVisibility(8);
            linearLayout = this.A03;
        } else {
            ArrayList A0x = C12980iv.A0x(this.A0D.A00());
            this.A08.setVisibility(8);
            if (A0x.size() == 0) {
                this.A01.setVisibility(8);
                linearLayout2 = this.A03;
            } else {
                AnonymousClass2SO r2 = (AnonymousClass2SO) A0x.get(0);
                this.A03.setVisibility(8);
                this.A01.setVisibility(0);
                C117315Zl.A0N(this.A05, r2.A00.A00);
                TextView textView = this.A04;
                String str = r2.A02;
                boolean equals = str.equals("active_pending");
                if (equals) {
                    i = R.string.linked_upi_number_subtext_if_registration_pending;
                } else {
                    boolean equals2 = str.equals("deregistered_pending");
                    i = R.string.linked_upi_number_subtext;
                    if (equals2) {
                        i = R.string.linked_upi_number_subtext_if_deregistration_pending;
                    }
                }
                textView.setText(i);
                if (equals || str.equals("deregistered_pending")) {
                    this.A00.setImageResource(R.drawable.ic_auto_pay);
                    linearLayout = this.A02;
                } else {
                    this.A00.setImageResource(R.drawable.ic_settings_phone);
                    linearLayout2 = this.A02;
                }
            }
            linearLayout2.setVisibility(0);
            return;
        }
        linearLayout.setVisibility(8);
    }

    public final boolean A31(int i) {
        if (!((AbstractActivityC121665jA) this).A0B.A0L()) {
            return true;
        }
        C1329668y r1 = ((AbstractActivityC121665jA) this).A0B;
        if (r1.A0N(r1.A07())) {
            return true;
        }
        Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class);
        A0D.putExtra("extra_setup_mode", 2);
        A0D.putExtra("extra_payments_entry_type", i);
        A0D.putExtra("extra_skip_value_props_display", false);
        A0D.putExtra("extra_referral_screen", "payments_profile");
        A0D.putExtra("extra_payment_name", this.A0A);
        A2v(A0D);
        startActivity(A0D);
        return false;
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        setContentView(R.layout.india_upi_profile_page);
        this.A0A = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payment_name");
        this.A0F = AbstractActivityC119235dO.A0N(this);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.upi_profile_title);
        }
        this.A0H.A06("onCreate");
        C14900mE r6 = ((ActivityC13810kN) this).A05;
        C18590sh r11 = this.A0E;
        this.A0B = new C120415g9(this, r6, ((AbstractActivityC121665jA) this).A0A, ((AbstractActivityC121685jC) this).A0K, ((AbstractActivityC121685jC) this).A0M, ((AbstractActivityC121665jA) this).A0D, r11);
        TextView A0M = C12970iu.A0M(this, R.id.profile_name);
        this.A07 = A0M;
        C117315Zl.A0N(A0M, C117295Zj.A0R(this.A0A));
        TextView A0M2 = C12970iu.A0M(this, R.id.profile_vpa);
        this.A06 = A0M2;
        C117315Zl.A0N(A0M2, ((AbstractActivityC121665jA) this).A0B.A04().A00);
        this.A03 = (LinearLayout) findViewById(R.id.set_up_upi_number_container);
        this.A05 = C12970iu.A0M(this, R.id.upi_number_text);
        this.A04 = C12970iu.A0M(this, R.id.upi_number_subtext);
        this.A00 = C117305Zk.A06(this, R.id.linked_number_image);
        this.A01 = (LinearLayout) findViewById(R.id.linked_upi_number_container);
        this.A02 = (LinearLayout) findViewById(R.id.remove_upi_number_container);
        this.A08 = (ConstraintLayout) findViewById(R.id.shimmer_layout_row);
        C117955b2 r0 = (C117955b2) C117315Zl.A06(new C1321565o(this), this).A00(C117955b2.class);
        this.A0C = r0;
        C117295Zj.A0r(this, r0.A02, 50);
        C117295Zj.A0r(this, this.A0C.A01, 49);
        C117295Zj.A0n(this.A02, this, 63);
        C117295Zj.A0n(this.A03, this, 64);
        A30(false);
        ((AbstractActivityC121665jA) this).A0D.AKg(0, null, "payments_profile", this.A0F);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        if (i == 28) {
            r2 = C12980iv.A0S(this);
            r2.A06(R.string.payments_generic_error);
            C117295Zj.A0q(r2, this, 51, R.string.ok);
        } else if (i != 38) {
            return super.onCreateDialog(i);
        } else {
            ((AbstractActivityC121665jA) this).A0D.AKg(C12980iv.A0i(), null, "alias_remove_confirm_dialog", "payments_profile");
            r2 = C12980iv.A0S(this);
            r2.A07(R.string.upi_number_deletion_dialog_title);
            r2.A06(R.string.upi_number_deletion_dialog_text);
            r2.setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() { // from class: X.62Y
                public final /* synthetic */ int A00 = 38;

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    IndiaUpiProfileDetailsActivity indiaUpiProfileDetailsActivity = IndiaUpiProfileDetailsActivity.this;
                    int i3 = this.A00;
                    ((AbstractActivityC121665jA) indiaUpiProfileDetailsActivity).A0D.AKg(C12960it.A0V(), C12980iv.A0k(), "alias_remove_confirm_dialog", "payments_profile");
                    C36021jC.A00(indiaUpiProfileDetailsActivity, i3);
                    indiaUpiProfileDetailsActivity.A2q();
                    if (indiaUpiProfileDetailsActivity.A0D.A00().size() > 0) {
                        C117955b2 r12 = indiaUpiProfileDetailsActivity.A0C;
                        C120415g9 r11 = indiaUpiProfileDetailsActivity.A0B;
                        AnonymousClass2SO r8 = (AnonymousClass2SO) indiaUpiProfileDetailsActivity.A0D.A00().iterator().next();
                        AnonymousClass1ZR A04 = ((AbstractActivityC121665jA) indiaUpiProfileDetailsActivity).A0B.A04();
                        String A0B = ((AbstractActivityC121665jA) indiaUpiProfileDetailsActivity).A0B.A0B();
                        C117315Zl.A0Q(r12.A02);
                        Log.i("PAY: deregisterAlias called");
                        ArrayList A0l = C12960it.A0l();
                        C117295Zj.A1M("alias_id", r8.A01, A0l);
                        C117295Zj.A1M("alias_value", (String) r8.A00.A00, A0l);
                        C117295Zj.A1M("alias_type", r8.A03, A0l);
                        if (!TextUtils.isEmpty(A0B)) {
                            C117295Zj.A1M("vpa_id", A0B, A0l);
                        }
                        C117295Zj.A1M("vpa", (String) A04.A00, A0l);
                        ArrayList A0l2 = C12960it.A0l();
                        C117295Zj.A1M("action", "deregister-alias", A0l2);
                        C117295Zj.A1M("device_id", r11.A05.A01(), A0l2);
                        C64513Fv r10 = ((C126705tJ) r11).A00;
                        if (r10 != null) {
                            r10.A04("deregister-alias");
                        }
                        C117305Zk.A1H(((C126705tJ) r11).A01, new C120815gn(r11.A00, r11.A01, r8, r11.A02, r10, r11, r12), new AnonymousClass1V8(new AnonymousClass1V8("alias", C117305Zk.A1b(A0l)), "account", C117305Zk.A1b(A0l2)));
                        return;
                    }
                    indiaUpiProfileDetailsActivity.A30(false);
                }
            });
            r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.62Z
                public final /* synthetic */ int A00 = 38;

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    IndiaUpiProfileDetailsActivity indiaUpiProfileDetailsActivity = IndiaUpiProfileDetailsActivity.this;
                    int i3 = this.A00;
                    ((AbstractActivityC121665jA) indiaUpiProfileDetailsActivity).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "alias_remove_confirm_dialog", "payments_profile");
                    C36021jC.A00(indiaUpiProfileDetailsActivity, i3);
                    indiaUpiProfileDetailsActivity.A2q();
                }
            });
        }
        return r2.create();
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        A30(false);
    }
}
