package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractActivityC121465iB;
import X.AbstractC118045bB;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01T;
import X.AnonymousClass03U;
import X.AnonymousClass14X;
import X.AnonymousClass2FL;
import X.AnonymousClass5s9;
import X.AnonymousClass608;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61M;
import X.AnonymousClass6JD;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118235bU;
import X.C122275lC;
import X.C122305lF;
import X.C122365lL;
import X.C122385lN;
import X.C122775m5;
import X.C123395n5;
import X.C125985s8;
import X.C127055ts;
import X.C128365vz;
import X.C12960it;
import X.C129685y8;
import X.C12970iu;
import X.C12980iv;
import X.C129865yQ;
import X.C129895yT;
import X.C12990iw;
import X.C130785zy;
import X.C1315463e;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C17070qD;
import X.C18610sj;
import X.C30861Zc;
import X.C30881Ze;
import X.C43951xu;
import X.C452120p;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity;
import com.whatsapp.payments.ui.NoviPayHubActivity;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviPayHubActivity extends AbstractActivityC121465iB {
    public C15550nR A00;
    public C15610nY A01;
    public C18610sj A02;
    public AnonymousClass61M A03;
    public C17070qD A04;
    public AnonymousClass60Y A05;
    public C129685y8 A06;
    public C123395n5 A07;
    public AnonymousClass14X A08;
    public boolean A09;

    public NoviPayHubActivity() {
        this(0);
    }

    public NoviPayHubActivity(int i) {
        this.A09 = false;
        C117295Zj.A0p(this, 84);
    }

    public static /* synthetic */ void A0D(C130785zy r4, NoviPayHubActivity noviPayHubActivity) {
        if (r4.A06() && r4.A02 != null) {
            noviPayHubActivity.finish();
            HashMap hashMap = new HashMap(10);
            Bundle A0D = C12970iu.A0D();
            Intent A0D2 = C12990iw.A0D(noviPayHubActivity, NoviPayBloksActivity.class);
            A0D.putString("screen_name", "novipay_p_login_password");
            hashMap.put("login_entry_point", "payment_settings_hub_row");
            A0D.putInt("login_entry_point", 1);
            A0D.putSerializable("screen_params", hashMap);
            A0D2.putExtras(A0D);
            noviPayHubActivity.startActivity(A0D2);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A03(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A08 = (AnonymousClass14X) A1M.AFM.get();
            this.A00 = C12960it.A0O(A1M);
            this.A01 = C12960it.A0P(A1M);
            this.A05 = C117305Zk.A0W(A1M);
            this.A04 = C117305Zk.A0P(A1M);
            this.A06 = (C129685y8) A1M.ADa.get();
            this.A02 = C117305Zk.A0M(A1M);
            this.A03 = C117305Zk.A0N(A1M);
        }
    }

    @Override // X.AbstractActivityC121465iB, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1000) {
            return new C122365lL(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_icon_text_row_item));
        }
        if (i == 1007) {
            return new C122775m5(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_divider));
        }
        switch (i) {
            case 1002:
                C14830m7 r5 = ((ActivityC13790kL) this).A05;
                AnonymousClass14X r8 = this.A08;
                AnonymousClass018 r6 = ((ActivityC13830kP) this).A01;
                return new C122275lC(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_hub_expandable_listview), this.A00, this.A01, r5, r6, this.A04, r8);
            case 1003:
                return new C122305lF(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_balance));
            case 1004:
                return new C122385lN(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_payment_methods_list), ((ActivityC13830kP) this).A01);
            default:
                return super.A2e(viewGroup, i);
        }
    }

    @Override // X.AbstractActivityC121465iB
    public void A2g(C127055ts r7) {
        String str;
        Class cls;
        int i;
        AbstractC28901Pl r4;
        Class cls2;
        super.A2g(r7);
        switch (r7.A00) {
            case 100:
                C129895yT A03 = ((AbstractActivityC121465iB) this).A00.A03();
                if (A03 == null || A03.A00()) {
                    str = "withdrawal";
                    A2j(str);
                    return;
                }
                Intent A0D = C12990iw.A0D(this, NoviPayLimitationsBloksActivity.class);
                A0D.putExtra("limitation_origin", 2);
                startActivity(A0D);
                return;
            case 101:
            case 110:
            case 113:
            default:
                return;
            case 102:
                cls = NoviPayHubTransactionHistoryActivity.class;
                startActivity(C12990iw.A0D(this, cls));
                return;
            case 103:
                AnonymousClass5s9 r0 = r7.A01;
                if (r0 != null) {
                    AnonymousClass01T r1 = (AnonymousClass01T) r0.A00;
                    Object obj = r1.A00;
                    if (obj != null) {
                        i = C12960it.A05(obj);
                    } else {
                        i = 0;
                    }
                    Object obj2 = r1.A01;
                    if (obj2 != null) {
                        r4 = (AbstractC28901Pl) obj2;
                    } else {
                        r4 = null;
                    }
                    if (r4 instanceof C30881Ze) {
                        cls2 = NoviPaymentCardDetailsActivity.class;
                    } else if (r4 instanceof C30861Zc) {
                        cls2 = NoviPaymentBankDetailsActivity.class;
                    } else {
                        return;
                    }
                    Intent A0D2 = C12990iw.A0D(this, cls2);
                    A0D2.putExtra("extra_number_of_payment_methods", i);
                    A0D2.putExtra("extra_bank_account", r4);
                    startActivityForResult(A0D2, 1);
                    return;
                }
                return;
            case 104:
                cls = NoviPayHubManageTopUpActivity.class;
                startActivity(C12990iw.A0D(this, cls));
                return;
            case 105:
                C129895yT A032 = ((AbstractActivityC121465iB) this).A00.A03();
                if (A032 == null || A032.A00()) {
                    str = "payment_settings";
                    A2j(str);
                    return;
                }
                Intent A0D = C12990iw.A0D(this, NoviPayLimitationsBloksActivity.class);
                A0D.putExtra("limitation_origin", 2);
                startActivity(A0D);
                return;
            case 106:
                cls = NoviPayHubAccountManagementActivity.class;
                startActivity(C12990iw.A0D(this, cls));
                return;
            case 107:
                cls = NoviPayHubSecurityActivity.class;
                startActivity(C12990iw.A0D(this, cls));
                return;
            case C43951xu.A03:
                ((ActivityC13790kL) this).A00.A06(this, AnonymousClass608.A00(((ActivityC13830kP) this).A01));
                return;
            case 109:
                AnonymousClass009.A04("https://novi.com/legal");
                Intent A04 = C117295Zj.A04("https://novi.com/legal");
                if (A04.resolveActivity(getPackageManager()) != null) {
                    startActivity(A04);
                    return;
                }
                return;
            case 111:
                AnonymousClass5s9 r02 = r7.A01;
                AnonymousClass009.A05(r02);
                C452120p r3 = (C452120p) r02.A00;
                new C129865yQ(((ActivityC13790kL) this).A00, this, this.A03).A02(r3, new Runnable(r3, this) { // from class: X.6II
                    public final /* synthetic */ C452120p A00;
                    public final /* synthetic */ NoviPayHubActivity A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NoviPayHubActivity noviPayHubActivity = this.A01;
                        C452120p r32 = this.A00;
                        AnonymousClass610 A033 = AnonymousClass610.A03("ERROR_CTA_CLICK", "NOVI_HUB", "HOME_TAB");
                        String string = noviPayHubActivity.getString(R.string.close);
                        C128365vz r12 = A033.A00;
                        r12.A0L = string;
                        r12.A0P = r32.A08;
                        r12.A0O = r32.A07;
                        noviPayHubActivity.A05.A05(r12);
                    }
                }, new Runnable(r3, this) { // from class: X.6IH
                    public final /* synthetic */ C452120p A00;
                    public final /* synthetic */ NoviPayHubActivity A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NoviPayHubActivity noviPayHubActivity = this.A01;
                        C452120p r32 = this.A00;
                        AnonymousClass610 A033 = AnonymousClass610.A03("ERROR_CTA_CLICK", "NOVI_HUB", "HOME_TAB");
                        String string = noviPayHubActivity.getString(R.string.payments_try_again);
                        C128365vz r12 = A033.A00;
                        r12.A0L = string;
                        r12.A0P = r32.A08;
                        r12.A0O = r32.A07;
                        noviPayHubActivity.A05.A05(r12);
                        noviPayHubActivity.A07.A09(noviPayHubActivity);
                    }
                });
                return;
            case 112:
                Intent A0D3 = C12990iw.A0D(this, NoviPayBloksActivity.class);
                A0D3.putExtra("screen_name", "novipay_p_report_transaction");
                C117305Zk.A12(A0D3, "claim_edu_origin", "novi_hub", C12970iu.A11());
                startActivityForResult(A0D3, 4);
                return;
            case 114:
                A2f();
                return;
            case 115:
                if (A2h()) {
                    Intent A0D4 = C12990iw.A0D(this, NoviAmountEntryActivity.class);
                    AnonymousClass5s9 r12 = r7.A01;
                    AnonymousClass009.A06(r12, "Event message is null");
                    A0D4.putExtra("account_info", (C1315463e) r12.A00);
                    A0D4.putExtra("amount_entry_type", "deposit");
                    AnonymousClass60Y r32 = this.A05;
                    AnonymousClass610 A033 = AnonymousClass610.A03("ADD_MONEY_CLICK", "NOVI_HUB", "HOME_TAB");
                    String string = getString(R.string.novi_deposit_money);
                    C128365vz r03 = A033.A00;
                    r03.A0L = string;
                    r32.A05(r03);
                    startActivity(A0D4);
                    return;
                }
                return;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2i(android.content.Intent r3) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x0015
            java.lang.String r1 = "action"
            boolean r0 = r3.hasExtra(r1)
            if (r0 == 0) goto L_0x0015
            java.lang.String r1 = r3.getStringExtra(r1)
            int r0 = r1.hashCode()
            switch(r0) {
                case -1675249184: goto L_0x0016;
                case -940242166: goto L_0x0019;
                case 1192345415: goto L_0x0024;
                default: goto L_0x0015;
            }
        L_0x0015:
            return
        L_0x0016:
            java.lang.String r0 = "add_new_debit_card"
            goto L_0x0026
        L_0x0019:
            java.lang.String r0 = "withdraw"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "withdrawal"
            goto L_0x002e
        L_0x0024:
            java.lang.String r0 = "add_new_bank_account"
        L_0x0026:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "payment_settings"
        L_0x002e:
            r2.A2j(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayHubActivity.A2i(android.content.Intent):void");
    }

    public final void A2j(String str) {
        boolean equals = str.equals("withdrawal");
        if (equals) {
            AnonymousClass610 A01 = AnonymousClass610.A01("WITHDRAW_MONEY_CLICK");
            String string = getString(R.string.novi_withdraw_money);
            C128365vz r1 = A01.A00;
            r1.A0L = string;
            this.A05.A05(r1);
        }
        Intent A0D = C12990iw.A0D(this, NoviPayHubAddPaymentMethodActivity.class);
        A0D.putExtra("extra_funding_category", str);
        int i = 2;
        if (equals) {
            i = 3;
        }
        startActivityForResult(A0D, i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        C123395n5 r1;
        C125985s8 r0;
        if (i == 1) {
            if (i2 == 2) {
                r1 = this.A07;
                r0 = new C125985s8(0);
            } else {
                return;
            }
        } else if (i != 2) {
            if (i == 3) {
                if (i2 == -1) {
                    r1 = this.A07;
                    r0 = new C125985s8(2);
                }
            } else if (i == 4 && i2 == -1) {
                C129685y8 r4 = this.A06;
                AnonymousClass016 A0T = C12980iv.A0T();
                r4.A05.Ab2(new AnonymousClass6JD(A0T, r4, 6));
                C117295Zj.A0r(this, A0T, 82);
                return;
            }
            super.onActivityResult(i, i2, intent);
            return;
        } else if (i2 == -1 || (intent != null && intent.getBooleanExtra("handle_add_payment_method_result", false))) {
            r1 = this.A07;
            r0 = new C125985s8(1);
        } else {
            return;
        }
        r1.A06(this, this, r0);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass60Y.A02(this.A05, "BACK_CLICK", "NOVI_HUB", "HOME_TAB", "ARROW");
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C123395n5 r2 = (C123395n5) C117315Zl.A06(new C118235bU(((AbstractActivityC121465iB) this).A01), this).A00(C123395n5.class);
        this.A07 = r2;
        ((AbstractC118045bB) r2).A00.A05(this, C117305Zk.A0B(this, 84));
        C123395n5 r22 = this.A07;
        ((AbstractC118045bB) r22).A01.A05(this, C117305Zk.A0B(this, 83));
        AbstractActivityC119265dR.A0B(this, this.A07);
        A2i(getIntent());
        AnonymousClass60Y.A02(this.A05, "NAVIGATION_START", "NOVI_HUB", "HOME_TAB", "SCREEN");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(this.A05, "NAVIGATION_END", "NOVI_HUB", "HOME_TAB", "SCREEN");
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        A2i(intent);
    }
}
