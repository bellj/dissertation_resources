package com.whatsapp.payments.ui;

import X.AbstractC28901Pl;
import X.AbstractC30791Yv;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass102;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61F;
import X.AnonymousClass63X;
import X.AnonymousClass63Y;
import X.AnonymousClass6F2;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117495a3;
import X.C128365vz;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1311161i;
import X.C1315863i;
import X.C1316263m;
import X.C1316363n;
import X.C1316563p;
import X.C17070qD;
import X.C30861Zc;
import X.C30881Ze;
import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.math.BigDecimal;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class NoviTransactionMethodDetailsFragment extends Hilt_NoviTransactionMethodDetailsFragment {
    public AnonymousClass018 A00;
    public AnonymousClass102 A01;
    public C17070qD A02;
    public C1315863i A03;
    public C1316263m A04;
    public AnonymousClass63X A05;
    public AnonymousClass60Y A06;
    public AnonymousClass61F A07;

    public static CharSequence A00(Context context, AnonymousClass018 r11, AnonymousClass63Y r12, C1315863i r13) {
        AbstractC30791Yv r7 = r12.A02;
        C1316563p r9 = r13.A04;
        if (r9 == null) {
            return "";
        }
        Object[] objArr = new Object[2];
        BigDecimal bigDecimal = BigDecimal.ONE;
        int i = 0;
        objArr[0] = r7.AAB(r11, bigDecimal, 0);
        AbstractC30791Yv r2 = r12.A01;
        BigDecimal bigDecimal2 = r9.A05;
        if (!bigDecimal.equals(bigDecimal2)) {
            i = 4;
        }
        return r7.AA7(context, C12960it.A0X(context, C117305Zk.A0k(r11, r2, bigDecimal2, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate));
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass60Y r6 = this.A06;
        AnonymousClass610 A02 = AnonymousClass610.A02("NAVIGATION_START", "SEND_MONEY");
        C128365vz r4 = A02.A00;
        r4.A0i = "PAYMENT_METHODS";
        A02.A05(this.A03, this.A04, null, this.A05);
        r6.A06(r4);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_send_money_review_method_details);
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        AnonymousClass60Y r2 = this.A06;
        C128365vz r0 = AnonymousClass610.A02("NAVIGATION_END", "SEND_MONEY").A00;
        r0.A0i = "REVIEW_TRANSACTION_DETAILS";
        r2.A06(r0);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        BigDecimal bigDecimal;
        AnonymousClass6F2 r6;
        Bundle A03 = A03();
        this.A04 = (C1316263m) C117315Zl.A03(A03, "arg_novi_balance");
        this.A03 = (C1315863i) C117315Zl.A03(A03, "arg_exchange_quote");
        AnonymousClass009.A05(A03.getParcelable("arg_payment_amount"));
        this.A05 = (AnonymousClass63X) A03.getParcelable("arg_deposit_draft");
        AbstractC30791Yv r62 = (AbstractC30791Yv) C117315Zl.A03(A03, "arg_transaction_currency");
        ArrayList parcelableArrayList = A03.getParcelableArrayList("arg_methods");
        AnonymousClass009.A05(parcelableArrayList);
        View inflate = View.inflate(A0B(), R.layout.novi_send_money_review_details_header, C117315Zl.A04(view, R.id.title_view));
        C12990iw.A1H(C12960it.A0I(inflate, R.id.send_money_review_details_header_title), this, R.string.novi_send_money_review_method_details);
        View A0D = AnonymousClass028.A0D(inflate, R.id.send_money_review_details_header_back);
        A0D.setVisibility(0);
        C117295Zj.A0n(A0D, this, 91);
        View A0D2 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_balance_layout);
        C1316263m r3 = this.A04;
        C12990iw.A1H(C12960it.A0I(A0D2, R.id.novi_send_money_review_transaction_line_item_lhs), this, R.string.novi_send_money_review_method_details_balance_label);
        TextView A0I = C12960it.A0I(A0D2, R.id.novi_send_money_review_transaction_line_item_rhs);
        AnonymousClass6F2 r0 = r3.A02;
        A0I.setText(C117305Zk.A0e(A0p(), this.A00, r0.A00, r0.A01, 0));
        AnonymousClass6F2 r02 = this.A03.A05.A02;
        if (r02 != null) {
            bigDecimal = r02.A01.A00;
        } else {
            bigDecimal = BigDecimal.ZERO;
        }
        if (this.A05 != null) {
            C117295Zj.A0o(AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_payment_method_container), this, parcelableArrayList, 24);
            PaymentMethodRow paymentMethodRow = (PaymentMethodRow) AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_payment_method_row);
            AbstractC28901Pl r2 = this.A05.A00;
            C1311161i.A0A(r2, paymentMethodRow);
            paymentMethodRow.A02.setText(A0I(R.string.novi_send_money_review_method_paying_with));
            paymentMethodRow.A02.setVisibility(0);
            paymentMethodRow.A05.setText(A19(r2));
            View A0D3 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_transaction_fee_layout);
            C12990iw.A1H(C12960it.A0I(A0D3, R.id.novi_send_money_review_transaction_line_item_lhs), this, R.string.novi_send_money_review_method_details_send_money_fee_label);
            TextView A0I2 = C12960it.A0I(A0D3, R.id.novi_send_money_review_transaction_line_item_rhs);
            A0I2.setText(C117305Zk.A0e(A0I2.getContext(), this.A00, r62, C117295Zj.A0D(r62, bigDecimal), 0));
            View A0D4 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_deposit_fee_layout);
            C1316563p r03 = this.A03.A04;
            if (r03 == null || (r6 = r03.A02) == null) {
                A0D4.setVisibility(8);
            } else {
                C12990iw.A1H(C12960it.A0I(A0D4, R.id.novi_send_money_review_transaction_line_item_lhs), this, R.string.novi_send_money_review_method_details_deposit_fee_label);
                C12960it.A0I(A0D4, R.id.novi_send_money_review_transaction_line_item_rhs).setText(C117305Zk.A0e(A0p(), this.A00, r6.A00, r6.A01, 0));
            }
            View A0D5 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_deposit_amount_layout);
            AnonymousClass63X r22 = this.A05;
            C12960it.A0I(A0D5, R.id.novi_send_money_review_transaction_line_item_lhs).setText(A19(r22.A00));
            TextView A0I3 = C12960it.A0I(A0D5, R.id.novi_send_money_review_transaction_line_item_rhs);
            AnonymousClass6F2 r04 = r22.A01.A02;
            A0I3.setText(C117305Zk.A0e(A0p(), this.A00, r04.A00, r04.A01, 0));
            TextView A0I4 = C12960it.A0I(view, R.id.novi_send_money_review_method_details_deposit_exchange_rate);
            C1315863i r32 = this.A03;
            A0I4.setText(A00(A01(), this.A00, r32.A01, r32));
            TextView A0I5 = C12960it.A0I(view, R.id.novi_send_money_review_method_details_amount_info);
            AnonymousClass63X r10 = this.A05;
            C1316363n r05 = r10.A01;
            AnonymousClass6F2 r9 = r05.A02;
            AbstractC30791Yv r33 = r9.A00;
            AnonymousClass6F2 r5 = r05.A01;
            AbstractC30791Yv r23 = r5.A00;
            String A0I6 = A0I(R.string.learn_more);
            Object[] objArr = new Object[4];
            objArr[0] = r23.AAA(this.A00, r5.A01, 1);
            objArr[1] = A19(r10.A00);
            objArr[2] = r33.AAA(this.A00, r9.A01, 0);
            CharSequence AA7 = r33.AA7(A0I5.getContext(), C12970iu.A0q(this, A0I6, objArr, 3, R.string.novi_send_money_review_method_details_summary_info));
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(AA7);
            spannableStringBuilder.setSpan(new C117495a3(this), AA7.length() - A0I6.length(), AA7.length(), 33);
            A0I5.setText(spannableStringBuilder);
            A0I5.setLinksClickable(true);
            C12990iw.A1F(A0I5);
            return;
        }
        View A0D6 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_method_details_fees_layout);
        C12990iw.A1H(C12960it.A0I(A0D6, R.id.novi_send_money_review_transaction_line_item_lhs), this, R.string.novi_send_money_review_extras_fees_label);
        TextView A0I7 = C12960it.A0I(A0D6, R.id.novi_send_money_review_transaction_line_item_rhs);
        A0I7.setText(C117305Zk.A0e(A0I7.getContext(), this.A00, r62, C117295Zj.A0D(r62, bigDecimal), 0));
        A0D6.setVisibility(0);
        TextView A0I8 = C12960it.A0I(view, R.id.novi_send_money_review_method_details_transaction_exchange_rate);
        C1315863i r34 = this.A03;
        A0I8.setText(A00(A01(), this.A00, r34.A01, r34));
        A0I8.setVisibility(0);
        C117305Zk.A15(view, R.id.novi_send_money_review_method_details_deposit_container);
    }

    public final String A19(AbstractC28901Pl r5) {
        if (r5 instanceof C30881Ze) {
            return C1311161i.A05(A01(), (C30881Ze) r5);
        }
        boolean z = r5 instanceof C30861Zc;
        Context A01 = A01();
        if (z) {
            return C1311161i.A03(A01, (C30861Zc) r5);
        }
        return C1311161i.A02(A01, this.A00, r5, this.A02, true);
    }
}
