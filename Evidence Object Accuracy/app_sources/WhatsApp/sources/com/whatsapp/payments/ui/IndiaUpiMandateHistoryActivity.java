package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.AbstractC35651iS;
import X.ActivityC121715je;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass69Q;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118175bO;
import X.C118445bp;
import X.C122265lB;
import X.C122595li;
import X.C127085tv;
import X.C128355vy;
import X.C12960it;
import X.C243515e;
import X.C30931Zj;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiMandateHistoryActivity extends ActivityC121715je {
    public AbstractC35651iS A00;
    public C243515e A01;
    public AnonymousClass6BE A02;
    public C118175bO A03;
    public C128355vy A04;
    public boolean A05;
    public final C30931Zj A06;

    public IndiaUpiMandateHistoryActivity() {
        this(0);
        this.A06 = C30931Zj.A00("IndiaUpiMandateHistoryActivity", "mandates", "IN");
    }

    public IndiaUpiMandateHistoryActivity(int i) {
        this.A05 = false;
        C117295Zj.A0p(this, 48);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = C117305Zk.A0T(A1M);
            this.A04 = (C128355vy) A1M.A9f.get();
            this.A01 = (C243515e) A1M.AEt.get();
        }
    }

    @Override // X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1002) {
            View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_expandable_listview);
            A0F.setBackgroundColor(C12960it.A09(A0F).getColor(R.color.primary_surface));
            return new C122265lB(A0F);
        } else if (i != 1003) {
            return super.A2e(viewGroup, i);
        } else {
            return new C122595li(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_payment_section_header_component));
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        this.A03.A04(new C127085tv(2));
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117295Zj.A0g(this, A1U, R.string.upi_mandate_row_title);
        }
        this.A06.A06("onCreate");
        C118175bO r2 = (C118175bO) C117315Zl.A06(new C118445bp(this.A02, this, this.A04), this).A00(C118175bO.class);
        this.A03 = r2;
        r2.A04(new C127085tv(0));
        C118175bO r3 = this.A03;
        r3.A01.A05(r3.A00, C117305Zk.A0B(this, 39));
        C118175bO r32 = this.A03;
        r32.A03.A05(r32.A00, C117305Zk.A0B(this, 38));
        AnonymousClass69Q r1 = new AnonymousClass69Q(this);
        this.A00 = r1;
        this.A01.A03(r1);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A01.A04(this.A00);
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            this.A03.A04(new C127085tv(2));
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
