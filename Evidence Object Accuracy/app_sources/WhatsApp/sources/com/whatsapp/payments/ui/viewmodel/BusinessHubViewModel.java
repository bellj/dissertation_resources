package com.whatsapp.payments.ui.viewmodel;

import X.AbstractC14440lR;
import X.AbstractC16710pd;
import X.AbstractC16870pt;
import X.AbstractC460624h;
import X.AnonymousClass015;
import X.AnonymousClass46M;
import X.AnonymousClass4UZ;
import X.AnonymousClass4Yq;
import X.AnonymousClass58A;
import X.AnonymousClass5J3;
import X.AnonymousClass5J4;
import X.C16700pc;
import X.C17070qD;
import X.C18660so;
import X.C248217a;
import X.C25861Bc;
import X.C25871Bd;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel;

/* loaded from: classes2.dex */
public final class BusinessHubViewModel extends AnonymousClass015 {
    public final AnonymousClass4UZ A00;
    public final C248217a A01;
    public final C18660so A02;
    public final AbstractC460624h A03;
    public final C25861Bc A04;
    public final C17070qD A05;
    public final AbstractC16870pt A06;
    public final C25871Bd A07;
    public final AbstractC14440lR A08;
    public final AbstractC16710pd A09 = AnonymousClass4Yq.A00(new AnonymousClass5J3());
    public final AbstractC16710pd A0A = AnonymousClass4Yq.A00(new AnonymousClass5J4());

    public BusinessHubViewModel(C248217a r3, C18660so r4, C25861Bc r5, C17070qD r6, AbstractC16870pt r7, C25871Bd r8, AbstractC14440lR r9) {
        C16700pc.A0E(r9, 1);
        C16700pc.A0E(r6, 2);
        C16700pc.A0E(r7, 3);
        C16700pc.A0E(r3, 4);
        C16700pc.A0E(r8, 5);
        C16700pc.A0E(r4, 6);
        C16700pc.A0E(r5, 7);
        this.A08 = r9;
        this.A05 = r6;
        this.A06 = r7;
        this.A01 = r3;
        this.A07 = r8;
        this.A02 = r4;
        this.A04 = r5;
        AnonymousClass46M r1 = new AnonymousClass46M(this);
        this.A00 = r1;
        AnonymousClass58A r0 = new AbstractC460624h() { // from class: X.58A
            @Override // X.AbstractC460624h
            public final void ATX(AbstractC28901Pl r32, AnonymousClass1V8 r42) {
                BusinessHubViewModel.this.A04(false);
            }
        };
        this.A03 = r0;
        r5.A03(r0);
        r3.A03(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0074, code lost:
        if (r0 != false) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        if (r1 != false) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.text.Spannable A00(android.content.Context r6, X.AnonymousClass2DI r7) {
        /*
            r4 = 0
            java.lang.String r1 = r7.A00()
            int r0 = r1.hashCode()
            switch(r0) {
                case -1810585926: goto L_0x006b;
                case -1714866505: goto L_0x0068;
                case 35394935: goto L_0x005e;
                case 479965251: goto L_0x005b;
                case 1339011704: goto L_0x0058;
                case 1925346054: goto L_0x004e;
                default: goto L_0x000c;
            }
        L_0x000c:
            r1 = 2131889355(0x7f120ccb, float:1.9413371E38)
        L_0x000f:
            java.lang.String r5 = r6.getString(r1)
            X.C16700pc.A0B(r5)
            r0 = 2131889355(0x7f120ccb, float:1.9413371E38)
            java.lang.String r0 = r6.getString(r0)
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x0033
            r0 = 2131889354(0x7f120cca, float:1.941337E38)
            java.lang.String r0 = r6.getString(r0)
            boolean r1 = r5.equals(r0)
            r0 = 2131100719(0x7f06042f, float:1.7813827E38)
            if (r1 == 0) goto L_0x0036
        L_0x0033:
            r0 = 2131100771(0x7f060463, float:1.7813933E38)
        L_0x0036:
            android.text.SpannableString r3 = new android.text.SpannableString
            r3.<init>(r5)
            int r0 = X.AnonymousClass00T.A00(r6, r0)
            android.text.style.ForegroundColorSpan r2 = new android.text.style.ForegroundColorSpan
            r2.<init>(r0)
            int r1 = r5.length()
            r0 = 33
            r3.setSpan(r2, r4, r1, r0)
            return r3
        L_0x004e:
            java.lang.String r0 = "ACTIVE"
            boolean r0 = r1.equals(r0)
            r1 = 2131889352(0x7f120cc8, float:1.9413365E38)
            goto L_0x0074
        L_0x0058:
            java.lang.String r0 = "HARD_BLOCKED"
            goto L_0x006d
        L_0x005b:
            java.lang.String r0 = "EXTERNALLY_DISABLED"
            goto L_0x006d
        L_0x005e:
            java.lang.String r0 = "PENDING"
            boolean r0 = r1.equals(r0)
            r1 = 2131889357(0x7f120ccd, float:1.9413375E38)
            goto L_0x0074
        L_0x0068:
            java.lang.String r0 = "SOFT_BLOCKED"
            goto L_0x006d
        L_0x006b:
            java.lang.String r0 = "INTEGRITY_BLOCKED"
        L_0x006d:
            boolean r0 = r1.equals(r0)
            r1 = 2131889354(0x7f120cca, float:1.941337E38)
        L_0x0074:
            if (r0 != 0) goto L_0x000f
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel.A00(android.content.Context, X.2DI):android.text.Spannable");
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A04.A04(this.A03);
        this.A01.A04(this.A00);
    }

    public final void A04(boolean z) {
        this.A08.Ab6(new RunnableBRunnable0Shape0S0110000_I0(this, 16, z));
    }
}
