package com.whatsapp.payments.ui;

import X.AbstractActivityC119305dV;
import X.AbstractC005102i;
import X.AbstractC16870pt;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass19Y;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C124295ov;
import X.C125815rr;
import X.C12960it;
import X.C12970iu;
import X.C15550nR;
import X.C17900ra;
import X.C18790t3;
import android.os.Bundle;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class BrazilPaymentCareTransactionSelectorActivity extends PaymentTransactionHistoryActivity {
    public AnonymousClass19Y A00;
    public C18790t3 A01;
    public C15550nR A02;
    public C17900ra A03;
    public AbstractC16870pt A04;
    public C124295ov A05;
    public boolean A06;

    public BrazilPaymentCareTransactionSelectorActivity() {
        this(0);
    }

    public BrazilPaymentCareTransactionSelectorActivity(int i) {
        this.A06 = false;
        C117295Zj.A0p(this, 15);
    }

    @Override // X.AbstractActivityC119305dV, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119305dV.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A02 = C12960it.A0O(A1M);
            this.A03 = (C17900ra) A1M.AF5.get();
            this.A00 = (AnonymousClass19Y) A1M.AI6.get();
            this.A01 = (C18790t3) A1M.AJw.get();
            this.A04 = (AbstractC16870pt) A1M.A20.get();
        }
    }

    public final C124295ov A2j() {
        C124295ov r0 = this.A05;
        if (r0 != null && r0.A00() == 1) {
            this.A05.A03(false);
        }
        Bundle A0D = C12970iu.A0D();
        A0D.putString("com.whatsapp.support.DescribeProblemActivity.from", "payments:settings");
        C18790t3 r5 = this.A01;
        C124295ov r02 = new C124295ov(A0D, this, this.A00, ((ActivityC13810kN) this).A06, r5, ((PaymentTransactionHistoryActivity) this).A05, null, null, ((ActivityC13810kN) this).A0D, this.A03, "payments:settings");
        this.A05 = r02;
        return r02;
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionHistoryActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0A(R.string.care_transaction_selection_title);
        ((PaymentTransactionHistoryActivity) this).A0G.A00 = new C125815rr(this);
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.bottom_button);
        textView.setVisibility(0);
        textView.setText(R.string.care_its_something_else);
        C117295Zj.A0n(textView, this, 9);
    }
}
