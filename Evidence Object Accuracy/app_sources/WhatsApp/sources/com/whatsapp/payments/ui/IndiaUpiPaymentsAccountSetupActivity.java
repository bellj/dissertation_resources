package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC18670sp;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1A7;
import X.AnonymousClass1FJ;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C1329668y;
import X.C30861Zc;
import X.C30931Zj;
import X.C32641cU;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import org.chromium.net.UrlRequest;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentsAccountSetupActivity extends AbstractActivityC121665jA {
    public AnonymousClass1FJ A00;
    public AnonymousClass1A7 A01;
    public boolean A02;
    public final C30931Zj A03;

    public IndiaUpiPaymentsAccountSetupActivity() {
        this(0);
        this.A03 = C117295Zj.A0G("IndiaUpiPaymentsAccountSetupActivity");
    }

    public IndiaUpiPaymentsAccountSetupActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 58);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A01 = (AnonymousClass1A7) A1M.AEs.get();
            this.A00 = (AnonymousClass1FJ) A1M.AEx.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0033, code lost:
        if (r0.equals(r1) != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A30(boolean r5) {
        /*
            r4 = this;
            X.1Zj r1 = r4.A03
            java.lang.String r0 = "showCompleteAndFinish "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            r0.append(r5)
            X.C117295Zj.A1F(r1, r0)
            r4.AaN()
            X.1FJ r1 = r4.A00
            X.69X r0 = new X.69X
            r0.<init>()
            r1.A00(r0)
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiBankAccountAddedLandingActivity> r0 = com.whatsapp.payments.ui.IndiaUpiBankAccountAddedLandingActivity.class
            android.content.Intent r3 = X.C12990iw.A0D(r4, r0)
            android.content.Intent r0 = r4.getIntent()
            java.lang.String r2 = "referral_screen"
            java.lang.String r0 = r0.getStringExtra(r2)
            if (r0 == 0) goto L_0x0042
            java.lang.String r1 = "setup_pin"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0042
        L_0x0035:
            r3.putExtra(r2, r1)
            r4.A2v(r3)
            r4.finish()
            r4.startActivity(r3)
            return
        L_0x0042:
            java.lang.String r1 = "nav_select_account"
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity.A30(boolean):void");
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.payments_title);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        Intent intent;
        Class cls;
        Class cls2;
        String str;
        super.onResume();
        C30931Zj r2 = this.A03;
        StringBuilder A0k = C12960it.A0k("onResume payment setup with mode: ");
        A0k.append(((AbstractActivityC121665jA) this).A03);
        C117295Zj.A1F(r2, A0k);
        if (!isFinishing()) {
            C12960it.A0t(((ActivityC13810kN) this).A09.A00.edit(), "payments_onboarding_banner_registration_started", true);
            C32641cU A00 = ((AbstractActivityC121685jC) this).A0I.A00();
            if (((AbstractActivityC121665jA) this).A0O && !((AbstractActivityC121665jA) this).A0Q && !C12980iv.A1W(((AbstractActivityC121665jA) this).A0C.A01(), "payment_account_recovered")) {
                int i = ((AbstractActivityC121665jA) this).A02;
                if (i == 2 || i == 3 || i == 6 || i == 7 || i == 8 || i == 10 || i == 11) {
                    cls2 = IndiaUpiPaymentsValuePropsBottomSheetActivity.class;
                } else {
                    cls2 = IndiaUpiPaymentsValuePropsActivity.class;
                }
                Intent A0D = C12990iw.A0D(this, cls2);
                finish();
                A0D.putExtra("extra_setup_mode", ((AbstractActivityC121665jA) this).A03);
                switch (((AbstractActivityC121665jA) this).A02) {
                    case 1:
                        str = "in_app_banner";
                        break;
                    case 2:
                    case 3:
                        str = "chat";
                        break;
                    case 4:
                    case 5:
                    case 12:
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        str = "payment_home";
                        break;
                    case 6:
                        str = "new_payment";
                        break;
                    case 7:
                        str = "payment_bank_account_details";
                        break;
                    case 8:
                        str = "qr_code_scan_prompt";
                        break;
                    case 9:
                        str = "deeplink";
                        break;
                    case 10:
                        str = "payment_composer_icon";
                        break;
                    case 11:
                        str = "order_details";
                        break;
                    default:
                        str = null;
                        break;
                }
                A0D.putExtra("referral_screen", str);
                A2v(A0D);
                if (getIntent() != null) {
                    A0D.putExtra("perf_start_time_ns", getIntent().getLongExtra("perf_start_time_ns", -1));
                    C117305Zk.A10(getIntent(), A0D, "perf_origin");
                }
                startActivity(A0D);
            } else if (A00 == null) {
                r2.A06("showNextStep is already complete");
                C12960it.A0t(C117295Zj.A05(((AbstractActivityC121665jA) this).A0C), "payments_home_onboarding_banner_dismissed", false);
                A30(true);
            } else {
                r2.A06(C12960it.A0b("showNextStep: ", A00));
                if (A00 == AbstractC18670sp.A04) {
                    r2.A0A("Unset step", null);
                } else {
                    String str2 = A00.A03;
                    if (str2.equals("tos_with_wallet") || str2.equals("tos_no_wallet")) {
                        intent = C12990iw.A0D(this, IndiaUpiPaymentsTosActivity.class);
                        finish();
                        intent.putExtra("stepName", str2);
                        intent.putExtra("extra_setup_mode", ((AbstractActivityC121665jA) this).A03);
                    } else if (str2.equals("add_card")) {
                        r2.A06("showAddCard not implemented");
                        return;
                    } else if (str2.equals("add_bank")) {
                        if (C12980iv.A1W(((AbstractActivityC121665jA) this).A0C.A01(), "payment_account_recovered")) {
                            C1329668y r1 = ((AbstractActivityC121665jA) this).A0B;
                            if (!r1.A0N(r1.A07()) && ((ActivityC13810kN) this).A0C.A07(1644)) {
                                int i2 = ((AbstractActivityC121665jA) this).A02;
                                if (i2 == 2 || i2 == 3 || i2 == 6) {
                                    cls = IndiaUpiProvideMoreInfoBottomSheetActivity.class;
                                } else {
                                    cls = IndiaUpiProvideMoreInfoActivity.class;
                                }
                                Intent A0D2 = C12990iw.A0D(this, cls);
                                A2v(A0D2);
                                startActivity(A0D2);
                            }
                        }
                        Intent A0D3 = C12990iw.A0D(this, IndiaUpiBankPickerActivity.class);
                        finish();
                        ((AbstractActivityC121665jA) this).A0N = true;
                        A2v(A0D3);
                        startActivity(A0D3);
                        return;
                    } else if (str2.equals("2fa")) {
                        C12960it.A0t(C117295Zj.A05(((AbstractActivityC121665jA) this).A0C), "payments_home_onboarding_banner_dismissed", false);
                        if (((AbstractActivityC121665jA) this).A03 == 1) {
                            intent = C12990iw.A0D(this, IndiaUpiPinPrimerFullSheetActivity.class);
                            C30861Zc r0 = ((AbstractActivityC121665jA) this).A04;
                            if (r0 != null) {
                                C117315Zl.A0M(intent, r0);
                            }
                            finish();
                            ((AbstractActivityC121665jA) this).A0N = true;
                        } else {
                            A30(false);
                            return;
                        }
                    } else {
                        return;
                    }
                    A2v(intent);
                    startActivity(intent);
                    return;
                }
                finish();
            }
        }
    }
}
