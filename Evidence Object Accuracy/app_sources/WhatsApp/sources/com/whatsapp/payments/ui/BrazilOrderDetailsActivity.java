package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractC136316Mb;
import X.AbstractC136536Mx;
import X.AbstractC14640lm;
import X.AbstractC28901Pl;
import X.AbstractC30891Zf;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12H;
import X.AnonymousClass14X;
import X.AnonymousClass1A7;
import X.AnonymousClass1FI;
import X.AnonymousClass1IS;
import X.AnonymousClass1KC;
import X.AnonymousClass2FL;
import X.AnonymousClass2I6;
import X.AnonymousClass60N;
import X.AnonymousClass617;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C118155bM;
import X.C119815f9;
import X.C123695nh;
import X.C129165xH;
import X.C12970iu;
import X.C12980iv;
import X.C133966Cu;
import X.C16170oZ;
import X.C16380ov;
import X.C18620sk;
import X.C20370ve;
import X.C22710zW;
import X.C25881Be;
import X.C30821Yy;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.payments.ui.orderdetails.PaymentCheckoutOrderDetailsViewV2;

/* loaded from: classes4.dex */
public class BrazilOrderDetailsActivity extends BrazilPaymentActivity implements AbstractC136316Mb {
    public long A00;
    public C16170oZ A01;
    public AnonymousClass12H A02;
    public C20370ve A03;
    public C22710zW A04;
    public AnonymousClass1A7 A05;
    public C123695nh A06;
    public C129165xH A07;
    public PaymentCheckoutOrderDetailsViewV2 A08;
    public AnonymousClass2I6 A09;
    public C118155bM A0A;
    public C25881Be A0B;
    public AnonymousClass617 A0C;
    public AnonymousClass14X A0D;
    public AnonymousClass1IS A0E;
    public AnonymousClass1FI A0F;
    public String A0G;
    public boolean A0H;
    public final AbstractC136536Mx A0I;

    @Override // X.AbstractC136316Mb
    public boolean AdW(int i) {
        return i == 405 || i == 401 || i == 403 || i == 420;
    }

    public BrazilOrderDetailsActivity() {
        this(0);
        this.A0I = new C133966Cu(this);
    }

    public BrazilOrderDetailsActivity(int i) {
        this.A0H = false;
        C117295Zj.A0p(this, 11);
    }

    @Override // X.AbstractActivityC121435hw, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1W(A1M, this);
            AbstractActivityC119235dO.A1X(A1M, this);
            AbstractActivityC119235dO.A1R(A09, A1M, (C18620sk) A1M.AFB.get(), this);
            this.A01 = (C16170oZ) A1M.AM4.get();
            this.A0B = (C25881Be) A1M.AEa.get();
            this.A0D = (AnonymousClass14X) A1M.AFM.get();
            this.A02 = (AnonymousClass12H) A1M.AC5.get();
            this.A04 = C117305Zk.A0O(A1M);
            this.A03 = (C20370ve) A1M.AEu.get();
            this.A05 = (AnonymousClass1A7) A1M.AEs.get();
            this.A0F = (AnonymousClass1FI) A1M.ADq.get();
            this.A09 = A09.A0B();
        }
    }

    @Override // com.whatsapp.payments.ui.BrazilPaymentActivity
    public void A2u(C30821Yy r3, AbstractC28901Pl r4, AnonymousClass1KC r5, String str, String str2, String str3, int i) {
        ((ActivityC13830kP) this).A05.Ab2(new Runnable(str2) { // from class: X.6I2
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C16470p4 r0;
                AnonymousClass1ZD r02;
                BrazilOrderDetailsActivity brazilOrderDetailsActivity = BrazilOrderDetailsActivity.this;
                String str4 = this.A01;
                C16380ov r1 = (C16380ov) ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A09.A0K.A03(brazilOrderDetailsActivity.A0E);
                if (r1 != null && (r0 = r1.A00) != null && (r02 = r0.A01) != null) {
                    r02.A02 = str4;
                    ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A09.A0W(r1);
                }
            }
        });
        super.A2u(r3, r4, r5, str, str2, str3, i);
    }

    @Override // com.whatsapp.payments.ui.BrazilPaymentActivity
    public void A2w(C119815f9 r2, int i) {
        super.A2w(r2, i);
        ((AbstractC30891Zf) r2).A02 = A2o();
    }

    @Override // X.AbstractC136316Mb
    public void Ads(AbstractC14640lm r5, int i, long j) {
        int i2 = R.string.order_details_order_successfully_paid_title;
        int i3 = R.string.order_details_order_successfully_paid_content;
        if (i == 401 || i == 403 || i == 420) {
            i2 = R.string.order_details_order_processing_payment_title;
            i3 = R.string.order_details_order_processing_payment_content;
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0B(false);
        A0S.setTitle(getString(i2));
        A0S.A0A(getString(i3));
        C117295Zj.A0q(A0S, this, 6, R.string.ok);
        A0S.setNegativeButton(R.string.catalog_product_message_biz, new DialogInterface.OnClickListener(r5, this, j) { // from class: X.62f
            public final /* synthetic */ long A00;
            public final /* synthetic */ AbstractC14640lm A01;
            public final /* synthetic */ BrazilOrderDetailsActivity A02;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A00 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                C117305Zk.A0z(this.A02, this.A01, this.A00);
            }
        });
        C12970iu.A1J(A0S);
    }

    @Override // com.whatsapp.payments.ui.BrazilPaymentActivity, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass60N r3;
        AnonymousClass617 r0 = this.A0C;
        if (!(r0 == null || (r3 = (AnonymousClass60N) r0.A01) == null)) {
            Bundle A0D = C12970iu.A0D();
            Boolean bool = r3.A04;
            if (bool != null) {
                A0D.putBoolean("should_show_shimmer_key", bool.booleanValue());
            }
            A0D.putParcelable("checkout_error_code_key", r3.A01);
            A0D.putParcelable("merchant_jid_key", r3.A00);
            A0D.putSerializable("merchant_status_key", r3.A02);
            C16380ov r02 = r3.A03;
            if (r02 != null) {
                A0D.putParcelable("payment_transaction_key", r02.A0L);
            }
            bundle.putBundle("save_order_detail_state_key", A0D);
        }
        super.onSaveInstanceState(bundle);
    }
}
