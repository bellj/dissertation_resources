package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121525iS;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121615j0;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC136316Mb;
import X.AbstractC136536Mx;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC16390ow;
import X.AbstractC28901Pl;
import X.AbstractC30891Zf;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass12H;
import X.AnonymousClass14X;
import X.AnonymousClass17Q;
import X.AnonymousClass1A7;
import X.AnonymousClass1FI;
import X.AnonymousClass1IS;
import X.AnonymousClass1QC;
import X.AnonymousClass1W9;
import X.AnonymousClass2I6;
import X.AnonymousClass65v;
import X.AnonymousClass68A;
import X.AnonymousClass68C;
import X.C004802e;
import X.C117295Zj;
import X.C117315Zl;
import X.C118155bM;
import X.C119705ey;
import X.C119755f3;
import X.C119835fB;
import X.C120385g6;
import X.C120695gb;
import X.C124235op;
import X.C126705tJ;
import X.C128335vw;
import X.C128795wg;
import X.C129165xH;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C130115yp;
import X.C133976Cv;
import X.C14830m7;
import X.C14850m9;
import X.C15650ng;
import X.C25881Be;
import X.C26061Bw;
import X.C30821Yy;
import X.C38171nd;
import X.C38211ni;
import X.C452120p;
import X.C64513Fv;
import X.EnumC124545pi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.orderdetails.PaymentCheckoutOrderDetailsViewV2;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class IndiaUpiCheckOrderDetailsActivity extends AbstractActivityC121615j0 implements AbstractC136316Mb {
    public long A00;
    public AnonymousClass018 A01;
    public AnonymousClass12H A02;
    public C120385g6 A03;
    public AnonymousClass1A7 A04;
    public AnonymousClass17Q A05;
    public C129165xH A06;
    public PaymentCheckoutOrderDetailsViewV2 A07;
    public AnonymousClass2I6 A08;
    public C118155bM A09;
    public C130115yp A0A;
    public C25881Be A0B;
    public AnonymousClass14X A0C;
    public AnonymousClass1IS A0D;
    public AnonymousClass1FI A0E;
    public String A0F = "WhatsappPay";
    public String A0G;
    public String A0H;
    public String A0I;
    public final AbstractC136536Mx A0J = new C133976Cv(this);

    @Override // X.AbstractActivityC121665jA
    public void A2v(Intent intent) {
        super.A2v(intent);
        intent.putExtra("extra_order_id", this.A0H);
        intent.putExtra("extra_order_expiry_ts_in_sec", this.A00);
        intent.putExtra("extra_payment_config_id", this.A0I);
    }

    @Override // X.AbstractActivityC121525iS
    public void A3T(C119705ey r3, C119705ey r4, C452120p r5, String str, String str2, boolean z) {
        super.A3T(r3, r4, r5, str, str2, z);
        if (r5 == null && r3 == null && r4 == null && str != null) {
            ((ActivityC13830kP) this).A05.Ab2(new Runnable(str) { // from class: X.6I9
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C16470p4 r0;
                    AnonymousClass1ZD r02;
                    IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = IndiaUpiCheckOrderDetailsActivity.this;
                    String str3 = this.A01;
                    C16380ov r1 = (C16380ov) ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A09.A0K.A03(indiaUpiCheckOrderDetailsActivity.A0D);
                    if (r1 != null && (r0 = r1.A00) != null && (r02 = r0.A01) != null) {
                        r02.A02 = str3;
                        ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A09.A0W(r1);
                    }
                }
            });
        }
    }

    public void A3b(C30821Yy r14) {
        AbstractC28901Pl r2 = ((AbstractActivityC121525iS) this).A0B;
        if (r2 == null) {
            A3P(this);
            return;
        }
        C119755f3 r0 = (C119755f3) r2.A08;
        if (r0 == null || C12970iu.A1Y(r0.A05.A00)) {
            A2C(R.string.register_wait_message);
            C120385g6 r5 = this.A03;
            String str = this.A0I;
            UserJid userJid = ((AbstractActivityC121525iS) this).A0C;
            C128795wg r6 = new C128795wg(r14, this);
            ArrayList A0l = C12960it.A0l();
            C117295Zj.A1M("action", "upi-get-p2m-config", A0l);
            if (str != null) {
                C117295Zj.A1M("payment-config-id", str, A0l);
            }
            if (userJid != null) {
                A0l.add(new AnonymousClass1W9(userJid, "receiver"));
            }
            C64513Fv r4 = ((C126705tJ) r5).A00;
            if (r4 != null) {
                r4.A04("upi-get-p2m-config");
            }
            r5.A04.A0F(new C120695gb(r5.A00, r5.A01, r5.A03, r4, r5, r6), C117295Zj.A0K(A0l), "get", C26061Bw.A0L);
            return;
        }
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("extra_bank_account", r2);
        IndiaUpiPinPrimerDialogFragment indiaUpiPinPrimerDialogFragment = new IndiaUpiPinPrimerDialogFragment();
        indiaUpiPinPrimerDialogFragment.A0U(A0D);
        indiaUpiPinPrimerDialogFragment.A04 = this;
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        paymentBottomSheet.A01 = indiaUpiPinPrimerDialogFragment;
        Adl(paymentBottomSheet, "IndiaUpiPinPrimerDialogFragment");
        A3R(paymentBottomSheet);
    }

    public void A3c(EnumC124545pi r12, C128335vw r13) {
        if (!(this instanceof IndiaUpiQuickBuyActivity)) {
            AbstractC14440lR r6 = ((ActivityC13830kP) this).A05;
            C15650ng r1 = ((AbstractActivityC121685jC) this).A09;
            AnonymousClass1A7 r4 = this.A04;
            AnonymousClass1QC.A09(((ActivityC13810kN) this).A05, r1, ((AbstractActivityC121525iS) this).A07, new AnonymousClass68C(this, r12, r13), r4, r13.A07, r6);
            return;
        }
        IndiaUpiQuickBuyActivity indiaUpiQuickBuyActivity = (IndiaUpiQuickBuyActivity) this;
        AnonymousClass1FI r2 = ((IndiaUpiCheckOrderDetailsActivity) indiaUpiQuickBuyActivity).A0E;
        AbstractC16390ow r9 = r13.A07;
        r2.A00(r9, ((IndiaUpiCheckOrderDetailsActivity) indiaUpiQuickBuyActivity).A0F, 12);
        AbstractC14440lR r10 = ((ActivityC13830kP) indiaUpiQuickBuyActivity).A05;
        C15650ng r5 = ((AbstractActivityC121685jC) indiaUpiQuickBuyActivity).A09;
        AnonymousClass1A7 r8 = ((IndiaUpiCheckOrderDetailsActivity) indiaUpiQuickBuyActivity).A04;
        AnonymousClass1QC.A09(((ActivityC13810kN) indiaUpiQuickBuyActivity).A05, r5, ((AbstractActivityC121525iS) indiaUpiQuickBuyActivity).A07, new AnonymousClass68A(indiaUpiQuickBuyActivity, r13), r8, r9, r10);
    }

    @Override // X.AbstractC136316Mb
    public boolean AdW(int i) {
        return C12960it.A1V(i, 405);
    }

    @Override // X.AbstractC136316Mb
    public void Ads(AbstractC14640lm r4, int i, long j) {
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0B(false);
        A0S.setTitle(getString(R.string.order_details_order_successfully_paid_title));
        A0S.A0A(getString(R.string.order_details_order_successfully_paid_content));
        C117295Zj.A0q(A0S, this, 21, R.string.ok);
        A0S.setNegativeButton(R.string.catalog_product_message_biz, new DialogInterface.OnClickListener(r4, this, j) { // from class: X.62g
            public final /* synthetic */ long A00;
            public final /* synthetic */ AbstractC14640lm A01;
            public final /* synthetic */ IndiaUpiCheckOrderDetailsActivity A02;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A00 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                C117305Zk.A0z(this.A02, this.A01, this.A00);
            }
        });
        C12970iu.A1J(A0S);
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!(this instanceof IndiaUpiQuickBuyActivity)) {
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0M(true);
            }
            PaymentCheckoutOrderDetailsViewV2 paymentCheckoutOrderDetailsViewV2 = (PaymentCheckoutOrderDetailsViewV2) C12960it.A0F(LayoutInflater.from(this), null, R.layout.checkout_order_details_screen);
            this.A07 = paymentCheckoutOrderDetailsViewV2;
            setContentView(paymentCheckoutOrderDetailsViewV2);
        }
        ((AbstractActivityC121525iS) this).A0i = true;
        this.A0H = getIntent().getStringExtra("extra_order_id");
        this.A0G = getIntent().getStringExtra("extra_order_discount_program_name");
        this.A00 = getIntent().getLongExtra("extra_order_expiry_ts_in_sec", 0);
        this.A0I = getIntent().getStringExtra("extra_payment_config_id");
        AnonymousClass1IS A02 = C38211ni.A02(getIntent());
        AnonymousClass009.A05(A02);
        this.A0D = A02;
        C119835fB r5 = ((AbstractActivityC121525iS) this).A0G;
        String str = this.A0H;
        if (str == null) {
            str = "";
        }
        ((AbstractC30891Zf) r5).A02 = new C38171nd(str, A02.A01, this.A00);
        Resources resources = getResources();
        C14850m9 r7 = ((ActivityC13810kN) this).A0C;
        AnonymousClass14X r10 = this.A0C;
        C130115yp r3 = new C130115yp(resources, this.A01, ((AbstractActivityC121685jC) this).A08, r7, ((AbstractActivityC121685jC) this).A0P, this.A0J, r10);
        this.A0A = r3;
        C129165xH r2 = new C129165xH(((AbstractActivityC121525iS) this).A07, this, r3, ((ActivityC13830kP) this).A05);
        this.A06 = r2;
        ((ActivityC001000l) this).A06.A00(new PaymentCheckoutOrderDetailsPresenter$$ExternalSyntheticLambda0(r2));
        C118155bM r0 = (C118155bM) C117315Zl.A06(new AnonymousClass65v(this.A02, ((ActivityC13810kN) this).A0C, null, ((AbstractActivityC121525iS) this).A0I, this.A08, this.A0D, ((ActivityC13830kP) this).A05, false), this).A00(C118155bM.class);
        this.A09 = r0;
        r0.A04();
        C117295Zj.A0r(this, this.A09.A02, 37);
        if (((AbstractActivityC121525iS) this).A0U != null || !AbstractActivityC119235dO.A1l(this)) {
            AaN();
        } else {
            C124235op r1 = new C124235op(this);
            ((AbstractActivityC121525iS) this).A0U = r1;
            C12990iw.A1N(r1, ((ActivityC13830kP) this).A05);
        }
        A3N();
        C14830m7 r13 = ((ActivityC13790kL) this).A05;
        this.A03 = new C120385g6(this, ((ActivityC13810kN) this).A05, r13, ((AbstractActivityC121665jA) this).A0A, ((AbstractActivityC121685jC) this).A0K, ((AbstractActivityC121685jC) this).A0M);
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (AbstractActivityC119235dO.A1l(this) && !((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge") && ((AbstractActivityC121665jA) this).A0B.A05().A00 == null) {
            ((AbstractActivityC121525iS) this).A0m.A06("onResume getChallenge");
            A2C(R.string.register_wait_message);
            ((AbstractActivityC121545iU) this).A06.A03("upi-get-challenge");
            A36();
        }
    }
}
