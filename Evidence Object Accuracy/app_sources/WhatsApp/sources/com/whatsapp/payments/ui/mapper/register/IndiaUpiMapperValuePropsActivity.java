package com.whatsapp.payments.ui.mapper.register;

import X.AbstractActivityC119365db;
import X.AbstractC28491Nn;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass6BE;
import X.C124935qP;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15570nT;
import X.C16700pc;
import X.C253118x;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.Button;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperValuePropsActivity;

/* loaded from: classes2.dex */
public final class IndiaUpiMapperValuePropsActivity extends AbstractActivityC119365db {
    public Button A00;
    public AnonymousClass6BE A01;
    public C253118x A02;

    public static /* synthetic */ void A02(Intent intent, IndiaUpiMapperValuePropsActivity indiaUpiMapperValuePropsActivity) {
        C16700pc.A0E(indiaUpiMapperValuePropsActivity, 0);
        AnonymousClass6BE r4 = indiaUpiMapperValuePropsActivity.A01;
        if (r4 != null) {
            r4.AKg(1, C12980iv.A0k(), "alias_intro", ActivityC13790kL.A0W(indiaUpiMapperValuePropsActivity));
            indiaUpiMapperValuePropsActivity.A2G(intent, true);
            return;
        }
        throw C16700pc.A06("fieldStatsLogger");
    }

    public static /* synthetic */ void A03(IndiaUpiMapperValuePropsActivity indiaUpiMapperValuePropsActivity) {
        C16700pc.A0E(indiaUpiMapperValuePropsActivity, 0);
        AnonymousClass6BE r4 = indiaUpiMapperValuePropsActivity.A01;
        if (r4 != null) {
            r4.AKg(C12960it.A0V(), 9, "alias_intro", ActivityC13790kL.A0W(indiaUpiMapperValuePropsActivity));
            return;
        }
        throw C16700pc.A06("fieldStatsLogger");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass6BE r3 = this.A01;
        if (r3 != null) {
            Integer A0V = C12960it.A0V();
            r3.AKg(A0V, A0V, "alias_intro", ActivityC13790kL.A0W(this));
            return;
        }
        throw C16700pc.A06("fieldStatsLogger");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        String stringExtra;
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
        setContentView(R.layout.india_upi_mapper_value_props);
        C253118x r6 = this.A02;
        if (r6 != null) {
            Object[] objArr = new Object[1];
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            Me me = r0.A00;
            if (me == null || (str = me.number) == null) {
                str = "";
            }
            AbstractC28491Nn.A05((TextEmojiLabel) findViewById(R.id.mapper_value_props_sub_title), ((ActivityC13810kN) this).A08, r6.A01(this, C12960it.A0X(this, str, objArr, 0, R.string.mapper_value_props_sub_title_text), new Runnable[]{new Runnable() { // from class: X.3kx
                @Override // java.lang.Runnable
                public final void run() {
                    IndiaUpiMapperValuePropsActivity.A03(IndiaUpiMapperValuePropsActivity.this);
                }
            }}, new String[]{"learn-more"}, new String[]{"https://faq.whatsapp.com/general/payments/about-using-your-mobile-number-as-your-UPI-number"}));
            C124935qP.A00(this);
            View findViewById = findViewById(R.id.mapper_value_props_continue);
            C16700pc.A0B(findViewById);
            Button button = (Button) findViewById;
            C16700pc.A0E(button, 0);
            this.A00 = button;
            Intent A0D = C12990iw.A0D(this, IndiaUpiMapperLinkActivity.class);
            A0D.putExtra("extra_payment_name", getIntent().getParcelableExtra("extra_payment_name"));
            A0D.addFlags(33554432);
            Button button2 = this.A00;
            if (button2 != null) {
                button2.setOnClickListener(new View.OnClickListener(A0D, this) { // from class: X.3la
                    public final /* synthetic */ Intent A00;
                    public final /* synthetic */ IndiaUpiMapperValuePropsActivity A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        IndiaUpiMapperValuePropsActivity.A02(this.A00, this.A01);
                    }
                });
                onConfigurationChanged(C12980iv.A0H(this));
                AnonymousClass6BE r3 = this.A01;
                if (r3 != null) {
                    Intent intent = getIntent();
                    if (intent == null) {
                        stringExtra = null;
                    } else {
                        stringExtra = intent.getStringExtra("extra_referral_screen");
                    }
                    r3.AKg(0, null, "alias_intro", stringExtra);
                    return;
                }
                throw C16700pc.A06("fieldStatsLogger");
            }
            throw C16700pc.A06("continueButton");
        }
        throw C16700pc.A06("linkifier");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        C16700pc.A0E(menuItem, 0);
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass6BE r4 = this.A01;
            if (r4 != null) {
                r4.AKg(C12960it.A0V(), C12970iu.A0h(), "alias_intro", ActivityC13790kL.A0W(this));
            } else {
                throw C16700pc.A06("fieldStatsLogger");
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
