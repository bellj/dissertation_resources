package com.whatsapp.payments.ui;

import X.AbstractC118085bF;
import X.AbstractC1308360d;
import X.AbstractC15460nI;
import X.AbstractC28901Pl;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01Y;
import X.AnonymousClass12P;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.AnonymousClass1A8;
import X.AnonymousClass4EV;
import X.AnonymousClass5UV;
import X.AnonymousClass61M;
import X.AnonymousClass68R;
import X.AnonymousClass6BE;
import X.AnonymousClass6M6;
import X.C117295Zj;
import X.C117315Zl;
import X.C117735aW;
import X.C117785ad;
import X.C119755f3;
import X.C123505nG;
import X.C128355vy;
import X.C128645wR;
import X.C129245xP;
import X.C12970iu;
import X.C12980iv;
import X.C129855yP;
import X.C12990iw;
import X.C1311161i;
import X.C1329668y;
import X.C14820m6;
import X.C15570nT;
import X.C27621Ig;
import X.C36021jC;
import X.C42971wC;
import X.C460124c;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsFragment;
import com.whatsapp.payments.ui.IndiaUpiProfileDetailsActivity;
import com.whatsapp.payments.ui.PaymentSettingsFragment;
import com.whatsapp.payments.ui.widget.TransactionsExpandableView;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentSettingsFragment extends Hilt_IndiaUpiPaymentSettingsFragment implements AnonymousClass5UV, AnonymousClass6M6 {
    public View A00 = null;
    public AnonymousClass12P A01;
    public C14820m6 A02;
    public C1329668y A03;
    public AnonymousClass18T A04;
    public AnonymousClass18S A05;
    public AnonymousClass61M A06;
    public AnonymousClass6BE A07;
    public AnonymousClass1A8 A08;
    public C123505nG A09;
    public C128355vy A0A;

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return true;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass1A8 r1 = this.A08;
        r1.A00.clear();
        r1.A02.add(C12970iu.A10(this));
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i != 1008) {
            if (i != 1009) {
                return;
            }
            if (i2 == -1 && intent != null && intent.getIntExtra("extra_remove_payment_account", 0) >= 1) {
                if (intent.getIntExtra("extra_remove_payment_account", 0) == 2) {
                    Intent A0D = C12990iw.A0D(A0p(), IndiaUpiPaymentsAccountSetupActivity.class);
                    A0D.putExtra("extra_setup_mode", 2);
                    A0v(A0D);
                    return;
                }
                C117315Zl.A0O(this);
                return;
            }
        }
        this.A0q.A00(false);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_scan_qr) {
            return super.A0z(menuItem);
        }
        A0v(C12990iw.A0D(A0p(), IndiaUpiQrCodeScanActivity.class));
        return true;
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
        this.A0r.A02();
        C123505nG r3 = this.A09;
        if (r3 != null) {
            boolean A0D = r3.A0D();
            r3.A01.A0A(Boolean.valueOf(A0D));
            if (A0D) {
                r3.A0B.Ab2(new Runnable() { // from class: X.6HF
                    @Override // java.lang.Runnable
                    public final void run() {
                        AnonymousClass016 r1;
                        Boolean bool;
                        AnonymousClass60O r2;
                        AnonymousClass60R r0;
                        C123505nG r6 = C123505nG.this;
                        C20370ve r8 = r6.A02;
                        boolean z = true;
                        List A0b = r8.A0b(new Integer[]{20}, new Integer[]{40}, -1);
                        C14850m9 r4 = r6.A04;
                        if (!r4.A07(1433)) {
                            Iterator it = A0b.iterator();
                            while (it.hasNext()) {
                                C119835fB r02 = (C119835fB) C117315Zl.A09(it).A0A;
                                if (!(r02 == null || (r0 = r02.A0B) == null || !C1310060v.A02(r0.A0E))) {
                                    it.remove();
                                }
                            }
                        }
                        if (A0b.isEmpty()) {
                            Integer[] numArr = new Integer[1];
                            C12960it.A1P(numArr, 417, 0);
                            Iterator it2 = r8.A0b(numArr, new Integer[]{40}, -1).iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    z = false;
                                    break;
                                }
                                AbstractC30891Zf r12 = C117315Zl.A09(it2).A0A;
                                if (r12 instanceof C119835fB) {
                                    AnonymousClass60R r13 = ((C119835fB) r12).A0B;
                                    if (!r4.A07(1433)) {
                                        if (r13 != null && !C1310060v.A02(r13.A0E)) {
                                            r2 = r13.A0C;
                                            if (r2 != null && r2.A08.equals("UNKNOWN") && r2.A09.equals("INIT")) {
                                                break;
                                            }
                                        }
                                    } else if (r13 != null) {
                                        r2 = r13.A0C;
                                        if (r2 != null) {
                                            break;
                                            break;
                                        }
                                        continue;
                                    } else {
                                        continue;
                                    }
                                }
                            }
                            r1 = r6.A00;
                            bool = Boolean.valueOf(z);
                        } else {
                            r1 = r6.A00;
                            bool = Boolean.TRUE;
                        }
                        r1.A0A(bool);
                    }
                });
            }
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A14() {
        super.A14();
        this.A08.A01(this);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String str;
        C123505nG r5;
        super.A17(bundle, view);
        new C128645wR(((PaymentSettingsFragment) this).A0W).A00(A0C());
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        String str2 = null;
        if (bundle2 != null && bundle2.getBoolean("extra_send_to_upi_id", false)) {
            new C129245xP(A0C(), this.A04, this.A05, null).A00(null);
        }
        C123505nG r4 = this.A09;
        if (!(r4 == null || ((PaymentSettingsFragment) this).A09 == null)) {
            C117295Zj.A0r(this, r4.A01, 48);
            C117295Zj.A0r(this, this.A09.A00, 47);
        }
        if (((PaymentSettingsFragment) this).A0I.A05(AbstractC15460nI.A0w)) {
            C117295Zj.A0m(view, R.id.privacy_banner_avatar, AnonymousClass00T.A00(A01(), R.color.payment_privacy_avatar_tint));
            C42971wC.A08(A01(), Uri.parse("https://faq.whatsapp.com/general/payments/about-payments-data"), this.A01, ((PaymentSettingsFragment) this).A0F, C12970iu.A0T(view, R.id.payment_privacy_banner_text), ((PaymentSettingsFragment) this).A0O, C12970iu.A0q(this, "learn-more", C12970iu.A1b(), 0, R.string.upi_payments_privacy_banner_text), "learn-more");
            C12980iv.A1B(view, R.id.payment_privacy_banner, 0);
        }
        AbstractC1308360d r42 = this.A0r;
        if (bundle2 != null) {
            str = bundle2.getString("notification-type");
            str2 = bundle2.getString("step-up-id");
        } else {
            str = null;
        }
        r42.A06(str, str2);
        View inflate = A04().inflate(R.layout.powered_by_upi_logo, (ViewGroup) ((PaymentSettingsFragment) this).A0A, false);
        if (((PaymentSettingsFragment) this).A0A.getChildCount() > 0) {
            ((PaymentSettingsFragment) this).A0A.removeAllViews();
        }
        ((PaymentSettingsFragment) this).A0A.addView(inflate);
        ((PaymentSettingsFragment) this).A0A.setVisibility(0);
        if (bundle2 != null && bundle2.getBoolean("extra_is_invalid_deep_link_url", false)) {
            C36021jC.A01(A0C(), 101);
        }
        if (this.A03.A0L() && ((PaymentSettingsFragment) this).A0b.A01().getInt("payments_upi_transactions_sync_status", 0) == 0 && (r5 = this.A09) != null) {
            long longValue = Long.valueOf(((AbstractC118085bF) r5).A06.A01().getLong("payments_upi_last_transactions_sync_time", 0)).longValue();
            if (longValue == 0 || ((AbstractC118085bF) r5).A04.A00() - longValue > C123505nG.A0C) {
                C123505nG r43 = this.A09;
                r43.A0B.Ab2(new Runnable(1, Integer.valueOf(r43.A04.A02(1782))) { // from class: X.6Jb
                    public final /* synthetic */ Integer A01;
                    public final /* synthetic */ Integer A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        C123505nG r52 = C123505nG.this;
                        Integer num = this.A01;
                        Integer num2 = this.A02;
                        C18600si r2 = ((AbstractC118085bF) r52).A06;
                        r2.A0C(((AbstractC118085bF) r52).A04.A00());
                        r2.A0A(1);
                        r52.A07.A00(new C1328768o(r52, num, num2), num, num2, null);
                    }
                });
            }
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AbstractC136326Mc
    public String AEP(AbstractC28901Pl r2) {
        C119755f3 r0 = (C119755f3) r2.A08;
        if (r0 == null || C12970iu.A1Y(r0.A05.A00)) {
            return super.AEP(r2);
        }
        return A0I(R.string.setup_pin_prompt);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r1.A0N(r1.A07()) == false) goto L_0x001e;
     */
    @Override // X.AbstractC136336Md
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ALz(boolean r9) {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r5 = "extra_skip_value_props_display"
            java.lang.String r4 = "extra_is_first_payment_method"
            r7 = 5
            java.lang.String r3 = "extra_payments_entry_type"
            if (r9 != 0) goto L_0x001e
            X.68y r0 = r8.A03
            boolean r0 = r0.A0L()
            if (r0 == 0) goto L_0x0040
            X.68y r1 = r8.A03
            java.lang.String r0 = r1.A07()
            boolean r0 = r1.A0N(r0)
            if (r0 != 0) goto L_0x0040
        L_0x001e:
            android.content.Context r1 = r8.A0p()
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity> r0 = com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity.class
            android.content.Intent r2 = X.C12990iw.A0D(r1, r0)
            r1 = 2
            java.lang.String r0 = "extra_setup_mode"
            r2.putExtra(r0, r1)
            r2.putExtra(r3, r7)
            r2.putExtra(r4, r9)
            r2.putExtra(r5, r6)
            java.lang.String r0 = "settingsAddPayment"
            X.C35741ib.A00(r2, r0)
            r8.A0v(r2)
            return
        L_0x0040:
            android.content.Context r1 = r8.A0p()
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiBankPickerActivity> r0 = com.whatsapp.payments.ui.IndiaUpiBankPickerActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r1, r0)
            r1.putExtra(r3, r7)
            r0 = 1
            r1.putExtra(r5, r0)
            r1.putExtra(r4, r6)
            r0 = 1008(0x3f0, float:1.413E-42)
            r8.startActivityForResult(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiPaymentSettingsFragment.ALz(boolean):void");
    }

    @Override // X.AnonymousClass5UV
    public void AOr(String str) {
        TransactionsExpandableView transactionsExpandableView = this.A0y;
        transactionsExpandableView.post(new Runnable() { // from class: X.6HZ
            @Override // java.lang.Runnable
            public final void run() {
                TransactionsExpandableView transactionsExpandableView2 = TransactionsExpandableView.this;
                for (int i = 0; i < transactionsExpandableView2.getChildCount(); i++) {
                    AbstractC136376Mh r0 = (AbstractC136376Mh) transactionsExpandableView2.A05.getChildAt(i);
                    if (r0 != null) {
                        r0.AaB();
                    }
                }
            }
        });
        TransactionsExpandableView transactionsExpandableView2 = this.A0x;
        transactionsExpandableView2.post(new Runnable() { // from class: X.6HZ
            @Override // java.lang.Runnable
            public final void run() {
                TransactionsExpandableView transactionsExpandableView2 = TransactionsExpandableView.this;
                for (int i = 0; i < transactionsExpandableView2.getChildCount(); i++) {
                    AbstractC136376Mh r0 = (AbstractC136376Mh) transactionsExpandableView2.A05.getChildAt(i);
                    if (r0 != null) {
                        r0.AaB();
                    }
                }
            }
        });
    }

    @Override // X.AbstractC136336Md
    public void ATY(AbstractC28901Pl r3) {
        Intent A0D = C12990iw.A0D(A0p(), IndiaUpiBankAccountDetailsActivity.class);
        C117315Zl.A0M(A0D, r3);
        startActivityForResult(A0D, 1009);
    }

    @Override // X.AnonymousClass6M6
    public void Abh(boolean z) {
        AbstractC1308360d r0;
        View view = ((AnonymousClass01E) this).A0A;
        if (view != null) {
            ViewGroup A04 = C117315Zl.A04(view, R.id.action_required_container);
            int i = 0;
            if (this.A00 == null && (r0 = this.A0r) != null) {
                if (r0.A0C.A02() != null) {
                    ((PaymentSettingsFragment) this).A0T.A04(AnonymousClass4EV.A00(((PaymentSettingsFragment) this).A0Q, this.A0r.A0C.A02()));
                }
                if (!((PaymentSettingsFragment) this).A0T.A02().isEmpty()) {
                    A04.removeAllViews();
                    C117785ad r4 = new C117785ad(A01());
                    List A02 = ((PaymentSettingsFragment) this).A0T.A02();
                    r4.A00(new C129855yP(new AnonymousClass68R(this), (C460124c) AnonymousClass01Y.A05(A02).get(0), A02.size()));
                    A04.addView(r4);
                    this.A00 = A04;
                }
            }
            if (!z) {
                i = 8;
            }
            A04.setVisibility(i);
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass6M5
    public void AfR(List list) {
        String str;
        super.AfR(list);
        if (A0c() && A0B() != null) {
            C117735aW r3 = new C117735aW(A01());
            r3.setBackgroundColor(A02().getColor(R.color.primary_surface));
            r3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            C117295Zj.A0n(r3.A05, this, 50);
            C117295Zj.A0n(r3.A04, this, 49);
            ((PaymentSettingsFragment) this).A0B.removeAllViews();
            if (((PaymentSettingsFragment) this).A0W.A0A() || this.A03.A0L()) {
                List list2 = this.A0p.A02;
                if (list2 == null || list2.isEmpty()) {
                    str = null;
                } else {
                    str = C1311161i.A09(list2);
                }
                String A00 = C1329668y.A00(this.A03);
                if (TextUtils.isEmpty(str)) {
                    str = this.A02.A00.getString("push_name", "");
                    ((PaymentSettingsFragment) this).A0d.A08(null, 1);
                }
                boolean z = false;
                if (((PaymentSettingsFragment) this).A0S.A07(1458)) {
                    String A03 = ((PaymentSettingsFragment) this).A0S.A03(1459);
                    String A07 = this.A03.A07();
                    if (!TextUtils.isEmpty(A03) && !TextUtils.isEmpty(A07) && A03.contains(this.A03.A07())) {
                        z = true;
                    }
                }
                C15570nT r0 = ((PaymentSettingsFragment) this).A0G;
                r0.A08();
                C27621Ig r02 = r0.A01;
                if (z) {
                    r3.A00(r02, str, A00);
                    ImageView imageView = r3.A01;
                    imageView.setVisibility(0);
                    imageView.setColorFilter(r3.getResources().getColor(R.color.payment_hub_expand_profile_row));
                    TypedValue typedValue = new TypedValue();
                    r3.getContext().getTheme().resolveAttribute(16843534, typedValue, true);
                    LinearLayout linearLayout = r3.A03;
                    linearLayout.setBackgroundResource(typedValue.resourceId);
                    linearLayout.setOnClickListener(new View.OnClickListener(str) { // from class: X.647
                        public final /* synthetic */ String A01;

                        {
                            this.A01 = r2;
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment = IndiaUpiPaymentSettingsFragment.this;
                            String str2 = this.A01;
                            indiaUpiPaymentSettingsFragment.A07.AKg(C12960it.A0V(), 129, "payment_home", null);
                            ActivityC000900k A0C = indiaUpiPaymentSettingsFragment.A0C();
                            AnonymousClass1ZR A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, str2, "accountHolderName");
                            Intent A0D = C12990iw.A0D(A0C, IndiaUpiProfileDetailsActivity.class);
                            A0D.putExtra("extra_payment_name", A0I);
                            A0D.putExtra("extra_referral_screen", "payment_home");
                            indiaUpiPaymentSettingsFragment.A0v(A0D);
                        }
                    });
                } else {
                    r3.A00(r02, str, A00);
                    r3.A03.setOnLongClickListener(new View.OnLongClickListener(A00) { // from class: X.64p
                        public final /* synthetic */ String A01;

                        {
                            this.A01 = r2;
                        }

                        @Override // android.view.View.OnLongClickListener
                        public final boolean onLongClick(View view) {
                            IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment = IndiaUpiPaymentSettingsFragment.this;
                            String str2 = this.A01;
                            ClipboardManager A0B = ((PaymentSettingsFragment) indiaUpiPaymentSettingsFragment).A0O.A0B();
                            if (A0B != null) {
                                try {
                                    A0B.setPrimaryClip(ClipData.newPlainText(str2, str2));
                                    ((PaymentSettingsFragment) indiaUpiPaymentSettingsFragment).A0F.A0E(indiaUpiPaymentSettingsFragment.A0I(R.string.vpa_copied_to_clipboard), 1);
                                    return true;
                                } catch (NullPointerException | SecurityException e) {
                                    Log.e("indiaupi/clipboard/", e);
                                }
                            }
                            return true;
                        }
                    });
                }
            }
            ((PaymentSettingsFragment) this).A0B.addView(r3);
            ((PaymentSettingsFragment) this).A0B.setVisibility(0);
            ((PaymentSettingsFragment) this).A06.setVisibility(0);
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AbstractC136346Me
    public void AfY(List list) {
        this.A08.A04(list);
        super.AfY(list);
        AbstractC118085bF r2 = this.A0t;
        if (r2 != null) {
            r2.A02 = list;
            r2.A07(this.A0m, this.A0v);
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AbstractC136346Me
    public void Afa(List list) {
        this.A0r.A02();
        this.A08.A04(list);
        super.Afa(list);
        AbstractC118085bF r2 = this.A0t;
        if (r2 != null) {
            r2.A03 = list;
            r2.A07(this.A0m, this.A0v);
        }
    }
}
