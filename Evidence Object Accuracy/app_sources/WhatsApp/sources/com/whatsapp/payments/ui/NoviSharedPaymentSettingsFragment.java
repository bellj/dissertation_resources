package com.whatsapp.payments.ui;

import X.AbstractC118085bF;
import X.AbstractC28901Pl;
import X.AbstractC460624h;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass102;
import X.AnonymousClass12P;
import X.AnonymousClass1BZ;
import X.AnonymousClass600;
import X.AnonymousClass608;
import X.AnonymousClass60Y;
import X.AnonymousClass613;
import X.AnonymousClass61F;
import X.AnonymousClass68H;
import X.AnonymousClass6BD;
import X.AnonymousClass6JD;
import X.C117295Zj;
import X.C119855fD;
import X.C126995tm;
import X.C128375w0;
import X.C129105xB;
import X.C129235xO;
import X.C12960it;
import X.C129685y8;
import X.C12970iu;
import X.C12980iv;
import X.C129895yT;
import X.C12990iw;
import X.C13000ix;
import X.C130095yn;
import X.C130105yo;
import X.C130125yq;
import X.C130155yt;
import X.C130295zB;
import X.C1315463e;
import X.C1316263m;
import X.C134306Ec;
import X.C14580lf;
import X.C14820m6;
import X.C20920wX;
import X.C22980zx;
import X.C25841Ba;
import X.C25861Bc;
import X.C27691It;
import X.C29941Vi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewStub;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviSharedPaymentSettingsFragment;
import com.whatsapp.payments.ui.PaymentSettingsFragment;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;

/* loaded from: classes4.dex */
public class NoviSharedPaymentSettingsFragment extends Hilt_NoviSharedPaymentSettingsFragment {
    public ShimmerFrameLayout A00;
    public C20920wX A01;
    public AnonymousClass12P A02;
    public C25841Ba A03;
    public AnonymousClass1BZ A04;
    public C14820m6 A05;
    public AnonymousClass102 A06;
    public C25861Bc A07;
    public C130155yt A08;
    public AnonymousClass600 A09;
    public C130125yq A0A;
    public C129895yT A0B;
    public AnonymousClass60Y A0C;
    public AnonymousClass6BD A0D;
    public AnonymousClass61F A0E;
    public C130105yo A0F;
    public C129235xO A0G;
    public C129685y8 A0H;
    public C130095yn A0I;
    public C129105xB A0J;
    public C128375w0 A0K;
    public C119855fD A0L;
    public C22980zx A0M;
    public final AbstractC460624h A0N = new AbstractC460624h() { // from class: X.69J
        @Override // X.AbstractC460624h
        public final void ATX(AbstractC28901Pl r4, AnonymousClass1V8 r5) {
            NoviSharedPaymentSettingsFragment noviSharedPaymentSettingsFragment = NoviSharedPaymentSettingsFragment.this;
            if (r4 != null) {
                AnonymousClass1ZY r1 = r4.A08;
                if ((r1 instanceof C119805f8) && ((PaymentSettingsFragment) noviSharedPaymentSettingsFragment).A0f.A04()) {
                    noviSharedPaymentSettingsFragment.A1U(((C119805f8) r1).A02.A01, true);
                }
            }
        }
    };

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136336Md
    public void ALz(boolean z) {
    }

    @Override // X.AbstractC136336Md
    public void ATY(AbstractC28901Pl r1) {
    }

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return true;
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A11() {
        super.A11();
        AnonymousClass61F r1 = this.A0E;
        C27691It r0 = r1.A03;
        if (r0 == null) {
            r0 = C13000ix.A03();
            r1.A03 = r0;
        }
        r0.A04(this);
        this.A07.A04(this.A0N);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
        C129895yT A03 = this.A0F.A03();
        if (A03 != null) {
            C129685y8 r4 = this.A0H;
            AnonymousClass016 A0T = C12980iv.A0T();
            r4.A05.Ab2(new AnonymousClass6JD(A0T, r4, 6));
            C117295Zj.A0r(this, A0T, 117);
        }
        A1V(A03);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        if (((PaymentSettingsFragment) this).A0f.A03()) {
            ViewStub viewStub = (ViewStub) view.findViewById(R.id.pay_hub_ext);
            viewStub.setLayoutResource(R.layout.pay_hub_row);
            viewStub.inflate().setVisibility(0);
            C12990iw.A1H(C12960it.A0J(view, R.id.pay_hub_title), this, R.string.novi_title);
            C12970iu.A0L(view, R.id.pay_hub_icon).setImageResource(C130105yo.A00(this.A0F));
            ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.pay_hub_desc_shimmer);
            this.A00 = shimmerFrameLayout;
            shimmerFrameLayout.A00();
            A1U(null, false);
            AnonymousClass61F r1 = this.A0E;
            C27691It r0 = r1.A03;
            if (r0 == null) {
                r0 = C13000ix.A03();
                r1.A03 = r0;
            }
            r0.A04(this);
            AnonymousClass61F r3 = this.A0E;
            C27691It r2 = r3.A03;
            if (r2 == null) {
                r2 = C13000ix.A03();
                r3.A03 = r2;
            }
            C14580lf A04 = r3.A04();
            A04.A00(new C134306Ec(r2, A04, r3));
            C117295Zj.A0r(this, r2, 116);
        }
        A1T(((AnonymousClass01E) this).A05);
        this.A07.A03(this.A0N);
        if (this.A05.A1J("novi_invite_asset_last_sync_timestamp", 1209600000)) {
            this.A0L.A0A(new AnonymousClass68H(this));
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment
    public void A1L(Intent intent) {
        super.A1L(intent);
        if (!((PaymentSettingsFragment) this).A0f.A04() || !intent.hasExtra("extra_account_removed") || !intent.getBooleanExtra("extra_account_removed", false)) {
            A1T(intent.getExtras());
            return;
        }
        A1U(null, false);
        ((PaymentSettingsFragment) this).A0F.A05(R.string.novi_account_removed_message, 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0060, code lost:
        if (r1 == null) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1T(android.os.Bundle r6) {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x0050
            java.lang.String r4 = "action"
            boolean r0 = r6.containsKey(r4)
            if (r0 == 0) goto L_0x0050
            java.lang.String r3 = r6.getString(r4)
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0067
            java.lang.String r0 = "reset_password"
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x0067
            X.0zW r0 = r5.A0f
            boolean r0 = r0.A04()
            if (r0 == 0) goto L_0x0050
            X.61F r0 = r5.A0E
            X.1It r0 = r0.A03
            if (r0 != 0) goto L_0x0051
            r0 = 0
        L_0x002b:
            java.lang.String r1 = "tpp_account_link"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0058
            X.60Y r1 = r5.A0C
            java.lang.String r0 = "DEEP_LINK"
            r1.A04 = r0
            X.5xB r3 = r5.A0J
            X.00k r2 = r5.A0B()
            java.lang.String r1 = r6.getString(r4)
            java.lang.String r0 = "access_code"
            java.lang.String r0 = r6.getString(r0)
            android.content.Intent r0 = r3.A00(r2, r1, r0)
            r5.A0v(r0)
        L_0x0050:
            return
        L_0x0051:
            java.lang.Object r0 = r0.A01()
            X.1Pl r0 = (X.AbstractC28901Pl) r0
            goto L_0x002b
        L_0x0058:
            r2 = 1
            if (r0 == 0) goto L_0x0062
            X.63e r1 = X.AnonymousClass61F.A02(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0063
        L_0x0062:
            r0 = 0
        L_0x0063:
            r5.A1W(r3, r0, r2)
            return
        L_0x0067:
            android.content.Context r1 = r5.A01()
            java.lang.Class<com.whatsapp.payments.ui.NoviPayBloksActivity> r0 = com.whatsapp.payments.ui.NoviPayBloksActivity.class
            android.content.Intent r2 = X.C12990iw.A0D(r1, r0)
            java.lang.String r1 = "screen_name"
            java.lang.String r0 = "novipay_p_reset_password"
            r2.putExtra(r1, r0)
            r5.A0v(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviSharedPaymentSettingsFragment.A1T(android.os.Bundle):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0040, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        X.C12980iv.A14(A02(), r2, com.whatsapp.R.color.settings_item_subtitle_text);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0051, code lost:
        if (r12.A08.A01 == false) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0053, code lost:
        r8.setOnClickListener(new X.AnonymousClass648(r12, r14));
        r7.setVisibility(8);
        r6.setVisibility(0);
        r5.setVisibility(8);
        r2.setVisibility(0);
        r2.setText(com.whatsapp.R.string.novi_force_update_app_update_alert_text);
        X.C12980iv.A14(A02(), r2, com.whatsapp.R.color.settings_alert_tint);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0077, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0078, code lost:
        if (r14 == false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007a, code lost:
        if (r13 == null) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007c, code lost:
        if (r10 != false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        X.C117295Zj.A0n(r8, r12, 89);
        r7.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0086, code lost:
        if (r9 == false) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0088, code lost:
        r6.setVisibility(8);
        r5.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
        r2.setVisibility(0);
        r7 = r13.A02;
        r2.setText(r7.A00.AA7(A01(), X.C12970iu.A0q(r12, r7.A06(((com.whatsapp.payments.ui.PaymentSettingsFragment) r12).A0R), new java.lang.Object[1], 0, com.whatsapp.R.string.novi_balance_with_value)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00af, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b0, code lost:
        r6.setVisibility(0);
        r5.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b7, code lost:
        X.C117295Zj.A0n(r8, r12, 88);
        X.C12990iw.A1H(r2, r12, com.whatsapp.R.string.novi_tap_to_view_balance);
        r7.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c5, code lost:
        if (r10 == false) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c7, code lost:
        r6.setVisibility(8);
        r2.setVisibility(8);
        r5.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d0, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d1, code lost:
        r5.setVisibility(8);
        r6.setVisibility(0);
        r2.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00da, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00db, code lost:
        X.C117295Zj.A0n(r8, r12, 90);
        r1 = ((com.whatsapp.payments.ui.PaymentSettingsFragment) r12).A0S.A07(1230);
        r0 = com.whatsapp.R.string.novi_description_without_cross_country;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00eb, code lost:
        if (r1 == false) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ed, code lost:
        r0 = com.whatsapp.R.string.novi_description;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00f0, code lost:
        r2.setText(r0);
        r2.setVisibility(0);
        r7.setVisibility(0);
        r6.setVisibility(8);
        r5.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ff, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003a, code lost:
        if (r11.A04.contains("READ_DISABLED") == false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003e, code lost:
        if (r11 != null) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1U(X.C1316263m r13, boolean r14) {
        /*
            r12 = this;
            android.view.View r1 = r12.A0A
            if (r1 == 0) goto L_0x0077
            r0 = 2131364880(0x7f0a0c10, float:1.834961E38)
            android.view.View r8 = r1.findViewById(r0)
            r0 = 2131364881(0x7f0a0c11, float:1.8349612E38)
            android.widget.TextView r2 = X.C12960it.A0J(r1, r0)
            r0 = 2131364877(0x7f0a0c0d, float:1.8349603E38)
            android.view.View r7 = r1.findViewById(r0)
            r0 = 2131364879(0x7f0a0c0f, float:1.8349608E38)
            android.view.View r6 = r1.findViewById(r0)
            r0 = 2131364878(0x7f0a0c0e, float:1.8349605E38)
            android.view.View r5 = r1.findViewById(r0)
            X.5yo r0 = r12.A0F
            X.5yT r11 = r0.A03()
            r1 = 1
            r4 = 0
            if (r11 == 0) goto L_0x003c
            java.util.List r3 = r11.A04
            java.lang.String r0 = "READ_DISABLED"
            boolean r0 = r3.contains(r0)
            r10 = 1
            if (r0 != 0) goto L_0x0040
        L_0x003c:
            r10 = 0
            r9 = 0
            if (r11 == 0) goto L_0x0041
        L_0x0040:
            r9 = 1
        L_0x0041:
            android.content.res.Resources r3 = r12.A02()
            r0 = 2131100876(0x7f0604cc, float:1.7814146E38)
            X.C12980iv.A14(r3, r2, r0)
            X.5yt r0 = r12.A08
            boolean r0 = r0.A01
            r3 = 8
            if (r0 == 0) goto L_0x0078
            X.648 r0 = new X.648
            r0.<init>(r14)
            r8.setOnClickListener(r0)
            r7.setVisibility(r3)
            r6.setVisibility(r4)
            r5.setVisibility(r3)
            r2.setVisibility(r4)
            r0 = 2131889775(0x7f120e6f, float:1.9414223E38)
            r2.setText(r0)
            android.content.res.Resources r1 = r12.A02()
            r0 = 2131100853(0x7f0604b5, float:1.78141E38)
            X.C12980iv.A14(r1, r2, r0)
        L_0x0077:
            return
        L_0x0078:
            if (r14 == 0) goto L_0x00db
            if (r13 == 0) goto L_0x00b7
            if (r10 != 0) goto L_0x00db
            r0 = 89
            X.C117295Zj.A0n(r8, r12, r0)
            r7.setVisibility(r3)
            if (r9 == 0) goto L_0x00b0
            r6.setVisibility(r3)
            r5.setVisibility(r4)
        L_0x008e:
            r2.setVisibility(r4)
            X.6F2 r7 = r13.A02
            X.1Yv r6 = r7.A00
            android.content.Context r5 = r12.A01()
            r3 = 2131889721(0x7f120e39, float:1.9414114E38)
            java.lang.Object[] r1 = new java.lang.Object[r1]
            X.018 r0 = r12.A0R
            java.lang.String r0 = r7.A06(r0)
            java.lang.String r0 = X.C12970iu.A0q(r12, r0, r1, r4, r3)
            java.lang.CharSequence r0 = r6.AA7(r5, r0)
            r2.setText(r0)
            return
        L_0x00b0:
            r6.setVisibility(r4)
            r5.setVisibility(r3)
            goto L_0x008e
        L_0x00b7:
            r0 = 88
            X.C117295Zj.A0n(r8, r12, r0)
            r0 = 2131889964(0x7f120f2c, float:1.9414606E38)
            X.C12990iw.A1H(r2, r12, r0)
            r7.setVisibility(r3)
            if (r10 == 0) goto L_0x00d1
            r6.setVisibility(r3)
            r2.setVisibility(r3)
            r5.setVisibility(r4)
            return
        L_0x00d1:
            r5.setVisibility(r3)
            r6.setVisibility(r4)
            r2.setVisibility(r4)
            return
        L_0x00db:
            r0 = 90
            X.C117295Zj.A0n(r8, r12, r0)
            X.0m9 r1 = r12.A0S
            r0 = 1230(0x4ce, float:1.724E-42)
            boolean r1 = r1.A07(r0)
            r0 = 2131889764(0x7f120e64, float:1.94142E38)
            if (r1 == 0) goto L_0x00f0
            r0 = 2131889763(0x7f120e63, float:1.9414199E38)
        L_0x00f0:
            r2.setText(r0)
            r2.setVisibility(r4)
            r7.setVisibility(r4)
            r6.setVisibility(r3)
            r5.setVisibility(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviSharedPaymentSettingsFragment.A1U(X.63m, boolean):void");
    }

    public final void A1V(C129895yT r3) {
        C1316263m r0;
        if (!C29941Vi.A00(this.A0B, r3)) {
            if (r3 != null) {
                if (r3.A04.contains("READ_DISABLED")) {
                    r0 = null;
                } else {
                    r0 = r3.A02;
                }
                A1U(r0, true);
            }
            this.A0B = r3;
        }
    }

    public final void A1W(String str, boolean z, boolean z2) {
        String str2;
        Class cls;
        String string;
        String str3;
        if (this.A08.A01) {
            C130295zB.A00(A0C(), C126995tm.A00(new Runnable() { // from class: X.6Gr
                @Override // java.lang.Runnable
                public final void run() {
                    try {
                        NoviSharedPaymentSettingsFragment.this.A0C().startActivity(C117295Zj.A04("market://details?id=com.whatsapp"));
                    } catch (ActivityNotFoundException unused) {
                        Log.e("[PAY] : GP store is not available");
                    }
                }
            }, R.string.upgrade), C126995tm.A00(null, R.string.not_now), A0I(R.string.payments_upgrade_error), A0I(R.string.payments_upgrade_error_message), false).show();
            return;
        }
        Bundle A0D = C12970iu.A0D();
        if (!this.A0E.A0D() || !this.A0E.A0E() || !this.A0E.A0F()) {
            HashMap hashMap = new HashMap(10);
            if (z) {
                if (this.A0E.A0D()) {
                    AnonymousClass61F r1 = this.A0E;
                    C1315463e r0 = r1.A01;
                    if (r0 != null) {
                        string = r0.A03;
                    } else {
                        string = r1.A0F.A02().getString("wavi_kyc_status", "NOT_READY_FOR_ASSESSMENT");
                        AnonymousClass009.A05(string);
                    }
                    if (string.equals("NOT_READY_FOR_ASSESSMENT")) {
                        hashMap.put("onboarding_app_flow_type", "RESUME_ONBOARDING");
                        str2 = "novipay_p_resume_onboarding";
                    } else if (string.equals("APPROVE")) {
                        str2 = "novipay_p_onboarding_success";
                    }
                }
                str2 = "novipay_p_login_password";
            } else {
                hashMap.put("onboarding_app_flow_type", "ONBOARDING");
                str2 = "novipay_p_connect_account_education";
            }
            cls = NoviPayBloksActivity.class;
            A0D.putString("screen_name", str2);
            hashMap.put("login_entry_point", "payment_settings_hub_row");
            A0D.putSerializable("screen_params", hashMap);
            if ("novipay_p_login_password".equals(str2)) {
                A0D.putInt("login_entry_point", 1);
            } else {
                A0D.putInt("login_entry_point", 6);
            }
        } else {
            cls = NoviPayHubActivity.class;
        }
        Intent A0D2 = C12990iw.A0D(A01(), cls);
        A0D2.putExtras(A0D);
        if (str != null && !TextUtils.isEmpty(str)) {
            A0D2.putExtra("action", str);
        }
        AnonymousClass60Y r12 = this.A0C;
        if (z2) {
            str3 = "DEEP_LINK";
        } else {
            str3 = "PAYMENTS";
        }
        r12.A04 = str3;
        A0v(A0D2);
        this.A0D.AKg(1, 68, "payment_home", null);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AbstractC136346Me
    public void AfY(List list) {
        List A03 = AnonymousClass613.A03(list);
        if (A03.size() > 3) {
            A03 = A03.subList(0, 3);
        }
        super.AfY(A03);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.payment_support_container) {
            this.A02.A06(A0p(), AnonymousClass608.A00(((PaymentSettingsFragment) this).A0R));
            AbstractC118085bF r1 = this.A0t;
            if (r1 != null) {
                r1.A06(null);
                return;
            }
            return;
        }
        super.onClick(view);
    }
}
