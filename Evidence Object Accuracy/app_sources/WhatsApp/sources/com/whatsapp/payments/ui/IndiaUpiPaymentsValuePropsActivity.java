package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121495iO;
import X.AbstractActivityC121665jA;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass064;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C12970iu;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.TextSwitcher;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentsValuePropsActivity extends AbstractActivityC121495iO {
    public TextSwitcher A00;
    public boolean A01;

    public IndiaUpiPaymentsValuePropsActivity() {
        this(0);
    }

    public IndiaUpiPaymentsValuePropsActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 61);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1U(A09, A1M, this);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass064 r2 = (AnonymousClass064) this.A00.getLayoutParams();
        r2.A0Y = (int) getResources().getDimension(R.dimen.payments_value_props_desc_max_width);
        this.A00.setLayoutParams(r2);
    }

    @Override // X.AbstractActivityC121495iO, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_payment_value_props);
        A2u(R.string.payments_activity_title, R.color.reg_title_color, R.id.payments_value_props_title_and_description_section);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.payments_activity_title);
        }
        TextView A0M = C12970iu.A0M(this, R.id.payments_value_props_title);
        boolean A07 = ((ActivityC13810kN) this).A0C.A07(1568);
        int i = R.string.payments_value_props_title_text;
        if (A07) {
            i = R.string.payments_value_props_title_text_v2;
        }
        A0M.setText(i);
        TextSwitcher textSwitcher = (TextSwitcher) findViewById(R.id.payments_value_props_desc);
        this.A00 = textSwitcher;
        A32(textSwitcher);
        C117295Zj.A0n(findViewById(R.id.payments_value_props_continue), this, 57);
        ((AbstractActivityC121665jA) this).A0C.A09();
    }
}
