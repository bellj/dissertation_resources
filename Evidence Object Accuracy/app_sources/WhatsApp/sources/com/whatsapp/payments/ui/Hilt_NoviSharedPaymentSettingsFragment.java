package com.whatsapp.payments.ui;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.AnonymousClass1BZ;
import X.AnonymousClass600;
import X.AnonymousClass6BD;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119855fD;
import X.C129105xB;
import X.C129235xO;
import X.C129685y8;
import X.C12980iv;
import X.C130095yn;
import X.C130105yo;
import X.C130125yq;
import X.C130155yt;
import X.C14820m6;
import X.C20910wW;
import X.C22980zx;
import X.C25841Ba;
import X.C25861Bc;
import X.C51062Sr;
import X.C51082St;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;

/* loaded from: classes4.dex */
public abstract class Hilt_NoviSharedPaymentSettingsFragment extends PaymentSettingsFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1S();
        return this.A00;
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1S()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.Hilt_NoviSharedPaymentSettingsFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1S();
        A1I();
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment
    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            NoviSharedPaymentSettingsFragment noviSharedPaymentSettingsFragment = (NoviSharedPaymentSettingsFragment) this;
            AnonymousClass01J A0C = C117295Zj.A0C(noviSharedPaymentSettingsFragment, (AbstractC51092Su) generatedComponent());
            C117295Zj.A16(A0C, noviSharedPaymentSettingsFragment);
            C117295Zj.A17(A0C, noviSharedPaymentSettingsFragment, C117295Zj.A0Q(A0C, noviSharedPaymentSettingsFragment));
            C117295Zj.A15(A0C, noviSharedPaymentSettingsFragment);
            noviSharedPaymentSettingsFragment.A02 = C12980iv.A0W(A0C);
            noviSharedPaymentSettingsFragment.A09 = (AnonymousClass600) A0C.ADB.get();
            noviSharedPaymentSettingsFragment.A0C = C117305Zk.A0W(A0C);
            noviSharedPaymentSettingsFragment.A01 = C20910wW.A00();
            noviSharedPaymentSettingsFragment.A03 = (C25841Ba) A0C.A1d.get();
            noviSharedPaymentSettingsFragment.A08 = (C130155yt) A0C.ADA.get();
            noviSharedPaymentSettingsFragment.A04 = (AnonymousClass1BZ) A0C.A1l.get();
            noviSharedPaymentSettingsFragment.A0E = C117305Zk.A0X(A0C);
            noviSharedPaymentSettingsFragment.A0H = (C129685y8) A0C.ADa.get();
            noviSharedPaymentSettingsFragment.A0M = (C22980zx) A0C.AFH.get();
            noviSharedPaymentSettingsFragment.A05 = (C14820m6) A0C.AN3.get();
            noviSharedPaymentSettingsFragment.A0G = (C129235xO) A0C.ADR.get();
            noviSharedPaymentSettingsFragment.A0F = (C130105yo) A0C.ADZ.get();
            noviSharedPaymentSettingsFragment.A0J = (C129105xB) A0C.ADb.get();
            noviSharedPaymentSettingsFragment.A0A = (C130125yq) A0C.ADJ.get();
            noviSharedPaymentSettingsFragment.A06 = C117305Zk.A0G(A0C);
            noviSharedPaymentSettingsFragment.A0L = (C119855fD) A0C.ADO.get();
            noviSharedPaymentSettingsFragment.A0I = (C130095yn) A0C.ADc.get();
            noviSharedPaymentSettingsFragment.A0D = (AnonymousClass6BD) A0C.ADT.get();
            noviSharedPaymentSettingsFragment.A07 = (C25861Bc) A0C.AEh.get();
            noviSharedPaymentSettingsFragment.A0K = C117315Zl.A0E(A0C);
        }
    }

    public final void A1S() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }
}
