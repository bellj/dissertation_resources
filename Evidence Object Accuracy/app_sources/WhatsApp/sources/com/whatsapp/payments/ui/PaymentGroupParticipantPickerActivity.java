package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.AbstractC16870pt;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass5oZ;
import X.AnonymousClass61I;
import X.AnonymousClass65P;
import X.C117295Zj;
import X.C117305Zk;
import X.C117585aE;
import X.C119665eq;
import X.C124055oN;
import X.C126985tl;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C15600nX;
import X.C15610nY;
import X.C16590pI;
import X.C17070qD;
import X.C21270x9;
import X.C21860y6;
import X.C22710zW;
import X.C238013b;
import X.C27131Gd;
import X.C48232Fc;
import X.C74503iB;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class PaymentGroupParticipantPickerActivity extends ActivityC13790kL {
    public ListView A00;
    public C48232Fc A01;
    public C238013b A02;
    public C15550nR A03;
    public AnonymousClass10S A04;
    public C15610nY A05;
    public AnonymousClass1J1 A06;
    public C21270x9 A07;
    public C16590pI A08;
    public C15600nX A09;
    public GroupJid A0A;
    public C21860y6 A0B;
    public C22710zW A0C;
    public C17070qD A0D;
    public AnonymousClass5oZ A0E;
    public C117585aE A0F;
    public C124055oN A0G;
    public C74503iB A0H;
    public AnonymousClass12F A0I;
    public String A0J;
    public ArrayList A0K;
    public boolean A0L;
    public final C27131Gd A0M;
    public final ArrayList A0N;

    public PaymentGroupParticipantPickerActivity() {
        this(0);
        this.A0N = C12960it.A0l();
        this.A0M = new C119665eq(this);
    }

    public PaymentGroupParticipantPickerActivity(int i) {
        this.A0L = false;
        C117295Zj.A0p(this, 102);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A08 = (C16590pI) A1M.AMg.get();
            this.A07 = C12970iu.A0W(A1M);
            this.A03 = C12960it.A0O(A1M);
            this.A05 = C12960it.A0P(A1M);
            this.A0D = C117305Zk.A0P(A1M);
            this.A02 = (C238013b) A1M.A1Z.get();
            this.A04 = (AnonymousClass10S) A1M.A46.get();
            this.A0I = (AnonymousClass12F) A1M.AJM.get();
            this.A0B = (C21860y6) A1M.AE6.get();
            this.A0C = C117305Zk.A0O(A1M);
            this.A09 = (C15600nX) A1M.A8x.get();
        }
    }

    public final void A2e(UserJid userJid) {
        Intent A0D = C12990iw.A0D(this.A08.A00, this.A0D.A02().AGb());
        A0D.putExtra("extra_jid", this.A0A.getRawString());
        A0D.putExtra("extra_receiver_jid", C15380n4.A03(userJid));
        finish();
        startActivity(A0D);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A01.A05()) {
            this.A01.A04(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        C126985tl r0 = (C126985tl) this.A00.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        if (r0 != null) {
            C15370n3 r2 = r0.A00;
            if (menuItem.getItemId() == 0) {
                C238013b r1 = this.A02;
                Jid A0B = r2.A0B(UserJid.class);
                AnonymousClass009.A05(A0B);
                r1.A0C(this, (UserJid) A0B);
                return true;
            }
        }
        return super.onContextItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C117295Zj.A0d(this);
        super.onCreate(bundle);
        this.A0H = C117305Zk.A0Z(this);
        this.A06 = this.A07.A04(this, "payment-group-participant-picker");
        setContentView(R.layout.payment_group_participant_picker);
        this.A0A = GroupJid.getNullable(getIntent().getStringExtra("extra_jid"));
        Intent intent = getIntent();
        if (intent != null) {
            this.A0J = intent.getStringExtra("referral_screen");
        }
        this.A0F = new C117585aE(this, this, this.A0N);
        ListView listView = (ListView) findViewById(R.id.group_participant_picker_list);
        this.A00 = listView;
        listView.setAdapter((ListAdapter) this.A0F);
        this.A00.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.65B
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = PaymentGroupParticipantPickerActivity.this;
                C126985tl r0 = ((C127885vD) view.getTag()).A04;
                if (r0 != null) {
                    C15370n3 r3 = r0.A00;
                    UserJid A05 = C15370n3.A05(r3);
                    int A00 = paymentGroupParticipantPickerActivity.A0C.A00(A05);
                    if (!paymentGroupParticipantPickerActivity.A02.A0I(A05) && A00 == 2) {
                        AnonymousClass009.A05(A05);
                        AnonymousClass3FP r32 = new AnonymousClass3FP(paymentGroupParticipantPickerActivity, paymentGroupParticipantPickerActivity, ((ActivityC13810kN) paymentGroupParticipantPickerActivity).A05, paymentGroupParticipantPickerActivity.A0D, paymentGroupParticipantPickerActivity.A0H, 
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003a: CONSTRUCTOR  (r3v1 'r32' X.3FP) = 
                              (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity)
                              (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity)
                              (wrap: X.0mE : 0x0026: IGET  (r6v0 X.0mE A[REMOVE]) = 
                              (wrap: ?? : ?: CAST (X.0kN) (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity))
                             X.0kN.A05 X.0mE)
                              (wrap: X.0qD : 0x0028: IGET  (r7v0 X.0qD A[REMOVE]) = (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity) com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity.A0D X.0qD)
                              (wrap: X.3iB : 0x002a: IGET  (r8v0 X.3iB A[REMOVE]) = (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity) com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity.A0H X.3iB)
                              (wrap: X.6IV : 0x002e: CONSTRUCTOR  (r9v0 X.6IV A[REMOVE]) = 
                              (r2v0 'A05' com.whatsapp.jid.UserJid)
                              (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity)
                             call: X.6IV.<init>(com.whatsapp.jid.UserJid, com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity):void type: CONSTRUCTOR)
                              (wrap: X.6JZ : 0x0033: CONSTRUCTOR  (r10v0 X.6JZ A[REMOVE]) = 
                              (r3v0 'r3' X.0n3)
                              (r2v0 'A05' com.whatsapp.jid.UserJid)
                              (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity)
                             call: X.6JZ.<init>(X.0n3, com.whatsapp.jid.UserJid, com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity):void type: CONSTRUCTOR)
                              false
                             call: X.3FP.<init>(android.content.Context, X.0kS, X.0mE, X.0qD, X.3iB, java.lang.Runnable, java.lang.Runnable, boolean):void type: CONSTRUCTOR in method: X.65B.onItemClick(android.widget.AdapterView, android.view.View, int, long):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002e: CONSTRUCTOR  (r9v0 X.6IV A[REMOVE]) = 
                              (r2v0 'A05' com.whatsapp.jid.UserJid)
                              (r4v0 'paymentGroupParticipantPickerActivity' com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity)
                             call: X.6IV.<init>(com.whatsapp.jid.UserJid, com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity):void type: CONSTRUCTOR in method: X.65B.onItemClick(android.widget.AdapterView, android.view.View, int, long):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 27 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IV, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 33 more
                            */
                        /*
                            this = this;
                            com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity r4 = com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity.this
                            java.lang.Object r0 = r14.getTag()
                            X.5vD r0 = (X.C127885vD) r0
                            X.5tl r0 = r0.A04
                            if (r0 == 0) goto L_0x0049
                            X.0n3 r3 = r0.A00
                            com.whatsapp.jid.UserJid r2 = X.C15370n3.A05(r3)
                            X.0zW r0 = r4.A0C
                            int r1 = r0.A00(r2)
                            X.13b r0 = r4.A02
                            boolean r0 = r0.A0I(r2)
                            if (r0 != 0) goto L_0x0049
                            r0 = 2
                            if (r1 != r0) goto L_0x0049
                            X.AnonymousClass009.A05(r2)
                            X.0mE r6 = r4.A05
                            X.0qD r7 = r4.A0D
                            X.3iB r8 = r4.A0H
                            X.6IV r9 = new X.6IV
                            r9.<init>(r2, r4)
                            X.6JZ r10 = new X.6JZ
                            r10.<init>(r3, r2, r4)
                            r11 = 0
                            r5 = r4
                            X.3FP r3 = new X.3FP
                            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
                            boolean r0 = r3.A02()
                            if (r0 == 0) goto L_0x004a
                            java.lang.String r1 = r4.A0J
                            r0 = 0
                            r3.A01(r2, r0, r1)
                        L_0x0049:
                            return
                        L_0x004a:
                            r4.A2e(r2)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass65B.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
                    }
                });
                registerForContextMenu(this.A00);
                this.A04.A03(this.A0M);
                Toolbar A08 = C117305Zk.A08(this);
                A1e(A08);
                this.A01 = new C48232Fc(this, findViewById(R.id.search_holder), new AnonymousClass65P(this), A08, ((ActivityC13830kP) this).A01);
                AbstractC005102i A1U = A1U();
                if (A1U != null) {
                    A1U.A0A(R.string.payments_pick_group_participant_activity_title);
                    A1U.A0M(true);
                }
                AnonymousClass5oZ r1 = this.A0E;
                if (r1 != null) {
                    r1.A03(true);
                    this.A0E = null;
                }
                C124055oN r12 = new C124055oN(this);
                this.A0G = r12;
                C12960it.A1E(r12, ((ActivityC13830kP) this).A05);
                A2C(R.string.register_wait_message);
                AbstractC16870pt A0U = C117305Zk.A0U(this.A0D);
                if (A0U != null) {
                    AnonymousClass61I.A03(null, A0U, "payment_contact_picker", this.A0J);
                }
            }

            @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                C15370n3 r4 = ((C126985tl) ((AdapterView) view).getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position)).A00;
                if (r4 != null && this.A02.A0I(C15370n3.A05(r4))) {
                    contextMenu.add(0, 0, 0, C12960it.A0X(this, this.A05.A04(r4), C12970iu.A1b(), 0, R.string.block_list_menu_unblock));
                    super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
                }
            }

            @Override // X.ActivityC13790kL, android.app.Activity
            public boolean onCreateOptionsMenu(Menu menu) {
                menu.add(0, R.id.menuitem_search, 0, getString(R.string.search)).setIcon(R.drawable.ic_action_search).setShowAsAction(10);
                return super.onCreateOptionsMenu(menu);
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onDestroy() {
                super.onDestroy();
                this.A06.A00();
                this.A04.A04(this.A0M);
                AnonymousClass5oZ r0 = this.A0E;
                if (r0 != null) {
                    r0.A03(true);
                    this.A0E = null;
                }
                C124055oN r02 = this.A0G;
                if (r02 != null) {
                    r02.A03(true);
                    this.A0G = null;
                }
            }

            @Override // X.ActivityC13810kN, android.app.Activity
            public boolean onOptionsItemSelected(MenuItem menuItem) {
                int itemId = menuItem.getItemId();
                if (itemId == R.id.menuitem_search) {
                    onSearchRequested();
                    return true;
                } else if (itemId != 16908332) {
                    return true;
                } else {
                    finish();
                    return true;
                }
            }

            @Override // android.app.Activity, android.view.Window.Callback
            public boolean onSearchRequested() {
                this.A01.A01();
                return false;
            }
        }
