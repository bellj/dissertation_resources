package com.whatsapp.payments.ui;

import X.AnonymousClass102;
import X.AnonymousClass18S;
import X.AnonymousClass6BE;
import X.C117305Zk;
import X.C121265hX;
import X.C129245xP;
import X.C12960it;
import X.C130395zL;
import X.C1329668y;
import X.C18600si;
import X.C18610sj;
import X.C18650sn;
import android.view.View;
import android.widget.ListView;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* loaded from: classes4.dex */
public class IndiaUpiContactPickerFragment extends Hilt_IndiaUpiContactPickerFragment {
    public AnonymousClass102 A00;
    public C1329668y A01;
    public C18650sn A02;
    public C18600si A03;
    public AnonymousClass18S A04;
    public C18610sj A05;
    public AnonymousClass6BE A06;
    public C121265hX A07;

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1o() {
        return true;
    }

    public static /* synthetic */ void A03(IndiaUpiContactPickerFragment indiaUpiContactPickerFragment) {
        new C129245xP(indiaUpiContactPickerFragment.A0C(), ((PaymentContactPickerFragment) indiaUpiContactPickerFragment).A01, indiaUpiContactPickerFragment.A04, "payment_contact_picker").A00(null);
        indiaUpiContactPickerFragment.A06.AKg(C12960it.A0V(), 132, "payment_contact_picker", indiaUpiContactPickerFragment.A1B().getString("referral_screen"));
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public void A1H() {
        boolean A00 = C130395zL.A00(this.A1O, this.A01.A07());
        int i = R.string.send_payment_to_vpa;
        if (A00) {
            i = R.string.send_payment_to_vpa_or_upi_number;
        }
        View A1D = A1D(C117305Zk.A0A(this, 31), R.drawable.ic_send_to_upi, 0, R.drawable.grey_circle_stroke, i);
        View A1D2 = A1D(C117305Zk.A0A(this, 32), R.drawable.ic_scan_qr, 0, R.drawable.green_circle, R.string.payment_scan_qr_title);
        ListView listView = (ListView) ((ContactPickerFragment) this).A09.findViewById(16908298);
        ((ContactPickerFragment) this).A0D = listView;
        listView.addHeaderView(A1D, null, true);
        ((ContactPickerFragment) this).A0D.addHeaderView(A1D2, null, true);
        super.A1H();
    }
}
