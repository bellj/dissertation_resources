package com.whatsapp.payments.ui.mapper.register;

import X.AbstractActivityC119375df;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass130;
import X.AnonymousClass1ZR;
import X.AnonymousClass6BE;
import X.C124935qP;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1329668y;
import X.C15570nT;
import X.C16700pc;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperConfirmationActivity;

/* loaded from: classes2.dex */
public final class IndiaUpiMapperConfirmationActivity extends AbstractActivityC119375df {
    public ImageView A00;
    public AnonymousClass130 A01;
    public C1329668y A02;
    public AnonymousClass6BE A03;

    public static /* synthetic */ void A02(IndiaUpiMapperConfirmationActivity indiaUpiMapperConfirmationActivity) {
        C16700pc.A0E(indiaUpiMapperConfirmationActivity, 0);
        indiaUpiMapperConfirmationActivity.setResult(-1);
        AnonymousClass6BE r4 = indiaUpiMapperConfirmationActivity.A03;
        if (r4 != null) {
            r4.AKg(C12960it.A0V(), 85, "alias_complete", ActivityC13790kL.A0W(indiaUpiMapperConfirmationActivity));
            indiaUpiMapperConfirmationActivity.finish();
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass6BE r3 = this.A03;
        if (r3 != null) {
            Integer A0V = C12960it.A0V();
            r3.AKg(A0V, A0V, "alias_complete", ActivityC13790kL.A0W(this));
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        String stringExtra;
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
        setContentView(R.layout.india_upi_mapper_confirmation_activity);
        C124935qP.A00(this);
        TextView A0M = C12970iu.A0M(this, R.id.payment_name);
        AnonymousClass1ZR r0 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payment_name");
        if (r0 == null || (str = (String) r0.A00) == null) {
            str = ((ActivityC13810kN) this).A09.A00.getString("push_name", "");
        }
        A0M.setText(str);
        View findViewById = findViewById(R.id.mapper_confirm_done);
        TextView A0M2 = C12970iu.A0M(this, R.id.vpa_id);
        TextView A0M3 = C12970iu.A0M(this, R.id.vpa_alias);
        View findViewById2 = findViewById(R.id.profile_icon_placeholder);
        C16700pc.A0B(findViewById2);
        ImageView imageView = (ImageView) findViewById2;
        C16700pc.A0E(imageView, 0);
        this.A00 = imageView;
        AnonymousClass130 r1 = this.A01;
        if (r1 != null) {
            r1.A05(imageView, R.drawable.avatar_contact);
            Resources resources = getResources();
            Object[] objArr = new Object[1];
            C1329668y r02 = this.A02;
            if (r02 != null) {
                A0M2.setText(C12990iw.A0o(resources, r02.A04().A00, objArr, 0, R.string.vpa_prefix));
                Resources resources2 = getResources();
                Object[] objArr2 = new Object[1];
                C15570nT r03 = ((ActivityC13790kL) this).A01;
                r03.A08();
                Me me = r03.A00;
                if (me == null) {
                    str2 = null;
                } else {
                    str2 = me.number;
                }
                A0M3.setText(C12990iw.A0o(resources2, str2, objArr2, 0, R.string.upi_number_prefix));
                findViewById.setOnClickListener(new View.OnClickListener() { // from class: X.3lV
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        IndiaUpiMapperConfirmationActivity.A02(IndiaUpiMapperConfirmationActivity.this);
                    }
                });
                AnonymousClass6BE r3 = this.A03;
                if (r3 != null) {
                    Intent intent = getIntent();
                    if (intent == null) {
                        stringExtra = null;
                    } else {
                        stringExtra = intent.getStringExtra("extra_referral_screen");
                    }
                    r3.AKg(0, null, "alias_complete", stringExtra);
                    return;
                }
                throw C16700pc.A06("indiaUpiFieldStatsLogger");
            }
            throw C16700pc.A06("paymentSharedPrefs");
        }
        throw C16700pc.A06("contactAvatars");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        C16700pc.A0E(menuItem, 0);
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass6BE r4 = this.A03;
            if (r4 != null) {
                r4.AKg(C12960it.A0V(), C12970iu.A0h(), "alias_complete", ActivityC13790kL.A0W(this));
            } else {
                throw C16700pc.A06("indiaUpiFieldStatsLogger");
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
