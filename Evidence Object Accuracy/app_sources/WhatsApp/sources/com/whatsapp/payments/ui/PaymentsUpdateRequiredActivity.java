package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C12970iu;
import X.C21740xu;
import X.C38241nl;
import X.C43951xu;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes4.dex */
public class PaymentsUpdateRequiredActivity extends ActivityC13790kL {
    public C21740xu A00;
    public WaImageView A01;
    public boolean A02;

    public PaymentsUpdateRequiredActivity() {
        this(0);
    }

    public PaymentsUpdateRequiredActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, C43951xu.A03);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C21740xu) A1M.AM1.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        WaImageView waImageView = this.A01;
        int i = 0;
        if (configuration.orientation == 2) {
            i = 8;
        }
        waImageView.setVisibility(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            boolean A03 = C38241nl.A03();
            int i = R.string.software_about_to_expire_title;
            if (A03) {
                i = R.string.software_deprecated_title;
            }
            C117305Zk.A16(A1U, i);
        }
        setContentView(R.layout.payment_update_required);
        TextView A0M = C12970iu.A0M(this, R.id.update_title);
        TextView A0M2 = C12970iu.A0M(this, R.id.update_description);
        if (C38241nl.A03()) {
            A0M.setText(R.string.software_deprecated_title);
            A0M2.setText(R.string.payment_update_whatsapp_desc_no_update);
        }
        TextView A0M3 = C12970iu.A0M(this, R.id.upgrade_button);
        boolean A032 = C38241nl.A03();
        int i2 = R.string.button_download;
        if (A032) {
            i2 = R.string.learn_more;
        }
        A0M3.setText(i2);
        C117295Zj.A0n(A0M3, this, 115);
        WaImageView waImageView = (WaImageView) findViewById(R.id.update_icon);
        this.A01 = waImageView;
        if (C38241nl.A03()) {
            waImageView.setImageResource(R.drawable.splash_logo);
        }
    }
}
