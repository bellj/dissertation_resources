package com.whatsapp.payments.ui;

import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass6DY;
import X.C117295Zj;
import X.C117305Zk;
import X.C12960it;
import X.C12970iu;
import X.C14900mE;
import X.C15890o4;
import X.C21280xA;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.qrcode.WaQrScannerView;

/* loaded from: classes4.dex */
public class IndiaUpiScanQrCodeFragment extends Hilt_IndiaUpiScanQrCodeFragment {
    public View A00;
    public View A01;
    public ImageView A02;
    public ImageView A03;
    public C14900mE A04;
    public AnonymousClass01d A05;
    public C15890o4 A06;
    public WaQrScannerView A07;
    public C21280xA A08;
    public String A09;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.qr_code_scanner);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C117305Zk.A15(view, R.id.education);
        this.A00 = AnonymousClass028.A0D(view, R.id.overlay);
        this.A07 = (WaQrScannerView) AnonymousClass028.A0D(view, R.id.qr_scanner_view);
        this.A01 = AnonymousClass028.A0D(view, R.id.shade);
        this.A07.setQrScannerCallback(new AnonymousClass6DY(this));
        ImageView A0K = C12970iu.A0K(view, R.id.qr_scan_from_gallery);
        this.A03 = A0K;
        A0K.setVisibility(0);
        C117295Zj.A0n(this.A03, this, 70);
        ImageView A0K2 = C12970iu.A0K(view, R.id.qr_scan_flash);
        this.A02 = A0K2;
        C117295Zj.A0n(A0K2, this, 69);
        this.A07.setVisibility(8);
        this.A00.setVisibility(8);
        this.A01.setVisibility(0);
    }

    public final void A19() {
        boolean Aee = this.A07.Aee();
        ImageView imageView = this.A02;
        if (Aee) {
            imageView.setVisibility(0);
            boolean AK9 = this.A07.AK9();
            ImageView imageView2 = this.A02;
            int i = R.drawable.flash_off;
            if (AK9) {
                i = R.drawable.flash_on;
            }
            imageView2.setImageResource(i);
            ImageView imageView3 = this.A02;
            int i2 = R.string.flash_off_action;
            if (!AK9) {
                i2 = R.string.flash_on_action;
            }
            imageView3.setContentDescription(A0I(i2));
            return;
        }
        imageView.setVisibility(8);
    }
}
