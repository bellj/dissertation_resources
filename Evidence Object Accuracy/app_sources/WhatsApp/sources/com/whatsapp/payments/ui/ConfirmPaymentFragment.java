package com.whatsapp.payments.ui;

import X.AbstractC124835qC;
import X.AbstractC28901Pl;
import X.AbstractC30871Zd;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass102;
import X.AnonymousClass1ZO;
import X.AnonymousClass1ZY;
import X.AnonymousClass6M1;
import X.AnonymousClass6N0;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1311161i;
import X.C17070qD;
import X.C22710zW;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* loaded from: classes4.dex */
public class ConfirmPaymentFragment extends Hilt_ConfirmPaymentFragment implements AnonymousClass6M1 {
    public int A00;
    public int A01;
    public View A02;
    public View A03;
    public View A04;
    public ViewGroup A05;
    public Button A06;
    public FrameLayout A07;
    public FrameLayout A08;
    public ProgressBar A09;
    public TextView A0A;
    public TextView A0B;
    public TextView A0C;
    public WaImageView A0D;
    public AnonymousClass01d A0E;
    public AnonymousClass018 A0F;
    public AnonymousClass1ZO A0G;
    public AnonymousClass102 A0H;
    public AbstractC28901Pl A0I;
    public C22710zW A0J;
    public C17070qD A0K;
    public AbstractC124835qC A0L;
    public AnonymousClass6N0 A0M;
    public PaymentMethodRow A0N;

    public static ConfirmPaymentFragment A00(AbstractC28901Pl r4, UserJid userJid, int i) {
        ConfirmPaymentFragment confirmPaymentFragment = new ConfirmPaymentFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("arg_payment_method", r4);
        if (userJid != null) {
            A0D.putString("arg_jid", userJid.getRawString());
        }
        A0D.putInt("arg_payment_type", i);
        confirmPaymentFragment.A0U(A0D);
        return confirmPaymentFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        int i = 0;
        View inflate = layoutInflater.inflate(R.layout.confirm_payment_fragment, viewGroup, false);
        this.A08 = (FrameLayout) inflate.findViewById(R.id.title_view);
        this.A0N = (PaymentMethodRow) inflate.findViewById(R.id.payment_method_row);
        this.A05 = C12980iv.A0P(inflate, R.id.transaction_description_container);
        this.A06 = (Button) inflate.findViewById(R.id.confirm_payment);
        this.A07 = (FrameLayout) AnonymousClass028.A0D(inflate, R.id.footer_view);
        this.A0A = C12960it.A0J(inflate, R.id.education);
        this.A09 = (ProgressBar) inflate.findViewById(R.id.confirm_payment_progressbar);
        this.A02 = AnonymousClass028.A0D(inflate, R.id.education_divider);
        inflate.findViewById(R.id.account_number_divider).setVisibility(8);
        C12980iv.A1B(inflate, R.id.payment_method_account_id, 8);
        ATY(this.A0I);
        this.A04 = inflate.findViewById(R.id.payment_to_merchant_options_container);
        this.A0C = C12960it.A0J(inflate, R.id.payment_to_merchant_options);
        this.A0D = (WaImageView) inflate.findViewById(R.id.payment_to_merchant_options_icon);
        this.A03 = inflate.findViewById(R.id.payment_rails_container);
        this.A0B = C12960it.A0J(inflate, R.id.payment_rails_label);
        AnonymousClass01E r3 = super.A0D;
        C117295Zj.A0o(inflate.findViewById(R.id.payment_method_container), this, r3, 6);
        C117295Zj.A0o(this.A05, this, r3, 7);
        C117295Zj.A0o(inflate.findViewById(R.id.payment_to_merchant_options_container), this, r3, 4);
        C117295Zj.A0o(inflate.findViewById(R.id.payment_rails_container), this, r3, 5);
        if (this.A0L != null) {
            ViewGroup A0P = C12980iv.A0P(inflate, R.id.contact_info_view);
            if (A0P != null) {
                this.A0L.AMP(A0P);
            }
            ViewGroup viewGroup2 = this.A05;
            if (viewGroup2 != null) {
                this.A0L.AMM(viewGroup2);
            }
            View findViewById = inflate.findViewById(R.id.payment_method_container);
            if (findViewById != null) {
                if (!this.A0L.Adt()) {
                    i = 8;
                }
                findViewById.setVisibility(i);
            }
            View findViewById2 = inflate.findViewById(R.id.transaction_amount_info_view);
            if (findViewById2 != null) {
                C117295Zj.A0o(findViewById2, this, r3, 3);
            }
            ViewGroup A0P2 = C12980iv.A0P(inflate, R.id.extra_info_view);
            if (A0P2 != null) {
                this.A0L.A6F(A0P2);
            }
        }
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A07 = null;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        AnonymousClass1ZO r0;
        AnonymousClass1ZO r02;
        super.A13();
        UserJid nullable = UserJid.getNullable(A03().getString("arg_jid"));
        if (nullable != null) {
            r0 = C117305Zk.A0F(nullable, this.A0K);
        } else {
            r0 = null;
        }
        this.A0G = r0;
        if (this.A0J.A08() && (r02 = this.A0G) != null && r02.A0C()) {
            if (this.A0I.A04() == 6 && this.A01 == 0) {
                this.A03.setVisibility(0);
                if (this.A0I.A08 != null) {
                    int i = this.A00;
                    TextView textView = this.A0B;
                    int i2 = R.string.confirm_payment_bottom_sheet_payment_rails_debit_label;
                    if (i == 0) {
                        i2 = R.string.confirm_payment_bottom_sheet_payment_rails_credit_label;
                    }
                    textView.setText(i2);
                }
            } else {
                this.A03.setVisibility(8);
            }
            A19(this.A01);
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A0I = (AbstractC28901Pl) C117315Zl.A03(A03(), "arg_payment_method");
        Integer valueOf = Integer.valueOf(A03().getInt("arg_payment_type"));
        AnonymousClass009.A05(valueOf);
        this.A01 = valueOf.intValue();
    }

    public void A19(int i) {
        WaImageView waImageView;
        int i2;
        this.A01 = i;
        this.A04.setVisibility(0);
        TextView textView = this.A0C;
        if (i == 0) {
            textView.setText(R.string.buying_goods_and_services);
            waImageView = this.A0D;
            i2 = R.drawable.cart;
        } else {
            textView.setText(R.string.sending_to_friends_and_family);
            waImageView = this.A0D;
            i2 = R.drawable.ic_contacts_storage_usage;
        }
        waImageView.setImageResource(i2);
    }

    @Override // X.AnonymousClass6M1
    public void ATY(AbstractC28901Pl r8) {
        boolean z;
        String str;
        String str2;
        AbstractC30871Zd r0;
        this.A0I = r8;
        AbstractC124835qC r02 = this.A0L;
        if (r02 != null) {
            z = r02.AdU(r8);
            if (z) {
                String ACJ = this.A0L.ACJ(r8);
                if (!TextUtils.isEmpty(ACJ)) {
                    this.A0N.A02.setText(ACJ);
                }
            }
        } else {
            z = false;
        }
        TextView textView = this.A0N.A02;
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        textView.setVisibility(C12960it.A02(i));
        AbstractC124835qC r03 = this.A0L;
        String str3 = null;
        if (r03 != null) {
            str = r03.ACK(r8);
        } else {
            str = null;
        }
        PaymentMethodRow paymentMethodRow = this.A0N;
        if (TextUtils.isEmpty(str)) {
            str = C1311161i.A02(A01(), this.A0F, r8, this.A0K, true);
        }
        paymentMethodRow.A05.setText(str);
        AbstractC124835qC r04 = this.A0L;
        if (r04 == null || (str3 = r04.AEO(r8)) == null) {
            AnonymousClass1ZY r05 = r8.A08;
            AnonymousClass009.A05(r05);
            if (!r05.A0A()) {
                str3 = A0I(R.string.payment_method_unverified);
            }
        }
        this.A0N.A02(str3);
        AbstractC124835qC r1 = this.A0L;
        if (r1 == null || !r1.AdV()) {
            C1311161i.A0A(r8, this.A0N);
        } else {
            r1.Adj(r8, this.A0N);
        }
        AbstractC124835qC r12 = this.A0L;
        if (r12 != null) {
            boolean AdO = r12.AdO(r8, this.A01);
            PaymentMethodRow paymentMethodRow2 = this.A0N;
            if (AdO) {
                paymentMethodRow2.A03(false);
                this.A0N.A02(A0I(R.string.payment_method_unavailable));
            } else {
                paymentMethodRow2.A03(true);
            }
        }
        C117295Zj.A0o(this.A06, this, r8, 2);
        AbstractC124835qC r13 = this.A0L;
        if (r13 != null) {
            str2 = r13.ABX(r8, this.A01);
        } else {
            str2 = "";
        }
        this.A06.setText(str2);
        this.A06.setEnabled(true);
        if (r8.A04() == 6 && (r0 = (AbstractC30871Zd) r8.A08) != null) {
            this.A00 = r0.A03;
        }
        AbstractC124835qC r14 = this.A0L;
        if (r14 != null) {
            r14.AMN(this.A08);
            FrameLayout frameLayout = this.A07;
            if (frameLayout != null) {
                this.A0L.AQh(frameLayout, r8);
            }
            String ACf = this.A0L.ACf(r8, this.A01);
            boolean isEmpty = TextUtils.isEmpty(ACf);
            TextView textView2 = this.A0A;
            if (!isEmpty) {
                textView2.setText(ACf);
            } else {
                textView2.setVisibility(8);
                this.A02.setVisibility(8);
            }
            this.A06.setEnabled(true);
        }
        AnonymousClass6N0 r15 = this.A0M;
        if (r15 != null) {
            r15.ATZ(r8, this.A0N);
        }
    }
}
