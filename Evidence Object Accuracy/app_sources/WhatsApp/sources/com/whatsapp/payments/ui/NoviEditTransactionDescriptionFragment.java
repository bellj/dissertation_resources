package com.whatsapp.payments.ui;

import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass19M;
import X.AnonymousClass367;
import X.AnonymousClass60Y;
import X.C100654mG;
import X.C117295Zj;
import X.C117325Zm;
import X.C117485a2;
import X.C128365vz;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1317363x;
import X.C16630pM;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.Button;

/* loaded from: classes4.dex */
public class NoviEditTransactionDescriptionFragment extends Hilt_NoviEditTransactionDescriptionFragment {
    public Button A00;
    public AnonymousClass01d A01;
    public AnonymousClass018 A02;
    public AnonymousClass19M A03;
    public AnonymousClass60Y A04;
    public C16630pM A05;
    public String A06;

    public static NoviEditTransactionDescriptionFragment A00(String str) {
        NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment = new NoviEditTransactionDescriptionFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("arg_payment_description", str);
        noviEditTransactionDescriptionFragment.A0U(A0D);
        return noviEditTransactionDescriptionFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        C128365vz.A02(this, "NAVIGATION_START", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_send_money_edit_payment_description);
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        C128365vz.A02(this, "NAVIGATION_END", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String string = A03().getString("arg_payment_description");
        AnonymousClass009.A05(string);
        this.A06 = string;
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.common_action_bar_header_back), this, 81);
        this.A00 = (Button) AnonymousClass028.A0D(view, R.id.save_description_button);
        EditText editText = (EditText) AnonymousClass028.A0D(view, R.id.payment_description_text);
        editText.requestFocus();
        editText.addTextChangedListener(new C1317363x(this));
        AnonymousClass367 r2 = new AnonymousClass367(editText, C12960it.A0I(view, R.id.counter), this.A01, this.A02, this.A03, this.A05, 140, 0, true);
        editText.setFilters(new InputFilter[]{new C100654mG(140)});
        editText.addTextChangedListener(r2);
        if (!TextUtils.isEmpty(this.A06) && editText.getText() != null) {
            editText.setText(this.A06);
            editText.setSelection(editText.getText().length());
        }
        C117295Zj.A0o(AnonymousClass028.A0D(view, R.id.save_description_button), this, editText, 22);
        TextView A0I = C12960it.A0I(view, R.id.novi_payment_description_disclaimer_text);
        String A0I2 = A0I(R.string.novi_payment_description_learn_more_link);
        String A0q = C12970iu.A0q(this, A0I2, new Object[1], 0, R.string.novi_payment_description_disclaimer);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A0q);
        C117325Zm.A04(spannableStringBuilder, new C117485a2(this), A0q, A0I2);
        A0I.setText(spannableStringBuilder);
        A0I.setLinksClickable(true);
        C12990iw.A1F(A0I);
    }
}
