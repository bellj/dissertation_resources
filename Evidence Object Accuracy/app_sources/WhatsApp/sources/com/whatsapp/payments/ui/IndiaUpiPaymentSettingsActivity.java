package com.whatsapp.payments.ui;

import X.AbstractActivityC119285dT;
import X.AbstractActivityC121725jj;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass6MA;
import X.C004802e;
import X.C117295Zj;
import X.C119865fE;
import X.C12980iv;
import X.C1309960u;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsActivity;

/* loaded from: classes4.dex */
public final class IndiaUpiPaymentSettingsActivity extends AbstractActivityC121725jj {
    public C119865fE A00;
    public C1309960u A01;
    public boolean A02;

    public IndiaUpiPaymentSettingsActivity() {
        this(0);
    }

    public IndiaUpiPaymentSettingsActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 56);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119285dT.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A01 = (C1309960u) A1M.A1b.get();
            this.A00 = (C119865fE) A1M.AEV.get();
        }
    }

    @Override // X.AbstractActivityC121725jj, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (((AbstractActivityC121725jj) this).A01.A03.A07(698)) {
            this.A00.A0A();
        }
        C117295Zj.A0e(this);
        this.A01.A02(new AnonymousClass6MA() { // from class: X.6D4
            @Override // X.AnonymousClass6MA
            public final void AVY() {
                C1309960u.A01(IndiaUpiPaymentSettingsActivity.this);
            }
        });
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e A0S;
        PaymentSettingsFragment paymentSettingsFragment = ((AbstractActivityC121725jj) this).A02;
        if (paymentSettingsFragment instanceof IndiaUpiPaymentSettingsFragment) {
            if (i == 100) {
                A0S = C12980iv.A0S(paymentSettingsFragment.A0C());
                A0S.A06(R.string.payments_request_status_requested_expired);
                A0S.A0B(false);
                C117295Zj.A0q(A0S, paymentSettingsFragment, 44, R.string.ok);
                A0S.A07(R.string.payments_request_status_request_expired);
            } else if (i == 101) {
                A0S = C12980iv.A0S(paymentSettingsFragment.A0C());
                A0S.A06(R.string.invalid_deep_link);
                A0S.A0B(true);
                C117295Zj.A0q(A0S, paymentSettingsFragment, 45, R.string.ok);
            }
            return A0S.create();
        }
        return super.onCreateDialog(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A01.A03()) {
            C1309960u.A01(this);
        }
    }
}
