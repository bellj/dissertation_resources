package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass102;
import X.AnonymousClass1ZR;
import X.AnonymousClass2FL;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117925az;
import X.C118475bs;
import X.C119755f3;
import X.C120515gJ;
import X.C120525gK;
import X.C127755v0;
import X.C128355vy;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C1308460e;
import X.C1329668y;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C17900ra;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C21860y6;
import X.C30861Zc;
import X.C30931Zj;
import X.C36021jC;
import X.C452120p;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;

/* loaded from: classes4.dex */
public class IndiaUpiCheckBalanceActivity extends AbstractActivityC121545iU {
    public C30861Zc A00;
    public AnonymousClass1ZR A01;
    public C117925az A02;
    public C128355vy A03;
    public boolean A04;
    public final C30931Zj A05;

    public IndiaUpiCheckBalanceActivity() {
        this(0);
        this.A05 = C117295Zj.A0G("IndiaUpiCheckPinActivity");
    }

    public IndiaUpiCheckBalanceActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 41);
    }

    public static /* synthetic */ void A02(IndiaUpiCheckBalanceActivity indiaUpiCheckBalanceActivity, C127755v0 r6) {
        int i = r6.A03;
        if (i == 0) {
            C30861Zc r4 = indiaUpiCheckBalanceActivity.A00;
            String str = r6.A01;
            String str2 = r6.A02;
            Intent A0D = C12990iw.A0D(indiaUpiCheckBalanceActivity, IndiaUpiBalanceDetailsActivity.class);
            A0D.putExtra("payment_bank_account", r4);
            A0D.putExtra("balance", str);
            A0D.putExtra("usable_balance", str2);
            indiaUpiCheckBalanceActivity.finish();
            indiaUpiCheckBalanceActivity.A2D(A0D);
        } else if (i == 1) {
            C452120p r2 = r6.A00;
            Bundle A0D2 = C12970iu.A0D();
            A0D2.putInt("error_code", r2.A00);
            int i2 = r2.A00;
            int i3 = 10;
            if (i2 != 11459) {
                i3 = 11;
                if (i2 != 11468) {
                    i3 = 12;
                    if (i2 != 11454) {
                        if (i2 == 11487 || i2 == 20697 || i2 == 20682) {
                            i3 = 27;
                        } else {
                            indiaUpiCheckBalanceActivity.A05.A06(" onCheckBalance failed; showErrorAndFinish");
                            indiaUpiCheckBalanceActivity.A39();
                            return;
                        }
                    }
                }
            }
            C36021jC.A02(indiaUpiCheckBalanceActivity, A0D2, i3);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
            this.A03 = (C128355vy) A1M.A9f.get();
        }
    }

    public final void A3H(String str) {
        C30861Zc r1 = this.A00;
        A3E((C119755f3) r1.A08, str, r1.A0B, (String) this.A01.A00, (String) C117295Zj.A0R(r1.A09), 3);
    }

    @Override // X.AnonymousClass6MS
    public void ARr(C452120p r4, String str) {
        Integer num;
        if (!TextUtils.isEmpty(str)) {
            this.A05.A06("onListKeys called");
            A3H(str);
        } else if (r4 != null && !AnonymousClass69E.A02(this, "upi-list-keys", r4.A00, false)) {
            if (((AbstractActivityC121545iU) this).A06.A07("upi-list-keys")) {
                AbstractActivityC119235dO.A1i(this);
                return;
            }
            C30931Zj r2 = this.A05;
            StringBuilder A0k = C12960it.A0k("onListKeys: ");
            if (str != null) {
                num = Integer.valueOf(str.length());
            } else {
                num = null;
            }
            A0k.append(num);
            r2.A06(C12960it.A0d(" failed; ; showErrorAndFinish", A0k));
            A39();
        }
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r3) {
        throw C12980iv.A0u(this.A05.A02("onSetPin unsupported"));
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00 = (C30861Zc) getIntent().getParcelableExtra("extra_bank_account");
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C15570nT r15 = ((ActivityC13790kL) this).A01;
        C17220qS r14 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r13 = ((AbstractActivityC121545iU) this).A0C;
        C17070qD r12 = ((AbstractActivityC121685jC) this).A0P;
        C21860y6 r11 = ((AbstractActivityC121685jC) this).A0I;
        C1308460e r10 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r9 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass102 r8 = ((AbstractActivityC121545iU) this).A02;
        C17900ra r7 = ((AbstractActivityC121685jC) this).A0N;
        AnonymousClass6BE r6 = ((AbstractActivityC121665jA) this).A0D;
        C18640sm r4 = ((ActivityC13810kN) this).A07;
        C18650sn r2 = ((AbstractActivityC121685jC) this).A0K;
        C1329668y r5 = ((AbstractActivityC121665jA) this).A0B;
        ((AbstractActivityC121545iU) this).A09 = new C120525gK(this, r02, r15, r4, r8, r0, r14, r10, r5, r11, r2, r9, r7, r12, this, r6, ((AbstractActivityC121545iU) this).A0B, r13);
        this.A01 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A2o(r5.A06()), "upiSequenceNumber");
        C14850m9 r03 = ((ActivityC13810kN) this).A0C;
        C14900mE r152 = ((ActivityC13810kN) this).A05;
        C15570nT r142 = ((ActivityC13790kL) this).A01;
        C17220qS r132 = ((AbstractActivityC121685jC) this).A0H;
        C17070qD r122 = ((AbstractActivityC121685jC) this).A0P;
        C18590sh r112 = ((AbstractActivityC121545iU) this).A0C;
        C1308460e r102 = ((AbstractActivityC121665jA) this).A0A;
        C21860y6 r92 = ((AbstractActivityC121685jC) this).A0I;
        C18610sj r82 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass102 r72 = ((AbstractActivityC121545iU) this).A02;
        AnonymousClass6BE r62 = ((AbstractActivityC121665jA) this).A0D;
        C120515gJ r52 = new C120515gJ(this, r152, r142, ((ActivityC13810kN) this).A07, r72, r03, r132, r102, ((AbstractActivityC121665jA) this).A0B, r92, ((AbstractActivityC121685jC) this).A0K, r82, r122, r62, ((AbstractActivityC121545iU) this).A0B, r112);
        C128355vy r42 = this.A03;
        C117925az r22 = (C117925az) C117315Zl.A06(new C118475bs(this.A00, this.A01, r52, r42), this).A00(C117925az.class);
        this.A02 = r22;
        r22.A01.A05(this, C117305Zk.A0B(this, 36));
        C117925az r23 = this.A02;
        r23.A07.A05(this, C117305Zk.A0B(this, 35));
        A2P(getString(R.string.register_wait_message));
        ((AbstractActivityC121545iU) this).A09.A00();
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 27) {
            if (i != 28) {
                switch (i) {
                    case 10:
                        return A32(new Runnable() { // from class: X.6Fy
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiCheckBalanceActivity indiaUpiCheckBalanceActivity = IndiaUpiCheckBalanceActivity.this;
                                C36021jC.A00(indiaUpiCheckBalanceActivity, 10);
                                String A0A = ((AbstractActivityC121665jA) indiaUpiCheckBalanceActivity).A0B.A0A();
                                if (!TextUtils.isEmpty(A0A)) {
                                    indiaUpiCheckBalanceActivity.A01 = C117305Zk.A0I(C117305Zk.A0J(), String.class, AbstractActivityC119235dO.A0O(indiaUpiCheckBalanceActivity), "upiSequenceNumber");
                                    indiaUpiCheckBalanceActivity.A3H(A0A);
                                    indiaUpiCheckBalanceActivity.A02.A00 = indiaUpiCheckBalanceActivity.A01;
                                    return;
                                }
                                indiaUpiCheckBalanceActivity.A2P(indiaUpiCheckBalanceActivity.getString(R.string.register_wait_message));
                                ((AbstractActivityC121545iU) indiaUpiCheckBalanceActivity).A09.A00();
                            }
                        }, getString(R.string.upi_check_balance_incorrect_pin_title), getString(R.string.upi_check_balance_incorrect_pin_message), i, R.string.payments_try_again, R.string.cancel);
                    case 11:
                        break;
                    case 12:
                        return A32(new Runnable() { // from class: X.6Fx
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiCheckBalanceActivity indiaUpiCheckBalanceActivity = IndiaUpiCheckBalanceActivity.this;
                                C117305Zk.A1D(indiaUpiCheckBalanceActivity, 12);
                                indiaUpiCheckBalanceActivity.A2q();
                                indiaUpiCheckBalanceActivity.finish();
                            }
                        }, getString(R.string.upi_check_balance_no_pin_set_title), getString(R.string.upi_check_balance_no_pin_set_message), i, R.string.learn_more, R.string.ok);
                    default:
                        return super.onCreateDialog(i);
                }
            }
            return A30(this.A00, i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A06(R.string.check_balance_balance_unavailable_message);
        A0S.A07(R.string.check_balance_balance_unavailable_title);
        C117295Zj.A0q(A0S, this, 20, R.string.ok);
        return A0S.create();
    }
}
