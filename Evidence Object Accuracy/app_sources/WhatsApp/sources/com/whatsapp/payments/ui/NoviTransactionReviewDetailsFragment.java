package com.whatsapp.payments.ui;

import X.AbstractC14640lm;
import X.AbstractC30781Yu;
import X.AbstractC30791Yv;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass63X;
import X.AnonymousClass63Y;
import X.AnonymousClass6F2;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C128365vz;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C130275z5;
import X.C1315163b;
import X.C1315863i;
import X.C1316263m;
import X.C1316363n;
import X.C15370n3;
import X.C15610nY;
import X.C20830wO;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import java.math.BigDecimal;

/* loaded from: classes4.dex */
public class NoviTransactionReviewDetailsFragment extends Hilt_NoviTransactionReviewDetailsFragment {
    public C15610nY A00;
    public AnonymousClass018 A01;
    public C20830wO A02;
    public C1315863i A03;
    public C1316263m A04;
    public C1315163b A05;
    public AnonymousClass63X A06;
    public AnonymousClass60Y A07;

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass60Y r6 = this.A07;
        AnonymousClass610 A02 = AnonymousClass610.A02("NAVIGATION_START", "SEND_MONEY");
        C128365vz r4 = A02.A00;
        r4.A0i = "REVIEW_TRANSACTION_DETAILS";
        A02.A05(this.A03, this.A04, this.A05, this.A06);
        r6.A06(r4);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_send_money_review_transaction_details);
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        AnonymousClass60Y r2 = this.A07;
        C128365vz r0 = AnonymousClass610.A02("NAVIGATION_END", "SEND_MONEY").A00;
        r0.A0i = "REVIEW_TRANSACTION_DETAILS";
        r2.A06(r0);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Bundle A03 = A03();
        AbstractC14640lm r7 = (AbstractC14640lm) C117315Zl.A03(A03, "arg_receiver_jid");
        this.A05 = (C1315163b) C117315Zl.A03(A03, "arg_transaction_data");
        this.A03 = (C1315863i) C117315Zl.A03(A03, "arg_exchange_quote");
        this.A04 = (C1316263m) C117315Zl.A03(A03, "arg_account_balance");
        this.A06 = (AnonymousClass63X) A03.getParcelable("arg_deposit_draft");
        C1315863i r2 = this.A03;
        boolean A1V = C117305Zk.A1V(r2.A00.A00, ((AbstractC30781Yu) r2.A01.A00).A04);
        View inflate = View.inflate(A0B(), R.layout.novi_send_money_review_details_header, C12980iv.A0P(view, R.id.title_view));
        C12960it.A0I(inflate, R.id.send_money_review_details_header_title).setText(R.string.novi_transaction_breakdown_title);
        View A0D = AnonymousClass028.A0D(inflate, R.id.send_money_review_details_header_back);
        A0D.setVisibility(0);
        C117295Zj.A0n(A0D, this, 92);
        TextView A0I = C12960it.A0I(view, R.id.novi_send_money_review_transaction_exchange_rate);
        C1315863i r3 = this.A03;
        A0I.setText(r3.A06.AHI(A01(), this.A01, r3));
        A1A(AnonymousClass028.A0D(view, R.id.novi_send_money_review_transaction_details_sender_crypto_layout), this.A05.A05.A00, A0I(R.string.novi_send_money_review_extras_sender_label));
        View A0D2 = AnonymousClass028.A0D(view, R.id.novi_send_money_review_transaction_details_sender_fiat_layout);
        TextView A0I2 = C12960it.A0I(view, R.id.novi_send_money_review_transaction_details_sender_exchange_rate);
        if (!A1V) {
            A19(A0D2, this.A05.A05.A00);
            C1315863i r4 = this.A03;
            A0I2.setText(C130275z5.A00(A01(), this.A01, r4.A01, r4));
        } else {
            A0D2.setVisibility(8);
            A0I2.setVisibility(8);
        }
        C15370n3 A01 = this.A02.A01(r7);
        A1A(AnonymousClass028.A0D(view, R.id.novi_send_money_review_transaction_details_receiver_crypto_layout), this.A05.A03.A01, C12970iu.A0q(this, this.A00.A04(A01), new Object[1], 0, R.string.novi_send_money_review_transaction_receiver_amount));
        A19(AnonymousClass028.A0D(view, R.id.novi_send_money_review_transaction_details_receiver_fiat_layout), this.A05.A03.A01);
        TextView A0I3 = C12960it.A0I(view, R.id.novi_send_money_review_transaction_details_receiver_exchange_rate);
        if (!A1V) {
            C1315863i r10 = this.A03;
            Context A012 = A01();
            AnonymousClass018 r9 = this.A01;
            AnonymousClass63Y r22 = r10.A00;
            AbstractC30791Yv r5 = r22.A02;
            Object[] objArr = new Object[2];
            int i = 0;
            objArr[0] = r5.AAB(r9, BigDecimal.ONE, 2);
            AbstractC30791Yv r23 = r22.A01;
            BigDecimal bigDecimal = r10.A02.A05;
            if (!BigDecimal.ONE.equals(bigDecimal)) {
                i = 4;
            }
            A0I3.setText(r5.AA7(A012, C12960it.A0X(A012, C117305Zk.A0k(r9, r23, bigDecimal, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate)));
            return;
        }
        A0I3.setVisibility(8);
    }

    public final void A19(View view, C1316363n r8) {
        C12960it.A0I(view, R.id.novi_send_money_review_transaction_line_item_lhs).setText(R.string.novi_conversion_summary_label);
        TextView A0I = C12960it.A0I(view, R.id.novi_send_money_review_transaction_line_item_rhs);
        Context context = view.getContext();
        AnonymousClass6F2 r0 = r8.A01;
        A0I.setText(C117305Zk.A0e(context, this.A01, r0.A00, r0.A01, 1));
    }

    public final void A1A(View view, C1316363n r8, String str) {
        C12960it.A0I(view, R.id.novi_send_money_review_transaction_line_item_lhs).setText(str);
        TextView A0I = C12960it.A0I(view, R.id.novi_send_money_review_transaction_line_item_rhs);
        Context context = view.getContext();
        AnonymousClass6F2 r0 = r8.A02;
        A0I.setText(C117305Zk.A0e(context, this.A01, r0.A00, r0.A01, 1));
    }
}
