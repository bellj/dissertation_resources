package com.whatsapp.payments.ui.widget;

import X.AbstractC15460nI;
import X.AbstractC30791Yv;
import X.AbstractC49172Jp;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass102;
import X.AnonymousClass14X;
import X.AnonymousClass23M;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4AB;
import X.AnonymousClass4CK;
import X.AnonymousClass5TC;
import X.AnonymousClass6D0;
import X.C117295Zj;
import X.C117305Zk;
import X.C118065bD;
import X.C127175u4;
import X.C12960it;
import X.C12970iu;
import X.C15450nH;
import X.C17900ra;
import X.C30821Yy;
import X.C30931Zj;
import X.C49152Jn;
import X.C49162Jo;
import X.EnumC49142Jm;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.InterceptingEditText;
import com.whatsapp.QrImageView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.IndiaUpiDisplaySecureQrCodeView;
import com.whatsapp.payments.ui.widget.PaymentAmountInputField;
import java.math.BigDecimal;
import java.util.EnumMap;

/* loaded from: classes4.dex */
public class IndiaUpiDisplaySecureQrCodeView extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public FrameLayout A01;
    public ImageView A02;
    public LinearLayout A03;
    public LinearLayout A04;
    public TextView A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public C49162Jo A09;
    public QrImageView A0A;
    public C15450nH A0B;
    public AnonymousClass018 A0C;
    public AnonymousClass102 A0D;
    public C17900ra A0E;
    public PaymentAmountInputField A0F;
    public C118065bD A0G;
    public AnonymousClass2P7 A0H;
    public boolean A0I;
    public final C30931Zj A0J;

    public IndiaUpiDisplaySecureQrCodeView(Context context) {
        super(context);
        A00();
        this.A0J = C117295Zj.A0G("IndiaUpiDisplaySecureQrCodeView");
        A02();
    }

    public IndiaUpiDisplaySecureQrCodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0J = C117295Zj.A0G("IndiaUpiDisplaySecureQrCodeView");
        A02();
    }

    public IndiaUpiDisplaySecureQrCodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0J = C117295Zj.A0G("IndiaUpiDisplaySecureQrCodeView");
        A02();
    }

    public IndiaUpiDisplaySecureQrCodeView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A0J = C117295Zj.A0G("IndiaUpiDisplaySecureQrCodeView");
        A02();
    }

    public IndiaUpiDisplaySecureQrCodeView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0I) {
            this.A0I = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A0B = (C15450nH) A00.AII.get();
            this.A0C = C12960it.A0R(A00);
            this.A0E = (C17900ra) A00.AF5.get();
            this.A0D = C117305Zk.A0G(A00);
        }
    }

    public void A01() {
        String str = this.A0G.A04().A05;
        if (str != null) {
            this.A06.setText(AnonymousClass14X.A02(getContext(), this.A0C, this.A0E.A00(), C117305Zk.A0E(this.A0E.A00(), str)));
        }
    }

    public final void A02() {
        C12960it.A0E(this).inflate(R.layout.india_upi_display_qr_code_view, (ViewGroup) this, true);
        setOrientation(1);
        this.A0A = (QrImageView) findViewById(R.id.qr_code);
        this.A05 = C12960it.A0I(this, R.id.add_amount);
        this.A06 = C12960it.A0I(this, R.id.display_payment_amount);
        this.A07 = C12960it.A0I(this, R.id.amount_input_error_text);
        this.A02 = C12970iu.A0K(this, R.id.dashed_underline);
        this.A0F = (PaymentAmountInputField) AnonymousClass028.A0D(this, R.id.user_payment_amount);
        AbstractC30791Yv A02 = this.A0D.A02("INR");
        PaymentAmountInputField paymentAmountInputField = this.A0F;
        paymentAmountInputField.A0E = A02;
        paymentAmountInputField.A03 = 1;
        C30821Yy A0D = C117295Zj.A0D(A02, new BigDecimal(this.A0B.A02(AbstractC15460nI.A1y)));
        this.A0F.A0G = new AnonymousClass6D0(getContext(), this.A0C, A02, A0D, A0D, A0D, null);
        this.A03 = C117305Zk.A07(this, R.id.add_or_display_amount);
        this.A00 = AnonymousClass028.A0D(this, R.id.user_amount_input);
        this.A04 = (LinearLayout) findViewById(R.id.qr_code_signing_secure_title_container);
        this.A08 = C12960it.A0J(this, R.id.qr_code_signing_retry_text);
        this.A01 = (FrameLayout) findViewById(R.id.progress_container);
    }

    public void A03(C127175u4 r7) {
        TextView textView;
        int i = r7.A01;
        int i2 = r7.A00;
        if (i != 0) {
            if (i == 1) {
                this.A0A.setVisibility(8);
                this.A03.setVisibility(8);
                this.A01.setVisibility(8);
                this.A07.setVisibility(4);
                this.A00.setVisibility(0);
                this.A0F.requestFocus();
                this.A0F.A04(true);
            } else if (i == 2) {
                this.A01.setVisibility(0);
                this.A0A.setVisibility(8);
                this.A00.setVisibility(8);
                this.A03.setVisibility(8);
                this.A0F.A03();
            } else {
                return;
            }
            this.A0B.A05(AbstractC15460nI.A0v);
            this.A04.setVisibility(8);
            textView = this.A08;
        } else {
            this.A01.setVisibility(8);
            this.A0F.A03();
            this.A0A.setVisibility(0);
            this.A00.setVisibility(8);
            this.A03.setVisibility(0);
            try {
                EnumMap enumMap = new EnumMap(AnonymousClass4AB.class);
                C49162Jo A00 = C49152Jn.A00(EnumC49142Jm.A02, this.A0G.A04().A05(), enumMap);
                this.A09 = A00;
                this.A0A.setQrCode(A00, new AbstractC49172Jp(i2) { // from class: X.66g
                    public final /* synthetic */ int A00;

                    {
                        this.A00 = r2;
                    }

                    @Override // X.AbstractC49172Jp
                    public final void AOS(QrImageView qrImageView) {
                        IndiaUpiDisplaySecureQrCodeView indiaUpiDisplaySecureQrCodeView = IndiaUpiDisplaySecureQrCodeView.this;
                        int i3 = this.A00;
                        C118065bD r2 = indiaUpiDisplaySecureQrCodeView.A0G;
                        r2.A02.A0B(new C127175u4(3, i3));
                    }
                });
            } catch (AnonymousClass4CK e) {
                this.A0J.A0A("display-qrcode/", e);
            }
            if (!this.A0B.A05(AbstractC15460nI.A0v)) {
                this.A04.setVisibility(8);
                this.A08.setVisibility(8);
            } else {
                boolean isEmpty = TextUtils.isEmpty(this.A0G.A04().A09);
                TextView textView2 = this.A08;
                if (!isEmpty) {
                    textView2.setVisibility(8);
                    this.A04.setVisibility(0);
                } else {
                    textView2.setVisibility(0);
                    this.A04.setVisibility(8);
                }
            }
            boolean isEmpty2 = TextUtils.isEmpty(this.A0G.A04().A05);
            TextView textView3 = this.A06;
            if (!isEmpty2) {
                textView3.setVisibility(0);
                this.A02.setVisibility(0);
                textView = this.A05;
            } else {
                textView3.setVisibility(8);
                this.A02.setVisibility(8);
                this.A05.setVisibility(0);
                return;
            }
        }
        textView.setVisibility(8);
    }

    public void A04(boolean z) {
        TextView textView;
        int i = 8;
        if (z) {
            this.A05.setVisibility(8);
            this.A02.setVisibility(8);
            textView = this.A08;
        } else {
            if (this.A06.getVisibility() == 8) {
                this.A05.setVisibility(0);
            } else {
                this.A02.setVisibility(0);
            }
            boolean A05 = this.A0B.A05(AbstractC15460nI.A0v);
            textView = this.A08;
            if (A05 && this.A04.getVisibility() != 0) {
                i = 0;
            }
        }
        textView.setVisibility(i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0H;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0H = r0;
        }
        return r0.generatedComponent();
    }

    public C49162Jo getQrCode() {
        return this.A09;
    }

    public String getUserInputAmount() {
        return C12970iu.A0p(this.A0F);
    }

    public void setup(C118065bD r5) {
        this.A0G = r5;
        C117295Zj.A0n(this.A03, r5, 181);
        this.A08.setText(AnonymousClass23M.A08(new Runnable() { // from class: X.6FD
            @Override // java.lang.Runnable
            public final void run() {
            }
        }, getContext().getString(R.string.upi_signing_qr_code_failed_retry_message), "try-again"));
        C117295Zj.A0n(this.A08, r5, 180);
        this.A0F.A08 = findViewById(R.id.send_payment_amount_container);
        this.A0F.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.64j
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                IndiaUpiDisplaySecureQrCodeView indiaUpiDisplaySecureQrCodeView = IndiaUpiDisplaySecureQrCodeView.this;
                if (z && !TextUtils.isEmpty(indiaUpiDisplaySecureQrCodeView.A0F.getText())) {
                    PaymentAmountInputField paymentAmountInputField = indiaUpiDisplaySecureQrCodeView.A0F;
                    paymentAmountInputField.setSelection(paymentAmountInputField.getText().length());
                }
            }
        });
        this.A0F.setErrorTextView(this.A07);
        this.A0F.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.65K
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                IndiaUpiDisplaySecureQrCodeView indiaUpiDisplaySecureQrCodeView = IndiaUpiDisplaySecureQrCodeView.this;
                if (i != 6) {
                    return false;
                }
                C118065bD r2 = indiaUpiDisplaySecureQrCodeView.A0G;
                r2.A03.A0B(C12970iu.A0p(indiaUpiDisplaySecureQrCodeView.A0F));
                return true;
            }
        });
        ((InterceptingEditText) this.A0F).A00 = new AnonymousClass5TC() { // from class: X.66e
            @Override // X.AnonymousClass5TC
            public final void AMn() {
                IndiaUpiDisplaySecureQrCodeView indiaUpiDisplaySecureQrCodeView = IndiaUpiDisplaySecureQrCodeView.this;
                C118065bD r2 = indiaUpiDisplaySecureQrCodeView.A0G;
                r2.A03.A0B(C12970iu.A0p(indiaUpiDisplaySecureQrCodeView.A0F));
            }
        };
    }
}
