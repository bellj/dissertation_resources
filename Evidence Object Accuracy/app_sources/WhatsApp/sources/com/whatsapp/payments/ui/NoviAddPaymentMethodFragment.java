package com.whatsapp.payments.ui;

import X.AnonymousClass028;
import X.AnonymousClass60Y;
import X.C117295Zj;
import X.C126045sF;
import X.C12960it;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviAddPaymentMethodFragment extends Hilt_NoviAddPaymentMethodFragment {
    public AnonymousClass60Y A00;
    public C126045sF A01;

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass60Y.A01(this.A00, "NAVIGATION_START", "SEND_MONEY", "ADD_DC_INFO", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_education_fragment);
        C117295Zj.A0n(AnonymousClass028.A0D(A0F, R.id.send_money_review_header_close), this, 76);
        C12960it.A0I(A0F, R.id.novi_education_description).setText(R.string.novi_add_payment_method_description);
        TextView A0I = C12960it.A0I(A0F, R.id.novi_education_action_button);
        A0I.setText(R.string.novi_add_debit_card_title);
        C117295Zj.A0n(A0I, this, 77);
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        AnonymousClass60Y.A01(this.A00, "NAVIGATION_END", "SEND_MONEY", "ADD_DC_INFO", "SCREEN");
    }
}
