package com.whatsapp.payments.ui;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C123405n6;
import X.C123425n8;

/* loaded from: classes4.dex */
public class BrazilPaymentIntegrityAppealActivity extends BrazilPaymentContactSupportActivity {
    public C123425n8 A00;
    public boolean A01;

    public BrazilPaymentIntegrityAppealActivity() {
        this(0);
    }

    public BrazilPaymentIntegrityAppealActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 20);
    }

    @Override // X.AbstractActivityC121445i2, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((BrazilPaymentContactSupportActivity) this).A00 = (C123405n6) A09.A03.get();
            this.A00 = (C123425n8) A09.A05.get();
        }
    }
}
