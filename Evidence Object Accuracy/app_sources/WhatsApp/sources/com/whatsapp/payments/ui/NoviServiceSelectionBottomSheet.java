package com.whatsapp.payments.ui;

import X.AnonymousClass018;
import X.C117305Zk;
import X.C117315Zl;
import X.C117915ay;
import X.C118225bT;
import X.C118545bz;
import X.C118665cB;
import X.C128375w0;
import X.C12960it;
import X.C12990iw;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviServiceSelectionBottomSheet extends Hilt_NoviServiceSelectionBottomSheet {
    public RecyclerView A00;
    public AnonymousClass018 A01;
    public C128375w0 A02;
    public C117915ay A03;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.service_selection_bottom_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A00 = (RecyclerView) view.findViewById(R.id.payment_selection_items);
        C118545bz r5 = new C118545bz(this.A01, this);
        C117915ay r4 = (C117915ay) C117315Zl.A06(new C118225bT(this.A02), this).A00(C117915ay.class);
        this.A03 = r4;
        IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(r5, 101);
        IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(this, 100);
        IDxObserverShape5S0100000_3_I1 A0B3 = C117305Zk.A0B(this, 99);
        r4.A01.A05(this, A0B);
        r4.A02.A05(this, A0B2);
        r4.A00.A05(this, A0B3);
        this.A00.setAdapter(r5);
        RecyclerView recyclerView = this.A00;
        A0p();
        C12990iw.A1K(recyclerView);
        this.A00.A0l(new C118665cB(view.getContext()));
    }
}
