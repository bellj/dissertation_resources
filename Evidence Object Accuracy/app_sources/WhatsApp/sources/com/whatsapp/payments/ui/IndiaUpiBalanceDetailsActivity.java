package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractC005102i;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C117325Zm;
import X.C119755f3;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1311161i;
import X.C30931Zj;
import android.os.Bundle;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiBalanceDetailsActivity extends AbstractActivityC121665jA {
    public TextView A00;
    public TextView A01;
    public TextView A02;
    public boolean A03;
    public final C30931Zj A04;

    public IndiaUpiBalanceDetailsActivity() {
        this(0);
        this.A04 = C117295Zj.A0G("IndiaUpiBalanceDetailsActivity");
    }

    public IndiaUpiBalanceDetailsActivity(int i) {
        this.A03 = false;
        C117295Zj.A0p(this, 31);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int A0E;
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        setContentView(R.layout.india_upi_account_balance_details);
        if (getIntent() == null || C12990iw.A0H(this) == null || C12990iw.A0H(this).get("payment_bank_account") == null || C12990iw.A0H(this).get("balance") == null) {
            this.A04.A04("got null bank account or balance; finishing");
            finish();
            return;
        }
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.account_balance_title);
        }
        this.A04.A06("onCreate");
        this.A02 = C12970iu.A0M(this, R.id.balance_text);
        this.A00 = C12970iu.A0M(this, R.id.account_name_text);
        this.A01 = C12970iu.A0M(this, R.id.account_type_text);
        AbstractC28901Pl r3 = (AbstractC28901Pl) C12990iw.A0H(this).get("payment_bank_account");
        String A07 = C1311161i.A07(r3);
        TextView textView = this.A00;
        StringBuilder A0j = C12960it.A0j(r3.A0B);
        C117325Zm.A09(A0j);
        textView.setText(C12960it.A0d(A07, A0j));
        C119755f3 r2 = (C119755f3) r3.A08;
        TextView textView2 = this.A01;
        if (r2 == null) {
            A0E = R.string.check_balance_account_type_unknown;
        } else {
            A0E = r2.A0E();
        }
        textView2.setText(A0E);
        this.A02.setText(getIntent().getStringExtra("balance"));
        if (r2 != null) {
            String str = r2.A0B;
            if ("OD_UNSECURED".equals(str) || "OD_SECURED".equals(str)) {
                C12970iu.A0M(this, R.id.balance).setText(R.string.account_current_balance);
                findViewById(R.id.available_balance_layout).setVisibility(0);
                C12970iu.A1N(this, R.id.divider_above_available_balance, 0);
                C12970iu.A0M(this, R.id.available_balance_text).setText(getIntent().getStringExtra("usable_balance"));
            }
        }
    }
}
