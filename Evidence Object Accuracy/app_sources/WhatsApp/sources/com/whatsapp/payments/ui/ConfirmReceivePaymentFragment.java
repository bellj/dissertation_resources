package com.whatsapp.payments.ui;

import X.AbstractC28901Pl;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass1ZY;
import X.AnonymousClass4UZ;
import X.AnonymousClass6M1;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C120085fb;
import X.C12960it;
import X.C12980iv;
import X.C1311161i;
import X.C14580lf;
import X.C17070qD;
import X.C248217a;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* loaded from: classes4.dex */
public abstract class ConfirmReceivePaymentFragment extends Hilt_ConfirmReceivePaymentFragment implements AnonymousClass6M1 {
    public Button A00;
    public C14580lf A01;
    public AbstractC28901Pl A02;
    public C248217a A03;
    public C17070qD A04;
    public PaymentMethodRow A05;
    public final AnonymousClass4UZ A06 = new C120085fb(this);

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.confirm_receive_payment_fragment);
        this.A05 = (PaymentMethodRow) A0F.findViewById(R.id.payment_method_row);
        this.A00 = (Button) A0F.findViewById(R.id.confirm_payment);
        View findViewById = A0F.findViewById(R.id.add_another_method);
        A0F.findViewById(R.id.account_number_divider).setVisibility(8);
        C12980iv.A1B(A0F, R.id.payment_method_account_id, 8);
        AbstractC28901Pl r0 = this.A02;
        AnonymousClass009.A05(r0);
        ATY(r0);
        AnonymousClass01E r2 = this.A0D;
        if (r2 != null) {
            C117295Zj.A0o(A0F.findViewById(R.id.payment_method_container), this, r2, 9);
            C117295Zj.A0o(findViewById, this, r2, 10);
        }
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A03.A04(this.A06);
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        C14580lf r0 = this.A01;
        if (r0 != null) {
            r0.A04();
        }
        this.A01 = C117305Zk.A0C(this.A04);
        this.A02 = (AbstractC28901Pl) C117315Zl.A03(A03(), "args_payment_method");
        this.A03.A03(this.A06);
    }

    @Override // X.AnonymousClass6M1
    public void ATY(AbstractC28901Pl r7) {
        this.A02 = r7;
        BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment = (BrazilConfirmReceivePaymentFragment) this;
        ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A05.A02.setVisibility(0);
        PaymentMethodRow paymentMethodRow = ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A05;
        paymentMethodRow.A05.setText(C1311161i.A02(brazilConfirmReceivePaymentFragment.A01(), brazilConfirmReceivePaymentFragment.A04, r7, ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A04, true));
        AnonymousClass1ZY r0 = r7.A08;
        AnonymousClass009.A05(r0);
        if (!r0.A0A()) {
            ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A05.A02(brazilConfirmReceivePaymentFragment.A0I(R.string.payment_method_unverified));
        }
        PaymentMethodRow paymentMethodRow2 = ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A05;
        if (C1311161i.A0B(r7)) {
            brazilConfirmReceivePaymentFragment.A0H.A02(r7, paymentMethodRow2);
        }
        ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A05.A03(true);
        C117295Zj.A0o(this.A00, this, r7, 8);
    }
}
