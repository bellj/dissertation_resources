package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121655j9;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass1IR;
import X.AnonymousClass2FL;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117945b1;
import X.C118455bq;
import X.C120565gO;
import X.C128355vy;
import X.C12970iu;
import X.C14900mE;
import X.C18590sh;
import X.C452120p;
import X.C64513Fv;
import X.DialogInterface$OnClickListenerC117805aj;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiPauseMandateActivity;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/* loaded from: classes4.dex */
public class IndiaUpiPauseMandateActivity extends AbstractActivityC121655j9 {
    public Button A00;
    public DatePicker A01;
    public DatePicker A02;
    public TextInputLayout A03;
    public TextInputLayout A04;
    public AnonymousClass61M A05;
    public C117945b1 A06;
    public C128355vy A07;
    public boolean A08;

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r1) {
    }

    @Override // X.AbstractC124835qC
    public boolean Adt() {
        return true;
    }

    public IndiaUpiPauseMandateActivity() {
        this(0);
    }

    public IndiaUpiPauseMandateActivity(int i) {
        this.A08 = false;
        C117295Zj.A0p(this, 51);
    }

    public static final long A02(DatePicker datePicker) {
        return new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth()).getTime().getTime();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
            AbstractActivityC119235dO.A1V(A09, A1M, this);
            this.A05 = C117305Zk.A0N(A1M);
            this.A07 = (C128355vy) A1M.A9f.get();
        }
    }

    public final DatePicker A3P(EditText editText, long j) {
        DateFormat dateInstance = DateFormat.getDateInstance(2, C12970iu.A14(((AbstractActivityC121545iU) this).A01));
        editText.setText(dateInstance.format(Long.valueOf(j)));
        Calendar instance = Calendar.getInstance();
        DialogInterface$OnClickListenerC117805aj r3 = new DialogInterface$OnClickListenerC117805aj(new DatePickerDialog.OnDateSetListener(editText, this, dateInstance) { // from class: X.61w
            public final /* synthetic */ EditText A00;
            public final /* synthetic */ IndiaUpiPauseMandateActivity A01;
            public final /* synthetic */ DateFormat A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // android.app.DatePickerDialog.OnDateSetListener
            public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                IndiaUpiPauseMandateActivity indiaUpiPauseMandateActivity = this.A01;
                this.A00.setText(this.A02.format(Long.valueOf(IndiaUpiPauseMandateActivity.A02(datePicker))));
                indiaUpiPauseMandateActivity.A3Q();
            }
        }, this, instance.get(1), instance.get(2), instance.get(5));
        C117295Zj.A0n(editText, r3, 43);
        return r3.A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0062, code lost:
        if (android.text.TextUtils.isEmpty(r11.A03.getError()) == false) goto L_0x0064;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3Q() {
        /*
            r11 = this;
            android.widget.DatePicker r0 = r11.A02
            long r2 = A02(r0)
            com.google.android.material.textfield.TextInputLayout r5 = r11.A04
            X.5b1 r4 = r11.A06
            long r0 = java.lang.System.currentTimeMillis()
            int r0 = X.C38121nY.A00(r2, r0)
            if (r0 >= 0) goto L_0x00ac
            X.0pI r1 = r4.A06
            r0 = 2131892626(0x7f121992, float:1.9420006E38)
            java.lang.String r0 = r1.A02(r0)
        L_0x001d:
            r5.setError(r0)
            android.widget.DatePicker r0 = r11.A01
            long r0 = A02(r0)
            com.google.android.material.textfield.TextInputLayout r9 = r11.A03
            X.5b1 r10 = r11.A06
            X.018 r4 = r10.A07
            java.util.Locale r5 = X.C12970iu.A14(r4)
            r4 = 2
            java.text.DateFormat r8 = java.text.DateFormat.getDateInstance(r4, r5)
            int r2 = X.C38121nY.A00(r0, r2)
            if (r2 > 0) goto L_0x0069
            X.0pI r0 = r10.A06
            android.content.Context r1 = r0.A00
            r0 = 2131892624(0x7f121990, float:1.9420002E38)
            java.lang.String r0 = r1.getString(r0)
        L_0x0046:
            r9.setError(r0)
            android.widget.Button r2 = r11.A00
            com.google.android.material.textfield.TextInputLayout r0 = r11.A04
            java.lang.CharSequence r0 = r0.getError()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0064
            com.google.android.material.textfield.TextInputLayout r0 = r11.A03
            java.lang.CharSequence r0 = r0.getError()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0065
        L_0x0064:
            r0 = 0
        L_0x0065:
            r2.setEnabled(r0)
            return
        L_0x0069:
            X.1IR r2 = r10.A01
            X.1Zf r2 = r2.A0A
            X.AnonymousClass009.A05(r2)
            X.5fB r2 = (X.C119835fB) r2
            X.60R r2 = r2.A0B
            X.AnonymousClass009.A05(r2)
            long r4 = r2.A01
            java.lang.String r2 = "Asia/Kolkata"
            java.util.TimeZone r2 = java.util.TimeZone.getTimeZone(r2)
            int r2 = r2.getRawOffset()
            long r2 = (long) r2
            long r4 = r4 - r2
            int r0 = X.C38121nY.A00(r0, r4)
            if (r0 <= 0) goto L_0x00aa
            X.0pI r0 = r10.A06
            android.content.Context r7 = r0.A00
            r6 = 2131892623(0x7f12198f, float:1.942E38)
            java.lang.Object[] r3 = X.C12970iu.A1b()
            r2 = 0
            X.0m7 r0 = r10.A05
            long r0 = r0.A02(r4)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = r8.format(r0)
            java.lang.String r0 = X.C12960it.A0X(r7, r0, r3, r2, r6)
            goto L_0x0046
        L_0x00aa:
            r0 = 0
            goto L_0x0046
        L_0x00ac:
            r0 = 0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiPauseMandateActivity.A3Q():void");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        setResult(0);
        finish();
    }

    @Override // X.AbstractActivityC121655j9, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C14900mE r5 = ((ActivityC13810kN) this).A05;
        C64513Fv r7 = ((AbstractActivityC121545iU) this).A06;
        C18590sh r10 = ((AbstractActivityC121545iU) this).A0C;
        C120565gO r3 = new C120565gO(this, r5, ((AbstractActivityC121685jC) this).A0K, r7, ((AbstractActivityC121685jC) this).A0M, ((AbstractActivityC121545iU) this).A08, r10);
        setContentView(R.layout.india_upi_pause_mandate);
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            A0K.A0M(true);
        }
        TextInputLayout textInputLayout = (TextInputLayout) AnonymousClass00T.A05(this, R.id.start_date);
        this.A04 = textInputLayout;
        long currentTimeMillis = System.currentTimeMillis();
        EditText editText = textInputLayout.A0L;
        AnonymousClass009.A03(editText);
        this.A02 = A3P(editText, currentTimeMillis);
        TextInputLayout textInputLayout2 = (TextInputLayout) AnonymousClass00T.A05(this, R.id.end_date);
        this.A03 = textInputLayout2;
        EditText editText2 = textInputLayout2.A0L;
        AnonymousClass009.A03(editText2);
        this.A01 = A3P(editText2, currentTimeMillis);
        Button button = (Button) AnonymousClass00T.A05(this, R.id.continue_button);
        this.A00 = button;
        C117295Zj.A0n(button, this, 44);
        C117945b1 r2 = (C117945b1) C117315Zl.A06(new C118455bq(r3, this.A07, AbstractActivityC119235dO.A0O(this)), this).A00(C117945b1.class);
        this.A06 = r2;
        r2.A02.A05(this, C117305Zk.A0B(this, 45));
        C117945b1 r32 = this.A06;
        AnonymousClass1IR r22 = (AnonymousClass1IR) getIntent().getParcelableExtra("extra_transaction_detail_data");
        r32.A01 = r22;
        r32.A0D.Ab2(new Runnable(r22, r32) { // from class: X.6Ij
            public final /* synthetic */ AnonymousClass1IR A00;
            public final /* synthetic */ C117945b1 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C117945b1 r23 = this.A01;
                AbstractC28901Pl A08 = r23.A08.A08(this.A00.A0H);
                r23.A00 = A08;
                if (A08 == null) {
                    r23.A02.A0A(new C128225vl(1));
                }
            }
        });
    }
}
