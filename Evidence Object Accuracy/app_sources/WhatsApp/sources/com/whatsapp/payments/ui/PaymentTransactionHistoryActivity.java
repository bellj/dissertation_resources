package com.whatsapp.payments.ui;

import X.AbstractActivityC119305dV;
import X.AbstractC005102i;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC35651iS;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass14X;
import X.AnonymousClass1A8;
import X.AnonymousClass1In;
import X.AnonymousClass5UV;
import X.AnonymousClass65Q;
import X.AnonymousClass69S;
import X.AnonymousClass6LY;
import X.AnonymousClass6M3;
import X.C004802e;
import X.C015607k;
import X.C117295Zj;
import X.C117305Zk;
import X.C118645c9;
import X.C121865kK;
import X.C121925kV;
import X.C121935kW;
import X.C124225oo;
import X.C124275ot;
import X.C12960it;
import X.C12970iu;
import X.C129795yJ;
import X.C12980iv;
import X.C12990iw;
import X.C133886Cm;
import X.C15240mn;
import X.C15650ng;
import X.C17070qD;
import X.C20380vf;
import X.C22710zW;
import X.C243515e;
import X.C30931Zj;
import X.C30941Zk;
import X.C30961Zm;
import X.C48232Fc;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviPaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.widget.MultiExclusionChip;
import com.whatsapp.payments.ui.widget.MultiExclusionChipGroup;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public class PaymentTransactionHistoryActivity extends AbstractActivityC119305dV implements AnonymousClass1In, AnonymousClass5UV, AnonymousClass6LY {
    public int A00;
    public View A01;
    public ProgressBar A02;
    public TextView A03;
    public C48232Fc A04;
    public AnonymousClass018 A05;
    public C15650ng A06;
    public C15240mn A07;
    public AbstractC14640lm A08;
    public C20380vf A09;
    public C243515e A0A;
    public C22710zW A0B;
    public C17070qD A0C;
    public AnonymousClass1A8 A0D;
    public C124275ot A0E;
    public C124225oo A0F;
    public C118645c9 A0G;
    public C129795yJ A0H;
    public MultiExclusionChipGroup A0I;
    public AnonymousClass14X A0J;
    public String A0K;
    public ArrayList A0L;
    public boolean A0M = false;
    public boolean A0N = false;
    public boolean A0O = false;
    public boolean A0P = false;
    public boolean A0Q = false;
    public boolean A0R = false;
    public boolean A0S = false;
    public final AbstractC35651iS A0T = new AnonymousClass69S(this);
    public final C30931Zj A0U = C117305Zk.A0V("PaymentTransactionHistoryActivity", "payment-settings");
    public final C30941Zk A0V = new C30941Zk();
    public final ArrayList A0W = C12960it.A0l();

    public final MultiExclusionChip A2e(String str) {
        MultiExclusionChip multiExclusionChip = (MultiExclusionChip) getLayoutInflater().inflate(R.layout.payment_filter_chip, (ViewGroup) null);
        C015607k.A0A(multiExclusionChip.getCheckedIcon(), getResources().getColor(R.color.searchBackground));
        multiExclusionChip.setText(str);
        return multiExclusionChip;
    }

    public void A2f() {
        C124275ot r3;
        C124275ot r0 = this.A0E;
        if (r0 != null) {
            r0.A03(true);
        }
        C124225oo r02 = this.A0F;
        if (r02 != null) {
            r02.A03(true);
        }
        boolean z = this.A0O;
        View view = this.A01;
        if (z) {
            view.setVisibility(0);
            return;
        }
        view.setVisibility(8);
        if (!((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0z) || TextUtils.isEmpty(this.A0K) || this.A08 != null) {
            if (!(this instanceof NoviPaymentTransactionHistoryActivity)) {
                r3 = new C124275ot(new AnonymousClass6M3() { // from class: X.6CW
                    @Override // X.AnonymousClass6M3
                    public final void AVd(C30941Zk r2, String str, List list, List list2) {
                        PaymentTransactionHistoryActivity.this.A2h(r2, str, list, list2);
                    }
                }, this, this.A0H, this.A0L);
            } else {
                NoviPaymentTransactionHistoryActivity noviPaymentTransactionHistoryActivity = (NoviPaymentTransactionHistoryActivity) this;
                r3 = new C121865kK(noviPaymentTransactionHistoryActivity, new AnonymousClass6M3() { // from class: X.6CV
                    @Override // X.AnonymousClass6M3
                    public final void AVd(C30941Zk r2, String str, List list, List list2) {
                        NoviPaymentTransactionHistoryActivity.this.A2h(r2, str, list, list2);
                    }
                }, ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A0H, ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A0L);
            }
            this.A0E = r3;
            C12990iw.A1N(r3, ((ActivityC13830kP) this).A05);
            return;
        }
        AnonymousClass14X r8 = this.A0J;
        C124225oo r1 = new C124225oo(this.A05, this.A07, this.A0C, this.A0V, new AnonymousClass6M3() { // from class: X.6CW
            @Override // X.AnonymousClass6M3
            public final void AVd(C30941Zk r2, String str, List list, List list2) {
                PaymentTransactionHistoryActivity.this.A2h(r2, str, list, list2);
            }
        }, this.A0H, r8, this.A0K, this.A0R);
        this.A0F = r1;
        C12990iw.A1N(r1, ((ActivityC13830kP) this).A05);
    }

    public final void A2g() {
        this.A04.A04(true);
        MultiExclusionChipGroup multiExclusionChipGroup = this.A0I;
        if (multiExclusionChipGroup != null) {
            for (int i = 0; i < multiExclusionChipGroup.getChildCount(); i++) {
                ((CompoundButton) multiExclusionChipGroup.getChildAt(i)).setChecked(false);
            }
            this.A0I.setVisibility(8);
        }
        A2f();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        if (r0.A01 == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009c, code lost:
        if (r0.A01 == false) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2h(X.C30941Zk r6, java.lang.String r7, java.util.List r8, java.util.List r9) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.PaymentTransactionHistoryActivity.A2h(X.1Zk, java.lang.String, java.util.List, java.util.List):void");
    }

    public final boolean A2i() {
        if (!isTaskRoot()) {
            return false;
        }
        Class AFa = this.A0C.A02().AFa();
        this.A0U.A06(C12960it.A0b("PaymentTransactionHistoryActivity maybeOpenPaymentSettings ", AFa));
        Intent A0D = C12990iw.A0D(this, AFa);
        if (Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask();
            startActivity(A0D);
            return true;
        }
        startActivity(A0D);
        return false;
    }

    @Override // X.AnonymousClass5UV
    public void AOr(String str) {
        this.A0G.A02();
    }

    @Override // X.AnonymousClass1In
    public void ATd() {
        A2f();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A04.A05()) {
            A2g();
        } else if (!A2i()) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C118645c9 r5;
        String stringExtra;
        int i;
        C117295Zj.A0d(this);
        super.onCreate(bundle);
        AnonymousClass009.A0F(this.A0B.A03());
        setContentView(R.layout.payment_transaction_history);
        this.A00 = getIntent().getIntExtra("extra_payment_flow_entry_point", 3);
        ((ActivityC13830kP) this).A05.Ab2(new Runnable() { // from class: X.6Gw
            @Override // java.lang.Runnable
            public final void run() {
                C20380vf.this.A00();
            }
        });
        this.A0A.A03(this.A0T);
        boolean z = this instanceof NoviPaymentTransactionHistoryActivity;
        if (!z) {
            boolean z2 = this instanceof IndiaPaymentTransactionHistoryActivity;
            AnonymousClass14X r13 = this.A0J;
            AnonymousClass018 r7 = this.A05;
            C30931Zj r10 = this.A0U;
            C15650ng r8 = this.A06;
            ArrayList A0l = C12960it.A0l();
            C129795yJ r12 = this.A0H;
            int i2 = this.A00;
            if (!z2) {
                r5 = new C118645c9(this, r7, r8, this, r10, this, r12, r13, A0l, i2);
            } else {
                r5 = new C121935kW(this, r7, r8, this, r10, this, r12, r13, A0l, i2);
            }
        } else {
            NoviPaymentTransactionHistoryActivity noviPaymentTransactionHistoryActivity = (NoviPaymentTransactionHistoryActivity) this;
            AnonymousClass14X r2 = ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A0J;
            AnonymousClass018 r9 = ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A05;
            C30931Zj r122 = noviPaymentTransactionHistoryActivity.A0U;
            r5 = new C121925kV(noviPaymentTransactionHistoryActivity, r9, ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A06, noviPaymentTransactionHistoryActivity, r122, noviPaymentTransactionHistoryActivity, noviPaymentTransactionHistoryActivity, ((PaymentTransactionHistoryActivity) noviPaymentTransactionHistoryActivity).A0H, r2, C12960it.A0l());
        }
        this.A0G = r5;
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.transaction_list);
        recyclerView.setAdapter(this.A0G);
        AnonymousClass028.A0m(recyclerView, true);
        AnonymousClass028.A0m(findViewById(16908292), true);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        this.A02 = progressBar;
        progressBar.setVisibility(0);
        this.A01 = findViewById(R.id.empty_list_container);
        this.A03 = C12970iu.A0M(this, R.id.empty_container_text);
        Toolbar A08 = C117305Zk.A08(this);
        A1e(A08);
        this.A0O = getIntent().getBooleanExtra("extra_show_empty_list_screen", false);
        this.A04 = new C48232Fc(this, findViewById(R.id.search_holder), new AnonymousClass65Q(this), A08, this.A05);
        this.A0Q = getIntent().getBooleanExtra("extra_for_mandates", false);
        this.A0P = getIntent().getBooleanExtra("extra_show_mandate_pending_requests", false);
        this.A0R = getIntent().getBooleanExtra("extra_show_requests", false);
        this.A0M = getIntent().getBooleanExtra("extra_disable_search", false);
        C30961Zm r22 = (C30961Zm) getIntent().getParcelableExtra("extra_predefined_search_filter");
        if (r22 != null) {
            this.A0V.A01 = r22;
        }
        this.A08 = AbstractC14640lm.A01(getIntent().getStringExtra("extra_jid"));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            if (this.A0R) {
                AnonymousClass018 r4 = this.A05;
                if (!z) {
                    i = R.plurals.payments_settings_payment_requests;
                } else {
                    i = R.plurals.payments_settings_pending_transactions;
                }
                stringExtra = r4.A0D(2, i);
            } else {
                stringExtra = getIntent().getStringExtra("extra_list_screen_configurable_title");
                if (TextUtils.isEmpty(stringExtra)) {
                    stringExtra = getString(R.string.payments_settings_payment_history);
                }
            }
            A1U.A0I(stringExtra);
            A1U.A0M(true);
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 100) {
            return super.onCreateDialog(i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A06(R.string.payments_request_status_requested_expired);
        A0S.A0B(false);
        C117295Zj.A0q(A0S, this, 70, R.string.ok);
        A0S.A07(R.string.payments_request_status_request_expired);
        return A0S.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!this.A0M && !this.A0O) {
            menu.add(0, R.id.menuitem_search, 0, getString(R.string.search)).setIcon(R.drawable.ic_action_search).setShowAsAction(10);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C124275ot r0 = this.A0E;
        if (r0 != null) {
            r0.A03(true);
        }
        C124225oo r02 = this.A0F;
        if (r02 != null) {
            r02.A03(true);
        }
        this.A0A.A04(this.A0T);
        this.A0E = null;
        this.A0F = null;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_search) {
            onSearchRequested();
            return true;
        } else if (itemId != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            finish();
            A2i();
            return true;
        }
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A0R = bundle.getBoolean("extra_show_requests");
        this.A08 = AbstractC14640lm.A01(bundle.getString("extra_jid"));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("extra_show_requests", this.A0R);
        AbstractC14640lm r0 = this.A08;
        if (r0 != null) {
            bundle.putString("extra_jid", r0.getRawString());
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A04.A01();
        C48232Fc r2 = this.A04;
        String string = getString(R.string.search_hint);
        SearchView searchView = r2.A02;
        if (searchView != null) {
            searchView.setQueryHint(string);
        }
        View findViewById = findViewById(R.id.search_back);
        if (((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0z) && !this.A0R && (this.A0N || this.A0S)) {
            C12970iu.A1N(this, R.id.appBarLayout, 0);
            if (this.A0I == null) {
                MultiExclusionChipGroup multiExclusionChipGroup = (MultiExclusionChipGroup) AnonymousClass028.A0D(findViewById(R.id.payment_filters), R.id.payment_filter_group);
                this.A0I = multiExclusionChipGroup;
                String string2 = getString(R.string.payment_search_filter_from_you);
                String string3 = getString(R.string.payment_search_filter_to_you);
                String string4 = getString(R.string.payments_transaction_status_complete);
                String string5 = getString(R.string.payment_search_filter_incomplete);
                MultiExclusionChip A2e = A2e(string2);
                MultiExclusionChip A2e2 = A2e(string3);
                MultiExclusionChip A2e3 = A2e(string4);
                MultiExclusionChip A2e4 = A2e(string5);
                if (this.A0S) {
                    ArrayList A0l = C12960it.A0l();
                    A0l.add(A2e);
                    A0l.add(A2e2);
                    multiExclusionChipGroup.A01(A0l);
                }
                if (this.A0N) {
                    ArrayList A0l2 = C12960it.A0l();
                    A0l2.add(A2e3);
                    A0l2.add(A2e4);
                    multiExclusionChipGroup.A01(A0l2);
                }
                multiExclusionChipGroup.A00 = new C133886Cm(this, A2e, A2e2, A2e3, A2e4);
            }
            this.A0I.setVisibility(0);
        }
        C117295Zj.A0n(findViewById, this, 111);
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        A2f();
        AnonymousClass1A8 r1 = this.A0D;
        r1.A00.clear();
        r1.A02.add(C12970iu.A10(this));
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        C124275ot r0 = this.A0E;
        if (r0 != null) {
            r0.A03(true);
        }
        C124225oo r02 = this.A0F;
        if (r02 != null) {
            r02.A03(true);
        }
        this.A0E = null;
        this.A0F = null;
        this.A0D.A01(this);
    }
}
