package com.whatsapp.payments.ui;

import X.AnonymousClass028;
import X.AnonymousClass4OZ;
import X.C117295Zj;
import X.C121885kQ;
import X.C12960it;
import X.C134136Dl;
import X.C88094Eg;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviWithdrawLocationDetailsSheet extends Hilt_NoviWithdrawLocationDetailsSheet {
    public C121885kQ A00;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_withdraw_location_details_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.novi_location_details_header_back), this, 95);
        Bundle bundle2 = this.A05;
        if (bundle2 != null) {
            Parcelable parcelable = bundle2.getParcelable("withdraw-location-data");
            C134136Dl r2 = new C134136Dl(null, this.A00.A04);
            C88094Eg.A00((ViewStub) AnonymousClass028.A0D(view, R.id.novi_withdraw_review_method), r2);
            r2.AYL(AnonymousClass028.A0D(view, R.id.novi_withdraw_review_method_inflated));
            r2.A6Q(new AnonymousClass4OZ(2, parcelable));
        }
    }
}
