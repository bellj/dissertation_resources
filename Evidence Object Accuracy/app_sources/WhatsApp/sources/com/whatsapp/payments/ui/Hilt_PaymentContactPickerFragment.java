package com.whatsapp.payments.ui;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.C117295Zj;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_PaymentContactPickerFragment extends ContactPickerFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    private void A03() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A03();
        return this.A00;
    }

    @Override // com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A03()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.Hilt_PaymentContactPickerFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment, com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A03();
        A18();
    }

    @Override // com.whatsapp.contact.picker.Hilt_ContactPickerFragment
    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            PaymentContactPickerFragment paymentContactPickerFragment = (PaymentContactPickerFragment) this;
            C51112Sw r2 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J A0B = C117295Zj.A0B(r2, paymentContactPickerFragment);
            C117295Zj.A11(A0B, paymentContactPickerFragment);
            C117295Zj.A13(A0B, paymentContactPickerFragment);
            C117295Zj.A10(A0B, paymentContactPickerFragment);
            C117295Zj.A0z(A0B, paymentContactPickerFragment);
            C117295Zj.A12(A0B, paymentContactPickerFragment);
            C117295Zj.A0v(r2, A0B, paymentContactPickerFragment);
            C117295Zj.A14(A0B, paymentContactPickerFragment);
        }
    }
}
