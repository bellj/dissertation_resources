package com.whatsapp.payments.ui;

import X.AbstractC14440lR;
import X.AbstractC467527b;
import X.AnonymousClass01J;
import X.AnonymousClass27X;
import X.AnonymousClass2P6;
import X.C117295Zj;
import X.C128615wO;
import X.C49262Kb;
import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.util.AttributeSet;
import java.io.File;

/* loaded from: classes4.dex */
public class NoviSelfieCameraView extends AnonymousClass27X implements AbstractC467527b {
    public int A00;
    public Handler A01;
    public C128615wO A02;
    public AbstractC14440lR A03;
    public File A04;
    public File A05;
    public Runnable A06;
    public Runnable A07;
    public Runnable A08;
    public boolean A09;

    @Override // X.AbstractC467527b
    public void AMh(float f, float f2) {
    }

    @Override // X.AbstractC467527b
    public void AMi(boolean z) {
    }

    @Override // X.AbstractC467527b
    public void ANb(int i) {
    }

    @Override // X.AbstractC467527b
    public void AUS(C49262Kb r1) {
    }

    @Override // X.AbstractC467527b
    public void AYD() {
    }

    public NoviSelfieCameraView(Context context) {
        super(context, null);
        A00();
        this.A0F = this;
    }

    public NoviSelfieCameraView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0F = this;
    }

    public NoviSelfieCameraView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0F = this;
    }

    public NoviSelfieCameraView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    @Override // X.AnonymousClass27Y
    public void A00() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            C117295Zj.A0y(A00, this);
            this.A03 = (AbstractC14440lR) A00.ANe.get();
            this.A02 = (C128615wO) A00.ADY.get();
        }
    }

    @Override // X.AbstractC467527b
    public void AUF() {
        if (Camera.getNumberOfCameras() > 1) {
            while (!this.A0N) {
                ALf();
            }
        }
    }

    @Override // X.AnonymousClass27X, android.view.SurfaceView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i);
    }
}
