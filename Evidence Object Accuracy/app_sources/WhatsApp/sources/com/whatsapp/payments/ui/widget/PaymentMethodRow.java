package com.whatsapp.payments.ui.widget;

import X.AbstractC117755aY;
import X.AnonymousClass028;
import X.C117305Zk;
import X.C123655nb;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes4.dex */
public class PaymentMethodRow extends AbstractC117755aY {
    public View A00;
    public ImageView A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public TextView A05;
    public ShimmerFrameLayout A06;
    public CopyableTextView A07;
    public WaImageView A08;

    public PaymentMethodRow(Context context) {
        super(context);
        A01();
    }

    public PaymentMethodRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public PaymentMethodRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public PaymentMethodRow(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
    }

    public void A01() {
        ShimmerFrameLayout shimmerFrameLayout;
        if (!(this instanceof C123655nb)) {
            C117305Zk.A14(C12960it.A0E(this), this, getLayoutRes());
            this.A00 = AnonymousClass028.A0D(this, R.id.payment_method_row_container);
            this.A01 = C12970iu.A0K(this, R.id.payment_method_provider_icon);
            this.A05 = C12960it.A0I(this, R.id.payment_method_bank_name);
            this.A07 = (CopyableTextView) AnonymousClass028.A0D(this, R.id.payment_method_account_id);
            this.A04 = C12960it.A0I(this, R.id.payment_method_provider_name);
            this.A03 = C12960it.A0I(this, R.id.payment_method_decorate);
            this.A08 = C12980iv.A0X(this, R.id.payment_method_decorate_icon);
            this.A02 = C12960it.A0I(this, R.id.payment_branding);
            this.A06 = (ShimmerFrameLayout) AnonymousClass028.A0D(this, R.id.payment_method_name_shimmer);
            this.A07.setVisibility(8);
            this.A04.setVisibility(8);
            this.A03.setVisibility(8);
            this.A08.setVisibility(8);
            this.A02.setVisibility(8);
            shimmerFrameLayout = this.A06;
        } else {
            C123655nb r2 = (C123655nb) this;
            C117305Zk.A14(C12960it.A0E(r2), r2, R.layout.novi_payment_selection_row);
            ((PaymentMethodRow) r2).A00 = AnonymousClass028.A0D(r2, R.id.payment_method_row_container);
            ((PaymentMethodRow) r2).A01 = C12970iu.A0K(r2, R.id.payment_method_provider_icon);
            r2.A05 = C12960it.A0I(r2, R.id.payment_method_bank_name);
            r2.A04 = C12960it.A0I(r2, R.id.payment_method_info);
            r2.A06 = (ShimmerFrameLayout) AnonymousClass028.A0D(r2, R.id.payment_method_name_shimmer);
            r2.A00 = (RadioButton) AnonymousClass028.A0D(r2, R.id.radio);
            r2.A04.setVisibility(8);
            shimmerFrameLayout = r2.A06;
        }
        shimmerFrameLayout.A00();
    }

    public void A02(String str) {
        int i = 0;
        if (TextUtils.isEmpty(str) || !str.contains("\n")) {
            this.A04.setSingleLine(true);
            this.A04.setEllipsize(TextUtils.TruncateAt.END);
        } else {
            this.A04.setSingleLine(false);
        }
        this.A04.setText(str);
        TextView textView = this.A04;
        if (TextUtils.isEmpty(str)) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public void A03(boolean z) {
        View view;
        if (!(this instanceof C123655nb)) {
            TextView textView = this.A05;
            Context context = getContext();
            if (!z) {
                C12960it.A0s(context, textView, R.color.payments_status_gray);
                view = this.A00;
            } else {
                C12960it.A0s(context, textView, R.color.list_item_title);
                return;
            }
        } else {
            C123655nb r3 = (C123655nb) this;
            TextView textView2 = r3.A05;
            Context context2 = r3.getContext();
            int i = R.color.payments_status_gray;
            if (z) {
                i = R.color.list_item_title;
            }
            C12960it.A0s(context2, textView2, i);
            r3.A00.setVisibility(C12960it.A02(z ? 1 : 0));
            if (!z) {
                view = ((PaymentMethodRow) r3).A00;
            } else {
                return;
            }
        }
        view.setBackground(null);
    }

    public int getLayoutRes() {
        return R.layout.payment_method_row;
    }

    public ImageView getMethodIconView() {
        return this.A01;
    }
}
