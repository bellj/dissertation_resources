package com.whatsapp.payments.ui;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import com.whatsapp.contact.picker.ContactPicker;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* loaded from: classes4.dex */
public final class IndiaUpiContactPicker extends ContactPicker {
    public boolean A00;

    public IndiaUpiContactPicker() {
        this(0);
    }

    public IndiaUpiContactPicker(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 43);
    }

    @Override // X.AnonymousClass2FU, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            C117295Zj.A0x(A1M, this, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            C117295Zj.A0u(A09, A1M, this);
        }
    }

    @Override // com.whatsapp.contact.picker.ContactPicker
    public ContactPickerFragment A2h() {
        return new IndiaUpiContactPickerFragment();
    }
}
