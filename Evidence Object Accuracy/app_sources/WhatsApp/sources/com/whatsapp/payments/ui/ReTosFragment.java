package com.whatsapp.payments.ui;

import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C12960it;
import X.C12970iu;
import X.C129925yW;
import X.C1329168s;
import X.C18610sj;
import X.C22710zW;
import X.C253118x;
import X.C30931Zj;
import X.C620233n;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public abstract class ReTosFragment extends Hilt_ReTosFragment {
    public Button A00;
    public ProgressBar A01;
    public AnonymousClass01d A02;
    public C129925yW A03;
    public C18610sj A04;
    public AnonymousClass61M A05;
    public C22710zW A06;
    public final C30931Zj A07 = C117305Zk.A0V("ReTosFragment", "onboarding");

    public static /* synthetic */ void A00(ReTosFragment reTosFragment) {
        reTosFragment.A1G(false);
        reTosFragment.A00.setVisibility(8);
        reTosFragment.A01.setVisibility(0);
        C18610sj r6 = reTosFragment.A04;
        boolean z = reTosFragment.A03().getBoolean("is_consumer");
        boolean z2 = reTosFragment.A03().getBoolean("is_merchant");
        C1329168s r4 = new C1329168s(reTosFragment);
        ArrayList A0l = C12960it.A0l();
        A0l.add(new AnonymousClass1W9("version", 2));
        if (z) {
            A0l.add(new AnonymousClass1W9("consumer", 1));
        }
        if (z2) {
            A0l.add(new AnonymousClass1W9("merchant", 1));
        }
        r6.A0G(new C620233n(r6.A05.A00, r6.A01, r4, r6.A0B, r6, z, z2), new AnonymousClass1V8("accept_pay", C117305Zk.A1b(A0l)), "set", "urn:xmpp:whatsapp:account", 0);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String A0I;
        String[] strArr;
        String[] strArr2;
        Runnable[] runnableArr;
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.retos_bottom_sheet);
        TextEmojiLabel A0T = C12970iu.A0T(A0F, R.id.retos_bottom_sheet_desc);
        AbstractC28491Nn.A04(A0T, this.A02);
        AbstractC28491Nn.A03(A0T);
        Context context = A0T.getContext();
        BrazilReTosFragment brazilReTosFragment = (BrazilReTosFragment) this;
        boolean z = brazilReTosFragment.A03().getBoolean("is_merchant");
        C253118x r14 = brazilReTosFragment.A01;
        if (z) {
            A0I = brazilReTosFragment.A0I(R.string.br_p2m_retos_bottom_sheet_desc);
            strArr = new String[]{"wa-merchant-terms", "fb-merchant-agreement", "cielo-merchant-agreement"};
            strArr2 = new String[3];
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.whatsapp.com/legal/merchant-terms/", strArr2, 0);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.facebook.com/legal/commerce_product_merchant_agreement", strArr2, 1);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.cielo.com.br/contrato-de-credenciamento-consolidado/", strArr2, 2);
            runnableArr = new Runnable[]{new Runnable() { // from class: X.6F6
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6F8
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6F4
                @Override // java.lang.Runnable
                public final void run() {
                }
            }};
        } else {
            A0I = brazilReTosFragment.A0I(R.string.br_p2p_retos_bottom_sheet_desc);
            strArr = new String[]{"wa-terms", "wa-privacy-policy", "fb-payments-terms", "fb-privacy-policy", "cielo-terms-and-privacy-policy"};
            strArr2 = new String[5];
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.whatsapp.com/legal/payments-terms-of-service-br#payments", strArr2, 0);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.whatsapp.com/legal/payments-terms-of-service-br#payments-privacy-policy", strArr2, 1);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.facebook.com/payments_terms", strArr2, 2);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.facebook.com/policy.php", strArr2, 3);
            C117295Zj.A1A(brazilReTosFragment.A00, "https://www.cielo.com.br/termos-fb-pay", strArr2, 4);
            runnableArr = new Runnable[]{new Runnable() { // from class: X.6FB
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6F5
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6FA
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6F9
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, new Runnable() { // from class: X.6F7
                @Override // java.lang.Runnable
                public final void run() {
                }
            }};
        }
        A0T.setText(r14.A01(context, A0I, runnableArr, strArr, strArr2));
        this.A01 = (ProgressBar) AnonymousClass028.A0D(A0F, R.id.progress_bar);
        Button button = (Button) AnonymousClass028.A0D(A0F, R.id.retos_bottom_sheet_button);
        this.A00 = button;
        C117295Zj.A0n(button, this, 116);
        return A0F;
    }

    public void A1M() {
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("is_consumer", true);
        A0D.putBoolean("is_merchant", false);
        A0U(A0D);
    }
}
