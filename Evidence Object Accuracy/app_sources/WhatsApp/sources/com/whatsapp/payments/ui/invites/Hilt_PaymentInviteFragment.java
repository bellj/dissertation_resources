package com.whatsapp.payments.ui.invites;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass17Z;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C121265hX;
import X.C12960it;
import X.C129685y8;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C129925yW;
import X.C130105yo;
import X.C1329668y;
import X.C15570nT;
import X.C16590pI;
import X.C18640sm;
import X.C18650sn;
import X.C20730wE;
import X.C21860y6;
import X.C22120yY;
import X.C22590zK;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;
import com.whatsapp.payments.service.Hilt_NoviPaymentInviteFragment;
import com.whatsapp.payments.service.NoviPaymentInviteFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_PaymentInviteFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A19()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.invites.Hilt_PaymentInviteFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (this instanceof Hilt_IndiaUpiPaymentInviteFragment) {
            Hilt_IndiaUpiPaymentInviteFragment hilt_IndiaUpiPaymentInviteFragment = (Hilt_IndiaUpiPaymentInviteFragment) this;
            if (!hilt_IndiaUpiPaymentInviteFragment.A02) {
                hilt_IndiaUpiPaymentInviteFragment.A02 = true;
                IndiaUpiPaymentInviteFragment indiaUpiPaymentInviteFragment = (IndiaUpiPaymentInviteFragment) hilt_IndiaUpiPaymentInviteFragment;
                AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_IndiaUpiPaymentInviteFragment.generatedComponent())).A0Y;
                C117295Zj.A19(r2, indiaUpiPaymentInviteFragment);
                indiaUpiPaymentInviteFragment.A03 = C12980iv.A0b(r2);
                indiaUpiPaymentInviteFragment.A05 = C12960it.A0S(r2);
                indiaUpiPaymentInviteFragment.A00 = C12970iu.A0R(r2);
                indiaUpiPaymentInviteFragment.A01 = (C15570nT) r2.AAr.get();
                indiaUpiPaymentInviteFragment.A06 = C12990iw.A0c(r2);
                indiaUpiPaymentInviteFragment.A0H = C117305Zk.A0P(r2);
                indiaUpiPaymentInviteFragment.A0C = C117315Zl.A0A(r2);
                indiaUpiPaymentInviteFragment.A09 = (C21860y6) r2.AE6.get();
                indiaUpiPaymentInviteFragment.A0D = (AnonymousClass18S) r2.AEw.get();
                indiaUpiPaymentInviteFragment.A0E = C117305Zk.A0M(r2);
                indiaUpiPaymentInviteFragment.A0G = C117305Zk.A0O(r2);
                indiaUpiPaymentInviteFragment.A04 = C117305Zk.A0G(r2);
                indiaUpiPaymentInviteFragment.A0A = (AnonymousClass18T) r2.AE9.get();
                indiaUpiPaymentInviteFragment.A07 = (C129925yW) r2.AEW.get();
                indiaUpiPaymentInviteFragment.A0J = (AnonymousClass17Z) r2.AEZ.get();
                indiaUpiPaymentInviteFragment.A0I = C117305Zk.A0T(r2);
                indiaUpiPaymentInviteFragment.A0F = C117305Zk.A0N(r2);
                indiaUpiPaymentInviteFragment.A02 = (C18640sm) r2.A3u.get();
                indiaUpiPaymentInviteFragment.A0B = (C18650sn) r2.AEe.get();
                indiaUpiPaymentInviteFragment.A08 = (C1329668y) r2.A9d.get();
                indiaUpiPaymentInviteFragment.A0L = (C121265hX) r2.A9a.get();
            }
        } else if (this instanceof Hilt_NoviPaymentInviteFragment) {
            Hilt_NoviPaymentInviteFragment hilt_NoviPaymentInviteFragment = (Hilt_NoviPaymentInviteFragment) this;
            if (!hilt_NoviPaymentInviteFragment.A02) {
                hilt_NoviPaymentInviteFragment.A02 = true;
                NoviPaymentInviteFragment noviPaymentInviteFragment = (NoviPaymentInviteFragment) hilt_NoviPaymentInviteFragment;
                AnonymousClass01J r22 = ((C51112Sw) ((AbstractC51092Su) hilt_NoviPaymentInviteFragment.generatedComponent())).A0Y;
                C117295Zj.A19(r22, noviPaymentInviteFragment);
                noviPaymentInviteFragment.A03 = C12960it.A0S(r22);
                noviPaymentInviteFragment.A00 = C12970iu.A0R(r22);
                noviPaymentInviteFragment.A0A = (AbstractC14440lR) r22.ANe.get();
                noviPaymentInviteFragment.A02 = (C16590pI) r22.AMg.get();
                noviPaymentInviteFragment.A04 = C117305Zk.A0P(r22);
                noviPaymentInviteFragment.A09 = (C22590zK) r22.ANP.get();
                noviPaymentInviteFragment.A05 = C117305Zk.A0X(r22);
                noviPaymentInviteFragment.A01 = (C20730wE) r22.A4J.get();
                noviPaymentInviteFragment.A07 = (C129685y8) r22.ADa.get();
                noviPaymentInviteFragment.A08 = (C22120yY) r22.ANn.get();
                noviPaymentInviteFragment.A06 = (C130105yo) r22.ADZ.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            C117295Zj.A19(((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y, (PaymentInviteFragment) this);
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
