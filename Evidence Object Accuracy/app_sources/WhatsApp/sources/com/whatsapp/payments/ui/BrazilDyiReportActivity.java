package com.whatsapp.payments.ui;

import X.AbstractActivityC121475iK;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass18U;
import X.AnonymousClass2FL;
import X.AnonymousClass605;
import X.AnonymousClass60T;
import X.AnonymousClass60U;
import X.AnonymousClass61E;
import X.AnonymousClass61M;
import X.AnonymousClass69D;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C129735yD;
import X.C129925yW;
import X.C130015yf;
import X.C1329568x;
import X.C15650ng;
import X.C16120oU;
import X.C16590pI;
import X.C17050qB;
import X.C18620sk;
import X.C18650sn;
import X.C18660so;
import X.C21860y6;
import X.C252018m;
import X.C25871Bd;
import android.app.Dialog;

/* loaded from: classes4.dex */
public class BrazilDyiReportActivity extends AbstractActivityC121475iK {
    public C1329568x A00;
    public AnonymousClass69D A01;
    public C129925yW A02;
    public C21860y6 A03;
    public C18660so A04;
    public AnonymousClass61M A05;
    public C129735yD A06;
    public boolean A07;

    public BrazilDyiReportActivity() {
        this(0);
    }

    public BrazilDyiReportActivity(int i) {
        this.A07 = false;
        C117295Zj.A0p(this, 8);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC121475iK) this).A0B = (C16590pI) A1M.AMg.get();
            ((AbstractActivityC121475iK) this).A0E = (C16120oU) A1M.ANE.get();
            ((AbstractActivityC121475iK) this).A03 = (AnonymousClass18U) A1M.AAU.get();
            ((AbstractActivityC121475iK) this).A0M = (AnonymousClass60U) A1M.A6I.get();
            this.A0U = (C252018m) A1M.A7g.get();
            ((AbstractActivityC121475iK) this).A09 = C12960it.A0P(A1M);
            ((AbstractActivityC121475iK) this).A0C = C12960it.A0R(A1M);
            this.A0T = C117315Zl.A0F(A1M);
            ((AbstractActivityC121475iK) this).A0K = C117305Zk.A0P(A1M);
            ((AbstractActivityC121475iK) this).A0D = (C15650ng) A1M.A4m.get();
            ((AbstractActivityC121475iK) this).A0O = (AnonymousClass605) A1M.AEj.get();
            ((AbstractActivityC121475iK) this).A0A = (C17050qB) A1M.ABL.get();
            ((AbstractActivityC121475iK) this).A0G = C117315Zl.A0A(A1M);
            ((AbstractActivityC121475iK) this).A0H = C117305Zk.A0M(A1M);
            ((AbstractActivityC121475iK) this).A0I = C117305Zk.A0O(A1M);
            this.A0P = (C130015yf) A1M.AEk.get();
            ((AbstractActivityC121475iK) this).A0J = (C18620sk) A1M.AFB.get();
            this.A0Q = (C25871Bd) A1M.ABZ.get();
            ((AbstractActivityC121475iK) this).A0F = (C18650sn) A1M.AEe.get();
            ((AbstractActivityC121475iK) this).A0L = (AnonymousClass60T) A1M.AFI.get();
            ((AbstractActivityC121475iK) this).A0N = (AnonymousClass61E) A1M.AEY.get();
            this.A00 = (C1329568x) A1M.A1o.get();
            this.A02 = (C129925yW) A1M.AEW.get();
            this.A01 = A09.A08();
            this.A05 = C117305Zk.A0N(A1M);
            this.A04 = (C18660so) A1M.AEf.get();
            this.A06 = A09.A0C();
            this.A03 = (C21860y6) A1M.AE6.get();
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A01 = this.A06.A01(null, this, i);
        if (A01 == null) {
            return super.onCreateDialog(i);
        }
        return A01;
    }
}
