package com.whatsapp.payments.ui;

import X.AbstractActivityC119225dN;
import X.AbstractActivityC119645em;
import X.AbstractActivityC121705jc;
import X.AbstractActivityC123635nW;
import X.AbstractC136196Lo;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass02B;
import X.AnonymousClass0U4;
import X.AnonymousClass102;
import X.AnonymousClass1US;
import X.AnonymousClass1V8;
import X.AnonymousClass2FL;
import X.AnonymousClass3FE;
import X.AnonymousClass5y1;
import X.AnonymousClass600;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass619;
import X.AnonymousClass61C;
import X.AnonymousClass61D;
import X.AnonymousClass61E;
import X.AnonymousClass61H;
import X.AnonymousClass61L;
import X.AnonymousClass61O;
import X.AnonymousClass61S;
import X.AnonymousClass68G;
import X.AnonymousClass6GY;
import X.C05000Nw;
import X.C05130Oj;
import X.C05500Pu;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117835an;
import X.C117845ao;
import X.C119445dz;
import X.C119465e1;
import X.C119845fC;
import X.C120965h2;
import X.C120975h3;
import X.C125065qd;
import X.C125695rf;
import X.C125705rg;
import X.C126795tS;
import X.C126825tV;
import X.C127375uO;
import X.C128085vX;
import X.C128365vz;
import X.C128485wB;
import X.C129005x1;
import X.C129105xB;
import X.C129585xx;
import X.C12960it;
import X.C129615y0;
import X.C129675y7;
import X.C129695y9;
import X.C12970iu;
import X.C12980iv;
import X.C129905yU;
import X.C12990iw;
import X.C130025yg;
import X.C130105yo;
import X.C130125yq;
import X.C130155yt;
import X.C130665zm;
import X.C130715zr;
import X.C130785zy;
import X.C1309860t;
import X.C1310460z;
import X.C1331369p;
import X.C22650zQ;
import X.C22980zx;
import X.C38211ni;
import X.C452120p;
import X.C87934Dp;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Pair;
import android.view.KeyEvent;
import com.facebook.redex.IDxAListenerShape0S0300000_3_I1;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class NoviPayBloksActivity extends AbstractActivityC123635nW {
    public int A00;
    public C128485wB A01;
    public AnonymousClass102 A02;
    public C119845fC A03;
    public AnonymousClass600 A04;
    public C128085vX A05;
    public C127375uO A06;
    public C130715zr A07;
    public C129695y9 A08;
    public C129005x1 A09;
    public C129905yU A0A;
    public C130025yg A0B;
    public C130785zy A0C;
    public C129105xB A0D;
    public C129675y7 A0E;
    public C129615y0 A0F;
    public AnonymousClass5y1 A0G;
    public C22980zx A0H;
    public C22650zQ A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public boolean A0O;

    public NoviPayBloksActivity() {
        this(0);
        this.A0J = "";
        this.A06 = null;
        this.A0M = null;
        this.A0N = null;
        this.A0K = null;
        this.A0L = null;
        this.A00 = 1;
    }

    public NoviPayBloksActivity(int i) {
        this.A0O = false;
        C117295Zj.A0p(this, 82);
    }

    public static String A0m(String str) {
        switch (str.hashCode()) {
            case 2571565:
                if (str.equals("TEXT")) {
                    return "text";
                }
                break;
            case 350565393:
                if (str.equals("DROPDOWN")) {
                    return "dropdown";
                }
                break;
            case 2016387302:
                if (str.equals("DIGITS")) {
                    return "digits";
                }
                break;
        }
        throw new AssertionError(C12960it.A0d(str, C12960it.A0k("[PAY] NoviBloksActivity/fieldEntryTypeToNoviKey/unknown address type: ")));
    }

    public static final String A0n(String str) {
        switch (str.hashCode()) {
            case 88833:
                if (str.equals("ZIP")) {
                    return "zip";
                }
                break;
            case 2068843:
                if (str.equals("CITY")) {
                    return "city";
                }
                break;
            case 72439485:
                if (str.equals("LINE1")) {
                    return "line1";
                }
                break;
            case 72439486:
                if (str.equals("LINE2")) {
                    return "line2";
                }
                break;
            case 72439487:
                if (str.equals("LINE3")) {
                    return "line3";
                }
                break;
            case 72439488:
                if (str.equals("LINE4")) {
                    return "line4";
                }
                break;
            case 79219825:
                if (str.equals("STATE")) {
                    return "state";
                }
                break;
            case 287066171:
                if (str.equals("STATE_TEXT")) {
                    return "state_text";
                }
                break;
            case 1675813750:
                if (str.equals("COUNTRY")) {
                    return "country";
                }
                break;
        }
        throw new AssertionError(C12960it.A0d(str, C12960it.A0k("[PAY] NoviBloksActivity/addressTypeToNoviKey/unknown address type: ")));
    }

    public static Map A0p(AnonymousClass1V8 r3) {
        String A0W = C117295Zj.A0W(r3, "next-step");
        HashMap A11 = C12970iu.A11();
        A11.put("next-step", A0W);
        return A11;
    }

    public static /* synthetic */ void A1J(FingerprintBottomSheet fingerprintBottomSheet, AnonymousClass3FE r19, NoviPayBloksActivity noviPayBloksActivity, Signature signature, Map map) {
        String str;
        String str2;
        byte[] A09 = ((AbstractActivityC121705jc) noviPayBloksActivity).A0N.A09();
        String encodeToString = Base64.encodeToString(AnonymousClass61L.A03(A09), 2);
        long A02 = C117315Zl.A02(noviPayBloksActivity);
        String A0n = C12990iw.A0n();
        AnonymousClass61C r10 = ((AbstractActivityC123635nW) noviPayBloksActivity).A0C;
        JSONObject A04 = r10.A04(A02);
        AnonymousClass61C.A01(A0n, A04);
        r10.A05(A04);
        String encodeToString2 = Base64.encodeToString(AnonymousClass61L.A03(r10.A05.A09()), 2);
        C130125yq r4 = r10.A04;
        KeyPair A022 = r4.A02();
        AnonymousClass009.A05(A022);
        try {
            A04.put("biometric_key_id", encodeToString2);
            A04.put("signing_key_registration", C117295Zj.A0a().put("key_type", "ECDSA_SECP256R1").put("pub_key_b64", Base64.encodeToString(A022.getPublic().getEncoded(), 2)));
        } catch (JSONException unused) {
            Log.e("PAY: IntentPayloadHelper/getBiometricLoginSignedIntent/toJson can't construct json");
        }
        C129585xx r6 = new C129585xx(r4, "BIOMETRIC_LOGIN", A04);
        C1310460z A01 = AnonymousClass61L.A01(((AbstractActivityC123635nW) noviPayBloksActivity).A06);
        C1310460z A00 = AnonymousClass61L.A00(((AbstractActivityC123635nW) noviPayBloksActivity).A06);
        if (A01 == null || A00 == null) {
            Log.e("PAY: NoviSharedPaymentSettingsActivity/performBiometricLogin signingKeyNode or encryptionKeyNode is null");
            return;
        }
        if (map != null) {
            str = C12970iu.A0t("login_attempt_id", map);
        } else {
            str = null;
        }
        AnonymousClass61S[] r12 = new AnonymousClass61S[4];
        AnonymousClass61S.A05("action", "novi-biometric-login", r12, 0);
        AnonymousClass61S.A05("login-request-id", A0n, r12, 1);
        AnonymousClass61S.A05("biometric_key_id", encodeToString, r12, 2);
        ArrayList A0m = C12960it.A0m(AnonymousClass61S.A00("biometric_type", "FINGER_PRINT"), r12, 3);
        if (!AnonymousClass1US.A0C(str)) {
            AnonymousClass61S.A03("login-attempt-id", str, A0m);
        }
        if (!(signature == null || A09 == null)) {
            try {
                JSONObject A023 = r6.A02();
                if (A023 != null) {
                    AnonymousClass61O r11 = new AnonymousClass61O(Base64.encodeToString(AnonymousClass61L.A03(A09), 10), A023);
                    signature.update(r11.A04());
                    byte[] sign = signature.sign();
                    AnonymousClass009.A06(sign, "PAY: NoviEncryptionManager/buildJwtPayload null signature");
                    r11.A01 = Base64.encodeToString(AnonymousClass61O.A01(sign), 11);
                    str2 = r11.A02();
                } else {
                    str2 = null;
                }
                C129585xx A024 = ((AbstractActivityC123635nW) noviPayBloksActivity).A0C.A02(A02);
                KeyPair A025 = ((AbstractActivityC123635nW) noviPayBloksActivity).A06.A02();
                if (str2 != null) {
                    KeyPair A03 = ((AbstractActivityC123635nW) noviPayBloksActivity).A06.A03();
                    C130155yt r122 = ((AbstractActivityC123635nW) noviPayBloksActivity).A04;
                    C1310460z A0B = C117315Zl.A0B("account", A0m);
                    ArrayList arrayList = A0B.A02;
                    arrayList.add(A01);
                    arrayList.add(A00);
                    C117305Zk.A1K(A0B, "biometric_login_signed_intent", AnonymousClass61S.A02("biometric_login_signed_intent", str2));
                    C117305Zk.A1K(A0B, "encryption_key_registration_signed_intent", AnonymousClass61S.A02("encryption_key_registration_signed_intent", A024.A01(A025)));
                    if (A03 != null) {
                        C117305Zk.A1R("trusted_app_login_signed_intent", arrayList, AnonymousClass61S.A02("trusted_app_login_signed_intent", r6.A01(A03)));
                    }
                    AnonymousClass016 A0T = C12980iv.A0T();
                    r122.A09(new C1331369p(A0T), A0B, 20, "set", 2);
                    A0T.A05(noviPayBloksActivity, new AnonymousClass02B(fingerprintBottomSheet, r19, noviPayBloksActivity) { // from class: X.65d
                        public final /* synthetic */ FingerprintBottomSheet A00;
                        public final /* synthetic */ AnonymousClass3FE A01;
                        public final /* synthetic */ NoviPayBloksActivity A02;

                        {
                            this.A02 = r3;
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AnonymousClass02B
                        public final void ANq(Object obj) {
                            this.A02.A2x(this.A00, this.A01, (C130785zy) obj);
                        }
                    });
                    return;
                }
            } catch (SignatureException unused2) {
                Log.e("PAY: NoviPayBloksActivity: performBiometricLogin SignatureException");
            }
        }
        noviPayBloksActivity.finish();
    }

    public static void A1R(AnonymousClass619 r2, String str, Map map) {
        if (r2 != null) {
            map.put(C12960it.A0d("_text", C12960it.A0j(str)), r2.A00);
            map.put(C12960it.A0d("_colors", C12960it.A0j(str)), AnonymousClass619.A03(r2.A01));
            map.put(C12960it.A0d("_links", C12960it.A0j(str)), AnonymousClass619.A03(r2.A02));
            map.put(C12960it.A0d("_styles", C12960it.A0j(str)), AnonymousClass619.A03(r2.A04));
            map.put(C12960it.A0d("_scales", C12960it.A0j(str)), AnonymousClass619.A03(r2.A03));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003c, code lost:
        if (r1.contains(r2) != false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0030, code lost:
        if (r1.contains(r2) == false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A1S(com.whatsapp.payments.ui.NoviPayBloksActivity r6) {
        /*
            X.5yo r0 = r6.A0A
            android.content.SharedPreferences r0 = r0.A02()
            java.lang.String r4 = "env_tier"
            java.lang.String r2 = X.C12980iv.A0p(r0, r4)
            X.5yt r0 = r6.A04
            java.lang.String r3 = r0.A00
            if (r3 == 0) goto L_0x0086
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x003e
            java.util.HashSet r1 = X.C12970iu.A12()
            java.lang.String r0 = "novi.wallet_core.rc"
            r1.add(r0)
            java.lang.String r0 = "novi.wallet_core.rc_stable"
            r1.add(r0)
            boolean r0 = r1.contains(r3)
            if (r0 == 0) goto L_0x0032
            boolean r0 = r1.contains(r2)
            if (r0 == 0) goto L_0x003e
        L_0x0032:
            boolean r0 = r1.contains(r3)
            if (r0 != 0) goto L_0x007d
            boolean r0 = r1.contains(r2)
            if (r0 == 0) goto L_0x007d
        L_0x003e:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x004e
            X.0lR r1 = r6.A05
            X.6GS r0 = new X.6GS
            r0.<init>()
            r1.Ab2(r0)
        L_0x004e:
            java.lang.String r0 = "novi.wallet_core.rc"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = "novi.wallet_core.rc_stable"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x007d
        L_0x005e:
            X.02e r5 = X.C12980iv.A0S(r6)
            r0 = 2131892220(0x7f1217fc, float:1.9419182E38)
            r5.A07(r0)
            r0 = 2131892219(0x7f1217fb, float:1.941918E38)
            r5.A06(r0)
            r2 = 2131890038(0x7f120f76, float:1.9414757E38)
            r1 = 4
            com.facebook.redex.IDxCListenerShape5S0000000_3_I1 r0 = new com.facebook.redex.IDxCListenerShape5S0000000_3_I1
            r0.<init>(r1)
            r5.setPositiveButton(r2, r0)
            X.C12970iu.A1J(r5)
        L_0x007d:
            X.5yo r0 = r6.A0A
            android.content.SharedPreferences$Editor r0 = X.C130105yo.A01(r0)
            X.C12970iu.A1D(r0, r4, r3)
        L_0x0086:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayBloksActivity.A1S(com.whatsapp.payments.ui.NoviPayBloksActivity):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k
    public void A1T(AnonymousClass01E r2) {
        super.A1T(r2);
        if (r2 instanceof ProgressDialogFragment) {
            ((ProgressDialogFragment) r2).A00 = new DialogInterface.OnKeyListener() { // from class: X.631
                @Override // android.content.DialogInterface.OnKeyListener
                public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    NoviPayBloksActivity noviPayBloksActivity = NoviPayBloksActivity.this;
                    if (i != 4) {
                        return false;
                    }
                    dialogInterface.dismiss();
                    noviPayBloksActivity.finish();
                    return true;
                }
            };
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0O) {
            this.A0O = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119225dN.A0B(A09, A1M, this, AbstractActivityC119225dN.A0A(A1M, this, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this))));
            AbstractActivityC119225dN.A0K(A1M, this, AbstractActivityC119225dN.A09(A1M, this));
            AbstractActivityC119225dN.A0D(A1M, AbstractActivityC119225dN.A02(A1M, this), this);
            this.A0E = (C129675y7) A1M.AKB.get();
            this.A0I = (C22650zQ) A1M.A4n.get();
            this.A04 = (AnonymousClass600) A1M.ADB.get();
            this.A01 = (C128485wB) A1M.A1h.get();
            this.A0H = (C22980zx) A1M.AFH.get();
            this.A07 = (C130715zr) A1M.AAE.get();
            this.A0D = (C129105xB) A1M.ADb.get();
            this.A02 = C117305Zk.A0G(A1M);
            this.A0A = (C129905yU) A1M.ADH.get();
            this.A08 = (C129695y9) A1M.ADQ.get();
            this.A09 = (C129005x1) A1M.ADC.get();
            this.A0B = (C130025yg) A1M.ADX.get();
            this.A03 = (C119845fC) A1M.AEM.get();
        }
    }

    public final String A2t(C1309860t r3) {
        C125695rf r0;
        Map map;
        String str;
        C125705rg r02;
        String str2 = r3.A00;
        if (str2 != null) {
            return str2;
        }
        C130665zm A00 = this.A0B.A00();
        if (A00 == null) {
            return null;
        }
        if ((r3 instanceof C120965h2) && (r02 = A00.A03) != null) {
            map = r02.A00;
            str = ((C120965h2) r3).A00;
        } else if (!(r3 instanceof C120975h3) || (r0 = A00.A00) == null) {
            return null;
        } else {
            map = r0.A00;
            str = ((C120975h3) r3).A00;
        }
        return C12970iu.A0t(str, map);
    }

    public final Map A2u(C1309860t r8) {
        HashMap A11 = C12970iu.A11();
        String str = r8.A01;
        A11.put("format", A0m(str));
        A11.put("title", r8.A02);
        String str2 = "1";
        String str3 = "0";
        if (r8.A04) {
            str3 = str2;
        }
        A11.put("editable", str3);
        if (!r8.A05) {
            str2 = "0";
        }
        A11.put("optional", str2);
        String A2t = A2t(r8);
        if (A2t == null) {
            A2t = "";
        }
        A11.put("value", A2t);
        if ("DROPDOWN".equals(str)) {
            JSONArray A0L = C117315Zl.A0L();
            List<C126825tV> list = r8.A03;
            AnonymousClass009.A05(list);
            for (C126825tV r3 : list) {
                C117315Zl.A0Y(r3.A01, "code", A0L, C117295Zj.A0a().put("name", r3.A00));
            }
            A11.put("values-json", A0L.toString());
        }
        return A11;
    }

    public final void A2v(int i) {
        if (i == 7) {
            C128365vz r1 = new AnonymousClass610("BIOMETRICS_TOO_MANY_ATTEMPTS_MODAL_CLICK", "LOGIN").A00;
            r1.A0j = "LOGIN_SCREEN";
            r1.A0J = "TOUCH_ID";
            r1.A0i = "BIOMETRICS";
            ((AbstractActivityC123635nW) this).A07.A05(r1);
        }
    }

    public final void A2w(Pair pair, AnonymousClass3FE r8, AnonymousClass61D r9, String str) {
        C128365vz r3 = new AnonymousClass610("BIOMETRICS_ENABLED_INITIATED", "LOGIN").A00;
        r3.A0j = "LOGIN_SCREEN";
        r3.A0J = "TOUCH_ID";
        r3.A0i = "BIOMETRICS";
        ((AbstractActivityC123635nW) this).A07.A05(r3);
        AnonymousClass610 r32 = new AnonymousClass610("BIOMETRICS_ENABLED_STATUS", "LOGIN");
        C128365vz r0 = r32.A00;
        r0.A0j = "LOGIN_SCREEN";
        r0.A0J = "TOUCH_ID";
        r0.A0i = "BIOMETRICS";
        r9.A02(pair, new IDxAListenerShape0S0300000_3_I1(r32, r8, this, 7), ((AbstractActivityC121705jc) this).A0N, str);
    }

    public final void A2x(BottomSheetDialogFragment bottomSheetDialogFragment, AnonymousClass3FE r5, C130785zy r6) {
        Object obj = r6.A02;
        if (obj == null || !"true".equals(((AnonymousClass1V8) obj).A0I("retos_required", null))) {
            A2y(bottomSheetDialogFragment, r5, r6);
            return;
        }
        this.A0C = r6;
        r5.A02("on_retos_required", null);
        AbstractActivityC123635nW.A1U(bottomSheetDialogFragment);
        AaN();
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2y(com.google.android.material.bottomsheet.BottomSheetDialogFragment r17, X.AnonymousClass3FE r18, X.C130785zy r19) {
        /*
        // Method dump skipped, instructions count: 394
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayBloksActivity.A2y(com.google.android.material.bottomsheet.BottomSheetDialogFragment, X.3FE, X.5zy):void");
    }

    public final void A2z(BottomSheetDialogFragment bottomSheetDialogFragment, AnonymousClass3FE r6, Map map) {
        this.A00 = 6;
        ((AbstractActivityC123635nW) this).A0H.put("submit_onboarding_details_key_callback", r6);
        C130155yt.A03(new AbstractC136196Lo(bottomSheetDialogFragment, r6, this, map) { // from class: X.6A3
            public final /* synthetic */ BottomSheetDialogFragment A00;
            public final /* synthetic */ AnonymousClass3FE A01;
            public final /* synthetic */ NoviPayBloksActivity A02;
            public final /* synthetic */ Map A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A03 = r4;
                this.A01 = r2;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r19) {
                C129895yT r10;
                C1315463e r8;
                AnonymousClass619 r7;
                AnonymousClass1V8 A0E;
                NoviPayBloksActivity noviPayBloksActivity = this.A02;
                BottomSheetDialogFragment bottomSheetDialogFragment2 = this.A00;
                Map map2 = this.A03;
                AnonymousClass3FE r3 = this.A01;
                AbstractActivityC123635nW.A1U(bottomSheetDialogFragment2);
                noviPayBloksActivity.AaN();
                if (r19.A06()) {
                    AnonymousClass1V8 r1 = (AnonymousClass1V8) r19.A02;
                    AnonymousClass102 r72 = noviPayBloksActivity.A02;
                    C127845v9 r4 = null;
                    try {
                        String A0H = r1.A0H("decision");
                        AnonymousClass1V8 A0E2 = r1.A0E("novi-account");
                        AnonymousClass1V8 A0E3 = r1.A0E("home_country");
                        AnonymousClass1V8 A0E4 = r1.A0E("welcome_info");
                        if (A0E2 != null) {
                            r8 = C1315463e.A00(r72, A0E2);
                            AnonymousClass1V8 A0E5 = A0E2.A0E("limitation");
                            r10 = A0E5 != null ? C124995qV.A00(r72, A0E5) : null;
                        } else {
                            r8 = null;
                            r10 = null;
                        }
                        C130645zk A00 = A0E3 != null ? C130645zk.A00(r72, A0E3) : null;
                        if (A0E4 == null || (A0E = A0E4.A0E("description")) == null) {
                            r7 = null;
                        } else {
                            r7 = AnonymousClass619.A00(A0E);
                        }
                        r4 = new C127845v9(r7, r8, A00, r10, A0H);
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: NoviOnboardingDetailsResponseData/createFromProtocolTreeNode failed.");
                    }
                    String str = r4.A04;
                    if ("APPROVE".equals(str)) {
                        C130105yo r0 = ((AbstractActivityC123635nW) noviPayBloksActivity).A0A;
                        C129895yT r62 = r4.A03;
                        r0.A04(r62);
                        ((AbstractActivityC123635nW) noviPayBloksActivity).A08.A0I.A0B(r62);
                        C1315463e r5 = r4.A01;
                        if (r5 != null) {
                            ((AbstractActivityC123635nW) noviPayBloksActivity).A08.A0A(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0096: INVOKE  
                                  (wrap: X.61F : 0x008f: IGET  (r1v6 X.61F A[REMOVE]) = (wrap: ?? : ?: CAST (X.5nW) (r12v0 'noviPayBloksActivity' com.whatsapp.payments.ui.NoviPayBloksActivity)) X.5nW.A08 X.61F)
                                  (wrap: X.67e : 0x0093: CONSTRUCTOR  (r0v14 X.67e A[REMOVE]) = (r12v0 'noviPayBloksActivity' com.whatsapp.payments.ui.NoviPayBloksActivity) call: X.67e.<init>(com.whatsapp.payments.ui.NoviPayBloksActivity):void type: CONSTRUCTOR)
                                  (r5v0 'r5' X.63e)
                                 type: VIRTUAL call: X.61F.A0A(X.20l, X.63e):void in method: X.6A3.AV8(X.5zy):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0093: CONSTRUCTOR  (r0v14 X.67e A[REMOVE]) = (r12v0 'noviPayBloksActivity' com.whatsapp.payments.ui.NoviPayBloksActivity) call: X.67e.<init>(com.whatsapp.payments.ui.NoviPayBloksActivity):void type: CONSTRUCTOR in method: X.6A3.AV8(X.5zy):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 37 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.67e, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 43 more
                                */
                            /*
                                this = this;
                                r1 = r18
                                com.whatsapp.payments.ui.NoviPayBloksActivity r12 = r1.A02
                                com.google.android.material.bottomsheet.BottomSheetDialogFragment r0 = r1.A00
                                java.util.Map r2 = r1.A03
                                X.3FE r3 = r1.A01
                                X.AbstractActivityC123635nW.A1U(r0)
                                r12.AaN()
                                r1 = r19
                                boolean r0 = r1.A06()
                                if (r0 == 0) goto L_0x00d6
                                java.lang.Object r1 = r1.A02
                                X.1V8 r1 = (X.AnonymousClass1V8) r1
                                X.102 r7 = r12.A02
                                r4 = 0
                                java.lang.String r0 = "decision"
                                java.lang.String r11 = r1.A0H(r0)     // Catch: 1V9 -> 0x006e
                                java.lang.String r0 = "novi-account"
                                X.1V8 r6 = r1.A0E(r0)     // Catch: 1V9 -> 0x006e
                                java.lang.String r0 = "home_country"
                                X.1V8 r5 = r1.A0E(r0)     // Catch: 1V9 -> 0x006e
                                java.lang.String r0 = "welcome_info"
                                X.1V8 r1 = r1.A0E(r0)     // Catch: 1V9 -> 0x006e
                                if (r6 == 0) goto L_0x004a
                                X.63e r8 = X.C1315463e.A00(r7, r6)     // Catch: 1V9 -> 0x006e
                                java.lang.String r0 = "limitation"
                                X.1V8 r0 = r6.A0E(r0)     // Catch: 1V9 -> 0x006e
                                if (r0 == 0) goto L_0x004d
                                X.5yT r10 = X.C124995qV.A00(r7, r0)     // Catch: 1V9 -> 0x006e
                                goto L_0x004e
                            L_0x004a:
                                r8 = r4
                                r10 = r4
                                goto L_0x004e
                            L_0x004d:
                                r10 = r4
                            L_0x004e:
                                if (r5 == 0) goto L_0x0055
                                X.5zk r9 = X.C130645zk.A00(r7, r5)     // Catch: 1V9 -> 0x006e
                                goto L_0x0056
                            L_0x0055:
                                r9 = r4
                            L_0x0056:
                                if (r1 == 0) goto L_0x0059
                                goto L_0x005b
                            L_0x0059:
                                r7 = r4
                                goto L_0x0067
                            L_0x005b:
                                java.lang.String r0 = "description"
                                X.1V8 r0 = r1.A0E(r0)     // Catch: 1V9 -> 0x006e
                                if (r0 == 0) goto L_0x0059
                                X.619 r7 = X.AnonymousClass619.A00(r0)     // Catch: 1V9 -> 0x006e
                            L_0x0067:
                                X.5v9 r6 = new X.5v9     // Catch: 1V9 -> 0x006e
                                r6.<init>(r7, r8, r9, r10, r11)     // Catch: 1V9 -> 0x006e
                                r4 = r6
                                goto L_0x0073
                            L_0x006e:
                                java.lang.String r0 = "PAY: NoviOnboardingDetailsResponseData/createFromProtocolTreeNode failed."
                                com.whatsapp.util.Log.e(r0)
                            L_0x0073:
                                java.lang.String r1 = r4.A04
                                java.lang.String r0 = "APPROVE"
                                boolean r0 = r0.equals(r1)
                                if (r0 == 0) goto L_0x00b8
                                X.5yo r0 = r12.A0A
                                X.5yT r6 = r4.A03
                                r0.A04(r6)
                                X.61F r0 = r12.A08
                                X.5au r0 = r0.A0I
                                r0.A0B(r6)
                                X.63e r5 = r4.A01
                                if (r5 == 0) goto L_0x00b0
                                X.61F r1 = r12.A08
                                X.67e r0 = new X.67e
                                r0.<init>(r12)
                                r1.A0A(r0, r5)
                                X.5yg r1 = r12.A0B
                                X.5zk r0 = r4.A02
                                r1.A02 = r0
                                X.63Y r15 = r5.A00
                                r1.A01 = r15
                                X.619 r13 = r4.A00
                                r1.A00 = r13
                                X.018 r14 = r12.A01
                                r17 = r2
                                r16 = r0
                                X.C125095qg.A00(r12, r13, r14, r15, r16, r17)
                            L_0x00b0:
                                if (r6 != 0) goto L_0x00b7
                                java.lang.String r0 = "on_success"
                                r3.A02(r0, r2)
                            L_0x00b7:
                                return
                            L_0x00b8:
                                java.lang.String r0 = "STEP_UP"
                                boolean r0 = r0.equals(r1)
                                java.lang.String r2 = "on_failure"
                                if (r0 == 0) goto L_0x00d2
                                X.5yo r0 = r12.A0A
                                X.5yT r1 = r4.A03
                                r0.A04(r1)
                                X.61F r0 = r12.A08
                                X.5au r0 = r0.A0I
                                r0.A0B(r1)
                                if (r1 != 0) goto L_0x00b7
                            L_0x00d2:
                                r3.A00(r2)
                                return
                            L_0x00d6:
                                java.lang.String r0 = X.C130785zy.A00(r1, r12)
                                r3.A00(r0)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A3.AV8(X.5zy):void");
                        }
                    }, ((AbstractActivityC123635nW) this).A04, AnonymousClass61S.A01("novi-submit-onboarding-details"), 9);
                }

                public final void A30(FingerprintBottomSheet fingerprintBottomSheet) {
                    C128365vz r1 = AnonymousClass610.A03("BIOMETRICS_CANCEL_LOGIN_CLICK", "LOGIN", "LOGIN_SCREEN").A00;
                    r1.A0i = "BIOMETRICS";
                    r1.A0J = "TOUCH_ID";
                    r1.A0L = getString(R.string.fingerprint_bottom_sheet_negative_button);
                    ((AbstractActivityC123635nW) this).A07.A05(r1);
                    if (fingerprintBottomSheet != null) {
                        fingerprintBottomSheet.A1B();
                    }
                }

                public final void A31(AnonymousClass3FE r7, C128085vX r8, Map map) {
                    HashMap A11 = C12970iu.A11();
                    A11.put("tos_version", Integer.valueOf(r8.A00));
                    A1R(r8.A03, "tos_header_title", A11);
                    A1R(r8.A02, "tos_header_description", A11);
                    A1R(r8.A01, "tos_footer", A11);
                    int i = 0;
                    while (true) {
                        C126795tS[] r1 = r8.A06;
                        if (i >= r1.length) {
                            break;
                        }
                        C126795tS r4 = r1[i];
                        StringBuilder A0k = C12960it.A0k("tos_sesction");
                        A0k.append(i);
                        A1R(r4.A01, C12960it.A0d("_title", A0k), A11);
                        StringBuilder A0j = C12960it.A0j("tos_sesction");
                        A0j.append(i);
                        A1R(r4.A00, C12960it.A0d("_description", A0j), A11);
                        i++;
                    }
                    A1R(r8.A05, "retos_title", A11);
                    A1R(r8.A04, "retos_description", A11);
                    if ("true".equals(map.get("clear_cache"))) {
                        this.A05 = null;
                    }
                    r7.A02("on_success", A11);
                }

                public final void A32(AnonymousClass3FE r14, String str) {
                    AnonymousClass61D A03 = AbstractActivityC119225dN.A03(this);
                    Pair A032 = ((AbstractActivityC121705jc) this).A0N.A03();
                    AnonymousClass009.A05(A032);
                    if (!AnonymousClass61H.A04(this, ((ActivityC13810kN) this).A0C)) {
                        FingerprintBottomSheet A02 = AnonymousClass61H.A02();
                        A02.A05 = new C119445dz(A032, A02, r14, A03, this, str);
                        Adm(A02);
                        return;
                    }
                    C05500Pu A01 = AnonymousClass61H.A01(this, new C117845ao(A032, r14, A03, this, str));
                    C05000Nw A00 = AnonymousClass61H.A00(getString(R.string.novi_biometrics_registration_start), getString(R.string.fingerprint_bottom_sheet_negative_button));
                    AnonymousClass0U4 A002 = AnonymousClass61E.A00();
                    if (A002 != null) {
                        A01.A01(A002, A00);
                    }
                }

                public final void A33(AnonymousClass3FE r7, Map map) {
                    if (!AnonymousClass61H.A04(this, ((ActivityC13810kN) this).A0C)) {
                        FingerprintBottomSheet A01 = FingerprintBottomSheet.A01(R.string.novi_fingerprint_title, R.string.fingerprint_bottom_sheet_negative_button, R.string.novi_fingerprint_password_bypass, R.layout.novi_header, R.layout.novi_finger_print, 2131951985);
                        A01.A05 = new C119465e1(A01, r7, this, map);
                        Adm(A01);
                        return;
                    }
                    C05500Pu A012 = AnonymousClass61H.A01(this, new C117835an(r7, this, map));
                    String string = getString(R.string.novi_fingerprint_title);
                    String string2 = getString(R.string.novi_fingerprint_password_bypass);
                    C05130Oj r1 = new C05130Oj();
                    r1.A03 = string;
                    r1.A00 = 15;
                    r1.A01 = string2;
                    r1.A04 = false;
                    C05000Nw A00 = r1.A00();
                    AnonymousClass0U4 A002 = AnonymousClass61E.A00();
                    if (A002 != null) {
                        A012.A01(A002, A00);
                    }
                }

                public final void A34(C452120p r4) {
                    ((AbstractActivityC123635nW) this).A05.A02(r4, new Runnable() { // from class: X.6GX
                        @Override // java.lang.Runnable
                        public final void run() {
                            NoviPayBloksActivity noviPayBloksActivity = NoviPayBloksActivity.this;
                            noviPayBloksActivity.setResult(0);
                            noviPayBloksActivity.finish();
                        }
                    }, null);
                }

                /* JADX WARNING: Code restructure failed: missing block: B:10:0x0031, code lost:
                    if (r34.equals("get_country_subdivisions") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
                    if (r34.equals("add_bank") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:14:0x0043, code lost:
                    if (r34.equals("add_card") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:16:0x004c, code lost:
                    if (r34.equals("accept_tos") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
                    if (r34.equals("complete_phone_verification") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x005e, code lost:
                    if (r34.equals("change_password") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:215:0x03b0, code lost:
                    if (r9 == false) goto L_0x03b2;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
                    if (r34.equals("verify_current_password") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x0070, code lost:
                    if (r34.equals("get_app_country_list") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x0079, code lost:
                    if (r34.equals("get_password_recovery_token") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x0082, code lost:
                    if (r34.equals("notify_welcome_screen_shown") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x008b, code lost:
                    if (r34.equals("save_date_of_birth") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:32:0x0094, code lost:
                    if (r34.equals("fetch_form_based_onboarding") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:34:0x009e, code lost:
                    if (r34.equals("reset_password") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a8, code lost:
                    if (r34.equals("biometric_login") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b2, code lost:
                    if (r34.equals("enter_password") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:40:0x00bc, code lost:
                    if (r34.equals("save_citizenship") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c6, code lost:
                    if (r34.equals("start_fingerprint_registration") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d0, code lost:
                    if (r34.equals("get_tos_content") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e5, code lost:
                    if (r34.equals("get_supported_country_list") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ef, code lost:
                    if (r34.equals("upload_kyc_image") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f9, code lost:
                    if (r34.equals("save_name") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:55:0x0103, code lost:
                    if (r34.equals("tpp_perform_account_link") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:57:0x010d, code lost:
                    if (r34.equals("start_phone_verification") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:59:0x0117, code lost:
                    if (r34.equals("pass_step_up_callback") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:61:0x0121, code lost:
                    if (r34.equals("get_name_fields") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:63:0x012b, code lost:
                    if (r34.equals("save_address") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:65:0x0135, code lost:
                    if (r34.equals("tpp_account_details") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:67:0x013f, code lost:
                    if (r34.equals("save_document") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:74:0x0159, code lost:
                    if (r34.equals("resume_add_bank") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:76:0x0163, code lost:
                    if (r34.equals("resume_add_card") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:78:0x016d, code lost:
                    if (r34.equals("get_bank_schema") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:80:0x0177, code lost:
                    if (r34.equals("get_config_picture_id_text") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:82:0x0181, code lost:
                    if (r34.equals("get_onboarding_success_params") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:84:0x018b, code lost:
                    if (r34.equals("perform_remove_account") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:86:0x0195, code lost:
                    if (r34.equals("get_card_schema") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:88:0x019f, code lost:
                    if (r34.equals("get_request_uuid") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a9, code lost:
                    if (r34.equals("save_country_of_residence") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:92:0x01b3, code lost:
                    if (r34.equals("resend_phone_verification") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:94:0x01bd, code lost:
                    if (r34.equals("close_novi_account") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:96:0x01c7, code lost:
                    if (r34.equals("create_account") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:98:0x01d1, code lost:
                    if (r34.equals("save_place_of_birth") == false) goto L_0x0012;
                 */
                /* JADX WARNING: Removed duplicated region for block: B:110:0x022a  */
                /* JADX WARNING: Removed duplicated region for block: B:111:0x022e A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:114:0x023a A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:117:0x0246  */
                /* JADX WARNING: Removed duplicated region for block: B:118:0x0249 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:121:0x0255  */
                /* JADX WARNING: Removed duplicated region for block: B:122:0x0258 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:125:0x0264 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:128:0x026f A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:131:0x027a A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:134:0x0285 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:137:0x0290 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:140:0x029b A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:143:0x02a6 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:146:0x02b1  */
                /* JADX WARNING: Removed duplicated region for block: B:149:0x02b9  */
                /* JADX WARNING: Removed duplicated region for block: B:150:0x02bc  */
                /* JADX WARNING: Removed duplicated region for block: B:153:0x02c4  */
                /* JADX WARNING: Removed duplicated region for block: B:154:0x02c7 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:157:0x02d2  */
                /* JADX WARNING: Removed duplicated region for block: B:160:0x02da  */
                /* JADX WARNING: Removed duplicated region for block: B:161:0x02dd A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:166:0x02f4 A[Catch: 5q3 -> 0x03d9, TryCatch #2 {5q3 -> 0x03d9, blocks: (B:104:0x0208, B:105:0x020c, B:106:0x020f, B:107:0x0217, B:108:0x021a, B:109:0x0229, B:111:0x022e, B:114:0x023a, B:118:0x0249, B:122:0x0258, B:125:0x0264, B:128:0x026f, B:131:0x027a, B:134:0x0285, B:137:0x0290, B:140:0x029b, B:143:0x02a6, B:147:0x02b3, B:151:0x02be, B:154:0x02c7, B:158:0x02d4, B:161:0x02dd, B:164:0x02e7, B:166:0x02f4, B:178:0x0316, B:182:0x031e, B:183:0x033a, B:185:0x033f, B:188:0x034a, B:191:0x0355, B:194:0x0360, B:197:0x036b, B:200:0x0376, B:203:0x0381, B:206:0x038c, B:209:0x0397, B:212:0x03a1, B:222:0x03c9, B:223:0x03d8), top: B:506:0x0208 }] */
                /* JADX WARNING: Removed duplicated region for block: B:180:0x031c  */
                /* JADX WARNING: Removed duplicated region for block: B:231:0x03f4  */
                /* JADX WARNING: Removed duplicated region for block: B:237:0x0495  */
                /* JADX WARNING: Removed duplicated region for block: B:239:0x04d3  */
                /* JADX WARNING: Removed duplicated region for block: B:241:0x0513  */
                /* JADX WARNING: Removed duplicated region for block: B:243:0x0527  */
                /* JADX WARNING: Removed duplicated region for block: B:245:0x053b  */
                /* JADX WARNING: Removed duplicated region for block: B:255:0x05a4  */
                /* JADX WARNING: Removed duplicated region for block: B:270:0x063c  */
                /* JADX WARNING: Removed duplicated region for block: B:272:0x0640  */
                /* JADX WARNING: Removed duplicated region for block: B:275:0x067e  */
                /* JADX WARNING: Removed duplicated region for block: B:277:0x0690  */
                /* JADX WARNING: Removed duplicated region for block: B:279:0x06b8  */
                /* JADX WARNING: Removed duplicated region for block: B:281:0x06bc  */
                /* JADX WARNING: Removed duplicated region for block: B:294:0x07b3  */
                /* JADX WARNING: Removed duplicated region for block: B:296:0x07da  */
                /* JADX WARNING: Removed duplicated region for block: B:302:0x07eb  */
                /* JADX WARNING: Removed duplicated region for block: B:308:0x0835  */
                /* JADX WARNING: Removed duplicated region for block: B:318:0x08c6  */
                /* JADX WARNING: Removed duplicated region for block: B:322:0x08ce  */
                /* JADX WARNING: Removed duplicated region for block: B:324:0x08dd  */
                /* JADX WARNING: Removed duplicated region for block: B:326:0x0910  */
                /* JADX WARNING: Removed duplicated region for block: B:328:0x0918  */
                /* JADX WARNING: Removed duplicated region for block: B:330:0x0938  */
                /* JADX WARNING: Removed duplicated region for block: B:343:0x09a0  */
                /* JADX WARNING: Removed duplicated region for block: B:391:0x0a78  */
                /* JADX WARNING: Removed duplicated region for block: B:393:0x0a7e  */
                /* JADX WARNING: Removed duplicated region for block: B:400:0x0aa9  */
                /* JADX WARNING: Removed duplicated region for block: B:402:0x0ab7  */
                /* JADX WARNING: Removed duplicated region for block: B:419:0x0b3c  */
                /* JADX WARNING: Removed duplicated region for block: B:421:0x0b65  */
                /* JADX WARNING: Removed duplicated region for block: B:422:0x0b73  */
                /* JADX WARNING: Removed duplicated region for block: B:423:0x0b7f  */
                /* JADX WARNING: Removed duplicated region for block: B:424:0x0ba3  */
                /* JADX WARNING: Removed duplicated region for block: B:427:0x0bca  */
                /* JADX WARNING: Removed duplicated region for block: B:433:0x0bf2  */
                /* JADX WARNING: Removed duplicated region for block: B:436:0x0c24  */
                /* JADX WARNING: Removed duplicated region for block: B:446:0x0c50  */
                /* JADX WARNING: Removed duplicated region for block: B:448:0x0c70  */
                /* JADX WARNING: Removed duplicated region for block: B:450:0x0c8f  */
                /* JADX WARNING: Removed duplicated region for block: B:452:0x0c9d  */
                /* JADX WARNING: Removed duplicated region for block: B:483:0x0d48  */
                /* JADX WARNING: Removed duplicated region for block: B:485:0x0d56  */
                /* JADX WARNING: Removed duplicated region for block: B:487:0x0d61  */
                /* JADX WARNING: Removed duplicated region for block: B:500:0x0e5c  */
                /* JADX WARNING: Removed duplicated region for block: B:512:0x021a A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:534:0x021a A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:535:0x021a A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:537:0x021a A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:571:0x0a3e A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:578:0x0a29 A[SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:99:0x01d5  */
                @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractC136496Mt
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void AZC(X.AnonymousClass3FE r33, java.lang.String r34, java.util.Map r35) {
                    /*
                    // Method dump skipped, instructions count: 4208
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayBloksActivity.AZC(X.3FE, java.lang.String, java.util.Map):void");
                }

                /* JADX WARNING: Code restructure failed: missing block: B:140:0x025b, code lost:
                    if (r0 != false) goto L_0x025d;
                 */
                /* JADX WARNING: Removed duplicated region for block: B:119:0x01ed A[RETURN] */
                /* JADX WARNING: Removed duplicated region for block: B:133:0x0227 A[RETURN] */
                @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractC136496Mt
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public java.lang.String AZE(java.lang.String r13, java.util.Map r14) {
                    /*
                    // Method dump skipped, instructions count: 1024
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayBloksActivity.AZE(java.lang.String, java.util.Map):java.lang.String");
                }

                @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
                public void onActivityResult(int i, int i2, Intent intent) {
                    super.onActivityResult(i, i2, intent);
                    if (i == 1006) {
                        setResult(i2, intent);
                    } else if (i == 1001) {
                        if (i2 == -1) {
                            startActivityForResult(C12990iw.A0D(this, NoviWithdrawCashStoreLocatorActivity.class), 1005);
                            return;
                        }
                        return;
                    } else if (i != 1002) {
                        if (i == 1003) {
                            if (i2 == -1) {
                                A2C(R.string.loading_spinner);
                                A2z(null, (AnonymousClass3FE) ((AbstractActivityC123635nW) this).A0H.get("submit_onboarding_details_key_callback"), C12970iu.A11());
                                return;
                            }
                        } else if (i == 1004) {
                            if ("report_transaction_nfm".equals(getIntent().getStringExtra("report_transaction_origin"))) {
                                setResult(i2, C87934Dp.A00(getIntent()));
                            }
                        } else if (i == 1005) {
                            setResult(i2);
                        } else if (i != 7387) {
                            return;
                        } else {
                            if (i2 == -1) {
                                this.A0F.A00(this);
                                return;
                            }
                        }
                    }
                    finish();
                }

                @Override // X.AbstractActivityC123635nW, X.AbstractActivityC119645em, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
                public void onBackPressed() {
                    if ("novipay_p_card_added".equals(A2f())) {
                        C117305Zk.A0x(this);
                        return;
                    }
                    if ("novipay_p_deposit_card_processing_interstitial".equals(A2f())) {
                        AnonymousClass60Y.A02(((AbstractActivityC123635nW) this).A07, "FLOW_SESSION_END", "ADD_MONEY", "TRANSACTION_STATUS", "SCREEN");
                    }
                    super.onBackPressed();
                }

                @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
                public void onCreate(Bundle bundle) {
                    Intent intent;
                    String valueOf;
                    String str;
                    super.onCreate(bundle);
                    this.A0G = new AnonymousClass5y1(this, ((AbstractActivityC121705jc) this).A03, 1001);
                    C125065qd.A00(this);
                    if (getIntent() != null && !getIntent().hasExtra("screen_name")) {
                        getIntent().putExtra("screen_name", "novipay_p_connect_account_education");
                    }
                    if ("novipay_p_login_password".equals(A2f()) && ((ActivityC13810kN) this).A0C.A07(703)) {
                        int A02 = ((AbstractActivityC121705jc) this).A0N.A02();
                        str = "can_login_with_biometric";
                        if (A02 == 1) {
                            intent = getIntent();
                            valueOf = "1";
                        } else if (A02 == 3) {
                            C130105yo.A01(((AbstractActivityC123635nW) this).A0A).remove("wavi_bio_skip_counter").apply();
                            intent = getIntent();
                            valueOf = "0";
                        }
                        AbstractActivityC119645em.A0O(intent, str, valueOf);
                    } else if ("novipay_p_close_novi_account".equals(A2f())) {
                        intent = getIntent();
                        valueOf = String.valueOf(((ActivityC13810kN) this).A0C.A07(1122));
                        str = "automatic_account_closure_enabled";
                        AbstractActivityC119645em.A0O(intent, str, valueOf);
                    }
                    if (((AbstractActivityC121705jc) this).A0E.A01() != null) {
                        AbstractActivityC119645em.A0O(getIntent(), "country_code_alpha2", ((AbstractActivityC121705jc) this).A0E.A01().A03);
                    }
                    String stringExtra = getIntent().getStringExtra("screen_name");
                    if (("novipay_p_connect_account_education".equals(stringExtra) || "novipay_p_login_password".equals(stringExtra)) && ((ActivityC13810kN) this).A09.A1J("payment_currency_metadata_last_sync_timestamp", 1209600000)) {
                        this.A03.A0A(new AnonymousClass68G(this));
                    }
                    ((AbstractActivityC123635nW) this).A01 = UserJid.getNullable(getIntent().getStringExtra("extra_inviter_jid"));
                    A2i();
                    C129675y7 r3 = this.A0E;
                    this.A0F = new C129615y0(((AbstractActivityC123635nW) this).A04, ((AbstractActivityC123635nW) this).A05, r3);
                    C117295Zj.A0r(this, r3.A04, 78);
                    C117295Zj.A0r(this, ((AbstractActivityC123635nW) this).A08.A0G, 79);
                    C117295Zj.A0r(this, ((AbstractActivityC123635nW) this).A08.A0I, 77);
                }

                @Override // X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
                public void onDestroy() {
                    if ("novipay_p_deposit_card_processing_interstitial".equals(A2f())) {
                        AnonymousClass60Y.A02(((AbstractActivityC123635nW) this).A07, "NAVIGATION_END", "ADD_MONEY", "TRANSACTION_STATUS", "SCREEN");
                    }
                    super.onDestroy();
                }

                @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity, X.AbstractC000300e
                public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
                    super.onRequestPermissionsResult(i, strArr, iArr);
                    AnonymousClass5y1 r3 = this.A0G;
                    AnonymousClass6GY r1 = new Runnable() { // from class: X.6GY
                        @Override // java.lang.Runnable
                        public final void run() {
                            NoviPayBloksActivity noviPayBloksActivity = NoviPayBloksActivity.this;
                            noviPayBloksActivity.startActivityForResult(C12990iw.A0D(noviPayBloksActivity, NoviWithdrawCashStoreLocatorActivity.class), 1005);
                        }
                    };
                    if (r3.A01 == i) {
                        for (int i2 = 0; i2 < strArr.length; i2++) {
                            if (iArr[i2] != 0) {
                                if (!r3.A00) {
                                    Activity activity = r3.A02;
                                    if (!AnonymousClass00T.A0G(activity, strArr[i2])) {
                                        C38211ni.A05(activity);
                                        return;
                                    }
                                    return;
                                } else {
                                    return;
                                }
                            }
                        }
                        r1.run();
                    }
                }
            }
