package com.whatsapp.payments.ui;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass614;
import X.AnonymousClass69M;
import X.AnonymousClass6BE;
import X.AnonymousClass6LW;
import X.C015607k;
import X.C117295Zj;
import X.C117315Zl;
import X.C117995b6;
import X.C118425bn;
import X.C12960it;
import X.C12970iu;
import X.C1310661b;
import X.C1329668y;
import X.C14830m7;
import X.C14850m9;
import X.C15450nH;
import X.C16590pI;
import X.C17900ra;
import X.C21860y6;
import X.C30931Zj;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiQrCodeScannedDialogFragment extends Hilt_IndiaUpiQrCodeScannedDialogFragment {
    public View A00;
    public View A01;
    public Button A02;
    public LinearLayout A03;
    public LinearLayout A04;
    public LinearLayout A05;
    public ProgressBar A06;
    public ProgressBar A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public C15450nH A0B;
    public C14830m7 A0C;
    public C16590pI A0D;
    public AnonymousClass018 A0E;
    public C14850m9 A0F;
    public AnonymousClass69M A0G;
    public C1329668y A0H;
    public C21860y6 A0I;
    public C17900ra A0J;
    public AnonymousClass6BE A0K;
    public C117995b6 A0L;
    public String A0M;
    public final C30931Zj A0N = C117295Zj.A0G("IndiaUpiQrCodeScannedDialogFragment");

    public static IndiaUpiQrCodeScannedDialogFragment A00(String str, String str2, String str3) {
        IndiaUpiQrCodeScannedDialogFragment indiaUpiQrCodeScannedDialogFragment = new IndiaUpiQrCodeScannedDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("ARG_URL", str);
        A0D.putString("external_payment_source", str2);
        A0D.putString("referral_screen", str3);
        indiaUpiQrCodeScannedDialogFragment.A0U(A0D);
        return indiaUpiQrCodeScannedDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        Bundle A03 = A03();
        this.A0L = (C117995b6) C117315Zl.A06(new C118425bn(this, A03.getString("ARG_URL"), A03.getString("external_payment_source")), this).A00(C117995b6.class);
        this.A0G = new AnonymousClass69M(this.A0B, this.A0H, this.A0K);
        C117295Zj.A0n(this.A02, this, 68);
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 1001) {
            if (this.A0I.A0B() || this.A0I.A0C()) {
                if (!this.A0F.A07(1933) || !AnonymousClass614.A02(this.A0M)) {
                    Bundle A03 = A03();
                    this.A0L.A04(A03.getString("ARG_URL"), A03.getString("external_payment_source"));
                    return;
                }
                A19();
                return;
            }
        } else if (i == 1002) {
            ActivityC000900k A0B = A0B();
            if (A0B instanceof AnonymousClass6LW) {
                ((Activity) ((AnonymousClass6LW) A0B)).setResult(i2, intent);
            }
        } else {
            return;
        }
        AnonymousClass01E r1 = super.A0D;
        if (r1 instanceof DialogFragment) {
            ((DialogFragment) r1).A1B();
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        this.A0N.A06("scanned payment QR code deep link");
        View inflate = A0C().getLayoutInflater().inflate(R.layout.india_upi_qr_code_detail_dialog, (ViewGroup) null);
        this.A01 = inflate;
        this.A07 = (ProgressBar) inflate.findViewById(R.id.progress);
        this.A03 = (LinearLayout) this.A01.findViewById(R.id.details_row);
        this.A09 = C12960it.A0J(this.A01, R.id.contact_info_title);
        this.A08 = C12960it.A0J(this.A01, R.id.contact_info_subtitle);
        this.A0A = C12960it.A0J(this.A01, R.id.error_desc);
        this.A02 = (Button) this.A01.findViewById(R.id.positive_button);
        this.A04 = (LinearLayout) this.A01.findViewById(R.id.prefill_amount);
        this.A05 = (LinearLayout) this.A01.findViewById(R.id.qr_code_secure_info_container);
        this.A00 = this.A01.findViewById(R.id.qr_code_secure_info_container_divider);
        Drawable[] compoundDrawables = C12960it.A0J(this.A01, R.id.warning_text).getCompoundDrawables();
        for (Drawable drawable : compoundDrawables) {
            if (drawable != null) {
                C015607k.A0A(drawable, A02().getColor(R.color.secondary_text));
            }
        }
        ProgressBar progressBar = (ProgressBar) this.A01.findViewById(R.id.button_progress_bar);
        this.A06 = progressBar;
        progressBar.getIndeterminateDrawable().setColorFilter(AnonymousClass00T.A00(A0p(), R.color.primary_surface), PorterDuff.Mode.SRC_IN);
        String string = A03().getString("referral_screen");
        this.A0M = string;
        this.A0K.AKg(0, null, "qr_code_scan_prompt", string);
        return this.A01;
    }

    public final void A19() {
        Object A01 = this.A0L.A06.A01();
        AnonymousClass009.A05(A01);
        C1310661b r0 = (C1310661b) A01;
        AnonymousClass69M r1 = this.A0G;
        ActivityC000900k A0C = A0C();
        String str = r0.A03;
        AnonymousClass009.A05(str);
        r1.A00(A0C, null, str, r0.A00, this.A0M);
        AnonymousClass01E r12 = super.A0D;
        if (r12 instanceof DialogFragment) {
            ((DialogFragment) r12).A1B();
        }
    }
}
