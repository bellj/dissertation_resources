package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121505iP;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass2FL;
import X.AnonymousClass69E;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.RoundedBottomSheetDialogFragment;

/* loaded from: classes4.dex */
public class IndiaUpiProvideMoreInfoBottomSheetActivity extends AbstractActivityC121505iP {
    public boolean A00;

    /* loaded from: classes4.dex */
    public class BottomSheetProvideMoreInfoFragment extends RoundedBottomSheetDialogFragment {
        @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
        public void A0l() {
            super.A0l();
            C117315Zl.A0O(this);
        }

        @Override // X.AnonymousClass01E
        public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
            View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_account_recovery_info_bottom_sheet);
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                C117295Zj.A0n(AnonymousClass028.A0D(A0F, R.id.close), this, 66);
                C117295Zj.A0n(AnonymousClass028.A0D(A0F, R.id.account_recovery_info_continue), A0B, 67);
            }
            return A0F;
        }
    }

    public IndiaUpiProvideMoreInfoBottomSheetActivity() {
        this(0);
    }

    public IndiaUpiProvideMoreInfoBottomSheetActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 68);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            ((AbstractActivityC121505iP) this).A04 = (AnonymousClass69E) A1M.A9W.get();
            ((AbstractActivityC121505iP) this).A00 = C117305Zk.A0G(A1M);
            ((AbstractActivityC121505iP) this).A02 = C117305Zk.A0N(A1M);
        }
    }

    @Override // X.AbstractActivityC121505iP, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        paymentBottomSheet.A01 = new BottomSheetProvideMoreInfoFragment();
        Adm(paymentBottomSheet);
    }
}
