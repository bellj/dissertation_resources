package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import android.view.MenuItem;

/* loaded from: classes4.dex */
public class IndiaUpiAccountRecoveryFinishActivity extends AbstractActivityC121665jA {
    public boolean A00;

    public IndiaUpiAccountRecoveryFinishActivity() {
        this(0);
    }

    public IndiaUpiAccountRecoveryFinishActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 30);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "notify_verification_complete", ((AbstractActivityC121665jA) this).A0K);
        super.onBackPressed();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x004d, code lost:
        if (r2 == 12) goto L_0x004f;
     */
    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            super.onCreate(r6)
            r0 = 2131559093(0x7f0d02b5, float:1.874352E38)
            r5.setContentView(r0)
            r0 = 2131361872(0x7f0a0050, float:1.8343509E38)
            android.widget.ImageView r1 = X.C117305Zk.A06(r5, r0)
            r0 = 2131232020(0x7f080514, float:1.8080137E38)
            r1.setImageResource(r0)
            r0 = 2131361874(0x7f0a0052, float:1.8343513E38)
            android.widget.TextView r1 = X.C12970iu.A0M(r5, r0)
            r0 = 2131892488(0x7f121908, float:1.9419726E38)
            r1.setText(r0)
            r0 = 2131361873(0x7f0a0051, float:1.834351E38)
            android.widget.TextView r1 = X.C12970iu.A0M(r5, r0)
            r0 = 2131892487(0x7f121907, float:1.9419724E38)
            r1.setText(r0)
            X.02i r1 = X.AbstractActivityC119235dO.A0K(r5)
            if (r1 == 0) goto L_0x003c
            r0 = 2131890367(0x7f1210bf, float:1.9415424E38)
            X.C117295Zj.A0g(r5, r1, r0)
        L_0x003c:
            r0 = 2131361871(0x7f0a004f, float:1.8343507E38)
            android.widget.TextView r3 = X.C12970iu.A0M(r5, r0)
            int r2 = r5.A02
            r0 = 5
            if (r2 == r0) goto L_0x004f
            r1 = 12
            r0 = 2131887811(0x7f1206c3, float:1.941024E38)
            if (r2 != r1) goto L_0x0052
        L_0x004f:
            r0 = 2131886730(0x7f12028a, float:1.9408047E38)
        L_0x0052:
            r3.setText(r0)
            r0 = 16
            X.C117295Zj.A0n(r3, r5, r0)
            X.6BE r4 = r5.A0D
            java.lang.Integer r3 = X.C12980iv.A0i()
            r2 = 0
            java.lang.String r1 = r5.A0K
            java.lang.String r0 = "notify_verification_complete"
            r4.AKg(r3, r2, r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiAccountRecoveryFinishActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "notify_verification_complete", ((AbstractActivityC121665jA) this).A0K);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
