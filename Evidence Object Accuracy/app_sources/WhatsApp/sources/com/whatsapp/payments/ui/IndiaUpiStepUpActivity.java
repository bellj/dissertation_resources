package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass102;
import X.AnonymousClass2FL;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117985b5;
import X.C118485bt;
import X.C120525gK;
import X.C127475uY;
import X.C128355vy;
import X.C129145xF;
import X.C12960it;
import X.C12980iv;
import X.C1308460e;
import X.C1329668y;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C17900ra;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C20400vh;
import X.C21860y6;
import X.C30861Zc;
import X.C30931Zj;
import X.C452120p;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiStepUpActivity;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiStepUpActivity extends AbstractActivityC121545iU {
    public C30861Zc A00;
    public C20400vh A01;
    public C128355vy A02;
    public C117985b5 A03;
    public String A04;
    public boolean A05;
    public final C30931Zj A06;
    public final List A07;

    public IndiaUpiStepUpActivity() {
        this(0);
        this.A06 = C117295Zj.A0G("IndiaUpiStepUpActivity");
        this.A07 = C12960it.A0l();
    }

    public IndiaUpiStepUpActivity(int i) {
        this.A05 = false;
        C117295Zj.A0p(this, 75);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
            this.A02 = (C128355vy) A1M.A9f.get();
            this.A01 = (C20400vh) A1M.AE8.get();
        }
    }

    @Override // X.AnonymousClass6MS
    public void ARr(C452120p r4, String str) {
        Integer num;
        if (!TextUtils.isEmpty(str)) {
            this.A06.A06("onListKeys called");
            C127475uY r1 = new C127475uY(1);
            r1.A01 = str;
            this.A03.A04(r1);
        } else if (r4 != null && !AnonymousClass69E.A02(this, "upi-list-keys", r4.A00, false)) {
            if (((AbstractActivityC121545iU) this).A06.A07("upi-list-keys")) {
                AbstractActivityC119235dO.A1i(this);
                return;
            }
            C30931Zj r2 = this.A06;
            StringBuilder A0k = C12960it.A0k("onListKeys: ");
            if (str != null) {
                num = Integer.valueOf(str.length());
            } else {
                num = null;
            }
            A0k.append(num);
            r2.A06(C12960it.A0d(" failed; ; showErrorAndFinish", A0k));
            A39();
        }
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r3) {
        throw C12980iv.A0u(this.A06.A02("onSetPin unsupported"));
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1014) {
            if (i2 == -1) {
                ((AbstractActivityC121665jA) this).A0C.A08();
                ((AbstractActivityC121685jC) this).A0D.A05(this.A07);
                this.A01.A02(null);
            }
            finish();
        }
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Parcelable parcelableExtra = getIntent().getParcelableExtra("extra_bank_account");
        AnonymousClass009.A06(parcelableExtra, "Bank account must be passed with intent extras");
        this.A00 = (C30861Zc) parcelableExtra;
        List list = this.A07;
        String stringExtra = getIntent().getStringExtra("extra_step_up_id");
        AnonymousClass009.A06(stringExtra, "Step up id must be passed as intent extra");
        list.add(stringExtra);
        C14850m9 r13 = ((ActivityC13810kN) this).A0C;
        C14900mE r0 = ((ActivityC13810kN) this).A05;
        C15570nT r14 = ((ActivityC13790kL) this).A01;
        C17220qS r12 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r11 = ((AbstractActivityC121545iU) this).A0C;
        C17070qD r10 = ((AbstractActivityC121685jC) this).A0P;
        C21860y6 r9 = ((AbstractActivityC121685jC) this).A0I;
        C1308460e r8 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r02 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass102 r15 = ((AbstractActivityC121545iU) this).A02;
        C17900ra r7 = ((AbstractActivityC121685jC) this).A0N;
        AnonymousClass6BE r6 = ((AbstractActivityC121665jA) this).A0D;
        C18640sm r5 = ((ActivityC13810kN) this).A07;
        C18650sn r3 = ((AbstractActivityC121685jC) this).A0K;
        C1329668y r2 = ((AbstractActivityC121665jA) this).A0B;
        ((AbstractActivityC121545iU) this).A09 = new C120525gK(this, r0, r14, r5, r15, r13, r12, r8, r2, r9, r3, r02, r7, r10, this, r6, ((AbstractActivityC121545iU) this).A0B, r11);
        C129145xF r92 = new C129145xF(this, r0, r5, r3, r02);
        String A2o = A2o(r2.A06());
        this.A04 = A2o;
        C128355vy r1 = this.A02;
        C18590sh r03 = ((AbstractActivityC121545iU) this).A0C;
        C117985b5 r32 = (C117985b5) C117315Zl.A06(new C118485bt(this.A00, ((AbstractActivityC121665jA) this).A0C, ((AbstractActivityC121545iU) this).A09, r92, this, r1, r03, A2o), this).A00(C117985b5.class);
        this.A03 = r32;
        r32.A00.A05(r32.A03, C117305Zk.A0B(this, 68));
        C117985b5 r33 = this.A03;
        r33.A01.A05(r33.A03, C117305Zk.A0B(this, 67));
        this.A03.A04(new C127475uY(0));
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 28) {
            if (i != 32) {
                switch (i) {
                    case 10:
                        return A32(new Runnable() { // from class: X.6GJ
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiStepUpActivity indiaUpiStepUpActivity = IndiaUpiStepUpActivity.this;
                                C36021jC.A00(indiaUpiStepUpActivity, 10);
                                String A0A = ((AbstractActivityC121665jA) indiaUpiStepUpActivity).A0B.A0A();
                                if (!TextUtils.isEmpty(A0A)) {
                                    String A0O = AbstractActivityC119235dO.A0O(indiaUpiStepUpActivity);
                                    indiaUpiStepUpActivity.A04 = A0O;
                                    C30861Zc r0 = indiaUpiStepUpActivity.A00;
                                    indiaUpiStepUpActivity.A3E((C119755f3) r0.A08, A0A, r0.A0B, A0O, (String) C117295Zj.A0R(r0.A09), 3);
                                    indiaUpiStepUpActivity.A03.A02 = indiaUpiStepUpActivity.A04;
                                    return;
                                }
                                ((AbstractActivityC121545iU) indiaUpiStepUpActivity).A09.A00();
                            }
                        }, getString(R.string.upi_check_balance_incorrect_pin_title), getString(R.string.upi_check_balance_incorrect_pin_message), i, R.string.payments_try_again, R.string.cancel);
                    case 11:
                        break;
                    case 12:
                        return A31(new Runnable() { // from class: X.6GI
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiStepUpActivity indiaUpiStepUpActivity = IndiaUpiStepUpActivity.this;
                                C117305Zk.A1D(indiaUpiStepUpActivity, 12);
                                indiaUpiStepUpActivity.A2q();
                                indiaUpiStepUpActivity.finish();
                            }
                        }, getString(R.string.payments_need_pin_to_continue), 12, R.string.learn_more, R.string.ok);
                    default:
                        return super.onCreateDialog(i);
                }
            } else {
                C004802e A0S = C12980iv.A0S(this);
                A0S.A06(R.string.payments_action_already_taken);
                C117295Zj.A0q(A0S, this, 65, R.string.ok);
                return A0S.create();
            }
        }
        return A30(this.A00, i);
    }
}
