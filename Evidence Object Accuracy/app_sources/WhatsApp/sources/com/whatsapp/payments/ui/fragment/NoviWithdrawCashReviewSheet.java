package com.whatsapp.payments.ui.fragment;

import X.AbstractC136466Mq;
import X.AnonymousClass028;
import X.AnonymousClass4OZ;
import X.AnonymousClass60Y;
import X.C117295Zj;
import X.C117305Zk;
import X.C121885kQ;
import X.C127445uV;
import X.C12960it;
import X.C134116Dj;
import X.C134126Dk;
import X.C134136Dl;
import X.C134156Dn;
import X.C15570nT;
import X.C16590pI;
import X.C21270x9;
import X.C27621Ig;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviWithdrawCashReviewSheet extends Hilt_NoviWithdrawCashReviewSheet {
    public C15570nT A00;
    public C21270x9 A01;
    public C16590pI A02;
    public AnonymousClass60Y A03;
    public C134156Dn A04;
    public C121885kQ A05;
    public AbstractC136466Mq A06;
    public C127445uV A07;
    public String A08;

    public NoviWithdrawCashReviewSheet(String str) {
        this.A08 = str;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_withdraw_cash_review_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        AnonymousClass60Y.A02(this.A03, "NAVIGATION_END", this.A08, "REVIEW_TRANSACTION", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Parcelable parcelable;
        Parcelable parcelable2;
        boolean z;
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.send_money_review_header_close), this, 122);
        Bundle bundle2 = super.A05;
        Parcelable parcelable3 = null;
        if (bundle2 != null) {
            parcelable3 = bundle2.getParcelable("withdraw-amount-data");
            parcelable = super.A05.getParcelable("withdraw-method-data");
            parcelable2 = super.A05.getParcelable("withdraw-transaction-data");
            z = super.A05.getBoolean("display-footer");
        } else {
            parcelable = null;
            parcelable2 = null;
            z = false;
        }
        C134116Dj r8 = new C134116Dj();
        r8.AYL(C117305Zk.A05(view, r8, R.id.novi_withdraw_review_amount, R.id.novi_withdraw_review_amount_inflated));
        r8.A6Q(new AnonymousClass4OZ(2, parcelable3));
        C15570nT r0 = this.A00;
        r0.A08();
        C27621Ig r7 = r0.A01;
        if (r7 != null) {
            this.A01.A04(this.A02.A00, "novi-withdraw-review-sheet").A06(r8.A05, r7);
        }
        C134136Dl r2 = new C134136Dl(C117305Zk.A0A(this, 123), this.A05.A04);
        r2.AYL(C117305Zk.A05(view, r2, R.id.novi_withdraw_review_method, R.id.novi_withdraw_review_method_inflated));
        r2.A6Q(new AnonymousClass4OZ(2, parcelable));
        if (z) {
            AnonymousClass028.A0D(view, R.id.bottom_divider).setVisibility(0);
            AnonymousClass028.A0D(view, R.id.novi_depost_review_footer).setVisibility(0);
        }
        C134126Dk r22 = new C134126Dk();
        r22.AYL(C117305Zk.A05(view, r22, R.id.novi_withdraw_review_summary, R.id.novi_withdraw_review_summary_inflated));
        r22.A6Q(new AnonymousClass4OZ(2, parcelable2));
        C134156Dn r23 = new C134156Dn();
        this.A04 = r23;
        r23.AYL(C117305Zk.A05(view, r23, R.id.novi_withdraw_review_confirm, R.id.novi_withdraw_review_confirm_inflated));
        this.A07 = new C127445uV(C117305Zk.A0A(this, 125), A0I(R.string.novi_confirm_button_label), true);
        this.A04.A6Q(new AnonymousClass4OZ(super.A05.getInt("initial-button-state", 2), this.A07));
        AnonymousClass60Y.A02(this.A03, "NAVIGATION_START", this.A08, "REVIEW_TRANSACTION", "SCREEN");
    }
}
