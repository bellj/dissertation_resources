package com.whatsapp.payments.ui;

import X.AbstractActivityC121695jQ;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass19Y;
import X.AnonymousClass2FL;
import X.AnonymousClass69D;
import X.C117295Zj;
import X.C117305Zk;
import X.C118035bA;
import X.C122335lI;
import X.C124295ov;
import X.C127105tx;
import X.C128215vk;
import X.C128345vx;
import X.C12960it;
import X.C12970iu;
import X.C129925yW;
import X.C17900ra;
import X.C18660so;
import X.C18790t3;
import X.C22710zW;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class BrazilMerchantDetailsListActivity extends AbstractActivityC121695jQ {
    public AnonymousClass19Y A00;
    public C18790t3 A01;
    public AnonymousClass69D A02;
    public C129925yW A03;
    public C18660so A04;
    public C17900ra A05;
    public C22710zW A06;
    public C124295ov A07;
    public C118035bA A08;
    public C128345vx A09;
    public boolean A0A;

    public BrazilMerchantDetailsListActivity() {
        this(0);
    }

    public BrazilMerchantDetailsListActivity(int i) {
        this.A0A = false;
        C117295Zj.A0p(this, 10);
    }

    public static /* synthetic */ void A0D(BrazilMerchantDetailsListActivity brazilMerchantDetailsListActivity, C128215vk r14) {
        Uri uri;
        String str;
        switch (r14.A01) {
            case 0:
                Context applicationContext = brazilMerchantDetailsListActivity.getApplicationContext();
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(applicationContext.getPackageName(), "com.whatsapp.payments.ui.MerchantPayoutTransactionHistoryActivity");
                brazilMerchantDetailsListActivity.startActivity(A0A);
                return;
            case 1:
                AbstractC14440lR r2 = ((ActivityC13830kP) brazilMerchantDetailsListActivity).A05;
                C124295ov r0 = brazilMerchantDetailsListActivity.A07;
                if (r0 != null && r0.A00() == 1) {
                    brazilMerchantDetailsListActivity.A07.A03(false);
                }
                Bundle A0D = C12970iu.A0D();
                A0D.putString("com.whatsapp.support.DescribeProblemActivity.from", "payments:settings");
                C18790t3 r8 = brazilMerchantDetailsListActivity.A01;
                C124295ov r3 = new C124295ov(A0D, brazilMerchantDetailsListActivity, brazilMerchantDetailsListActivity.A00, ((ActivityC13810kN) brazilMerchantDetailsListActivity).A06, r8, ((ActivityC13830kP) brazilMerchantDetailsListActivity).A01, null, null, ((ActivityC13810kN) brazilMerchantDetailsListActivity).A0D, brazilMerchantDetailsListActivity.A05, "payments:settings");
                brazilMerchantDetailsListActivity.A07 = r3;
                C12960it.A1E(r3, r2);
                return;
            case 2:
                uri = r14.A03;
                AnonymousClass009.A05(uri);
                str = "android.intent.action.VIEW";
                break;
            case 3:
                uri = Uri.fromParts("tel", r14.A05, null);
                str = "android.intent.action.DIAL";
                break;
            case 4:
                brazilMerchantDetailsListActivity.AaN();
                Context applicationContext2 = brazilMerchantDetailsListActivity.getApplicationContext();
                HashMap hashMap = r14.A07;
                String str2 = r14.A06;
                Intent A0A2 = C12970iu.A0A();
                A0A2.setClassName(applicationContext2.getPackageName(), "com.whatsapp.payments.ui.BrazilPayBloksActivity");
                A0A2.putExtra("screen_params", hashMap);
                A0A2.putExtra("screen_name", str2);
                brazilMerchantDetailsListActivity.A2E(A0A2, 1);
                return;
            case 5:
                if (r14.A08) {
                    brazilMerchantDetailsListActivity.A2P(brazilMerchantDetailsListActivity.getString(r14.A02));
                    return;
                } else {
                    brazilMerchantDetailsListActivity.AaN();
                    return;
                }
            case 6:
                brazilMerchantDetailsListActivity.Ado(r14.A00);
                return;
            case 7:
                brazilMerchantDetailsListActivity.A02.A01(brazilMerchantDetailsListActivity, ((ActivityC13810kN) brazilMerchantDetailsListActivity).A0C, brazilMerchantDetailsListActivity.A03, r14.A04.A00, R.string.payments_generic_error).show();
                return;
            default:
                return;
        }
        Intent intent = new Intent(str, uri);
        if (intent.resolveActivity(brazilMerchantDetailsListActivity.getPackageManager()) != null) {
            brazilMerchantDetailsListActivity.startActivity(intent);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC121695jQ) this).A00 = C117305Zk.A0P(A1M);
            this.A01 = (C18790t3) A1M.AJw.get();
            this.A00 = (AnonymousClass19Y) A1M.AI6.get();
            this.A06 = C117305Zk.A0O(A1M);
            this.A02 = A09.A08();
            this.A05 = (C17900ra) A1M.AF5.get();
            this.A03 = (C129925yW) A1M.AEW.get();
            this.A04 = (C18660so) A1M.AEf.get();
            this.A09 = (C128345vx) A1M.A23.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.seller_account_is_removed) {
            finish();
        }
    }

    @Override // X.AbstractActivityC121695jQ, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i != 302) {
            return super.A2e(viewGroup, i);
        }
        return new C122335lI(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.merchant_payout_detail_row_item_view));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            this.A08.A05(new C127105tx(3));
        }
    }
}
