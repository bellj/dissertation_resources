package com.whatsapp.payments.ui;

import X.AbstractC136196Lo;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass13N;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass3FV;
import X.AnonymousClass5UM;
import X.AnonymousClass5y1;
import X.AnonymousClass60Y;
import X.AnonymousClass61F;
import X.AnonymousClass61L;
import X.AnonymousClass61M;
import X.AnonymousClass61S;
import X.AnonymousClass65O;
import X.AnonymousClass66U;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117395Zt;
import X.C121885kQ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C129865yQ;
import X.C13010iy;
import X.C130125yq;
import X.C130155yt;
import X.C130305zC;
import X.C130575zd;
import X.C1310460z;
import X.C1310561a;
import X.C15890o4;
import X.C244615p;
import X.C35781ij;
import X.C35961j4;
import X.C36311jg;
import X.C36321jh;
import X.C36331ji;
import X.C38211ni;
import X.C48232Fc;
import X.C52812bj;
import X.C618632v;
import X.C92254Vd;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import androidx.appcompat.widget.SearchView;
import com.google.android.gms.maps.GoogleMapOptions;
import com.whatsapp.R;
import com.whatsapp.location.PlaceInfo;
import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;
import com.whatsapp.payments.ui.widget.PayToolbar;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public class NoviWithdrawCashStoreLocatorActivity extends ActivityC13790kL implements LocationListener {
    public Location A00;
    public View A01;
    public View A02;
    public ProgressBar A03;
    public C35961j4 A04;
    public C36331ji A05;
    public C36311jg A06;
    public C244615p A07;
    public C48232Fc A08;
    public C15890o4 A09;
    public AnonymousClass3FV A0A;
    public C618632v A0B;
    public C92254Vd A0C;
    public C52812bj A0D;
    public AnonymousClass61M A0E;
    public C130155yt A0F;
    public C129865yQ A0G;
    public C130125yq A0H;
    public AnonymousClass60Y A0I;
    public AnonymousClass61F A0J;
    public C121885kQ A0K;
    public AnonymousClass5y1 A0L;
    public boolean A0M;
    public boolean A0N;
    public final DataSetObserver A0O;

    public NoviWithdrawCashStoreLocatorActivity() {
        this(0);
        this.A0M = false;
        this.A0O = new C117395Zt(this);
    }

    public NoviWithdrawCashStoreLocatorActivity(int i) {
        this.A0N = false;
        C117295Zj.A0p(this, 95);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0N) {
            this.A0N = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0I = C117305Zk.A0W(A1M);
            this.A0K = (C121885kQ) A1M.ADN.get();
            this.A0F = (C130155yt) A1M.ADA.get();
            this.A0J = C117305Zk.A0X(A1M);
            this.A09 = C12970iu.A0Y(A1M);
            this.A07 = (C244615p) A1M.A8H.get();
            this.A0H = (C130125yq) A1M.ADJ.get();
            this.A0E = C117305Zk.A0N(A1M);
        }
    }

    public final void A2e(double d, double d2, float f) {
        ProgressBar progressBar = this.A03;
        if (progressBar != null) {
            progressBar.setVisibility(0);
        }
        C52812bj r1 = this.A0D;
        if (r1 != null) {
            r1.A01 = null;
            r1.notifyDataSetChanged();
        }
        C130155yt r4 = this.A0F;
        C1310460z A01 = AnonymousClass61S.A01("novi-get-withdraw-cash-locations");
        AnonymousClass61S[] r7 = new AnonymousClass61S[2];
        r7[0] = new AnonymousClass61S("latitude", C1310561a.A00(d));
        C117305Zk.A1K(A01, "coordinates", C12960it.A0m(new AnonymousClass61S("longitude", C1310561a.A00(d2)), r7, 1));
        C117305Zk.A1K(A01, "radius", C12980iv.A0x(Collections.singletonList(new AnonymousClass61S("value", (double) f))));
        A01.A02.add(AnonymousClass61L.A02(this.A0H, true));
        C117305Zk.A1K(A01, "image_scale_factor", AnonymousClass61S.A02("value", "ONE_X"));
        r4.A0B(new AbstractC136196Lo(d, d2, f) { // from class: X.6A5
            public final /* synthetic */ double A00;
            public final /* synthetic */ double A01;
            public final /* synthetic */ float A02;

            {
                this.A00 = r2;
                this.A01 = r4;
                this.A02 = r6;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r20) {
                Object obj;
                NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = NoviWithdrawCashStoreLocatorActivity.this;
                double d3 = this.A00;
                double d4 = this.A01;
                float f2 = this.A02;
                C12970iu.A1G(noviWithdrawCashStoreLocatorActivity.A03);
                if (!r20.A06() || (obj = r20.A02) == null) {
                    noviWithdrawCashStoreLocatorActivity.A0G.A02(r20.A00, null, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00eb: INVOKE  
                          (wrap: X.5yQ : 0x00de: IGET  (r1v0 X.5yQ A[REMOVE]) = (r13v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity) com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity.A0G X.5yQ)
                          (wrap: X.20p : 0x00e0: IGET  (r0v3 X.20p A[REMOVE]) = (r20v0 'r20' X.5zy) X.5zy.A00 X.20p)
                          (null java.lang.Runnable)
                          (wrap: X.6Jt : 0x00e8: CONSTRUCTOR  (r12v0 X.6Jt A[REMOVE]) = 
                          (r13v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity)
                          (r14v0 'd3' double)
                          (r2v0 'd4' double)
                          (r5v0 'f2' float)
                         call: X.6Jt.<init>(com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity, double, double, float):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.5yQ.A02(X.20p, java.lang.Runnable, java.lang.Runnable):void in method: X.6A5.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00e8: CONSTRUCTOR  (r12v0 X.6Jt A[REMOVE]) = 
                          (r13v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity)
                          (r14v0 'd3' double)
                          (r2v0 'd4' double)
                          (r5v0 'f2' float)
                         call: X.6Jt.<init>(com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity, double, double, float):void type: CONSTRUCTOR in method: X.6A5.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Jt, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                    // Method dump skipped, instructions count: 244
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A5.AV8(X.5zy):void");
                }
            }, A01, "get", 1);
        }

        public final void A2f(int i) {
            View view = this.A01;
            if (view != null && this.A0C != null) {
                view.clearAnimation();
                this.A0C.A00(i, true);
            }
        }

        public void A2g(String str) {
            SearchView searchView;
            C48232Fc r0 = this.A08;
            if (!(r0 == null || (searchView = r0.A02) == null)) {
                searchView.clearFocus();
            }
            C52812bj r02 = this.A0D;
            C130575zd r8 = null;
            if (r02 != null) {
                r02.A01 = null;
                r02.notifyDataSetChanged();
            }
            ProgressBar progressBar = this.A03;
            if (progressBar != null) {
                progressBar.setVisibility(0);
            }
            AnonymousClass3FV r03 = this.A0A;
            if (r03 != null) {
                Location A01 = r03.A01();
                r8 = new C130575zd(A01.getLatitude(), A01.getLongitude());
            }
            C1310460z A012 = AnonymousClass61S.A01("novi-address-type-ahead-search");
            C117305Zk.A1K(A012, "query", AnonymousClass61S.A02("value", str));
            A012.A02.add(AnonymousClass61L.A02(this.A0H, true));
            if (r8 != null) {
                AnonymousClass61S[] r6 = new AnonymousClass61S[2];
                r6[0] = new AnonymousClass61S("latitude", C1310561a.A00(r8.A00));
                C117305Zk.A1K(A012, "coordinates", C12960it.A0m(new AnonymousClass61S("longitude", C1310561a.A00(r8.A01)), r6, 1));
            }
            this.A0F.A0B(new AbstractC136196Lo(str) { // from class: X.69x
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AbstractC136196Lo
                public final void AV8(C130785zy r14) {
                    Object obj;
                    C130575zd A00;
                    NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = NoviWithdrawCashStoreLocatorActivity.this;
                    String str2 = this.A01;
                    C12970iu.A1G(noviWithdrawCashStoreLocatorActivity.A03);
                    if (!r14.A06() || (obj = r14.A02) == null) {
                        noviWithdrawCashStoreLocatorActivity.A0G.A02(r14.A00, null, 
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00a0: INVOKE  
                              (wrap: X.5yQ : 0x0097: IGET  (r2v0 X.5yQ A[REMOVE]) = (r7v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity) com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity.A0G X.5yQ)
                              (wrap: X.20p : 0x0099: IGET  (r1v0 X.20p A[REMOVE]) = (r14v0 'r14' X.5zy) X.5zy.A00 X.20p)
                              (null java.lang.Runnable)
                              (wrap: X.6IP : 0x009d: CONSTRUCTOR  (r0v2 X.6IP A[REMOVE]) = 
                              (r7v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity)
                              (r3v0 'str2' java.lang.String)
                             call: X.6IP.<init>(com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity, java.lang.String):void type: CONSTRUCTOR)
                             type: VIRTUAL call: X.5yQ.A02(X.20p, java.lang.Runnable, java.lang.Runnable):void in method: X.69x.AV8(X.5zy):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x009d: CONSTRUCTOR  (r0v2 X.6IP A[REMOVE]) = 
                              (r7v0 'noviWithdrawCashStoreLocatorActivity' com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity)
                              (r3v0 'str2' java.lang.String)
                             call: X.6IP.<init>(com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity, java.lang.String):void type: CONSTRUCTOR in method: X.69x.AV8(X.5zy):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 23 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IP, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 29 more
                            */
                        /*
                            this = this;
                            com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity r7 = com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity.this
                            java.lang.String r3 = r13.A01
                            android.widget.ProgressBar r0 = r7.A03
                            X.C12970iu.A1G(r0)
                            boolean r0 = r14.A06()
                            r6 = 0
                            if (r0 == 0) goto L_0x0097
                            java.lang.Object r1 = r14.A02
                            if (r1 == 0) goto L_0x0097
                            X.1V8 r1 = (X.AnonymousClass1V8) r1
                            java.lang.String r0 = "address"
                            java.util.List r0 = r1.A0J(r0)
                            java.util.ArrayList r5 = X.C12960it.A0l()
                            java.util.Iterator r3 = r0.iterator()
                        L_0x0024:
                            boolean r0 = r3.hasNext()
                            if (r0 == 0) goto L_0x004b
                            X.1V8 r1 = X.C117305Zk.A0d(r3)
                            java.lang.String r0 = "full_address"
                            java.lang.String r2 = r1.A0H(r0)     // Catch: 1V9 -> 0x0024
                            java.lang.String r0 = "coordinates"
                            X.1V8 r0 = r1.A0E(r0)     // Catch: 1V9 -> 0x0024
                            if (r0 == 0) goto L_0x0024
                            X.5zd r1 = X.C130575zd.A00(r0)     // Catch: 1V9 -> 0x0024
                            if (r1 == 0) goto L_0x0024
                            X.5tR r0 = new X.5tR     // Catch: 1V9 -> 0x0024
                            r0.<init>(r1, r2)     // Catch: 1V9 -> 0x0024
                            r5.add(r0)
                            goto L_0x0024
                        L_0x004b:
                            int r0 = r5.size()
                            if (r0 <= 0) goto L_0x0096
                            X.3FV r0 = r7.A0A
                            if (r0 == 0) goto L_0x0096
                            X.60Y r4 = r7.A0I
                            java.lang.String r3 = "LOCATION_STORE_PIN_LOADED"
                            java.lang.String r2 = "WITHDRAW_MONEY"
                            java.lang.String r1 = "SELECT_LOCATION"
                            java.lang.String r0 = "SCREEN"
                            X.AnonymousClass60Y.A02(r4, r3, r2, r1, r0)
                            r0 = 0
                            java.lang.Object r2 = r5.get(r0)
                            X.5tR r2 = (X.C126785tR) r2
                            X.2Fc r0 = r7.A08
                            androidx.appcompat.widget.SearchView r1 = r0.A02
                            java.lang.String r0 = r2.A01
                            r1.A0F(r0)
                            X.5zd r0 = r2.A00
                            double r8 = r0.A00
                            double r10 = r0.A01
                            r12 = 0
                            r7.A2e(r8, r10, r12)
                            X.3FV r4 = r7.A0A
                            java.lang.String r0 = ""
                            android.location.Location r5 = new android.location.Location
                            r5.<init>(r0)
                            r5.setLatitude(r8)
                            r5.setLongitude(r10)
                            r0 = 1097859072(0x41700000, float:15.0)
                            java.lang.Float r7 = java.lang.Float.valueOf(r0)
                            r9 = 1
                            r8 = r6
                            r4.A03(r5, r6, r7, r8, r9)
                        L_0x0096:
                            return
                        L_0x0097:
                            X.5yQ r2 = r7.A0G
                            X.20p r1 = r14.A00
                            X.6IP r0 = new X.6IP
                            r0.<init>(r7, r3)
                            r2.A02(r1, r6, r0)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C1332169x.AV8(X.5zy):void");
                    }
                }, A012, "get", 1);
            }

            @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
            public void onActivityResult(int i, int i2, Intent intent) {
                if (i == 1) {
                    if (i2 == -1) {
                        setResult(i2);
                        finish();
                    }
                } else if (i != 2 || i2 != -1) {
                    super.onActivityResult(i, i2, intent);
                } else if (this.A04 != null && this.A09.A03()) {
                    this.A04.A0L(true);
                    this.A04.A01().A00();
                }
            }

            @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
            public void onBackPressed() {
                C48232Fc r0 = this.A08;
                if (r0 == null || !r0.A05()) {
                    super.onBackPressed();
                } else {
                    this.A08.A04(true);
                }
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onCreate(Bundle bundle) {
                C618632v r1;
                super.onCreate(bundle);
                this.A0G = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A0E);
                this.A0L = new AnonymousClass5y1(this, this.A09, 2);
                setContentView(R.layout.novi_withdraw_cash_store_locator);
                PayToolbar A0a = C117305Zk.A0a(this);
                C130305zC.A01(this, ((ActivityC13830kP) this).A01, A0a, getString(R.string.select_a_location), R.drawable.ic_back, false);
                View A0D = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.bottom_sheet);
                this.A01 = A0D;
                this.A02 = AnonymousClass028.A0D(A0D, R.id.location_access_view);
                View A0D2 = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.full_screen);
                View A0D3 = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.my_location);
                ViewGroup A04 = C117315Zl.A04(((ActivityC13810kN) this).A00, R.id.map_holder);
                ProgressBar progressBar = (ProgressBar) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.progressbar_small);
                this.A03 = progressBar;
                progressBar.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.novi_pay_common_spinner_width);
                GoogleMapOptions googleMapOptions = new GoogleMapOptions();
                googleMapOptions.A00 = 1;
                googleMapOptions.A0C = false;
                googleMapOptions.A05 = false;
                googleMapOptions.A08 = true;
                googleMapOptions.A06 = false;
                googleMapOptions.A0A = true;
                googleMapOptions.A09 = true;
                C618632v r0 = new C618632v(this, googleMapOptions);
                this.A0B = r0;
                A04.addView(r0);
                this.A0B.A04(bundle);
                if (C35781ij.A00(this) != 0) {
                    finish();
                    return;
                }
                if (this.A04 == null && (r1 = this.A0B) != null) {
                    this.A04 = r1.A07(new AnonymousClass66U(this));
                }
                this.A05 = C36321jh.A00(BitmapFactory.decodeResource(getResources(), R.drawable.pin_location_green));
                this.A01.setVisibility(0);
                this.A0C = new C92254Vd(getResources(), this.A01, new AnonymousClass5UM() { // from class: X.68U
                    @Override // X.AnonymousClass5UM
                    public final void ATC(float f) {
                        C618632v r2 = NoviWithdrawCashStoreLocatorActivity.this.A0B;
                        if (r2 != null) {
                            r2.setPadding(0, 0, 0, (int) f);
                        }
                    }
                });
                double d = (double) Resources.getSystem().getDisplayMetrics().heightPixels;
                int i = (int) (d * 0.5d);
                A2f(i);
                A0D2.setOnClickListener(new View.OnClickListener(i, (int) (d * 0.25d)) { // from class: X.64J
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ int A01;

                    {
                        this.A00 = r2;
                        this.A01 = r3;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = NoviWithdrawCashStoreLocatorActivity.this;
                        int i2 = this.A00;
                        int i3 = this.A01;
                        boolean z = noviWithdrawCashStoreLocatorActivity.A0M;
                        View view2 = noviWithdrawCashStoreLocatorActivity.A01;
                        if (z) {
                            view2.clearAnimation();
                            noviWithdrawCashStoreLocatorActivity.A01.setVisibility(0);
                            noviWithdrawCashStoreLocatorActivity.A2f(i2);
                        } else {
                            view2.clearAnimation();
                            noviWithdrawCashStoreLocatorActivity.A2f(i3);
                        }
                        noviWithdrawCashStoreLocatorActivity.A0M = !noviWithdrawCashStoreLocatorActivity.A0M;
                    }
                });
                C117295Zj.A0n(A0D3, this, 93);
                C52812bj r12 = new C52812bj(this, this.A0K.A04, false);
                this.A0D = r12;
                r12.registerDataSetObserver(this.A0O);
                ListView listView = (ListView) findViewById(R.id.places_list);
                listView.setAdapter((ListAdapter) this.A0D);
                listView.addHeaderView(getLayoutInflater().inflate(R.layout.location_nearby_places_row, (ViewGroup) null), null, false);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener(listView, this) { // from class: X.65D
                    public final /* synthetic */ ListView A00;
                    public final /* synthetic */ NoviWithdrawCashStoreLocatorActivity A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.widget.AdapterView.OnItemClickListener
                    public final void onItemClick(AdapterView adapterView, View view, int i2, long j) {
                        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = this.A01;
                        int headerViewsCount = i2 - this.A00.getHeaderViewsCount();
                        List list = noviWithdrawCashStoreLocatorActivity.A0D.A01;
                        if (list != null && headerViewsCount >= 0 && headerViewsCount < list.size()) {
                            PlaceInfo placeInfo = (PlaceInfo) noviWithdrawCashStoreLocatorActivity.A0D.A01.get(headerViewsCount);
                            C1316163l r6 = new C1316163l(new C130575zd(placeInfo.A01, placeInfo.A02), placeInfo.A08, placeInfo.A0B, placeInfo.A05, placeInfo.A06);
                            C128365vz r13 = new AnonymousClass610("SELECT_STORE_PIN_CLICK", "WITHDRAW_MONEY", "SELECT_LOCATION", "PIN").A00;
                            r13.A0h = r6.A04;
                            noviWithdrawCashStoreLocatorActivity.A0I.A05(r13);
                            noviWithdrawCashStoreLocatorActivity.A0J.A04().A00(new AnonymousClass6EY(r6, noviWithdrawCashStoreLocatorActivity));
                        }
                    }
                });
                this.A08 = new C48232Fc(this, AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.search_holder), new AnonymousClass65O(this), A0a, ((ActivityC13830kP) this).A01);
                C117295Zj.A0n(this.A02, this, 94);
                AnonymousClass60Y.A02(this.A0I, "NAVIGATION_START", "WITHDRAW_MONEY", "SELECT_LOCATION", "SCREEN");
            }

            @Override // X.ActivityC13790kL, android.app.Activity
            public boolean onCreateOptionsMenu(Menu menu) {
                menu.add(0, 1001, 0, getString(R.string.search)).setIcon(AnonymousClass2GE.A04(getResources().getDrawable(R.drawable.ic_action_search), getResources().getColor(R.color.fb_pay_hub_icon_tint))).setShowAsAction(1);
                menu.add(0, 1002, 0, getString(R.string.refresh)).setIcon(AnonymousClass2GE.A04(getResources().getDrawable(R.drawable.ic_action_refresh), getResources().getColor(R.color.fb_pay_hub_icon_tint))).setShowAsAction(1);
                return true;
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onDestroy() {
                C618632v r0 = this.A0B;
                if (r0 != null) {
                    r0.A00();
                }
                C52812bj r1 = this.A0D;
                if (r1 != null) {
                    r1.unregisterDataSetObserver(this.A0O);
                }
                AnonymousClass60Y.A02(this.A0I, "NAVIGATION_START", "WITHDRAW_MONEY", "SELECT_LOCATION", "SCREEN");
                super.onDestroy();
            }

            @Override // android.location.LocationListener
            public void onLocationChanged(Location location) {
                AnonymousClass3FV r1;
                if (this.A00 == null && (r1 = this.A0A) != null) {
                    r1.A03(location, null, Float.valueOf(15.0f), null, false);
                    A2e(location.getLatitude(), location.getLongitude(), 0.0f);
                }
                if (AnonymousClass13N.A03(location, this.A00)) {
                    this.A00 = location;
                }
            }

            @Override // X.ActivityC13810kN, android.app.Activity
            public boolean onOptionsItemSelected(MenuItem menuItem) {
                AnonymousClass3FV r0;
                int itemId = menuItem.getItemId();
                if (itemId == 1001) {
                    C48232Fc r02 = this.A08;
                    if (r02 != null && !r02.A05()) {
                        this.A08.A01();
                    }
                } else if (itemId == 1002 && (r0 = this.A0A) != null) {
                    Location A01 = r0.A01();
                    A2e(A01.getLatitude(), A01.getLongitude(), (float) this.A0A.A00());
                }
                return super.onOptionsItemSelected(menuItem);
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
            public void onPause() {
                C618632v r0 = this.A0B;
                if (r0 != null) {
                    r0.A02();
                    C618632v r02 = this.A0B;
                    SensorManager sensorManager = r02.A05;
                    if (sensorManager != null) {
                        sensorManager.unregisterListener(r02.A0C);
                    }
                }
                this.A07.A04(this);
                super.onPause();
            }

            @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity, X.AbstractC000300e
            public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
                super.onRequestPermissionsResult(i, strArr, iArr);
                AnonymousClass5y1 r1 = this.A0L;
                if (r1.A01 == i) {
                    for (int i2 = 0; i2 < strArr.length; i2++) {
                        if (iArr[i2] != 0) {
                            if (!r1.A00) {
                                Activity activity = r1.A02;
                                if (!AnonymousClass00T.A0G(activity, strArr[i2])) {
                                    C38211ni.A05(activity);
                                    return;
                                }
                                return;
                            } else {
                                return;
                            }
                        }
                    }
                }
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
            public void onResume() {
                C618632v r1;
                super.onResume();
                C618632v r0 = this.A0B;
                if (r0 != null) {
                    r0.A03();
                    this.A0B.A08();
                }
                if (this.A04 == null && (r1 = this.A0B) != null) {
                    this.A04 = r1.A07(new AnonymousClass66U(this));
                }
                this.A07.A05(this, "store-locator-onresume", 0.0f, 2, 5000, 1000);
                this.A02.setVisibility(C13010iy.A00(this.A09.A03() ? 1 : 0));
            }
        }
