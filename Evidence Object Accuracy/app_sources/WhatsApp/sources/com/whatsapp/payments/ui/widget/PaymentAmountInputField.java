package com.whatsapp.payments.ui.widget;

import X.AbstractC136126Lc;
import X.AbstractC136486Ms;
import X.AbstractC30791Yv;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1IT;
import X.AnonymousClass2P6;
import X.AnonymousClass4EI;
import X.AnonymousClass6HX;
import X.C117335Zn;
import X.C119165d1;
import X.C125375r9;
import X.C12960it;
import X.C12970iu;
import X.C14900mE;
import X.C27531Hw;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import java.math.BigDecimal;
import java.util.HashSet;

/* loaded from: classes4.dex */
public class PaymentAmountInputField extends AnonymousClass1IT {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public ValueAnimator A05;
    public ValueAnimator A06;
    public TextPaint A07;
    public View A08;
    public Animation A09;
    public TextView A0A;
    public C14900mE A0B;
    public AnonymousClass018 A0C;
    public AnonymousClass018 A0D;
    public AbstractC30791Yv A0E;
    public AbstractC136486Ms A0F;
    public AbstractC136126Lc A0G;
    public String A0H;
    public BigDecimal A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public final Runnable A0O;

    public PaymentAmountInputField(Context context) {
        super(context);
        A02();
        this.A0O = new AnonymousClass6HX(this);
        this.A0N = false;
        this.A0J = true;
        A0B(null);
    }

    public PaymentAmountInputField(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A0O = new AnonymousClass6HX(this);
        this.A0N = false;
        this.A0J = true;
        A0B(attributeSet);
    }

    public PaymentAmountInputField(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A0O = new AnonymousClass6HX(this);
        this.A0N = false;
        this.A0J = true;
        A0B(attributeSet);
    }

    public PaymentAmountInputField(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    public static String A00(AnonymousClass018 r2) {
        String A00 = AnonymousClass4EI.A00(r2);
        String str = ",";
        if (A00.contains(str)) {
            str = ".";
        }
        if (str.equals(".")) {
            return "\\.";
        }
        return str;
    }

    @Override // X.AbstractC14290lC
    public void A02() {
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((WaEditText) this).A03 = C12960it.A0R(A00);
            ((WaEditText) this).A02 = C12960it.A0Q(A00);
            this.A0B = C12970iu.A0R(A00);
            this.A0C = C12960it.A0R(A00);
        }
    }

    @Override // X.AnonymousClass1IT
    public float A05(String str) {
        return A08(str, getTextSize());
    }

    @Override // X.AnonymousClass1IT
    public void A07(boolean z) {
        AbstractC136486Ms r1 = this.A0F;
        if (r1 != null) {
            Editable text = getText();
            AnonymousClass009.A05(text);
            r1.AQf(text.toString(), z);
        }
    }

    public final float A08(String str, float f) {
        String str2;
        String str3;
        float f2 = 0.0f;
        if (TextUtils.isEmpty(str)) {
            return 0.0f;
        }
        if (this.A07 == null) {
            TextPaint textPaint = new TextPaint();
            this.A07 = textPaint;
            textPaint.setTypeface(C27531Hw.A03(getContext()));
        }
        int indexOf = TextUtils.indexOf(str, AnonymousClass4EI.A00(this.A0D).charAt(0));
        if (indexOf > 0) {
            str2 = str.substring(0, indexOf);
            str3 = str.substring(indexOf);
        } else {
            str2 = str;
            str3 = "";
        }
        this.A07.setTextSize(f);
        float measureText = this.A07.measureText(str2);
        if (!TextUtils.isEmpty(str3)) {
            this.A07.setTextSize(f * 1.0f);
            f2 = this.A07.measureText(str3);
        }
        return measureText + f2;
    }

    public void A09() {
        if (this.A08 != null) {
            ValueAnimator valueAnimator = this.A05;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            } else {
                float dimensionPixelSize = (float) getResources().getDimensionPixelSize(R.dimen.currency_symbol_wiggle_animation_displacement);
                View view = this.A08;
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                ofFloat.setInterpolator(new AccelerateInterpolator());
                ofFloat.setRepeatCount(3);
                ofFloat.setRepeatMode(2);
                ofFloat.setDuration(65L);
                ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view, dimensionPixelSize) { // from class: X.61t
                    public final /* synthetic */ float A00;
                    public final /* synthetic */ View A01;

                    {
                        this.A01 = r1;
                        this.A00 = r2;
                    }

                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        this.A01.setTranslationX(valueAnimator2.getAnimatedFraction() * this.A00);
                    }
                });
                this.A05 = ofFloat;
                ofFloat.addListener(new C117335Zn(this));
            }
            this.A05.start();
        }
    }

    public final void A0A() {
        String str = "0123456789";
        if (this.A0J) {
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(AnonymousClass4EI.A00(this.A0D).charAt(0));
            str = A0j.toString();
        }
        setKeyListener(DigitsKeyListener.getInstance(str));
    }

    public final void A0B(AttributeSet attributeSet) {
        boolean z = false;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C125375r9.A02);
            boolean z2 = obtainStyledAttributes.getBoolean(0, false);
            this.A0L = obtainStyledAttributes.getBoolean(1, false);
            obtainStyledAttributes.recycle();
            z = z2;
        } else {
            this.A0L = false;
        }
        this.A0D = this.A0C;
        this.A0H = C12970iu.A0p(this);
        this.A02 = -1;
        this.A04 = C12960it.A09(this).getConfiguration().orientation;
        setInputType(2);
        A0A();
        setFilterTouchesWhenObscured(true);
        setCursorVisible(true);
        setFocusable(true);
        setSingleLine(true);
        addTextChangedListener(this);
        setAutoScaleTextSize(z);
        this.A0N = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0079 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0C(java.lang.String r7) {
        /*
            r6 = this;
            float r5 = r6.getTextSize()
            boolean r0 = r6.A0N
            if (r0 == 0) goto L_0x0080
            int r0 = r6.A02
            if (r0 <= 0) goto L_0x0080
            r4 = 0
        L_0x000d:
            float r3 = r6.A00
            r2 = 1065353216(0x3f800000, float:1.0)
            float r1 = (float) r4
            r0 = 1041865114(0x3e19999a, float:0.15)
            float r1 = r1 * r0
            float r2 = r2 - r1
            float r3 = r3 * r2
            float r2 = r6.A08(r7, r3)
            if (r4 != 0) goto L_0x006b
            int r0 = r6.A02
            float r1 = (float) r0
            r0 = 1056964608(0x3f000000, float:0.5)
        L_0x0023:
            float r1 = r1 * r0
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0074
        L_0x0028:
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x006a
            android.animation.ValueAnimator r0 = r6.A06
            if (r0 == 0) goto L_0x003b
            boolean r0 = r0.isRunning()
            if (r0 == 0) goto L_0x003b
            android.animation.ValueAnimator r0 = r6.A06
            r0.cancel()
        L_0x003b:
            r0 = 2
            float[] r1 = new float[r0]
            r0 = 0
            r1[r0] = r5
            r0 = 1
            r1[r0] = r3
            android.animation.ValueAnimator r2 = android.animation.ValueAnimator.ofFloat(r1)
            r6.A06 = r2
            r1 = 1073741824(0x40000000, float:2.0)
            android.view.animation.DecelerateInterpolator r0 = new android.view.animation.DecelerateInterpolator
            r0.<init>(r1)
            r2.setInterpolator(r0)
            android.animation.ValueAnimator r2 = r6.A06
            r0 = 100
            r2.setDuration(r0)
            android.animation.ValueAnimator r1 = r6.A06
            X.61q r0 = new X.61q
            r0.<init>()
            r1.addUpdateListener(r0)
            android.animation.ValueAnimator r0 = r6.A06
            r0.start()
        L_0x006a:
            return
        L_0x006b:
            r0 = 1
            if (r4 != r0) goto L_0x0074
            int r0 = r6.A02
            float r1 = (float) r0
            r0 = 1061158912(0x3f400000, float:0.75)
            goto L_0x0023
        L_0x0074:
            int r4 = r4 + 1
            r0 = 2
            if (r4 <= r0) goto L_0x000d
            float r0 = r6.A01
            float r3 = java.lang.Math.max(r3, r0)
            goto L_0x0028
        L_0x0080:
            float r3 = r6.A00
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.PaymentAmountInputField.A0C(java.lang.String):void");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v10, resolved type: android.text.SpannableStringBuilder */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006c, code lost:
        if (r5 != false) goto L_0x0080;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0067  */
    @Override // X.AnonymousClass1IT, android.text.TextWatcher
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void afterTextChanged(android.text.Editable r11) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.PaymentAmountInputField.afterTextChanged(android.text.Editable):void");
    }

    @Override // X.AnonymousClass1IT, android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!TextUtils.isEmpty(charSequence)) {
            this.A0I = this.A0E.AAD(this.A0D, charSequence.toString());
        }
    }

    @Override // X.AnonymousClass1IT
    public int getCursorColor() {
        return AnonymousClass00T.A00(getContext(), R.color.primary_light);
    }

    @Override // X.AnonymousClass1IT
    public int getCursorVerticalPadding() {
        return getResources().getDimensionPixelSize(R.dimen.pay_amount_cursor_vertical_padding);
    }

    @Override // X.AnonymousClass1IT
    public int getCursorWidth() {
        return getResources().getDimensionPixelSize(R.dimen.pay_amount_cursor_width);
    }

    private ViewGroup getFirstNonWrapContentParent() {
        if (getParent() != null) {
            ViewParent parent = getParent();
            while (true) {
                ViewGroup viewGroup = (ViewGroup) parent;
                if (viewGroup == null) {
                    break;
                } else if (viewGroup.getLayoutParams().width != -2) {
                    return viewGroup;
                } else {
                    parent = viewGroup.getParent();
                }
            }
        }
        return null;
    }

    public AnonymousClass018 getWhatsAppLocale() {
        return this.A0D;
    }

    @Override // android.widget.TextView, android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        int i = configuration.orientation;
        if (i != this.A04) {
            this.A04 = i;
            this.A02 = -1;
        }
        super.onConfigurationChanged(configuration);
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int width;
        int i5;
        super.onLayout(z, i, i2, i3, i4);
        if (this.A0K && this.A02 == -1) {
            if (getLayoutParams().width == -2) {
                ViewGroup firstNonWrapContentParent = getFirstNonWrapContentParent();
                if (firstNonWrapContentParent == null) {
                    width = Resources.getSystem().getDisplayMetrics().widthPixels;
                } else {
                    ViewGroup viewGroup = (ViewGroup) getParent();
                    HashSet A12 = C12970iu.A12();
                    i5 = firstNonWrapContentParent.getWidth();
                    while (!A12.contains(firstNonWrapContentParent)) {
                        for (int i6 = 0; i6 < viewGroup.getChildCount(); i6++) {
                            View childAt = viewGroup.getChildAt(i6);
                            if (childAt != this && !A12.contains(childAt)) {
                                ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(childAt);
                                i5 -= (childAt.getWidth() + A0H.rightMargin) + A0H.leftMargin;
                                A12.add(childAt);
                            }
                        }
                        ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(viewGroup);
                        i5 -= ((A0H2.leftMargin + A0H2.rightMargin) + viewGroup.getPaddingRight()) + viewGroup.getPaddingLeft();
                        A12.add(viewGroup);
                        viewGroup = (ViewGroup) viewGroup.getParent();
                    }
                    this.A02 = i5;
                }
            } else {
                width = getWidth();
            }
            i5 = (width - getCompoundPaddingLeft()) - getCompoundPaddingRight();
            this.A02 = i5;
        }
    }

    @Override // android.widget.TextView
    public void onSelectionChanged(int i, int i2) {
        Editable text = getText();
        if (TextUtils.isEmpty(text) || (i == text.length() && i2 == text.length())) {
            super.onSelectionChanged(i, i2);
        } else {
            setSelection(text.length(), text.length());
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if ((i == 4 || i == 8) && this.A0A != null) {
            this.A0B.A0G(this.A0O);
            this.A09.cancel();
            this.A09.reset();
            this.A0A.setVisibility(8);
        }
    }

    public void setAllowDecimal(boolean z) {
        this.A0J = z;
        A0A();
    }

    public final void setAmount(String str, String str2) {
        if (this.A0L) {
            char charAt = AnonymousClass4EI.A00(this.A0D).charAt(0);
            int indexOf = TextUtils.indexOf(str, charAt);
            AbstractC30791Yv r2 = this.A0E;
            if (r2 != null) {
                AnonymousClass018 r1 = this.A0D;
                this.A0H = r2.AA9(r1, r2.AAD(r1, str2));
            }
            if (indexOf != -1) {
                int indexOf2 = TextUtils.indexOf(this.A0H, charAt);
                StringBuilder A0h = C12960it.A0h();
                String str3 = this.A0H;
                if (indexOf2 != -1) {
                    str3 = str3.substring(0, indexOf2);
                }
                A0h.append(str3);
                str = C12960it.A0d(str.substring(indexOf), A0h);
            } else {
                return;
            }
        }
        this.A0H = str;
    }

    public void setAutoScaleTextSize(boolean z) {
        this.A0K = z;
        if (z) {
            float textSize = getTextSize();
            this.A00 = textSize;
            this.A01 = textSize * 0.7f;
        }
    }

    public void setCurrency(AbstractC30791Yv r1) {
        this.A0E = r1;
    }

    public void setErrorTextView(TextView textView) {
        this.A0A = textView;
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        this.A09 = alphaAnimation;
        alphaAnimation.setDuration(500);
        this.A09.setAnimationListener(new C119165d1(textView, this));
    }

    public void setFormatWithCommas(boolean z) {
        this.A0L = z;
    }

    public void setInputAmountType(int i) {
        this.A03 = i;
    }

    public void setInputAmountValidator(AbstractC136126Lc r1) {
        this.A0G = r1;
    }

    public void setOnAmountChangedLister(AbstractC136486Ms r1) {
        this.A0F = r1;
    }

    public void setPaymentAmountContainer(View view) {
        this.A08 = view;
    }

    @Override // android.widget.TextView
    public void setTextSize(float f) {
        super.setTextSize(f);
        if (this.A0K) {
            float textSize = getTextSize();
            this.A00 = textSize;
            this.A01 = textSize * 0.7f;
            A0C(this.A0H);
        }
    }

    public void setWhatsAppLocale(AnonymousClass018 r1) {
        this.A0D = r1;
    }
}
