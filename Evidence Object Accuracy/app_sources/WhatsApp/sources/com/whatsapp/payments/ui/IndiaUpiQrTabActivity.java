package com.whatsapp.payments.ui;

import X.AbstractActivityC119395dj;
import X.AbstractC005102i;
import X.AbstractC116105Ud;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass4OE;
import X.AnonymousClass69M;
import X.AnonymousClass6BE;
import X.C117875at;
import X.C118885cX;
import X.C124185ok;
import X.C12960it;
import X.C12990iw;
import X.C1309960u;
import X.C1329668y;
import X.C15890o4;
import X.C21860y6;
import X.C22190yg;
import X.C28141Kv;
import X.C35751ig;
import X.C41691tw;
import X.C63883Dh;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import com.whatsapp.PagerSlidingTabStrip;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;
import com.whatsapp.qrcode.WaQrScannerView;

/* loaded from: classes4.dex */
public final class IndiaUpiQrTabActivity extends AbstractActivityC119395dj {
    public static String A0F;
    public ViewPager A00;
    public PagerSlidingTabStrip A01;
    public C15890o4 A02;
    public AnonymousClass69M A03;
    public C1329668y A04;
    public C21860y6 A05;
    public AnonymousClass6BE A06;
    public IndiaUpiMyQrFragment A07;
    public C117875at A08;
    public IndiaUpiScanQrCodeFragment A09;
    public C1309960u A0A;
    public C63883Dh A0B;
    public C22190yg A0C;
    public boolean A0D = false;
    public final AbstractC116105Ud A0E = new AbstractC116105Ud() { // from class: X.6DW
        @Override // X.AbstractC116105Ud
        public final void AVK(String str, int i) {
            IndiaUpiQrTabActivity indiaUpiQrTabActivity = IndiaUpiQrTabActivity.this;
            indiaUpiQrTabActivity.AaN();
            if (!indiaUpiQrTabActivity.AJN()) {
                int i2 = R.string.error_load_image;
                if (i != 0) {
                    i2 = R.string.contact_qr_scan_toast_no_valid_code;
                    if (i != 1) {
                        if (i == 2) {
                            indiaUpiQrTabActivity.Adl(indiaUpiQrTabActivity.A03.AG0(str, "payments_camera_gallery", 9), "GALLERY_QR_CODE");
                            return;
                        }
                        return;
                    }
                }
                String string = indiaUpiQrTabActivity.getString(i2);
                C004802e A0S = C12980iv.A0S(indiaUpiQrTabActivity);
                C12970iu.A1I(A0S);
                A0S.A0A(string);
                C12970iu.A1J(A0S);
            }
        }
    };

    @Override // X.ActivityC13810kN, X.ActivityC000900k
    public void A1T(AnonymousClass01E r2) {
        super.A1T(r2);
        if (r2 instanceof IndiaUpiMyQrFragment) {
            this.A07 = (IndiaUpiMyQrFragment) r2;
        } else if (r2 instanceof IndiaUpiScanQrCodeFragment) {
            this.A09 = (IndiaUpiScanQrCodeFragment) r2;
        }
    }

    public void A2e() {
        int A02 = this.A02.A02("android.permission.CAMERA");
        IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment = this.A09;
        WaQrScannerView waQrScannerView = indiaUpiScanQrCodeFragment.A07;
        if (A02 != 0) {
            waQrScannerView.setVisibility(8);
            indiaUpiScanQrCodeFragment.A00.setVisibility(8);
            indiaUpiScanQrCodeFragment.A01.setVisibility(0);
            C35751ig r5 = new C35751ig(this);
            r5.A01 = R.drawable.permission_cam;
            int[] iArr = {R.string.localized_app_name};
            r5.A02 = R.string.permission_cam_access_on_scan_payment_qr;
            r5.A0A = iArr;
            int[] iArr2 = {R.string.localized_app_name};
            r5.A03 = R.string.permission_cam_access_on_scan_payment_qr_perm_denied;
            r5.A08 = iArr2;
            r5.A0C = new String[]{"android.permission.CAMERA"};
            r5.A06 = true;
            A2E(r5.A00(), 1);
            return;
        }
        waQrScannerView.setVisibility(0);
        indiaUpiScanQrCodeFragment.A00.setVisibility(0);
        indiaUpiScanQrCodeFragment.A01.setVisibility(8);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            if (i2 != 0) {
                IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment = this.A09;
                indiaUpiScanQrCodeFragment.A07.setVisibility(0);
                indiaUpiScanQrCodeFragment.A00.setVisibility(0);
                indiaUpiScanQrCodeFragment.A01.setVisibility(8);
            } else if (this.A08.A00.length == 2) {
                this.A00.A0F(C28141Kv.A01(((ActivityC13830kP) this).A01) ? 1 : 0, true);
            } else {
                finish();
            }
        } else if (i == 202) {
            if (i2 == -1) {
                this.A07.A19();
            }
        } else if (i == 203 && i2 == -1 && intent != null) {
            Uri data = intent.getData();
            if (data != null) {
                A2C(R.string.register_wait_message);
                C12990iw.A1N(new C124185ok(data, this, this.A0C, this.A09.A07.getWidth(), this.A09.A07.getHeight()), ((ActivityC13830kP) this).A05);
                return;
            }
            ((ActivityC13810kN) this).A05.A07(R.string.error_load_image, 0);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C117875at r1;
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        super.onCreate(bundle);
        getWindow().addFlags(128);
        setContentView(R.layout.india_upi_qr_tab);
        this.A0B = new C63883Dh();
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.menuitem_scan_qr);
            A1U.A0M(true);
        }
        A0F = getIntent().getStringExtra("extra_account_holder_name");
        AbstractC005102i A1U2 = A1U();
        AnonymousClass009.A05(A1U2);
        A1U2.A0M(true);
        this.A00 = (ViewPager) findViewById(R.id.payment_qr_pager);
        this.A01 = (PagerSlidingTabStrip) findViewById(R.id.payment_qr_tab_strip);
        if (this.A05.A0A()) {
            this.A01.setVisibility(0);
            if (A1U != null) {
                A1U.A0A(R.string.qr_code_action_bar_text);
            }
            r1 = new C117875at(A0V(), this, 2);
        } else {
            this.A01.setVisibility(8);
            r1 = new C117875at(A0V(), this, 1);
        }
        this.A08 = r1;
        this.A00.setAdapter(r1);
        this.A00.A0G(new C118885cX(this));
        AnonymousClass028.A0c(this.A01, 0);
        this.A01.setViewPager(this.A00);
        this.A00.A0F(0, false);
        C117875at r3 = this.A08;
        int i = 0;
        while (true) {
            AnonymousClass4OE[] r12 = r3.A00;
            if (i < r12.length) {
                AnonymousClass4OE r0 = r12[i];
                r0.A00.setSelected(C12960it.A1V(i, 0));
                i++;
            } else {
                this.A03 = new AnonymousClass69M(((ActivityC13810kN) this).A06, this.A04, this.A06);
                return;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0B.A01(getWindow(), ((ActivityC13810kN) this).A08);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        this.A0B.A00(getWindow());
        super.onStop();
    }
}
