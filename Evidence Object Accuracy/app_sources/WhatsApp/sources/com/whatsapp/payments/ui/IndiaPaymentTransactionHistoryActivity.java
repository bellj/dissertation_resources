package com.whatsapp.payments.ui;

import X.AbstractActivityC119305dV;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;

/* loaded from: classes4.dex */
public class IndiaPaymentTransactionHistoryActivity extends PaymentTransactionHistoryActivity {
    public boolean A00;

    public IndiaPaymentTransactionHistoryActivity() {
        this(0);
    }

    public IndiaPaymentTransactionHistoryActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 29);
    }

    @Override // X.AbstractActivityC119305dV, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119305dV.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
        }
    }
}
