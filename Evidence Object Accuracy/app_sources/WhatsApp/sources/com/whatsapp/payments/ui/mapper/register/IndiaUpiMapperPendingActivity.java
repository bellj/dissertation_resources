package com.whatsapp.payments.ui.mapper.register;

import X.AbstractActivityC119335dY;
import X.ActivityC13790kL;
import X.AnonymousClass6BE;
import X.C124935qP;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C16700pc;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiProfileDetailsActivity;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperPendingActivity;

/* loaded from: classes2.dex */
public final class IndiaUpiMapperPendingActivity extends AbstractActivityC119335dY {
    public AnonymousClass6BE A00;

    public static /* synthetic */ void A02(IndiaUpiMapperPendingActivity indiaUpiMapperPendingActivity) {
        C16700pc.A0E(indiaUpiMapperPendingActivity, 0);
        AnonymousClass6BE r5 = indiaUpiMapperPendingActivity.A00;
        if (r5 != null) {
            r5.AKg(1, 129, "pending_alias_setup", ActivityC13790kL.A0W(indiaUpiMapperPendingActivity));
            Intent A0D = C12990iw.A0D(indiaUpiMapperPendingActivity, IndiaUpiProfileDetailsActivity.class);
            A0D.addFlags(67108864);
            indiaUpiMapperPendingActivity.A2G(A0D, true);
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    public static /* synthetic */ void A03(IndiaUpiMapperPendingActivity indiaUpiMapperPendingActivity) {
        C16700pc.A0E(indiaUpiMapperPendingActivity, 0);
        AnonymousClass6BE r4 = indiaUpiMapperPendingActivity.A00;
        if (r4 != null) {
            r4.AKg(C12960it.A0V(), 121, "pending_alias_setup", ActivityC13790kL.A0W(indiaUpiMapperPendingActivity));
            indiaUpiMapperPendingActivity.setResult(-1);
            indiaUpiMapperPendingActivity.finish();
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass6BE r3 = this.A00;
        if (r3 != null) {
            Integer A0V = C12960it.A0V();
            r3.AKg(A0V, A0V, "pending_alias_setup", ActivityC13790kL.A0W(this));
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String stringExtra;
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
        setContentView(R.layout.india_upi_mapper_pending_activity);
        C124935qP.A00(this);
        View findViewById = findViewById(R.id.mapper_pending_done_button);
        View findViewById2 = findViewById(R.id.mapper_pending_profile_button);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: X.3lX
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                IndiaUpiMapperPendingActivity.A03(IndiaUpiMapperPendingActivity.this);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: X.3lW
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                IndiaUpiMapperPendingActivity.A02(IndiaUpiMapperPendingActivity.this);
            }
        });
        AnonymousClass6BE r4 = this.A00;
        if (r4 != null) {
            Integer A0i = C12980iv.A0i();
            Intent intent = getIntent();
            if (intent == null) {
                stringExtra = null;
            } else {
                stringExtra = intent.getStringExtra("extra_referral_screen");
            }
            r4.AKg(A0i, null, "pending_alias_setup", stringExtra);
            return;
        }
        throw C16700pc.A06("indiaUpiFieldStatsLogger");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        C16700pc.A0E(menuItem, 0);
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass6BE r4 = this.A00;
            if (r4 != null) {
                r4.AKg(C12960it.A0V(), C12970iu.A0h(), "pending_alias_setup", ActivityC13790kL.A0W(this));
            } else {
                throw C16700pc.A06("indiaUpiFieldStatsLogger");
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
