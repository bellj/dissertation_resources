package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.AnonymousClass2FL;
import X.AnonymousClass3AX;
import X.C117295Zj;
import X.C117305Zk;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public final class IndiaUpiInteropSendToUpiActivity extends AbstractActivityC121665jA {
    public AnonymousClass18T A00;
    public AnonymousClass18S A01;
    public boolean A02;

    public IndiaUpiInteropSendToUpiActivity() {
        this(0);
    }

    public IndiaUpiInteropSendToUpiActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 47);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A01 = (AnonymousClass18S) A1M.AEw.get();
            this.A00 = (AnonymousClass18T) A1M.AE9.get();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_interop_vpa_send_to_upi);
        A1e(C117305Zk.A08(this));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.new_payment);
        }
        View findViewById = findViewById(R.id.send_to_upi_container);
        AnonymousClass3AX.A00(findViewById, R.drawable.ic_send_to_upi, 0, R.drawable.grey_circle_stroke, R.string.send_payment_to_vpa);
        C117295Zj.A0n(findViewById, this, 41);
    }
}
