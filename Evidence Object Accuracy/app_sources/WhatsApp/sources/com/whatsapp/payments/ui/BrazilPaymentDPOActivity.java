package com.whatsapp.payments.ui;

import X.AbstractActivityC119325dX;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass1US;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C123415n7;
import X.C12960it;
import X.C12990iw;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentDPOActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes4.dex */
public class BrazilPaymentDPOActivity extends AbstractActivityC119325dX {
    public C123415n7 A00;
    public List A01;
    public boolean A02;

    public BrazilPaymentDPOActivity() {
        this(0);
    }

    public BrazilPaymentDPOActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 19);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C123415n7) A09.A04.get();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: android.widget.CheckBox[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AbstractActivityC119325dX
    public void A2e() {
        super.A2e();
        AnonymousClass00T.A05(this, R.id.warning).setVisibility(8);
        ((AbstractActivityC119325dX) this).A05.setVisibility(8);
        AnonymousClass00T.A05(this, R.id.conditions_container).setVisibility(0);
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.condition_relocated_checkbox);
        textView.setText(R.string.restore_payments_condition_relocated);
        TextView textView2 = (TextView) AnonymousClass00T.A05(this, R.id.condition_travelled_checkbox);
        textView2.setText(R.string.restore_payments_condition_travelled);
        TextView textView3 = (TextView) AnonymousClass00T.A05(this, R.id.condition_foreign_method_checkbox);
        textView3.setText(R.string.restore_payments_condition_foreign_method);
        CheckBox[] checkBoxArr = new CheckBox[3];
        C12990iw.A1P(textView, textView2, checkBoxArr);
        checkBoxArr[2] = textView3;
        List<TextView> asList = Arrays.asList(checkBoxArr);
        this.A01 = asList;
        C123415n7 r2 = this.A00;
        ArrayList A0l = C12960it.A0l();
        for (TextView textView4 : asList) {
            A0l.add(textView4.getText().toString());
        }
        r2.A05.A01("list_of_conditions", AnonymousClass1US.A09("|", (CharSequence[]) A0l.toArray(new String[0])));
        for (CompoundButton compoundButton : this.A01) {
            compoundButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.65G
                @Override // android.widget.CompoundButton.OnCheckedChangeListener
                public final void onCheckedChanged(CompoundButton compoundButton2, boolean z) {
                    C123415n7 r22 = BrazilPaymentDPOActivity.this.A00;
                    String charSequence = compoundButton2.getText().toString();
                    AnonymousClass3FW A0S = C117305Zk.A0S();
                    C117325Zm.A07(A0S);
                    A0S.A01("checkbox_text", charSequence);
                    AbstractC16870pt r23 = r22.A06;
                    Integer A0V = C12960it.A0V();
                    int i = 123;
                    if (z) {
                        i = 122;
                    }
                    r23.AKi(A0S, A0V, Integer.valueOf(i), "restore_payment", null);
                }
            });
        }
        C117295Zj.A0n(((AbstractActivityC119325dX) this).A01, this, 10);
    }
}
