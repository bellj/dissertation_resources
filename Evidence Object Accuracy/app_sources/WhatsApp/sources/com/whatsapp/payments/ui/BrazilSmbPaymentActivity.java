package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C18620sk;

/* loaded from: classes4.dex */
public class BrazilSmbPaymentActivity extends BrazilPaymentActivity {
    public boolean A00;

    public BrazilSmbPaymentActivity() {
        this(0);
    }

    public BrazilSmbPaymentActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 24);
    }

    @Override // X.AbstractActivityC121435hw, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1W(A1M, this);
            AbstractActivityC119235dO.A1X(A1M, this);
            AbstractActivityC119235dO.A1R(A09, A1M, (C18620sk) A1M.AFB.get(), this);
        }
    }
}
