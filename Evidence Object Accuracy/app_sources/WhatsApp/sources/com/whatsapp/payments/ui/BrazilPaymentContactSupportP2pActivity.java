package com.whatsapp.payments.ui;

import X.AbstractActivityC119325dX;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C123445nA;

/* loaded from: classes4.dex */
public class BrazilPaymentContactSupportP2pActivity extends AbstractActivityC119325dX {
    public C123445nA A00;
    public boolean A01;

    public BrazilPaymentContactSupportP2pActivity() {
        this(0);
    }

    public BrazilPaymentContactSupportP2pActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 18);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C123445nA) A09.A02.get();
        }
    }

    @Override // X.AbstractActivityC119325dX
    public void A2f() {
        super.A2f();
        C117295Zj.A0r(this, this.A00.A00, 23);
    }
}
