package com.whatsapp.payments.ui.orderdetails;

import X.AnonymousClass028;
import X.C117295Zj;
import X.C118585c3;
import X.C121955kg;
import X.C121965kh;
import X.C125955s5;
import X.C127005tn;
import X.C127725ux;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1315763h;
import X.C247116o;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes4.dex */
public class PaymentOptionsBottomSheet extends Hilt_PaymentOptionsBottomSheet {
    public RecyclerView A00;
    public C247116o A01;
    public C127725ux A02;
    public String A03;
    public List A04;

    public static PaymentOptionsBottomSheet A00(String str, List list) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("selected_payment_method", str);
        A0D.putParcelableArrayList("payment_method_list", C12980iv.A0x(list));
        PaymentOptionsBottomSheet paymentOptionsBottomSheet = new PaymentOptionsBottomSheet();
        paymentOptionsBottomSheet.A0U(A0D);
        return paymentOptionsBottomSheet;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putString("selected_payment_method", this.A03);
        bundle.putParcelableArrayList("payment_method_list", C12980iv.A0x(this.A04));
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_checkout_order_details_payment_options_bottom_sheet);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A02 = null;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        if (bundle == null) {
            this.A03 = A03().getString("selected_payment_method", "WhatsappPay");
            bundle = A03();
        } else {
            this.A03 = bundle.getString("selected_payment_method", "WhatsappPay");
        }
        this.A04 = bundle.getParcelableArrayList("payment_method_list");
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.close), this, 131);
        C118585c3 r7 = new C118585c3();
        String str = this.A03;
        List<C1315763h> list = this.A04;
        C125955s5 r0 = new C125955s5(this);
        C247116o r6 = this.A01;
        r7.A00 = str;
        List list2 = r7.A01;
        list2.clear();
        C127005tn r4 = new C127005tn(r0, r7);
        list2.add(new C121965kh(r4, "WhatsappPay".equals(str)));
        for (C1315763h r2 : list) {
            list2.add(new C121955kg(r6, r2, r4, str.equals(r2.A06)));
        }
        RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(view, R.id.payment_option_recycler_view);
        this.A00 = recyclerView;
        recyclerView.setAdapter(r7);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.continue_button), this, 132);
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            dialog.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.632
                @Override // android.content.DialogInterface.OnShowListener
                public final void onShow(DialogInterface dialogInterface) {
                    View findViewById = ((Dialog) dialogInterface).findViewById(R.id.design_bottom_sheet);
                    AnonymousClass009.A03(findViewById);
                    BottomSheetBehavior A00 = BottomSheetBehavior.A00(findViewById);
                    A00.A0M(3);
                    A00.A0L(findViewById.getHeight());
                }
            });
        }
    }
}
