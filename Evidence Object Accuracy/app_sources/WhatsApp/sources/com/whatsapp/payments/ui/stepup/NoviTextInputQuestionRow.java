package com.whatsapp.payments.ui.stepup;

import X.AnonymousClass028;
import X.C12960it;
import X.C42941w9;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;

/* loaded from: classes4.dex */
public class NoviTextInputQuestionRow extends LinearLayout {
    public int A00;
    public View A01;
    public View A02;
    public TextInputLayout A03;
    public TextInputLayout A04;
    public WaEditText A05;
    public WaTextView A06;
    public WaTextView A07;
    public NoviTextInputSpinner A08;

    public NoviTextInputQuestionRow(Context context) {
        super(context);
        A00();
    }

    public NoviTextInputQuestionRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public NoviTextInputQuestionRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        C12960it.A0E(this).inflate(R.layout.novi_text_input_question_row, (ViewGroup) this, true);
        this.A02 = AnonymousClass028.A0D(this, R.id.novi_text_input_text_question_container);
        this.A07 = C12960it.A0N(this, R.id.novi_text_input_text_question_title);
        this.A04 = (TextInputLayout) AnonymousClass028.A0D(this, R.id.novi_text_input_text_question_input_layout);
        this.A05 = (WaEditText) AnonymousClass028.A0D(this, R.id.novi_text_input_text_question_answer);
        this.A01 = AnonymousClass028.A0D(this, R.id.novi_text_input_dropdown_question_container);
        this.A06 = C12960it.A0N(this, R.id.novi_text_input_dropdown_question_title);
        this.A03 = (TextInputLayout) AnonymousClass028.A0D(this, R.id.novi_text_input_dropdown_question_answer_text);
        this.A08 = (NoviTextInputSpinner) AnonymousClass028.A0D(this, R.id.novi_text_input_dropdown_question_answer_spinner);
        C42941w9.A04(this.A05);
        C42941w9.A04(this.A03);
    }
}
