package com.whatsapp.payments.ui;

import X.AbstractC136366Mg;
import X.AbstractC28901Pl;
import X.AbstractC30781Yu;
import X.AbstractC30791Yv;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass1J1;
import X.AnonymousClass60Y;
import X.AnonymousClass6F2;
import X.AnonymousClass6M1;
import X.AnonymousClass6N0;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C125685re;
import X.C128095vY;
import X.C12960it;
import X.C12970iu;
import X.C130105yo;
import X.C1315863i;
import X.C15370n3;
import X.C15610nY;
import X.C17070qD;
import X.C20830wO;
import X.C21270x9;
import X.C28391Mz;
import X.C30821Yy;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.NoviConfirmPaymentFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentDescriptionRow;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.math.BigDecimal;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviConfirmPaymentFragment extends Hilt_NoviConfirmPaymentFragment implements AbstractC136366Mg, AnonymousClass6M1 {
    public C15610nY A00;
    public AnonymousClass1J1 A01;
    public C21270x9 A02;
    public AnonymousClass018 A03;
    public C20830wO A04;
    public AbstractC28901Pl A05;
    public AbstractC28901Pl A06;
    public UserJid A07;
    public C17070qD A08;
    public C125685re A09;
    public C128095vY A0A;
    public AnonymousClass60Y A0B;
    public C130105yo A0C;
    public AnonymousClass6N0 A0D;
    public PaymentDescriptionRow A0E;
    public PaymentMethodRow A0F;
    public String A0G;

    @Override // X.AbstractC136366Mg
    public boolean A6x() {
        return true;
    }

    public static NoviConfirmPaymentFragment A00(AbstractC28901Pl r3, AbstractC28901Pl r4, UserJid userJid, String str) {
        NoviConfirmPaymentFragment noviConfirmPaymentFragment = new NoviConfirmPaymentFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("arg_jid", userJid);
        A0D.putParcelable("arg_payment_primary_method", r3);
        A0D.putParcelable("arg_payment_secondary_method", r4);
        A0D.putString("arg_transaction_draft", str);
        noviConfirmPaymentFragment.A0U(A0D);
        return noviConfirmPaymentFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass60Y.A01(this.A0B, "NAVIGATION_START", "SEND_MONEY", "REVIEW_TRANSACTION", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        BigDecimal bigDecimal;
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.novi_confirm_payment_fragment);
        C117295Zj.A0n(AnonymousClass028.A0D(A0F, R.id.send_money_review_header_close), this, 78);
        View A0D = AnonymousClass028.A0D(A0F, R.id.novi_send_money_review_contact);
        C12960it.A0I(A0D, R.id.novi_send_money_review_contact_action).setText(R.string.novi_send_money_review_action_send_label);
        TextView A0I = C12960it.A0I(A0D, R.id.novi_send_money_review_contact_name);
        C15370n3 A01 = this.A04.A01(this.A07);
        A0I.setText(this.A00.A04(A01));
        this.A01.A06(C12970iu.A0K(A0D, R.id.novi_send_money_review_contact_photo), A01);
        View A0D2 = AnonymousClass028.A0D(A0F, R.id.novi_send_money_review_transaction_summary);
        AnonymousClass01E r7 = super.A0D;
        C117295Zj.A0o(A0D2, this, r7, 20);
        View A0D3 = AnonymousClass028.A0D(A0F, R.id.novi_send_money_payment_description_container);
        C117295Zj.A0o(A0D3, this, r7, 19);
        PaymentDescriptionRow paymentDescriptionRow = (PaymentDescriptionRow) AnonymousClass028.A0D(A0D3, R.id.novi_send_money_payment_description_row);
        this.A0E = paymentDescriptionRow;
        paymentDescriptionRow.A01(this.A0G);
        AnonymousClass018 r8 = this.A03;
        TextView A0I2 = C12960it.A0I(A0D2, R.id.novi_send_money_review_transaction_receiver_crypto);
        AnonymousClass6F2 r0 = this.A0A.A05.A03.A01.A02;
        AbstractC30791Yv r9 = r0.A00;
        A0I2.setText(r9.AA7(A0D2.getContext(), C12970iu.A0q(this, r9.AAA(r8, r0.A01, 0), new Object[1], 0, R.string.novi_send_money_review_transaction_receiver_crypto)));
        TextView A0I3 = C12960it.A0I(A0D2, R.id.novi_send_money_review_transaction_receiver_fiat);
        AnonymousClass6F2 r02 = this.A0A.A05.A03.A01.A01;
        A0I3.setVisibility(0);
        AbstractC30791Yv r92 = r02.A00;
        A0I3.setText(r92.AA7(A0D2.getContext(), C12970iu.A0q(this, r92.AAA(r8, r02.A01, 1), new Object[1], 0, R.string.novi_conversion_summary)));
        TextView A0I4 = C12960it.A0I(A0D2, R.id.novi_send_money_review_transaction_exchange_rate);
        A0I4.setVisibility(0);
        C1315863i r2 = this.A0A.A04;
        A0I4.setText(r2.A06.AHI(A01(), r8, r2));
        View A0D4 = AnonymousClass028.A0D(A0F, R.id.novi_send_money_payment_method_container);
        C117295Zj.A0o(A0D4, this, r7, 21);
        PaymentMethodRow paymentMethodRow = (PaymentMethodRow) AnonymousClass028.A0D(A0D4, R.id.novi_send_money_payment_method_row);
        this.A0F = paymentMethodRow;
        A19(this.A06, this.A0A.A03.A01, paymentMethodRow);
        View A0D5 = AnonymousClass028.A0D(A0F, R.id.novi_send_money_review_extras);
        AnonymousClass6F2 r03 = this.A0A.A05.A05.A00.A02;
        C30821Yy r11 = r03.A01;
        AbstractC30791Yv r93 = r03.A00;
        C12960it.A0I(A0D5, R.id.novi_send_money_review_extras_sender_amount).setText(C117305Zk.A0e(A0p(), this.A03, r93, r11, 0));
        AnonymousClass6F2 r04 = this.A0A.A05.A02.A02;
        if (r04 != null) {
            bigDecimal = r04.A01.A00;
        } else {
            bigDecimal = BigDecimal.ZERO;
        }
        TextView A0I5 = C12960it.A0I(A0D5, R.id.novi_send_money_review_extras_fee_amount);
        Context A0p = A0p();
        AnonymousClass018 r1 = this.A03;
        int i = ((AbstractC30781Yu) r93).A01;
        A0I5.setText(C117305Zk.A0e(A0p, r1, r93, new C30821Yy(bigDecimal, i), 0));
        C12960it.A0I(A0D5, R.id.novi_send_money_review_extras_total_amount).setText(C117305Zk.A0e(A0p(), this.A03, r93, new C30821Yy(r11.A00.add(bigDecimal), i), 0));
        ActivityC000900k A0C = A0C();
        View A0D6 = AnonymousClass028.A0D(A0F, R.id.novi_send_money_container);
        Button button = (Button) AnonymousClass028.A0D(A0F, R.id.novi_send_money_confirm_payment);
        ProgressBar progressBar = (ProgressBar) AnonymousClass028.A0D(A0F, R.id.novi_send_money_confirm_payment_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(AnonymousClass00T.A00(A0C, R.color.white), PorterDuff.Mode.SRC_IN);
        if (C28391Mz.A02()) {
            A0D6.setElevation(A02().getDimension(R.dimen.novi_pay_button_elevation));
        }
        button.setText(R.string.payments_send_money);
        button.setEnabled(true);
        button.setOnClickListener(new View.OnClickListener(button, progressBar, this) { // from class: X.64H
            public final /* synthetic */ Button A00;
            public final /* synthetic */ ProgressBar A01;
            public final /* synthetic */ NoviConfirmPaymentFragment A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NoviConfirmPaymentFragment noviConfirmPaymentFragment = this.A02;
                Button button2 = this.A00;
                ProgressBar progressBar2 = this.A01;
                AnonymousClass60Y r94 = noviConfirmPaymentFragment.A0B;
                AnonymousClass610 A03 = AnonymousClass610.A03("TRANSACTION_SUBMIT_CLICK", "SEND_MONEY", "REVIEW_TRANSACTION");
                String A0I6 = noviConfirmPaymentFragment.A0I(R.string.payments_send_money);
                C128365vz r6 = A03.A00;
                r6.A0L = A0I6;
                C128095vY r5 = noviConfirmPaymentFragment.A0A;
                A03.A05(r5.A04, r5.A03.A01, r5.A05, r5.A00);
                C128365vz.A01(r6, noviConfirmPaymentFragment.A0G);
                r94.A06(r6);
                if (noviConfirmPaymentFragment.A0D != null) {
                    button2.setVisibility(8);
                    progressBar2.setVisibility(0);
                    AnonymousClass1ZO A0F2 = C117305Zk.A0F(noviConfirmPaymentFragment.A07, noviConfirmPaymentFragment.A08);
                    if (A0F2 != null) {
                        A0F2.A09(1);
                    }
                    noviConfirmPaymentFragment.A0D.AOW(view, progressBar2, A0F2, noviConfirmPaymentFragment.A05, (PaymentBottomSheet) ((AnonymousClass01E) noviConfirmPaymentFragment).A0D);
                }
            }
        });
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        AnonymousClass1J1 r0 = this.A01;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        super.A0s();
        AnonymousClass60Y.A01(this.A0B, "NAVIGATION_END", "SEND_MONEY", "REVIEW_TRANSACTION", "SCREEN");
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        Object obj;
        super.A16(bundle);
        this.A07 = (UserJid) C117315Zl.A03(A03(), "arg_jid");
        this.A05 = (AbstractC28901Pl) C117315Zl.A03(A03(), "arg_payment_primary_method");
        this.A06 = (AbstractC28901Pl) A03().getParcelable("arg_payment_secondary_method");
        C125685re r3 = this.A09;
        String string = A03().getString("arg_transaction_draft");
        synchronized (r3) {
            HashMap hashMap = r3.A00;
            obj = hashMap.get(string);
            hashMap.remove(string);
        }
        AnonymousClass009.A05(obj);
        C128095vY r0 = (C128095vY) obj;
        this.A0A = r0;
        this.A0G = r0.A01;
        C125685re r1 = this.A09;
        synchronized (r1) {
            r1.A00.clear();
        }
        this.A01 = this.A02.A04(A0C(), "novi-confirm-payment-fragment");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0032, code lost:
        if (r7.A01.A01.A02() == false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A19(X.AbstractC28901Pl r6, X.C1316263m r7, com.whatsapp.payments.ui.widget.PaymentMethodRow r8) {
        /*
            r5 = this;
            X.5yo r0 = r5.A0C
            int r1 = X.C130105yo.A00(r0)
            android.widget.ImageView r0 = r8.A01
            r0.setImageResource(r1)
            if (r6 == 0) goto L_0x0077
            r4 = 0
            int r1 = r6.A04()
            r3 = 1
            if (r1 == r3) goto L_0x006b
            r0 = 4
            if (r1 == r0) goto L_0x006b
            r0 = 2
            if (r1 != r0) goto L_0x0026
            android.content.Context r1 = r5.A01()
            r0 = r6
            X.1Zc r0 = (X.C30861Zc) r0
            java.lang.String r4 = X.C1311161i.A03(r1, r0)
        L_0x0026:
            r2 = 0
            if (r7 == 0) goto L_0x0034
            X.6F2 r0 = r7.A01
            X.1Yy r0 = r0.A01
            boolean r1 = r0.A02()
            r0 = 1
            if (r1 != 0) goto L_0x0035
        L_0x0034:
            r0 = 0
        L_0x0035:
            if (r4 == 0) goto L_0x0077
            if (r0 == 0) goto L_0x0042
            r1 = 2131889952(0x7f120f20, float:1.9414582E38)
            java.lang.Object[] r0 = new java.lang.Object[r3]
            java.lang.String r4 = X.C12970iu.A0q(r5, r4, r0, r2, r1)
        L_0x0042:
            android.widget.TextView r0 = r8.A05
            r0.setText(r4)
            android.widget.TextView r1 = r8.A02
            r0 = 0
            r1.setVisibility(r0)
            r0 = 2131889958(0x7f120f26, float:1.9414594E38)
            java.lang.String r1 = r5.A0I(r0)
            android.widget.TextView r0 = r8.A02
            r0.setText(r1)
            r0 = 2131361870(0x7f0a004e, float:1.8343505E38)
            X.C117305Zk.A15(r8, r0)
            X.6N0 r1 = r5.A0D
            if (r1 == 0) goto L_0x006a
            if (r6 == 0) goto L_0x006a
            com.whatsapp.payments.ui.widget.PaymentMethodRow r0 = r5.A0F
            r1.ATZ(r6, r0)
        L_0x006a:
            return
        L_0x006b:
            android.content.Context r1 = r5.A01()
            r0 = r6
            X.1Ze r0 = (X.C30881Ze) r0
            java.lang.String r4 = X.C1311161i.A05(r1, r0)
            goto L_0x0026
        L_0x0077:
            r0 = 2131889951(0x7f120f1f, float:1.941458E38)
            java.lang.String r4 = r5.A0I(r0)
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviConfirmPaymentFragment.A19(X.1Pl, X.63m, com.whatsapp.payments.ui.widget.PaymentMethodRow):void");
    }

    @Override // X.AbstractC136366Mg
    public void ATU(String str) {
        this.A0G = str;
        this.A0E.A01(str);
        AnonymousClass6N0 r0 = this.A0D;
        if (r0 != null) {
            r0.AXn(str);
        }
    }

    @Override // X.AnonymousClass6M1
    public void ATY(AbstractC28901Pl r3) {
        this.A06 = r3;
        A19(r3, this.A0A.A03.A01, this.A0F);
    }
}
