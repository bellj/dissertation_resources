package com.whatsapp.payments.ui.stepup;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass14X;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117325Zm;
import X.C117515a5;
import X.C118125bJ;
import X.C118365bh;
import X.C118525bx;
import X.C118555c0;
import X.C126065sH;
import X.C127415uS;
import X.C128365vz;
import X.C128375w0;
import X.C12960it;
import X.C129675y7;
import X.C12970iu;
import X.C129865yQ;
import X.C12990iw;
import X.C130155yt;
import X.C1316663q;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C17070qD;
import X.C21270x9;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.payments.ui.stepup.NoviTextInputStepUpActivity;
import org.wawebrtc.MediaCodecVideoEncoder;

/* loaded from: classes4.dex */
public class NoviTextInputStepUpActivity extends ActivityC13790kL {
    public int A00;
    public View A01;
    public LinearLayout A02;
    public LinearLayout A03;
    public ProgressBar A04;
    public ScrollView A05;
    public RecyclerView A06;
    public RecyclerView A07;
    public ShimmerFrameLayout A08;
    public WaButton A09;
    public C15550nR A0A;
    public C15610nY A0B;
    public C21270x9 A0C;
    public AnonymousClass018 A0D;
    public AnonymousClass61M A0E;
    public C17070qD A0F;
    public C130155yt A0G;
    public C129865yQ A0H;
    public AnonymousClass60Y A0I;
    public C1316663q A0J;
    public C129675y7 A0K;
    public C128375w0 A0L;
    public C118125bJ A0M;
    public AnonymousClass14X A0N;
    public String A0O;
    public boolean A0P;

    public NoviTextInputStepUpActivity() {
        this(0);
    }

    public NoviTextInputStepUpActivity(int i) {
        this.A0P = false;
        C117295Zj.A0p(this, 118);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0P) {
            this.A0P = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0L = C117315Zl.A0E(A1M);
            this.A0K = (C129675y7) A1M.AKB.get();
            this.A0G = (C130155yt) A1M.ADA.get();
            this.A0D = C12960it.A0R(A1M);
            this.A0A = C12960it.A0O(A1M);
            this.A0C = C12970iu.A0W(A1M);
            this.A0B = C12960it.A0P(A1M);
            this.A0N = (AnonymousClass14X) A1M.AFM.get();
            this.A0F = C117305Zk.A0P(A1M);
            this.A0I = C117305Zk.A0W(A1M);
            this.A0E = C117305Zk.A0N(A1M);
        }
    }

    public final void A2e(AnonymousClass610 r5) {
        AnonymousClass60Y r3 = this.A0I;
        C128365vz r2 = r5.A00;
        r2.A0g = "STEP_UP_MANUAL";
        C1316663q r1 = this.A0J;
        r2.A0E = r1.A02;
        r2.A0f = r1.A03;
        r2.A0D = this.A0O;
        r3.A05(r2);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C127415uS.A00(this.A0J, this.A0K, "CANCELED", this.A00);
        setResult(0);
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_text_input_stepup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.text_input_step_up_toolbar);
        Drawable A04 = AnonymousClass00T.A04(this, R.drawable.novi_wordmark);
        AnonymousClass009.A05(A04);
        toolbar.setLogo(AnonymousClass2GE.A03(this, A04, R.color.novi_header));
        toolbar.setNavigationIcon(AnonymousClass2GF.A00(this, this.A0D, R.drawable.ic_close));
        toolbar.setNavigationOnClickListener(C117305Zk.A0A(this, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT));
        this.A04 = (ProgressBar) findViewById(R.id.submit_button_progressbar);
        this.A04.getIndeterminateDrawable().setColorFilter(new PorterDuffColorFilter(AnonymousClass00T.A00(this, R.color.white), PorterDuff.Mode.SRC_IN));
        AnonymousClass028.A0V(this.A04, getResources().getDimension(R.dimen.novi_pay_button_elevation));
        this.A0H = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A0E);
        Parcelable parcelableExtra = getIntent().getParcelableExtra("step_up");
        AnonymousClass009.A05(parcelableExtra);
        this.A0J = (C1316663q) parcelableExtra;
        this.A00 = getIntent().getIntExtra("step_up_origin_action", 1);
        this.A0O = getIntent().getStringExtra("acct_restriction_type");
        C128375w0 r1 = this.A0L;
        if (bundle == null) {
            bundle = C12990iw.A0H(this);
        }
        this.A0M = (C118125bJ) C117315Zl.A06(new C118365bh(bundle, r1), this).A00(C118125bJ.class);
        this.A08 = (ShimmerFrameLayout) findViewById(R.id.novi_text_input_questions_shimmer);
        this.A06 = (RecyclerView) findViewById(R.id.text_input_step_up_account_questions);
        C118525bx r12 = new C118525bx();
        this.A06.setAdapter(r12);
        C12990iw.A1K(this.A06);
        this.A07 = (RecyclerView) findViewById(R.id.text_input_step_up_transaction_questions);
        C14830m7 r8 = ((ActivityC13790kL) this).A05;
        AnonymousClass14X r11 = this.A0N;
        C118555c0 r5 = new C118555c0(this.A0A, this.A0B, r8, this.A0D, this.A0F, r11);
        this.A07.setAdapter(r5);
        C12990iw.A1K(this.A07);
        C118125bJ r4 = this.A0M;
        IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(r12, 132);
        IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(r5, 134);
        r4.A02.A05(this, A0B);
        r4.A03.A05(this, A0B2);
        this.A01 = findViewById(R.id.submit_button_container);
        WaButton waButton = (WaButton) findViewById(R.id.submit_button);
        this.A09 = waButton;
        C117295Zj.A0n(waButton, this, 143);
        this.A03 = (LinearLayout) findViewById(R.id.security_note_container);
        TextView A0M = C12970iu.A0M(this, R.id.security_label);
        String string = getString(R.string.learn_more);
        String A0X = C12960it.A0X(this, string, new Object[1], 0, R.string.novi_payment_information_encrypted_text);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A0X);
        C117325Zm.A04(spannableStringBuilder, new C117515a5(this, string), A0X, string);
        A0M.setText(spannableStringBuilder);
        A0M.setLinksClickable(true);
        C12990iw.A1F(A0M);
        C118125bJ r2 = this.A0M;
        r2.A0B.A05(this, C117305Zk.A0B(this, 133));
        this.A0M.A05(new C126065sH(0));
        this.A02 = (LinearLayout) findViewById(R.id.footer_container);
        ScrollView scrollView = (ScrollView) findViewById(R.id.content_scrollview);
        this.A05 = scrollView;
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.654
            @Override // android.view.ViewTreeObserver.OnScrollChangedListener
            public final void onScrollChanged() {
                float f;
                NoviTextInputStepUpActivity noviTextInputStepUpActivity = NoviTextInputStepUpActivity.this;
                boolean canScrollVertically = noviTextInputStepUpActivity.A05.canScrollVertically(1);
                LinearLayout linearLayout = noviTextInputStepUpActivity.A02;
                if (canScrollVertically) {
                    f = noviTextInputStepUpActivity.getResources().getDimension(R.dimen.novi_pay_footer_elevation);
                } else {
                    f = 0.0f;
                }
                AnonymousClass028.A0V(linearLayout, f);
            }
        });
        this.A0M.A04(new AnonymousClass610("NAVIGATION_START", "TEXT_INPUT", "BUTTON"));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0M.A04(new AnonymousClass610("NAVIGATION_END", "TEXT_INPUT", "BUTTON"));
    }
}
