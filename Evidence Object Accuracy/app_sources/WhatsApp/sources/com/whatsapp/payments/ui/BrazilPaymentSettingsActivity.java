package com.whatsapp.payments.ui;

import X.AbstractActivityC119285dT;
import X.AbstractActivityC121725jj;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C117295Zj;
import X.C12960it;
import X.C12980iv;
import X.C130065yk;
import X.C16700pc;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.BrazilPaymentSettingsActivity;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes4.dex */
public class BrazilPaymentSettingsActivity extends AbstractActivityC121725jj {
    public C130065yk A00;
    public boolean A01;

    public BrazilPaymentSettingsActivity() {
        this(0);
    }

    public BrazilPaymentSettingsActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 22);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119285dT.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A00 = (C130065yk) A1M.A1z.get();
        }
    }

    @Override // X.AbstractActivityC121725jj
    public void A2e(String str) {
        String A0g;
        String A0g2;
        String A0g3;
        String A0g4;
        String A0g5;
        if (((AbstractActivityC121725jj) this).A00.A02.A07() && ((ActivityC13810kN) this).A0C.A07(1601)) {
            C16700pc.A0E(str, 0);
            Uri parse = Uri.parse(str);
            List<String> pathSegments = parse.getPathSegments();
            if (((pathSegments.size() == 3 && (A0g3 = C12960it.A0g(pathSegments, 0)) != null && A0g3.equalsIgnoreCase("pay") && (A0g4 = C12960it.A0g(pathSegments, 1)) != null && A0g4.equalsIgnoreCase("br") && (A0g5 = C12960it.A0g(pathSegments, 2)) != null && A0g5.equalsIgnoreCase("add-credential")) || (pathSegments.size() == 2 && (A0g = C12960it.A0g(pathSegments, 0)) != null && A0g.equalsIgnoreCase("br") && (A0g2 = C12960it.A0g(pathSegments, 1)) != null && A0g2.equalsIgnoreCase("add-credential"))) && parse.getQueryParameterNames().contains("pushAccountData")) {
                try {
                    String queryParameter = Uri.parse(str).getQueryParameter("pushAccountData");
                    Log.i("Push Prov deeplink received");
                    C004802e A0S = C12980iv.A0S(this);
                    A0S.A07(R.string.add_card_from_deep_link_set_up_payments_title);
                    A0S.A06(R.string.add_card_from_deep_link_set_up_payments_desc);
                    A0S.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(queryParameter) { // from class: X.62R
                        public final /* synthetic */ String A01;

                        {
                            this.A01 = r2;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            BrazilPaymentSettingsActivity brazilPaymentSettingsActivity = BrazilPaymentSettingsActivity.this;
                            String str2 = this.A01;
                            String A01 = brazilPaymentSettingsActivity.A00.A01();
                            Intent A0D = C12990iw.A0D(brazilPaymentSettingsActivity, BrazilPayBloksActivity.class);
                            if (A01 == null) {
                                A01 = "brpay_p_add_credential_router";
                            }
                            A0D.putExtra("screen_name", A01);
                            AbstractActivityC119645em.A0O(A0D, "referral_screen", "deeplink");
                            AbstractActivityC119645em.A0O(A0D, "credential_push_data", str2);
                            brazilPaymentSettingsActivity.startActivity(A0D);
                        }
                    });
                    A0S.A04(new DialogInterface.OnDismissListener() { // from class: X.62s
                        @Override // android.content.DialogInterface.OnDismissListener
                        public final void onDismiss(DialogInterface dialogInterface) {
                            Log.i("Cancelled push_provision_interstitial dialog");
                        }
                    });
                    A0S.A0B(false);
                    A0S.A05();
                    return;
                } catch (NullPointerException | UnsupportedOperationException unused) {
                    Log.i("Unable to read query param pushAccountData");
                }
            }
        }
        super.A2e(str);
    }
}
