package com.whatsapp.payments.ui;

import X.AnonymousClass009;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class PaymentTypePickerFragment extends Hilt_PaymentTypePickerFragment {
    public static Bundle A00(int i) {
        String str;
        Bundle A0D = C12970iu.A0D();
        if (i != 0) {
            str = "friendsAndFamily";
        } else {
            str = "goodAndServices";
        }
        A0D.putString("arg_type", str);
        return A0D;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_type_picker_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        int i;
        int i2;
        String string = A03().getString("arg_type", "goodAndServices");
        AnonymousClass009.A05(string);
        C12960it.A0J(view, R.id.buying_goods_and_services_text).setText(R.string.buying_goods_and_services);
        TextView A0J = C12960it.A0J(view, R.id.buying_goods_and_services_hint_text);
        boolean z = this instanceof BrazilPaymentTypePickerFragment;
        if (!z) {
            i = R.string.buying_goods_and_services_hint;
        } else {
            i = R.string.br_buying_goods_and_services_hint;
        }
        A0J.setText(A0I(i));
        C12960it.A0J(view, R.id.sending_to_friends_and_family_text).setText(R.string.sending_to_friends_and_family);
        TextView A0J2 = C12960it.A0J(view, R.id.sending_to_friends_and_family_hint_text);
        if (!z) {
            i2 = R.string.sending_to_friends_and_family_hint;
        } else {
            i2 = R.string.br_sending_to_friends_and_family_hint;
        }
        A0J2.setText(A0I(i2));
        boolean equals = string.equals("goodAndServices");
        View findViewById = view.findViewById(R.id.buying_goods_and_services_check);
        if (equals) {
            findViewById.setVisibility(0);
            C12980iv.A1B(view, R.id.sending_to_friends_and_family_check, 4);
        } else {
            findViewById.setVisibility(4);
            C12980iv.A1B(view, R.id.sending_to_friends_and_family_check, 0);
        }
        C117295Zj.A0n(view.findViewById(R.id.sending_to_friends_and_family_container), this, 114);
        C117295Zj.A0n(view.findViewById(R.id.buying_goods_and_services_container), this, 113);
        C117295Zj.A0n(view.findViewById(R.id.back), this, 112);
    }
}
