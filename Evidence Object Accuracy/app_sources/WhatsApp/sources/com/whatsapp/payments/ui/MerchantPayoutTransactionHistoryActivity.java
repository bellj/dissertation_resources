package com.whatsapp.payments.ui;

import X.AbstractC001200n;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117935b0;
import X.C118415bm;
import X.C118635c8;
import X.C124095ob;
import X.C129395xe;
import X.C12990iw;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.StickyHeadersRecyclerView;

/* loaded from: classes4.dex */
public class MerchantPayoutTransactionHistoryActivity extends ActivityC13790kL {
    public ViewGroup A00;
    public FrameLayout A01;
    public StickyHeadersRecyclerView A02;
    public C118635c8 A03;
    public C117935b0 A04;
    public C129395xe A05;
    public boolean A06;

    public MerchantPayoutTransactionHistoryActivity() {
        this(0);
    }

    public MerchantPayoutTransactionHistoryActivity(int i) {
        this.A06 = false;
        C117295Zj.A0p(this, 78);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A05 = (C129395xe) A1M.AEy.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        int A01 = C117305Zk.A01(this, R.layout.payout_transaction_history);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117295Zj.A0h(this, A1U, R.string.payment_merchant_payouts_title, A01);
        }
        this.A03 = new C118635c8(this);
        this.A01 = (FrameLayout) findViewById(R.id.loading_container);
        this.A00 = (ViewGroup) findViewById(R.id.transaction_list_container);
        StickyHeadersRecyclerView stickyHeadersRecyclerView = (StickyHeadersRecyclerView) findViewById(R.id.transaction_list);
        this.A02 = stickyHeadersRecyclerView;
        stickyHeadersRecyclerView.setAdapter(this.A03);
        C117935b0 r3 = (C117935b0) C117315Zl.A06(new C118415bm(this, this.A05), this).A00(C117935b0.class);
        this.A04 = r3;
        C12990iw.A1J(r3.A00, true);
        C12990iw.A1J(r3.A01, false);
        C12990iw.A1N(new C124095ob(r3.A06, r3), r3.A09);
        C117935b0 r5 = this.A04;
        IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(this, 71);
        IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(this, 73);
        IDxObserverShape5S0100000_3_I1 A0B3 = C117305Zk.A0B(this, 72);
        AnonymousClass016 r0 = r5.A02;
        AbstractC001200n r1 = r5.A03;
        r0.A05(r1, A0B);
        r5.A00.A05(r1, A0B2);
        r5.A01.A05(r1, A0B3);
    }
}
