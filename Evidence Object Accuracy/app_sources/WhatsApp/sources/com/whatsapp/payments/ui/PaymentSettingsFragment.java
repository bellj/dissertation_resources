package com.whatsapp.payments.ui;

import X.AbstractActivityC121725jj;
import X.AbstractC118085bF;
import X.AbstractC1308360d;
import X.AbstractC1311261j;
import X.AbstractC136336Md;
import X.AbstractC136346Me;
import X.AbstractC14440lR;
import X.AbstractC28901Pl;
import X.AbstractC35651iS;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01d;
import X.AnonymousClass102;
import X.AnonymousClass116;
import X.AnonymousClass14X;
import X.AnonymousClass17Z;
import X.AnonymousClass19Y;
import X.AnonymousClass1A7;
import X.AnonymousClass1In;
import X.AnonymousClass2S0;
import X.AnonymousClass2SM;
import X.AnonymousClass60P;
import X.AnonymousClass60T;
import X.AnonymousClass613;
import X.AnonymousClass61E;
import X.AnonymousClass61I;
import X.AnonymousClass6M5;
import X.C117305Zk;
import X.C117615aH;
import X.C118025b9;
import X.C123495nF;
import X.C123505nG;
import X.C123675ne;
import X.C124285ou;
import X.C124295ov;
import X.C125085qf;
import X.C129135xE;
import X.C129395xe;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C130015yf;
import X.C1310260x;
import X.C1311161i;
import X.C1329368u;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C16590pI;
import X.C17070qD;
import X.C17900ra;
import X.C18050rp;
import X.C18600si;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C18660so;
import X.C18790t3;
import X.C18810t5;
import X.C20380vf;
import X.C20390vg;
import X.C20400vh;
import X.C21860y6;
import X.C22710zW;
import X.C243515e;
import X.C253118x;
import X.C34271fr;
import X.C35741ib;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperValuePropsActivity;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import com.whatsapp.payments.ui.widget.TransactionsExpandableView;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public abstract class PaymentSettingsFragment extends Hilt_PaymentSettingsFragment implements View.OnClickListener, AbstractC1311261j, AbstractC136336Md, AnonymousClass1In, AbstractC136346Me, AnonymousClass6M5 {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public FrameLayout A09;
    public FrameLayout A0A;
    public FrameLayout A0B;
    public LinearLayout A0C;
    public ListView A0D;
    public TextView A0E;
    public C14900mE A0F;
    public C15570nT A0G;
    public AnonymousClass19Y A0H;
    public C15450nH A0I;
    public C18790t3 A0J;
    public AnonymousClass116 A0K;
    public C15550nR A0L;
    public C15610nY A0M;
    public C18640sm A0N;
    public AnonymousClass01d A0O;
    public C14830m7 A0P;
    public C16590pI A0Q;
    public AnonymousClass018 A0R;
    public C14850m9 A0S;
    public C18050rp A0T;
    public C18810t5 A0U;
    public C20380vf A0V;
    public C21860y6 A0W;
    public C20400vh A0X;
    public C18650sn A0Y;
    public C18660so A0Z;
    public C20390vg A0a;
    public C18600si A0b;
    public C243515e A0c;
    public C18610sj A0d;
    public C17900ra A0e;
    public C22710zW A0f;
    public C17070qD A0g;
    public C129135xE A0h;
    public AnonymousClass1A7 A0i;
    public AnonymousClass60T A0j;
    public C124295ov A0k;
    public AnonymousClass17Z A0l;
    public AnonymousClass2S0 A0m;
    public AnonymousClass61E A0n;
    public C130015yf A0o;
    public C117615aH A0p;
    public C1329368u A0q;
    public AbstractC1308360d A0r;
    public C118025b9 A0s;
    public AbstractC118085bF A0t;
    public C129395xe A0u;
    public AnonymousClass60P A0v;
    public C123675ne A0w;
    public TransactionsExpandableView A0x;
    public TransactionsExpandableView A0y;
    public AnonymousClass14X A0z;
    public C253118x A10;
    public AbstractC14440lR A11;
    public String A12;
    public List A13 = C12960it.A0l();
    public List A14 = C12960it.A0l();
    public List A15 = C12960it.A0l();

    @Override // X.AbstractC1311261j
    public int AEM(AbstractC28901Pl r2) {
        return 0;
    }

    @Override // X.AbstractC1311261j
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC1311261j
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    public static final String A00(Resources resources, C1310260x r4) {
        if (r4 == null) {
            return "";
        }
        int i = r4.A00;
        if (i != 0) {
            Object[] objArr = r4.A04;
            if (objArr == null || objArr.length <= 0) {
                return resources.getString(i);
            }
            return resources.getString(i, objArr);
        }
        String str = r4.A03;
        if (str == null) {
            return "";
        }
        return str;
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        AbstractC1308360d r0;
        int intExtra;
        String quantityString;
        if (i != 1) {
            if (i != 48) {
                if (i != 150) {
                    if (i == 501) {
                        View view = ((AnonymousClass01E) this).A0A;
                        if (intent != null && view != null) {
                            if (i2 == -1) {
                                UserJid nullable = UserJid.getNullable(intent.getStringExtra("extra_invitee_jid"));
                                if (nullable != null) {
                                    quantityString = C12990iw.A0o(A02(), this.A0M.A08(this.A0L.A0B(nullable)), new Object[1], 0, R.string.payments_invite_sent_snackbar);
                                } else {
                                    return;
                                }
                            } else if (i2 == 501 && (intExtra = intent.getIntExtra("extra_inviter_count", 0)) > 0) {
                                Resources A02 = A02();
                                Object[] objArr = new Object[1];
                                C12960it.A1P(objArr, intExtra, 0);
                                quantityString = A02.getQuantityString(R.plurals.payments_multi_invite_snackbar, intExtra, objArr);
                            } else {
                                return;
                            }
                            C34271fr.A00(view, quantityString, -1).A03();
                            return;
                        }
                        return;
                    }
                } else if (i2 == -1) {
                    A1O(null);
                    return;
                } else {
                    return;
                }
            } else if (i2 == -1) {
                A0C().finish();
                return;
            } else {
                return;
            }
        } else if (i2 == -1 && (r0 = this.A0r) != null) {
            r0.A00();
        }
        super.A0t(i, i2, intent);
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            ActivityC000900k A0C = A0C();
            if (A0C instanceof AbstractActivityC121725jj) {
                A0C.finish();
                ((AbstractActivityC121725jj) A0C).A2f();
            }
            return true;
        } else if (menuItem.getItemId() != R.id.menuitem_debug) {
            return false;
        } else {
            String AF9 = this.A0g.A02().AF9();
            if (TextUtils.isEmpty(AF9)) {
                return false;
            }
            A0v(C12970iu.A0A().setClassName(A0C(), AF9));
            return true;
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_settings_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        C1329368u r2 = this.A0q;
        if (r2 != null) {
            C124285ou r1 = r2.A02;
            if (r1 != null) {
                r1.A03(true);
            }
            r2.A02 = null;
            AbstractC35651iS r12 = r2.A00;
            if (r12 != null) {
                r2.A09.A04(r12);
            }
        }
        C124295ov r13 = this.A0k;
        if (r13 != null) {
            r13.A03(false);
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        ActivityC000900k A0C = A0C();
        if (A0C instanceof ActivityC13810kN) {
            ((ActivityC13810kN) A0C).A2C(R.string.payments_loading);
        }
        this.A0q.A00(true);
        this.A03.setVisibility(C12960it.A02(A1Q() ? 1 : 0));
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A0M();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x03b6, code lost:
        if ((r38 instanceof com.whatsapp.payments.ui.BrazilPaymentSettingsFragment) == false) goto L_0x01d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0484, code lost:
        if ((r0.A01.A00() - X.C12980iv.A0F(r0.A02(), "wavi_methods_last_sync_time")) <= 3600000) goto L_0x0486;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x04b0, code lost:
        if (r11.A0E.A0G() == false) goto L_0x04b2;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r39, android.view.View r40) {
        /*
        // Method dump skipped, instructions count: 1341
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.PaymentSettingsFragment.A17(android.os.Bundle, android.view.View):void");
    }

    public String A1J() {
        if (!(this instanceof IndiaUpiPaymentSettingsFragment)) {
            return null;
        }
        C123505nG r0 = ((IndiaUpiPaymentSettingsFragment) this).A09;
        AnonymousClass009.A05(r0);
        int A0C = r0.A0C();
        if (A0C == 1) {
            return "finish_setup";
        }
        if (A0C == 2) {
            return "onboarding_banner";
        }
        if (A0C == 4) {
            return "add_upi_number_banner";
        }
        if (A0C == 5) {
            return "notify_verification_banner";
        }
        return null;
    }

    public void A1K() {
        AbstractC14440lR r2 = this.A11;
        C124295ov r0 = this.A0k;
        if (r0 != null && r0.A00() == 1) {
            this.A0k.A03(false);
        }
        Bundle A0D = C12970iu.A0D();
        A0D.putString("com.whatsapp.support.DescribeProblemActivity.from", "payments:settings");
        C18790t3 r8 = this.A0J;
        C124295ov r3 = new C124295ov(A0D, (ActivityC13810kN) A0C(), this.A0H, this.A0I, r8, this.A0R, null, null, this.A0U, this.A0e, "payments:settings");
        this.A0k = r3;
        C12960it.A1E(r3, r2);
    }

    public void A1L(Intent intent) {
        Bundle extras = intent.getExtras();
        boolean z = false;
        if (extras != null) {
            z = extras.getBoolean("extra_force_get_methods", false);
        }
        this.A0q.A02(A1R(), z);
    }

    public void A1M(String str) {
        if (this instanceof BrazilPaymentSettingsFragment) {
            BrazilPaymentSettingsFragment brazilPaymentSettingsFragment = (BrazilPaymentSettingsFragment) this;
            String A01 = brazilPaymentSettingsFragment.A08.A01();
            if (A01 == null || brazilPaymentSettingsFragment.A08.A06.A03()) {
                brazilPaymentSettingsFragment.A0v(C12990iw.A0D(brazilPaymentSettingsFragment.A0p(), BrazilFbPayHubActivity.class));
                AbstractC118085bF r3 = brazilPaymentSettingsFragment.A0t;
                if (r3 != null) {
                    AnonymousClass61I.A01(AnonymousClass61I.A00(r3.A04, null, brazilPaymentSettingsFragment.A0m, null, false), r3.A08, 37, "payment_home", null, 1);
                    return;
                }
                return;
            }
            brazilPaymentSettingsFragment.A1S(A01);
            AbstractC118085bF r2 = brazilPaymentSettingsFragment.A0t;
            if (r2 != null) {
                r2.A09(brazilPaymentSettingsFragment.A0m, 36, str);
            }
        }
    }

    public void A1N(String str) {
        Intent A0D;
        String str2;
        String str3;
        int i;
        if (this instanceof IndiaUpiPaymentSettingsFragment) {
            IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment = (IndiaUpiPaymentSettingsFragment) this;
            C123505nG r0 = indiaUpiPaymentSettingsFragment.A09;
            AnonymousClass009.A05(r0);
            int A0C = r0.A0C();
            if (A0C == 1) {
                AbstractC118085bF r3 = indiaUpiPaymentSettingsFragment.A0t;
                if (r3 != null) {
                    r3.A09(null, 85, str);
                }
                A0D = C12990iw.A0D(indiaUpiPaymentSettingsFragment.A0C(), IndiaUpiPaymentsAccountSetupActivity.class);
                A0D.putExtra("extra_setup_mode", 2);
                A0D.putExtra("extra_payments_entry_type", 5);
                A0D.putExtra("extra_is_first_payment_method", true);
                A0D.putExtra("extra_skip_value_props_display", true);
                StringBuilder A0j = C12960it.A0j("payment_home");
                A0j.append(".");
                A0D.putExtra("extra_referral_screen", C12960it.A0d("finish_setup", A0j));
                str2 = "resumeOnboardingBanner";
            } else if (A0C == 2 || A0C == 3) {
                indiaUpiPaymentSettingsFragment.A1O(str);
                return;
            } else if (A0C == 4) {
                AbstractC118085bF r2 = indiaUpiPaymentSettingsFragment.A0t;
                if (r2 != null) {
                    r2.A08(null, 127, str);
                }
                A0D = C12990iw.A0D(indiaUpiPaymentSettingsFragment.A0C(), IndiaUpiMapperValuePropsActivity.class);
                StringBuilder A0j2 = C12960it.A0j("payment_home");
                A0j2.append(".");
                A0D.putExtra("extra_referral_screen", C12960it.A0d("add_upi_number_banner", A0j2));
                AnonymousClass2SM A0J = C117305Zk.A0J();
                List list = indiaUpiPaymentSettingsFragment.A0p.A02;
                if (list == null || list.isEmpty()) {
                    str3 = null;
                } else {
                    str3 = C1311161i.A09(list);
                }
                A0D.putExtra("extra_payment_name", C117305Zk.A0I(A0J, String.class, str3, "accountHolderName"));
                indiaUpiPaymentSettingsFragment.A0v(A0D);
            } else if (A0C == 5) {
                AbstractC118085bF r1 = indiaUpiPaymentSettingsFragment.A0t;
                if (r1 != null) {
                    r1.A05(1, 139);
                }
                A0D = C12990iw.A0D(indiaUpiPaymentSettingsFragment.A0C(), IndiaUpiPaymentsAccountSetupActivity.class);
                A0D.putExtra("extra_payments_entry_type", 1);
                StringBuilder A0j3 = C12960it.A0j("payment_home");
                A0j3.append(".");
                A0D.putExtra("extra_referral_screen", C12960it.A0d("notify_verification_banner", A0j3));
                A0D.putExtra("extra_payment_flow_entry_point", 2);
                A0D.putExtra("extra_setup_mode", 2);
                A0D.putExtra("extra_is_first_payment_method", true);
                A0D.putExtra("extra_skip_value_props_display", true);
                str2 = "accountRecoveryBanner";
            } else {
                return;
            }
            C35741ib.A00(A0D, str2);
            indiaUpiPaymentSettingsFragment.A0v(A0D);
        } else if (this instanceof BrazilPaymentSettingsFragment) {
            BrazilPaymentSettingsFragment brazilPaymentSettingsFragment = (BrazilPaymentSettingsFragment) this;
            C123495nF r12 = brazilPaymentSettingsFragment.A09;
            AnonymousClass009.A05(r12);
            AnonymousClass60P r02 = brazilPaymentSettingsFragment.A0v;
            if (r02 != null) {
                i = r02.A01;
            } else {
                i = 0;
            }
            int A0C2 = r12.A0C(i);
            if (A0C2 == 1) {
                brazilPaymentSettingsFragment.A1M(str);
            } else if (A0C2 == 2) {
                brazilPaymentSettingsFragment.A1S(brazilPaymentSettingsFragment.A08.A01());
            }
        }
    }

    public void A1O(String str) {
        String str2;
        if (!(this instanceof IndiaUpiPaymentSettingsFragment)) {
            AbstractC118085bF r2 = this.A0t;
            if (r2 != null) {
                r2.A08(this.A0m, 38, str);
            }
            Intent A0D = C12990iw.A0D(A0C(), PaymentContactPicker.class);
            A0D.putExtra("for_payments", true);
            A0D.putExtra("referral_screen", "payment_home");
            startActivityForResult(A0D, 501);
            return;
        }
        Hilt_IndiaUpiPaymentSettingsFragment hilt_IndiaUpiPaymentSettingsFragment = (Hilt_IndiaUpiPaymentSettingsFragment) this;
        boolean A0C = ((PaymentSettingsFragment) hilt_IndiaUpiPaymentSettingsFragment).A0W.A0C();
        AbstractC118085bF r22 = hilt_IndiaUpiPaymentSettingsFragment.A0t;
        if (!A0C) {
            if (r22 != null) {
                r22.A09(hilt_IndiaUpiPaymentSettingsFragment.A0m, 36, str);
            }
            Intent A0D2 = C12990iw.A0D(hilt_IndiaUpiPaymentSettingsFragment.A0p(), IndiaUpiPaymentsAccountSetupActivity.class);
            A0D2.putExtra("extra_setup_mode", 1);
            A0D2.putExtra("extra_payments_entry_type", 4);
            A0D2.putExtra("extra_is_first_payment_method", true);
            A0D2.putExtra("extra_skip_value_props_display", false);
            C35741ib.A00(A0D2, "settingsNewPayment");
            hilt_IndiaUpiPaymentSettingsFragment.A0v(A0D2);
            return;
        }
        if (r22 != null) {
            int i = 38;
            if (TextUtils.equals("onboarding_banner", str)) {
                i = 85;
            }
            hilt_IndiaUpiPaymentSettingsFragment.A0t.A08(hilt_IndiaUpiPaymentSettingsFragment.A0m, Integer.valueOf(i), str);
        }
        Intent A0D3 = C12990iw.A0D(hilt_IndiaUpiPaymentSettingsFragment.A0p(), IndiaUpiContactPicker.class);
        A0D3.putExtra("for_payments", true);
        if (TextUtils.equals("onboarding_banner", str)) {
            StringBuilder A0j = C12960it.A0j("payment_home");
            A0j.append(".");
            str2 = C12960it.A0d("onboarding_banner", A0j);
        } else {
            str2 = "new_payment";
        }
        A0D3.putExtra("referral_screen", str2);
        hilt_IndiaUpiPaymentSettingsFragment.startActivityForResult(A0D3, 501);
    }

    public final void A1P(boolean z) {
        Class cls;
        AbstractC118085bF r3 = this.A0t;
        if (r3 != null) {
            AnonymousClass2S0 r2 = this.A0m;
            int i = 45;
            if (z) {
                i = 46;
            }
            AnonymousClass61I.A01(AnonymousClass61I.A00(r3.A04, null, r2, null, false), r3.A08, Integer.valueOf(i), "payment_home", null, 1);
        }
        ActivityC000900k A0C = A0C();
        if (this instanceof NoviSharedPaymentSettingsFragment) {
            cls = NoviPaymentTransactionHistoryActivity.class;
        } else if (!(this instanceof IndiaUpiPaymentSettingsFragment)) {
            cls = PaymentTransactionHistoryActivity.class;
        } else {
            cls = IndiaPaymentTransactionHistoryActivity.class;
        }
        Intent A0D = C12990iw.A0D(A0C, cls);
        A0D.putExtra("extra_show_requests", z);
        A0v(A0D);
    }

    public boolean A1Q() {
        if (this instanceof NoviSharedPaymentSettingsFragment) {
            NoviSharedPaymentSettingsFragment noviSharedPaymentSettingsFragment = (NoviSharedPaymentSettingsFragment) this;
            if (noviSharedPaymentSettingsFragment.A0F.A05() || !((PaymentSettingsFragment) noviSharedPaymentSettingsFragment).A0S.A07(860) || !((PaymentSettingsFragment) noviSharedPaymentSettingsFragment).A0S.A07(900) || !noviSharedPaymentSettingsFragment.A0E.A0E()) {
                return false;
            }
            return true;
        } else if (!(this instanceof IndiaUpiPaymentSettingsFragment) || !this.A0S.A07(733) || !this.A0S.A07(783)) {
            return false;
        } else {
            if (this.A0W.A0C() || this.A0W.A0B()) {
                return true;
            }
            return false;
        }
    }

    public boolean A1R() {
        if (this instanceof IndiaUpiPaymentSettingsFragment) {
            return false;
        }
        C18600si r1 = this.A0b;
        return C12960it.A1U(((r1.A01.A00() - C12980iv.A0F(r1.A01(), "payments_all_transactions_last_sync_time")) > TimeUnit.DAYS.toMillis(7) ? 1 : ((r1.A01.A00() - C12980iv.A0F(r1.A01(), "payments_all_transactions_last_sync_time")) == TimeUnit.DAYS.toMillis(7) ? 0 : -1)));
    }

    @Override // X.AbstractC136326Mc
    public String AEP(AbstractC28901Pl r2) {
        return C1311161i.A06(A0C(), r2) != null ? C1311161i.A06(A0C(), r2) : "";
    }

    @Override // X.AnonymousClass1In
    public void ATd() {
        this.A0q.A00(false);
    }

    @Override // X.AbstractC1311261j
    public /* synthetic */ boolean AdN(AbstractC28901Pl r2) {
        return false;
    }

    @Override // X.AnonymousClass6M5
    public void AfR(List list) {
        int i;
        int i2;
        if (A0c() && A0B() != null) {
            this.A13 = list;
            this.A05.setVisibility(0);
            C117615aH r0 = this.A0p;
            r0.A02 = list;
            r0.notifyDataSetChanged();
            View view = ((AnonymousClass01E) this).A0A;
            if (view != null && (this instanceof BrazilPaymentSettingsFragment)) {
                C12980iv.A1B(view, R.id.payment_settings_services_section_header, 8);
                C12980iv.A1B(view, R.id.payment_settings_row_container, 0);
                C12980iv.A1B(view, R.id.payment_settings_row_separator, 0);
                if (!(!((BrazilPaymentSettingsFragment) this).A08.A06.A03())) {
                    i = R.id.payment_settings_row;
                    C12980iv.A1B(view, R.id.payment_settings_row, 0);
                    i2 = R.id.payment_settings_row_add_method;
                } else {
                    i = R.id.payment_settings_row_add_method;
                    C12980iv.A1B(view, R.id.payment_settings_row_add_method, 0);
                    i2 = R.id.payment_settings_row;
                }
                C12980iv.A1B(view, i2, 8);
                view.findViewById(i).setOnClickListener(this);
            }
            C125085qf.A00(this.A0D);
            AbstractC118085bF r2 = this.A0t;
            if (r2 != null) {
                r2.A01 = list;
                r2.A07(this.A0m, this.A0v);
            }
        }
    }

    @Override // X.AbstractC136346Me
    public void AfY(List list) {
        String A0I;
        if (A0c() && A0B() != null) {
            if (list == null) {
                list = C12960it.A0l();
            }
            this.A14 = list;
            this.A05.setVisibility(0);
            if (this.A14.isEmpty()) {
                this.A07.setVisibility(8);
                this.A0x.setVisibility(8);
                return;
            }
            this.A0x.setVisibility(0);
            this.A07.setVisibility(0);
            this.A0x.A01(this.A14);
            TransactionsExpandableView transactionsExpandableView = this.A0x;
            List list2 = this.A14;
            if (!(this instanceof NoviSharedPaymentSettingsFragment)) {
                A0I = this.A0R.A0D((long) list2.size(), R.plurals.payments_settings_payment_requests);
            } else {
                A0I = A0I(R.string.novi_payments_settings_transactions_pending);
            }
            transactionsExpandableView.setTitle(A0I);
        }
    }

    @Override // X.AbstractC136346Me
    public void Afa(List list) {
        if (A0c() && A0B() != null) {
            if (list == null) {
                list = C12960it.A0l();
            }
            this.A15 = list;
            this.A05.setVisibility(0);
            TransactionsExpandableView transactionsExpandableView = this.A0y;
            List list2 = this.A15;
            if (this instanceof NoviSharedPaymentSettingsFragment) {
                NoviSharedPaymentSettingsFragment noviSharedPaymentSettingsFragment = (NoviSharedPaymentSettingsFragment) this;
                AnonymousClass102 r1 = noviSharedPaymentSettingsFragment.A06;
                C15570nT r0 = ((PaymentSettingsFragment) noviSharedPaymentSettingsFragment).A0G;
                r0.A08();
                list2 = AnonymousClass613.A02(r1, r0.A05, list2);
            }
            transactionsExpandableView.A01(list2);
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.payment_support_container) {
            AbstractC118085bF r1 = this.A0t;
            if (r1 != null) {
                r1.A06(this.A0m);
            }
            A1K();
        } else if (view.getId() == R.id.send_payment_fab) {
            if (!this.A0K.A00()) {
                RequestPermissionActivity.A0O(this, R.string.permission_contacts_access_on_new_payment_request, R.string.permission_contacts_access_on_new_payment);
            } else {
                A1O(null);
            }
        } else if (view.getId() == R.id.add_new_account || view.getId() == R.id.payment_settings_row_add_method) {
            ALz(C12960it.A1T(this.A0p.getCount()));
        } else if (view.getId() == R.id.payment_settings_row) {
            A1M(null);
        }
    }
}
