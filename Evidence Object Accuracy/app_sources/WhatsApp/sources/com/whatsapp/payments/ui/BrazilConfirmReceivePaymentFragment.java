package com.whatsapp.payments.ui;

import X.AnonymousClass018;
import X.AnonymousClass102;
import X.AnonymousClass60Z;
import X.AnonymousClass69D;
import X.C129095xA;
import X.C129925yW;
import X.C129945yY;
import X.C130065yk;
import X.C1309660r;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C17220qS;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C18660so;
import X.C22710zW;

/* loaded from: classes4.dex */
public class BrazilConfirmReceivePaymentFragment extends Hilt_BrazilConfirmReceivePaymentFragment {
    public C14900mE A00;
    public C15570nT A01;
    public C18640sm A02;
    public C14830m7 A03;
    public AnonymousClass018 A04;
    public AnonymousClass102 A05;
    public C14850m9 A06;
    public C17220qS A07;
    public C1309660r A08;
    public AnonymousClass69D A09;
    public AnonymousClass60Z A0A;
    public C129925yW A0B;
    public C18650sn A0C;
    public C18660so A0D;
    public C18610sj A0E;
    public C22710zW A0F;
    public C129095xA A0G;
    public C129945yY A0H;
    public C130065yk A0I;
    public C18590sh A0J;
}
