package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC18670sp;
import X.AbstractC28491Nn;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass17V;
import X.AnonymousClass1FK;
import X.AnonymousClass2FL;
import X.AnonymousClass2SP;
import X.AnonymousClass60V;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C122205l5;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C253118x;
import X.C30931Zj;
import X.C32631cT;
import X.C32641cU;
import X.C35741ib;
import X.C452120p;
import X.C452220q;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.IndiaUpiPaymentsTosActivity;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentsTosActivity extends AbstractActivityC121665jA implements AnonymousClass1FK {
    public C32641cU A00;
    public AnonymousClass17V A01;
    public AnonymousClass69E A02;
    public C122205l5 A03;
    public C253118x A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final AnonymousClass2SP A08;
    public final C30931Zj A09;

    public IndiaUpiPaymentsTosActivity() {
        this(0);
        this.A00 = AbstractC18670sp.A04;
        this.A05 = false;
        this.A07 = false;
        this.A08 = new AnonymousClass2SP();
        this.A09 = C117315Zl.A0D("IndiaUpiPaymentsTosActivity");
    }

    public IndiaUpiPaymentsTosActivity(int i) {
        this.A06 = false;
        C117295Zj.A0p(this, 60);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A04 = C117315Zl.A0H(A1M);
            this.A01 = (AnonymousClass17V) A1M.AEX.get();
            this.A02 = (AnonymousClass69E) A1M.A9W.get();
            this.A03 = (C122205l5) A1M.A9Z.get();
        }
    }

    public final void A30(int i) {
        AbstractActivityC119235dO.A1j(this.A03, 3);
        ((AbstractActivityC121665jA) this).A0D.reset();
        C117305Zk.A1L(this.A01);
        this.A09.A0A("showErrorAndFinish", null);
        findViewById(R.id.progress).setVisibility(4);
        AnonymousClass60V A04 = this.A02.A04(null, i);
        if (A04.A00 != 0) {
            MessageDialogFragment.A00(A04.A01(this)).A01().A1F(A0V(), null);
        } else {
            Ado(R.string.payments_tos_error);
        }
    }

    public final void A31(String str) {
        AnonymousClass2SP r1;
        int i;
        boolean equals = "https://www.whatsapp.com/legal/payments/india/terms".equals(str);
        Integer A0V = C12960it.A0V();
        if (equals || "https://www.whatsapp.com/legal/payments/india/privacy-policy".equals(str)) {
            r1 = this.A08;
            i = 20;
        } else if ("https://www.whatsapp.com/legal/payments/india/psp".equals(str)) {
            r1 = this.A08;
            i = 31;
        } else {
            return;
        }
        r1.A08 = Integer.valueOf(i);
        r1.A09 = A0V;
        AbstractActivityC119235dO.A1b(r1, this);
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        C30931Zj r2 = this.A09;
        StringBuilder A0k = C12960it.A0k("got request error for accept-tos: ");
        A0k.append(r4.A00);
        C117295Zj.A1F(r2, A0k);
        A30(r4.A00);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        C30931Zj r2 = this.A09;
        StringBuilder A0k = C12960it.A0k("got response error for accept-tos: ");
        A0k.append(r4.A00);
        C117295Zj.A1F(r2, A0k);
        A30(r4.A00);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        C30931Zj r2 = this.A09;
        StringBuilder A0k = C12960it.A0k("got response for accept-tos: ");
        A0k.append(r4.A02);
        C117295Zj.A1F(r2, A0k);
        AbstractActivityC119235dO.A1d(this);
        if (!this.A00.A03.equals("tos_no_wallet")) {
            return;
        }
        if (r4.A00) {
            AbstractActivityC119235dO.A1j(this.A03, 3);
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.payments_tos_outage);
            C117295Zj.A0q(A0S, this, 47, R.string.ok);
            A0S.A05();
            return;
        }
        C32631cT A02 = ((AbstractActivityC121665jA) this).A0C.A02();
        if (A02 != null) {
            String str = A02.A02;
            if (!TextUtils.isEmpty(str) && str.startsWith("tos_upgrade_step_up")) {
                ((AbstractActivityC121665jA) this).A0C.A08();
            }
        }
        ((AbstractActivityC121685jC) this).A0I.A06(this.A00);
        setResult(-1);
        if (this.A05) {
            Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class);
            A2v(A0D);
            C35741ib.A00(A0D, "tosAccept");
            A2G(A0D, true);
            return;
        }
        finish();
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A07) {
            setResult(0);
            finish();
            return;
        }
        super.onBackPressed();
        AnonymousClass2SP r1 = this.A08;
        r1.A08 = C12970iu.A0h();
        r1.A09 = C12960it.A0V();
        AbstractActivityC119235dO.A1b(r1, this);
        AbstractActivityC119235dO.A1j(this.A03, 4);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        View findViewById = findViewById(R.id.hero_img);
        int i = 0;
        if (configuration.orientation == 2) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass2SP r3;
        Boolean bool;
        super.onCreate(bundle);
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("stepName");
            if (!TextUtils.isEmpty(stringExtra)) {
                this.A00 = ((AbstractActivityC121685jC) this).A0I.A01(stringExtra);
                this.A05 = true;
            } else {
                this.A00 = ((AbstractActivityC121685jC) this).A0I.A01("tos_no_wallet");
            }
            ((AbstractActivityC121665jA) this).A03 = getIntent().getIntExtra("extra_setup_mode", 1);
        }
        setContentView(R.layout.india_upi_payment_tos);
        A2u(R.string.payments_activity_title, R.color.reg_title_color, R.id.scroll_view);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.payments_activity_title);
            A1U.A0M(true);
        }
        TextView A0M = C12970iu.A0M(this, R.id.title);
        if (getIntent() == null || !getIntent().getBooleanExtra("extra_show_updated_tos", false)) {
            A0M.setText(R.string.payments_tos_title);
            r3 = this.A08;
            bool = Boolean.FALSE;
        } else {
            this.A07 = true;
            A0M.setText(R.string.payments_tos_updated_title);
            r3 = this.A08;
            bool = Boolean.TRUE;
        }
        r3.A02 = bool;
        C117295Zj.A0n(findViewById(R.id.learn_more), this, 56);
        String[] strArr = new String[3];
        C117295Zj.A1A(((ActivityC13790kL) this).A02, "https://www.whatsapp.com/legal/payments/india/terms", strArr, 0);
        C117295Zj.A1A(((ActivityC13790kL) this).A02, "https://www.whatsapp.com/legal/payments/india/privacy-policy", strArr, 1);
        C117295Zj.A1A(((ActivityC13790kL) this).A02, "https://www.whatsapp.com/legal/payments/india/psp", strArr, 2);
        AbstractC28491Nn.A05((TextEmojiLabel) findViewById(R.id.payments_tos_desc), ((ActivityC13810kN) this).A08, this.A04.A01(this, getString(R.string.payments_tos_desc_text), new Runnable[]{new Runnable() { // from class: X.6G9
            @Override // java.lang.Runnable
            public final void run() {
                IndiaUpiPaymentsTosActivity.this.A31("https://www.whatsapp.com/legal/payments/india/terms");
            }
        }, new Runnable() { // from class: X.6G7
            @Override // java.lang.Runnable
            public final void run() {
                IndiaUpiPaymentsTosActivity.this.A31("https://www.whatsapp.com/legal/payments/india/privacy-policy");
            }
        }, new Runnable() { // from class: X.6G8
            @Override // java.lang.Runnable
            public final void run() {
                IndiaUpiPaymentsTosActivity.this.A31("https://www.whatsapp.com/legal/payments/india/psp");
            }
        }}, new String[]{"terms", "privacy-policy", "payment-provider-terms"}, strArr));
        View findViewById = findViewById(R.id.payments_tos_continue);
        findViewById.setOnClickListener(new IDxCListenerShape2S0200000_3_I1(this, 15, findViewById));
        C30931Zj r2 = this.A09;
        StringBuilder A0k = C12960it.A0k("onCreate step: ");
        A0k.append(this.A00);
        C117295Zj.A1F(r2, A0k);
        AnonymousClass6BE r1 = ((AbstractActivityC121665jA) this).A0D;
        r1.reset();
        r3.A0Z = "tos_page";
        r3.A09 = 0;
        r1.AKf(r3);
        if (C117305Zk.A1U(((ActivityC13810kN) this).A0C)) {
            this.A0X = this.A0Y.A00(this);
        }
        onConfigurationChanged(C12980iv.A0H(this));
        ((AbstractActivityC121665jA) this).A0C.A09();
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        ((AbstractActivityC121685jC) this).A0P.A04(this);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass2SP r1 = this.A08;
            r1.A08 = C12970iu.A0h();
            r1.A09 = C12960it.A0V();
            AbstractActivityC119235dO.A1b(r1, this);
            AbstractActivityC119235dO.A1j(this.A03, 4);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A07 = bundle.getBoolean("extra_show_updated_tos");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A03.A00.A09("tosShown");
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("extra_show_updated_tos", this.A07);
    }
}
