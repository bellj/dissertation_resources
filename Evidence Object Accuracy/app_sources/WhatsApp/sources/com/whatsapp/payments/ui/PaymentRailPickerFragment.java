package com.whatsapp.payments.ui;

import X.AbstractC28901Pl;
import X.AbstractC30871Zd;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C30881Ze;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class PaymentRailPickerFragment extends Hilt_PaymentRailPickerFragment {
    public AnonymousClass018 A00;

    public static PaymentRailPickerFragment A00(int i) {
        String str;
        PaymentRailPickerFragment paymentRailPickerFragment = new PaymentRailPickerFragment();
        Bundle A0D = C12970iu.A0D();
        if (i != 0) {
            str = "debit";
        } else {
            str = "credit";
        }
        A0D.putString("arg_type", str);
        paymentRailPickerFragment.A0U(A0D);
        return paymentRailPickerFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_rail_picker_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String string = A03().getString("arg_type", "credit");
        AnonymousClass009.A05(string);
        boolean equals = string.equals("credit");
        View findViewById = view.findViewById(R.id.credit_card_check);
        if (equals) {
            findViewById.setVisibility(0);
            C12980iv.A1B(view, R.id.debit_card_check, 4);
        } else {
            findViewById.setVisibility(4);
            C12980iv.A1B(view, R.id.debit_card_check, 0);
        }
        C117295Zj.A0n(view.findViewById(R.id.payment_rail_credit_card_container), this, 103);
        C117295Zj.A0n(view.findViewById(R.id.payment_rail_debit_card_container), this, 104);
        C117295Zj.A0n(view.findViewById(R.id.back), this, 105);
    }

    public final void A19(int i) {
        AbstractC30871Zd r0;
        ConfirmPaymentFragment confirmPaymentFragment = (ConfirmPaymentFragment) A08();
        if (confirmPaymentFragment != null) {
            confirmPaymentFragment.A00 = i;
            TextView textView = confirmPaymentFragment.A0B;
            int i2 = R.string.confirm_payment_bottom_sheet_payment_rails_debit_label;
            if (i == 0) {
                i2 = R.string.confirm_payment_bottom_sheet_payment_rails_credit_label;
            }
            textView.setText(i2);
            AbstractC28901Pl r1 = confirmPaymentFragment.A0I;
            if ((r1 instanceof C30881Ze) && (r0 = (AbstractC30871Zd) r1.A08) != null) {
                r0.A03 = i;
            }
        }
        PaymentBottomSheet paymentBottomSheet = (PaymentBottomSheet) this.A0D;
        if (paymentBottomSheet != null) {
            paymentBottomSheet.A1K();
        }
    }
}
