package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractActivityC121465iB;
import X.AbstractC118045bB;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass5s9;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118265bX;
import X.C122315lG;
import X.C122375lM;
import X.C123385n4;
import X.C126025sD;
import X.C127055ts;
import X.C128365vz;
import X.C12960it;
import X.C12970iu;
import X.C1310561a;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* loaded from: classes4.dex */
public class NoviPayHubManageTopUpActivity extends AbstractActivityC121465iB {
    public WaButton A00;
    public AnonymousClass60Y A01;
    public C123385n4 A02;
    public boolean A03;

    public NoviPayHubManageTopUpActivity() {
        this(0);
    }

    public NoviPayHubManageTopUpActivity(int i) {
        this.A03 = false;
        C117295Zj.A0p(this, 86);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A03(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A01 = C117305Zk.A0W(A1M);
        }
    }

    @Override // X.AbstractActivityC121465iB, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1004) {
            return new C122375lM(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_payment_methods_list), ((ActivityC13830kP) this).A01);
        } else if (i != 1006) {
            return super.A2e(viewGroup, i);
        } else {
            return new C122315lG(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_desc));
        }
    }

    @Override // X.AbstractActivityC121465iB
    public void A2g(C127055ts r4) {
        super.A2g(r4);
        int i = r4.A00;
        if (i == 201) {
            AnonymousClass5s9 r0 = r4.A01;
            if (r0 != null) {
                this.A00.setEnabled(C12970iu.A1Y(r0.A00));
            }
        } else if (i == 405) {
            AnonymousClass5s9 r02 = r4.A01;
            if (r02 != null) {
                C1310561a.A06(this, new C126025sD((String) r02.A00));
            }
        } else if (i == 500) {
            A2C(R.string.register_wait_message);
        } else if (i == 501) {
            AaN();
            this.A00.setEnabled(false);
        }
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C123385n4 r2 = (C123385n4) C117315Zl.A06(new C118265bX(((AbstractActivityC121465iB) this).A01), this).A00(C123385n4.class);
        this.A02 = r2;
        ((AbstractC118045bB) r2).A00.A05(this, C117305Zk.A0B(this, 88));
        C123385n4 r22 = this.A02;
        ((AbstractC118045bB) r22).A01.A05(this, C117305Zk.A0B(this, 87));
        AbstractActivityC119265dR.A0B(this, this.A02);
        AnonymousClass60Y r23 = this.A01;
        C128365vz r0 = new AnonymousClass610("FLOW_SESSION_START", "NOVI_HUB").A00;
        r0.A0j = "SELECT_FI_TYPE";
        r23.A05(r0);
        AnonymousClass60Y.A02(this.A01, "NAVIGATION_START", "NOVI_HUB", "SELECT_FI_TYPE", "SCREEN");
        WaButton waButton = (WaButton) findViewById(R.id.novi_pay_hub_manage_top_up_save);
        this.A00 = waButton;
        C117295Zj.A0n(waButton, this, 85);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(this.A01, "NAVIGATION_END", "NOVI_HUB", "SELECT_FI_TYPE", "SCREEN");
        AnonymousClass60Y r2 = this.A01;
        C128365vz r0 = new AnonymousClass610("FLOW_SESSION_END", "NOVI_HUB").A00;
        r0.A0j = "SELECT_FI_TYPE";
        r2.A05(r0);
    }
}
