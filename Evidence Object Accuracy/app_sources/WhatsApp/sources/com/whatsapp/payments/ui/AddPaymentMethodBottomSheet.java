package com.whatsapp.payments.ui;

import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass17V;
import X.AnonymousClass2SP;
import X.C003501n;
import X.C117295Zj;
import X.C117305Zk;
import X.C119965fO;
import X.C127715uw;
import X.C12960it;
import X.C16120oU;
import X.C17900ra;
import X.C17930rd;
import X.C30931Zj;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes4.dex */
public class AddPaymentMethodBottomSheet extends Hilt_AddPaymentMethodBottomSheet {
    public AnonymousClass01d A00;
    public C16120oU A01;
    public C17900ra A02;
    public AnonymousClass17V A03;
    public C127715uw A04;
    public Runnable A05;
    public final C30931Zj A06 = C117305Zk.A0V("AddPaymentMethodBottomSheet", "payment-settings");

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i != 10) {
            super.A0t(i, i2, intent);
        } else if (i2 == -1) {
            Runnable runnable = this.A05;
            if (runnable != null) {
                runnable.run();
            }
        } else {
            A1B();
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        TextView A0J;
        TextEmojiLabel textEmojiLabel;
        TextView A0J2;
        View inflate = layoutInflater.inflate(R.layout.add_payment_method_bottom_sheet, viewGroup, false);
        C127715uw r0 = this.A04;
        if (r0 != null) {
            String str = r0.A03;
            if (!TextUtils.isEmpty(str) && (A0J2 = C12960it.A0J(inflate, R.id.add_payment_method_bottom_sheet_title)) != null) {
                A0J2.setText(str);
            }
            String str2 = this.A04.A02;
            if (!TextUtils.isEmpty(str2) && (textEmojiLabel = (TextEmojiLabel) inflate.findViewById(R.id.add_payment_method_bottom_sheet_desc)) != null) {
                AbstractC28491Nn.A05(textEmojiLabel, this.A00, str2);
            }
            String str3 = this.A04.A01;
            if (!TextUtils.isEmpty(str3) && (A0J = C12960it.A0J(inflate, R.id.add_payment_method)) != null) {
                A0J.setText(str3);
            }
            if (!TextUtils.isEmpty(null)) {
                View A0D = AnonymousClass028.A0D(inflate, R.id.extra_info_education_divider);
                View A0D2 = AnonymousClass028.A0D(inflate, R.id.extra_info_education_container);
                TextView A0I = C12960it.A0I(inflate, R.id.extra_info_education_text);
                A0D.setVisibility(0);
                A0D2.setVisibility(0);
                A0I.setText((CharSequence) null);
            }
        }
        C17930rd A01 = this.A02.A01();
        if (A01 == null) {
            this.A06.A05("createEvent/null country");
        } else {
            C119965fO r3 = new C119965fO();
            AnonymousClass17V r2 = this.A03;
            byte[] bArr = new byte[8];
            r2.A03.nextBytes(bArr);
            String A03 = C003501n.A03(bArr);
            r2.A02 = A03;
            r3.A02 = A03;
            r3.A01 = A01.A03;
            this.A01.A07(r3);
        }
        C17930rd A012 = this.A02.A01();
        if (A012 == null) {
            this.A06.A05("createUserActionEvent/null country");
        } else {
            AnonymousClass2SP r32 = new AnonymousClass2SP();
            AnonymousClass17V r22 = this.A03;
            byte[] bArr2 = new byte[8];
            r22.A03.nextBytes(bArr2);
            String A032 = C003501n.A03(bArr2);
            r22.A02 = A032;
            r32.A0U = A032;
            r32.A0R = A012.A03;
            r32.A0Z = "get_started";
            r32.A09 = 0;
            this.A01.A07(r32);
        }
        C117295Zj.A0n(inflate.findViewById(R.id.add_payment_method), this, 6);
        return inflate;
    }
}
