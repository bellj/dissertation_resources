package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractActivityC121465iB;
import X.AbstractC14590lg;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass5s9;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61C;
import X.AnonymousClass61F;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C122465lV;
import X.C127055ts;
import X.C128365vz;
import X.C129005x1;
import X.C129295xU;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C130125yq;
import X.C130155yt;
import X.C134366Ei;
import X.C15890o4;
import X.C17070qD;
import X.C20910wW;
import X.C20920wX;
import X.C30861Zc;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity;
import com.whatsapp.util.Log;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviPayHubAddPaymentMethodActivity extends AbstractActivityC121465iB {
    public C20920wX A00;
    public C15890o4 A01;
    public C17070qD A02;
    public C130155yt A03;
    public C130125yq A04;
    public AnonymousClass60Y A05;
    public AnonymousClass61F A06;
    public C129005x1 A07;
    public AnonymousClass61C A08;
    public C129295xU A09;
    public String A0A;
    public boolean A0B;
    public boolean A0C;

    public NoviPayHubAddPaymentMethodActivity() {
        this(0);
        this.A0B = false;
    }

    public NoviPayHubAddPaymentMethodActivity(int i) {
        this.A0C = false;
        C117295Zj.A0p(this, 85);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A03(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A05 = C117305Zk.A0W(A1M);
            this.A02 = C117305Zk.A0P(A1M);
            this.A00 = C20910wW.A00();
            this.A03 = (C130155yt) A1M.ADA.get();
            this.A09 = (C129295xU) A1M.A0I.get();
            this.A06 = C117305Zk.A0X(A1M);
            this.A01 = C12970iu.A0Y(A1M);
            this.A04 = (C130125yq) A1M.ADJ.get();
            this.A07 = (C129005x1) A1M.ADC.get();
            this.A08 = A1M.A3w();
        }
    }

    @Override // X.AbstractActivityC121465iB, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1008) {
            return new C122465lV(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_add_payment_method_row));
        }
        return super.A2e(viewGroup, i);
    }

    @Override // X.AbstractActivityC121465iB
    public void A2g(C127055ts r8) {
        Intent intent;
        String str;
        Intent putExtra;
        super.A2g(r8);
        int i = r8.A00;
        switch (i) {
            case 500:
                A2C(R.string.payments_loading);
                return;
            case 501:
                AaN();
                return;
            case 502:
                finish();
                return;
            default:
                int i2 = 120;
                switch (i) {
                    case 600:
                        if (A2h()) {
                            this.A09.A00(((ActivityC13790kL) this).A00, this, this.A0A, 120);
                            return;
                        }
                        return;
                    case 601:
                        Bundle A0D = C12970iu.A0D();
                        HashMap hashMap = new HashMap(10);
                        hashMap.put("onboarding_app_flow_type", this.A0A);
                        A0D.putSerializable("screen_params", hashMap);
                        Intent putExtras = C12990iw.A0D(this, NoviPayBloksActivity.class).putExtra("screen_name", "novipay_p_add_bank").putExtras(A0D);
                        if (this.A0B) {
                            i2 = 121;
                        }
                        startActivityForResult(putExtras, i2);
                        return;
                    case 602:
                        if (!this.A01.A03()) {
                            intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
                            str = "novipay_p_store_locator_permission_interstitial";
                        } else if (C12980iv.A1W(((AbstractActivityC121465iB) this).A00.A02(), "location_permission_interstitial_shown")) {
                            putExtra = C12990iw.A0D(this, NoviWithdrawCashStoreLocatorActivity.class);
                            startActivityForResult(putExtra, 124);
                            return;
                        } else {
                            intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
                            str = "novipay_p_store_locator_permission_granted_before_interstitial";
                        }
                        putExtra = intent.putExtra("screen_name", str);
                        startActivityForResult(putExtra, 124);
                        return;
                    case 603:
                        AnonymousClass5s9 r0 = r8.A01;
                        AnonymousClass009.A05(r0);
                        this.A06.A04().A00(new C134366Ei((C30861Zc) r0.A00, this, 122));
                        return;
                    default:
                        Log.e(C12960it.A0W(i, "PAY: NoviPayHubAddPaymentMethodActivity/handleEvent/unsupported event: eventId="));
                        return;
                }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!(i == 120 || i == 122)) {
            if (i == 123) {
                i2 = -1;
            } else {
                if (i == 121) {
                    if (i2 == -1) {
                        AnonymousClass009.A05(intent);
                        AnonymousClass009.A05(intent);
                        Bundle extras = intent.getExtras();
                        AnonymousClass009.A05(extras);
                        Serializable serializable = extras.getSerializable("finish_activity_result");
                        AnonymousClass009.A05(serializable);
                        String str = (String) ((AbstractMap) serializable).get("added_bank_credential_id");
                        AnonymousClass009.A06(str, "PAY: NoviPayHubAddPaymentMethodActivity/added bank credential ID is missing for withdrawal");
                        C117315Zl.A0R(((ActivityC13810kN) this).A05, this.A02.A00().A01(str), new AbstractC14590lg() { // from class: X.6EI
                            @Override // X.AbstractC14590lg
                            public final void accept(Object obj) {
                                NoviPayHubAddPaymentMethodActivity noviPayHubAddPaymentMethodActivity = NoviPayHubAddPaymentMethodActivity.this;
                                noviPayHubAddPaymentMethodActivity.A06.A04().A00(new C134366Ei((C30861Zc) ((AbstractC28901Pl) obj), noviPayHubAddPaymentMethodActivity, 123));
                            }
                        });
                        return;
                    }
                } else if (i == 124) {
                    if (i2 == -1) {
                        setResult(i2);
                        finish();
                    }
                    return;
                }
                super.onActivityResult(i, i2, intent);
                return;
            }
        }
        setResult(i2, intent);
        finish();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        String str;
        super.onBackPressed();
        AnonymousClass60Y r4 = this.A05;
        String str2 = this.A0A;
        if (this.A0B) {
            str = "WITHDRAW_METHOD";
        } else {
            str = "PAYMENT_METHODS";
        }
        AnonymousClass60Y.A02(r4, "BACK_CLICK", str2, str, "ARROW");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x009b  */
    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r14) {
        /*
            r13 = this;
            super.onCreate(r14)
            android.os.Bundle r1 = X.C12990iw.A0H(r13)
            X.AnonymousClass009.A05(r1)
            java.lang.String r0 = "extra_funding_category"
            java.lang.String r3 = "payment_settings"
            java.lang.String r2 = r1.getString(r0, r3)
            java.lang.String r0 = "withdrawal"
            boolean r1 = r2.equals(r0)
            r13.A0B = r1
            boolean r0 = r2.equals(r3)
            java.lang.String r5 = "NOVI_HUB"
            if (r1 == 0) goto L_0x00b0
            java.lang.String r0 = "WITHDRAW_MONEY"
        L_0x0024:
            r13.A0A = r0
            X.5w0 r1 = r13.A01
            X.5bl r0 = new X.5bl
            r0.<init>(r1, r2)
            X.02A r1 = X.C117315Zl.A06(r0, r13)
            java.lang.Class<X.5n3> r0 = X.C123375n3.class
            X.015 r2 = r1.A00(r0)
            X.5bB r2 = (X.AbstractC118045bB) r2
            r0 = 86
            com.facebook.redex.IDxObserverShape5S0100000_3_I1 r1 = X.C117305Zk.A0B(r13, r0)
            X.016 r0 = r2.A00
            r0.A05(r13, r1)
            r0 = 85
            com.facebook.redex.IDxObserverShape5S0100000_3_I1 r1 = X.C117305Zk.A0B(r13, r0)
            X.1It r0 = r2.A01
            r0.A05(r13, r1)
            X.AbstractActivityC119265dR.A0B(r13, r2)
            X.0m7 r8 = r13.A05
            X.0m9 r9 = r13.A0C
            X.0wX r7 = r13.A00
            X.5yt r10 = r13.A03
            X.5yq r11 = r13.A04
            X.61C r12 = r13.A08
            X.61D r6 = new X.61D
            r6.<init>(r7, r8, r9, r10, r11, r12)
            r0 = 10
            com.facebook.redex.IDxAListenerShape19S0100000_3_I1 r4 = new com.facebook.redex.IDxAListenerShape19S0100000_3_I1
            r4.<init>(r13, r0)
            X.5yt r3 = r6.A03
            java.lang.String r0 = "novi-get-bank-schema"
            X.60z r2 = X.AnonymousClass61S.A01(r0)
            r1 = 2
            com.facebook.redex.IDxAListenerShape19S0100000_3_I1 r0 = new com.facebook.redex.IDxAListenerShape19S0100000_3_I1
            r0.<init>(r4, r1)
            X.C130155yt.A00(r0, r3, r2)
            boolean r0 = r13.A0B
            java.lang.String r3 = "FLOW_SESSION_START"
            java.lang.String r4 = "WITHDRAW_METHOD"
            if (r0 == 0) goto L_0x00a5
            X.60Y r2 = r13.A05
            java.lang.String r1 = r13.A0A
        L_0x0087:
            X.610 r0 = new X.610
            r0.<init>(r3, r1)
            X.5vz r0 = r0.A00
            r0.A0j = r4
            r2.A06(r0)
        L_0x0093:
            X.60Y r3 = r13.A05
            java.lang.String r2 = r13.A0A
            boolean r0 = r13.A0B
            if (r0 != 0) goto L_0x009d
            java.lang.String r4 = "PAYMENT_METHODS"
        L_0x009d:
            java.lang.String r1 = "NAVIGATION_START"
            java.lang.String r0 = "SCREEN"
            X.AnonymousClass60Y.A02(r3, r1, r2, r4, r0)
            return
        L_0x00a5:
            java.lang.String r1 = r13.A0A
            boolean r0 = r1.equals(r5)
            if (r0 == 0) goto L_0x0093
            X.60Y r2 = r13.A05
            goto L_0x0087
        L_0x00b0:
            if (r0 == 0) goto L_0x00b5
            r0 = r5
            goto L_0x0024
        L_0x00b5:
            java.lang.String r0 = "SEND_MONEY"
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        String str;
        String str2;
        AnonymousClass60Y r2;
        C129005x1 r1 = this.A07;
        r1.A00 = null;
        r1.A01.clear();
        r1.A02.clear();
        super.onDestroy();
        AnonymousClass60Y r5 = this.A05;
        String str3 = this.A0A;
        if (this.A0B) {
            str = "WITHDRAW_METHOD";
        } else {
            str = "PAYMENT_METHODS";
        }
        AnonymousClass60Y.A02(r5, "NAVIGATION_END", str3, str, "SCREEN");
        if (this.A0B) {
            r2 = this.A05;
            str2 = this.A0A;
        } else {
            str2 = this.A0A;
            if (str2.equals("NOVI_HUB")) {
                r2 = this.A05;
            } else {
                return;
            }
        }
        C128365vz r0 = new AnonymousClass610("FLOW_SESSION_END", str2).A00;
        r0.A0j = "WITHDRAW_METHOD";
        r2.A06(r0);
    }
}
