package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractActivityC121465iB;
import X.AbstractC118045bB;
import X.AbstractC136196Lo;
import X.ActivityC121715je;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass600;
import X.AnonymousClass608;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61C;
import X.AnonymousClass61D;
import X.AnonymousClass61E;
import X.AnonymousClass61F;
import X.AnonymousClass61L;
import X.AnonymousClass61M;
import X.AnonymousClass61S;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118255bW;
import X.C122435lS;
import X.C122445lT;
import X.C123365n2;
import X.C124745q2;
import X.C126995tm;
import X.C127035tq;
import X.C127055ts;
import X.C128365vz;
import X.C129585xx;
import X.C12960it;
import X.C129865yQ;
import X.C12990iw;
import X.C130105yo;
import X.C130125yq;
import X.C130155yt;
import X.C130295zB;
import X.C1310460z;
import X.C1315463e;
import X.C1332369z;
import X.C20910wW;
import X.C20920wX;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Pair;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.redex.IDxAListenerShape0S0300000_3_I1;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.ui.NoviPayHubSecurityActivity;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import java.security.KeyPair;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class NoviPayHubSecurityActivity extends AbstractActivityC121465iB {
    public C20920wX A00;
    public AnonymousClass61M A01;
    public C130155yt A02;
    public AnonymousClass61D A03;
    public C129865yQ A04;
    public C130125yq A05;
    public AnonymousClass60Y A06;
    public AnonymousClass61F A07;
    public AnonymousClass61E A08;
    public AnonymousClass61C A09;
    public C123365n2 A0A;
    public boolean A0B;

    public NoviPayHubSecurityActivity() {
        this(0);
    }

    public NoviPayHubSecurityActivity(int i) {
        this.A0B = false;
        C117295Zj.A0p(this, 87);
    }

    public static /* synthetic */ void A0D(NoviPayHubSecurityActivity noviPayHubSecurityActivity, C127035tq r12) {
        AnonymousClass03U A0C;
        AnonymousClass03U A0C2;
        String str;
        String str2;
        int i = r12.A00;
        if (i != 300) {
            if (i == 304 && (A0C2 = ((ActivityC121715je) noviPayHubSecurityActivity).A00.A0C(r12.A01)) != null) {
                try {
                    SwitchCompat switchCompat = (SwitchCompat) A0C2.A0H.findViewById(R.id.toggle_button);
                    boolean z = !switchCompat.isChecked();
                    switchCompat.setChecked(z);
                    switchCompat.setEnabled(false);
                    C12960it.A0t(C130105yo.A01(((AbstractActivityC121465iB) noviPayHubSecurityActivity).A00), "payment_login_require_otp", z);
                    C1315463e r0 = noviPayHubSecurityActivity.A07.A01;
                    if (r0 == null || (str = r0.A02) == null) {
                        throw new C124745q2();
                    }
                    AnonymousClass61D r6 = noviPayHubSecurityActivity.A03;
                    C1332369z r4 = new AbstractC136196Lo(switchCompat, noviPayHubSecurityActivity, z) { // from class: X.69z
                        public final /* synthetic */ SwitchCompat A00;
                        public final /* synthetic */ NoviPayHubSecurityActivity A01;
                        public final /* synthetic */ boolean A02;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                            this.A02 = r3;
                        }

                        @Override // X.AbstractC136196Lo
                        public final void AV8(C130785zy r62) {
                            NoviPayHubSecurityActivity noviPayHubSecurityActivity2 = this.A01;
                            SwitchCompat switchCompat2 = this.A00;
                            boolean z2 = this.A02;
                            switchCompat2.setEnabled(true);
                            if (!r62.A06()) {
                                C129865yQ.A00(noviPayHubSecurityActivity2.A04, r62);
                                switchCompat2.setChecked(!z2);
                            }
                        }
                    };
                    if (z) {
                        str2 = "SMS_OTP";
                    } else {
                        str2 = "NONE";
                    }
                    C1310460z A01 = AnonymousClass61S.A01("novi-change-preferred-two-factor-method-auth");
                    AnonymousClass61S A00 = AnonymousClass61S.A00("new-preferred-two-factor-method", str2);
                    ArrayList arrayList = A01.A01;
                    arrayList.add(A00);
                    if (r6.A02.A07(822)) {
                        long A002 = r6.A01.A00();
                        String A0n = C12990iw.A0n();
                        AnonymousClass61C r2 = r6.A05;
                        JSONObject A04 = r2.A04(A002);
                        try {
                            A04.put("account_id", str);
                        } catch (JSONException unused) {
                            Log.e("PAY: SignedIntentPayloadManager/addNoviAccountId/toJson can't construct json");
                        }
                        AnonymousClass61C.A01(str2, A04);
                        try {
                            A04.put("new_preferred_two_factor_method", A0n);
                        } catch (JSONException unused2) {
                            Log.e("PAY: IntentPayloadHelper/getToggleRequireOtpOnLoginIntent/toJson can't construct json");
                        }
                        C129585xx r1 = new C129585xx(r2.A04, "REQUIRE_OTP_ON_LOGIN", A04);
                        KeyPair A02 = r6.A04.A02();
                        if (A02 != null) {
                            AnonymousClass61S.A03("change-preferred-two-factor-method-intent", r1.A01(A02), arrayList);
                        } else {
                            throw new C124745q2();
                        }
                    }
                    r6.A03.A0B(r4, A01, "set", 5);
                } catch (C124745q2 unused3) {
                    Intent A0D = C12990iw.A0D(noviPayHubSecurityActivity, NoviPayBloksActivity.class);
                    A0D.putExtra("screen_name", "novipay_p_login_password");
                    A0D.putExtra("login_entry_point", 5);
                    noviPayHubSecurityActivity.startActivity(A0D);
                }
            }
        } else if (Build.VERSION.SDK_INT >= 23 && (A0C = ((ActivityC121715je) noviPayHubSecurityActivity).A00.A0C(r12.A01)) != null) {
            noviPayHubSecurityActivity.A2j((SwitchCompat) A0C.A0H.findViewById(R.id.toggle_button));
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A03(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A00 = C20910wW.A00();
            this.A06 = C117305Zk.A0W(A1M);
            this.A02 = (C130155yt) A1M.ADA.get();
            this.A07 = C117305Zk.A0X(A1M);
            this.A05 = (C130125yq) A1M.ADJ.get();
            this.A08 = (AnonymousClass61E) A1M.AEY.get();
            this.A09 = A1M.A3w();
            this.A01 = C117305Zk.A0N(A1M);
        }
    }

    @Override // X.AbstractActivityC121465iB, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1005) {
            return new C122445lT(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_toggle));
        }
        if (i != 1006) {
            return super.A2e(viewGroup, i);
        }
        return new C122435lS(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_text_row_item));
    }

    @Override // X.AbstractActivityC121465iB
    public void A2g(C127055ts r5) {
        Intent intent;
        int i;
        Intent intent2;
        AnonymousClass608 r2;
        super.A2g(r5);
        switch (r5.A00) {
            case 301:
                if (A2h()) {
                    intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
                    intent.putExtra("screen_name", "novipay_p_change_password");
                    i = 100;
                    startActivityForResult(intent, i);
                    return;
                }
                return;
            case 302:
                r2 = new AnonymousClass608(((ActivityC13830kP) this).A01, "718126525487171");
                intent2 = new Intent("android.intent.action.VIEW", r2.A01());
                startActivity(intent2);
                return;
            case 303:
                if (this.A07.A0H()) {
                    intent2 = WaBloksActivity.A09(this, "com.bloks.www.payments.whatsapp.novi.tpp", null);
                    startActivity(intent2);
                    return;
                }
                intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
                intent.putExtra("screen_name", "novipay_p_login_password");
                i = 101;
                startActivityForResult(intent, i);
                return;
            case 304:
            default:
                return;
            case 305:
                r2 = new AnonymousClass608(((ActivityC13830kP) this).A01);
                r2.A00.append("WA");
                intent2 = new Intent("android.intent.action.VIEW", r2.A01());
                startActivity(intent2);
                return;
        }
    }

    public final void A2i(Pair pair, SwitchCompat switchCompat, FingerprintBottomSheet fingerprintBottomSheet, AnonymousClass61D r8) {
        String str;
        C1315463e r0 = this.A07.A01;
        if (r0 == null) {
            str = null;
        } else {
            str = r0.A02;
        }
        r8.A02(pair, new IDxAListenerShape0S0300000_3_I1(switchCompat, fingerprintBottomSheet, this, 9), this.A08, str);
    }

    public final void A2j(SwitchCompat switchCompat) {
        String str;
        C128365vz r1 = new AnonymousClass610("BIOMETRICS_ENABLE_CLICK", "SECURITY_PRIVACY", "SECURITY_PRIVACY_LIST", "TOGGLE").A00;
        r1.A0i = "BIOMETRICS";
        r1.A0J = "TOUCH_ID";
        if (!switchCompat.isChecked() || this.A08.A02() != 1) {
            r1.A02 = Boolean.TRUE;
            r1.A0I = "disabled";
            this.A06.A05(r1);
            C130295zB.A00(this, C126995tm.A00(new Runnable(switchCompat, this) { // from class: X.6IJ
                public final /* synthetic */ SwitchCompat A00;
                public final /* synthetic */ NoviPayHubSecurityActivity A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    NoviPayHubSecurityActivity noviPayHubSecurityActivity = this.A01;
                    SwitchCompat switchCompat2 = this.A00;
                    C128365vz r12 = AnonymousClass610.A03("BIOMETRICS_MODAL_USE_CLICK", "SECURITY_PRIVACY", "SECURITY_PRIVACY_LIST").A00;
                    r12.A0i = "BIOMETRICS";
                    r12.A0J = "TOUCH_ID";
                    r12.A0L = noviPayHubSecurityActivity.getString(R.string.btn_continue);
                    noviPayHubSecurityActivity.A06.A05(r12);
                    Pair A03 = noviPayHubSecurityActivity.A08.A03();
                    if (!AnonymousClass61H.A04(noviPayHubSecurityActivity, ((ActivityC13810kN) noviPayHubSecurityActivity).A0C)) {
                        FingerprintBottomSheet A02 = AnonymousClass61H.A02();
                        A02.A1M(new C119435dy(A03, switchCompat2, A02, noviPayHubSecurityActivity));
                        noviPayHubSecurityActivity.Adm(A02);
                        return;
                    }
                    C05500Pu A01 = AnonymousClass61H.A01(noviPayHubSecurityActivity, new C117825am(A03, switchCompat2, noviPayHubSecurityActivity));
                    C05000Nw A00 = AnonymousClass61H.A00(noviPayHubSecurityActivity.getString(R.string.novi_biometrics_registration_start), noviPayHubSecurityActivity.getString(R.string.fingerprint_bottom_sheet_negative_button));
                    AnonymousClass0U4 A002 = AnonymousClass61E.A00();
                    if (A002 != null) {
                        A01.A01(A002, A00);
                    }
                }
            }, R.string.btn_continue), C126995tm.A00(new Runnable() { // from class: X.6Gf
                @Override // java.lang.Runnable
                public final void run() {
                    NoviPayHubSecurityActivity noviPayHubSecurityActivity = NoviPayHubSecurityActivity.this;
                    C128365vz r12 = AnonymousClass610.A03("BIOMETRICS_MODAL_CANCEL_CLICK", "SECURITY_PRIVACY", "SECURITY_PRIVACY_LIST").A00;
                    r12.A0i = "BIOMETRICS";
                    r12.A0J = "TOUCH_ID";
                    r12.A0L = noviPayHubSecurityActivity.getString(R.string.back);
                    noviPayHubSecurityActivity.A06.A05(r12);
                }
            }, R.string.not_now), getString(R.string.novi_biometric_enabled_title), getString(R.string.novi_biometric_enabled_message), true).show();
            C128365vz r12 = new AnonymousClass610("BIOMETRICS_MODAL_VPV", "SECURITY_PRIVACY", "SECURITY_PRIVACY_LIST", "MODAL").A00;
            r12.A0i = "BIOMETRICS";
            this.A06.A05(r12);
            return;
        }
        r1.A0X = "BIOMETRICS_DISABLE_CLICK";
        r1.A02 = Boolean.FALSE;
        r1.A0I = "enabled";
        this.A06.A05(r1);
        AnonymousClass61D r7 = this.A03;
        C1315463e r0 = this.A07.A01;
        if (r0 == null) {
            str = null;
        } else {
            str = r0.A02;
        }
        AnonymousClass61E r5 = this.A08;
        C130105yo r4 = ((AbstractActivityC121465iB) this).A00;
        IDxAListenerShape1S0200000_3_I1 A09 = C117305Zk.A09(switchCompat, this, 39);
        String str2 = AnonymousClass600.A03;
        C130155yt r3 = r7.A03;
        String A05 = r3.A05();
        long A00 = r7.A01.A00();
        String encodeToString = Base64.encodeToString(AnonymousClass61L.A03(r5.A09()), 2);
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("key_id", encodeToString);
            A0a.put("account_id", str);
            C117295Zj.A1L(str2, A05, A0a, A00);
        } catch (JSONException unused) {
            Log.e("PAY: IntentPayloadHelper/getBiometricRevokeKeyIntentPayload/toJson can't construct json");
        }
        C130125yq r8 = r7.A04;
        C129585xx r72 = new C129585xx(r8, "REVOKE_BIOMETRIC_KEY", A0a);
        AnonymousClass61S[] r2 = new AnonymousClass61S[2];
        AnonymousClass61S.A04("action", "novi-revoke-biometric-key", r2);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("biometric_key_id", encodeToString), r2, 1);
        C117305Zk.A1K(A0F, "revoke_biometric_key_intent", AnonymousClass61S.A02("value", r72.A01(r8.A02())));
        C130155yt.A01(new IDxAListenerShape0S0300000_3_I1(r4, A09, r5, 1), r3, A0F);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 100) {
            if (i2 == -1) {
                finish();
            }
        } else if (i == 101 && i2 == -1) {
            startActivity(WaBloksActivity.A09(this, "com.bloks.www.payments.whatsapp.novi.tpp", null));
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C123365n2 r2 = (C123365n2) C117315Zl.A06(new C118255bW(((AbstractActivityC121465iB) this).A01), this).A00(C123365n2.class);
        this.A0A = r2;
        ((AbstractC118045bB) r2).A00.A05(this, C117305Zk.A0B(this, 92));
        C123365n2 r22 = this.A0A;
        ((AbstractC118045bB) r22).A01.A05(this, C117305Zk.A0B(this, 90));
        C117295Zj.A0r(this, this.A0A.A00, 89);
        AbstractActivityC119265dR.A0B(this, this.A0A);
        C117295Zj.A0r(this, this.A07.A0G, 91);
        this.A04 = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A01);
        this.A03 = new AnonymousClass61D(this.A00, ((ActivityC13790kL) this).A05, ((ActivityC13810kN) this).A0C, this.A02, this.A05, this.A09);
    }
}
