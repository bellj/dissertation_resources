package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121505iP;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass69E;
import X.C117295Zj;
import X.C117305Zk;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiProvideMoreInfoActivity extends AbstractActivityC121505iP {
    public boolean A00;

    public IndiaUpiProvideMoreInfoActivity() {
        this(0);
    }

    public IndiaUpiProvideMoreInfoActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 67);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            ((AbstractActivityC121505iP) this).A04 = (AnonymousClass69E) A1M.A9W.get();
            ((AbstractActivityC121505iP) this).A00 = C117305Zk.A0G(A1M);
            ((AbstractActivityC121505iP) this).A02 = C117305Zk.A0N(A1M);
        }
    }

    @Override // X.AbstractActivityC121505iP, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_account_recovery_info);
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            C117295Zj.A0g(this, A0K, R.string.payments_activity_title);
        }
        C117295Zj.A0n(findViewById(R.id.account_recovery_info_continue), this, 65);
    }
}
