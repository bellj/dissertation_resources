package com.whatsapp.payments.ui;

import X.AbstractC16870pt;
import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.AnonymousClass69B;
import X.C117295Zj;
import X.C117305Zk;
import X.C119865fE;
import X.C128345vx;
import X.C130065yk;
import X.C1329568x;
import X.C25841Ba;
import X.C25891Bf;
import X.C51062Sr;
import X.C51082St;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;

/* loaded from: classes4.dex */
public abstract class Hilt_BrazilPaymentSettingsFragment extends PaymentSettingsFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    private void A01() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A01();
        return this.A00;
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A01()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.Hilt_BrazilPaymentSettingsFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A01();
        A1I();
    }

    @Override // com.whatsapp.payments.ui.Hilt_PaymentSettingsFragment, com.whatsapp.base.Hilt_WaDialogFragment
    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            BrazilPaymentSettingsFragment brazilPaymentSettingsFragment = (BrazilPaymentSettingsFragment) this;
            AnonymousClass01J A0C = C117295Zj.A0C(brazilPaymentSettingsFragment, (AbstractC51092Su) generatedComponent());
            C117295Zj.A16(A0C, brazilPaymentSettingsFragment);
            C117295Zj.A17(A0C, brazilPaymentSettingsFragment, C117295Zj.A0Q(A0C, brazilPaymentSettingsFragment));
            C117295Zj.A15(A0C, brazilPaymentSettingsFragment);
            brazilPaymentSettingsFragment.A00 = (C25841Ba) A0C.A1d.get();
            brazilPaymentSettingsFragment.A02 = (C1329568x) A0C.A1o.get();
            brazilPaymentSettingsFragment.A07 = (AnonymousClass69B) A0C.A1r.get();
            brazilPaymentSettingsFragment.A01 = C117305Zk.A0G(A0C);
            brazilPaymentSettingsFragment.A05 = (AbstractC16870pt) A0C.A20.get();
            brazilPaymentSettingsFragment.A04 = C117305Zk.A0N(A0C);
            brazilPaymentSettingsFragment.A03 = (C119865fE) A0C.AEV.get();
            brazilPaymentSettingsFragment.A08 = (C130065yk) A0C.A1z.get();
            brazilPaymentSettingsFragment.A0A = (C128345vx) A0C.A23.get();
            brazilPaymentSettingsFragment.A06 = (C25891Bf) A0C.A1w.get();
        }
    }
}
