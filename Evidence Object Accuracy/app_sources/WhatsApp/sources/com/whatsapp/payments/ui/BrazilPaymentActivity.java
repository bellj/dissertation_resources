package com.whatsapp.payments.ui;

import X.AbstractActivityC119645em;
import X.AbstractActivityC121435hw;
import X.AbstractActivityC121685jC;
import X.AbstractC1310961f;
import X.AbstractC136516Mv;
import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16870pt;
import X.AbstractC28901Pl;
import X.AbstractC30791Yv;
import X.AbstractC30871Zd;
import X.AbstractC35651iS;
import X.ActivityC000800j;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass102;
import X.AnonymousClass17Z;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1KC;
import X.AnonymousClass1KS;
import X.AnonymousClass1Q5;
import X.AnonymousClass1ZO;
import X.AnonymousClass1ZY;
import X.AnonymousClass20C;
import X.AnonymousClass2S0;
import X.AnonymousClass4UZ;
import X.AnonymousClass607;
import X.AnonymousClass60T;
import X.AnonymousClass60Z;
import X.AnonymousClass617;
import X.AnonymousClass61E;
import X.AnonymousClass61I;
import X.AnonymousClass69D;
import X.AnonymousClass69P;
import X.AnonymousClass6CL;
import X.AnonymousClass6CN;
import X.AnonymousClass6D0;
import X.AnonymousClass6M9;
import X.C117305Zk;
import X.C117315Zl;
import X.C118025b9;
import X.C119425dx;
import X.C119815f9;
import X.C120075fa;
import X.C121255hW;
import X.C124205om;
import X.C125805rq;
import X.C126095sK;
import X.C127135u0;
import X.C127145u1;
import X.C127155u2;
import X.C127505ub;
import X.C127715uw;
import X.C127785v3;
import X.C128295vs;
import X.C128305vt;
import X.C129095xA;
import X.C129135xE;
import X.C129385xd;
import X.C12960it;
import X.C12970iu;
import X.C129735yD;
import X.C12990iw;
import X.C129925yW;
import X.C129945yY;
import X.C130015yf;
import X.C130065yk;
import X.C1309660r;
import X.C1311161i;
import X.C1329568x;
import X.C133326Ai;
import X.C134066De;
import X.C134146Dm;
import X.C14580lf;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15380n4;
import X.C15570nT;
import X.C15610nY;
import X.C16630pM;
import X.C17070qD;
import X.C18590sh;
import X.C18600si;
import X.C18610sj;
import X.C18620sk;
import X.C18640sm;
import X.C18650sn;
import X.C18660so;
import X.C20350vc;
import X.C22460z7;
import X.C22540zF;
import X.C243515e;
import X.C248217a;
import X.C25871Bd;
import X.C28861Ph;
import X.C30771Yt;
import X.C30821Yy;
import X.C30921Zi;
import X.C38171nd;
import X.C50942Ry;
import X.C50952Rz;
import X.C94074bD;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.MenuItem;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.jid.UserJid;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;
import com.whatsapp.payments.CheckFirstTransaction;
import com.whatsapp.payments.ui.AddPaymentMethodBottomSheet;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentSettingsActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public class BrazilPaymentActivity extends AbstractActivityC121435hw implements AbstractC136516Mv, AbstractC1310961f, AnonymousClass6M9 {
    public int A00;
    public Context A01;
    public C14580lf A02;
    public C15610nY A03;
    public AnonymousClass018 A04;
    public AnonymousClass102 A05;
    public C1329568x A06;
    public C1309660r A07;
    public AnonymousClass69D A08;
    public AnonymousClass60Z A09;
    public CheckFirstTransaction A0A;
    public C129925yW A0B;
    public C248217a A0C;
    public C18660so A0D;
    public C18600si A0E;
    public C243515e A0F;
    public C18620sk A0G;
    public C22540zF A0H;
    public C22460z7 A0I;
    public AnonymousClass60T A0J;
    public AbstractC16870pt A0K;
    public C129385xd A0L;
    public C129095xA A0M;
    public C121255hW A0N;
    public AnonymousClass61E A0O;
    public C130015yf A0P;
    public C25871Bd A0Q;
    public C129945yY A0R;
    public C130065yk A0S;
    public ConfirmPaymentFragment A0T;
    public C129735yD A0U;
    public PaymentView A0V;
    public C18590sh A0W;
    public C16630pM A0X;
    public String A0Y;
    public String A0Z;
    public boolean A0a = false;
    public final AnonymousClass4UZ A0b = new C120075fa(this);
    public final AbstractC35651iS A0c = new AnonymousClass69P(this);

    @Override // X.AbstractC136516Mv
    public ActivityC000800j AAa() {
        return this;
    }

    @Override // X.AbstractC136516Mv
    public String AFG() {
        return null;
    }

    @Override // X.AbstractC136516Mv
    public boolean AK3() {
        return false;
    }

    @Override // X.AbstractC1310961f
    public void ALy() {
    }

    @Override // X.AbstractC136486Ms
    public void AMA(String str) {
    }

    @Override // X.AbstractC1310961f
    public void ATV() {
    }

    @Override // X.AbstractC1310961f
    public /* synthetic */ void ATa() {
    }

    @Override // X.AbstractC1310961f
    public void AVs() {
    }

    public static /* synthetic */ void A02(BottomSheetDialogFragment bottomSheetDialogFragment, BrazilPaymentActivity brazilPaymentActivity) {
        AlertDialog create = new AlertDialog.Builder(brazilPaymentActivity).setMessage(brazilPaymentActivity.A01.getString(R.string.notification_payment_step_up_required_message)).setPositiveButton(brazilPaymentActivity.A01.getString(R.string.btn_continue), new DialogInterface.OnClickListener(bottomSheetDialogFragment, brazilPaymentActivity) { // from class: X.62P
            public final /* synthetic */ BottomSheetDialogFragment A00;
            public final /* synthetic */ BrazilPaymentActivity A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BrazilPaymentActivity brazilPaymentActivity2 = this.A01;
                this.A00.A1C();
                brazilPaymentActivity2.startActivity(C12990iw.A0D(brazilPaymentActivity2, BrazilPaymentSettingsActivity.class));
            }
        }).create();
        create.setOnDismissListener(new IDxDListenerShape14S0100000_3_I1(bottomSheetDialogFragment, 3));
        create.show();
    }

    public static /* synthetic */ boolean A03(AbstractC28901Pl r4, int i) {
        AbstractC30871Zd r3 = (AbstractC30871Zd) r4.A08;
        if (r3 == null || !C1311161i.A0B(r4) || i != 1) {
            return false;
        }
        String str = r3.A0N;
        if (str == null || !(!"DISABLED".equals(str))) {
            return true;
        }
        return false;
    }

    public C38171nd A2o() {
        if (!(this instanceof BrazilOrderDetailsActivity)) {
            return null;
        }
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = (BrazilOrderDetailsActivity) this;
        String str = brazilOrderDetailsActivity.A0G;
        AnonymousClass009.A05(str);
        return new C38171nd(str, brazilOrderDetailsActivity.A0E.A01, brazilOrderDetailsActivity.A00);
    }

    public AnonymousClass607 A2p(AbstractC28901Pl r43, AnonymousClass20C r44, String str, String str2, String str3) {
        String str4;
        AnonymousClass1ZY r12;
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        C15570nT r04 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r05 = ((ActivityC13830kP) this).A05;
        C129135xE r06 = super.A0Q;
        C17070qD r14 = super.A0P;
        C18590sh r10 = this.A0W;
        C129385xd r9 = this.A0L;
        C130015yf r8 = this.A0P;
        C18610sj r7 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass17Z r6 = super.A0T;
        C18640sm r5 = ((ActivityC13810kN) this).A07;
        C18650sn r4 = ((AbstractActivityC121685jC) this).A0K;
        AnonymousClass61E r3 = this.A0O;
        AnonymousClass60T r2 = this.A0J;
        String str5 = r43.A0A;
        UserJid userJid = ((AbstractActivityC121685jC) this).A0G;
        AnonymousClass009.A05(userJid);
        if (!"p2m".equals(str) || r43.A04() != 6 || (r12 = r43.A08) == null) {
            str4 = null;
        } else {
            str4 = ((AbstractC30871Zd) r12).A03 == 1 ? "debit" : "credit";
        }
        return new AnonymousClass607(this, r03, r04, r5, r0, r44, r44, A2o(), r02, userJid, r4, r7, r14, r06, r2, r9, r6, A2q(r44.A02, ((AbstractActivityC121685jC) this).A01), r3, r8, r10, r05, str5, str3, str4, str, str2);
    }

    public C50952Rz A2q(C30821Yy r5, int i) {
        C50942Ry r2;
        if (i == 0 && (r2 = super.A0T.A00().A01) != null) {
            if (r5.A00.compareTo(r2.A09.A00.A02.A00) >= 0) {
                return r2.A08;
            }
        }
        return null;
    }

    public final AddPaymentMethodBottomSheet A2r(String str) {
        boolean A08 = ((AbstractActivityC121685jC) this).A0O.A08();
        Context context = this.A01;
        int i = R.string.accept_payment_add_debit_bottom_sheet_desc;
        if (A08) {
            i = R.string.send_payment_add_method_bottom_sheet_desc;
        }
        String string = context.getString(i);
        Intent A0D = C12990iw.A0D(this, BrazilPayBloksActivity.class);
        A0D.putExtra("screen_name", str);
        A0D.putExtra("hide_send_payment_cta", true);
        AbstractActivityC119645em.A0O(A0D, "referral_screen", "get_started");
        this.A0S.A03(A0D, "p2m_context");
        C127715uw r2 = new C127715uw(A0D, null, string, null);
        AddPaymentMethodBottomSheet addPaymentMethodBottomSheet = new AddPaymentMethodBottomSheet();
        addPaymentMethodBottomSheet.A0U(C12970iu.A0D());
        addPaymentMethodBottomSheet.A04 = r2;
        return addPaymentMethodBottomSheet;
    }

    public final void A2s(C30821Yy r17, AbstractC28901Pl r18) {
        AnonymousClass1KS r10;
        C30921Zi r6;
        AbstractC15340mz r9;
        Integer num;
        C14580lf A01;
        AnonymousClass1ZO r2;
        int i;
        PaymentView paymentView = this.A0V;
        if (paymentView != null) {
            r10 = paymentView.getStickerIfSelected();
        } else {
            r10 = null;
        }
        AnonymousClass2S0 r12 = null;
        if (paymentView != null) {
            r6 = paymentView.getPaymentBackground();
        } else {
            r6 = null;
        }
        if (r10 == null && r6 == null) {
            A01 = null;
        } else {
            C20350vc r5 = super.A0S;
            AbstractC14640lm r7 = ((AbstractActivityC121685jC) this).A0E;
            AnonymousClass009.A05(r7);
            UserJid userJid = ((AbstractActivityC121685jC) this).A0G;
            long j = ((AbstractActivityC121685jC) this).A02;
            if (j != 0) {
                r9 = ((AbstractActivityC121685jC) this).A09.A0K.A00(j);
            } else {
                r9 = null;
            }
            PaymentView paymentView2 = this.A0V;
            if (paymentView2 != null) {
                num = paymentView2.getStickerSendOrigin();
            } else {
                num = null;
            }
            A01 = r5.A01(r6, r7, userJid, r9, r10, num);
        }
        AbstractC30791Yv A02 = this.A05.A02("BRL");
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        if (((AbstractActivityC121685jC) this).A0G != null) {
            C17070qD r0 = super.A0P;
            r0.A03();
            r2 = r0.A09.A05(((AbstractActivityC121685jC) this).A0G);
        } else {
            r2 = null;
        }
        C118025b9 r02 = super.A0X;
        if (!(r02 == null || r02.A00.A01() == null)) {
            r12 = (AnonymousClass2S0) ((AnonymousClass617) super.A0X.A00.A01()).A01;
        }
        UserJid userJid2 = ((AbstractActivityC121685jC) this).A0G;
        AnonymousClass009.A05(userJid2);
        if (r2 == null || r2.A05 == null || !r2.A0C()) {
            i = 1;
        } else {
            i = r2.A05();
        }
        ConfirmPaymentFragment A00 = ConfirmPaymentFragment.A00(r18, userJid2, i);
        paymentBottomSheet.A01 = A00;
        A00.A0M = new AnonymousClass6CN(A01, r17, r12, this, A00, paymentBottomSheet);
        A00.A0L = new AnonymousClass6CL(A02, r17, r18, r12, this, A00);
        this.A0T = A00;
        this.A0N.AL9("confirm_payment", this.A00);
        Adm(paymentBottomSheet);
    }

    public final void A2t(C30821Yy r18, AbstractC28901Pl r19, AnonymousClass1KC r20, String str, String str2) {
        FingerprintBottomSheet A0D = C117305Zk.A0D();
        int intValue = r18.A00.scaleByPowerOfTen(3).intValue();
        AbstractC30791Yv r3 = C30771Yt.A04;
        C94074bD r2 = new C94074bD();
        r2.A02 = (long) intValue;
        r2.A01 = 1000;
        r2.A03 = r3;
        A0D.A05 = new C119425dx(this, A0D, ((ActivityC13790kL) this).A05, A2p(r19, r2.A00(), str2, "fingerprint", this.A0Z), this.A0P, new C133326Ai(A0D, r18, r19, r20, this, str, str2));
        this.A0N.AL9("enter_fingerprint", this.A00);
        Adm(A0D);
    }

    public void A2u(C30821Yy r12, AbstractC28901Pl r13, AnonymousClass1KC r14, String str, String str2, String str3, int i) {
        String paymentNote;
        List mentionedJids;
        PaymentView paymentView = this.A0V;
        if (paymentView == null) {
            mentionedJids = C12960it.A0l();
            paymentNote = "";
        } else {
            paymentNote = paymentView.getPaymentNote();
            mentionedJids = this.A0V.getMentionedJids();
        }
        C28861Ph A2f = A2f(paymentNote, mentionedJids);
        C119815f9 r6 = new C119815f9();
        r6.A02 = str;
        r6.A04 = A2f.A0z.A01;
        r6.A03 = this.A0W.A01();
        A2w(r6, i);
        CheckFirstTransaction checkFirstTransaction = this.A0A;
        if (checkFirstTransaction != null) {
            checkFirstTransaction.A00.A00(new AbstractC14590lg(r12, r13, r14, r6, this, A2f, str2, str3) { // from class: X.6En
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ AbstractC28901Pl A01;
                public final /* synthetic */ AnonymousClass1KC A02;
                public final /* synthetic */ C119815f9 A03;
                public final /* synthetic */ BrazilPaymentActivity A04;
                public final /* synthetic */ C28861Ph A05;
                public final /* synthetic */ String A06;
                public final /* synthetic */ String A07;

                {
                    this.A04 = r5;
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A06 = r7;
                    this.A07 = r8;
                    this.A02 = r3;
                    this.A05 = r6;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    BrazilPaymentActivity brazilPaymentActivity = this.A04;
                    C119815f9 r62 = this.A03;
                    C30821Yy r3 = this.A00;
                    AbstractC28901Pl r4 = this.A01;
                    String str4 = this.A06;
                    String str5 = this.A07;
                    AnonymousClass1KC r5 = this.A02;
                    C28861Ph r8 = this.A05;
                    r62.A01 = (Boolean) obj;
                    AbstractC30791Yv A02 = brazilPaymentActivity.A05.A02("BRL");
                    C12960it.A1E(new C124205om(A02, r3, r4, r5, r62, brazilPaymentActivity, r8, str4, str5), ((ActivityC13830kP) brazilPaymentActivity).A05);
                }
            });
            return;
        }
        AbstractC30791Yv A02 = this.A05.A02("BRL");
        C12960it.A1E(new C124205om(A02, r12, r13, r14, r6, this, A2f, str2, str3), ((ActivityC13830kP) this).A05);
    }

    public void A2v(C30821Yy r4, boolean z) {
        this.A02.A04();
        C14580lf A0C = C117305Zk.A0C(super.A0P);
        this.A02 = A0C;
        C117315Zl.A0R(((ActivityC13810kN) this).A05, A0C, new AbstractC14590lg(r4, this, z) { // from class: X.6Eh
            public final /* synthetic */ C30821Yy A00;
            public final /* synthetic */ BrazilPaymentActivity A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
                if (r6 != null) goto L_0x0057;
             */
            @Override // X.AbstractC14590lg
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void accept(java.lang.Object r9) {
                /*
                    r8 = this;
                    r6 = r9
                    com.whatsapp.payments.ui.BrazilPaymentActivity r5 = r8.A01
                    boolean r2 = r8.A02
                    X.1Yy r3 = r8.A00
                    java.util.List r6 = (java.util.List) r6
                    X.0zW r0 = r5.A0O
                    boolean r0 = r0.A08()
                    if (r0 == 0) goto L_0x0017
                    X.1nd r0 = r5.A2o()
                    if (r0 != 0) goto L_0x002b
                L_0x0017:
                    X.0zW r1 = r5.A0O
                    boolean r0 = r1.A08()
                    if (r0 == 0) goto L_0x003e
                    X.0m9 r1 = r1.A03
                    r0 = 1746(0x6d2, float:2.447E-42)
                    boolean r0 = r1.A07(r0)
                    if (r0 == 0) goto L_0x003e
                    if (r2 == 0) goto L_0x003e
                L_0x002b:
                    r7 = 0
                L_0x002c:
                    int r0 = r6.size()
                    if (r7 >= r0) goto L_0x0056
                    X.1Pl r0 = X.C117315Zl.A08(r6, r7)
                    int r1 = r0.A03
                    r0 = 2
                    if (r1 == r0) goto L_0x0057
                    int r7 = r7 + 1
                    goto L_0x002c
                L_0x003e:
                    int r7 = X.C1311161i.A01(r6)
                    if (r6 == 0) goto L_0x0045
                    goto L_0x0057
                L_0x0045:
                    java.lang.String r0 = "brpay_p_add_card"
                    com.whatsapp.payments.ui.AddPaymentMethodBottomSheet r4 = r5.A2r(r0)
                    X.6K5 r2 = new X.6K5
                    r2.<init>(r3, r4, r5, r6, r7)
                    r4.A05 = r2
                    r5.Adm(r4)
                    goto L_0x0067
                L_0x0056:
                    r7 = 0
                L_0x0057:
                    int r0 = r6.size()
                    if (r0 <= 0) goto L_0x0045
                    X.1Pl r0 = X.C117315Zl.A08(r6, r7)
                    X.AnonymousClass009.A05(r0)
                    r5.A2s(r3, r0)
                L_0x0067:
                    X.0lf r0 = r5.A02
                    r0.A04()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C134356Eh.accept(java.lang.Object):void");
            }
        });
    }

    public void A2w(C119815f9 r2, int i) {
        if (i == 1) {
            A2k(r2);
        }
    }

    @Override // X.AbstractC136516Mv
    public boolean AJr() {
        return TextUtils.isEmpty(this.A0l);
    }

    @Override // X.AbstractC136486Ms
    public void APq(String str) {
        C121255hW r4 = this.A0N;
        int i = this.A00;
        AnonymousClass1Q5 A00 = r4.A00(Integer.valueOf(i), "p2p_flow_tag");
        if (A00 != null) {
            A00.A07.AL0("error_message", str, A00.A06.A05, i);
        }
        AnonymousClass61I.A02(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, super.A0U, null, true), this.A0K, "new_payment");
    }

    @Override // X.AbstractC136486Ms
    public void AQf(String str, boolean z) {
        if (!TextUtils.isEmpty(str) && !z) {
            A2l(this.A0K, super.A0U);
        }
    }

    @Override // X.AbstractC1310961f
    public void AR5() {
        AnonymousClass2S0 r3 = super.A0U;
        if (r3 != null && r3.A01 != null) {
            AbstractC16870pt r2 = this.A0K;
            Bundle A0D = C12970iu.A0D();
            PaymentIncentiveViewFragment paymentIncentiveViewFragment = new PaymentIncentiveViewFragment(r2, r3);
            paymentIncentiveViewFragment.A0U(A0D);
            paymentIncentiveViewFragment.A05 = new C125805rq(paymentIncentiveViewFragment);
            Adm(paymentIncentiveViewFragment);
        }
    }

    @Override // X.AbstractC1310961f
    public void ATT() {
        AbstractC14640lm r0 = ((AbstractActivityC121685jC) this).A0E;
        AnonymousClass009.A05(r0);
        if (C15380n4.A0J(r0) && ((AbstractActivityC121685jC) this).A00 == 0) {
            A2i(C12990iw.A0H(this));
        }
    }

    @Override // X.AbstractC1310961f
    public void AV5(C30821Yy r8, String str) {
        String A01 = this.A0S.A01();
        if (A01 == null) {
            this.A02.A00(new AbstractC14590lg(r8, this) { // from class: X.6EV
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ BrazilPaymentActivity A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    BrazilPaymentActivity brazilPaymentActivity = this.A01;
                    C30821Yy r3 = this.A00;
                    List list = (List) obj;
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        AbstractC28901Pl A0H = C117305Zk.A0H(it);
                        if (C1311161i.A0B(A0H) && A0H.A08 != null && A0H.A00 == 2) {
                            brazilPaymentActivity.A2j(r3);
                            return;
                        }
                    }
                    if (list.size() > 0) {
                        ((ActivityC13810kN) brazilPaymentActivity).A05.A0H(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003e: INVOKE  
                              (wrap: X.0mE : 0x0037: IGET  (r1v0 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.0kN) (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity)) X.0kN.A05 X.0mE)
                              (wrap: X.6I5 : 0x003b: CONSTRUCTOR  (r0v4 X.6I5 A[REMOVE]) = 
                              (wrap: X.1Ze : 0x0035: CHECK_CAST (r2v2 X.1Ze A[REMOVE]) = (X.1Ze) (wrap: java.lang.Object : 0x0031: INVOKE  (r2v1 java.lang.Object A[REMOVE]) = 
                              (r6v1 'list' java.util.List)
                              (wrap: int : 0x002d: INVOKE  (r0v3 int A[REMOVE]) = (r6v1 'list' java.util.List) type: STATIC call: X.61i.A01(java.util.List):int)
                             type: INTERFACE call: java.util.List.get(int):java.lang.Object))
                              (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity)
                             call: X.6I5.<init>(X.1Ze, com.whatsapp.payments.ui.BrazilPaymentActivity):void type: CONSTRUCTOR)
                             type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6EV.accept(java.lang.Object):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003b: CONSTRUCTOR  (r0v4 X.6I5 A[REMOVE]) = 
                              (wrap: X.1Ze : 0x0035: CHECK_CAST (r2v2 X.1Ze A[REMOVE]) = (X.1Ze) (wrap: java.lang.Object : 0x0031: INVOKE  (r2v1 java.lang.Object A[REMOVE]) = 
                              (r6v1 'list' java.util.List)
                              (wrap: int : 0x002d: INVOKE  (r0v3 int A[REMOVE]) = (r6v1 'list' java.util.List) type: STATIC call: X.61i.A01(java.util.List):int)
                             type: INTERFACE call: java.util.List.get(int):java.lang.Object))
                              (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity)
                             call: X.6I5.<init>(X.1Ze, com.whatsapp.payments.ui.BrazilPaymentActivity):void type: CONSTRUCTOR in method: X.6EV.accept(java.lang.Object):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 21 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6I5, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 27 more
                            */
                        /*
                            this = this;
                            com.whatsapp.payments.ui.BrazilPaymentActivity r4 = r5.A01
                            X.1Yy r3 = r5.A00
                            java.util.List r6 = (java.util.List) r6
                            java.util.Iterator r2 = r6.iterator()
                        L_0x000a:
                            boolean r0 = r2.hasNext()
                            if (r0 == 0) goto L_0x0027
                            X.1Pl r1 = X.C117305Zk.A0H(r2)
                            boolean r0 = X.C1311161i.A0B(r1)
                            if (r0 == 0) goto L_0x000a
                            X.1ZY r0 = r1.A08
                            if (r0 == 0) goto L_0x000a
                            int r1 = r1.A00
                            r0 = 2
                            if (r1 != r0) goto L_0x000a
                            r4.A2j(r3)
                            return
                        L_0x0027:
                            int r0 = r6.size()
                            if (r0 <= 0) goto L_0x0042
                            int r0 = X.C1311161i.A01(r6)
                            java.lang.Object r2 = r6.get(r0)
                            X.1Ze r2 = (X.C30881Ze) r2
                            X.0mE r1 = r4.A05
                            X.6I5 r0 = new X.6I5
                            r0.<init>(r2, r4)
                            r1.A0H(r0)
                            return
                        L_0x0042:
                            java.lang.String r0 = "PAY: BrazilPaymentActivity/onRequestPayment: Can't launch ConfirmReceiveFragment"
                            com.whatsapp.util.Log.e(r0)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6EV.accept(java.lang.Object):void");
                    }
                });
                return;
            }
            String string = this.A01.getString(R.string.add_debit_card_title);
            String string2 = this.A01.getString(R.string.add_debit_card_education);
            String string3 = this.A01.getString(R.string.add_debit_card_button);
            Intent A0D = C12990iw.A0D(this, BrazilPayBloksActivity.class);
            A0D.putExtra("screen_name", A01);
            A0D.putExtra("hide_send_payment_cta", true);
            AbstractActivityC119645em.A0O(A0D, "referral_screen", "get_started");
            HashMap A11 = C12970iu.A11();
            A11.put("verification_needed", "0");
            A11.put("add_debit_only", "1");
            A0D.putExtra("screen_params", A11);
            this.A0S.A03(A0D, "p2p_context");
            C127715uw r2 = new C127715uw(A0D, string, string2, string3);
            AddPaymentMethodBottomSheet addPaymentMethodBottomSheet = new AddPaymentMethodBottomSheet();
            addPaymentMethodBottomSheet.A0U(C12970iu.A0D());
            addPaymentMethodBottomSheet.A04 = r2;
            addPaymentMethodBottomSheet.A05 = new Runnable(r8, this) { // from class: X.6I4
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ BrazilPaymentActivity A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.A01.A2j(this.A00);
                }
            };
            Adm(addPaymentMethodBottomSheet);
        }

        @Override // X.AbstractC1310961f
        public void AVp(C30821Yy r4) {
            String A01 = this.A0S.A01();
            if (A01 != null) {
                AddPaymentMethodBottomSheet A2r = A2r(A01);
                A2r.A05 = new Runnable(r4, A2r, this) { // from class: X.6JV
                    public final /* synthetic */ C30821Yy A00;
                    public final /* synthetic */ AddPaymentMethodBottomSheet A01;
                    public final /* synthetic */ BrazilPaymentActivity A02;

                    {
                        this.A02 = r3;
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        BrazilPaymentActivity brazilPaymentActivity = this.A02;
                        AddPaymentMethodBottomSheet addPaymentMethodBottomSheet = this.A01;
                        C30821Yy r3 = this.A00;
                        addPaymentMethodBottomSheet.A1B();
                        if (brazilPaymentActivity.A2n()) {
                            brazilPaymentActivity.A2C(R.string.register_wait_message);
                            C117315Zl.A0R(((ActivityC13810kN) brazilPaymentActivity).A05, ((AbstractActivityC121685jC) brazilPaymentActivity).A04, 
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001e: INVOKE  
                                  (wrap: X.0mE : 0x001c: IGET  (r0v4 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.0kN) (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity)) X.0kN.A05 X.0mE)
                                  (wrap: X.0lf : 0x0015: IGET  (r2v0 X.0lf A[REMOVE]) = (wrap: ?? : ?: CAST (X.5jC) (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity)) X.5jC.A04 X.0lf)
                                  (wrap: X.6ET : 0x0019: CONSTRUCTOR  (r1v0 X.6ET A[REMOVE]) = (r3v0 'r3' X.1Yy), (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity) call: X.6ET.<init>(X.1Yy, com.whatsapp.payments.ui.BrazilPaymentActivity):void type: CONSTRUCTOR)
                                 type: STATIC call: X.5Zl.A0R(X.0mE, X.0lf, X.0lg):void in method: X.6JV.run():void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: CONSTRUCTOR  (r1v0 X.6ET A[REMOVE]) = (r3v0 'r3' X.1Yy), (r4v0 'brazilPaymentActivity' com.whatsapp.payments.ui.BrazilPaymentActivity) call: X.6ET.<init>(X.1Yy, com.whatsapp.payments.ui.BrazilPaymentActivity):void type: CONSTRUCTOR in method: X.6JV.run():void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 23 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6ET, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 29 more
                                */
                            /*
                                this = this;
                                com.whatsapp.payments.ui.BrazilPaymentActivity r4 = r5.A02
                                com.whatsapp.payments.ui.AddPaymentMethodBottomSheet r0 = r5.A01
                                X.1Yy r3 = r5.A00
                                r0.A1B()
                                boolean r0 = r4.A2n()
                                if (r0 == 0) goto L_0x0022
                                r0 = 2131891267(0x7f121443, float:1.941725E38)
                                r4.A2C(r0)
                                X.0lf r2 = r4.A04
                                X.6ET r1 = new X.6ET
                                r1.<init>(r3, r4)
                                X.0mE r0 = r4.A05
                                X.C117315Zl.A0R(r0, r2, r1)
                                return
                            L_0x0022:
                                r0 = 0
                                r4.A2v(r3, r0)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6JV.run():void");
                        }
                    };
                    Adm(A2r);
                } else if (A2n()) {
                    A2C(R.string.register_wait_message);
                    C117315Zl.A0R(((ActivityC13810kN) this).A05, ((AbstractActivityC121685jC) this).A04, new AbstractC14590lg(r4, this) { // from class: X.6EU
                        public final /* synthetic */ C30821Yy A00;
                        public final /* synthetic */ BrazilPaymentActivity A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            String str;
                            BrazilPaymentActivity brazilPaymentActivity = this.A01;
                            C30821Yy r2 = this.A00;
                            brazilPaymentActivity.AaN();
                            boolean booleanValue = ((Boolean) obj).booleanValue();
                            if (booleanValue) {
                                str = "p2m";
                            } else {
                                str = "p2p";
                            }
                            brazilPaymentActivity.A0o = str;
                            brazilPaymentActivity.A2v(r2, booleanValue);
                            ((AbstractActivityC121685jC) brazilPaymentActivity).A04.A04();
                        }
                    });
                } else {
                    A2v(r4, false);
                }
            }

            @Override // X.AbstractC1310961f
            public void AVq() {
                AbstractActivityC121685jC.A1o(this, this.A0K, super.A0U, 47);
            }

            @Override // X.AbstractC1310961f
            public void AXP(boolean z) {
                int i;
                AnonymousClass2S0 r2 = super.A0U;
                AbstractC16870pt r1 = this.A0K;
                if (z) {
                    i = 49;
                } else {
                    i = 48;
                }
                AbstractActivityC121685jC.A1o(this, r1, r2, i);
            }

            @Override // X.AnonymousClass6M9
            public /* bridge */ /* synthetic */ Object AZb() {
                AbstractC30791Yv A02 = this.A05.A02("BRL");
                AbstractC14640lm r0 = ((AbstractActivityC121685jC) this).A0E;
                String str = this.A0h;
                AnonymousClass1KS r14 = super.A0c;
                Integer num = this.A0e;
                String str2 = this.A0n;
                int i = 2;
                if (this.A0s) {
                    i = 0;
                }
                C127155u2 r3 = new C127155u2(i, 0);
                C126095sK r1 = new C126095sK(false);
                C127135u0 r02 = new C127135u0(NumberEntryKeyboard.A00(this.A04), this.A0q);
                String str3 = this.A0l;
                String str4 = this.A0i;
                String str5 = this.A0k;
                C127505ub r4 = new C127505ub(A02, null, 0);
                AnonymousClass018 r7 = this.A04;
                C30821Yy AEA = A02.AEA();
                C128295vs r21 = new C128295vs(new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolAfterAmount), new int[]{0, 0, 0, 0}), new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolBeforeAmount), new int[]{0, 0, 0, 0}), r4, new AnonymousClass6D0(this, r7, A02, AEA, A02.AEV(), AEA, null), null, str3, str4, str5, R.style.SendPaymentAmountInput, true, true, true);
                C14850m9 r11 = ((ActivityC13810kN) this).A0C;
                AnonymousClass19M r9 = ((ActivityC13810kN) this).A0B;
                AnonymousClass01d r8 = ((ActivityC13810kN) this).A08;
                AnonymousClass018 r72 = this.A04;
                AnonymousClass1AB r6 = this.A0d;
                C134146Dm r18 = new C134146Dm(this, r8, r72, r9, r11, new C134066De(), this.A0X, r6);
                C127145u1 r73 = new C127145u1(this, r11.A07(811));
                C22460z7 r92 = this.A0I;
                return new C128305vt(r0, r18, this, this, r21, new C127785v3(((AbstractActivityC121685jC) this).A0C, this.A0H, r92, false), r02, r1, r73, r3, r14, num, str, str2, false);
            }

            @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
            public void onActivityResult(int i, int i2, Intent intent) {
                if (i != 1) {
                    super.onActivityResult(i, i2, intent);
                    return;
                }
                this.A02.A04();
                C14580lf A0C = C117305Zk.A0C(super.A0P);
                this.A02 = A0C;
                if (i2 == -1) {
                    C117315Zl.A0R(((ActivityC13810kN) this).A05, A0C, new AbstractC14590lg(intent, this) { // from class: X.6ES
                        public final /* synthetic */ Intent A00;
                        public final /* synthetic */ BrazilPaymentActivity A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            BrazilPaymentActivity brazilPaymentActivity = this.A01;
                            Intent intent2 = this.A00;
                            List list = (List) obj;
                            if (brazilPaymentActivity.A0T != null) {
                                String stringExtra = intent2.getStringExtra("payment_method_credential_id");
                                Iterator it = list.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                    AbstractC28901Pl A0H = C117305Zk.A0H(it);
                                    if (A0H.A0A.equals(stringExtra)) {
                                        brazilPaymentActivity.A0T.ATY(A0H);
                                        break;
                                    }
                                }
                            }
                            brazilPaymentActivity.A02.A04();
                        }
                    });
                }
            }

            @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
            public void onBackPressed() {
                PaymentView paymentView = this.A0V;
                if (paymentView == null || !paymentView.A0I()) {
                    AbstractC14640lm r0 = ((AbstractActivityC121685jC) this).A0E;
                    AnonymousClass009.A05(r0);
                    if (!C15380n4.A0J(r0) || ((AbstractActivityC121685jC) this).A00 != 0) {
                        AnonymousClass61I.A01(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, super.A0U, null, true), this.A0K, 1, "new_payment", null, 1);
                        finish();
                        return;
                    }
                    ((AbstractActivityC121685jC) this).A0G = null;
                    A2i(C12990iw.A0H(this));
                }
            }

            /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0076  */
            /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
            @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onCreate(android.os.Bundle r6) {
                /*
                    r5 = this;
                    super.onCreate(r6)
                    X.0pI r0 = r5.A07
                    android.content.Context r0 = r0.A00
                    r5.A01 = r0
                    X.0qD r0 = r5.A0P
                    X.0lf r0 = X.C117305Zk.A0C(r0)
                    r5.A02 = r0
                    X.17a r1 = r5.A0C
                    X.4UZ r0 = r5.A0b
                    r1.A03(r0)
                    com.whatsapp.jid.UserJid r0 = r5.A0G
                    if (r0 != 0) goto L_0x0035
                    X.0lm r1 = r5.A0E
                    X.AnonymousClass009.A05(r1)
                    boolean r0 = X.C15380n4.A0J(r1)
                    if (r0 == 0) goto L_0x002f
                    android.os.Bundle r0 = X.C12990iw.A0H(r5)
                    r5.A2i(r0)
                L_0x002e:
                    return
                L_0x002f:
                    com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.of(r1)
                    r5.A0G = r0
                L_0x0035:
                    r5.A2h(r6)
                    if (r6 != 0) goto L_0x008b
                    X.5hW r2 = r5.A0N
                    r1 = 185470254(0xb0e0d2e, float:2.735812E-32)
                    java.lang.String r0 = r5.A0Y
                    java.lang.Integer r0 = r2.A01(r0, r1)
                    if (r0 == 0) goto L_0x004d
                    int r0 = r0.intValue()
                L_0x004b:
                    r5.A00 = r0
                L_0x004d:
                    android.content.Intent r0 = r5.getIntent()
                    if (r0 == 0) goto L_0x006c
                    android.content.Intent r1 = r5.getIntent()
                    java.lang.String r0 = "extra_request_id"
                    java.lang.String r0 = r1.getStringExtra(r0)
                    r5.A0Z = r0
                    android.content.Intent r2 = r5.getIntent()
                    r1 = 0
                    java.lang.String r0 = "extra_should_open_transaction_detail_after_send_override"
                    boolean r0 = r2.getBooleanExtra(r0, r1)
                    r5.A0u = r0
                L_0x006c:
                    X.0m9 r1 = r5.A0C
                    r0 = 1482(0x5ca, float:2.077E-42)
                    boolean r0 = r1.A07(r0)
                    if (r0 == 0) goto L_0x002e
                    X.0lR r4 = r5.A05
                    X.0qD r3 = r5.A0P
                    X.0si r2 = r5.A0E
                    X.0y6 r0 = r5.A0I
                    com.whatsapp.payments.CheckFirstTransaction r1 = new com.whatsapp.payments.CheckFirstTransaction
                    r1.<init>(r0, r2, r3, r4)
                    r5.A0A = r1
                    X.04x r0 = r5.A06
                    r0.A00(r1)
                    return
                L_0x008b:
                    java.lang.String r0 = "flow_instance_key"
                    int r0 = r6.getInt(r0)
                    goto L_0x004b
                */
                throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.BrazilPaymentActivity.onCreate(android.os.Bundle):void");
            }

            @Override // android.app.Activity
            public Dialog onCreateDialog(int i) {
                Dialog A01 = this.A0U.A01(null, this, i);
                if (A01 == null) {
                    return super.onCreateDialog(i);
                }
                return A01;
            }

            @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onDestroy() {
                super.onDestroy();
                if (!this.A0a) {
                    this.A0N.A04(this.A00, 4);
                    this.A0F.A04(this.A0c);
                }
                this.A0K.reset();
                this.A0C.A04(this.A0b);
            }

            @Override // X.ActivityC13810kN, android.app.Activity
            public boolean onOptionsItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() != 16908332) {
                    return false;
                }
                AbstractC14640lm r0 = ((AbstractActivityC121685jC) this).A0E;
                AnonymousClass009.A05(r0);
                if (!C15380n4.A0J(r0) || ((AbstractActivityC121685jC) this).A00 != 0) {
                    finish();
                    return true;
                }
                ((AbstractActivityC121685jC) this).A0G = null;
                A2i(C12990iw.A0H(this));
                return true;
            }

            @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onSaveInstanceState(Bundle bundle) {
                super.onSaveInstanceState(bundle);
                bundle.putInt("flow_instance_key", this.A00);
            }
        }
