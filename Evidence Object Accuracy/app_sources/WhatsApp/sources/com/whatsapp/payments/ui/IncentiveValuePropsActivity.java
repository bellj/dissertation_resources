package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass3FW;
import X.AnonymousClass617;
import X.AnonymousClass61I;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117965b3;
import X.C118025b9;
import X.C129395xe;
import X.C12960it;
import X.C1321765q;
import X.C253118x;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* loaded from: classes4.dex */
public class IncentiveValuePropsActivity extends ActivityC13790kL {
    public View A00;
    public View A01;
    public Button A02;
    public TextEmojiLabel A03;
    public WaImageView A04;
    public WaTextView A05;
    public C117965b3 A06;
    public C129395xe A07;
    public C253118x A08;
    public boolean A09;

    public IncentiveValuePropsActivity() {
        this(0);
    }

    public IncentiveValuePropsActivity(int i) {
        this.A09 = false;
        C117295Zj.A0p(this, 28);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A08 = C117315Zl.A0H(A1M);
            this.A07 = (C129395xe) A1M.AEy.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.incentives_value_props);
        Toolbar A08 = C117305Zk.A08(this);
        TextView textView = (TextView) LayoutInflater.from(this).inflate(R.layout.onboarding_title_view, (ViewGroup) A08, false);
        C12960it.A0s(this, textView, R.color.reg_title_color);
        textView.setText(R.string.payments_activity_title);
        A08.addView(textView);
        A1e(A08);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.payments_activity_title);
            A08.setBackgroundColor(AnonymousClass00T.A00(this, R.color.primary_surface));
            A1U.A0D(AnonymousClass2GE.A04(getResources().getDrawable(R.drawable.ic_close), AnonymousClass00T.A00(this, R.color.ob_action_bar_icon)));
            A1U.A0P(false);
        }
        this.A05 = (WaTextView) findViewById(R.id.incentives_value_props_title);
        this.A03 = (TextEmojiLabel) findViewById(R.id.incentives_value_props_desc);
        this.A00 = findViewById(R.id.incentive_security_blurb_view);
        this.A01 = findViewById(R.id.payment_processor_logo);
        this.A02 = (Button) findViewById(R.id.incentives_value_props_continue);
        WaImageView waImageView = (WaImageView) findViewById(R.id.incentive_security_icon_view);
        this.A04 = waImageView;
        AnonymousClass2GE.A05(this, waImageView, R.color.payment_privacy_avatar_tint);
        C118025b9 A00 = this.A07.A00(this);
        AnonymousClass016 r1 = A00.A01;
        r1.A0A(AnonymousClass617.A01(A00.A04.A00()));
        C117295Zj.A0r(this, r1, 34);
        C117965b3 r0 = (C117965b3) C117315Zl.A06(new C1321765q(this.A07), this).A00(C117965b3.class);
        this.A06 = r0;
        C117295Zj.A0r(this, r0.A00, 33);
        C117965b3 r4 = this.A06;
        String stringExtra = getIntent().getStringExtra("referral_screen");
        AnonymousClass3FW A0S = C117305Zk.A0S();
        A0S.A02("is_payment_account_setup", r4.A01.A0A());
        AnonymousClass61I.A03(A0S, C117305Zk.A0U(r4.A02), "incentive_value_prop", stringExtra);
    }
}
