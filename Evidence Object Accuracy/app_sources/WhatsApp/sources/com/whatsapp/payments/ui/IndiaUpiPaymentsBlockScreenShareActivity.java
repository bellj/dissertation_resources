package com.whatsapp.payments.ui;

import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass6BE;
import X.C016907y;
import X.C117295Zj;
import X.C117305Zk;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentsBlockScreenShareActivity extends ActivityC13830kP {
    public AnonymousClass6BE A00;
    public boolean A01;

    public IndiaUpiPaymentsBlockScreenShareActivity() {
        this(0);
    }

    public IndiaUpiPaymentsBlockScreenShareActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 59);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            this.A00 = C117305Zk.A0T(ActivityC13830kP.A1M(C117295Zj.A09(this), this));
        }
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(2);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().setDimAmount(0.8f);
        int A00 = AnonymousClass00T.A00(this, R.color.gray_activity);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().clearFlags(67108864);
            getWindow().setStatusBarColor(C016907y.A03(0.3f, A00, AnonymousClass00T.A00(this, R.color.lightStatusBarBackgroundColor)));
        }
        setContentView(R.layout.india_upi_payment_block_screen_share);
        C117295Zj.A0n(findViewById(R.id.close), this, 55);
        this.A00.AKg(0, null, "block_screen_share", null);
    }
}
