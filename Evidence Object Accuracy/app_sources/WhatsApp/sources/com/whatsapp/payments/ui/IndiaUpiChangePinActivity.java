package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121665jA;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1ZS;
import X.AnonymousClass1ZY;
import X.AnonymousClass2FL;
import X.AnonymousClass69E;
import X.C117295Zj;
import X.C119755f3;
import X.C124015oJ;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1311161i;
import X.C30861Zc;
import X.C30931Zj;
import X.C36021jC;
import X.C452120p;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiChangePinActivity;
import org.chromium.net.UrlRequest;

/* loaded from: classes4.dex */
public class IndiaUpiChangePinActivity extends AbstractActivityC121545iU {
    public ProgressBar A00;
    public TextView A01;
    public C30861Zc A02;
    public String A03;
    public boolean A04;
    public final C30931Zj A05;

    public IndiaUpiChangePinActivity() {
        this(0);
        this.A05 = C117295Zj.A0G("IndiaUpiChangePinActivity");
    }

    public IndiaUpiChangePinActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 40);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
        }
    }

    @Override // X.AbstractActivityC121545iU
    public void A3A() {
        if (!((AbstractActivityC121545iU) this).A06.A07.contains("pin-entry-ui")) {
            if (!(getIntent() == null || C12990iw.A0H(this) == null)) {
                this.A02 = (C30861Zc) C12990iw.A0H(this).get("extra_bank_account");
            }
            if (this.A02 == null) {
                C12960it.A1E(new C124015oJ(this), ((ActivityC13830kP) this).A05);
                return;
            }
            ((AbstractActivityC121545iU) this).A06.A02("pin-entry-ui");
            if (this.A02 != null) {
                ((AbstractActivityC121545iU) this).A09.A00();
                return;
            }
            this.A05.A06("could not find bank account; showErrorAndFinish");
            A39();
        }
    }

    @Override // X.AnonymousClass6MS
    public void ARr(C452120p r12, String str) {
        Integer num;
        AnonymousClass1ZY r0;
        C30861Zc r02;
        ((AbstractActivityC121665jA) this).A0D.A04(this.A02, r12, 1);
        if (!TextUtils.isEmpty(str) && (r02 = this.A02) != null && r02.A08 != null) {
            this.A03 = AbstractActivityC119235dO.A0O(this);
            ((AbstractActivityC121545iU) this).A06.A03("upi-get-credential");
            C30861Zc r03 = this.A02;
            A3E((C119755f3) r03.A08, str, r03.A0B, this.A03, (String) AnonymousClass1ZS.A01(r03.A09), 2);
        } else if (r12 != null && !AnonymousClass69E.A02(this, "upi-list-keys", r12.A00, true)) {
            if (((AbstractActivityC121545iU) this).A06.A07("upi-list-keys")) {
                ((AbstractActivityC121665jA) this).A0B.A0C();
                ((ActivityC13810kN) this).A05.A07(R.string.payments_still_working, 1);
                ((AbstractActivityC121545iU) this).A09.A00();
                return;
            }
            C30931Zj r3 = this.A05;
            StringBuilder A0k = C12960it.A0k("IndiaUpiChangePinActivity: onListKeys: ");
            if (str != null) {
                num = Integer.valueOf(str.length());
            } else {
                num = null;
            }
            A0k.append(num);
            A0k.append(" bankAccount: ");
            A0k.append(this.A02);
            A0k.append(" countrydata: ");
            C30861Zc r04 = this.A02;
            if (r04 != null) {
                r0 = r04.A08;
            } else {
                r0 = null;
            }
            A0k.append(r0);
            r3.A08("payment-settings", C12960it.A0d(" failed; ; showErrorAndFinish", A0k), null);
            A39();
        }
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r5) {
        ((AbstractActivityC121665jA) this).A0D.A04(this.A02, r5, 7);
        if (r5 == null) {
            this.A05.A06("onSetPin success; showSuccessAndFinish");
            A2r();
            Object[] A1b = C12970iu.A1b();
            A1b[0] = C1311161i.A07(this.A02);
            Adr(A1b, 0, R.string.payments_change_pin_success);
        } else if (!AnonymousClass69E.A02(this, "upi-change-mpin", r5.A00, true)) {
            int i = r5.A00;
            int i2 = 10;
            if (i != 11459) {
                i2 = 11;
                if (i != 11468) {
                    i2 = 12;
                    if (i != 11454) {
                        if (i == 11456 || i == 11471) {
                            i2 = 13;
                        } else {
                            this.A05.A06(" onSetPin failed; showErrorAndFinish");
                            A39();
                            return;
                        }
                    }
                }
            }
            C36021jC.A01(this, i2);
        }
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_pin_change);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I(((AbstractActivityC121545iU) this).A01.A00.getResources().getString(R.string.payments_change_upi_pin_title));
            A1U.A0M(true);
        }
        this.A01 = C12970iu.A0M(this, R.id.payments_upi_pin_setup_desc);
        this.A00 = (ProgressBar) findViewById(R.id.progress);
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        this.A01.setVisibility(4);
        this.A00.setVisibility(4);
        switch (i) {
            case 10:
                return A31(new Runnable() { // from class: X.6Fw
                    @Override // java.lang.Runnable
                    public final void run() {
                        IndiaUpiChangePinActivity indiaUpiChangePinActivity = IndiaUpiChangePinActivity.this;
                        indiaUpiChangePinActivity.A01.setVisibility(0);
                        indiaUpiChangePinActivity.A00.setVisibility(0);
                        String A0A = ((AbstractActivityC121665jA) indiaUpiChangePinActivity).A0B.A0A();
                        if (!TextUtils.isEmpty(A0A)) {
                            String A0O = AbstractActivityC119235dO.A0O(indiaUpiChangePinActivity);
                            indiaUpiChangePinActivity.A03 = A0O;
                            C30861Zc r0 = indiaUpiChangePinActivity.A02;
                            indiaUpiChangePinActivity.A3E((C119755f3) r0.A08, A0A, r0.A0B, A0O, (String) AnonymousClass1ZS.A01(r0.A09), 2);
                            return;
                        }
                        ((AbstractActivityC121545iU) indiaUpiChangePinActivity).A09.A00();
                    }
                }, getString(R.string.payments_change_pin_invalid_pin), i, R.string.yes, R.string.no);
            case 11:
                return A31(new Runnable() { // from class: X.6Fu
                    @Override // java.lang.Runnable
                    public final void run() {
                        IndiaUpiChangePinActivity indiaUpiChangePinActivity = IndiaUpiChangePinActivity.this;
                        indiaUpiChangePinActivity.A01.setVisibility(0);
                        indiaUpiChangePinActivity.A00.setVisibility(0);
                        AbstractActivityC119235dO.A1a(indiaUpiChangePinActivity.A02, indiaUpiChangePinActivity, true);
                    }
                }, getString(R.string.payments_pin_max_retries), i, R.string.yes, R.string.no);
            case 12:
                return A31(new Runnable() { // from class: X.6Fv
                    @Override // java.lang.Runnable
                    public final void run() {
                        IndiaUpiChangePinActivity indiaUpiChangePinActivity = IndiaUpiChangePinActivity.this;
                        indiaUpiChangePinActivity.A01.setVisibility(0);
                        indiaUpiChangePinActivity.A00.setVisibility(0);
                        AbstractActivityC119235dO.A1a(indiaUpiChangePinActivity.A02, indiaUpiChangePinActivity, false);
                    }
                }, getString(R.string.payments_pin_no_pin_set), i, R.string.yes, R.string.no);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                ((AbstractActivityC121665jA) this).A0B.A0D();
                return A31(new Runnable() { // from class: X.6Ft
                    @Override // java.lang.Runnable
                    public final void run() {
                        IndiaUpiChangePinActivity indiaUpiChangePinActivity = IndiaUpiChangePinActivity.this;
                        indiaUpiChangePinActivity.A01.setVisibility(0);
                        indiaUpiChangePinActivity.A00.setVisibility(0);
                        indiaUpiChangePinActivity.A36();
                    }
                }, getString(R.string.payments_set_pin_retry), i, R.string.yes, R.string.no);
            default:
                return super.onCreateDialog(i);
        }
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        C30861Zc r0 = (C30861Zc) bundle.getParcelable("bankAccountSavedInst");
        this.A02 = r0;
        if (r0 != null) {
            this.A02.A08 = (AnonymousClass1ZY) bundle.getParcelable("countryDataSavedInst");
        }
        this.A03 = bundle.getString("seqNumSavedInst");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C30931Zj r2 = this.A05;
        StringBuilder A0k = C12960it.A0k("onResume with states: ");
        A0k.append(((AbstractActivityC121545iU) this).A06);
        C117295Zj.A1F(r2, A0k);
        if (!((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge") && ((AbstractActivityC121665jA) this).A0B.A05().A00 == null) {
            ((AbstractActivityC121545iU) this).A06.A03("upi-get-challenge");
            A36();
        } else if (!((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge")) {
            A3A();
        }
    }

    @Override // X.AbstractActivityC121545iU, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass1ZY r1;
        super.onSaveInstanceState(bundle);
        C30861Zc r12 = this.A02;
        if (r12 != null) {
            bundle.putParcelable("bankAccountSavedInst", r12);
        }
        C30861Zc r0 = this.A02;
        if (!(r0 == null || (r1 = r0.A08) == null)) {
            bundle.putParcelable("countryDataSavedInst", r1);
        }
        String str = this.A03;
        if (str != null) {
            bundle.putString("seqNumSavedInst", str);
        }
    }
}
