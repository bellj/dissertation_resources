package com.whatsapp.payments.ui.invites;

import X.AbstractC1310761d;
import X.AbstractC14640lm;
import X.AnonymousClass17V;
import X.AnonymousClass1XC;
import X.AnonymousClass2SP;
import X.AnonymousClass4OZ;
import X.C117295Zj;
import X.C118025b9;
import X.C129395xe;
import X.C12960it;
import X.C12970iu;
import X.C15550nR;
import X.C15610nY;
import X.C16120oU;
import X.C18600si;
import X.C18610sj;
import X.C21270x9;
import X.C22480z9;
import X.C26281Cs;
import X.C26291Ct;
import X.C456522m;
import X.C74503iB;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public abstract class PaymentInviteFragment extends Hilt_PaymentInviteFragment {
    public C15550nR A00;
    public C15610nY A01;
    public C21270x9 A02;
    public C16120oU A03;
    public C22480z9 A04;
    public AnonymousClass17V A05;
    public AbstractC1310761d A06;
    public C74503iB A07;
    public C118025b9 A08;
    public C129395xe A09;
    public String A0A;
    public List A0B;

    public static Bundle A01(String str, ArrayList arrayList, int i, boolean z, boolean z2) {
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("payment_service", i);
        A0D.putParcelableArrayList("user_jids", arrayList);
        A0D.putBoolean("requires_sync", z);
        A0D.putString("referral_screen", str);
        A0D.putBoolean("show_incentive_blurb", z2);
        return A0D;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_invite_bottom_sheet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006f, code lost:
        if (r7.A04(X.C117305Zk.A0K(r7.A03), r7.A04.A00()) == false) goto L_0x0071;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r31, android.view.View r32) {
        /*
        // Method dump skipped, instructions count: 434
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.invites.PaymentInviteFragment.A17(android.os.Bundle, android.view.View):void");
    }

    public void A1A() {
        C22480z9 r9 = this.A04;
        List<AbstractC14640lm> list = this.A0B;
        int i = A03().getInt("payment_service");
        for (AbstractC14640lm r6 : list) {
            long A00 = r9.A01.A00() + 7776000000L;
            C18600si r10 = r9.A03;
            Map A07 = r10.A07(r10.A01().getString("payments_invitee_jids_with_expiry", ""));
            Number number = (Number) A07.get(r6);
            if (number == null || number.longValue() < A00) {
                A07.put(r6, Long.valueOf(A00));
                C12970iu.A1D(C117295Zj.A05(r10), "payments_invitee_jids_with_expiry", C18600si.A00(A07));
            }
            C18610sj r7 = r9.A04;
            r7.A0I.A06("userActionSendPaymentInvite");
            AnonymousClass1XC r1 = new AnonymousClass1XC(r7.A0M.A07.A02(r6, true), r7.A04.A00());
            r1.A00 = i;
            r1.A01 = A00;
            r1.A0T(DefaultCrypto.BUFFER_SIZE);
            r7.A06.A0S(r1);
            C26281Cs r72 = r7.A0H.A01;
            String rawString = r6.getRawString();
            synchronized (r72) {
                C26291Ct r5 = r72.A01;
                C456522m A002 = r5.A00();
                A002.A01++;
                A002.A0C.add(rawString);
                r5.A01(A002);
            }
        }
        this.A07.A04(2);
        A1C(this.A0B.size(), true);
    }

    public void A1B() {
        StringBuilder A0k = C12960it.A0k("showProgress(");
        A0k.append(false);
        Log.i(C12960it.A0d(")", A0k));
        this.A06.A6Q(new AnonymousClass4OZ(2, this.A0B));
    }

    public void A1C(int i, boolean z) {
        if (this instanceof IndiaUpiPaymentInviteFragment) {
            IndiaUpiPaymentInviteFragment indiaUpiPaymentInviteFragment = (IndiaUpiPaymentInviteFragment) this;
            AnonymousClass2SP r2 = new AnonymousClass2SP();
            r2.A0Z = "payment_invite_prompt";
            String str = ((PaymentInviteFragment) indiaUpiPaymentInviteFragment).A0A;
            if (str == null) {
                str = "chat";
            }
            r2.A0Y = str;
            indiaUpiPaymentInviteFragment.A1D(r2);
            int i2 = 1;
            r2.A09 = 1;
            if (z) {
                i2 = 54;
            }
            r2.A08 = Integer.valueOf(i2);
            r2.A0I = Long.valueOf((long) i);
            indiaUpiPaymentInviteFragment.A0I.AKf(r2);
        }
    }
}
