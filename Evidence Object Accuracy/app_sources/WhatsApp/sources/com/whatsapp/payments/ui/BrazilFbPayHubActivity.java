package com.whatsapp.payments.ui;

import X.AbstractC1311261j;
import X.AbstractC28901Pl;
import X.AbstractView$OnClickListenerC121485iL;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1A7;
import X.AnonymousClass2FL;
import X.AnonymousClass605;
import X.AnonymousClass60T;
import X.AnonymousClass61E;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C129135xE;
import X.C12960it;
import X.C12970iu;
import X.C129735yD;
import X.C12990iw;
import X.C129925yW;
import X.C129945yY;
import X.C130015yf;
import X.C130065yk;
import X.C1311161i;
import X.C1329568x;
import X.C15650ng;
import X.C18590sh;
import X.C18620sk;
import X.C18650sn;
import X.C18660so;
import X.C20380vf;
import X.C20390vg;
import X.C20400vh;
import X.C21860y6;
import X.C22710zW;
import X.C243515e;
import X.C25871Bd;
import android.content.Intent;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public class BrazilFbPayHubActivity extends AbstractView$OnClickListenerC121485iL implements AbstractC1311261j {
    public C15650ng A00;
    public C1329568x A01;
    public C129925yW A02;
    public AnonymousClass61M A03;
    public C22710zW A04;
    public C18620sk A05;
    public AnonymousClass61E A06;
    public AnonymousClass605 A07;
    public C130015yf A08;
    public C25871Bd A09;
    public C129945yY A0A;
    public C130065yk A0B;
    public C129735yD A0C;
    public C18590sh A0D;
    public boolean A0E;

    @Override // X.AbstractC1311261j
    public int AEM(AbstractC28901Pl r2) {
        return 0;
    }

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return true;
    }

    @Override // X.AbstractC1311261j
    public boolean AdV() {
        return true;
    }

    public BrazilFbPayHubActivity() {
        this(0);
    }

    public BrazilFbPayHubActivity(int i) {
        this.A0E = false;
        C117295Zj.A0p(this, 9);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0E) {
            this.A0E = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractView$OnClickListenerC121485iL) this).A0I = (C129135xE) A1M.AFj.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0H = C117305Zk.A0P(A1M);
            ((AbstractView$OnClickListenerC121485iL) this).A0E = C117315Zl.A0A(A1M);
            ((AbstractView$OnClickListenerC121485iL) this).A09 = (C21860y6) A1M.AE6.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0G = C117305Zk.A0M(A1M);
            ((AbstractView$OnClickListenerC121485iL) this).A0B = (C18650sn) A1M.AEe.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0J = (AnonymousClass1A7) A1M.AEs.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0K = (AnonymousClass60T) A1M.AFI.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0C = (C18660so) A1M.AEf.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0F = (C243515e) A1M.AEt.get();
            ((AbstractView$OnClickListenerC121485iL) this).A08 = (C20380vf) A1M.ACR.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0D = (C20390vg) A1M.AEi.get();
            ((AbstractView$OnClickListenerC121485iL) this).A0A = (C20400vh) A1M.AE8.get();
            this.A0D = C117315Zl.A0F(A1M);
            this.A07 = (AnonymousClass605) A1M.AEj.get();
            this.A00 = (C15650ng) A1M.A4m.get();
            this.A01 = (C1329568x) A1M.A1o.get();
            this.A0A = (C129945yY) A1M.A1q.get();
            this.A08 = (C130015yf) A1M.AEk.get();
            this.A04 = C117305Zk.A0O(A1M);
            this.A02 = (C129925yW) A1M.AEW.get();
            this.A05 = (C18620sk) A1M.AFB.get();
            this.A03 = C117305Zk.A0N(A1M);
            this.A09 = (C25871Bd) A1M.ABZ.get();
            this.A06 = (AnonymousClass61E) A1M.AEY.get();
            this.A0B = (C130065yk) A1M.A1z.get();
            this.A0C = A09.A0C();
        }
    }

    @Override // X.AbstractC136336Md
    public void ALz(boolean z) {
        String str;
        String A01 = this.A0B.A01();
        Intent A0D = C12990iw.A0D(this, BrazilPayBloksActivity.class);
        this.A0B.A03(A0D, "generic_context");
        HashMap A11 = C12970iu.A11();
        A11.put("referral_screen", "fbpay_payment_settings");
        if (A01 != null) {
            A0D.putExtra("screen_name", A01);
        } else {
            if (z) {
                str = "1";
            } else {
                str = "0";
            }
            A11.put("verification_needed", str);
            A0D.putExtra("screen_name", "brpay_p_add_card");
        }
        A0D.putExtra("screen_params", A11);
        A2D(A0D);
    }

    @Override // X.AbstractC136336Md
    public void ATY(AbstractC28901Pl r3) {
        if (r3.A04() != 5) {
            Intent A0D = C12990iw.A0D(this, BrazilPaymentCardDetailsActivity.class);
            C117315Zl.A0M(A0D, r3);
            startActivity(A0D);
        }
    }

    @Override // X.AbstractC1311261j
    public /* synthetic */ boolean AdN(AbstractC28901Pl r2) {
        return false;
    }

    @Override // X.AbstractC1311261j
    public void Adj(AbstractC28901Pl r2, PaymentMethodRow paymentMethodRow) {
        if (C1311161i.A0B(r2)) {
            this.A0A.A02(r2, paymentMethodRow);
        }
    }

    @Override // X.AbstractView$OnClickListenerC121485iL, X.AnonymousClass6M5
    public void AfR(List list) {
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl A0H = C117305Zk.A0H(it);
            if (A0H.A04() == 5) {
                A0l.add(A0H);
            } else {
                A0l2.add(A0H);
            }
        }
        super.AfR(A0l2);
    }

    @Override // X.AbstractView$OnClickListenerC121485iL, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (!this.A0B.A06.A03()) {
            finish();
        }
    }
}
