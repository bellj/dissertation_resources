package com.whatsapp.payments.ui;

import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.C004902f;
import X.C117295Zj;
import X.C117305Zk;
import X.C119125cv;
import X.C12960it;
import X.C12980iv;
import X.C30931Zj;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class PaymentBottomSheet extends Hilt_PaymentBottomSheet {
    public DialogInterface.OnDismissListener A00;
    public AnonymousClass01E A01;
    public C30931Zj A02 = C117305Zk.A0V("PaymentBottomSheet", "payment");

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Log.i("onCreateView()");
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_bottom_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Log.i("onViewCreated()");
        if (this.A01 == null) {
            A1B();
            return;
        }
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view.findViewById(R.id.bottom_sheet));
        A00.A0L(0);
        A00.A0M(3);
        A00.A0E = new C119125cv(this);
        C117295Zj.A0n(view.findViewById(R.id.dismiss_space), this, 96);
        view.findViewById(R.id.fragment_container).setOnTouchListener(new View.OnTouchListener() { // from class: X.64t
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                return true;
            }
        });
        C004902f r1 = new C004902f(A0E());
        r1.A06(this.A01, R.id.fragment_container);
        r1.A0F(null);
        r1.A01();
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Log.i("onCreateDialog()");
        RelativeLayout relativeLayout = new RelativeLayout(A0B());
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        Dialog dialog = new Dialog(A01());
        dialog.requestWindowFeature(1);
        dialog.setContentView(relativeLayout);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
            window.getAttributes().windowAnimations = R.style.BottomSheetDialogFragmentAnimation;
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
        return dialog;
    }

    public void A1K() {
        AnonymousClass01F A0E = A0E();
        int A03 = A0E.A03();
        A0E.A0H();
        if (A03 <= 1) {
            A1B();
        }
    }

    public void A1L(AnonymousClass01E r4) {
        this.A02.A04(C12960it.A0d(r4.getClass().getName(), C12960it.A0k("navigate-to fragment=")));
        C004902f r2 = new C004902f(A0E());
        r2.A02 = 17432576;
        r2.A03 = 17432577;
        r2.A05 = 17432576;
        r2.A06 = 17432577;
        r2.A05((AnonymousClass01E) C12980iv.A0o(A0E().A0U.A02()));
        r2.A07(r4, R.id.fragment_container);
        r2.A0F(null);
        r2.A01();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        Log.i("onDismiss(dialog)");
        DialogInterface.OnDismissListener onDismissListener = this.A00;
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialogInterface);
        }
    }
}
