package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119755f3;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C1311161i;
import X.C14900mE;
import X.C30861Zc;
import X.C42971wC;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class IndiaUpiPinPrimerFullSheetActivity extends AbstractActivityC121665jA {
    public C30861Zc A00;
    public String A01;
    public boolean A02;

    public IndiaUpiPinPrimerFullSheetActivity() {
        this(0);
    }

    public IndiaUpiPinPrimerFullSheetActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 64);
    }

    public static Intent A02(Context context, C30861Zc r3, boolean z) {
        String str;
        Intent A0D = C12990iw.A0D(context, IndiaUpiPinPrimerFullSheetActivity.class);
        C117315Zl.A0M(A0D, r3);
        if (z) {
            str = "forgot_pin";
        } else {
            str = "setup_pin";
        }
        A0D.putExtra("event_screen", str);
        return A0D;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    public final void A30() {
        C119755f3 r5 = (C119755f3) this.A00.A08;
        View A0D = AbstractActivityC119235dO.A0D(this);
        Bitmap A05 = this.A00.A05();
        ImageView A0K = C12970iu.A0K(A0D, R.id.provider_icon);
        if (A05 != null) {
            A0K.setImageBitmap(A05);
        } else {
            A0K.setImageResource(R.drawable.av_bank);
        }
        C12960it.A0I(A0D, R.id.account_number).setText(C1311161i.A02(this, ((ActivityC13830kP) this).A01, this.A00, ((AbstractActivityC121685jC) this).A0P, false));
        C117315Zl.A0N(C12960it.A0I(A0D, R.id.account_name), C117295Zj.A0R(r5.A03));
        C12960it.A0I(A0D, R.id.account_type).setText(r5.A0E());
        C14900mE r10 = ((ActivityC13810kN) this).A05;
        AnonymousClass12P r9 = ((ActivityC13790kL) this).A00;
        AnonymousClass01d r12 = ((ActivityC13810kN) this).A08;
        String A0X = C12960it.A0X(this, "learn-more", C12970iu.A1b(), 0, R.string.payments_upi_pin_primer_security_note);
        C42971wC.A08(this, Uri.parse("https://faq.whatsapp.com/general/payments/about-payments-data"), r9, r10, (TextEmojiLabel) findViewById(R.id.note), r12, A0X, "learn-more");
        C117295Zj.A0n(findViewById(R.id.continue_button), this, 61);
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Class cls;
        if (i != 1012) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            if (intent != null && intent.hasExtra("extra_bank_account")) {
                C30861Zc r0 = (C30861Zc) intent.getParcelableExtra("extra_bank_account");
                this.A00 = r0;
                ((AbstractActivityC121665jA) this).A04 = r0;
            }
            switch (((AbstractActivityC121665jA) this).A02) {
                case 0:
                    Intent A0A = C12970iu.A0A();
                    A0A.putExtra("extra_bank_account", this.A00);
                    setResult(-1, A0A);
                    finish();
                    return;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 8:
                case 9:
                case 10:
                case 11:
                    if (((AbstractActivityC121665jA) this).A0O) {
                        A2q();
                        cls = IndiaUpiPaymentsAccountSetupActivity.class;
                    } else {
                        cls = IndiaUpiBankAccountAddedLandingActivity.class;
                    }
                    Intent A0D = C12990iw.A0D(this, cls);
                    A0D.putExtra("referral_screen", this.A01);
                    A2v(A0D);
                    finish();
                    startActivity(A0D);
                    return;
                case 7:
                default:
                    return;
            }
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), this.A01, null);
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        getWindow().addFlags(DefaultCrypto.BUFFER_SIZE);
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_pin_primer_full_sheet);
        String stringExtra = getIntent().getStringExtra("event_screen");
        this.A01 = stringExtra;
        if (TextUtils.isEmpty(stringExtra)) {
            this.A01 = "setup_pin";
        } else if (this.A01.equals("forgot_pin")) {
            C12970iu.A0M(this, R.id.title).setText(R.string.payments_forgot_upi_pin_primer_title);
            C12970iu.A0M(this, R.id.desc).setText(R.string.payments_forgot_upi_pin_primer_desc);
        }
        this.A00 = (C30861Zc) getIntent().getParcelableExtra("extra_bank_account");
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            C117305Zk.A16(A0K, R.string.payments_activity_title);
        }
        C30861Zc r0 = this.A00;
        if (r0 == null || r0.A08 == null) {
            Log.w("Screen called without account, fetching account from local db to setup pin");
            ((ActivityC13830kP) this).A05.Ab2(new Runnable() { // from class: X.6GB
                @Override // java.lang.Runnable
                public final void run() {
                    IndiaUpiPinPrimerFullSheetActivity indiaUpiPinPrimerFullSheetActivity = IndiaUpiPinPrimerFullSheetActivity.this;
                    AbstractC28901Pl A01 = C241414j.A01(C117295Zj.A0Z(((AbstractActivityC121685jC) indiaUpiPinPrimerFullSheetActivity).A0P));
                    if (A01 == null) {
                        Log.e("no valid account found, finishing");
                        ((ActivityC13810kN) indiaUpiPinPrimerFullSheetActivity).A05.A0H(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: INVOKE  
                              (wrap: X.0mE : 0x0013: IGET  (r1v1 X.0mE A[REMOVE]) = 
                              (wrap: ?? : ?: CAST (X.0kN) (r2v0 'indiaUpiPinPrimerFullSheetActivity' com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity))
                             X.0kN.A05 X.0mE)
                              (wrap: X.6GA : 0x0017: CONSTRUCTOR  (r0v6 X.6GA A[REMOVE]) = (r2v0 'indiaUpiPinPrimerFullSheetActivity' com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity) call: X.6GA.<init>(com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity):void type: CONSTRUCTOR)
                             type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6GB.run():void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: CONSTRUCTOR  (r0v6 X.6GA A[REMOVE]) = (r2v0 'indiaUpiPinPrimerFullSheetActivity' com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity) call: X.6GA.<init>(com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity):void type: CONSTRUCTOR in method: X.6GB.run():void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 23 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6GA, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 29 more
                            */
                        /*
                            this = this;
                            com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity r2 = com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity.this
                            X.0qD r0 = r2.A0P
                            java.util.List r0 = X.C117295Zj.A0Z(r0)
                            X.1Pl r0 = X.C241414j.A01(r0)
                            if (r0 != 0) goto L_0x001e
                            java.lang.String r0 = "no valid account found, finishing"
                            com.whatsapp.util.Log.e(r0)
                            X.0mE r1 = r2.A05
                            X.6GA r0 = new X.6GA
                            r0.<init>(r2)
                            r1.A0H(r0)
                            return
                        L_0x001e:
                            X.1Zc r0 = (X.C30861Zc) r0
                            r2.A00 = r0
                            X.0mE r1 = r2.A05
                            X.6GC r0 = new X.6GC
                            r0.<init>(r2)
                            r1.A0H(r0)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6GB.run():void");
                    }
                });
            } else {
                A30();
            }
            ((AbstractActivityC121665jA) this).A0D.AKg(C12980iv.A0i(), null, this.A01, null);
        }

        @Override // X.ActivityC13790kL, android.app.Activity
        public boolean onCreateOptionsMenu(Menu menu) {
            A2w(menu);
            return super.onCreateOptionsMenu(menu);
        }

        @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
        public boolean onOptionsItemSelected(MenuItem menuItem) {
            if (menuItem.getItemId() == R.id.menuitem_help) {
                String str = this.A01;
                C004802e A0S = C12980iv.A0S(this);
                A0S.A06(R.string.context_help_pin_setup_primer);
                A2x(A0S, str);
                return true;
            }
            if (menuItem.getItemId() == 16908332) {
                ((AbstractActivityC121665jA) this).A0D.AKg(1, C12970iu.A0h(), this.A01, null);
            }
            return super.onOptionsItemSelected(menuItem);
        }
    }
