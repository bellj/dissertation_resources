package com.whatsapp.payments.ui;

import X.AbstractActivityC119385di;
import X.AnonymousClass00T;
import X.C117295Zj;
import X.C117315Zl;
import X.C118115bI;
import X.C119135cw;
import X.C12960it;
import X.C1321865r;
import X.C17070qD;
import X.C18590sh;
import X.C18600si;
import X.C18610sj;
import X.C22710zW;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;

/* loaded from: classes4.dex */
public class ViralityLinkVerifierActivity extends AbstractActivityC119385di {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public WaButton A04;
    public WaButton A05;
    public WaTextView A06;
    public WaTextView A07;
    public C18600si A08;
    public C18610sj A09;
    public C22710zW A0A;
    public C17070qD A0B;
    public C18590sh A0C;

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C118115bI r3 = (C118115bI) C117315Zl.A06(new C1321865r(getIntent().getData(), this), this).A00(C118115bI.class);
        setContentView(R.layout.virality_link_verifier_activity);
        C117295Zj.A0n(AnonymousClass00T.A05(this, R.id.virality_activity_root_view), this, 118);
        this.A01 = AnonymousClass00T.A05(this, R.id.actionable_container);
        this.A03 = AnonymousClass00T.A05(this, R.id.virality_texts_container);
        this.A02 = AnonymousClass00T.A05(this, R.id.progress_container);
        this.A07 = C12960it.A0N(this.A03, R.id.payment_enabled_or_not_title);
        this.A06 = C12960it.A0N(this.A03, R.id.virality_description_text);
        WaButton waButton = (WaButton) AnonymousClass00T.A05(this, R.id.done_or_cancel_button);
        this.A04 = waButton;
        C117295Zj.A0n(waButton, this, 117);
        WaButton waButton2 = (WaButton) AnonymousClass00T.A05(this, R.id.go_to_payments_button);
        this.A05 = waButton2;
        C117295Zj.A0o(waButton2, this, r3, 27);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(AnonymousClass00T.A05(this, R.id.virality_bottom_sheet));
        A00.A0L(0);
        A00.A0M(3);
        A00.A0E = new C119135cw(this);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(0);
            getWindow().setNavigationBarColor(AnonymousClass00T.A00(this, R.color.black));
        }
        C117295Zj.A0r(this, r3.A00, 124);
    }
}
