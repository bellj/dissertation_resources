package com.whatsapp.payments.ui.widget;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.C117305Zk;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class NoviPayHubBalanceView extends LinearLayout implements AnonymousClass004 {
    public LinearLayout A00;
    public LinearLayout A01;
    public TextView A02;
    public TextView A03;
    public ShimmerFrameLayout A04;
    public ShimmerFrameLayout A05;
    public AnonymousClass2P7 A06;
    public boolean A07;

    public NoviPayHubBalanceView(Context context) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        A00(context);
    }

    public NoviPayHubBalanceView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        A00(context);
    }

    public NoviPayHubBalanceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        A00(context);
    }

    public NoviPayHubBalanceView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        C117305Zk.A14(LayoutInflater.from(context), this, R.layout.balance);
        this.A02 = C12960it.A0I(this, R.id.balance);
        this.A03 = C12960it.A0I(this, R.id.conversion_summary);
        this.A04 = (ShimmerFrameLayout) AnonymousClass028.A0D(this, R.id.balance_shimmer);
        this.A05 = (ShimmerFrameLayout) AnonymousClass028.A0D(this, R.id.conversion_summary_shimmer);
        this.A01 = C117305Zk.A07(this, R.id.balance_error_container);
        this.A00 = C117305Zk.A07(this, R.id.balance_container);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public int getBalanceContainerVisibility() {
        return this.A00.getVisibility();
    }

    public int getBalanceFailContainerVisibility() {
        return this.A01.getVisibility();
    }
}
