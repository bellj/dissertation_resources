package com.whatsapp.payments.ui.widget;

import X.AbstractC117715aU;
import X.AbstractC121025h8;
import X.AbstractC130195yx;
import X.AbstractC136376Mh;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.AbstractC30891Zf;
import X.AbstractC38231nk;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass18P;
import X.AnonymousClass1A8;
import X.AnonymousClass1IR;
import X.AnonymousClass1IS;
import X.AnonymousClass1In;
import X.AnonymousClass1J1;
import X.AnonymousClass1ZR;
import X.AnonymousClass1ZS;
import X.AnonymousClass23M;
import X.AnonymousClass2GE;
import X.AnonymousClass60O;
import X.AnonymousClass60R;
import X.C117305Zk;
import X.C117315Zl;
import X.C119835fB;
import X.C120065fZ;
import X.C123685nf;
import X.C123745nn;
import X.C123755no;
import X.C123765np;
import X.C129185xJ;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14830m7;
import X.C14850m9;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C17070qD;
import X.C21270x9;
import X.C22710zW;
import X.C28861Ph;
import X.C30061Vy;
import X.C42941w9;
import X.C92984Yl;
import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.components.Button;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes4.dex */
public class PeerPaymentTransactionRow extends AbstractC117715aU implements AbstractC136376Mh {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public FrameLayout A04;
    public ImageView A05;
    public ImageView A06;
    public ImageView A07;
    public LinearLayout A08;
    public LinearLayout A09;
    public TextView A0A;
    public TextEmojiLabel A0B;
    public TextEmojiLabel A0C;
    public TextEmojiLabel A0D;
    public TextEmojiLabel A0E;
    public WaImageView A0F;
    public WaTextView A0G;
    public WaTextView A0H;
    public Button A0I;
    public AnonymousClass130 A0J;
    public C15550nR A0K;
    public C15610nY A0L;
    public AnonymousClass1J1 A0M;
    public C21270x9 A0N;
    public AnonymousClass018 A0O;
    public C15650ng A0P;
    public AnonymousClass1IR A0Q;
    public C14850m9 A0R;
    public AnonymousClass13H A0S;
    public AnonymousClass1In A0T;
    public AnonymousClass18P A0U;
    public C22710zW A0V;
    public C17070qD A0W;
    public AnonymousClass1A8 A0X;
    public AnonymousClass14X A0Y;
    public String A0Z;

    public PeerPaymentTransactionRow(Context context) {
        super(context);
        A02();
    }

    public PeerPaymentTransactionRow(Context context, AnonymousClass1In r3, int i) {
        this(context);
        String str;
        this.A0T = r3;
        if (i != 2) {
            str = i != 3 ? i != 4 ? "unknown" : "mandate_payment_screen" : "payment_transaction_history";
        } else {
            str = "payment_home";
        }
        this.A0Z = str;
        this.A00 = i;
    }

    public PeerPaymentTransactionRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public PeerPaymentTransactionRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }

    public void A02() {
        C117305Zk.A14(C12960it.A0E(this), this, getLayoutResourceId());
        setBackground(AnonymousClass00T.A04(getContext(), R.drawable.selector_orange_gradient));
        this.A06 = C12970iu.A0L(this, R.id.transaction_icon);
        this.A0D = (TextEmojiLabel) findViewById(R.id.transaction_receiver);
        this.A02 = findViewById(R.id.payment_note_container);
        this.A05 = C12970iu.A0L(this, R.id.media_indicator);
        this.A0C = (TextEmojiLabel) findViewById(R.id.transaction_note);
        this.A0B = (TextEmojiLabel) findViewById(R.id.transaction_amount);
        this.A0A = C12960it.A0J(this, R.id.transaction_status);
        this.A03 = findViewById(R.id.transaction_shimmer);
        this.A07 = C12970iu.A0L(this, R.id.type_icon);
        this.A0H = (WaTextView) findViewById(R.id.requested_from_note);
        this.A01 = findViewById(R.id.action_buttons_container);
        this.A08 = (LinearLayout) findViewById(R.id.transaction_row_details);
        this.A0F = (WaImageView) findViewById(R.id.transaction_loading_error);
        this.A04 = (FrameLayout) findViewById(R.id.custom_country_view_container);
        this.A09 = (LinearLayout) findViewById(R.id.transaction_row_not_supported);
        this.A0E = (TextEmojiLabel) findViewById(R.id.transaction_receiver_not_supported);
        this.A0G = (WaTextView) findViewById(R.id.transaction_not_supported_description);
        this.A0M = this.A0N.A03(getContext(), "peer-payment-transaction-row");
        C42941w9.A04(this.A0C);
        AnonymousClass2GE.A05(getContext(), this.A0F, R.color.payments_error_exclamation);
        setOnClickListener(C117305Zk.A0A(this, 196));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        if (r1 != 200) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
            r5 = this;
            android.content.Context r0 = r5.getContext()
            X.2TT r3 = new X.2TT
            r3.<init>(r0)
            X.1IR r2 = r5.A0Q
            int r1 = r2.A03
            r0 = 1
            if (r1 == r0) goto L_0x0055
            r0 = 2
            if (r1 == r0) goto L_0x0086
            r0 = 9
            if (r1 == r0) goto L_0x0032
            r0 = 10
            if (r1 == r0) goto L_0x0086
            r0 = 20
            if (r1 == r0) goto L_0x0055
            r0 = 100
            if (r1 == r0) goto L_0x0055
            r0 = 200(0xc8, float:2.8E-43)
            if (r1 == r0) goto L_0x0086
        L_0x0027:
            X.130 r2 = r5.A0J
            android.widget.ImageView r1 = r5.A06
            r0 = 2131231140(0x7f0801a4, float:1.8078353E38)
            r2.A05(r1, r0)
            return
        L_0x0032:
            X.0qD r0 = r5.A0W
            X.1ng r0 = X.C117305Zk.A0K(r0)
            if (r0 == 0) goto L_0x0027
            X.130 r4 = r5.A0J
            android.widget.ImageView r3 = r5.A06
            X.1IR r0 = r5.A0Q
            int r2 = r0.A01
            r0 = 1
            r1 = 2131231940(0x7f0804c4, float:1.8079975E38)
            if (r2 == r0) goto L_0x0051
            r0 = 2
            r1 = 2131231939(0x7f0804c3, float:1.8079973E38)
            if (r2 == r0) goto L_0x0051
            r1 = 2131231140(0x7f0801a4, float:1.8078353E38)
        L_0x0051:
            r4.A05(r3, r1)
            return
        L_0x0055:
            com.whatsapp.jid.UserJid r1 = r2.A0D
            if (r1 == 0) goto L_0x0027
            X.0nR r0 = r5.A0K
            X.0n3 r2 = r0.A0B(r1)
            X.1J1 r1 = r5.A0M
            android.widget.ImageView r0 = r5.A06
            r1.A06(r0, r2)
            android.widget.ImageView r2 = r5.A06
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r0 = 2131892353(0x7f121881, float:1.9419452E38)
            java.lang.String r0 = r3.A00(r0)
            r1.append(r0)
            X.1IR r0 = r5.A0Q
            com.whatsapp.jid.UserJid r0 = r0.A0D
            java.lang.String r0 = X.C12970iu.A0s(r0, r1)
            X.AnonymousClass028.A0k(r2, r0)
            android.widget.ImageView r1 = r5.A06
            r0 = 197(0xc5, float:2.76E-43)
            goto L_0x00b6
        L_0x0086:
            com.whatsapp.jid.UserJid r1 = r2.A0E
            if (r1 == 0) goto L_0x0027
            X.0nR r0 = r5.A0K
            X.0n3 r2 = r0.A0B(r1)
            X.1J1 r1 = r5.A0M
            android.widget.ImageView r0 = r5.A06
            r1.A06(r0, r2)
            android.widget.ImageView r2 = r5.A06
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r0 = 2131892353(0x7f121881, float:1.9419452E38)
            java.lang.String r0 = r3.A00(r0)
            r1.append(r0)
            X.1IR r0 = r5.A0Q
            com.whatsapp.jid.UserJid r0 = r0.A0E
            java.lang.String r0 = X.C12970iu.A0s(r0, r1)
            X.AnonymousClass028.A0k(r2, r0)
            android.widget.ImageView r1 = r5.A06
            r0 = 194(0xc2, float:2.72E-43)
        L_0x00b6:
            X.C117295Zj.A0n(r1, r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow.A03():void");
    }

    /* renamed from: A04 */
    public void A6W(AnonymousClass1IR r12) {
        Context context;
        int i;
        String string;
        TextEmojiLabel textEmojiLabel;
        Context context2;
        int i2;
        boolean contains;
        int i3;
        Resources A09;
        int i4;
        AnonymousClass14X r8;
        AnonymousClass1IR r2;
        Context context3;
        int i5;
        int i6;
        AnonymousClass1IS r0;
        AnonymousClass60O r02;
        AnonymousClass1ZR A0B;
        boolean z = this instanceof C123685nf;
        if (!z) {
            this.A0Q = r12;
        } else {
            C123685nf r3 = (C123685nf) this;
            r3.A0Q = r12;
            C14830m7 r82 = r3.A01;
            AbstractC130195yx A00 = new C129185xJ(r3.getContext(), r3.A0K, r3.A0L, r82, r3.A0O, r3.A0Y).A00(r12.A03);
            r3.A02 = A00;
            A00.A06(r3.A0Q);
        }
        A03();
        this.A06.setContentDescription(getTransactionTitle());
        this.A06.setOnClickListener(null);
        boolean A0I = r12.A0I();
        View view = this.A01;
        if (!A0I) {
            view.setVisibility(8);
            this.A08.setVisibility(8);
            this.A09.setVisibility(0);
            this.A0E.setText(getTransactionTitle());
            this.A0G.setText(AnonymousClass23M.A08(new Runnable() { // from class: X.6FG
                @Override // java.lang.Runnable
                public final void run() {
                }
            }, getContext().getString(R.string.payments_transaction_version_not_supported), "update-whatsapp"));
            this.A0G.setLinkTextColor(AnonymousClass00T.A00(getContext(), R.color.link_color));
            setOnClickListener(C117305Zk.A0A(this, 195));
            return;
        }
        view.setVisibility(0);
        this.A08.setVisibility(0);
        this.A09.setVisibility(8);
        this.A0D.setText(getTransactionTitle());
        AbstractC15340mz A0E = this.A0P.A0E(r12);
        AbstractC30891Zf r32 = r12.A0A;
        if (r32 == null || (A0B = r32.A0B()) == null || AnonymousClass1ZS.A02(A0B)) {
            setupTransactionMessage(A0E);
        } else {
            TextEmojiLabel textEmojiLabel2 = this.A0C;
            Object obj = r32.A0B().A00;
            AnonymousClass009.A05(obj);
            C117315Zl.A0N(textEmojiLabel2, obj);
            ImageView imageView = this.A05;
            if (imageView != null) {
                imageView.setVisibility(8);
            }
            TextEmojiLabel textEmojiLabel3 = this.A0C;
            if (textEmojiLabel3 != null) {
                textEmojiLabel3.setVisibility(0);
            }
        }
        AbstractC38231nk ACH = this.A0W.A02().ACH();
        this.A04.removeAllViews();
        if (ACH != null) {
            Context context4 = getContext();
            AbstractC30891Zf r10 = r12.A0A;
            C120065fZ r1 = (C120065fZ) ACH;
            ArrayList A0l = C12960it.A0l();
            if (r10 instanceof C119835fB) {
                C119835fB r102 = (C119835fB) r10;
                if (!TextUtils.isEmpty(r102.A0Q) && r1.A01.A05(AbstractC15460nI.A0y)) {
                    A0l.add(LayoutInflater.from(context4).inflate(R.layout.payment_transaction_row_url_textview, (ViewGroup) null));
                }
                AnonymousClass60R r03 = r102.A0B;
                if (!(r03 == null || (r02 = r03.A0C) == null || !r02.A01())) {
                    A0l.add(LayoutInflater.from(context4).inflate(R.layout.india_upi_payment_transaction_row_update_view, (ViewGroup) null));
                }
            }
            if (!A0l.isEmpty()) {
                LinearLayout linearLayout = new LinearLayout(context4);
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                linearLayout.setOrientation(1);
                Iterator it = A0l.iterator();
                while (it.hasNext()) {
                    linearLayout.addView((View) it.next());
                }
                this.A04.addView(linearLayout);
                this.A04.setVisibility(0);
            }
        }
        if (!z) {
            if (A0E == null || (r0 = A0E.A0z) == null) {
                r8 = this.A0Y;
                r2 = this.A0Q;
            } else {
                r8 = this.A0Y;
                if (r0.A00 == null || (r2 = A0E.A0L) == null) {
                    string = "";
                }
            }
            boolean A0F = r2.A0F();
            C15570nT r13 = r8.A00;
            if (!A0F) {
                boolean A0F2 = r13.A0F(r2.A0D);
                C22710zW r14 = r8.A0B;
                if (r14.A08() || r14.A05()) {
                    int i7 = r2.A03;
                    context3 = r8.A05.A00;
                    if (i7 == 100) {
                        i5 = R.string.payments_transaction_short_status_purchase;
                    } else {
                        i5 = R.string.payments_transaction_short_status_sent;
                        if (A0F2) {
                            i5 = R.string.payments_transaction_short_status_received;
                        }
                    }
                } else {
                    context3 = r8.A05.A00;
                    i5 = R.string.payment_row_transaction_short_status;
                    if (A0F2) {
                        i5 = R.string.payments_transaction_short_status_to_you;
                    }
                }
            } else if (!r13.A0F(r2.A0E)) {
                context3 = r8.A05.A00;
                i5 = R.string.payment_row_request_short_status;
            } else if (r2.A03 == 40 && ((i6 = r2.A02) == 401 || i6 == 417 || i6 == 418)) {
                context3 = r8.A05.A00;
                i5 = R.string.payment_row_scheduled_request_approved;
            } else {
                context3 = r8.A05.A00;
                i5 = R.string.payments_request_short_status_from_you;
            }
            string = context3.getString(i5);
        } else {
            AbstractC130195yx r22 = ((C123685nf) this).A02;
            if (r22 instanceof C123765np) {
                C123765np r23 = (C123765np) r22;
                AbstractC121025h8 r04 = r23.A01;
                AnonymousClass009.A05(r04);
                int i8 = r04.A02;
                if (i8 == 1) {
                    context = r23.A02;
                    i = R.string.novi_withdraw_option_cash;
                } else if (i8 == 2) {
                    context = r23.A02;
                    i = R.string.novi_withdraw_option_bank;
                } else {
                    throw C12970iu.A0f("PAY: NoviTransactionWithdrawalUiExtension getSubtitle: Can't load an unsupported withdrawal type");
                }
            } else if (r22 instanceof C123745nn) {
                context = r22.A05;
                i = R.string.payments_transaction_short_status_to_you;
            } else if (!(r22 instanceof C123755no)) {
                context = r22.A05;
                i = R.string.novi_deposit_type_debit_card;
            } else {
                C123755no r24 = (C123755no) r22;
                boolean A08 = r24.A08();
                context = r24.A03;
                i = R.string.payments_transaction_short_status_to_you;
                if (A08) {
                    i = R.string.payment_row_transaction_short_status;
                }
            }
            string = context.getString(i);
        }
        this.A07.setVisibility(8);
        if (!TextUtils.isEmpty(string)) {
            this.A0H.setText(string);
            this.A0H.setVisibility(0);
            if (this.A0V.A08() || this.A0V.A05()) {
                int i9 = r12.A03;
                if (i9 == 100 || i9 == 200) {
                    i3 = R.drawable.cart;
                    A09 = C12960it.A09(this);
                    i4 = R.dimen.payment_transaction_type_drawable_cart_icon_w;
                } else {
                    i3 = R.drawable.ic_settings_contacts;
                    A09 = C12960it.A09(this);
                    i4 = R.dimen.payment_transaction_type_drawable_peers_icon_w;
                }
                int dimension = (int) A09.getDimension(i4);
                this.A07.setVisibility(0);
                C12990iw.A0x(getContext(), this.A07, i3);
                this.A07.getLayoutParams().width = dimension;
                this.A07.getLayoutParams().height = dimension;
            }
        } else {
            this.A0H.setVisibility(4);
        }
        setupRowButtons(A0E, ACH);
        this.A0B.setText(getAmountText());
        boolean Adb = this.A0W.A02().AF5().Adb(r12);
        TextEmojiLabel textEmojiLabel4 = this.A0B;
        if (Adb) {
            C92984Yl.A00(textEmojiLabel4);
        } else {
            C92984Yl.A01(textEmojiLabel4);
            if (r12.A0D() || AnonymousClass14X.A07(r12)) {
                textEmojiLabel = this.A0B;
                context2 = getContext();
                i2 = R.color.settings_item_title_text;
            } else {
                textEmojiLabel = this.A0B;
                context2 = getContext();
                i2 = R.color.payments_status_gray;
            }
            C12960it.A0s(context2, textEmojiLabel, i2);
        }
        int statusColor = getStatusColor();
        String statusLabel = getStatusLabel();
        boolean isEmpty = TextUtils.isEmpty(statusLabel);
        TextView textView = this.A0A;
        if (!isEmpty) {
            textView.setText(statusLabel);
            this.A0A.setTextColor(statusColor);
            this.A0A.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        if (r12.A03 == 1000) {
            this.A0B.setVisibility(8);
            AnonymousClass1A8 r25 = this.A0X;
            String str = r12.A0K;
            if (TextUtils.isEmpty(str)) {
                contains = false;
            } else {
                contains = r25.A00.contains(str);
            }
            View view2 = this.A03;
            if (contains) {
                view2.setVisibility(8);
                this.A0F.setVisibility(0);
            } else {
                view2.setVisibility(0);
                this.A0F.setVisibility(8);
            }
            this.A0A.setVisibility(8);
        }
    }

    @Override // X.AbstractC136376Mh
    public void AaB() {
        AnonymousClass1IR r1 = this.A0Q;
        if (r1 != null && this.A0T != null) {
            A6W(r1);
        }
    }

    public CharSequence getAmountText() {
        Context context;
        int i;
        String A0I = this.A0Y.A0I(this.A0Q);
        if (!this.A0Q.A0F()) {
            int i2 = this.A0Q.A03;
            if (i2 == 1 || i2 == 100) {
                context = getContext();
                i = R.string.payments_history_amount_debited;
            } else if (i2 == 2 || i2 == 200) {
                context = getContext();
                i = R.string.payments_history_amount_credited;
            }
            A0I = C12960it.A0X(context, A0I, new Object[1], 0, i);
        }
        return this.A0Q.A00().AA7(getContext(), A0I);
    }

    public AnonymousClass1In getCallback() {
        return this.A0T;
    }

    public int getLayoutResourceId() {
        return R.layout.payment_transaction_row;
    }

    public int getStatusColor() {
        return AnonymousClass00T.A00(getContext(), AnonymousClass14X.A01(this.A0Q));
    }

    public String getStatusLabel() {
        return this.A0Y.A0J(this.A0Q);
    }

    public String getTransactionTitle() {
        return this.A0Y.A0S(this.A0Q, false);
    }

    public void setCallback(AnonymousClass1In r1) {
        this.A0T = r1;
    }

    public void setLoggingScreenName(String str) {
        this.A0Z = str;
    }

    public void setupRowButtons(AbstractC15340mz r13, AbstractC38231nk r14) {
        Button button = (Button) AnonymousClass028.A0D(this, R.id.accept_payment_button);
        this.A0I = button;
        AnonymousClass18P r3 = this.A0U;
        View view = this.A01;
        AnonymousClass1In r8 = this.A0T;
        AnonymousClass1IR r6 = this.A0Q;
        String str = this.A0Z;
        view.setVisibility(8);
        if (r6.A0C()) {
            r3.A04(view, null, r6, r8, false);
        } else if (r6.A02 == 102) {
            r3.A02(view, button, r6);
        } else {
            r3.A03(view, null, r6, r14, r8, r13, str, false);
        }
    }

    public void setupTransactionMessage(AbstractC15340mz r5) {
        ImageView imageView;
        int i;
        if ((r5 instanceof C28861Ph) && !TextUtils.isEmpty(r5.A0I())) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(r5.A0I());
            this.A0S.A02(getContext(), spannableStringBuilder, r5.A0o);
            this.A0C.A0E(spannableStringBuilder);
            imageView = this.A05;
            i = 8;
        } else if ((this.A0R.A07(812) || this.A0R.A07(811)) && (r5 instanceof C30061Vy)) {
            ImageView imageView2 = this.A05;
            if (imageView2 != null) {
                imageView2.setImageDrawable(AnonymousClass2GE.A01(getContext(), R.drawable.msg_status_sticker, R.color.msgStatusTint));
            }
            this.A0C.setText(R.string.payment_sticker_transaction_row_message);
            imageView = this.A05;
            i = 0;
        } else {
            ImageView imageView3 = this.A05;
            if (imageView3 != null) {
                imageView3.setVisibility(8);
            }
            TextEmojiLabel textEmojiLabel = this.A0C;
            if (textEmojiLabel != null) {
                textEmojiLabel.setVisibility(8);
                return;
            }
            return;
        }
        if (imageView != null) {
            imageView.setVisibility(i);
        }
        TextEmojiLabel textEmojiLabel2 = this.A0C;
        if (textEmojiLabel2 != null) {
            textEmojiLabel2.setVisibility(0);
        }
    }
}
