package com.whatsapp.payments.ui.stepup;

import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatSpinner;

/* loaded from: classes4.dex */
public class NoviTextInputSpinner extends AppCompatSpinner {
    public NoviTextInputSpinner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.widget.AbsSpinner, android.widget.AdapterView
    public void setSelection(int i) {
        setSelection(i, false);
    }

    @Override // android.widget.AbsSpinner
    public void setSelection(int i, boolean z) {
        boolean A1V = C12960it.A1V(i, getSelectedItemPosition());
        super.setSelection(i, z);
        if (A1V && getOnItemSelectedListener() != null) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), i, getSelectedItemId());
        }
    }
}
