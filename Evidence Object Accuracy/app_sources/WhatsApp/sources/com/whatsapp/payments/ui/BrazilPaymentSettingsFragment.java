package com.whatsapp.payments.ui;

import X.AbstractActivityC119645em;
import X.AbstractC1308360d;
import X.AbstractC16870pt;
import X.AbstractC28901Pl;
import X.AnonymousClass01E;
import X.AnonymousClass01Y;
import X.AnonymousClass102;
import X.AnonymousClass2AC;
import X.AnonymousClass3FW;
import X.AnonymousClass4EV;
import X.AnonymousClass61M;
import X.AnonymousClass68Q;
import X.AnonymousClass69B;
import X.AnonymousClass6M6;
import X.C117315Zl;
import X.C117785ad;
import X.C119865fE;
import X.C123495nF;
import X.C124955qR;
import X.C128345vx;
import X.C12960it;
import X.C129855yP;
import X.C12990iw;
import X.C130065yk;
import X.C1329568x;
import X.C25841Ba;
import X.C25891Bf;
import X.C35741ib;
import X.C460124c;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.IDxCListenerShape5S0000000_3_I1;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes4.dex */
public class BrazilPaymentSettingsFragment extends Hilt_BrazilPaymentSettingsFragment implements AnonymousClass6M6 {
    public C25841Ba A00;
    public AnonymousClass102 A01;
    public C1329568x A02;
    public C119865fE A03;
    public AnonymousClass61M A04;
    public AbstractC16870pt A05;
    public C25891Bf A06;
    public AnonymousClass69B A07;
    public C130065yk A08;
    public C123495nF A09;
    public C128345vx A0A;

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136336Md
    public void ATY(AbstractC28901Pl r1) {
    }

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return true;
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 2 && i2 == -1) {
            A0v(C12990iw.A0D(A0p(), BrazilFbPayHubActivity.class));
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String str;
        Uri uri;
        super.A17(bundle, view);
        super.A16(bundle);
        this.A00.A0D(null, "payment_settings");
        if (((PaymentSettingsFragment) this).A0S.A07(698)) {
            this.A03.A0A();
        }
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        String str2 = null;
        if (!(bundle2 == null || (uri = (Uri) bundle2.getParcelable("extra_deep_link_url")) == null || !C124955qR.A00(uri, this.A07))) {
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], R.string.blocked_campaign_deep_link);
            A01.A02(new IDxCListenerShape5S0000000_3_I1(1), R.string.ok);
            A01.A01().A1F(A0E(), null);
        }
        AbstractC1308360d r2 = this.A0r;
        if (bundle2 != null) {
            str = bundle2.getString("notification-type");
            str2 = bundle2.getString("step-up-id");
        } else {
            str = null;
        }
        r2.A06(str, str2);
    }

    @Override // com.whatsapp.payments.ui.PaymentSettingsFragment
    public void A1K() {
        if (((PaymentSettingsFragment) this).A0f.A03.A07(1359)) {
            AnonymousClass3FW r2 = new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
            r2.A01("hc_entrypoint", "wa_payment_hub_support");
            r2.A01("app_type", "consumer");
            this.A05.AKi(r2, C12960it.A0V(), 39, "payment_home", null);
            A0v(C12990iw.A0D(A01(), BrazilPaymentCareTransactionSelectorActivity.class));
            return;
        }
        super.A1K();
    }

    public final void A1S(String str) {
        Intent A0D = C12990iw.A0D(A0p(), BrazilPayBloksActivity.class);
        A0D.putExtra("screen_name", str);
        this.A08.A03(A0D, "generic_context");
        AbstractActivityC119645em.A0O(A0D, "referral_screen", "wa_payment_settings");
        C35741ib.A00(A0D, "payment_settings");
        startActivityForResult(A0D, 2);
    }

    @Override // X.AbstractC136336Md
    public void ALz(boolean z) {
        A1M(null);
    }

    @Override // X.AnonymousClass6M6
    public void Abh(boolean z) {
        View view = ((AnonymousClass01E) this).A0A;
        if (view != null) {
            ViewGroup A04 = C117315Zl.A04(view, R.id.action_required_container);
            AbstractC1308360d r0 = this.A0r;
            int i = 0;
            if (r0 != null) {
                if (r0.A0C.A02() != null) {
                    ((PaymentSettingsFragment) this).A0T.A04(AnonymousClass4EV.A00(((PaymentSettingsFragment) this).A0Q, this.A0r.A0C.A02()));
                }
                List A02 = ((PaymentSettingsFragment) this).A0T.A02();
                if (!A02.isEmpty()) {
                    A04.removeAllViews();
                    C117785ad r4 = new C117785ad(A01());
                    r4.A00(new C129855yP(new AnonymousClass68Q(this), (C460124c) AnonymousClass01Y.A05(A02).get(0), A02.size()));
                    A04.addView(r4);
                }
            }
            if (!z) {
                i = 8;
            }
            A04.setVisibility(i);
        }
    }
}
