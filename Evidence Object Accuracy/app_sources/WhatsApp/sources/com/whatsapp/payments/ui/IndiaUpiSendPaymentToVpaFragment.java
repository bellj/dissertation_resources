package com.whatsapp.payments.ui;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass102;
import X.AnonymousClass18O;
import X.AnonymousClass18S;
import X.AnonymousClass18T;
import X.AnonymousClass1P3;
import X.AnonymousClass1ZR;
import X.AnonymousClass1ZS;
import X.AnonymousClass60V;
import X.AnonymousClass68Z;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.AnonymousClass6MA;
import X.C117295Zj;
import X.C117315Zl;
import X.C118165bN;
import X.C118205bR;
import X.C120485gG;
import X.C121265hX;
import X.C123805ny;
import X.C129245xP;
import X.C12960it;
import X.C130395zL;
import X.C1308460e;
import X.C1309960u;
import X.C1329668y;
import X.C14850m9;
import X.C14900mE;
import X.C17220qS;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C252718t;
import X.C30931Zj;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment;

/* loaded from: classes4.dex */
public class IndiaUpiSendPaymentToVpaFragment extends Hilt_IndiaUpiSendPaymentToVpaFragment {
    public EditText A00;
    public ProgressBar A01;
    public TextView A02;
    public TextView A03;
    public C14900mE A04;
    public Button A05;
    public Button A06;
    public C18640sm A07;
    public AnonymousClass018 A08;
    public AnonymousClass102 A09;
    public C14850m9 A0A;
    public AnonymousClass1ZR A0B;
    public AnonymousClass1ZR A0C;
    public C17220qS A0D;
    public AnonymousClass68Z A0E;
    public C1308460e A0F;
    public C1329668y A0G;
    public AnonymousClass18T A0H;
    public C18650sn A0I;
    public AnonymousClass18S A0J;
    public C18610sj A0K;
    public C120485gG A0L;
    public AnonymousClass6BE A0M;
    public C129245xP A0N;
    public AnonymousClass69E A0O;
    public C118165bN A0P;
    public AnonymousClass18O A0Q;
    public C121265hX A0R;
    public C1309960u A0S;
    public C18590sh A0T;
    public C252718t A0U;
    public boolean A0V;
    public final C30931Zj A0W = C30931Zj.A00("IndiaUpiSendPaymentToVpaDialogFragment", "payment", "IN");

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        this.A0S.A02(new AnonymousClass6MA() { // from class: X.6D5
            @Override // X.AnonymousClass6MA
            public final void AVY() {
                C1309960u.A01(IndiaUpiSendPaymentToVpaFragment.this.A0B());
            }
        });
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_enter_vpa_dialog);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A0L = null;
        this.A00 = null;
        this.A01 = null;
        this.A02 = null;
        this.A05 = null;
        this.A06 = null;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (this.A0S.A03()) {
            C1309960u.A01(A0B());
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        EditText editText;
        int i;
        this.A0P = (C118165bN) C117315Zl.A06(new C118205bR(this), this).A00(C118165bN.class);
        Context A0p = A0p();
        C14900mE r2 = this.A04;
        C17220qS r4 = this.A0D;
        C18590sh r10 = this.A0T;
        this.A0L = new C120485gG(A0p, r2, this.A09, r4, this.A0E, this.A0F, this.A0I, this.A0K, this.A0R, r10);
        this.A00 = (EditText) AnonymousClass028.A0D(view, R.id.account_id_handle);
        this.A01 = (ProgressBar) AnonymousClass028.A0D(view, R.id.progress);
        this.A02 = C12960it.A0I(view, R.id.error_text);
        this.A05 = (Button) AnonymousClass028.A0D(view, R.id.close_dialog_button);
        this.A06 = (Button) AnonymousClass028.A0D(view, R.id.primary_payment_button);
        this.A03 = C12960it.A0I(view, R.id.title_text);
        this.A06.setEnabled(false);
        boolean A00 = C130395zL.A00(this.A0A, this.A0G.A07());
        this.A0V = A00;
        TextView textView = this.A03;
        if (A00) {
            textView.setText(R.string.upi_enter_vpa_or_upi_number_title);
            editText = this.A00;
            i = R.string.upi_enter_vpa_or_upi_number_hint;
        } else {
            textView.setText(R.string.upi_enter_vpa_title);
            editText = this.A00;
            i = R.string.upi_enter_vpa_hint;
        }
        editText.setHint(i);
        this.A00.addTextChangedListener(new C123805ny(this));
        C117295Zj.A0n(this.A05, this, 73);
        C117295Zj.A0n(this.A06, this, 72);
        Bundle bundle2 = super.A05;
        if (bundle2 != null) {
            AnonymousClass1ZR r22 = (AnonymousClass1ZR) bundle2.getParcelable("extra_payment_handle");
            if (!AnonymousClass1ZS.A02(r22)) {
                EditText editText2 = this.A00;
                Object obj = r22.A00;
                AnonymousClass009.A05(obj);
                C117315Zl.A0N(editText2, obj);
                A19();
            }
        }
        this.A0M.AKg(0, null, "enter_user_payment_id", null);
        C117295Zj.A0s(A0G(), this.A0P.A01, this, 66);
        C117295Zj.A0s(A0G(), this.A0P.A03, this, 65);
        C117295Zj.A0s(A0G(), this.A0P.A02, this, 64);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: X.5gG */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v1, types: [com.whatsapp.jid.UserJid, X.1ZR] */
    /* JADX WARN: Type inference failed for: r4v2 */
    /* JADX WARN: Type inference failed for: r4v3 */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0128  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A19() {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment.A19():void");
    }

    public final void A1A(UserJid userJid, AnonymousClass1ZR r11) {
        C129245xP r1 = this.A0N;
        if (r1 != null) {
            PaymentBottomSheet paymentBottomSheet = r1.A01;
            if (paymentBottomSheet != null) {
                paymentBottomSheet.A1B();
            }
            r1.A06.A00(r1.A02, new AnonymousClass1P3(r11, r1) { // from class: X.66t
                public final /* synthetic */ AnonymousClass1ZR A00;
                public final /* synthetic */ C129245xP A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass1P3
                public final void AVM(boolean z) {
                    C129245xP r5 = this.A01;
                    AnonymousClass1ZR r2 = this.A00;
                    if (z) {
                        Bundle A0D = C12970iu.A0D();
                        A0D.putParcelable("extra_payment_handle", r2);
                        IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment = new IndiaUpiSendPaymentToVpaFragment();
                        indiaUpiSendPaymentToVpaFragment.A0U(A0D);
                        r5.A00 = indiaUpiSendPaymentToVpaFragment;
                        r5.A00(null);
                        return;
                    }
                    AbstractC13860kS r4 = r5.A04;
                    Object[] A1b = C12970iu.A1b();
                    A1b[0] = r5.A03.getString(R.string.india_upi_payment_id_name);
                    r4.Adr(A1b, 0, R.string.unblock_payment_id_error_default);
                }
            }, userJid, r11, false, false);
        }
    }

    public final void A1B(AnonymousClass60V r6) {
        C30931Zj r2 = this.A0W;
        StringBuilder A0k = C12960it.A0k("showErrorText: ");
        A0k.append(r6.A00);
        C117295Zj.A1F(r2, A0k);
        this.A02.setVisibility(0);
        this.A02.setText(r6.A01(A01()));
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            AnonymousClass028.A0M(AnonymousClass00T.A03(A0B, R.color.red_button_text), this.A00);
        }
        this.A0M.AKg(0, 51, "enter_user_payment_id", null);
    }
}
