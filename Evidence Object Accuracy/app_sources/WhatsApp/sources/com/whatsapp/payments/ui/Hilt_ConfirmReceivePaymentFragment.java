package com.whatsapp.payments.ui;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass60Z;
import X.C117305Zk;
import X.C117315Zl;
import X.C129095xA;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C129925yW;
import X.C129945yY;
import X.C130065yk;
import X.C1309660r;
import X.C15570nT;
import X.C18640sm;
import X.C18650sn;
import X.C18660so;
import X.C248217a;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_ConfirmReceivePaymentFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A19()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.Hilt_ConfirmReceivePaymentFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (this instanceof Hilt_BrazilConfirmReceivePaymentFragment) {
            Hilt_BrazilConfirmReceivePaymentFragment hilt_BrazilConfirmReceivePaymentFragment = (Hilt_BrazilConfirmReceivePaymentFragment) this;
            if (!hilt_BrazilConfirmReceivePaymentFragment.A02) {
                hilt_BrazilConfirmReceivePaymentFragment.A02 = true;
                BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment = (BrazilConfirmReceivePaymentFragment) hilt_BrazilConfirmReceivePaymentFragment;
                C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) hilt_BrazilConfirmReceivePaymentFragment.generatedComponent());
                AnonymousClass01J r2 = r3.A0Y;
                C12960it.A1B(r2, brazilConfirmReceivePaymentFragment);
                ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A04 = C117305Zk.A0P(r2);
                ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A03 = (C248217a) r2.AE5.get();
                brazilConfirmReceivePaymentFragment.A03 = C12980iv.A0b(r2);
                brazilConfirmReceivePaymentFragment.A06 = C12960it.A0S(r2);
                brazilConfirmReceivePaymentFragment.A00 = C12970iu.A0R(r2);
                brazilConfirmReceivePaymentFragment.A01 = (C15570nT) r2.AAr.get();
                brazilConfirmReceivePaymentFragment.A07 = C12990iw.A0c(r2);
                brazilConfirmReceivePaymentFragment.A04 = C12960it.A0R(r2);
                brazilConfirmReceivePaymentFragment.A0J = C117315Zl.A0F(r2);
                brazilConfirmReceivePaymentFragment.A08 = (C1309660r) r2.A1p.get();
                brazilConfirmReceivePaymentFragment.A0H = (C129945yY) r2.A1q.get();
                brazilConfirmReceivePaymentFragment.A0E = C117305Zk.A0M(r2);
                brazilConfirmReceivePaymentFragment.A0F = C117305Zk.A0O(r2);
                brazilConfirmReceivePaymentFragment.A0G = (C129095xA) r2.ACy.get();
                brazilConfirmReceivePaymentFragment.A05 = C117305Zk.A0G(r2);
                brazilConfirmReceivePaymentFragment.A0B = (C129925yW) r2.AEW.get();
                brazilConfirmReceivePaymentFragment.A09 = r3.A0V.A08();
                brazilConfirmReceivePaymentFragment.A0A = (AnonymousClass60Z) r2.A24.get();
                brazilConfirmReceivePaymentFragment.A02 = (C18640sm) r2.A3u.get();
                brazilConfirmReceivePaymentFragment.A0C = (C18650sn) r2.AEe.get();
                brazilConfirmReceivePaymentFragment.A0D = (C18660so) r2.AEf.get();
                brazilConfirmReceivePaymentFragment.A0I = (C130065yk) r2.A1z.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            ConfirmReceivePaymentFragment confirmReceivePaymentFragment = (ConfirmReceivePaymentFragment) this;
            AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            C12960it.A1B(r1, confirmReceivePaymentFragment);
            confirmReceivePaymentFragment.A04 = C117305Zk.A0P(r1);
            confirmReceivePaymentFragment.A03 = (C248217a) r1.AE5.get();
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
