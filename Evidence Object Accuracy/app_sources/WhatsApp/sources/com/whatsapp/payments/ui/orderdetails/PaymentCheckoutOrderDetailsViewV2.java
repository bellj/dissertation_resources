package com.whatsapp.payments.ui.orderdetails;

import X.AbstractC14440lR;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass14X;
import X.AnonymousClass19O;
import X.AnonymousClass19Q;
import X.AnonymousClass1FB;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C14850m9;
import X.C18640sm;
import X.C21270x9;
import X.C21770xx;
import X.C247116o;
import X.C252918v;
import X.C37071lG;
import X.C53832fK;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes4.dex */
public class PaymentCheckoutOrderDetailsViewV2 extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public Button A01;
    public RelativeLayout A02;
    public RecyclerView A03;
    public WaTextView A04;
    public WaTextView A05;
    public WaTextView A06;
    public C247116o A07;
    public C21770xx A08;
    public AnonymousClass19Q A09;
    public AnonymousClass1FB A0A;
    public C37071lG A0B;
    public C53832fK A0C;
    public C21270x9 A0D;
    public C18640sm A0E;
    public AnonymousClass018 A0F;
    public C14850m9 A0G;
    public AnonymousClass14X A0H;
    public AnonymousClass19O A0I;
    public AbstractC14440lR A0J;
    public AnonymousClass2P7 A0K;
    public boolean A0L;

    public PaymentCheckoutOrderDetailsViewV2(Context context) {
        this(context, null);
    }

    public PaymentCheckoutOrderDetailsViewV2(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PaymentCheckoutOrderDetailsViewV2(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public PaymentCheckoutOrderDetailsViewV2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2P6 r2 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r1 = r2.A06;
            this.A0G = C12960it.A0S(r1);
            this.A0D = C12970iu.A0W(r1);
            this.A0B = new C37071lG((C252918v) r2.A03.A1E.A2x.get());
            this.A0F = C12960it.A0R(r1);
            this.A0J = (AbstractC14440lR) r1.ANe.get();
            this.A0H = (AnonymousClass14X) r1.AFM.get();
            this.A08 = (C21770xx) r1.A2s.get();
            this.A0A = (AnonymousClass1FB) r1.AGH.get();
            this.A09 = (AnonymousClass19Q) r1.A2u.get();
            this.A0E = (C18640sm) r1.A3u.get();
            this.A0I = (AnonymousClass19O) r1.ACO.get();
            this.A07 = (C247116o) r1.A3W.get();
        }
        LayoutInflater.from(context).inflate(R.layout.payment_checkout_order_details2_view, (ViewGroup) this, true);
        this.A03 = (RecyclerView) AnonymousClass028.A0D(this, R.id.order_detail_recycler_view);
        this.A05 = C12960it.A0N(this, R.id.total_key);
        this.A06 = C12960it.A0N(this, R.id.total_amount);
        this.A01 = (Button) AnonymousClass028.A0D(this, R.id.proceed_to_pay_btn);
        this.A04 = C12960it.A0N(this, R.id.expiry_footer);
        this.A00 = AnonymousClass028.A0D(this, R.id.shadow_top);
        this.A02 = (RelativeLayout) AnonymousClass028.A0D(this, R.id.buttons);
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x01fe A[LOOP:1: B:38:0x01f8->B:40:0x01fe, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x02bb  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.ActivityC000800j r28, X.C15570nT r29, X.EnumC124545pi r30, X.C128335vw r31, java.lang.String r32, int r33) {
        /*
        // Method dump skipped, instructions count: 780
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.orderdetails.PaymentCheckoutOrderDetailsViewV2.A00(X.00j, X.0nT, X.5pi, X.5vw, java.lang.String, int):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0K;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0K = r0;
        }
        return r0.generatedComponent();
    }
}
