package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractC001200n;
import X.AbstractC005102i;
import X.AbstractC116305Ux;
import X.AbstractC15460nI;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass02P;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass652;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118065bD;
import X.C118305bb;
import X.C126155sQ;
import X.C128355vy;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C134186Dq;
import X.C15570nT;
import X.C21270x9;
import X.C252718t;
import X.C25921Bi;
import X.C624437g;
import X.C63883Dh;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.payments.ui.IndiaUpiSecureQrCodeDisplayActivity;
import com.whatsapp.payments.ui.widget.IndiaUpiDisplaySecureQrCodeView;

/* loaded from: classes4.dex */
public class IndiaUpiSecureQrCodeDisplayActivity extends AbstractActivityC121665jA {
    public View A00;
    public TextView A01;
    public AnonymousClass130 A02;
    public AnonymousClass1J1 A03;
    public C21270x9 A04;
    public C128355vy A05;
    public IndiaUpiDisplaySecureQrCodeView A06;
    public C118065bD A07;
    public C25921Bi A08;
    public boolean A09;
    public final C63883Dh A0A;

    public IndiaUpiSecureQrCodeDisplayActivity() {
        this(0);
        this.A0A = new C63883Dh();
    }

    public IndiaUpiSecureQrCodeDisplayActivity(int i) {
        this.A09 = false;
        C117295Zj.A0p(this, 72);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A04 = C12970iu.A0W(A1M);
            this.A02 = (AnonymousClass130) A1M.A41.get();
            this.A08 = (C25921Bi) A1M.AIa.get();
            this.A05 = (C128355vy) A1M.A9f.get();
        }
    }

    public final void A30() {
        if (this.A06.A09 != null && !isFinishing() && this.A00 != null) {
            A32(false);
            this.A00.setDrawingCacheEnabled(true);
            this.A00.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            View view = this.A00;
            view.layout(0, 0, view.getMeasuredWidth(), this.A00.getMeasuredHeight());
            this.A00.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
            this.A00.buildDrawingCache(true);
            C25921Bi.A00(this, Bitmap.createBitmap(this.A00.getDrawingCache()), this.A07.A04().A04);
            this.A00.setDrawingCacheEnabled(false);
            A32(true);
        }
    }

    public final void A31() {
        View rootView = getWindow().getDecorView().getRootView();
        if (C252718t.A00(rootView)) {
            shareQrCodeOnKeyboardDismissed(rootView);
            return;
        }
        this.A06.A04(true);
        A32(false);
        this.A00.setDrawingCacheEnabled(true);
        C25921Bi r1 = this.A08;
        Context applicationContext = getApplicationContext();
        Bitmap drawingCache = this.A00.getDrawingCache();
        C134186Dq r3 = new AbstractC116305Ux() { // from class: X.6Dq
            @Override // X.AbstractC116305Ux
            public final void AVz(Intent intent) {
                IndiaUpiSecureQrCodeDisplayActivity indiaUpiSecureQrCodeDisplayActivity = IndiaUpiSecureQrCodeDisplayActivity.this;
                if (intent == null || intent.resolveActivity(indiaUpiSecureQrCodeDisplayActivity.getPackageManager()) == null) {
                    ((ActivityC13810kN) indiaUpiSecureQrCodeDisplayActivity).A05.A07(R.string.share_qr_code_failed, 1);
                    return;
                }
                indiaUpiSecureQrCodeDisplayActivity.startActivityForResult(intent, 1006);
                indiaUpiSecureQrCodeDisplayActivity.A00.setDrawingCacheEnabled(false);
            }
        };
        C12960it.A1E(new C624437g(applicationContext, drawingCache, r1.A00, r3), r1.A01);
        A32(true);
    }

    public final void A32(boolean z) {
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (r0.A01 == null) {
            return;
        }
        if (z) {
            AnonymousClass1J1 r2 = this.A03;
            C15570nT r02 = ((ActivityC13790kL) this).A01;
            r02.A08();
            r2.A06(C117305Zk.A06(this, R.id.contact_photo), r02.A01);
        } else if (C12970iu.A01(((ActivityC13810kN) this).A09.A00, "privacy_profile_photo") != 0) {
            AnonymousClass130 r22 = this.A02;
            ImageView A06 = C117305Zk.A06(this, R.id.contact_photo);
            C15570nT r03 = ((ActivityC13790kL) this).A01;
            r03.A08();
            r22.A06(A06, r03.A01);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1006) {
            this.A06.A04(false);
        } else if (i != 202) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            A31();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(R.layout.india_payments_dispay_secure_qr_code);
        this.A01 = C12970iu.A0M(this, R.id.scan_to_pay_info);
        this.A06 = (IndiaUpiDisplaySecureQrCodeView) findViewById(R.id.display_qr_code_view);
        String str2 = null;
        if (C12990iw.A0H(this) != null) {
            str = C12990iw.A0H(this).getString("extra_account_holder_name");
        } else {
            str = null;
        }
        if (!TextUtils.isEmpty(str)) {
            str2 = str.trim();
        }
        C118065bD r4 = (C118065bD) C117315Zl.A06(new C118305bb(this, this.A05), this).A00(C118065bD.class);
        this.A07 = r4;
        IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(this, 63);
        IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(this, 62);
        AnonymousClass02P r0 = r4.A02;
        AbstractC001200n r1 = r4.A00;
        r0.A05(r1, A0B);
        r4.A01.A05(r1, A0B2);
        r4.A07(str2);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.my_qr_code);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back);
            C117305Zk.A13(getResources(), drawable, R.color.colorGrayQRCodeIcon);
            A1U.A0D(drawable);
            A1U.A0M(true);
            A1U.A07(0.0f);
            View findViewById = findViewById(R.id.parent_view);
            findViewById.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener(findViewById, A1U, this) { // from class: X.657
                public final /* synthetic */ View A00;
                public final /* synthetic */ AbstractC005102i A01;
                public final /* synthetic */ IndiaUpiSecureQrCodeDisplayActivity A02;

                {
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    this.A01.A07(C117295Zj.A00(this.A02, this.A00));
                }
            });
        }
        this.A06.setup(this.A07);
        this.A03 = this.A04.A04(this, "india-upi-secure-qr-code-display-activity");
        A32(true);
        CopyableTextView copyableTextView = (CopyableTextView) findViewById(R.id.user_wa_vpa);
        String str3 = this.A07.A04().A0C;
        copyableTextView.A02 = str3;
        copyableTextView.setText(C12960it.A0X(this, str3, new Object[1], 0, R.string.vpa_prefix));
        C12970iu.A0M(this, R.id.user_account_name).setText(this.A07.A04().A04);
        C12970iu.A0M(this, R.id.user_wa_phone).setText(C117305Zk.A0h(((ActivityC13790kL) this).A01));
        this.A00 = findViewById(R.id.qrcode_view);
        this.A01.setText(C12960it.A0X(this, this.A07.A04().A04, new Object[1], 0, R.string.scan_this_code_to_pay_user));
        this.A07.A06(null, 0);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        Drawable drawable = getResources().getDrawable(R.drawable.ic_action_share);
        C117305Zk.A13(getResources(), drawable, R.color.colorGrayQRCodeIcon);
        menu.add(0, R.id.menuitem_share_qr, 0, R.string.share).setIcon(drawable).setShowAsAction(1);
        if (Build.VERSION.SDK_INT >= 19) {
            menu.add(0, R.id.menuitem_print, 0, R.string.print_qr_code);
        }
        if (((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0v)) {
            menu.add(0, R.id.menuitem_sign_qr, 0, R.string.upi_signing_qr_code_revoke_menu_item);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A03.A00();
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_share_qr) {
            if (AnonymousClass00T.A01(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                int i = Build.VERSION.SDK_INT;
                int i2 = R.string.payment_permission_storage_need_write_access_v30;
                if (i < 30) {
                    i2 = R.string.payment_permission_storage_need_write_access;
                }
                startActivityForResult(RequestPermissionActivity.A03(this, R.string.payment_permission_storage_need_write_access_request, i2, true), 202);
                return true;
            }
            this.A07.A06(new C126155sQ(C12970iu.A0p(this.A06.A0F)), 4);
            return true;
        } else if (itemId == 16908332) {
            AnonymousClass00T.A08(this);
            return true;
        } else {
            if (itemId == R.id.menuitem_print) {
                A30();
            } else if (itemId == R.id.menuitem_sign_qr) {
                this.A07.A05(-1);
            }
            return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        IndiaUpiDisplaySecureQrCodeView indiaUpiDisplaySecureQrCodeView = this.A06;
        if (indiaUpiDisplaySecureQrCodeView.A00.getVisibility() == 0) {
            indiaUpiDisplaySecureQrCodeView.A0F.requestFocus();
            indiaUpiDisplaySecureQrCodeView.A0F.A04(true);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0A.A01(getWindow(), ((ActivityC13810kN) this).A08);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0A.A00(getWindow());
    }

    public final void shareQrCodeOnKeyboardDismissed(View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass652(view, this));
        ((ActivityC13790kL) this).A0D.A01(view);
    }
}
