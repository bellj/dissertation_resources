package com.whatsapp.payments.ui;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass19Q;
import X.AnonymousClass1FB;
import X.AnonymousClass1ZC;
import X.AnonymousClass1ZD;
import X.AnonymousClass1ZF;
import X.AnonymousClass1ZG;
import X.AnonymousClass1ZH;
import X.AnonymousClass2FL;
import X.AnonymousClass3EX;
import X.C117295Zj;
import X.C117315Zl;
import X.C118605c5;
import X.C118655cA;
import X.C12960it;
import X.C21770xx;
import X.C252918v;
import X.C37071lG;
import X.C53832fK;
import X.C66023Lz;
import X.C67383Rh;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape5S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public class PaymentCheckoutOrderDetailsItemListActivity extends ActivityC13790kL {
    public RecyclerView A00;
    public C21770xx A01;
    public AnonymousClass19Q A02;
    public AnonymousClass1FB A03;
    public C252918v A04;
    public C53832fK A05;
    public AnonymousClass018 A06;
    public boolean A07;

    public PaymentCheckoutOrderDetailsItemListActivity() {
        this(0);
    }

    public PaymentCheckoutOrderDetailsItemListActivity(int i) {
        this.A07 = false;
        C117295Zj.A0p(this, 98);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (C21770xx) A1M.A2s.get();
            this.A06 = C12960it.A0R(A1M);
            this.A04 = (C252918v) A1M.A2x.get();
            this.A03 = (AnonymousClass1FB) A1M.AGH.get();
            this.A02 = (AnonymousClass19Q) A1M.A2u.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.payment_checkout_order_details_item_list_view);
        String stringExtra = getIntent().getStringExtra("message_title");
        AnonymousClass1ZD r4 = (AnonymousClass1ZD) getIntent().getParcelableExtra("message_content");
        UserJid nullable = UserJid.getNullable(getIntent().getStringExtra("business_owner_jid"));
        AnonymousClass009.A05(r4);
        List<C66023Lz> list = r4.A04.A08;
        AnonymousClass009.A0E(!list.isEmpty());
        AnonymousClass009.A05(nullable);
        ArrayList A0l = C12960it.A0l();
        for (C66023Lz r0 : list) {
            String A00 = r0.A00();
            if (!TextUtils.isEmpty(A00)) {
                A0l.add(new AnonymousClass1ZG(A00));
            }
        }
        AnonymousClass1ZF r5 = new AnonymousClass1ZF(null, A0l);
        String A002 = ((C66023Lz) list.get(0)).A00();
        if (A002 == null) {
            A002 = "";
        }
        AnonymousClass1ZC r10 = new AnonymousClass1ZC(nullable, new AnonymousClass1ZH(A002, r4.A0C, false), Collections.singletonList(r5));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0I(stringExtra);
        }
        this.A00 = (RecyclerView) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.item_list);
        C118605c5 r3 = new C118605c5(new C37071lG(this.A04), this.A06, r4);
        this.A00.A0l(new C118655cA());
        this.A00.setAdapter(r3);
        C53832fK r02 = (C53832fK) C117315Zl.A06(new C67383Rh(getApplication(), this.A03, new AnonymousClass3EX(this.A01, this.A02, nullable, ((ActivityC13830kP) this).A05), ((ActivityC13810kN) this).A07, nullable, r10), this).A00(C53832fK.class);
        this.A05 = r02;
        r02.A01.A05(this, new IDxObserverShape5S0200000_3_I1(r3, 2, this));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A05.A04();
    }
}
