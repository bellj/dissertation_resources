package com.whatsapp.payments.ui;

import X.AbstractActivityC119275dS;
import X.AbstractC005102i;
import X.AbstractC28901Pl;
import X.AbstractView$OnClickListenerC121765jx;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass60Y;
import X.AnonymousClass61F;
import X.C117295Zj;
import X.C117305Zk;
import X.C129235xO;
import X.C12960it;
import X.C12970iu;
import X.C1311161i;
import X.C30861Zc;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity;

/* loaded from: classes4.dex */
public class NoviPaymentBankDetailsActivity extends AbstractView$OnClickListenerC121765jx {
    public FrameLayout A00;
    public AnonymousClass60Y A01;
    public AnonymousClass61F A02;
    public C129235xO A03;
    public boolean A04;

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override // android.view.Window.Callback
    public void onPointerCaptureChanged(boolean z) {
    }

    public NoviPaymentBankDetailsActivity() {
        this(0);
    }

    public NoviPaymentBankDetailsActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 89);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119275dS.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A01 = C117305Zk.A0W(A1M);
            this.A02 = C117305Zk.A0X(A1M);
            this.A03 = (C129235xO) A1M.ADR.get();
        }
    }

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2i(AbstractC28901Pl r6, boolean z) {
        super.A2i(r6, z);
        ((AbstractView$OnClickListenerC121765jx) this).A03.setText(C1311161i.A03(this, (C30861Zc) r6));
        View inflate = getLayoutInflater().inflate(R.layout.novi_payment_card_details_alert, (ViewGroup) null);
        View A0D = AnonymousClass028.A0D(inflate, R.id.novi_payment_card_details_alert_icon);
        TextView A0I = C12960it.A0I(inflate, R.id.novi_payment_card_details_alert_message);
        A0D.setVisibility(8);
        A0I.setText(R.string.novi_payment_bank_status);
        this.A00.addView(inflate);
        View inflate2 = getLayoutInflater().inflate(R.layout.novi_payment_method_option_remove, (ViewGroup) null);
        ((AbstractView$OnClickListenerC121765jx) this).A00 = AnonymousClass00T.A00(this, R.color.fb_pay_hub_icon_tint);
        String string = getString(R.string.remove_bank_account);
        C12960it.A0I(inflate2, R.id.novi_payment_method_remove_option_text).setText(string);
        AnonymousClass2GE.A07(C12970iu.A0K(inflate2, R.id.novi_payment_method_remove_option_icon), ((AbstractView$OnClickListenerC121765jx) this).A00);
        ((ViewGroup) findViewById(R.id.widget_container)).addView(inflate2);
        inflate2.setOnClickListener(new View.OnClickListener(r6, this, string) { // from class: X.64I
            public final /* synthetic */ AbstractC28901Pl A00;
            public final /* synthetic */ NoviPaymentBankDetailsActivity A01;
            public final /* synthetic */ String A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                NoviPaymentBankDetailsActivity noviPaymentBankDetailsActivity = this.A01;
                AbstractC28901Pl r3 = this.A00;
                String str = this.A02;
                C128365vz.A00(r3, noviPaymentBankDetailsActivity.A01, "REMOVE_FI_CLICK");
                C130295zB.A00(noviPaymentBankDetailsActivity, C126995tm.A00(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003c: INVOKE  
                      (wrap: X.04S : 0x0038: INVOKE  (r0v5 X.04S A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (wrap: X.5tm : 0x0028: INVOKE  (r5v0 X.5tm A[REMOVE]) = 
                      (wrap: X.6IK : 0x0025: CONSTRUCTOR  (r0v3 X.6IK A[REMOVE]) = (r3v0 'r3' X.1Pl), (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6IK.<init>(X.1Pl, com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.novi_confirm_button_label int)
                     type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                      (wrap: X.5tm : 0x0034: INVOKE  (r6v0 X.5tm A[REMOVE]) = 
                      (wrap: X.6Gh : 0x0031: CONSTRUCTOR  (r0v4 X.6Gh A[REMOVE]) = (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6Gh.<init>(com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.cancel int)
                     type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                      (wrap: java.lang.String : 0x0015: INVOKE  (r7v0 java.lang.String A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (r2v0 'str' java.lang.String)
                      (wrap: java.lang.Object[] : 0x0010: INVOKE  (r0v1 java.lang.Object[] A[REMOVE]) =  type: STATIC call: X.0iu.A1b():java.lang.Object[])
                      (0 int)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.novi_payment_remove_method_dialog_title int)
                     type: STATIC call: X.0it.A0X(android.content.Context, java.lang.Object, java.lang.Object[], int, int):java.lang.String)
                      (wrap: java.lang.String : 0x001c: INVOKE  (r8v0 java.lang.String A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.novi_payment_remove_bank_dialog_message int)
                     type: VIRTUAL call: android.content.Context.getString(int):java.lang.String)
                      false
                     type: STATIC call: X.5zB.A00(android.content.Context, X.5tm, X.5tm, java.lang.String, java.lang.String, boolean):X.04S)
                     type: VIRTUAL call: android.app.Dialog.show():void in method: X.64I.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0038: INVOKE  (r0v5 X.04S A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (wrap: X.5tm : 0x0028: INVOKE  (r5v0 X.5tm A[REMOVE]) = 
                      (wrap: X.6IK : 0x0025: CONSTRUCTOR  (r0v3 X.6IK A[REMOVE]) = (r3v0 'r3' X.1Pl), (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6IK.<init>(X.1Pl, com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.novi_confirm_button_label int)
                     type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                      (wrap: X.5tm : 0x0034: INVOKE  (r6v0 X.5tm A[REMOVE]) = 
                      (wrap: X.6Gh : 0x0031: CONSTRUCTOR  (r0v4 X.6Gh A[REMOVE]) = (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6Gh.<init>(com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.cancel int)
                     type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                      (wrap: java.lang.String : 0x0015: INVOKE  (r7v0 java.lang.String A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (r2v0 'str' java.lang.String)
                      (wrap: java.lang.Object[] : 0x0010: INVOKE  (r0v1 java.lang.Object[] A[REMOVE]) =  type: STATIC call: X.0iu.A1b():java.lang.Object[])
                      (0 int)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.novi_payment_remove_method_dialog_title int)
                     type: STATIC call: X.0it.A0X(android.content.Context, java.lang.Object, java.lang.Object[], int, int):java.lang.String)
                      (wrap: java.lang.String : 0x001c: INVOKE  (r8v0 java.lang.String A[REMOVE]) = 
                      (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.novi_payment_remove_bank_dialog_message int)
                     type: VIRTUAL call: android.content.Context.getString(int):java.lang.String)
                      false
                     type: STATIC call: X.5zB.A00(android.content.Context, X.5tm, X.5tm, java.lang.String, java.lang.String, boolean):X.04S in method: X.64I.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.addArgDot(InsnGen.java:93)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:767)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0028: INVOKE  (r5v0 X.5tm A[REMOVE]) = 
                      (wrap: X.6IK : 0x0025: CONSTRUCTOR  (r0v3 X.6IK A[REMOVE]) = (r3v0 'r3' X.1Pl), (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6IK.<init>(X.1Pl, com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR)
                      (wrap: ?? : ?: SGET   com.whatsapp.R.string.novi_confirm_button_label int)
                     type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm in method: X.64I.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0025: CONSTRUCTOR  (r0v3 X.6IK A[REMOVE]) = (r3v0 'r3' X.1Pl), (r4v0 'noviPaymentBankDetailsActivity' com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity) call: X.6IK.<init>(X.1Pl, com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity):void type: CONSTRUCTOR in method: X.64I.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 27 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IK, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 33 more
                    */
                /*
                    this = this;
                    com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity r4 = r10.A01
                    X.1Pl r3 = r10.A00
                    java.lang.String r2 = r10.A02
                    X.60Y r1 = r4.A01
                    java.lang.String r0 = "REMOVE_FI_CLICK"
                    X.C128365vz.A00(r3, r1, r0)
                    r1 = 2131889831(0x7f120ea7, float:1.9414337E38)
                    java.lang.Object[] r0 = X.C12970iu.A1b()
                    r9 = 0
                    java.lang.String r7 = X.C12960it.A0X(r4, r2, r0, r9, r1)
                    r0 = 2131889828(0x7f120ea4, float:1.941433E38)
                    java.lang.String r8 = r4.getString(r0)
                    r1 = 2131889745(0x7f120e51, float:1.9414162E38)
                    X.6IK r0 = new X.6IK
                    r0.<init>(r3, r4)
                    X.5tm r5 = X.C126995tm.A00(r0, r1)
                    r1 = 2131886925(0x7f12034d, float:1.9408443E38)
                    X.6Gh r0 = new X.6Gh
                    r0.<init>(r4)
                    X.5tm r6 = X.C126995tm.A00(r0, r1)
                    X.04S r0 = X.C130295zB.A00(r4, r5, r6, r7, r8, r9)
                    r0.show()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass64I.onClick(android.view.View):void");
            }
        });
        setResult(1);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass60Y.A02(this.A01, "BACK_CLICK", "NOVI_HUB", "FI_INFO", "ARROW");
    }

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.payment_bank_details_title);
            A2h();
            ((AbstractView$OnClickListenerC121765jx) this).A0G.A0B(((AbstractView$OnClickListenerC121765jx) this).A0G.getCurrentContentInsetLeft(), 0);
        }
        this.A00 = (FrameLayout) findViewById(R.id.method_details_alert_container);
        C117305Zk.A06(this, R.id.help_icon).setImageResource(R.drawable.ic_help_novi);
        C12970iu.A0M(this, R.id.help_label).setText(R.string.novi_help_center);
        ((AbstractView$OnClickListenerC121765jx) this).A04.setVisibility(8);
        C12970iu.A1N(this, R.id.default_payment_method_container, 8);
        C117295Zj.A0r(this, this.A02.A0G, 95);
        AnonymousClass60Y.A02(this.A01, "NAVIGATION_START", "NOVI_HUB", "FI_INFO", "SCREEN");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(this.A01, "NAVIGATION_END", "NOVI_HUB", "FI_INFO", "SCREEN");
    }
}
