package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1ZY;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C119755f3;
import X.C12960it;
import X.C12970iu;
import X.C1311161i;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class IndiaUpiPinSetUpCompletedActivity extends AbstractActivityC121665jA {
    public boolean A00;

    public IndiaUpiPinSetUpCompletedActivity() {
        this(0);
    }

    public IndiaUpiPinSetUpCompletedActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 65);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "pin_created", null);
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass1ZY r4;
        getWindow().addFlags(DefaultCrypto.BUFFER_SIZE);
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_pin_set_up_completed);
        AbstractC28901Pl r6 = (AbstractC28901Pl) getIntent().getParcelableExtra("extra_bank_account");
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            C117305Zk.A16(A0K, R.string.payments_activity_title);
        }
        if (r6 == null || (r4 = r6.A08) == null) {
            Log.e("Screen called without valid account, finishing");
            finish();
            return;
        }
        C119755f3 r42 = (C119755f3) r4;
        View A0D = AbstractActivityC119235dO.A0D(this);
        Bitmap A05 = r6.A05();
        ImageView A0K2 = C12970iu.A0K(A0D, R.id.provider_icon);
        if (A05 != null) {
            A0K2.setImageBitmap(A05);
        } else {
            A0K2.setImageResource(R.drawable.av_bank);
        }
        C12960it.A0I(A0D, R.id.account_number).setText(C1311161i.A02(this, ((ActivityC13830kP) this).A01, r6, ((AbstractActivityC121685jC) this).A0P, false));
        C117315Zl.A0N(C12960it.A0I(A0D, R.id.account_name), C117295Zj.A0R(r42.A03));
        C12960it.A0I(A0D, R.id.account_type).setText(r42.A0E());
        if (getIntent().getBooleanExtra("on_settings_page", false)) {
            C12970iu.A0M(this, R.id.continue_button).setText(R.string.done);
        }
        C117295Zj.A0n(findViewById(R.id.continue_button), this, 62);
        ((AbstractActivityC121665jA) this).A0D.AKg(0, null, "pin_created", null);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "pin_created", null);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
