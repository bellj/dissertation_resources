package com.whatsapp.payments.ui;

import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass6BE;
import X.AnonymousClass6LU;
import X.C117295Zj;
import X.C12960it;
import X.C12980iv;
import X.C14900mE;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentRaiseComplaintFragment extends Hilt_IndiaUpiPaymentRaiseComplaintFragment {
    public AnonymousClass12P A00;
    public C14900mE A01;
    public AnonymousClass01d A02;
    public AnonymousClass6BE A03;
    public AnonymousClass6LU A04;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A04 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_payment_raise_complaint_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.complaint_button), this, 47);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.close), this, 48);
        this.A03.AKg(C12980iv.A0i(), null, "raise_complaint_prompt", null);
    }
}
