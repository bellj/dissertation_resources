package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121685jC;
import X.AbstractC1310961f;
import X.AbstractC136516Mv;
import X.AbstractC14590lg;
import X.AbstractC14640lm;
import X.AbstractC30791Yv;
import X.ActivityC000800j;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass102;
import X.AnonymousClass12P;
import X.AnonymousClass1BZ;
import X.AnonymousClass1KS;
import X.AnonymousClass2FL;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61F;
import X.AnonymousClass61M;
import X.AnonymousClass6BD;
import X.AnonymousClass6D0;
import X.AnonymousClass6F2;
import X.AnonymousClass6M9;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118145bL;
import X.C121735jr;
import X.C125905s0;
import X.C126095sK;
import X.C126995tm;
import X.C127135u0;
import X.C127145u1;
import X.C127155u2;
import X.C127505ub;
import X.C127785v3;
import X.C128295vs;
import X.C128305vt;
import X.C128365vz;
import X.C128375w0;
import X.C12960it;
import X.C12980iv;
import X.C129865yQ;
import X.C130105yo;
import X.C130125yq;
import X.C130155yt;
import X.C130295zB;
import X.C134086Dg;
import X.C15380n4;
import X.C16630pM;
import X.C20910wW;
import X.C20920wX;
import X.C22460z7;
import X.C22540zF;
import X.C30821Yy;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.MenuItem;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;
import com.whatsapp.payments.ui.NoviSharedPaymentActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.math.BigDecimal;
import java.util.List;

/* loaded from: classes4.dex */
public class NoviSharedPaymentActivity extends AbstractActivityC121685jC implements AbstractC136516Mv, AbstractC1310961f, AnonymousClass6M9 {
    public C20920wX A00;
    public AnonymousClass1BZ A01;
    public AnonymousClass018 A02;
    public AbstractC30791Yv A03;
    public AnonymousClass102 A04;
    public AnonymousClass61M A05;
    public C130155yt A06;
    public C129865yQ A07;
    public C22540zF A08;
    public C22460z7 A09;
    public C130125yq A0A;
    public AnonymousClass60Y A0B;
    public AnonymousClass6BD A0C;
    public AnonymousClass61F A0D;
    public C130105yo A0E;
    public C134086Dg A0F;
    public C128375w0 A0G;
    public C118145bL A0H;
    public C121735jr A0I;
    public PaymentView A0J;
    public C16630pM A0K;
    public boolean A0L;

    @Override // X.AbstractC136516Mv
    public ActivityC000800j AAa() {
        return this;
    }

    @Override // X.AbstractC136516Mv
    public String AFG() {
        return null;
    }

    @Override // X.AbstractC136516Mv
    public boolean AK3() {
        return false;
    }

    @Override // X.AbstractC1310961f
    public void ALy() {
    }

    @Override // X.AbstractC136486Ms
    public void APq(String str) {
    }

    @Override // X.AbstractC136486Ms
    public void AQf(String str, boolean z) {
    }

    @Override // X.AbstractC1310961f
    public void AR5() {
    }

    @Override // X.AbstractC1310961f
    public void ATT() {
    }

    @Override // X.AbstractC1310961f
    public void ATV() {
    }

    @Override // X.AbstractC1310961f
    public /* synthetic */ void ATa() {
    }

    @Override // X.AbstractC1310961f
    public void AV5(C30821Yy r1, String str) {
    }

    @Override // X.AbstractC1310961f
    public void AVq() {
    }

    @Override // X.AbstractC1310961f
    public void AVs() {
    }

    @Override // X.AbstractC1310961f
    public void AXP(boolean z) {
    }

    public NoviSharedPaymentActivity() {
        this(0);
    }

    public NoviSharedPaymentActivity(int i) {
        this.A0L = false;
        C117295Zj.A0p(this, 93);
    }

    public static /* synthetic */ void A02(NoviSharedPaymentActivity noviSharedPaymentActivity) {
        super.onBackPressed();
        C128365vz r2 = new AnonymousClass610("BACK_CLICK", "SEND_MONEY", "ENTER_AMOUNT", "SCREEN").A00;
        r2.A00 = Boolean.valueOf(!TextUtils.isEmpty(noviSharedPaymentActivity.A0J.getPaymentNote()));
        noviSharedPaymentActivity.A0B.A06(r2);
        noviSharedPaymentActivity.A0C.AKg(1, 1, "new_payment", null);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            this.A02 = C12960it.A0R(A1M);
            this.A0B = C117305Zk.A0W(A1M);
            this.A00 = C20910wW.A00();
            this.A06 = (C130155yt) A1M.ADA.get();
            this.A01 = (AnonymousClass1BZ) A1M.A1l.get();
            this.A0D = C117305Zk.A0X(A1M);
            this.A0A = (C130125yq) A1M.ADJ.get();
            this.A0E = (C130105yo) A1M.ADZ.get();
            this.A04 = C117305Zk.A0G(A1M);
            this.A0K = (C16630pM) A1M.AIc.get();
            this.A05 = C117305Zk.A0N(A1M);
            this.A0G = C117315Zl.A0E(A1M);
            this.A09 = (C22460z7) A1M.AEF.get();
            this.A0C = (AnonymousClass6BD) A1M.ADT.get();
            this.A08 = (C22540zF) A1M.AEC.get();
        }
    }

    public final void A2o(Runnable runnable) {
        if (C118145bL.A02(this.A0H)) {
            C130295zB.A00(this, C126995tm.A00(new Runnable(runnable) { // from class: X.6Gq
                public final /* synthetic */ Runnable A00;

                {
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.A00.run();
                }
            }, R.string.novi_payment_exit_tpp_go_back), C126995tm.A00(new Runnable(runnable) { // from class: X.6IO
                public final /* synthetic */ Runnable A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    NoviSharedPaymentActivity noviSharedPaymentActivity = NoviSharedPaymentActivity.this;
                    Runnable runnable2 = this.A01;
                    C118145bL r2 = noviSharedPaymentActivity.A0H;
                    IDxAListenerShape1S0200000_3_I1 A09 = C117305Zk.A09(runnable2, noviSharedPaymentActivity, 40);
                    AnonymousClass61D A00 = C118145bL.A00(r2);
                    Object A01 = r2.A0z.A01();
                    AnonymousClass009.A05(A01);
                    C130155yt r3 = A00.A03;
                    AnonymousClass61S[] r22 = new AnonymousClass61S[2];
                    AnonymousClass61S.A04("action", "novi-decline-tpp-transaction-request", r22);
                    C130155yt.A01(new IDxAListenerShape19S0100000_3_I1(A09, 4), r3, C117295Zj.A0F(AnonymousClass61S.A00("tpp_transaction_request_id", (String) A01), r22, 1));
                }
            }, R.string.novi_payment_exit_tpp_decline), getString(R.string.novi_payment_exit_tpp_message_title_text), getString(R.string.novi_payment_exit_tpp_message_text), false).show();
            return;
        }
        runnable.run();
    }

    @Override // X.AbstractC136516Mv
    public boolean AJr() {
        return TextUtils.isEmpty(this.A0l) && !C118145bL.A02(this.A0H);
    }

    @Override // X.AbstractC136486Ms
    public void AMA(String str) {
        C118145bL r4 = this.A0H;
        AbstractC30791Yv r1 = r4.A01;
        if (r1 != null) {
            BigDecimal AAD = r1.AAD(r4.A0K, str);
            if (AAD == null) {
                AAD = new BigDecimal(0);
            }
            r4.A0C.A0B(new AnonymousClass6F2(r4.A01, C117295Zj.A0D(r4.A01, AAD)));
        }
    }

    @Override // X.AbstractC1310961f
    public void AVp(C30821Yy r12) {
        UserJid userJid;
        this.A0C.AKg(C12960it.A0V(), C12980iv.A0k(), "new_payment", null);
        C118145bL r5 = this.A0H;
        AbstractC14640lm r3 = ((AbstractActivityC121685jC) this).A0E;
        long j = ((AbstractActivityC121685jC) this).A02;
        PaymentView paymentView = this.A0J;
        AnonymousClass1KS stickerIfSelected = paymentView.getStickerIfSelected();
        Integer stickerSendOrigin = paymentView.getStickerSendOrigin();
        if (C118145bL.A02(r5)) {
            userJid = (UserJid) this.A0H.A0t.A01();
        } else {
            userJid = ((AbstractActivityC121685jC) this).A0G;
        }
        r5.A00.A00(new AbstractC14590lg(r12, r3, userJid, r5, stickerIfSelected, stickerSendOrigin, this.A0J.getMentionedJids(), j) { // from class: X.6Eo
            public final /* synthetic */ long A00;
            public final /* synthetic */ C30821Yy A01;
            public final /* synthetic */ AbstractC14640lm A02;
            public final /* synthetic */ UserJid A03;
            public final /* synthetic */ C118145bL A04;
            public final /* synthetic */ AnonymousClass1KS A05;
            public final /* synthetic */ Integer A06;
            public final /* synthetic */ List A07;

            {
                this.A04 = r4;
                this.A01 = r1;
                this.A02 = r2;
                this.A00 = r8;
                this.A05 = r5;
                this.A06 = r6;
                this.A03 = r3;
                this.A07 = r7;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                Object obj2;
                C118145bL r10 = this.A04;
                C30821Yy r1 = this.A01;
                AbstractC14640lm r0 = this.A02;
                long j2 = this.A00;
                AnonymousClass1KS r11 = this.A05;
                Integer num = this.A06;
                UserJid userJid2 = this.A03;
                List list = this.A07;
                List list2 = (List) obj;
                AnonymousClass61F r52 = r10.A0b;
                if (!r10.A0E(AnonymousClass61F.A00(list2))) {
                    C1315463e r13 = (C1315463e) r10.A0r.A01();
                    boolean A0G = r52.A0G();
                    if (r13 == null || A0G) {
                        AnonymousClass016 r53 = r10.A0F;
                        if (r53.A01() != null) {
                            r1 = (C30821Yy) r53.A01();
                        }
                        Object A01 = r10.A0E.A01();
                        AnonymousClass009.A05(A01);
                        AnonymousClass6F2 r6 = new AnonymousClass6F2(((AnonymousClass63Y) A01).A02, r1);
                        AbstractC28901Pl A012 = AnonymousClass61F.A01(list2);
                        Object A013 = r10.A0p.A01();
                        AnonymousClass009.A05(A013);
                        C1315863i r54 = (C1315863i) A013;
                        C27691It r4 = r10.A0s;
                        if (r4.A01() != null) {
                            obj2 = r4.A01();
                        } else {
                            obj2 = r13.A01;
                        }
                        AnonymousClass009.A05(obj2);
                        C1316263m r42 = (C1316263m) obj2;
                        if (r42.A02.compareTo(r6) >= 0 || A012 != null) {
                            if (r1.A02()) {
                                C128095vY A00 = r10.A0Y.A00();
                                AnonymousClass610 A03 = AnonymousClass610.A03("REVIEW_TRANSACTION_CLICK", "SEND_MONEY", "ENTER_AMOUNT");
                                A03.A05(A00.A04, A00.A03.A01, A00.A05, null);
                                C128365vz r14 = A03.A00;
                                C128365vz.A01(r14, r10.A0A);
                                r10.A0a.A06(r14);
                            }
                            C129325xX r15 = r10.A0Y;
                            r15.A09 = r10.A06(A012, r6, r54, r42);
                            r15.A0A = r10.A0A;
                            r10.A0x.A0B(new C129525xr(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00f5: INVOKE  
                                  (wrap: X.1It : 0x00d7: IGET  (r1v3 X.1It A[REMOVE]) = (r10v0 'r10' X.5bL) X.5bL.A0x X.1It)
                                  (wrap: X.5xr : 0x00f2: CONSTRUCTOR  (r0v17 X.5xr A[REMOVE]) = 
                                  (wrap: X.6DH : 0x00ed: CONSTRUCTOR  (r12v1 X.6DH A[REMOVE]) = 
                                  (r0v0 'r0' X.0lm)
                                  (r8v0 'userJid2' com.whatsapp.jid.UserJid)
                                  (r6v0 'r6' X.6F2)
                                  (r5v4 'r54' X.63i)
                                  (r4v5 'r42' X.63m)
                                  (wrap: X.5vY : 0x00d3: INVOKE  (r18v0 X.5vY A[REMOVE]) = (r1v2 'r15' X.5xX) type: VIRTUAL call: X.5xX.A00():X.5vY)
                                  (r10v0 'r10' X.5bL)
                                  (r11v0 'r11' X.1KS)
                                  (r9v0 'num' java.lang.Integer)
                                  (r7v0 'list' java.util.List)
                                  (r2v0 'j2' long)
                                 call: X.6DH.<init>(X.0lm, com.whatsapp.jid.UserJid, X.6F2, X.63i, X.63m, X.5vY, X.5bL, X.1KS, java.lang.Integer, java.util.List, long):void type: CONSTRUCTOR)
                                 call: X.5xr.<init>(X.6MC):void type: CONSTRUCTOR)
                                 type: VIRTUAL call: X.017.A0B(java.lang.Object):void in method: X.6Eo.accept(java.lang.Object):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00f2: CONSTRUCTOR  (r0v17 X.5xr A[REMOVE]) = 
                                  (wrap: X.6DH : 0x00ed: CONSTRUCTOR  (r12v1 X.6DH A[REMOVE]) = 
                                  (r0v0 'r0' X.0lm)
                                  (r8v0 'userJid2' com.whatsapp.jid.UserJid)
                                  (r6v0 'r6' X.6F2)
                                  (r5v4 'r54' X.63i)
                                  (r4v5 'r42' X.63m)
                                  (wrap: X.5vY : 0x00d3: INVOKE  (r18v0 X.5vY A[REMOVE]) = (r1v2 'r15' X.5xX) type: VIRTUAL call: X.5xX.A00():X.5vY)
                                  (r10v0 'r10' X.5bL)
                                  (r11v0 'r11' X.1KS)
                                  (r9v0 'num' java.lang.Integer)
                                  (r7v0 'list' java.util.List)
                                  (r2v0 'j2' long)
                                 call: X.6DH.<init>(X.0lm, com.whatsapp.jid.UserJid, X.6F2, X.63i, X.63m, X.5vY, X.5bL, X.1KS, java.lang.Integer, java.util.List, long):void type: CONSTRUCTOR)
                                 call: X.5xr.<init>(X.6MC):void type: CONSTRUCTOR in method: X.6Eo.accept(java.lang.Object):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 39 more
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00ed: CONSTRUCTOR  (r12v1 X.6DH A[REMOVE]) = 
                                  (r0v0 'r0' X.0lm)
                                  (r8v0 'userJid2' com.whatsapp.jid.UserJid)
                                  (r6v0 'r6' X.6F2)
                                  (r5v4 'r54' X.63i)
                                  (r4v5 'r42' X.63m)
                                  (wrap: X.5vY : 0x00d3: INVOKE  (r18v0 X.5vY A[REMOVE]) = (r1v2 'r15' X.5xX) type: VIRTUAL call: X.5xX.A00():X.5vY)
                                  (r10v0 'r10' X.5bL)
                                  (r11v0 'r11' X.1KS)
                                  (r9v0 'num' java.lang.Integer)
                                  (r7v0 'list' java.util.List)
                                  (r2v0 'j2' long)
                                 call: X.6DH.<init>(X.0lm, com.whatsapp.jid.UserJid, X.6F2, X.63i, X.63m, X.5vY, X.5bL, X.1KS, java.lang.Integer, java.util.List, long):void type: CONSTRUCTOR in method: X.6Eo.accept(java.lang.Object):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 45 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6DH, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 51 more
                                */
                            /*
                            // Method dump skipped, instructions count: 249
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.C134426Eo.accept(java.lang.Object):void");
                        }
                    });
                }

                @Override // X.AnonymousClass6M9
                public /* bridge */ /* synthetic */ Object AZb() {
                    if (this.A0F == null) {
                        C134086Dg r1 = new C134086Dg();
                        this.A0F = r1;
                        r1.A00 = C117305Zk.A0A(this, 87);
                    }
                    AbstractC14640lm r0 = ((AbstractActivityC121685jC) this).A0E;
                    String str = this.A0h;
                    AnonymousClass1KS r02 = this.A0c;
                    Integer num = this.A0e;
                    String str2 = this.A0n;
                    C127155u2 r3 = new C127155u2(0, 0);
                    C126095sK r2 = new C126095sK(false);
                    C127135u0 r12 = new C127135u0(NumberEntryKeyboard.A00(this.A02), this.A0q);
                    String str3 = this.A0l;
                    String str4 = this.A0i;
                    String str5 = this.A0k;
                    C127505ub r7 = new C127505ub(this.A03, null, this.A0H.A04());
                    Pair pair = new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolAfterAmount), new int[]{16, 0, 4, 0});
                    Pair pair2 = new Pair(Integer.valueOf((int) R.style.SendPaymentCurrencySymbolBeforeAmount), new int[]{4, 0, 0, 0});
                    C134086Dg r03 = this.A0F;
                    C125905s0 r11 = new C125905s0(this);
                    AbstractC30791Yv r10 = this.A03;
                    AnonymousClass018 r9 = this.A02;
                    C30821Yy AEA = r10.AEA();
                    C128295vs r21 = new C128295vs(pair, pair2, r7, new AnonymousClass6D0(this, r9, r10, AEA, r10.AEV(), AEA, r11), r03, str3, str4, str5, R.style.NoviSendPaymentAmountInput, true, true, true);
                    C127145u1 r92 = new C127145u1(this, ((ActivityC13810kN) this).A0C.A07(811));
                    C22460z7 r8 = this.A09;
                    return new C128305vt(r0, null, this, this, r21, new C127785v3(((AbstractActivityC121685jC) this).A0C, this.A08, r8, false), r12, r2, r92, r3, r02, num, str, str2, false);
                }

                @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
                public void onActivityResult(int i, int i2, Intent intent) {
                    super.onActivityResult(i, i2, intent);
                    if (i == 7387 && i2 == -1) {
                        C118145bL r0 = this.A0H;
                        r0.A0h.A00((ActivityC13790kL) AnonymousClass12P.A00(r0.A12));
                    }
                }

                @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
                public void onBackPressed() {
                    A2o(new Runnable() { // from class: X.6Go
                        @Override // java.lang.Runnable
                        public final void run() {
                            NoviSharedPaymentActivity.A02(NoviSharedPaymentActivity.this);
                        }
                    });
                }

                @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
                public void onCreate(Bundle bundle) {
                    String str;
                    super.onCreate(bundle);
                    this.A07 = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A05);
                    setContentView(R.layout.send_payment_screen);
                    if (this.A0m == null) {
                        AbstractC14640lm r1 = ((AbstractActivityC121685jC) this).A0E;
                        if (!C15380n4.A0J(r1) || ((AbstractActivityC121685jC) this).A0G != null) {
                            ((AbstractActivityC121685jC) this).A0G = UserJid.of(r1);
                        } else {
                            A2i(null);
                            return;
                        }
                    }
                    A2h(bundle);
                    AnonymousClass60Y r4 = this.A0B;
                    r4.A04 = "ATTACHMENT_TRAY";
                    AnonymousClass60Y.A01(r4, "FLOW_SESSION_START", "SEND_MONEY", "ENTER_AMOUNT", "SCREEN");
                    Intent intent = getIntent();
                    if (intent != null) {
                        str = intent.getStringExtra("extra_referral_screen");
                        if (TextUtils.isEmpty(str)) {
                            str = intent.getStringExtra("referral_screen");
                        }
                    } else {
                        str = null;
                    }
                    this.A0C.AKg(C12980iv.A0i(), null, "new_payment", str);
                }

                @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
                public void onDestroy() {
                    super.onDestroy();
                    AnonymousClass60Y.A01(this.A0B, "FLOW_SESSION_END", "SEND_MONEY", "ENTER_AMOUNT", "SCREEN");
                }

                @Override // X.ActivityC13810kN, android.app.Activity
                public boolean onOptionsItemSelected(MenuItem menuItem) {
                    if (menuItem.getItemId() != 16908332) {
                        return super.onOptionsItemSelected(menuItem);
                    }
                    A2o(new Runnable() { // from class: X.6Gn
                        @Override // java.lang.Runnable
                        public final void run() {
                            NoviSharedPaymentActivity noviSharedPaymentActivity = NoviSharedPaymentActivity.this;
                            noviSharedPaymentActivity.A0C.AKg(C12960it.A0V(), C12970iu.A0h(), "new_payment", null);
                            noviSharedPaymentActivity.finish();
                        }
                    });
                    return true;
                }

                @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
                public void onStart() {
                    super.onStart();
                    AnonymousClass60Y.A01(this.A0B, "NAVIGATION_START", "SEND_MONEY", "ENTER_AMOUNT", "SCREEN");
                }

                @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
                public void onStop() {
                    super.onStop();
                    AnonymousClass60Y.A01(this.A0B, "NAVIGATION_END", "SEND_MONEY", "ENTER_AMOUNT", "SCREEN");
                }
            }
