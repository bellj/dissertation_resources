package com.whatsapp.payments.ui;

import X.AbstractActivityC119275dS;
import X.AbstractActivityC121755jw;
import X.AbstractC118055bC;
import X.AbstractView$OnClickListenerC121765jx;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass102;
import X.AnonymousClass2FL;
import X.AnonymousClass605;
import X.AnonymousClass60T;
import X.AnonymousClass60Z;
import X.AnonymousClass61E;
import X.AnonymousClass69D;
import X.C117295Zj;
import X.C117305Zk;
import X.C121185hO;
import X.C125035qZ;
import X.C12990iw;
import X.C129945yY;
import X.C130015yf;
import X.C130065yk;
import X.C1309660r;
import X.C1329568x;
import X.C17220qS;
import X.C18660so;
import X.C22710zW;
import X.C36021jC;
import android.os.Build;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* loaded from: classes4.dex */
public class BrazilPaymentCardDetailsActivity extends AbstractActivityC121755jw {
    public AnonymousClass102 A00;
    public C17220qS A01;
    public C1329568x A02;
    public C1309660r A03;
    public AnonymousClass69D A04;
    public AnonymousClass60Z A05;
    public C18660so A06;
    public C22710zW A07;
    public AnonymousClass60T A08;
    public AnonymousClass61E A09;
    public AnonymousClass605 A0A;
    public C130015yf A0B;
    public C129945yY A0C;
    public C130065yk A0D;
    public AbstractC118055bC A0E;
    public boolean A0F;

    public BrazilPaymentCardDetailsActivity() {
        this(0);
    }

    public BrazilPaymentCardDetailsActivity(int i) {
        this.A0F = false;
        C117295Zj.A0p(this, 14);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119275dS.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            AbstractActivityC119275dS.A03(A1M, this);
            this.A01 = C12990iw.A0c(A1M);
            this.A0A = (AnonymousClass605) A1M.AEj.get();
            this.A03 = (C1309660r) A1M.A1p.get();
            this.A02 = (C1329568x) A1M.A1o.get();
            this.A0C = (C129945yY) A1M.A1q.get();
            this.A07 = C117305Zk.A0O(A1M);
            this.A0B = (C130015yf) A1M.AEk.get();
            this.A00 = C117305Zk.A0G(A1M);
            this.A05 = (AnonymousClass60Z) A1M.A24.get();
            this.A08 = (AnonymousClass60T) A1M.AFI.get();
            this.A09 = (AnonymousClass61E) A1M.AEY.get();
            this.A06 = (C18660so) A1M.AEf.get();
            this.A0D = (C130065yk) A1M.A1z.get();
            this.A04 = A09.A08();
        }
    }

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2g() {
        C36021jC.A01(this, 201);
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f8  */
    @Override // X.AbstractActivityC121755jw, X.AbstractView$OnClickListenerC121765jx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2i(X.AbstractC28901Pl r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 402
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity.A2i(X.1Pl, boolean):void");
    }

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2j(boolean z) {
        FingerprintBottomSheet fingerprintBottomSheet;
        String A0U = C117295Zj.A0U(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05);
        PinBottomSheetDialogFragment A00 = C125035qZ.A00();
        if (Build.VERSION.SDK_INT >= 23) {
            fingerprintBottomSheet = C117305Zk.A0D();
        } else {
            fingerprintBottomSheet = null;
        }
        C121185hO r4 = new C121185hO(((ActivityC13790kL) this).A01, ((ActivityC13790kL) this).A05, ((AbstractActivityC121755jw) this).A06, this.A09, A0U, ((AbstractView$OnClickListenerC121765jx) this).A09.A0A);
        AbstractC118055bC r1 = this.A0E;
        if (r1 != null) {
            r1.A05(this, fingerprintBottomSheet, r4, A00, A0U, "REMOVEMETHOD", "FB");
        }
    }
}
