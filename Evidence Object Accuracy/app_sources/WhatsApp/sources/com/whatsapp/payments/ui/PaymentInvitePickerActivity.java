package com.whatsapp.payments.ui;

import X.AbstractActivityC36611kC;
import X.AbstractC38141na;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass10S;
import X.AnonymousClass116;
import X.AnonymousClass118;
import X.AnonymousClass12F;
import X.AnonymousClass1AR;
import X.AnonymousClass1ZO;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass3FP;
import X.AnonymousClass3RL;
import X.C117295Zj;
import X.C117305Zk;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C16170oZ;
import X.C17070qD;
import X.C21860y6;
import X.C22330yu;
import X.C22710zW;
import X.C238013b;
import X.C244215l;
import X.C64043Ea;
import X.C74503iB;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.Jid;
import com.whatsapp.payments.ui.PaymentInvitePickerActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public class PaymentInvitePickerActivity extends AbstractActivityC36611kC {
    public C21860y6 A00;
    public C22710zW A01;
    public C17070qD A02;
    public C74503iB A03;
    public boolean A04;

    @Override // X.AbstractActivityC36611kC
    public int A2j() {
        return Integer.MAX_VALUE;
    }

    @Override // X.AbstractActivityC36611kC
    public int A2k() {
        return 1;
    }

    @Override // X.AbstractActivityC36611kC
    public boolean A37() {
        return true;
    }

    public PaymentInvitePickerActivity() {
        this(0);
    }

    public PaymentInvitePickerActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 103);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC36611kC) this).A0B = (AnonymousClass1AR) A1M.ALM.get();
            ((AbstractActivityC36611kC) this).A0C = (C16170oZ) A1M.AM4.get();
            ((AbstractActivityC36611kC) this).A0N = C12970iu.A0W(A1M);
            ((AbstractActivityC36611kC) this).A0J = C12960it.A0O(A1M);
            ((AbstractActivityC36611kC) this).A0L = C12960it.A0P(A1M);
            ((AbstractActivityC36611kC) this).A0F = (C238013b) A1M.A1Z.get();
            ((AbstractActivityC36611kC) this).A0K = (AnonymousClass10S) A1M.A46.get();
            this.A0U = (AnonymousClass12F) A1M.AJM.get();
            ((AbstractActivityC36611kC) this).A0I = (AnonymousClass116) A1M.A3z.get();
            this.A0S = C12960it.A0R(A1M);
            ((AbstractActivityC36611kC) this).A0G = (C22330yu) A1M.A3I.get();
            this.A0T = (C244215l) A1M.A8y.get();
            this.A0R = (AnonymousClass118) A1M.A42.get();
            this.A02 = C117305Zk.A0P(A1M);
            this.A00 = (C21860y6) A1M.AE6.get();
            this.A01 = C117305Zk.A0O(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public int A2g() {
        return R.string.payments_invite_others_title;
    }

    @Override // X.AbstractActivityC36611kC
    public int A2h() {
        return R.string.payments_multi_invite_no_contacts_to_invite;
    }

    @Override // X.AbstractActivityC36611kC
    public int A2i() {
        return R.plurals.payments_invite_limit_warning;
    }

    @Override // X.AbstractActivityC36611kC
    public int A2l() {
        return R.string.next;
    }

    @Override // X.AbstractActivityC36611kC
    public Drawable A2m() {
        return AnonymousClass2GF.A00(this, this.A0S, R.drawable.ic_fab_next);
    }

    @Override // X.AbstractActivityC36611kC
    public void A2r() {
        String str;
        ArrayList A0x = C12980iv.A0x(A2p());
        Intent intent = getIntent();
        if (intent != null) {
            str = intent.getStringExtra("referral_screen");
        } else {
            str = null;
        }
        AnonymousClass3FP r4 = new AnonymousClass3FP(this, this, ((ActivityC13810kN) this).A05, this.A02, this.A03, null, new Runnable(A0x) { // from class: X.6IW
            public final /* synthetic */ ArrayList A01;

            {
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                int i;
                Intent putExtra;
                PaymentInvitePickerActivity paymentInvitePickerActivity = PaymentInvitePickerActivity.this;
                ArrayList arrayList = this.A01;
                if (arrayList.size() == 1) {
                    i = -1;
                    putExtra = C12970iu.A0A().putExtra("extra_invitee_jid", ((Jid) arrayList.get(0)).getRawString());
                } else {
                    i = 501;
                    putExtra = C12970iu.A0A().putExtra("extra_inviter_count", arrayList.size());
                }
                paymentInvitePickerActivity.setResult(i, putExtra);
                paymentInvitePickerActivity.finish();
            }
        }, false);
        AnonymousClass009.A0E(r4.A02());
        AbstractC38141na AFL = r4.A03.A02().AFL();
        if (AFL != null) {
            C74503iB r1 = r4.A04;
            r1.A04(0);
            DialogFragment AFK = AFL.AFK(str, A0x, false, false);
            r4.A01.Adm(AFK);
            r1.A00.A05(AFK, new AnonymousClass3RL(AFK, r4));
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A30(C64043Ea r3, C15370n3 r4) {
        super.A30(r3, r4);
        TextEmojiLabel textEmojiLabel = r3.A02;
        textEmojiLabel.setVisibility(0);
        textEmojiLabel.setText(R.string.payments_multi_invite_picker_subtitle);
    }

    @Override // X.AbstractActivityC36611kC
    public void A35(ArrayList arrayList) {
        ArrayList A0l = C12960it.A0l();
        super.A35(A0l);
        AbstractC38141na AFL = this.A02.A02().AFL();
        if (AFL != null) {
            C17070qD r0 = this.A02;
            r0.A03();
            List<AnonymousClass1ZO> A0E = r0.A09.A0E(new int[]{2}, AFL.AFW());
            HashMap A11 = C12970iu.A11();
            for (AnonymousClass1ZO r1 : A0E) {
                A11.put(r1.A05, r1);
            }
            Iterator it = A0l.iterator();
            while (it.hasNext()) {
                C15370n3 r3 = (C15370n3) it.next();
                Object obj = A11.get(r3.A0A());
                if (!((AbstractActivityC36611kC) this).A0F.A0I(C15370n3.A05(r3)) && obj != null) {
                    arrayList.add(r3);
                }
            }
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent.hasExtra("extra_multi_invite_picker_title")) {
            setTitle(intent.getIntExtra("extra_multi_invite_picker_title", R.string.payments_invite_others_title));
        }
        this.A03 = C117305Zk.A0Z(this);
    }
}
