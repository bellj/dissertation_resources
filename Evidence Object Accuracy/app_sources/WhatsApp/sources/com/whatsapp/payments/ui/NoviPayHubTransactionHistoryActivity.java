package com.whatsapp.payments.ui;

import X.AbstractActivityC119295dU;
import X.AbstractC001200n;
import X.AbstractC35651iS;
import X.ActivityC13790kL;
import X.AnonymousClass016;
import X.AnonymousClass102;
import X.AnonymousClass14X;
import X.AnonymousClass1IR;
import X.AnonymousClass20A;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass613;
import X.AnonymousClass61M;
import X.AnonymousClass69R;
import X.AnonymousClass6JQ;
import X.AnonymousClass6KE;
import X.C117295Zj;
import X.C117305Zk;
import X.C118515bw;
import X.C12980iv;
import X.C130095yn;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C17070qD;
import X.C243515e;
import X.C27631Ih;
import X.C30931Zj;
import android.os.Bundle;
import com.whatsapp.payments.ui.widget.PayToolbar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes4.dex */
public class NoviPayHubTransactionHistoryActivity extends AbstractActivityC119295dU implements AbstractC001200n {
    public C15550nR A00;
    public C15610nY A01;
    public AnonymousClass102 A02;
    public C243515e A03;
    public AnonymousClass61M A04;
    public C17070qD A05;
    public AnonymousClass60Y A06;
    public C130095yn A07;
    public C118515bw A08;
    public PayToolbar A09;
    public AnonymousClass14X A0A;
    public List A0B;
    public AtomicBoolean A0C = new AtomicBoolean(false);
    public final AbstractC35651iS A0D = new AnonymousClass69R(this);
    public final C30931Zj A0E = C117305Zk.A0V("NoviPayHubTransactionHistoryActivity", "payment");

    public void A2e() {
        String str;
        AnonymousClass20A r0;
        if (!(this instanceof NoviClaimableTransactionListActivity)) {
            if (!this.A0B.isEmpty()) {
                List list = this.A0B;
                String str2 = ((AnonymousClass1IR) list.get(list.size() - 1)).A0K;
                if (!(str2 == null || (r0 = (AnonymousClass20A) this.A07.A0B.get(str2)) == null)) {
                    str = r0.A00;
                    if (!r0.A01) {
                        return;
                    }
                    this.A0C.set(true);
                    C130095yn r3 = this.A07;
                    AnonymousClass016 A0T = C12980iv.A0T();
                    r3.A0A.Ab2(new AnonymousClass6JQ(A0T, r3, str));
                    C117295Zj.A0r(this, A0T, 93);
                }
            }
            str = null;
            this.A0C.set(true);
            C130095yn r3 = this.A07;
            AnonymousClass016 A0T = C12980iv.A0T();
            r3.A0A.Ab2(new AnonymousClass6JQ(A0T, r3, str));
            C117295Zj.A0r(this, A0T, 93);
        }
    }

    public void A2f() {
        if (!(this instanceof NoviClaimableTransactionListActivity)) {
            AnonymousClass102 r3 = this.A02;
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            C27631Ih r2 = r0.A05;
            ArrayList A0x = C12980iv.A0x(this.A07.A0C.values());
            Collections.sort(A0x, new AnonymousClass6KE());
            this.A0B = AnonymousClass613.A02(r3, r2, A0x);
        }
        this.A08.A02();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        this.A06.A05(new AnonymousClass610("BACK_CLICK", "PAYMENT_HISTORY", "PAYMENT_HISTORY", "ARROW").A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        if (r6.getBoolean("is_loading") == false) goto L_0x0028;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            super.onCreate(r6)
            r0 = 2131559463(0x7f0d0427, float:1.874427E38)
            r5.setContentView(r0)
            com.whatsapp.payments.ui.widget.PayToolbar r2 = X.C117305Zk.A0a(r5)
            r5.A09 = r2
            boolean r0 = r5 instanceof com.whatsapp.payments.ui.NoviClaimableTransactionListActivity
            X.018 r1 = r5.A01
            if (r0 != 0) goto L_0x006c
            r0 = 2131889786(0x7f120e7a, float:1.9414245E38)
        L_0x0018:
            X.C130305zC.A00(r5, r1, r2, r0)
            java.util.concurrent.atomic.AtomicBoolean r2 = r5.A0C
            if (r6 == 0) goto L_0x0028
            java.lang.String r0 = "is_loading"
            boolean r1 = r6.getBoolean(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            r2.set(r0)
            r0 = 2131364907(0x7f0a0c2b, float:1.8349664E38)
            android.view.View r2 = r5.findViewById(r0)
            androidx.recyclerview.widget.RecyclerView r2 = (androidx.recyclerview.widget.RecyclerView) r2
            androidx.recyclerview.widget.LinearLayoutManager r1 = new androidx.recyclerview.widget.LinearLayoutManager
            r1.<init>()
            r2.setLayoutManager(r1)
            X.5bw r0 = new X.5bw
            r0.<init>(r5)
            r5.A08 = r0
            r2.setAdapter(r0)
            X.5cD r0 = new X.5cD
            r0.<init>(r1, r5)
            r2.A0n(r0)
            X.15e r1 = r5.A03
            X.1iS r0 = r5.A0D
            r1.A03(r0)
            r5.A2f()
            X.60Y r4 = r5.A06
            java.lang.String r3 = "NAVIGATION_START"
            java.lang.String r2 = "PAYMENT_HISTORY"
            java.lang.String r1 = "SCREEN"
            X.610 r0 = new X.610
            r0.<init>(r3, r2, r2, r1)
            X.5vz r0 = r0.A00
            r4.A05(r0)
            return
        L_0x006c:
            r0 = 2131889738(0x7f120e4a, float:1.9414148E38)
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.A05(new AnonymousClass610("NAVIGATION_END", "PAYMENT_HISTORY", "PAYMENT_HISTORY", "SCREEN").A00);
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("is_loading", this.A0C.get());
    }
}
