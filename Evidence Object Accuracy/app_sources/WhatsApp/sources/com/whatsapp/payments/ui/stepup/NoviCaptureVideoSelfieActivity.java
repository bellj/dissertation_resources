package com.whatsapp.payments.ui.stepup;

import X.AbstractC27291Gt;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117975b4;
import X.C118335be;
import X.C126015sC;
import X.C128375w0;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C130105yo;
import X.C1310561a;
import X.C15890o4;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.payments.ui.NoviSelfieCameraOverlay;
import com.whatsapp.payments.ui.NoviSelfieCameraView;
import com.whatsapp.payments.ui.stepup.NoviCaptureVideoSelfieActivity;
import com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView;
import java.io.File;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class NoviCaptureVideoSelfieActivity extends ActivityC13790kL {
    public int A00;
    public Handler A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public Chip A06;
    public WaButton A07;
    public WaTextView A08;
    public C15890o4 A09;
    public C130105yo A0A;
    public NoviSelfieCameraOverlay A0B;
    public NoviSelfieCameraView A0C;
    public C117975b4 A0D;
    public C128375w0 A0E;
    public NoviSelfieFaceAnimationView A0F;
    public boolean A0G;
    public boolean A0H;

    public NoviCaptureVideoSelfieActivity() {
        this(0);
        this.A00 = 3;
        this.A0G = true;
    }

    public NoviCaptureVideoSelfieActivity(int i) {
        this.A0H = false;
        C117295Zj.A0p(this, 115);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A09 = C12970iu.A0Y(A1M);
            this.A0A = (C130105yo) A1M.ADZ.get();
            this.A0E = C117315Zl.A0E(A1M);
        }
    }

    public final void A2e() {
        NoviSelfieCameraView noviSelfieCameraView = this.A0C;
        noviSelfieCameraView.AeT();
        Handler handler = noviSelfieCameraView.A01;
        if (handler != null) {
            Runnable runnable = noviSelfieCameraView.A07;
            if (runnable != null) {
                handler.removeCallbacks(runnable);
                noviSelfieCameraView.A07 = null;
            }
            noviSelfieCameraView.A01 = null;
        }
        noviSelfieCameraView.A08 = null;
        noviSelfieCameraView.A06 = null;
        File file = noviSelfieCameraView.A05;
        if (file != null) {
            file.delete();
            noviSelfieCameraView.A05 = null;
        }
        File file2 = noviSelfieCameraView.A04;
        if (file2 != null) {
            file2.delete();
            noviSelfieCameraView.A04 = null;
        }
        this.A0F.A00();
        setResult(0);
        finish();
    }

    public final void A2f() {
        Chip chip = this.A06;
        boolean z = this.A0G;
        int i = R.string.novi_selfie_audio_instructions_off;
        if (z) {
            i = R.string.novi_selfie_audio_instructions_on;
        }
        chip.setText(i);
        Resources resources = getResources();
        boolean z2 = this.A0G;
        int i2 = R.drawable.ic_volume_off;
        if (z2) {
            i2 = R.drawable.ic_volume_on;
        }
        Drawable drawable = resources.getDrawable(i2);
        AnonymousClass009.A05(drawable);
        this.A06.setChipIcon(AnonymousClass2GE.A03(this, drawable, R.color.novi_selfie_audio_chip_icon_tint));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 30) {
            if (i2 == -1) {
                C12960it.A0t(C130105yo.A01(this.A0A), "wavi_seen_camera_permission_education", true);
                NoviSelfieCameraView noviSelfieCameraView = this.A0C;
                noviSelfieCameraView.A05();
                noviSelfieCameraView.Aap();
                return;
            }
            setResult(0);
        } else if (i == 0) {
            setResult(i2);
        } else {
            return;
        }
        finish();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_capture_video_selfie);
        C1310561a.A03(this, this.A09, this.A0A);
        this.A0C = (NoviSelfieCameraView) findViewById(R.id.selfie_camera);
        this.A04 = findViewById(R.id.selfie_oval);
        this.A0B = (NoviSelfieCameraOverlay) findViewById(R.id.selfie_overlay);
        this.A0F = (NoviSelfieFaceAnimationView) findViewById(R.id.selfie_face_animation);
        this.A02 = findViewById(R.id.selfie_capture_instructions);
        this.A08 = (WaTextView) findViewById(R.id.selfie_countdown_timer);
        this.A03 = findViewById(R.id.close);
        Chip chip = (Chip) findViewById(R.id.audio_instructions_toggle_chip);
        this.A06 = chip;
        C117295Zj.A0n(chip, this, 137);
        NoviSelfieFaceAnimationView noviSelfieFaceAnimationView = this.A0F;
        noviSelfieFaceAnimationView.A03 = new TextToSpeech(noviSelfieFaceAnimationView.A01, new TextToSpeech.OnInitListener(new Runnable() { // from class: X.6H2
            @Override // java.lang.Runnable
            public final void run() {
                NoviCaptureVideoSelfieActivity noviCaptureVideoSelfieActivity = NoviCaptureVideoSelfieActivity.this;
                if (noviCaptureVideoSelfieActivity.A0F.A0E) {
                    noviCaptureVideoSelfieActivity.A0G = true;
                    noviCaptureVideoSelfieActivity.A06.setVisibility(0);
                    noviCaptureVideoSelfieActivity.A2f();
                    return;
                }
                noviCaptureVideoSelfieActivity.A06.setVisibility(8);
                noviCaptureVideoSelfieActivity.A0G = false;
            }
        }) { // from class: X.63s
            public final /* synthetic */ Runnable A01;

            {
                this.A01 = r2;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
                if (r2 == -2) goto L_0x0019;
             */
            @Override // android.speech.tts.TextToSpeech.OnInitListener
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void onInit(int r6) {
                /*
                    r5 = this;
                    com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView r4 = com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView.this
                    java.lang.Runnable r3 = r5.A01
                    if (r6 != 0) goto L_0x0020
                    android.speech.tts.TextToSpeech r1 = r4.A03
                    X.018 r0 = r4.A0A
                    java.util.Locale r0 = X.C12970iu.A14(r0)
                    int r2 = r1.setLanguage(r0)
                    r0 = -1
                    if (r2 == r0) goto L_0x0019
                    r1 = -2
                    r0 = 1
                    if (r2 != r1) goto L_0x001a
                L_0x0019:
                    r0 = 0
                L_0x001a:
                    r4.A0E = r0
                L_0x001c:
                    r3.run()
                    return
                L_0x0020:
                    java.lang.String r0 = "PAY: NoviSelfieFaceAnimationView/Text to speech initialization failed"
                    com.whatsapp.util.Log.e(r0)
                    goto L_0x001c
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C1316863s.onInit(int):void");
            }
        });
        C117295Zj.A0n(this.A03, this, 136);
        this.A07 = (WaButton) findViewById(R.id.start_video_capture_button);
        this.A07.setCompoundDrawablesWithIntrinsicBounds(AnonymousClass2GE.A01(this, R.drawable.ic_voip_video_control, R.color.primary_surface), (Drawable) null, (Drawable) null, (Drawable) null);
        View findViewById = findViewById(R.id.start_video_capture);
        this.A05 = findViewById;
        C117295Zj.A0n(findViewById, this, 135);
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this.A04);
        A0H.setMargins(0, (int) (((float) this.A0B.A01) + getResources().getDimension(R.dimen.novi_selfie_oval_top_margin)), 0, 0);
        this.A04.setLayoutParams(A0H);
        ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(this.A0F);
        A0H2.setMargins(0, (int) (((float) this.A0B.A00) - getResources().getDimension(R.dimen.novi_selfie_face_animation_view_camera_padding)), 0, 0);
        this.A0F.setLayoutParams(A0H2);
        this.A0F.setVisibility(8);
        ViewGroup.MarginLayoutParams A0H3 = C12970iu.A0H(this.A02);
        A0H3.setMargins((int) getResources().getDimension(R.dimen.novi_selfie_capture_instructions_side_margin), (int) (((float) this.A0B.A00) + getResources().getDimension(R.dimen.novi_selfie_capture_instructions_top_margin)), (int) getResources().getDimension(R.dimen.novi_selfie_capture_instructions_side_margin), 0);
        this.A02.setLayoutParams(A0H3);
        C12970iu.A0M(this, R.id.novi_selfie_instructions_one).setText(AbstractC27291Gt.A07(C12970iu.A14(((ActivityC13830kP) this).A01), "1."));
        C12970iu.A0M(this, R.id.novi_selfie_instructions_two).setText(AbstractC27291Gt.A07(C12970iu.A14(((ActivityC13830kP) this).A01), "2."));
        C12970iu.A0M(this, R.id.novi_selfie_instructions_three).setText(AbstractC27291Gt.A07(C12970iu.A14(((ActivityC13830kP) this).A01), "3."));
        C128375w0 r1 = this.A0E;
        if (bundle == null) {
            bundle = C12990iw.A0H(this);
        }
        C117975b4 r2 = (C117975b4) C117315Zl.A06(new C118335be(bundle, r1), this).A00(C117975b4.class);
        this.A0D = r2;
        r2.A02.A05(this, C117305Zk.A0B(this, 129));
        this.A0D.A04(this, new C126015sC(0));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (getIntent() != null && C12990iw.A0H(this) != null) {
            Bundle A0H = C12990iw.A0H(this);
            AnonymousClass009.A05(A0H);
            String string = A0H.getString("video_selfie_challenge_id");
            AnonymousClass009.A05(string);
            bundle.putString("video_selfie_challenge_id", string);
            String string2 = A0H.getString("disable_face_rec");
            AnonymousClass009.A05(string2);
            bundle.putString("disable_face_rec", string2);
            ArrayList<String> stringArrayList = A0H.getStringArrayList("video_selfie_head_directions");
            AnonymousClass009.A05(stringArrayList);
            bundle.putStringArrayList("video_selfie_head_directions", stringArrayList);
            bundle.putParcelable("step_up", A0H.getParcelable("step_up"));
            bundle.putInt("step_up_origin_action", A0H.getInt("step_up_origin_action", 1));
        }
    }
}
