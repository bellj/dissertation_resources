package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC136256Lv;
import X.AbstractC14440lR;
import X.AbstractC30851Zb;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00X;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02M;
import X.AnonymousClass102;
import X.AnonymousClass12P;
import X.AnonymousClass17V;
import X.AnonymousClass1FK;
import X.AnonymousClass1ZY;
import X.AnonymousClass2FL;
import X.AnonymousClass2SP;
import X.AnonymousClass3CT;
import X.AnonymousClass46O;
import X.AnonymousClass60I;
import X.AnonymousClass60V;
import X.AnonymousClass60W;
import X.AnonymousClass69E;
import X.AnonymousClass6MR;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118535by;
import X.C119755f3;
import X.C120455gD;
import X.C120805gm;
import X.C122205l5;
import X.C125825rs;
import X.C125835rt;
import X.C126705tJ;
import X.C128025vR;
import X.C128645wR;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C129925yW;
import X.C130525zY;
import X.C1308460e;
import X.C1311161i;
import X.C1329668y;
import X.C14900mE;
import X.C16590pI;
import X.C17070qD;
import X.C17220qS;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C18790t3;
import X.C21860y6;
import X.C22480z9;
import X.C30861Zc;
import X.C30931Zj;
import X.C38721ob;
import X.C38771og;
import X.C42971wC;
import X.C452120p;
import X.C452220q;
import X.C64513Fv;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiBankAccountPickerActivity extends AbstractActivityC121665jA implements AnonymousClass1FK, AnonymousClass6MR, AbstractC136256Lv {
    public int A00;
    public int A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public ImageView A08;
    public TextView A09;
    public TextView A0A;
    public RecyclerView A0B;
    public C18790t3 A0C;
    public AnonymousClass102 A0D;
    public C129925yW A0E;
    public C119755f3 A0F;
    public C22480z9 A0G;
    public C64513Fv A0H;
    public AnonymousClass60I A0I;
    public C120455gD A0J;
    public AnonymousClass17V A0K;
    public AnonymousClass69E A0L;
    public C122205l5 A0M;
    public C128645wR A0N;
    public AnonymousClass60W A0O;
    public C18590sh A0P;
    public C38721ob A0Q;
    public String A0R;
    public ArrayList A0S;
    public List A0T;
    public boolean A0U;
    public boolean A0V;
    public final AnonymousClass2SP A0W;
    public final C30931Zj A0X;

    @Override // X.AnonymousClass6MR
    public void AP4(C452120p r1) {
    }

    public IndiaUpiBankAccountPickerActivity() {
        this(0);
        this.A0X = C117315Zl.A0D("IndiaUpiBankAccountPickerActivity");
        this.A01 = -1;
        this.A0W = new AnonymousClass2SP();
    }

    public IndiaUpiBankAccountPickerActivity(int i) {
        this.A0U = false;
        C117295Zj.A0p(this, 34);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0U) {
            this.A0U = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A0C = (C18790t3) A1M.AJw.get();
            this.A0P = C117315Zl.A0F(A1M);
            this.A0K = (AnonymousClass17V) A1M.AEX.get();
            this.A0L = (AnonymousClass69E) A1M.A9W.get();
            this.A0D = C117305Zk.A0G(A1M);
            this.A0E = (C129925yW) A1M.AEW.get();
            this.A0G = (C22480z9) A1M.AEc.get();
            this.A0O = A09.A0F();
            this.A0M = (C122205l5) A1M.A9Z.get();
        }
    }

    public void A30() {
        ArrayList arrayList = this.A0S;
        if (arrayList == null || arrayList.size() == 0) {
            this.A08.setVisibility(0);
            this.A09.setVisibility(8);
            this.A04.setVisibility(4);
            this.A06.setVisibility(0);
            this.A0B.setVisibility(8);
            this.A02.setVisibility(8);
            this.A03.setVisibility(8);
            this.A07.setVisibility(0);
            this.A05.setVisibility(8);
            this.A08.setImageDrawable(getResources().getDrawable(R.drawable.ic_account_search));
            this.A0A.setText(R.string.account_search_title);
            this.A0I.A01(this.A0F);
        } else {
            this.A0W.A0H = Long.valueOf((long) arrayList.size());
            this.A0T = C12960it.A0l();
            this.A01 = -1;
            this.A0V = false;
            int i = 0;
            while (true) {
                ArrayList arrayList2 = this.A0S;
                if (i >= arrayList2.size()) {
                    break;
                }
                C119755f3 r7 = (C119755f3) arrayList2.get(i);
                this.A0T.add(new C128025vR((String) C117295Zj.A0R(r7.A03), C1311161i.A08((String) C117295Zj.A0R(((AbstractC30851Zb) r7).A02)), (String) C117295Zj.A0R(((AbstractC30851Zb) r7).A01), getString(r7.A0E()), r7.A0H));
                i++;
            }
            this.A04.setVisibility(0);
            this.A06.setVisibility(8);
            this.A0B.setVisibility(0);
            int i2 = 0;
            while (true) {
                if (i2 >= this.A0T.size()) {
                    break;
                }
                C128025vR r1 = (C128025vR) this.A0T.get(i2);
                if (this.A01 == -1 && !r1.A05) {
                    this.A01 = i2;
                    r1.A00 = true;
                    break;
                }
                i2++;
            }
            this.A08.setVisibility(0);
            this.A07.setVisibility(0);
            this.A02.setVisibility(0);
            this.A03.setVisibility(0);
            this.A05.setVisibility(0);
            this.A08.setImageDrawable(AnonymousClass00X.A04(null, getResources(), R.drawable.ic_account_found));
            int size = this.A0S.size();
            TextView textView = this.A0A;
            if (size == 1) {
                textView.setText(R.string.payments_add_bank_account_single_title);
                this.A09.setVisibility(8);
            } else {
                textView.setText(R.string.payments_add_bank_account_multiple_title);
                this.A09.setText(R.string.payments_add_bank_account_desc);
                this.A09.setVisibility(0);
            }
            if (this.A01 == -1) {
                this.A02.setEnabled(false);
                this.A03.setVisibility(4);
            } else {
                this.A03.setVisibility(0);
                this.A02.setEnabled(true);
                C117295Zj.A0n(this.A02, this, 27);
            }
            List list = this.A0T;
            if (list != null) {
                this.A0B.setAdapter(new C118535by(new C125835rt(this), this, list));
                this.A0M.A00.A09("bankAccountPickerShown");
            }
        }
        invalidateOptionsMenu();
    }

    public final void A31() {
        if (this.A01 < 0) {
            Log.e("selected account position is invalid");
            return;
        }
        this.A0M.A00.A09("bankAccountAddClicked");
        this.A02.setVisibility(8);
        this.A0V = true;
        AnonymousClass02M r1 = this.A0B.A0N;
        if (r1 != null) {
            r1.A02();
        }
        C120455gD r2 = this.A0J;
        C119755f3 r7 = (C119755f3) this.A0S.get(this.A01);
        boolean z = ((AbstractActivityC121665jA) this).A0N;
        C125825rs r3 = new C125825rs(this);
        Log.i("PAY: IndiaUpiPaymentSetup registerVpa called");
        C64513Fv r12 = ((C126705tJ) r2).A00;
        r12.A04("upi-register-vpa");
        C17220qS r6 = r2.A06;
        String A01 = r6.A01();
        String A012 = r2.A0B.A01();
        String str = (String) C117295Zj.A0R(r7.A06);
        String A07 = r2.A07.A07();
        String str2 = (String) C117295Zj.A0R(r7.A09);
        String str3 = r7.A0F;
        String str4 = "1";
        str4 = "0";
        if (!z) {
        }
        C130525zY r8 = new C130525zY(new AnonymousClass3CT(A01), A012, str, A07, str2, str3, str4, str4);
        r2.A00 = r7;
        r6.A09(new C120805gm(r2.A02, r2.A03, r2.A09, r12, r2, r3), r8.A00, A01, 204, 0);
        ((AbstractActivityC121665jA) this).A0D.AeG();
        AnonymousClass2SP r32 = this.A0W;
        r32.A0G = Long.valueOf((long) this.A01);
        r32.A08 = C12980iv.A0k();
        r32.A0Z = "nav_select_account";
        r32.A09 = 1;
        AbstractActivityC119235dO.A1b(r32, this);
    }

    public final void A32(C30861Zc r4) {
        String str;
        this.A0X.A06(C12960it.A0d(this.A0H.toString(), C12960it.A0k("showSuccessAndFinish: ")));
        A2r();
        ((AbstractActivityC121665jA) this).A04 = r4;
        StringBuilder A0k = C12960it.A0k("Is first payment method:");
        A0k.append(((AbstractActivityC121665jA) this).A0O);
        A0k.append(", entry point:");
        Log.i(C12960it.A0f(A0k, ((AbstractActivityC121665jA) this).A02));
        switch (((AbstractActivityC121665jA) this).A02) {
            case 0:
                Log.e("Entry point not provided while onboarding");
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 9:
            case 10:
                break;
            case 6:
            case 11:
                if (!((AbstractActivityC121665jA) this).A0O) {
                    if (r4 != null) {
                        C119755f3 r0 = (C119755f3) r4.A08;
                        if (r0 == null) {
                            str = "Invalid bank's country data";
                        } else if (!C12970iu.A1Y(r0.A05.A00)) {
                            Intent A02 = IndiaUpiPinPrimerFullSheetActivity.A02(this, ((AbstractActivityC121665jA) this).A04, false);
                            C117315Zl.A0M(A02, ((AbstractActivityC121665jA) this).A04);
                            AbstractActivityC119235dO.A0n(A02, this);
                            return;
                        }
                    } else {
                        str = "Invalid Bank Account added is null";
                    }
                    Log.e(str);
                    finish();
                    return;
                }
                break;
            case 7:
            default:
                return;
        }
        A2q();
        AbstractActivityC119235dO.A0n(C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class), this);
    }

    public final void A33(AnonymousClass60V r5, boolean z) {
        int i = r5.A00;
        this.A0X.A06(C12960it.A0W(i, "showSuccessAndFinish: resId "));
        A2r();
        if (i == 0) {
            i = R.string.payments_setup_error;
            String str = this.A0H.A04;
            if ("upi-register-vpa".equalsIgnoreCase(str)) {
                i = R.string.payments_error_vpa_handle;
            }
            if ("upi-get-accounts".equalsIgnoreCase(str)) {
                i = R.string.get_accounts_failure_reason;
            }
        }
        if (((AbstractActivityC121665jA) this).A0N || z) {
            A2q();
            Intent A0D = C12990iw.A0D(this, IndiaUpiOnboardingErrorEducationActivity.class);
            if (r5.A01 != null) {
                A0D.putExtra("error_text", r5.A01(this));
            }
            A0D.putExtra("error", i);
            A0D.putExtra("error_type", this.A00);
            int i2 = this.A00;
            if (i2 >= 1 && i2 <= 6) {
                C117315Zl.A0M(A0D, this.A0F);
            }
            if (!((AbstractActivityC121665jA) this).A0N) {
                A0D.putExtra("try_again", 1);
            }
            if (this.A00 == 1) {
                A0D.putExtra("extra_error_screen_name", "bank_account_not_found");
                A0D.putExtra("extra_referral_screen", "device_binding");
            }
            A0D.addFlags(335544320);
            A2v(A0D);
            A2G(A0D, true);
        } else {
            Ado(i);
        }
        AbstractActivityC119235dO.A1j(this.A0M, 3);
    }

    public final void A34(Integer num) {
        AnonymousClass2SP r1 = this.A0W;
        r1.A0Z = "nav_select_account";
        r1.A09 = C12960it.A0V();
        r1.A08 = num;
        AbstractActivityC119235dO.A1b(r1, this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        if (r9.size() <= 0) goto L_0x0040;
     */
    @Override // X.AnonymousClass6MR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AN8(X.C452120p r8, java.util.ArrayList r9) {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiBankAccountPickerActivity.AN8(X.20p, java.util.ArrayList):void");
    }

    @Override // X.AbstractC136256Lv
    public void AUo(C30861Zc r5, C452120p r6) {
        String str;
        C30931Zj r3 = this.A0X;
        r3.A04(C12960it.A0b("onRegisterVpa registered: ", r5));
        AnonymousClass2SP A01 = ((AbstractActivityC121665jA) this).A0D.A01(r6, 5);
        int i = this.A01;
        if (i >= 0) {
            str = ((C119755f3) this.A0S.get(i)).A0C;
        } else {
            str = "";
        }
        A01.A0O = str;
        A01.A0Z = "nav_select_account";
        AbstractActivityC119235dO.A1b(A01, this);
        r3.A04(C12960it.A0b("logRegisterVpa: ", A01));
        C122205l5 r1 = this.A0M;
        short s = 3;
        if (r6 == null) {
            s = 2;
        }
        AbstractActivityC119235dO.A1j(r1, s);
        AbstractActivityC119235dO.A1d(this);
        boolean z = false;
        if (r5 != null) {
            AnonymousClass1ZY r0 = r5.A08;
            if (r0 != null && C12970iu.A1Y(((C119755f3) r0).A05.A00)) {
                z = true;
            }
            this.A0G.A00(((AbstractActivityC121685jC) this).A0F, 3, z);
            A32(r5);
        } else if (r6 == null || r6.A00 != 11472) {
            A33(this.A0L.A04(this.A0H, 0), false);
        } else {
            ((AbstractActivityC121685jC) this).A0M.A08(this, 2);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        this.A0X.A06(C12960it.A0b("getPaymentMethods. paymentNetworkError: ", r4));
        A33(this.A0L.A04(this.A0H, r4.A00), false);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        this.A0X.A06(C12960it.A0b("getPaymentMethods. paymentNetworkError: ", r4));
        if (!AnonymousClass69E.A02(this, "upi-register-vpa", r4.A00, true)) {
            A33(this.A0L.A04(this.A0H, r4.A00), false);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        C30931Zj r2 = this.A0X;
        StringBuilder A0k = C12960it.A0k("getPaymentMethods. onResponseSuccess: ");
        A0k.append(r4.A02);
        C117295Zj.A1F(r2, A0k);
        List list = ((AnonymousClass46O) r4).A00;
        if (list == null || list.isEmpty()) {
            A33(this.A0L.A04(this.A0H, 0), false);
            return;
        }
        ((AbstractActivityC121685jC) this).A0I.A06(((AbstractActivityC121685jC) this).A0I.A01("add_bank"));
        A32(null);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        this.A0X.A06("onBackPressed");
        A34(C12960it.A0V());
        A2s();
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C117295Zj.A0d(this);
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        this.A0N = new C128645wR(((AbstractActivityC121685jC) this).A0I);
        AnonymousClass009.A05(C12990iw.A0H(this));
        this.A0S = C12990iw.A0H(this).getParcelableArrayList("extra_accounts_list");
        this.A0R = C12990iw.A0H(this).getString("extra_selected_account_bank_logo");
        this.A0F = (C119755f3) getIntent().getParcelableExtra("extra_selected_bank");
        C64513Fv r1 = ((AbstractActivityC121665jA) this).A0A.A04;
        this.A0H = r1;
        r1.A02("upi-bank-account-picker");
        C14900mE r2 = ((ActivityC13810kN) this).A05;
        C17220qS r5 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r14 = this.A0P;
        C17070qD r11 = ((AbstractActivityC121685jC) this).A0P;
        C21860y6 r12 = ((AbstractActivityC121685jC) this).A0I;
        AnonymousClass102 r4 = this.A0D;
        C1308460e r7 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r10 = ((AbstractActivityC121685jC) this).A0M;
        C18640sm r0 = ((ActivityC13810kN) this).A07;
        C18650sn r9 = ((AbstractActivityC121685jC) this).A0K;
        C1329668y r8 = ((AbstractActivityC121665jA) this).A0B;
        this.A0J = new C120455gD(this, r2, r0, r4, r5, r7, r8, r12, r9, r10, r11, this, r14);
        C16590pI r3 = ((AbstractActivityC121685jC) this).A07;
        AbstractC14440lR r15 = ((ActivityC13830kP) this).A05;
        this.A0I = new AnonymousClass60I(r2, r3, r4, r5, this.A0F, r7, r8, r9, r10, r11, this, this.A0O, r14, r15);
        File file = new File(getCacheDir(), "BankLogos");
        if (!file.mkdirs() && !file.isDirectory()) {
            this.A0X.A06("BankAccountPickerUI/create unable to create bank logos cache directory");
        }
        C38771og r22 = new C38771og(((ActivityC13810kN) this).A05, this.A0C, ((ActivityC13810kN) this).A0D, file, "india-upi-bank-account-picker");
        r22.A00 = getResources().getDimensionPixelSize(R.dimen.india_upi_bank_picker_thumb_size);
        this.A0Q = r22.A00();
        setContentView(R.layout.india_upi_bank_account_picker);
        this.A02 = findViewById(R.id.add_button);
        this.A03 = findViewById(R.id.progress);
        this.A07 = findViewById(R.id.upi_logo);
        this.A06 = findViewById(R.id.shimmer_layout);
        this.A0B = (RecyclerView) findViewById(R.id.recycler_view);
        this.A04 = findViewById(R.id.header_divider);
        this.A0A = C12970iu.A0M(this, R.id.bank_account_picker_title);
        this.A09 = C12970iu.A0M(this, R.id.bank_account_picker_description);
        this.A08 = C117305Zk.A06(this, R.id.hero_img);
        this.A05 = findViewById(R.id.note_layout);
        AbstractC005102i A0K = AbstractActivityC119235dO.A0K(this);
        if (A0K != null) {
            A0K.A0M(true);
            A0K.A0A(R.string.payments_bank_account_picker_activity_title);
        }
        C14900mE r152 = ((ActivityC13810kN) this).A05;
        AnonymousClass12P r142 = ((ActivityC13790kL) this).A00;
        AnonymousClass01d r42 = ((ActivityC13810kN) this).A08;
        C42971wC.A08(this, Uri.parse("https://faq.whatsapp.com/general/payments/learn-more-about-sharing-the-legal-name-on-your-bank-account"), r142, r152, C12970iu.A0T(this.A05, R.id.note_name_visible_to_others), r42, C12960it.A0X(this, "learn-more", new Object[1], 0, R.string.payments_name_visible_to_others), "learn-more");
        A30();
        ((AbstractActivityC121665jA) this).A0D.AKg(0, null, "nav_select_account", null);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        A2w(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0J.A01 = null;
        ((AbstractActivityC121685jC) this).A0P.A04(this);
        this.A0Q.A02.A02(false);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_help && !this.A0V && this.A06.getVisibility() != 0) {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.context_help_banks_accounts_screen);
            A2x(A0S, "nav_select_account");
            return true;
        } else if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            this.A0X.A06("action bar home");
            A34(1);
            A2s();
            return true;
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu.findItem(R.id.menuitem_help) != null) {
            menu.findItem(R.id.menuitem_help).setVisible(C12960it.A1S(this.A06.getVisibility()));
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
