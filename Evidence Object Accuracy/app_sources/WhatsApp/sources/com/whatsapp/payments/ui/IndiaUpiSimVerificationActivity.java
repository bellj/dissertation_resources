package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass0OC;
import X.AnonymousClass2FL;
import X.AnonymousClass60W;
import X.AnonymousClass6BE;
import X.AnonymousClass6M2;
import X.C004802e;
import X.C117295Zj;
import X.C122205l5;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15890o4;
import X.C22120yY;
import X.C35741ib;
import android.content.Intent;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiSimVerificationActivity extends AbstractActivityC121665jA implements AnonymousClass6M2 {
    public C15890o4 A00;
    public C122205l5 A01;
    public AnonymousClass60W A02;
    public C22120yY A03;
    public boolean A04;

    public IndiaUpiSimVerificationActivity() {
        this(0);
    }

    public IndiaUpiSimVerificationActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 74);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A03 = (C22120yY) A1M.ANn.get();
            this.A00 = C12970iu.A0Y(A1M);
            this.A02 = A09.A0F();
            this.A01 = (C122205l5) A1M.A9Z.get();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.payments_sms_permission_msg || i == R.string.payments_error_sms_airplane || i == R.string.payments_error_sms_sim || i == R.string.payments_sim_verification_phone_number_not_matched_title || i == R.string.payments_sim_verification_phone_number_not_matched_desc) {
            finish();
        } else {
            super.A2A(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r1.A08() == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A30() {
        /*
        // Method dump skipped, instructions count: 642
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiSimVerificationActivity.A30():void");
    }

    public final void A31() {
        this.A01.A00.A09("verifyNumberClicked");
        Intent A0D = C12990iw.A0D(this, IndiaUpiDeviceBindStepActivity.class);
        A0D.putExtras(C12990iw.A0H(this));
        C35741ib.A00(A0D, "verifyNumber");
        A2v(A0D);
        startActivity(A0D);
        finish();
    }

    @Override // X.AnonymousClass6M2
    public void AW4(SubscriptionInfo subscriptionInfo) {
        if (Build.VERSION.SDK_INT >= 22) {
            ((AbstractActivityC121665jA) this).A0B.A0E(subscriptionInfo.getSubscriptionId());
            A31();
            return;
        }
        ((AbstractActivityC121665jA) this).A0R.A05("Why sim picker is showing for < 22 api level?");
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 153) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            ((AbstractActivityC121665jA) this).A0D.AKg(1, 66, "allow_sms_dialog", null);
            A30();
        } else {
            Ado(R.string.payments_sms_permission_msg);
            ((AbstractActivityC121665jA) this).A0D.AKg(1, 67, "allow_sms_dialog", null);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass6BE r3 = ((AbstractActivityC121665jA) this).A0D;
        Integer A0V = C12960it.A0V();
        r3.AKg(A0V, A0V, "verify_number", null);
        if (!((AbstractActivityC121665jA) this).A0B.A0L()) {
            Intent A0D = C12990iw.A0D(this, IndiaUpiBankPickerActivity.class);
            A2v(A0D);
            startActivity(A0D);
            finish();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0102, code lost:
        if (android.text.TextUtils.isEmpty(r6) == false) goto L_0x00dd;
     */
    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            super.onCreate(r10)
            r0 = 2131559158(0x7f0d02f6, float:1.8743652E38)
            r9.setContentView(r0)
            r1 = 2131232527(0x7f08070f, float:1.8081166E38)
            r0 = 2131365663(0x7f0a0f1f, float:1.8351198E38)
            r9.A2t(r1, r0)
            X.02i r1 = r9.A1U()
            r7 = 1
            if (r1 == 0) goto L_0x0026
            r0 = 2131890367(0x7f1210bf, float:1.9415424E38)
            java.lang.String r0 = r9.getString(r0)
            r1.A0I(r0)
            r1.A0M(r7)
        L_0x0026:
            r0 = 2131363050(0x7f0a04ea, float:1.8345898E38)
            android.widget.TextView r5 = X.C12970iu.A0M(r9, r0)
            r6 = 2131890633(0x7f1211c9, float:1.9415963E38)
            java.lang.Object[] r3 = new java.lang.Object[r7]
            X.0nT r0 = r9.A01
            java.lang.String r2 = X.C117305Zk.A0g(r0)
            r0 = 0
            if (r2 == 0) goto L_0x0043
            r1 = 32
            r0 = 160(0xa0, float:2.24E-43)
            java.lang.String r0 = r2.replace(r1, r0)
        L_0x0043:
            r4 = 0
            java.lang.String r0 = X.C12960it.A0X(r9, r0, r3, r4, r6)
            r5.setText(r0)
            X.01d r0 = r9.A08
            android.telephony.TelephonyManager r2 = r0.A0N()
            X.0o4 r1 = r9.A00
            java.lang.String r0 = "android.permission.SEND_SMS"
            int r0 = r1.A02(r0)
            if (r0 != 0) goto L_0x00dd
            boolean r0 = r1.A08()
            if (r0 == 0) goto L_0x00dd
            boolean r0 = X.C18640sm.A03(r9)
            if (r0 != 0) goto L_0x00dd
            if (r2 == 0) goto L_0x00dd
            int r1 = r2.getSimState()
            r0 = 5
            if (r1 != r0) goto L_0x00dd
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 22
            if (r1 < r0) goto L_0x00dd
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = X.AnonymousClass00T.A01(r9, r0)
            if (r0 != 0) goto L_0x00dd
            X.60W r0 = r9.A02
            java.util.List r2 = r0.A05(r9)
            int r1 = r2.size()
            r0 = 2
            if (r1 != r0) goto L_0x00dd
            r0 = 2131365184(0x7f0a0d40, float:1.8350226E38)
            android.widget.TextView r3 = X.C12970iu.A0M(r9, r0)
            X.0nT r0 = r9.A01
            r0.A08()
            X.1Ih r1 = r0.A05
            java.lang.String r8 = X.C12960it.A0g(r2, r4)
            java.lang.String r6 = X.C12960it.A0g(r2, r7)
            if (r1 == 0) goto L_0x00d7
            java.lang.String r0 = r1.user
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00d7
            java.lang.String r2 = r1.user
            X.60W r0 = r9.A02
            X.0zT r1 = r0.A01
            X.6BE r0 = r0.A0C
            boolean r0 = X.AnonymousClass60W.A00(r1, r0, r8, r2)
            if (r0 != 0) goto L_0x00d7
            X.60W r0 = r9.A02
            X.0zT r1 = r0.A01
            X.6BE r0 = r0.A0C
            boolean r0 = X.AnonymousClass60W.A00(r1, r0, r6, r2)
            if (r0 == 0) goto L_0x00f8
            r2 = 2131890634(0x7f1211ca, float:1.9415965E38)
            java.lang.Object[] r1 = new java.lang.Object[r7]
            X.0nT r0 = r9.A01
            java.lang.String r0 = X.C117305Zk.A0g(r0)
            java.lang.String r0 = X.C12960it.A0X(r9, r0, r1, r4, r2)
            r5.setText(r0)
        L_0x00d7:
            r0 = 2131890636(0x7f1211cc, float:1.941597E38)
            r3.setText(r0)
        L_0x00dd:
            r0 = 2131366600(0x7f0a12c8, float:1.8353098E38)
            android.view.View r1 = r9.findViewById(r0)
            r0 = 75
            X.C117295Zj.A0n(r1, r9, r0)
            X.6BE r5 = r9.A0D
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r2 = 0
            java.lang.String r1 = r9.A0K
            java.lang.String r0 = "verify_number"
            r5.AKg(r3, r2, r0, r1)
            return
        L_0x00f8:
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 != 0) goto L_0x00d7
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x00dd
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiSimVerificationActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        A2w(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_help) {
            C004802e A0S = C12980iv.A0S(this);
            AnonymousClass0OC r1 = A0S.A01;
            r1.A0C = null;
            r1.A01 = R.layout.india_upi_sim_verification_context_help_view;
            A2x(A0S, "verify_number");
            return true;
        }
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A01.A00.A09("verifyNumberShown");
    }
}
