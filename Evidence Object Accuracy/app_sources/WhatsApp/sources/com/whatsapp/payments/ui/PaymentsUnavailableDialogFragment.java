package com.whatsapp.payments.ui;

import X.ActivityC000900k;
import X.AnonymousClass01E;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class PaymentsUnavailableDialogFragment extends Hilt_PaymentsUnavailableDialogFragment {
    public AnonymousClass19Y A00;
    public AnonymousClass11G A01;

    public static PaymentsUnavailableDialogFragment A00() {
        PaymentsUnavailableDialogFragment paymentsUnavailableDialogFragment = new PaymentsUnavailableDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("arg_is_underage_unavailability", false);
        paymentsUnavailableDialogFragment.A0U(A0D);
        return paymentsUnavailableDialogFragment;
    }

    public static PaymentsUnavailableDialogFragment A01() {
        PaymentsUnavailableDialogFragment paymentsUnavailableDialogFragment = new PaymentsUnavailableDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("arg_is_underage_unavailability", true);
        paymentsUnavailableDialogFragment.A0U(A0D);
        return paymentsUnavailableDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        boolean z;
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            z = bundle2.getBoolean("arg_is_underage_unavailability");
        } else {
            z = false;
        }
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.payments_unavailable_title);
        int i = R.string.payments_unavailable_generic_description;
        if (z) {
            i = R.string.payments_unavailable_underage_description;
        }
        A0O.A06(i);
        A0O.A0B(false);
        int i2 = R.string.ok;
        if (z) {
            i2 = R.string.cancel;
        }
        A0O.setPositiveButton(i2, null);
        if (z) {
            A0O.A00(R.string.register_contact_support, new IDxCListenerShape9S0100000_2_I1(this, 44));
        }
        return A0O.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.finish();
        }
    }
}
