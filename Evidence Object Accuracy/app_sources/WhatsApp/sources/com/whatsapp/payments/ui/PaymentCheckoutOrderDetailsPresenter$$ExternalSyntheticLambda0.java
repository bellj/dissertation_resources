package com.whatsapp.payments.ui;

import X.AbstractC001200n;
import X.AbstractC16350or;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.C117315Zl;
import X.C125285r0;
import X.C129165xH;

/* loaded from: classes4.dex */
public final /* synthetic */ class PaymentCheckoutOrderDetailsPresenter$$ExternalSyntheticLambda0 implements AnonymousClass054 {
    public final /* synthetic */ C129165xH A00;

    public /* synthetic */ PaymentCheckoutOrderDetailsPresenter$$ExternalSyntheticLambda0(C129165xH r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass054
    public final void AWQ(AnonymousClass074 r4, AbstractC001200n r5) {
        AbstractC16350or r0;
        C129165xH r2 = this.A00;
        if (C117315Zl.A01(r4, C125285r0.A00) == 1 && (r0 = r2.A01) != null) {
            r0.A03(true);
            r2.A01 = null;
        }
    }
}
