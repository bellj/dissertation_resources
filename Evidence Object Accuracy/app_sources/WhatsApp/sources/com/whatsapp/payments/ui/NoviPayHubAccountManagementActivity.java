package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractActivityC121465iB;
import X.AbstractC118045bB;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass61F;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118245bV;
import X.C122395lO;
import X.C122435lS;
import X.C123355n1;
import X.C127055ts;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C130155yt;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviPayHubAccountManagementActivity extends AbstractActivityC121465iB {
    public C130155yt A00;
    public AnonymousClass61F A01;
    public boolean A02;

    public NoviPayHubAccountManagementActivity() {
        this(0);
    }

    public NoviPayHubAccountManagementActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 83);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A03(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A00 = (C130155yt) A1M.ADA.get();
            this.A01 = C117305Zk.A0X(A1M);
        }
    }

    @Override // X.AbstractActivityC121465iB, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 1001) {
            return new C122395lO(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_personal_info_row_item));
        }
        if (i != 1006) {
            return super.A2e(viewGroup, i);
        }
        return new C122435lS(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_pay_hub_text_row_item));
    }

    @Override // X.AbstractActivityC121465iB
    public void A2g(C127055ts r5) {
        Intent intent;
        int i;
        String str;
        super.A2g(r5);
        int i2 = r5.A00;
        if (i2 == 110) {
            intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
            intent.putExtra("screen_name", "novipay_p_remove_account");
            i = 101;
        } else if (i2 == 113) {
            intent = C12990iw.A0D(this, NoviPayBloksActivity.class);
            intent.putExtra("screen_name", "novipay_p_close_novi_account");
            i = 100;
        } else if (i2 == 400) {
            Intent A0D = C12990iw.A0D(this, NoviPayBloksActivity.class);
            A0D.putExtra("screen_name", "novipay_p_download_information");
            HashMap A11 = C12970iu.A11();
            if (getPackageManager().getLaunchIntentForPackage("com.novi.wallet") != null) {
                str = "1";
            } else {
                str = "0";
            }
            A11.put("is_novi_app_installed", str);
            A0D.putExtra("screen_params", A11);
            startActivity(A0D);
            return;
        } else if (i2 == 500) {
            A2C(R.string.payments_loading);
            return;
        } else if (i2 == 501) {
            AaN();
            return;
        } else {
            return;
        }
        startActivityForResult(intent, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        if (r8 == -1) goto L_0x0025;
     */
    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r7, int r8, android.content.Intent r9) {
        /*
            r6 = this;
            java.lang.Class<com.whatsapp.payments.ui.NoviSharedPaymentSettingsActivity> r5 = com.whatsapp.payments.ui.NoviSharedPaymentSettingsActivity.class
            r4 = 1
            java.lang.String r3 = "extra_account_removed"
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1 = -1
            r0 = 100
            if (r7 != r0) goto L_0x001f
            if (r8 != r1) goto L_0x001b
            X.0m9 r1 = r6.A0C
            r0 = 1122(0x462, float:1.572E-42)
            boolean r0 = r1.A07(r0)
            if (r0 != 0) goto L_0x0025
            r6.A2f()
        L_0x001b:
            super.onActivityResult(r7, r8, r9)
            return
        L_0x001f:
            r0 = 101(0x65, float:1.42E-43)
            if (r7 != r0) goto L_0x001b
            if (r8 != r1) goto L_0x001b
        L_0x0025:
            android.content.Intent r0 = X.C12990iw.A0D(r6, r5)
            r0.addFlags(r2)
            r0.putExtra(r3, r4)
            r6.startActivity(r0)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.NoviPayHubAccountManagementActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC118045bB r2 = (AbstractC118045bB) C117315Zl.A06(new C118245bV(((AbstractActivityC121465iB) this).A01), this).A00(C123355n1.class);
        r2.A00.A05(this, C117305Zk.A0B(this, 81));
        r2.A01.A05(this, C117305Zk.A0B(this, 80));
        AbstractActivityC119265dR.A0B(this, r2);
    }
}
