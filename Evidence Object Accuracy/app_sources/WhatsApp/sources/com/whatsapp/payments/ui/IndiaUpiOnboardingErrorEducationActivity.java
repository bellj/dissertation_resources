package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C12960it;
import X.C12990iw;
import android.content.Intent;
import android.view.MenuItem;

/* loaded from: classes4.dex */
public class IndiaUpiOnboardingErrorEducationActivity extends AbstractActivityC121665jA {
    public boolean A00;

    public IndiaUpiOnboardingErrorEducationActivity() {
        this(0);
    }

    public IndiaUpiOnboardingErrorEducationActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 50);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    public final void A30() {
        Class cls;
        int i = 0;
        if (getIntent() != null) {
            i = getIntent().getIntExtra("try_again", 0);
        }
        if (((AbstractActivityC121665jA) this).A0N || i <= 0) {
            cls = IndiaUpiPaymentsAccountSetupActivity.class;
        } else {
            if (i == 1) {
                cls = IndiaUpiBankPickerActivity.class;
            }
            finish();
        }
        Intent A0D = C12990iw.A0D(this, cls);
        A2v(A0D);
        startActivity(A0D);
        finish();
    }

    public final void A31(int i) {
        if (getIntent().hasExtra("extra_error_screen_name")) {
            ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), Integer.valueOf(i), getIntent().getStringExtra("extra_error_screen_name"), AbstractActivityC119235dO.A0N(this));
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A31(1);
        A30();
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ea A[LOOP:0: B:24:0x00e4->B:26:0x00ea, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x023b  */
    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
        // Method dump skipped, instructions count: 626
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiOnboardingErrorEducationActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A31(1);
        A30();
        return true;
    }
}
