package com.whatsapp.payments.ui;

import X.AbstractC13860kS;
import X.AbstractC15460nI;
import X.AbstractC16870pt;
import X.AbstractC38141na;
import X.AbstractC38191ng;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass102;
import X.AnonymousClass18T;
import X.AnonymousClass1ZO;
import X.AnonymousClass2S0;
import X.AnonymousClass3FP;
import X.AnonymousClass617;
import X.AnonymousClass61I;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118025b9;
import X.C120495gH;
import X.C129395xe;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C133776Cb;
import X.C14850m9;
import X.C14900mE;
import X.C15370n3;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C18610sj;
import X.C21860y6;
import X.C22710zW;
import X.C50942Ry;
import X.C74503iB;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentContactPickerFragment;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public class PaymentContactPickerFragment extends Hilt_PaymentContactPickerFragment {
    public C21860y6 A00;
    public AnonymousClass18T A01;
    public C22710zW A02;
    public AbstractC16870pt A03;
    public AnonymousClass2S0 A04;
    public C74503iB A05;
    public C118025b9 A06;
    public C129395xe A07;
    public String A08;
    public Map A09 = C12970iu.A11();

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1e() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1f() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1g() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1h() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1i() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1j() {
        return false;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1n() {
        return true;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment, X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        this.A0m.A00.A1U().A0A(R.string.new_payment);
        this.A08 = A1B().getString("referral_screen");
        this.A05 = C117305Zk.A0Z(A0C());
        this.A03 = C117305Zk.A0U(this.A1X);
        if (C117305Zk.A1U(this.A1O)) {
            C118025b9 A00 = this.A07.A00(A0C());
            this.A06 = A00;
            A00.A01.A0A(AnonymousClass617.A01(A00.A04.A00()));
            C117295Zj.A0s(A0C(), this.A06.A01, this, 119);
            return;
        }
        A1r();
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public String A1E(C15370n3 r4) {
        if (this.A02.A00(C15370n3.A05(r4)) != 2) {
            return A0I(R.string.contact_cant_receive_payments);
        }
        return null;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public String A1F(C15370n3 r3) {
        Jid A0B = r3.A0B(UserJid.class);
        if (A0B == null) {
            return null;
        }
        AnonymousClass1ZO r1 = (AnonymousClass1ZO) this.A09.get(A0B);
        AbstractC38141na AFL = this.A1X.A02().AFL();
        if (r1 == null || AFL == null || r1.A06(AFL.AFW()) != 2) {
            return null;
        }
        return A0I(R.string.payments_multi_invite_picker_subtitle);
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public void A1d(List list) {
        HashMap A11 = C12970iu.A11();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1ZO r1 = (AnonymousClass1ZO) it.next();
            A11.put(r1.A05, r1);
        }
        this.A09 = A11;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1k() {
        AnonymousClass2S0 r3 = this.A04;
        if (r3 == null || r3.A00(C117295Zj.A03(this.A13)) != 1) {
            return false;
        }
        return true;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1m() {
        if (!(this instanceof IndiaUpiContactPickerFragment)) {
            return this.A1O.A07(544) && this.A1X.A02().AFL() != null;
        }
        return C12960it.A1W(this.A1X.A02().AFL());
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1p(Intent intent, C15370n3 r15) {
        UserJid A05 = C15370n3.A05(r15);
        if (this.A02.A00(A05) != 2) {
            return true;
        }
        if (intent == null) {
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                intent = A0B.getIntent();
            } else {
                intent = null;
            }
        }
        C14900mE r7 = ((ContactPickerFragment) this).A0K;
        C17070qD r8 = this.A1X;
        AnonymousClass3FP r4 = new AnonymousClass3FP(A0B(), (AbstractC13860kS) A0C(), r7, r8, this.A05, new Runnable(A05, this) { // from class: X.6IT
            public final /* synthetic */ UserJid A00;
            public final /* synthetic */ PaymentContactPickerFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.A01.A1s(this.A00);
            }
        }, new Runnable(A05, this) { // from class: X.6IU
            public final /* synthetic */ UserJid A00;
            public final /* synthetic */ PaymentContactPickerFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PaymentContactPickerFragment paymentContactPickerFragment = this.A01;
                UserJid userJid = this.A00;
                ActivityC000900k A0B2 = paymentContactPickerFragment.A0B();
                if (A0B2 != null) {
                    A0B2.setResult(-1, C12970iu.A0A().putExtra("extra_invitee_jid", userJid.getRawString()));
                    A0B2.finish();
                }
            }
        }, true);
        if (r4.A02()) {
            this.A0m.Ady(0, R.string.register_wait_message);
            String str = null;
            if (intent != null) {
                str = intent.getStringExtra("referral_screen");
            }
            r4.A01(A05, new C133776Cb(this), str);
            return true;
        }
        A1s(A05);
        return true;
    }

    @Override // com.whatsapp.contact.picker.ContactPickerFragment
    public boolean A1q(C15370n3 r9) {
        C50942Ry r0;
        UserJid A05 = C15370n3.A05(r9);
        C118025b9 r6 = this.A06;
        if (r6 == null) {
            return false;
        }
        Map map = this.A09;
        AnonymousClass2S0 A00 = r6.A04.A00();
        AbstractC38191ng A0K = C117305Zk.A0K(r6.A03);
        if (A0K == null) {
            return false;
        }
        C14850m9 r2 = A0K.A07;
        if (r2.A07(979) || !r6.A04(A0K, A00)) {
            return false;
        }
        AnonymousClass1ZO r1 = (AnonymousClass1ZO) map.get(A05);
        if (!C117305Zk.A1U(r2) || (r0 = A00.A01) == null || A0K.A07(r1, A05, r0) != 1) {
            return false;
        }
        return true;
    }

    public final void A1r() {
        if (this.A03 != null) {
            AnonymousClass61I.A03(AnonymousClass61I.A00(this.A13, null, this.A04, null, false), this.A03, "payment_contact_picker", this.A08);
        }
    }

    public void A1s(UserJid userJid) {
        if (!(this instanceof IndiaUpiContactPickerFragment)) {
            Intent A00 = this.A01.A00(A0p(), false, false);
            A00.putExtra("referral_screen", "payment_contact_picker");
            A00.putExtra("extra_jid", userJid.getRawString());
            A0v(A00);
            C117315Zl.A0O(this);
            return;
        }
        IndiaUpiContactPickerFragment indiaUpiContactPickerFragment = (IndiaUpiContactPickerFragment) this;
        Context A0p = indiaUpiContactPickerFragment.A0p();
        C14850m9 r12 = indiaUpiContactPickerFragment.A1O;
        C14900mE r11 = ((ContactPickerFragment) indiaUpiContactPickerFragment).A0K;
        C15570nT r10 = ((ContactPickerFragment) indiaUpiContactPickerFragment).A0L;
        C17220qS r9 = indiaUpiContactPickerFragment.A1W;
        C17070qD r8 = indiaUpiContactPickerFragment.A1X;
        C21860y6 r7 = ((PaymentContactPickerFragment) indiaUpiContactPickerFragment).A00;
        C18610sj r6 = indiaUpiContactPickerFragment.A05;
        AnonymousClass102 r5 = indiaUpiContactPickerFragment.A00;
        AnonymousClass6BE r4 = indiaUpiContactPickerFragment.A06;
        new C120495gH(A0p, r11, r10, r5, r12, r9, indiaUpiContactPickerFragment.A01, r7, indiaUpiContactPickerFragment.A02, null, r6, r8, r4, indiaUpiContactPickerFragment.A07).A00(userJid, null, indiaUpiContactPickerFragment.A03.A03());
        ActivityC000900k A0B = indiaUpiContactPickerFragment.A0B();
        if (A0B instanceof ActivityC13810kN) {
            Intent A0D = C12990iw.A0D(A0B, indiaUpiContactPickerFragment.A1X.A02().AGb());
            A0D.putExtra("extra_jid", userJid.getRawString());
            A0D.putExtra("extra_is_pay_money_only", !indiaUpiContactPickerFragment.A1X.A0E.A00.A05(AbstractC15460nI.A0u));
            A0D.putExtra("referral_screen", "payment_contact_picker");
            ((ActivityC13810kN) A0B).A2G(A0D, true);
            return;
        }
        Log.e("India Payments' contact picker activity is null");
    }
}
