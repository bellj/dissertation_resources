package com.whatsapp.payments.ui.stepup;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61M;
import X.C004802e;
import X.C06380Tj;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117365Zq;
import X.C118015b8;
import X.C118345bf;
import X.C125005qW;
import X.C126035sE;
import X.C128155ve;
import X.C128365vz;
import X.C128375w0;
import X.C12960it;
import X.C129675y7;
import X.C12970iu;
import X.C12980iv;
import X.C129865yQ;
import X.C12990iw;
import X.C1316663q;
import X.C18610sj;
import X.C18650sn;
import X.C44891zj;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxCListenerShape5S0000000_3_I1;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.payments.ui.stepup.NoviReviewVideoSelfieActivity;
import com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView;
import com.whatsapp.videoplayback.VideoSurfaceView;
import java.util.List;

/* loaded from: classes4.dex */
public class NoviReviewVideoSelfieActivity extends ActivityC13790kL {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public CircularProgressBar A06;
    public WaButton A07;
    public WaTextView A08;
    public WaTextView A09;
    public WaTextView A0A;
    public C18650sn A0B;
    public C18610sj A0C;
    public AnonymousClass61M A0D;
    public C129865yQ A0E;
    public AnonymousClass60Y A0F;
    public C129675y7 A0G;
    public C128375w0 A0H;
    public C118015b8 A0I;
    public NoviSelfieFaceAnimationView A0J;
    public VideoSurfaceView A0K;
    public boolean A0L;
    public final BroadcastReceiver A0M;

    public NoviReviewVideoSelfieActivity() {
        this(0);
        this.A0M = new C117365Zq(this);
    }

    public NoviReviewVideoSelfieActivity(int i) {
        this.A0L = false;
        C117295Zj.A0p(this, 117);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0G = (C129675y7) A1M.AKB.get();
            this.A0F = C117305Zk.A0W(A1M);
            this.A0C = C117305Zk.A0M(A1M);
            this.A0B = (C18650sn) A1M.AEe.get();
            this.A0D = C117305Zk.A0N(A1M);
            this.A0H = C117315Zl.A0E(A1M);
        }
    }

    public void A2e(C128155ve r11) {
        long j;
        int i = -1;
        switch (r11.A00) {
            case 0:
                if (!TextUtils.isEmpty(r11.A06)) {
                    this.A0K.setVideoPath(r11.A06);
                    this.A0K.start();
                    NoviSelfieFaceAnimationView noviSelfieFaceAnimationView = this.A0J;
                    List list = r11.A07;
                    noviSelfieFaceAnimationView.A00();
                    noviSelfieFaceAnimationView.A0F = false;
                    noviSelfieFaceAnimationView.A09.setVisibility(8);
                    noviSelfieFaceAnimationView.A02 = C12970iu.A0E();
                    noviSelfieFaceAnimationView.A04(list);
                    return;
                }
                return;
            case 1:
                if (this.A0K.isPlaying()) {
                    return;
                }
                this.A0K.start();
                NoviSelfieFaceAnimationView noviSelfieFaceAnimationView = this.A0J;
                List list = r11.A07;
                noviSelfieFaceAnimationView.A00();
                noviSelfieFaceAnimationView.A0F = false;
                noviSelfieFaceAnimationView.A09.setVisibility(8);
                noviSelfieFaceAnimationView.A02 = C12970iu.A0E();
                noviSelfieFaceAnimationView.A04(list);
                return;
            case 2:
                this.A03.setVisibility(8);
                this.A05.setVisibility(0);
                this.A04.setVisibility(8);
                this.A02.setVisibility(8);
                this.A07.setVisibility(0);
                this.A0K.pause();
                this.A0K.seekTo(0);
                this.A0J.A02();
                this.A01.setVisibility(8);
                this.A00.setVisibility(8);
                this.A0J.setVisibility(8);
                this.A0K.setBackgroundColor(getResources().getColor(R.color.black_alpha_50));
                this.A06.setVisibility(0);
                j = 0;
                long j2 = r11.A03;
                WaTextView waTextView = this.A08;
                Object[] A1a = C12980iv.A1a();
                A1a[0] = C44891zj.A00(((ActivityC13830kP) this).A01, j, true, false).first;
                waTextView.setText(C12960it.A0X(this, C44891zj.A00(((ActivityC13830kP) this).A01, j2, true, false).first, A1a, 1, R.string.novi_selfie_upload_progress));
                this.A06.setMax(100);
                this.A06.setProgress(Math.min(95, (int) ((j * 100) / j2)));
                return;
            case 3:
                j = r11.A02;
                long j2 = r11.A03;
                WaTextView waTextView = this.A08;
                Object[] A1a = C12980iv.A1a();
                A1a[0] = C44891zj.A00(((ActivityC13830kP) this).A01, j, true, false).first;
                waTextView.setText(C12960it.A0X(this, C44891zj.A00(((ActivityC13830kP) this).A01, j2, true, false).first, A1a, 1, R.string.novi_selfie_upload_progress));
                this.A06.setMax(100);
                this.A06.setProgress(Math.min(95, (int) ((j * 100) / j2)));
                return;
            case 4:
                this.A08.setVisibility(8);
                this.A0A.setText(getResources().getString(R.string.novi_selfie_verifying));
                this.A09.setText(getResources().getString(R.string.novi_selfie_verify_progress_description));
                this.A07.setText(R.string.novi_selfie_verify_in_background);
                return;
            case 5:
            case 9:
                this.A0K.A00();
                this.A0J.A00();
                setResult(i);
                finish();
                return;
            case 6:
                this.A0E.A02(r11.A04, new Runnable(r11) { // from class: X.6If
                    public final /* synthetic */ C128155ve A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NoviReviewVideoSelfieActivity noviReviewVideoSelfieActivity = NoviReviewVideoSelfieActivity.this;
                        if (this.A01.A04.A00 == 456) {
                            noviReviewVideoSelfieActivity.A0K.A00();
                            noviReviewVideoSelfieActivity.A0J.A00();
                            noviReviewVideoSelfieActivity.setResult(-1);
                            noviReviewVideoSelfieActivity.finish();
                            return;
                        }
                        noviReviewVideoSelfieActivity.A2e(new C128155ve(7));
                    }
                }, null);
                return;
            case 7:
                i = 0;
                this.A0K.A00();
                this.A0J.A00();
                setResult(i);
                finish();
                return;
            case 8:
                this.A03.setVisibility(0);
                this.A05.setVisibility(8);
                this.A04.setVisibility(0);
                this.A02.setVisibility(0);
                this.A07.setVisibility(8);
                this.A01.setVisibility(0);
                this.A0J.setVisibility(0);
                this.A0K.setBackgroundColor(0);
                this.A06.setVisibility(8);
                C004802e A0S = C12980iv.A0S(this);
                A0S.A03(new IDxCListenerShape5S0000000_3_I1(8), C117295Zj.A0S(this, A0S, R.string.novi_selfie_upload_media_failed));
                A0S.A05();
                return;
            case 10:
                this.A0K.A00();
                this.A0J.A00();
                setResult(-1);
                C1316663q r2 = r11.A05;
                if (r2 != null) {
                    C125005qW.A00(this, r2, this.A0G, r11.A01);
                    return;
                }
                finish();
                return;
            default:
                return;
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C128365vz r3 = new AnonymousClass610("EXIT_CLICK", "SELFIE_UPLOAD", "LINK").A00;
        r3.A0W = "SELFIE";
        C129675y7 r2 = this.A0G;
        C1316663q r1 = r2.A01;
        if (r1 != null) {
            r3.A0E = r1.A02;
            r3.A0f = r1.A03;
        }
        if (r2.A00 != 10) {
            this.A0F.A05(r3);
        }
        this.A0I.A04(this, new C126035sE(3));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_review_video_selfie);
        this.A0K = (VideoSurfaceView) findViewById(R.id.selfie_video_view);
        this.A01 = findViewById(R.id.selfie_replay_button);
        this.A00 = findViewById(R.id.loading_progress);
        this.A0J = (NoviSelfieFaceAnimationView) findViewById(R.id.review_selfie_face_animation);
        this.A03 = findViewById(R.id.review_instructions);
        this.A05 = findViewById(R.id.selfie_upload_instructions);
        this.A04 = findViewById(R.id.security_note);
        this.A02 = findViewById(R.id.action_buttons_container);
        this.A07 = (WaButton) findViewById(R.id.upload_selfie_background);
        this.A08 = (WaTextView) findViewById(R.id.file_upload_progress);
        this.A0A = (WaTextView) findViewById(R.id.selfie_upload_title);
        this.A09 = (WaTextView) findViewById(R.id.selfie_upload_description);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.circular_progress_bar);
        this.A06 = circularProgressBar;
        circularProgressBar.A0C = AnonymousClass00T.A00(this, R.color.white);
        this.A06.A0B = AnonymousClass00T.A00(this, R.color.white_alpha_40);
        Toolbar toolbar = (Toolbar) findViewById(R.id.review_selfie_toolbar);
        Drawable A04 = AnonymousClass00T.A04(this, R.drawable.novi_wordmark);
        AnonymousClass009.A05(A04);
        toolbar.setLogo(AnonymousClass2GE.A03(this, A04, R.color.novi_header));
        toolbar.setNavigationIcon(AnonymousClass2GF.A00(this, ((ActivityC13830kP) this).A01, R.drawable.ic_close));
        toolbar.setNavigationOnClickListener(C117305Zk.A0A(this, 141));
        View findViewById = this.A0J.findViewById(R.id.novi_selfie_animation_face_container);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) findViewById.getLayoutParams();
        int dimension = (int) getResources().getDimension(R.dimen.novi_selfie_review_face_animation_size);
        layoutParams.width = dimension;
        layoutParams.height = dimension;
        findViewById.setLayoutParams(layoutParams);
        if (Build.VERSION.SDK_INT >= 21) {
            this.A0J.setElevation(findViewById(R.id.video_card_view).getElevation());
        }
        VideoSurfaceView videoSurfaceView = this.A0K;
        videoSurfaceView.A0B = new MediaPlayer.OnPreparedListener() { // from class: X.63P
            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer) {
                NoviReviewVideoSelfieActivity.this.A00.setVisibility(8);
            }
        };
        videoSurfaceView.A09 = new MediaPlayer.OnCompletionListener() { // from class: X.63N
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                NoviReviewVideoSelfieActivity.this.A01.setVisibility(0);
            }
        };
        C117295Zj.A0n(this.A01, this, 142);
        C117295Zj.A0n(findViewById(R.id.submit_selfie_button), this, 138);
        C117295Zj.A0n(findViewById(R.id.retake_selfie_button), this, 139);
        C117295Zj.A0n(this.A07, this, 140);
        C128375w0 r1 = this.A0H;
        if (bundle == null) {
            bundle = C12990iw.A0H(this);
        }
        C118015b8 r2 = (C118015b8) C117315Zl.A06(new C118345bf(bundle, r1), this).A00(C118015b8.class);
        this.A0I = r2;
        r2.A02.A05(this, C117305Zk.A0B(this, 131));
        this.A0I.A04(this, new C126035sE(0));
        C06380Tj.A00(this).A02(this.A0M, new IntentFilter("NoviReviewVideoSelfieActivity.selfie_service_events"));
        this.A0E = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A0D);
        C128365vz r3 = new AnonymousClass610("NAVIGATION_START", "SELFIE_UPLOAD", "BODY").A00;
        r3.A0W = "SELFIE";
        C129675y7 r22 = this.A0G;
        C1316663q r12 = r22.A01;
        if (r12 != null) {
            r3.A0E = r12.A02;
            r3.A0f = r12.A03;
        }
        if (r22.A00 != 10) {
            this.A0F.A05(r3);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C06380Tj.A00(this).A01(this.A0M);
        C128365vz r3 = new AnonymousClass610("NAVIGATION_END", "SELFIE_UPLOAD", "BODY").A00;
        r3.A0W = "SELFIE";
        C129675y7 r2 = this.A0G;
        C1316663q r1 = r2.A01;
        if (r1 != null) {
            r3.A0E = r1.A02;
            r3.A0f = r1.A03;
        }
        if (r2.A00 != 10) {
            this.A0F.A05(r3);
        }
    }
}
