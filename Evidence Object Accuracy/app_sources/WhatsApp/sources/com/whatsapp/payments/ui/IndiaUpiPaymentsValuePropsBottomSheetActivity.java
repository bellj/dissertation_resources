package com.whatsapp.payments.ui;

import X.AbstractActivityC121495iO;
import X.AbstractActivityC121605ir;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.ActivityC000900k;
import X.AnonymousClass028;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C117315Zl;
import X.C12960it;
import X.C12990iw;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextSwitcher;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.RoundedBottomSheetDialogFragment;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentsValuePropsBottomSheetActivity extends AbstractActivityC121605ir {

    /* loaded from: classes4.dex */
    public class BottomSheetValuePropsFragment extends RoundedBottomSheetDialogFragment {
        @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
        public void A0l() {
            super.A0l();
            ActivityC000900k A0B = A0B();
            if (A0B instanceof IndiaUpiPaymentsValuePropsBottomSheetActivity) {
                ((AbstractActivityC121495iO) A0B).A31();
            }
            C117315Zl.A0O(this);
        }

        @Override // X.AnonymousClass01E
        public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
            View inflate = layoutInflater.inflate(R.layout.india_upi_payment_value_props_bottom_sheet, viewGroup, false);
            View A0D = AnonymousClass028.A0D(inflate, R.id.close);
            AbstractActivityC121495iO r2 = (AbstractActivityC121495iO) A0B();
            if (r2 != null) {
                C117295Zj.A0o(A0D, this, r2, 16);
                TextView A0I = C12960it.A0I(inflate, R.id.value_props_sub_title);
                View A0D2 = AnonymousClass028.A0D(inflate, R.id.value_props_contact_img);
                TextSwitcher textSwitcher = (TextSwitcher) AnonymousClass028.A0D(inflate, R.id.value_props_desc);
                TextView A0I2 = C12960it.A0I(inflate, R.id.value_props_continue);
                if (((AbstractActivityC121665jA) r2).A02 == 2) {
                    A0I2.setText(R.string.btn_continue);
                    A0D2.setVisibility(8);
                    C12990iw.A1H(A0I, this, R.string.payments_value_props_invites_desc_text);
                    textSwitcher.setText(A0I(R.string.payments_value_props_invites_contacts_desc_text));
                    r2.A33(null);
                    if (((AbstractActivityC121685jC) r2).A0F != null) {
                        AnonymousClass6BE r5 = ((AbstractActivityC121665jA) r2).A0D;
                        r5.A02.A07(r5.A03(C12960it.A0V(), 55, "chat", r2.A02, r2.A0g, r2.A0f, C12960it.A1V(((AbstractActivityC121665jA) r2).A02, 11)));
                    }
                } else {
                    r2.A32(textSwitcher);
                    if (((AbstractActivityC121665jA) r2).A02 == 11) {
                        C12990iw.A1H(A0I, this, R.string.payments_value_props_p2m_bottom_sheet_subtitle_text_1);
                        AnonymousClass028.A0D(inflate, R.id.value_props_sub_title_2).setVisibility(0);
                    }
                }
                C117295Zj.A0n(A0I2, r2, 58);
            }
            return inflate;
        }
    }

    @Override // X.AbstractActivityC121495iO, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        paymentBottomSheet.A01 = new BottomSheetValuePropsFragment();
        Adm(paymentBottomSheet);
    }
}
