package com.whatsapp.payments.ui;

import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass6M2;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C12960it;
import X.C12970iu;
import X.C15570nT;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiSimPickerDialogFragment extends Hilt_IndiaUpiSimPickerDialogFragment {
    public C15570nT A00;
    public AnonymousClass6M2 A01;
    public List A02;

    public static /* synthetic */ void A00(RadioGroup radioGroup, IndiaUpiSimPickerDialogFragment indiaUpiSimPickerDialogFragment) {
        List list;
        indiaUpiSimPickerDialogFragment.A1B();
        AnonymousClass6M2 r2 = indiaUpiSimPickerDialogFragment.A01;
        if (r2 != null && (list = indiaUpiSimPickerDialogFragment.A02) != null) {
            r2.AW4((SubscriptionInfo) list.get(radioGroup.getCheckedRadioButtonId()));
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A01 = null;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null && dialog.getWindow() != null) {
            ((DialogFragment) this).A03.getWindow().setLayout(-1, -2);
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_sim_picker);
    }

    @Override // com.whatsapp.payments.ui.Hilt_IndiaUpiSimPickerDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A01 = (AnonymousClass6M2) A0B();
        } catch (ClassCastException e) {
            Log.e(C12960it.A0d(e.getMessage(), C12960it.A0k("onAttach:")));
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String A0J;
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            this.A02 = bundle2.getParcelableArrayList("extra_subscriptions");
        }
        C12960it.A0I(view, R.id.title).setText(C12970iu.A0q(this, C117305Zk.A0g(this.A00), new Object[1], 0, R.string.payments_sim_picker_title));
        ViewGroup A04 = C117315Zl.A04(view, R.id.radio_group);
        A04.removeAllViews();
        if (this.A02 != null) {
            int i = 0;
            while (i < this.A02.size()) {
                SubscriptionInfo subscriptionInfo = (SubscriptionInfo) this.A02.get(i);
                TextView textView = (TextView) LayoutInflater.from(A0p()).inflate(R.layout.india_upi_sim_picker_radio_button, A04, false);
                textView.setId(i);
                i++;
                if (Build.VERSION.SDK_INT < 22 || TextUtils.isEmpty(subscriptionInfo.getDisplayName())) {
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, i, 0);
                    A0J = A0J(R.string.sim_1_with_placeholder, objArr);
                } else {
                    StringBuilder A0h = C12960it.A0h();
                    Object[] objArr2 = new Object[1];
                    C12960it.A1P(objArr2, i, 0);
                    A0h.append(A0J(R.string.sim_1_with_placeholder, objArr2));
                    A0h.append(" - ");
                    A0J = C12970iu.A0s(subscriptionInfo.getDisplayName(), A0h);
                }
                textView.setText(A0J);
                A04.addView(textView);
            }
            if (A04.getChildCount() > 0) {
                ((CompoundButton) A04.getChildAt(0)).setChecked(true);
            }
        }
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.cancel_button), this, 74);
        C117295Zj.A0o(AnonymousClass028.A0D(view, R.id.confirm_button), this, A04, 17);
    }
}
