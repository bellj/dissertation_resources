package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC1311261j;
import X.AbstractC28901Pl;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117615aH;
import X.C12980iv;
import X.C1311161i;
import X.C30931Zj;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentMethodSelectionActivity;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentMethodSelectionActivity extends AbstractActivityC121665jA implements AbstractC1311261j {
    public C117615aH A00;
    public boolean A01;
    public final C30931Zj A02;

    @Override // X.AbstractC1311261j
    public int AEM(AbstractC28901Pl r2) {
        return 0;
    }

    @Override // X.AbstractC1311261j
    public String AEN(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC136326Mc
    public String AEP(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC1311261j
    public boolean AdT() {
        return false;
    }

    @Override // X.AbstractC1311261j
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC1311261j
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    public IndiaUpiPaymentMethodSelectionActivity() {
        this(0);
        this.A02 = C117295Zj.A0G("IndiaUpiPaymentMethodSelectionActivity");
    }

    public IndiaUpiPaymentMethodSelectionActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 55);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    @Override // X.AbstractC136326Mc
    public String AEQ(AbstractC28901Pl r4) {
        return C1311161i.A02(this, ((ActivityC13830kP) this).A01, r4, ((AbstractActivityC121685jC) this).A0P, false);
    }

    @Override // X.AbstractC1311261j
    public /* synthetic */ boolean AdN(AbstractC28901Pl r2) {
        return false;
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.india_upi_select_payment_method);
        if (getIntent() == null) {
            this.A02.A04("got null bank account or balance; finishing");
            finish();
            return;
        }
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I("Select bank account");
            A1U.A0M(true);
        }
        this.A02.A06("onCreate");
        AbsListView absListView = (AbsListView) findViewById(R.id.payment_methods_list);
        C117615aH r0 = new C117615aH(this, ((ActivityC13830kP) this).A01, ((AbstractActivityC121685jC) this).A0P, this);
        this.A00 = r0;
        r0.A02 = (List) getIntent().getSerializableExtra("bank_accounts");
        r0.notifyDataSetChanged();
        absListView.setAdapter((ListAdapter) this.A00);
        absListView.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.65A
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                IndiaUpiPaymentMethodSelectionActivity indiaUpiPaymentMethodSelectionActivity = IndiaUpiPaymentMethodSelectionActivity.this;
                AbstractC28901Pl A08 = C117315Zl.A08(indiaUpiPaymentMethodSelectionActivity.A00.A02, i);
                C119755f3 r02 = (C119755f3) A08.A08;
                if (r02 == null || C12970iu.A1Y(r02.A05.A00)) {
                    Intent A0D = C12990iw.A0D(indiaUpiPaymentMethodSelectionActivity, IndiaUpiCheckBalanceActivity.class);
                    C117315Zl.A0M(A0D, A08);
                    indiaUpiPaymentMethodSelectionActivity.startActivity(A0D);
                    return;
                }
                C36021jC.A01(indiaUpiPaymentMethodSelectionActivity, 29);
            }
        });
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 29) {
            return super.onCreateDialog(i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A07(R.string.upi_check_balance_no_pin_set_title);
        A0S.A06(R.string.upi_check_balance_no_pin_set_message);
        C117295Zj.A0q(A0S, this, 42, R.string.learn_more);
        C117305Zk.A18(A0S, this, 43, R.string.ok);
        return A0S.create();
    }
}
