package com.whatsapp.payments.ui.widget;

import X.AbstractC117775ab;
import X.AbstractC136376Mh;
import X.AnonymousClass1A8;
import X.AnonymousClass1IR;
import X.AnonymousClass2GE;
import X.C117305Zk;
import X.C12960it;
import X.C12970iu;
import X.C17070qD;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class PaymentInteropShimmerRow extends AbstractC117775ab implements AbstractC136376Mh {
    public View A00;
    public View A01;
    public AnonymousClass1IR A02;
    public C17070qD A03;
    public AnonymousClass1A8 A04;

    public PaymentInteropShimmerRow(Context context) {
        super(context);
        A01();
    }

    public PaymentInteropShimmerRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public PaymentInteropShimmerRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public final void A01() {
        C12960it.A0E(this).inflate(R.layout.payment_transaction_shimmer, this);
        setOrientation(1);
        this.A00 = findViewById(R.id.payment_shimmer);
        this.A01 = findViewById(R.id.static_shimmer);
        AnonymousClass2GE.A05(getContext(), C12970iu.A0L(this, R.id.transaction_loading_error), R.color.payments_error_exclamation);
        setOnClickListener(C117305Zk.A0A(this, 188));
    }

    /* renamed from: A02 */
    public void A6W(AnonymousClass1IR r5) {
        boolean contains;
        this.A02 = r5;
        AnonymousClass1A8 r2 = this.A04;
        String str = r5.A0K;
        if (TextUtils.isEmpty(str)) {
            contains = false;
        } else {
            contains = r2.A00.contains(str);
        }
        View view = this.A00;
        if (contains) {
            view.setVisibility(8);
            this.A01.setVisibility(0);
            return;
        }
        view.setVisibility(0);
        this.A01.setVisibility(8);
    }

    @Override // X.AbstractC136376Mh
    public void AaB() {
        AnonymousClass1IR r0 = this.A02;
        if (r0 != null) {
            A6W(r0);
        }
    }
}
