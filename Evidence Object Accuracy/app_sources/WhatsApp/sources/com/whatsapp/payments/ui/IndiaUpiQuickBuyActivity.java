package com.whatsapp.payments.ui;

import X.AnonymousClass6LX;
import android.content.Intent;

/* loaded from: classes4.dex */
public class IndiaUpiQuickBuyActivity extends IndiaUpiCheckOrderDetailsActivity implements AnonymousClass6LX {
    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 0 || i2 == 252 || i2 == 251 || i2 == 250) {
            finish();
            overridePendingTransition(0, 0);
        }
    }
}
