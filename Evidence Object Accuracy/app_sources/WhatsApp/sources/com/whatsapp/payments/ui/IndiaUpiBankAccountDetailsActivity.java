package com.whatsapp.payments.ui;

import X.AbstractActivityC119275dS;
import X.AbstractC005102i;
import X.AbstractC136106La;
import X.AbstractC15460nI;
import X.AbstractC28901Pl;
import X.AbstractC36671kL;
import X.AbstractView$OnClickListenerC121765jx;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass102;
import X.AnonymousClass17Q;
import X.AnonymousClass1ZR;
import X.AnonymousClass1ZY;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass69E;
import X.AnonymousClass6BE;
import X.AnonymousClass6MA;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117325Zm;
import X.C119755f3;
import X.C120515gJ;
import X.C121265hX;
import X.C124115od;
import X.C125125qj;
import X.C128645wR;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C1308460e;
import X.C1309960u;
import X.C1311161i;
import X.C1329668y;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C18590sh;
import X.C18610sj;
import X.C18650sn;
import X.C21860y6;
import X.C30861Zc;
import X.C30931Zj;
import X.RunnableC134636Fj;
import X.View$OnClickListenerC117685aR;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;
import com.whatsapp.payments.ui.PaymentDeleteAccountActivity;

/* loaded from: classes4.dex */
public class IndiaUpiBankAccountDetailsActivity extends AbstractView$OnClickListenerC121765jx implements AbstractC136106La {
    public C30861Zc A00;
    public AnonymousClass102 A01;
    public C17220qS A02;
    public C1308460e A03;
    public C1329668y A04;
    public C18650sn A05;
    public C18610sj A06;
    public C120515gJ A07;
    public AnonymousClass6BE A08;
    public AnonymousClass17Q A09;
    public AnonymousClass69E A0A;
    public C121265hX A0B;
    public View$OnClickListenerC117685aR A0C;
    public C1309960u A0D;
    public C128645wR A0E;
    public C18590sh A0F;
    public boolean A0G;
    public final C30931Zj A0H;

    public IndiaUpiBankAccountDetailsActivity() {
        this(0);
        this.A0H = C117295Zj.A0G("IndiaUpiBankAccountDetailsActivity");
    }

    public IndiaUpiBankAccountDetailsActivity(int i) {
        this.A0G = false;
        C117295Zj.A0p(this, 33);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0G) {
            this.A0G = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119275dS.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A09 = (AnonymousClass17Q) A1M.A7n.get();
            this.A02 = C12990iw.A0c(A1M);
            this.A0F = C117315Zl.A0F(A1M);
            this.A0A = (AnonymousClass69E) A1M.A9W.get();
            this.A03 = (C1308460e) A1M.A9c.get();
            this.A0D = (C1309960u) A1M.A1b.get();
            this.A06 = C117305Zk.A0M(A1M);
            this.A01 = C117305Zk.A0G(A1M);
            this.A08 = C117305Zk.A0T(A1M);
            this.A05 = (C18650sn) A1M.AEe.get();
            this.A04 = (C1329668y) A1M.A9d.get();
            this.A0B = (C121265hX) A1M.A9a.get();
        }
    }

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2g() {
        RunnableC134636Fj r3 = new Runnable() { // from class: X.6Fj
            @Override // java.lang.Runnable
            public final void run() {
                RunnableC134636Fj.super.A2g();
            }
        };
        C12960it.A1E(new C124115od(this, r3, 103), ((AbstractView$OnClickListenerC121765jx) this).A0H);
    }

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2i(AbstractC28901Pl r7, boolean z) {
        View view;
        int i;
        super.A2i(r7, z);
        C30861Zc r0 = (C30861Zc) r7;
        this.A00 = r0;
        if (z) {
            String A07 = C1311161i.A07(r0);
            TextView textView = ((AbstractView$OnClickListenerC121765jx) this).A03;
            StringBuilder A0j = C12960it.A0j(this.A00.A0B);
            C117325Zm.A09(A0j);
            textView.setText(C12960it.A0d(A07, A0j));
            ((AbstractView$OnClickListenerC121765jx) this).A04.setText(C12960it.A0X(this, this.A04.A04().A00, new Object[1], 0, R.string.vpa_prefix));
            ((AbstractView$OnClickListenerC121765jx) this).A04.A02 = C1329668y.A00(this.A04);
            ((AbstractView$OnClickListenerC121765jx) this).A04.A03 = getString(R.string.vpa_copied_to_clipboard);
            AnonymousClass1ZY r2 = this.A00.A08;
            if (r2 instanceof C119755f3) {
                ((AbstractView$OnClickListenerC121765jx) this).A02.setText(((C119755f3) r2).A0E());
            }
            ((ViewGroup) findViewById(R.id.payment_method_additional_details_container)).addView(LayoutInflater.from(this).inflate(R.layout.india_upi_payment_setting_view_balance_row, (ViewGroup) null));
            C117295Zj.A0n(findViewById(R.id.check_balance_container), this, 26);
            AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.check_balance_icon), AnonymousClass00T.A00(this, R.color.settings_icon));
            findViewById(R.id.default_payment_method_divider).setVisibility(8);
            this.A0C = new View$OnClickListenerC117685aR(this);
            ((ViewGroup) findViewById(R.id.widget_container)).addView(this.A0C);
            View$OnClickListenerC117685aR r22 = this.A0C;
            r22.A07 = this;
            r22.findViewById(R.id.reset_upi_pin_container).setOnClickListener(r22);
            r22.A02 = C12960it.A0J(r22, R.id.reset_upi_pin);
            r22.A00 = r22.findViewById(R.id.change_upi_pin_container);
            r22.A01 = r22.findViewById(R.id.switch_payment_provider_container);
            AnonymousClass1ZR r02 = ((C119755f3) r7.A08).A05;
            r22.A06 = r02;
            if (!C12970iu.A1Y(r02.A00)) {
                r22.A02.setText(R.string.payments_reset_upi_pin_activity_title);
                view = r22.A00;
                i = 8;
            } else {
                view = r22.A00;
                i = 0;
            }
            view.setVisibility(i);
            r22.A00.setOnClickListener(r22);
            r22.A01.setOnClickListener(r22);
            this.A0C.A01.setVisibility(C12960it.A02(!C12960it.A1S(((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0s) ? 1 : 0) ? 1 : 0));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        if (r5 == 1017) goto L_0x0021;
     */
    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r5, int r6, android.content.Intent r7) {
        /*
            r4 = this;
            r0 = -1
            if (r6 != r0) goto L_0x000a
            if (r5 != 0) goto L_0x000e
            X.5wR r0 = r4.A0E
            r0.A00(r4)
        L_0x000a:
            super.onActivityResult(r5, r6, r7)
            return
        L_0x000e:
            r0 = 1012(0x3f4, float:1.418E-42)
            if (r5 != r0) goto L_0x0018
            X.5aR r0 = r4.A0C
            r0.A00()
            goto L_0x000a
        L_0x0018:
            r0 = 1016(0x3f8, float:1.424E-42)
            if (r5 != r0) goto L_0x0036
            X.5aR r0 = r4.A0C
            r0.A00()
        L_0x0021:
            X.1Zc r3 = r4.A00
            r2 = 1
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiPinSetUpCompletedActivity> r0 = com.whatsapp.payments.ui.IndiaUpiPinSetUpCompletedActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r4, r0)
            X.C117315Zl.A0M(r1, r3)
            java.lang.String r0 = "on_settings_page"
            r1.putExtra(r0, r2)
            r4.startActivity(r1)
            goto L_0x000a
        L_0x0036:
            r0 = 1017(0x3f9, float:1.425E-42)
            if (r5 != r0) goto L_0x000a
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        this.A0D.A02(new AnonymousClass6MA() { // from class: X.6D1
            @Override // X.AnonymousClass6MA
            public final void AVY() {
                C1309960u.A01(IndiaUpiBankAccountDetailsActivity.this);
            }
        });
        this.A0E = new C128645wR(((AbstractView$OnClickListenerC121765jx) this).A0A);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.payments_bank_account_details);
            A1U.A0M(true);
        }
        this.A0H.A06("onCreate");
        C12970iu.A0K(getLayoutInflater().inflate(R.layout.india_upi_psp_footer_row, (ViewGroup) findViewById(R.id.footer_container), true), R.id.psp_logo).setImageResource(C125125qj.A00(this.A04.A07()).A00);
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r15 = ((AbstractView$OnClickListenerC121765jx) this).A05;
        C15570nT r13 = ((ActivityC13790kL) this).A01;
        C17220qS r12 = this.A02;
        C17070qD r11 = ((AbstractView$OnClickListenerC121765jx) this).A0D;
        C18590sh r10 = this.A0F;
        C1308460e r9 = this.A03;
        C21860y6 r8 = ((AbstractView$OnClickListenerC121765jx) this).A0A;
        C18610sj r7 = this.A06;
        AnonymousClass102 r6 = this.A01;
        AnonymousClass6BE r5 = this.A08;
        this.A07 = new C120515gJ(this, r15, r13, ((ActivityC13810kN) this).A07, r6, r0, r12, r9, this.A04, r8, this.A05, r7, r11, r5, this.A0B, r10);
    }

    @Override // X.AbstractView$OnClickListenerC121765jx, android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        int i2;
        int i3;
        switch (i) {
            case 100:
                C17070qD r0 = ((AbstractView$OnClickListenerC121765jx) this).A0D;
                r0.A03();
                boolean A1U = C12960it.A1U(r0.A08.A0T(1).size());
                r3 = C12980iv.A0S(this);
                int i4 = R.string.switch_psp_dialog_title;
                if (A1U) {
                    i4 = R.string.switch_psp_dialog_title_with_warning;
                }
                r3.A0A(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, getString(i4)));
                r3.A0B(true);
                r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.62S
                    public final /* synthetic */ int A00 = 100;

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i5) {
                        C36021jC.A00(IndiaUpiBankAccountDetailsActivity.this, this.A00);
                    }
                });
                r3.setPositiveButton(R.string.payments_remove_and_continue, new DialogInterface.OnClickListener() { // from class: X.62T
                    public final /* synthetic */ int A00 = 100;

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i5) {
                        IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = IndiaUpiBankAccountDetailsActivity.this;
                        C36021jC.A00(indiaUpiBankAccountDetailsActivity, this.A00);
                        indiaUpiBankAccountDetailsActivity.A0H.A06("unlinking the payment account.");
                        Intent A0D = C12990iw.A0D(indiaUpiBankAccountDetailsActivity, PaymentDeleteAccountActivity.class);
                        A0D.putExtra("extra_remove_payment_account", 1);
                        indiaUpiBankAccountDetailsActivity.startActivityForResult(A0D, 0);
                    }
                });
                r3.A08(new DialogInterface.OnCancelListener() { // from class: X.628
                    public final /* synthetic */ int A00 = 100;

                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        C36021jC.A00(IndiaUpiBankAccountDetailsActivity.this, this.A00);
                    }
                });
                break;
            case 101:
                r3 = C12980iv.A0S(this);
                r3.A07(R.string.upi_check_balance_no_pin_set_title);
                r3.A06(R.string.upi_check_balance_no_pin_set_message);
                C117295Zj.A0q(r3, this, 12, R.string.learn_more);
                i2 = R.string.ok;
                i3 = 13;
                C117305Zk.A18(r3, this, i3, i2);
                break;
            case 102:
            default:
                return super.onCreateDialog(i);
            case 103:
                r3 = C12980iv.A0S(this);
                r3.A06(R.string.upi_mandate_check_alert_message_remove_account);
                i2 = R.string.ok;
                i3 = 11;
                C117305Zk.A18(r3, this, i3, i2);
                break;
            case 104:
                r3 = C12980iv.A0S(this);
                r3.A06(R.string.upi_mandate_check_alert_message_switch_payment_provider);
                i2 = R.string.ok;
                i3 = 14;
                C117305Zk.A18(r3, this, i3, i2);
                break;
        }
        return r3.create();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0D.A03()) {
            C1309960u.A01(this);
        }
    }
}
