package com.whatsapp.payments.ui;

import X.AbstractC16870pt;
import X.AbstractC28491Nn;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass2S0;
import X.AnonymousClass61I;
import X.C117295Zj;
import X.C125805rq;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C14900mE;
import X.C252818u;
import X.C253118x;
import X.C50942Ry;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.PaymentIncentiveViewFragment;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class PaymentIncentiveViewFragment extends Hilt_PaymentIncentiveViewFragment {
    public AnonymousClass12P A00;
    public C14900mE A01;
    public C252818u A02;
    public AnonymousClass01d A03;
    public C14830m7 A04;
    public C125805rq A05;
    public C253118x A06;
    public final AbstractC16870pt A07;
    public final AnonymousClass2S0 A08;

    public PaymentIncentiveViewFragment(AbstractC16870pt r1, AnonymousClass2S0 r2) {
        this.A08 = r2;
        this.A07 = r1;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A05 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.payment_incentive_view_component);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass2S0 r2 = this.A08;
        C50942Ry r8 = r2.A01;
        AnonymousClass61I.A03(AnonymousClass61I.A00(this.A04, null, r2, null, true), this.A07, "incentive_details", "new_payment");
        if (r8 == null) {
            Log.e("PAY: PaymentIncentiveViewFragment/PaymentIncentiveOfferInfo is null or has null items in it");
            return;
        }
        TextView A0I = C12960it.A0I(view, R.id.payment_incentive_bottom_sheet_title);
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.payment_incentive_bottom_sheet_body);
        A0I.setText(r8.A0F);
        String str = r8.A0C;
        if (!TextUtils.isEmpty(str)) {
            C253118x r9 = this.A06;
            Context context = view.getContext();
            Object[] A1a = C12980iv.A1a();
            A1a[0] = r8.A0B;
            String[] strArr = new String[1];
            C117295Zj.A1A(this.A02, str, strArr, 0);
            AbstractC28491Nn.A05(A0T, this.A03, r9.A01(context, C12970iu.A0q(this, "learn-more", A1a, 1, R.string.incentives_learn_more_desc_text), new Runnable[]{new Runnable() { // from class: X.6Gv
                @Override // java.lang.Runnable
                public final void run() {
                    PaymentIncentiveViewFragment paymentIncentiveViewFragment = PaymentIncentiveViewFragment.this;
                    AnonymousClass61I.A01(AnonymousClass61I.A00(paymentIncentiveViewFragment.A04, null, paymentIncentiveViewFragment.A08, null, true), paymentIncentiveViewFragment.A07, 86, "incentive_details", null, 1);
                }
            }}, new String[]{"learn-more"}, strArr));
        } else {
            A0T.setText(r8.A0B);
        }
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.ok_button), this, 99);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.back), this, 100);
    }
}
