package com.whatsapp.payments.ui;

import X.AbstractActivityC119295dU;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass60Y;
import X.AnonymousClass61S;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C125655rb;
import X.C12960it;
import X.C12980iv;
import X.C130095yn;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class NoviClaimableTransactionListActivity extends NoviPayHubTransactionHistoryActivity {
    public ProgressDialog A00;
    public C125655rb A01;
    public boolean A02;

    public NoviClaimableTransactionListActivity() {
        this(0);
    }

    public NoviClaimableTransactionListActivity(int i) {
        this.A02 = false;
        C117295Zj.A0p(this, 80);
    }

    @Override // X.AbstractActivityC119295dU, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119295dU.A02(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            this.A01 = (C125655rb) A1M.ADF.get();
        }
    }

    public void A2g() {
        ((NoviPayHubTransactionHistoryActivity) this).A0B.clear();
        ((NoviPayHubTransactionHistoryActivity) this).A0C.set(true);
        this.A00.show();
        C130095yn r6 = ((NoviPayHubTransactionHistoryActivity) this).A07;
        AnonymousClass016 A0T = C12980iv.A0T();
        ArrayList A0l = C12960it.A0l();
        AnonymousClass61S.A03("action", "novi-get-claimable-transactions", A0l);
        if (!TextUtils.isEmpty(null)) {
            AnonymousClass61S.A03("before", null, A0l);
        }
        r6.A07.A0B(C117305Zk.A09(A0T, r6, 11), C117315Zl.A0B("account", A0l), "get", 3);
        C117295Zj.A0r(this, A0T, 75);
    }

    @Override // com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ((NoviPayHubTransactionHistoryActivity) this).A0B = C12960it.A0l();
        super.onCreate(bundle);
        ProgressDialog progressDialog = new ProgressDialog(this);
        this.A00 = progressDialog;
        progressDialog.setMessage(getString(R.string.novi_payment_fetch_transactions));
        A2g();
        C117295Zj.A0r(this, this.A01.A00, 74);
        AnonymousClass60Y.A02(((NoviPayHubTransactionHistoryActivity) this).A06, "NAVIGATION_START", "REPORT_TRANSACTION", "SELECT_TRANSACTION", "LIST");
    }

    @Override // com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass60Y.A02(((NoviPayHubTransactionHistoryActivity) this).A06, "NAVIGATION_END", "REPORT_TRANSACTION", "SELECT_TRANSACTION", "LIST");
    }
}
