package com.whatsapp.payments.ui;

import X.AbstractC001200n;
import X.AbstractC005102i;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.AnonymousClass00T;
import X.AnonymousClass00X;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02P;
import X.AnonymousClass05J;
import X.AnonymousClass05L;
import X.AnonymousClass06W;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.C117305Zk;
import X.C117315Zl;
import X.C118065bD;
import X.C118315bc;
import X.C126155sQ;
import X.C128355vy;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C14900mE;
import X.C15570nT;
import X.C17900ra;
import X.C21270x9;
import X.C25921Bi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.payments.ui.IndiaUpiMyQrFragment;
import com.whatsapp.payments.ui.widget.IndiaUpiDisplaySecureQrCodeView;

/* loaded from: classes4.dex */
public class IndiaUpiMyQrFragment extends Hilt_IndiaUpiMyQrFragment {
    public View A00;
    public ImageView A01;
    public ImageView A02;
    public TextView A03;
    public C14900mE A04;
    public C15570nT A05;
    public AnonymousClass130 A06;
    public AnonymousClass1J1 A07;
    public C21270x9 A08;
    public C14820m6 A09;
    public AnonymousClass018 A0A;
    public C17900ra A0B;
    public C128355vy A0C;
    public IndiaUpiDisplaySecureQrCodeView A0D;
    public C118065bD A0E;
    public C25921Bi A0F;
    public final AnonymousClass05L A0G = A06(new AnonymousClass05J() { // from class: X.65M
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            IndiaUpiMyQrFragment indiaUpiMyQrFragment = IndiaUpiMyQrFragment.this;
            if (((C06830Vg) obj).A00 == -1) {
                indiaUpiMyQrFragment.A19();
            }
        }
    }, new AnonymousClass06W());

    public static IndiaUpiMyQrFragment A00(String str) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("extra_account_holder_name", str);
        A0D.putInt("action_bar_title_res_id", 0);
        A0D.putBoolean("bottom_icon_visible", true);
        IndiaUpiMyQrFragment indiaUpiMyQrFragment = new IndiaUpiMyQrFragment();
        indiaUpiMyQrFragment.A0U(A0D);
        return indiaUpiMyQrFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        AbstractC005102i A1U;
        super.A0s();
        Bundle bundle = super.A05;
        ActivityC000900k A0C = A0C();
        if ((A0C instanceof ActivityC13790kL) && bundle != null && bundle.getInt("action_bar_title_res_id", 0) != 0 && (A1U = ((ActivityC000800j) A0C).A1U()) != null) {
            C117305Zk.A16(A1U, bundle.getInt("action_bar_title_res_id"));
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i == 1006) {
            this.A0D.A04(false);
        } else {
            super.A0t(i, i2, intent);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(0, R.id.menuitem_share_qr, 0, R.string.share).setIcon(AnonymousClass00X.A04(A01().getTheme(), A02(), R.drawable.ic_action_share)).setShowAsAction(1);
        if (Build.VERSION.SDK_INT >= 19) {
            menu.add(0, R.id.menuitem_print, 0, R.string.print_qr_code);
        }
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_share_qr) {
            if (AnonymousClass00T.A01(A01(), "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                int i = Build.VERSION.SDK_INT;
                int i2 = R.string.payment_permission_storage_need_write_access_v30;
                if (i < 30) {
                    i2 = R.string.payment_permission_storage_need_write_access;
                }
                this.A0G.A00(null, RequestPermissionActivity.A03(A01(), R.string.payment_permission_storage_need_write_access_request, i2, true));
            } else {
                A19();
                return true;
            }
        } else if (menuItem.getItemId() != R.id.menuitem_print) {
            return false;
        } else {
            if (Build.VERSION.SDK_INT >= 19) {
                A1A();
                return true;
            }
        }
        return true;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_payments_dispay_secure_qr_code);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A07.A00();
        this.A00 = null;
        this.A02 = null;
        this.A03 = null;
        this.A0D = null;
        this.A01 = null;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        String str;
        boolean z;
        this.A07 = this.A08.A04(view.getContext(), "india-upi-my-qr-fragment");
        this.A00 = AnonymousClass028.A0D(view, R.id.qrcode_view);
        this.A02 = C12970iu.A0K(view, R.id.contact_photo);
        this.A03 = C12960it.A0I(view, R.id.scan_to_pay_info);
        this.A0D = (IndiaUpiDisplaySecureQrCodeView) AnonymousClass028.A0D(view, R.id.display_qr_code_view);
        this.A01 = C12970iu.A0K(view, R.id.bottom_icon);
        Bundle bundle2 = super.A05;
        if (bundle2 != null) {
            str = bundle2.getString("extra_account_holder_name");
            z = bundle2.getBoolean("bottom_icon_visible", true);
        } else {
            str = null;
            z = true;
        }
        ImageView imageView = this.A01;
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        imageView.setVisibility(C12960it.A02(i));
        C118065bD r7 = (C118065bD) C117315Zl.A06(new C118315bc(this, this.A0C), this).A00(C118065bD.class);
        this.A0E = r7;
        IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(this, 44);
        IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(this, 43);
        AnonymousClass02P r0 = r7.A02;
        AbstractC001200n r1 = r7.A00;
        r0.A05(r1, A0B);
        r7.A01.A05(r1, A0B2);
        r7.A07(str);
        this.A0D.setup(this.A0E);
        A1B(true);
        CopyableTextView copyableTextView = (CopyableTextView) AnonymousClass028.A0D(view, R.id.user_wa_vpa);
        String str2 = this.A0E.A04().A0C;
        copyableTextView.A02 = str2;
        copyableTextView.setText(C12970iu.A0q(this, str2, new Object[1], 0, R.string.vpa_prefix));
        C12960it.A0I(view, R.id.user_account_name).setText(this.A0E.A04().A04);
        C12960it.A0I(view, R.id.user_wa_phone).setText(C117305Zk.A0h(this.A05));
        this.A03.setText(C12970iu.A0q(this, this.A0E.A04().A04, new Object[1], 0, R.string.scan_this_code_to_pay_user));
        this.A0E.A06(null, 0);
        A0M();
    }

    public void A19() {
        this.A0E.A06(new C126155sQ(C12970iu.A0p(this.A0D.A0F)), 4);
    }

    public void A1A() {
        if (this.A0D.A09 != null && A0B() != null && this.A00 != null) {
            A1B(false);
            this.A00.setDrawingCacheEnabled(true);
            this.A00.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            View view = this.A00;
            view.layout(0, 0, view.getMeasuredWidth(), this.A00.getMeasuredHeight());
            this.A00.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
            this.A00.buildDrawingCache(true);
            C25921Bi.A00(A0C(), Bitmap.createBitmap(this.A00.getDrawingCache()), this.A0E.A04().A04);
            this.A00.setDrawingCacheEnabled(false);
            A1B(true);
        }
    }

    public final void A1B(boolean z) {
        C15570nT r0 = this.A05;
        r0.A08();
        if (r0.A01 == null) {
            return;
        }
        if (z) {
            AnonymousClass1J1 r2 = this.A07;
            C15570nT r02 = this.A05;
            r02.A08();
            r2.A06(this.A02, r02.A01);
        } else if (C12970iu.A01(this.A09.A00, "privacy_profile_photo") != 0) {
            AnonymousClass130 r22 = this.A06;
            ImageView imageView = this.A02;
            C15570nT r03 = this.A05;
            r03.A08();
            r22.A06(imageView, r03.A01);
        }
    }
}
