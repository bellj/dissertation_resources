package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1ZS;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C248217a;
import android.content.Intent;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class IndiaUpiBankAccountAddedLandingActivity extends AbstractActivityC121665jA {
    public ImageView A00;
    public TextView A01;
    public TextView A02;
    public Button A03;
    public Button A04;
    public C248217a A05;
    public boolean A06;

    public IndiaUpiBankAccountAddedLandingActivity() {
        this(0);
    }

    public IndiaUpiBankAccountAddedLandingActivity(int i) {
        this.A06 = false;
        C117295Zj.A0p(this, 32);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            this.A05 = (C248217a) A1M.AE5.get();
        }
    }

    public void A30() {
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12980iv.A0k(), "registration_complete", null);
    }

    public final void A31() {
        if (((AbstractActivityC121685jC) this).A0E != null || !AnonymousClass1ZS.A03(((AbstractActivityC121665jA) this).A08)) {
            Intent A0D = C12990iw.A0D(this, IndiaUpiSendPaymentActivity.class);
            A2v(A0D);
            startActivity(A0D);
        } else {
            Log.e(C12960it.A0f(C12960it.A0k("openPaymentActivity, jid and vpa is null, payment entry type = "), ((AbstractActivityC121665jA) this).A02));
        }
        finish();
    }

    public final void A32(ImageView imageView) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        int applyDimension = (int) TypedValue.applyDimension(1, 64.0f, getResources().getDisplayMetrics());
        layoutParams.width = applyDimension;
        layoutParams.height = applyDimension;
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(R.drawable.ic_hero_bank_added);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "registration_complete", null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0223  */
    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
        // Method dump skipped, instructions count: 636
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiBankAccountAddedLandingActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            ((AbstractActivityC121665jA) this).A0D.AKg(C12960it.A0V(), C12970iu.A0h(), "registration_complete", null);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public final void setupIncentiveInfoContainer(View view) {
        if (((AbstractActivityC121665jA) this).A00 == 20) {
            view.setVisibility(0);
            C12960it.A0I(view, R.id.incentive_info_text).setText(R.string.incentives_bank_account_added_blurb);
            return;
        }
        view.setVisibility(8);
    }
}
