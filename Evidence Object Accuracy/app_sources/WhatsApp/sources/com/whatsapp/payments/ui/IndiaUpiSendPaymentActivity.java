package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121525iS;
import X.AbstractActivityC121545iU;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.AbstractC1310961f;
import X.AbstractC136516Mv;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC30791Yv;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass102;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1KS;
import X.AnonymousClass1Q5;
import X.AnonymousClass1ZS;
import X.AnonymousClass2FL;
import X.AnonymousClass2SP;
import X.AnonymousClass60V;
import X.AnonymousClass614;
import X.AnonymousClass61I;
import X.AnonymousClass692;
import X.AnonymousClass6AN;
import X.AnonymousClass6BE;
import X.AnonymousClass6D0;
import X.AnonymousClass6M9;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C118025b9;
import X.C119675er;
import X.C120485gG;
import X.C120495gH;
import X.C121265hX;
import X.C123705nj;
import X.C123715nk;
import X.C124045oM;
import X.C124235op;
import X.C124245oq;
import X.C126095sK;
import X.C126885tb;
import X.C127135u0;
import X.C127145u1;
import X.C127155u2;
import X.C127505ub;
import X.C127785v3;
import X.C128295vs;
import X.C128305vt;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C1329668y;
import X.C134006Cy;
import X.C134016Cz;
import X.C134066De;
import X.C134146Dm;
import X.C14850m9;
import X.C14900mE;
import X.C14920mG;
import X.C15570nT;
import X.C17070qD;
import X.C17220qS;
import X.C18610sj;
import X.C18650sn;
import X.C21860y6;
import X.C22460z7;
import X.C27131Gd;
import X.C30771Yt;
import X.C30821Yy;
import X.C36021jC;
import X.RunnableC135466Io;
import X.RunnableC135686Jk;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.math.BigDecimal;
import java.util.List;

/* loaded from: classes4.dex */
public class IndiaUpiSendPaymentActivity extends AbstractActivityC121525iS implements AnonymousClass6M9 {
    public int A00;
    public C124045oM A01;
    public C124245oq A02;
    public C123715nk A03;
    public AnonymousClass6D0 A04;
    public C14920mG A05;
    public BigDecimal A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public final C27131Gd A0A;
    public final AbstractC1310961f A0B;
    public final AbstractC136516Mv A0C;

    public IndiaUpiSendPaymentActivity() {
        this(0);
        this.A00 = 0;
        this.A08 = false;
        this.A09 = false;
        this.A0A = new C119675er(this);
        this.A0C = new C134016Cz(this);
        this.A0B = new C134006Cy(this);
    }

    public IndiaUpiSendPaymentActivity(int i) {
        this.A07 = false;
        C117295Zj.A0p(this, 73);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            AbstractActivityC119235dO.A1Z(A1M, this);
            ((AbstractActivityC121525iS) this).A0Y = AbstractActivityC119235dO.A0M(A09, A1M, this, AbstractActivityC119235dO.A0m(A1M, this));
            this.A05 = (C14920mG) A1M.ALr.get();
        }
    }

    public final void A3b() {
        int i;
        if (this.A08) {
            int i2 = this.A00;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 == 2) {
                        i = 36;
                    } else if (i2 == 3) {
                        i = 22;
                    } else if (i2 == 4) {
                        i = 35;
                    } else if (i2 == 5) {
                        this.A08 = false;
                        C36021jC.A00(this, 37);
                        PaymentView paymentView = ((AbstractActivityC121525iS) this).A0W;
                        if (paymentView != null) {
                            paymentView.A03();
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                    C36021jC.A00(this, 37);
                    C36021jC.A01(this, i);
                    this.A00 = 0;
                    return;
                }
                C36021jC.A01(this, 37);
            } else if (this.A01 == null) {
                C36021jC.A01(this, 37);
                C124045oM r1 = new C124045oM(this);
                this.A01 = r1;
                C12990iw.A1N(r1, ((ActivityC13830kP) this).A05);
            }
        }
    }

    public final void A3c() {
        int i;
        String str;
        int i2;
        PaymentView paymentView;
        PaymentView paymentView2 = ((AbstractActivityC121525iS) this).A0W;
        if (paymentView2 != null && !((AbstractActivityC121525iS) this).A0k) {
            if (((ActivityC13810kN) this).A00 == null) {
                setContentView(paymentView2);
            }
            A3N();
            if (!C117305Zk.A1U(((ActivityC13810kN) this).A0C) || ((ActivityC13810kN) this).A0C.A07(979)) {
                AnonymousClass61I.A03(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, ((AbstractActivityC121685jC) this).A0U, null, true), ((AbstractActivityC121665jA) this).A0D, "new_payment", ((AbstractActivityC121525iS) this).A0d);
            } else {
                C118025b9 A00 = ((AbstractActivityC121685jC) this).A0Y.A00(this);
                ((AbstractActivityC121685jC) this).A0X = A00;
                if (A00 != null) {
                    A00.A05.Ab2(new RunnableC135466Io(A00, false));
                    C117295Zj.A0r(this, ((AbstractActivityC121685jC) this).A0X.A00, 46);
                    C118025b9 r5 = ((AbstractActivityC121685jC) this).A0X;
                    r5.A05.Ab2(new RunnableC135686Jk(((AbstractActivityC121525iS) this).A0C, r5, C117295Zj.A03(((ActivityC13790kL) this).A05)));
                }
            }
            String str2 = ((AbstractActivityC121525iS) this).A0e;
            if (!(str2 == null || (paymentView = ((AbstractActivityC121525iS) this).A0W) == null)) {
                paymentView.A1G = str2;
            }
            List list = ((AbstractActivityC121525iS) this).A0g;
            if (list != null) {
                list.clear();
            }
            if (((AbstractActivityC121525iS) this).A0U != null || !AbstractActivityC119235dO.A1l(this)) {
                AaN();
            } else {
                C124235op r1 = new C124235op(this);
                ((AbstractActivityC121525iS) this).A0U = r1;
                C12960it.A1E(r1, ((ActivityC13830kP) this).A05);
            }
            if (((AbstractActivityC121685jC) this).A0F != null) {
                if (TextUtils.isEmpty(((AbstractActivityC121525iS) this).A0d)) {
                    ((AbstractActivityC121525iS) this).A0d = "chat";
                }
                i = 53;
                i2 = 1;
                str = "new_payment";
            } else {
                i = null;
                str = "enter_user_payment_id";
                i2 = 0;
            }
            ((AbstractActivityC121665jA) this).A0D.AKg(Integer.valueOf(i2), i, str, ((AbstractActivityC121525iS) this).A0d);
        }
    }

    public final void A3d() {
        if (this.A09) {
            A3c();
            if (AbstractActivityC119235dO.A1l(this) && this.A00 != 5) {
                C124045oM r1 = new C124045oM(this);
                this.A01 = r1;
                C12990iw.A1N(r1, ((ActivityC13830kP) this).A05);
                return;
            }
            return;
        }
        if (AnonymousClass1ZS.A02(((AbstractActivityC121665jA) this).A06)) {
            if (A3Y()) {
                String A00 = C1329668y.A00(((AbstractActivityC121665jA) this).A0B);
                if (A00 == null || !A00.equals(((AbstractActivityC121665jA) this).A08.A00)) {
                    A2C(R.string.payment_vpa_verify_in_progress);
                    ((AbstractActivityC121525iS) this).A0N.A00(((AbstractActivityC121665jA) this).A08, null, new AnonymousClass692(this, new Runnable() { // from class: X.6GE
                        @Override // java.lang.Runnable
                        public final void run() {
                            IndiaUpiSendPaymentActivity.this.A3c();
                        }
                    }));
                    return;
                }
                A3X(new AnonymousClass60V(R.string.payment_self_vpa_error_text), new Object[0]);
                return;
            } else if (((AbstractActivityC121525iS) this).A0C != null) {
                C124245oq r12 = new C124245oq(this);
                this.A02 = r12;
                C12990iw.A1N(r12, ((ActivityC13830kP) this).A05);
            } else {
                finish();
                return;
            }
        }
        A3c();
    }

    public final void A3e(String str, String str2) {
        AnonymousClass2SP A02 = ((AbstractActivityC121665jA) this).A0D.A02(4, 51, "new_payment", ((AbstractActivityC121525iS) this).A0d);
        A02.A0S = str;
        A02.A0T = str2;
        AbstractActivityC119235dO.A1b(A02, this);
    }

    @Override // X.AnonymousClass6M9
    public /* bridge */ /* synthetic */ Object AZb() {
        C30821Yy AEV;
        C134146Dm r18;
        AbstractC30791Yv A02 = ((AbstractActivityC121545iU) this).A02.A02("INR");
        C126885tb r2 = ((AbstractActivityC121525iS) this).A0T;
        if (r2.A00) {
            r2.A00 = false;
            if (TextUtils.isEmpty(((AbstractActivityC121685jC) this).A0h)) {
                ((AbstractActivityC121685jC) this).A0h = getString(R.string.settings_quick_tip_payment_note);
            }
            if (TextUtils.isEmpty(((AbstractActivityC121685jC) this).A0k)) {
                ((AbstractActivityC121685jC) this).A0k = A02.AEV().toString();
            }
        }
        if (!TextUtils.isEmpty(((AbstractActivityC121685jC) this).A0k)) {
            AEV = C117295Zj.A0D(A02, new BigDecimal(((AbstractActivityC121685jC) this).A0k));
        } else {
            AEV = A02.AEV();
        }
        C30821Yy A0D = C117295Zj.A0D(A02, new BigDecimal(((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A21)));
        if (!A3Y()) {
            C14850m9 r8 = ((ActivityC13810kN) this).A0C;
            AnonymousClass19M r7 = ((ActivityC13810kN) this).A0B;
            AnonymousClass01d r6 = ((ActivityC13810kN) this).A08;
            AnonymousClass018 r4 = ((AbstractActivityC121545iU) this).A01;
            AnonymousClass1AB r3 = ((AbstractActivityC121685jC) this).A0d;
            r18 = new C134146Dm(this, r6, r4, r7, r8, this.A03, ((AbstractActivityC121525iS) this).A0Z, r3);
        } else {
            r18 = null;
        }
        this.A04 = new AnonymousClass6D0(this, ((AbstractActivityC121545iU) this).A01, A02, ((AbstractActivityC121525iS) this).A0X.A00(((AbstractActivityC121685jC) this).A0j, ((AbstractActivityC121685jC) this).A0k, ((AbstractActivityC121685jC) this).A0i), AEV, A0D, null);
        AbstractC14640lm r1 = ((AbstractActivityC121685jC) this).A0E;
        String str = ((AbstractActivityC121685jC) this).A0h;
        AnonymousClass1KS r12 = ((AbstractActivityC121685jC) this).A0c;
        Integer num = ((AbstractActivityC121685jC) this).A0e;
        String str2 = ((AbstractActivityC121685jC) this).A0n;
        AbstractC1310961f r14 = this.A0B;
        C127155u2 r72 = new C127155u2(!this.A0s ? 1 : 0, getIntent().getIntExtra("extra_transfer_direction", 0));
        C126095sK r62 = new C126095sK(!AbstractActivityC119235dO.A1l(this));
        C127135u0 r32 = new C127135u0(NumberEntryKeyboard.A00(((AbstractActivityC121545iU) this).A01), this.A0q);
        AbstractC136516Mv r13 = this.A0C;
        String str3 = ((AbstractActivityC121685jC) this).A0l;
        String str4 = ((AbstractActivityC121685jC) this).A0i;
        String str5 = ((AbstractActivityC121685jC) this).A0k;
        C127505ub r82 = new C127505ub(A02, null, 0);
        Integer valueOf = Integer.valueOf((int) R.style.IndiaSendPaymentCurrencySymbolAmount);
        C128295vs r21 = new C128295vs(new Pair(valueOf, new int[]{0, 0, 0, 0}), new Pair(valueOf, new int[]{0, 0, 0, 0}), r82, this.A04, null, str3, str4, str5, R.style.IndiaSendPaymentAmountInput, false, false, false);
        C127145u1 r83 = new C127145u1(this, ((ActivityC13810kN) this).A0C.A07(811));
        C22460z7 r9 = ((AbstractActivityC121525iS) this).A0P;
        return new C128305vt(r1, r18, r14, r13, r21, new C127785v3(((AbstractActivityC121685jC) this).A0C, ((AbstractActivityC121525iS) this).A0O, r9, ((ActivityC13810kN) this).A0C.A07(629)), r32, r62, r83, r72, r12, num, str, str2, true);
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1015) {
            super.onActivityResult(i, i2, intent);
        } else {
            this.A0B.ATV();
        }
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((AbstractActivityC121545iU) this).A06.A03 = ((AbstractActivityC121545iU) this).A0B;
        if (bundle == null) {
            String A0N = AbstractActivityC119235dO.A0N(this);
            if (A0N == null) {
                A0N = ((AbstractActivityC121525iS) this).A0d;
            }
            ((AbstractActivityC121545iU) this).A0B.A01(A0N, 185472016);
            C121265hX r6 = ((AbstractActivityC121545iU) this).A0B;
            boolean z = !A3Y();
            AnonymousClass1Q5 A00 = r6.A00(123, "p2p_flow_tag");
            if (A00 != null) {
                A00.A07.AL1("wa_to_wa", A00.A06.A05, 123, z);
            }
        }
        ((AbstractActivityC121525iS) this).A02.A03(this.A0A);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            boolean z2 = this.A0s;
            int i = R.string.new_payment;
            if (z2) {
                i = R.string.payments_send_payment_text;
            }
            A1U.A0A(i);
            A1U.A0M(true);
            if (!this.A0s) {
                A1U.A07(0.0f);
            }
        }
        PaymentView paymentView = (PaymentView) LayoutInflater.from(this).inflate(R.layout.send_payment_screen, (ViewGroup) null, false);
        ((AbstractActivityC121525iS) this).A0W = paymentView;
        paymentView.A0C(this);
        this.A09 = getIntent().getBooleanExtra("verify-vpa-in-background", false);
        if (((ActivityC13810kN) this).A0C.A07(1933) && AnonymousClass614.A02(((AbstractActivityC121525iS) this).A0d)) {
            this.A06 = new BigDecimal(((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1z));
        }
        if (A3Y()) {
            C123705nj r3 = new C123705nj();
            this.A03 = r3;
            PaymentView paymentView2 = ((AbstractActivityC121525iS) this).A0W;
            if (paymentView2 != null) {
                paymentView2.A0E(r3, R.id.payment_bottom_button, R.id.payment_bottom_button_inflated);
                ((C134066De) this.A03).A00 = C117305Zk.A0A(((AbstractActivityC121525iS) this).A0W, 191);
            }
            ((AbstractActivityC121525iS) this).A0N = new C120485gG(this, ((ActivityC13810kN) this).A05, ((AbstractActivityC121545iU) this).A02, ((AbstractActivityC121685jC) this).A0H, ((AbstractActivityC121525iS) this).A0F, ((AbstractActivityC121665jA) this).A0A, ((AbstractActivityC121685jC) this).A0K, ((AbstractActivityC121685jC) this).A0M, ((AbstractActivityC121545iU) this).A0B, ((AbstractActivityC121545iU) this).A0C);
            return;
        }
        this.A03 = new C123715nk();
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        int i2;
        int i3;
        if (i == 29) {
            r2 = C12980iv.A0S(this);
            r2.A07(R.string.upi_check_balance_no_pin_set_title);
            r2.A06(R.string.upi_check_balance_no_pin_set_message);
            C117295Zj.A0q(r2, this, 61, R.string.learn_more);
            C117305Zk.A18(r2, this, 64, R.string.ok);
        } else if (i != 39) {
            switch (i) {
                case 35:
                    r2 = C12980iv.A0S(this);
                    r2.A07(R.string.verify_upi_id_failed_title);
                    r2.A06(R.string.verify_upi_id_failed_desc);
                    i2 = R.string.ok;
                    i3 = 62;
                    break;
                case 36:
                    r2 = C12980iv.A0S(this);
                    r2.A07(R.string.payments_upi_something_went_wrong);
                    r2.A06(R.string.payments_upi_no_internet_desc);
                    i2 = R.string.ok;
                    i3 = 59;
                    break;
                case 37:
                    ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage(getString(R.string.register_wait_message));
                    progressDialog.setCancelable(false);
                    progressDialog.setButton(-1, getString(R.string.cancel), new IDxCListenerShape10S0100000_3_I1(this, 63));
                    return progressDialog;
                default:
                    return super.onCreateDialog(i);
            }
            C117295Zj.A0q(r2, this, i3, i2);
            r2.A0B(true);
        } else {
            A3e("-10021", "MAX_AMOUNT_2K_ALERT");
            r2 = C12980iv.A0S(this);
            r2.A0A(C12960it.A0X(this, C30771Yt.A05.AAB(((AbstractActivityC121545iU) this).A01, this.A06, 0), new Object[1], 0, R.string.payments_qr_dialog_payment_from_non_verified_merchant_exceeded_limit));
            C117295Zj.A0q(r2, this, 60, R.string.ok);
            r2.A0B(false);
        }
        return r2.create();
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121545iU, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        ((AbstractActivityC121545iU) this).A0B.A04(123, 4);
        ((AbstractActivityC121525iS) this).A02.A04(this.A0A);
        C124245oq r0 = this.A02;
        if (r0 != null) {
            r0.A03(true);
        }
        C124045oM r02 = this.A01;
        if (r02 != null) {
            r02.A03(true);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        PaymentView paymentView = ((AbstractActivityC121525iS) this).A0W;
        if (paymentView != null) {
            paymentView.A04 = paymentView.A0w.AAa().getCurrentFocus();
        }
    }

    @Override // X.AbstractActivityC121525iS, X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (AbstractActivityC119235dO.A1l(this)) {
            if (!((AbstractActivityC121545iU) this).A06.A07.contains("upi-get-challenge") && ((AbstractActivityC121665jA) this).A0B.A05().A00 == null) {
                ((AbstractActivityC121525iS) this).A0m.A06("onResume getChallenge");
                A2C(R.string.register_wait_message);
                ((AbstractActivityC121545iU) this).A06.A03("upi-get-challenge");
                A36();
                return;
            } else if (TextUtils.isEmpty((CharSequence) ((AbstractActivityC121665jA) this).A0B.A04().A00)) {
                C14850m9 r7 = ((ActivityC13810kN) this).A0C;
                C14900mE r4 = ((ActivityC13810kN) this).A05;
                C15570nT r5 = ((ActivityC13790kL) this).A01;
                C17220qS r8 = ((AbstractActivityC121685jC) this).A0H;
                C17070qD r14 = ((AbstractActivityC121685jC) this).A0P;
                C21860y6 r10 = ((AbstractActivityC121685jC) this).A0I;
                C18610sj r13 = ((AbstractActivityC121685jC) this).A0M;
                AnonymousClass102 r6 = ((AbstractActivityC121545iU) this).A02;
                AnonymousClass6BE r15 = ((AbstractActivityC121665jA) this).A0D;
                C18650sn r11 = ((AbstractActivityC121685jC) this).A0K;
                new C120495gH(this, r4, r5, r6, r7, r8, ((AbstractActivityC121665jA) this).A0B, r10, r11, ((AbstractActivityC121545iU) this).A06, r13, r14, r15, ((AbstractActivityC121545iU) this).A0B).A01(new AnonymousClass6AN(this));
                return;
            }
        }
        A3A();
    }
}
