package com.whatsapp.payments.ui;

import X.AbstractC136306Ma;
import X.AbstractC28901Pl;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1311161i;
import X.C14900mE;
import X.C42971wC;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class IndiaUpiPinPrimerDialogFragment extends Hilt_IndiaUpiPinPrimerDialogFragment {
    public AnonymousClass12P A00;
    public C14900mE A01;
    public AnonymousClass01d A02;
    public AnonymousClass6BE A03;
    public AbstractC136306Ma A04;

    @Override // X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A04 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_pin_primer_bottom_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Bundle bundle2 = this.A05;
        if (bundle2 != null) {
            AbstractC28901Pl r7 = (AbstractC28901Pl) bundle2.getParcelable("extra_bank_account");
            if (!(r7 == null || r7.A08 == null)) {
                C12960it.A0I(view, R.id.desc).setText(C12990iw.A0o(A02(), C1311161i.A07(r7), new Object[1], 0, R.string.payments_upi_pin_primer_desc_bottom_sheet));
            }
            Context context = view.getContext();
            C14900mE r72 = this.A01;
            AnonymousClass12P r6 = this.A00;
            AnonymousClass01d r9 = this.A02;
            C42971wC.A08(context, Uri.parse("https://faq.whatsapp.com/general/payments/about-payments-data"), r6, r72, C12970iu.A0T(view, R.id.note), r9, C12970iu.A0q(this, "learn-more", new Object[1], 0, R.string.payments_upi_pin_primer_security_note), "learn-more");
        }
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.continue_button), this, 59);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.close), this, 60);
        this.A03.AKg(0, null, "setup_pin_prompt", null);
    }
}
