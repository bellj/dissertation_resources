package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.AbstractC16870pt;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1IR;
import X.AnonymousClass2FL;
import X.AnonymousClass3FW;
import X.AnonymousClass604;
import X.C117295Zj;
import X.C117305Zk;
import X.C117325Zm;
import X.C127915vG;
import X.C128315vu;
import X.C128345vx;
import X.C12960it;
import X.C12990iw;
import X.C130065yk;
import X.C31001Zq;
import android.content.Intent;
import android.view.MenuItem;

/* loaded from: classes4.dex */
public class BrazilPaymentTransactionDetailActivity extends PaymentTransactionDetailsListActivity {
    public AbstractC16870pt A00;
    public C130065yk A01;
    public C128345vx A02;
    public boolean A03;

    public BrazilPaymentTransactionDetailActivity() {
        this(0);
    }

    public BrazilPaymentTransactionDetailActivity(int i) {
        this.A03 = false;
        C117295Zj.A0p(this, 23);
    }

    @Override // X.AbstractActivityC121795k0, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A09(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            AbstractActivityC119265dR.A0A(A1M, this);
            ((PaymentTransactionDetailsListActivity) this).A0I = AbstractActivityC119265dR.A02(A09, A1M, this, A1M.AEw);
            this.A01 = (C130065yk) A1M.A1z.get();
            this.A02 = (C128345vx) A1M.A23.get();
            this.A00 = (AbstractC16870pt) A1M.A20.get();
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity
    public void A2f(C128315vu r5) {
        AnonymousClass1IR r2;
        int i = r5.A00;
        if (i != 0) {
            if (i != 10) {
                if (i != 501) {
                    switch (i) {
                        case 23:
                            A2h(r5, 124, "wa_p2m_receipt_report_transaction");
                            break;
                        case 24:
                            Intent A0D = C12990iw.A0D(this, BrazilPaymentSettingsActivity.class);
                            A0D.putExtra("referral_screen", "chat");
                            startActivity(A0D);
                            finish();
                            return;
                    }
                } else {
                    return;
                }
            }
            if (i == 22) {
                C127915vG r0 = ((PaymentTransactionDetailsListActivity) this).A0O.A06;
                if (r0 != null) {
                    r2 = r0.A01;
                } else {
                    r2 = r5.A05;
                }
                String str = null;
                if (r2 != null && AnonymousClass604.A00(r2)) {
                    str = r2.A03 == 200 ? "wa_smb_p2m_payment_details" : "wa_p2m_receipt_support";
                }
                A2h(r5, 39, str);
            } else {
                A2g(C12960it.A0V(), 39);
            }
        } else {
            A2g(0, null);
        }
        super.A2f(r5);
    }

    public final void A2h(C128315vu r9, Integer num, String str) {
        AnonymousClass1IR r2;
        AnonymousClass3FW A0S;
        C127915vG r0 = ((PaymentTransactionDetailsListActivity) this).A0O.A06;
        if (r0 != null) {
            r2 = r0.A01;
        } else {
            r2 = r9.A05;
        }
        if (r2 == null || !AnonymousClass604.A00(r2)) {
            A0S = C117305Zk.A0S();
        } else {
            A0S = C117305Zk.A0S();
            C117325Zm.A07(A0S);
            A0S.A01("transaction_id", r2.A0K);
            A0S.A01("transaction_status", C31001Zq.A05(r2.A03, r2.A02));
            A0S.A01("transaction_status_name", this.A0Q.A0J(r2));
        }
        A0S.A01("hc_entrypoint", str);
        A0S.A01("app_type", "consumer");
        this.A00.AKi(A0S, C12960it.A0V(), num, "payment_transaction_details", null);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        Integer A0V = C12960it.A0V();
        A2g(A0V, A0V);
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            Integer A0V = C12960it.A0V();
            A2g(A0V, A0V);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
