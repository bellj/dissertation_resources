package com.whatsapp.payments.ui.widget;

import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass1IR;
import X.AnonymousClass60O;
import X.AnonymousClass60R;
import X.AnonymousClass6BE;
import X.C117295Zj;
import X.C117305Zk;
import X.C117895av;
import X.C119835fB;
import X.C125125qj;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1310060v;
import X.C1329668y;
import X.C14900mE;
import X.C17070qD;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C243515e;
import X.C30821Yy;
import X.C30931Zj;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class MandateUpdateBottomSheetFragment extends Hilt_MandateUpdateBottomSheetFragment {
    public Button A00;
    public Button A01;
    public LinearLayout A02;
    public TextView A03;
    public C14900mE A04;
    public C18640sm A05;
    public AnonymousClass018 A06;
    public AnonymousClass1IR A07;
    public C1329668y A08;
    public C18650sn A09;
    public C243515e A0A;
    public C18610sj A0B;
    public C17070qD A0C;
    public AnonymousClass6BE A0D;
    public C117895av A0E;
    public C1310060v A0F;
    public C18590sh A0G;
    public AbstractC14440lR A0H;
    public final C30931Zj A0I = C30931Zj.A00("MandateUpdateBottomSheetFragment", "payment", "IN");

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.india_upi_mandate_update_bottom_sheet);
        this.A03 = C12960it.A0I(A0F, R.id.title);
        this.A02 = C117305Zk.A07(A0F, R.id.update_mandate_container);
        this.A00 = (Button) AnonymousClass028.A0D(A0F, R.id.positive_button);
        this.A01 = (Button) AnonymousClass028.A0D(A0F, R.id.negative_button);
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C30821Yy r2;
        this.A0D.AKh(C12980iv.A0i(), null, "approve_mandate_update_request_prompt", "payment_transaction_details", true);
        this.A0E = (C117895av) new AnonymousClass02A(A0C()).A00(C117895av.class);
        C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.close), this, 183);
        String A07 = this.A08.A07();
        if (!TextUtils.isEmpty(A07)) {
            C12970iu.A0K(view, R.id.psp_logo).setImageResource(C125125qj.A00(A07).A00);
        }
        this.A07 = (AnonymousClass1IR) A03().getParcelable("transaction");
        this.A02.setVisibility(0);
        C119835fB r4 = (C119835fB) this.A07.A0A;
        AnonymousClass60R r0 = r4.A0B;
        AnonymousClass009.A05(r0);
        AnonymousClass60O r1 = r0.A0C;
        boolean equals = r1.A09.equals("PENDING");
        TextView textView = this.A03;
        int i = R.string.upi_mandate_update_request_bottom_sheet_pay_title;
        if (equals) {
            i = R.string.upi_mandate_transaction_detail_processing_update_bottom_sheet_header;
        }
        textView.setText(i);
        long j = r1.A00;
        int i2 = (j > r4.A0B.A01 ? 1 : (j == r4.A0B.A01 ? 0 : -1));
        boolean z = false;
        int i3 = R.string.upi_mandate_bottom_row_item_valid_date;
        if (i2 != 0) {
            z = true;
            i3 = R.string.upi_mandate_bottom_row_item_new_valid_date;
        }
        String A0I = A0I(i3);
        String A03 = this.A0F.A03(j);
        LinearLayout linearLayout = this.A02;
        int i4 = R.color.secondary_text;
        if (z) {
            i4 = R.color.primary_text;
        }
        linearLayout.addView(A19(linearLayout, A0I, A03, i4, false));
        boolean equals2 = this.A07.A08.equals(r1.A00());
        int i5 = R.string.upi_mandate_bottom_row_item_new_amount;
        if (equals2) {
            i5 = R.string.upi_mandate_bottom_row_item_amount;
        }
        String A0I2 = A0I(i5);
        C1310060v r3 = this.A0F;
        if (r1.A00() != null) {
            r2 = r1.A00();
        } else {
            r2 = this.A07.A08;
        }
        String str = r1.A07;
        if (str == null) {
            str = r4.A0B.A0F;
        }
        String A04 = r3.A04(r2, str);
        LinearLayout linearLayout2 = this.A02;
        linearLayout2.addView(A19(linearLayout2, A0I2, A04, R.color.primary_text, true));
        if (!r1.A09.equals("INIT") || !r1.A08.equals("UNKNOWN")) {
            this.A00.setVisibility(8);
            this.A01.setVisibility(8);
            return;
        }
        C117295Zj.A0n(this.A00, this, 182);
        this.A01.setVisibility(0);
        C117295Zj.A0n(this.A01, this, 184);
    }

    public final View A19(LinearLayout linearLayout, CharSequence charSequence, CharSequence charSequence2, int i, boolean z) {
        View A0F = C12960it.A0F(LayoutInflater.from(A0B()), linearLayout, R.layout.india_upi_mandate_detail_row_item);
        TextView A0I = C12960it.A0I(A0F, R.id.left_text);
        TextView A0I2 = C12960it.A0I(A0F, R.id.right_text);
        A0I.setText(charSequence);
        A0I2.setText(charSequence2);
        if (z) {
            A0I.setTypeface(A0I.getTypeface(), 1);
            A0I2.setTypeface(A0I2.getTypeface(), 1);
        }
        C12960it.A0s(A0I.getContext(), A0I, i);
        C12960it.A0s(A0I2.getContext(), A0I2, i);
        return A0F;
    }
}
