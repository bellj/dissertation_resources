package com.whatsapp.payments.ui;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass14X;
import X.AnonymousClass1IR;
import X.AnonymousClass2FL;
import X.AnonymousClass60Y;
import X.AnonymousClass610;
import X.AnonymousClass61M;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C117695aS;
import X.C118105bH;
import X.C118355bg;
import X.C125645ra;
import X.C125655rb;
import X.C126025sD;
import X.C127495ua;
import X.C128205vj;
import X.C128375w0;
import X.C12960it;
import X.C12970iu;
import X.C129865yQ;
import X.C12990iw;
import X.C130305zC;
import X.C1310561a;
import X.C1317263w;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C17070qD;
import X.C452120p;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviCreateClaimActivity;
import com.whatsapp.payments.ui.widget.PayToolbar;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NoviCreateClaimActivity extends ActivityC13790kL {
    public View A00;
    public Button A01;
    public EditText A02;
    public ProgressBar A03;
    public ScrollView A04;
    public C15550nR A05;
    public C15610nY A06;
    public AnonymousClass1IR A07;
    public AnonymousClass61M A08;
    public C17070qD A09;
    public C129865yQ A0A;
    public C125655rb A0B;
    public AnonymousClass60Y A0C;
    public C118105bH A0D;
    public C128375w0 A0E;
    public C117695aS A0F;
    public PayToolbar A0G;
    public AnonymousClass14X A0H;
    public boolean A0I;

    public NoviCreateClaimActivity() {
        this(0);
    }

    public NoviCreateClaimActivity(int i) {
        this.A0I = false;
        C117295Zj.A0p(this, 81);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0I) {
            this.A0I = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0H = (AnonymousClass14X) A1M.AFM.get();
            this.A05 = C12960it.A0O(A1M);
            this.A06 = C12960it.A0P(A1M);
            this.A0B = (C125655rb) A1M.ADF.get();
            this.A0C = C117305Zk.A0W(A1M);
            this.A09 = C117305Zk.A0P(A1M);
            this.A08 = C117305Zk.A0N(A1M);
            this.A0E = C117315Zl.A0E(A1M);
        }
    }

    public void A2e(C127495ua r12) {
        int i = r12.A00;
        if (i == 0) {
            Intent A0D = C12990iw.A0D(this, NoviPayBloksActivity.class);
            HashMap A11 = C12970iu.A11();
            A11.put("novi_claim_id", r12.A01.getString("novi_claim_id"));
            A11.put("novi_claims_transaction_id", r12.A01.getString("novi_claims_transaction_id"));
            A11.put("novi_claims_receiver_label", r12.A01.getString("novi_claims_receiver_label"));
            A11.put("novi_claims_receiver_name", r12.A01.getString("novi_claims_receiver_name"));
            A11.put("novi_claims_amount", r12.A01.getString("novi_claims_amount"));
            A11.put("novi_claims_tramsaction_timestamp", r12.A01.getString("novi_claims_tramsaction_timestamp"));
            A11.put("novi_claims_claim_timestamp", r12.A01.getString("novi_claims_claim_timestamp"));
            A11.put("novi_claims_addotional_information", r12.A01.getString("novi_claims_addotional_information"));
            A0D.putExtra("screen_name", "novipay_p_received_claim");
            A0D.putExtra("screen_params", A11);
            C125655rb r2 = this.A0B;
            r2.A00.A0B(new C125645ra("COMPLETED"));
            startActivity(A0D);
            finish();
        } else if (i == 1) {
            AnonymousClass1IR r6 = (AnonymousClass1IR) r12.A01.getParcelable("transaction_info");
            if (r6 != null) {
                this.A07 = r6;
                C117695aS r0 = this.A0F;
                C14830m7 r4 = ((ActivityC13790kL) this).A05;
                AnonymousClass14X r9 = this.A0H;
                r0.A6W(new C128205vj(this.A05, this.A06, r4, ((ActivityC13830kP) this).A01, r6, this.A09, null, r9, true));
                return;
            }
            Log.e("PAY: NoviCreateClaimActivity/handleEvents PaymentTransactionInfo is null");
        } else if (i == 2) {
            this.A01.setVisibility(0);
            this.A03.setVisibility(8);
            C452120p r02 = r12.A02;
            if (r02 == null || r02.A00 != 542720003) {
                C125655rb r22 = this.A0B;
                r22.A00.A0B(new C125645ra("ERROR"));
                this.A0A.A02(r12.A02, null, new Runnable() { // from class: X.6GL
                    @Override // java.lang.Runnable
                    public final void run() {
                        NoviCreateClaimActivity.this.finish();
                    }
                });
                return;
            }
            C1310561a.A06(this, new C126025sD("loginScreen"));
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        this.A0C.A06(AnonymousClass610.A00(this, "BACK_CLICK", "ARROW").A00);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_claim);
        PayToolbar A0a = C117305Zk.A0a(this);
        this.A0G = A0a;
        C130305zC.A01(this, ((ActivityC13830kP) this).A01, A0a, "", R.drawable.ic_back, true);
        findViewById(R.id.toolbar_bottom_divider).setVisibility(0);
        C117695aS r0 = new C117695aS(this);
        this.A0F = r0;
        r0.A02(false);
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.create_claim_body);
        viewGroup.addView(this.A0F, viewGroup.indexOfChild(findViewById(R.id.create_claim_instructions)) + 1);
        this.A04 = (ScrollView) findViewById(R.id.payment_claims_scroll_view_layout);
        this.A02 = (EditText) findViewById(R.id.additional_information);
        this.A00 = findViewById(R.id.create_claim_button_container);
        this.A01 = (Button) findViewById(R.id.create_claim_button);
        this.A03 = (ProgressBar) findViewById(R.id.create_claim_button_progressbar);
        this.A03.getIndeterminateDrawable().setColorFilter(AnonymousClass00T.A00(this, R.color.white), PorterDuff.Mode.SRC_IN);
        this.A02.addTextChangedListener(new C1317263w(this));
        this.A02.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.64i
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                NoviCreateClaimActivity noviCreateClaimActivity = NoviCreateClaimActivity.this;
                if (z) {
                    noviCreateClaimActivity.A0C.A06(AnonymousClass610.A00(noviCreateClaimActivity, "ADD_DISPUTE_DETAILS_CLICK", "INPUT_BOX").A00);
                    ((ActivityC13810kN) noviCreateClaimActivity).A05.A0J(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001c: INVOKE  
                          (wrap: X.0mE : 0x0013: IGET  (r3v0 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.0kN) (r4v0 'noviCreateClaimActivity' com.whatsapp.payments.ui.NoviCreateClaimActivity)) X.0kN.A05 X.0mE)
                          (wrap: X.6GM : 0x0017: CONSTRUCTOR  (r2v0 X.6GM A[REMOVE]) = (r4v0 'noviCreateClaimActivity' com.whatsapp.payments.ui.NoviCreateClaimActivity) call: X.6GM.<init>(com.whatsapp.payments.ui.NoviCreateClaimActivity):void type: CONSTRUCTOR)
                          (400 long)
                         type: VIRTUAL call: X.0mE.A0J(java.lang.Runnable, long):void in method: X.64i.onFocusChange(android.view.View, boolean):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: CONSTRUCTOR  (r2v0 X.6GM A[REMOVE]) = (r4v0 'noviCreateClaimActivity' com.whatsapp.payments.ui.NoviCreateClaimActivity) call: X.6GM.<init>(com.whatsapp.payments.ui.NoviCreateClaimActivity):void type: CONSTRUCTOR in method: X.64i.onFocusChange(android.view.View, boolean):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6GM, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        com.whatsapp.payments.ui.NoviCreateClaimActivity r4 = com.whatsapp.payments.ui.NoviCreateClaimActivity.this
                        if (r7 == 0) goto L_0x001f
                        java.lang.String r1 = "ADD_DISPUTE_DETAILS_CLICK"
                        java.lang.String r0 = "INPUT_BOX"
                        X.610 r0 = X.AnonymousClass610.A00(r4, r1, r0)
                        X.60Y r1 = r4.A0C
                        X.5vz r0 = r0.A00
                        r1.A06(r0)
                        X.0mE r3 = r4.A05
                        X.6GM r2 = new X.6GM
                        r2.<init>(r4)
                        r0 = 400(0x190, double:1.976E-321)
                        r3.A0J(r2, r0)
                    L_0x001f:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.View$OnFocusChangeListenerC1318464i.onFocusChange(android.view.View, boolean):void");
                }
            });
            C117295Zj.A0n(this.A01, this, 80);
            C118105bH r2 = (C118105bH) C117315Zl.A06(new C118355bg(this.A0E, getIntent().getStringExtra("novi_claims_transaction_id")), this).A00(C118105bH.class);
            this.A0D = r2;
            r2.A07.Ab2(new Runnable() { // from class: X.6HP
                @Override // java.lang.Runnable
                public final void run() {
                    C118105bH r4 = C118105bH.this;
                    C17070qD r02 = r4.A04;
                    r02.A03();
                    r4.A00 = r02.A08.A0M(r4.A08);
                    C127495ua r3 = new C127495ua(1);
                    Bundle A0D = C12970iu.A0D();
                    A0D.putParcelable("transaction_info", r4.A00);
                    r3.A01 = A0D;
                    r4.A01.A0A(r3);
                }
            });
            C118105bH r22 = this.A0D;
            r22.A01.A05(this, C117305Zk.A0B(this, 76));
            this.A0A = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A08);
            this.A0C.A06(AnonymousClass610.A00(this, "NAVIGATION_START", "SCREEN").A00);
        }

        @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
        public void onDestroy() {
            super.onDestroy();
            this.A0C.A06(AnonymousClass610.A00(this, "NAVIGATION_END", "SCREEN").A00);
        }
    }
