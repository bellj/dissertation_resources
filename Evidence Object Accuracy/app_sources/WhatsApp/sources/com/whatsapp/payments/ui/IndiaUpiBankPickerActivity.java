package com.whatsapp.payments.ui;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121535iT;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass07G;
import X.AnonymousClass17V;
import X.AnonymousClass2FL;
import X.AnonymousClass2SP;
import X.AnonymousClass65N;
import X.AnonymousClass69E;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C118595c4;
import X.C118675cC;
import X.C122205l5;
import X.C124255or;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C18790t3;
import X.C30931Zj;
import X.C38721ob;
import X.C38771og;
import X.C42941w9;
import X.C48232Fc;
import X.C64513Fv;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public final class IndiaUpiBankPickerActivity extends AbstractActivityC121535iT {
    public View A00;
    public View A01;
    public View A02;
    public LinearLayout A03;
    public TextView A04;
    public RecyclerView A05;
    public RecyclerView A06;
    public C48232Fc A07;
    public C18790t3 A08;
    public C64513Fv A09;
    public AnonymousClass17V A0A;
    public C118595c4 A0B;
    public C118595c4 A0C;
    public C124255or A0D;
    public C38721ob A0E;
    public String A0F;
    public ArrayList A0G;
    public List A0H;
    public List A0I;
    public List A0J;
    public boolean A0K;
    public boolean A0L;
    public final AnonymousClass2SP A0M;
    public final C30931Zj A0N;

    public IndiaUpiBankPickerActivity() {
        this(0);
        this.A0N = C117315Zl.A0D("IndiaUpiBankPickerActivity");
        this.A0M = new AnonymousClass2SP();
    }

    public IndiaUpiBankPickerActivity(int i) {
        this.A0K = false;
        C117295Zj.A0p(this, 35);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0K) {
            this.A0K = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
            ((AbstractActivityC121535iT) this).A05 = (AnonymousClass69E) A1M.A9W.get();
            ((AbstractActivityC121535iT) this).A00 = C117305Zk.A0G(A1M);
            ((AbstractActivityC121535iT) this).A06 = (C122205l5) A1M.A9Z.get();
            this.A08 = (C18790t3) A1M.AJw.get();
            this.A0A = (AnonymousClass17V) A1M.AEX.get();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN
    public void A2A(int i) {
        RecyclerView recyclerView = this.A05;
        if (recyclerView != null) {
            recyclerView.setEnabled(true);
        }
        if (i != R.string.payments_bank_accounts_not_found) {
            super.A2A(i);
        }
    }

    public final void A34(Integer num) {
        AnonymousClass2SP r1 = this.A0M;
        r1.A0Z = "nav_bank_select";
        r1.A09 = C12960it.A0V();
        r1.A08 = num;
        AbstractActivityC119235dO.A1b(r1, this);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A07.A05()) {
            this.A07.A04(true);
            this.A0M.A0P = this.A0F;
            A34(1);
            return;
        }
        A34(1);
        A2s();
    }

    @Override // X.AbstractActivityC121535iT, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0d(this);
        File file = new File(getCacheDir(), "BankLogos");
        if (!file.mkdirs() && !file.isDirectory()) {
            this.A0N.A06("create unable to create bank logos cache directory");
        }
        this.A0E = new C38771og(((ActivityC13810kN) this).A05, this.A08, ((ActivityC13810kN) this).A0D, file, "india-upi-bank-picker-activity").A00();
        setContentView(R.layout.india_upi_payment_bank_picker);
        A2u(R.string.payments_bank_picker_activity_title, R.color.neutral_toolbar_title_text, R.id.data_layout);
        AnonymousClass018 r10 = ((ActivityC13830kP) this).A01;
        this.A07 = new C48232Fc(this, findViewById(R.id.search_holder), new AnonymousClass65N(this), C117305Zk.A08(this), r10);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0A(R.string.payments_bank_picker_activity_title);
        }
        this.A00 = findViewById(R.id.data_layout);
        this.A02 = findViewById(R.id.shimmer_layout);
        this.A04 = C12970iu.A0M(this, R.id.bank_picker_empty_tv);
        this.A01 = findViewById(R.id.popular_banks_group);
        this.A03 = (LinearLayout) findViewById(R.id.list_items_layout);
        C12970iu.A19(this, C12960it.A0J(findViewById(R.id.grid_view_title), R.id.header_text), R.string.payments_bank_picker_popular_banks_header);
        this.A06 = (RecyclerView) findViewById(R.id.grid_view);
        this.A05 = (RecyclerView) findViewById(R.id.bank_picker_list);
        this.A0C = new C118595c4(this, false);
        this.A0B = new C118595c4(this, true);
        this.A05.setAdapter(this.A0C);
        this.A06.setAdapter(this.A0B);
        A33(C12960it.A0l());
        C64513Fv r1 = ((AbstractActivityC121665jA) this).A0A.A04;
        this.A09 = r1;
        r1.A02("upi-bank-picker");
        ((AbstractActivityC121665jA) this).A0D.AeG();
        this.A0L = false;
        this.A05.A0n(new C118675cC(this));
        AnonymousClass2SP r2 = this.A0M;
        r2.A0Z = "nav_bank_select";
        r2.A09 = C12980iv.A0i();
        r2.A02 = Boolean.valueOf(((AbstractActivityC121685jC) this).A0I.A0E("add_bank"));
        r2.A03 = Boolean.valueOf(this.A0L);
        if (getIntent() != null) {
            r2.A0Y = AbstractActivityC119235dO.A0N(this);
        }
        AbstractActivityC119235dO.A1b(r2, this);
        ((AbstractActivityC121665jA) this).A0C.A09();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem add = menu.add(0, R.id.menuitem_search, 0, ((ActivityC13830kP) this).A01.A00.getResources().getString(R.string.search));
        add.setIcon(R.drawable.ic_action_search).setShowAsAction(9);
        AnonymousClass07G.A00(ColorStateList.valueOf(AnonymousClass00T.A00(this, R.color.ob_action_bar_icon)), add);
        A2w(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC121535iT, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C124255or r1 = this.A0D;
        if (r1 != null) {
            r1.A03(true);
            this.A0D = null;
        }
        this.A0E.A02.A02(false);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_help) {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.context_help_banks_screen);
            A2x(A0S, "nav_bank_select");
        } else if (itemId == R.id.menuitem_search) {
            onSearchRequested();
            return true;
        } else if (itemId == 16908332) {
            this.A0N.A04("action bar home");
            A34(1);
            A2s();
            return true;
        }
        return true;
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A0M.A04 = Boolean.TRUE;
        this.A07.A01();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int applyDimension = (int) TypedValue.applyDimension(1, 16.0f, displayMetrics);
        C42941w9.A07(this.A07.A02, ((ActivityC13830kP) this).A01, applyDimension, 0);
        int applyDimension2 = (int) TypedValue.applyDimension(1, 8.0f, displayMetrics);
        C42941w9.A07(this.A07.A06.findViewById(R.id.search_back), ((ActivityC13830kP) this).A01, applyDimension2, 0);
        C48232Fc r2 = this.A07;
        String string = getString(R.string.payments_bank_picker_search_query_hint);
        SearchView searchView = r2.A02;
        if (searchView != null) {
            searchView.setQueryHint(string);
        }
        C117295Zj.A0n(findViewById(R.id.search_back), this, 28);
        A34(65);
        return false;
    }
}
