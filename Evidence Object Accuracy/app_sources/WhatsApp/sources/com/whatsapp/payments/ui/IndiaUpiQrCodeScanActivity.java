package com.whatsapp.payments.ui;

import X.AbstractActivityC41101su;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass6DX;
import X.C117295Zj;
import X.C12970iu;
import X.C12990iw;
import X.C21280xA;
import X.C30931Zj;
import X.C35751ig;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.qrcode.WaQrScannerView;

/* loaded from: classes4.dex */
public final class IndiaUpiQrCodeScanActivity extends AbstractActivityC41101su {
    public boolean A00;
    public final C30931Zj A01;

    public IndiaUpiQrCodeScanActivity() {
        this(0);
        this.A01 = C30931Zj.A00("IndiaUpiQrCodeScanActivity", "payment", "IN");
    }

    public IndiaUpiQrCodeScanActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 69);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC41101su) this).A04 = (C21280xA) A1M.AMU.get();
            ((AbstractActivityC41101su) this).A02 = C12970iu.A0Y(A1M);
        }
    }

    @Override // X.AbstractActivityC41101su
    public void A2f() {
        Vibrator A0K = ((ActivityC13810kN) this).A08.A0K();
        if (A0K != null) {
            A0K.vibrate(75);
        }
        Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentLauncherActivity.class);
        A0D.putExtra("intent_source", true);
        A0D.setData(Uri.parse(((AbstractActivityC41101su) this).A05));
        startActivity(A0D);
        finish();
    }

    @Override // X.AbstractActivityC41101su
    public void A2h(C35751ig r6) {
        int[] iArr = {R.string.localized_app_name};
        r6.A02 = R.string.permission_cam_access_on_scan_payment_qr;
        r6.A0A = iArr;
        int[] iArr2 = {R.string.localized_app_name};
        r6.A03 = R.string.permission_cam_access_on_scan_payment_qr_perm_denied;
        r6.A08 = iArr2;
    }

    @Override // X.AbstractActivityC41101su, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        A1b(5);
        super.onCreate(bundle);
        getWindow().addFlags(128);
        setContentView(getLayoutInflater().inflate(R.layout.india_upi_qr_code_scanner, (ViewGroup) null, false));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.menuitem_scan_qr);
            A1U.A0M(true);
        }
        AbstractC005102i A1U2 = A1U();
        AnonymousClass009.A05(A1U2);
        A1U2.A0M(true);
        A1g(false);
        WaQrScannerView waQrScannerView = (WaQrScannerView) findViewById(R.id.qr_scanner_view);
        ((AbstractActivityC41101su) this).A03 = waQrScannerView;
        waQrScannerView.setQrScannerCallback(new AnonymousClass6DX(this));
        C12970iu.A1N(this, R.id.overlay, 0);
        A2e();
    }
}
