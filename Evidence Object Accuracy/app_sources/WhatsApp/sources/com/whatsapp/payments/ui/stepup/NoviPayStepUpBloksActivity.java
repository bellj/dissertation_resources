package com.whatsapp.payments.ui.stepup;

import X.AbstractActivityC119225dN;
import X.AbstractActivityC123635nW;
import X.AbstractC130285z6;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass60L;
import X.C117295Zj;
import X.C117305Zk;
import X.C121285hb;
import X.C121325hf;
import X.C121365hj;
import X.C121385hl;
import X.C125065qd;
import X.C127415uS;
import X.C128485wB;
import X.C129665y6;
import X.C129675y7;
import X.C129695y9;
import X.C12970iu;
import X.C129865yQ;
import X.C12990iw;
import X.C1308860i;
import X.C1316663q;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Stack;

/* loaded from: classes4.dex */
public class NoviPayStepUpBloksActivity extends AbstractActivityC123635nW {
    public int A00;
    public C128485wB A01;
    public C129865yQ A02;
    public C129695y9 A03;
    public C121325hf A04;
    public C129665y6 A05;
    public C1316663q A06;
    public AbstractC130285z6 A07;
    public C129675y7 A08;
    public Map A09;
    public boolean A0A;

    public NoviPayStepUpBloksActivity() {
        this(0);
    }

    public NoviPayStepUpBloksActivity(int i) {
        this.A0A = false;
        C117295Zj.A0p(this, 116);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119225dN.A0B(A09, A1M, this, AbstractActivityC119225dN.A0A(A1M, this, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this))));
            AbstractActivityC119225dN.A0K(A1M, this, AbstractActivityC119225dN.A09(A1M, this));
            AbstractActivityC119225dN.A0D(A1M, AbstractActivityC119225dN.A02(A1M, this), this);
            this.A08 = (C129675y7) A1M.AKB.get();
            this.A05 = A09.A0A();
            this.A01 = (C128485wB) A1M.A1h.get();
            this.A03 = (C129695y9) A1M.ADQ.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01f1, code lost:
        if (r19.equals("start_doc_upload_step_up") == false) goto L_0x0012;
     */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x039e  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0593  */
    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractC136496Mt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AZC(X.AnonymousClass3FE r18, java.lang.String r19, java.util.Map r20) {
        /*
        // Method dump skipped, instructions count: 1752
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.stepup.NoviPayStepUpBloksActivity.AZC(X.3FE, java.lang.String, java.util.Map):void");
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractC136496Mt
    public String AZE(String str, Map map) {
        C121385hl A00;
        AnonymousClass60L r0;
        String str2 = (String) map.remove("case");
        if (TextUtils.isEmpty(str2)) {
            return "";
        }
        switch (str2.hashCode()) {
            case 188123007:
                if (str2.equals("saved_dob")) {
                    if (!C117305Zk.A1T(this) || (A00 = this.A08.A00()) == null || (r0 = ((C121285hb) A00.A02.get(0)).A01) == null) {
                        return "";
                    }
                    return C1308860i.A00(r0.A02, r0.A01, r0.A00);
                }
                break;
            case 596166250:
                if (str2.equals("calculate_max_dob_date")) {
                    int parseInt = Integer.parseInt(str);
                    Calendar instance = Calendar.getInstance();
                    instance.add(1, -parseInt);
                    return String.valueOf(instance.getTimeInMillis());
                }
                break;
            case 862784673:
                if (str2.equals("try_another_method")) {
                    C1316663q r2 = this.A06;
                    r2.A00 = (r2.A00 + 1) % r2.A05.size();
                    startActivity(C12990iw.A0D(this, NoviPayStepUpBloksActivity.class));
                    return "";
                }
                break;
            case 1429442740:
                if (str2.equals("capture_video_selfie_step_up")) {
                    AbstractC130285z6 r02 = this.A07;
                    if (r02 == null || !(r02 instanceof C121365hj)) {
                        Log.e("Video Selfie step up challenge missing");
                        return "";
                    }
                    String A0t = C12970iu.A0t("disable_face_rec", map);
                    if (TextUtils.isEmpty(A0t)) {
                        A0t = "false";
                    }
                    C121365hj r03 = (C121365hj) this.A07;
                    String str3 = r03.A00;
                    ArrayList<String> arrayList = r03.A01;
                    C1316663q r3 = this.A06;
                    int i = this.A00;
                    Intent A0D = C12990iw.A0D(this, NoviCaptureVideoSelfieActivity.class);
                    AnonymousClass009.A05(str3);
                    A0D.putExtra("video_selfie_challenge_id", str3);
                    AnonymousClass009.A05(A0t);
                    A0D.putExtra("disable_face_rec", A0t);
                    AnonymousClass009.A05(arrayList);
                    A0D.putStringArrayListExtra("video_selfie_head_directions", arrayList);
                    AnonymousClass009.A05(r3);
                    A0D.putExtra("step_up", r3);
                    A0D.putExtra("step_up_origin_action", i);
                    startActivityForResult(A0D, 0);
                    return "";
                }
                break;
        }
        map.put("case", str2);
        return super.AZE(str, map);
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 0 && i2 == -1) {
            finish();
        }
    }

    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC119645em, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        if (C117305Zk.A1T(this) && !((AbstractActivityC123635nW) this).A0F) {
            if (!"novipay_p_dropdown_selector".equals(A2f())) {
                Stack stack = this.A08.A03.A00;
                if (!(stack.size() == 0 || stack.peek() == null)) {
                    stack.pop();
                    return;
                }
            } else {
                return;
            }
        }
        C127415uS.A00(this.A06, this.A08, "CANCELED", this.A00);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractActivityC123635nW, X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        C125065qd.A00(this);
        C129675y7 r0 = this.A08;
        C1316663q r1 = r0.A01;
        this.A06 = r1;
        this.A00 = r0.A00;
        if (!(r1 == null || r1.A05.size() == 0)) {
            String str3 = C117305Zk.A0Y(this.A06).A01;
            if (getIntent().getBooleanExtra("should_perform_alternative_step_up", false)) {
                str3 = C117305Zk.A0Y(this.A06).A00;
            }
            if (!"TEXT_INPUT".equals(str3) || !((ActivityC13810kN) this).A0C.A07(989)) {
                switch (str3.hashCode()) {
                    case -1852691096:
                        if (str3.equals("SELFIE")) {
                            str = "novipay_p_capture_video_selfie_education";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    case -1504126555:
                        if (str3.equals("DOCUMENT_UPLOAD")) {
                            str = "novipay_p_doc_upload_intro";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    case -1362600187:
                        if (str3.equals("SMS_OTP")) {
                            str = "novipay_p_verify_phone_intro";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    case 74901:
                        if (str3.equals("KYC")) {
                            str = "novipay_p_identity_content";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    case 77859202:
                        if (str3.equals("REKYC")) {
                            str = "novipay_p_rekyc_intro";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    case 1793934804:
                        if (str3.equals("PASSWORD_CHANGE")) {
                            str = "novipay_p_reset_password";
                            break;
                        }
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                    default:
                        str = null;
                        C127415uS.A00(this.A06, this.A08, "FAIL", this.A00);
                        finish();
                        break;
                }
                Map map = (Map) getIntent().getSerializableExtra("screen_params");
                if (map == null) {
                    map = C12970iu.A11();
                }
                if (this.A06.A05.size() > 1) {
                    str2 = "1";
                } else {
                    str2 = "0";
                }
                map.put("multiple_step_up_choices", str2);
                if (this.A00 == 10) {
                    map.put("logging_disabled", "true");
                }
                map.put("step_up_entry_point", this.A06.A03);
                if (((ActivityC13810kN) this).A0C.A07(1328)) {
                    map.put("step_up_origin_action", Integer.toString(this.A00));
                }
                map.put("step_up_action_id", this.A06.A02);
                getIntent().putExtra("screen_params", (Serializable) map);
                getIntent().putExtra("screen_name", str);
                A2i();
                this.A02 = new C129865yQ(((ActivityC13790kL) this).A00, this, ((AbstractActivityC123635nW) this).A03);
                C117295Zj.A0r(this, ((AbstractActivityC123635nW) this).A08.A0G, 130);
                return;
            }
            C1316663q r5 = this.A06;
            int i = this.A00;
            String stringExtra = getIntent().getStringExtra("acct_restriction_type");
            Intent A0D = C12990iw.A0D(this, NoviTextInputStepUpActivity.class);
            AnonymousClass009.A05(r5);
            A0D.putExtra("step_up", r5);
            A0D.putExtra("step_up_origin_action", i);
            A0D.putExtra("acct_restriction_type", stringExtra);
            startActivity(A0D);
        }
        finish();
    }
}
