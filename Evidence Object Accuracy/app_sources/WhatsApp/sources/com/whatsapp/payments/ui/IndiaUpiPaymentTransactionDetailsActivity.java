package com.whatsapp.payments.ui;

import X.AbstractActivityC119265dR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2SP;
import X.AnonymousClass6BE;
import X.AnonymousClass6LU;
import X.C004802e;
import X.C117295Zj;
import X.C117305Zk;
import X.C122295lE;
import X.C122345lJ;
import X.C122485lX;
import X.C122505lZ;
import X.C122635lm;
import X.C123575nN;
import X.C126075sI;
import X.C128355vy;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C25911Bh;
import X.C30931Zj;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity;

/* loaded from: classes4.dex */
public class IndiaUpiPaymentTransactionDetailsActivity extends PaymentTransactionDetailsListActivity implements AnonymousClass6LU {
    public AnonymousClass6BE A00;
    public C123575nN A01;
    public C128355vy A02;
    public C25911Bh A03;
    public boolean A04;
    public final AnonymousClass2SP A05;
    public final C30931Zj A06;

    public IndiaUpiPaymentTransactionDetailsActivity() {
        this(0);
        this.A06 = C117295Zj.A0G("IndiaUpiPaymentTransactionDetailsActivity");
        this.A05 = new AnonymousClass2SP();
    }

    public IndiaUpiPaymentTransactionDetailsActivity(int i) {
        this.A04 = false;
        C117295Zj.A0p(this, 57);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k
    public void A1T(AnonymousClass01E r2) {
        super.A1T(r2);
        if (r2 instanceof ProgressDialogFragment) {
            ((ProgressDialogFragment) r2).A00 = new DialogInterface.OnKeyListener() { // from class: X.630
                @Override // android.content.DialogInterface.OnKeyListener
                public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    IndiaUpiPaymentTransactionDetailsActivity indiaUpiPaymentTransactionDetailsActivity = IndiaUpiPaymentTransactionDetailsActivity.this;
                    if (i != 4) {
                        return false;
                    }
                    dialogInterface.dismiss();
                    indiaUpiPaymentTransactionDetailsActivity.finish();
                    return true;
                }
            };
        }
    }

    @Override // X.AbstractActivityC121795k0, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119265dR.A09(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
            AbstractActivityC119265dR.A0A(A1M, this);
            ((PaymentTransactionDetailsListActivity) this).A0I = AbstractActivityC119265dR.A02(A09, A1M, this, A1M.AEw);
            this.A03 = (C25911Bh) A1M.A77.get();
            this.A00 = C117305Zk.A0T(A1M);
            this.A02 = (C128355vy) A1M.A9f.get();
        }
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        switch (i) {
            case 1000:
                return new C122485lX(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_payment_detail_footer_banner));
            case 1001:
                View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_localization_failed);
                AnonymousClass2GE.A07(C12970iu.A0L(A0F, R.id.payment_empty_icon), C12960it.A09(viewGroup).getColor(R.color.icon_color_disabled));
                return new C122505lZ(A0F);
            case 1002:
            case 1003:
            default:
                return super.A2e(viewGroup, i);
            case 1004:
                return new C122635lm(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_payment_amount_header_view_component));
            case 1005:
                return new C122345lJ(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_transaction_detail_banner));
            case 1006:
                return new C122295lE(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.india_upi_mandate_detail_see_all_txn_row_item));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0074  */
    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2f(X.C128315vu r12) {
        /*
        // Method dump skipped, instructions count: 510
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity.A2f(X.5vu):void");
    }

    public final void A2h() {
        this.A00.AKg(C12960it.A0V(), 138, "payment_transaction_details", null);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        Integer A0V = C12960it.A0V();
        A2g(A0V, A0V);
        this.A01.A0E(new C126075sI(301));
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 100) {
            return super.onCreateDialog(i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A06(R.string.payments_request_status_requested_expired);
        A0S.A0B(false);
        C117295Zj.A0q(A0S, this, 46, R.string.ok);
        A0S.A07(R.string.payments_request_status_request_expired);
        return A0S.create();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        C123575nN r2 = this.A01;
        if (r2 != null) {
            r2.A01 = intent.getBooleanExtra("extra_return_after_completion", false);
        }
        super.onNewIntent(intent);
    }

    @Override // com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        if (C12990iw.A0H(this) != null) {
            bundle.putAll(C12990iw.A0H(this));
        }
        super.onSaveInstanceState(bundle);
    }
}
