package com.whatsapp.payments.ui.widget;

import X.AbstractC119185d3;
import X.C117315Zl;
import X.C125315r3;
import X.C125375r9;
import X.C12960it;
import X.C12970iu;
import X.EnumC124495pd;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes4.dex */
public class PayToolbar extends AbstractC119185d3 {
    public View A00;
    public FrameLayout A01;
    public TextView A02;
    public WaImageView A03;
    public EnumC124495pd A04;

    public PayToolbar(Context context) {
        super(context);
        C12960it.A0E(this).inflate(R.layout.pay_header, (ViewGroup) this, true);
    }

    public PayToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        EnumC124495pd r1;
        C12960it.A0E(this).inflate(R.layout.pay_header, (ViewGroup) this, true);
        if (attributeSet != null && !isInEditMode()) {
            int i = 0;
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, C125375r9.A01, 0, 0);
            int resourceId = obtainStyledAttributes.getResourceId(2, 0);
            if (resourceId != 0) {
                setContentDescription(context.getString(resourceId));
            }
            findViewById(R.id.back).setVisibility(!obtainStyledAttributes.getBoolean(0, false) ? 8 : i);
            int i2 = obtainStyledAttributes.getInt(3, -1);
            if (i2 >= 0) {
                EnumC124495pd[] values = EnumC124495pd.values();
                int length = values.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        r1 = null;
                        break;
                    }
                    r1 = values[i3];
                    if (r1.ordinal() == i2) {
                        break;
                    }
                    i3++;
                }
                this.A04 = r1;
            }
            this.A02 = C12960it.A0J(this, R.id.title);
            this.A01 = (FrameLayout) findViewById(R.id.title_layout);
            this.A03 = (WaImageView) findViewById(R.id.fbpay_logo);
            this.A00 = findViewById(R.id.lock);
            setLockIconVisibility(obtainStyledAttributes.getBoolean(1, true));
        }
    }

    public PayToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C12960it.A0E(this).inflate(R.layout.pay_header, (ViewGroup) this, true);
    }

    @Override // com.whatsapp.BidiToolbar, androidx.appcompat.widget.Toolbar, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        super.onLayout(z, i, i2, i3, i4);
        FrameLayout frameLayout = this.A01;
        if (frameLayout != null && frameLayout.getVisibility() != 8 && this.A04 != null) {
            int width = getWidth();
            int height = getHeight();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int paddingTop = getPaddingTop();
            int paddingBottom = getPaddingBottom();
            int currentContentInsetLeft = getCurrentContentInsetLeft();
            int currentContentInsetRight = getCurrentContentInsetRight();
            int i9 = width - paddingRight;
            int i10 = height - paddingBottom;
            FrameLayout frameLayout2 = this.A01;
            if (frameLayout2 == null) {
                i6 = -1;
            } else {
                int measuredWidth = frameLayout2.getMeasuredWidth();
                int i11 = 0;
                if (frameLayout2.getLayoutParams() != null) {
                    ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(frameLayout2);
                    i11 = A0H.leftMargin;
                    i5 = A0H.rightMargin;
                } else {
                    i5 = 0;
                }
                i6 = measuredWidth + i11 + i5;
            }
            FrameLayout frameLayout3 = this.A01;
            if (frameLayout3 == null) {
                i8 = -1;
            } else {
                int measuredHeight = frameLayout3.getMeasuredHeight();
                int i12 = 0;
                if (frameLayout3.getLayoutParams() != null) {
                    ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(frameLayout3);
                    i12 = A0H2.topMargin;
                    i7 = A0H2.bottomMargin;
                } else {
                    i7 = 0;
                }
                i8 = measuredHeight + i12 + i7;
            }
            if (C117315Zl.A01(this.A04, C125315r3.A00) == 1) {
                int i13 = height >> 1;
                int i14 = i8 >> 1;
                int i15 = paddingLeft - currentContentInsetLeft;
                paddingLeft = Math.max(i15, ((width >> 1) + i15) - (i6 >> 1));
                i9 = Math.min(i9 - currentContentInsetRight, i6 + paddingLeft);
                paddingTop = Math.max(paddingTop, i13 - i14);
                i10 = Math.min(i10, i13 + i14);
            }
            this.A01.layout(paddingLeft, paddingTop, i9, i10);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        if (r4 == false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        if (r4 == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (X.C42941w9.A01 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setLockIconVisibility(boolean r6) {
        /*
            r5 = this;
            android.view.View r1 = r5.A00
            r3 = 0
            int r0 = X.C12960it.A02(r6)
            r1.setVisibility(r0)
            X.018 r0 = r5.A00
            boolean r0 = X.C28141Kv.A01(r0)
            if (r0 != 0) goto L_0x0017
            boolean r0 = X.C42941w9.A01
            r4 = 0
            if (r0 != 0) goto L_0x0018
        L_0x0017:
            r4 = 1
        L_0x0018:
            android.view.View r0 = r5.A00
            android.view.ViewGroup$LayoutParams r2 = r0.getLayoutParams()
            android.widget.FrameLayout$LayoutParams r2 = (android.widget.FrameLayout.LayoutParams) r2
            r0 = 3
            if (r4 == 0) goto L_0x0024
            r0 = 5
        L_0x0024:
            r0 = r0 | 16
            r2.gravity = r0
            android.content.res.Resources r1 = X.C12960it.A09(r5)
            r0 = 2131166018(0x7f070342, float:1.794627E38)
            int r1 = r1.getDimensionPixelOffset(r0)
            r0 = 0
            if (r4 != 0) goto L_0x0038
            r0 = r1
            r1 = 0
        L_0x0038:
            r2.setMargins(r0, r3, r1, r3)
            android.widget.FrameLayout r0 = r5.A01
            android.view.ViewGroup$MarginLayoutParams r2 = X.C12970iu.A0H(r0)
            android.content.res.Resources r1 = X.C12960it.A09(r5)
            r0 = 2131166019(0x7f070343, float:1.7946272E38)
            int r1 = r1.getDimensionPixelOffset(r0)
            if (r6 == 0) goto L_0x0051
            r0 = r1
            if (r4 != 0) goto L_0x0056
        L_0x0051:
            r0 = 0
            if (r6 == 0) goto L_0x0056
            if (r4 == 0) goto L_0x0057
        L_0x0056:
            r1 = 0
        L_0x0057:
            r2.setMargins(r1, r3, r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.ui.widget.PayToolbar.setLockIconVisibility(boolean):void");
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setLogo(int i) {
        this.A02.setVisibility(8);
        if (i != R.id.fbpay_logo) {
            this.A03.setVisibility(0);
        }
        this.A03.setImageResource(i);
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setLogo(Drawable drawable) {
        this.A02.setVisibility(8);
        this.A03.setVisibility(0);
        this.A03.setImageDrawable(drawable);
    }

    public void setOnLockClicked(View.OnClickListener onClickListener) {
    }

    @Override // com.whatsapp.util.MarqueeToolbar, androidx.appcompat.widget.Toolbar
    public void setTitle(CharSequence charSequence) {
        TextView textView;
        int i = 8;
        if (charSequence == null || !getContext().getString(R.string.facebook_pay).equalsIgnoreCase(charSequence.toString())) {
            this.A03.setVisibility(8);
            this.A02.setText(charSequence);
            textView = this.A02;
            i = 0;
        } else {
            textView = this.A02;
        }
        textView.setVisibility(i);
    }
}
