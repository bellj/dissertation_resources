package com.whatsapp.payments;

import X.AbstractC001200n;
import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.C117315Zl;
import X.C125185qq;
import X.C12980iv;
import X.C14580lf;
import X.C17070qD;
import X.C18600si;
import X.C21860y6;
import android.database.Cursor;
import com.whatsapp.payments.CheckFirstTransaction;

/* loaded from: classes4.dex */
public class CheckFirstTransaction implements AnonymousClass054 {
    public final C14580lf A00 = new C14580lf();
    public final C21860y6 A01;
    public final C18600si A02;
    public final C17070qD A03;
    public final AbstractC14440lR A04;

    public CheckFirstTransaction(C21860y6 r2, C18600si r3, C17070qD r4, AbstractC14440lR r5) {
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r4, AbstractC001200n r5) {
        Boolean valueOf;
        C14580lf r1;
        Boolean bool;
        int A01 = C117315Zl.A01(r4, C125185qq.A00);
        if (A01 == 1) {
            if (!this.A01.A0A()) {
                r1 = this.A00;
                bool = Boolean.TRUE;
            } else {
                C18600si r2 = this.A02;
                if (!r2.A01().contains("payment_is_first_send") || (valueOf = Boolean.valueOf(C12980iv.A1W(r2.A01(), "payment_is_first_send"))) == null || valueOf.booleanValue()) {
                    this.A04.Ab2(new Runnable() { // from class: X.6FP
                        @Override // java.lang.Runnable
                        public final void run() {
                            int i;
                            String str;
                            CheckFirstTransaction checkFirstTransaction = CheckFirstTransaction.this;
                            C14580lf r42 = checkFirstTransaction.A00;
                            C17070qD r0 = checkFirstTransaction.A03;
                            r0.A03();
                            C20370ve r9 = r0.A08;
                            if (r9.A0g()) {
                                i = 2;
                                str = "SELECT COUNT(*) FROM pay_transaction";
                            } else {
                                i = 1;
                                str = "SELECT COUNT(*) FROM pay_transactions";
                            }
                            long j = 0;
                            C16310on A012 = r9.A04.get();
                            try {
                                Cursor A09 = A012.A03.A09(str, null);
                                if (A09 != null) {
                                    if (A09.moveToNext()) {
                                        j = A09.getLong(0);
                                    } else {
                                        C30931Zj r22 = r9.A09;
                                        StringBuilder A0j = C12960it.A0j("PaymentTransactionStore/countAllTransactions/version=");
                                        A0j.append(i);
                                        r22.A06(C12960it.A0d("/db no message", A0j));
                                    }
                                    A09.close();
                                } else {
                                    C30931Zj r23 = r9.A09;
                                    StringBuilder A0j2 = C12960it.A0j("PaymentTransactionStore/countAllTransactions/version=");
                                    A0j2.append(i);
                                    r23.A06(C12960it.A0d("/db no cursor ", A0j2));
                                }
                                A012.close();
                                boolean z = false;
                                if (j <= 0) {
                                    z = true;
                                }
                                r42.A02(Boolean.valueOf(z));
                            } catch (Throwable th) {
                                try {
                                    A012.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                    });
                    this.A00.A00(new AbstractC14590lg() { // from class: X.6EE
                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            C18600si r0 = C18600si.this;
                            C12960it.A0t(C117295Zj.A05(r0), "payment_is_first_send", C12970iu.A1Y(obj));
                        }
                    });
                }
                r1 = this.A00;
                bool = Boolean.FALSE;
            }
            r1.A02(bool);
            this.A00.A00(new AbstractC14590lg() { // from class: X.6EE
                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    C18600si r0 = C18600si.this;
                    C12960it.A0t(C117295Zj.A05(r0), "payment_is_first_send", C12970iu.A1Y(obj));
                }
            });
        } else if (A01 == 2) {
            this.A00.A04();
        }
    }
}
