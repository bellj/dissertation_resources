package com.whatsapp.payments;

import X.AbstractC16830pp;
import X.AbstractC16840pq;
import X.AbstractC248317b;
import X.AbstractC30781Yu;
import X.AbstractC30791Yv;
import X.AbstractC38041nQ;
import X.AnonymousClass01N;
import X.AnonymousClass6BG;
import X.C125615rX;
import X.C12960it;
import X.C12970iu;
import X.C17900ra;
import X.C17930rd;
import X.C22710zW;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* loaded from: classes4.dex */
public class PaymentConfiguration implements AbstractC248317b {
    public AbstractC38041nQ A00;
    public final C125615rX A01;
    public final C17900ra A02;
    public final C22710zW A03;

    public PaymentConfiguration(C125615rX r1, C17900ra r2, C22710zW r3) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        if (r8.equals("US") == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
        if (r8.equals("IN") == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007a, code lost:
        if (r8.equals("GT") == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0081, code lost:
        if (r8.equals("BR") == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ac, code lost:
        if (r9.equals("INR") == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b5, code lost:
        if (r9.equals("BRL") == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c6, code lost:
        r0 = r2.get("IN");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cb, code lost:
        r0 = r2.get("BR");
     */
    @Override // X.AbstractC248317b
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC16830pp AFY(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            X.5rX r1 = r7.A01
            if (r8 == 0) goto L_0x000a
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r8 = r8.toUpperCase(r0)
        L_0x000a:
            java.util.Map r2 = r1.A00
            boolean r1 = r2.containsKey(r8)
            r0 = 0
            if (r1 != 0) goto L_0x0024
            java.lang.StringBuilder r2 = X.C12960it.A0h()
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentFactory/unmapped service for country code="
        L_0x0019:
            r2.append(r1)
            java.lang.String r1 = X.C12960it.A0d(r8, r2)
        L_0x0020:
            com.whatsapp.util.Log.e(r1)
            return r0
        L_0x0024:
            if (r9 == 0) goto L_0x002c
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r9 = r9.toUpperCase(r1)
        L_0x002c:
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            java.lang.String r6 = "IN"
            java.lang.String r5 = "BR"
            java.lang.String r3 = "US"
            java.lang.String r4 = "GT"
            if (r1 != 0) goto L_0x0046
            int r1 = r9.hashCode()
            switch(r1) {
                case 66044: goto L_0x00af;
                case 72653: goto L_0x00a6;
                case 2614186: goto L_0x0084;
                default: goto L_0x0041;
            }
        L_0x0041:
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentService/currency set/unmapped service for currency"
            com.whatsapp.util.Log.e(r1)
        L_0x0046:
            boolean r1 = android.text.TextUtils.isEmpty(r8)
            if (r1 != 0) goto L_0x0065
            int r1 = r8.hashCode()
            switch(r1) {
                case 2128: goto L_0x007d;
                case 2285: goto L_0x0076;
                case 2341: goto L_0x006f;
                case 2718: goto L_0x0068;
                default: goto L_0x0053;
            }
        L_0x0053:
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentService/country="
            java.lang.StringBuilder r2 = X.C12960it.A0k(r1)
            r2.append(r8)
            java.lang.String r1 = "/unmapped service"
            java.lang.String r1 = X.C12960it.A0d(r1, r2)
            com.whatsapp.util.Log.e(r1)
        L_0x0065:
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentService/unmapped service for country and currency"
            goto L_0x0020
        L_0x0068:
            boolean r1 = r8.equals(r3)
            if (r1 != 0) goto L_0x00bc
            goto L_0x0053
        L_0x006f:
            boolean r1 = r8.equals(r6)
            if (r1 != 0) goto L_0x00c6
            goto L_0x0053
        L_0x0076:
            boolean r1 = r8.equals(r4)
            if (r1 != 0) goto L_0x00c1
            goto L_0x0053
        L_0x007d:
            boolean r1 = r8.equals(r5)
            if (r1 != 0) goto L_0x00cb
            goto L_0x0053
        L_0x0084:
            java.lang.String r1 = "USDP"
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x0041
            boolean r1 = android.text.TextUtils.isEmpty(r8)
            if (r1 != 0) goto L_0x00b8
            boolean r1 = r8.equals(r4)
            if (r1 != 0) goto L_0x00c1
            boolean r1 = r8.equals(r3)
            if (r1 != 0) goto L_0x00bc
            java.lang.StringBuilder r2 = X.C12960it.A0h()
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentService/currency set/unmapped service for country="
            goto L_0x0019
        L_0x00a6:
            java.lang.String r1 = "INR"
            boolean r1 = r9.equals(r1)
            if (r1 != 0) goto L_0x00c6
            goto L_0x0041
        L_0x00af:
            java.lang.String r1 = "BRL"
            boolean r1 = r9.equals(r1)
            if (r1 != 0) goto L_0x00cb
            goto L_0x0041
        L_0x00b8:
            java.lang.String r1 = "PAY: PaymentConfigurationMap/getPaymentService/currency set/requires country but is empty"
            goto L_0x0020
        L_0x00bc:
            java.lang.Object r0 = r2.get(r3)
            goto L_0x00cf
        L_0x00c1:
            java.lang.Object r0 = r2.get(r4)
            goto L_0x00cf
        L_0x00c6:
            java.lang.Object r0 = r2.get(r6)
            goto L_0x00cf
        L_0x00cb:
            java.lang.Object r0 = r2.get(r5)
        L_0x00cf:
            X.01N r0 = (X.AnonymousClass01N) r0
            java.lang.Object r0 = r0.get()
            X.0pp r0 = (X.AbstractC16830pp) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.PaymentConfiguration.AFY(java.lang.String, java.lang.String):X.0pp");
    }

    @Override // X.AbstractC248317b
    public AbstractC16830pp AFZ(String str) {
        AnonymousClass6BG r3;
        C125615rX r4 = this.A01;
        synchronized (r4) {
            r3 = null;
            Iterator A0n = C12960it.A0n(r4.A00);
            while (A0n.hasNext()) {
                AnonymousClass6BG r1 = (AnonymousClass6BG) ((AnonymousClass01N) C12970iu.A15(A0n).getValue()).get();
                if (str.equalsIgnoreCase(r1.A06)) {
                    r3 = r1;
                }
            }
        }
        return r3;
    }

    @Override // X.AbstractC248317b
    /* renamed from: AGf */
    public AbstractC16830pp AGe() {
        String str;
        C17900ra r1 = this.A02;
        C17930rd A01 = r1.A01();
        if (A01 == null) {
            Log.e("PAY: PaymentConfiguration/getService/null country");
        } else {
            String str2 = A01.A03;
            AbstractC38041nQ AIy = AIy(str2);
            AbstractC30791Yv A00 = r1.A00();
            if (A00 != null) {
                str = ((AbstractC30781Yu) A00).A04;
            } else {
                str = null;
            }
            Log.e(C12960it.A0d(str2, C12960it.A0k("PAY: PaymentConfiguration/getService/defaulted to countryCode=")));
            if (AIy != null) {
                return AIy.AFX(str);
            }
        }
        return null;
    }

    @Override // X.AbstractC248417c
    public /* bridge */ /* synthetic */ AbstractC16840pq AGg(String str, String str2) {
        return AGh(str, null);
    }

    @Override // X.AbstractC248317b
    public AbstractC16830pp AGh(String str, String str2) {
        AbstractC38041nQ AIy = AIy(str);
        if (AIy != null) {
            return AIy.AFX(null);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c A[RETURN] */
    @Override // X.AbstractC248317b
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC38041nQ AIy(java.lang.String r12) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.PaymentConfiguration.AIy(java.lang.String):X.1nQ");
    }
}
