package com.whatsapp.payments;

import X.AbstractActivityC121705jc;
import X.AbstractC136196Lo;
import X.AbstractC136506Mu;
import X.AbstractC451020e;
import X.AnonymousClass1FK;
import X.AnonymousClass1V8;
import X.AnonymousClass3FE;
import X.AnonymousClass6MO;
import X.AnonymousClass6MV;
import X.AnonymousClass6MW;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C125625rY;
import X.C126725tL;
import X.C126925tf;
import X.C126935tg;
import X.C128055vU;
import X.C128125vb;
import X.C128725wZ;
import X.C128865wn;
import X.C128875wo;
import X.C128905wr;
import X.C128925wt;
import X.C128935wu;
import X.C129215xM;
import X.C129225xN;
import X.C129335xY;
import X.C12960it;
import X.C130765zw;
import X.C130785zy;
import X.C14900mE;
import X.C18650sn;
import X.C30931Zj;
import X.C452120p;
import android.content.Context;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes4.dex */
public class IDxRCallbackShape0S0200000_3_I1 extends AbstractC451020e {
    public Object A00;
    public Object A01;
    public final int A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public IDxRCallbackShape0S0200000_3_I1(Context context, C14900mE r2, C18650sn r3, Object obj, Object obj2, int i) {
        super(context, r2, r3);
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    @Override // X.AbstractC451020e
    public List A01(AnonymousClass1V8 r8) {
        if (7 - this.A02 != 0) {
            return super.A01(r8);
        }
        ArrayList A0l = C12960it.A0l();
        Iterator it = r8.A0J("error").iterator();
        while (it.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it);
            if (A0d != null) {
                String A0I = A0d.A0I("code", null);
                String A0I2 = A0d.A0I("text", null);
                String A0I3 = A0d.A0I("auth-ticket-fp", null);
                if (A0I != null) {
                    int parseInt = Integer.parseInt(A0I);
                    C452120p A0L = C117305Zk.A0L();
                    A0L.A00 = parseInt;
                    A0L.A09 = A0I2;
                    A0L.A06 = A0I3;
                    A0l.add(A0L);
                }
            }
        }
        return A0l;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        switch (this.A02) {
            case 0:
                ((C129335xY) this.A00).A0B.A05(C12960it.A0b("BrazilAddCredentialAction : onRequestError: ", r4));
                AbstractActivityC121705jc.A0l(((C126935tg) this.A01).A00, null, r4.A00);
                return;
            case 1:
                Log.i(C12960it.A0b("PAY: BrazilFetchNetworkInfoAction/onRequestError: ", r4));
                ((C128935wu) this.A01).A00(null);
                return;
            case 2:
                C117315Zl.A0S(((C126925tf) this.A01).A00);
                return;
            case 3:
                ((C129225xN) this.A00).A06.AQz(r4, null);
                return;
            case 4:
                Log.i(C12960it.A0b("PAY: BrazilGetVerificationMethods onRequestError: ", r4));
                ((AnonymousClass6MO) this.A01).AVP(r4, null);
                return;
            case 5:
                Log.i(C12960it.A0b("PAY: BrazilFetchEloChallengeIdAction onRequestError: ", r4));
                ((C125625rY) this.A01).A00.A02(r4, null);
                return;
            case 6:
                Log.i(C12960it.A0b("PAY: BrazilSubmitVerificationMethodAction onRequestError: ", r4));
                ((C128905wr) this.A01).A00(r4, null);
                return;
            case 7:
                StringBuilder A0k = C12960it.A0k("PAY: MFAAction/onRequestError - ");
                A0k.append(((C128125vb) this.A00).A05);
                Log.w(C12960it.A0Z(r4, ": ", A0k));
                ((C126725tL) this.A01).A01.A00(new C130765zw(r4));
                return;
            case 8:
                ((AnonymousClass1FK) this.A01).AV3(r4);
                return;
            case 9:
                ((C128725wZ) this.A01).A00(r4);
                return;
            case 10:
                ((C128865wn) this.A01).A00(r4);
                return;
            case 11:
                ((C128875wo) this.A01).A00(r4);
                return;
            case 12:
                ((AbstractC136506Mu) this.A01).AKZ(r4);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                ((C129215xM) this.A00).A05.A05(C12960it.A0f(C12960it.A0k("providerKey: onRequestError "), r4.A00));
                ((AnonymousClass6MV) this.A01).APo(r4);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                ((AnonymousClass6MW) this.A01).APo(r4);
                return;
            case 15:
                ((C128925wt) this.A01).A00(r4);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((BrazilPayBloksActivity) this.A00).A2n((AnonymousClass3FE) this.A01);
                return;
            case 17:
                Log.e("PAY: NoviUtils/getResourceEncryptionKey onRequestError");
                C130785zy.A03(r4, (AbstractC136196Lo) this.A00);
                return;
            default:
                super.A02(r4);
                return;
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r5) {
        AnonymousClass3FE r0;
        switch (this.A02) {
            case 0:
                ((C129335xY) this.A00).A0B.A05(C12960it.A0b("BrazilAddCredentialAction : onResponseError: ", r5));
                AbstractActivityC121705jc.A0l(((C126935tg) this.A01).A00, null, r5.A00);
                return;
            case 1:
                Log.i(C12960it.A0b("PAY: BrazilFetchNetworkInfoAction/onResponseError: ", r5));
                ((C128935wu) this.A01).A00(null);
                return;
            case 2:
                r0 = ((C126925tf) this.A01).A00;
                break;
            case 3:
                ((C129225xN) this.A00).A06.AQz(r5, null);
                return;
            case 4:
                Log.i(C12960it.A0b("PAY: BrazilGetVerificationMethods onResponseError: ", r5));
                ((AnonymousClass6MO) this.A01).AVP(r5, null);
                return;
            case 5:
                Log.i(C12960it.A0b("PAY: BrazilFetchEloChallengeIdAction onResponseError: ", r5));
                ((C125625rY) this.A01).A00.A02(r5, null);
                return;
            case 6:
                Log.i(C12960it.A0b("PAY: BrazilSubmitVerificationMethodAction onResponseError: ", r5));
                ((C128905wr) this.A01).A00(r5, null);
                return;
            case 7:
                StringBuilder A0k = C12960it.A0k("PAY: MFAAction/onResponseError - ");
                A0k.append(((C128125vb) this.A00).A05);
                Log.e(C12960it.A0Z(r5, ": ", A0k));
                ((C126725tL) this.A01).A01.A00(new C130765zw(r5));
                return;
            case 8:
                ((AnonymousClass1FK) this.A01).AVA(r5);
                return;
            case 9:
                ((C128725wZ) this.A01).A00(r5);
                return;
            case 10:
                if (r5.A00 == 10756) {
                    C128865wn r3 = (C128865wn) this.A01;
                    C30931Zj r1 = r3.A01.A07;
                    StringBuilder A0k2 = C12960it.A0k("performNameCheck onNameCheckComplete, eligible: ");
                    A0k2.append(false);
                    C117295Zj.A1F(r1, A0k2);
                    r0 = r3.A02.A00;
                    break;
                } else {
                    ((C128865wn) this.A01).A00(r5);
                    return;
                }
            case 11:
                ((C128055vU) this.A00).A06.A05(C12960it.A0b("performDobComplianceCheck onResponseError: ", r5));
                ((C128875wo) this.A01).A00(r5);
                return;
            case 12:
                ((AbstractC136506Mu) this.A01).AKZ(r5);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                ((C129215xM) this.A00).A05.A05(C12960it.A0f(C12960it.A0k("providerKey: onResponseError "), r5.A00));
                ((AnonymousClass6MV) this.A01).APo(r5);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                ((AnonymousClass6MW) this.A01).APo(r5);
                return;
            case 15:
                A02(r5);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((BrazilPayBloksActivity) this.A00).A2n((AnonymousClass3FE) this.A01);
                return;
            case 17:
                Log.e("PAY: NoviUtils/getResourceEncryptionKey onResponseError");
                C130785zy.A03(r5, (AbstractC136196Lo) this.A00);
                return;
            default:
                super.A03(r5);
                return;
        }
        C117315Zl.A0S(r0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:280:0x078c  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0790  */
    @Override // X.AbstractC451020e
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AnonymousClass1V8 r15) {
        /*
        // Method dump skipped, instructions count: 2002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1.A04(X.1V8):void");
    }
}
