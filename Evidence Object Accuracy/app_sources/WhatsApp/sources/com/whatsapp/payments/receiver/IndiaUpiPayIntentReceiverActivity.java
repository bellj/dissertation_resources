package com.whatsapp.payments.receiver;

import X.AbstractActivityC119235dO;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C117295Zj;
import X.C128645wR;
import X.C12980iv;
import X.C12990iw;
import X.C1310661b;
import X.C21860y6;
import X.C36021jC;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiPaymentLauncherActivity;

/* loaded from: classes4.dex */
public class IndiaUpiPayIntentReceiverActivity extends AbstractActivityC121665jA {
    public boolean A00;

    public IndiaUpiPayIntentReceiverActivity() {
        this(0);
    }

    public IndiaUpiPayIntentReceiverActivity(int i) {
        this.A00 = false;
        C117295Zj.A0p(this, 5);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119235dO.A1S(A09, A1M, this, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this));
            AbstractActivityC119235dO.A1Y(A1M, this);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1020) {
            setResult(i2, intent);
            finish();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C128645wR r2 = new C128645wR(((AbstractActivityC121685jC) this).A0I);
        if (C1310661b.A00(getIntent().getData(), "DEEP_LINK") == null) {
            finish();
            return;
        }
        C21860y6 r1 = r2.A00;
        if (r1.A0B()) {
            Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentLauncherActivity.class);
            A0D.setData(getIntent().getData());
            startActivityForResult(A0D, 1020);
            return;
        }
        boolean A0C = r1.A0C();
        int i = SearchActionVerificationClientService.NOTIFICATION_ID;
        if (A0C) {
            i = 10001;
        }
        C36021jC.A01(this, i);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        int i2;
        int i3;
        if (i == 10000) {
            r2 = C12980iv.A0S(this);
            r2.A07(R.string.payment_intent_error_cannot_continue_dialog_title);
            r2.A06(R.string.payment_intent_error_no_account);
            i2 = R.string.ok;
            i3 = 5;
        } else if (i != 10001) {
            return super.onCreateDialog(i);
        } else {
            r2 = C12980iv.A0S(this);
            r2.A07(R.string.payment_intent_error_cannot_continue_dialog_title);
            r2.A06(R.string.payment_intent_error_no_pin_set);
            i2 = R.string.ok;
            i3 = 4;
        }
        C117295Zj.A0q(r2, this, i3, i2);
        r2.A0B(false);
        return r2.create();
    }
}
