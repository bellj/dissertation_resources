package com.whatsapp.payments.pin.ui;

import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass66Y;
import X.AnonymousClass6MZ;
import X.C117295Zj;
import X.C119145cx;
import X.C125035qZ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C130015yf;
import X.C13010iy;
import X.C14830m7;
import X.CountDownTimerC117445Zy;
import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;

/* loaded from: classes4.dex */
public class PinBottomSheetDialogFragment extends Hilt_PinBottomSheetDialogFragment {
    public long A00;
    public CountDownTimer A01;
    public View A02;
    public ProgressBar A03;
    public TextView A04;
    public CodeInputField A05;
    public Button A06;
    public C14830m7 A07;
    public AnonymousClass018 A08;
    public C130015yf A09;
    public C125035qZ A0A;
    public AnonymousClass6MZ A0B;
    public boolean A0C;

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.setRequestedOrientation(10);
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Window window;
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.pin_bottom_sheet);
        Dialog dialog = ((DialogFragment) this).A03;
        if (!(dialog == null || (window = dialog.getWindow()) == null)) {
            window.setSoftInputMode(3);
        }
        this.A02 = A0F.findViewById(R.id.pin_text_container);
        this.A03 = (ProgressBar) A0F.findViewById(R.id.progress_bar);
        this.A04 = C12960it.A0J(A0F, R.id.error_text);
        Button button = (Button) AnonymousClass028.A0D(A0F, R.id.forgot_pin_button);
        this.A06 = button;
        String A0I = A0I(R.string.payment_pin_term_default);
        if (this.A0A != null) {
            String string = A01().getString(R.string.payment_pin_term_default);
            if (!TextUtils.isEmpty(string)) {
                A0I = string;
            }
        }
        button.setText(C12970iu.A0q(this, A0I, C12970iu.A1b(), 0, R.string.payment_forgot_pin));
        C117295Zj.A0n(this.A06, this, 5);
        boolean z = this.A0C;
        this.A0C = z;
        Button button2 = this.A06;
        if (button2 != null) {
            button2.setVisibility(C13010iy.A00(z ? 1 : 0));
        }
        CodeInputField codeInputField = (CodeInputField) A0F.findViewById(R.id.code);
        this.A05 = codeInputField;
        codeInputField.A07(new AnonymousClass66Y(this), 6, A02().getColor(R.color.fb_pay_input_color));
        ((NumberEntryKeyboard) A0F.findViewById(R.id.number_entry_keyboard)).A06 = this.A05;
        if (this.A0A != null) {
            layoutInflater.inflate(R.layout.pay_header, C12980iv.A0P(A0F, R.id.title_view), true);
            C12970iu.A19(A01(), C12960it.A0J(A0F, R.id.header_text), R.string.payment_pin_bottom_sheet_title);
        }
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        long A00 = this.A09.A00() * 1000;
        if (A00 > this.A07.A00() || this.A01 != null) {
            A1P(A00, false);
        }
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.setRequestedOrientation(1);
        }
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        super.A1L(view);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        A00.A0E = new C119145cx(A00, this);
    }

    public void A1M() {
        A1G(true);
        this.A02.setVisibility(0);
        this.A03.setVisibility(8);
        this.A05.setEnabled(true);
    }

    public void A1N() {
        A1G(false);
        this.A02.setVisibility(4);
        this.A04.setVisibility(4);
        this.A03.setVisibility(0);
        this.A05.setEnabled(false);
    }

    public void A1O(int i) {
        CountDownTimer countDownTimer = this.A01;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A01 = null;
        }
        this.A05.setErrorState(true);
        this.A05.A05();
        this.A04.setText(this.A08.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.payment_pin_retry_attempts, (long) i));
        TextView textView = this.A04;
        C12960it.A0s(textView.getContext(), textView, R.color.code_input_error);
        this.A04.setVisibility(0);
    }

    public final void A1P(long j, boolean z) {
        CountDownTimer countDownTimer = this.A01;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A01 = null;
        }
        this.A00 = j;
        TextView textView = this.A04;
        C12960it.A0s(textView.getContext(), textView, R.color.secondary_text);
        this.A04.setVisibility(0);
        this.A05.setErrorState(true);
        this.A05.setEnabled(false);
        if (z) {
            this.A05.A05();
        }
        this.A01 = new CountDownTimerC117445Zy(this, j - this.A07.A00()).start();
    }
}
