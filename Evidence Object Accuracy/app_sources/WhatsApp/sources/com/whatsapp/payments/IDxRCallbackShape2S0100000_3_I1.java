package com.whatsapp.payments;

import X.AbstractC129955yZ;
import X.AbstractC136266Lw;
import X.AbstractC451020e;
import X.AbstractC451720l;
import X.AnonymousClass009;
import X.AnonymousClass1V8;
import X.AnonymousClass1V9;
import X.AnonymousClass1ZW;
import X.AnonymousClass5g0;
import X.C117295Zj;
import X.C117305Zk;
import X.C119775f5;
import X.C119785f6;
import X.C120315fy;
import X.C120325fz;
import X.C120335g1;
import X.C120355g3;
import X.C120365g4;
import X.C127975vM;
import X.C128075vW;
import X.C128245vn;
import X.C128535wG;
import X.C128575wK;
import X.C129205xL;
import X.C129345xZ;
import X.C12960it;
import X.C12970iu;
import X.C129725yC;
import X.C13000ix;
import X.C130675zn;
import X.C14900mE;
import X.C18600si;
import X.C18650sn;
import X.C30881Ze;
import X.C452120p;
import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class IDxRCallbackShape2S0100000_3_I1 extends AbstractC451020e {
    public Object A00;
    public final int A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public IDxRCallbackShape2S0100000_3_I1(Context context, C14900mE r2, C18650sn r3, Object obj, int i) {
        super(context, r2, r3);
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        switch (this.A01) {
            case 0:
                Log.i(C12960it.A0b("PAY: BrazilAddCardAction onRequestError: ", r4));
                ((C120335g1) this.A00).A0G.A00(null, r4, null, false);
                return;
            case 1:
                Log.i(C12960it.A0b("PAY: BrazilDeviceBindingAction onRequestError: ", r4));
                ((C129725yC) this.A00).A0B.AP5(null, r4, null);
                return;
            case 2:
                Log.i(C12960it.A0b("PAY: BrazilDeviceBindingAction onRequestError: ", r4));
                ((C129345xZ) this.A00).A0B.A00(null);
                return;
            case 3:
                Log.e(C12960it.A0b("PAY: BrazilMerchantLinkAction request error: ", r4));
                ((C128245vn) this.A00).A07.A00(null, r4);
                return;
            case 4:
                Log.e(C12960it.A0b("PAY: BrazilMerchantPreLinkAction request error: ", r4));
                ((AnonymousClass5g0) this.A00).A07.A00(r4, null);
                return;
            case 5:
                ((C120315fy) this.A00).A0B.A00(r4);
                return;
            case 6:
                ((C120325fz) this.A00).A08.A00(C117305Zk.A0L());
                return;
            case 7:
                Log.i(C12960it.A0b("PAY: BrazilVerifyCardOTPSendAction onRequestError: ", r4));
                ((C120365g4) this.A00).A05.A00(null, r4);
                return;
            case 8:
                Log.i(C12960it.A0b("PAY: BrazilVerifyCardSendAuthCodeAction onRequestError: ", r4));
                ((C120355g3) this.A00).A04.A00(r4);
                return;
            case 9:
                C129205xL r2 = (C129205xL) this.A00;
                r2.A06.A06(C12960it.A0b("onRequestError: ", r4));
                r2.A05.AVO(r4, null);
                return;
            default:
                super.A02(r4);
                return;
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r4) {
        switch (this.A01) {
            case 0:
                Log.i(C12960it.A0b("PAY: BrazilAddCardAction onResponseError: ", r4));
                ((C120335g1) this.A00).A0G.A00(null, r4, null, false);
                return;
            case 1:
                Log.i(C12960it.A0b("PAY: BrazilDeviceBindingAction onResponseError: ", r4));
                ((C129725yC) this.A00).A0B.AP5(null, r4, null);
                return;
            case 2:
                Log.i(C12960it.A0b("PAY: BrazilDeviceBindingAction onResponseError: ", r4));
                ((C129345xZ) this.A00).A0B.A00(null);
                return;
            case 3:
                Log.e(C12960it.A0b("PAY: BrazilMerchantLinkAction response error: ", r4));
                ((C128245vn) this.A00).A07.A00(null, r4);
                return;
            case 4:
                Log.e(C12960it.A0b("PAY: BrazilMerchantPreLinkAction response error: ", r4));
                ((AnonymousClass5g0) this.A00).A07.A00(r4, null);
                return;
            case 5:
                Log.e(C12960it.A0b("PAY: BrazilMerchantRegAction/regMerchant: onResponseError: ", r4));
                ((C120315fy) this.A00).A0B.A00(r4);
                return;
            case 6:
                ((C120325fz) this.A00).A08.A00(C117305Zk.A0L());
                return;
            case 7:
                Log.i(C12960it.A0b("PAY: BrazilVerifyCardOTPSendAction onResponseError: ", r4));
                ((C120365g4) this.A00).A05.A00(null, r4);
                return;
            case 8:
                Log.i(C12960it.A0b("PAY: BrazilVerifyCardSendAuthCodeAction onResponseError: ", r4));
                ((C120355g3) this.A00).A04.A00(r4);
                return;
            case 9:
                C129205xL r2 = (C129205xL) this.A00;
                r2.A06.A06(C12960it.A0b("onResponseError: ", r4));
                r2.A05.AVO(r4, null);
                return;
            default:
                super.A03(r4);
                return;
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r10) {
        AnonymousClass1V8 A0E;
        AnonymousClass1V8[] r0;
        int length;
        AnonymousClass1V8 A0E2;
        AnonymousClass1V8[] r02;
        int length2;
        AnonymousClass1V8 A0E3;
        AnonymousClass1V8 A0E4;
        AnonymousClass1V8 A0E5;
        switch (this.A01) {
            case 0:
                AnonymousClass1V8 A0c = C117305Zk.A0c(r10);
                int i = 0;
                if (A0c == null || (A0E = A0c.A0E("card")) == null) {
                    ((C120335g1) this.A00).A0G.A00(null, C117305Zk.A0L(), null, false);
                    return;
                }
                C119775f5 r2 = new C119775f5();
                C120335g1 r7 = (C120335g1) this.A00;
                r2.A01(r7.A07, A0E, 0);
                C30881Ze r5 = (C30881Ze) r2.A05();
                r7.A0F.A01(null, r5);
                if (r2.A0A()) {
                    r7.A0C.A00().A03(new AbstractC451720l(r5, this) { // from class: X.67g
                        public final /* synthetic */ C30881Ze A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                            ((C120335g1) iDxRCallbackShape2S0100000_3_I1.A00).A0G.A00(this.A00, null, null, true);
                        }
                    }, r5);
                    return;
                } else if (!r2.A07) {
                    ArrayList A0l = C12960it.A0l();
                    AnonymousClass1V8 A0E6 = A0c.A0E("verify-method-list");
                    if (A0E6 == null || (r0 = A0E6.A03) == null || (length = r0.length) <= 0) {
                        r7.A0C.A00().A03(new AbstractC451720l(r5, this, A0l) { // from class: X.67w
                            public final /* synthetic */ C30881Ze A00;
                            public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;
                            public final /* synthetic */ ArrayList A02;

                            {
                                this.A01 = r2;
                                this.A00 = r1;
                                this.A02 = r3;
                            }

                            @Override // X.AbstractC451720l
                            public final void AM7(List list) {
                                IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                                ((C120335g1) iDxRCallbackShape2S0100000_3_I1.A00).A0G.A00(this.A00, null, this.A02, false);
                            }
                        }, r5);
                        return;
                    }
                    do {
                        A0l.add(new C130675zn(A0E6.A0D(i)));
                        i++;
                    } while (i < length);
                    r7.A0C.A00().A03(new AbstractC451720l(r5, this, A0l) { // from class: X.67w
                        public final /* synthetic */ C30881Ze A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;
                        public final /* synthetic */ ArrayList A02;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                            this.A02 = r3;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                            ((C120335g1) iDxRCallbackShape2S0100000_3_I1.A00).A0G.A00(this.A00, null, this.A02, false);
                        }
                    }, r5);
                    return;
                } else {
                    r7.A0C.A00().A03(new AbstractC451720l(r5, this) { // from class: X.67h
                        public final /* synthetic */ C30881Ze A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                            ((C120335g1) iDxRCallbackShape2S0100000_3_I1.A00).A0G.A00(this.A00, null, null, false);
                        }
                    }, r5);
                    return;
                }
            case 1:
                AnonymousClass1V8 A0c2 = C117305Zk.A0c(r10);
                if (A0c2 == null || (A0E2 = A0c2.A0E("card")) == null) {
                    ((C129725yC) this.A00).A0B.AP5(null, C117305Zk.A0L(), null);
                    return;
                }
                C119775f5 r1 = new C119775f5();
                C129725yC r8 = (C129725yC) this.A00;
                int i2 = 0;
                r1.A01(r8.A05, A0E2, 0);
                C30881Ze r52 = (C30881Ze) r1.A05();
                ArrayList A0l2 = C12960it.A0l();
                AnonymousClass1V8 A0E7 = A0c2.A0E("verify-method-list");
                if (A0E7 == null || (r02 = A0E7.A03) == null || (length2 = r02.length) <= 0) {
                    r8.A0B.AP5(r52, null, A0l2);
                    return;
                }
                do {
                    A0l2.add(new C130675zn(A0E7.A0D(i2)));
                    i2++;
                } while (i2 < length2);
                r8.A0B.AP5(r52, null, A0l2);
                return;
            case 2:
                AnonymousClass1V8 A0c3 = C117305Zk.A0c(r10);
                if (A0c3 == null || (A0E3 = A0c3.A0E("elo")) == null) {
                    C128535wG r12 = ((C129345xZ) this.A00).A0B;
                    new C452120p();
                    r12.A00(null);
                    return;
                }
                C128075vW r6 = new C128075vW("1", A0E3.A0I("challenge_id", null), null, 5);
                String A0I = A0E3.A0I("ciphered_wallet_secret", null);
                C129345xZ r53 = (C129345xZ) this.A00;
                C18600si r4 = r53.A06.A01;
                String string = r4.A01().getString("payment_trusted_device_elo_wallet_store", null);
                JSONObject jSONObject = null;
                try {
                    if (string != null) {
                        jSONObject = C13000ix.A05(string);
                    } else {
                        jSONObject = C117295Zj.A0a();
                    }
                    jSONObject.put("wallet_secret", A0I);
                } catch (JSONException unused) {
                    r4.A02.A06("Failed to updated the wallet_secret");
                }
                C12970iu.A1D(C117295Zj.A05(r4), "payment_trusted_device_elo_wallet_store", jSONObject.toString());
                r53.A0B.A00(r6);
                return;
            case 3:
                try {
                    AnonymousClass1V8 A0F = r10.A0F("account");
                    C452120p A00 = C452120p.A00(A0F);
                    if (A00 != null) {
                        ((C128245vn) this.A00).A07.A00(null, A00);
                        return;
                    }
                    C119785f6 r54 = new C119785f6();
                    C128245vn r42 = (C128245vn) this.A00;
                    r54.A01(r42.A03, A0F.A0F("merchant"), 0);
                    AnonymousClass1ZW r22 = (AnonymousClass1ZW) r54.A05();
                    r42.A06.A00().A03(new AbstractC451720l(r22, this) { // from class: X.67k
                        public final /* synthetic */ AnonymousClass1ZW A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                            ((C128245vn) iDxRCallbackShape2S0100000_3_I1.A00).A07.A00(this.A00, null);
                        }
                    }, r22);
                    return;
                } catch (AnonymousClass1V9 e) {
                    Log.e("PAY: BrazilMerchantLinkAction/regMerchant: invalid response message", e);
                    ((C128245vn) this.A00).A07.A00(null, C117305Zk.A0L());
                    return;
                }
            case 4:
                AnonymousClass1V8 A0c4 = C117305Zk.A0c(r10);
                AnonymousClass009.A05(A0c4);
                C127975vM r3 = new C127975vM(A0c4);
                C452120p r23 = r3.A00;
                C128575wK r03 = ((AnonymousClass5g0) this.A00).A07;
                if (r23 == null) {
                    r03.A00(null, r3);
                    return;
                } else {
                    r03.A00(r23, null);
                    return;
                }
            case 5:
                try {
                    AnonymousClass1V8 A0F2 = r10.A0F("account");
                    C452120p A002 = C452120p.A00(A0F2);
                    if (A002 != null) {
                        ((C120315fy) this.A00).A0B.A00(A002);
                        return;
                    }
                    C119785f6 r43 = new C119785f6();
                    C120315fy r32 = (C120315fy) this.A00;
                    r43.A01(r32.A05, A0F2.A0F("merchant"), 0);
                    AnonymousClass1ZW r24 = (AnonymousClass1ZW) r43.A05();
                    r32.A08.A00().A03(new AbstractC451720l(r24, this) { // from class: X.67l
                        public final /* synthetic */ AnonymousClass1ZW A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            ((C120315fy) this.A01.A00).A0B.A00(null);
                        }
                    }, r24);
                    return;
                } catch (AnonymousClass1V9 unused2) {
                    Log.e("PAY: BrazilMerchantRegAction/regMerchant: invalid response message");
                    ((C120315fy) this.A00).A0B.A00(C117305Zk.A0L());
                    return;
                }
            case 6:
                try {
                    AnonymousClass1V8 A0F3 = r10.A0F("account");
                    C452120p A003 = C452120p.A00(A0F3);
                    if (A003 != null) {
                        ((C120325fz) this.A00).A08.A00(A003);
                        return;
                    }
                    C119785f6 r44 = new C119785f6();
                    C120325fz r33 = (C120325fz) this.A00;
                    r44.A01(r33.A04, A0F3.A0F("merchant"), 0);
                    AnonymousClass1ZW r25 = (AnonymousClass1ZW) r44.A05();
                    r33.A07.A00().A03(new AbstractC451720l(r25, this) { // from class: X.67m
                        public final /* synthetic */ AnonymousClass1ZW A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            ((C120325fz) this.A01.A00).A08.A00(null);
                        }
                    }, r25);
                    return;
                } catch (AnonymousClass1V9 unused3) {
                    Log.e("PAY: BrazilMerchantRegAction/regMerchant: invalid response message");
                    ((C120325fz) this.A00).A08.A00(C117305Zk.A0L());
                    return;
                }
            case 7:
                Log.i("PAY: BrazilVerifyCardOTPSendAction success");
                AnonymousClass1V8 A0c5 = C117305Zk.A0c(r10);
                if (A0c5 == null || (A0E4 = A0c5.A0E("card")) == null) {
                    ((C120365g4) this.A00).A05.A00(null, C117305Zk.A0L());
                    return;
                }
                C119775f5 r26 = new C119775f5();
                C120365g4 r34 = (C120365g4) this.A00;
                r26.A01(r34.A01, A0E4, 0);
                C30881Ze r27 = (C30881Ze) r26.A05();
                ((AbstractC129955yZ) r34).A07.A00().A03(new AbstractC451720l(r27, this) { // from class: X.67n
                    public final /* synthetic */ C30881Ze A00;
                    public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC451720l
                    public final void AM7(List list) {
                        IDxRCallbackShape2S0100000_3_I1 iDxRCallbackShape2S0100000_3_I1 = this.A01;
                        ((C120365g4) iDxRCallbackShape2S0100000_3_I1.A00).A05.A00(this.A00, null);
                    }
                }, r27);
                return;
            case 8:
                Log.i("PAY: BrazilVerifyCardSendAuthCodeAction success");
                AnonymousClass1V8 A0c6 = C117305Zk.A0c(r10);
                if (!(A0c6 == null || (A0E5 = A0c6.A0E("card")) == null)) {
                    C119775f5 r28 = new C119775f5();
                    C120355g3 r35 = (C120355g3) this.A00;
                    r28.A01(r35.A01, A0E5, 0);
                    C30881Ze r29 = (C30881Ze) r28.A05();
                    ((AbstractC129955yZ) r35).A07.A00().A03(new AbstractC451720l(r29, this) { // from class: X.67o
                        public final /* synthetic */ C30881Ze A00;
                        public final /* synthetic */ IDxRCallbackShape2S0100000_3_I1 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC451720l
                        public final void AM7(List list) {
                            ((C120355g3) this.A01.A00).A04.A00(null);
                        }
                    }, r29);
                }
                Log.i(C12960it.A0b("PAY: BrazilVerifyCardSendAuthCodeAction onResponseSuccess: ", r10));
                return;
            case 9:
                AnonymousClass1V8 A0c7 = C117305Zk.A0c(r10);
                AbstractC136266Lw r13 = ((C129205xL) this.A00).A05;
                if (A0c7 != null) {
                    r13.AVO(null, A0c7.A0I("token-id", null));
                    return;
                } else {
                    r13.AVO(C117305Zk.A0L(), null);
                    return;
                }
            default:
                super.A04(r10);
                return;
        }
    }
}
