package com.whatsapp.payments.service;

import X.AbstractC14440lR;
import X.AnonymousClass61F;
import X.C129685y8;
import X.C130105yo;
import X.C14850m9;
import X.C14900mE;
import X.C16590pI;
import X.C17070qD;
import X.C20730wE;
import X.C22120yY;
import X.C22590zK;
import com.whatsapp.R;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class NoviPaymentInviteFragment extends Hilt_NoviPaymentInviteFragment {
    public C14900mE A00;
    public C20730wE A01;
    public C16590pI A02;
    public C14850m9 A03;
    public C17070qD A04;
    public AnonymousClass61F A05;
    public C130105yo A06;
    public C129685y8 A07;
    public C22120yY A08;
    public C22590zK A09;
    public AbstractC14440lR A0A;

    public final void A1D(int i) {
        if (i == 0 || i == 1) {
            Log.i("dismiss()");
            ((PaymentInviteFragment) this).A07.A04(3);
            this.A00.A0E(A0C().getString(R.string.payments_gating_generic_ineligibility_message), 1);
        } else if (i == 2) {
            A1B();
        } else if (i == 3) {
            Log.i("startPaymentFlow()");
            ((PaymentInviteFragment) this).A07.A04(1);
        }
    }
}
