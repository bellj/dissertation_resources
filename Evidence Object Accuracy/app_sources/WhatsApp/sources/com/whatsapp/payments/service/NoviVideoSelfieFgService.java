package com.whatsapp.payments.service;

import X.AbstractServiceC123775nr;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass1UY;
import X.AnonymousClass60Y;
import X.C005602s;
import X.C117305Zk;
import X.C128615wO;
import X.C12960it;
import X.C129695y9;
import X.C12990iw;
import X.C130155yt;
import X.C1316663q;
import X.C133536Bd;
import X.C17070qD;
import X.C18360sK;
import X.C22630zO;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;

/* loaded from: classes4.dex */
public class NoviVideoSelfieFgService extends AbstractServiceC123775nr {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public AnonymousClass01d A07;
    public C17070qD A08;
    public C130155yt A09;
    public AnonymousClass60Y A0A;
    public C129695y9 A0B;
    public C1316663q A0C;
    public C128615wO A0D;
    public String A0E;
    public String A0F;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static /* synthetic */ void A00(NoviVideoSelfieFgService noviVideoSelfieFgService) {
        long j = noviVideoSelfieFgService.A03 + noviVideoSelfieFgService.A06;
        Intent intent = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
        intent.putExtra("extra_event_type", "extra_event_upload_progress");
        intent.putExtra("extra_total_file_size", noviVideoSelfieFgService.A04);
        intent.putExtra("extra_total_file_size_uploaded", j);
        noviVideoSelfieFgService.A03(noviVideoSelfieFgService.A00, j, noviVideoSelfieFgService.A04);
        C117305Zk.A0y(noviVideoSelfieFgService, intent);
    }

    public final void A03(int i, long j, long j2) {
        int i2 = (int) ((j * 100) / j2);
        String string = getString(R.string.novi_selfie_upload_media);
        String A0X = C12960it.A0X(this, Integer.valueOf(i2), new Object[1], 0, R.string.novi_selfie_upload_media_progress_percentage);
        Class AFa = this.A08.A02().AFa();
        if (AFa == null) {
            Log.e("[PAY] : NoviVideoSelfieFgService/ postNotification payment settings not supported");
            return;
        }
        Intent A0D = C12990iw.A0D(this, AFa);
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A0A(string);
        A00.A09(A0X);
        A00.A09 = AnonymousClass1UY.A00(this, 0, A0D, 0);
        int i3 = -2;
        if (Build.VERSION.SDK_INT >= 26) {
            i3 = -1;
        }
        A00.A03 = i3;
        if (i2 < 100) {
            A00.A03(100, i2, false);
        } else {
            A00.A03(100, 100, true);
        }
        C18360sK.A01(A00, R.drawable.notifybar);
        A01(i, A00.A01(), 29);
    }

    public final void A04(String str) {
        String string = getString(R.string.novi_title);
        Class AFa = this.A08.A02().AFa();
        if (AFa == null) {
            Log.e("[PAY] : NoviVideoSelfieFgService/ onSelfieCompleteAndPostNotification payment settings not supported");
            return;
        }
        Intent A0D = C12990iw.A0D(this, AFa);
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A0A(string);
        A00.A09(str);
        A00.A09 = AnonymousClass1UY.A00(this, 0, A0D, 0);
        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
        notificationCompat$BigTextStyle.A09(str);
        A00.A08(notificationCompat$BigTextStyle);
        int i = -2;
        if (Build.VERSION.SDK_INT >= 26) {
            i = -1;
        }
        A00.A03 = i;
        A00.A0D(true);
        C18360sK.A01(A00, R.drawable.notifybar);
        Notification A01 = A00.A01();
        NotificationManager A08 = this.A07.A08();
        if (A08 != null) {
            A08.notify(30, A01);
        }
        stopSelf();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("PAY: NoviVideoSelfieFgService/onDestroy");
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        Log.i("PAY: NoviVideoSelfieFgService/onStartCommand");
        this.A00 = i2;
        Bundle extras = intent.getExtras();
        AnonymousClass009.A05(extras);
        this.A0C = (C1316663q) extras.getParcelable("extra_step_up");
        this.A01 = extras.getInt("extra_step_up_origin_action");
        this.A0E = extras.getString("extra_step_up_challenge_id");
        String string = extras.getString("extra_disable_face_rec");
        AnonymousClass009.A05(string);
        this.A0F = string;
        A03(i2, 0, 1);
        String str = this.A0C.A04;
        File A00 = this.A0D.A00("selfie.mp4");
        File A002 = this.A0D.A00("selfie.jpeg");
        if (A00 == null || !A00.exists() || A002 == null || !A002.exists()) {
            Intent intent2 = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
            intent2.putExtra("extra_event_type", "extra_event_upload_failed");
            C117305Zk.A0y(this, intent2);
            return 2;
        }
        this.A02 = A002.length();
        long length = A00.length();
        this.A05 = length;
        this.A04 = this.A02 + length;
        Intent intent3 = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
        intent3.putExtra("extra_event_type", "extra_event_upload_begin");
        intent3.putExtra("extra_total_file_size", this.A04);
        C117305Zk.A0y(this, intent3);
        this.A0B.A01(new C133536Bd(this, A00, str), A002, "SELFIE_VIDEO", str);
        return 2;
    }
}
