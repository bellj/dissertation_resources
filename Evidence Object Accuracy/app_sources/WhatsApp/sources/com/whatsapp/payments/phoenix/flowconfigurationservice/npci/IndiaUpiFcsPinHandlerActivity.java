package com.whatsapp.payments.phoenix.flowconfigurationservice.npci;

import X.AbstractActivityC121545iU;
import X.AbstractActivityC121645j6;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC17770rM;
import X.AbstractC50172Ok;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass01H;
import X.AnonymousClass102;
import X.AnonymousClass17Q;
import X.AnonymousClass1ZR;
import X.AnonymousClass2AC;
import X.AnonymousClass2SM;
import X.AnonymousClass3FB;
import X.AnonymousClass4S1;
import X.AnonymousClass4WU;
import X.AnonymousClass5A3;
import X.AnonymousClass60V;
import X.AnonymousClass6BE;
import X.C004802e;
import X.C120525gK;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C1308460e;
import X.C1329668y;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C16700pc;
import X.C17070qD;
import X.C17120qI;
import X.C17220qS;
import X.C17520qw;
import X.C17530qx;
import X.C17900ra;
import X.C18590sh;
import X.C18610sj;
import X.C18640sm;
import X.C18650sn;
import X.C19750uc;
import X.C21860y6;
import X.C30861Zc;
import X.C30931Zj;
import X.C36021jC;
import X.C452120p;
import X.C49032Iw;
import X.C87394Bi;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import com.whatsapp.R;
import com.whatsapp.payments.phoenix.flowconfigurationservice.npci.IndiaUpiFcsPinHandlerActivity;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public final class IndiaUpiFcsPinHandlerActivity extends AbstractActivityC121645j6 {
    public C49032Iw A00;
    public C30861Zc A01;
    public AnonymousClass1ZR A02;
    public AnonymousClass4S1 A03;
    public C19750uc A04;
    public AnonymousClass01H A05;
    public String A06;
    public String A07;
    public final C30931Zj A08 = C30931Zj.A00("IndiaUpiFcsPinHandlerActivity", "payment-settings", "IN");
    public final AnonymousClass4WU A09 = new AnonymousClass4WU(this);

    @Override // X.AbstractActivityC121545iU
    public void A3A() {
    }

    @Override // X.AbstractActivityC121545iU
    public void A3B() {
    }

    public static /* synthetic */ void A02(IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity) {
        C16700pc.A0E(indiaUpiFcsPinHandlerActivity, 0);
        C36021jC.A00(indiaUpiFcsPinHandlerActivity, 12);
        ((ActivityC13790kL) indiaUpiFcsPinHandlerActivity).A00.Ab9(indiaUpiFcsPinHandlerActivity, Uri.parse("https://faq.whatsapp.com/android/payments/how-to-change-or-set-up-new-upi-pin/?india=1"));
        indiaUpiFcsPinHandlerActivity.A2q();
        indiaUpiFcsPinHandlerActivity.finish();
    }

    public static /* synthetic */ void A03(IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity) {
        C16700pc.A0E(indiaUpiFcsPinHandlerActivity, 0);
        C36021jC.A00(indiaUpiFcsPinHandlerActivity, 27);
        indiaUpiFcsPinHandlerActivity.A2q();
        indiaUpiFcsPinHandlerActivity.finish();
    }

    public static /* synthetic */ void A09(IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity) {
        C16700pc.A0E(indiaUpiFcsPinHandlerActivity, 0);
        C36021jC.A00(indiaUpiFcsPinHandlerActivity, 10);
        AnonymousClass3FB r3 = new AnonymousClass3FB("upi_p2p_check_balance", null, null);
        C30861Zc r0 = indiaUpiFcsPinHandlerActivity.A01;
        if (r0 == null) {
            throw C16700pc.A06("paymentBankAccount");
        }
        C17520qw r02 = new C17520qw("credential_id", r0.A0A);
        Map singletonMap = Collections.singletonMap(r02.first, r02.second);
        C16700pc.A0B(singletonMap);
        AnonymousClass01H r03 = indiaUpiFcsPinHandlerActivity.A05;
        if (r03 != null) {
            ((AnonymousClass17Q) r03.get()).A06(null, null, r3, "payment_bank_account_details", singletonMap);
            indiaUpiFcsPinHandlerActivity.A2q();
            indiaUpiFcsPinHandlerActivity.finish();
            return;
        }
        throw C16700pc.A06("fdsManagerLazy");
    }

    public static final /* synthetic */ void A0A(IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity) {
        indiaUpiFcsPinHandlerActivity.A2q();
        indiaUpiFcsPinHandlerActivity.finish();
    }

    public static final /* synthetic */ void A0B(IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity, Map map) {
        Map map2;
        Number number;
        if (map != null) {
            Object obj = map.get("error");
            if ((obj instanceof Map) && (map2 = (Map) obj) != null) {
                Object obj2 = map2.get("code");
                if ((obj2 instanceof Integer) && (number = (Number) obj2) != null) {
                    int intValue = number.intValue();
                    Bundle A0D = C12970iu.A0D();
                    A0D.putInt("error_code", intValue);
                    int i = 12;
                    if (intValue != 11454) {
                        i = 10;
                        if (intValue != 11459) {
                            i = 11;
                            if (intValue != 11468) {
                                if (intValue == 11487 || intValue == 20682 || intValue == 20697) {
                                    i = 27;
                                } else {
                                    indiaUpiFcsPinHandlerActivity.A39();
                                    return;
                                }
                            }
                        }
                    }
                    C36021jC.A02(indiaUpiFcsPinHandlerActivity, A0D, i);
                    return;
                }
            }
        }
        indiaUpiFcsPinHandlerActivity.A08.A05("Error code is null");
    }

    @Override // X.AbstractActivityC121545iU
    public void A37() {
        AaN();
        C36021jC.A01(this, 19);
    }

    @Override // X.AbstractActivityC121545iU
    public void A39() {
        AnonymousClass60V A03 = ((AbstractActivityC121545iU) this).A0A.A03(((AbstractActivityC121545iU) this).A06);
        A2r();
        if (A03.A00() == 0) {
            A03.A02();
        }
        String A01 = A03.A01(this);
        AnonymousClass2AC r0 = new AnonymousClass2AC();
        r0.A08 = A01;
        C12960it.A16(r0.A01(), this);
    }

    @Override // X.AbstractActivityC121545iU
    public void A3G(HashMap hashMap) {
        C16700pc.A0E(hashMap, 0);
        String A00 = C1308460e.A00("MPIN", hashMap);
        AnonymousClass1ZR r0 = this.A02;
        if (r0 == null) {
            throw C16700pc.A06("seqNumber");
        }
        Object obj = r0.A00;
        if (A00 != null && obj != null) {
            C17520qw[] r2 = new C17520qw[2];
            C17520qw.A00("mpin", A00, r2, 0);
            C17520qw.A00("npci_common_library_transaction_id", obj, r2, 1);
            Map A02 = C17530qx.A02(r2);
            C19750uc r1 = this.A04;
            if (r1 != null) {
                String str = this.A06;
                if (str == null) {
                    throw C16700pc.A06("fdsManagerId");
                }
                AnonymousClass17Q A002 = r1.A00(str);
                C16700pc.A0C(A002);
                AbstractC17770rM AGE = A002.A02().A0F.AGE("native_flow_npci_common_library");
                C16700pc.A0C(AGE);
                AGE.A03(A02);
                return;
            }
            throw C16700pc.A06("fdsManagerRegistry");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0082  */
    @Override // X.AnonymousClass6MS
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ARr(X.C452120p r11, java.lang.String r12) {
        /*
            r10 = this;
            r5 = r12
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            r3 = r10
            if (r0 != 0) goto L_0x0085
            X.1Zj r1 = r10.A08
            java.lang.String r0 = "onListKeys called"
            r1.A06(r0)
            X.C16700pc.A0C(r12)
            X.1Zc r2 = r10.A01
            java.lang.String r0 = "paymentBankAccount"
            r1 = 0
            if (r2 != 0) goto L_0x001e
            java.lang.RuntimeException r0 = X.C16700pc.A06(r0)
            throw r0
        L_0x001e:
            java.lang.String r6 = r2.A0B
            X.1ZR r0 = r10.A02
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = "seqNumber"
            java.lang.RuntimeException r0 = X.C16700pc.A06(r0)
            throw r0
        L_0x002c:
            java.lang.Object r7 = r0.A00
            java.lang.String r7 = (java.lang.String) r7
            X.1ZY r4 = r2.A08
            boolean r0 = r4 instanceof X.C119755f3
            if (r0 == 0) goto L_0x0043
            X.5f3 r4 = (X.C119755f3) r4
        L_0x0038:
            java.lang.String r1 = r10.A07
            if (r1 != 0) goto L_0x0045
            java.lang.String r0 = "pinOp"
            java.lang.RuntimeException r0 = X.C16700pc.A06(r0)
            throw r0
        L_0x0043:
            r4 = r1
            goto L_0x0038
        L_0x0045:
            int r0 = r1.hashCode()
            switch(r0) {
                case -2131583866: goto L_0x0061;
                case 110760: goto L_0x006e;
                case 74085029: goto L_0x0053;
                case 398918110: goto L_0x006b;
                case 1985322040: goto L_0x0056;
                default: goto L_0x004c;
            }
        L_0x004c:
            java.lang.String r0 = "Unexpected pin operation"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0053:
            java.lang.String r0 = "check_balance"
            goto L_0x0070
        L_0x0056:
            java.lang.String r0 = "set_pin"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004c
            r9 = 1
            goto L_0x0077
        L_0x0061:
            java.lang.String r0 = "change_pin"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004c
            r9 = 2
            goto L_0x0077
        L_0x006b:
            java.lang.String r0 = "check_pin"
            goto L_0x0070
        L_0x006e:
            java.lang.String r0 = "pay"
        L_0x0070:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004c
            r9 = 3
        L_0x0077:
            X.1ZR r0 = r2.A09
            if (r0 != 0) goto L_0x0082
            r8 = 0
        L_0x007c:
            java.lang.String r8 = (java.lang.String) r8
            r3.A3E(r4, r5, r6, r7, r8, r9)
            return
        L_0x0082:
            java.lang.Object r8 = r0.A00
            goto L_0x007c
        L_0x0085:
            if (r11 == 0) goto L_0x00ae
            int r2 = r11.A00
            r0 = 0
            java.lang.String r1 = "upi-list-keys"
            boolean r0 = X.AnonymousClass69E.A02(r10, r1, r2, r0)
            if (r0 != 0) goto L_0x00ae
            X.3Fv r0 = r10.A06
            boolean r0 = r0.A07(r1)
            if (r0 == 0) goto L_0x00af
            X.68y r0 = r10.A0B
            r0.A0C()
            r10.AaN()
            r0 = 2131890645(0x7f1211d5, float:1.9415988E38)
            r10.A2C(r0)
            X.5gK r0 = r10.A09
            r0.A00()
        L_0x00ae:
            return
        L_0x00af:
            X.1Zj r2 = r10.A08
            java.lang.String r0 = "onListKeys: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            if (r12 != 0) goto L_0x00ca
            r0 = 0
        L_0x00ba:
            r1.append(r0)
            java.lang.String r0 = " failed; ; showErrorAndFinish"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.A06(r0)
            r10.A39()
            return
        L_0x00ca:
            int r0 = r12.length()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.payments.phoenix.flowconfigurationservice.npci.IndiaUpiFcsPinHandlerActivity.ARr(X.20p, java.lang.String):void");
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r3) {
        throw new C87394Bi(C16700pc.A08("An operation is not implemented: ", "Not yet implemented"));
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Parcelable parcelableExtra = getIntent().getParcelableExtra("extra_bank_account");
        C16700pc.A0C(parcelableExtra);
        C16700pc.A0B(parcelableExtra);
        this.A01 = (C30861Zc) parcelableExtra;
        String stringExtra = getIntent().getStringExtra("extra_india_upi_pin_op");
        C16700pc.A0C(stringExtra);
        C16700pc.A0B(stringExtra);
        this.A07 = stringExtra;
        String stringExtra2 = getIntent().getStringExtra("extra_fds_manager_id");
        C16700pc.A0C(stringExtra2);
        C16700pc.A0B(stringExtra2);
        this.A06 = stringExtra2;
        String stringExtra3 = getIntent().getStringExtra("extra_fcs_observer_id");
        C16700pc.A0C(stringExtra3);
        C16700pc.A0B(stringExtra3);
        C49032Iw r0 = this.A00;
        if (r0 != null) {
            AnonymousClass4S1 r3 = new AnonymousClass4S1(this.A09, (C17120qI) r0.A00.A03.ALs.get(), stringExtra3);
            this.A03 = r3;
            r3.A01.A02(r3.A03).A00(new AbstractC50172Ok() { // from class: X.59w
                @Override // X.AbstractC50172Ok
                public final void APz(Object obj) {
                    AnonymousClass4S1 r2 = AnonymousClass4S1.this;
                    AnonymousClass5A3 r5 = (AnonymousClass5A3) obj;
                    C16700pc.A0F(r2, r5);
                    if (C16700pc.A0O(r2.A02, r5.A01)) {
                        switch (r5.A00.ordinal()) {
                            case 1:
                                AnonymousClass4WU r22 = r2.A00;
                                String str = r5.A02;
                                C16700pc.A0E(str, 0);
                                if (str.equals("send_fds_iq")) {
                                    IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity = r22.A00;
                                    indiaUpiFcsPinHandlerActivity.AaN();
                                    IndiaUpiFcsPinHandlerActivity.A0A(indiaUpiFcsPinHandlerActivity);
                                    return;
                                }
                                return;
                            case 2:
                                AnonymousClass4WU r32 = r2.A00;
                                String str2 = r5.A02;
                                Map map = r5.A03;
                                C16700pc.A0E(str2, 0);
                                if (str2.equals("send_fds_iq")) {
                                    IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity2 = r32.A00;
                                    indiaUpiFcsPinHandlerActivity2.AaN();
                                    IndiaUpiFcsPinHandlerActivity.A0B(indiaUpiFcsPinHandlerActivity2, map);
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }
            }, AnonymousClass5A3.class, r3);
            C14850m9 r02 = ((ActivityC13810kN) this).A0C;
            C14900mE r03 = ((ActivityC13810kN) this).A05;
            C15570nT r15 = ((ActivityC13790kL) this).A01;
            C17220qS r14 = ((AbstractActivityC121685jC) this).A0H;
            C18590sh r13 = ((AbstractActivityC121545iU) this).A0C;
            C17070qD r12 = ((AbstractActivityC121685jC) this).A0P;
            C21860y6 r11 = ((AbstractActivityC121685jC) this).A0I;
            C1308460e r10 = ((AbstractActivityC121665jA) this).A0A;
            C18610sj r9 = ((AbstractActivityC121685jC) this).A0M;
            AnonymousClass102 r8 = ((AbstractActivityC121545iU) this).A02;
            C17900ra r7 = ((AbstractActivityC121685jC) this).A0N;
            AnonymousClass6BE r6 = ((AbstractActivityC121665jA) this).A0D;
            C18640sm r4 = ((ActivityC13810kN) this).A07;
            C18650sn r32 = ((AbstractActivityC121685jC) this).A0K;
            C1329668y r2 = ((AbstractActivityC121665jA) this).A0B;
            ((AbstractActivityC121545iU) this).A09 = new C120525gK(this, r03, r15, r4, r8, r02, r14, r10, r2, r11, r32, r9, r7, r12, this, r6, ((AbstractActivityC121545iU) this).A0B, r13);
            this.A02 = new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, A2o(r2.A06()), "upiSequenceNumber");
            A2P(getString(R.string.register_wait_message));
            ((AbstractActivityC121545iU) this).A09.A00();
            return;
        }
        throw C16700pc.A06("fcsResourceExecutionCallbackHandlerFactory");
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog create;
        if (i != 27) {
            if (i != 28) {
                switch (i) {
                    case 10:
                        create = A32(new Runnable() { // from class: X.3kw
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiFcsPinHandlerActivity.A09(IndiaUpiFcsPinHandlerActivity.this);
                            }
                        }, getString(R.string.upi_check_balance_incorrect_pin_title), getString(R.string.upi_check_balance_incorrect_pin_message), i, R.string.payments_try_again, R.string.cancel);
                        break;
                    case 11:
                        break;
                    case 12:
                        create = A32(new Runnable() { // from class: X.3kv
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiFcsPinHandlerActivity.A02(IndiaUpiFcsPinHandlerActivity.this);
                            }
                        }, getString(R.string.upi_check_balance_no_pin_set_title), getString(R.string.upi_check_balance_no_pin_set_message), i, R.string.learn_more, R.string.ok);
                        break;
                    default:
                        create = super.onCreateDialog(i);
                        break;
                }
            }
            C30861Zc r0 = this.A01;
            if (r0 == null) {
                throw C16700pc.A06("paymentBankAccount");
            }
            create = A30(r0, i);
        } else {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.check_balance_balance_unavailable_message);
            A0S.A07(R.string.check_balance_balance_unavailable_title);
            C12970iu.A1L(A0S, this, 43, R.string.ok);
            create = A0S.create();
        }
        C16700pc.A0B(create);
        return create;
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass4S1 r2 = this.A03;
        if (r2 == null) {
            throw C16700pc.A06("fcsResourceExecutionCallbackHandler");
        }
        r2.A01.A02(r2.A03).A02(AnonymousClass5A3.class, r2);
    }
}
