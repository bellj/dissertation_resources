package com.whatsapp.documentpicker;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass2FL;
import X.AnonymousClass2GL;
import X.C14350lI;
import X.C22190yg;
import X.C26511Dt;
import X.C33421e0;
import X.C41511te;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.mediaview.PhotoView;

/* loaded from: classes2.dex */
public class DocumentPreviewActivity extends AnonymousClass2GL {
    public PhotoView A00;
    public C26511Dt A01;
    public C22190yg A02;
    public boolean A03;

    public DocumentPreviewActivity() {
        this(0);
    }

    public DocumentPreviewActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 67);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13790kL.A0e(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            this.A01 = (C26511Dt) A1M.A69.get();
            this.A02 = (C22190yg) A1M.AB6.get();
        }
    }

    @Override // X.AnonymousClass2GL, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Bitmap bitmap;
        super.onCreate(bundle);
        PhotoView photoView = new PhotoView(this);
        this.A00 = photoView;
        photoView.setId(R.id.document_preview);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1, 17);
        layoutParams.setMargins(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.gif_vide_view_bottom_margin));
        this.A00.setLayoutParams(layoutParams);
        byte[] A09 = this.A01.A09(C14350lI.A03((Uri) getIntent().getParcelableExtra("uri")), "application/pdf");
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (A09 != null) {
            bitmap = C41511te.A00(options, A09, 2000);
        } else {
            bitmap = null;
        }
        this.A00.setTag(getIntent().getParcelableExtra("uri"));
        this.A00.setImageBitmap(bitmap);
        this.A00.A05(bitmap);
        ((AnonymousClass2GL) this).A01.addView(this.A00, 0);
        AnonymousClass01d r1 = ((ActivityC13810kN) this).A08;
        Uri uri = (Uri) getIntent().getParcelableExtra("uri");
        AnonymousClass009.A05(uri);
        setTitle(C26511Dt.A04(uri, r1));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C33421e0 r2 = ((AnonymousClass2GL) this).A0B;
        if (r2 != null) {
            r2.A00.getViewTreeObserver().removeGlobalOnLayoutListener(r2.A01);
            r2.A05.A08();
            r2.A03.dismiss();
            ((AnonymousClass2GL) this).A0B = null;
        }
    }

    @Override // X.AnonymousClass2GL, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (getIntent().getParcelableExtra("uri") != null) {
            this.A00.setTag(getIntent().getParcelableExtra("uri"));
        }
    }
}
