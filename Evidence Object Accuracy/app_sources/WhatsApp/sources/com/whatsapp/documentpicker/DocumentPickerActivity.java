package com.whatsapp.documentpicker;

import X.AbstractC009504t;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02Q;
import X.AnonymousClass03M;
import X.AnonymousClass0Q3;
import X.AnonymousClass0QL;
import X.AnonymousClass11P;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1A1;
import X.AnonymousClass1J1;
import X.AnonymousClass23N;
import X.AnonymousClass2FL;
import X.AnonymousClass4XI;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15380n4;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C18000rk;
import X.C18640sm;
import X.C21270x9;
import X.C22190yg;
import X.C40691s6;
import X.C41691tw;
import X.C52832bl;
import X.C53992fv;
import X.C66673Om;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.documentpicker.DocumentPickerActivity;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class DocumentPickerActivity extends ActivityC13770kJ implements AnonymousClass03M {
    public int A00;
    public MenuItem A01;
    public View A02;
    public AbstractC009504t A03;
    public C15550nR A04;
    public C15610nY A05;
    public AnonymousClass1J1 A06;
    public C21270x9 A07;
    public AnonymousClass19D A08;
    public AnonymousClass11P A09;
    public AnonymousClass018 A0A;
    public C52832bl A0B;
    public AbstractC14640lm A0C;
    public C22190yg A0D;
    public AnonymousClass01H A0E;
    public AnonymousClass01H A0F;
    public String A0G;
    public ArrayList A0H;
    public List A0I;
    public List A0J;
    public boolean A0K;
    public boolean A0L;
    public final AnonymousClass02Q A0M;
    public final List A0N;

    @Override // X.AnonymousClass03M
    public void AS8(AnonymousClass0QL r1) {
    }

    public DocumentPickerActivity() {
        this(0);
        this.A0N = C12960it.A0l();
        this.A00 = 0;
        this.A0M = new C66673Om(this);
    }

    public DocumentPickerActivity(int i) {
        this.A0K = false;
        ActivityC13830kP.A1P(this, 66);
    }

    public static /* synthetic */ void A02(DocumentPickerActivity documentPickerActivity) {
        if (documentPickerActivity.A0B.getCount() == 0) {
            if (documentPickerActivity.A0I == null) {
                C12970iu.A1N(documentPickerActivity, R.id.search_no_matches, 8);
                C12970iu.A1N(documentPickerActivity, R.id.progress, 0);
            } else {
                ArrayList arrayList = documentPickerActivity.A0H;
                if (arrayList == null || arrayList.isEmpty()) {
                    TextView A0M = C12970iu.A0M(documentPickerActivity, R.id.search_no_matches);
                    A0M.setVisibility(0);
                    A0M.setText(R.string.no_documents_found);
                } else {
                    TextView A0M2 = C12970iu.A0M(documentPickerActivity, R.id.search_no_matches);
                    A0M2.setVisibility(0);
                    A0M2.setText(C12960it.A0X(documentPickerActivity, documentPickerActivity.A0G, C12970iu.A1b(), 0, R.string.search_no_results));
                }
                C12970iu.A1N(documentPickerActivity, R.id.progress, 8);
            }
            C12970iu.A1N(documentPickerActivity, 16908292, 0);
            return;
        }
        C12970iu.A1N(documentPickerActivity, 16908292, 8);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0K) {
            this.A0K = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A07 = C12970iu.A0W(A1M);
            this.A04 = C12960it.A0O(A1M);
            this.A05 = C12960it.A0P(A1M);
            this.A0A = C12960it.A0R(A1M);
            this.A08 = (AnonymousClass19D) A1M.ABo.get();
            this.A09 = (AnonymousClass11P) A1M.ABp.get();
            this.A0E = C18000rk.A00(A1M.ADw);
            this.A0F = C18000rk.A00(A1M.AID);
            this.A0D = (C22190yg) A1M.AB6.get();
        }
    }

    public final void A2g() {
        Intent A0E = C12990iw.A0E("android.intent.action.OPEN_DOCUMENT");
        A0E.addCategory("android.intent.category.OPENABLE");
        A0E.setType("*/*");
        A0E.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
        A2E(A0E, 1);
    }

    public final void A2h(Uri uri) {
        String str;
        File A0A = this.A0D.A0A(uri);
        if (A0A != null) {
            str = A0A.getPath();
        } else {
            str = null;
        }
        startActivityForResult(C12970iu.A0A().setClassName(getPackageName(), "com.whatsapp.documentpicker.DocumentPreviewActivity").putExtra("jid", C15380n4.A03(this.A0C)).putExtra("file_path", str).putExtra("uri", uri), 30);
    }

    public final void A2i(AnonymousClass4XI r10) {
        AbstractC009504t r0;
        List list = this.A0N;
        if (list.contains(r10)) {
            list.remove(r10);
            boolean isEmpty = list.isEmpty();
            r0 = this.A03;
            if (isEmpty) {
                r0.A05();
            }
            r0.A06();
        } else if (list.size() >= 30) {
            C14900mE r2 = ((ActivityC13810kN) this).A05;
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 30, 0);
            r2.A0E(getString(R.string.share_too_many_items_with_placeholder, objArr), 0);
        } else {
            list.add(r10);
            r0 = this.A03;
            r0.A06();
        }
        if (!list.isEmpty()) {
            AnonymousClass01d r5 = ((ActivityC13810kN) this).A08;
            Resources resources = getResources();
            int size = list.size();
            Object[] objArr2 = new Object[1];
            C12960it.A1P(objArr2, list.size(), 0);
            AnonymousClass23N.A00(this, r5, resources.getQuantityString(R.plurals.n_items_selected, size, objArr2));
        }
        this.A0B.notifyDataSetChanged();
    }

    public final void A2j(Collection collection) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            A0l.add(Uri.fromFile(((AnonymousClass4XI) it.next()).A02));
        }
        if (!((ActivityC13810kN) this).A0C.A07(1750) || A0l.size() != 1) {
            C12960it.A16(SendDocumentsConfirmationDialogFragment.A00(this.A0C, A0l, false), this);
        } else {
            A2h((Uri) A0l.get(0));
        }
    }

    @Override // X.AnonymousClass03M
    public AnonymousClass0QL AOi(Bundle bundle, int i) {
        return new C53992fv(this, ((ActivityC13810kN) this).A04, this.A0A, ((ActivityC13810kN) this).A0C);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        if (r6.isEmpty() != false) goto L_0x0012;
     */
    @Override // X.AnonymousClass03M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void AS2(X.AnonymousClass0QL r5, java.lang.Object r6) {
        /*
            r4 = this;
            java.util.List r6 = (java.util.List) r6
            r4.A0I = r6
            android.view.MenuItem r3 = r4.A01
            r2 = 1
            if (r3 == 0) goto L_0x0016
            if (r6 == 0) goto L_0x0012
            boolean r1 = r6.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r3.setVisible(r0)
        L_0x0016:
            java.lang.String r1 = r4.A0G
            X.2bl r0 = r4.A0B
            android.widget.Filter r0 = r0.getFilter()
            r0.filter(r1)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 19
            if (r1 < r0) goto L_0x003a
            java.util.List r0 = r4.A0I
            if (r0 == 0) goto L_0x0035
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x003a
            boolean r0 = r4.A0L
            if (r0 != 0) goto L_0x003a
        L_0x0035:
            r4.A0L = r2
            r4.A2g()
        L_0x003a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.documentpicker.DocumentPickerActivity.AS2(X.0QL, java.lang.Object):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A02(this, R.color.primary);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        super.AXD(r2);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri data;
        Uri uri;
        super.onActivityResult(i, i2, intent);
        if (i != 1) {
            if (i == 30 && i2 == -1) {
                setResult(i2, intent);
            } else {
                return;
            }
        } else if (i2 == -1) {
            try {
                ArrayList A0l = C12960it.A0l();
                ClipData clipData = intent.getClipData();
                boolean z = false;
                if (clipData != null) {
                    for (int i3 = 0; i3 < clipData.getItemCount(); i3++) {
                        ClipData.Item itemAt = clipData.getItemAt(i3);
                        if (!(itemAt == null || (uri = itemAt.getUri()) == null)) {
                            A0l.add(uri);
                        }
                    }
                }
                if (A0l.isEmpty() && (data = intent.getData()) != null) {
                    A0l.add(data);
                }
                if (!A0l.isEmpty()) {
                    Iterator it = A0l.iterator();
                    while (it.hasNext()) {
                        try {
                            grantUriPermission("com.whatsapp", (Uri) it.next(), 1);
                        } catch (SecurityException e) {
                            Log.w("docpicker/permission ", e);
                        }
                    }
                    if (!((ActivityC13810kN) this).A0C.A07(1750) || A0l.size() != 1) {
                        AbstractC14640lm r1 = this.A0C;
                        List list = this.A0I;
                        if (list == null || list.isEmpty()) {
                            z = true;
                        }
                        C12960it.A16(SendDocumentsConfirmationDialogFragment.A00(r1, A0l, z), this);
                        return;
                    }
                    A2h((Uri) A0l.get(0));
                    return;
                }
                return;
            } catch (IOException e2) {
                e2.printStackTrace();
                return;
            }
        } else if (i2 == 0) {
            List list2 = this.A0I;
            if (list2 != null && !list2.isEmpty()) {
                return;
            }
        } else {
            return;
        }
        finish();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (ActivityC13810kN.A1I(this)) {
            this.A0E.get();
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.documents);
        boolean z = true;
        C12970iu.A0N(this).A0M(true);
        AbstractC14640lm A0c = C12970iu.A0c(this);
        AnonymousClass009.A06(A0c, "rawJid is not a valid chat jid string");
        this.A0C = A0c;
        C14820m6 r0 = ((ActivityC13810kN) this).A09;
        this.A00 = r0.A00.getInt("document_picker_sort", this.A00);
        setContentView(R.layout.document_picker);
        this.A0B = new C52832bl(this);
        if (Build.VERSION.SDK_INT >= 19) {
            A2e().addHeaderView(getLayoutInflater().inflate(R.layout.document_picker_header, (ViewGroup) null, false));
        }
        A2f(this.A0B);
        A2e().setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.3O9
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                DocumentPickerActivity documentPickerActivity = DocumentPickerActivity.this;
                int headerViewsCount = i - documentPickerActivity.A2e().getHeaderViewsCount();
                if (headerViewsCount < 0) {
                    documentPickerActivity.A2g();
                    return;
                }
                AnonymousClass4XI r1 = (AnonymousClass4XI) documentPickerActivity.A0J.get(headerViewsCount);
                if (documentPickerActivity.A03 != null) {
                    documentPickerActivity.A2i(r1);
                    return;
                }
                try {
                    documentPickerActivity.A2j(Collections.singletonList(r1));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        A2e().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: X.3OI
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                DocumentPickerActivity documentPickerActivity = DocumentPickerActivity.this;
                int headerViewsCount = i - documentPickerActivity.A2e().getHeaderViewsCount();
                if (headerViewsCount >= 0) {
                    AnonymousClass4XI r1 = (AnonymousClass4XI) documentPickerActivity.A0J.get(headerViewsCount);
                    if (documentPickerActivity.A03 != null) {
                        documentPickerActivity.A2i(r1);
                    } else {
                        List list = documentPickerActivity.A0N;
                        list.clear();
                        list.add(r1);
                        AnonymousClass01d r6 = ((ActivityC13810kN) documentPickerActivity).A08;
                        Resources resources = documentPickerActivity.getResources();
                        int size = list.size();
                        Object[] objArr = new Object[1];
                        C12960it.A1P(objArr, list.size(), 0);
                        AnonymousClass23N.A00(documentPickerActivity, r6, resources.getQuantityString(R.plurals.n_items_selected, size, objArr));
                        documentPickerActivity.A03 = documentPickerActivity.A1W(documentPickerActivity.A0M);
                        documentPickerActivity.A0B.notifyDataSetChanged();
                        return true;
                    }
                }
                return true;
            }
        });
        if (bundle == null || !bundle.getBoolean("system_picker_auto_started")) {
            z = false;
        }
        this.A0L = z;
        new AnonymousClass0Q3(this, AHb()).A02(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x004b, code lost:
        if (r1 != false) goto L_0x004d;
     */
    @Override // X.ActivityC13790kL, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onCreateOptionsMenu(android.view.Menu r5) {
        /*
            r4 = this;
            android.view.MenuInflater r1 = r4.getMenuInflater()
            r0 = 2131623948(0x7f0e000c, float:1.8875062E38)
            r1.inflate(r0, r5)
            X.02i r0 = X.C12970iu.A0N(r4)
            android.content.Context r2 = r0.A02()
            r1 = 2131952002(0x7f130182, float:1.9540434E38)
            r0 = 0
            androidx.appcompat.widget.SearchView r3 = new androidx.appcompat.widget.SearchView
            r3.<init>(r2, r0, r1)
            r0 = 2131365738(0x7f0a0f6a, float:1.835135E38)
            android.widget.TextView r1 = X.C12960it.A0J(r3, r0)
            r0 = 2131100112(0x7f0601d0, float:1.7812596E38)
            X.C12960it.A0s(r4, r1, r0)
            r0 = 2131891448(0x7f1214f8, float:1.9417616E38)
            java.lang.String r0 = r4.getString(r0)
            r3.setQueryHint(r0)
            X.3Ox r0 = new X.3Ox
            r0.<init>(r4)
            r3.A0B = r0
            r0 = 2131364436(0x7f0a0a54, float:1.834871E38)
            android.view.MenuItem r2 = r5.findItem(r0)
            r4.A01 = r2
            java.util.List r0 = r4.A0I
            if (r0 == 0) goto L_0x004d
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x004e
        L_0x004d:
            r0 = 0
        L_0x004e:
            r2.setVisible(r0)
            android.view.MenuItem r0 = r4.A01
            r0.setActionView(r3)
            android.view.MenuItem r1 = r4.A01
            r0 = 10
            r1.setShowAsAction(r0)
            android.view.MenuItem r1 = r4.A01
            X.4mU r0 = new X.4mU
            r0.<init>(r4)
            r1.setOnActionExpandListener(r0)
            r0 = 2131100111(0x7f0601cf, float:1.7812594E38)
            int r2 = X.AnonymousClass00T.A00(r4, r0)
            android.view.MenuItem r0 = r4.A01
            android.graphics.drawable.Drawable r0 = r0.getIcon()
            android.graphics.drawable.Drawable r1 = X.AnonymousClass2GE.A04(r0, r2)
            android.view.MenuItem r0 = r4.A01
            r0.setIcon(r1)
            r0 = 2131364448(0x7f0a0a60, float:1.8348733E38)
            android.view.MenuItem r1 = r5.findItem(r0)
            android.graphics.drawable.Drawable r0 = r1.getIcon()
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A04(r0, r2)
            r1.setIcon(r0)
            boolean r0 = super.onCreateOptionsMenu(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.documentpicker.DocumentPickerActivity.onCreateOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A02(this.A02, this.A09);
            AnonymousClass1J1 r0 = this.A06;
            if (r0 != null) {
                r0.A00();
                this.A06 = null;
            }
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        SharedPreferences.Editor putInt;
        if (menuItem.getItemId() == R.id.menuitem_sort_by_name) {
            this.A00 = 0;
            putInt = C12960it.A08(((ActivityC13810kN) this).A09).putInt("document_picker_sort", 0);
        } else if (menuItem.getItemId() == R.id.menuitem_sort_by_date) {
            this.A00 = 1;
            putInt = C12960it.A08(((ActivityC13810kN) this).A09).putInt("document_picker_sort", 1);
        } else {
            if (menuItem.getItemId() == 16908332) {
                finish();
                return true;
            }
            return true;
        }
        putInt.apply();
        invalidateOptionsMenu();
        this.A0B.getFilter().filter(this.A0G);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A07(this.A09);
            ((AnonymousClass1A1) this.A0E.get()).A02(((ActivityC13810kN) this).A00);
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem findItem = menu.findItem(R.id.menuitem_sort_by_name);
        MenuItem findItem2 = menu.findItem(R.id.menuitem_sort_by_date);
        if (this.A00 == 0) {
            findItem.setChecked(true);
            return true;
        }
        findItem2.setChecked(true);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (ActivityC13810kN.A1I(this)) {
            boolean z = ((AnonymousClass1A1) this.A0E.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r15 = ((ActivityC13810kN) this).A0C;
                C14900mE r13 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
                C21270x9 r10 = this.A07;
                C15550nR r9 = this.A04;
                C15610nY r8 = this.A05;
                AnonymousClass018 r7 = this.A0A;
                Pair A00 = C40691s6.A00(this, view, this.A02, r13, r12, r9, r8, this.A06, r10, this.A08, this.A09, ((ActivityC13810kN) this).A09, r7, r15, r11, this.A0E, this.A0F, "document-picker-activity");
                this.A02 = (View) A00.first;
                this.A06 = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A09, this.A0E);
            }
            ((AnonymousClass1A1) this.A0E.get()).A01();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("system_picker_auto_started", this.A0L);
    }

    @Override // android.app.Activity, X.AbstractC13870kT
    public void startActivityForResult(Intent intent, int i) {
        try {
            super.startActivityForResult(intent, i);
        } catch (ActivityNotFoundException e) {
            Log.e("docpicker/pick-from-doc-provider ", e);
            ((ActivityC13810kN) this).A05.A07(R.string.activity_not_found, 0);
        }
    }

    /* loaded from: classes2.dex */
    public class SendDocumentsConfirmationDialogFragment extends Hilt_DocumentPickerActivity_SendDocumentsConfirmationDialogFragment {
        public C15550nR A00;
        public C15610nY A01;
        public C18640sm A02;
        public AnonymousClass01d A03;
        public AnonymousClass018 A04;
        public AnonymousClass19M A05;

        public static SendDocumentsConfirmationDialogFragment A00(AbstractC14640lm r4, ArrayList arrayList, boolean z) {
            SendDocumentsConfirmationDialogFragment sendDocumentsConfirmationDialogFragment = new SendDocumentsConfirmationDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("jid", r4.getRawString());
            A0D.putParcelableArrayList("uri_list", arrayList);
            A0D.putBoolean("finish_on_cancel", z);
            sendDocumentsConfirmationDialogFragment.A0U(A0D);
            return sendDocumentsConfirmationDialogFragment;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
            if (r13 <= 100) goto L_0x00d5;
         */
        @Override // androidx.fragment.app.DialogFragment
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.app.Dialog A1A(android.os.Bundle r23) {
            /*
            // Method dump skipped, instructions count: 400
            */
            throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.documentpicker.DocumentPickerActivity.SendDocumentsConfirmationDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
        }
    }
}
