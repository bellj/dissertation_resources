package com.whatsapp.shareinvitelink;

import X.AbstractActivityC42891w4;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC42911w6;
import X.AbstractC42921w7;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2V9;
import X.AnonymousClass3ZQ;
import X.AnonymousClass4AB;
import X.AnonymousClass4CK;
import X.C103514qs;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20710wC;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C42881w2;
import X.C49152Jn;
import X.C52132aI;
import X.C63623Ch;
import X.C72923fO;
import X.C83663xe;
import X.C83673xf;
import X.C83683xg;
import X.C88144El;
import X.EnumC49142Jm;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.growthlock.InviteLinkUnavailableDialogFragment;
import com.whatsapp.util.Log;
import java.util.EnumMap;

/* loaded from: classes2.dex */
public class ShareInviteLinkActivity extends AbstractActivityC42891w4 implements AbstractC42911w6, AbstractC42921w7 {
    public AnonymousClass2V9 A00;
    public AnonymousClass2V9 A01;
    public C83663xe A02;
    public C83683xg A03;
    public C83673xf A04;
    public C15550nR A05;
    public C15610nY A06;
    public C16120oU A07;
    public C20710wC A08;
    public C15580nU A09;
    public C17220qS A0A;
    public String A0B;
    public boolean A0C;
    public final BroadcastReceiver A0D;
    public final C42881w2 A0E;

    public ShareInviteLinkActivity() {
        this(0);
        this.A0D = new C72923fO(this);
        this.A0E = new C42881w2(this);
    }

    public ShareInviteLinkActivity(int i) {
        this.A0C = false;
        A0R(new C103514qs(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A07 = (C16120oU) r1.ANE.get();
            this.A0A = (C17220qS) r1.ABt.get();
            this.A05 = (C15550nR) r1.A45.get();
            this.A06 = (C15610nY) r1.AMe.get();
            this.A08 = (C20710wC) r1.A8m.get();
        }
    }

    public final void A2j() {
        String str;
        StringBuilder sb = new StringBuilder("invitelink/printlink/");
        sb.append(this.A0B);
        sb.append(" jid:");
        sb.append(this.A09);
        Log.i(sb.toString());
        if (this.A09 != null && this.A0B != null) {
            try {
                EnumMap enumMap = new EnumMap(AnonymousClass4AB.class);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("whatsapp://chat?code=");
                sb2.append(this.A0B);
                C63623Ch r5 = C49152Jn.A00(EnumC49142Jm.A03, sb2.toString(), enumMap).A04;
                C15370n3 A0A = this.A05.A0A(this.A09);
                if (A0A == null) {
                    str = "invitelink/print/no-contact";
                } else {
                    String string = getString(R.string.share_invite_link_qr_code, this.A06.A04(A0A));
                    PrintManager A00 = AnonymousClass01d.A00(this);
                    if (A00 == null) {
                        str = "invitelink/print/no-print-manager";
                    } else {
                        A00.print(string, new C52132aI(this, r5, ((ActivityC13810kN) this).A0B, string), null);
                        return;
                    }
                }
                Log.e(str);
            } catch (AnonymousClass4CK e) {
                Log.i("invitelink/", e);
            }
        }
    }

    public final void A2k(String str) {
        String obj;
        this.A0B = str;
        if (TextUtils.isEmpty(str)) {
            obj = null;
        } else {
            StringBuilder sb = new StringBuilder("https://chat.whatsapp.com/");
            sb.append(str);
            obj = sb.toString();
        }
        if (!TextUtils.isEmpty(str)) {
            ((AbstractActivityC42891w4) this).A01.setText(obj);
            boolean A0b = this.A08.A0b(this.A09);
            int i = R.string.share_invite_link_message;
            if (A0b) {
                i = R.string.share_invite_link_message_parent_group;
            }
            String string = getString(i, obj);
            C15370n3 A0A = this.A05.A0A(this.A09);
            if (A0A == null) {
                Log.e("invitelink/share/no-contact");
            } else {
                C83683xg r3 = this.A03;
                r3.A02 = string;
                r3.A01 = getString(R.string.share_invite_link_subject, this.A06.A04(A0A));
                this.A03.A00 = getString(R.string.share_invite_link_via);
            }
            this.A04.A00 = string;
            this.A02.A00 = obj;
            return;
        }
        A2l(false);
        ((AbstractActivityC42891w4) this).A01.setText(" \n ");
    }

    public final void A2l(boolean z) {
        ((AbstractActivityC42891w4) this).A01.setEnabled(z);
        ((AnonymousClass2V9) this.A02).A00.setEnabled(z);
        this.A01.A00.setEnabled(z);
        ((AnonymousClass2V9) this.A03).A00.setEnabled(z);
        this.A00.A00.setEnabled(z);
        ((AnonymousClass2V9) this.A04).A00.setEnabled(z);
    }

    public final void A2m(boolean z) {
        StringBuilder sb = new StringBuilder("invitelink/sendgetlink/recreate:");
        sb.append(z);
        Log.i(sb.toString());
        if (z) {
            A2l(false);
            A1g(true);
        }
        AnonymousClass3ZQ r1 = new AnonymousClass3ZQ(((ActivityC13810kN) this).A05, this.A0A, this, z);
        C15580nU r0 = this.A09;
        AnonymousClass009.A05(r0);
        r1.A00(r0);
    }

    @Override // X.AbstractC42921w7
    public void ARo(int i, String str, boolean z) {
        A2l(true);
        A1g(false);
        if (str != null) {
            StringBuilder sb = new StringBuilder("invitelink/gotcode/");
            sb.append(str);
            sb.append(" recreate:");
            sb.append(z);
            Log.i(sb.toString());
            C20710wC r0 = this.A08;
            r0.A0x.put(this.A09, str);
            A2k(str);
            if (z) {
                Ado(R.string.reset_link_complete);
                return;
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("invitelink/failed/");
        sb2.append(i);
        Log.i(sb2.toString());
        if (i == 436) {
            Adm(InviteLinkUnavailableDialogFragment.A00(true, true));
            C20710wC r02 = this.A08;
            r02.A0x.remove(this.A09);
            A2k(null);
            return;
        }
        ((ActivityC13810kN) this).A05.A07(C88144El.A00(i, this.A08.A0b(this.A09)), 0);
        if (TextUtils.isEmpty(this.A0B)) {
            finish();
        }
    }

    @Override // X.AbstractC42911w6
    public void Aat() {
        A2m(true);
    }

    @Override // X.AbstractActivityC42891w4, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.share_invite_link_title);
        A2i();
        C83673xf A2h = A2h();
        this.A04 = A2h;
        A2h.A01 = new RunnableBRunnable0Shape7S0200000_I0_7(this, 45, 4);
        C83663xe A2f = A2f();
        this.A02 = A2f;
        A2f.A01 = new RunnableBRunnable0Shape7S0200000_I0_7(this, 45, 1);
        C83683xg A2g = A2g();
        this.A03 = A2g;
        ((AnonymousClass2V9) A2g).A01 = new RunnableBRunnable0Shape7S0200000_I0_7(this, 45, 3);
        AnonymousClass2V9 r1 = new AnonymousClass2V9();
        this.A00 = r1;
        r1.A00 = A2e();
        this.A00.A00(new ViewOnClickCListenerShape4S0100000_I0_4(this, 33), getString(R.string.settings_qr), R.drawable.ic_scan_qr);
        this.A00.A00.setVisibility(0);
        AnonymousClass2V9 r12 = new AnonymousClass2V9();
        this.A01 = r12;
        r12.A00 = A2e();
        this.A01.A00(new ViewOnClickCListenerShape4S0100000_I0_4(this, 32), getString(R.string.revoke_invite_link), R.drawable.ic_revoke_invite);
        C15580nU A04 = C15580nU.A04(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A04);
        this.A09 = A04;
        boolean A0b = this.A08.A0b(A04);
        TextView textView = (TextView) findViewById(R.id.share_link_description);
        int i = R.string.invite_link_description;
        if (A0b) {
            i = R.string.invite_link_description_parent_group;
        }
        textView.setText(i);
        if (this.A05.A0A(this.A09) == null) {
            StringBuilder sb = new StringBuilder("invitelink/sharelink/no-contact ");
            sb.append(this.A09);
            Log.e(sb.toString());
            finish();
            return;
        }
        A2m(false);
        if (Build.VERSION.SDK_INT >= 18) {
            registerReceiver(this.A0D, new IntentFilter("android.nfc.action.ADAPTER_STATE_CHANGED"));
        }
        C20710wC r0 = this.A08;
        r0.A0e.A03(this.A0E);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (NfcAdapter.getDefaultAdapter(this) != null) {
            menu.add(0, R.id.menuitem_write_tag, 0, R.string.write_nfc_tag);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= 18) {
            unregisterReceiver(this.A0D);
        }
        C20710wC r0 = this.A08;
        r0.A0e.A04(this.A0E);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_print) {
            A2j();
        } else if (itemId != R.id.menuitem_write_tag) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            StringBuilder sb = new StringBuilder("invitelink/writetag/");
            sb.append(this.A0B);
            sb.append(" jid:");
            sb.append(this.A09);
            Log.i(sb.toString());
            if (!(this.A09 == null || (str = this.A0B) == null)) {
                Intent intent = new Intent();
                intent.setClassName(getPackageName(), "com.whatsapp.writenfctag.WriteNfcTagActivity");
                intent.putExtra("mime", "application/com.whatsapp.join");
                intent.putExtra("data", str);
                startActivity(intent);
                return true;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0014, code lost:
        if (r1 == false) goto L_0x0016;
     */
    @Override // android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onPrepareOptionsMenu(android.view.Menu r4) {
        /*
            r3 = this;
            r0 = 2131364467(0x7f0a0a73, float:1.8348772E38)
            android.view.MenuItem r2 = r4.findItem(r0)
            if (r2 == 0) goto L_0x001a
            android.nfc.NfcAdapter r0 = android.nfc.NfcAdapter.getDefaultAdapter(r3)
            if (r0 == 0) goto L_0x0016
            boolean r1 = r0.isEnabled()
            r0 = 1
            if (r1 != 0) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            r2.setEnabled(r0)
        L_0x001a:
            boolean r0 = super.onPrepareOptionsMenu(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.shareinvitelink.ShareInviteLinkActivity.onPrepareOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C20710wC r0 = this.A08;
        A2k((String) r0.A0x.get(this.A09));
    }
}
