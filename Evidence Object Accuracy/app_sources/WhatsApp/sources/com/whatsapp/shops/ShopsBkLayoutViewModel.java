package com.whatsapp.shops;

import X.AbstractC74513iC;
import X.AnonymousClass009;
import X.AnonymousClass01H;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C18640sm;
import X.C27691It;
import X.C91064Qh;
import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes4.dex */
public class ShopsBkLayoutViewModel extends AbstractC74513iC {
    public final C18640sm A00;
    public final C27691It A01 = C13000ix.A03();
    public final C27691It A02 = C13000ix.A03();

    public ShopsBkLayoutViewModel(C18640sm r2, AnonymousClass01H r3) {
        super(r3);
        this.A00 = r2;
    }

    @Override // X.AbstractC74513iC
    public boolean A04(C91064Qh r5) {
        int i = r5.A00;
        if (i != 1) {
            if (i == 2) {
                Intent A0A = C12970iu.A0A();
                A0A.putExtra("error_code", 475);
                this.A01.A0B(A0A);
                return false;
            } else if (!(i == 3 || i == 4 || i == 6 || i == 7)) {
                AnonymousClass009.A07("BkLayoutViewModel: invalid error status");
                return false;
            }
        }
        Log.e("BkLayoutViewModel: layout fetch error");
        boolean A0B = this.A00.A0B();
        int i2 = R.string.no_internet_message;
        if (A0B) {
            i2 = R.string.error_invalid_link;
        }
        Log.e("BkLayoutViewModel: layout fetch error");
        C12960it.A1A(this.A02, i2);
        return false;
    }
}
