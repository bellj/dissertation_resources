package com.whatsapp.shops;

import X.AnonymousClass015;
import X.AnonymousClass18X;
import X.C16120oU;

/* loaded from: classes4.dex */
public class ShopsProductPreviewFragmentViewModel extends AnonymousClass015 {
    public final C16120oU A00;
    public final AnonymousClass18X A01;

    public ShopsProductPreviewFragmentViewModel(C16120oU r1, AnonymousClass18X r2) {
        this.A01 = r2;
        this.A00 = r1;
    }
}
