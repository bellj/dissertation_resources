package com.whatsapp.shops;

import X.AbstractC74513iC;
import X.C117295Zj;
import X.C12960it;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.wabloks.base.BkFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragmentViewModel;

/* loaded from: classes4.dex */
public abstract class ShopsBkFragment extends BkFragment {
    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        ShopsBkLayoutViewModel shopsBkLayoutViewModel = (ShopsBkLayoutViewModel) ((BkFragment) this).A05;
        if (((AbstractC74513iC) shopsBkLayoutViewModel).A01) {
            shopsBkLayoutViewModel.A01.A04(A0G());
            return;
        }
        throw C12960it.A0U("BkLayoutViewModel must be initialized");
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        ShopsBkLayoutViewModel shopsBkLayoutViewModel = (ShopsBkLayoutViewModel) ((BkFragment) this).A05;
        if (((AbstractC74513iC) shopsBkLayoutViewModel).A01) {
            C117295Zj.A0s(A0G(), shopsBkLayoutViewModel.A01, this, 162);
            return;
        }
        throw C12960it.A0U("BkLayoutViewModel must be initialized");
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public Class A19() {
        if (!(this instanceof PrivacyNoticeFragment)) {
            return ShopsBkLayoutViewModel.class;
        }
        return PrivacyNoticeFragmentViewModel.class;
    }
}
