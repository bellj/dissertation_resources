package com.whatsapp.shops;

import X.AnonymousClass009;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass18U;
import X.C123955oD;
import X.C127795v4;
import X.C12960it;
import X.C16120oU;
import X.C253619c;
import X.RunnableC135066Ha;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.shops.ShopsProductPreviewFragment;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedList;

/* loaded from: classes4.dex */
public class ShopsProductPreviewFragment extends Hilt_ShopsProductPreviewFragment {
    public FrameLayout A00;
    public ConstraintLayout A01;
    public ShimmerFrameLayout A02;
    public AnonymousClass18U A03;
    public C253619c A04;
    public C16120oU A05;
    public ShopsProductPreviewFragmentViewModel A06;
    public C127795v4 A07;
    public Runnable A08;
    public String A09;
    public final Handler A0A = new Handler();

    public static ShopsProductPreviewFragment A00(String str) {
        ShopsProductPreviewFragment shopsProductPreviewFragment = new ShopsProductPreviewFragment();
        Bundle A03 = shopsProductPreviewFragment.A03();
        A03.putString("screen_name", "com.bloks.www.minishops.whatsapp.products_preview_h_scroll");
        A03.putString("shopUrl", str);
        return shopsProductPreviewFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.fragment_shops_product_preview);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        String string = A03().getString("shopUrl");
        AnonymousClass009.A05(string);
        this.A09 = string;
        this.A06 = (ShopsProductPreviewFragmentViewModel) new AnonymousClass02A(this).A00(ShopsProductPreviewFragmentViewModel.class);
    }

    @Override // com.whatsapp.shops.ShopsBkFragment, com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        A05();
        this.A02 = (ShimmerFrameLayout) AnonymousClass028.A0D(view, R.id.shimmer_container);
        this.A01 = (ConstraintLayout) AnonymousClass028.A0D(view, R.id.placeholder_container);
        AnonymousClass028.A0D(view, R.id.see_all).setOnClickListener(new C123955oD(this));
        this.A00 = (FrameLayout) view.findViewById(R.id.bloks_dialogfragment);
        RunnableC135066Ha r3 = new Runnable() { // from class: X.6Ha
            public static Integer A00(AbstractMap abstractMap, int i) {
                Integer valueOf = Integer.valueOf(i);
                if (!abstractMap.containsKey(valueOf)) {
                    abstractMap.put(valueOf, new AnonymousClass0SY());
                }
                return valueOf;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShopsProductPreviewFragment shopsProductPreviewFragment = ShopsProductPreviewFragment.this;
                int width = shopsProductPreviewFragment.A01.getWidth();
                int height = shopsProductPreviewFragment.A01.getHeight();
                Context A01 = shopsProductPreviewFragment.A01();
                int A012 = AnonymousClass3G9.A01(A01, 8.0f);
                LinkedList linkedList = new LinkedList();
                int i = A012 << 1;
                int min = Math.min((width - i) / 3, height - i);
                int i2 = 0;
                int i3 = 0;
                do {
                    View view2 = new View(A01);
                    view2.setBackgroundResource(R.drawable.rounded_grey_box);
                    view2.setId(AnonymousClass028.A02());
                    shopsProductPreviewFragment.A01.addView(view2);
                    linkedList.add(view2);
                    i3++;
                } while (i3 < 3);
                do {
                    int id = C117325Zm.A01(linkedList, i2).getId();
                    AnonymousClass0U6 r32 = new AnonymousClass0U6();
                    r32.A02(id).A02.A0c = min;
                    r32.A02(id).A02.A0a = min;
                    HashMap hashMap = r32.A00;
                    AnonymousClass0SO r0 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                    r0.A0m = R.id.placeholder_container;
                    r0.A0l = -1;
                    r0.A08 = -1;
                    AnonymousClass0SO r02 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                    r02.A0A = R.id.placeholder_container;
                    r02.A0B = -1;
                    r02.A08 = -1;
                    if (i2 == 0) {
                        AnonymousClass0SO r03 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r03.A0j = R.id.placeholder_container;
                        r03.A0i = -1;
                        int id2 = C117325Zm.A01(linkedList, 1).getId();
                        AnonymousClass0SO r04 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r04.A0I = id2;
                        r04.A0H = -1;
                        r32.A02(id).A02.A0U = 1;
                    } else if (i2 == 2) {
                        int id3 = C117325Zm.A01(linkedList, 1).getId();
                        AnonymousClass0SO r05 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r05.A0i = id3;
                        r05.A0j = -1;
                        AnonymousClass0SO r06 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r06.A0H = R.id.placeholder_container;
                        r06.A0I = -1;
                    } else {
                        int id4 = C117325Zm.A01(linkedList, i2 - 1).getId();
                        AnonymousClass0SO r07 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r07.A0i = id4;
                        r07.A0j = -1;
                        int id5 = C117325Zm.A01(linkedList, i2 + 1).getId();
                        AnonymousClass0SO r08 = ((AnonymousClass0SY) hashMap.get(A00(hashMap, id))).A02;
                        r08.A0I = id5;
                        r08.A0H = -1;
                    }
                    ConstraintLayout constraintLayout = shopsProductPreviewFragment.A01;
                    r32.A04(constraintLayout);
                    constraintLayout.A0B = null;
                    constraintLayout.requestLayout();
                    i2++;
                } while (i2 < 3);
                shopsProductPreviewFragment.A02.A02();
            }
        };
        this.A08 = r3;
        this.A0A.postDelayed(r3, 200);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public int A18() {
        return R.id.bk_container;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public void A1A() {
        Runnable runnable = this.A08;
        if (runnable != null) {
            this.A0A.removeCallbacks(runnable);
        }
        this.A02.A03();
        this.A02.setVisibility(8);
    }
}
