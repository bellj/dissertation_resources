package com.whatsapp.support;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC33561eV;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15610nY;
import X.C16590pI;
import X.C241814n;
import X.C254119h;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ReportSpamDialogFragment extends Hilt_ReportSpamDialogFragment {
    public C14900mE A00;
    public C15450nH A01;
    public C15550nR A02;
    public C15610nY A03;
    public C254119h A04;
    public C16590pI A05;
    public C241814n A06;
    public AbstractC15340mz A07;
    public AbstractC33561eV A08;
    public AbstractC14440lR A09;

    public static ReportSpamDialogFragment A00(AbstractC14640lm r3, UserJid userJid, AbstractC33561eV r5, String str, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        ReportSpamDialogFragment reportSpamDialogFragment = new ReportSpamDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", r3.getRawString());
        if (userJid != null) {
            bundle.putString("userJid", userJid.getRawString());
        }
        if (str != null) {
            bundle.putString("flow", str);
        }
        bundle.putBoolean("hasLoggedInPairedDevices", z);
        bundle.putInt("upsellAction", i);
        bundle.putBoolean("upsellCheckboxActionDefault", z2);
        bundle.putBoolean("shouldDeleteChatOnBlock", z3);
        bundle.putBoolean("shouldOpenHomeScreenAction", z4);
        reportSpamDialogFragment.A08 = r5;
        reportSpamDialogFragment.A0U(bundle);
        return reportSpamDialogFragment;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0170  */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r20) {
        /*
        // Method dump skipped, instructions count: 455
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.support.ReportSpamDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }
}
