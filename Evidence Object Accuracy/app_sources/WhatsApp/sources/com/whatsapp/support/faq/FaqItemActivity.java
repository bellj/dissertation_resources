package com.whatsapp.support.faq;

import X.AbstractC15460nI;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass1MK;
import X.AnonymousClass2FL;
import X.AnonymousClass3FC;
import X.C12960it;
import X.C12970iu;
import X.C17070qD;
import X.C25771At;
import X.C52662bT;
import X.C73513gM;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class FaqItemActivity extends ActivityC13790kL {
    public long A00;
    public long A01;
    public long A02;
    public C25771At A03;
    public C17070qD A04;
    public AnonymousClass3FC A05;
    public boolean A06;
    public final WebViewClient A07;

    public FaqItemActivity() {
        this(0);
        this.A07 = new C52662bT(this);
    }

    public FaqItemActivity(int i) {
        this.A06 = false;
        ActivityC13830kP.A1P(this, 129);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A03 = (C25771At) A1M.A7p.get();
            this.A04 = (C17070qD) A1M.AFC.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        ActivityC13790kL.A0k(this);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass3FC r0 = this.A05;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.search_help_center);
        A1U().A0M(true);
        setContentView(R.layout.faq_item);
        A1U().A0I(getIntent().getStringExtra("title"));
        String stringExtra = getIntent().getStringExtra("content");
        String stringExtra2 = getIntent().getStringExtra("url");
        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.setWebViewClient(this.A07);
        webView.loadDataWithBaseURL(stringExtra2, stringExtra, "text/html", AnonymousClass01V.A08, null);
        this.A00 = getIntent().getLongExtra("article_id", -1);
        this.A02 = 0;
        String stringExtra3 = getIntent().getStringExtra("contact_us_context");
        if (!getIntent().getBooleanExtra("show_contact_support_button", false)) {
            return;
        }
        if (!AnonymousClass1MK.A00(stringExtra3) || !((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0q)) {
            String stringExtra4 = getIntent().getStringExtra("contact_us_context");
            View findViewById = findViewById(R.id.bottom_button_container);
            RunnableBRunnable0Shape1S1100000_I1 runnableBRunnable0Shape1S1100000_I1 = new RunnableBRunnable0Shape1S1100000_I1(4, stringExtra4, this);
            AnonymousClass3FC r10 = new AnonymousClass3FC(webView, findViewById, getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation));
            this.A05 = r10;
            r10.A02(this, new C73513gM(this, runnableBRunnable0Shape1S1100000_I1), C12970iu.A0M(this, R.id.does_not_match_button), getString(R.string.does_not_match_button), R.style.FaqInlineLink);
            C12960it.A11(this.A05.A01, runnableBRunnable0Shape1S1100000_I1, 30);
            findViewById.setVisibility(0);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A02 += System.currentTimeMillis() - this.A01;
        this.A01 = System.currentTimeMillis();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A01 = System.currentTimeMillis();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        ActivityC13790kL.A0k(this);
    }
}
