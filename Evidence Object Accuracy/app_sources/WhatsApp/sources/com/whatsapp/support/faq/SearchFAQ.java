package com.whatsapp.support.faq;

import X.AbstractC15460nI;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.AnonymousClass1MH;
import X.AnonymousClass1MI;
import X.AnonymousClass1MK;
import X.AnonymousClass3FC;
import X.AnonymousClass43T;
import X.AnonymousClass4S6;
import X.C100534m4;
import X.C16120oU;
import X.C254819o;
import X.C52692bW;
import X.C73523gN;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class SearchFAQ extends AnonymousClass1MH implements AnonymousClass1MI {
    public int A00;
    public AnonymousClass19Y A01;
    public C16120oU A02;
    public AnonymousClass11G A03;
    public C254819o A04;
    public AnonymousClass3FC A05;
    public String A06;
    public String A07;
    public String A08;
    public ArrayList A09;
    public HashMap A0A;
    public HashSet A0B;
    public List A0C;

    public final void A2g(int i) {
        AnonymousClass43T r3 = new AnonymousClass43T();
        r3.A00 = Integer.valueOf(i);
        r3.A01 = this.A07;
        r3.A02 = ((ActivityC13830kP) this).A01.A06();
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(this, 26, r3));
    }

    public final void A2h(AnonymousClass4S6 r9) {
        HashSet hashSet = this.A0B;
        String str = r9.A03;
        hashSet.add(str);
        String str2 = r9.A02;
        String str3 = r9.A01;
        long j = r9.A00;
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.support.faq.FaqItemActivity");
        intent.putExtra("title", str2);
        intent.putExtra("content", str3);
        intent.putExtra("url", str);
        intent.putExtra("article_id", j);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override // X.AnonymousClass1MI
    public void AV0(boolean z) {
        A2g(3);
        if (z) {
            setResult(-1);
            finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            long longExtra = intent.getLongExtra("total_time_spent", 0);
            long longExtra2 = intent.getLongExtra("article_id", -1);
            HashMap hashMap = this.A0A;
            Long valueOf = Long.valueOf(longExtra2);
            if (hashMap.containsKey(valueOf)) {
                longExtra += ((Number) this.A0A.get(valueOf)).longValue();
            }
            this.A0A.put(valueOf, Long.valueOf(longExtra));
            TextUtils.join(", ", this.A0A.entrySet());
            Iterator it = this.A0A.values().iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2g(2);
        super.onBackPressed();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A05.A00();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Runnable runnable;
        super.onCreate(bundle);
        int i = 0;
        boolean booleanExtra = getIntent().getBooleanExtra("com.whatsapp.support.faq.SearchFAQ.usePaymentsFlow", false);
        setTitle(R.string.search_help_center);
        A1U().A0M(true);
        setContentView(R.layout.search_faq);
        this.A0B = new HashSet();
        if (bundle != null) {
            String[] stringArray = bundle.getStringArray("FaqItemsReadTitles");
            if (stringArray != null) {
                Collections.addAll(this.A0B, stringArray);
            }
            if (bundle.containsKey("timeSpentPerArticle")) {
                HashMap hashMap = (HashMap) bundle.getSerializable("timeSpentPerArticle");
                this.A0A = hashMap;
                hashMap.size();
            }
        }
        Intent intent = getIntent();
        this.A06 = intent.getStringExtra("com.whatsapp.support.faq.SearchFAQ.from");
        ArrayList arrayList = new ArrayList();
        if (this.A0A == null) {
            this.A0A = new HashMap();
        }
        int intExtra = intent.getIntExtra("com.whatsapp.support.faq.SearchFAQ.count", 0);
        this.A00 = intExtra;
        if (booleanExtra) {
            ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("payments_support_faqs");
            ArrayList parcelableArrayListExtra2 = intent.getParcelableArrayListExtra("payments_support_topics");
            Bundle bundleExtra = intent.getBundleExtra("describe_problem_bundle");
            Iterator it = parcelableArrayListExtra.iterator();
            while (it.hasNext()) {
                C100534m4 r0 = (C100534m4) it.next();
                arrayList.add(new AnonymousClass4S6(r0.A02, r0.A00, r0.A03, Long.parseLong(r0.A01)));
            }
            runnable = new RunnableBRunnable0Shape1S0300000_I0_1(this, parcelableArrayListExtra2, bundleExtra, 49);
        } else {
            this.A07 = intent.getStringExtra("com.whatsapp.support.faq.SearchFAQ.problem");
            this.A08 = intent.getStringExtra("com.whatsapp.support.faq.SearchFAQ.status");
            this.A09 = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
            String[] stringArrayExtra = intent.getStringArrayExtra("com.whatsapp.support.faq.SearchFAQ.additionalDetails");
            if (stringArrayExtra != null) {
                ArrayList arrayList2 = new ArrayList();
                for (String str : stringArrayExtra) {
                    String[] split = str.split(":");
                    if (split.length == 2) {
                        arrayList2.add(new Pair(split[0], split[1]));
                    }
                }
                this.A0C = arrayList2;
            }
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.titles");
            ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.descriptions");
            ArrayList<String> stringArrayListExtra3 = intent.getStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.urls");
            ArrayList<String> stringArrayListExtra4 = intent.getStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.ids");
            if (!(stringArrayListExtra == null || stringArrayListExtra2 == null || stringArrayListExtra3 == null || stringArrayListExtra4 == null)) {
                if (stringArrayListExtra.size() < intExtra) {
                    intExtra = stringArrayListExtra.size();
                }
                if (stringArrayListExtra2.size() < intExtra) {
                    intExtra = stringArrayListExtra2.size();
                }
                if (stringArrayListExtra3.size() < intExtra) {
                    intExtra = stringArrayListExtra3.size();
                }
                if (stringArrayListExtra4.size() < intExtra) {
                    intExtra = stringArrayListExtra4.size();
                }
                for (int i2 = 0; i2 < intExtra; i2++) {
                    long parseLong = Long.parseLong(stringArrayListExtra4.get(i2));
                    stringArrayListExtra.get(i2);
                    stringArrayListExtra3.get(i2);
                    arrayList.add(new AnonymousClass4S6(stringArrayListExtra.get(i2), stringArrayListExtra2.get(i2), stringArrayListExtra3.get(i2), parseLong));
                }
            }
            runnable = new RunnableBRunnable0Shape8S0200000_I0_8(this, 25, intent);
        }
        ListAdapter r8 = new C52692bW(this, this, arrayList);
        ListView A2e = A2e();
        LayoutInflater A01 = AnonymousClass01d.A01(this);
        AnonymousClass009.A05(A01);
        A2e.addHeaderView(A01.inflate(R.layout.search_faq_header, (ViewGroup) null), null, false);
        A2f(r8);
        registerForContextMenu(A2e);
        if (arrayList.size() == 1) {
            A2h((AnonymousClass4S6) arrayList.get(0));
        }
        View findViewById = findViewById(R.id.bottom_button_container);
        AnonymousClass3FC r02 = new AnonymousClass3FC(A2e, findViewById, getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation));
        this.A05 = r02;
        r02.A00();
        this.A05.A02(this, new C73523gN(this, runnable), (TextView) findViewById(R.id.does_not_match_button), getString(R.string.does_not_match_button), R.style.FaqInlineLink);
        this.A05.A01.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(runnable, 2));
        if (AnonymousClass1MK.A00(this.A06) && ((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A0q)) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            A2g(2);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            HashSet hashSet = this.A0B;
            if (hashSet != null && hashSet.size() > 0) {
                bundle.putStringArray("FaqItemsReadTitles", (String[]) this.A0B.toArray(new String[0]));
            }
            HashMap hashMap = this.A0A;
            if (hashMap != null && hashMap.size() > 0) {
                bundle.putSerializable("timeSpentPerArticle", hashMap);
            }
        }
        super.onSaveInstanceState(bundle);
    }
}
