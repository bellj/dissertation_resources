package com.whatsapp.support;

import X.AbstractC005102i;
import X.AbstractC15460nI;
import X.AbstractC28901Pl;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass10G;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.AnonymousClass1MI;
import X.AnonymousClass1MK;
import X.AnonymousClass23P;
import X.AnonymousClass23Q;
import X.AnonymousClass2UZ;
import X.AnonymousClass3FC;
import X.AnonymousClass43T;
import X.AnonymousClass47O;
import X.AnonymousClass5X2;
import X.C16120oU;
import X.C17050qB;
import X.C17070qD;
import X.C18790t3;
import X.C22190yg;
import X.C22650zQ;
import X.C252018m;
import X.C254819o;
import X.C39351pj;
import X.C627338j;
import X.C627638m;
import X.C73483gJ;
import X.C91934Tu;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class DescribeProblemActivity extends AnonymousClass23P implements AnonymousClass23Q, AnonymousClass1MI {
    public int A00;
    public Uri A01;
    public EditText A02;
    public AnonymousClass10G A03;
    public AnonymousClass19Y A04;
    public C18790t3 A05;
    public C17050qB A06;
    public C16120oU A07;
    public AnonymousClass11G A08;
    public C627638m A09;
    public WhatsAppLibLoader A0A;
    public AnonymousClass5X2 A0B;
    public C17070qD A0C;
    public C254819o A0D;
    public AnonymousClass3FC A0E;
    public C252018m A0F;
    public C22650zQ A0G;
    public C22190yg A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public boolean A0L;
    public final Uri[] A0M = new Uri[3];

    public final String A2e() {
        ArrayList<String> stringArrayListExtra;
        if (!getIntent().hasExtra("com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicTitles") || (stringArrayListExtra = getIntent().getStringArrayListExtra("com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicTitles")) == null || stringArrayListExtra.isEmpty()) {
            return this.A02.getText().toString().trim();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.payments_support_email_topic_prefix));
        sb.append(" ");
        StringBuilder sb2 = new StringBuilder(sb.toString());
        for (int i = 0; i < stringArrayListExtra.size(); i++) {
            sb2.append(stringArrayListExtra.get(i));
            if (i < stringArrayListExtra.size() - 1) {
                sb2.append(", ");
            }
        }
        StringBuilder sb3 = new StringBuilder("\n\n");
        sb3.append(this.A02.getText().toString().trim());
        sb2.append(sb3.toString());
        return sb2.toString();
    }

    public final void A2f() {
        List list;
        A2g(3, A2e());
        C254819o r4 = this.A0D;
        String str = this.A0J;
        String str2 = this.A0I;
        String str3 = this.A0K;
        String A2e = A2e();
        Uri[] uriArr = this.A0M;
        AnonymousClass5X2 r0 = this.A0B;
        if (r0 != null) {
            list = r0.AF3();
        } else {
            list = null;
        }
        ArrayList arrayList = new ArrayList();
        for (Uri uri : uriArr) {
            if (uri != null) {
                arrayList.add(uri);
            }
        }
        r4.A01(this, null, str, A2e, str2, str3, arrayList, list, true);
    }

    public final void A2g(int i, String str) {
        AnonymousClass43T r1 = new AnonymousClass43T();
        r1.A00 = Integer.valueOf(i);
        r1.A01 = str;
        r1.A02 = ((ActivityC13830kP) this).A01.A06();
        this.A07.A06(r1);
    }

    public final void A2h(Uri uri, int i) {
        int i2;
        this.A0M[i] = uri;
        AnonymousClass2UZ r3 = (AnonymousClass2UZ) ((ViewGroup) AnonymousClass00T.A05(this, R.id.screenshots)).getChildAt(i);
        if (uri != null) {
            Point point = new Point();
            getWindowManager().getDefaultDisplay().getSize(point);
            int i3 = point.x / 3;
            try {
                r3.setScreenshot(this.A0H.A08(uri, i3 / 2, i3, this.A0A.A03(), false));
                r3.setContentDescription(getString(R.string.describe_problem_screenshot));
                return;
            } catch (C39351pj e) {
                StringBuilder sb = new StringBuilder("descprob/screenshot/not-an-image ");
                sb.append(uri);
                Log.e(sb.toString(), e);
                i2 = R.string.error_file_is_not_a_image;
                Ado(i2);
                r3.setContentDescription(getString(R.string.describe_problem_add_screenshot));
            } catch (IOException e2) {
                StringBuilder sb2 = new StringBuilder("descprob/screenshot/io-exception ");
                sb2.append(uri);
                Log.e(sb2.toString(), e2);
                i2 = R.string.error_load_image;
                Ado(i2);
                r3.setContentDescription(getString(R.string.describe_problem_add_screenshot));
            }
        } else {
            Bitmap bitmap = r3.A02;
            if (bitmap != null) {
                bitmap.recycle();
                r3.A02 = null;
            }
            r3.A02();
        }
        r3.setContentDescription(getString(R.string.describe_problem_add_screenshot));
    }

    @Override // X.AnonymousClass23Q
    public void APf() {
        this.A09 = null;
        A2f();
    }

    @Override // X.AnonymousClass1MI
    public void AV0(boolean z) {
        finish();
    }

    @Override // X.AnonymousClass23Q
    public void AVf(C91934Tu r14) {
        String str = this.A0J;
        String str2 = r14.A02;
        ArrayList<? extends Parcelable> arrayList = r14.A05;
        String str3 = this.A0K;
        int i = r14.A00;
        ArrayList<String> arrayList2 = r14.A06;
        ArrayList<String> arrayList3 = r14.A03;
        ArrayList<String> arrayList4 = r14.A07;
        ArrayList<String> arrayList5 = r14.A04;
        List list = r14.A08;
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.support.faq.SearchFAQ");
        intent.putExtra("com.whatsapp.support.faq.SearchFAQ.from", str);
        intent.putExtra("com.whatsapp.support.faq.SearchFAQ.problem", str2);
        intent.putExtra("com.whatsapp.support.faq.SearchFAQ.status", str3);
        intent.putExtra("com.whatsapp.support.faq.SearchFAQ.count", i);
        intent.putStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.titles", arrayList2);
        intent.putStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.descriptions", arrayList3);
        intent.putStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.urls", arrayList4);
        intent.putStringArrayListExtra("com.whatsapp.support.faq.SearchFAQ.ids", arrayList5);
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
        if (list != null) {
            String[] strArr = new String[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                Pair pair = (Pair) list.get(i2);
                StringBuilder sb = new StringBuilder();
                sb.append((String) pair.first);
                sb.append(":");
                sb.append((String) pair.second);
                strArr[i2] = sb.toString();
            }
            intent.putExtra("com.whatsapp.support.faq.SearchFAQ.additionalDetails", strArr);
        }
        A2E(intent, 32);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri data;
        int i3;
        if ((i & 16) == 16) {
            if (i2 == -1) {
                if (intent.getBooleanExtra("is_removed", false)) {
                    i3 = i - 16;
                    data = null;
                } else {
                    data = intent.getData();
                    if (data != null) {
                        try {
                            grantUriPermission("com.whatsapp", data, 1);
                        } catch (SecurityException e) {
                            Log.w("descprob/permission", e);
                        }
                        i3 = i - 16;
                    } else {
                        Ado(R.string.error_load_image);
                        return;
                    }
                }
                A2h(data, i3);
            }
        } else if (i != 32) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2g(1, null);
        super.onBackPressed();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A0E.A00();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setTitle(R.string.describe_problem_contact_support);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0N(true);
        setContentView(R.layout.describe_problem);
        View findViewById = findViewById(R.id.scroll_view);
        this.A02 = (EditText) findViewById(R.id.describe_problem_description_et);
        View findViewById2 = findViewById(R.id.describe_problem_error);
        Button button = (Button) findViewById(R.id.next_btn);
        boolean z = false;
        if (this.A02.getText().toString().length() >= 1) {
            z = true;
        }
        button.setEnabled(z);
        this.A02.addTextChangedListener(new AnonymousClass47O(button, this));
        button.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 49, findViewById2));
        Intent intent = getIntent();
        this.A0J = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.from");
        this.A0K = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.serverstatus");
        this.A0I = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.emailAddress");
        this.A01 = this.A0F.A02(this.A0D.A00(), "general", null, null);
        if (AnonymousClass1MK.A00(this.A0J)) {
            String A03 = ((ActivityC13810kN) this).A06.A03(AbstractC15460nI.A2L);
            if (!TextUtils.isEmpty(A03)) {
                this.A0I = A03;
            }
            String A032 = ((ActivityC13810kN) this).A06.A03(AbstractC15460nI.A2M);
            if (!TextUtils.isEmpty(A032)) {
                this.A01 = Uri.parse(A032);
            }
        }
        ViewGroup viewGroup = (ViewGroup) AnonymousClass00T.A05(this, R.id.screenshots);
        viewGroup.removeAllViews();
        if (AnonymousClass1MK.A00(this.A0J)) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicIDs");
            ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicTitles");
            this.A0B = this.A0C.A02().AFE();
            String stringExtra = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.paymentBankPhone");
            AbstractC28901Pl r3 = (AbstractC28901Pl) intent.getParcelableExtra("com.whatsapp.support.DescribeProblemActivity.paymentMethod");
            String stringExtra2 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.paymentFBTxnId");
            String stringExtra3 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.paymentBankTxnId");
            String stringExtra4 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.paymentErrorCode");
            String stringExtra5 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.paymentStatus");
            ViewGroup viewGroup2 = (ViewGroup) findViewById(R.id.payment_information_container);
            AnonymousClass5X2 r13 = this.A0B;
            if (r13 != null && !"payments:account-details".equals(this.A0J)) {
                r13.AcT(stringExtra2, stringExtra3, stringExtra4, stringExtra5, stringArrayListExtra);
                viewGroup2.addView(r13.buildPaymentHelpSupportSection(this, r3, stringExtra));
                viewGroup2.setVisibility(0);
            }
            TextView textView = (TextView) findViewById(R.id.optional_title);
            if (stringArrayListExtra2 == null || stringArrayListExtra2.isEmpty()) {
                textView.setVisibility(8);
            } else {
                StringBuilder sb = new StringBuilder(stringArrayListExtra2.get(0));
                for (int i = 1; i < stringArrayListExtra2.size(); i++) {
                    if (i == 1) {
                        sb.append(" (");
                    }
                    sb.append(stringArrayListExtra2.get(i));
                    if (i == stringArrayListExtra2.size() - 1) {
                        str = ")";
                    } else {
                        str = ", ";
                    }
                    sb.append(str);
                }
                textView.setText(sb.toString());
                textView.setVisibility(0);
            }
            View findViewById3 = findViewById(R.id.add_screenshots);
            AnonymousClass5X2 r0 = this.A0B;
            if (r0 != null && !r0.AIL()) {
                findViewById3.setVisibility(8);
                viewGroup.setVisibility(8);
            }
        }
        int intExtra = intent.getIntExtra("com.whatsapp.support.DescribeProblemActivity.type", 0);
        this.A00 = intExtra;
        if (intExtra == 1 || intExtra == 2 || intExtra == 3) {
            A1U.A0A(R.string.describe_problem_contact_us);
        } else {
            A1U.A0A(R.string.describe_problem_contact_support);
        }
        String stringExtra6 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.description");
        if (stringExtra6 != null && stringExtra6.length() > 0) {
            this.A02.setText(stringExtra6);
            this.A0L = true;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.medium_thumbnail_padding);
        int i2 = 0;
        do {
            AnonymousClass2UZ r2 = new AnonymousClass2UZ(this);
            r2.setOnClickListener(new ViewOnClickCListenerShape0S0101000_I0(this, i2, 8));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -2, 1.0f);
            layoutParams.bottomMargin = dimensionPixelSize;
            layoutParams.rightMargin = dimensionPixelSize;
            layoutParams.topMargin = dimensionPixelSize;
            layoutParams.leftMargin = dimensionPixelSize;
            viewGroup.addView(r2, layoutParams);
            String stringExtra7 = intent.getStringExtra("com.whatsapp.support.DescribeProblemActivity.uri");
            if (i2 == 0 && stringExtra7 != null) {
                Uri parse = Uri.parse(stringExtra7);
                this.A0M[0] = parse;
                r2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                r2.setImageURI(parse);
            }
            i2++;
        } while (i2 < 3);
        if (bundle != null) {
            Parcelable[] parcelableArray = bundle.getParcelableArray("screenshots");
            for (int i3 = 0; i3 < parcelableArray.length; i3++) {
                if (parcelableArray[i3] != null) {
                    A2h((Uri) parcelableArray[i3], i3);
                }
            }
        }
        if (this.A00 == 2) {
            A2f();
        }
        AnonymousClass3FC r02 = new AnonymousClass3FC(findViewById, findViewById(R.id.bottom_button_container), getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation));
        this.A0E = r02;
        r02.A00();
        String string = getString(R.string.describe_problem_help_center);
        this.A0E.A02(this, new C73483gJ(this), (TextView) findViewById(R.id.describe_problem_help), string, R.style.DescribeProblemInlineLink);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 2) {
            return super.onCreateDialog(i);
        }
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.searching));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C627638m r1 = this.A09;
        if (r1 != null) {
            r1.A03(false);
        }
        C627338j r12 = this.A0D.A00;
        if (r12 != null) {
            r12.A03(false);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        A2g(1, null);
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A02.clearFocus();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelableArray("screenshots", this.A0M);
    }
}
