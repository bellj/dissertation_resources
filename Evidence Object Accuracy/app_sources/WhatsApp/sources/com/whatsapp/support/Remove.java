package com.whatsapp.support;

import X.AbstractC009404s;
import X.ActivityC000900k;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2FH;
import X.C12960it;
import X.C12970iu;
import X.C48572Gu;
import X.C66573Oc;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class Remove extends ActivityC000900k implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public boolean A01;
    public final Object A02;
    public volatile AnonymousClass2FH A03;

    public Remove() {
        this(0);
    }

    public Remove(int i) {
        this.A02 = C12970iu.A0l();
        this.A01 = false;
        A0R(new C66573Oc(this));
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    this.A03 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A03.generatedComponent();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.title_remove);
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("is_removed", true);
        C12960it.A0q(this, A0A);
    }
}
