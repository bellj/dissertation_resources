package com.whatsapp;

import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* loaded from: classes2.dex */
public class WAChatIntroBottomSheet extends RoundedBottomSheetDialogFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.wa_chat_intro_bottomsheet, viewGroup, false);
        ViewOnClickCListenerShape17S0100000_I1 viewOnClickCListenerShape17S0100000_I1 = new ViewOnClickCListenerShape17S0100000_I1(this, 1);
        AnonymousClass028.A0D(inflate, R.id.close_button).setOnClickListener(viewOnClickCListenerShape17S0100000_I1);
        AnonymousClass028.A0D(inflate, R.id.continue_button).setOnClickListener(viewOnClickCListenerShape17S0100000_I1);
        C12960it.A0I(inflate, R.id.header).setText(C12970iu.A0q(this, "WhatsApp", new Object[1], 0, R.string.wac_intro_bottomsheet_header));
        return inflate;
    }
}
