package com.whatsapp.businessdirectory.view.fragment;

import X.AbstractC05270Ox;
import X.AbstractC115845Td;
import X.AbstractC115935Tm;
import X.AbstractC116645Wg;
import X.AbstractC116655Wh;
import X.AbstractC75133jM;
import X.ActivityC000900k;
import X.AnonymousClass01E;
import X.AnonymousClass05J;
import X.AnonymousClass05L;
import X.AnonymousClass06W;
import X.AnonymousClass07E;
import X.AnonymousClass13N;
import X.AnonymousClass1B0;
import X.AnonymousClass1B9;
import X.AnonymousClass3CJ;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C15890o4;
import X.C21740xu;
import X.C244415n;
import X.C251118d;
import X.C252718t;
import X.C30211Wn;
import X.C54142gF;
import X.C54732hC;
import X.C59672vC;
import X.C63363Bh;
import X.C68433Vj;
import X.C90954Pw;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.businessdirectory.util.LocationUpdateListener;
import com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchQueryFragment;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import java.util.Set;

/* loaded from: classes2.dex */
public class BusinessDirectorySearchQueryFragment extends Hilt_BusinessDirectorySearchQueryFragment implements AbstractC116645Wg, AbstractC116655Wh, AbstractC115845Td, AbstractC115935Tm {
    public AnonymousClass05L A00 = A06(new AnonymousClass05J() { // from class: X.3Od
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = BusinessDirectorySearchQueryFragment.this;
            C06830Vg r4 = (C06830Vg) obj;
            if (r4.A00 == -1) {
                C48152En r1 = (C48152En) r4.A01.getParcelableExtra("search_query_selected");
                AnonymousClass009.A05(r1);
                businessDirectorySearchQueryFragment.A09.A0L(r1);
                return;
            }
            AnonymousClass3E8 r2 = businessDirectorySearchQueryFragment.A09.A0Q;
            r2.A01.A03 = null;
            C12990iw.A1O(r2.A02, r2, 41);
        }
    }, new AnonymousClass06W());
    public C21740xu A01;
    public C251118d A02;
    public AnonymousClass1B0 A03;
    public AnonymousClass1B9 A04;
    public LocationUpdateListener A05;
    public C54142gF A06;
    public AnonymousClass3CJ A07;
    public AbstractC75133jM A08;
    public BusinessDirectorySearchQueryViewModel A09;
    public C15890o4 A0A;
    public C252718t A0B;
    public final AnonymousClass05L A0C = A06(new AnonymousClass05J() { // from class: X.4rA
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = BusinessDirectorySearchQueryFragment.this;
            if (((C06830Vg) obj).A00 == -1) {
                businessDirectorySearchQueryFragment.A09.A0U.A06();
            }
        }
    }, new AnonymousClass06W());
    public final AbstractC05270Ox A0D = new C54732hC(this);

    public static /* synthetic */ void A00(BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment, C90954Pw r5) {
        FilterBottomSheetDialogFragment filterBottomSheetDialogFragment = new FilterBottomSheetDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelableArrayList("arg-categories", r5.A01);
        A0D.putParcelable("arg-selected-category", r5.A00);
        A0D.putString("arg-parent-category-title", null);
        A0D.putParcelableArrayList("arg-selected-categories", r5.A02);
        filterBottomSheetDialogFragment.A0U(A0D);
        filterBottomSheetDialogFragment.A02 = businessDirectorySearchQueryFragment;
        filterBottomSheetDialogFragment.A1F(businessDirectorySearchQueryFragment.A0E(), "filter-bottom-sheet");
    }

    @Override // X.AnonymousClass01E
    public void A0k(Bundle bundle) {
        super.A0k(bundle);
        AnonymousClass01E A0A = A0E().A0A("filter-bottom-sheet");
        if (A0A != null) {
            ((FilterBottomSheetDialogFragment) A0A).A02 = this;
        }
        LocationOptionPickerFragment locationOptionPickerFragment = (LocationOptionPickerFragment) A0E().A0A("location-options-bottom-sheet");
        if (locationOptionPickerFragment != null) {
            locationOptionPickerFragment.A02 = this;
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel;
        int i3;
        if (i == 34) {
            C68433Vj r0 = this.A09.A0U;
            if (i2 == -1) {
                r0.A04();
                businessDirectorySearchQueryViewModel = this.A09;
                i3 = 5;
            } else {
                r0.A05();
                businessDirectorySearchQueryViewModel = this.A09;
                i3 = 6;
            }
            businessDirectorySearchQueryViewModel.A0L.A01(i3, 0);
        }
        super.A0t(i, i2, intent);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        String str;
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = this.A09;
        AnonymousClass07E r2 = businessDirectorySearchQueryViewModel.A0H;
        r2.A04("saved_search_session_started", Boolean.valueOf(businessDirectorySearchQueryViewModel.A09));
        r2.A04("business_search_queries", businessDirectorySearchQueryViewModel.A08);
        r2.A04("saved_search_state", Integer.valueOf(businessDirectorySearchQueryViewModel.A04));
        C63363Bh r0 = (C63363Bh) businessDirectorySearchQueryViewModel.A0P.A04.A01();
        if (r0 != null) {
            str = r0.A06;
        } else {
            str = null;
        }
        r2.A04("saved_search_query", str);
        businessDirectorySearchQueryViewModel.A0R.A09(r2);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.business_directory_search_fragment);
        RecyclerView A0R = C12990iw.A0R(A0F, R.id.search_list);
        this.A08 = new C59672vC(this);
        A0p();
        A0R.setLayoutManager(new LinearLayoutManager(1));
        A0R.setAdapter(this.A06);
        A0R.A0n(this.A08);
        A0R.A0n(this.A0D);
        this.A0K.A00(this.A05);
        C12960it.A19(A0G(), this.A05.A01, this, 32);
        C12970iu.A1P(A0G(), this.A09.A0G, this, 16);
        C12960it.A19(A0G(), this.A09.A0Z, this, 34);
        C12970iu.A1P(A0G(), this.A09.A0W, this, 15);
        C12960it.A19(A0G(), this.A09.A0X, this, 36);
        C12960it.A19(A0G(), this.A09.A0U.A02, this, 35);
        C12960it.A19(A0G(), this.A09.A0Y, this, 33);
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        AnonymousClass1B9 r1 = this.A04;
        synchronized (r1) {
            r1.A01.remove(this);
        }
        ActivityC000900k A0B = A0B();
        if (A0B instanceof BusinessDirectoryActivity) {
            ((BusinessDirectoryActivity) A0B).A07 = null;
        }
    }

    @Override // com.whatsapp.businessdirectory.view.fragment.Hilt_BusinessDirectorySearchQueryFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        ActivityC000900k A0B = A0B();
        if (A0B instanceof BusinessDirectoryActivity) {
            ((BusinessDirectoryActivity) A0B).A07 = this;
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        AnonymousClass1B9 r1 = this.A04;
        synchronized (r1) {
            r1.A01.add(this);
        }
        this.A09 = (BusinessDirectorySearchQueryViewModel) C13000ix.A02(this).A00(BusinessDirectorySearchQueryViewModel.class);
    }

    public final BusinessDirectoryActivity A1A() {
        if (A0C() instanceof BusinessDirectoryActivity) {
            return (BusinessDirectoryActivity) A0C();
        }
        throw C12960it.A0U("BusinessDirectorySearchQueryFragment should be attached to BusinessDirectoryActivity");
    }

    public final void A1B() {
        if (!RequestPermissionActivity.A0V(A0C(), C244415n.A02)) {
            this.A07.A00(A01(), this);
        } else if (this.A0A.A03()) {
            this.A09.A0U.A06();
        } else {
            AnonymousClass13N.A01(this);
            C12960it.A1A(this.A09.A0U.A02, 2);
        }
    }

    @Override // X.AbstractC116645Wg
    public void ANL() {
        this.A09.A0F(62);
    }

    @Override // X.AbstractC116655Wh
    public void ASD() {
        if (this.A0A.A03()) {
            this.A09.A0U.A04();
        } else {
            AnonymousClass13N.A01(this);
        }
        this.A09.A0L.A01(3, 0);
    }

    @Override // X.AbstractC116655Wh
    public void ASE() {
        this.A09.A0U.A05();
    }

    @Override // X.AbstractC116655Wh
    public void ASF() {
        this.A09.A0U.A05();
        this.A09.A0L.A01(4, 0);
    }

    @Override // X.AbstractC115935Tm
    public void ASH() {
        this.A09.A0U.A06();
    }

    @Override // X.AbstractC116645Wg
    public void ASs(Set set) {
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = this.A09;
        businessDirectorySearchQueryViewModel.A0R.A02 = set;
        businessDirectorySearchQueryViewModel.A0R(businessDirectorySearchQueryViewModel.A05());
        this.A09.A0F(64);
    }

    @Override // X.AbstractC115845Td
    public void AVe() {
        this.A09.A0U.A06();
    }

    @Override // X.AbstractC116645Wg
    public void AW5(C30211Wn r3) {
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = this.A09;
        businessDirectorySearchQueryViewModel.A0R.A00 = r3;
        businessDirectorySearchQueryViewModel.A0R(businessDirectorySearchQueryViewModel.A05());
        this.A09.A0O(r3, 2);
    }
}
