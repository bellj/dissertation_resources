package com.whatsapp.businessdirectory.view.fragment;

import X.AbstractC115935Tm;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass05J;
import X.AnonymousClass05L;
import X.AnonymousClass06W;
import X.AnonymousClass3CJ;
import X.C004802e;
import X.C012906c;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C16430p0;
import X.C244415n;
import X.C28531Ny;
import X.C35751ig;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;
import com.whatsapp.businessdirectory.view.activity.DirectorySetNeighborhoodActivity;
import com.whatsapp.businessdirectory.view.fragment.LocationOptionPickerFragment;
import com.whatsapp.businessdirectory.viewmodel.LocationOptionPickerViewModel;
import java.util.Map;

/* loaded from: classes2.dex */
public class LocationOptionPickerFragment extends Hilt_LocationOptionPickerFragment {
    public RecyclerView A00;
    public AnonymousClass3CJ A01;
    public AbstractC115935Tm A02;
    public LocationOptionPickerViewModel A03;
    public final AnonymousClass05L A04 = A06(new AnonymousClass05J() { // from class: X.3Oe
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            LocationOptionPickerFragment locationOptionPickerFragment = LocationOptionPickerFragment.this;
            if (((C06830Vg) obj).A00 == -1) {
                C12960it.A1A(locationOptionPickerFragment.A03.A07, 2);
            }
        }
    }, new AnonymousClass06W());
    public final AnonymousClass05L A05 = A06(new AnonymousClass05J() { // from class: X.3Of
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            LocationOptionPickerFragment locationOptionPickerFragment = LocationOptionPickerFragment.this;
            int i = ((C06830Vg) obj).A00;
            LocationOptionPickerViewModel locationOptionPickerViewModel = locationOptionPickerFragment.A03;
            if (i == -1) {
                locationOptionPickerViewModel.A02.A01(5, 1);
                locationOptionPickerViewModel.A03.A00();
                C12960it.A1A(locationOptionPickerViewModel.A07, 2);
                return;
            }
            locationOptionPickerViewModel.A02.A01(6, 1);
        }
    }, new AnonymousClass06W());
    public final AnonymousClass05L A06 = A06(new AnonymousClass05J() { // from class: X.3Og
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            LocationOptionPickerFragment locationOptionPickerFragment = LocationOptionPickerFragment.this;
            Boolean bool = (Boolean) ((Map) obj).get("android.permission.ACCESS_FINE_LOCATION");
            if (bool != null && bool.booleanValue()) {
                LocationOptionPickerViewModel locationOptionPickerViewModel = locationOptionPickerFragment.A03;
                locationOptionPickerViewModel.A02.A01(5, 1);
                locationOptionPickerViewModel.A03.A00();
                C12960it.A1A(locationOptionPickerViewModel.A07, 2);
            }
        }
    }, new C012906c());

    public static LocationOptionPickerFragment A00(AbstractC115935Tm r3, int i) {
        LocationOptionPickerFragment locationOptionPickerFragment = new LocationOptionPickerFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("source", i);
        locationOptionPickerFragment.A0U(A0D);
        locationOptionPickerFragment.A02 = r3;
        return locationOptionPickerFragment;
    }

    public static /* synthetic */ void A01(LocationOptionPickerFragment locationOptionPickerFragment, Integer num) {
        int intValue = num.intValue();
        if (intValue == 0) {
            locationOptionPickerFragment.A01.A00(locationOptionPickerFragment.A01(), locationOptionPickerFragment.A03);
        } else if (intValue == 1) {
            C35751ig r3 = new C35751ig(locationOptionPickerFragment.A0C());
            r3.A01 = R.drawable.permission_location;
            r3.A0C = C244415n.A02;
            r3.A0B = new String[]{"android.permission.ACCESS_COARSE_LOCATION"};
            r3.A03 = R.string.permission_location_access_on_searching_businesses;
            r3.A02 = R.string.permission_location_info_on_searching_businesses;
            locationOptionPickerFragment.A05.A00(null, r3.A00());
        } else if (intValue == 2) {
            locationOptionPickerFragment.A02.ASH();
            locationOptionPickerFragment.A1B();
        } else if (intValue == 3) {
            boolean A0G = AnonymousClass00T.A0G(locationOptionPickerFragment.A0C(), "android.permission.ACCESS_FINE_LOCATION");
            int i = R.string.permission_location_access_upgrade_on_searching_businesses_fallback;
            int i2 = R.string.biz_dir_open_settings;
            if (A0G) {
                i = R.string.permission_location_access_upgrade_on_searching_businesses;
                i2 = R.string.biz_user_request_precise_location_action;
            }
            C004802e A0S = C12980iv.A0S(locationOptionPickerFragment.A0C());
            A0S.A07(R.string.biz_user_current_location_imprecise_disclaimer);
            A0S.A06(i);
            A0S.setPositiveButton(i2, new DialogInterface.OnClickListener(A0G) { // from class: X.3KM
                public final /* synthetic */ boolean A01;

                {
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    LocationOptionPickerFragment locationOptionPickerFragment2 = LocationOptionPickerFragment.this;
                    if (this.A01) {
                        locationOptionPickerFragment2.A06.A00(null, C244415n.A02);
                    } else {
                        C38211ni.A05(locationOptionPickerFragment2.A0C());
                    }
                }
            });
            A0S.setNegativeButton(R.string.not_now, null);
            C12970iu.A1J(A0S);
        } else if (intValue == 4) {
            AnonymousClass05L r5 = locationOptionPickerFragment.A04;
            Context A01 = locationOptionPickerFragment.A01();
            int i3 = ((AnonymousClass01E) locationOptionPickerFragment).A05.getInt("source", -1);
            Intent A0D = C12990iw.A0D(A01, DirectorySetNeighborhoodActivity.class);
            A0D.putExtra("source", i3);
            r5.A00(null, A0D);
        } else if (intValue == 5) {
            locationOptionPickerFragment.A04.A00(null, C12990iw.A0D(locationOptionPickerFragment.A01(), DirectorySetLocationMapActivity.class));
        } else {
            throw C12960it.A0U(C12960it.A0b("LocationOptionPickerFragment/onViewAction view action not handled: ", num));
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.location_options_picker_fragment);
        this.A00 = C12990iw.A0R(A0F, R.id.rv_location_options);
        C12960it.A17(this, this.A03.A00, 18);
        C12960it.A17(this, this.A03.A07, 17);
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            LocationOptionPickerViewModel locationOptionPickerViewModel = this.A03;
            int i = bundle2.getInt("source", -1);
            C16430p0 r4 = locationOptionPickerViewModel.A02;
            Integer valueOf = Integer.valueOf(i);
            Integer A01 = locationOptionPickerViewModel.A04.A01();
            C28531Ny r1 = new C28531Ny();
            C13000ix.A06(r1, 35);
            r1.A0A = valueOf;
            r1.A03 = A01;
            r4.A03(r1);
        }
        return A0F;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A03 = (LocationOptionPickerViewModel) C13000ix.A02(this).A00(LocationOptionPickerViewModel.class);
    }
}
