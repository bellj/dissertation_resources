package com.whatsapp.businessdirectory.view.activity;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C48922Il;
import X.C54112gC;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.viewmodel.DirectorySearchHistoryViewModel;

/* loaded from: classes2.dex */
public class DirectorySearchHistoryActivity extends ActivityC13790kL {
    public RecyclerView A00;
    public C54112gC A01;
    public DirectorySearchHistoryViewModel A02;
    public boolean A03;

    public DirectorySearchHistoryActivity() {
        this(0);
    }

    public DirectorySearchHistoryActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 26);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = new C54112gC((C48922Il) A1L.A0V.get());
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_directory_search_history);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0A(R.string.dir_search_history_title);
        this.A02 = (DirectorySearchHistoryViewModel) C13000ix.A02(this).A00(DirectorySearchHistoryViewModel.class);
        this.A00 = C12990iw.A0R(((ActivityC13810kN) this).A00, R.id.recyclerView);
        this.A00.setLayoutManager(new LinearLayoutManager(1));
        this.A00.setAdapter(this.A01);
        C12960it.A17(this, this.A02.A01, 8);
        C12960it.A18(this, this.A02.A08, 26);
        C12960it.A17(this, this.A02.A02, 7);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menu_item_clear_search_history, 0, R.string.dir_clear_all);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menu_item_clear_search_history) {
            C12970iu.A1Q(this.A02.A08, 0);
        } else if (menuItem.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
