package com.whatsapp.businessdirectory.view.fragment;

import X.AbstractC010305c;
import X.AbstractC115845Td;
import X.AbstractC115935Tm;
import X.AbstractC116645Wg;
import X.AbstractC116655Wh;
import X.AbstractC75133jM;
import X.ActivityC000900k;
import X.ActivityC001000l;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass02A;
import X.AnonymousClass05J;
import X.AnonymousClass05L;
import X.AnonymousClass06T;
import X.AnonymousClass06W;
import X.AnonymousClass07E;
import X.AnonymousClass13N;
import X.AnonymousClass1B9;
import X.AnonymousClass3CJ;
import X.AnonymousClass4JK;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15890o4;
import X.C21740xu;
import X.C251118d;
import X.C30211Wn;
import X.C53792fE;
import X.C53862fQ;
import X.C54142gF;
import X.C59662vB;
import X.C68433Vj;
import X.C74183hW;
import X.C90954Pw;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.util.LocationUpdateListener;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchFragment;
import com.whatsapp.jid.Jid;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes2.dex */
public class BusinessDirectorySearchFragment extends Hilt_BusinessDirectorySearchFragment implements AbstractC116645Wg, AbstractC116655Wh, AbstractC115845Td, AbstractC115935Tm {
    public RecyclerView A00;
    public AnonymousClass4JK A01;
    public C21740xu A02;
    public C251118d A03;
    public AnonymousClass1B9 A04;
    public LocationUpdateListener A05;
    public C54142gF A06;
    public AnonymousClass3CJ A07;
    public AbstractC75133jM A08;
    public C53862fQ A09;
    public C15890o4 A0A;
    public AnonymousClass018 A0B;
    public final AbstractC010305c A0C = new C74183hW(this);
    public final AnonymousClass05L A0D = A06(new AnonymousClass05J() { // from class: X.4r9
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            BusinessDirectorySearchFragment businessDirectorySearchFragment = BusinessDirectorySearchFragment.this;
            if (((C06830Vg) obj).A00 == -1) {
                businessDirectorySearchFragment.A09.A09();
            }
        }
    }, new AnonymousClass06W());

    public static BusinessDirectorySearchFragment A00() {
        BusinessDirectorySearchFragment businessDirectorySearchFragment = new BusinessDirectorySearchFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("argument_business_list_search_state", "nearby_business");
        businessDirectorySearchFragment.A0U(A0D);
        return businessDirectorySearchFragment;
    }

    public static BusinessDirectorySearchFragment A01() {
        BusinessDirectorySearchFragment businessDirectorySearchFragment = new BusinessDirectorySearchFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("FORCE_ROOT_CATEGORIES", true);
        businessDirectorySearchFragment.A0U(A0D);
        return businessDirectorySearchFragment;
    }

    public static BusinessDirectorySearchFragment A02(Jid jid, String str) {
        BusinessDirectorySearchFragment businessDirectorySearchFragment = new BusinessDirectorySearchFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("directory_biz_chaining_jid", jid);
        A0D.putString("directory_biz_chaining_name", str);
        businessDirectorySearchFragment.A0U(A0D);
        return businessDirectorySearchFragment;
    }

    public static /* synthetic */ void A03(BusinessDirectorySearchFragment businessDirectorySearchFragment, C90954Pw r5) {
        if (r5 != null) {
            FilterBottomSheetDialogFragment filterBottomSheetDialogFragment = new FilterBottomSheetDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putParcelableArrayList("arg-categories", r5.A01);
            A0D.putParcelable("arg-selected-category", r5.A00);
            A0D.putString("arg-parent-category-title", null);
            A0D.putParcelableArrayList("arg-selected-categories", r5.A02);
            filterBottomSheetDialogFragment.A0U(A0D);
            filterBottomSheetDialogFragment.A02 = businessDirectorySearchFragment;
            filterBottomSheetDialogFragment.A1F(businessDirectorySearchFragment.A0E(), "filter-bottom-sheet");
        }
    }

    @Override // X.AnonymousClass01E
    public void A0k(Bundle bundle) {
        super.A0k(bundle);
        AnonymousClass01E A0A = A0E().A0A("filter-bottom-sheet");
        if (A0A != null) {
            ((FilterBottomSheetDialogFragment) A0A).A02 = this;
        }
        LocationOptionPickerFragment locationOptionPickerFragment = (LocationOptionPickerFragment) A0E().A0A("location-options-bottom-sheet");
        if (locationOptionPickerFragment != null) {
            locationOptionPickerFragment.A02 = this;
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        C53862fQ r0;
        int i3;
        if (i == 34) {
            C68433Vj r02 = this.A09.A0L;
            if (i2 == -1) {
                r02.A04();
                r0 = this.A09;
                i3 = 5;
            } else {
                r02.A05();
                r0 = this.A09;
                i3 = 6;
            }
            r0.A0E.A01(i3, 0);
        }
        super.A0t(i, i2, intent);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        C53862fQ r3 = this.A09;
        AnonymousClass07E r2 = r3.A0C;
        r2.A04("saved_search_state_stack", C12980iv.A0x(r3.A04));
        r2.A04("saved_second_level_category", r3.A0P.A01());
        r2.A04("saved_parent_category", r3.A0O.A01());
        r2.A04("saved_search_state", Integer.valueOf(r3.A01));
        r2.A04("saved_force_root_category", Boolean.valueOf(r3.A05));
        r3.A0I.A09(r2);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.business_directory_search_fragment);
        this.A00 = C12990iw.A0R(A0F, R.id.search_list);
        A0p();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(1);
        this.A08 = new C59662vB(this);
        this.A00.setLayoutManager(linearLayoutManager);
        this.A00.A0n(this.A08);
        this.A00.setAdapter(this.A06);
        this.A0K.A00(this.A05);
        C12960it.A19(A0G(), this.A05.A01, this, 28);
        C12960it.A19(A0G(), this.A09.A0R, this, 29);
        C53862fQ r2 = this.A09;
        C68433Vj r1 = r2.A0L;
        if (r1.A00.A01() == null) {
            r1.A06();
        }
        C12970iu.A1P(A0G(), r2.A0B, this, 14);
        C12970iu.A1P(A0G(), this.A09.A0N, this, 12);
        C12970iu.A1P(A0G(), this.A09.A07, this, 11);
        C12960it.A19(A0G(), this.A09.A0Q, this, 30);
        C12960it.A19(A0G(), this.A09.A0L.A02, this, 31);
        C12970iu.A1P(A0G(), this.A09.A0A, this, 13);
        ((ActivityC001000l) A0C()).A04.A01(this.A0C, A0G());
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        AnonymousClass1B9 r1 = this.A04;
        synchronized (r1) {
            r1.A01.remove(this);
        }
        Iterator it = this.A0C.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass06T) it.next()).cancel();
        }
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        RecyclerView recyclerView = this.A00;
        if (recyclerView != null) {
            recyclerView.A0o(this.A08);
            this.A00.setAdapter(null);
            this.A00 = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        C53862fQ r2 = this.A09;
        Iterator it = r2.A0T.iterator();
        if (it.hasNext()) {
            it.next();
            throw C12980iv.A0n("isVisibilityChanged");
        } else {
            r2.A0L.A06();
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        boolean z = A03().getBoolean("FORCE_ROOT_CATEGORIES");
        String string = A03().getString("argument_business_list_search_state");
        this.A09 = (C53862fQ) new AnonymousClass02A(new C53792fE(bundle, this, this.A01, (C30211Wn) A03().getParcelable("INITIAL_CATEGORY"), (Jid) A03().getParcelable("directory_biz_chaining_jid"), string, z), this).A00(C53862fQ.class);
        AnonymousClass1B9 r1 = this.A04;
        synchronized (r1) {
            r1.A01.add(this);
        }
    }

    public final void A1A(String str) {
        ActivityC000900k r1;
        int i;
        switch (str.hashCode()) {
            case -1126816384:
                if (str.equals("nearby_business")) {
                    r1 = A0C();
                    i = R.string.biz_dir_nearby_business_title;
                    r1.setTitle(A0I(i));
                    return;
                }
                A0C().setTitle(str);
                return;
            case 1014375387:
                if (str.equals("product_name")) {
                    r1 = A0C();
                    i = R.string.biz_screen_title_v2;
                    r1.setTitle(A0I(i));
                    return;
                }
                A0C().setTitle(str);
                return;
            case 2044323616:
                if (str.equals("business_chaining")) {
                    String string = A03().getString("directory_biz_chaining_name");
                    if (string != null) {
                        A1A(C12970iu.A0q(this, string, new Object[1], 0, R.string.biz_dir_similar_to));
                        return;
                    }
                    return;
                }
                A0C().setTitle(str);
                return;
            default:
                A0C().setTitle(str);
                return;
        }
    }

    @Override // X.AbstractC116645Wg
    public void ANL() {
        this.A09.A0H(62);
    }

    @Override // X.AbstractC116655Wh
    public void ASD() {
        if (this.A0A.A03()) {
            this.A09.A0L.A04();
        } else {
            AnonymousClass13N.A01(this);
        }
        this.A09.A0E.A01(3, 0);
    }

    @Override // X.AbstractC116655Wh
    public void ASE() {
        this.A09.A0L.A05();
    }

    @Override // X.AbstractC116655Wh
    public void ASF() {
        this.A09.A0L.A05();
        this.A09.A0E.A01(4, 0);
    }

    @Override // X.AbstractC115935Tm
    public void ASH() {
        this.A09.A09();
    }

    @Override // X.AbstractC116645Wg
    public void ASs(Set set) {
        C53862fQ r1 = this.A09;
        r1.A0I.A02 = set;
        r1.A0D();
        this.A09.A0H(64);
    }

    @Override // X.AbstractC115845Td
    public void AVe() {
        this.A09.A09();
    }

    @Override // X.AbstractC116645Wg
    public void AW5(C30211Wn r3) {
        C53862fQ r1 = this.A09;
        r1.A0I.A00 = r3;
        r1.A0D();
        this.A09.A0L(r3, 2);
    }
}
