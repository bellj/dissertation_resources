package com.whatsapp.businessdirectory.view.custom;

import X.AbstractC116645Wg;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass07E;
import X.AnonymousClass4JJ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C30211Wn;
import X.C53782fD;
import X.C53822fI;
import X.C54242gP;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class FilterBottomSheetDialogFragment extends Hilt_FilterBottomSheetDialogFragment {
    public AnonymousClass4JJ A00;
    public C54242gP A01;
    public AbstractC116645Wg A02 = null;
    public C53822fI A03;
    public final AbstractView$OnClickListenerC34281fs A04 = new ViewOnClickCListenerShape17S0100000_I1(this, 26);

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        C53822fI r3 = this.A03;
        AnonymousClass07E r2 = r3.A02;
        r2.A04("saved_all_categories", r3.A00);
        r2.A04("saved_selected_categories", C12980iv.A0x(r3.A03));
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String A0I;
        View inflate = layoutInflater.inflate(R.layout.filter_bottom_sheet_dialog_fragment, viewGroup, false);
        C12960it.A0z(AnonymousClass028.A0D(inflate, R.id.iv_close), this, 14);
        TextView A0I2 = C12960it.A0I(inflate, R.id.tv_title);
        String string = A03().getString("arg-parent-category-title");
        if (string != null) {
            A0I = C12970iu.A0q(this, string, C12970iu.A1b(), 0, R.string.biz_dir_filter_bottom_sheet_title);
        } else {
            A0I = A0I(R.string.biz_dir_filter);
        }
        A0I2.setText(A0I);
        this.A01 = new C54242gP(this, this.A03.A04);
        ((RecyclerView) inflate.findViewById(R.id.rv_categories)).setAdapter(this.A01);
        C12970iu.A1P(A0G(), this.A03.A01, this, 10);
        View A0D = AnonymousClass028.A0D(inflate, R.id.btn_clear);
        AbstractView$OnClickListenerC34281fs r1 = this.A04;
        A0D.setOnClickListener(r1);
        View A0D2 = AnonymousClass028.A0D(inflate, R.id.btn_apply);
        A0D2.setOnClickListener(r1);
        if (!this.A03.A04) {
            A0D.setVisibility(8);
            A0D2.setVisibility(8);
            AnonymousClass028.A0D(inflate, R.id.footer_separator).setVisibility(8);
        }
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        ArrayList parcelableArrayList = A03().getParcelableArrayList("arg-categories");
        ArrayList parcelableArrayList2 = A03().getParcelableArrayList("arg-selected-categories");
        this.A03 = (C53822fI) new AnonymousClass02A(new C53782fD(bundle, this, this.A00, (C30211Wn) A03().getParcelable("arg-selected-category"), parcelableArrayList, parcelableArrayList2), this).A00(C53822fI.class);
    }
}
