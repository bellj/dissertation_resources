package com.whatsapp.businessdirectory.view.activity;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1B9;
import X.AnonymousClass2FL;
import X.AnonymousClass406;
import X.AnonymousClass4AE;
import X.AnonymousClass4T7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C15890o4;
import X.C16430p0;
import X.C251118d;
import X.C28531Ny;
import X.C48232Fc;
import X.C48952Io;
import X.C54302gV;
import X.C66753Ou;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class DirectorySetNeighborhoodActivity extends ActivityC13790kL {
    public RecyclerView A00;
    public C48232Fc A01;
    public C251118d A02;
    public AnonymousClass1B9 A03;
    public C54302gV A04;
    public DirectorySetNeighborhoodViewModel A05;
    public C15890o4 A06;
    public boolean A07;
    public boolean A08;

    public DirectorySetNeighborhoodActivity() {
        this(0);
    }

    public DirectorySetNeighborhoodActivity(int i) {
        this.A07 = false;
        ActivityC13830kP.A1P(this, 27);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = C12970iu.A0V(A1M);
            this.A06 = C12970iu.A0Y(A1M);
            this.A04 = new C54302gV((C48952Io) A1L.A0X.get());
            this.A03 = (AnonymousClass1B9) A1M.A1P.get();
        }
    }

    public final void A2e() {
        C48232Fc r0 = this.A01;
        if (r0 != null) {
            r0.A01();
            C48232Fc r2 = this.A01;
            String string = getString(R.string.biz_dir_search_for_location_hint);
            SearchView searchView = r2.A02;
            if (searchView != null) {
                searchView.setQueryHint(string);
            }
            this.A05.A07(C12960it.A0l());
            C12960it.A0z(this.A01.A06.findViewById(R.id.search_back), this, 10);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 35 && i2 == -1) {
            setResult(-1);
            finish();
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        DirectorySetNeighborhoodViewModel directorySetNeighborhoodViewModel;
        AnonymousClass4T7 r3;
        C48232Fc r0 = this.A01;
        if (r0 == null || !r0.A05()) {
            directorySetNeighborhoodViewModel = this.A05;
            List list = directorySetNeighborhoodViewModel.A08;
            if (list.size() == 1) {
                directorySetNeighborhoodViewModel.A01.A0A(AnonymousClass4AE.FINISH);
                return;
            } else {
                list.remove(0);
                r3 = (AnonymousClass4T7) list.get(0);
            }
        } else {
            this.A01.A04(true);
            directorySetNeighborhoodViewModel = this.A05;
            List list2 = directorySetNeighborhoodViewModel.A08;
            int size = list2.size();
            r3 = (AnonymousClass4T7) C12980iv.A0o(list2);
            if (size != 1) {
                directorySetNeighborhoodViewModel.A05(r3);
                return;
            }
        }
        ArrayList A0l = C12960it.A0l();
        A0l.add(new AnonymousClass406(0));
        A0l.addAll(directorySetNeighborhoodViewModel.A04(r3.A05));
        directorySetNeighborhoodViewModel.A07(A0l);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_set_location);
        if (bundle != null) {
            this.A08 = bundle.getBoolean("arg_show_search_view", false);
        }
        setTitle(getString(R.string.biz_dir_pick_neighborhood));
        this.A05 = (DirectorySetNeighborhoodViewModel) C13000ix.A02(this).A00(DirectorySetNeighborhoodViewModel.class);
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0N(true);
        A0N.A0M(true);
        this.A01 = new C48232Fc(this, findViewById(R.id.search_holder), new C66753Ou(this), A0Q, ((ActivityC13830kP) this).A01);
        if (this.A08) {
            A2e();
        }
        this.A00 = C12990iw.A0R(((ActivityC13810kN) this).A00, R.id.recyclerView);
        this.A00.setLayoutManager(new LinearLayoutManager(1));
        C12960it.A18(this, this.A05.A00, 27);
        C12960it.A17(this, this.A05.A01, 9);
        DirectorySetNeighborhoodViewModel directorySetNeighborhoodViewModel = this.A05;
        int intExtra = getIntent().getIntExtra("source", -1);
        C16430p0 r4 = directorySetNeighborhoodViewModel.A02;
        Integer valueOf = Integer.valueOf(intExtra);
        Integer A01 = directorySetNeighborhoodViewModel.A05.A01();
        C28531Ny r1 = new C28531Ny();
        C13000ix.A06(r1, 35);
        r1.A0A = valueOf;
        r1.A03 = A01;
        r4.A03(r1);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, getString(R.string.search)).setIcon(R.drawable.ic_action_search).setShowAsAction(2);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        menuItem.getItemId();
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
            return true;
        } else if (menuItem.getItemId() != 1) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            A2e();
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r0.A05() == false) goto L_0x000e;
     */
    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSaveInstanceState(android.os.Bundle r3) {
        /*
            r2 = this;
            super.onSaveInstanceState(r3)
            X.2Fc r0 = r2.A01
            if (r0 == 0) goto L_0x000e
            boolean r0 = r0.A05()
            r1 = 1
            if (r0 != 0) goto L_0x000f
        L_0x000e:
            r1 = 0
        L_0x000f:
            java.lang.String r0 = "arg_show_search_view"
            r3.putBoolean(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.view.activity.DirectorySetNeighborhoodActivity.onSaveInstanceState(android.os.Bundle):void");
    }
}
