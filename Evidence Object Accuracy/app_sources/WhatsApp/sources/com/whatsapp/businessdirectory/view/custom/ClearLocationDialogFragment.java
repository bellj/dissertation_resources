package com.whatsapp.businessdirectory.view.custom;

import X.AnonymousClass028;
import X.AnonymousClass1B9;
import X.C004802e;
import X.C12960it;
import X.C12980iv;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ClearLocationDialogFragment extends Hilt_ClearLocationDialogFragment {
    public AnonymousClass1B9 A00;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        View A0N = C12980iv.A0N(A01(), R.layout.clear_location_dialog);
        View A0D = AnonymousClass028.A0D(A0N, R.id.clear_btn);
        View A0D2 = AnonymousClass028.A0D(A0N, R.id.cancel_btn);
        C12960it.A0z(A0D, this, 11);
        C12960it.A0z(A0D2, this, 12);
        C004802e A0K = C12960it.A0K(this);
        A0K.setView(A0N);
        A0K.A0B(true);
        return A0K.create();
    }
}
