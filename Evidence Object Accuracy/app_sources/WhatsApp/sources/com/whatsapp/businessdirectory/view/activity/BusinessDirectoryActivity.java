package com.whatsapp.businessdirectory.view.activity;

import X.AbstractActivityC58462pW;
import X.AbstractC005102i;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass1B0;
import X.AnonymousClass1B1;
import X.AnonymousClass1B2;
import X.AnonymousClass5IP;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13000ix;
import X.C14850m9;
import X.C14960mK;
import X.C16430p0;
import X.C18360sK;
import X.C22660zR;
import X.C251118d;
import X.C252618s;
import X.C43511x9;
import X.C48232Fc;
import X.C66743Ot;
import X.C68563Vw;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchFragment;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchQueryFragment;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectoryActivityViewModel;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.Timer;
import java.util.TimerTask;

/* loaded from: classes2.dex */
public class BusinessDirectoryActivity extends AbstractActivityC58462pW {
    public Menu A00;
    public C48232Fc A01;
    public C251118d A02;
    public AnonymousClass1B2 A03;
    public C16430p0 A04;
    public AnonymousClass1B0 A05;
    public AnonymousClass1B1 A06;
    public BusinessDirectorySearchQueryFragment A07;
    public BusinessDirectoryActivityViewModel A08;
    public C18360sK A09;
    public C22660zR A0A;
    public C252618s A0B;
    public TimerTask A0C;
    public boolean A0D;
    public boolean A0E;
    public final Timer A0F = new Timer();

    public final BusinessDirectorySearchFragment A2e() {
        AnonymousClass01E A0A = A0V().A0A("BusinessDirectorySearchFragment");
        if (A0A instanceof BusinessDirectorySearchFragment) {
            return (BusinessDirectorySearchFragment) A0A;
        }
        return null;
    }

    public void A2f() {
        C48232Fc r0 = this.A01;
        if (r0 != null && !r0.A05()) {
            this.A01.A01();
            C14850m9 r1 = this.A02.A00;
            if (!r1.A07(450) || !r1.A07(1883)) {
                C48232Fc r2 = this.A01;
                String string = getString(R.string.biz_dir_search_query_hint);
                SearchView searchView = r2.A02;
                if (searchView != null) {
                    searchView.setQueryHint(string);
                }
            } else {
                TimerTask timerTask = this.A0C;
                if (timerTask != null) {
                    timerTask.cancel();
                }
                AnonymousClass5IP r12 = new AnonymousClass5IP(this);
                this.A0C = r12;
                this.A0F.schedule(r12, 0, 4000);
            }
            this.A01.A02.requestFocus();
            C12960it.A0z(this.A01.A06.findViewById(R.id.search_back), this, 9);
        }
    }

    public void A2g() {
        Menu menu = this.A00;
        if (menu != null && menu.findItem(1) == null) {
            this.A00.add(0, 1, 0, getString(R.string.search)).setIcon(R.drawable.ic_action_search).setShowAsAction(2);
        }
        this.A0D = true;
    }

    public final void A2h(AnonymousClass01E r5, boolean z) {
        String A0r = C12980iv.A0r(r5);
        AnonymousClass01F A0V = A0V();
        if (A0V.A0A(A0r) == null) {
            C004902f r1 = new C004902f(A0V);
            r1.A0B(r5, A0r, R.id.business_search_container_view);
            if (z) {
                r1.A0F(A0r);
            }
            r1.A01();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C48232Fc r0 = this.A01;
        if (r0 != null && r0.A05()) {
            BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = this.A07;
            if (businessDirectorySearchQueryFragment != null) {
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = businessDirectorySearchQueryFragment.A09;
                synchronized (businessDirectorySearchQueryViewModel.A0a) {
                    businessDirectorySearchQueryViewModel.A0A();
                    C16430p0 r3 = businessDirectorySearchQueryViewModel.A0L;
                    r3.A08(businessDirectorySearchQueryViewModel.A0N.A01(), C12980iv.A0l(businessDirectorySearchQueryViewModel.A02), C12980iv.A0l(businessDirectorySearchQueryViewModel.A01), null, null, null, null, C12980iv.A0l(businessDirectorySearchQueryViewModel.A03), null, 44);
                    C68563Vw r1 = businessDirectorySearchQueryViewModel.A0P;
                    if (!r1.A02) {
                        r3.A0D(r1.A01());
                    }
                }
            }
            this.A01.A04(true);
        }
        ((ActivityC001000l) this).A04.A00();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        BusinessDirectorySearchFragment businessDirectorySearchFragment;
        super.onCreate(bundle);
        setContentView(R.layout.activity_business_directory);
        if (bundle != null) {
            this.A0D = bundle.getBoolean("arg_show_search_menu", false);
            this.A0E = bundle.getBoolean("arg_show_search_view", false);
        }
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0N(true);
        A0N.A0M(true);
        this.A01 = new C48232Fc(this, findViewById(R.id.search_holder), new C66743Ot(this), A0Q, ((ActivityC13830kP) this).A01);
        if (this.A0E) {
            A2f();
        }
        setTitle(getString(R.string.biz_screen_title_v2));
        this.A08 = (BusinessDirectoryActivityViewModel) C13000ix.A02(this).A00(BusinessDirectoryActivityViewModel.class);
        if (bundle == null) {
            Parcelable parcelableExtra = getIntent().getParcelableExtra("INITIAL_CATEGORY");
            if (parcelableExtra == null) {
                Jid jid = (Jid) getIntent().getParcelableExtra("directory_biz_chaining_jid");
                String stringExtra = getIntent().getStringExtra("directory_biz_chaining_name");
                if (jid == null || stringExtra == null) {
                    A2h(new BusinessDirectorySearchQueryFragment(), false);
                    A2f();
                    return;
                }
                businessDirectorySearchFragment = BusinessDirectorySearchFragment.A02(jid, stringExtra);
            } else {
                businessDirectorySearchFragment = new BusinessDirectorySearchFragment();
                Bundle A0D = C12970iu.A0D();
                A0D.putParcelable("INITIAL_CATEGORY", parcelableExtra);
                businessDirectorySearchFragment.A0U(A0D);
            }
            A2h(businessDirectorySearchFragment, false);
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 4, 1, getString(R.string.biz_dir_contact_us_browsing));
        this.A00 = menu;
        if (this.A0D) {
            A2g();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        TimerTask timerTask = this.A0C;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 1) {
            A2h(new BusinessDirectorySearchQueryFragment(), true);
            A2f();
            return true;
        } else if (itemId == 2) {
            C12960it.A0t(this.A08.A00.A02.A00().edit(), "show_request_permission_dialog", true);
            Toast.makeText(this, (int) R.string.biz_dir_search_location_wiped, 0).show();
            return true;
        } else if (itemId == 3) {
            try {
                startActivity((Intent) Class.forName("com.whatsapp.businessdirectory.view.activity.DirectoryMapActivity").getDeclaredMethod("createStartIntent", Context.class).invoke(null, this));
                return true;
            } catch (Exception e) {
                Log.e("BusinessDirectoryActivity/onOptionsItemSelected", e);
                return false;
            }
        } else if (itemId == 4) {
            startActivity(new C14960mK().A0e(this, null, null, "biz-directory-browsing", null));
            return true;
        } else if (itemId != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            BusinessDirectorySearchFragment A2e = A2e();
            if (A2e == null || !A2e.A0e()) {
                ((ActivityC001000l) this).A04.A00();
                return true;
            }
            A2e.A09.A08();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        if (this.A0A.A00) {
            Log.i("BusinessDirectoryActivity/onResume WhatsApp login failed");
            this.A09.A04(20, null);
            C43511x9.A00(this);
        }
        super.onResume();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r0 == null) goto L_0x0010;
     */
    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSaveInstanceState(android.os.Bundle r4) {
        /*
            r3 = this;
            super.onSaveInstanceState(r4)
            android.view.Menu r1 = r3.A00
            r2 = 0
            r0 = 1
            if (r1 == 0) goto L_0x0010
            android.view.MenuItem r0 = r1.findItem(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            java.lang.String r0 = "arg_show_search_menu"
            r4.putBoolean(r0, r1)
            X.2Fc r0 = r3.A01
            if (r0 == 0) goto L_0x0021
            boolean r0 = r0.A05()
            if (r0 == 0) goto L_0x0021
            r2 = 1
        L_0x0021:
            java.lang.String r0 = "arg_show_search_view"
            r4.putBoolean(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity.onSaveInstanceState(android.os.Bundle):void");
    }
}
