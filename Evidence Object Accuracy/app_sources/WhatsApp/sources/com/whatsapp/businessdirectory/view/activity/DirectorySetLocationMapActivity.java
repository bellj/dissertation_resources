package com.whatsapp.businessdirectory.view.activity;

import X.AbstractC115695So;
import X.AbstractC115705Sp;
import X.AbstractC115725Sr;
import X.AbstractC115755Su;
import X.AbstractC116575Vz;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC48942In;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1B0;
import X.AnonymousClass1B3;
import X.AnonymousClass1B5;
import X.AnonymousClass1P6;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3AS;
import X.AnonymousClass3ER;
import X.AnonymousClass3LU;
import X.AnonymousClass4IX;
import X.AnonymousClass54O;
import X.C004802e;
import X.C102864pp;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16430p0;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C244615p;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C35961j4;
import X.C41691tw;
import X.C48122Ek;
import X.C56462kv;
import X.C618632v;
import X.C65193Io;
import X.C89324Jn;
import X.DialogInterface$OnClickListenerC96414fP;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DirectorySetLocationMapActivity extends ActivityC13790kL implements AnonymousClass1P6 {
    public Bundle A00;
    public C35961j4 A01;
    public C244615p A02;
    public C21740xu A03;
    public C16430p0 A04;
    public AnonymousClass3ER A05;
    public AnonymousClass1B0 A06;
    public AnonymousClass1B3 A07;
    public AnonymousClass3LU A08;
    public C15890o4 A09;
    public AnonymousClass018 A0A;
    public C618632v A0B;
    public C16030oK A0C;
    public WhatsAppLibLoader A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final AbstractC115755Su A0H;

    public DirectorySetLocationMapActivity() {
        this(0);
        this.A0G = true;
        this.A0H = new AbstractC115755Su() { // from class: X.50h
            @Override // X.AbstractC115755Su
            public final void ASP(C35961j4 r2) {
                DirectorySetLocationMapActivity.A02(r2, DirectorySetLocationMapActivity.this);
            }
        };
    }

    public DirectorySetLocationMapActivity(int i) {
        this.A0E = false;
        A0R(new C102864pp(this));
    }

    public static /* synthetic */ void A02(C35961j4 r7, DirectorySetLocationMapActivity directorySetLocationMapActivity) {
        C35961j4 r1;
        AnonymousClass4IX A02;
        Double d;
        if (directorySetLocationMapActivity.A01 == null) {
            directorySetLocationMapActivity.A01 = r7;
            AnonymousClass009.A06(r7, "DirectorySetLocationMapActivity/setUpMap map is not available");
            AnonymousClass3LU r2 = directorySetLocationMapActivity.A08;
            AnonymousClass009.A06(r2.A03, "DirectorySetLocationMapActivity/setUpMap ui.centerView is not available");
            AnonymousClass009.A06(r2.A01, "DirectorySetLocationMapActivity/setUpMap ui.centerFillerView is not available");
            AnonymousClass009.A06(r2.A02, "DirectorySetLocationMapActivity/setUpMap ui.centerPinView is not available");
            r7.A0M(false);
            directorySetLocationMapActivity.A01.A0K(false);
            if (directorySetLocationMapActivity.A09.A03() && directorySetLocationMapActivity.A08.A0E) {
                directorySetLocationMapActivity.A01.A0L(true);
            } else if (directorySetLocationMapActivity.A09.A03()) {
                AnonymousClass3LU r12 = directorySetLocationMapActivity.A08;
                if (!r12.A0E) {
                    r12.A01(new C89324Jn(directorySetLocationMapActivity));
                }
            }
            directorySetLocationMapActivity.A01.A01().A00();
            directorySetLocationMapActivity.A01.A0H(new AbstractC115725Sr() { // from class: X.50g
                @Override // X.AbstractC115725Sr
                public final void ASM(LatLng latLng) {
                    DirectorySetLocationMapActivity.this.A08.A03.setVisibility(0);
                }
            });
            directorySetLocationMapActivity.A01.A0F(new AbstractC115705Sp() { // from class: X.3TC
                @Override // X.AbstractC115705Sp
                public final void ANd(int i) {
                    DirectorySetLocationMapActivity directorySetLocationMapActivity2 = DirectorySetLocationMapActivity.this;
                    if (i == 1) {
                        directorySetLocationMapActivity2.A08.A01.setVisibility(0);
                        directorySetLocationMapActivity2.A08.A02.startAnimation(C12960it.A0H(directorySetLocationMapActivity2.A08.A01.getHeight()));
                        directorySetLocationMapActivity2.A08.A03.setVisibility(0);
                    }
                }
            });
            directorySetLocationMapActivity.A01.A0E(new AbstractC115695So() { // from class: X.3T9
                @Override // X.AbstractC115695So
                public final void ANc() {
                    DirectorySetLocationMapActivity directorySetLocationMapActivity2 = DirectorySetLocationMapActivity.this;
                    if (directorySetLocationMapActivity2.A08.A01.getVisibility() == 0) {
                        directorySetLocationMapActivity2.A08.A01.setVisibility(8);
                        directorySetLocationMapActivity2.A08.A02.startAnimation(C12960it.A0H(-directorySetLocationMapActivity2.A08.A01.getHeight()));
                    }
                    C35961j4 r13 = directorySetLocationMapActivity2.A01;
                    AnonymousClass009.A06(r13, "DirectorySetLocationMapActivity/setUpMap map is not available");
                    directorySetLocationMapActivity2.A08.A09 = Double.valueOf(r13.A02().A03.A00);
                    directorySetLocationMapActivity2.A08.A0A = Double.valueOf(directorySetLocationMapActivity2.A01.A02().A03.A01);
                    directorySetLocationMapActivity2.A08.A0B = Float.valueOf(directorySetLocationMapActivity2.A01.A02().A02);
                    AnonymousClass3LU r14 = directorySetLocationMapActivity2.A08;
                    if (r14.A0G) {
                        r14.A08 = null;
                        r14.A06.setVisibility(0);
                        AnonymousClass3LU r0 = directorySetLocationMapActivity2.A08;
                        directorySetLocationMapActivity2.A2i(new C68573Vx(directorySetLocationMapActivity2), r0.A09, r0.A0A);
                        return;
                    }
                    C48122Ek A00 = directorySetLocationMapActivity2.A07.A00();
                    if (A00 == null) {
                        A00 = C48122Ek.A00();
                    }
                    directorySetLocationMapActivity2.A08.A02(A00.A06);
                    AnonymousClass3LU r15 = directorySetLocationMapActivity2.A08;
                    r15.A08 = A00;
                    r15.A0G = true;
                }
            });
            int dimensionPixelSize = directorySetLocationMapActivity.getResources().getDimensionPixelSize(R.dimen.map_padding);
            directorySetLocationMapActivity.A01.A08(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
            Bundle bundle = directorySetLocationMapActivity.A00;
            if (bundle != null) {
                if (bundle.containsKey("camera_zoom")) {
                    float f = directorySetLocationMapActivity.A00.getFloat("camera_zoom");
                    double d2 = directorySetLocationMapActivity.A00.getDouble("camera_lat");
                    double d3 = directorySetLocationMapActivity.A00.getDouble("camera_lng");
                    directorySetLocationMapActivity.A08.A0G = directorySetLocationMapActivity.A00.getBoolean("should_update_address");
                    directorySetLocationMapActivity.A01.A0A(C65193Io.A02(new LatLng(d2, d3), f));
                }
                directorySetLocationMapActivity.A00 = null;
            } else {
                AnonymousClass3LU r22 = directorySetLocationMapActivity.A08;
                Double d4 = r22.A09;
                if (d4 == null || (d = r22.A0A) == null) {
                    C48122Ek A00 = directorySetLocationMapActivity.A07.A00();
                    if (A00 == null) {
                        A00 = C48122Ek.A00();
                    }
                    float floatValue = directorySetLocationMapActivity.A08.A0B.floatValue();
                    if ("city_default".equals(A00.A07)) {
                        floatValue = 10.0f;
                    }
                    LatLng latLng = new LatLng(A00.A03.doubleValue(), A00.A04.doubleValue());
                    r1 = directorySetLocationMapActivity.A01;
                    A02 = C65193Io.A02(latLng, floatValue);
                } else {
                    Float f2 = r22.A0B;
                    LatLng latLng2 = new LatLng(d4.doubleValue(), d.doubleValue());
                    r1 = directorySetLocationMapActivity.A01;
                    A02 = C65193Io.A02(latLng2, f2.floatValue());
                }
                r1.A0A(A02);
            }
            if (C41691tw.A08(directorySetLocationMapActivity)) {
                C56462kv.A00(directorySetLocationMapActivity, R.raw.night_map_style_json);
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0E) {
            this.A0E = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A03 = (C21740xu) r2.AM1.get();
            this.A0A = (AnonymousClass018) r2.ANb.get();
            this.A0D = (WhatsAppLibLoader) r2.ANa.get();
            this.A09 = (C15890o4) r2.AN1.get();
            this.A02 = (C244615p) r2.A8H.get();
            this.A0C = (C16030oK) r2.AAd.get();
            this.A04 = (C16430p0) r2.A5y.get();
            this.A07 = (AnonymousClass1B3) r2.AI0.get();
            this.A06 = (AnonymousClass1B0) r2.A2K.get();
            this.A05 = new AnonymousClass3ER((AbstractC48942In) r1.A0W.get(), (AnonymousClass1B5) r2.A5t.get());
        }
    }

    public final void A2e() {
        AnonymousClass3ER r5 = this.A05;
        AnonymousClass3LU r4 = this.A08;
        r5.A01(new LatLng(r4.A09.doubleValue(), r4.A0A.doubleValue()), this, r4.A0C, "pin_on_map", 10.0f);
    }

    public final void A2f() {
        C35961j4 r0 = this.A01;
        if (r0 != null) {
            r0.A0L(true);
            this.A08.A00();
            View view = this.A08.A03;
            if (view != null) {
                view.setVisibility(0);
            }
            C618632v r02 = this.A0B;
            r02.A03 = 1;
            r02.A0A(1);
        }
    }

    public final void A2g() {
        AaN();
        Adr(new Object[0], R.string.biz_dir_location_generic_error, R.string.biz_dir_generic_error);
        this.A04.A05(3, 28, 2);
    }

    public final void A2h(DialogInterface.OnClickListener onClickListener, int i) {
        int i2;
        AaN();
        if (i == -1) {
            AaN();
            C004802e r2 = new C004802e(this);
            r2.A07(R.string.biz_dir_location_generic_error);
            r2.A06(R.string.biz_dir_network_error);
            r2.setPositiveButton(R.string.try_again, onClickListener);
            r2.setNegativeButton(R.string.cancel, null);
            r2.A05();
            i2 = 1;
        } else if (i == 1 || i == 2 || i == 3) {
            A2g();
            return;
        } else if (i == 4) {
            AnonymousClass3AS.A00(this, this.A03);
            i2 = 6;
        } else {
            return;
        }
        this.A04.A05(3, 28, i2);
    }

    public void A2i(AbstractC116575Vz r8, Double d, Double d2) {
        if (!((ActivityC13810kN) this).A07.A0B()) {
            r8.AQs(-1);
        } else {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, d, d2, r8, 10));
        }
    }

    @Override // X.AnonymousClass1P6
    public void ARL(int i) {
        A2h(new DialogInterface.OnClickListener() { // from class: X.4fO
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                DirectorySetLocationMapActivity.this.A2e();
            }
        }, i);
    }

    @Override // X.AnonymousClass1P6
    public void ARM(C48122Ek r3) {
        this.A08.A08 = r3;
        try {
            this.A06.A01(r3);
            AaN();
            setResult(-1);
            finish();
        } catch (Exception e) {
            A2g();
            Log.e("DirectoryUserLocationPickerUI/onOptionsItemSelected Failed to store search location", e);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 34) {
            LocationSharingService.A01(getApplicationContext(), this.A0C);
            if (i2 == -1) {
                AnonymousClass3LU r0 = this.A08;
                r0.A0D = true;
                r0.A0J.A02.A00().edit().putBoolean("DIRECTORY_LOCATION_INFO_ACCEPTED", true).apply();
                A2f();
                return;
            }
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r15) {
        /*
        // Method dump skipped, instructions count: 450
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        AnonymousClass3LU r1 = this.A08;
        if (i != 2) {
            return super.onCreateDialog(i);
        }
        DialogInterface$OnClickListenerC96414fP r2 = new DialogInterface.OnClickListener() { // from class: X.4fP
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                AnonymousClass3LU r3 = AnonymousClass3LU.this;
                r3.A07.startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 0);
                C36021jC.A00(r3.A07, 2);
            }
        };
        C004802e r12 = new C004802e(r1.A07);
        r12.A07(R.string.gps_required_title);
        r12.A06(R.string.gps_required_body);
        r12.setNegativeButton(R.string.cancel, null);
        r12.A0B(true);
        r12.setPositiveButton(R.string.biz_dir_open_settings, r2);
        return r12.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, getString(R.string.done)).setShowAsAction(2);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0B.A00();
        super.onDestroy();
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        this.A0B.A01();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 0) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2C(R.string.biz_dir_set_location_on_map_progress_dialog_message);
        if (!TextUtils.isEmpty(this.A08.A0C)) {
            A2e();
            return true;
        }
        AnonymousClass3LU r0 = this.A08;
        A2i(new AnonymousClass54O(this), r0.A09, r0.A0A);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        this.A0B.A02();
        C618632v r0 = this.A0B;
        SensorManager sensorManager = r0.A05;
        if (sensorManager != null) {
            sensorManager.unregisterListener(r0.A0C);
        }
        this.A0F = this.A09.A03();
        AnonymousClass3LU r1 = this.A08;
        r1.A0H.A04(r1);
        super.onPause();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        C35961j4 r1;
        super.onResume();
        if (this.A09.A03() != this.A0F && this.A09.A03() && this.A08.A0D && (r1 = this.A01) != null) {
            r1.A0L(true);
        }
        this.A0B.A03();
        this.A0B.A08();
        if (this.A01 == null) {
            this.A01 = this.A0B.A07(this.A0H);
        }
        AnonymousClass3LU r12 = this.A08;
        r12.A0H.A05(r12, "user-location-picker", 0.0f, 3, 5000, 1000);
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        if (this.A01 != null) {
            bundle.putFloat("camera_zoom", this.A08.A0B.floatValue());
            bundle.putDouble("camera_lat", this.A08.A09.doubleValue());
            bundle.putDouble("camera_lng", this.A08.A0A.doubleValue());
            bundle.putBoolean("should_update_address", this.A08.A0G);
            bundle.putInt("map_location_mode", this.A0B.A03);
        }
        bundle.putBoolean("zoom_to_user", this.A0G);
        this.A0B.A05(bundle);
        this.A08.A03.setVisibility(0);
        super.onSaveInstanceState(bundle);
    }
}
