package com.whatsapp.businessdirectory.view.fragment;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass1B8;
import X.AnonymousClass1B9;
import X.AnonymousClass3CJ;
import X.AnonymousClass4JK;
import X.C12960it;
import X.C12970iu;
import X.C21740xu;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;

/* loaded from: classes2.dex */
public abstract class Hilt_BusinessDirectorySearchFragment extends AnonymousClass01E implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A19()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.view.fragment.Hilt_BusinessDirectorySearchFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            BusinessDirectorySearchFragment businessDirectorySearchFragment = (BusinessDirectorySearchFragment) this;
            C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r4 = r3.A0Y;
            businessDirectorySearchFragment.A02 = (C21740xu) r4.AM1.get();
            businessDirectorySearchFragment.A0B = C12960it.A0R(r4);
            businessDirectorySearchFragment.A0A = C12970iu.A0Y(r4);
            businessDirectorySearchFragment.A05 = r3.A03();
            businessDirectorySearchFragment.A03 = C12970iu.A0V(r4);
            businessDirectorySearchFragment.A07 = new AnonymousClass3CJ((AnonymousClass1B8) r4.A2C.get());
            businessDirectorySearchFragment.A04 = (AnonymousClass1B9) r4.A1P.get();
            businessDirectorySearchFragment.A01 = (AnonymousClass4JK) r3.A02.get();
            businessDirectorySearchFragment.A06 = r3.A04();
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
