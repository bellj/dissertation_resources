package com.whatsapp.businessdirectory.util;

import X.AbstractC14440lR;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.C14900mE;
import X.C16590pI;
import X.C244615p;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import androidx.lifecycle.OnLifecycleEvent;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;

/* loaded from: classes.dex */
public class LocationUpdateListener implements LocationListener, AnonymousClass03H {
    public boolean A00;
    public final AnonymousClass016 A01 = new AnonymousClass016();
    public final C244615p A02;
    public final C14900mE A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final AbstractC14440lR A06;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public LocationUpdateListener(C244615p r2, C14900mE r3, C16590pI r4, AnonymousClass018 r5, AbstractC14440lR r6) {
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r6;
        this.A05 = r5;
        this.A02 = r2;
    }

    public void A00() {
        this.A00 = true;
        disconnectListener();
        connectListener();
    }

    public final void A01(Location location) {
        this.A06.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(this, 26, location));
    }

    @OnLifecycleEvent(AnonymousClass074.ON_RESUME)
    private void connectListener() {
        if (this.A00) {
            this.A02.A05(this, "user-location-picker", 100.0f, 3, 1000, 1000);
        }
    }

    @OnLifecycleEvent(AnonymousClass074.ON_PAUSE)
    private void disconnectListener() {
        this.A02.A04(this);
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        this.A00 = false;
        A01(location);
    }
}
