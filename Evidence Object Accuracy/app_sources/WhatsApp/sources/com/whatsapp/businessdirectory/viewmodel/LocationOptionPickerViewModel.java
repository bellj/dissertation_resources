package com.whatsapp.businessdirectory.viewmodel;

import X.AbstractC116655Wh;
import X.AnonymousClass014;
import X.AnonymousClass016;
import X.AnonymousClass1B0;
import X.AnonymousClass1B3;
import X.C12960it;
import X.C12980iv;
import X.C13000ix;
import X.C15890o4;
import X.C16430p0;
import X.C16590pI;
import X.C251118d;
import X.C27691It;

/* loaded from: classes2.dex */
public class LocationOptionPickerViewModel extends AnonymousClass014 implements AbstractC116655Wh {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final C251118d A01;
    public final C16430p0 A02;
    public final AnonymousClass1B0 A03;
    public final AnonymousClass1B3 A04;
    public final C16590pI A05;
    public final C15890o4 A06;
    public final C27691It A07 = C13000ix.A03();

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004b, code lost:
        if (((android.location.LocationManager) r4.A05.A00.getSystemService("location")).isProviderEnabled("gps") == false) goto L_0x004d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LocationOptionPickerViewModel(android.app.Application r5, X.C251118d r6, X.C16430p0 r7, X.AnonymousClass1B0 r8, X.AnonymousClass1B3 r9, X.C16590pI r10, X.C15890o4 r11) {
        /*
            r4 = this;
            r4.<init>(r5)
            X.1It r0 = X.C13000ix.A03()
            r4.A07 = r0
            X.016 r0 = X.C12980iv.A0T()
            r4.A00 = r0
            r4.A05 = r10
            r4.A02 = r7
            r4.A06 = r11
            r4.A01 = r6
            r4.A04 = r9
            r4.A03 = r8
            java.util.ArrayList r2 = X.C12960it.A0l()
            X.40G r3 = new X.40G
            r3.<init>(r4)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 31
            if (r1 < r0) goto L_0x004d
            boolean r0 = r11.A03()
            if (r0 == 0) goto L_0x004d
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = r11.A02(r0)
            if (r0 == 0) goto L_0x004d
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            java.lang.String r0 = "location"
            java.lang.Object r1 = r1.getSystemService(r0)
            android.location.LocationManager r1 = (android.location.LocationManager) r1
            java.lang.String r0 = "gps"
            boolean r0 = r1.isProviderEnabled(r0)
            r1 = 1
            if (r0 != 0) goto L_0x004e
        L_0x004d:
            r1 = 0
        L_0x004e:
            r3.A00 = r1
            r2.add(r3)
            X.18d r1 = r4.A01
            X.0pI r0 = r4.A05
            android.content.Context r0 = r0.A00
            boolean r0 = r1.A05(r0)
            if (r0 == 0) goto L_0x006d
            X.40E r0 = new X.40E
            r0.<init>(r4)
        L_0x0064:
            r2.add(r0)
            X.016 r0 = r4.A00
            r0.A0A(r2)
            return
        L_0x006d:
            X.40F r0 = new X.40F
            r0.<init>(r4)
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.viewmodel.LocationOptionPickerViewModel.<init>(android.app.Application, X.18d, X.0p0, X.1B0, X.1B3, X.0pI, X.0o4):void");
    }

    public final void A04() {
        if (this.A06.A03()) {
            this.A03.A00();
            C12960it.A1A(this.A07, 2);
            return;
        }
        this.A07.A0A(C12960it.A0V());
    }

    @Override // X.AbstractC116655Wh
    public void ASD() {
        this.A02.A01(3, 1);
        this.A03.A02(true);
        A04();
    }

    @Override // X.AbstractC116655Wh
    public void ASE() {
        ASF();
    }

    @Override // X.AbstractC116655Wh
    public void ASF() {
        this.A02.A01(4, 1);
    }
}
