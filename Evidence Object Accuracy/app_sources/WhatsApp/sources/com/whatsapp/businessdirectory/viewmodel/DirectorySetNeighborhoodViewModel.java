package com.whatsapp.businessdirectory.viewmodel;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass1B0;
import X.AnonymousClass1B3;
import X.AnonymousClass1CO;
import X.AnonymousClass404;
import X.AnonymousClass406;
import X.AnonymousClass4NC;
import X.AnonymousClass4T7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16430p0;
import X.C84793zv;
import android.text.TextUtils;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.text.CollationKey;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class DirectorySetNeighborhoodViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C16430p0 A02;
    public final AnonymousClass1CO A03;
    public final AnonymousClass1B0 A04;
    public final AnonymousClass1B3 A05;
    public final AnonymousClass018 A06;
    public final AbstractC14440lR A07;
    public final List A08;

    public DirectorySetNeighborhoodViewModel(C16430p0 r5, AnonymousClass1CO r6, AnonymousClass1B0 r7, AnonymousClass1B3 r8, AnonymousClass018 r9, AbstractC14440lR r10) {
        ArrayList A0l = C12960it.A0l();
        this.A08 = A0l;
        this.A07 = r10;
        this.A06 = r9;
        this.A02 = r5;
        this.A03 = r6;
        this.A05 = r8;
        this.A04 = r7;
        A0l.add(0, r6.A00());
        ArrayList A0l2 = C12960it.A0l();
        A0l2.add(new AnonymousClass406(0));
        A0l2.addAll(A04(((AnonymousClass4T7) A0l.get(0)).A05));
        A07(A0l2);
    }

    public final List A04(List list) {
        ArrayList A0l = C12960it.A0l();
        if (list.isEmpty()) {
            A0l.add(new C84793zv());
        } else {
            int i = 0;
            while (i < list.size()) {
                AnonymousClass4T7 r3 = (AnonymousClass4T7) list.get(i);
                i++;
                A0l.add(new AnonymousClass404(new ViewOnClickCListenerShape2S0201000_I1(this, r3, i, 1), r3.A04));
            }
        }
        return A0l;
    }

    public final void A05(AnonymousClass4T7 r6) {
        ArrayList A0l = C12960it.A0l();
        A0l.add(new AnonymousClass406(1));
        A0l.addAll(A04(r6.A05));
        List list = this.A08;
        if (list.size() == 1) {
            list.add(0, r6);
        } else {
            list.set(0, r6);
        }
        A07(A0l);
    }

    public final void A06(String str, List list, List list2) {
        int i;
        if (!TextUtils.isEmpty(str.trim())) {
            int length = str.length();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass4T7 r6 = (AnonymousClass4T7) it.next();
                Collator instance = Collator.getInstance(C12970iu.A14(this.A06));
                int i2 = 0;
                instance.setStrength(0);
                CollationKey collationKey = instance.getCollationKey(str);
                while (true) {
                    String str2 = r6.A04;
                    int length2 = str2.length();
                    if (i2 < length2 && (i = i2 + length) <= length2) {
                        if (collationKey.compareTo(instance.getCollationKey(str2.substring(i2, i))) == 0 && !list2.contains(r6)) {
                            list2.add(r6);
                        }
                        i2++;
                    }
                }
                A06(str, r6.A05, list2);
            }
        }
    }

    public final void A07(List list) {
        String str;
        List list2 = this.A08;
        int size = list2.size();
        AnonymousClass016 r2 = this.A00;
        if (size == 1) {
            str = CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID;
        } else {
            str = ((AnonymousClass4T7) C12980iv.A0o(list2)).A04;
        }
        r2.A0A(new AnonymousClass4NC(str, list));
    }
}
