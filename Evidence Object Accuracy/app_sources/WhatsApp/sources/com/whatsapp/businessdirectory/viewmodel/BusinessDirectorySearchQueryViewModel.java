package com.whatsapp.businessdirectory.viewmodel;

import X.AbstractC115855Te;
import X.AbstractC115865Tf;
import X.AbstractC115875Tg;
import X.AbstractC115885Th;
import X.AbstractC115895Ti;
import X.AbstractC115915Tk;
import X.AbstractC115925Tl;
import X.AbstractC49232Jx;
import X.AbstractC49242Jy;
import X.AbstractC49252Jz;
import X.AnonymousClass009;
import X.AnonymousClass014;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass02B;
import X.AnonymousClass02P;
import X.AnonymousClass07E;
import X.AnonymousClass1B2;
import X.AnonymousClass1B3;
import X.AnonymousClass2Jw;
import X.AnonymousClass2K0;
import X.AnonymousClass2K2;
import X.AnonymousClass2K3;
import X.AnonymousClass2K4;
import X.AnonymousClass3E8;
import X.AnonymousClass3M8;
import X.AnonymousClass401;
import X.AnonymousClass405;
import X.AnonymousClass407;
import X.AnonymousClass430;
import X.AnonymousClass4G8;
import X.AnonymousClass4N3;
import X.AnonymousClass4N7;
import X.AnonymousClass4N8;
import X.AnonymousClass4RT;
import X.AnonymousClass4T8;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C16430p0;
import X.C251118d;
import X.C27691It;
import X.C28531Ny;
import X.C30211Wn;
import X.C48112Ej;
import X.C48122Ek;
import X.C48142Em;
import X.C48152En;
import X.C48172Ep;
import X.C59482uo;
import X.C59512ur;
import X.C63363Bh;
import X.C68433Vj;
import X.C68553Vv;
import X.C68563Vw;
import X.C84813zx;
import X.C90954Pw;
import X.C91524Sb;
import X.C92174Uv;
import android.app.Application;
import android.os.Handler;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class BusinessDirectorySearchQueryViewModel extends AnonymousClass014 implements AnonymousClass2Jw, AbstractC49232Jx, AbstractC49242Jy, AbstractC49252Jz {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public C30211Wn A05;
    public Runnable A06 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 23);
    public Runnable A07;
    public LinkedList A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final Handler A0C;
    public final Handler A0D;
    public final AnonymousClass017 A0E;
    public final AnonymousClass017 A0F;
    public final AnonymousClass02P A0G;
    public final AnonymousClass07E A0H;
    public final C14900mE A0I;
    public final C251118d A0J;
    public final AnonymousClass1B2 A0K;
    public final C16430p0 A0L;
    public final AnonymousClass2K0 A0M;
    public final AnonymousClass1B3 A0N;
    public final C92174Uv A0O;
    public final C68563Vw A0P;
    public final AnonymousClass3E8 A0Q;
    public final AnonymousClass2K2 A0R;
    public final C68553Vv A0S;
    public final AnonymousClass4N8 A0T;
    public final C68433Vj A0U;
    public final C14830m7 A0V;
    public final C27691It A0W = new C27691It();
    public final C27691It A0X;
    public final C27691It A0Y = new C27691It();
    public final C27691It A0Z;
    public final LinkedList A0a;
    public volatile boolean A0b;

    @Override // X.AbstractC49232Jx
    public void AO0() {
    }

    public BusinessDirectorySearchQueryViewModel(Application application, AnonymousClass07E r15, C14900mE r16, C251118d r17, AnonymousClass1B2 r18, C16430p0 r19, AnonymousClass2K0 r20, AnonymousClass1B3 r21, C92174Uv r22, AbstractC115855Te r23, AnonymousClass3E8 r24, AbstractC115915Tk r25, C68553Vv r26, AnonymousClass4N8 r27, AbstractC115925Tl r28, C14830m7 r29) {
        super(application);
        this.A0V = r29;
        this.A0I = r16;
        this.A0H = r15;
        this.A0D = new Handler();
        this.A0C = new Handler();
        this.A0a = new LinkedList();
        AnonymousClass02P r2 = new AnonymousClass02P();
        this.A0G = r2;
        this.A0Z = new C27691It();
        this.A0X = new C27691It();
        this.A0J = r17;
        this.A0O = r22;
        this.A0M = r20;
        this.A0K = r18;
        this.A0N = r21;
        this.A0Q = r24;
        this.A0L = r19;
        r20.A09 = this;
        AnonymousClass2K2 A7v = r25.A7v(new AbstractC115895Ti() { // from class: X.54U
            @Override // X.AbstractC115895Ti
            public final boolean AJK() {
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = BusinessDirectorySearchQueryViewModel.this;
                C92174Uv r0 = businessDirectorySearchQueryViewModel.A0O;
                C48122Ek A04 = businessDirectorySearchQueryViewModel.A04();
                C14850m9 r1 = r0.A00.A00;
                return r1.A07(450) && r1.A07(1882) && A04 != null && A04.A04();
            }
        });
        this.A0R = A7v;
        boolean z = false;
        this.A04 = 0;
        this.A08 = new LinkedList();
        Map map = r15.A02;
        if (map.get("business_search_queries") != null) {
            this.A08.addAll((Collection) map.get("business_search_queries"));
        }
        this.A09 = map.get("saved_search_session_started") != null ? ((Boolean) map.get("saved_search_session_started")).booleanValue() : z;
        A7v.A08(r15);
        C68563Vw A81 = r23.A81(new AbstractC115865Tf() { // from class: X.54P
            @Override // X.AbstractC115865Tf
            public final boolean AJq() {
                return BusinessDirectorySearchQueryViewModel.this.A0V();
            }
        }, new AbstractC115875Tg() { // from class: X.54R
            @Override // X.AbstractC115875Tg
            public final C48122Ek AGS() {
                return BusinessDirectorySearchQueryViewModel.this.A04();
            }
        }, new AbstractC115885Th() { // from class: X.54T
            @Override // X.AbstractC115885Th
            public final String AGZ() {
                return null;
            }
        }, A7v, this, 0);
        this.A0P = A81;
        C68433Vj A7w = r28.A7w(this, this);
        this.A0U = A7w;
        this.A0T = r27;
        AnonymousClass016 r4 = r27.A00;
        this.A0F = r4;
        this.A0S = r26;
        AnonymousClass016 r1 = r26.A01;
        this.A0E = r1;
        this.A0A = true;
        r20.A07 = r27;
        r2.A0D(r4, new AnonymousClass02B() { // from class: X.4sj
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                BusinessDirectorySearchQueryViewModel.this.A0N((AnonymousClass4RT) obj);
            }
        });
        r2.A0D(r1, new AnonymousClass02B() { // from class: X.4si
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                BusinessDirectorySearchQueryViewModel.this.A0M((C91524Sb) obj);
            }
        });
        r2.A0D(A7w.A00, new AnonymousClass02B() { // from class: X.3Q1
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C59512ur r0;
                int i;
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = BusinessDirectorySearchQueryViewModel.this;
                boolean z2 = true;
                businessDirectorySearchQueryViewModel.A0A = true;
                ArrayList A0l = C12960it.A0l();
                if (businessDirectorySearchQueryViewModel.A0Y()) {
                    A0l.add(obj);
                }
                C68433Vj r5 = businessDirectorySearchQueryViewModel.A0U;
                switch (r5.A00.A00) {
                    case 0:
                    case 2:
                    case 5:
                        C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 8);
                        if (!TextUtils.isEmpty(businessDirectorySearchQueryViewModel.A05())) {
                            if (!businessDirectorySearchQueryViewModel.A0J.A00() || businessDirectorySearchQueryViewModel.A04 != 1) {
                                z2 = false;
                            }
                            A0l.add(new AnonymousClass401(z2));
                        } else {
                            businessDirectorySearchQueryViewModel.A0S(A0l);
                            if (businessDirectorySearchQueryViewModel.A0W()) {
                                A0l.addAll(businessDirectorySearchQueryViewModel.A08());
                            } else {
                                A0l.add(new C84783zu());
                            }
                        }
                        businessDirectorySearchQueryViewModel.A0B();
                        break;
                    case 1:
                        C12970iu.A1Q(businessDirectorySearchQueryViewModel.A0Z, 5);
                        break;
                    case 3:
                        r0 = new C59512ur(businessDirectorySearchQueryViewModel, 0);
                        A0l.add(r0);
                        C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 9);
                        businessDirectorySearchQueryViewModel.A0L.A05(C68433Vj.A00(r5), 28, 0);
                        break;
                    case 4:
                        C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 9);
                        A0l.addAll(businessDirectorySearchQueryViewModel.A0S.A00());
                        break;
                    case 6:
                        i = 6;
                        r0 = new C59512ur(businessDirectorySearchQueryViewModel, i);
                        A0l.add(r0);
                        C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 9);
                        businessDirectorySearchQueryViewModel.A0L.A05(C68433Vj.A00(r5), 28, 0);
                        break;
                    case 7:
                        i = 7;
                        r0 = new C59512ur(businessDirectorySearchQueryViewModel, i);
                        A0l.add(r0);
                        C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 9);
                        businessDirectorySearchQueryViewModel.A0L.A05(C68433Vj.A00(r5), 28, 0);
                        break;
                }
                businessDirectorySearchQueryViewModel.A0G.A0A(A0l);
                businessDirectorySearchQueryViewModel.A0L.A06(C68433Vj.A00(r5), 25, r5.A01());
            }
        });
        r2.A0D(A81.A04, new AnonymousClass02B() { // from class: X.3Q2
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                List list;
                List A0l;
                C37171lc A03;
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = BusinessDirectorySearchQueryViewModel.this;
                C63363Bh r13 = (C63363Bh) obj;
                String str = r13.A06;
                if (str != null) {
                    int i = r13.A02;
                    if (i != 10) {
                        switch (i) {
                            case 1:
                                C12960it.A1A(businessDirectorySearchQueryViewModel.A0Z, 4);
                                return;
                            case 2:
                                list = businessDirectorySearchQueryViewModel.A07();
                                list.addAll(r13.A08);
                                break;
                            case 3:
                                AnonymousClass4N3 r0 = r13.A05;
                                AnonymousClass009.A05(r0);
                                businessDirectorySearchQueryViewModel.A0J(r0.A01, str, 0, r13.A08.size(), r13.A05.A00);
                                return;
                            case 4:
                                AnonymousClass4N3 r02 = r13.A05;
                                AnonymousClass009.A05(r02);
                                businessDirectorySearchQueryViewModel.A0I(r02.A01, str, 0, r13.A08.size(), r13.A05.A00);
                                return;
                            case 5:
                                businessDirectorySearchQueryViewModel.A0L.A05(Integer.valueOf(businessDirectorySearchQueryViewModel.A04().A01()), 28, 7);
                                return;
                            case 6:
                                list = businessDirectorySearchQueryViewModel.A07();
                                AnonymousClass4T8 r03 = r13.A04;
                                if (r03 != null) {
                                    A0l = r03.A05;
                                } else {
                                    A0l = C12960it.A0l();
                                }
                                if (businessDirectorySearchQueryViewModel.A0J.A00() && (A03 = businessDirectorySearchQueryViewModel.A0R.A03(businessDirectorySearchQueryViewModel, A0l)) != null) {
                                    list.add(A03);
                                }
                                list.add(new C59582uy(businessDirectorySearchQueryViewModel, r13.A06, 0));
                                break;
                            case 7:
                                C16430p0 r5 = businessDirectorySearchQueryViewModel.A0L;
                                int i2 = (r13.A00 + 1) * 14;
                                AnonymousClass4T8 r04 = r13.A04;
                                AnonymousClass009.A05(r04);
                                long min = (long) Math.min(i2, r04.A03.size());
                                Integer A01 = businessDirectorySearchQueryViewModel.A0N.A01();
                                C28531Ny r12 = new C28531Ny();
                                C13000ix.A06(r12, 13);
                                r12.A0K = Long.valueOf(min);
                                r12.A03 = A01;
                                r5.A03(r12);
                                return;
                            default:
                                return;
                        }
                        businessDirectorySearchQueryViewModel.A0G.A0A(list);
                        return;
                    }
                    businessDirectorySearchQueryViewModel.A09();
                }
            }
        });
        r2.A0D(r24.A00, new AnonymousClass02B() { // from class: X.3Q3
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = BusinessDirectorySearchQueryViewModel.this;
                List list = (List) obj;
                if (!businessDirectorySearchQueryViewModel.A0X() && !businessDirectorySearchQueryViewModel.A0B) {
                    List A07 = businessDirectorySearchQueryViewModel.A07();
                    if (!list.isEmpty() && businessDirectorySearchQueryViewModel.A0U()) {
                        A07.add(new C59482uo(businessDirectorySearchQueryViewModel, list));
                        businessDirectorySearchQueryViewModel.A0L.A07(businessDirectorySearchQueryViewModel.A0N.A01(), null, null, 47);
                    }
                    if (businessDirectorySearchQueryViewModel.A0W()) {
                        AnonymousClass017 r3 = businessDirectorySearchQueryViewModel.A0E;
                        C91524Sb r0 = (C91524Sb) r3.A01();
                        if (r0 != null) {
                            businessDirectorySearchQueryViewModel.A0M(r0);
                            return;
                        } else if ((r3.A01() == null || ((C91524Sb) r3.A01()).A01 == 0) && businessDirectorySearchQueryViewModel.A0U()) {
                            A07.addAll(businessDirectorySearchQueryViewModel.A08());
                        }
                    } else {
                        AnonymousClass017 r32 = businessDirectorySearchQueryViewModel.A0F;
                        if (businessDirectorySearchQueryViewModel.A0U()) {
                            AnonymousClass4RT r02 = (AnonymousClass4RT) r32.A01();
                            if (r02 != null) {
                                businessDirectorySearchQueryViewModel.A0N(r02);
                                return;
                            } else if (r32.A01() == null || ((AnonymousClass4RT) r32.A01()).A01 == 0) {
                                A07.add(new C84783zu());
                            }
                        }
                    }
                    businessDirectorySearchQueryViewModel.A0G.A0A(A07);
                }
                businessDirectorySearchQueryViewModel.A0B = false;
            }
        });
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C68433Vj r2 = this.A0U;
        C48112Ej r0 = r2.A00;
        r0.A02.removeCallbacks(r0.A08);
        r2.A04.A00();
        r2.A01 = null;
        this.A0S.A02.A06 = null;
        AnonymousClass2K0 r02 = this.A0M;
        r02.A09 = null;
        r02.A07 = null;
    }

    public final C48122Ek A04() {
        C48122Ek A00 = this.A0N.A00();
        return A00 == null ? C48122Ek.A00() : A00;
    }

    public final String A05() {
        String str;
        C63363Bh r0 = (C63363Bh) this.A0P.A04.A01();
        return (r0 == null || (str = r0.A06) == null) ? "" : str;
    }

    public final List A06() {
        C63363Bh r2 = (C63363Bh) this.A0P.A04.A01();
        if (A0V() && r2 != null && this.A04 == 1) {
            List list = r2.A08;
            if (!list.isEmpty()) {
                return list;
            }
        }
        return new ArrayList();
    }

    public final List A07() {
        ArrayList arrayList = new ArrayList();
        C48112Ej r2 = this.A0U.A00;
        Object A01 = r2.A01();
        if (A01 != null) {
            if (A0Y()) {
                arrayList.add(A01);
            }
            if (r2.A00 == 4) {
                arrayList.addAll(this.A0S.A00());
            }
        }
        return arrayList;
    }

    public final List A08() {
        ArrayList arrayList = new ArrayList();
        if (this.A0J.A01()) {
            arrayList.add(new AnonymousClass407(true));
        }
        arrayList.add(new C84813zx());
        return arrayList;
    }

    public void A09() {
        C63363Bh r1;
        AnonymousClass2K4 r4;
        if (A0V()) {
            C68563Vw r3 = this.A0P;
            AnonymousClass016 r2 = r3.A04;
            C63363Bh r0 = (C63363Bh) r2.A01();
            if ((r0 == null || r0.A02 != 9) && this.A04 == 1 && (r1 = (C63363Bh) r2.A01()) != null && r1.A06 != null) {
                r3.A02();
                AnonymousClass2K0 r32 = this.A0M;
                String str = r1.A06;
                C48122Ek A04 = A04();
                boolean A042 = this.A0J.A04();
                if (A0V()) {
                    C63363Bh r02 = (C63363Bh) r2.A01();
                    if (r02 != null) {
                        r4 = r02.A03;
                    } else {
                        r4 = new AnonymousClass2K4(150, null);
                    }
                } else {
                    r4 = null;
                }
                r32.A02(r4, null, A04, str, A042, true);
            }
        }
    }

    public final void A0A() {
        synchronized (this.A0a) {
            C16430p0 r4 = this.A0L;
            LinkedList linkedList = this.A08;
            C14850m9 r1 = r4.A03.A00;
            if (r1.A07(450) && r1.A07(1521) && linkedList != null && !linkedList.isEmpty()) {
                LinkedHashSet linkedHashSet = new LinkedHashSet(linkedList);
                JSONArray jSONArray = new JSONArray();
                Iterator it = linkedHashSet.iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    JSONObject jSONObject = new JSONObject();
                    try {
                        if (AnonymousClass4G8.A00.matcher(str).find()) {
                            str = "number_sequence";
                        } else if (str.length() > 30) {
                            str = str.substring(0, 30);
                            jSONObject.put("long_query_string", true);
                        }
                        jSONObject.put("query_content", str);
                    } catch (JSONException unused) {
                        Log.e("DirectorySearchQueryAnalyticsUtil/getJsonArray: Query JSON mapping failed");
                    }
                    jSONArray.put(jSONObject);
                }
                AnonymousClass430 r12 = new AnonymousClass430();
                r12.A00 = jSONArray.toString();
                r4.A04.A07(r12);
            }
            linkedList.clear();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:70:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B() {
        /*
        // Method dump skipped, instructions count: 334
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel.A0B():void");
    }

    public final void A0C() {
        if (A06().isEmpty()) {
            List A07 = A07();
            boolean z = true;
            if (!this.A0J.A00() || this.A04 != 1) {
                z = false;
            }
            A07.add(new AnonymousClass401(z));
            this.A0G.A0A(A07);
        }
    }

    public final void A0D() {
        ArrayList arrayList = new ArrayList(A06());
        arrayList.add(new C59512ur(this, 2));
        A0T(arrayList);
        this.A0L.A05(Integer.valueOf(this.A0U.A02()), 28, 2);
    }

    public final void A0E() {
        C14850m9 r1 = this.A0J.A00;
        if (!r1.A07(450) || !r1.A07(1705)) {
            A0C();
            return;
        }
        List A07 = A07();
        A07.add(new AnonymousClass405(1));
        this.A0G.A0A(A07);
    }

    public void A0F(int i) {
        int i2;
        long j;
        Boolean bool;
        String str;
        String str2;
        C16430p0 r10 = this.A0L;
        AnonymousClass4T8 r0 = this.A0P.A05.A04;
        if (r0 != null) {
            i2 = r0.A03.size();
        } else {
            i2 = 0;
        }
        long j2 = (long) i2;
        AnonymousClass2K2 r1 = this.A0R;
        List list = r1.A01;
        if (list != null) {
            j = (long) list.size();
        } else {
            j = 0;
        }
        if (r1.A07.AJK()) {
            bool = Boolean.valueOf(r1.A03);
        } else {
            bool = null;
        }
        if (r1.A04) {
            str = "has_catalog";
        } else {
            str = null;
        }
        if (r1.A05) {
            str2 = "open_now";
        } else {
            str2 = null;
        }
        Integer A01 = this.A0N.A01();
        String A05 = r1.A05();
        C28531Ny r12 = new C28531Ny();
        r12.A06 = Integer.valueOf(i);
        r12.A03 = A01;
        r12.A0K = Long.valueOf(j2);
        r12.A0T = str;
        r12.A0N = Long.valueOf(j);
        r12.A00 = bool;
        r12.A0U = str2;
        r12.A0V = A05;
        r10.A04(r12);
    }

    public final void A0G(int i) {
        if (i == -1) {
            ArrayList arrayList = new ArrayList(A06());
            arrayList.add(new C59512ur(this, 1));
            A0T(arrayList);
            this.A0L.A05(Integer.valueOf(this.A0U.A02()), 28, 1);
        } else if (i == 1 || i == 2 || i == 3) {
            A0D();
        } else if (i == 4) {
            this.A0L.A05(Integer.valueOf(this.A0U.A02()), 28, 6);
            this.A0Z.A0A(2);
        }
    }

    public final void A0H(AnonymousClass3M8 r10) {
        this.A0Q.A00(new C48172Ep(r10.A0A, TextUtils.join(",", r10.A0C), r10.A07, System.currentTimeMillis()));
    }

    public final void A0I(AnonymousClass3M8 r20, String str, int i, int i2, int i3) {
        A0H(r20);
        C16430p0 r8 = this.A0L;
        r8.A08(this.A0N.A01(), Long.valueOf((long) this.A02), Long.valueOf((long) this.A01), Long.valueOf((long) i), null, Long.valueOf((long) i2), Long.valueOf((long) i3), Long.valueOf((long) this.A03), str, 56);
        r8.A09(1, this.A0P.A0D, r20.A09, r20.A00, r20.A01, i3, 1);
    }

    public final void A0J(AnonymousClass3M8 r20, String str, int i, int i2, int i3) {
        A0H(r20);
        C16430p0 r8 = this.A0L;
        r8.A08(this.A0N.A01(), Long.valueOf((long) this.A02), Long.valueOf((long) this.A01), Long.valueOf((long) i), null, Long.valueOf((long) i2), Long.valueOf((long) i3), Long.valueOf((long) this.A03), str, 54);
        r8.A09(0, this.A0P.A0D, r20.A09, r20.A00, r20.A01, i3, 1);
    }

    public final void A0K(C48142Em r7, int i) {
        List list = (List) this.A0Q.A00.A01();
        this.A0L.A07(this.A0N.A01(), Long.valueOf((long) list.size()), Long.valueOf(((long) list.indexOf(r7)) + 1), i);
    }

    public void A0L(C48152En r3) {
        this.A0B = true;
        this.A0X.A0A(r3.A00);
        this.A0Z.A0A(3);
        A0E();
        ((C48142Em) r3).A00 = System.currentTimeMillis();
        this.A0Q.A00(r3);
    }

    public final void A0M(C91524Sb r15) {
        C27691It r1;
        int i;
        int i2;
        int i3;
        switch (r15.A01) {
            case 1:
                this.A0A = false;
                if (!A0X()) {
                    A0T(r15.A04);
                    return;
                }
                return;
            case 2:
                C30211Wn r2 = r15.A03;
                this.A05 = r2;
                this.A00 = 0;
                this.A0Z.A0A(0);
                C16430p0 r5 = this.A0L;
                String str = r2.A00;
                C68433Vj r0 = this.A0U;
                int A02 = r0.A02();
                int A01 = r0.A01();
                C28531Ny r12 = new C28531Ny();
                r12.A06 = 63;
                r12.A0Y = str;
                r12.A03 = Integer.valueOf(A02);
                r12.A01 = 0;
                if (A01 == 0) {
                    A01 = 2;
                }
                r12.A07 = Integer.valueOf(A01);
                r5.A03(r12);
                C68553Vv r02 = this.A0S;
                synchronized (C68553Vv.class) {
                    C91524Sb r13 = r02.A05;
                    r13.A03 = null;
                    r13.A01 = 1;
                }
                return;
            case 3:
                C27691It r14 = this.A0Z;
                r14.A0B(9);
                r14.A0A(1);
                A0G(r15.A00);
                return;
            case 4:
                A0G(r15.A00);
                return;
            case 5:
                r1 = this.A0Z;
                i = 11;
                r1.A0A(Integer.valueOf(i));
                return;
            case 6:
                C16430p0 r16 = this.A0L;
                AnonymousClass4N3 r03 = r15.A02;
                if (r03 == null) {
                    i2 = -1;
                } else {
                    i2 = r03.A00;
                }
                long j = (long) i2;
                C68433Vj r04 = this.A0U;
                r16.A0B(null, null, r04.A02(), 4, r04.A01(), 0, j, 3, 0);
                return;
            case 7:
                C16430p0 r4 = this.A0L;
                C68433Vj r05 = this.A0U;
                int A022 = r05.A02();
                int A012 = r05.A01();
                C28531Ny r17 = new C28531Ny();
                r17.A06 = 65;
                r17.A03 = Integer.valueOf(A022);
                r17.A01 = 4;
                if (A012 == 0) {
                    A012 = 2;
                }
                r17.A07 = Integer.valueOf(A012);
                r4.A03(r17);
                r1 = this.A0Z;
                i = 13;
                r1.A0A(Integer.valueOf(i));
                return;
            case 8:
                r1 = this.A0Z;
                i = 12;
                r1.A0A(Integer.valueOf(i));
                return;
            case 9:
            case 10:
                r1 = this.A0Y;
                i = 6;
                r1.A0A(Integer.valueOf(i));
                return;
            case 11:
                C16430p0 r18 = this.A0L;
                AnonymousClass4N3 r06 = r15.A02;
                if (r06 == null) {
                    i3 = -1;
                } else {
                    i3 = r06.A00;
                }
                long j2 = (long) i3;
                C68433Vj r07 = this.A0U;
                r18.A0C(null, null, r07.A02(), 4, r07.A01(), 0, j2, 3, 0);
                return;
            default:
                return;
        }
    }

    public final void A0N(AnonymousClass4RT r8) {
        int i = r8.A01;
        if (i != 1) {
            if (i == 2) {
                AnonymousClass4N7 r0 = r8.A02;
                AnonymousClass009.A05(r0);
                C30211Wn r3 = r0.A01;
                int i2 = r0.A00;
                this.A05 = r3;
                this.A00 = 0;
                this.A0Z.A0A(0);
                C16430p0 r1 = this.A0L;
                long j = (long) i2;
                String str = r3.A00;
                C68433Vj r02 = this.A0U;
                r1.A0A(str, r02.A02(), r02.A01(), j);
                this.A0T.A01.A01 = 1;
            } else if (i == 3) {
                C16430p0 r2 = this.A0L;
                C68433Vj r32 = this.A0U;
                r2.A05(Integer.valueOf(r32.A02()), 28, 3);
                ArrayList arrayList = new ArrayList();
                int i3 = r32.A00.A00;
                if (!(i3 == 4 || i3 == 5)) {
                    arrayList.add(new C59512ur(this, 3));
                }
                A0T(arrayList);
            } else if (i == 4) {
                A0G(r8.A00);
            }
        } else if (!A0X()) {
            List list = r8.A03;
            A0T(list);
            C16430p0 r4 = this.A0L;
            long size = (long) list.size();
            C68433Vj r03 = this.A0U;
            r4.A02(r03.A02(), size, r03.A01());
        }
    }

    public void A0O(C30211Wn r11, int i) {
        String str;
        Boolean bool;
        String str2;
        String str3;
        if (r11 == null) {
            str = null;
        } else {
            str = r11.A00;
        }
        C16430p0 r8 = this.A0L;
        AnonymousClass2K2 r1 = this.A0R;
        if (r1.A07.AJK()) {
            bool = Boolean.valueOf(r1.A03);
        } else {
            bool = null;
        }
        if (r1.A04) {
            str2 = "has_catalog";
        } else {
            str2 = null;
        }
        if (r1.A05) {
            str3 = "open_now";
        } else {
            str3 = null;
        }
        Integer A01 = this.A0N.A01();
        String A05 = r1.A05();
        C28531Ny r2 = new C28531Ny();
        r2.A06 = 63;
        r2.A03 = A01;
        r2.A0T = str2;
        r2.A0Y = str;
        r2.A00 = bool;
        r2.A0U = str3;
        r2.A08 = Integer.valueOf(i);
        r2.A0V = A05;
        r2.A06 = 63;
        r8.A04(r2);
    }

    public void A0P(String str) {
        synchronized (this.A0a) {
            A0Q(str);
        }
    }

    public final void A0Q(String str) {
        String str2;
        String trim = str.trim();
        C68563Vw r8 = this.A0P;
        r8.A06(trim);
        AnonymousClass07E r7 = this.A0H;
        Map map = r7.A02;
        String str3 = (String) map.get("saved_search_query");
        int i = 0;
        if (!TextUtils.isEmpty(str3)) {
            C63363Bh r0 = (C63363Bh) r8.A04.A01();
            if (r0 != null) {
                str2 = r0.A06;
            } else {
                str2 = null;
            }
            if (str3.equals(str2) && map.get("saved_search_state") != null) {
                i = ((Number) map.get("saved_search_state")).intValue();
            }
        }
        this.A04 = i;
        r7.A04("saved_search_state", null);
        r7.A04("saved_search_query", null);
        LinkedList linkedList = this.A0a;
        linkedList.add(trim);
        if (!TextUtils.isEmpty(trim)) {
            if (!this.A09) {
                AnonymousClass1B2 r2 = this.A0K;
                Random random = r2.A01;
                if (random == null) {
                    random = new Random();
                    r2.A01 = random;
                }
                r2.A00 = Long.toHexString(random.nextLong());
                C16430p0 r5 = this.A0L;
                Integer A01 = this.A0N.A01();
                C28531Ny r22 = new C28531Ny();
                r22.A06 = 41;
                r22.A0I = 1L;
                r22.A03 = A01;
                r5.A03(r22);
                this.A09 = true;
            }
            if (this.A04 == 1) {
                A0R(trim);
                return;
            }
            this.A0C.postDelayed(this.A06, 500);
            Runnable runnable = this.A07;
            if (runnable != null) {
                this.A0D.removeCallbacks(runnable);
            }
            RunnableBRunnable0Shape0S1100000_I0 runnableBRunnable0Shape0S1100000_I0 = new RunnableBRunnable0Shape0S1100000_I0(15, trim, this);
            this.A07 = runnableBRunnable0Shape0S1100000_I0;
            this.A0D.postDelayed(runnableBRunnable0Shape0S1100000_I0, 500);
            return;
        }
        synchronized (linkedList) {
            linkedList.clear();
            this.A0C.removeCallbacks(this.A06);
            A0B();
        }
    }

    public final void A0R(String str) {
        String str2;
        AnonymousClass2K4 r9;
        int i;
        LinkedList linkedList = this.A0a;
        synchronized (linkedList) {
            if (!TextUtils.isEmpty(str)) {
                C68563Vw r2 = this.A0P;
                AnonymousClass016 r3 = r2.A04;
                C63363Bh r1 = (C63363Bh) r3.A01();
                if (r1 != null) {
                    str2 = r1.A06;
                } else {
                    str2 = null;
                }
                r2.A06(str2);
                this.A04 = 1;
                this.A0Z.A0A(3);
                linkedList.clear();
                A0C();
                AnonymousClass2K0 r8 = this.A0M;
                C48122Ek A04 = A04();
                boolean A042 = this.A0J.A04();
                if (A0V()) {
                    C63363Bh r12 = (C63363Bh) r3.A01();
                    if (r12 != null) {
                        r9 = r12.A03;
                    } else {
                        r9 = new AnonymousClass2K4(150, null);
                    }
                } else {
                    r9 = null;
                }
                AnonymousClass2K2 r4 = this.A0R;
                List A06 = r4.A06(true);
                if (r4.A05) {
                    i = 1;
                } else {
                    i = null;
                }
                r8.A02(r9, new AnonymousClass2K3(i, A06, r4.A04), A04, str, A042, true);
                this.A0L.A08(this.A0N.A01(), Long.valueOf((long) this.A02), Long.valueOf((long) this.A01), null, null, null, null, Long.valueOf((long) this.A03), str, 55);
            }
        }
    }

    public final void A0S(List list) {
        List list2 = (List) this.A0Q.A00.A01();
        if (list2 != null && !list2.isEmpty() && A0U()) {
            list.add(new C59482uo(this, list2));
        }
    }

    public final void A0T(List list) {
        List A07 = A07();
        A0S(A07);
        A07.addAll(list);
        this.A0G.A0A(A07);
    }

    public boolean A0U() {
        C48112Ej r1 = this.A0U.A00;
        C48122Ek r0 = r1.A01;
        int i = r1.A00;
        if (r0 != null) {
            return i == 2 || i == 5 || i == 0;
        }
        return false;
    }

    public boolean A0V() {
        String str;
        C48122Ek A04 = A04();
        if (A04 == null || (str = A04.A07) == null) {
            return false;
        }
        if (str.equals("device") || str.equals("pin_on_map")) {
            return true;
        }
        return false;
    }

    public boolean A0W() {
        C14850m9 r1 = this.A0J.A00;
        return r1.A07(450) && r1.A07(1704);
    }

    public final boolean A0X() {
        C63363Bh r0 = (C63363Bh) this.A0P.A04.A01();
        return r0 != null && !TextUtils.isEmpty(r0.A06);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        if (r4 == 4) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0Y() {
        /*
            r5 = this;
            X.3Vj r0 = r5.A0U
            X.2Ej r0 = r0.A00
            int r4 = r0.A00
            r3 = 4
            r2 = 0
            r0 = 1
            if (r4 == r0) goto L_0x0017
            r0 = 3
            if (r4 == r0) goto L_0x0017
            r0 = 6
            if (r4 == r0) goto L_0x0017
            r0 = 7
            if (r4 == r0) goto L_0x0017
            r1 = 0
            if (r4 != r3) goto L_0x0018
        L_0x0017:
            r1 = 1
        L_0x0018:
            X.3Vv r0 = r5.A0S
            X.016 r0 = r0.A01
            java.lang.Object r0 = r0.A01()
            X.4Sb r0 = (X.C91524Sb) r0
            if (r0 == 0) goto L_0x002a
            int r0 = r0.A01
            if (r0 != r3) goto L_0x002a
        L_0x0028:
            r2 = 1
        L_0x0029:
            return r2
        L_0x002a:
            boolean r0 = r5.A0X()
            if (r0 != 0) goto L_0x0028
            if (r1 != 0) goto L_0x0028
            X.18d r0 = r5.A0J
            boolean r0 = r0.A01()
            if (r0 != 0) goto L_0x0029
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel.A0Y():boolean");
    }

    @Override // X.AnonymousClass2Jw
    public void ANn() {
        C90954Pw A04 = this.A0R.A04();
        if (A04 != null) {
            this.A0W.A0A(A04);
        }
    }

    @Override // X.AbstractC49242Jy
    public void ANp() {
        this.A0U.A03();
        C27691It r2 = this.A0Y;
        int i = 7;
        if (this.A04 == 0) {
            i = 6;
        }
        r2.A0B(Integer.valueOf(i));
    }

    @Override // X.AbstractC49232Jx
    public void ANy(int i) {
        C27691It r1;
        int i2;
        if (i == 0 || i == 7 || i == 6) {
            this.A0L.A05(Integer.valueOf(this.A0U.A02()), 29, 0);
            r1 = this.A0Y;
            i2 = 8;
        } else if (i == 3) {
            this.A0L.A05(Integer.valueOf(this.A0U.A02()), 29, 3);
            r1 = this.A0Y;
            i2 = 5;
        } else {
            return;
        }
        r1.A0A(Integer.valueOf(i2));
    }

    @Override // X.AnonymousClass2Jw
    public void ANz() {
        this.A0R.A07();
        A0R(A05());
        A0O(null, 1);
    }

    @Override // X.AnonymousClass2Jw
    public void APK(boolean z) {
        this.A0R.A03 = z;
        A0R(A05());
        A0O(null, 1);
    }

    @Override // X.AnonymousClass2Jw
    public void AQY() {
        C90954Pw A04 = this.A0R.A04();
        if (A04 != null) {
            this.A0W.A0A(A04);
        }
        A0F(60);
    }

    @Override // X.AnonymousClass2Jw
    public void ARD(boolean z) {
        this.A0R.A04 = z;
        A0R(A05());
        A0O(null, 1);
    }

    @Override // X.AbstractC49252Jz
    public void ARK(int i) {
        A0D();
    }

    @Override // X.AbstractC49232Jx
    public void ASG() {
        this.A0Z.A0A(6);
        this.A0L.A05(Integer.valueOf(this.A0U.A02()), 34, 0);
    }

    @Override // X.AnonymousClass2Jw
    public void ASq() {
        C90954Pw A04 = this.A0R.A04();
        if (A04 != null) {
            this.A0W.A0A(A04);
        }
        A0F(61);
    }

    @Override // X.AnonymousClass2Jw
    public void ATE(boolean z) {
        this.A0R.A05 = z;
        A0R(A05());
        A0O(null, 1);
    }

    @Override // X.AbstractC49232Jx
    public void AVS() {
        C68433Vj r2 = this.A0U;
        r2.A06();
        this.A0Z.A0A(5);
        this.A0L.A05(Integer.valueOf(r2.A02()), 31, 0);
    }

    @Override // X.AbstractC49232Jx
    public void AVT() {
        A0B();
    }

    @Override // X.AbstractC49232Jx
    public void AVi() {
        A0A();
        this.A0Z.A0A(10);
    }

    @Override // X.AnonymousClass2Jw
    public void AVl(C30211Wn r3) {
        String str;
        this.A0R.A00 = null;
        C63363Bh r0 = (C63363Bh) this.A0P.A04.A01();
        if (r0 != null) {
            str = r0.A06;
        } else {
            str = null;
        }
        A0R(str);
    }

    @Override // X.AnonymousClass2Jw
    public void AY1(C30211Wn r2) {
        this.A0R.A00 = r2;
        A0R(A05());
        A0O(r2, 1);
    }
}
