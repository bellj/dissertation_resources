package com.whatsapp.businessdirectory.viewmodel;

import X.AnonymousClass014;
import X.AnonymousClass016;
import X.AnonymousClass02P;
import X.AnonymousClass1B3;
import X.AnonymousClass3E8;
import X.C12980iv;
import X.C13000ix;
import X.C14830m7;
import X.C15550nR;
import X.C16430p0;
import X.C27691It;
import X.C48142Em;
import android.app.Application;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import java.util.List;

/* loaded from: classes2.dex */
public class DirectorySearchHistoryViewModel extends AnonymousClass014 {
    public boolean A00 = true;
    public final AnonymousClass02P A01;
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final C16430p0 A03;
    public final AnonymousClass1B3 A04;
    public final AnonymousClass3E8 A05;
    public final C15550nR A06;
    public final C14830m7 A07;
    public final C27691It A08 = C13000ix.A03();

    public DirectorySearchHistoryViewModel(Application application, C16430p0 r6, AnonymousClass1B3 r7, AnonymousClass3E8 r8, C15550nR r9, C14830m7 r10) {
        super(application);
        AnonymousClass02P r3 = new AnonymousClass02P();
        this.A01 = r3;
        this.A07 = r10;
        this.A06 = r9;
        this.A04 = r7;
        this.A03 = r6;
        this.A05 = r8;
        r3.A0D(r8.A00, new IDxObserverShape3S0100000_1_I1(this, 41));
        this.A03.A07(this.A04.A01(), null, null, 47);
    }

    public static /* synthetic */ void A00(C48142Em r6, DirectorySearchHistoryViewModel directorySearchHistoryViewModel, int i) {
        List A0z = C12980iv.A0z(directorySearchHistoryViewModel.A05.A00);
        directorySearchHistoryViewModel.A03.A07(directorySearchHistoryViewModel.A04.A01(), C12980iv.A0l(A0z.size()), Long.valueOf(((long) A0z.indexOf(r6)) + 1), i);
    }
}
