package com.whatsapp.media.download;

import X.AnonymousClass043;
import X.AnonymousClass0GK;
import X.AnonymousClass0GL;
import X.C006503b;
import X.C14350lI;
import android.content.Context;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;
import java.io.File;

/* loaded from: classes2.dex */
public class ExpressPathGarbageCollectWorker extends Worker {
    public ExpressPathGarbageCollectWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        String A03 = this.A01.A01.A03("file_path");
        if (A03 == null) {
            Log.e("expressPathGarbageCollectWorker/doWork file path is null");
            return new AnonymousClass0GK();
        }
        C14350lI.A0M(new File(A03));
        return new AnonymousClass0GL(C006503b.A01);
    }
}
