package com.whatsapp.media.download.service;

import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AbstractC14640lm;
import X.AbstractC16130oV;
import X.AnonymousClass009;
import X.AnonymousClass01H;
import X.AnonymousClass01N;
import X.AnonymousClass1JK;
import X.AnonymousClass1UY;
import X.C002601e;
import X.C005602s;
import X.C14960mK;
import X.C15550nR;
import X.C15610nY;
import X.C16150oX;
import X.C16590pI;
import X.C18360sK;
import X.C22370yy;
import X.C22630zO;
import X.C35741ib;
import X.ExecutorC27271Gr;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.IBinder;
import com.whatsapp.R;
import com.whatsapp.media.download.service.MediaDownloadService;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public final class MediaDownloadService extends AnonymousClass1JK {
    public C15550nR A00;
    public C15610nY A01;
    public C16590pI A02;
    public C22370yy A03;
    public ExecutorC27271Gr A04;
    public AbstractC14440lR A05;
    public AbstractC14590lg A06;
    public boolean A07 = false;
    public boolean A08;
    public final AnonymousClass01H A09 = new C002601e(null, new AnonymousClass01N() { // from class: X.5EF
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return C12970iu.A0E();
        }
    });

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public MediaDownloadService() {
        super("media-download-service", true);
    }

    public final void A03(String str, String str2, ArrayList arrayList, int i) {
        AbstractC16130oV r3;
        AbstractC14640lm r1;
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "sending_media@1";
        A00.A0I = "progress";
        A00.A05(System.currentTimeMillis());
        A00.A0B(str);
        A00.A0A(str);
        A00.A09(str2);
        boolean z = false;
        if (!(arrayList == null || (r3 = (AbstractC16130oV) arrayList.get(0)) == null || (r1 = r3.A0z.A00) == null)) {
            Intent A0g = new C14960mK().A0g(this, this.A00.A0B(r1));
            C35741ib.A01(A0g, "MediaDownloadService");
            A00.A09 = AnonymousClass1UY.A00(this, 5, A0g, 134217728);
            C16150oX r0 = r3.A02;
            AnonymousClass009.A05(r0);
            int i2 = (int) r0.A0C;
            if (i2 >= 0) {
                if (arrayList.size() > 1) {
                    z = true;
                }
                A00.A03(100, i2, z);
            }
        }
        C18360sK.A01(A00, 17301633);
        A01(i, A00.A01(), 221770003);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        Log.i("media-download-service/onCreate");
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("media-download-service/onDestroy");
        AbstractC14590lg r1 = this.A06;
        if (r1 != null) {
            this.A03.A0D.A02(r1);
            this.A06 = null;
        }
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        StringBuilder sb = new StringBuilder("media-download-service/onStartCommand:");
        sb.append(intent);
        sb.append("; startId: ");
        sb.append(i2);
        sb.append(" largeMediaDownloadsInProgress=");
        sb.append(this.A08);
        Log.i(sb.toString());
        if (intent != null) {
            if ("com.whatsapp.media.download.service.MediaDownloadService.DOWNLOAD_STARTED".equals(intent.getAction())) {
                this.A08 = true;
            } else if ("com.whatsapp.media.download.service.MediaDownloadService.DOWNLOADS_COMPLETED".equals(intent.getAction())) {
                this.A08 = false;
            }
        }
        A03(getString(R.string.app_name), getResources().getQuantityString(R.plurals.downloading_document, 1), null, i2);
        if (!this.A08) {
            ((AnonymousClass1JK) this).A01.A01(this.A02.A00, MediaDownloadService.class);
            return 2;
        } else if (this.A06 != null) {
            return 2;
        } else {
            this.A06 = new AbstractC14590lg(i2) { // from class: X.3bX
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    boolean z;
                    Resources resources;
                    int i3;
                    String quantityString;
                    Resources resources2;
                    int i4;
                    Object[] objArr;
                    int i5;
                    MediaDownloadService mediaDownloadService = MediaDownloadService.this;
                    int i6 = this.A00;
                    ArrayList arrayList = (ArrayList) obj;
                    if (!arrayList.isEmpty()) {
                        if (arrayList.isEmpty()) {
                            quantityString = null;
                        } else {
                            Object obj2 = arrayList.get(0);
                            if (arrayList.isEmpty()) {
                                z = false;
                                break;
                            }
                            byte b = ((AbstractC15340mz) arrayList.get(0)).A0y;
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                if (C12980iv.A0f(it).A0y != b) {
                                    z = false;
                                    break;
                                }
                            }
                            z = true;
                            if (z) {
                                if (obj2 instanceof C16440p1) {
                                    resources = mediaDownloadService.getResources();
                                    i3 = R.plurals.downloading_document;
                                } else if (obj2 instanceof AnonymousClass1X2) {
                                    resources = mediaDownloadService.getResources();
                                    i3 = R.plurals.downloading_video;
                                }
                                int size = arrayList.size();
                                Object[] objArr2 = new Object[1];
                                C12960it.A1P(objArr2, arrayList.size(), 0);
                                quantityString = resources.getQuantityString(i3, size, objArr2);
                            }
                            resources = mediaDownloadService.getResources();
                            i3 = R.plurals.downloading_file;
                            int size = arrayList.size();
                            Object[] objArr2 = new Object[1];
                            C12960it.A1P(objArr2, arrayList.size(), 0);
                            quantityString = resources.getQuantityString(i3, size, objArr2);
                        }
                        String str = null;
                        if (!arrayList.isEmpty()) {
                            AbstractC16130oV r2 = (AbstractC16130oV) arrayList.get(0);
                            if (!(r2 instanceof C16440p1)) {
                                AbstractC14640lm r1 = r2.A0z.A00;
                                if (r1 != null) {
                                    String A02 = AbstractC32741cf.A02(mediaDownloadService.A01.A04(mediaDownloadService.A00.A0B(r1)));
                                    if (arrayList.size() == 1) {
                                        str = C12960it.A0X(mediaDownloadService, A02, new Object[1], 0, R.string.notification_downloading_single_media_from);
                                    } else {
                                        resources2 = mediaDownloadService.getResources();
                                        i4 = R.plurals.notification_downloading_multiple_media_from;
                                        i5 = arrayList.size() - 1;
                                        objArr = new Object[2];
                                        objArr[0] = A02;
                                        C12960it.A1P(objArr, arrayList.size() - 1, 1);
                                        str = resources2.getQuantityString(i4, i5, objArr);
                                    }
                                }
                            } else if (arrayList.size() == 1) {
                                str = r2.A16();
                            } else {
                                resources2 = mediaDownloadService.getResources();
                                i4 = R.plurals.notification_downloading_multiple_media_filename;
                                i5 = arrayList.size() - 1;
                                objArr = new Object[2];
                                objArr[0] = r2.A16();
                                C12960it.A1P(objArr, arrayList.size() - 1, 1);
                                str = resources2.getQuantityString(i4, i5, objArr);
                            }
                        }
                        ((Handler) mediaDownloadService.A09.get()).post(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003f: INVOKE  
                              (wrap: android.os.Handler : 0x0038: CHECK_CAST (r0v20 android.os.Handler A[REMOVE]) = (android.os.Handler) (wrap: java.lang.Object : 0x0034: INVOKE  (r0v19 java.lang.Object A[REMOVE]) = 
                              (wrap: X.01H : 0x0032: IGET  (r0v18 X.01H A[REMOVE]) = (r8v0 'mediaDownloadService' com.whatsapp.media.download.service.MediaDownloadService) com.whatsapp.media.download.service.MediaDownloadService.A09 X.01H)
                             type: INTERFACE call: X.01H.get():java.lang.Object))
                              (wrap: X.3lL : 0x003c: CONSTRUCTOR  (r7v0 X.3lL A[REMOVE]) = 
                              (r8v0 'mediaDownloadService' com.whatsapp.media.download.service.MediaDownloadService)
                              (r9v1 'quantityString' java.lang.String)
                              (r10v1 'str' java.lang.String)
                              (r11v1 'arrayList' java.util.ArrayList)
                              (r12v0 'i6' int)
                             call: X.3lL.<init>(com.whatsapp.media.download.service.MediaDownloadService, java.lang.String, java.lang.String, java.util.ArrayList, int):void type: CONSTRUCTOR)
                             type: VIRTUAL call: android.os.Handler.post(java.lang.Runnable):boolean in method: X.3bX.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003c: CONSTRUCTOR  (r7v0 X.3lL A[REMOVE]) = 
                              (r8v0 'mediaDownloadService' com.whatsapp.media.download.service.MediaDownloadService)
                              (r9v1 'quantityString' java.lang.String)
                              (r10v1 'str' java.lang.String)
                              (r11v1 'arrayList' java.util.ArrayList)
                              (r12v0 'i6' int)
                             call: X.3lL.<init>(com.whatsapp.media.download.service.MediaDownloadService, java.lang.String, java.lang.String, java.util.ArrayList, int):void type: CONSTRUCTOR in method: X.3bX.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 23 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3lL, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 29 more
                            */
                        /*
                        // Method dump skipped, instructions count: 266
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C70593bX.accept(java.lang.Object):void");
                    }
                };
                ExecutorC27271Gr r2 = this.A04;
                if (r2 == null) {
                    r2 = new ExecutorC27271Gr(this.A05, false);
                    this.A04 = r2;
                }
                C22370yy r0 = this.A03;
                r0.A0D.A03(this.A06, r2);
                return 2;
            }
        }
    }
