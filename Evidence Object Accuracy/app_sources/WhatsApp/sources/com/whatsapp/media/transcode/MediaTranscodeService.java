package com.whatsapp.media.transcode;

import X.AbstractC18860tB;
import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass109;
import X.AnonymousClass12H;
import X.AnonymousClass1m7;
import X.AnonymousClass5B7;
import X.C005602s;
import X.C15550nR;
import X.C15610nY;
import X.C18360sK;
import X.C22630zO;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class MediaTranscodeService extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public static final HashMap A0B = new HashMap();
    public int A00;
    public C15550nR A01;
    public C15610nY A02;
    public AbstractC18860tB A03;
    public AnonymousClass12H A04;
    public AnonymousClass109 A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public final Object A09;
    public volatile C71083cM A0A;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public MediaTranscodeService() {
        this(0);
        this.A08 = false;
        this.A00 = -1;
    }

    public MediaTranscodeService(int i) {
        this.A09 = new Object();
        this.A07 = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c3, code lost:
        if (r1 != 13) goto L_0x00c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0181, code lost:
        if (r0 != 13) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01f8, code lost:
        if (r0 != 13) goto L_0x01fa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00() {
        /*
        // Method dump skipped, instructions count: 549
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.media.transcode.MediaTranscodeService.A00():void");
    }

    public final void A01(C005602s r3, String str, int i, boolean z) {
        r3.A0I = "progress";
        r3.A05(System.currentTimeMillis());
        r3.A0A(getString(R.string.app_name));
        r3.A09(str);
        if (i >= 0) {
            boolean z2 = false;
            if (i == 0) {
                z2 = true;
            }
            r3.A03(100, i, z2);
        }
        if (!z) {
            r3.A0B(str);
        }
        C18360sK.A01(r3, 17301640);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A0A == null) {
            synchronized (this.A09) {
                if (this.A0A == null) {
                    this.A0A = new C71083cM(this);
                }
            }
        }
        return this.A0A.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        Log.i("MediaTranscodeService/onCreate");
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A01 = (C15550nR) r1.A45.get();
            this.A02 = (C15610nY) r1.AMe.get();
            this.A04 = (AnonymousClass12H) r1.AC5.get();
            this.A05 = (AnonymousClass109) r1.AI8.get();
        }
        super.onCreate();
        AnonymousClass1m7 r12 = new AnonymousClass1m7(this);
        this.A03 = r12;
        this.A04.A03(r12);
    }

    @Override // android.app.Service
    public void onDestroy() {
        A0B.size();
        this.A08 = false;
        stopForeground(true);
        this.A04.A04(this.A03);
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null || !"com.whatsapp.media.transcode.MediaTranscodeService.STOP".equals(intent.getAction())) {
            A00();
            return 2;
        }
        if (Build.VERSION.SDK_INT >= 26) {
            C005602s A00 = C22630zO.A00(this);
            A00.A0J = "sending_media@1";
            A00.A0A(getString(R.string.app_name));
            A00.A09(getString(R.string.sending_message));
            A00.A03 = -1;
            C18360sK.A01(A00, 17301640);
            startForeground(3, A00.A01());
        }
        this.A08 = false;
        boolean stopSelfResult = stopSelfResult(i2);
        StringBuilder sb = new StringBuilder("MediaTranscodeService/stopService success:");
        sb.append(stopSelfResult);
        Log.i(sb.toString());
        return 2;
    }
}
