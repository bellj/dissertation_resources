package com.whatsapp.push;

import X.AbstractC019809h;
import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C251318f;
import X.C251618i;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class WAFbnsPreloadReceiver extends AbstractC019809h {
    public C251318f A00;
    public C251618i A01;
    public final Object A02;
    public volatile boolean A03;

    public WAFbnsPreloadReceiver() {
        this(0);
    }

    public WAFbnsPreloadReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // X.AbstractC019809h
    public void A00(Context context, String str) {
        C251618i r4 = this.A01;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = C13000ix.A05(str).getJSONObject("params");
                String optString = jSONObject.optString("id", null);
                String optString2 = jSONObject.optString("ip", null);
                String optString3 = jSONObject.optString("cl_sess", null);
                String optString4 = jSONObject.optString("mmsov", null);
                String optString5 = jSONObject.optString("fbips", null);
                String optString6 = jSONObject.optString("er_ri", null);
                boolean equals = "1".equals(jSONObject.optString("notify", null));
                String optString7 = jSONObject.optString("push_id", null);
                String optString8 = jSONObject.optString("push_event_id", null);
                String optString9 = jSONObject.optString("push_ts", null);
                String optString10 = jSONObject.optString("pn", null);
                synchronized (r4) {
                    r4.A00(null, null, optString, optString2, optString3, optString4, optString5, optString6, optString7, optString8, optString9, optString10, 1, equals);
                }
            } catch (JSONException e) {
                Log.e(C12960it.A0d(str, C12960it.A0k("WAFbnsPreloadReceiver/handleFbnsPush: invalid payload:")), e);
            }
        }
    }

    @Override // X.AbstractC019809h
    public void A01(Context context, String str) {
        C251318f r3 = this.A00;
        if (!r3.A01()) {
            Log.i("FbnsTokenManager/onTokenRecevied fbns disabled for account");
        } else if (TextUtils.isEmpty(str)) {
            Log.e("FbnsTokenManager/onTokenRecevied tokenFromFb is null");
        } else {
            synchronized (r3) {
                SharedPreferences sharedPreferences = r3.A04.A00;
                String string = sharedPreferences.getString("fbns_token", null);
                if (str.equals(sharedPreferences.getString("last_server_fbns_token", null))) {
                    Log.w("FbnsTokenManager/onTokenReceived called with token that is already on the server side");
                } else {
                    r3.A05.A0J(str, "fbns", null);
                    C12970iu.A1D(sharedPreferences.edit(), "last_server_fbns_token", str);
                }
                if (str.equals(string)) {
                    Log.w("FbnsTokenManager/onTokenRecevied token already saved");
                } else {
                    C12970iu.A1B(sharedPreferences.edit().putString("fbns_token", str), "fbns_app_vers", 221770000);
                }
            }
        }
    }

    @Override // X.AbstractC019809h, android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = (C251618i) r1.A9Q.get();
                    this.A00 = (C251318f) r1.A7m.get();
                    this.A03 = true;
                }
            }
        }
        super.onReceive(context, intent);
    }
}
