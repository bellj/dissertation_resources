package com.whatsapp.push;

import X.AbstractC14640lm;
import X.AbstractC15590nW;
import X.AbstractC15710nm;
import X.AbstractServiceC003701q;
import X.AbstractServiceC003801r;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass5B7;
import X.C14820m6;
import X.C14850m9;
import X.C15450nH;
import X.C15600nX;
import X.C15860o1;
import X.C19800uh;
import X.C20660w7;
import X.C33281dk;
import X.C58182oH;
import X.C71083cM;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class RegistrationIntentService extends AbstractServiceC003701q implements AnonymousClass004 {
    public AbstractC15710nm A00;
    public C15450nH A01;
    public AnonymousClass01d A02;
    public C14820m6 A03;
    public C15600nX A04;
    public C14850m9 A05;
    public C20660w7 A06;
    public C19800uh A07;
    public C15860o1 A08;
    public boolean A09;
    public final Object A0A;
    public volatile C71083cM A0B;

    public RegistrationIntentService() {
        this(0);
    }

    public RegistrationIntentService(int i) {
        this.A0A = new Object();
        this.A09 = false;
    }

    public static void A01(Context context) {
        Log.i("GCM: refreshing gcm token");
        AbstractServiceC003801r.A00(context, new Intent("com.whatsapp.action.REFRESH", null, context, RegistrationIntentService.class), RegistrationIntentService.class, 4);
    }

    public static void A02(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder("GCM: verifying registration; serverRegistrationId=");
        sb.append(str);
        Log.i(sb.toString());
        Intent intent = new Intent("com.whatsapp.action.VERIFY", null, context, RegistrationIntentService.class);
        if (!TextUtils.isEmpty(str)) {
            intent.putExtra("registrationId", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            intent.putExtra("mutedChatsHash", str2);
        }
        AbstractServiceC003801r.A00(context, intent, RegistrationIntentService.class, 4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0269, code lost:
        if (r5.isEmpty() != false) goto L_0x027b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0273, code lost:
        if (r21.A05.A07(283) == false) goto L_0x027b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0275, code lost:
        r21.A06.A0J(r4, "gcm", r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x027b, code lost:
        r21.A06.A0J(r4, "gcm", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0281, code lost:
        if (r16 == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0283, code lost:
        if (r18 == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0285, code lost:
        if (r11 != false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0292, code lost:
        if (r21.A03.A00.getBoolean("saved_gcm_token_server_unreg", false) != false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0294, code lost:
        com.whatsapp.util.Log.i("GCM: verifying tokenUnregisteredOnServer fetched saved token");
        r21.A00.AaV("gcm-retrieved-saved-token", null, false);
        r21.A03.A00.edit().putBoolean("saved_gcm_token_server_unreg", true).apply();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02b9, code lost:
        throw new java.lang.AssertionError("GCM: empty token returned but no exception thrown");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02ba, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02bf, code lost:
        if (r3.getMessage() == null) goto L_0x02e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02d9, code lost:
        com.whatsapp.util.Log.e("GCM: security exception caught; switching to long-connect", r3);
        r21.A03.A0G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02e5, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02e6, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02eb, code lost:
        if (r3.getMessage() != null) goto L_0x02ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02ed, code lost:
        r1 = r3.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02f6, code lost:
        switch(r1.hashCode()) {
            case -1992442893: goto L_0x02ff;
            case -1800638118: goto L_0x0308;
            case -1579216525: goto L_0x0311;
            case -1515255836: goto L_0x031b;
            case -829011630: goto L_0x0324;
            case -630263762: goto L_0x032d;
            case -595928767: goto L_0x0336;
            case 370784008: goto L_0x033f;
            case 855732677: goto L_0x0373;
            case 901541140: goto L_0x0399;
            case 1750400351: goto L_0x03cb;
            default: goto L_0x02f9;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02fe, code lost:
        throw new java.lang.AssertionError(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0305, code lost:
        if (r1.equals("SERVICE_NOT_AVAILABLE") != false) goto L_0x0307;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x030e, code lost:
        if (r1.equals("QUOTA_EXCEEDED") != false) goto L_0x0310;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0317, code lost:
        if (r1.equals("PHONE_REGISTRATION_ERROR") != false) goto L_0x0319;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0321, code lost:
        if (r1.equals("AUTHENTICATION_FAILED") != false) goto L_0x0323;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x032a, code lost:
        if (r1.equals("INVALID_PARAMETERS") != false) goto L_0x032c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0333, code lost:
        if (r1.equals("INTERNAL_SERVER_ERROR") != false) goto L_0x0335;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x033c, code lost:
        if (r1.equals("TIMEOUT") != false) goto L_0x033e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0078, code lost:
        if (android.text.TextUtils.isEmpty(r3) == false) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0345, code lost:
        if (r1.equals("BACKOFF") != false) goto L_0x0347;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0347, code lost:
        r4 = java.lang.Math.min(r22.getLongExtra("delay_ms", 15000), 86400000L);
        r22.putExtra("delay_ms", 2 * r4);
        r3 = r21.A02.A04();
        r2 = X.AnonymousClass1UY.A03(r21, r22, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0365, code lost:
        if (r3 == null) goto L_0x03de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0367, code lost:
        r3.cancel(r2);
        r3.set(3, android.os.SystemClock.elapsedRealtime() + r4, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0379, code lost:
        if (r1.equals("TOO_MANY_REGISTRATIONS") != false) goto L_0x037b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x037b, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("GCM: attempted to register for GCM but registration count was exceeded already; exceptionMessage=");
        r1.append(r3.getMessage());
        com.whatsapp.util.Log.e(r1.toString());
        r21.A03.A0G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x039f, code lost:
        if (r1.equals("ACCOUNT_MISSING") != false) goto L_0x03a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x03a1, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("GCM: attempted to register for GCM but received undocumented error; exceptionMessage=");
        r1.append(r3.getMessage());
        r1.append("; playServicesAvailable=");
        r1.append(X.AnonymousClass1UB.A00(r21));
        com.whatsapp.util.Log.e(r1.toString());
        r21.A03.A0G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03d1, code lost:
        if (r1.equals("MISSING_INSTANCEID_SERVICE") != false) goto L_0x03d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x03d3, code lost:
        com.whatsapp.util.Log.e("GCM: attempted to register for GCM but Google Play Services was missing");
        r21.A03.A0G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03de, code lost:
        com.whatsapp.util.Log.w("RegistrationIntentService/onHandleWork AlarmManager is null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03e6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03ec, code lost:
        throw new java.lang.AssertionError(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007c, code lost:
        if (r20 != false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007e, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0083, code lost:
        if (android.text.TextUtils.equals(r3, r11) == false) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0086, code lost:
        r17 = android.text.TextUtils.isEmpty(r11);
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008b, code lost:
        if (221770000 == r13) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008d, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        if (r18 != false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0090, code lost:
        if (r17 != false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0092, code lost:
        if (r6 != false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0094, code lost:
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0096, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0098, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009a, code lost:
        if (r18 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009c, code lost:
        if (r17 != false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a8, code lost:
        if (r21.A03.A00.getBoolean("saved_gcm_token_server_unreg", false) == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b2, code lost:
        if (r21.A01.A05(X.AbstractC15460nI.A0O) == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b4, code lost:
        r4 = new java.lang.StringBuilder();
        r4.append("GCM: verifying tokenUnregisteredOnServer deleting savedToken:");
        r4.append(r11);
        com.whatsapp.util.Log.i(r4.toString());
        r14 = r21.A07.A00;
        r13 = com.google.firebase.iid.FirebaseInstanceId.getInstance(X.C13030j1.A00());
        r4 = "FCM";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e4, code lost:
        if (r4.isEmpty() != false) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ec, code lost:
        if (r4.equalsIgnoreCase("fcm") != false) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f4, code lost:
        if (r4.equalsIgnoreCase("gcm") == false) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f6, code lost:
        r4 = "*";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f8, code lost:
        r13.A04(r13.A06.A04(com.google.firebase.iid.FirebaseInstanceId.A01(), r14, r4));
        r13 = com.google.firebase.iid.FirebaseInstanceId.A08;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0107, code lost:
        monitor-enter(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0108, code lost:
        r4 = X.C13450jk.A01(r14, r4);
        r0 = r13.A01.edit();
        r0.remove(r4);
        r0.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0123, code lost:
        throw new java.io.IOException("MAIN_THREAD");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0124, code lost:
        monitor-exit(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0125, code lost:
        r21.A03.A00.edit().putBoolean("saved_gcm_token_server_unreg", false).apply();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0135, code lost:
        r4 = com.google.firebase.iid.FirebaseInstanceId.getInstance(X.C13030j1.A00()).A05(r21.A07.A00, "FCM");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x014b, code lost:
        if (android.text.TextUtils.isEmpty(r4) != false) goto L_0x02b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x014d, code lost:
        r6 = new java.lang.StringBuilder();
        r6.append("GCM: token retrieved successfully; token=");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0157, code lost:
        if (r4 == null) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x015a, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x015c, code lost:
        r0 = r4.length();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0160, code lost:
        r6.append(r0);
        r6.append(" bytes; applicationVersion=");
        r6.append(221770000);
        com.whatsapp.util.Log.i(r6.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0172, code lost:
        if (r17 == false) goto L_0x017f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0174, code lost:
        com.whatsapp.util.Log.i("GCM: no previously saved token");
        r21.A03.A0W(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x017f, code lost:
        r11 = !android.text.TextUtils.equals(r4, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0185, code lost:
        if (r16 != false) goto L_0x0239;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0187, code lost:
        if (r11 != false) goto L_0x0239;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0189, code lost:
        if (r20 == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x018b, code lost:
        r5 = r22.getStringExtra("mutedChatsHash");
        r3 = A06(r21.A08.A0B());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x019f, code lost:
        if (r3.isEmpty() != false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a9, code lost:
        if (r21.A05.A07(283) == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01ab, code lost:
        r11 = !android.text.TextUtils.isEmpty(r5);
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01b6, code lost:
        if (r3.size() <= 0) goto L_0x01b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01b8, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01b9, code lost:
        if (r11 == false) goto L_0x0229;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01bb, code lost:
        if (r0 == false) goto L_0x022b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01bd, code lost:
        r15 = java.security.MessageDigest.getInstance("SHA-256");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01c3, code lost:
        r14 = X.AnonymousClass01V.A0A;
        r18 = r3.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01cd, code lost:
        if (r18.hasNext() == false) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01cf, code lost:
        r13 = (X.C33281dk) r18.next();
        r15.update(r13.A01.getRawString().getBytes(r14));
        r0 = r13.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01e4, code lost:
        if (r0 != null) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01e6, code lost:
        r0 = r13.A02.longValue();
        r16 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01f0, code lost:
        if (r0 == -1) goto L_0x01f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01f2, code lost:
        r16 = java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01f8, code lost:
        r0 = java.lang.Long.valueOf(r16);
        r13.A00 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01fe, code lost:
        r15.update(r0.toString().getBytes(r14));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x020a, code lost:
        r1 = new byte[6];
        java.lang.System.arraycopy(r15.digest(), 0, r1, 0, 6);
        r0 = android.util.Base64.encodeToString(r1, 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0219, code lost:
        if (r0 == null) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x021b, code lost:
        r0 = !r0.equalsIgnoreCase(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0229, code lost:
        if (r0 == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x022b, code lost:
        com.whatsapp.util.Log.i("GCM: sending client config with new muted chats");
        r21.A06.A0J(r4, "gcm", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0239, code lost:
        r21.A03.A00.edit().putString("c2dm_reg_id", r4).putInt("c2dm_app_vers", 221770000).apply();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x024c, code lost:
        if (r19 != false) goto L_0x0254;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0252, code lost:
        if (android.text.TextUtils.equals(r4, r3) != false) goto L_0x0281;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0254, code lost:
        com.whatsapp.util.Log.i("GCM: sending client config with new token");
        r5 = A06(r21.A08.A0B());
     */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0367 A[Catch: all -> 0x03ed, TryCatch #4 {all -> 0x03ed, blocks: (B:11:0x0051, B:13:0x0072, B:17:0x007e, B:20:0x0086, B:31:0x009e, B:33:0x00aa, B:35:0x00b4, B:37:0x00e0, B:39:0x00e6, B:41:0x00ee, B:44:0x00f8, B:45:0x0107, B:48:0x011a, B:49:0x011b, B:50:0x011c, B:51:0x0123, B:52:0x0124, B:53:0x0125, B:54:0x0135, B:56:0x014d, B:60:0x015c, B:61:0x0160, B:63:0x0174, B:64:0x017f, B:68:0x018b, B:70:0x01a1, B:72:0x01ab, B:77:0x01bd, B:78:0x01c3, B:79:0x01c9, B:81:0x01cf, B:83:0x01e6, B:85:0x01f2, B:86:0x01f8, B:87:0x01fe, B:88:0x020a, B:90:0x021b, B:91:0x0221, B:92:0x0228, B:94:0x022b, B:95:0x0239, B:97:0x024e, B:99:0x0254, B:101:0x026b, B:103:0x0275, B:104:0x027b, B:108:0x0287, B:110:0x0294, B:111:0x02b2, B:112:0x02b9, B:114:0x02bb, B:116:0x02c1, B:118:0x02cd, B:120:0x02d9, B:121:0x02e5, B:123:0x02e7, B:125:0x02ed, B:126:0x02f6, B:127:0x02f9, B:128:0x02fe, B:129:0x02ff, B:132:0x0308, B:135:0x0311, B:138:0x031b, B:141:0x0324, B:144:0x032d, B:147:0x0336, B:150:0x033f, B:152:0x0347, B:154:0x0367, B:155:0x0373, B:157:0x037b, B:158:0x0399, B:160:0x03a1, B:161:0x03cb, B:163:0x03d3, B:164:0x03de, B:167:0x03e7, B:168:0x03ec), top: B:177:0x0051, inners: #5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x03de A[Catch: all -> 0x03ed, TRY_LEAVE, TryCatch #4 {all -> 0x03ed, blocks: (B:11:0x0051, B:13:0x0072, B:17:0x007e, B:20:0x0086, B:31:0x009e, B:33:0x00aa, B:35:0x00b4, B:37:0x00e0, B:39:0x00e6, B:41:0x00ee, B:44:0x00f8, B:45:0x0107, B:48:0x011a, B:49:0x011b, B:50:0x011c, B:51:0x0123, B:52:0x0124, B:53:0x0125, B:54:0x0135, B:56:0x014d, B:60:0x015c, B:61:0x0160, B:63:0x0174, B:64:0x017f, B:68:0x018b, B:70:0x01a1, B:72:0x01ab, B:77:0x01bd, B:78:0x01c3, B:79:0x01c9, B:81:0x01cf, B:83:0x01e6, B:85:0x01f2, B:86:0x01f8, B:87:0x01fe, B:88:0x020a, B:90:0x021b, B:91:0x0221, B:92:0x0228, B:94:0x022b, B:95:0x0239, B:97:0x024e, B:99:0x0254, B:101:0x026b, B:103:0x0275, B:104:0x027b, B:108:0x0287, B:110:0x0294, B:111:0x02b2, B:112:0x02b9, B:114:0x02bb, B:116:0x02c1, B:118:0x02cd, B:120:0x02d9, B:121:0x02e5, B:123:0x02e7, B:125:0x02ed, B:126:0x02f6, B:127:0x02f9, B:128:0x02fe, B:129:0x02ff, B:132:0x0308, B:135:0x0311, B:138:0x031b, B:141:0x0324, B:144:0x032d, B:147:0x0336, B:150:0x033f, B:152:0x0347, B:154:0x0367, B:155:0x0373, B:157:0x037b, B:158:0x0399, B:160:0x03a1, B:161:0x03cb, B:163:0x03d3, B:164:0x03de, B:167:0x03e7, B:168:0x03ec), top: B:177:0x0051, inners: #5 }] */
    @Override // X.AbstractServiceC003801r
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.content.Intent r22) {
        /*
        // Method dump skipped, instructions count: 1056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.push.RegistrationIntentService.A05(android.content.Intent):void");
    }

    public final List A06(List list) {
        ArrayList arrayList = new ArrayList();
        int A02 = this.A05.A02(1657);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C33281dk r2 = (C33281dk) it.next();
            AbstractC14640lm r1 = r2.A01;
            if ((r1 instanceof AbstractC15590nW) && this.A04.A00((AbstractC15590nW) r1) >= A02) {
                arrayList.add(r2);
            }
        }
        return arrayList;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A0B == null) {
            synchronized (this.A0A) {
                if (this.A0B == null) {
                    this.A0B = new C71083cM(this);
                }
            }
        }
        return this.A0B.generatedComponent();
    }

    @Override // X.AbstractServiceC003801r, android.app.Service
    public void onCreate() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A05 = (C14850m9) r1.A04.get();
            this.A00 = (AbstractC15710nm) r1.A4o.get();
            this.A06 = (C20660w7) r1.AIB.get();
            this.A01 = (C15450nH) r1.AII.get();
            this.A07 = (C19800uh) r1.A8R.get();
            this.A02 = (AnonymousClass01d) r1.ALI.get();
            this.A08 = (C15860o1) r1.A3H.get();
            this.A03 = (C14820m6) r1.AN3.get();
            this.A04 = (C15600nX) r1.A8x.get();
        }
        super.onCreate();
    }
}
