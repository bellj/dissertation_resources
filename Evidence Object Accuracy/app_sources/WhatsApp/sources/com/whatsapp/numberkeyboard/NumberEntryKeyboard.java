package com.whatsapp.numberkeyboard;

import X.AbstractC69143Yc;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass1IO;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3BU;
import X.AnonymousClass3NH;
import X.AnonymousClass46I;
import X.AnonymousClass46J;
import X.AnonymousClass4EI;
import X.AnonymousClass4O0;
import X.AnonymousClass5W6;
import X.C016307r;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaImageView;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class NumberEntryKeyboard extends LinearLayout implements AnonymousClass004 {
    public static final int A0J = ((int) Math.floor(20.399999618530273d));
    public int A00;
    public int A01;
    public long A02;
    public Paint A03;
    public RectF A04;
    public View A05;
    public EditText A06;
    public AnonymousClass01d A07;
    public AnonymousClass018 A08;
    public AnonymousClass1IO A09;
    public AnonymousClass5W6 A0A;
    public AnonymousClass2P7 A0B;
    public Map A0C;
    public Map A0D;
    public boolean A0E;
    public boolean A0F;
    public View[][] A0G;
    public AnonymousClass4O0[][] A0H;
    public final View.OnTouchListener A0I;

    public NumberEntryKeyboard(Context context) {
        this(context, null);
    }

    public NumberEntryKeyboard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NumberEntryKeyboard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        this.A0C = new HashMap();
        this.A02 = -1;
        this.A0I = new AnonymousClass3NH(this);
        A02(context, attributeSet);
    }

    public NumberEntryKeyboard(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
        this.A0C = new HashMap();
        this.A02 = -1;
        this.A0I = new AnonymousClass3NH(this);
        A02(context, attributeSet);
    }

    public static AnonymousClass5W6 A00(AnonymousClass018 r1) {
        if (AnonymousClass4EI.A00(r1).equals(".")) {
            return new AnonymousClass46J();
        }
        return new AnonymousClass46I();
    }

    public void A01() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A08 = (AnonymousClass018) r1.ANb.get();
            this.A07 = (AnonymousClass01d) r1.ALI.get();
        }
    }

    public final void A02(Context context, AttributeSet attributeSet) {
        View[] viewArr;
        ViewGroup viewGroup;
        LinearLayout.inflate(getContext(), R.layout.number_entry_keyboard, this);
        ViewGroup viewGroup2 = (ViewGroup) findViewById(R.id.custom_key_container);
        boolean z = false;
        if (attributeSet != null && context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0D, 0, 0).getInteger(0, 0) == 1) {
            AnonymousClass5W6 A00 = A00(this.A08);
            this.A0A = A00;
            WaImageView waImageView = new WaImageView(context);
            waImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            waImageView.setImageResource(((AbstractC69143Yc) A00).A00);
            C016307r.A01(PorterDuff.Mode.SRC_IN, waImageView);
            C016307r.A00(ColorStateList.valueOf(AnonymousClass00T.A00(context, R.color.number_entry_keyboard_text_color)), waImageView);
            viewGroup2.addView(waImageView);
        }
        View[][] viewArr2 = new View[4];
        View[] viewArr3 = new View[3];
        if (!this.A08.A04().A06) {
            viewArr3[0] = findViewById(R.id.one_key);
            viewArr3[1] = findViewById(R.id.two_key);
            viewArr3[2] = findViewById(R.id.three_key);
            viewArr2[0] = viewArr3;
            View[] viewArr4 = new View[3];
            viewArr4[0] = findViewById(R.id.four_key);
            viewArr4[1] = findViewById(R.id.five_key);
            viewArr4[2] = findViewById(R.id.six_key);
            viewArr2[1] = viewArr4;
            View[] viewArr5 = new View[3];
            viewArr5[0] = findViewById(R.id.seven_key);
            viewArr5[1] = findViewById(R.id.eight_key);
            viewArr5[2] = findViewById(R.id.nine_key);
            viewArr2[2] = viewArr5;
            View[] viewArr6 = new View[3];
            viewArr6[0] = viewGroup2;
            viewArr6[1] = findViewById(R.id.zero_key);
            viewGroup = findViewById(R.id.backspace_key);
            viewArr = viewArr6;
        } else {
            viewArr3[0] = findViewById(R.id.three_key);
            viewArr3[1] = findViewById(R.id.two_key);
            viewArr3[2] = findViewById(R.id.one_key);
            viewArr2[0] = viewArr3;
            View[] viewArr7 = new View[3];
            viewArr7[0] = findViewById(R.id.six_key);
            viewArr7[1] = findViewById(R.id.five_key);
            viewArr7[2] = findViewById(R.id.four_key);
            viewArr2[1] = viewArr7;
            View[] viewArr8 = new View[3];
            viewArr8[0] = findViewById(R.id.nine_key);
            viewArr8[1] = findViewById(R.id.eight_key);
            viewArr8[2] = findViewById(R.id.seven_key);
            viewArr2[2] = viewArr8;
            View[] viewArr9 = new View[3];
            viewArr9[0] = findViewById(R.id.backspace_key);
            viewArr9[1] = findViewById(R.id.zero_key);
            viewGroup = viewGroup2;
            viewArr = viewArr9;
        }
        viewArr[2] = viewGroup;
        viewArr2[3] = viewArr;
        this.A0G = viewArr2;
        if (attributeSet != null && context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0D, 0, 0).getInteger(0, 0) == 1) {
            setCustomKey(A00(this.A08));
        }
        int dimensionPixelSize = getContext().getResources().getDimensionPixelSize(R.dimen.number_entry_keyboard_horizontal_padding);
        setPadding(dimensionPixelSize, 0, dimensionPixelSize, 0);
        setOrientation(1);
        setBackgroundColor(AnonymousClass00T.A00(getContext(), R.color.number_entry_keyboard_bg_color));
        for (int i = 0; i < this.A0G.length; i++) {
            int i2 = 0;
            while (true) {
                View[][] viewArr10 = this.A0G;
                if (i2 < viewArr10[i].length) {
                    View view = viewArr10[i][i2];
                    if (view != null && (view instanceof TextView)) {
                        TextView textView = (TextView) view;
                        NumberFormat A0J2 = this.A08.A0J();
                        int id = view.getId();
                        int i3 = 0;
                        if (id != R.id.zero_key) {
                            i3 = 1;
                            if (id != R.id.one_key) {
                                i3 = 2;
                                if (id != R.id.two_key) {
                                    i3 = 3;
                                    if (id != R.id.three_key) {
                                        i3 = 4;
                                        if (id != R.id.four_key) {
                                            i3 = 5;
                                            if (id != R.id.five_key) {
                                                i3 = 6;
                                                if (id != R.id.six_key) {
                                                    i3 = 7;
                                                    if (id != R.id.seven_key) {
                                                        i3 = 9;
                                                        if (id == R.id.eight_key) {
                                                            i3 = 8;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        textView.setText(A0J2.format((long) i3));
                    }
                    i2++;
                }
            }
        }
        if (Settings.System.getFloat(context.getContentResolver(), "window_animation_scale", 1.0f) == 1.0f) {
            z = true;
        }
        this.A0E = z;
        if (z) {
            Paint paint = new Paint(1);
            this.A03 = paint;
            paint.setColor(AnonymousClass00T.A00(context, R.color.number_entry_keyboard_ripple_color));
            this.A03.setStyle(Paint.Style.FILL);
            this.A03.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
            this.A04 = new RectF();
            this.A0D = new HashMap();
            this.A09 = new AnonymousClass1IO(this);
        }
        setOnTouchListener(this.A0I);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.A0E) {
            for (Number number : this.A0D.keySet()) {
                AnonymousClass3BU r6 = (AnonymousClass3BU) this.A0D.get(Long.valueOf(number.longValue()));
                PointF pointF = r6.A04;
                float f = r6.A00;
                float f2 = pointF.x;
                float f3 = f / 2.0f;
                float f4 = pointF.y;
                this.A04.set(f2 - f3, f4 - f3, f2 + f3, f4 + f3);
                this.A03.setAlpha(r6.A01);
                canvas.drawOval(this.A04, this.A03);
            }
        }
    }

    @Override // android.widget.LinearLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        int i5;
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            float width = (float) getWidth();
            float height = (float) getHeight();
            View[][] viewArr = this.A0G;
            int length = viewArr[0].length;
            float f2 = width / ((float) length);
            int length2 = viewArr.length;
            float f3 = height / ((float) length2);
            int floor = ((int) Math.floor((double) f2)) + 12;
            this.A00 = floor;
            this.A01 = (int) (((float) floor) / 2.0f);
            this.A0H = (AnonymousClass4O0[][]) Array.newInstance(AnonymousClass4O0.class, length2, length);
            for (int i6 = 0; i6 < this.A0G.length; i6++) {
                int i7 = 0;
                while (true) {
                    View[][] viewArr2 = this.A0G;
                    int length3 = viewArr2[0].length;
                    if (i7 < length3) {
                        View view = viewArr2[i6][i7];
                        float f4 = ((float) i7) * f2;
                        float f5 = ((float) i6) * f3;
                        float f6 = f4 + f2;
                        float f7 = f5 + f3;
                        if (i7 == 0) {
                            i5 = getPaddingLeft();
                        } else if (i7 == length3 - 1) {
                            i5 = -getPaddingRight();
                        } else {
                            f = 0.0f;
                            AnonymousClass4O0 r1 = new AnonymousClass4O0(new PointF(((f4 + f6) / 2.0f) + f, (f5 + f7) / 2.0f), new RectF(f4, f5, f6, f7));
                            this.A0H[i6][i7] = r1;
                            this.A0C.put(view, r1);
                            i7++;
                        }
                        f = (float) i5;
                        AnonymousClass4O0 r1 = new AnonymousClass4O0(new PointF(((f4 + f6) / 2.0f) + f, (f5 + f7) / 2.0f), new RectF(f4, f5, f6, f7));
                        this.A0H[i6][i7] = r1;
                        this.A0C.put(view, r1);
                        i7++;
                    }
                }
            }
        }
    }

    public void setCustomKey(AnonymousClass5W6 r5) {
        this.A0A = r5;
        View[] viewArr = this.A0G[3];
        char c = 2;
        if (!this.A08.A04().A06) {
            c = 0;
        }
        ViewGroup viewGroup = (ViewGroup) viewArr[c];
        viewGroup.removeAllViews();
        if (r5 != null) {
            Context context = getContext();
            WaImageView waImageView = new WaImageView(context);
            waImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            waImageView.setImageResource(((AbstractC69143Yc) r5).A00);
            C016307r.A01(PorterDuff.Mode.SRC_IN, waImageView);
            C016307r.A00(ColorStateList.valueOf(AnonymousClass00T.A00(context, R.color.number_entry_keyboard_text_color)), waImageView);
            viewGroup.addView(waImageView);
        }
        invalidate();
    }

    public void setEditText(WaEditText waEditText) {
        this.A06 = waEditText;
    }
}
