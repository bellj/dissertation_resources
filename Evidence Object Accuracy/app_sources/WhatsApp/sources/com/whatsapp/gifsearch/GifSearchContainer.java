package com.whatsapp.gifsearch;

import X.AbstractC018308n;
import X.AbstractC05270Ox;
import X.AbstractC253919f;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.AnonymousClass321;
import X.AnonymousClass326;
import X.AnonymousClass327;
import X.AnonymousClass3EY;
import X.AnonymousClass434;
import X.AnonymousClass435;
import X.AnonymousClass47K;
import X.AnonymousClass5RW;
import X.AnonymousClass5UF;
import X.C14820m6;
import X.C14850m9;
import X.C15320mw;
import X.C15330mx;
import X.C16120oU;
import X.C16630pM;
import X.C252718t;
import X.C253719d;
import X.C253819e;
import X.C254019g;
import X.C469928m;
import X.C54462gl;
import X.C74933j1;
import X.C75023jB;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;

/* loaded from: classes2.dex */
public class GifSearchContainer extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public RecyclerView A06;
    public WaEditText A07;
    public AnonymousClass01d A08;
    public C14820m6 A09;
    public C15330mx A0A;
    public C14850m9 A0B;
    public C16120oU A0C;
    public C253719d A0D;
    public C54462gl A0E;
    public AnonymousClass5RW A0F;
    public AbstractC253919f A0G;
    public AnonymousClass5UF A0H;
    public C16630pM A0I;
    public C252718t A0J;
    public AnonymousClass2P7 A0K;
    public CharSequence A0L;
    public String A0M;
    public boolean A0N;
    public boolean A0O;
    public final AbstractC018308n A0P;
    public final AbstractC05270Ox A0Q;
    public final C469928m A0R;
    public final AbstractView$OnClickListenerC34281fs A0S;
    public final AbstractView$OnClickListenerC34281fs A0T;
    public final AbstractView$OnClickListenerC34281fs A0U;
    public final Runnable A0V;

    public GifSearchContainer(Context context) {
        super(context);
        if (!this.A0N) {
            this.A0N = true;
            generatedComponent();
        }
        this.A0O = false;
        this.A0V = new RunnableBRunnable0Shape6S0100000_I0_6(this, 38);
        this.A0R = new AnonymousClass47K(this);
        this.A0S = new ViewOnClickCListenerShape14S0100000_I0_1(this, 31);
        this.A0U = new ViewOnClickCListenerShape14S0100000_I0_1(this, 32);
        this.A0T = new ViewOnClickCListenerShape14S0100000_I0_1(this, 33);
        this.A0Q = new C75023jB(this);
        this.A0P = new C74933j1(this);
    }

    public GifSearchContainer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A0O = false;
        this.A0V = new RunnableBRunnable0Shape6S0100000_I0_6(this, 38);
        this.A0R = new AnonymousClass47K(this);
        this.A0S = new ViewOnClickCListenerShape14S0100000_I0_1(this, 31);
        this.A0U = new ViewOnClickCListenerShape14S0100000_I0_1(this, 32);
        this.A0T = new ViewOnClickCListenerShape14S0100000_I0_1(this, 33);
        this.A0Q = new C75023jB(this);
        this.A0P = new C74933j1(this);
    }

    public GifSearchContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0N) {
            this.A0N = true;
            generatedComponent();
        }
        this.A0O = false;
        this.A0V = new RunnableBRunnable0Shape6S0100000_I0_6(this, 38);
        this.A0R = new AnonymousClass47K(this);
        this.A0S = new ViewOnClickCListenerShape14S0100000_I0_1(this, 31);
        this.A0U = new ViewOnClickCListenerShape14S0100000_I0_1(this, 32);
        this.A0T = new ViewOnClickCListenerShape14S0100000_I0_1(this, 33);
        this.A0Q = new C75023jB(this);
        this.A0P = new C74933j1(this);
    }

    public GifSearchContainer(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A0N) {
            this.A0N = true;
            generatedComponent();
        }
        this.A0O = false;
        this.A0V = new RunnableBRunnable0Shape6S0100000_I0_6(this, 38);
        this.A0R = new AnonymousClass47K(this);
        this.A0S = new ViewOnClickCListenerShape14S0100000_I0_1(this, 31);
        this.A0U = new ViewOnClickCListenerShape14S0100000_I0_1(this, 32);
        this.A0T = new ViewOnClickCListenerShape14S0100000_I0_1(this, 33);
        this.A0Q = new C75023jB(this);
        this.A0P = new C74933j1(this);
    }

    public GifSearchContainer(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A0N) {
            this.A0N = true;
            generatedComponent();
        }
    }

    public void A00() {
        int i;
        this.A0J.A01(this.A07);
        setVisibility(8);
        AbstractC253919f r0 = this.A0G;
        if (r0 != null) {
            C16120oU r2 = this.A0C;
            AnonymousClass434 r1 = new AnonymousClass434();
            if (!(r0 instanceof C254019g)) {
                i = 0;
            } else {
                i = 1;
            }
            r1.A00 = Integer.valueOf(i);
            r2.A07(r1);
        }
        this.A0G = null;
    }

    public void A01(Activity activity, AnonymousClass01d r5, C14820m6 r6, C14850m9 r7, C16120oU r8, C15320mw r9, C253719d r10, AbstractC253919f r11, AnonymousClass5UF r12, C16630pM r13, C252718t r14) {
        int i;
        this.A0G = r11;
        this.A0B = r7;
        this.A0D = r10;
        this.A0J = r14;
        this.A0C = r8;
        this.A08 = r5;
        this.A09 = r6;
        this.A0I = r13;
        this.A0H = r12;
        this.A0A = r9;
        setupViews(activity);
        setVisibility(0);
        this.A03.setVisibility(8);
        this.A04.setVisibility(8);
        this.A05.setVisibility(8);
        this.A03.setVisibility(0);
        AbstractC253919f r0 = this.A0G;
        if (r0 != null) {
            this.A0E.A0F(r0.A00());
        }
        this.A0M = "";
        this.A07.setText("");
        this.A07.requestFocus();
        this.A07.A04(false);
        C16120oU r2 = this.A0C;
        AbstractC253919f r02 = this.A0G;
        AnonymousClass435 r1 = new AnonymousClass435();
        if (!(r02 instanceof C254019g)) {
            i = 0;
        } else {
            i = 1;
        }
        r1.A00 = Integer.valueOf(i);
        r2.A07(r1);
    }

    public final void A02(CharSequence charSequence) {
        AnonymousClass3EY r0;
        if (this.A0G != null) {
            this.A04.setVisibility(8);
            this.A05.setVisibility(8);
            this.A03.setVisibility(0);
            boolean isEmpty = TextUtils.isEmpty(charSequence);
            C54462gl r2 = this.A0E;
            AbstractC253919f r1 = this.A0G;
            if (isEmpty) {
                r0 = r1.A00();
            } else if (!(r1 instanceof C254019g)) {
                r0 = new AnonymousClass326((C253819e) r1, charSequence);
            } else {
                r0 = new AnonymousClass327((C254019g) r1, charSequence);
            }
            r2.A0F(r0);
            this.A0M = charSequence.toString();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0K;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0K = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (getMeasuredHeight() != i4 - i2) {
            if (!this.A0O) {
                post(new RunnableBRunnable0Shape6S0100000_I0_6(this, 37));
            }
            this.A0O = !this.A0O;
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        SharedPreferences sharedPreferences;
        String str;
        int i3;
        if (!isInEditMode()) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (mode != 1073741824) {
                int suggestedMinimumHeight = getSuggestedMinimumHeight();
                if (!C252718t.A00(this)) {
                    int i4 = getResources().getConfiguration().orientation;
                    if (i4 == 1) {
                        sharedPreferences = this.A09.A00;
                        str = "keyboard_height_portrait";
                    } else if (i4 != 2) {
                        i3 = getResources().getDimensionPixelSize(R.dimen.gif_search_keyboard_height);
                        suggestedMinimumHeight += i3;
                    } else {
                        sharedPreferences = this.A09.A00;
                        str = "keyboard_height_landscape";
                    }
                    i3 = sharedPreferences.getInt(str, 0);
                    suggestedMinimumHeight += i3;
                }
                size = Math.min(suggestedMinimumHeight, size);
            }
            i2 = View.MeasureSpec.makeMeasureSpec(size, mode);
        }
        super.onMeasure(i, i2);
    }

    public void setOnActionListener(AnonymousClass5RW r1) {
        this.A0F = r1;
    }

    public void setSearchContainerGravity(int i) {
        this.A00 = i;
    }

    private void setupRecyclerView(ViewGroup viewGroup) {
        RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(viewGroup, R.id.search_result);
        this.A06 = recyclerView;
        recyclerView.A0n(this.A0Q);
        this.A06.A0l(this.A0P);
        C253719d r4 = this.A0D;
        AnonymousClass321 r1 = new AnonymousClass321(this.A08, this.A0C, r4, this, this.A0H, this.A0I);
        this.A0E = r1;
        this.A06.setAdapter(r1);
    }

    private void setupSearchContainer(ViewGroup viewGroup) {
        String str;
        this.A04 = AnonymousClass028.A0D(viewGroup, R.id.no_results);
        this.A05 = AnonymousClass028.A0D(viewGroup, R.id.retry_panel);
        this.A02 = AnonymousClass028.A0D(viewGroup, R.id.search_container);
        WaEditText waEditText = (WaEditText) AnonymousClass028.A0D(viewGroup, R.id.search_bar);
        this.A07 = waEditText;
        waEditText.addTextChangedListener(this.A0R);
        this.A07.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 5));
        if (this.A0G != null) {
            WaEditText waEditText2 = this.A07;
            Resources resources = getResources();
            Object[] objArr = new Object[1];
            if (!(this.A0G instanceof C254019g)) {
                str = "Giphy";
            } else {
                str = "Tenor";
            }
            objArr[0] = str;
            waEditText2.setHint(resources.getString(R.string.gif_search_hint, objArr));
        }
        View A0D = AnonymousClass028.A0D(viewGroup, R.id.clear_search_btn);
        this.A01 = A0D;
        A0D.setOnClickListener(this.A0T);
        this.A03 = AnonymousClass028.A0D(viewGroup, R.id.progress_container);
        AnonymousClass028.A0D(viewGroup, R.id.back).setOnClickListener(this.A0S);
        AnonymousClass028.A0D(viewGroup, R.id.retry_button).setOnClickListener(this.A0U);
    }

    private void setupViews(Activity activity) {
        if (getChildCount() <= 0) {
            ViewGroup viewGroup = (ViewGroup) activity.getLayoutInflater().inflate(R.layout.gif_search, (ViewGroup) this, false);
            setupRecyclerView(viewGroup);
            setupSearchContainer(viewGroup);
            View view = this.A02;
            if (view != null) {
                viewGroup.removeView(view);
                if (this.A00 == 48) {
                    viewGroup.addView(this.A02, 0);
                } else {
                    viewGroup.addView(this.A02, viewGroup.getChildCount());
                }
            }
            addView(viewGroup);
        }
    }
}
