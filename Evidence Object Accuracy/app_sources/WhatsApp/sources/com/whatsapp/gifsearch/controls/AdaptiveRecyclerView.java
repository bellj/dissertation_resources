package com.whatsapp.gifsearch.controls;

import X.AbstractC35731ia;
import X.AnonymousClass02H;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/* loaded from: classes2.dex */
public class AdaptiveRecyclerView extends RecyclerView {
    public StaggeredGridLayoutManager A00;
    public final Rect A01 = C12980iv.A0J();

    public AdaptiveRecyclerView(Context context) {
        super(context);
        A0y();
    }

    public AdaptiveRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0y();
    }

    public AdaptiveRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0y();
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public void A0X(int i) {
        if (i == 0) {
            A0M();
        }
    }

    public final void A0y() {
        if (this.A00 == null) {
            this.A00 = new StaggeredGridLayoutManager(1);
            setItemAnimator(null);
            StaggeredGridLayoutManager staggeredGridLayoutManager = this.A00;
            staggeredGridLayoutManager.A13(null);
            if (0 != staggeredGridLayoutManager.A01) {
                staggeredGridLayoutManager.A01 = 0;
                staggeredGridLayoutManager.A0E();
            }
            setLayoutManager(this.A00);
        }
        StaggeredGridLayoutManager staggeredGridLayoutManager2 = this.A00;
        if (staggeredGridLayoutManager2 != null) {
            staggeredGridLayoutManager2.A1K(getSpanCount());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public StaggeredGridLayoutManager getLayoutManager() {
        return this.A00;
    }

    private int getSpanCount() {
        float width;
        Rect rect = this.A01;
        getHitRect(rect);
        Context context = getContext();
        if (rect == null) {
            width = 0.0f;
        } else {
            width = (float) rect.width();
        }
        Activity A00 = AbstractC35731ia.A00(context);
        int i = 0;
        if (A00 == null || A00.isFinishing() || Float.compare(width, 0.0f) == 0) {
            return 1;
        }
        float A01 = width / C12960it.A01(context);
        int[] iArr = {360, 480, 600, 720, 840, 960, 1280, 1440, 1920, 1600};
        do {
            int i2 = iArr[i];
            if (Float.compare(A01, (float) i2) < 0) {
                switch (i2) {
                    case 360:
                    case 480:
                        return 2;
                    case 600:
                    case 720:
                        return 3;
                    case 840:
                    case 960:
                        return 4;
                    case 1280:
                    case 1440:
                        return 5;
                    case 1600:
                    case 1920:
                        return 6;
                    case 2240:
                        return 7;
                    default:
                        return 1;
                }
            } else {
                i++;
            }
        } while (i < 10);
        return 7;
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        StaggeredGridLayoutManager staggeredGridLayoutManager;
        super.onSizeChanged(i, i2, i3, i4);
        if (i3 != i && (staggeredGridLayoutManager = this.A00) != null) {
            staggeredGridLayoutManager.A1K(getSpanCount());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public void setLayoutManager(AnonymousClass02H r2) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = this.A00;
        if (staggeredGridLayoutManager == null || r2 == staggeredGridLayoutManager) {
            super.setLayoutManager(r2);
            return;
        }
        throw C12970iu.A0f("LayoutManager cannot be replaced for this RecyclerView");
    }
}
