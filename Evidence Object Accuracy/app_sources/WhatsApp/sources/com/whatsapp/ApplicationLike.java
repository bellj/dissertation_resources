package com.whatsapp;

import android.content.res.Configuration;

/* loaded from: classes.dex */
public interface ApplicationLike {
    void onConfigurationChanged(Configuration configuration);

    void onCreate();
}
