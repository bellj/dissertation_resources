package com.whatsapp;

import X.AbstractC017308c;
import X.AnonymousClass009;
import X.C015607k;
import X.C14850m9;
import X.C235812f;
import X.C42781vo;
import X.C54202gL;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.IntentChooserBottomSheetDialogFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class IntentChooserBottomSheetDialogFragment extends Hilt_IntentChooserBottomSheetDialogFragment {
    public int A00;
    public int A01;
    public GridLayoutManager A02;
    public RecyclerView A03;
    public C14850m9 A04;
    public C235812f A05;
    public Integer A06;
    public ArrayList A07;

    public static IntentChooserBottomSheetDialogFragment A00(List list, int i, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt("title_resource", i);
        bundle.putParcelableArrayList("choosable_intents", new ArrayList<>(list));
        bundle.putInt("request_code", i2);
        IntentChooserBottomSheetDialogFragment intentChooserBottomSheetDialogFragment = new IntentChooserBottomSheetDialogFragment();
        intentChooserBottomSheetDialogFragment.A0U(bundle);
        return intentChooserBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        boolean A07 = this.A04.A07(689);
        int i = R.layout.intent_selector;
        if (A07) {
            i = R.layout.intent_selector_group_profile_editor;
        }
        View inflate = layoutInflater.inflate(i, viewGroup, false);
        Bundle A03 = A03();
        this.A00 = A03.getInt("request_code");
        ArrayList parcelableArrayList = A03.getParcelableArrayList("choosable_intents");
        AnonymousClass009.A05(parcelableArrayList);
        this.A07 = new ArrayList(parcelableArrayList);
        this.A01 = A03.getInt("title_resource");
        if (A03.containsKey("parent_fragment")) {
            this.A06 = Integer.valueOf(A03.getInt("parent_fragment"));
        }
        Iterator it = this.A07.iterator();
        while (it.hasNext()) {
            Integer num = ((C42781vo) it.next()).A04;
            if (num != null) {
                this.A05.A02(num.intValue());
            }
        }
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        this.A03 = (RecyclerView) inflate.findViewById(R.id.intent_recycler);
        int A1M = A1M();
        A01();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(A1M);
        this.A02 = gridLayoutManager;
        this.A03.setLayoutManager(gridLayoutManager);
        ArrayList arrayList = this.A07;
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            C42781vo r1 = (C42781vo) it2.next();
            if (r1.A05) {
                arrayList2.add(r1);
                it2.remove();
            }
        }
        Toolbar toolbar = (Toolbar) inflate.findViewById(R.id.toolbar);
        if (toolbar != null) {
            Iterator it3 = arrayList2.iterator();
            while (it3.hasNext()) {
                C42781vo r7 = (C42781vo) it3.next();
                Drawable drawable = A02().getDrawable(r7.A06);
                if (r7.A02 != null) {
                    drawable = C015607k.A03(drawable);
                    C015607k.A0A(drawable, r7.A02.intValue());
                }
                toolbar.getMenu().add(0, r7.A00, 0, r7.A07).setIcon(drawable).setIntent(r7.A08).setShowAsAction(r7.A01);
            }
            toolbar.A0R = new AbstractC017308c() { // from class: X.3P0
                @Override // X.AbstractC017308c
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    IntentChooserBottomSheetDialogFragment intentChooserBottomSheetDialogFragment = IntentChooserBottomSheetDialogFragment.this;
                    if (intentChooserBottomSheetDialogFragment.A0K.A02.compareTo(AnonymousClass05I.STARTED) < 0) {
                        return false;
                    }
                    if (intentChooserBottomSheetDialogFragment.A06 == null) {
                        intentChooserBottomSheetDialogFragment.A0C().startActivityForResult(menuItem.getIntent(), intentChooserBottomSheetDialogFragment.A00);
                    } else {
                        AnonymousClass01E A072 = intentChooserBottomSheetDialogFragment.A0F().A07(intentChooserBottomSheetDialogFragment.A06.intValue());
                        AnonymousClass009.A05(A072);
                        A072.startActivityForResult(menuItem.getIntent(), intentChooserBottomSheetDialogFragment.A00);
                    }
                    intentChooserBottomSheetDialogFragment.A1B();
                    return true;
                }
            };
        }
        this.A03.setAdapter(new C54202gL(this, this.A07));
        textView.setText(this.A01);
        return inflate;
    }

    public final int A1M() {
        if (!this.A04.A07(689)) {
            return 4;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        A0C().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels / A02().getDimensionPixelSize(R.dimen.intent_selector_minimum_item_width);
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int A1M = A1M();
        A01();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(A1M);
        this.A02 = gridLayoutManager;
        this.A03.setLayoutManager(gridLayoutManager);
    }
}
