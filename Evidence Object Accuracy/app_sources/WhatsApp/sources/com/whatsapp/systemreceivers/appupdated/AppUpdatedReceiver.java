package com.whatsapp.systemreceivers.appupdated;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C12980iv;
import X.C16700pc;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes2.dex */
public final class AppUpdatedReceiver extends BroadcastReceiver {
    public Set A00;
    public final Object A01;
    public volatile boolean A02;

    public AppUpdatedReceiver() {
        this(0);
    }

    public AppUpdatedReceiver(int i) {
        this.A02 = false;
        this.A01 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String action;
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = ((AnonymousClass01J) AnonymousClass22D.A00(context)).A4w();
                    this.A02 = true;
                }
            }
        }
        C16700pc.A0E(context, 0);
        if (intent == null) {
            action = null;
        } else {
            action = intent.getAction();
        }
        if ("android.intent.action.MY_PACKAGE_REPLACED".equals(action)) {
            Set set = this.A00;
            if (set != null) {
                Iterator it = set.iterator();
                if (it.hasNext()) {
                    it.next();
                    throw C12980iv.A0n("onAppUpdated");
                }
                return;
            }
            throw C16700pc.A06("appUpdatedObservers");
        }
    }
}
