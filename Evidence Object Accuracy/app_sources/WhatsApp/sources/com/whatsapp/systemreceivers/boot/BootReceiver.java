package com.whatsapp.systemreceivers.boot;

import X.AbstractC20200vN;
import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C15510nN;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;
import java.util.Set;

/* loaded from: classes2.dex */
public class BootReceiver extends BroadcastReceiver {
    public C15510nN A00;
    public Set A01;
    public final Object A02;
    public volatile boolean A03;

    public BootReceiver() {
        this(0);
    }

    public BootReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = r1.A4x();
                    this.A00 = (C15510nN) r1.AHZ.get();
                    this.A03 = true;
                }
            }
        }
        if (intent != null && "android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Log.i("BootReceiver; boot completed.");
            if (this.A00.A01()) {
                for (AbstractC20200vN r0 : this.A01) {
                    r0.ANK();
                }
            }
        }
    }
}
