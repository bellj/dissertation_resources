package com.whatsapp.gallery;

import X.AbstractC13890kV;
import X.AbstractC14640lm;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC35481hz;
import X.AbstractC35601iM;
import X.AbstractC35611iN;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass12H;
import X.AnonymousClass19O;
import X.AnonymousClass2T3;
import X.C15250mo;
import X.C15650ng;
import X.C15660nh;
import X.C35311hc;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.whatsapp.R;
import com.whatsapp.scroller.RecyclerFastScroller;

/* loaded from: classes2.dex */
public class MediaGalleryFragment extends Hilt_MediaGalleryFragment implements AbstractC35481hz {
    public C15650ng A00;
    public C15660nh A01;
    public AnonymousClass12H A02;
    public AbstractC14640lm A03;
    public AnonymousClass19O A04;
    public final AbstractC18860tB A05 = new C35311hc(this);

    @Override // X.AbstractC35481hz
    public void AVc(C15250mo r1) {
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        AbstractC14640lm A01 = AbstractC14640lm.A01(A0C().getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        this.A03 = A01;
        AnonymousClass028.A0m(((MediaGalleryFragmentBase) this).A08, true);
        AnonymousClass028.A0m(A05().findViewById(R.id.no_media), true);
        A1H(false);
        ActivityC000900k A0B = A0B();
        if (A0B instanceof MediaGalleryActivity) {
            ((MediaGalleryFragmentBase) this).A08.A0n(((MediaGalleryActivity) A0B).A0q);
            ((RecyclerFastScroller) ((AnonymousClass01E) this).A0A.findViewById(R.id.scroller)).setAppBarLayout((CoordinatorLayout) A0B().findViewById(R.id.coordinator), (AppBarLayout) A0B().findViewById(R.id.appbar));
        }
        this.A02.A03(this.A05);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A02.A04(this.A05);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r5, AnonymousClass2T3 r6) {
        AbstractC16130oV r3 = ((AbstractC35601iM) r5).A03;
        boolean A1J = A1J();
        AbstractC13890kV r0 = (AbstractC13890kV) A0B();
        if (A1J) {
            r6.setChecked(r0.Af1(r3));
            return true;
        }
        r0.AeE(r3);
        r6.setChecked(true);
        return true;
    }

    @Override // X.AbstractC35481hz
    public void AVm() {
        ((MediaGalleryFragmentBase) this).A06.A02();
    }
}
