package com.whatsapp.gallery;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass12H;
import X.AnonymousClass12I;
import X.AnonymousClass12P;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass1BB;
import X.AnonymousClass1BK;
import X.AnonymousClass1CY;
import X.C14830m7;
import X.C14900mE;
import X.C15450nH;
import X.C15650ng;
import X.C15670ni;
import X.C15890o4;
import X.C20050v8;
import X.C253218y;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_GalleryFragmentBase extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A19()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallery.Hilt_GalleryFragmentBase.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (this instanceof Hilt_LinksGalleryFragment) {
            Hilt_LinksGalleryFragment hilt_LinksGalleryFragment = (Hilt_LinksGalleryFragment) this;
            if (!hilt_LinksGalleryFragment.A02) {
                hilt_LinksGalleryFragment.A02 = true;
                LinksGalleryFragment linksGalleryFragment = (LinksGalleryFragment) hilt_LinksGalleryFragment;
                AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_LinksGalleryFragment.generatedComponent())).A0Y;
                ((WaFragment) linksGalleryFragment).A00 = (AnonymousClass182) r2.A94.get();
                ((WaFragment) linksGalleryFragment).A01 = (AnonymousClass180) r2.ALt.get();
                ((GalleryFragmentBase) linksGalleryFragment).A03 = (C14830m7) r2.ALb.get();
                ((GalleryFragmentBase) linksGalleryFragment).A0E = (AbstractC14440lR) r2.ANe.get();
                ((GalleryFragmentBase) linksGalleryFragment).A05 = (AnonymousClass018) r2.ANb.get();
                ((GalleryFragmentBase) linksGalleryFragment).A06 = (C15650ng) r2.A4m.get();
                ((GalleryFragmentBase) linksGalleryFragment).A08 = (AnonymousClass12H) r2.AC5.get();
                ((GalleryFragmentBase) linksGalleryFragment).A09 = (AnonymousClass12I) r2.ACH.get();
                ((GalleryFragmentBase) linksGalleryFragment).A04 = (C15890o4) r2.AN1.get();
                linksGalleryFragment.A05 = (AnonymousClass19M) r2.A6R.get();
                linksGalleryFragment.A00 = (AnonymousClass12P) r2.A0H.get();
                linksGalleryFragment.A04 = (AnonymousClass1BK) r2.AFZ.get();
                linksGalleryFragment.A03 = (C20050v8) r2.AAV.get();
                linksGalleryFragment.A02 = (C253218y) r2.AAF.get();
                linksGalleryFragment.A06 = (AnonymousClass19O) r2.ACO.get();
            }
        } else if (this instanceof Hilt_DocumentsGalleryFragment) {
            Hilt_DocumentsGalleryFragment hilt_DocumentsGalleryFragment = (Hilt_DocumentsGalleryFragment) this;
            if (!hilt_DocumentsGalleryFragment.A02) {
                hilt_DocumentsGalleryFragment.A02 = true;
                DocumentsGalleryFragment documentsGalleryFragment = (DocumentsGalleryFragment) hilt_DocumentsGalleryFragment;
                AnonymousClass01J r22 = ((C51112Sw) ((AbstractC51092Su) hilt_DocumentsGalleryFragment.generatedComponent())).A0Y;
                ((WaFragment) documentsGalleryFragment).A00 = (AnonymousClass182) r22.A94.get();
                ((WaFragment) documentsGalleryFragment).A01 = (AnonymousClass180) r22.ALt.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A03 = (C14830m7) r22.ALb.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A0E = (AbstractC14440lR) r22.ANe.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A05 = (AnonymousClass018) r22.ANb.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A06 = (C15650ng) r22.A4m.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A08 = (AnonymousClass12H) r22.AC5.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A09 = (AnonymousClass12I) r22.ACH.get();
                ((GalleryFragmentBase) documentsGalleryFragment).A04 = (C15890o4) r22.AN1.get();
                documentsGalleryFragment.A07 = (AnonymousClass1CY) r22.ABO.get();
                documentsGalleryFragment.A02 = (C14900mE) r22.A8X.get();
                documentsGalleryFragment.A01 = (AbstractC15710nm) r22.A4o.get();
                documentsGalleryFragment.A03 = (C15450nH) r22.AII.get();
                documentsGalleryFragment.A00 = (AnonymousClass12P) r22.A0H.get();
                documentsGalleryFragment.A05 = (C253218y) r22.AAF.get();
                documentsGalleryFragment.A06 = (C15670ni) r22.AIb.get();
                documentsGalleryFragment.A04 = (AnonymousClass1BB) r22.A68.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            GalleryFragmentBase galleryFragmentBase = (GalleryFragmentBase) this;
            AnonymousClass01J r23 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            ((WaFragment) galleryFragmentBase).A00 = (AnonymousClass182) r23.A94.get();
            ((WaFragment) galleryFragmentBase).A01 = (AnonymousClass180) r23.ALt.get();
            galleryFragmentBase.A03 = (C14830m7) r23.ALb.get();
            galleryFragmentBase.A0E = (AbstractC14440lR) r23.ANe.get();
            galleryFragmentBase.A05 = (AnonymousClass018) r23.ANb.get();
            galleryFragmentBase.A06 = (C15650ng) r23.A4m.get();
            galleryFragmentBase.A08 = (AnonymousClass12H) r23.AC5.get();
            galleryFragmentBase.A09 = (AnonymousClass12I) r23.ACH.get();
            galleryFragmentBase.A04 = (C15890o4) r23.AN1.get();
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
