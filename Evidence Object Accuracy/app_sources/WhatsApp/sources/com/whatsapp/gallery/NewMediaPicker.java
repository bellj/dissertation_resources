package com.whatsapp.gallery;

import X.AbstractC009504t;
import X.C16700pc;
import X.C41691tw;
import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.MediaPicker;

/* loaded from: classes2.dex */
public final class NewMediaPicker extends MediaPicker {
    @Override // com.whatsapp.gallerypicker.MediaPicker, X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        C16700pc.A0E(r2, 0);
        super.AXD(r2);
        C41691tw.A02(this, R.color.gallery_toolbar_background);
    }

    @Override // com.whatsapp.gallerypicker.MediaPicker, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            setResult(-1, intent);
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }
}
