package com.whatsapp.gallery;

import X.AbstractC017308c;
import X.AbstractC14440lR;
import X.AbstractC14710lv;
import X.AbstractC16710pd;
import X.AbstractC35611iN;
import X.AbstractC454421p;
import X.AbstractC48852Ic;
import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01A;
import X.AnonymousClass01F;
import X.AnonymousClass01T;
import X.AnonymousClass028;
import X.AnonymousClass02M;
import X.AnonymousClass05I;
import X.AnonymousClass070;
import X.AnonymousClass1s8;
import X.AnonymousClass24W;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass4Yq;
import X.C018108l;
import X.C14830m7;
import X.C16590pI;
import X.C16700pc;
import X.C16760pi;
import X.C16770pj;
import X.C18720su;
import X.C453421e;
import X.C457522x;
import X.C50962Se;
import X.C50972Sf;
import X.C50982Sg;
import X.C50992Sh;
import X.C51002Sj;
import X.C51022Sn;
import X.C51032So;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;
import com.whatsapp.WaMediaThumbnailView;
import com.whatsapp.gallery.GalleryRecentsFragment;
import com.whatsapp.gallery.GalleryTabHostFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public final class GalleryTabHostFragment extends Hilt_GalleryTabHostFragment implements AnonymousClass070, AbstractC48852Ic {
    public long A00;
    public View A01;
    public ViewGroup A02;
    public Toolbar A03;
    public RecyclerView A04;
    public ViewPager A05;
    public C18720su A06;
    public C14830m7 A07;
    public C16590pI A08;
    public AnonymousClass018 A09;
    public AbstractC14440lR A0A;
    public final Handler A0B = new Handler(Looper.getMainLooper());
    public final AbstractC16710pd A0C = AnonymousClass4Yq.A00(new C50962Se(this));
    public final AbstractC16710pd A0D = AnonymousClass4Yq.A00(new C50972Sf(this));

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public static final View A00(ViewGroup viewGroup) {
        C16700pc.A0E(viewGroup, 0);
        View childAt = viewGroup.getChildAt(0);
        if (childAt != null) {
            return childAt;
        }
        StringBuilder sb = new StringBuilder("Index: ");
        sb.append(0);
        sb.append(", Size: ");
        sb.append(viewGroup.getChildCount());
        throw new IndexOutOfBoundsException(sb.toString());
    }

    @Override // com.whatsapp.base.WaFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        ViewPager viewPager;
        super.A0n(z);
        if (this.A0K.A02.compareTo(AnonymousClass05I.RESUMED) >= 0 && (viewPager = this.A05) != null) {
            viewPager.setCurrentItem(0);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        AbstractC14710lv r1;
        AnonymousClass1s8 ABD;
        ActivityC000900k A0B = A0B();
        if (!(!(A0B instanceof AbstractC14710lv) || (r1 = (AbstractC14710lv) A0B) == null || (ABD = r1.ABD()) == null || ABD.A08 == null)) {
            ABD.A0D(i, i2, intent);
        }
        if (i != 101) {
            if (i == 91) {
                if (i2 != -1) {
                    return;
                }
                if (A1J()) {
                    if (intent != null) {
                        ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                        if (parcelableArrayListExtra == null) {
                            Uri data = intent.getData();
                            if (data != null) {
                                parcelableArrayListExtra = new ArrayList(1);
                                parcelableArrayListExtra.add(data);
                            } else {
                                return;
                            }
                        }
                        startActivityForResult(A1A(parcelableArrayListExtra), 90);
                        return;
                    }
                    return;
                }
            } else if (i != 90) {
                return;
            } else {
                if (i2 != -1) {
                    if (i2 == 2) {
                        ActivityC000900k A0B2 = A0B();
                        if (A0B2 != null) {
                            A0B2.setResult(2);
                        }
                    } else {
                        return;
                    }
                }
            }
            ActivityC000900k A0B3 = A0B();
            if (A0B3 != null) {
                A0B3.setResult(-1, intent);
            }
        } else if (i2 != -1) {
            if (i2 == 0) {
                A1E();
                return;
            }
            return;
        }
        ActivityC000900k A0B4 = A0B();
        if (A0B4 != null) {
            A0B4.finish();
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.gallery_tab_host, viewGroup, false);
        C16700pc.A0B(inflate);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        List list;
        super.A12();
        ((C457522x) this.A0C.getValue()).A00();
        View view = this.A01;
        if (view != null) {
            view.setOnClickListener(null);
        }
        this.A01 = null;
        ViewPager viewPager = this.A05;
        if (viewPager != null) {
            viewPager.setAdapter(null);
        }
        ViewPager viewPager2 = this.A05;
        if (!(viewPager2 == null || (list = viewPager2.A0c) == null)) {
            list.remove(this);
        }
        this.A05 = null;
        this.A02 = null;
        RecyclerView recyclerView = this.A04;
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        this.A04 = null;
        this.A03 = null;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        ViewPager viewPager;
        Intent intent;
        int intExtra;
        AbstractC14710lv r2;
        C16700pc.A0E(view, 0);
        if (this.A07 != null) {
            this.A00 = System.currentTimeMillis();
            ViewPager viewPager2 = (ViewPager) view.findViewById(R.id.gallery_view_pager);
            AnonymousClass028.A0m(viewPager2, true);
            ActivityC000900k A0B = A0B();
            AnonymousClass1s8 r7 = null;
            if ((A0B instanceof AbstractC14710lv) && (r2 = (AbstractC14710lv) A0B) != null) {
                r7 = r2.ABD();
            }
            Resources resources = A1B().A00.getResources();
            Bundle bundle2 = super.A05;
            int i = 7;
            if (bundle2 != null) {
                i = bundle2.getInt("include", 7);
            }
            AnonymousClass01F A0E = A0E();
            C16700pc.A0B(resources);
            viewPager2.setAdapter(new C50982Sg(resources, A0E, r7, this, i));
            viewPager2.A0G(this);
            this.A05 = viewPager2;
            ((TabLayout) view.findViewById(R.id.gallery_tab_layout)).setupWithViewPager(this.A05);
            this.A02 = (ViewGroup) view.findViewById(R.id.gallery_selected_container);
            View findViewById = view.findViewById(R.id.toolbar);
            C16700pc.A0B(findViewById);
            Toolbar toolbar = (Toolbar) findViewById;
            this.A03 = toolbar;
            Drawable A01 = AnonymousClass2GE.A01(A1B().A00, R.drawable.ic_back, R.color.gallery_toolbar_icon);
            C16700pc.A0B(A01);
            AnonymousClass018 r22 = this.A09;
            if (r22 != null) {
                toolbar.setNavigationIcon(new AnonymousClass2GF(A01, r22));
                toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 3));
                toolbar.setTitleTextColor(AnonymousClass00T.A00(A1B().A00, R.color.gallery_toolbar_text));
                Menu menu = toolbar.getMenu();
                C16700pc.A0B(menu);
                SubMenu subMenu = menu.findItem(R.id.more).getSubMenu();
                C16700pc.A0B(subMenu);
                Bundle bundle3 = super.A05;
                int i2 = 7;
                if (bundle3 != null) {
                    i2 = bundle3.getInt("include", 7);
                }
                C51032So r3 = new C51032So(this, A1B(), new C51022Sn(subMenu, this), i2);
                AbstractC14440lR r23 = this.A0A;
                if (r23 != null) {
                    r23.Aaz(r3, new Void[0]);
                    toolbar.A0R = new AbstractC017308c() { // from class: X.2Sp
                        @Override // X.AbstractC017308c
                        public final boolean onMenuItemClick(MenuItem menuItem) {
                            GalleryTabHostFragment galleryTabHostFragment = GalleryTabHostFragment.this;
                            C16700pc.A0E(galleryTabHostFragment, 0);
                            if (menuItem.getItemId() != R.id.multi_select) {
                                return false;
                            }
                            if (!galleryTabHostFragment.A1I()) {
                                return true;
                            }
                            C50982Sg A1D = galleryTabHostFragment.A1D();
                            if (A1D != null) {
                                ((GalleryRecentsFragment) A1D.A05.getValue()).A03 = true;
                            }
                            menuItem.setVisible(false);
                            Toolbar toolbar2 = galleryTabHostFragment.A03;
                            if (toolbar2 == null) {
                                return true;
                            }
                            toolbar2.setTitle(galleryTabHostFragment.A1B().A00.getResources().getString(R.string.select_multiple_title));
                            return true;
                        }
                    };
                    ActivityC000900k A0B2 = A0B();
                    if ((A0B2 == null || (intent = A0B2.getIntent()) == null || !((intExtra = intent.getIntExtra("origin", 1)) == 2 || intExtra == 15 || intExtra == 18 || intExtra == 7 || intExtra == 8 || intExtra == 12 || intExtra == 13)) && (viewPager = this.A05) != null) {
                        viewPager.A0F(1, false);
                    }
                    C16700pc.A0B(view.getContext());
                    View findViewById2 = view.findViewById(R.id.gallery_selected_media);
                    C16700pc.A0B(findViewById2);
                    RecyclerView recyclerView = (RecyclerView) findViewById2;
                    this.A04 = recyclerView;
                    recyclerView.A0h = true;
                    recyclerView.setAdapter((AnonymousClass02M) this.A0D.getValue());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
                    linearLayoutManager.A1Q(0);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    View findViewById3 = view.findViewById(R.id.gallery_done_btn);
                    C16700pc.A0B(findViewById3);
                    this.A01 = findViewById3;
                    findViewById3.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 2));
                    return;
                }
                C16700pc.A0K("waWorkers");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            C16700pc.A0K("whatsAppLocale");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        C16700pc.A0K("time");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final Intent A1A(ArrayList arrayList) {
        Intent intent;
        Intent intent2;
        Intent intent3;
        Intent intent4;
        Intent intent5;
        Intent intent6;
        Intent intent7;
        ActivityC000900k A0B = A0B();
        int i = 1;
        if (!(A0B == null || (intent7 = A0B.getIntent()) == null)) {
            i = intent7.getIntExtra("origin", 1);
        }
        AnonymousClass24W r5 = new AnonymousClass24W(A0C());
        if (this.A07 != null) {
            r5.A02 = System.currentTimeMillis() - this.A00;
            ActivityC000900k A0B2 = A0B();
            boolean z = false;
            if (!(A0B2 == null || (intent6 = A0B2.getIntent()) == null)) {
                z = intent6.getBooleanExtra("number_from_url", false);
            }
            r5.A0D = z;
            ActivityC000900k A0B3 = A0B();
            String str = null;
            if (!(A0B3 == null || (intent5 = A0B3.getIntent()) == null)) {
                str = intent5.getStringExtra("jid");
            }
            r5.A08 = str;
            ActivityC000900k A0B4 = A0B();
            int i2 = Integer.MAX_VALUE;
            if (!(A0B4 == null || (intent4 = A0B4.getIntent()) == null)) {
                i2 = intent4.getIntExtra("max_items", Integer.MAX_VALUE);
            }
            r5.A00 = i2;
            r5.A01 = i;
            ActivityC000900k A0B5 = A0B();
            long j = 0;
            if (!(A0B5 == null || (intent3 = A0B5.getIntent()) == null)) {
                j = intent3.getLongExtra("picker_open_time", 0);
            }
            r5.A03 = j;
            ActivityC000900k A0B6 = A0B();
            String str2 = null;
            if (!(A0B6 == null || (intent2 = A0B6.getIntent()) == null)) {
                str2 = intent2.getStringExtra("quoted_group_jid");
            }
            r5.A09 = str2;
            ActivityC000900k A0B7 = A0B();
            long j2 = 0;
            if (!(A0B7 == null || (intent = A0B7.getIntent()) == null)) {
                j2 = intent.getLongExtra("quoted_message_row_id", 0);
            }
            r5.A04 = j2;
            boolean z2 = false;
            if (i != 20) {
                z2 = true;
            }
            r5.A0F = z2;
            r5.A0G = true;
            r5.A0C = arrayList;
            return r5.A00();
        }
        C16700pc.A0K("time");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final C16590pI A1B() {
        C16590pI r0 = this.A08;
        if (r0 != null) {
            return r0;
        }
        C16700pc.A0K("waContext");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final C51002Sj A1C(List list) {
        RecyclerView recyclerView;
        int size = list.size();
        if (!AbstractC454421p.A00 || ((C50992Sh) this.A0D.getValue()).A02.size() != 1 || (recyclerView = this.A04) == null || recyclerView.getChildCount() != 1 || size != 1 || super.A0A == null) {
            return new C51002Sj(null, null, null, null, null, 15);
        }
        RecyclerView recyclerView2 = this.A04;
        if (recyclerView2 == null) {
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        String obj = ((AbstractC35611iN) list.get(0)).AAE().toString();
        C16700pc.A0B(obj);
        return new C51002Sj((WaMediaThumbnailView) A00((ViewGroup) A00(recyclerView2)), obj);
    }

    public final C50982Sg A1D() {
        AnonymousClass01A r1;
        ViewPager viewPager = this.A05;
        if (viewPager == null) {
            r1 = null;
        } else {
            r1 = viewPager.A0V;
        }
        if (r1 instanceof C50982Sg) {
            return (C50982Sg) r1;
        }
        return null;
    }

    public final void A1E() {
        C50982Sg A1D = A1D();
        if (A1D != null) {
            GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) A1D.A05.getValue();
            if (false != galleryRecentsFragment.A03) {
                galleryRecentsFragment.A05.clear();
            }
            galleryRecentsFragment.A03 = false;
            galleryRecentsFragment.A05.clear();
            galleryRecentsFragment.A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (((X.C50992Sh) r5.A0D.getValue()).A02.size() != 0) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1F(int r6) {
        /*
            r5 = this;
            androidx.appcompat.widget.Toolbar r0 = r5.A03
            r4 = 0
            r3 = 0
            if (r0 == 0) goto L_0x0010
            android.view.Menu r0 = r0.getMenu()
            if (r0 == 0) goto L_0x0010
            android.view.MenuItem r3 = r0.getItem(r4)
        L_0x0010:
            r2 = 1
            if (r3 == 0) goto L_0x0030
            if (r6 != 0) goto L_0x002c
            boolean r0 = r5.A1I()
            if (r0 == 0) goto L_0x002c
            X.0pd r0 = r5.A0D
            java.lang.Object r0 = r0.getValue()
            X.2Sh r0 = (X.C50992Sh) r0
            java.util.List r0 = r0.A02
            int r1 = r0.size()
            r0 = 1
            if (r1 == 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r3.setVisible(r0)
        L_0x0030:
            androidx.appcompat.widget.Toolbar r0 = r5.A03
            if (r0 == 0) goto L_0x0046
            android.view.Menu r0 = r0.getMenu()
            if (r0 == 0) goto L_0x0046
            android.view.MenuItem r0 = r0.getItem(r2)
            if (r0 == 0) goto L_0x0046
            if (r6 != r2) goto L_0x0043
            r4 = 1
        L_0x0043:
            r0.setVisible(r4)
        L_0x0046:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallery.GalleryTabHostFragment.A1F(int):void");
    }

    public void A1G(List list) {
        Uri data;
        Bundle bundle;
        View view;
        String str;
        AbstractC14710lv r1;
        AnonymousClass1s8 ABD;
        List arrayList;
        View view2;
        C16700pc.A0E(list, 0);
        if (A1J()) {
            ActivityC000900k A0B = A0B();
            if (!(A0B instanceof AbstractC14710lv) || (r1 = (AbstractC14710lv) A0B) == null || (ABD = r1.ABD()) == null) {
                C51002Sj A1C = A1C(list);
                ArrayList arrayList2 = new ArrayList(C16760pi.A0D(list));
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    arrayList2.add(((AbstractC35611iN) it.next()).AAE());
                }
                Intent A1A = A1A(new ArrayList(arrayList2));
                ActivityC000900k A0B2 = A0B();
                if (A0B2 == null || (view = A1C.A01) == null || (str = A1C.A03) == null) {
                    bundle = null;
                } else {
                    bundle = C018108l.A01(A0B2, view, str).A03();
                }
                A0P(A1A, 1, bundle);
            } else if (!list.isEmpty()) {
                C51002Sj A1C2 = A1C(list);
                ArrayList arrayList3 = new ArrayList(C16760pi.A0D(list));
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    arrayList3.add(((AbstractC35611iN) it2.next()).AAE());
                }
                String str2 = A1C2.A03;
                if (str2 == null || (view2 = A1C2.A01) == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = C16770pj.A0J(new AnonymousClass01T(view2, str2));
                }
                ABD.A0F(A1C2.A00, this, A1C2.A02, arrayList3, arrayList);
            }
        } else {
            ArrayList arrayList4 = new ArrayList(C16760pi.A0D(list));
            Iterator it3 = list.iterator();
            while (it3.hasNext()) {
                arrayList4.add(((AbstractC35611iN) it3.next()).AAE());
            }
            ArrayList<? extends Parcelable> arrayList5 = new ArrayList<>(arrayList4);
            ActivityC000900k A0C = A0C();
            Intent intent = new Intent();
            Intent intent2 = A0C.getIntent();
            Uri uri = null;
            if (intent2 == null) {
                data = null;
            } else {
                data = intent2.getData();
            }
            intent.putExtra("bucket_uri", data);
            intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList5);
            if (arrayList5.size() == 1) {
                uri = (Uri) arrayList5.get(0);
            }
            intent.setData(uri);
            A0C.setResult(-1, intent);
            A0C.finish();
        }
    }

    public void A1H(Set set) {
        ViewGroup viewGroup;
        int currentItem;
        Toolbar toolbar = this.A03;
        String str = null;
        if (toolbar != null) {
            if (!set.isEmpty()) {
                AnonymousClass018 r8 = this.A09;
                if (r8 != null) {
                    str = r8.A0I(new Object[]{Integer.valueOf(set.size())}, R.plurals.n_photos_selected, (long) set.size());
                } else {
                    C16700pc.A0K("whatsAppLocale");
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
            } else {
                C50982Sg A1D = A1D();
                if (A1D != null) {
                    GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) A1D.A05.getValue();
                    if (false != galleryRecentsFragment.A03) {
                        galleryRecentsFragment.A05.clear();
                    }
                    galleryRecentsFragment.A03 = false;
                }
                Bundle bundle = super.A05;
                if (bundle != null) {
                    str = bundle.getString("gallery_picker_title");
                }
            }
            toolbar.setTitle(str);
        }
        int i = 8;
        if (!set.isEmpty()) {
            i = 0;
        }
        ViewGroup viewGroup2 = this.A02;
        if ((viewGroup2 == null || viewGroup2.getVisibility() != i) && (viewGroup = this.A02) != null) {
            viewGroup.setVisibility(i);
        }
        C50992Sh r1 = (C50992Sh) this.A0D.getValue();
        List list = r1.A02;
        list.clear();
        list.addAll(set);
        r1.A02();
        ViewPager viewPager = this.A05;
        if (viewPager == null) {
            currentItem = 0;
        } else {
            currentItem = viewPager.getCurrentItem();
        }
        A1F(currentItem);
    }

    public boolean A1I() {
        ActivityC000900k A0B;
        Intent intent;
        Intent intent2;
        ActivityC000900k A0B2 = A0B();
        int i = Integer.MAX_VALUE;
        if (!(A0B2 == null || (intent2 = A0B2.getIntent()) == null)) {
            i = intent2.getIntExtra("max_items", Integer.MAX_VALUE);
        }
        if (i <= 1) {
            return false;
        }
        if (A1J() || ((A0B = A0B()) != null && (intent = A0B.getIntent()) != null && intent.getBooleanExtra("is_in_multi_select_mode_only", false))) {
            return true;
        }
        return false;
    }

    public final boolean A1J() {
        Intent intent;
        ActivityC000900k A0B = A0B();
        return (A0B == null || (intent = A0B.getIntent()) == null || !intent.getBooleanExtra("preview", true)) ? false : true;
    }

    @Override // X.AbstractC48852Ic
    public void AGY(C453421e r2, Collection collection) {
        C16700pc.A0E(collection, 0);
        C16700pc.A0E(r2, 1);
        C50982Sg A1D = A1D();
        if (A1D != null) {
            A1D.AGY(r2, collection);
        }
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        String string;
        A1E();
        Toolbar toolbar = this.A03;
        if (toolbar != null) {
            Bundle bundle = super.A05;
            if (bundle == null) {
                string = null;
            } else {
                string = bundle.getString("gallery_picker_title");
            }
            toolbar.setTitle(string);
        }
        A1F(i);
    }

    @Override // X.AbstractC48852Ic
    public void AZy() {
        C50982Sg A1D = A1D();
        if (A1D != null) {
            A1D.AZy();
        }
    }

    @Override // X.AbstractC48852Ic
    public void Acp(C453421e r2, Collection collection, Collection collection2) {
        C16700pc.A0E(collection, 0);
        C16700pc.A0E(collection2, 1);
        C16700pc.A0E(r2, 2);
        C50982Sg A1D = A1D();
        if (A1D != null) {
            A1D.Acp(r2, collection, collection2);
        }
    }
}
