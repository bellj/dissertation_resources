package com.whatsapp.gallery;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AbstractC51202Tg;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass185;
import X.AnonymousClass19O;
import X.AnonymousClass1CY;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15650ng;
import X.C15660nh;
import X.C15670ni;
import X.C15690nk;
import X.C15890o4;
import X.C16590pI;
import X.C18720su;
import X.C19390u2;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;
import com.whatsapp.camera.bottomsheet.Hilt_CameraMediaPickerFragment;
import com.whatsapp.gallerypicker.Hilt_MediaPickerFragment;
import com.whatsapp.gallerypicker.MediaPickerFragment;
import com.whatsapp.storage.Hilt_StorageUsageMediaGalleryFragment;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_MediaGalleryFragmentBase extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A19()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallery.Hilt_MediaGalleryFragmentBase.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v7, types: [com.whatsapp.gallerypicker.MediaPickerFragment] */
    /* JADX WARN: Type inference failed for: r1v18, types: [com.whatsapp.gallery.MediaGalleryFragmentBase] */
    public void A18() {
        AbstractC51092Su r3;
        Hilt_NewMediaPickerFragment hilt_NewMediaPickerFragment;
        AbstractC51092Su r32;
        Hilt_CameraMediaPickerFragment hilt_CameraMediaPickerFragment;
        if (this instanceof Hilt_StorageUsageMediaGalleryFragment) {
            Hilt_StorageUsageMediaGalleryFragment hilt_StorageUsageMediaGalleryFragment = (Hilt_StorageUsageMediaGalleryFragment) this;
            if (!hilt_StorageUsageMediaGalleryFragment.A02) {
                hilt_StorageUsageMediaGalleryFragment.A02 = true;
                StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = (StorageUsageMediaGalleryFragment) hilt_StorageUsageMediaGalleryFragment;
                C51112Sw r33 = (C51112Sw) ((AbstractC51092Su) hilt_StorageUsageMediaGalleryFragment.generatedComponent());
                AnonymousClass01J r2 = r33.A0Y;
                ((WaFragment) storageUsageMediaGalleryFragment).A00 = (AnonymousClass182) r2.A94.get();
                ((WaFragment) storageUsageMediaGalleryFragment).A01 = (AnonymousClass180) r2.ALt.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A09 = (C18720su) r2.A2c.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0F = (C14850m9) r2.A04.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0B = (C16590pI) r2.AMg.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0N = (AbstractC14440lR) r2.ANe.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0A = (AnonymousClass01d) r2.ALI.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0D = (AbstractC51202Tg) r33.A0U.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0C = (C15890o4) r2.AN1.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A07 = (C14900mE) r2.A8X.get();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0K = r33.A0V.A06();
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0E = (AnonymousClass018) r2.ANb.get();
                storageUsageMediaGalleryFragment.A09 = (AnonymousClass1CY) r2.ABO.get();
                storageUsageMediaGalleryFragment.A02 = (AbstractC15710nm) r2.A4o.get();
                storageUsageMediaGalleryFragment.A03 = (C15450nH) r2.AII.get();
                storageUsageMediaGalleryFragment.A01 = (AnonymousClass12P) r2.A0H.get();
                storageUsageMediaGalleryFragment.A04 = (C15650ng) r2.A4m.get();
                storageUsageMediaGalleryFragment.A05 = (C15660nh) r2.ABE.get();
                storageUsageMediaGalleryFragment.A0A = (AnonymousClass19O) r2.ACO.get();
                storageUsageMediaGalleryFragment.A07 = (C15670ni) r2.AIb.get();
                storageUsageMediaGalleryFragment.A06 = (AnonymousClass12H) r2.AC5.get();
            }
        } else if (this instanceof Hilt_MediaPickerFragment) {
            Hilt_MediaPickerFragment hilt_MediaPickerFragment = (Hilt_MediaPickerFragment) this;
            if (hilt_MediaPickerFragment instanceof Hilt_NewMediaPickerFragment) {
                Hilt_NewMediaPickerFragment hilt_NewMediaPickerFragment2 = (Hilt_NewMediaPickerFragment) hilt_MediaPickerFragment;
                if (!hilt_NewMediaPickerFragment2.A02) {
                    hilt_NewMediaPickerFragment2.A02 = true;
                    r3 = (AbstractC51092Su) hilt_NewMediaPickerFragment2.generatedComponent();
                    hilt_NewMediaPickerFragment = hilt_NewMediaPickerFragment2;
                } else {
                    return;
                }
            } else if (!hilt_MediaPickerFragment.A02) {
                hilt_MediaPickerFragment.A02 = true;
                r3 = (AbstractC51092Su) hilt_MediaPickerFragment.generatedComponent();
                hilt_NewMediaPickerFragment = (MediaPickerFragment) hilt_MediaPickerFragment;
            } else {
                return;
            }
            C51112Sw r34 = (C51112Sw) r3;
            AnonymousClass01J r22 = r34.A0Y;
            ((WaFragment) hilt_NewMediaPickerFragment).A00 = (AnonymousClass182) r22.A94.get();
            ((WaFragment) hilt_NewMediaPickerFragment).A01 = (AnonymousClass180) r22.ALt.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A09 = (C18720su) r22.A2c.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0F = (C14850m9) r22.A04.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0B = (C16590pI) r22.AMg.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0N = (AbstractC14440lR) r22.ANe.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0A = (AnonymousClass01d) r22.ALI.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0D = (AbstractC51202Tg) r34.A0U.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0C = (C15890o4) r22.AN1.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A07 = (C14900mE) r22.A8X.get();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0K = r34.A0V.A06();
            ((MediaGalleryFragmentBase) hilt_NewMediaPickerFragment).A0E = (AnonymousClass018) r22.ANb.get();
            ((MediaPickerFragment) hilt_NewMediaPickerFragment).A06 = (C15450nH) r22.AII.get();
            ((MediaPickerFragment) hilt_NewMediaPickerFragment).A08 = (C19390u2) r22.AFY.get();
            ((MediaPickerFragment) hilt_NewMediaPickerFragment).A0A = (C15690nk) r22.A74.get();
            ((MediaPickerFragment) hilt_NewMediaPickerFragment).A09 = (AnonymousClass185) r22.AHy.get();
        } else if (this instanceof Hilt_MediaGalleryFragment) {
            Hilt_MediaGalleryFragment hilt_MediaGalleryFragment = (Hilt_MediaGalleryFragment) this;
            if (!hilt_MediaGalleryFragment.A02) {
                hilt_MediaGalleryFragment.A02 = true;
                MediaGalleryFragment mediaGalleryFragment = (MediaGalleryFragment) hilt_MediaGalleryFragment;
                C51112Sw r35 = (C51112Sw) ((AbstractC51092Su) hilt_MediaGalleryFragment.generatedComponent());
                AnonymousClass01J r23 = r35.A0Y;
                ((WaFragment) mediaGalleryFragment).A00 = (AnonymousClass182) r23.A94.get();
                ((WaFragment) mediaGalleryFragment).A01 = (AnonymousClass180) r23.ALt.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A09 = (C18720su) r23.A2c.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0F = (C14850m9) r23.A04.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0B = (C16590pI) r23.AMg.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0N = (AbstractC14440lR) r23.ANe.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0A = (AnonymousClass01d) r23.ALI.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0D = (AbstractC51202Tg) r35.A0U.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0C = (C15890o4) r23.AN1.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A07 = (C14900mE) r23.A8X.get();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0K = r35.A0V.A06();
                ((MediaGalleryFragmentBase) mediaGalleryFragment).A0E = (AnonymousClass018) r23.ANb.get();
                mediaGalleryFragment.A00 = (C15650ng) r23.A4m.get();
                mediaGalleryFragment.A01 = (C15660nh) r23.ABE.get();
                mediaGalleryFragment.A04 = (AnonymousClass19O) r23.ACO.get();
                mediaGalleryFragment.A02 = (AnonymousClass12H) r23.AC5.get();
            }
        } else if (!(this instanceof Hilt_GalleryRecentsFragment)) {
            if (this instanceof Hilt_CameraMediaPickerFragment) {
                Hilt_CameraMediaPickerFragment hilt_CameraMediaPickerFragment2 = (Hilt_CameraMediaPickerFragment) this;
                if (!hilt_CameraMediaPickerFragment2.A02) {
                    hilt_CameraMediaPickerFragment2.A02 = true;
                    r32 = (AbstractC51092Su) hilt_CameraMediaPickerFragment2.generatedComponent();
                    hilt_CameraMediaPickerFragment = hilt_CameraMediaPickerFragment2;
                } else {
                    return;
                }
            } else if (!this.A02) {
                this.A02 = true;
                r32 = (AbstractC51092Su) generatedComponent();
                hilt_CameraMediaPickerFragment = (MediaGalleryFragmentBase) this;
            } else {
                return;
            }
            C51112Sw r36 = (C51112Sw) r32;
            AnonymousClass01J r24 = r36.A0Y;
            ((WaFragment) hilt_CameraMediaPickerFragment).A00 = (AnonymousClass182) r24.A94.get();
            ((WaFragment) hilt_CameraMediaPickerFragment).A01 = (AnonymousClass180) r24.ALt.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A09 = (C18720su) r24.A2c.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0F = (C14850m9) r24.A04.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0B = (C16590pI) r24.AMg.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0N = (AbstractC14440lR) r24.ANe.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0A = (AnonymousClass01d) r24.ALI.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0D = (AbstractC51202Tg) r36.A0U.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0C = (C15890o4) r24.AN1.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A07 = (C14900mE) r24.A8X.get();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0K = r36.A0V.A06();
            ((MediaGalleryFragmentBase) hilt_CameraMediaPickerFragment).A0E = (AnonymousClass018) r24.ANb.get();
        } else {
            Hilt_GalleryRecentsFragment hilt_GalleryRecentsFragment = (Hilt_GalleryRecentsFragment) this;
            if (!hilt_GalleryRecentsFragment.A02) {
                hilt_GalleryRecentsFragment.A02 = true;
                GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) hilt_GalleryRecentsFragment;
                C51112Sw r37 = (C51112Sw) ((AbstractC51092Su) hilt_GalleryRecentsFragment.generatedComponent());
                AnonymousClass01J r25 = r37.A0Y;
                ((WaFragment) galleryRecentsFragment).A00 = (AnonymousClass182) r25.A94.get();
                ((WaFragment) galleryRecentsFragment).A01 = (AnonymousClass180) r25.ALt.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A09 = (C18720su) r25.A2c.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0F = (C14850m9) r25.A04.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0B = (C16590pI) r25.AMg.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0N = (AbstractC14440lR) r25.ANe.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0A = (AnonymousClass01d) r25.ALI.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0D = (AbstractC51202Tg) r37.A0U.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0C = (C15890o4) r25.AN1.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A07 = (C14900mE) r25.A8X.get();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0K = r37.A0V.A06();
                ((MediaGalleryFragmentBase) galleryRecentsFragment).A0E = (AnonymousClass018) r25.ANb.get();
                galleryRecentsFragment.A02 = (C19390u2) r25.AFY.get();
            }
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
