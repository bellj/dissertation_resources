package com.whatsapp.gallery;

import X.AbstractActivityC58472pX;
import X.AbstractC009504t;
import X.AbstractC05270Ox;
import X.AbstractC13890kV;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC35481hz;
import X.AbstractC35511i9;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01T;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02Q;
import X.AnonymousClass109;
import X.AnonymousClass11G;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.AnonymousClass13H;
import X.AnonymousClass193;
import X.AnonymousClass19I;
import X.AnonymousClass19J;
import X.AnonymousClass19M;
import X.AnonymousClass1A6;
import X.AnonymousClass1AB;
import X.AnonymousClass1CY;
import X.AnonymousClass1IS;
import X.AnonymousClass1X3;
import X.AnonymousClass23N;
import X.AnonymousClass2GE;
import X.AnonymousClass2z4;
import X.AnonymousClass3AJ;
import X.AnonymousClass3DI;
import X.AnonymousClass3MQ;
import X.AnonymousClass3TX;
import X.AnonymousClass3UM;
import X.AnonymousClass3UN;
import X.AnonymousClass4ZH;
import X.AnonymousClass52T;
import X.AnonymousClass55O;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15240mn;
import X.C15250mo;
import X.C15380n4;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C18850tA;
import X.C20050v8;
import X.C20710wC;
import X.C22100yW;
import X.C22180yf;
import X.C22370yy;
import X.C22700zV;
import X.C23000zz;
import X.C231510o;
import X.C239613r;
import X.C242114q;
import X.C252018m;
import X.C252718t;
import X.C253018w;
import X.C253218y;
import X.C255419u;
import X.C28141Kv;
import X.C28391Mz;
import X.C32731ce;
import X.C35451ht;
import X.C38211ni;
import X.C41691tw;
import X.C45041zy;
import X.C53072cg;
import X.C53732f8;
import X.C54742hD;
import X.C64533Fx;
import X.C64993Hs;
import X.C66793Oy;
import X.C88054Ec;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class MediaGalleryActivity extends AbstractActivityC58472pX implements AbstractC13890kV {
    public int A00;
    public int A01 = 1;
    public int A02 = 2;
    public int A03 = 0;
    public MenuItem A04;
    public AnonymousClass02Q A05;
    public AbstractC009504t A06;
    public C239613r A07;
    public C16170oZ A08;
    public AnonymousClass12T A09;
    public C18850tA A0A;
    public C15550nR A0B;
    public C22700zV A0C;
    public C15610nY A0D;
    public AnonymousClass19I A0E;
    public C35451ht A0F;
    public AnonymousClass1A6 A0G;
    public C255419u A0H;
    public C15410nB A0I;
    public C15890o4 A0J;
    public C15650ng A0K;
    public C15240mn A0L;
    public C15250mo A0M = new C15250mo(((ActivityC13830kP) this).A01);
    public C15600nX A0N;
    public C253218y A0O;
    public C20050v8 A0P;
    public AnonymousClass12H A0Q;
    public C242114q A0R;
    public C22100yW A0S;
    public C22180yf A0T;
    public C231510o A0U;
    public AnonymousClass193 A0V;
    public C16120oU A0W;
    public C20710wC A0X;
    public AnonymousClass11G A0Y;
    public AbstractC14640lm A0Z;
    public AnonymousClass109 A0a;
    public C22370yy A0b;
    public AnonymousClass13H A0c;
    public AnonymousClass19J A0d;
    public C16630pM A0e;
    public C88054Ec A0f;
    public AnonymousClass12F A0g;
    public C253018w A0h;
    public AnonymousClass12U A0i;
    public AnonymousClass1AB A0j;
    public C252018m A0k;
    public C23000zz A0l;
    public AnonymousClass1CY A0m;
    public AnonymousClass01H A0n;
    public String A0o = "";
    public ArrayList A0p;
    public final AbstractC05270Ox A0q = new C54742hD(this);

    @Override // X.AbstractC13890kV
    public /* synthetic */ void A5t(AnonymousClass1IS r1) {
    }

    @Override // X.AbstractC13890kV
    public void A5u(Drawable drawable, View view) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void A8w(AnonymousClass1IS r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AAC(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AUy(AbstractC15340mz r1, boolean z) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AbN(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void Ach(AbstractC15340mz r1) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void Ad0(AbstractC15340mz r1, int i) {
    }

    @Override // X.AbstractC13890kV
    public boolean AdM(AnonymousClass1IS r2) {
        return true;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AfW(AnonymousClass1X3 r1, long j) {
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AfZ(AbstractC15340mz r1) {
    }

    public static /* synthetic */ AbstractC35481hz A02(MediaGalleryActivity mediaGalleryActivity) {
        int i = mediaGalleryActivity.A00;
        Iterator A0o = ActivityC13810kN.A0o(mediaGalleryActivity);
        while (A0o.hasNext()) {
            AnonymousClass01E r1 = (AnonymousClass01E) A0o.next();
            if ((i == mediaGalleryActivity.A03 && (r1 instanceof MediaGalleryFragment)) || ((i == mediaGalleryActivity.A01 && (r1 instanceof DocumentsGalleryFragment)) || (i == mediaGalleryActivity.A02 && (r1 instanceof LinksGalleryFragment)))) {
                return (AbstractC35481hz) r1;
            }
        }
        return null;
    }

    public final void A2e() {
        C35451ht r1;
        AbstractC009504t r2 = this.A06;
        if (r2 != null && (r1 = this.A0F) != null) {
            if (r1.A04.isEmpty()) {
                r2.A05();
                return;
            }
            AnonymousClass01d r8 = ((ActivityC13810kN) this).A08;
            AnonymousClass018 r7 = ((ActivityC13830kP) this).A01;
            HashMap hashMap = r1.A04;
            long size = (long) hashMap.size();
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, hashMap.size(), 0);
            AnonymousClass23N.A00(this, r8, r7.A0I(A1b, R.plurals.n_items_selected, size));
            this.A06.A06();
        }
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ AnonymousClass3DI AAl() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int ABe() {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public C64533Fx ABj() {
        return this.A0E.A01;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int ACM(AnonymousClass1X3 r2) {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ PollVoterViewModel AFm() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public ArrayList AGT() {
        return this.A0p;
    }

    @Override // X.AbstractC13900kW
    public /* synthetic */ AnonymousClass1AB AGx() {
        return null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int AH8(AbstractC15340mz r2) {
        return 0;
    }

    @Override // X.AbstractC13890kV
    public boolean AIM() {
        return C12960it.A1W(this.A0F);
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJl() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public boolean AJm(AbstractC15340mz r3) {
        C35451ht r0 = this.A0F;
        if (r0 != null) {
            if (r0.A04.containsKey(r3.A0z)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJv() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AKC(AbstractC15340mz r2) {
        return false;
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        if (C28391Mz.A03()) {
            C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        } else {
            C41691tw.A02(this, R.color.neutral_primary_dark);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r3) {
        super.AXD(r3);
        C41691tw.A07(getWindow(), false);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.AbstractC13890kV
    public void Acq(List list, boolean z) {
        if (this.A0F != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC15340mz A0f = C12980iv.A0f(it);
                C35451ht r0 = this.A0F;
                AnonymousClass1IS r1 = A0f.A0z;
                HashMap hashMap = r0.A04;
                if (z) {
                    hashMap.put(r1, A0f);
                } else {
                    hashMap.remove(r1);
                }
            }
            A2e();
        }
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AdX() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean Adn() {
        return false;
    }

    @Override // X.AbstractC13890kV
    public void AeE(AbstractC15340mz r9) {
        C14900mE r4 = ((ActivityC13810kN) this).A05;
        AnonymousClass12H r3 = this.A0Q;
        C35451ht r0 = new C35451ht(r4, new AnonymousClass55O(this), this.A0F, r3);
        this.A0F = r0;
        r0.A04.put(r9.A0z, r9);
        this.A06 = A1W(this.A05);
        AnonymousClass01d r7 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r6 = ((ActivityC13830kP) this).A01;
        C35451ht r42 = this.A0F;
        long size = (long) r42.A04.size();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1O(A1b, r42.A04.size());
        AnonymousClass23N.A00(this, r7, r6.A0I(A1b, R.plurals.n_items_selected, size));
    }

    @Override // X.AbstractC13890kV
    public boolean Af1(AbstractC15340mz r4) {
        C35451ht r0 = this.A0F;
        if (r0 == null) {
            return false;
        }
        AnonymousClass1IS r2 = r4.A0z;
        boolean containsKey = r0.A04.containsKey(r2);
        HashMap hashMap = this.A0F.A04;
        if (containsKey) {
            hashMap.remove(r2);
        } else {
            hashMap.put(r2, r4);
        }
        A2e();
        return !containsKey;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 2) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            if (this.A0F != null) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                C32731ce r6 = null;
                if (AnonymousClass4ZH.A00(((ActivityC13810kN) this).A0C, A07)) {
                    AnonymousClass009.A05(intent);
                    r6 = (C32731ce) intent.getParcelableExtra("status_distribution");
                }
                ArrayList A0x = C12980iv.A0x(this.A0F.A04.values());
                Collections.sort(A0x, new C45041zy());
                Iterator it = A0x.iterator();
                while (it.hasNext()) {
                    this.A08.A06(this.A07, r6, C12980iv.A0f(it), A07);
                }
                if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                    A2a(A07);
                } else {
                    ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, this.A0B.A0B((AbstractC14640lm) A07.get(0))));
                }
            } else {
                Log.w("mediagallery/forward/failed");
                ((ActivityC13810kN) this).A05.A07(R.string.message_forward_failed, 0);
            }
            AbstractC009504t r0 = this.A06;
            if (r0 != null) {
                r0.A05();
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A0E.A00(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        List<AnonymousClass1IS> A04;
        super.onCreate(bundle);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        C252718t r04 = ((ActivityC13790kL) this).A0D;
        AnonymousClass13H r05 = this.A0c;
        C253018w r06 = this.A0h;
        AbstractC15710nm r07 = ((ActivityC13810kN) this).A03;
        C15570nT r08 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r2 = ((ActivityC13830kP) this).A05;
        AnonymousClass12U r09 = this.A0i;
        C16120oU r010 = this.A0W;
        AnonymousClass19M r011 = ((ActivityC13810kN) this).A0B;
        C15450nH r012 = ((ActivityC13810kN) this).A06;
        C18850tA r013 = this.A0A;
        C16170oZ r014 = this.A08;
        C231510o r015 = this.A0U;
        C88054Ec r016 = this.A0f;
        AnonymousClass12P r017 = ((ActivityC13790kL) this).A00;
        C15550nR r018 = this.A0B;
        C22180yf r019 = this.A0T;
        AnonymousClass01d r020 = ((ActivityC13810kN) this).A08;
        C15610nY r021 = this.A0D;
        AnonymousClass018 r022 = ((ActivityC13830kP) this).A01;
        C20710wC r023 = this.A0X;
        AnonymousClass12F r024 = this.A0g;
        C253218y r025 = this.A0O;
        AnonymousClass12T r026 = this.A09;
        AnonymousClass193 r15 = this.A0V;
        C242114q r14 = this.A0R;
        C23000zz r13 = this.A0l;
        C22700zV r12 = this.A0C;
        C14820m6 r11 = ((ActivityC13810kN) this).A09;
        C22370yy r10 = this.A0b;
        C22100yW r9 = this.A0S;
        this.A05 = new AnonymousClass2z4(r017, r07, r03, r08, r012, r014, this, r026, r013, r018, r12, r021, this.A0G, this.A0H, r020, r0, r11, r022, this.A0N, r025, r14, r9, r019, r011, r015, r15, r02, r010, this, r023, this.A0a, r10, r05, this.A0e, r016, r024, r06, r09, r13, r04, r2, this.A0n);
        r2.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0I, 37));
        setTitle(R.string.all_media);
        setContentView(R.layout.media_gallery);
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        C12970iu.A0N(this).A0M(true);
        if (C28391Mz.A02()) {
            C12970iu.A1N(this, R.id.separator, 8);
        }
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        AbstractC14640lm A0c = C12970iu.A0c(this);
        AnonymousClass009.A05(A0c);
        this.A0Z = A0c;
        A2O(this.A0D.A04(this.A0B.A0B(A0c)));
        if (getIntent().getBooleanExtra("alert", false)) {
            this.A0m.A01(this);
        }
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.A02 = -1;
        C53732f8 r122 = new C53732f8(A0V());
        ArrayList A0l = C12960it.A0l();
        C12970iu.A1T(Integer.valueOf((int) R.string.gallery_tab_media), new MediaGalleryFragment(), A0l);
        C12970iu.A1T(Integer.valueOf((int) R.string.gallery_tab_documents), new DocumentsGalleryFragment(), A0l);
        if (this.A0P.A04.A01("links_ready", 0) != 0) {
            C12970iu.A1T(Integer.valueOf((int) R.string.gallery_tab_links), new LinksGalleryFragment(), A0l);
        }
        if (C28141Kv.A00(((ActivityC13830kP) this).A01)) {
            Collections.reverse(A0l);
        }
        for (int i = 0; i < A0l.size(); i++) {
            AnonymousClass01T r027 = (AnonymousClass01T) A0l.get(i);
            Number number = (Number) r027.A00;
            Object obj = r027.A01;
            int intValue = number.intValue();
            String string = getString(intValue);
            r122.A01.add(obj);
            r122.A00.add(string);
            if (intValue == R.string.gallery_tab_media) {
                this.A03 = i;
            } else if (number.intValue() == R.string.gallery_tab_documents) {
                this.A01 = i;
            } else if (number.intValue() == R.string.gallery_tab_links) {
                this.A02 = i;
            }
        }
        viewPager.setAdapter(r122);
        List list = r122.A01;
        viewPager.setOffscreenPageLimit(list.size());
        viewPager.A0F(this.A03, false);
        this.A00 = this.A03;
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        AnonymousClass028.A0c(tabLayout, 0);
        if (list.size() > 1) {
            tabLayout.A0A(AnonymousClass00T.A00(this, R.color.mediaGalleryTabInactive), AnonymousClass00T.A00(this, R.color.mediaGalleryTabActive));
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setOnTabSelectedListener(new AnonymousClass3TX(viewPager, this));
        } else {
            ((C53072cg) A0Q.getLayoutParams()).A00 = 0;
            tabLayout.setVisibility(8);
        }
        if (!(bundle == null || (A04 = C38211ni.A04(bundle)) == null)) {
            for (AnonymousClass1IS r7 : A04) {
                AbstractC15340mz A03 = this.A0K.A0K.A03(r7);
                if (A03 != null) {
                    C35451ht r5 = this.A0F;
                    if (r5 == null) {
                        r5 = new C35451ht(((ActivityC13810kN) this).A05, new AnonymousClass55O(this), null, this.A0Q);
                        this.A0F = r5;
                    }
                    r5.A04.put(r7, A03);
                }
            }
            if (this.A0F != null) {
                this.A06 = A1W(this.A05);
            }
        }
    }

    @Override // android.app.Activity
    public void onCreate(Bundle bundle, PersistableBundle persistableBundle) {
        super.onCreate(bundle, persistableBundle);
        this.A0E.A00(this);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        AnonymousClass12P r5;
        C252018m r4;
        C14820m6 r3;
        boolean z;
        C14850m9 r1;
        AnonymousClass12P r52;
        C252018m r42;
        C14820m6 r32;
        boolean z2;
        C14850m9 r12;
        if (i != 13) {
            if (i != 19) {
                switch (i) {
                    case 23:
                        r12 = ((ActivityC13810kN) this).A0C;
                        r52 = ((ActivityC13790kL) this).A00;
                        r42 = this.A0k;
                        r32 = ((ActivityC13810kN) this).A09;
                        z2 = true;
                        break;
                    case 24:
                        r12 = ((ActivityC13810kN) this).A0C;
                        r52 = ((ActivityC13790kL) this).A00;
                        r42 = this.A0k;
                        r32 = ((ActivityC13810kN) this).A09;
                        z2 = false;
                        break;
                    case 25:
                        r1 = ((ActivityC13810kN) this).A0C;
                        r5 = ((ActivityC13790kL) this).A00;
                        r4 = this.A0k;
                        r3 = ((ActivityC13810kN) this).A09;
                        z = true;
                        break;
                    default:
                        return super.onCreateDialog(i);
                }
                return AnonymousClass3AJ.A00(this, r52, new AnonymousClass3UN(this, r32, i, r12.A07(1333)), r42, z2);
            }
            r1 = ((ActivityC13810kN) this).A0C;
            r5 = ((ActivityC13790kL) this).A00;
            r4 = this.A0k;
            r3 = ((ActivityC13810kN) this).A09;
            z = false;
            return AnonymousClass3AJ.A00(this, r5, new AnonymousClass3UM(this, r3, i, r1.A07(1333)), r4, z);
        }
        C35451ht r2 = this.A0F;
        if (r2 == null || r2.A04.isEmpty()) {
            Log.e("mediagallery/dialog/delete no messages");
            return super.onCreateDialog(i);
        }
        StringBuilder A0k = C12960it.A0k("mediagallery/dialog/delete/");
        A0k.append(r2.A04.size());
        C12960it.A1F(A0k);
        HashSet hashSet = new HashSet(this.A0F.A04.values());
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C14830m7 r03 = ((ActivityC13790kL) this).A05;
        AbstractC14440lR r04 = ((ActivityC13830kP) this).A05;
        AnonymousClass19M r15 = ((ActivityC13810kN) this).A0B;
        C16170oZ r14 = this.A08;
        C15550nR r10 = this.A0B;
        C15610nY r9 = this.A0D;
        AnonymousClass018 r8 = ((ActivityC13830kP) this).A01;
        C20710wC r7 = this.A0X;
        AnonymousClass11G r6 = this.A0Y;
        C14820m6 r53 = ((ActivityC13810kN) this).A09;
        C15600nX r43 = this.A0N;
        AnonymousClass19J r33 = this.A0d;
        AbstractC14640lm r13 = this.A0Z;
        return C64993Hs.A00(this, new AnonymousClass52T(this, 13), new AbstractC35511i9() { // from class: X.52W
            @Override // X.AbstractC35511i9
            public final void AOx() {
                MediaGalleryActivity mediaGalleryActivity = MediaGalleryActivity.this;
                C35451ht r05 = mediaGalleryActivity.A0F;
                if (r05 != null) {
                    r05.A04.clear();
                }
                AbstractC009504t r06 = mediaGalleryActivity.A06;
                if (r06 != null) {
                    r06.A05();
                }
            }
        }, r02, r14, r10, r9, null, r03, r53, r8, r43, r15, r0, r7, r6, r33, r04, C64993Hs.A01(this, r10, r9, r13, hashSet), hashSet, true);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        this.A0M = this.A0L.A09(this.A0Z);
        if (this.A0L.A0O()) {
            SearchView searchView = new SearchView(this);
            C12960it.A0s(this, C12960it.A0J(searchView, R.id.search_src_text), R.color.search_text_color);
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.A0B = new C66793Oy(this);
            boolean z = false;
            MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(AnonymousClass2GE.A01(this, R.drawable.ic_action_search_teal, R.color.lightActionBarItemDrawableTint));
            this.A04 = icon;
            icon.setActionView(searchView);
            this.A04.setShowAsAction(10);
            this.A04.setOnActionExpandListener(new AnonymousClass3MQ(this));
            MenuItem menuItem = this.A04;
            if (this.A00 != this.A03) {
                z = true;
            }
            menuItem.setVisible(z);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1AB r0 = this.A0j;
        if (r0 != null) {
            r0.A03();
        }
        C35451ht r02 = this.A0F;
        if (r02 != null) {
            r02.A00();
            this.A0F = null;
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0I, 37));
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        C35451ht r0 = this.A0F;
        if (r0 != null) {
            ArrayList A0l = C12960it.A0l();
            Iterator A0t = C12990iw.A0t(r0.A04);
            while (A0t.hasNext()) {
                A0l.add(C12980iv.A0f(A0t).A0z);
            }
            C38211ni.A09(bundle, A0l);
        }
    }
}
