package com.whatsapp.gallery;

import X.AbstractC35481hz;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass1BK;
import X.C12960it;
import X.C20050v8;
import X.C253218y;
import X.C616131a;
import X.C63563Cb;
import X.ExecutorC27271Gr;
import android.content.Context;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class LinksGalleryFragment extends Hilt_LinksGalleryFragment implements AbstractC35481hz {
    public AnonymousClass12P A00;
    public C63563Cb A01;
    public C253218y A02;
    public C20050v8 A03;
    public AnonymousClass1BK A04;
    public AnonymousClass19M A05;
    public AnonymousClass19O A06;

    @Override // com.whatsapp.gallery.GalleryFragmentBase, X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        C616131a r1 = new C616131a(this);
        ((GalleryFragmentBase) this).A0A = r1;
        ((GalleryFragmentBase) this).A02.setAdapter(r1);
        C12960it.A0J(A05(), R.id.empty_text).setText(R.string.no_urls_found);
    }

    @Override // com.whatsapp.gallery.Hilt_LinksGalleryFragment, com.whatsapp.gallery.Hilt_GalleryFragmentBase, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A01 = new C63563Cb(new ExecutorC27271Gr(((GalleryFragmentBase) this).A0E, false));
    }
}
