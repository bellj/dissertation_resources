package com.whatsapp.gallery;

import X.AbstractC15710nm;
import X.AbstractC35481hz;
import X.AnonymousClass12P;
import X.AnonymousClass1BB;
import X.AnonymousClass1CY;
import X.C12960it;
import X.C14900mE;
import X.C15450nH;
import X.C15670ni;
import X.C253218y;
import X.C616231b;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DocumentsGalleryFragment extends Hilt_DocumentsGalleryFragment implements AbstractC35481hz {
    public AnonymousClass12P A00;
    public AbstractC15710nm A01;
    public C14900mE A02;
    public C15450nH A03;
    public AnonymousClass1BB A04;
    public C253218y A05;
    public C15670ni A06;
    public AnonymousClass1CY A07;

    @Override // com.whatsapp.gallery.GalleryFragmentBase, X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        C616231b r1 = new C616231b(this);
        ((GalleryFragmentBase) this).A0A = r1;
        ((GalleryFragmentBase) this).A02.setAdapter(r1);
        C12960it.A0J(A05(), R.id.empty_text).setText(R.string.no_documents_found);
    }
}
