package com.whatsapp.gallery;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC18860tB;
import X.AbstractC35481hz;
import X.AbstractC54432gi;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02N;
import X.AnonymousClass12H;
import X.AnonymousClass12I;
import X.AnonymousClass1BB;
import X.AnonymousClass38L;
import X.C14830m7;
import X.C15240mn;
import X.C15250mo;
import X.C15650ng;
import X.C15890o4;
import X.C16310on;
import X.C16520pA;
import X.C20050v8;
import X.C32301bw;
import X.C32311bx;
import X.C32321by;
import X.C34971h0;
import X.C35321hd;
import X.C43951xu;
import X.C626538b;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public abstract class GalleryFragmentBase extends Hilt_GalleryFragmentBase implements AbstractC35481hz {
    public int A00 = -1;
    public View A01;
    public RecyclerView A02;
    public C14830m7 A03;
    public C15890o4 A04;
    public AnonymousClass018 A05;
    public C15650ng A06;
    public C15250mo A07 = new C15250mo(this.A05);
    public AnonymousClass12H A08;
    public AnonymousClass12I A09;
    public AbstractC54432gi A0A;
    public AnonymousClass38L A0B;
    public C626538b A0C;
    public AbstractC14640lm A0D;
    public AbstractC14440lR A0E;
    public String A0F = "";
    public final AbstractC18860tB A0G = new C35321hd(this);
    public final String A0H;
    public final ArrayList A0I = new ArrayList();

    public GalleryFragmentBase(String str) {
        this.A0H = str;
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        AbstractC14640lm A01 = AbstractC14640lm.A01(A0C().getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        this.A0D = A01;
        View A05 = A05();
        this.A01 = A05.findViewById(16908292);
        RecyclerView recyclerView = (RecyclerView) A05.findViewById(R.id.grid);
        this.A02 = recyclerView;
        AnonymousClass028.A0m(recyclerView, true);
        AnonymousClass028.A0m(super.A0A.findViewById(16908292), true);
        ActivityC000900k A0B = A0B();
        if (A0B instanceof MediaGalleryActivity) {
            this.A02.A0n(((MediaGalleryActivity) A0B).A0q);
        }
        this.A08.A03(this.A0G);
        View view = super.A0A;
        if (view != null) {
            view.findViewById(R.id.progress_bar).setVisibility(0);
        }
        A1B();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.gallery_fragment, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A08.A04(this.A0G);
        Cursor A0E = this.A0A.A0E(null);
        if (A0E != null) {
            A0E.close();
        }
        C626538b r0 = this.A0C;
        if (r0 != null) {
            r0.A08();
            this.A0C = null;
        }
        AnonymousClass38L r1 = this.A0B;
        if (r1 != null) {
            r1.A03(true);
            synchronized (r1) {
                AnonymousClass02N r02 = r1.A00;
                if (r02 != null) {
                    r02.A01();
                }
            }
            this.A0B = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        A1C();
    }

    public Cursor A1A(AnonymousClass02N r13, C15250mo r14, AbstractC14640lm r15) {
        C16310on r4;
        Cursor cursor;
        Cursor cursor2;
        if (!(this instanceof LinksGalleryFragment)) {
            DocumentsGalleryFragment documentsGalleryFragment = (DocumentsGalleryFragment) this;
            C15650ng r7 = ((GalleryFragmentBase) documentsGalleryFragment).A06;
            AnonymousClass1BB r9 = documentsGalleryFragment.A04;
            C15240mn r3 = r9.A01;
            long A04 = r3.A04();
            C16310on r42 = r9.A02.get();
            try {
                r14.A01();
                if (!(!r14.A02().isEmpty())) {
                    cursor2 = r42.A03.A07(r13, C32301bw.A06, new String[]{String.valueOf(r9.A00.A02(r15))});
                } else if (A04 == 1) {
                    cursor2 = r42.A03.A07(r13, C34971h0.A00, new String[]{r3.A0G(r14.A01()), String.valueOf(r9.A00.A02(r15))});
                } else {
                    boolean z = false;
                    if (A04 == 5) {
                        z = true;
                    }
                    AnonymousClass009.A0A("unknown fts version", z);
                    r14.A02 = 100;
                    cursor2 = r42.A03.A07(r13, C34971h0.A05, new String[]{r3.A0B(r13, r14, null)});
                }
                r42.close();
                return new C16520pA(cursor2, r7, r15, false);
            } finally {
            }
        } else {
            C20050v8 r43 = ((LinksGalleryFragment) this).A03;
            boolean z2 = false;
            if (r43.A04.A01("links_ready", 0) == 2) {
                z2 = true;
            }
            if (z2) {
                C15240mn r32 = r43.A02;
                long A042 = r32.A04();
                String l = Long.toString(r43.A01.A02(r15));
                r4 = r43.A03.get();
                try {
                    if (!r14.A02().isEmpty()) {
                        r14.A01();
                        if (A042 == 1) {
                            cursor = r4.A03.A07(r13, C34971h0.A03, new String[]{l, r32.A0G(r14.A01())});
                        } else {
                            r14.A02 = C43951xu.A03;
                            cursor = r4.A03.A07(r13, C34971h0.A04, new String[]{r32.A0B(r13, r14, null)});
                        }
                    } else {
                        cursor = r4.A03.A07(r13, C32311bx.A01, new String[]{l});
                    }
                } finally {
                }
            } else {
                String rawString = r15.getRawString();
                C15240mn r8 = r43.A02;
                long A043 = r8.A04();
                r4 = r43.A03.get();
                try {
                    if (!r14.A02().isEmpty()) {
                        String A01 = r14.A01();
                        String str = null;
                        if (A043 == 1) {
                            if (!TextUtils.isEmpty(A01)) {
                                str = r8.A0G(A01);
                            }
                            cursor = r4.A03.A07(r13, C34971h0.A01, new String[]{rawString, str});
                        } else {
                            r14.A02 = C43951xu.A03;
                            cursor = r4.A03.A07(r13, C34971h0.A02, new String[]{r8.A0B(r13, r14, null)});
                        }
                    } else {
                        cursor = r4.A03.A07(r13, C32321by.A00, new String[]{rawString});
                    }
                } finally {
                    try {
                        r4.close();
                    } catch (Throwable unused) {
                    }
                }
            }
            r4.close();
            return cursor;
        }
    }

    public final void A1B() {
        AnonymousClass38L r1 = this.A0B;
        if (r1 != null) {
            r1.A03(true);
            synchronized (r1) {
                AnonymousClass02N r0 = r1.A00;
                if (r0 != null) {
                    r0.A01();
                }
            }
        }
        C626538b r02 = this.A0C;
        if (r02 != null) {
            r02.A08();
        }
        AnonymousClass38L r2 = new AnonymousClass38L(this.A07, this, this.A0D);
        this.A0B = r2;
        this.A0E.Aaz(r2, new Void[0]);
    }

    public final void A1C() {
        if (this.A00 == -1) {
            return;
        }
        if (!this.A04.A07() || this.A00 <= 0) {
            this.A01.setVisibility(0);
            this.A02.setVisibility(8);
            return;
        }
        this.A01.setVisibility(8);
        this.A02.setVisibility(0);
    }

    @Override // X.AbstractC35481hz
    public void AVc(C15250mo r3) {
        if (!TextUtils.equals(this.A0F, r3.A01())) {
            this.A0F = r3.A01();
            this.A07 = r3;
            A1B();
        }
    }

    @Override // X.AbstractC35481hz
    public void AVm() {
        this.A0A.A02();
    }
}
