package com.whatsapp.gallery;

import X.AbstractC009504t;
import X.AbstractC35611iN;
import X.AnonymousClass02M;
import X.AnonymousClass2T3;
import X.C12960it;
import X.C12980iv;
import X.C16700pc;
import X.C457522x;
import X.C50992Sh;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.MediaPickerFragment;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public final class NewMediaPickerFragment extends Hilt_NewMediaPickerFragment {
    public LayoutInflater A00;
    public Menu A01;
    public View A02;
    public ViewGroup A03;
    public RecyclerView A04;
    public final Set A05 = new LinkedHashSet();

    public NewMediaPickerFragment() {
        ((MediaGalleryFragmentBase) this).A0Q = true;
    }

    @Override // com.whatsapp.gallerypicker.MediaPickerFragment, X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        C16700pc.A0F(menu, menuInflater);
        super.A0y(menu, menuInflater);
        this.A01 = menu;
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        this.A00 = layoutInflater;
        View A10 = super.A10(bundle, layoutInflater, viewGroup);
        if (A10 == null) {
            return null;
        }
        ViewGroup viewGroup2 = (ViewGroup) C16700pc.A03(A10, R.id.root);
        viewGroup2.addView(layoutInflater.inflate(R.layout.gallery_multi_selected_thumbs_view, viewGroup2, false));
        return A10;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        View view = this.A02;
        if (view != null) {
            view.setOnClickListener(null);
        }
        this.A02 = null;
        RecyclerView recyclerView = this.A04;
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        this.A04 = null;
        this.A03 = null;
    }

    @Override // com.whatsapp.gallerypicker.MediaPickerFragment, com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A13() {
        super.A13();
        A1P();
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        super.A17(bundle, view);
        this.A03 = C12980iv.A0P(view, R.id.gallery_selected_container);
        C16700pc.A0B(view.getContext());
        RecyclerView recyclerView = (RecyclerView) C16700pc.A03(view, R.id.gallery_selected_media);
        this.A04 = recyclerView;
        recyclerView.A0h = true;
        LayoutInflater layoutInflater = this.A00;
        if (layoutInflater == null) {
            throw C16700pc.A06("inflater");
        }
        C457522x r1 = ((MediaGalleryFragmentBase) this).A0L;
        C16700pc.A0B(r1);
        recyclerView.setAdapter(new C50992Sh(layoutInflater, r1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(0);
        recyclerView.setLayoutManager(linearLayoutManager);
        View A03 = C16700pc.A03(view, R.id.gallery_done_btn);
        this.A02 = A03;
        C12960it.A0x(A03, this, 20);
    }

    @Override // com.whatsapp.gallerypicker.MediaPickerFragment, com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r3, AnonymousClass2T3 r4) {
        Menu menu;
        Menu menu2;
        C16700pc.A0I(r3, r4);
        if (!A1J() && (menu = this.A01) != null && menu.size() > 0 && (menu2 = this.A01) != null) {
            MenuItem item = menu2.getItem(0);
            C16700pc.A0B(item);
            A0z(item);
        }
        return super.A1L(r3, r4);
    }

    @Override // com.whatsapp.gallerypicker.MediaPickerFragment
    public void A1M() {
        super.A1M();
        this.A05.clear();
        A1P();
    }

    @Override // com.whatsapp.gallerypicker.MediaPickerFragment
    public void A1N(AbstractC35611iN r4) {
        ViewGroup viewGroup;
        AbstractC009504t r0;
        C50992Sh r1;
        if (r4 != null) {
            super.A1N(r4);
            boolean A1J = A1J();
            Set set = this.A05;
            if (A1J) {
                if (!set.remove(r4)) {
                    set.add(r4);
                }
                int A02 = C12960it.A02(!set.isEmpty());
                ViewGroup viewGroup2 = this.A03;
                if ((viewGroup2 == null || viewGroup2.getVisibility() != A02) && (viewGroup = this.A03) != null) {
                    viewGroup.setVisibility(A02);
                }
                RecyclerView recyclerView = this.A04;
                AnonymousClass02M r12 = null;
                if (recyclerView != null) {
                    r12 = recyclerView.A0N;
                }
                if ((r12 instanceof C50992Sh) && (r1 = (C50992Sh) r12) != null) {
                    List list = r1.A02;
                    list.clear();
                    list.addAll(set);
                    r1.A02();
                }
                if (set.isEmpty() && (r0 = ((MediaPickerFragment) this).A05) != null) {
                    r0.A05();
                    return;
                }
                return;
            }
            set.add(r4);
        }
    }

    public final void A1P() {
        ViewGroup viewGroup;
        C50992Sh r1;
        if (C12980iv.A0x(((MediaPickerFragment) this).A0D.A00.values()).isEmpty()) {
            this.A05.clear();
        }
        Set set = this.A05;
        int A02 = C12960it.A02(!set.isEmpty());
        ViewGroup viewGroup2 = this.A03;
        if ((viewGroup2 == null || viewGroup2.getVisibility() != A02) && (viewGroup = this.A03) != null) {
            viewGroup.setVisibility(A02);
        }
        RecyclerView recyclerView = this.A04;
        AnonymousClass02M r12 = null;
        if (recyclerView != null) {
            r12 = recyclerView.A0N;
        }
        if ((r12 instanceof C50992Sh) && (r1 = (C50992Sh) r12) != null) {
            List list = r1.A02;
            list.clear();
            list.addAll(set);
            r1.A02();
        }
    }
}
