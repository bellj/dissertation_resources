package com.whatsapp.gallery;

import X.AbstractC13890kV;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC35581iK;
import X.AbstractC35601iM;
import X.AbstractC35611iN;
import X.AbstractC42671vd;
import X.AbstractC454421p;
import X.AbstractC51202Tg;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass02M;
import X.AnonymousClass1CY;
import X.AnonymousClass1IS;
import X.AnonymousClass23N;
import X.AnonymousClass286;
import X.AnonymousClass2FO;
import X.AnonymousClass2T3;
import X.AnonymousClass2TD;
import X.AnonymousClass2TF;
import X.AnonymousClass2TH;
import X.AnonymousClass2TJ;
import X.AnonymousClass2TK;
import X.AnonymousClass2TM;
import X.AnonymousClass2TP;
import X.AnonymousClass2TS;
import X.AnonymousClass2TT;
import X.C14850m9;
import X.C14900mE;
import X.C15670ni;
import X.C15890o4;
import X.C16440p1;
import X.C16590pI;
import X.C16700pc;
import X.C18720su;
import X.C26511Dt;
import X.C35591iL;
import X.C457522x;
import X.C51162Tc;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.StickyHeadersRecyclerView;
import com.whatsapp.camera.bottomsheet.CameraMediaPickerFragment;
import com.whatsapp.gallery.MediaGalleryFragment;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.gallerypicker.MediaPickerFragment;
import com.whatsapp.scroller.RecyclerFastScroller;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public abstract class MediaGalleryFragmentBase extends Hilt_MediaGalleryFragmentBase {
    public static final Bitmap A0U = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public Drawable A04;
    public View A05;
    public AnonymousClass02M A06;
    public C14900mE A07;
    public StickyHeadersRecyclerView A08;
    public C18720su A09;
    public AnonymousClass01d A0A;
    public C16590pI A0B;
    public C15890o4 A0C;
    public AbstractC51202Tg A0D;
    public AnonymousClass018 A0E;
    public C14850m9 A0F;
    public AnonymousClass2TH A0G;
    public AbstractC35581iK A0H;
    public AnonymousClass2TF A0I;
    public AnonymousClass286 A0J;
    public AnonymousClass2FO A0K;
    public C457522x A0L;
    public RecyclerFastScroller A0M;
    public AbstractC14440lR A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q = false;
    public final ContentObserver A0R;
    public final Handler A0S;
    public final List A0T = new ArrayList();

    public abstract boolean A1L(AbstractC35611iN v, AnonymousClass2T3 v2);

    public MediaGalleryFragmentBase() {
        Handler handler = new Handler(Looper.getMainLooper());
        this.A0S = handler;
        this.A0R = new AnonymousClass2TD(handler, this);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putInt("sort_type", this.A02);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.image_gallery, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Log.i("mediagalleryfragmentbase/destroy");
        super.A11();
        A1D();
        this.A0O = false;
        C457522x r0 = this.A0L;
        if (r0 != null) {
            r0.A00();
            this.A0L = null;
        }
        AbstractC35581iK r1 = this.A0H;
        if (r1 != null) {
            r1.unregisterContentObserver(this.A0R);
            this.A0H.close();
            this.A0H = null;
        }
        this.A06 = null;
        this.A00 = 0;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        A1C();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00cf, code lost:
        if (r2 == 1) goto L_0x00d1;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r6, android.view.View r7) {
        /*
            r5 = this;
            java.lang.String r0 = "sort_type"
            r4 = 0
            if (r6 != 0) goto L_0x000a
            android.os.Bundle r6 = r5.A05
            if (r6 == 0) goto L_0x0010
        L_0x000a:
            int r0 = r6.getInt(r0, r4)
            r5.A02 = r0
        L_0x0010:
            android.content.Context r1 = r5.A01()
            r0 = 2131100212(0x7f060234, float:1.78128E38)
            int r1 = X.AnonymousClass00T.A00(r1, r0)
            r5.A01 = r1
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r5.A04 = r0
            android.content.res.Resources r1 = r5.A02()
            r0 = 2131166062(0x7f07036e, float:1.7946359E38)
            int r0 = r1.getDimensionPixelSize(r0)
            r5.A03 = r0
            boolean r0 = r5 instanceof com.whatsapp.storage.StorageUsageMediaGalleryFragment
            r5.A0P = r0
            r0 = 2131364609(0x7f0a0b01, float:1.834906E38)
            android.view.View r0 = r7.findViewById(r0)
            r5.A05 = r0
            r0 = 2131363780(0x7f0a07c4, float:1.8347378E38)
            android.view.View r0 = r7.findViewById(r0)
            com.whatsapp.StickyHeadersRecyclerView r0 = (com.whatsapp.StickyHeadersRecyclerView) r0
            r5.A08 = r0
            X.2TW r1 = new X.2TW
            r1.<init>(r5, r5)
            r5.A06 = r1
            com.whatsapp.StickyHeadersRecyclerView r0 = r5.A08
            r0.setAdapter(r1)
            r0 = 2131365668(0x7f0a0f24, float:1.8351208E38)
            android.view.View r1 = X.AnonymousClass028.A0D(r7, r0)
            com.whatsapp.scroller.RecyclerFastScroller r1 = (com.whatsapp.scroller.RecyclerFastScroller) r1
            r5.A0M = r1
            X.018 r0 = r5.A0E
            X.1Kv r0 = r0.A04()
            boolean r0 = r0.A06
            r1.A0C = r0
            com.whatsapp.scroller.RecyclerFastScroller r1 = r5.A0M
            com.whatsapp.StickyHeadersRecyclerView r0 = r5.A08
            r1.setRecyclerView(r0)
            android.content.Context r0 = r5.A0p()
            android.widget.ImageView r3 = new android.widget.ImageView
            r3.<init>(r0)
            X.018 r2 = r5.A0E
            android.content.Context r1 = r5.A01()
            r0 = 2131231518(0x7f08031e, float:1.807912E38)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r1, r0)
            X.2GF r0 = new X.2GF
            r0.<init>(r1, r2)
            r3.setImageDrawable(r0)
            com.whatsapp.scroller.RecyclerFastScroller r0 = r5.A0M
            r0.setThumbView(r3)
            X.00k r0 = r5.A0C()
            android.view.LayoutInflater r2 = r0.getLayoutInflater()
            r1 = 2131559264(0x7f0d0360, float:1.8743867E38)
            com.whatsapp.StickyHeadersRecyclerView r0 = r5.A08
            android.view.View r4 = r2.inflate(r1, r0, r4)
            r0 = 2131363519(0x7f0a06bf, float:1.834685E38)
            android.view.View r3 = r4.findViewById(r0)
            android.widget.TextView r3 = (android.widget.TextView) r3
            X.C27531Hw.A06(r3)
            X.2TY r1 = new X.2TY
            r1.<init>()
            r0 = 0
            X.01e r2 = new X.01e
            r2.<init>(r0, r1)
            com.whatsapp.scroller.RecyclerFastScroller r1 = r5.A0M
            X.2TZ r0 = new X.2TZ
            r0.<init>(r3, r5, r2)
            r1.setBubbleView(r4, r0)
            com.whatsapp.scroller.RecyclerFastScroller r3 = r5.A0M
            int r2 = r5.A02
            if (r2 == 0) goto L_0x00d1
            r1 = 1
            r0 = 8
            if (r2 != r1) goto L_0x00d2
        L_0x00d1:
            r0 = 0
        L_0x00d2:
            r3.setVisibility(r0)
            X.0su r4 = r5.A09
            android.content.Context r0 = r5.A01()
            android.content.ContentResolver r3 = r0.getContentResolver()
            android.os.Looper r0 = android.os.Looper.getMainLooper()
            android.os.Handler r2 = new android.os.Handler
            r2.<init>(r0)
            java.lang.String r1 = "media-gallery-fragment"
            X.22x r0 = new X.22x
            r0.<init>(r3, r2, r4, r1)
            r5.A0L = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallery.MediaGalleryFragmentBase.A17(android.os.Bundle, android.view.View):void");
    }

    public AnonymousClass2T3 A1A(Uri uri) {
        if (uri == null) {
            return null;
        }
        int childCount = this.A08.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.A08.getChildAt(i);
            if (childAt instanceof AnonymousClass2T3) {
                AnonymousClass2T3 r1 = (AnonymousClass2T3) childAt;
                if (uri.equals(r1.getUri())) {
                    return r1;
                }
            }
        }
        return null;
    }

    public AnonymousClass2TK A1B() {
        if (this instanceof StorageUsageMediaGalleryFragment) {
            return new AnonymousClass2TK() { // from class: X.2TN
                @Override // X.AnonymousClass2TK
                public final AbstractC35581iK A8O(boolean z) {
                    StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = StorageUsageMediaGalleryFragment.this;
                    AnonymousClass440 r5 = new AnonymousClass440(storageUsageMediaGalleryFragment.A04, storageUsageMediaGalleryFragment.A05, storageUsageMediaGalleryFragment.A08, storageUsageMediaGalleryFragment.A0A, ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A02, storageUsageMediaGalleryFragment.A00);
                    if (((C35591iL) r5).A01 == null) {
                        ((C35591iL) r5).A01 = new C16520pA(r5.A00(), r5.A02, r5.A04, false);
                    }
                    return r5;
                }
            };
        }
        if (this instanceof MediaPickerFragment) {
            MediaPickerFragment mediaPickerFragment = (MediaPickerFragment) this;
            ActivityC000900k A0B = mediaPickerFragment.A0B();
            if (A0B == null) {
                return null;
            }
            return new AnonymousClass2TM(A0B.getIntent().getData(), ((MediaGalleryFragmentBase) mediaPickerFragment).A0B, ((MediaGalleryFragmentBase) mediaPickerFragment).A0K, mediaPickerFragment.A08, mediaPickerFragment.A00);
        } else if (this instanceof MediaGalleryFragment) {
            return new AnonymousClass2TK() { // from class: X.2TL
                @Override // X.AnonymousClass2TK
                public final AbstractC35581iK A8O(boolean z) {
                    MediaGalleryFragment mediaGalleryFragment = MediaGalleryFragment.this;
                    C35591iL r5 = new C35591iL(mediaGalleryFragment.A00, mediaGalleryFragment.A01, mediaGalleryFragment.A03, mediaGalleryFragment.A04);
                    if (r5.A01 == null) {
                        r5.A01 = new C16520pA(r5.A00(), r5.A02, r5.A04, false);
                    }
                    return r5;
                }
            };
        } else {
            CameraMediaPickerFragment cameraMediaPickerFragment = (CameraMediaPickerFragment) this;
            return new AnonymousClass2TJ(((MediaGalleryFragmentBase) cameraMediaPickerFragment).A0K, cameraMediaPickerFragment.A06);
        }
    }

    public void A1C() {
        if (this.A0H == null) {
            return;
        }
        if (!this.A0C.A07() || this.A0H.getCount() <= 0) {
            this.A05.setVisibility(0);
            this.A08.setVisibility(8);
            return;
        }
        this.A05.setVisibility(8);
        this.A08.setVisibility(0);
    }

    public final void A1D() {
        AnonymousClass2TF r0 = this.A0I;
        if (r0 != null) {
            r0.A03(true);
            this.A0I = null;
        }
        AnonymousClass286 r02 = this.A0J;
        if (r02 != null) {
            r02.A03(true);
            this.A0J = null;
        }
        AnonymousClass2TH r03 = this.A0G;
        if (r03 != null) {
            r03.A03(true);
            this.A0G = null;
        }
    }

    public final void A1E() {
        if (this.A0P && this.A0H != null) {
            AnonymousClass2TH r1 = this.A0G;
            if (r1 != null) {
                r1.A03(true);
            }
            this.A0G = new AnonymousClass2TH(this, this.A0H, new AnonymousClass2TP(this));
            this.A0O = false;
            this.A06.A02();
            this.A0N.Aaz(this.A0G, new Void[0]);
        }
    }

    public void A1F(int i) {
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            AnonymousClass23N.A00(A0B, this.A0A, this.A0E.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.n_items_selected, (long) i));
        }
    }

    public void A1G(AbstractC35611iN r10, AnonymousClass2T3 r11) {
        AnonymousClass1IS r4;
        Intent intent;
        Context context;
        ActivityC000900k r0;
        if (this instanceof StorageUsageMediaGalleryFragment) {
            StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = (StorageUsageMediaGalleryFragment) this;
            AbstractC16130oV r6 = ((AbstractC35601iM) r10).A03;
            if (storageUsageMediaGalleryFragment.A1J()) {
                r11.setChecked(((AbstractC13890kV) storageUsageMediaGalleryFragment.A0C()).Af1(r6));
                ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A06.A02();
                return;
            } else if (r10.getType() != 4) {
                AnonymousClass2TS r2 = new AnonymousClass2TS(storageUsageMediaGalleryFragment.A0C());
                r2.A07 = true;
                r2.A05 = true;
                r4 = r6.A0z;
                r2.A03 = r4.A00;
                r2.A04 = r4;
                r2.A02 = 2;
                r2.A01 = 2;
                intent = r2.A00();
                AbstractC454421p.A07(storageUsageMediaGalleryFragment.A0C(), intent, r11);
                context = storageUsageMediaGalleryFragment.A01();
                r0 = storageUsageMediaGalleryFragment.A0C();
            } else if (r6 instanceof C16440p1) {
                AnonymousClass1CY r7 = storageUsageMediaGalleryFragment.A09;
                C14900mE r42 = ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A07;
                AbstractC15710nm r22 = storageUsageMediaGalleryFragment.A02;
                AbstractC14440lR r8 = ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0N;
                C15670ni r5 = storageUsageMediaGalleryFragment.A07;
                C26511Dt.A06(storageUsageMediaGalleryFragment.A01, r22, (ActivityC13810kN) storageUsageMediaGalleryFragment.A0B(), r42, r5, (C16440p1) r6, r7, r8);
                return;
            } else {
                return;
            }
        } else if (this instanceof MediaPickerFragment) {
            ((MediaPickerFragment) this).A1N(r10);
            return;
        } else if (this instanceof MediaGalleryFragment) {
            MediaGalleryFragment mediaGalleryFragment = (MediaGalleryFragment) this;
            AbstractC16130oV r23 = ((AbstractC35601iM) r10).A03;
            if (mediaGalleryFragment.A1J()) {
                r11.setChecked(((AbstractC13890kV) mediaGalleryFragment.A0B()).Af1(r23));
                return;
            }
            AnonymousClass2TS r1 = new AnonymousClass2TS(mediaGalleryFragment.A0C());
            r1.A05 = true;
            r1.A03 = mediaGalleryFragment.A03;
            r4 = r23.A0z;
            r1.A04 = r4;
            r1.A02 = 2;
            r1.A00 = 34;
            intent = r1.A00();
            AbstractC454421p.A07(mediaGalleryFragment.A0C(), intent, r11);
            context = mediaGalleryFragment.A01();
            r0 = mediaGalleryFragment.A0C();
        } else if (!(this instanceof GalleryRecentsFragment)) {
            ((CameraMediaPickerFragment) this).A1P(r10);
            return;
        } else {
            GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) this;
            if (galleryRecentsFragment.A1J()) {
                galleryRecentsFragment.A1N(r10);
                return;
            }
            Map map = galleryRecentsFragment.A05;
            Uri AAE = r10.AAE();
            C16700pc.A0B(AAE);
            map.put(AAE, r10);
            GalleryTabHostFragment galleryTabHostFragment = galleryRecentsFragment.A01;
            if (galleryTabHostFragment != null) {
                List singletonList = Collections.singletonList(r10);
                C16700pc.A0B(singletonList);
                galleryTabHostFragment.A1G(singletonList);
                return;
            }
            return;
        }
        AbstractC454421p.A08(context, intent, r11, new AnonymousClass2TT(r0), AbstractC42671vd.A0Z(r4.toString()));
    }

    public void A1H(boolean z) {
        StringBuilder sb = new StringBuilder("mediagalleryfragmentbase/rebake unmounted:");
        sb.append(z);
        Log.i(sb.toString());
        A1D();
        AbstractC35581iK r1 = this.A0H;
        if (r1 != null) {
            r1.unregisterContentObserver(this.A0R);
            this.A0H.close();
            this.A0H = null;
        }
        A1I(true);
        this.A00 = 0;
        this.A06.A02();
        this.A0T.clear();
        AnonymousClass2TK A1B = A1B();
        if (A1B != null) {
            AnonymousClass2TF r2 = new AnonymousClass2TF(A0G(), new C51162Tc(this), A1B, z);
            this.A0I = r2;
            this.A0N.Aaz(r2, new Void[0]);
        }
    }

    public void A1I(boolean z) {
        View view = super.A0A;
        if (view != null) {
            View findViewById = view.findViewById(R.id.progress_bar);
            int i = 8;
            if (z) {
                i = 0;
            }
            findViewById.setVisibility(i);
        }
    }

    public boolean A1J() {
        ActivityC000900k r0;
        if (!(this instanceof StorageUsageMediaGalleryFragment)) {
            if (!(this instanceof MediaPickerFragment)) {
                if (this instanceof MediaGalleryFragment) {
                    r0 = A0B();
                } else if (this instanceof GalleryRecentsFragment) {
                    GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) this;
                    return galleryRecentsFragment.A03 || (galleryRecentsFragment.A05.isEmpty() ^ true);
                } else if (((CameraMediaPickerFragment) this).A02.getVisibility() != 0) {
                    return false;
                }
            } else if (((MediaPickerFragment) this).A05 == null) {
                return false;
            }
            return true;
        }
        r0 = A0C();
        return ((AbstractC13890kV) r0).AIM();
    }

    public boolean A1K(int i) {
        AbstractC16130oV r1;
        HashSet hashSet;
        AbstractC35581iK r0;
        AbstractC35611iN AEC;
        if (!(this instanceof StorageUsageMediaGalleryFragment)) {
            if (this instanceof MediaPickerFragment) {
                MediaPickerFragment mediaPickerFragment = (MediaPickerFragment) this;
                hashSet = mediaPickerFragment.A0E;
                r0 = ((MediaGalleryFragmentBase) mediaPickerFragment).A0H;
                AnonymousClass009.A05(r0);
            } else if (this instanceof MediaGalleryFragment) {
                AbstractC35601iM A01 = ((C35591iL) this.A0H).AEC(i);
                AnonymousClass009.A05(A01);
                return ((AbstractC13890kV) A0B()).AJm(A01.A03);
            } else if (!(this instanceof GalleryRecentsFragment)) {
                CameraMediaPickerFragment cameraMediaPickerFragment = (CameraMediaPickerFragment) this;
                hashSet = cameraMediaPickerFragment.A05;
                r0 = ((MediaGalleryFragmentBase) cameraMediaPickerFragment).A0H;
            } else {
                GalleryRecentsFragment galleryRecentsFragment = (GalleryRecentsFragment) this;
                AbstractC35581iK r02 = ((MediaGalleryFragmentBase) galleryRecentsFragment).A0H;
                if (r02 == null || (AEC = r02.AEC(i)) == null) {
                    return false;
                }
                Map map = galleryRecentsFragment.A05;
                Uri AAE = AEC.AAE();
                C16700pc.A0B(AAE);
                return map.containsKey(AAE);
            }
            return hashSet.contains(r0.AEC(i).AAE());
        }
        AbstractC35581iK r03 = this.A0H;
        if (r03 == null) {
            return false;
        }
        AbstractC35611iN AEC2 = r03.AEC(i);
        if (!(AEC2 instanceof AbstractC35601iM) || (r1 = ((AbstractC35601iM) AEC2).A03) == null || !((AbstractC13890kV) A0C()).AJm(r1)) {
            return false;
        }
        return true;
    }
}
