package com.whatsapp.gallery;

import X.AbstractC35611iN;
import X.AbstractC48852Ic;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01Y;
import X.AnonymousClass02M;
import X.AnonymousClass05I;
import X.AnonymousClass1WQ;
import X.AnonymousClass1s8;
import X.AnonymousClass2FO;
import X.AnonymousClass2T3;
import X.AnonymousClass2TJ;
import X.AnonymousClass2TK;
import X.AnonymousClass2TM;
import X.AnonymousClass5K0;
import X.C10700f3;
import X.C112835Ex;
import X.C114025Jx;
import X.C16590pI;
import X.C16700pc;
import X.C19390u2;
import X.C39341ph;
import X.C453421e;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.StickyHeadersRecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public final class GalleryRecentsFragment extends Hilt_GalleryRecentsFragment implements AbstractC48852Ic {
    public AnonymousClass1s8 A00;
    public GalleryTabHostFragment A01;
    public C19390u2 A02;
    public boolean A03;
    public final List A04 = new ArrayList();
    public final Map A05 = new LinkedHashMap();

    public GalleryRecentsFragment() {
        ((MediaGalleryFragmentBase) this).A0Q = true;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        A1M();
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.gallery_recents_fragment, viewGroup, false);
        C16700pc.A0B(inflate);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        StickyHeadersRecyclerView stickyHeadersRecyclerView = ((MediaGalleryFragmentBase) this).A08;
        if (stickyHeadersRecyclerView != null) {
            for (ImageView imageView : new AnonymousClass1WQ(new AnonymousClass5K0(), new C112835Ex(new C114025Jx(), new C10700f3(stickyHeadersRecyclerView)), false)) {
                imageView.setImageDrawable(null);
            }
        }
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        super.A17(bundle, view);
        StickyHeadersRecyclerView stickyHeadersRecyclerView = ((MediaGalleryFragmentBase) this).A08;
        if (stickyHeadersRecyclerView != null) {
            stickyHeadersRecyclerView.setBackgroundColor(AnonymousClass00T.A00(view.getContext(), R.color.media_recents_grid_background));
        }
        AZy();
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public AnonymousClass2TK A1B() {
        Bundle bundle = ((AnonymousClass01E) this).A05;
        if (bundle == null || bundle.getInt("include", 7) == 7) {
            return new AnonymousClass2TJ(((MediaGalleryFragmentBase) this).A0K, this.A04);
        }
        C16590pI r4 = ((MediaGalleryFragmentBase) this).A0B;
        AnonymousClass2FO r5 = ((MediaGalleryFragmentBase) this).A0K;
        C19390u2 r6 = this.A02;
        if (r6 != null) {
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            int i = 7;
            if (bundle2 != null) {
                i = bundle2.getInt("include", 7);
            }
            return new AnonymousClass2TM(null, r4, r5, r6, i);
        }
        C16700pc.A0K("perfTimerFactory");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r3, AnonymousClass2T3 r4) {
        Boolean valueOf;
        C16700pc.A0E(r3, 0);
        GalleryTabHostFragment galleryTabHostFragment = this.A01;
        if (galleryTabHostFragment == null) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(galleryTabHostFragment.A1I());
        }
        if (C16700pc.A0O(valueOf, Boolean.TRUE)) {
            return A1N(r3);
        }
        return false;
    }

    public final void A1M() {
        GalleryTabHostFragment galleryTabHostFragment = this.A01;
        if (galleryTabHostFragment != null) {
            Collection values = this.A05.values();
            C16700pc.A0E(values, 0);
            ArrayList arrayList = new ArrayList();
            for (Object obj : values) {
                if (obj != null) {
                    arrayList.add(obj);
                }
            }
            galleryTabHostFragment.A1H(AnonymousClass01Y.A08(arrayList));
        }
        A1F(this.A05.size());
        AnonymousClass02M r0 = ((MediaGalleryFragmentBase) this).A06;
        if (r0 != null) {
            r0.A02();
        }
    }

    public final boolean A1N(AbstractC35611iN r7) {
        Map map = this.A05;
        Uri AAE = r7.AAE();
        C16700pc.A0B(AAE);
        if (map.containsKey(AAE)) {
            map.remove(AAE);
        } else if (map.size() < 30) {
            map.put(AAE, r7);
        } else {
            ((MediaGalleryFragmentBase) this).A07.A0E(A02().getString(R.string.share_too_many_items_with_placeholder, 30), 0);
            return false;
        }
        A1M();
        return true;
    }

    @Override // X.AbstractC48852Ic
    public void AGY(C453421e r5, Collection collection) {
        C16700pc.A0E(collection, 0);
        C16700pc.A0E(r5, 1);
        C453421e r3 = new C453421e();
        collection.clear();
        for (Map.Entry entry : this.A05.entrySet()) {
            collection.add(entry.getKey());
            r3.A03(new C39341ph((Uri) entry.getKey()));
        }
        Map map = r5.A00;
        map.clear();
        map.putAll(r3.A00);
    }

    @Override // X.AbstractC48852Ic
    public void AZy() {
        if (((AnonymousClass01E) this).A0K.A02.compareTo(AnonymousClass05I.CREATED) >= 0) {
            A1H(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r5.containsAll(r12) == false) goto L_0x001b;
     */
    @Override // X.AbstractC48852Ic
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Acp(X.C453421e r11, java.util.Collection r12, java.util.Collection r13) {
        /*
            r10 = this;
            r0 = 0
            X.C16700pc.A0E(r12, r0)
            r0 = 1
            X.C16700pc.A0E(r13, r0)
            java.util.List r5 = r10.A04
            int r1 = r5.size()
            int r0 = r12.size()
            if (r1 != r0) goto L_0x001b
            boolean r0 = r5.containsAll(r12)
            r9 = 1
            if (r0 != 0) goto L_0x001c
        L_0x001b:
            r9 = 0
        L_0x001c:
            java.util.Map r4 = r10.A05
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x002d
            boolean r0 = r13.isEmpty()
            if (r0 == 0) goto L_0x002d
            if (r9 == 0) goto L_0x002d
            return
        L_0x002d:
            java.util.LinkedHashMap r6 = new java.util.LinkedHashMap
            r6.<init>()
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x003a:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x005c
            java.lang.Object r2 = r3.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r0 = r2.getKey()
            boolean r0 = r13.contains(r0)
            if (r0 == 0) goto L_0x003a
            java.lang.Object r1 = r2.getKey()
            java.lang.Object r0 = r2.getValue()
            r6.put(r1, r0)
            goto L_0x003a
        L_0x005c:
            java.util.LinkedHashMap r3 = new java.util.LinkedHashMap
            r3.<init>(r6)
            java.util.Iterator r8 = r13.iterator()
        L_0x0065:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x00bf
            java.lang.Object r6 = r8.next()
            boolean r0 = r3.containsKey(r6)
            if (r0 != 0) goto L_0x0065
            X.1s8 r2 = r10.A00
            if (r2 == 0) goto L_0x00bd
            boolean r0 = r2.A0R
            if (r0 == 0) goto L_0x009d
            java.util.List r0 = r2.A0u
            java.util.Iterator r1 = r0.iterator()
        L_0x0083:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x009d
            java.lang.Object r7 = r1.next()
            X.1iN r7 = (X.AbstractC35611iN) r7
            android.net.Uri r0 = r7.AAE()
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0083
        L_0x0099:
            r3.put(r6, r7)
            goto L_0x0065
        L_0x009d:
            X.1iK r0 = r2.A0G
            if (r0 == 0) goto L_0x00bd
            r1 = 0
        L_0x00a2:
            X.1iK r0 = r2.A0G
            int r0 = r0.getCount()
            if (r1 >= r0) goto L_0x00bd
            X.1iK r0 = r2.A0G
            X.1iN r7 = r0.AEC(r1)
            android.net.Uri r0 = r7.AAE()
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0099
            int r1 = r1 + 1
            goto L_0x00a2
        L_0x00bd:
            r7 = 0
            goto L_0x0099
        L_0x00bf:
            r4.clear()
            r4.putAll(r3)
            if (r9 != 0) goto L_0x00d0
            r5.clear()
            r5.addAll(r12)
            r10.AZy()
        L_0x00d0:
            r10.A1M()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallery.GalleryRecentsFragment.Acp(X.21e, java.util.Collection, java.util.Collection):void");
    }
}
