package com.whatsapp.appwidget;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass3OQ;
import X.AnonymousClass5B7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C22630zO;
import X.C22670zS;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import android.widget.RemoteViewsService;

/* loaded from: classes2.dex */
public class WidgetService extends RemoteViewsService implements AnonymousClass004 {
    public C22670zS A00;
    public C15550nR A01;
    public C15610nY A02;
    public C14830m7 A03;
    public AnonymousClass018 A04;
    public C22630zO A05;
    public boolean A06;
    public final Object A07;
    public volatile C71083cM A08;

    public WidgetService() {
        this(0);
    }

    public WidgetService(int i) {
        this.A07 = C12970iu.A0l();
        this.A06 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A08 == null) {
            synchronized (this.A07) {
                if (this.A08 == null) {
                    this.A08 = new C71083cM(this);
                }
            }
        }
        return this.A08.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A03 = C12980iv.A0b(r1);
            this.A00 = (C22670zS) r1.A0V.get();
            this.A01 = C12960it.A0O(r1);
            this.A02 = C12960it.A0P(r1);
            this.A04 = C12960it.A0R(r1);
            this.A05 = (C22630zO) r1.AD7.get();
        }
        super.onCreate();
    }

    @Override // android.widget.RemoteViewsService
    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new AnonymousClass3OQ(getApplicationContext(), this.A00, this.A01, this.A02, this.A03, this.A04, this.A05);
    }
}
