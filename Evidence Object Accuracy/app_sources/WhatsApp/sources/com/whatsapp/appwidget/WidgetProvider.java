package com.whatsapp.appwidget;

import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass10Y;
import X.AnonymousClass1UY;
import X.AnonymousClass22D;
import X.C14900mE;
import X.C14960mK;
import X.C15680nj;
import X.C19990v2;
import X.C21290xB;
import X.C22670zS;
import X.C35741ib;
import X.RunnableC55852jY;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class WidgetProvider extends AppWidgetProvider {
    public static ArrayList A0A;
    public C14900mE A00;
    public RunnableC55852jY A01;
    public C21290xB A02;
    public C22670zS A03;
    public AnonymousClass018 A04;
    public C19990v2 A05;
    public C15680nj A06;
    public AnonymousClass10Y A07;
    public final Object A08;
    public volatile boolean A09;

    public WidgetProvider() {
        this(0);
    }

    public WidgetProvider(int i) {
        this.A09 = false;
        this.A08 = new Object();
    }

    public static RemoteViews A00(Context context, C22670zS r10, AnonymousClass018 r11, int i, int i2, int i3) {
        Intent A02;
        String str;
        Intent A022;
        boolean A03 = r10.A03();
        if (i2 <= 100 || i3 <= 100) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_small);
            ArrayList arrayList = A0A;
            if (arrayList != null) {
                int size = arrayList.size();
                remoteViews.setTextViewText(R.id.count, Integer.toString(size));
                float f = 30.0f;
                if (i2 < 100) {
                    if (size > 99) {
                        f = 14.0f;
                    } else if (size > 9) {
                        f = 20.0f;
                    }
                }
                remoteViews.setFloat(R.id.count, "setTextSize", f);
            }
            if (A03) {
                A02 = C14960mK.A04(context);
            } else {
                A02 = C14960mK.A02(context);
            }
            remoteViews.setOnClickPendingIntent(R.id.header, AnonymousClass1UY.A00(context, 1, A02, 134217728));
            return remoteViews;
        }
        RemoteViews remoteViews2 = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
        ArrayList arrayList2 = A0A;
        if (arrayList2 == null) {
            str = "";
        } else if (arrayList2.size() > 0) {
            str = r11.A0I(new Object[]{Integer.valueOf(arrayList2.size())}, R.plurals.unread_message_count, (long) arrayList2.size());
            remoteViews2.setViewVisibility(R.id.subtitle, 0);
        } else {
            str = context.getString(R.string.no_unread_messages);
            remoteViews2.setViewVisibility(R.id.subtitle, 8);
        }
        remoteViews2.setTextViewText(R.id.subtitle, str);
        Intent intent = new Intent(context, WidgetService.class);
        intent.putExtra("appWidgetId", i);
        intent.setData(Uri.parse(intent.toUri(1)));
        remoteViews2.setRemoteAdapter(i, R.id.list_view_widget, intent);
        Intent intent2 = new Intent(context, Conversation.class);
        C35741ib.A01(intent2, "WidgetProvider");
        intent2.setAction("android.intent.action.VIEW");
        int i4 = 134217728;
        if (AnonymousClass1UY.A01) {
            i4 = 167772160;
        }
        remoteViews2.setPendingIntentTemplate(R.id.list_view_widget, PendingIntent.getActivity(context, 1, intent2, i4));
        if (A03) {
            A022 = C14960mK.A04(context);
        } else {
            A022 = C14960mK.A02(context);
        }
        remoteViews2.setOnClickPendingIntent(R.id.header, AnonymousClass1UY.A00(context, 1, A022, 134217728));
        remoteViews2.setEmptyView(R.id.list_view_widget, R.id.empty_view);
        boolean A06 = r10.A06();
        int i5 = R.string.no_unread_messages;
        if (!A06) {
            i5 = R.string.widget_content_hidden;
        }
        remoteViews2.setTextViewText(R.id.empty_view, context.getString(i5));
        return remoteViews2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002b, code lost:
        if (r5 != 0) goto L_0x0033;
     */
    @Override // android.appwidget.AppWidgetProvider
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAppWidgetOptionsChanged(android.content.Context r7, android.appwidget.AppWidgetManager r8, int r9, android.os.Bundle r10) {
        /*
            r6 = this;
            if (r10 == 0) goto L_0x002d
            java.lang.String r0 = "appWidgetMinWidth"
            int r4 = r10.getInt(r0)
            java.lang.String r0 = "appWidgetMinHeight"
            int r5 = r10.getInt(r0)
            java.lang.String r0 = "widgetprovider/onappwidgetoptionschanged "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = "x"
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            if (r4 == 0) goto L_0x002d
            if (r5 != 0) goto L_0x0033
        L_0x002d:
            r4 = 2147483647(0x7fffffff, float:NaN)
            r5 = 2147483647(0x7fffffff, float:NaN)
        L_0x0033:
            X.0zS r1 = r6.A03
            X.018 r2 = r6.A04
            r0 = r7
            r3 = r9
            android.widget.RemoteViews r0 = A00(r0, r1, r2, r3, r4, r5)
            r8.updateAppWidget(r9, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.appwidget.WidgetProvider.onAppWidgetOptionsChanged(android.content.Context, android.appwidget.AppWidgetManager, int, android.os.Bundle):void");
    }

    @Override // android.appwidget.AppWidgetProvider, android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A09) {
            synchronized (this.A08) {
                if (!this.A09) {
                    AnonymousClass01J r2 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = (C14900mE) r2.A8X.get();
                    this.A05 = (C19990v2) r2.A3M.get();
                    this.A02 = (C21290xB) r2.ANf.get();
                    this.A03 = (C22670zS) r2.A0V.get();
                    this.A04 = (AnonymousClass018) r2.ANb.get();
                    this.A07 = (AnonymousClass10Y) r2.AAR.get();
                    this.A06 = (C15680nj) r2.A4e.get();
                    this.A09 = true;
                }
            }
        }
        super.onReceive(context, intent);
    }

    @Override // android.appwidget.AppWidgetProvider
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        StringBuilder sb = new StringBuilder("widgetprovider/update ");
        sb.append(iArr.length);
        Log.i(sb.toString());
        RunnableC55852jY r0 = this.A01;
        if (r0 != null) {
            r0.A08.set(true);
            this.A02.A00().removeCallbacks(this.A01);
        }
        C14900mE r4 = this.A00;
        C19990v2 r7 = this.A05;
        this.A01 = new RunnableC55852jY(appWidgetManager, context, r4, this.A03, this.A04, r7, this.A06, this.A07, iArr);
        this.A02.A00().post(this.A01);
        super.onUpdate(context, appWidgetManager, iArr);
    }
}
