package com.whatsapp;

import X.AbstractC35611iN;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaMediaThumbnailView extends WaImageView {
    public Bitmap A00;
    public AbstractC35611iN A01;

    public WaMediaThumbnailView(Context context) {
        super(context);
    }

    public WaMediaThumbnailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WaMediaThumbnailView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public AbstractC35611iN getMediaItem() {
        return this.A01;
    }

    public Bitmap getThumbnail() {
        return this.A00;
    }

    public void setMediaItem(AbstractC35611iN r1) {
        this.A01 = r1;
    }

    public void setThumbnail(Bitmap bitmap) {
        this.A00 = bitmap;
        setImageBitmap(bitmap);
    }
}
