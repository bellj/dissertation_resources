package com.whatsapp.account.delete;

import X.AbstractC005102i;
import X.AbstractC115375Rh;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C22710zW;
import X.C22730zY;
import X.C253118x;
import X.C41691tw;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.BulletSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DeleteAccountActivity extends ActivityC13790kL implements AbstractC115375Rh {
    public C22730zY A00;
    public C22710zW A01;
    public C253118x A02;
    public boolean A03;

    public DeleteAccountActivity() {
        this(0);
    }

    public DeleteAccountActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 6);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = (C253118x) A1M.AAW.get();
            this.A01 = (C22710zW) A1M.AF7.get();
            this.A00 = (C22730zY) A1M.A8Y.get();
        }
    }

    public final void A2e(TextView textView, CharSequence charSequence) {
        SpannableStringBuilder A0J = C12990iw.A0J(charSequence);
        A0J.setSpan(new BulletSpan((int) getResources().getDimension(R.dimen.settings_bullet_span_gap)), 0, A0J.length(), 0);
        textView.setText(A0J);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        View findViewById;
        super.onCreate(bundle);
        setContentView(R.layout.delete_account);
        setTitle(R.string.settings_delete_account);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        ImageView imageView = (ImageView) findViewById(R.id.change_number_icon);
        AnonymousClass2GF.A01(this, imageView, ((ActivityC13830kP) this).A01, R.drawable.ic_settings_change_number);
        C41691tw.A06(this, imageView);
        C12970iu.A0M(this, R.id.delete_account_instructions).setText(R.string.delete_account_instructions);
        C12960it.A10(findViewById(R.id.delete_account_change_number_option), this, 10);
        A2e(C12970iu.A0M(this, R.id.delete_whatsapp_account_warning_text), getString(R.string.delete_account_item_1));
        A2e(C12970iu.A0M(this, R.id.delete_message_history_warning_text), getString(R.string.delete_account_item_2));
        A2e(C12970iu.A0M(this, R.id.delete_whatsapp_group_warning_text), getString(R.string.delete_account_item_3));
        A2e(C12970iu.A0M(this, R.id.delete_google_drive_warning_text), getString(R.string.delete_account_item_4));
        A2e(C12970iu.A0M(this, R.id.delete_payments_account_warning_text), getString(R.string.delete_account_item_5));
        if (!this.A00.A09() || ((ActivityC13810kN) this).A09.A09() == null) {
            C12970iu.A1N(this, R.id.delete_google_drive_warning_text, 8);
        }
        if (!this.A01.A07()) {
            findViewById = findViewById(R.id.delete_payments_account_warning_text);
        } else {
            C22710zW r1 = this.A01;
            if (r1.A04() && r1.A05.A01().getBoolean("payments_has_willow_account", false)) {
                A2e(C12970iu.A0M(this, R.id.delete_payments_account_warning_text), getString(R.string.delete_account_item_6));
                C12970iu.A1N(this, R.id.delete_account_instructions_with_payments, 0);
                A2e(C12970iu.A0M(this, R.id.delete_account_instructions_payments_1), getString(R.string.delete_account_instructions_with_payments_item_1));
                TextView A0M = C12970iu.A0M(this, R.id.delete_account_instructions_payments_2);
                A0M.setVisibility(0);
                C12990iw.A1F(A0M);
                A2e(A0M, C253118x.A00(this, getString(R.string.delete_account_instructions_with_payments_novi)));
                findViewById = findViewById(R.id.delete_account_instructions);
            }
            AnonymousClass01E A07 = A0V().A07(R.id.delete_account_match_phone_number_fragment);
            AnonymousClass009.A05(A07);
            AbstractView$OnClickListenerC34281fs.A02(findViewById(R.id.delete_account_submit), this, A07, 0);
        }
        findViewById.setVisibility(8);
        AnonymousClass01E A07 = A0V().A07(R.id.delete_account_match_phone_number_fragment);
        AnonymousClass009.A05(A07);
        AbstractView$OnClickListenerC34281fs.A02(findViewById(R.id.delete_account_submit), this, A07, 0);
    }
}
