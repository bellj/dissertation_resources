package com.whatsapp.account.delete;

import X.AbstractC005102i;
import X.AbstractC11640gc;
import X.AbstractC11650gd;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass07N;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.C004802e;
import X.C102774pg;
import X.C12970iu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.DialogInterface$OnClickListenerC65553Kc;
import X.ViewTreeObserver$OnPreDrawListenerC101834oA;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.account.delete.DeleteAccountFeedback;

/* loaded from: classes2.dex */
public class DeleteAccountFeedback extends ActivityC13790kL {
    public static final int[] A09 = {R.string.delete_account_reason_changed_device, R.string.delete_account_reason_change_phone_number, R.string.delete_account_reason_temporarily, R.string.delete_account_reason_missing_feature, R.string.delete_account_reason_not_working, R.string.delete_account_reason_other};
    public int A00;
    public int A01;
    public View A02;
    public EditText A03;
    public ScrollView A04;
    public AnonymousClass07N A05;
    public DialogFragment A06;
    public boolean A07;
    public boolean A08;

    public DeleteAccountFeedback() {
        this(0);
        this.A01 = -1;
        this.A07 = false;
    }

    public DeleteAccountFeedback(int i) {
        this.A08 = false;
        A0R(new C102774pg(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
        }
    }

    public final void A2e() {
        float f;
        boolean canScrollVertically = this.A04.canScrollVertically(1);
        View view = this.A02;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public final void A2f() {
        this.A04.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101834oA(this));
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (Build.VERSION.SDK_INT >= 21) {
            A2f();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_delete_account);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.delete_account_feedback);
        this.A04 = (ScrollView) findViewById(R.id.scroll_view);
        this.A03 = (EditText) findViewById(R.id.delete_reason_additional_comments_edittext);
        this.A02 = findViewById(R.id.bottom_button_container);
        TextView textView = (TextView) findViewById(R.id.select_delete_reason);
        textView.setBackground(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.abc_spinner_textfield_background_material), ((ActivityC13830kP) this).A01));
        this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
        if (bundle != null) {
            this.A01 = bundle.getInt("delete_reason_selected", -1);
            this.A07 = bundle.getBoolean("delete_reason_showing", false);
            EditText editText = this.A03;
            int i = this.A01;
            int i2 = R.string.delete_account_additional_comments_hint;
            if (i == 2) {
                i2 = R.string.delete_account_additional_comments_temporarily;
            }
            editText.setHint(getString(i2));
        }
        int i3 = this.A01;
        int[] iArr = A09;
        int length = iArr.length;
        if (i3 >= length || i3 < 0) {
            textView.setText("");
        } else {
            textView.setText(iArr[i3]);
        }
        this.A05 = new AnonymousClass07N(this, findViewById(R.id.delete_reason_prompt));
        for (int i4 = 0; i4 < length; i4++) {
            this.A05.A04.add(0, i4, 0, iArr[i4]);
        }
        AnonymousClass07N r1 = this.A05;
        r1.A00 = new AbstractC11640gc() { // from class: X.4rB
            @Override // X.AbstractC11640gc
            public final void APH(AnonymousClass07N r3) {
                DeleteAccountFeedback.this.A07 = false;
            }
        };
        r1.A01 = new AbstractC11650gd(textView, this) { // from class: X.4rC
            public final /* synthetic */ TextView A00;
            public final /* synthetic */ DeleteAccountFeedback A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC11650gd
            public final boolean onMenuItemClick(MenuItem menuItem) {
                DeleteAccountFeedback deleteAccountFeedback = this.A01;
                TextView textView2 = this.A00;
                deleteAccountFeedback.A01 = menuItem.getItemId();
                textView2.setText(menuItem.getTitle());
                EditText editText2 = deleteAccountFeedback.A03;
                int i5 = deleteAccountFeedback.A01;
                int i6 = R.string.delete_account_additional_comments_hint;
                if (i5 == 2) {
                    i6 = R.string.delete_account_additional_comments_temporarily;
                }
                editText2.setHint(deleteAccountFeedback.getString(i6));
                return false;
            }
        };
        textView.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 16));
        findViewById(R.id.delete_account_submit).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 17));
        ((ActivityC13810kN) this).A00.post(new RunnableBRunnable0Shape1S0100000_I0_1(this, 39));
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
            this.A04.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oV
                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    DeleteAccountFeedback.this.A2e();
                }
            });
            A2f();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("delete_reason_selected", this.A01);
        bundle.putBoolean("delete_reason_showing", this.A07);
        super.onSaveInstanceState(bundle);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        AnonymousClass07N r1 = this.A05;
        if (r1 != null) {
            r1.A00 = null;
            r1.A05.A01();
        }
    }

    /* loaded from: classes2.dex */
    public class ChangeNumberMessageDialogFragment extends Hilt_DeleteAccountFeedback_ChangeNumberMessageDialogFragment {
        public static DialogFragment A00(String str) {
            ChangeNumberMessageDialogFragment changeNumberMessageDialogFragment = new ChangeNumberMessageDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("deleteReason", 1);
            A0D.putString("additionalComments", str);
            changeNumberMessageDialogFragment.A0U(A0D);
            return changeNumberMessageDialogFragment;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int i = A03().getInt("deleteReason", -1);
            String string = A03().getString("additionalComments");
            C004802e A0O = C12970iu.A0O(this);
            A0O.A0A(C12970iu.A0q(this, A0I(R.string.settings_change_number), C12970iu.A1b(), 0, R.string.delete_account_change_number_dialog_prompt));
            C12970iu.A1M(A0O, this, 4, R.string.settings_change_number);
            A0O.setNegativeButton(R.string.settings_delete_account_short, new DialogInterface$OnClickListenerC65553Kc(this, string, i));
            return A0O.create();
        }
    }
}
