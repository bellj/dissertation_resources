package com.whatsapp.account.delete;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass108;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14960mK;
import X.C18000rk;
import X.C19550uI;
import X.C20660w7;
import X.C22710zW;
import X.C22730zY;
import X.C22880zn;
import X.ViewTreeObserver$OnPreDrawListenerC101824o9;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DeleteAccountConfirmation extends ActivityC13790kL {
    public int A00;
    public Handler A01;
    public View A02;
    public ScrollView A03;
    public AnonymousClass108 A04;
    public C22880zn A05;
    public C22730zY A06;
    public C19550uI A07;
    public C20660w7 A08;
    public C22710zW A09;
    public AnonymousClass01H A0A;
    public boolean A0B;

    public DeleteAccountConfirmation() {
        this(0);
    }

    public DeleteAccountConfirmation(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 7);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A08 = C12990iw.A0d(A1M);
            this.A05 = (C22880zn) A1M.A5W.get();
            this.A07 = A1M.A3K();
            this.A0A = C18000rk.A00(A1M.AN3);
            this.A09 = (C22710zW) A1M.AF7.get();
            this.A06 = (C22730zY) A1M.A8Y.get();
        }
    }

    public final void A2e() {
        float f;
        boolean canScrollVertically = this.A03.canScrollVertically(1);
        View view = this.A02;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public final void A2f() {
        this.A03.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101824o9(this));
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (Build.VERSION.SDK_INT >= 21) {
            A2f();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            super.onCreate(r8)
            X.0nN r1 = r7.A0B
            X.2aA r0 = new X.2aA
            r0.<init>(r7, r1)
            r7.A01 = r0
            X.3UU r0 = new X.3UU
            r0.<init>(r7)
            r7.A04 = r0
            r0 = 2131891612(0x7f12159c, float:1.9417949E38)
            r7.setTitle(r0)
            X.02i r0 = r7.A1U()
            r6 = 1
            if (r0 == 0) goto L_0x0023
            r0.A0M(r6)
        L_0x0023:
            r0 = 2131558872(0x7f0d01d8, float:1.8743072E38)
            r7.setContentView(r0)
            r0 = 2131365663(0x7f0a0f1f, float:1.8351198E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ScrollView r0 = (android.widget.ScrollView) r0
            r7.A03 = r0
            r0 = 2131362285(0x7f0a01ed, float:1.8344346E38)
            android.view.View r0 = r7.findViewById(r0)
            r7.A02 = r0
            r0 = 2131363027(0x7f0a04d3, float:1.8345851E38)
            android.view.View r1 = r7.findViewById(r0)
            r0 = 11
            X.C12960it.A10(r1, r7, r0)
            r0 = 2131363020(0x7f0a04cc, float:1.8345837E38)
            android.widget.TextView r5 = X.C12970iu.A0M(r7, r0)
            r0 = 2131891615(0x7f12159f, float:1.9417955E38)
            java.lang.String r4 = r7.getString(r0)
            android.content.res.Resources r0 = r7.getResources()
            r2 = 2131166801(0x7f070651, float:1.7947858E38)
            int r0 = r0.getDimensionPixelSize(r2)
            r7.A00 = r0
            X.0zY r0 = r7.A06
            boolean r0 = r0.A09()
            r3 = 0
            if (r0 == 0) goto L_0x00b4
            X.0m6 r0 = r7.A09
            java.lang.String r0 = r0.A09()
            if (r0 == 0) goto L_0x00b4
            X.0zW r0 = r7.A09
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x00b4
            r1 = 2131891617(0x7f1215a1, float:1.941796E38)
        L_0x0080:
            java.lang.Object[] r0 = new java.lang.Object[r6]
            java.lang.String r4 = X.C12960it.A0X(r7, r4, r0, r3, r1)
        L_0x0086:
            r5.setText(r4)
            X.0zn r0 = r7.A05
            X.108 r1 = r7.A04
            java.util.concurrent.CopyOnWriteArrayList r0 = r0.A0s
            r0.add(r1)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x00b3
            android.content.res.Resources r0 = r7.getResources()
            int r0 = r0.getDimensionPixelSize(r2)
            r7.A00 = r0
            android.widget.ScrollView r0 = r7.A03
            android.view.ViewTreeObserver r1 = r0.getViewTreeObserver()
            X.4oU r0 = new X.4oU
            r0.<init>()
            r1.addOnScrollChangedListener(r0)
            r7.A2f()
        L_0x00b3:
            return
        L_0x00b4:
            X.0zY r0 = r7.A06
            boolean r0 = r0.A09()
            if (r0 == 0) goto L_0x00c8
            X.0m6 r0 = r7.A09
            java.lang.String r0 = r0.A09()
            if (r0 == 0) goto L_0x00c8
            r1 = 2131891616(0x7f1215a0, float:1.9417957E38)
            goto L_0x0080
        L_0x00c8:
            X.0zW r0 = r7.A09
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x0086
            r1 = 2131891618(0x7f1215a2, float:1.9417961E38)
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.account.delete.DeleteAccountConfirmation.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        int i2;
        int i3;
        if (i != 1) {
            if (i == 2) {
                r3 = C12980iv.A0S(this);
                r3.A0A(C12960it.A0X(this, getString(R.string.connectivity_self_help_instructions), new Object[1], 0, R.string.register_check_connectivity));
                i2 = R.string.ok;
                i3 = 5;
            } else if (i != 3) {
                return super.onCreateDialog(i);
            } else {
                r3 = C12980iv.A0S(this);
                r3.A06(R.string.delete_account_failed);
                i2 = R.string.ok;
                i3 = 6;
            }
            C12970iu.A1L(r3, this, i3, i2);
            return r3.create();
        }
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.delete_account_processing));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C22880zn r0 = this.A05;
        r0.A0s.remove(this.A04);
        this.A01.removeMessages(0);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        int A00 = ((ActivityC13790kL) this).A0B.A00();
        if (!((ActivityC13790kL) this).A0B.A01() && A00 != 6) {
            Log.e(C12960it.A0W(A00, "deleteaccountconfirm/wrong-state bounce to main "));
            startActivity(C14960mK.A04(this));
            finish();
        }
    }
}
