package com.whatsapp;

import X.AbstractC14440lR;
import X.AbstractC16350or;
import X.AnonymousClass004;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass38R;
import X.AnonymousClass533;
import X.AnonymousClass5RF;
import X.AnonymousClass5RG;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.mediacomposer.VideoComposerFragment;
import java.io.File;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class VideoTimelineView extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public long A0D;
    public long A0E;
    public long A0F;
    public long A0G;
    public AnonymousClass5RF A0H;
    public AnonymousClass5RG A0I;
    public AbstractC16350or A0J;
    public AbstractC14440lR A0K;
    public AnonymousClass2P7 A0L;
    public File A0M;
    public ArrayList A0N;
    public boolean A0O;
    public final Paint A0P;
    public final Rect A0Q;
    public final RectF A0R;

    public VideoTimelineView(Context context) {
        super(context);
        A02();
        this.A0P = new Paint(1);
        this.A0R = new RectF();
        this.A0Q = new Rect();
        this.A00 = 1.0f;
        this.A07 = -1;
        this.A05 = 12.0f;
        this.A0B = -1;
        this.A06 = 12.0f;
        this.A0C = -1;
        this.A08 = 855638016;
    }

    public VideoTimelineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A0P = new Paint(1);
        this.A0R = new RectF();
        this.A0Q = new Rect();
        this.A00 = 1.0f;
        this.A07 = -1;
        this.A05 = 12.0f;
        this.A0B = -1;
        this.A06 = 12.0f;
        this.A0C = -1;
        this.A08 = 855638016;
        A04(context, attributeSet);
    }

    public VideoTimelineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A0P = new Paint(1);
        this.A0R = new RectF();
        this.A0Q = new Rect();
        this.A00 = 1.0f;
        this.A07 = -1;
        this.A05 = 12.0f;
        this.A0B = -1;
        this.A06 = 12.0f;
        this.A0C = -1;
        this.A08 = 855638016;
        A04(context, attributeSet);
    }

    public VideoTimelineView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A02();
        this.A0P = new Paint(1);
        this.A0R = new RectF();
        this.A0Q = new Rect();
        this.A00 = 1.0f;
        this.A07 = -1;
        this.A05 = 12.0f;
        this.A0B = -1;
        this.A06 = 12.0f;
        this.A0C = -1;
        this.A08 = 855638016;
        A04(context, attributeSet);
    }

    public VideoTimelineView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A02();
    }

    public final int A00(long j) {
        return Math.min(getPaddingLeft() + getTimelineWidth(), Math.max(getPaddingLeft(), (int) (((long) getPaddingLeft()) + ((((long) getTimelineWidth()) * j) / this.A0D))));
    }

    public final long A01(float f) {
        return Math.min(this.A0D, Math.max((long) ((((float) this.A0D) * (f - ((float) getPaddingLeft()))) / ((float) getTimelineWidth())), 0L));
    }

    public void A02() {
        if (!this.A0O) {
            this.A0O = true;
            this.A0K = (AbstractC14440lR) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANe.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0103, code lost:
        if ((r1 - r5) > r3) goto L_0x0105;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(float r17) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.VideoTimelineView.A03(float):void");
    }

    public final void A04(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0O);
            this.A00 = obtainStyledAttributes.getDimension(1, this.A00);
            this.A07 = obtainStyledAttributes.getInteger(0, this.A07);
            this.A05 = obtainStyledAttributes.getDimension(5, this.A05);
            this.A0B = obtainStyledAttributes.getInteger(3, this.A0B);
            this.A06 = obtainStyledAttributes.getDimension(6, this.A06);
            this.A0C = obtainStyledAttributes.getInteger(4, this.A0C);
            this.A08 = obtainStyledAttributes.getInteger(2, this.A08);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0L;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0L = r0;
        }
        return r0.generatedComponent();
    }

    private int getTimelineHeight() {
        return Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom());
    }

    private int getTimelineWidth() {
        return Math.max(0, (getWidth() - getPaddingLeft()) - getPaddingRight());
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AbstractC16350or r2 = this.A0J;
        if (r2 != null) {
            r2.A03(true);
            this.A0J = null;
        }
        this.A0N = null;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        float f;
        int i2;
        float f2;
        super.onDraw(canvas);
        if (this.A0M != null) {
            int timelineWidth = getTimelineWidth();
            int timelineHeight = getTimelineHeight();
            if (timelineHeight > 0 && timelineWidth > 0) {
                if (this.A09 != timelineWidth) {
                    this.A09 = timelineWidth;
                    this.A0N = null;
                    AbstractC16350or r0 = this.A0J;
                    if (r0 != null) {
                        r0.A03(true);
                        this.A0J = null;
                    }
                }
                if (this.A0N != null) {
                    float f3 = ((float) timelineWidth) / ((float) (timelineWidth / timelineHeight));
                    RectF rectF = this.A0R;
                    rectF.top = (float) getPaddingTop();
                    rectF.bottom = (float) (getPaddingTop() + timelineHeight);
                    for (int i3 = 0; i3 < this.A0N.size(); i3++) {
                        float paddingLeft = ((float) getPaddingLeft()) + (((float) i3) * f3);
                        rectF.left = paddingLeft;
                        rectF.right = paddingLeft + f3;
                        Bitmap bitmap = (Bitmap) this.A0N.get(i3);
                        if (bitmap != null) {
                            int width = bitmap.getWidth();
                            int height = bitmap.getHeight();
                            Rect rect = this.A0Q;
                            if (width > height) {
                                rect.top = 0;
                                rect.bottom = height;
                                int i4 = (width - height) / 2;
                                rect.left = i4;
                                rect.right = i4 + height;
                            } else {
                                rect.left = 0;
                                rect.right = width;
                                int i5 = (height - width) / 2;
                                rect.top = i5;
                                rect.bottom = i5 + width;
                            }
                            canvas.drawBitmap(bitmap, rect, rectF, this.A0P);
                        }
                    }
                } else if (this.A0J == null) {
                    int i6 = timelineWidth / timelineHeight;
                    this.A0N = new ArrayList(i6);
                    AnonymousClass38R r13 = new AnonymousClass38R(this, this.A0M, ((float) timelineWidth) / ((float) i6), (float) timelineHeight, i6);
                    this.A0J = r13;
                    this.A0K.Aaz(r13, new Void[0]);
                }
                if (this.A0H != null) {
                    float A00 = (float) A00(this.A0F);
                    float A002 = (float) A00(this.A0G);
                    Paint paint = this.A0P;
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(this.A08);
                    RectF rectF2 = this.A0R;
                    rectF2.set((float) getPaddingLeft(), (float) getPaddingTop(), A00, (float) (getHeight() - getPaddingBottom()));
                    canvas.drawRect(rectF2, paint);
                    rectF2.set(A002, (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()));
                    canvas.drawRect(rectF2, paint);
                    AnonymousClass5RG r02 = this.A0I;
                    if (r02 != null) {
                        VideoComposerFragment videoComposerFragment = ((AnonymousClass533) r02).A00;
                        if (videoComposerFragment.A0M.A0B()) {
                            videoComposerFragment.A01 = (long) videoComposerFragment.A0M.A01();
                        }
                        long j = videoComposerFragment.A01;
                        if (j >= 0 && j >= this.A0F && j <= this.A0G) {
                            paint.setColor(this.A07);
                            paint.setStyle(Paint.Style.STROKE);
                            paint.setStrokeWidth(this.A00 / 2.0f);
                            float A003 = (float) A00(j);
                            canvas.drawLine(A003, (float) getPaddingTop(), A003, (float) (getHeight() - getPaddingBottom()), paint);
                        }
                        if (((AnonymousClass533) this.A0I).A00.A0M.A0B()) {
                            invalidate();
                        }
                    }
                    paint.setColor(this.A07);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(this.A00);
                    rectF2.set(A00 - 1.0f, (float) getPaddingTop(), 1.0f + A002, (float) (getHeight() - getPaddingBottom()));
                    canvas.drawRect(rectF2, paint);
                    paint.setStyle(Paint.Style.FILL);
                    if (this.A0A == 1) {
                        i = this.A0C;
                    } else {
                        i = this.A0B;
                    }
                    paint.setColor(i);
                    int i7 = timelineHeight / 2;
                    float paddingTop = (float) (getPaddingTop() + i7);
                    if (this.A0A == 1) {
                        f = this.A06;
                    } else {
                        f = this.A05;
                    }
                    canvas.drawCircle(A00, paddingTop, f, paint);
                    if (this.A0A == 2) {
                        i2 = this.A0C;
                    } else {
                        i2 = this.A0B;
                    }
                    paint.setColor(i2);
                    float paddingTop2 = (float) (getPaddingTop() + i7);
                    if (this.A0A == 2) {
                        f2 = this.A06;
                    } else {
                        f2 = this.A05;
                    }
                    canvas.drawCircle(A002, paddingTop2, f2, paint);
                }
            }
        } else if (isInEditMode()) {
            Paint paint2 = this.A0P;
            paint2.setStyle(Paint.Style.FILL);
            paint2.setColor(this.A08);
            RectF rectF3 = this.A0R;
            rectF3.set((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()));
            canvas.drawRect(rectF3, paint2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        if (r1 != 3) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0096, code lost:
        if ((r5 / ((float) getWidth())) <= 0.5f) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d6, code lost:
        if (r5 >= (r8 - r10)) goto L_0x00d8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009e  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.VideoTimelineView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setMaxTrim(long j) {
        this.A0E = j;
    }

    public void setTrimListener(AnonymousClass5RF r1) {
        this.A0H = r1;
    }

    public void setVideoPlayback(AnonymousClass5RG r1) {
        this.A0I = r1;
    }
}
