package com.whatsapp.protocol;

import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class VoipStanzaChildNode {
    public final AnonymousClass1W9[] attributes;
    public final VoipStanzaChildNode[] children;
    public final byte[] data;
    public final String tag;

    public VoipStanzaChildNode(String str, AnonymousClass1W9[] r2, VoipStanzaChildNode[] voipStanzaChildNodeArr, byte[] bArr) {
        this.tag = str;
        this.attributes = r2;
        this.children = voipStanzaChildNodeArr;
        this.data = bArr;
    }

    public static VoipStanzaChildNode fromProtocolTreeNode(AnonymousClass1V8 r7) {
        VoipStanzaChildNode[] voipStanzaChildNodeArr;
        AnonymousClass1V8[] r6 = r7.A03;
        if (r6 != null) {
            int length = r6.length;
            voipStanzaChildNodeArr = new VoipStanzaChildNode[length];
            int i = 0;
            int i2 = 0;
            while (i < length) {
                voipStanzaChildNodeArr[i2] = fromProtocolTreeNode(r6[i]);
                i++;
                i2++;
            }
        } else {
            voipStanzaChildNodeArr = null;
        }
        return new VoipStanzaChildNode(r7.A00, r7.A0L(), voipStanzaChildNodeArr, r7.A01);
    }

    public AnonymousClass1W9[] getAttributesCopy() {
        AnonymousClass1W9[] r1 = this.attributes;
        if (r1 != null) {
            return (AnonymousClass1W9[]) Arrays.copyOf(r1, r1.length);
        }
        return null;
    }

    public Object[] getAttributesFlattedCopy() {
        AnonymousClass1W9[] r7 = this.attributes;
        if (r7 == null) {
            return null;
        }
        int length = r7.length;
        Object[] objArr = new Object[length << 1];
        int i = 0;
        for (AnonymousClass1W9 r2 : r7) {
            int i2 = i + 1;
            objArr[i] = r2.A02;
            Jid jid = r2.A01;
            i = i2 + 1;
            if (jid != null) {
                objArr[i2] = jid;
            } else {
                objArr[i2] = r2.A03;
            }
        }
        return objArr;
    }

    public VoipStanzaChildNode[] getChildrenCopy() {
        VoipStanzaChildNode[] voipStanzaChildNodeArr = this.children;
        if (voipStanzaChildNodeArr != null) {
            return (VoipStanzaChildNode[]) Arrays.copyOf(voipStanzaChildNodeArr, voipStanzaChildNodeArr.length);
        }
        return null;
    }

    public byte[] getDataCopy() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return Arrays.copyOf(bArr, bArr.length);
        }
        return null;
    }

    public String getTag() {
        return this.tag;
    }

    public boolean hasAttribute(AnonymousClass1W9 r6) {
        AnonymousClass1W9[] r4 = this.attributes;
        if (r4 != null) {
            for (AnonymousClass1W9 r0 : r4) {
                if (r0.equals(r6)) {
                    return true;
                }
            }
        }
        return false;
    }

    public AnonymousClass1V8 toProtocolTreeNode() {
        int length;
        byte[] bArr = this.data;
        if (bArr != null) {
            return new AnonymousClass1V8(this.tag, bArr, this.attributes);
        }
        AnonymousClass1V8[] r6 = null;
        VoipStanzaChildNode[] voipStanzaChildNodeArr = this.children;
        if (voipStanzaChildNodeArr == null || (length = voipStanzaChildNodeArr.length) <= 0) {
            return new AnonymousClass1V8(this.tag, this.attributes, r6);
        }
        r6 = new AnonymousClass1V8[length];
        int i = 0;
        int i2 = 0;
        do {
            r6[i2] = voipStanzaChildNodeArr[i].toProtocolTreeNode();
            i++;
            i2++;
        } while (i < length);
        return new AnonymousClass1V8(this.tag, this.attributes, r6);
    }

    /* loaded from: classes2.dex */
    public class Builder {
        public Map attributes;
        public List children;
        public byte[] data;
        public final String tag;

        public Builder(String str) {
            this.tag = str;
        }

        private Builder addAttribute(String str, AnonymousClass1W9 r4) {
            Map map = this.attributes;
            if (map == null) {
                map = new HashMap();
                this.attributes = map;
            }
            if (map.put(str, r4) == null) {
                return this;
            }
            throw new IllegalArgumentException("node may not have duplicate attributes");
        }

        public Builder addAttribute(String str, Jid jid) {
            addAttribute(str, new AnonymousClass1W9(jid, str));
            return this;
        }

        public Builder addAttribute(String str, String str2) {
            addAttribute(str, new AnonymousClass1W9(str, str2));
            return this;
        }

        public Builder addAttributes(AnonymousClass1W9[] r5) {
            if (r5 != null) {
                for (AnonymousClass1W9 r0 : r5) {
                    addAttribute(r0.A02, r0.A03);
                }
            }
            return this;
        }

        public Builder addChild(VoipStanzaChildNode voipStanzaChildNode) {
            if (this.data == null) {
                List list = this.children;
                if (list == null) {
                    list = new ArrayList();
                    this.children = list;
                }
                list.add(voipStanzaChildNode);
                return this;
            }
            throw new IllegalArgumentException("node may not have both data and children");
        }

        public Builder addChildren(VoipStanzaChildNode[] voipStanzaChildNodeArr) {
            if (voipStanzaChildNodeArr != null) {
                for (VoipStanzaChildNode voipStanzaChildNode : voipStanzaChildNodeArr) {
                    addChild(voipStanzaChildNode);
                }
            }
            return this;
        }

        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0030 */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: X.1W9[] */
        /* JADX DEBUG: Multi-variable search result rejected for r5v1, resolved type: X.1W9[] */
        /* JADX DEBUG: Multi-variable search result rejected for r5v2, resolved type: X.1W9[] */
        /* JADX WARN: Multi-variable type inference failed */
        public VoipStanzaChildNode build() {
            AnonymousClass1W9[] r5;
            int size;
            Map map = this.attributes;
            VoipStanzaChildNode[] voipStanzaChildNodeArr = null;
            if (map == null || (size = map.size()) <= 0) {
                r5 = 0;
            } else {
                r5 = new AnonymousClass1W9[size];
                int i = 0;
                for (Map.Entry entry : this.attributes.entrySet()) {
                    r5[i] = entry.getValue();
                    i++;
                }
            }
            List list = this.children;
            if (list != null) {
                voipStanzaChildNodeArr = (VoipStanzaChildNode[]) list.toArray(new VoipStanzaChildNode[0]);
            }
            return new VoipStanzaChildNode(this.tag, r5, voipStanzaChildNodeArr, this.data);
        }

        public Builder setData(byte[] bArr) {
            if (this.children == null) {
                this.data = bArr;
                return this;
            }
            throw new IllegalArgumentException("node may not have both data and children");
        }
    }
}
