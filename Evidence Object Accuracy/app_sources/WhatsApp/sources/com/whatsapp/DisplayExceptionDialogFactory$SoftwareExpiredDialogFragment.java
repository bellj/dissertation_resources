package com.whatsapp;

import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.C14830m7;
import X.C20640w5;
import X.C21740xu;
import X.DialogC58312oc;
import X.DialogInterface$OnCancelListenerC96114es;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment extends Hilt_DisplayExceptionDialogFactory_SoftwareExpiredDialogFragment {
    public AnonymousClass12P A00;
    public C20640w5 A01;
    public C21740xu A02;
    public AnonymousClass01d A03;
    public C14830m7 A04;
    public AnonymousClass018 A05;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Log.w("home/dialog software-expired");
        ActivityC000900k A0C = A0C();
        C14830m7 r7 = this.A04;
        C21740xu r5 = this.A02;
        DialogC58312oc r1 = new DialogC58312oc(A0C, this.A00, this.A01, r5, this.A03, r7, this.A05);
        r1.setOnCancelListener(new DialogInterface$OnCancelListenerC96114es(A0C));
        return r1;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.finish();
        }
    }
}
