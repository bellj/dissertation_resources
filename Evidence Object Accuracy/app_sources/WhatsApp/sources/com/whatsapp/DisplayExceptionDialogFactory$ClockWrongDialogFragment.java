package com.whatsapp;

import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.C14830m7;
import X.C20640w5;
import X.DialogC58302ob;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Process;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$ClockWrongDialogFragment extends Hilt_DisplayExceptionDialogFactory_ClockWrongDialogFragment {
    public C20640w5 A00;
    public AnonymousClass01d A01;
    public C14830m7 A02;
    public AnonymousClass018 A03;
    public boolean A04 = true;

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (!this.A00.A03()) {
            A1B();
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Log.w("home/dialog clock-wrong");
        ActivityC000900k A0C = A0C();
        DialogC58302ob r1 = new DialogC58302ob(A0C, this.A00, this.A01, this.A02, this.A03);
        r1.setOnCancelListener(new DialogInterface.OnCancelListener(A0C) { // from class: X.4er
            public final /* synthetic */ Activity A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                this.A00.finish();
                Process.killProcess(Process.myPid());
            }
        });
        return r1;
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A04 = false;
        A1B();
        new DisplayExceptionDialogFactory$ClockWrongDialogFragment().A1F(A0C().A0V(), getClass().getName());
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.A04 && A0B() != null) {
            A0C().finish();
        }
    }
}
