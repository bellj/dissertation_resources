package com.whatsapp;

import X.AbstractC49172Jp;
import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P7;
import X.AnonymousClass4CK;
import X.C49152Jn;
import X.C49162Jo;
import X.C52582bL;
import X.C63623Ch;
import X.EnumC49142Jm;
import X.animation.Animation$AnimationListenerC102164oh;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/* loaded from: classes2.dex */
public class QrImageView extends View implements AnonymousClass004 {
    public static final Random A09 = new Random();
    public int A00;
    public Paint A01;
    public RectF A02;
    public Drawable A03;
    public C49162Jo A04;
    public AnonymousClass2P7 A05;
    public ArrayList A06;
    public boolean A07;
    public boolean A08;

    public QrImageView(Context context) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        this.A01 = new Paint();
        this.A02 = new RectF();
        A00(context, null);
    }

    public QrImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A01 = new Paint();
        this.A02 = new RectF();
        A00(context, attributeSet);
    }

    public QrImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        this.A01 = new Paint();
        this.A02 = new RectF();
        A00(context, attributeSet);
    }

    public QrImageView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        this.A01 = new Paint();
        this.A02 = new RectF();
        A00(context, attributeSet);
    }

    public QrImageView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0E);
            this.A08 = obtainStyledAttributes.getBoolean(1, true);
            this.A00 = obtainStyledAttributes.getInt(0, -16777216);
            this.A03 = obtainStyledAttributes.getDrawable(2);
            obtainStyledAttributes.recycle();
        }
        if (isInEditMode()) {
            try {
                this.A04 = C49152Jn.A00(EnumC49142Jm.A03, "This is a sample QR Code", null);
            } catch (AnonymousClass4CK e) {
                throw new RuntimeException(e);
            }
        }
    }

    public final void A01(AbstractC49172Jp r5) {
        ArrayList arrayList = this.A06;
        if (arrayList == null || arrayList.isEmpty()) {
            C63623Ch r0 = this.A04.A04;
            int i = r0.A01 * r0.A00;
            ArrayList arrayList2 = new ArrayList(i);
            this.A06 = arrayList2;
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(Integer.valueOf(i2));
            }
        }
        C52582bL r2 = new C52582bL(this);
        r2.setDuration(1200);
        r2.setInterpolator(new LinearInterpolator());
        if (r5 != null) {
            r2.setAnimationListener(new animation.Animation$AnimationListenerC102164oh(r5, this));
        }
        startAnimation(r2);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.A08 && this.A04 != null) {
            A01(null);
        }
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A06 = null;
        clearAnimation();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        C49162Jo r0 = this.A04;
        if (r0 != null) {
            C63623Ch r15 = r0.A04;
            int i = r15.A01;
            int i2 = r15.A00;
            RectF rectF = this.A02;
            float width = rectF.width() / ((float) i);
            float height = rectF.height() / ((float) i2);
            Paint paint = this.A01;
            paint.setColor(-1);
            canvas.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), paint);
            paint.setColor(this.A00);
            for (int i3 = 0; i3 < i; i3++) {
                for (int i4 = 0; i4 < i2; i4++) {
                    if (r15.A02[i4][i3] == 1) {
                        float f = rectF.left;
                        float f2 = rectF.top;
                        canvas.drawRect((float) ((int) ((((float) i3) * width) + f)), (float) ((int) ((((float) i4) * height) + f2)), f + (((float) (i3 + 1)) * width), f2 + (((float) (i4 + 1)) * height), paint);
                    }
                }
            }
            paint.setColor(-1);
            if (this.A06 != null && !isInEditMode()) {
                Iterator it = this.A06.iterator();
                while (it.hasNext()) {
                    int intValue = ((Number) it.next()).intValue();
                    int i5 = intValue % i;
                    int i6 = intValue / i;
                    float f3 = rectF.left;
                    float f4 = rectF.top;
                    canvas.drawRect((float) ((int) ((((float) i5) * width) + f3)), (float) ((int) ((((float) i6) * height) + f4)), f3 + (((float) (i5 + 1)) * width), f4 + (((float) (i6 + 1)) * height), paint);
                }
            }
            if (this.A03 != null) {
                ArrayList arrayList = this.A06;
                if (arrayList == null || arrayList.isEmpty() || isInEditMode()) {
                    this.A03.draw(canvas);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0044  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            super.onMeasure(r10, r11)
            int r6 = r9.getMeasuredWidth()
            int r0 = r9.getPaddingLeft()
            int r6 = r6 - r0
            int r0 = r9.getPaddingRight()
            int r6 = r6 - r0
            int r5 = r9.getMeasuredHeight()
            int r0 = r9.getPaddingTop()
            int r5 = r5 - r0
            int r0 = r9.getPaddingBottom()
            int r5 = r5 - r0
            r8 = 1073741824(0x40000000, float:2.0)
            r7 = 0
            if (r6 <= r5) goto L_0x0069
            int r0 = r6 - r5
            float r4 = (float) r0
            float r4 = r4 / r8
            r1 = r5
        L_0x0029:
            r0 = 0
        L_0x002a:
            android.graphics.RectF r3 = r9.A02
            float r2 = (float) r1
            r3.set(r7, r7, r2, r2)
            r3.offset(r4, r0)
            int r0 = r9.getPaddingLeft()
            float r1 = (float) r0
            int r0 = r9.getPaddingTop()
            float r0 = (float) r0
            r3.offset(r1, r0)
            android.graphics.drawable.Drawable r0 = r9.A03
            if (r0 == 0) goto L_0x0068
            r0 = 1049414861(0x3e8ccccd, float:0.275)
            float r2 = r2 * r0
            r1 = 1056964608(0x3f000000, float:0.5)
            float r2 = r2 + r1
            int r4 = (int) r2
            int r6 = r6 - r4
            float r0 = (float) r6
            float r0 = r0 / r8
            float r0 = r0 + r1
            int r3 = (int) r0
            int r0 = r9.getPaddingLeft()
            int r3 = r3 + r0
            int r5 = r5 - r4
            float r0 = (float) r5
            float r0 = r0 / r8
            float r0 = r0 + r1
            int r2 = (int) r0
            int r0 = r9.getPaddingTop()
            int r2 = r2 + r0
            int r1 = r3 + r4
            int r4 = r4 + r2
            android.graphics.drawable.Drawable r0 = r9.A03
            r0.setBounds(r3, r2, r1, r4)
        L_0x0068:
            return
        L_0x0069:
            if (r5 <= r6) goto L_0x0072
            int r0 = r5 - r6
            float r0 = (float) r0
            float r0 = r0 / r8
            r1 = r6
            r4 = 0
            goto L_0x002a
        L_0x0072:
            r1 = r6
            r4 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.QrImageView.onMeasure(int, int):void");
    }

    public void setQrCode(C49162Jo r2) {
        setQrCode(r2, null);
    }

    public void setQrCode(C49162Jo r2, AbstractC49172Jp r3) {
        this.A04 = r2;
        if (this.A08 && AnonymousClass028.A0q(this) && r2 != null) {
            A01(r3);
        } else if (r3 != null) {
            r3.AOS(this);
        }
    }
}
