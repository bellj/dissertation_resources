package com.whatsapp.perf.profilo;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractServiceC003701q;
import X.AnonymousClass004;
import X.AnonymousClass00C;
import X.AnonymousClass01J;
import X.AnonymousClass16A;
import X.AnonymousClass57N;
import X.AnonymousClass5B7;
import X.C14820m6;
import X.C18640sm;
import X.C18790t3;
import X.C18800t4;
import X.C19930uu;
import X.C28471Ni;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;

/* loaded from: classes2.dex */
public class ProfiloUploadService extends AbstractServiceC003701q implements AnonymousClass004 {
    public AbstractC15710nm A00;
    public C18790t3 A01;
    public C18640sm A02;
    public C14820m6 A03;
    public C18800t4 A04;
    public C19930uu A05;
    public AbstractC14440lR A06;
    public boolean A07;
    public final Object A08;
    public volatile C71083cM A09;

    public ProfiloUploadService() {
        this(0);
    }

    public ProfiloUploadService(int i) {
        this.A08 = new Object();
        this.A07 = false;
    }

    @Override // X.AbstractServiceC003801r
    public void A05(Intent intent) {
        File[] listFiles;
        int length;
        File file = new File(getCacheDir(), "profilo/upload");
        if (!(!file.exists() || (listFiles = file.listFiles(new FilenameFilter() { // from class: X.5BO
            @Override // java.io.FilenameFilter
            public final boolean accept(File file2, String str) {
                return str.endsWith(".log");
            }
        })) == null || (length = listFiles.length) == 0)) {
            for (int i = 1; i < length; i++) {
                listFiles[i].delete();
                listFiles[i].getPath();
            }
            File file2 = listFiles[0];
            if (this.A02.A05(true) == 1) {
                try {
                    C28471Ni r7 = new C28471Ni(this.A01, new AnonymousClass57N(this, file2), this.A04, "https://crashlogs.whatsapp.net/wa_profilo_data", this.A05.A00(), 7, false, false, false);
                    r7.A06("access_token", "1063127757113399|745146ffa34413f9dbb5469f5370b7af");
                    r7.A06("from", this.A00.A00());
                    r7.A05(new FileInputStream(file2), "file", file2.getName(), 0, file2.length());
                    AnonymousClass16A r3 = (AnonymousClass16A) this.A00;
                    r7.A06("agent", r3.A0D.A02(r3.A07, AnonymousClass00C.A01()));
                    r7.A06("build_id", String.valueOf(388062852L));
                    r7.A06("device_id", this.A03.A0A());
                    r7.A02(null);
                    return;
                } catch (Exception | OutOfMemoryError e) {
                    Log.w("ProfiloUpload/Error Uploading file", e);
                }
            }
            file2.delete();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A09 == null) {
            synchronized (this.A08) {
                if (this.A09 == null) {
                    this.A09 = new C71083cM(this);
                }
            }
        }
        return this.A09.generatedComponent();
    }

    @Override // X.AbstractServiceC003801r, android.app.Service
    public void onCreate() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A05 = (C19930uu) r1.AM5.get();
            this.A00 = (AbstractC15710nm) r1.A4o.get();
            this.A06 = (AbstractC14440lR) r1.ANe.get();
            this.A01 = (C18790t3) r1.AJw.get();
            this.A04 = (C18800t4) r1.AHs.get();
            this.A02 = (C18640sm) r1.A3u.get();
            this.A03 = (C14820m6) r1.AN3.get();
        }
        super.onCreate();
    }
}
