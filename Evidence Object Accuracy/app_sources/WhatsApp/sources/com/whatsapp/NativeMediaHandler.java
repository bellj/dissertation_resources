package com.whatsapp;

import X.C16590pI;

/* loaded from: classes2.dex */
public class NativeMediaHandler {
    public final C16590pI A00;

    public static native void initFileHandlingCallbacks(NativeMediaHandler nativeMediaHandler);

    public NativeMediaHandler(C16590pI r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055 A[Catch: all -> 0x007e, TRY_ENTER, TryCatch #1 {Exception -> 0x0085, blocks: (B:15:0x0047, B:20:0x007a, B:17:0x0055, B:19:0x0076), top: B:27:0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0076 A[Catch: all -> 0x007e, TRY_LEAVE, TryCatch #1 {Exception -> 0x0085, blocks: (B:15:0x0047, B:20:0x007a, B:17:0x0055, B:19:0x0076), top: B:27:0x0047 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int openFile(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            java.lang.String r5 = "; path="
            java.lang.String r4 = "; mode="
            r6 = 0
            if (r8 == 0) goto L_0x00a4
            if (r9 == 0) goto L_0x00a4
            r1 = r8
            java.lang.String r0 = "/mnt/content/"
            boolean r0 = r8.startsWith(r0)
            if (r0 == 0) goto L_0x0035
            java.lang.String r0 = "content://"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r0 = 13
            java.lang.String r0 = r8.substring(r0)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
        L_0x0026:
            android.net.Uri r3 = android.net.Uri.parse(r1)
        L_0x002a:
            java.lang.String r0 = "r+"
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x0047
            java.lang.String r9 = "rw"
            goto L_0x0047
        L_0x0035:
            java.lang.String r0 = "/"
            boolean r0 = r8.startsWith(r0)
            if (r0 == 0) goto L_0x0026
            java.io.File r0 = new java.io.File
            r0.<init>(r8)
            android.net.Uri r3 = android.net.Uri.fromFile(r0)
            goto L_0x002a
        L_0x0047:
            X.0pI r0 = r7.A00     // Catch: Exception -> 0x0085
            android.content.Context r0 = r0.A00     // Catch: Exception -> 0x0085
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch: Exception -> 0x0085
            android.os.ParcelFileDescriptor r2 = r0.openFileDescriptor(r3, r9)     // Catch: Exception -> 0x0085
            if (r2 != 0) goto L_0x0076
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x007e
            r1.<init>()     // Catch: all -> 0x007e
            java.lang.String r0 = "nativemediahandler/openFile failed, not opened; uri="
            r1.append(r0)     // Catch: all -> 0x007e
            r1.append(r3)     // Catch: all -> 0x007e
            r1.append(r4)     // Catch: all -> 0x007e
            r1.append(r9)     // Catch: all -> 0x007e
            r1.append(r5)     // Catch: all -> 0x007e
            r1.append(r8)     // Catch: all -> 0x007e
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x007e
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x007e
            return r6
        L_0x0076:
            int r0 = r2.detachFd()     // Catch: all -> 0x007e
            r2.close()     // Catch: Exception -> 0x0085
            return r0
        L_0x007e:
            r0 = move-exception
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch: all -> 0x0084
        L_0x0084:
            throw r0     // Catch: Exception -> 0x0085
        L_0x0085:
            r2 = move-exception
            java.lang.String r1 = "nativemediahandler/openFile failed; uri="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            r0.append(r4)
            r0.append(r9)
            r0.append(r5)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0, r2)
            return r6
        L_0x00a4:
            java.lang.String r0 = "nativemediahandler/openFile wrong arguments; path="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r8)
            java.lang.String r0 = " mode="
            r1.append(r0)
            r1.append(r9)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.NativeMediaHandler.openFile(java.lang.String, java.lang.String):int");
    }
}
