package com.whatsapp.avatar.home;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass12V;
import X.AnonymousClass3BT;
import X.AnonymousClass5K4;
import X.C1111658i;
import X.C113985Jt;
import X.C13000ix;
import X.C16700pc;
import X.C20980wd;
import X.C235812f;
import X.C27691It;
import X.C72113e2;
import X.C83893y4;
import X.C83903y5;
import X.C83913y6;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* loaded from: classes2.dex */
public final class AvatarHomeViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = new AnonymousClass016(C83893y4.A00);
    public final C1111658i A01;
    public final AnonymousClass12V A02;
    public final AnonymousClass3BT A03;
    public final C20980wd A04;
    public final C235812f A05;
    public final C27691It A06 = C13000ix.A03();
    public final C27691It A07 = C13000ix.A03();

    public AvatarHomeViewModel(AnonymousClass12V r4, AnonymousClass3BT r5, C20980wd r6, C235812f r7) {
        C16700pc.A0E(r7, 1);
        C16700pc.A0H(r4, r6);
        this.A05 = r7;
        this.A02 = r4;
        this.A04 = r6;
        this.A03 = r5;
        C1111658i r0 = new C1111658i(this);
        this.A01 = r0;
        r7.A01(1);
        r6.A03(r0);
        r4.A00(new AnonymousClass5K4(this));
    }

    public static final /* synthetic */ void A00(AvatarHomeViewModel avatarHomeViewModel, boolean z) {
        AnonymousClass016 r3 = avatarHomeViewModel.A00;
        Object A01 = r3.A01();
        if (!z) {
            avatarHomeViewModel.A05.A02(1);
            r3.A0B(new C83903y5(false));
        } else if ((A01 instanceof C83903y5) || C16700pc.A0O(A01, C83893y4.A00)) {
            avatarHomeViewModel.A05.A02(4);
            r3.A0B(new C83913y6(false, false));
            AnonymousClass3BT r5 = avatarHomeViewModel.A03;
            C72113e2 r4 = new C72113e2(avatarHomeViewModel);
            r5.A04.Ab6(new RunnableBRunnable0Shape3S0300000_I1(r5, new C113985Jt(), r4, 32));
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A04.A04(this.A01);
        this.A05.A00(1);
    }
}
