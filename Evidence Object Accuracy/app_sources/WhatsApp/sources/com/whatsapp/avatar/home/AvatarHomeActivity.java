package com.whatsapp.avatar.home;

import X.AbstractC005102i;
import X.AbstractC16710pd;
import X.AbstractC16850pr;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass03B;
import X.AnonymousClass17J;
import X.AnonymousClass18U;
import X.AnonymousClass1AG;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3CR;
import X.AnonymousClass3UX;
import X.AnonymousClass4GO;
import X.AnonymousClass4Yq;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15570nT;
import X.C16700pc;
import X.C18000rk;
import X.C18120rw;
import X.C18160s0;
import X.C18840t8;
import X.C235812f;
import X.C27631Ih;
import X.C41691tw;
import X.C52162aM;
import X.C63293Ba;
import X.C64173En;
import X.C65963Lt;
import X.C70973cB;
import X.C71843db;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.bottomsheet.LockableBottomSheetBehavior;
import com.whatsapp.components.FloatingActionButton;
import com.whatsapp.components.MainChildCoordinatorLayout;
import com.whatsapp.util.Log;
import java.util.Map;

/* loaded from: classes2.dex */
public final class AvatarHomeActivity extends ActivityC13790kL {
    public View A00;
    public View A01;
    public FrameLayout A02;
    public LinearLayout A03;
    public LinearLayout A04;
    public LinearLayout A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public CircularProgressBar A09;
    public AnonymousClass18U A0A;
    public WaButton A0B;
    public WaImageView A0C;
    public WaTextView A0D;
    public WaTextView A0E;
    public WaTextView A0F;
    public LockableBottomSheetBehavior A0G;
    public FloatingActionButton A0H;
    public MainChildCoordinatorLayout A0I;
    public C18120rw A0J;
    public C63293Ba A0K;
    public boolean A0L;
    public final AbstractC16710pd A0M;

    public AvatarHomeActivity() {
        this(0);
        this.A0M = AnonymousClass4Yq.A01(new C71843db(this));
    }

    public AvatarHomeActivity(int i) {
        this.A0L = false;
        ActivityC13830kP.A1P(this, 13);
    }

    @Override // X.ActivityC000800j
    public boolean A1h() {
        if (!A2f()) {
            return super.A1h();
        }
        return false;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(r2, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0A = C12990iw.A0T(A1M);
            this.A0J = A1M.A42();
            this.A0K = new C63293Ba(C12970iu.A0S(A1M), new AnonymousClass17J(C12960it.A0R(A1M)), C12980iv.A0b(A1M), (C235812f) A1M.A13.get(), (C18840t8) A1M.A1V.get(), C18000rk.A00(A1M.A0u), A1M.A14);
        }
    }

    public final void A2e(boolean z) {
        MainChildCoordinatorLayout mainChildCoordinatorLayout = this.A0I;
        if (mainChildCoordinatorLayout == null) {
            throw C16700pc.A06("coordinatorLayout");
        }
        mainChildCoordinatorLayout.post(new RunnableBRunnable0Shape1S0110000_I1(this, 1, z));
    }

    public final boolean A2f() {
        LockableBottomSheetBehavior lockableBottomSheetBehavior = this.A0G;
        if (lockableBottomSheetBehavior == null) {
            throw C16700pc.A06("bottomSheetBehavior");
        }
        int i = lockableBottomSheetBehavior.A0B;
        if (i != 3 && i != 5) {
            return false;
        }
        lockableBottomSheetBehavior.A0M(4);
        return true;
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!A2f()) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_avatar_home);
        this.A0I = (MainChildCoordinatorLayout) C16700pc.A00(this, R.id.coordinator);
        this.A03 = (LinearLayout) C16700pc.A00(this, R.id.avatar_home_sheet);
        this.A04 = (LinearLayout) C16700pc.A00(this, R.id.avatar_new_user_container);
        this.A02 = (FrameLayout) C16700pc.A00(this, R.id.avatar_set_container);
        this.A05 = (LinearLayout) C16700pc.A00(this, R.id.avatar_privacy);
        this.A01 = C16700pc.A00(this, R.id.avatar_bottom_sheet_padding);
        LinearLayout linearLayout = this.A03;
        if (linearLayout == null) {
            throw C16700pc.A06("containerAvatarSheet");
        }
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(linearLayout);
        if (A00 != null) {
            this.A0G = (LockableBottomSheetBehavior) A00;
            TextView textView = (TextView) C16700pc.A00(this, R.id.avatar_placeholder_subtitle);
            C52162aM.A00(textView);
            this.A08 = textView;
            this.A07 = (TextView) C16700pc.A00(this, R.id.avatar_placeholder_subtitle_learn_more);
            this.A06 = (TextView) C16700pc.A00(this, R.id.avatar_privacy_subtitle);
            WaImageView waImageView = (WaImageView) C16700pc.A00(this, R.id.avatar_set_image);
            C12960it.A10(waImageView, this, 19);
            this.A0C = waImageView;
            this.A09 = (CircularProgressBar) C16700pc.A00(this, R.id.avatar_set_progress);
            WaTextView waTextView = (WaTextView) C16700pc.A00(this, R.id.avatar_browse_stickers);
            C12960it.A10(waTextView, this, 25);
            this.A0D = waTextView;
            WaTextView waTextView2 = (WaTextView) C16700pc.A00(this, R.id.avatar_create_profile_photo);
            C12960it.A10(waTextView2, this, 20);
            this.A0E = waTextView2;
            WaTextView waTextView3 = (WaTextView) C16700pc.A00(this, R.id.avatar_delete);
            C12960it.A10(waTextView3, this, 22);
            this.A0F = waTextView3;
            this.A00 = C16700pc.A00(this, R.id.avatar_privacy_divider);
            WaButton waButton = (WaButton) C16700pc.A00(this, R.id.avatar_create_avatar_button);
            C12960it.A10(waButton, this, 21);
            this.A0B = waButton;
            FloatingActionButton floatingActionButton = (FloatingActionButton) C16700pc.A00(this, R.id.avatar_home_fab);
            C12960it.A10(floatingActionButton, this, 26);
            this.A0H = floatingActionButton;
            setTitle(R.string.avatars_title);
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0A(R.string.avatars_title);
                A1U.A0M(true);
            }
            String string = getString(R.string.avatar_home_privacy_subtitle);
            C16700pc.A0B(string);
            String string2 = getString(R.string.avatar_home_privacy_subtitle_link);
            C16700pc.A0B(string2);
            StringBuilder A0j = C12960it.A0j(string);
            A0j.append(' ');
            String A0d = C12960it.A0d(string2, A0j);
            SpannableString spannableString = new SpannableString(A0d);
            spannableString.setSpan(C12980iv.A0M(this, R.color.link_color), AnonymousClass03B.A03(A0d, string2, 0), AnonymousClass03B.A03(A0d, string2, 0) + string2.length(), 33);
            TextView textView2 = this.A06;
            if (textView2 == null) {
                throw C16700pc.A06("avatarPrivacySubtitleTextView");
            }
            textView2.setText(spannableString);
            LinearLayout linearLayout2 = this.A05;
            if (linearLayout2 == null) {
                throw C16700pc.A06("containerPrivacy");
            }
            C12960it.A10(linearLayout2, this, 24);
            AbstractC16710pd r13 = this.A0M;
            C12960it.A18(this, ((AvatarHomeViewModel) r13.getValue()).A00, 0);
            C12960it.A18(this, ((AvatarHomeViewModel) r13.getValue()).A06, 2);
            Log.i("AvatarHomeActivity/prefetching avatar editor");
            C63293Ba r11 = this.A0K;
            if (r11 != null) {
                if (((C18160s0) r11.A06.get()).A01() == null) {
                    Log.e("AvatarEditorPrefetchProxy: error while prefetching avatar editor due to missing access token");
                } else {
                    C235812f r2 = r11.A03;
                    r2.A03(null, null, null, 1);
                    long currentTimeMillis = System.currentTimeMillis();
                    AnonymousClass1AG r9 = (AnonymousClass1AG) r11.A05.get();
                    boolean A08 = C41691tw.A08(this);
                    C15570nT r3 = r11.A00;
                    r3.A08();
                    C27631Ih r32 = r3.A05;
                    C16700pc.A0C(r32);
                    String rawString = r32.getRawString();
                    C16700pc.A0B(rawString);
                    C65963Lt r15 = AnonymousClass4GO.A00;
                    AnonymousClass3CR r5 = new AnonymousClass3CR();
                    String str = r2.A01;
                    if (str == null) {
                        str = C12990iw.A0n();
                        r2.A01 = str;
                    }
                    C16700pc.A0C(str);
                    C16700pc.A0E(str, 1);
                    Map map = r5.A00;
                    map.put("logging_session_id", str);
                    map.put("logging_surface", "wa_settings");
                    map.put("logging_mechanism", "wa_settings_item");
                    String A002 = r5.A00();
                    AnonymousClass3UX r4 = new AnonymousClass3UX(r11, currentTimeMillis);
                    C64173En r33 = new C64173En(this, A0V(), r9.A00, rawString, null, A08);
                    C70973cB r22 = new C70973cB(r4, r33);
                    r9.A02.A00(this, r9.A01, r33);
                    AbstractC16850pr r14 = r9.A03;
                    r14.A02(r15, r22, null, "com.bloks.www.avatar.editor.cds.launcher.async", A002, null, r14.A00.contains("com.bloks.www.avatar.editor.cds.launcher.async"));
                }
                C12960it.A18(this, ((AvatarHomeViewModel) r13.getValue()).A07, 1);
                return;
            }
            throw C16700pc.A06("avatarEditorPrefetchProxy");
        }
        throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.bottomsheet.LockableBottomSheetBehavior<android.widget.LinearLayout?>");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        C16700pc.A0E(menuItem, 0);
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (A2f()) {
            return true;
        }
        finish();
        return true;
    }
}
