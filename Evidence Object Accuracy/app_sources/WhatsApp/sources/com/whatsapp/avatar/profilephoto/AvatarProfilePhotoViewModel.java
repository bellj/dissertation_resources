package com.whatsapp.avatar.profilephoto;

import X.AbstractC14440lR;
import X.AnonymousClass00T;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass10Z;
import X.AnonymousClass12V;
import X.AnonymousClass1WF;
import X.AnonymousClass3BT;
import X.AnonymousClass4JQ;
import X.AnonymousClass4JR;
import X.C113995Ju;
import X.C13000ix;
import X.C14830m7;
import X.C14900mE;
import X.C15570nT;
import X.C16700pc;
import X.C16770pj;
import X.C18170s1;
import X.C20980wd;
import X.C235812f;
import X.C27541Hx;
import X.C27691It;
import X.C58712rG;
import X.C58722rH;
import X.C69313Yt;
import X.C72153e6;
import X.EnumC870349y;
import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape1S0310000_I1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.List;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public final class AvatarProfilePhotoViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final AnonymousClass4JQ A03;
    public final AnonymousClass4JR A04;
    public final C69313Yt A05;
    public final C14830m7 A06;
    public final AnonymousClass12V A07;
    public final AnonymousClass3BT A08;
    public final C18170s1 A09;
    public final C20980wd A0A;
    public final C235812f A0B;
    public final AnonymousClass10Z A0C;
    public final C27691It A0D = C13000ix.A03();
    public final AbstractC14440lR A0E;
    public final List A0F;

    public AvatarProfilePhotoViewModel(C14900mE r22, C15570nT r23, AnonymousClass4JQ r24, AnonymousClass4JR r25, C14830m7 r26, AnonymousClass12V r27, AnonymousClass3BT r28, C18170s1 r29, C20980wd r30, C235812f r31, AnonymousClass10Z r32, AbstractC14440lR r33) {
        C16700pc.A0E(r26, 1);
        C16700pc.A0E(r22, 2);
        C16700pc.A0E(r23, 3);
        C16700pc.A0E(r33, 4);
        C16700pc.A0E(r31, 5);
        C16700pc.A0E(r32, 7);
        C16700pc.A0E(r27, 8);
        C16700pc.A0E(r30, 9);
        this.A06 = r26;
        this.A01 = r22;
        this.A02 = r23;
        this.A0E = r33;
        this.A0B = r31;
        this.A09 = r29;
        this.A0C = r32;
        this.A07 = r27;
        this.A0A = r30;
        this.A04 = r25;
        this.A03 = r24;
        this.A08 = r28;
        AnonymousClass1WF r17 = AnonymousClass1WF.A00;
        this.A00 = new AnonymousClass016(new C27541Hx(null, null, r17, r17, false, false));
        Context context = r25.A00.A00;
        this.A0F = C16770pj.A0I(new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_1), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_1), true), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_2), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_2), false), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_3), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_3), false), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_4), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_4), false), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_5), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_5), false), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_6), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_6), false), new C58722rH(AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_7), AnonymousClass00T.A00(context.getApplicationContext(), R.color.group_editor_color_highlight_1), false));
        C69313Yt r0 = new C69313Yt(this);
        this.A05 = r0;
        r30.A03(r0);
        List A0I = C16770pj.A0I(new C58712rG(Integer.valueOf(AnonymousClass00T.A00(this.A04.A00.A00.getApplicationContext(), R.color.group_editor_color_highlight_1)), true), new C58712rG(null, false), new C58712rG(null, false), new C58712rG(null, false), new C58712rG(null, false));
        List<C58722rH> list = this.A0F;
        for (C58722rH r14 : list) {
            if (r14.A02) {
                this.A00.A0B(new C27541Hx(r14, null, A0I, list, false, true));
                if (r27.A01()) {
                    A04(false, 0);
                    return;
                } else if (this.A09.A00.A01() != null) {
                    this.A0D.A0B(EnumC870349y.A01);
                    return;
                } else {
                    Log.e("AvatarProfilePhotoViewModel/showAvatarEditor error while prefetching avatar editor due to missing access token");
                    return;
                }
            }
        }
        throw new NoSuchElementException("Collection contains no element matching the predicate.");
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0A.A04(this.A05);
    }

    public final void A04(boolean z, int i) {
        AnonymousClass3BT r2 = this.A08;
        C72153e6 r4 = new C72153e6(this, i);
        r2.A04.Ab6(new RunnableBRunnable0Shape1S0310000_I1(r2, new C113995Ju(), r4, 1, z));
    }
}
