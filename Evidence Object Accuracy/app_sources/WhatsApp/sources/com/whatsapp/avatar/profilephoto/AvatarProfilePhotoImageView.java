package com.whatsapp.avatar.profilephoto;

import X.AbstractC16710pd;
import X.AbstractC37201lg;
import X.AnonymousClass4G4;
import X.AnonymousClass4Yq;
import X.AnonymousClass5JG;
import X.AnonymousClass5JH;
import X.AnonymousClass5JI;
import X.AnonymousClass5JJ;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C16700pc;
import X.EnumC870249x;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.whatsapp.CircleWaImageView;

/* loaded from: classes2.dex */
public final class AvatarProfilePhotoImageView extends CircleWaImageView {
    public float A00;
    public int A01;
    public final Paint A02;
    public final Paint A03;
    public final Paint A04;
    public final AbstractC16710pd A05 = AnonymousClass4Yq.A01(new AnonymousClass5JG(this));
    public final AbstractC16710pd A06 = AnonymousClass4Yq.A01(new AnonymousClass5JH(this));
    public final AbstractC16710pd A07 = AnonymousClass4Yq.A01(new AnonymousClass5JI(this));
    public final AbstractC16710pd A08 = AnonymousClass4Yq.A01(new AnonymousClass5JJ(this));

    public AvatarProfilePhotoImageView(Context context) {
        super(context, null);
        Paint A0F = C12990iw.A0F();
        A0F.setColor(getBorderColorIdle());
        A0F.setStrokeWidth(getBorderStrokeWidthIdle());
        A0F.setStyle(Paint.Style.STROKE);
        A0F.setAntiAlias(true);
        A0F.setDither(true);
        this.A03 = A0F;
        this.A02 = AbstractC37201lg.A00(this);
        Paint paint = new Paint();
        paint.setColor(getColorNeutral());
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setDither(true);
        this.A04 = paint;
        A05(null);
    }

    public AvatarProfilePhotoImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Paint A0F = C12990iw.A0F();
        A0F.setColor(getBorderColorIdle());
        A0F.setStrokeWidth(getBorderStrokeWidthIdle());
        A0F.setStyle(Paint.Style.STROKE);
        A0F.setAntiAlias(true);
        A0F.setDither(true);
        this.A03 = A0F;
        this.A02 = AbstractC37201lg.A00(this);
        Paint paint = new Paint();
        paint.setColor(getColorNeutral());
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setDither(true);
        this.A04 = paint;
        A05(attributeSet);
    }

    public AvatarProfilePhotoImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Paint A0F = C12990iw.A0F();
        A0F.setColor(getBorderColorIdle());
        A0F.setStrokeWidth(getBorderStrokeWidthIdle());
        A0F.setStyle(Paint.Style.STROKE);
        A0F.setAntiAlias(true);
        A0F.setDither(true);
        this.A03 = A0F;
        this.A02 = AbstractC37201lg.A00(this);
        Paint paint = new Paint();
        paint.setColor(getColorNeutral());
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setDither(true);
        this.A04 = paint;
        A05(attributeSet);
    }

    public final void A04() {
        Paint paint = this.A03;
        paint.setColor(getBorderColorIdle());
        paint.setStrokeWidth(getBorderStrokeWidthIdle());
        this.A04.setStrokeWidth(0.0f);
        this.A00 = getBorderStrokeWidthIdle();
        invalidate();
    }

    public final void A05(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, AnonymousClass4G4.A00);
        C16700pc.A0B(obtainStyledAttributes);
        if (obtainStyledAttributes.hasValue(0)) {
            this.A01 = obtainStyledAttributes.getInt(0, this.A01);
        }
        obtainStyledAttributes.recycle();
    }

    public final void A06(EnumC870249x r4, float f, int i) {
        float f2;
        float f3;
        C16700pc.A0E(r4, 0);
        Paint paint = this.A03;
        int ordinal = r4.ordinal();
        switch (ordinal) {
            case 0:
                break;
            default:
                throw C12990iw.A0v();
            case 1:
                i = getBorderColorIdle();
                break;
        }
        paint.setColor(i);
        switch (ordinal) {
            case 0:
                f2 = getBorderStrokeWidthSelected();
                break;
            case 1:
                f2 = getBorderStrokeWidthIdle();
                break;
            default:
                throw C12990iw.A0v();
        }
        paint.setStrokeWidth(f2);
        Paint paint2 = this.A04;
        switch (ordinal) {
            case 0:
                break;
            default:
                throw C12990iw.A0v();
            case 1:
                f = 0.0f;
                break;
        }
        paint2.setStrokeWidth(f);
        switch (ordinal) {
            case 0:
                f3 = getBorderStrokeWidthSelected();
                break;
            case 1:
                f3 = getBorderStrokeWidthIdle();
                break;
            default:
                throw C12990iw.A0v();
        }
        this.A00 = f3;
        invalidate();
    }

    private final int getBorderColorIdle() {
        return C12960it.A05(this.A05.getValue());
    }

    private final float getBorderStrokeWidthIdle() {
        return ((Number) this.A06.getValue()).floatValue();
    }

    private final float getBorderStrokeWidthSelected() {
        return ((Number) this.A07.getValue()).floatValue();
    }

    private final int getColorNeutral() {
        return C12960it.A05(this.A08.getValue());
    }

    @Override // com.whatsapp.CircleWaImageView, com.whatsapp.WaImageView, android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        C16700pc.A0E(canvas, 0);
        int width = getWidth() >> 1;
        float f = (float) width;
        float A00 = (float) C13000ix.A00(this);
        float min = (float) (Math.min(C12960it.A04(this, getWidth()), C12960it.A03(this)) >> 1);
        canvas.drawCircle(f, A00, min, this.A02);
        super.onDraw(canvas);
        Paint paint = this.A04;
        if (paint.getStrokeWidth() > 0.0f) {
            canvas.drawCircle(f, A00, min - this.A00, paint);
        }
        canvas.drawCircle(f, A00, min, this.A03);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize;
        int i3 = this.A01;
        if (i3 == 0) {
            defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
        } else if (i3 == 1) {
            defaultSize = ImageView.getDefaultSize(getSuggestedMinimumHeight(), i2);
        } else {
            throw C12970iu.A0f(C16700pc.A08("Illegal value: ", Integer.valueOf(i3)));
        }
        setMeasuredDimension(defaultSize, defaultSize);
    }

    public final void setAvatarPoseBackgroundColor(int i) {
        this.A02.setColor(i);
        invalidate();
    }

    public final void setAvatarPoseBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap);
    }

    public static /* synthetic */ void setBorderStyle$default(AvatarProfilePhotoImageView avatarProfilePhotoImageView, EnumC870249x r2, int i, float f, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            f = 0.0f;
        }
        avatarProfilePhotoImageView.A06(r2, f, i);
    }
}
