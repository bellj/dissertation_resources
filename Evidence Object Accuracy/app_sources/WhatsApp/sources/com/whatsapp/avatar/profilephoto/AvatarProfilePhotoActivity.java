package com.whatsapp.avatar.profilephoto;

import X.AbstractC005102i;
import X.AbstractC16710pd;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass02H;
import X.AnonymousClass0B6;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass3NU;
import X.AnonymousClass4Yq;
import X.C12960it;
import X.C12980iv;
import X.C16700pc;
import X.C18120rw;
import X.C27541Hx;
import X.C41691tw;
import X.C54102gB;
import X.C58702rF;
import X.C58722rH;
import X.C71853dc;
import X.C72123e3;
import X.C72133e4;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.BidiToolbar;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class AvatarProfilePhotoActivity extends ActivityC13790kL {
    public MenuItem A00;
    public View A01;
    public ProgressBar A02;
    public ShimmerFrameLayout A03;
    public BidiToolbar A04;
    public WaButton A05;
    public AvatarProfilePhotoImageView A06;
    public C18120rw A07;
    public boolean A08;
    public final C54102gB A09;
    public final C54102gB A0A;
    public final AbstractC16710pd A0B;

    public AvatarProfilePhotoActivity() {
        this(0);
        this.A0B = AnonymousClass4Yq.A01(new C71853dc(this));
        this.A0A = new C54102gB(new C72133e4(this));
        this.A09 = new C54102gB(new C72123e3(this));
    }

    public AvatarProfilePhotoActivity(int i) {
        this.A08 = false;
        ActivityC13830kP.A1P(this, 14);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A07 = A1M.A42();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        View view;
        super.onCreate(bundle);
        setContentView(R.layout.activity_avatar_profile_photo);
        BidiToolbar bidiToolbar = (BidiToolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        A1e(bidiToolbar);
        bidiToolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), ((ActivityC13830kP) this).A01));
        bidiToolbar.setTitle(R.string.avatar_profile_photo_title);
        this.A04 = bidiToolbar;
        C41691tw.A02(this, R.color.gallery_status_bar_background);
        C41691tw.A07(getWindow(), !C41691tw.A08(this));
        WaButton waButton = (WaButton) AnonymousClass00T.A05(this, R.id.avatar_profile_photo_options);
        C12960it.A10(waButton, this, 27);
        this.A05 = waButton;
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I(getString(R.string.avatar_profile_photo_title));
        }
        C54102gB r2 = this.A0A;
        RecyclerView recyclerView = (RecyclerView) AnonymousClass00T.A05(this, R.id.avatar_pose_recycler);
        recyclerView.setAdapter(r2);
        recyclerView.setItemAnimator(null);
        recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager() { // from class: com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity$setupRecyclerView$1$1
            @Override // X.AnonymousClass02H
            public boolean A18(AnonymousClass0B6 r3) {
                C16700pc.A0E(r3, 0);
                ((ViewGroup.MarginLayoutParams) r3).width = (int) (((float) ((AnonymousClass02H) this).A03) * 0.2f);
                return true;
            }
        });
        C54102gB r22 = this.A09;
        RecyclerView recyclerView2 = (RecyclerView) AnonymousClass00T.A05(this, R.id.avatar_color_recycler);
        recyclerView2.setAdapter(r22);
        recyclerView2.setItemAnimator(null);
        recyclerView2.getContext();
        recyclerView2.setLayoutManager(new LinearLayoutManager() { // from class: com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity$setupRecyclerView$1$1
            @Override // X.AnonymousClass02H
            public boolean A18(AnonymousClass0B6 r3) {
                C16700pc.A0E(r3, 0);
                ((ViewGroup.MarginLayoutParams) r3).width = (int) (((float) ((AnonymousClass02H) this).A03) * 0.2f);
                return true;
            }
        });
        this.A06 = (AvatarProfilePhotoImageView) AnonymousClass00T.A05(this, R.id.avatar_pose);
        this.A01 = AnonymousClass00T.A05(this, R.id.pose_layout);
        this.A02 = (ProgressBar) AnonymousClass00T.A05(this, R.id.profile_image_progress);
        this.A03 = (ShimmerFrameLayout) AnonymousClass00T.A05(this, R.id.pose_shimmer);
        AbstractC16710pd r23 = this.A0B;
        C12960it.A18(this, ((AvatarProfilePhotoViewModel) r23.getValue()).A00, 4);
        C12960it.A18(this, ((AvatarProfilePhotoViewModel) r23.getValue()).A0D, 3);
        if (C12980iv.A0H(this).orientation == 2 && (view = this.A01) != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NU(view, this));
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) {
            getMenuInflater().inflate(R.menu.avatar_profile_photo, menu);
            MenuItem findItem = menu.findItem(R.id.menu_avatar_profile_photo_save);
            this.A00 = findItem;
            if (findItem != null) {
                findItem.setIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_done, R.color.icon_secondary), ((ActivityC13830kP) this).A01));
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        C58702rF r3;
        C16700pc.A0E(menuItem, 0);
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_avatar_profile_photo_save) {
            AvatarProfilePhotoViewModel avatarProfilePhotoViewModel = (AvatarProfilePhotoViewModel) this.A0B.getValue();
            Log.i("AvatarProfilePhotoViewModel/onSavePhotoClicked()");
            AnonymousClass016 r1 = avatarProfilePhotoViewModel.A00;
            C27541Hx r0 = (C27541Hx) r1.A01();
            C58722rH r4 = null;
            if (r0 == null) {
                r3 = null;
            } else {
                r3 = r0.A01;
            }
            C27541Hx r02 = (C27541Hx) r1.A01();
            if (r02 != null) {
                r4 = r02.A00;
            }
            if (r3 == null || r4 == null) {
                Log.i("AvatarProfilePhotoViewModel/onSavePhotoClicked(null value)");
            } else {
                r1.A0B(C27541Hx.A00(null, null, C16700pc.A04(r1), null, null, 62, true, false));
                avatarProfilePhotoViewModel.A0E.Ab6(new RunnableBRunnable0Shape3S0300000_I1(avatarProfilePhotoViewModel, r3, r4, 7));
            }
        } else if (itemId == 16908332) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
