package com.whatsapp.avatar.profilephoto;

import X.AbstractC16710pd;
import X.AnonymousClass00T;
import X.AnonymousClass4Yq;
import X.AnonymousClass5JE;
import X.AnonymousClass5JF;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C16700pc;
import X.C51012Sk;
import X.EnumC870249x;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class AvatarProfilePhotoColorView extends View {
    public EnumC870249x A00;
    public final Paint A01;
    public final Paint A02;
    public final AbstractC16710pd A03;
    public final AbstractC16710pd A04;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public AvatarProfilePhotoColorView(Context context) {
        this(context, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AvatarProfilePhotoColorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C16700pc.A0E(context, 1);
        this.A03 = AnonymousClass4Yq.A01(new AnonymousClass5JE(this));
        this.A04 = AnonymousClass4Yq.A01(new AnonymousClass5JF(this));
        this.A00 = EnumC870249x.A01;
        Paint A0F = C12990iw.A0F();
        A0F.setStrokeWidth(getBorderStrokeWidthSelected());
        C12990iw.A13(A0F);
        A0F.setAntiAlias(true);
        A0F.setDither(true);
        this.A02 = A0F;
        Paint A0F2 = C12990iw.A0F();
        C12970iu.A16(AnonymousClass00T.A00(context, R.color.secondary_text), A0F2);
        A0F2.setAntiAlias(true);
        A0F2.setDither(true);
        this.A01 = A0F2;
    }

    public /* synthetic */ AvatarProfilePhotoColorView(Context context, AttributeSet attributeSet, int i, C51012Sk r5) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    private final float getBorderStrokeWidthSelected() {
        return ((Number) this.A03.getValue()).floatValue();
    }

    private final float getSelectedBorderMargin() {
        return ((Number) this.A04.getValue()).floatValue();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        float f;
        C16700pc.A0E(canvas, 0);
        int width = getWidth() >> 1;
        int A00 = C13000ix.A00(this);
        float min = ((float) Math.min(C12960it.A04(this, getWidth()), C12960it.A03(this))) / 2.0f;
        EnumC870249x r0 = this.A00;
        EnumC870249x r4 = EnumC870249x.A02;
        if (r0 == r4) {
            f = min - getSelectedBorderMargin();
        } else {
            f = min;
        }
        float f2 = (float) width;
        float f3 = (float) A00;
        canvas.drawCircle(f2, f3, f, this.A01);
        if (this.A00 == r4) {
            canvas.drawCircle(f2, f3, min, this.A02);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize = View.getDefaultSize(getSuggestedMinimumHeight(), i2);
        setMeasuredDimension(defaultSize, defaultSize);
    }
}
