package com.whatsapp.avatar.di;

import X.C127195u6;
import X.C127535ue;
import X.C16700pc;
import X.C16790pl;
import X.C17830rS;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public final class AvatarBloksModule {
    public static final Set A00(C17830rS r6) {
        Set singleton = Collections.singleton(Pattern.compile("com\\.bloks\\.www\\.(avatar)(\\.[0-9a-zA-Z_]+)+"));
        C16700pc.A0B(singleton);
        Set singleton2 = Collections.singleton(new C16790pl(singleton, new C127535ue(null, r6, new C127195u6(null, 3651100555017197L))));
        C16700pc.A0B(singleton2);
        return singleton2;
    }
}
