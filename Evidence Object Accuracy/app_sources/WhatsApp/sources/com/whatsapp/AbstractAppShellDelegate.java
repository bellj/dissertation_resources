package com.whatsapp;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC18730sv;
import X.AnonymousClass006;
import X.AnonymousClass009;
import X.AnonymousClass00B;
import X.AnonymousClass00C;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01K;
import X.AnonymousClass01M;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass025;
import X.AnonymousClass11R;
import X.AnonymousClass12O;
import X.AnonymousClass143;
import X.AnonymousClass1DP;
import X.AnonymousClass1HM;
import X.AnonymousClass1HO;
import X.AnonymousClass1HP;
import X.AnonymousClass1HR;
import X.AnonymousClass1HS;
import X.AnonymousClass1HT;
import X.AnonymousClass1HU;
import X.AnonymousClass1HV;
import X.AnonymousClass1HW;
import X.AnonymousClass1HX;
import X.AnonymousClass1HY;
import X.AnonymousClass1HZ;
import X.C003401m;
import X.C003601o;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14950mJ;
import X.C15210mk;
import X.C15550nR;
import X.C16120oU;
import X.C18070rr;
import X.C18280sC;
import X.C18640sm;
import X.C19350ty;
import X.C19390u2;
import X.C19980v1;
import X.C20220vP;
import X.C20640w5;
import X.C20890wU;
import X.C21030wi;
import X.C21820y2;
import X.C21840y4;
import X.C21940yE;
import X.C22050yP;
import X.C247716u;
import X.C27071Fx;
import X.C27261Gq;
import X.C27511Hu;
import X.C27531Hw;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Looper;
import android.os.MessageQueue;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.AbstractAppShellDelegate;
import com.whatsapp.breakpad.BreakpadManager;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.security.Security;

/* loaded from: classes.dex */
public class AbstractAppShellDelegate implements ApplicationLike {
    public static final String COMPRESSED_LIBS_ARCHIVE_NAME = "libs.spk.zst";
    public final Context appContext;
    public final AnonymousClass006 appStartStat;
    public C18070rr applicationCreatePerfTracker;
    public boolean asyncInitStarted;
    public AnonymousClass018 whatsAppLocale;

    public AbstractAppShellDelegate(Context context, AnonymousClass006 r2) {
        this.appContext = context;
        this.appStartStat = r2;
    }

    public static void A00(Context context) {
        AnonymousClass01J r0 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        C20640w5 r12 = (C20640w5) r0.AHk.get();
        C15550nR r11 = (C15550nR) r0.A45.get();
        AnonymousClass01d r10 = (AnonymousClass01d) r0.ALI.get();
        C22050yP r9 = (C22050yP) r0.A7v.get();
        C21840y4 r8 = (C21840y4) r0.ACr.get();
        AnonymousClass12O r7 = (AnonymousClass12O) r0.AMG.get();
        C20220vP r6 = (C20220vP) r0.AC3.get();
        C247716u r5 = (C247716u) r0.AD0.get();
        C21820y2 r4 = (C21820y2) r0.AHx.get();
        C21940yE r3 = (C21940yE) r0.AMa.get();
        C18640sm A7Z = r0.A7Z();
        AnonymousClass1DP r2 = (AnonymousClass1DP) r0.A4N.get();
        A02(context, (AnonymousClass11R) r0.A93.get(), r12, (C18280sC) r0.A1O.get(), r11, r2, A7Z, r5, (AnonymousClass143) r0.AFr.get(), r10, r0.Aen(), r0.A3L(), r9, r3, r6, r8, r4, r7, (C14860mA) r0.ANU.get());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0357, code lost:
        if (r49.A00 == null) goto L_0x0359;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.content.Context r81) {
        /*
        // Method dump skipped, instructions count: 2495
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.AbstractAppShellDelegate.A01(android.content.Context):void");
    }

    public static void A02(Context context, AnonymousClass11R r15, C20640w5 r16, C18280sC r17, C15550nR r18, AnonymousClass1DP r19, C18640sm r20, C247716u r21, AnonymousClass143 r22, AnonymousClass01d r23, C14830m7 r24, C14850m9 r25, C22050yP r26, C21940yE r27, C20220vP r28, C21840y4 r29, C21820y2 r30, AnonymousClass12O r31, C14860mA r32) {
        C003401m.A01("AppAsyncInit/BroadcastReceiver");
        new RunnableBRunnable0Shape0S0400000_I0(new AnonymousClass1HR(r17, r22), context, r23, r25, 8).run();
        new RunnableBRunnable0Shape5S0200000_I0_5(new AnonymousClass1HS(r20, r21, r23, r24, r25, r26, r27), 23, context).run();
        boolean z = !AnonymousClass1HT.A00(r23);
        AnonymousClass1HT.A04 = z;
        r30.A05(z);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        context.registerReceiver(AnonymousClass1HT.A05, intentFilter);
        context.registerReceiver(r15, new IntentFilter("android.intent.action.HEADSET_PLUG"));
        new RunnableBRunnable0Shape15S0100000_I1_1(r29, 39).run();
        context.registerReceiver(new AnonymousClass1HU(r28, r32), new IntentFilter("com.whatsapp.alarm.WEB_RENOTIFY"), AnonymousClass01V.A09, null);
        context.registerReceiver(new AnonymousClass1HV(r16), new IntentFilter("android.intent.action.TIME_SET"));
        context.registerReceiver(new AnonymousClass1HW(), new IntentFilter("android.intent.action.TIMEZONE_CHANGED"));
        context.registerReceiver(new AnonymousClass1HX(r31), new IntentFilter("android.intent.action.LOCALE_CHANGED"));
        context.registerReceiver(new AnonymousClass1HY(r18), new IntentFilter("android.intent.action.LOCALE_CHANGED"));
        r19.A0I.A00.registerReceiver(new AnonymousClass1HZ(r19.A05), new IntentFilter("android.intent.action.LOCALE_CHANGED"));
        C003401m.A00();
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: SSATransform
        java.lang.IndexOutOfBoundsException: bitIndex < 0: -126
        	at java.util.BitSet.get(BitSet.java:623)
        	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.fillBasicBlockInfo(LiveVarAnalysis.java:65)
        	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.runAnalysis(LiveVarAnalysis.java:36)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:55)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static void A03(android.content.Context r129, boolean r130) {
        /*
        // Method dump skipped, instructions count: 17538
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.AbstractAppShellDelegate.A03(android.content.Context, boolean):void");
    }

    private boolean decompressAsset(C21030wi r12, C14830m7 r13, boolean z, C16120oU r15, C14950mJ r16, C14820m6 r17, AbstractC15710nm r18) {
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            if (!r12.A04(this.appContext, COMPRESSED_LIBS_ARCHIVE_NAME, 0, false, z)) {
                return true;
            }
            C27071Fx r2 = new C27071Fx();
            r2.A01 = COMPRESSED_LIBS_ARCHIVE_NAME;
            r2.A00 = Long.valueOf(SystemClock.uptimeMillis() - uptimeMillis);
            r15.A07(r2);
            return true;
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("AbstractAppShellDelegate/decompressLibraries: Error decompressing archive libs.spk.zst, usingLegacyMethod: ");
            sb.append(z);
            Log.w(sb.toString(), e);
            maybeReportDecompressionFailure(r16, e, r17, r18);
            return false;
        }
    }

    private void decompressLibraries(WhatsAppLibLoader whatsAppLibLoader, C21030wi r11, C14830m7 r12, AbstractC15710nm r13, C16120oU r14, C14950mJ r15, C14820m6 r16) {
        if (whatsAppLibLoader.A04(this.appContext)) {
            r11.A01(this.appContext);
            if (!decompressAsset(r11, r12, false, r14, r15, r16, r13) && decompressAsset(r11, r12, true, r14, r15, r16, r13)) {
                r13.AaV("AbstractAppShellDelegate/decompressLibraries/fallback", null, false);
                return;
            }
            return;
        }
        Log.e("AbstractAppShellDelegate/decompressLibraries: Could not load decompressor libraries");
    }

    public C18070rr getApplicationCreatePerfTracker() {
        C18070rr r0 = this.applicationCreatePerfTracker;
        AnonymousClass009.A05(r0);
        return r0;
    }

    private void initCrashHandling(C19350ty r1, AbstractC18730sv r2) {
        r1.A03(r2);
        C003601o.A01(r1);
    }

    private void initLogging(C18640sm r2) {
        Log.connectivityInfoProvider = new AnonymousClass1HM(r2);
    }

    private void initStartupPathPerfLogging(AnonymousClass01K r4) {
        this.applicationCreatePerfTracker = r4.A61();
        getApplicationCreatePerfTracker().A02(this.appStartStat.A02);
        getApplicationCreatePerfTracker().A01();
    }

    private void installAnrDetector(C20890wU r4, WhatsAppLibLoader whatsAppLibLoader, C15210mk r6, C19980v1 r7) {
        getApplicationCreatePerfTracker().A04("InstallAnrDetector");
        if (whatsAppLibLoader.A05(this.appContext)) {
            r4.A02(new RunnableBRunnable0Shape0S0100000_I0(this, 18), "breakpad");
            r4.A02(new RunnableBRunnable0Shape0S0000000_I0(0), "abort_hook");
            r4.A02(new RunnableBRunnable0Shape0S0100000_I0(r6, 19), "anr_detector");
        }
        JniBridge.setDependencies(r7);
        getApplicationCreatePerfTracker().A03("InstallAnrDetector");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$installAnrDetector$0() {
        BreakpadManager.A00(this.appContext);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$queueAsyncInit$2(C19390u2 r4, AnonymousClass01K r5) {
        AnonymousClass1HO A00 = r4.A00();
        try {
            r5.A5z().A00();
            A01(this.appContext);
        } finally {
            A00.A00(null);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$queueAsyncInit$3(AnonymousClass01K r5, boolean z) {
        Log.i("app-init main thread idle");
        synchronized (this) {
            if (!this.asyncInitStarted) {
                this.asyncInitStarted = true;
                AbstractC14440lR Ag3 = r5.Ag3();
                C19390u2 AZ9 = r5.AZ9();
                if (z) {
                    A00(this.appContext);
                }
                Ag3.Ab2(new RunnableBRunnable0Shape0S0300000_I0(this, AZ9, r5, 2));
            }
        }
        return false;
    }

    private void logDebugInfo() {
        StringBuilder sb = new StringBuilder("AbstractAppShellDelegate/debug_info: pkg=");
        sb.append(this.appContext.getPackageName());
        sb.append("; v=");
        sb.append(AnonymousClass00C.A01());
        sb.append("; vc=");
        sb.append(221770000);
        sb.append("; p=");
        sb.append("consumer");
        sb.append("; e=");
        sb.append(180L);
        sb.append("; g=");
        sb.append("smb-v2.22.18.1-1-g55a55a79eba");
        sb.append("; t=");
        sb.append(1659716253000L);
        sb.append("; d=");
        sb.append(Build.MANUFACTURER);
        sb.append(" ");
        sb.append(Build.MODEL);
        sb.append("; os=Android ");
        sb.append(Build.VERSION.RELEASE);
        sb.append("; abis=");
        sb.append(TextUtils.join(",", AnonymousClass1HP.A03()));
        Log.i(sb.toString());
    }

    private void maybeReportDecompressionFailure(C14950mJ r5, Exception exc, C14820m6 r7, AbstractC15710nm r8) {
        StringBuilder sb = new StringBuilder("AbstractAppShellDelegate/maybeReportDecompressionFailure: available internal storage: ");
        sb.append(r5.A02());
        Log.i(sb.toString());
        Log.e("AbstractAppShellDelegate/maybeReportDecompressionFailure", exc);
        if (r7.A1J("decompression_failure_reported_timestamp", 86400000)) {
            r8.AaV("AbstractAppShellDelegate/maybeReportDecompressionFailure", "superpack decompression failed", true);
            r7.A0k("decompression_failure_reported_timestamp");
        }
    }

    @Override // com.whatsapp.ApplicationLike
    public void onConfigurationChanged(Configuration configuration) {
        AnonymousClass018 r0 = this.whatsAppLocale;
        AnonymousClass009.A05(r0);
        r0.A0Q(configuration);
        AnonymousClass018 r02 = this.whatsAppLocale;
        AnonymousClass009.A05(r02);
        r02.A0L();
        C27531Hw.A04();
    }

    /* JADX INFO: finally extract failed */
    @Override // com.whatsapp.ApplicationLike
    public void onCreate() {
        AnonymousClass01K r1 = (AnonymousClass01K) AnonymousClass01M.A00(this.appContext, AnonymousClass01K.class);
        initLogging(r1.A7Z());
        Log.i("AbstractAppShellDelegate/onCreate");
        AbstractC15710nm A7p = r1.A7p();
        AnonymousClass00B r4 = Log.LOGGER_THREAD;
        synchronized (r4) {
            r4.A00 = A7p;
        }
        initCrashHandling(r1.A7q(), r1.A6o());
        initStartupPathPerfLogging(r1);
        logDebugInfo();
        getApplicationCreatePerfTracker().A04("DecompressLibraries");
        decompressLibraries(r1.Ag7(), r1.Ag6(), r1.Aen(), r1.A7p(), r1.Ag5(), r1.AeW(), r1.Ag2());
        getApplicationCreatePerfTracker().A03("DecompressLibraries");
        installAnrDetector(r1.Ab8(), r1.Ag7(), r1.A5y(), r1.AKK());
        r1.AeO().A01();
        r1.AeO().A08("app_creation_on_create");
        r1.AAF().A00(this.appContext.getString(R.string.gcm_defaultSenderId));
        setBouncyCastleProvider();
        setStrictModePolicyForAppInit();
        C003401m.A01("AppShell/onCreate");
        try {
            C14850m9 A5U = r1.A5U();
            C27261Gq.A01(A5U.A07(334));
            this.whatsAppLocale = r1.Ag8();
            C14820m6 Ag2 = r1.Ag2();
            getApplicationCreatePerfTracker().A04("ConfigProdDependencies");
            getApplicationCreatePerfTracker().A03("ConfigProdDependencies");
            getApplicationCreatePerfTracker().A04("MainThreadInit");
            boolean A07 = A5U.A07(1762);
            A03(this.appContext, A07);
            getApplicationCreatePerfTracker().A03("MainThreadInit");
            AnonymousClass009.A01 = false;
            AnonymousClass009.A00.open();
            queueAsyncInit(r1, A07);
            C003401m.A00();
            AnonymousClass025.A00(Ag2.A04());
            getApplicationCreatePerfTracker().A00();
            r1.AeO().A07("app_creation_on_create");
        } catch (Throwable th) {
            C003401m.A00();
            throw th;
        }
    }

    private void queueAsyncInit(AnonymousClass01K r3, boolean z) {
        Looper.myQueue().addIdleHandler(new MessageQueue.IdleHandler(r3, this, z) { // from class: X.1HQ
            public final /* synthetic */ AnonymousClass01K A00;
            public final /* synthetic */ AbstractAppShellDelegate A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // android.os.MessageQueue.IdleHandler
            public final boolean queueIdle() {
                return this.A01.lambda$queueAsyncInit$3(this.A00, this.A02);
            }
        });
    }

    private void setBouncyCastleProvider() {
        getApplicationCreatePerfTracker().A04("SetBouncyCastleProvider");
        C27511Hu r2 = new C27511Hu();
        if (Build.VERSION.SDK_INT < 19) {
            Security.insertProviderAt(r2, 1);
        } else {
            Security.addProvider(r2);
        }
        getApplicationCreatePerfTracker().A03("SetBouncyCastleProvider");
    }

    private void setStrictModePolicyForAppInit() {
        getApplicationCreatePerfTracker().A04("SetStrictModePolicyForAppInit");
        getApplicationCreatePerfTracker().A03("SetStrictModePolicyForAppInit");
    }
}
