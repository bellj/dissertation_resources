package com.whatsapp.util;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass1PE;
import X.AnonymousClass1V0;
import X.AnonymousClass31K;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14960mK;
import X.C15580nU;
import X.C15600nX;
import X.C19990v2;
import X.C244415n;
import X.C75273ja;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.whatsapp.components.InviteViaLinkView;
import com.whatsapp.growthlock.InviteLinkUnavailableDialogFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape3S0400000_I1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public ViewOnClickCListenerShape3S0400000_I1(Object obj, Object obj2, Object obj3, Object obj4, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A03 = obj2;
        this.A02 = obj3;
        this.A01 = obj4;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AnonymousClass1V0 r0;
        Intent A0b;
        switch (this.A04) {
            case 0:
                AnonymousClass31K r1 = (AnonymousClass31K) this.A03;
                if (r1 != null) {
                    r1.A0B = Boolean.TRUE;
                }
                AbstractC14640lm r4 = (AbstractC14640lm) this.A02;
                Object obj = this.A00;
                C19990v2 r02 = ((InviteViaLinkView) obj).A00;
                AnonymousClass009.A05(r4);
                AnonymousClass1PE A06 = r02.A06(r4);
                if (A06 == null || (r0 = A06.A0b) == null || r0.A00 != 1) {
                    View view2 = (View) obj;
                    Context context = view2.getContext();
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(context.getPackageName(), "com.whatsapp.shareinvitelink.ShareInviteLinkActivity");
                    C12980iv.A13(A0A, r4);
                    C12990iw.A10(A0A, view2);
                    return;
                }
                ((ActivityC13810kN) this.A01).Adm(InviteLinkUnavailableDialogFragment.A00(false, true));
                return;
            case 1:
                AbstractC14640lm r12 = ((AbstractC15340mz) this.A02).A0z.A00;
                AnonymousClass009.A05(r12);
                ((C244415n) this.A01).A08((Context) this.A00, r12, (UserJid) this.A03);
                return;
            case 2:
                Context context2 = ((View) this.A03).getContext();
                boolean A0D = ((C15600nX) this.A01).A0D((GroupJid) this.A02);
                C75273ja r03 = (C75273ja) this.A00;
                if (A0D) {
                    A0b = C14960mK.A0J(context2, r03.A00);
                } else {
                    C15580nU r2 = r03.A00;
                    A0b = C14960mK.A0b(context2, null, 9);
                    A0b.putExtra("parent_group_jid_to_link", r2.getRawString());
                }
                context2.startActivity(A0b, null);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
