package com.whatsapp.util;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass01F;
import X.AnonymousClass01N;
import X.AnonymousClass12N;
import X.AnonymousClass12O;
import X.C06790Vc;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C38211ni;
import X.C68023Tu;
import X.C90814Pi;
import android.view.View;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.calling.views.PermissionDialogFragment;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape3S0110000_I1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public boolean A01;
    public final int A02;

    public ViewOnClickCListenerShape3S0110000_I1(Object obj, int i, boolean z) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = z;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        switch (this.A02) {
            case 0:
                Log.i("UserNoticeBanner/update/banner dismissed");
                if (!this.A01) {
                    ((C68023Tu) this.A00).A05.A04();
                }
                C68023Tu r4 = (C68023Tu) this.A00;
                r4.A04.A01(10);
                View view2 = r4.A00;
                AnonymousClass009.A03(view2);
                view2.setVisibility(8);
                AnonymousClass12O r0 = r4.A05;
                r0.A06();
                AnonymousClass12N r1 = r0.A08;
                C12970iu.A1C(r1.A00().edit(), "current_user_notice_banner_dismiss_timestamp", r0.A01.A00());
                AnonymousClass01N r2 = r4.A06;
                if (r2.get() != null) {
                    r4.A01.A01((C90814Pi) r2.get());
                    return;
                }
                return;
            case 1:
                boolean z = this.A01;
                PermissionDialogFragment permissionDialogFragment = (PermissionDialogFragment) this.A00;
                if (z) {
                    permissionDialogFragment.A0A = true;
                    C38211ni.A05(permissionDialogFragment.A0C());
                    return;
                }
                permissionDialogFragment.A01.dismiss();
                C14820m6 r02 = permissionDialogFragment.A08;
                String[] strArr = permissionDialogFragment.A0D;
                RequestPermissionActivity.A0P(r02, strArr);
                if (permissionDialogFragment.A0F != null) {
                    AnonymousClass01F A0F = permissionDialogFragment.A0F();
                    if (A0F.A02 != null) {
                        A0F.A0D.addLast(new C06790Vc(permissionDialogFragment.A0T, 100));
                        A0F.A02.A00(null, strArr);
                        return;
                    }
                    return;
                }
                StringBuilder A0k = C12960it.A0k("Fragment ");
                A0k.append(permissionDialogFragment);
                throw C12960it.A0U(C12960it.A0d(" not attached to Activity", A0k));
            default:
                super.A04(view);
                return;
        }
    }
}
