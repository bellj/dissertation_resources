package com.whatsapp.util;

import X.AbstractC42671vd;
import X.AbstractC454421p;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass19N;
import X.AnonymousClass2TT;
import X.C12970iu;
import X.C12990iw;
import X.C44691zO;
import X.C52702bX;
import X.C55142hr;
import X.C75543k1;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.whatsapp.CatalogImageListActivity;
import com.whatsapp.CatalogMediaView;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0101000_I1 extends AbstractView$OnClickListenerC34281fs {
    public int A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape2S0101000_I1(Object obj, int i, int i2) {
        this.A02 = i2;
        this.A01 = obj;
        this.A00 = i;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        CatalogCarouselDetailImageView catalogCarouselDetailImageView;
        switch (this.A02) {
            case 0:
                Context context = view.getContext();
                C75543k1 r0 = (C75543k1) this.A01;
                CatalogImageListActivity catalogImageListActivity = r0.A03;
                C44691zO r7 = catalogImageListActivity.A05;
                AnonymousClass2TT r4 = r0.A02;
                int i = this.A00;
                UserJid userJid = catalogImageListActivity.A09;
                Intent A0D = C12990iw.A0D(context, CatalogMediaView.class);
                A0D.putExtra("product", r7);
                A0D.putExtra("target_image_index", i);
                A0D.putExtra("cached_jid", userJid.getRawString());
                AbstractC454421p.A07(context, A0D, view);
                AbstractC454421p.A08(context, A0D, view, r4, AbstractC42671vd.A0Z(AnonymousClass19N.A00(i, r7.A0D)));
                catalogImageListActivity.A06.A03(catalogImageListActivity.A09, 28, catalogImageListActivity.A05.A0D, 9);
                return;
            case 1:
                C55142hr r1 = (C55142hr) this.A01;
                ThumbnailButton thumbnailButton = r1.A01;
                if (thumbnailButton.getTag(R.id.loaded_image_url) != null) {
                    catalogCarouselDetailImageView = r1.A02;
                    String str = catalogCarouselDetailImageView.A02.A0D;
                    int i2 = this.A00;
                    thumbnailButton.setTag(AnonymousClass19N.A00(i2, str));
                    Context context2 = catalogCarouselDetailImageView.getContext();
                    UserJid userJid2 = catalogCarouselDetailImageView.A08;
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(context2.getPackageName(), "com.whatsapp.CatalogMediaView");
                    A0A.putExtra("target_image_index", i2);
                    A0A.putExtra("cached_jid", userJid2.getRawString());
                    A0A.putExtra("product", catalogCarouselDetailImageView.A02);
                    AbstractC454421p.A07(catalogCarouselDetailImageView.getContext(), A0A, thumbnailButton);
                    AbstractC454421p.A08(catalogCarouselDetailImageView.getContext(), A0A, thumbnailButton, new AnonymousClass2TT(catalogCarouselDetailImageView.getContext()), AbstractC42671vd.A0Z(AnonymousClass19N.A00(i2, catalogCarouselDetailImageView.A02.A0D)));
                    break;
                } else {
                    return;
                }
            case 2:
                C55142hr r12 = (C55142hr) this.A01;
                ThumbnailButton thumbnailButton2 = r12.A01;
                if (thumbnailButton2.getTag(R.id.loaded_image_url) != null) {
                    catalogCarouselDetailImageView = r12.A02;
                    Activity A02 = AnonymousClass12P.A02(catalogCarouselDetailImageView);
                    String str2 = catalogCarouselDetailImageView.A02.A0D;
                    int i3 = this.A00;
                    AnonymousClass028.A0k(thumbnailButton2, AbstractC42671vd.A0Z(AnonymousClass19N.A00(i3, str2)));
                    Context context3 = catalogCarouselDetailImageView.getContext();
                    UserJid userJid3 = catalogCarouselDetailImageView.A08;
                    Intent A0A2 = C12970iu.A0A();
                    A0A2.setClassName(context3.getPackageName(), "com.whatsapp.CatalogImageListActivity");
                    A0A2.putExtra("image_index", i3);
                    A0A2.putExtra("cached_jid", userJid3.getRawString());
                    A0A2.putExtra("product", catalogCarouselDetailImageView.A02);
                    A02.startActivity(A0A2, AbstractC454421p.A05(A02, thumbnailButton2, AnonymousClass028.A0J(thumbnailButton2)));
                    break;
                } else {
                    return;
                }
            case 3:
                C52702bX r13 = (C52702bX) this.A01;
                r13.A00 = this.A00;
                r13.notifyDataSetChanged();
                return;
            default:
                super.A04(view);
                return;
        }
        catalogCarouselDetailImageView.A03.A03(catalogCarouselDetailImageView.A08, 25, catalogCarouselDetailImageView.A02.A0D, 6);
    }
}
