package com.whatsapp.util;

import X.AbstractC16130oV;
import X.AbstractC454421p;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC000800j;
import X.AnonymousClass01T;
import X.AnonymousClass12P;
import X.AnonymousClass1KS;
import X.AnonymousClass2y2;
import X.AnonymousClass3DB;
import X.AnonymousClass4AE;
import X.AnonymousClass4N7;
import X.AnonymousClass4N8;
import X.AnonymousClass4RT;
import X.AnonymousClass4T7;
import X.C018108l;
import X.C12960it;
import X.C30211Wn;
import X.C53432eP;
import X.C55082hl;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0201000_I1 extends AbstractView$OnClickListenerC34281fs {
    public int A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ViewOnClickCListenerShape2S0201000_I1(Object obj, Object obj2, int i, int i2) {
        this.A03 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = i;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        Bundle A03;
        switch (this.A03) {
            case 0:
                AnonymousClass4N8 r4 = (AnonymousClass4N8) this.A01;
                AnonymousClass4RT r3 = r4.A01;
                r3.A01 = 2;
                r3.A02 = new AnonymousClass4N7((C30211Wn) this.A02, this.A00);
                r4.A00.A0A(r3);
                return;
            case 1:
                DirectorySetNeighborhoodViewModel directorySetNeighborhoodViewModel = (DirectorySetNeighborhoodViewModel) this.A01;
                directorySetNeighborhoodViewModel.A01.A0B(AnonymousClass4AE.HIDE_SEARCH);
                AnonymousClass4T7 r42 = (AnonymousClass4T7) this.A02;
                if (r42.A05.isEmpty()) {
                    directorySetNeighborhoodViewModel.A07.Ab2(new RunnableBRunnable0Shape1S0201000_I1(r42, directorySetNeighborhoodViewModel, this.A00, 6));
                    return;
                }
                directorySetNeighborhoodViewModel.A05(r42);
                return;
            case 2:
                AnonymousClass3DB r9 = (AnonymousClass3DB) this.A01;
                AnonymousClass2y2 r8 = r9.A05;
                Intent A1N = r8.A1N();
                ActivityC000800j r32 = (ActivityC000800j) AnonymousClass12P.A01(r8.getContext(), ActivityC000800j.class);
                if (!AbstractC454421p.A00) {
                    A03 = null;
                } else {
                    ArrayList A0l = C12960it.A0l();
                    int i = this.A00;
                    if (i < 3 || r8.A05.size() == 4) {
                        A1N.putExtra("start_index", i);
                        r9.A00((AbstractC16130oV) r8.A05.get(i), A0l);
                    } else {
                        int i2 = 0;
                        do {
                            ((AnonymousClass3DB) r8.A0K.get(i2)).A00((AbstractC16130oV) r8.A05.get(i2), A0l);
                            i2++;
                        } while (i2 < 3);
                    }
                    A03 = C018108l.A02(r32, (AnonymousClass01T[]) A0l.toArray(new AnonymousClass01T[0])).A03();
                    r32.A0k(new C53432eP(r32, this));
                }
                r32.startActivity(A1N, A03);
                return;
            case 3:
                ((Activity) this.A01).startActivityForResult((Intent) this.A02, this.A00);
                return;
            case 4:
                C55082hl r0 = (C55082hl) this.A01;
                r0.A06.AWl((AnonymousClass1KS) this.A02, r0.A08, this.A00);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
