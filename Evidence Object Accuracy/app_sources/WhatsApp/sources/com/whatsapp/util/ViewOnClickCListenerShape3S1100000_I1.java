package com.whatsapp.util;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass1OY;
import X.AnonymousClass34a;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape3S1100000_I1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public String A01;
    public final int A02;

    public ViewOnClickCListenerShape3S1100000_I1(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        switch (this.A02) {
            case 0:
                String str = this.A01;
                Uri parse = Uri.parse(str);
                if (parse.getScheme() == null) {
                    parse = Uri.parse(C12960it.A0d(str, C12960it.A0k("http://")));
                }
                try {
                    C12990iw.A10(C12970iu.A0B(parse), (View) this.A00);
                    return;
                } catch (ActivityNotFoundException unused) {
                    ((AnonymousClass1OY) this.A00).A0J.A07(R.string.activity_not_found, 0);
                    return;
                }
            case 1:
                AnonymousClass34a r0 = (AnonymousClass34a) this.A00;
                r0.A01.Ab9(r0.getContext(), Uri.parse(this.A01));
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
