package com.whatsapp.util;

import X.C12960it;
import android.system.ErrnoException;
import android.system.Os;
import java.io.File;

/* loaded from: classes3.dex */
public class BaseMediaFileUtils$OsRename {
    public static int attempt(File file, File file2) {
        try {
            Os.rename(file.getAbsolutePath(), file2.getAbsolutePath());
            return -1;
        } catch (ErrnoException e) {
            Log.e(C12960it.A0f(C12960it.A0k("MMS Os.rename also failed, errno="), e.errno), e);
            return e.errno;
        }
    }
}
