package com.whatsapp.util;

import X.AnonymousClass1IF;
import X.AnonymousClass1JM;
import X.AnonymousClass1JN;
import X.AnonymousClass1JO;
import X.C14580lf;
import X.C18850tA;
import X.C27351Gz;
import X.ExecutorC27271Gr;
import java.util.Map;

/* loaded from: classes2.dex */
public class RunnableTRunnableShape13S0200000_I0 extends AnonymousClass1IF {
    public Object A00;
    public Object A01;
    public final int A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(C14580lf r2, C18850tA r3) {
        super("SyncManager/doPreCompanionLogoutTask");
        this.A02 = 4;
        this.A00 = r3;
        this.A01 = r2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(C18850tA r2, C27351Gz r3) {
        super("SyncManager/onLocaleChanged");
        this.A02 = 3;
        this.A00 = r2;
        this.A01 = r3;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(C18850tA r2, Map map) {
        super("SyncManager/prepareAndSendRequest");
        this.A02 = 1;
        this.A00 = r2;
        this.A01 = map;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(ExecutorC27271Gr r1, Runnable runnable, String str, int i) {
        super(str);
        this.A02 = i;
        this.A00 = r1;
        this.A01 = runnable;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(AnonymousClass1JM r2, C18850tA r3) {
        super("SyncManager/CriticalDataUploadManager/startObserver");
        this.A02 = 2;
        this.A00 = r3;
        this.A01 = r2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RunnableTRunnableShape13S0200000_I0(AnonymousClass1JN r2, AnonymousClass1JO r3) {
        super("SyncManager/onDeviceRemoved");
        this.A02 = 0;
        this.A00 = r2;
        this.A01 = r3;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r21v6, types: [java.util.List] */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // java.lang.Runnable
    public void run() {
        /*
        // Method dump skipped, instructions count: 2828
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.RunnableTRunnableShape13S0200000_I0.run():void");
    }
}
