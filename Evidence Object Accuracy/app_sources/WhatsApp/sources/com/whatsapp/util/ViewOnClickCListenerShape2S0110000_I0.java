package com.whatsapp.util;

import X.AbstractActivityC33001d7;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass19Z;
import X.AnonymousClass1SF;
import X.C14820m6;
import X.C15370n3;
import X.C16970q3;
import android.view.View;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallLogActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.quickcontact.QuickContactActivity;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0110000_I0 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public boolean A01;
    public final int A02;

    public ViewOnClickCListenerShape2S0110000_I0(Object obj, int i, boolean z) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = z;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        switch (this.A02) {
            case 0:
                CallLogActivity callLogActivity = (CallLogActivity) this.A00;
                AnonymousClass19Z r4 = callLogActivity.A0S;
                C15370n3 r3 = callLogActivity.A0K;
                if (r4.A03(callLogActivity, GroupJid.of(callLogActivity.A0O), AnonymousClass1SF.A0C(((ActivityC13790kL) callLogActivity).A01, callLogActivity.A0B, callLogActivity.A0J, r3), 4, this.A01) == 0) {
                    callLogActivity.finish();
                    return;
                }
                return;
            case 1:
                AbstractActivityC33001d7 r32 = (AbstractActivityC33001d7) this.A00;
                r32.A2i();
                if (this.A01) {
                    C16970q3.A00(r32, r32.findViewById(R.id.content), r32.A01, r32.A2g());
                    return;
                }
                r32.Adl(MuteDialogFragment.A00(r32.A2g()), null);
                return;
            case 2:
                QuickContactActivity quickContactActivity = (QuickContactActivity) this.A00;
                if (AnonymousClass1SF.A0P(((ActivityC13810kN) quickContactActivity).A0C)) {
                    C14820m6 r1 = ((ActivityC13810kN) quickContactActivity).A09;
                    C15370n3 r5 = quickContactActivity.A0M;
                    boolean z = this.A01;
                    if (r1.A00.getInt("call_confirmation_dialog_count", 0) < 5 || r5.A0K()) {
                        AnonymousClass1SF.A0G(quickContactActivity, r5, 7, z);
                        return;
                    }
                }
                boolean z2 = this.A01;
                AnonymousClass19Z r52 = quickContactActivity.A0h;
                C15370n3 r33 = quickContactActivity.A0M;
                if (r52.A03(quickContactActivity, quickContactActivity.A0R, AnonymousClass1SF.A0C(((ActivityC13790kL) quickContactActivity).A01, quickContactActivity.A0B, quickContactActivity.A0K, r33), 7, z2) == 0) {
                    quickContactActivity.A2g(false);
                    return;
                }
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
