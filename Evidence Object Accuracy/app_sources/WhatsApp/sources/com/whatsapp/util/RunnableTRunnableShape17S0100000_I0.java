package com.whatsapp.util;

import X.AnonymousClass1IF;
import X.C18850tA;

/* loaded from: classes2.dex */
public class RunnableTRunnableShape17S0100000_I0 extends AnonymousClass1IF {
    public Object A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RunnableTRunnableShape17S0100000_I0(X.C18850tA r2, int r3) {
        /*
            r1 = this;
            if (r3 == 0) goto L_0x000a
            java.lang.String r0 = "SyncManager/scheduleSync"
        L_0x0004:
            r1.A00 = r2
            r1.<init>(r0)
            return
        L_0x000a:
            java.lang.String r0 = "SyncManager/onFMessagePeerSent"
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.RunnableTRunnableShape17S0100000_I0.<init>(X.0tA, int):void");
    }

    @Override // java.lang.Runnable
    public void run() {
        ((C18850tA) this.A00).A0G();
    }
}
