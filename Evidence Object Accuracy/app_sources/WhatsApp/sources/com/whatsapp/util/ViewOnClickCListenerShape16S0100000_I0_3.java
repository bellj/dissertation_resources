package com.whatsapp.util;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.C14310lE;
import X.C14830m7;
import X.C15580nU;
import X.C15650ng;
import X.C16170oZ;
import X.C18120rw;
import X.C22680zT;
import X.C30041Vv;
import X.C627038g;
import X.C88124Ej;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.status.playback.widget.StatusEditText;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;
import com.whatsapp.twofactor.SetEmailFragment;
import com.whatsapp.twofactor.TwoFactorAuthActivity;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape16S0100000_I0_3 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape16S0100000_I0_3(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        TextView textView;
        int i;
        switch (this.A01) {
            case 0:
                StickerStorePackPreviewActivity stickerStorePackPreviewActivity = (StickerStorePackPreviewActivity) this.A00;
                stickerStorePackPreviewActivity.A0I.A02(5);
                C18120rw.A00(stickerStorePackPreviewActivity, stickerStorePackPreviewActivity.A0G);
                return;
            case 1:
                TextStatusComposerActivity textStatusComposerActivity = (TextStatusComposerActivity) this.A00;
                C14310lE r2 = textStatusComposerActivity.A0G;
                String str = r2.A05;
                textStatusComposerActivity.A0X = str;
                textStatusComposerActivity.A0U.A03 = str;
                r2.A09(str);
                textStatusComposerActivity.A0D = null;
                textStatusComposerActivity.A2f();
                if (textStatusComposerActivity.A0c) {
                    StatusEditText statusEditText = textStatusComposerActivity.A0T;
                    statusEditText.A02 = false;
                    statusEditText.A0F();
                    return;
                }
                return;
            case 2:
                SetEmailFragment setEmailFragment = (SetEmailFragment) this.A00;
                Log.i("setemailfragment/submit");
                int i2 = setEmailFragment.A00;
                if (i2 != 1) {
                    if (i2 == 2) {
                        TwoFactorAuthActivity twoFactorAuthActivity = setEmailFragment.A06;
                        if (TextUtils.equals(twoFactorAuthActivity.A04, twoFactorAuthActivity.A05)) {
                            setEmailFragment.A06.A2e();
                            return;
                        } else {
                            textView = setEmailFragment.A04;
                            i = R.string.two_factor_auth_email_mismatch;
                        }
                    } else {
                        return;
                    }
                } else if (Patterns.EMAIL_ADDRESS.matcher(setEmailFragment.A06.A04).matches()) {
                    setEmailFragment.A06.A2g(SetEmailFragment.A00(2), true);
                    return;
                } else {
                    textView = setEmailFragment.A04;
                    i = R.string.two_factor_auth_email_incorrect_format;
                }
                textView.setText(i);
                return;
            case 3:
                C88124Ej.A00(((UserNoticeBottomSheetDialogFragment) this.A00).A08);
                return;
            case 4:
                ViewSharedContactArrayActivity viewSharedContactArrayActivity = (ViewSharedContactArrayActivity) this.A00;
                AbstractC14440lR r1 = ((ActivityC13830kP) viewSharedContactArrayActivity).A05;
                C14830m7 r6 = ((ActivityC13790kL) viewSharedContactArrayActivity).A05;
                C16170oZ r5 = viewSharedContactArrayActivity.A01;
                AnonymousClass018 r7 = viewSharedContactArrayActivity.A0A;
                C15650ng r8 = viewSharedContactArrayActivity.A0C;
                C22680zT r4 = viewSharedContactArrayActivity.A00;
                AbstractC14640lm r9 = viewSharedContactArrayActivity.A0E;
                AnonymousClass009.A05(r9);
                ArrayList arrayList = viewSharedContactArrayActivity.A0N;
                ArrayList arrayList2 = viewSharedContactArrayActivity.A0O;
                boolean booleanExtra = viewSharedContactArrayActivity.getIntent().getBooleanExtra("has_number_from_url", false);
                r1.Aaz(new C627038g(r4, r5, r6, r7, r8, r9, C15580nU.A04(viewSharedContactArrayActivity.getIntent().getStringExtra("quoted_group_jid")), C30041Vv.A09(viewSharedContactArrayActivity.getIntent().getBundleExtra("quoted_message")), viewSharedContactArrayActivity, arrayList, arrayList2, booleanExtra), new Void[0]);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
