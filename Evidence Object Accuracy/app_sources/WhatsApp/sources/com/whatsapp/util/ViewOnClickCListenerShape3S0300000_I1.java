package com.whatsapp.util;

import X.AbstractC15340mz;
import X.AbstractC15590nW;
import X.AbstractC35611iN;
import X.AbstractC37061lF;
import X.AbstractC61152zU;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass03U;
import X.AnonymousClass12P;
import X.AnonymousClass1JV;
import X.AnonymousClass1OY;
import X.AnonymousClass283;
import X.AnonymousClass2T3;
import X.AnonymousClass3E9;
import X.AnonymousClass3IJ;
import X.AnonymousClass3M5;
import X.AnonymousClass4KD;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14650lo;
import X.C14960mK;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C16590pI;
import X.C248917h;
import X.C27631Ih;
import X.C28861Ph;
import X.C30721Yo;
import X.C36161jQ;
import X.C41341tN;
import X.C41421tV;
import X.C48142Em;
import X.C48152En;
import X.C48172Ep;
import X.C53262da;
import X.C53882fY;
import X.C60082vr;
import X.C61122zR;
import X.C61132zS;
import X.C64343Fe;
import X.C84483zN;
import X.C84493zO;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.biz.order.view.fragment.OrderDetailFragment;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.community.AddGroupsToCommunityViewModel;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import java.util.ArrayList;
import java.util.Set;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape3S0300000_I1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ViewOnClickCListenerShape3S0300000_I1(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        C36161jQ r0;
        Set set;
        switch (this.A03) {
            case 0:
                C53262da r2 = (C53262da) this.A00;
                r2.A0A.AW8((int[]) this.A01);
                r2.dismiss();
                ((View) this.A02).setSelected(true);
                return;
            case 1:
                int A00 = ((AnonymousClass03U) this.A00).A00();
                if (A00 != -1) {
                    ((CartFragment) this.A01).A1P(((C84493zO) ((AbstractC37061lF) this.A02).ACa(A00)).A00.A01.A0D);
                    return;
                }
                return;
            case 2:
                Context context = view.getContext();
                ((AnonymousClass12P) this.A01).A06(context, C14960mK.A0R(context, (UserJid) this.A02, 9, true));
                return;
            case 3:
                int A002 = ((AnonymousClass03U) this.A00).A00();
                if (A002 != -1) {
                    AnonymousClass3M5 r02 = ((C84483zN) ((AbstractC37061lF) this.A01).ACa(A002)).A00;
                    OrderDetailFragment orderDetailFragment = (OrderDetailFragment) this.A02;
                    String str = r02.A06;
                    orderDetailFragment.A06.A01(orderDetailFragment.A0K, null, null, 46, null, null, str, orderDetailFragment.A0Q, null, null, 39);
                    C53882fY r03 = orderDetailFragment.A0D;
                    Context A01 = orderDetailFragment.A01();
                    AnonymousClass283.A02(A01, C14960mK.A0c(A01, false), r03.A08, null, null, str, 9, false);
                    return;
                }
                return;
            case 4:
                C48142Em r4 = (C48142Em) this.A02;
                if (r4 instanceof C48152En) {
                    BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = ((AnonymousClass3E9) this.A01).A01;
                    C48152En r42 = (C48152En) r4;
                    businessDirectorySearchQueryViewModel.A0K(r42, 48);
                    businessDirectorySearchQueryViewModel.A0L(r42);
                    return;
                } else if (r4 instanceof C48172Ep) {
                    C48172Ep r43 = (C48172Ep) r4;
                    BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel2 = ((AnonymousClass3E9) this.A01).A01;
                    businessDirectorySearchQueryViewModel2.A0K(r43, 58);
                    ((C48142Em) r43).A00 = System.currentTimeMillis();
                    businessDirectorySearchQueryViewModel2.A0Q.A00(r43);
                    C60082vr r04 = (C60082vr) this.A00;
                    r04.A01.A01(r04.A0H, null, null, r43.A02);
                    return;
                } else {
                    return;
                }
            case 5:
                C15370n3 r3 = (C15370n3) this.A02;
                AddGroupsToCommunityViewModel addGroupsToCommunityViewModel = ((AnonymousClass4KD) this.A01).A00.A01;
                if (r3.A0B(AnonymousClass1JV.class) != null) {
                    set = addGroupsToCommunityViewModel.A0B;
                    if (set.remove(r3)) {
                        r0 = addGroupsToCommunityViewModel.A09;
                    } else {
                        return;
                    }
                } else if (r3.A0B(AbstractC15590nW.class) != null && addGroupsToCommunityViewModel.A01.remove(r3)) {
                    r0 = addGroupsToCommunityViewModel.A08;
                    set = addGroupsToCommunityViewModel.A01;
                } else {
                    return;
                }
                r0.A0A(set);
                addGroupsToCommunityViewModel.A04();
                return;
            case 6:
                AnonymousClass1OY r05 = (AnonymousClass1OY) this.A00;
                r05.A0K.AbA(r05.getContext(), Uri.parse(((AnonymousClass3IJ) this.A01).A03), ((C28861Ph) this.A02).A00);
                return;
            case 7:
                C64343Fe r5 = (C64343Fe) this.A00;
                AbstractC15340mz r1 = (AbstractC15340mz) this.A02;
                if (r1 instanceof C28861Ph) {
                    r5.A04.AbA(r5.A03, Uri.parse(((AnonymousClass3IJ) this.A01).A03), ((C28861Ph) r1).A00);
                    return;
                }
                r5.A04.Ab9(r5.A03, Uri.parse(((AnonymousClass3IJ) this.A01).A03));
                return;
            case 8:
                C61122zR r52 = (C61122zR) this.A00;
                C16590pI r7 = r52.A04;
                C15550nR r6 = r52.A03;
                AnonymousClass018 r44 = ((AbstractC61152zU) r52).A04;
                C14650lo r22 = r52.A02;
                C15570nT r06 = r52.A01;
                r06.A08();
                C27631Ih r9 = r06.A05;
                AnonymousClass009.A05(r9);
                String A05 = r52.A01.A05();
                C30721Yo r8 = new C30721Yo(r22, r6, r7, r44);
                r8.A08.A01 = A05;
                r8.A0A(r9, C248917h.A04(r9), null, 2, true);
                try {
                    String A012 = new C41421tV(r52.A00, ((AbstractC61152zU) r52).A04).A01(r8);
                    Intent className = C12970iu.A0A().setClassName(r52.getContext().getPackageName(), "com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity");
                    className.putExtra("edit_mode", true);
                    C12980iv.A13(className, (Jid) this.A01);
                    className.putExtra("vcard", A012);
                    C12990iw.A10(className, r52);
                } catch (C41341tN e) {
                    Log.e("ReciprocalShare", e);
                }
                ((Runnable) this.A02).run();
                return;
            case 9:
                Context context2 = (Context) this.A01;
                ArrayList A003 = ((C61132zS) this.A00).A01.A00(UserJid.of((Jid) this.A02));
                A003.size();
                Intent A0D = C12990iw.A0D(context2, ViewSharedContactArrayActivity.class);
                A0D.putExtra("edit_mode", false);
                A0D.putParcelableArrayListExtra("vcard_sender_infos", A003);
                context2.startActivity(A0D);
                return;
            case 10:
                AnonymousClass2T3 r23 = (AnonymousClass2T3) this.A02;
                AbstractC35611iN r12 = r23.A05;
                if (r12 != null) {
                    ((MediaGalleryFragmentBase) this.A01).A1G(r12, r23);
                    return;
                }
                return;
            default:
                super.A04(view);
                return;
        }
    }

    @Override // X.AbstractView$OnClickListenerC34281fs, android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass2T3 r1;
        AbstractC35611iN r0;
        if (10 - this.A03 == 0) {
            MediaGalleryFragmentBase mediaGalleryFragmentBase = (MediaGalleryFragmentBase) this.A01;
            if (mediaGalleryFragmentBase.A1J() && (r0 = (r1 = (AnonymousClass2T3) this.A02).A05) != null) {
                mediaGalleryFragmentBase.A1G(r0, r1);
                return;
            }
        }
        super.onClick(view);
    }
}
