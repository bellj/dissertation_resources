package com.whatsapp.util;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass01E;
import android.view.View;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.conversation.ConversationAttachmentContentView;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S1100000_I0 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public String A01;
    public final int A02;

    public ViewOnClickCListenerShape2S1100000_I0(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    public ViewOnClickCListenerShape2S1100000_I0(AcceptInviteLinkActivity acceptInviteLinkActivity) {
        this.A02 = 0;
        this.A00 = acceptInviteLinkActivity;
        this.A01 = "invite-via-link-unavailable";
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        switch (this.A02) {
            case 0:
                AcceptInviteLinkActivity acceptInviteLinkActivity = (AcceptInviteLinkActivity) this.A00;
                acceptInviteLinkActivity.A05.A00(acceptInviteLinkActivity, this.A01);
                return;
            case 1:
                AnonymousClass01E r0 = (AnonymousClass01E) this.A00;
                r0.A01();
                r0.A01();
                throw new UnsupportedOperationException();
            case 2:
                ((ConversationAttachmentContentView) this.A00).A0D.A0C(this.A01, false);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
