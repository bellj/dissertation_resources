package com.whatsapp.util;

import X.AbstractView$OnClickListenerC34281fs;
import X.C53262da;
import android.view.View;
import com.whatsapp.WaImageButton;
import java.util.List;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0301000_I1 extends AbstractView$OnClickListenerC34281fs {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public ViewOnClickCListenerShape0S0301000_I1(C53262da r1, WaImageButton waImageButton, List list, int i, int i2) {
        this.A04 = i2;
        this.A01 = r1;
        this.A00 = i;
        this.A03 = list;
        this.A02 = waImageButton;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        C53262da r3;
        switch (this.A04) {
            case 0:
                r3 = (C53262da) this.A01;
                r3.A00 = this.A00;
                for (View view2 : (List) this.A03) {
                    view2.setSelected(false);
                }
                break;
            case 1:
                r3 = (C53262da) this.A01;
                r3.A01 = this.A00;
                for (View view3 : (List) this.A03) {
                    view3.setSelected(false);
                }
                break;
            default:
                super.A04(view);
                return;
        }
        ((View) this.A02).setSelected(true);
        r3.A04();
    }
}
