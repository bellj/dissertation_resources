package com.whatsapp.util;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass12P;
import X.AnonymousClass13N;
import X.AnonymousClass1XP;
import X.AnonymousClass3IJ;
import X.AnonymousClass4NS;
import X.C12980iv;
import X.C12990iw;
import X.C15570nT;
import X.C27621Ig;
import X.C30341Xa;
import X.C30751Yr;
import X.C64343Fe;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.GroupChatLiveLocationsActivity;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0500000_I1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public ViewOnClickCListenerShape2S0500000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i) {
        this.A05 = i;
        this.A00 = obj;
        this.A03 = obj2;
        this.A02 = obj3;
        this.A01 = obj4;
        this.A04 = obj5;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        Class cls;
        UserJid userJid;
        switch (this.A05) {
            case 0:
                C30341Xa r8 = (C30341Xa) this.A03;
                C30751Yr r5 = r8.A02;
                if (r5 == null) {
                    if (r8.A0z.A02) {
                        C15570nT r0 = (C15570nT) this.A02;
                        r0.A08();
                        C27621Ig r02 = r0.A01;
                        AnonymousClass009.A05(r02);
                        userJid = (UserJid) r02.A0D;
                    } else {
                        userJid = (UserJid) this.A04;
                    }
                    AnonymousClass009.A05(userJid);
                    double d = ((AnonymousClass1XP) r8).A00;
                    double d2 = ((AnonymousClass1XP) r8).A01;
                    long j = r8.A0I;
                    r5 = new C30751Yr(userJid);
                    r5.A00 = d;
                    r5.A01 = d2;
                    r5.A05 = j;
                }
                Context context = (Context) this.A00;
                AbstractC14640lm r1 = r8.A0z.A00;
                AnonymousClass009.A05(r1);
                if (((AnonymousClass13N) this.A01).A06(context)) {
                    cls = GroupChatLiveLocationsActivity2.class;
                } else {
                    cls = GroupChatLiveLocationsActivity.class;
                }
                Intent A0D = C12990iw.A0D(context, cls);
                C12980iv.A13(A0D, r1);
                A0D.putExtra("final_location_jid", r5.A06.getRawString());
                A0D.putExtra("final_location_timestamp", r5.A05);
                A0D.putExtra("final_location_latitude", r5.A00);
                A0D.putExtra("final_location_longitude", r5.A01);
                context.startActivity(A0D);
                return;
            case 1:
                AnonymousClass3IJ r12 = (AnonymousClass3IJ) this.A02;
                String str = r12.A03;
                String str2 = r12.A02;
                ((Conversation) AnonymousClass12P.A01(((C64343Fe) this.A00).A03, Conversation.class)).A3C((AnonymousClass4NS) this.A01, ((AbstractC15340mz) this.A03).A0z, str, str2, (Bitmap[]) this.A04, r12.A01);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
