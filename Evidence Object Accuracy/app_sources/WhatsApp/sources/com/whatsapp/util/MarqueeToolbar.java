package com.whatsapp.util;

import X.AnonymousClass028;
import X.AnonymousClass1MN;
import X.AnonymousClass2QB;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import java.lang.reflect.Field;

/* loaded from: classes2.dex */
public class MarqueeToolbar extends AnonymousClass1MN {
    public Runnable A00;
    public boolean A01 = false;

    public MarqueeToolbar(Context context) {
        super(context);
    }

    public MarqueeToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MarqueeToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void A0J() {
        if (!this.A01) {
            try {
                Field declaredField = Toolbar.class.getDeclaredField("mTitleTextView");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(this);
                if (obj == null) {
                    return;
                }
                if (!(obj instanceof TextView)) {
                    this.A01 = true;
                    return;
                }
                TextView textView = (TextView) obj;
                textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                textView.setMarqueeRepeatLimit(1);
                textView.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(textView, 19));
                AnonymousClass028.A0g(textView, new AnonymousClass2QB(this));
                if (this.A00 == null) {
                    RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = new RunnableBRunnable0Shape8S0200000_I0_8(this, 33, textView);
                    this.A00 = runnableBRunnable0Shape8S0200000_I0_8;
                    postDelayed(runnableBRunnable0Shape8S0200000_I0_8, 1000);
                }
            } catch (IllegalAccessException | NoSuchFieldException e) {
                Log.e("util/marqueetoolbar", e);
                this.A01 = true;
            }
        }
    }

    @Override // androidx.appcompat.widget.Toolbar, android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.A00;
        if (runnable != null) {
            removeCallbacks(runnable);
            this.A00 = null;
        }
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setTitle(int i) {
        super.setTitle(i);
        A0J();
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        A0J();
    }
}
