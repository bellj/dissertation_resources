package com.whatsapp.util;

import X.AbstractView$OnClickListenerC34281fs;
import X.C42511vK;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape14S0100000_I0_1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public final int A01;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ViewOnClickCListenerShape14S0100000_I0_1(C42511vK r2) {
        this(r2, 13);
        this.A01 = 13;
    }

    public ViewOnClickCListenerShape14S0100000_I0_1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a0, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x00a2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01b8  */
    @Override // X.AbstractView$OnClickListenerC34281fs
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(android.view.View r16) {
        /*
        // Method dump skipped, instructions count: 2458
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1.A04(android.view.View):void");
    }
}
