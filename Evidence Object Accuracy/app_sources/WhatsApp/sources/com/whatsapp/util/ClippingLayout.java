package com.whatsapp.util;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class ClippingLayout extends FrameLayout implements AnonymousClass004 {
    public Rect A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public ClippingLayout(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ClippingLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }

    public ClippingLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ClippingLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        Rect rect = this.A00;
        if (rect != null) {
            canvas.clipRect(rect);
        }
        return super.drawChild(canvas, view, j);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void setClipBounds(Rect rect) {
        Rect rect2;
        Rect rect3 = this.A00;
        if (rect != rect3) {
            if (rect == null) {
                rect2 = null;
            } else if (!rect.equals(rect3)) {
                Rect rect4 = this.A00;
                if (rect4 == null) {
                    rect2 = new Rect(rect);
                } else {
                    rect4.set(rect);
                    invalidate();
                }
            } else {
                return;
            }
            this.A00 = rect2;
            invalidate();
        }
    }
}
