package com.whatsapp.util;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass2P7;
import X.C28391Mz;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.util.FloatingChildLayout;

/* loaded from: classes2.dex */
public class FloatingChildLayout extends FrameLayout implements AnonymousClass004 {
    public static final boolean A0C = C28391Mz.A02();
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ValueAnimator A06;
    public Rect A07;
    public View.OnTouchListener A08;
    public View A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;

    public FloatingChildLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A07 = new Rect();
        this.A02 = 0;
        this.A06 = null;
        this.A03 = 0;
        Resources resources = getResources();
        this.A05 = -1;
        this.A01 = (resources.getInteger(17694720) * 11) / 10;
        ValueAnimator ofInt = ValueAnimator.ofInt(0, 127);
        this.A06 = ofInt;
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eY
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                FloatingChildLayout.this.setBackgroundColor(C12960it.A05(valueAnimator.getAnimatedValue()) << 24);
            }
        });
        super.setBackgroundDrawable(new ColorDrawable(0));
    }

    public FloatingChildLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A0B) {
            this.A0B = true;
            generatedComponent();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a2, code lost:
        if (r13.A04 == 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r13.A04 == 0) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(java.lang.Runnable r14, boolean r15) {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.FloatingChildLayout.A00(java.lang.Runnable, boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public View getChild() {
        return this.A09;
    }

    private Rect getTargetInWindow() {
        Rect rect = new Rect();
        getWindowVisibleDisplayFrame(rect);
        Rect rect2 = new Rect(this.A07);
        rect2.offset(-rect.left, -rect.top);
        return rect2;
    }

    public int getTopPosition() {
        return this.A05;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(16908290);
        this.A09 = findViewById;
        findViewById.setDuplicateParentStateEnabled(true);
        if (!A0C) {
            this.A09.setAlpha(0.0f);
        }
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int width;
        int height;
        int centerX;
        int centerY;
        View view = this.A09;
        Rect targetInWindow = getTargetInWindow();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = this.A05;
        if (i5 >= 0) {
            centerX = (getWidth() - measuredWidth) >> 1;
            centerY = this.A05;
        } else if (i5 == -1) {
            centerX = targetInWindow.centerX() - (measuredWidth >> 1);
            centerY = targetInWindow.centerY() - Math.round(((float) measuredHeight) * 0.75f);
        } else if (i5 == -2) {
            width = (getWidth() - measuredWidth) >> 1;
            height = (getHeight() - measuredHeight) >> 1;
            view.layout(width, height, view.getMeasuredWidth() + width, view.getMeasuredHeight() + height);
        } else {
            return;
        }
        int width2 = getWidth();
        if (measuredWidth > width2) {
            width = (width2 - measuredWidth) >> 1;
        } else {
            width = Math.min(Math.max(centerX, 0), width2 - measuredWidth);
        }
        int height2 = getHeight();
        if (measuredHeight > height2) {
            height = (height2 - measuredHeight) >> 1;
        } else {
            height = Math.min(Math.max(centerY, 0), height2 - measuredHeight);
        }
        view.layout(width, height, view.getMeasuredWidth() + width, view.getMeasuredHeight() + height);
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        View.OnTouchListener onTouchListener = this.A08;
        if (onTouchListener != null) {
            return onTouchListener.onTouch(this, motionEvent);
        }
        return false;
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        AnonymousClass009.A0A("don't setBackground(), it is managed internally", false);
    }

    public void setBackgroundColorAlpha(int i) {
        setBackgroundColor(i << 24);
    }

    public void setChildTargetScreen(Rect rect) {
        this.A07 = rect;
        requestLayout();
    }

    public void setOnOutsideTouchListener(View.OnTouchListener onTouchListener) {
        this.A08 = onTouchListener;
    }

    public void setRevealAnimation(int i) {
        this.A04 = i;
    }

    public void setStartingAlpha(float f) {
        this.A00 = f;
    }

    public void setTopPosition(int i) {
        this.A05 = i;
    }
}
