package com.whatsapp.util;

import X.AnonymousClass00B;
import X.AnonymousClass00C;
import X.AnonymousClass00H;
import X.AnonymousClass00J;
import X.AnonymousClass01p;
import X.C004502a;
import android.os.Looper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

/* loaded from: classes.dex */
public class Log {
    public static final CharsetEncoder DO_LOG_ENCODER = Charset.defaultCharset().newEncoder();
    public static final ByteBuffer DO_LOG_ENCODE_BUFFER = ByteBuffer.allocate(16384);
    public static final AnonymousClass00B LOGGER_THREAD;
    public static final PrintStream ORIGINAL_SYSTEM_ERR = System.err;
    public static FileChannel channel = null;
    public static final Object compressFileLock = new Object();
    public static AnonymousClass01p connectivityInfoProvider = null;
    public static int level = 5;
    public static final AtomicReference logDirRef = new AtomicReference();
    public static File logFile;
    public static final CountDownLatch logFileLatch = new CountDownLatch(1);
    public static File logTempFile;
    public static final Object tempFileLock = new Object();
    public static final ReentrantLock writeFileLock = new ReentrantLock(true);

    static {
        AnonymousClass00B r0 = new AnonymousClass00B();
        r0.start();
        LOGGER_THREAD = r0;
    }

    public static int A00(File file, File file2) {
        int length;
        String name = file2.getName();
        int length2 = name.length() + 1;
        File[] listFiles = file.getParentFile().listFiles();
        if (listFiles == null) {
            return 0;
        }
        int i = 0;
        for (File file3 : listFiles) {
            String name2 = file3.getName();
            if (name2.startsWith(name) && length2 < (length = name2.length())) {
                try {
                    int parseInt = Integer.parseInt(name2.substring(length2, length));
                    if (parseInt > i) {
                        i = parseInt;
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        return i;
    }

    public static void a(String str) {
        log("LL_A ", str);
    }

    public static void a(boolean z) {
        if (!z) {
            log("LL_A ", "Assertion Failed");
        }
    }

    public static String adorn(String str, StringBuilder sb) {
        String str2;
        StringBuilder sb2;
        Thread currentThread = Thread.currentThread();
        long id = currentThread.getId();
        String name = currentThread.getName();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        AnonymousClass01p r0 = connectivityInfoProvider;
        if (r0 != null) {
            str2 = r0.AAR();
        } else {
            str2 = "D";
        }
        sb3.append(str2);
        sb3.append(" ");
        String obj = sb3.toString();
        if (level < 5) {
            int length = obj.length() + 20 + name.length() + 4;
            if (sb.length() > 16384) {
                sb2 = new StringBuilder(length + 16384 + 3);
                sb2.append(obj);
                sb2.append('[');
                sb2.append(id);
                sb2.append(':');
                sb2.append(name);
                sb2.append("] ");
                sb2.append(sb.substring(0, 16384));
                sb2.append("...");
            } else {
                sb2 = new StringBuilder(length + sb.length());
                sb2.append(obj);
                sb2.append('[');
                sb2.append(id);
                sb2.append(':');
                sb2.append(name);
                sb2.append("] ");
                sb2.append((CharSequence) sb);
            }
            return sb2.toString();
        }
        StackTraceElement[] stackTrace = currentThread.getStackTrace();
        String str3 = "";
        String str4 = str3;
        int i = 5;
        while (true) {
            if (i >= stackTrace.length) {
                break;
            }
            StackTraceElement stackTraceElement = stackTrace[i];
            if (!stackTraceElement.isNativeMethod()) {
                if (stackTraceElement.getFileName() != null) {
                    str3 = stackTraceElement.getFileName();
                    str4 = String.valueOf(stackTraceElement.getLineNumber());
                    break;
                }
                str4 = String.valueOf(stackTraceElement.getLineNumber());
                str3 = "(null)";
            } else if (i == 5) {
                str3 = stackTraceElement.getFileName();
                str4 = "native";
            }
            i++;
        }
        StringBuilder sb4 = new StringBuilder(obj.length() + 1 + 20 + 1 + name.length() + 1 + str3.length() + 1 + str4.length() + 1 + sb.length());
        sb4.append(obj);
        sb4.append('[');
        sb4.append(id);
        sb4.append(':');
        sb4.append(name);
        sb4.append(']');
        sb4.append(str3);
        sb4.append(':');
        sb4.append(str4);
        sb4.append(' ');
        sb4.append((CharSequence) sb);
        return sb4.toString();
    }

    public static void blockingLog(int i, String str) {
        if (i <= level) {
            String adorn = adorn(getLogPrefix(i), new StringBuilder(str));
            logAdorned(adorn, true);
            if (level == 5) {
                logToLogcat(i, adorn);
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        	at java.util.BitSet.or(BitSet.java:940)
        	at jadx.core.utils.BlockUtils.lambda$getPathCross$3(BlockUtils.java:689)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:689)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:728)
        	at jadx.core.dex.visitors.regions.RegionMaker.processTryCatchBlocks(RegionMaker.java:1037)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:55)
        */
    public static java.io.File compress() {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.Log.compress():java.io.File");
    }

    public static void d(String str) {
        int i = level;
        if (i == 5) {
            logToLogcat(4, log("LL_D ", str));
        } else if (i >= 4) {
            log("LL_D ", str);
        }
    }

    public static void d(String str, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("; exception=");
        sb.append(th);
        String obj = sb.toString();
        int i = level;
        if (i == 5) {
            logToLogcat(4, log("LL_D ", obj, getStackTraceString(th)));
        } else if (i >= 4) {
            log("LL_D ", obj, getStackTraceString(th));
        }
    }

    public static void d(Throwable th) {
        int i = level;
        if (i == 5) {
            logToLogcat(4, log("LL_D ", getStackTraceString(th)));
        } else if (i >= 4) {
            log("LL_D ", getStackTraceString(th));
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:38:0x0097 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.util.concurrent.locks.ReentrantLock */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.Calendar] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.concurrent.locks.ReentrantLock] */
    public static void doLogToFile(String str) {
        StringBuilder sb = new StringBuilder();
        ReentrantLock instance = Calendar.getInstance();
        sb.append(instance.get(1));
        sb.append('-');
        if (instance.get(2) < 9) {
            sb.append('0');
        }
        sb.append(instance.get(2) + 1);
        sb.append('-');
        if (instance.get(5) < 10) {
            sb.append('0');
        }
        sb.append(instance.get(5));
        sb.append(' ');
        if (instance.get(11) < 10) {
            sb.append('0');
        }
        sb.append(instance.get(11));
        sb.append(':');
        if (instance.get(12) < 10) {
            sb.append('0');
        }
        sb.append(instance.get(12));
        sb.append(':');
        if (instance.get(13) < 10) {
            sb.append('0');
        }
        sb.append(instance.get(13));
        sb.append('.');
        if (instance.get(14) < 10) {
            sb.append("00");
        } else if (instance.get(14) < 100) {
            sb.append('0');
        }
        try {
            sb.append(instance.get(14));
            sb.append(' ');
            instance = writeFileLock;
            instance.lock();
            try {
                if (initialize()) {
                    FileChannel fileChannel = channel;
                    if (fileChannel != null) {
                        fileChannel.position(fileChannel.size());
                        FileChannel fileChannel2 = channel;
                        CharsetEncoder charsetEncoder = DO_LOG_ENCODER;
                        ByteBuffer byteBuffer = DO_LOG_ENCODE_BUFFER;
                        encodeAndWriteToChannel(fileChannel2, charsetEncoder, byteBuffer, sb);
                        encodeAndWriteToChannel(channel, charsetEncoder, byteBuffer, str);
                        encodeAndWriteToChannel(channel, charsetEncoder, byteBuffer, "\n");
                    } else {
                        throw new NullPointerException();
                    }
                }
            } catch (IOException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("IOException on ");
                sb2.append(str);
                android.util.Log.e("WhatsApp", sb2.toString(), e);
            }
        } finally {
            instance.unlock();
        }
    }

    public static void doLogToLogcat(int i, String str) {
        if (i == 0 || i == 5) {
            android.util.Log.v("WhatsApp", str);
        } else if (i == 4) {
            android.util.Log.d("WhatsApp", str);
        } else if (i == 3) {
            android.util.Log.i("WhatsApp", str);
        } else if (i == 2) {
            android.util.Log.w("WhatsApp", str);
        } else if (i == 1) {
            android.util.Log.e("WhatsApp", str);
        }
    }

    public static void e(String str) {
        if (level == 5) {
            logToLogcat(1, log("LL_E ", str));
        } else {
            log("LL_E ", str);
        }
    }

    public static void e(String str, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("; exception=");
        sb.append(th);
        String obj = sb.toString();
        int i = level;
        String stackTraceString = getStackTraceString(th);
        if (i == 5) {
            logToLogcat(1, log("LL_E ", obj, stackTraceString));
        } else {
            log("LL_E ", obj, stackTraceString);
        }
    }

    public static void e(Throwable th) {
        int i = level;
        String stackTraceString = getStackTraceString(th);
        if (i == 5) {
            logToLogcat(1, log("LL_E ", stackTraceString));
        } else {
            log("LL_E ", stackTraceString);
        }
    }

    public static void encodeAndWriteToChannel(FileChannel fileChannel, CharsetEncoder charsetEncoder, ByteBuffer byteBuffer, CharSequence charSequence) {
        CharBuffer wrap = CharBuffer.wrap(charSequence);
        charsetEncoder.reset();
        CodingErrorAction codingErrorAction = CodingErrorAction.REPLACE;
        charsetEncoder.onMalformedInput(codingErrorAction);
        charsetEncoder.onUnmappableCharacter(codingErrorAction);
        byteBuffer.clear();
        CoderResult coderResult = CoderResult.OVERFLOW;
        while (coderResult.isOverflow()) {
            coderResult = charsetEncoder.encode(wrap, byteBuffer, true);
            byteBuffer.flip();
            if (coderResult.isError()) {
                coderResult.throwException();
            }
            fileChannel.write(byteBuffer);
            byteBuffer.clear();
        }
    }

    public static void flush() {
        try {
            blockingLog(5, "log/flush/start");
            AnonymousClass00B r4 = LOGGER_THREAD;
            if (Thread.currentThread() == r4) {
                int size = r4.A02.size();
                for (int i = 0; i < size; i++) {
                    r4.A00();
                }
            } else {
                FutureTask futureTask = new FutureTask(AnonymousClass00B.A05, null);
                r4.A01(futureTask);
                while (!futureTask.isDone()) {
                    try {
                        futureTask.get();
                    } catch (InterruptedException | ExecutionException unused) {
                    }
                }
            }
            blockingLog(5, "log/flush/logs written");
            blockingLog(5, "log/flush/forcing to disk");
            ReentrantLock reentrantLock = writeFileLock;
            reentrantLock.lock();
            FileChannel fileChannel = channel;
            if (fileChannel != null && fileChannel.isOpen()) {
                channel.force(true);
            }
            reentrantLock.unlock();
            blockingLog(5, "log/flush/end");
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("log/flush/failed");
            sb.append("; exception=");
            sb.append(e);
            String obj = sb.toString();
            String stackTraceString = getStackTraceString(e);
            StringBuilder sb2 = new StringBuilder(obj.length() + 1 + stackTraceString.length());
            sb2.append(obj);
            sb2.append("\n");
            sb2.append(stackTraceString);
            String adorn = adorn("LL_E ", sb2);
            doLogToFile(adorn);
            if (level == 5) {
                logToLogcat(1, adorn);
            }
        }
    }

    public static ArrayList getLatestLogs(int i) {
        File file = logFile;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        ArrayList arrayList = new ArrayList();
        Date date = new Date();
        String pattern = simpleDateFormat.toPattern();
        String A02 = C004502a.A02(file.getName(), ".gz");
        String A01 = C004502a.A01(file.getName());
        File[] listFiles = file.getParentFile().listFiles();
        if (listFiles != null) {
            Arrays.sort(listFiles);
            for (File file2 : listFiles) {
                String name = file2.getName();
                if (name.startsWith(A01) && name.endsWith(A02)) {
                    int length = A01.length();
                    try {
                        if ((date.getTime() - simpleDateFormat.parse(name.substring(length, pattern.length() + length)).getTime()) / TimeUnit.DAYS.toMillis(1) < ((long) i)) {
                            arrayList.add(file2);
                        }
                    } catch (ParseException unused) {
                    }
                }
            }
        }
        return arrayList;
    }

    public static String getLogPrefix(int i) {
        if (i == 0) {
            return "LL_A ";
        }
        if (i == 1) {
            return "LL_E ";
        }
        if (i == 2) {
            return "LL_W ";
        }
        if (i == 3) {
            return "LL_I ";
        }
        if (i == 4) {
            return "LL_D ";
        }
        if (i == 5) {
            return "LL_V ";
        }
        StringBuilder sb = new StringBuilder("Invalid log level ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static String getStackTraceString(Throwable th) {
        if (th == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(stackTraceStartPhrase());
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        sb.append(stringWriter.toString());
        sb.append("### end stack trace");
        return sb.toString();
    }

    public static void i(String str) {
        int i = level;
        if (i == 5) {
            logToLogcat(3, log("LL_I ", str));
        } else if (i >= 3) {
            log("LL_I ", str);
        }
    }

    public static void i(String str, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("; exception=");
        sb.append(th);
        String obj = sb.toString();
        int i = level;
        if (i == 5) {
            logToLogcat(3, log("LL_I ", obj, getStackTraceString(th)));
        } else if (i >= 3) {
            log("LL_I ", obj, getStackTraceString(th));
        }
    }

    public static void i(Throwable th) {
        int i = level;
        if (i == 5) {
            logToLogcat(3, log("LL_I ", getStackTraceString(th)));
        } else if (i >= 3) {
            log("LL_I ", getStackTraceString(th));
        }
    }

    public static boolean initialize() {
        FileChannel fileChannel = channel;
        if (fileChannel != null && fileChannel.isOpen()) {
            return true;
        }
        if (Looper.myLooper() == Looper.getMainLooper() && logFileLatch.getCount() == 1) {
            throw new AssertionError("If Log.initialize() is called on the main thread,  Log.setApplicationContext() must have been called beforehand.");
        }
        try {
            logFileLatch.await();
            File file = (File) logDirRef.get();
            if (file == null || (!file.exists() && !file.mkdirs())) {
                return false;
            }
            channel = new FileOutputStream(logFile, true).getChannel();
            PrintStream printStream = System.err;
            PrintStream printStream2 = ORIGINAL_SYSTEM_ERR;
            boolean z = false;
            if (printStream != printStream2) {
                z = true;
            }
            System.setErr(new PrintStream((OutputStream) new AnonymousClass00J(new AnonymousClass00H(printStream2), Channels.newOutputStream(channel)), true));
            if (z) {
                printStream.close();
            } else {
                printStream.flush();
            }
            Calendar instance = Calendar.getInstance();
            int i = (instance.get(15) + instance.get(16)) / 60000;
            int i2 = i / 60;
            Locale locale = Locale.US;
            Object[] objArr = new Object[3];
            char c = '+';
            if (i2 < 0) {
                c = '-';
            }
            objArr[0] = Character.valueOf(c);
            objArr[1] = Integer.valueOf(Math.abs(i2));
            objArr[2] = Integer.valueOf(Math.abs(i % 60));
            String format = String.format(locale, "%c%02d%02d", objArr);
            StringBuilder sb = new StringBuilder("==== logfile level=");
            sb.append(level);
            sb.append(" tz=");
            sb.append(format);
            sb.append(" ====");
            logAdorned(adorn("LL_I ", new StringBuilder(sb.toString())), true);
            FileChannel fileChannel2 = channel;
            if (fileChannel2 == null || !fileChannel2.isOpen()) {
                return false;
            }
            return true;
        } catch (FileNotFoundException | InterruptedException unused) {
            return false;
        }
    }

    public static String log(String str, String str2) {
        String adorn = adorn(str, new StringBuilder(str2));
        logAdorned(adorn, false);
        return adorn;
    }

    public static String log(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(str2.length() + 1 + str3.length());
        sb.append(str2);
        sb.append("\n");
        sb.append(str3);
        String adorn = adorn(str, sb);
        logAdorned(adorn, false);
        return adorn;
    }

    public static void log(int i, String str) {
        if (i <= level) {
            String log = log(getLogPrefix(i), str);
            if (level == 5) {
                logToLogcat(i, log);
            }
        }
    }

    public static String logAdorned(String str, boolean z) {
        Thread currentThread;
        AnonymousClass00B r0;
        if (!z && (currentThread = Thread.currentThread()) != (r0 = LOGGER_THREAD)) {
            if (currentThread == r0) {
                blockingLog(2, "postLog called on the logging thread. Next log will be out of order.");
            } else {
                r0.A01(str);
                return str;
            }
        }
        doLogToFile(str);
        return str;
    }

    public static void logToLogcat(int i, String str) {
        int length = str.length();
        if (length > 4000) {
            StringBuilder sb = new StringBuilder(4006);
            int i2 = 0;
            while (length - i2 > 4000) {
                if (i2 > 0) {
                    sb.append("...");
                }
                sb.append(str.substring(i2, (i2 + 4000) - 3));
                sb.append("...");
                doLogToLogcat(i, sb.toString());
                i2 += 3997;
                sb.setLength(0);
            }
            StringBuilder sb2 = new StringBuilder(4006);
            if (i2 > 0) {
                sb2.append("...");
            }
            sb2.append(str.substring(i2));
            str = sb2.toString();
        }
        doLogToLogcat(i, str);
    }

    public static boolean rotate() {
        boolean z;
        synchronized (tempFileLock) {
            ReentrantLock reentrantLock = writeFileLock;
            reentrantLock.lock();
            if (initialize()) {
                FileChannel fileChannel = channel;
                if (fileChannel != null) {
                    try {
                        fileChannel.close();
                        channel = null;
                        File file = logFile;
                        File file2 = logTempFile;
                        if (file.exists()) {
                            int A00 = A00(file, file2);
                            StringBuilder sb = new StringBuilder();
                            sb.append(file2.getPath());
                            sb.append(".");
                            sb.append(A00 + 1);
                            try {
                                z = file.renameTo(new File(sb.toString()));
                            } catch (SecurityException unused) {
                            }
                            initialize();
                            reentrantLock.unlock();
                            return z;
                        }
                        z = false;
                        initialize();
                        reentrantLock.unlock();
                        return z;
                    } catch (IOException unused2) {
                        channel = null;
                    } catch (Throwable th) {
                        channel = null;
                        throw th;
                    }
                } else {
                    throw new NullPointerException();
                }
            }
            reentrantLock.unlock();
            return false;
        }
    }

    public static String stackTraceStartPhrase() {
        String A00 = AnonymousClass00C.A00();
        StringBuilder sb = new StringBuilder("### begin stack trace ");
        sb.append(A00);
        sb.append("\n");
        return sb.toString();
    }

    public static void v(String str) {
        if (level >= 5) {
            logToLogcat(5, log("LL_V ", str));
        }
    }

    public static void v(String str, Throwable th) {
        if (level >= 5) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("; exception=");
            sb.append(th);
            logToLogcat(5, log("LL_V ", sb.toString(), getStackTraceString(th)));
        }
    }

    public static void v(Throwable th) {
        if (level >= 5) {
            logToLogcat(5, log("LL_V ", getStackTraceString(th)));
        }
    }

    public static void w(String str) {
        int i = level;
        if (i == 5) {
            logToLogcat(2, log("LL_W ", str));
        } else if (i >= 2) {
            log("LL_W ", str);
        }
    }

    public static void w(String str, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("; exception=");
        sb.append(th);
        String obj = sb.toString();
        int i = level;
        if (i == 5) {
            logToLogcat(2, log("LL_W ", obj, getStackTraceString(th)));
        } else if (i >= 2) {
            log("LL_W ", obj, getStackTraceString(th));
        }
    }

    public static void w(Throwable th) {
        int i = level;
        if (i == 5) {
            logToLogcat(2, log("LL_W ", getStackTraceString(th)));
        } else if (i >= 2) {
            log("LL_W ", getStackTraceString(th));
        }
    }
}
