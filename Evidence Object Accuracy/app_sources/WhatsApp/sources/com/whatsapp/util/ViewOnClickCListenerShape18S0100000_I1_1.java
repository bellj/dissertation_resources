package com.whatsapp.util;

import X.AbstractActivityC13750kH;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16130oV;
import X.AbstractC16350or;
import X.AbstractC28551Oa;
import X.AbstractC33441e5;
import X.AbstractC35731ia;
import X.AbstractC42671vd;
import X.AbstractC47392An;
import X.AbstractC61152zU;
import X.AbstractRunnableC14570le;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass073;
import X.AnonymousClass0G7;
import X.AnonymousClass0OC;
import X.AnonymousClass12P;
import X.AnonymousClass15I;
import X.AnonymousClass190;
import X.AnonymousClass1KS;
import X.AnonymousClass1OY;
import X.AnonymousClass1XP;
import X.AnonymousClass1YO;
import X.AnonymousClass249;
import X.AnonymousClass2YR;
import X.AnonymousClass2xW;
import X.AnonymousClass2y0;
import X.AnonymousClass2y2;
import X.AnonymousClass307;
import X.AnonymousClass341;
import X.AnonymousClass3FR;
import X.AnonymousClass3J7;
import X.AnonymousClass3XR;
import X.AnonymousClass4I7;
import X.AnonymousClass4TL;
import X.AnonymousClass4TX;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15260mp;
import X.C15370n3;
import X.C15380n4;
import X.C15570nT;
import X.C15580nU;
import X.C15890o4;
import X.C16150oX;
import X.C16170oZ;
import X.C16460p3;
import X.C17050qB;
import X.C20320vZ;
import X.C235512c;
import X.C254919p;
import X.C25661Ag;
import X.C25991Bp;
import X.C26161Cg;
import X.C27631Ih;
import X.C27671Iq;
import X.C27701Iu;
import X.C28581Od;
import X.C30041Vv;
import X.C30061Vy;
import X.C30721Yo;
import X.C30741Yq;
import X.C36021jC;
import X.C38211ni;
import X.C43951xu;
import X.C53232dO;
import X.C60492xx;
import X.C60792ye;
import X.C60832yi;
import X.C60852yk;
import X.C617331x;
import X.C618532u;
import X.C618932z;
import X.C622335s;
import X.C628438u;
import X.C63103Ah;
import X.C64853Hd;
import X.C72563eo;
import X.C72643ew;
import X.C72653ex;
import X.C861846a;
import X.C91394Ro;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.conversation.conversationrow.ConversationRowContact$MessageSharedContactDialogFragment;
import com.whatsapp.group.GroupParticipantsSearchFragment;
import com.whatsapp.group.GroupSettingsActivity;
import com.whatsapp.invites.InviteGroupParticipantsActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediacomposer.bottombar.recipients.RecipientsView;
import com.whatsapp.migration.android.view.GoogleMigrateImporterActivity;
import com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel;
import com.whatsapp.picker.search.StickerSearchDialogFragment;
import com.whatsapp.polls.PollCreatorActivity;
import com.whatsapp.polls.PollCreatorViewModel;
import com.whatsapp.reactions.ReactionEmojiTextView;
import com.whatsapp.registration.EULA;
import com.whatsapp.stickers.StickerInfoDialogFragment;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.twofactor.SetCodeFragment;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.wamsys.JniBridge;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape18S0100000_I1_1 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public final int A01;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ViewOnClickCListenerShape18S0100000_I1_1(C60492xx r2) {
        this(r2, 7);
        this.A01 = 7;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ViewOnClickCListenerShape18S0100000_I1_1(X.AnonymousClass2y0 r2, int r3) {
        /*
            r1 = this;
            r1.A01 = r3
            switch(r3) {
                case 2: goto L_0x000a;
                case 3: goto L_0x000c;
                case 4: goto L_0x000e;
                case 5: goto L_0x0010;
                default: goto L_0x0005;
            }
        L_0x0005:
            r0 = 6
        L_0x0006:
            r1.<init>(r2, r0)
            return
        L_0x000a:
            r0 = 2
            goto L_0x0006
        L_0x000c:
            r0 = 3
            goto L_0x0006
        L_0x000e:
            r0 = 4
            goto L_0x0006
        L_0x0010:
            r0 = 5
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1.<init>(X.2y0, int):void");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ViewOnClickCListenerShape18S0100000_I1_1(C60792ye r2) {
        this(r2, 11);
        this.A01 = 11;
    }

    public ViewOnClickCListenerShape18S0100000_I1_1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AnimatorSet animatorSet;
        C25661Ag r1;
        String str;
        AnonymousClass016 r12;
        boolean z;
        String str2;
        String A0X;
        int length;
        switch (this.A01) {
            case 0:
                ((AbstractC47392An) this.A00).A02(true);
                return;
            case 1:
                AnonymousClass2xW r0 = (AnonymousClass2xW) this.A00;
                Conversation conversation = ((AbstractC47392An) r0).A01;
                conversation.startActivity(C14960mK.A0O(conversation, r0.A00.A0D, false, true, true));
                return;
            case 2:
                AnonymousClass2y0 r3 = (AnonymousClass2y0) this.A00;
                C30721Yo r02 = r3.A0C;
                if (r02 == null) {
                    Log.w("conversationrowcontact/addcontactonclicklistener/contact is null");
                    ((AnonymousClass1OY) r3).A0J.A07(R.string.error_parse_vcard, 0);
                    return;
                }
                Bitmap bitmap = null;
                byte[] bArr = r02.A09;
                if (bArr != null && (length = bArr.length) > 0) {
                    bitmap = BitmapFactory.decodeByteArray(bArr, 0, length);
                }
                AbstractActivityC13750kH r4 = (AbstractActivityC13750kH) AbstractC35731ia.A01(r3.getContext(), AbstractActivityC13750kH.class);
                C30721Yo r32 = r3.A0C;
                if (!(r32 == null || r4 == null)) {
                    r4.A0u = r32;
                    C004802e A0S = C12980iv.A0S(r4);
                    A0S.A06(R.string.add_contact_as_new_or_existing);
                    A0S.setPositiveButton(R.string.new_contact, new DialogInterface.OnClickListener(bitmap, r4, r32) { // from class: X.3Ki
                        public final /* synthetic */ Bitmap A00;
                        public final /* synthetic */ AbstractActivityC13750kH A01;
                        public final /* synthetic */ C30721Yo A02;

                        {
                            this.A01 = r2;
                            this.A02 = r3;
                            this.A00 = r1;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            AbstractActivityC13750kH r42 = this.A01;
                            if (((ActivityC13790kL) r42).A00.A09(r42, AnonymousClass190.A00(r42, this.A00, this.A02, true), 41)) {
                                r42.A0n.A02(true, 6);
                            }
                        }
                    });
                    A0S.setNegativeButton(R.string.existing_contact, new DialogInterface.OnClickListener(bitmap, r4, r32) { // from class: X.3Kh
                        public final /* synthetic */ Bitmap A00;
                        public final /* synthetic */ AbstractActivityC13750kH A01;
                        public final /* synthetic */ C30721Yo A02;

                        {
                            this.A01 = r2;
                            this.A02 = r3;
                            this.A00 = r1;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            AbstractActivityC13750kH r42 = this.A01;
                            if (((ActivityC13790kL) r42).A00.A09(r42, AnonymousClass190.A00(r42, this.A00, this.A02, false), 41)) {
                                r42.A0n.A02(false, 6);
                            }
                        }
                    });
                    C12970iu.A1J(A0S);
                    return;
                }
                return;
            case 3:
                AnonymousClass2y0 r7 = (AnonymousClass2y0) this.A00;
                if (r7.A1O(r7.A0C)) {
                    C30721Yo r03 = r7.A0C;
                    ArrayList A0l = C12960it.A0l();
                    List<AnonymousClass4TL> list = r03.A02;
                    if (list != null) {
                        for (AnonymousClass4TL r2 : list) {
                            if (r2.A01 == ContactsContract.CommonDataKinds.Email.class) {
                                A0l.add(r2.A02);
                            }
                        }
                    }
                    if (A0l.isEmpty()) {
                        ArrayList arrayList = r7.A0S;
                        if (arrayList.size() == 1) {
                            A06((String) arrayList.get(0));
                            return;
                        }
                    }
                    ArrayList arrayList2 = r7.A0S;
                    if (!arrayList2.isEmpty() || A0l.size() != 1) {
                        ArrayList A0w = C12980iv.A0w(arrayList2.size() + A0l.size());
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            A0w.add(((AbstractC28551Oa) r7).A0K.A0G(C12970iu.A0x(it)));
                        }
                        Iterator it2 = A0l.iterator();
                        while (it2.hasNext()) {
                            A0w.add(((AbstractC28551Oa) r7).A0K.A0F(C12970iu.A0x(it2)));
                        }
                        boolean isEmpty = TextUtils.isEmpty(r7.A0C.A08.A01);
                        Context context = r7.getContext();
                        if (isEmpty) {
                            A0X = context.getString(R.string.invite_contact_via);
                        } else {
                            A0X = C12960it.A0X(context, r7.A0C.A08.A01, new Object[1], 0, R.string.invite_named_contact_via);
                        }
                        C004802e A0S2 = C12980iv.A0S(r7.getContext());
                        A0S2.setTitle(A0X);
                        IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I1 = new IDxCListenerShape3S0200000_1_I1(A0w, 6, this);
                        AnonymousClass0OC r04 = A0S2.A01;
                        r04.A0M = (CharSequence[]) A0w.toArray(new String[0]);
                        r04.A05 = iDxCListenerShape3S0200000_1_I1;
                        C12970iu.A1J(A0S2);
                        return;
                    }
                    A05((String) A0l.get(0));
                    return;
                }
                return;
            case 4:
                AnonymousClass2y0 r6 = (AnonymousClass2y0) this.A00;
                if (r6.A00 == 1) {
                    Iterator it3 = r6.A0R.iterator();
                    while (it3.hasNext()) {
                        UserJid userJid = (UserJid) it3.next();
                        if (userJid != null) {
                            AnonymousClass190 r22 = r6.A05;
                            Context context2 = r6.getContext();
                            C30721Yo r05 = r6.A0C;
                            if (r05 != null) {
                                str2 = r05.A08.A08;
                            } else {
                                str2 = null;
                            }
                            r22.A01(context2, userJid, str2);
                            return;
                        }
                    }
                    return;
                }
                C30041Vv.A0C(r6.getFMessage());
                ActivityC000900k r33 = (ActivityC000900k) AbstractC35731ia.A01(r6.getContext(), ActivityC000800j.class);
                if (!(r6.A0C == null || r33 == null)) {
                    ArrayList A0l2 = C12960it.A0l();
                    ArrayList A0l3 = C12960it.A0l();
                    for (int i = 0; i < r6.A0C.A05.size(); i++) {
                        if (r6.A0R.get(i) != null) {
                            A0l2.add(((C30741Yq) r6.A0C.A05.get(i)).A02);
                            A0l3.add(((C30741Yq) r6.A0C.A05.get(i)).A03);
                        } else {
                            A0l2.add(null);
                            A0l3.add(null);
                        }
                    }
                    ConversationRowContact$MessageSharedContactDialogFragment.A00(r6.A0C.A08.A08, r6.A0R, A0l2, A0l3).A1F(r33.A0V(), null);
                    return;
                }
                return;
            case 5:
                Object obj = this.A00;
                AnonymousClass2y0 r23 = (AnonymousClass2y0) obj;
                ActivityC13790kL r5 = (ActivityC13790kL) AbstractC35731ia.A01(r23.getContext(), ActivityC13790kL.class);
                ArrayList arrayList3 = r23.A0R;
                if (!(arrayList3.isEmpty() || arrayList3.get(0) == null || r5 == null)) {
                    UserJid userJid2 = (UserJid) arrayList3.get(0);
                    C15370n3 A0B = ((AnonymousClass1OY) r23).A0X.A0B(userJid2);
                    AnonymousClass1OY r42 = (AnonymousClass1OY) obj;
                    if (((AbstractC28551Oa) r42).A0L.A07(875)) {
                        Jid jid = A0B.A0D;
                        if (jid instanceof UserJid) {
                            r42.A0L.A0F(jid);
                        }
                    }
                    if (!A0B.A0J()) {
                        C628438u r13 = r23.A07;
                        if (r13 != null) {
                            r13.A03(true);
                            r23.A07 = null;
                        }
                        C628438u r34 = new C628438u(((AnonymousClass1OY) r23).A0L, r5, r23.A03, r23.A06, userJid2);
                        r23.A07 = r34;
                        C12990iw.A1N(r34, r23.A1P);
                        return;
                    }
                    r5.startActivity(new C14960mK().A0h(r5, A0B, C12960it.A0V()));
                    return;
                }
                return;
            case 6:
                AnonymousClass2y0 r52 = (AnonymousClass2y0) this.A00;
                if (r52.A0C != null) {
                    Context context3 = r52.getContext();
                    Bundle A03 = C30041Vv.A03(r52.getFMessage().A0z);
                    Intent className = C12970iu.A0A().setClassName(context3.getPackageName(), "com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity");
                    className.putExtra("edit_mode", false);
                    className.putExtra("vcard_message", A03);
                    C12990iw.A10(className, r52);
                    return;
                }
                Log.w("conversationrowcontact/onclicklistener/vcard is empty");
                ((AnonymousClass1OY) r52).A0J.A07(R.string.error_parse_vcard, 0);
                return;
            case 7:
                AbstractC28551Oa r53 = (AbstractC28551Oa) this.A00;
                Context context4 = r53.getContext();
                Bundle A032 = C30041Vv.A03(r53.getFMessage().A0z);
                Intent className2 = C12970iu.A0A().setClassName(context4.getPackageName(), "com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity");
                className2.putExtra("edit_mode", false);
                className2.putExtra("vcard_message", A032);
                C12990iw.A10(className2, r53);
                return;
            case 8:
                AnonymousClass2y2 r43 = (AnonymousClass2y2) this.A00;
                for (AbstractC16130oV r24 : r43.A05) {
                    if (C63103Ah.A00(r24)) {
                        C26161Cg r06 = r43.A04;
                        AnonymousClass009.A05(r06);
                        AbstractRunnableC14570le r07 = (AbstractRunnableC14570le) r06.A07.get(r24);
                        if (r07 != null) {
                            r07.cancel();
                        }
                    } else if (AbstractC15340mz.A00(r24).A0a) {
                        if (r24.A0z.A02) {
                            r43.A02.A05(r24, false);
                        }
                        r43.A03.A0C(r24, false, false);
                    }
                }
                return;
            case 9:
                AnonymousClass2y2 r44 = (AnonymousClass2y2) this.A00;
                for (AbstractC16130oV r25 : r44.A05) {
                    C16150oX A00 = AbstractC15340mz.A00(r25);
                    if (!A00.A0P && !A00.A0a && !C30041Vv.A11(r25)) {
                        ((AnonymousClass1OY) r44).A0M.A05(r25, true, true);
                    }
                }
                return;
            case 10:
                ArrayList A0l4 = C12960it.A0l();
                AnonymousClass2y2 r54 = (AnonymousClass2y2) this.A00;
                for (AbstractC16130oV r35 : r54.A05) {
                    if (C63103Ah.A00(r35)) {
                        C26161Cg r08 = r54.A04;
                        AnonymousClass009.A05(r08);
                        r08.A00(r35);
                    } else {
                        C16150oX A002 = AbstractC15340mz.A00(r35);
                        if (!A002.A0P && !A002.A0a && r35.A08 != null && A002.A07 != 1) {
                            A0l4.add(r35);
                        }
                    }
                }
                C16170oZ r45 = ((AnonymousClass1OY) r54).A0R;
                Activity A02 = AnonymousClass12P.A02(r54);
                if (A0l4.size() != 0) {
                    C17050qB r9 = r45.A0P;
                    C14950mJ r10 = r45.A0V;
                    if (r9.A04(new AnonymousClass249(A02, r10))) {
                        C618932z r55 = new C618932z(A02, r45.A05, r45.A0N, r9, r10, C30041Vv.A04(r45.A0c, (AbstractC15340mz) A0l4.get(0)), r45.A0y);
                        Iterator it4 = A0l4.iterator();
                        while (it4.hasNext()) {
                            r45.A15.A07(r55, (AbstractC16130oV) it4.next(), 0);
                        }
                        return;
                    }
                    return;
                }
                return;
            case 11:
                AnonymousClass1OY r09 = (AnonymousClass1OY) this.A00;
                r09.A0R.A0S((AnonymousClass1XP) ((AbstractC28551Oa) r09).A0O);
                return;
            case 12:
                C64853Hd r36 = (C64853Hd) this.A00;
                AbstractC16130oV r26 = r36.A00;
                if (AbstractC15340mz.A00(r26).A0a) {
                    if (r26.A0z.A02) {
                        r36.A0E.A05(r26, false);
                    }
                    r36.A0F.A0C(r36.A00, false, false);
                    return;
                }
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C64853Hd r46 = (C64853Hd) this.A00;
                AbstractC16130oV r27 = r46.A00;
                C16150oX A003 = AbstractC15340mz.A00(r27);
                if ((!A003.A0P || A003.A0X) && !A003.A0a && r27.A08 != null && A003.A07 != 1) {
                    r46.A02 = true;
                    r46.A09.A03((ActivityC13810kN) AnonymousClass12P.A02(r46.A05), r46.A00, true);
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C64853Hd r37 = (C64853Hd) this.A00;
                C15890o4 r14 = r37.A0B;
                if (r14 == null || RequestPermissionActivity.A0W(view.getContext(), r14)) {
                    AbstractC16130oV r28 = r37.A00;
                    C16150oX A004 = AbstractC15340mz.A00(r28);
                    if (!(A004.A0P || A004.A0a)) {
                        r37.A08.A05(r28, true, true);
                        return;
                    }
                    return;
                }
                return;
            case 15:
                C64853Hd r38 = (C64853Hd) this.A00;
                AnonymousClass1KS A1C = ((C30061Vy) r38.A00).A1C();
                StickerView stickerView = r38.A0H;
                if (!stickerView.A03) {
                    stickerView.A03();
                }
                ((ActivityC13810kN) AnonymousClass12P.A01(r38.A05.getContext(), ActivityC13810kN.class)).Adm(StickerInfoDialogFragment.A00(A1C, r38.A00.A0z.A02));
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                AnonymousClass3FR r010 = (AnonymousClass3FR) this.A00;
                C60832yi r39 = r010.A0B;
                AbstractC16130oV r29 = r010.A05;
                r39.A18(r29.A0z.A00, r29.A0V);
                return;
            case 17:
                C60852yk r56 = (C60852yk) this.A00;
                AbstractC16130oV r310 = (AbstractC16130oV) ((AbstractC28551Oa) r56).A0O;
                C16150oX A005 = AbstractC15340mz.A00(r310);
                if (A005.A07 == 1) {
                    ((AnonymousClass1OY) r56).A0J.A05(R.string.gallery_unsafe_video_removed, 1);
                    return;
                }
                ((AbstractC42671vd) r56).A08.A04(view);
                if (r310.A08 == null) {
                    return;
                }
                if (A005.A0Z) {
                    r56.A1P();
                    return;
                } else {
                    Log.e("streamdownload/unable to open playback");
                    return;
                }
            case 18:
                C53232dO r62 = (C53232dO) this.A00;
                if (!(view instanceof ReactionEmojiTextView)) {
                    AnonymousClass009.A07("Given view is not ReactionEmojiTextView.");
                }
                ReactionEmojiTextView reactionEmojiTextView = (ReactionEmojiTextView) view;
                AnimatorSet animatorSet2 = r62.A00;
                if (animatorSet2 == null) {
                    animatorSet2 = new AnimatorSet();
                    r62.A00 = animatorSet2;
                }
                if (animatorSet2.isRunning()) {
                    r62.A00.cancel();
                }
                for (int i2 = 0; i2 < r62.getChildCount(); i2++) {
                    View childAt = r62.getChildAt(i2);
                    if ((childAt instanceof ReactionEmojiTextView) && childAt.isSelected() && !childAt.equals(reactionEmojiTextView)) {
                        r62.A00.play(C53232dO.A00(r62, (ReactionEmojiTextView) childAt));
                    }
                }
                boolean isSelected = reactionEmojiTextView.isSelected();
                AnimatorSet animatorSet3 = r62.A00;
                if (isSelected) {
                    animatorSet = C53232dO.A00(r62, reactionEmojiTextView);
                } else {
                    AnimatorSet animatorSet4 = new AnimatorSet();
                    ObjectAnimator duration = ObjectAnimator.ofFloat(reactionEmojiTextView, "foregroundScale", 1.0f, 0.7f).setDuration(80L);
                    Interpolator interpolator = C53232dO.A0C;
                    duration.setInterpolator(interpolator);
                    duration.addListener(new C72643ew(r62, reactionEmojiTextView, 0.7f));
                    ObjectAnimator duration2 = ObjectAnimator.ofFloat(reactionEmojiTextView, "foregroundScale", 0.7f, 1.0f).setDuration(80L);
                    duration2.setInterpolator(interpolator);
                    duration2.addListener(new C72643ew(r62, reactionEmojiTextView, 1.0f));
                    animatorSet4.playSequentially(duration, duration2);
                    ObjectAnimator duration3 = ObjectAnimator.ofFloat(reactionEmojiTextView, "backgroundScale", 0.0f, 1.0f).setDuration(250L);
                    duration3.addListener(new C72653ex(r62, reactionEmojiTextView, 1.0f));
                    duration3.setInterpolator(AnonymousClass3J7.A00);
                    animatorSet4.playTogether(duration, duration3);
                    animatorSet4.addListener(new C72563eo(r62, reactionEmojiTextView));
                    animatorSet = animatorSet4;
                }
                animatorSet3.play(animatorSet);
                r62.A00.addListener(new AnonymousClass2YR(reactionEmojiTextView, this));
                r62.A00.start();
                return;
            case 19:
            case C25991Bp.A0S:
            case 36:
                ((DialogFragment) this.A00).A1B();
                return;
            case C43951xu.A01:
                ((AbstractC61152zU) this.A00).A00.setVisibility(8);
                return;
            case 21:
                C15260mp r15 = (C15260mp) ((AnonymousClass4TX) this.A00).A00;
                if (r15.A00 != 0) {
                    r15.A0I(0);
                    return;
                }
                return;
            case 22:
                AnonymousClass3XR r311 = (AnonymousClass3XR) this.A00;
                C15260mp r210 = (C15260mp) r311.A00;
                if (r210.A00 != 1) {
                    r210.A0I(1);
                    r311.A00();
                    return;
                }
                return;
            case 23:
                AnonymousClass307 r47 = new AnonymousClass307();
                C617331x r312 = (C617331x) this.A00;
                int i3 = r312.A00.A00;
                int i4 = 0;
                if (i3 != 1) {
                    if (i3 == 2) {
                        i4 = 1;
                    } else {
                        throw C12970iu.A0f(C12960it.A0W(i3, "Unexpected provider type "));
                    }
                }
                r47.A00 = Integer.valueOf(i4);
                r312.A06.A07(r47);
                r312.A08.AR4(r312.A00);
                return;
            case 24:
                ((GroupParticipantsSearchFragment) this.A00).A1B();
                return;
            case 25:
                GroupSettingsActivity groupSettingsActivity = (GroupSettingsActivity) this.A00;
                groupSettingsActivity.Adl(GroupSettingsActivity.EditGroupInfoDialogFragment.A00(groupSettingsActivity.A0C, groupSettingsActivity.A07.A0i), null);
                return;
            case 26:
                GroupSettingsActivity groupSettingsActivity2 = (GroupSettingsActivity) this.A00;
                groupSettingsActivity2.Adl(GroupSettingsActivity.SendMessagesDialogFragment.A00(groupSettingsActivity2.A0C, groupSettingsActivity2.A07.A0W), null);
                return;
            case 27:
                GroupSettingsActivity groupSettingsActivity3 = (GroupSettingsActivity) this.A00;
                groupSettingsActivity3.Adl(GroupSettingsActivity.RestrictFrequentlyForwardedDialogFragment.A00(groupSettingsActivity3.A0C, groupSettingsActivity3.A07.A0g), null);
                return;
            case 28:
                GroupSettingsActivity groupSettingsActivity4 = (GroupSettingsActivity) this.A00;
                ArrayList A0l5 = C12960it.A0l();
                Iterator it5 = groupSettingsActivity4.A06.A02(groupSettingsActivity4.A0C).A07().iterator();
                while (it5.hasNext()) {
                    AnonymousClass1YO r211 = (AnonymousClass1YO) it5.next();
                    int i5 = r211.A01;
                    if (!(i5 == 0 || i5 == 2)) {
                        C15570nT r011 = ((ActivityC13790kL) groupSettingsActivity4).A01;
                        UserJid userJid3 = r211.A03;
                        if (!r011.A0F(userJid3)) {
                            A0l5.add(userJid3);
                        }
                    }
                }
                C15580nU r313 = groupSettingsActivity4.A0C;
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(groupSettingsActivity4.getPackageName(), "com.whatsapp.group.EditGroupAdminsSelector");
                A0A.putExtra("gid", r313.getRawString());
                A0A.putExtra("selected", C15380n4.A06(A0l5));
                groupSettingsActivity4.startActivityForResult(A0A, 17);
                return;
            case 29:
                InviteGroupParticipantsActivity inviteGroupParticipantsActivity = (InviteGroupParticipantsActivity) this.A00;
                C16170oZ r122 = inviteGroupParticipantsActivity.A02;
                String A04 = inviteGroupParticipantsActivity.A04.A04(inviteGroupParticipantsActivity.A0A);
                List<C91394Ro> list2 = inviteGroupParticipantsActivity.A0I;
                byte[] bArr2 = inviteGroupParticipantsActivity.A0K;
                String stringText = inviteGroupParticipantsActivity.A0G.getStringText();
                for (C91394Ro r314 : list2) {
                    C20320vZ r012 = r122.A1G;
                    UserJid userJid4 = r314.A02;
                    C15580nU r142 = r314.A01;
                    C15570nT r212 = r122.A07;
                    r212.A08();
                    C27631Ih r132 = r212.A05;
                    String str3 = r314.A03;
                    long A006 = r122.A0R.A00();
                    long j = r314.A00;
                    int A022 = r122.A0b.A02(r142);
                    C28581Od r16 = new C28581Od(r012.A07.A02(userJid4, true), A006);
                    ((AbstractC15340mz) r16).A02 = 1;
                    r16.A02 = r142;
                    r16.A03 = r132;
                    r16.A05 = A04;
                    r16.A06 = str3;
                    r16.A01 = j;
                    r16.A07 = false;
                    r16.A00 = A022;
                    r16.A04 = stringText;
                    if (bArr2 != null) {
                        C16460p3 A0G = r16.A0G();
                        AnonymousClass009.A05(A0G);
                        A0G.A02(bArr2);
                    }
                    r122.A0M(r16);
                    r122.A0f.A0S(r16);
                }
                inviteGroupParticipantsActivity.setResult(-1);
                C14900mE r72 = ((ActivityC13810kN) inviteGroupParticipantsActivity).A05;
                AnonymousClass018 r63 = inviteGroupParticipantsActivity.A08;
                long size = (long) inviteGroupParticipantsActivity.A0I.size();
                Object[] A1b = C12970iu.A1b();
                A1b[0] = Integer.valueOf(inviteGroupParticipantsActivity.A0I.size());
                r72.A0E(r63.A0I(A1b, R.plurals.group_invites_sent, size), 0);
                inviteGroupParticipantsActivity.finish();
                return;
            case 31:
                C36021jC.A01(((C618532u) this.A00).A06.A0E, 0);
                return;
            case 32:
                AbstractC33441e5 r213 = ((RecipientsView) this.A00).A02;
                if (r213 != null) {
                    r213.AUk("status_chip".equals(view.getTag()));
                    return;
                }
                return;
            case 33:
                GoogleMigrateImporterViewModel googleMigrateImporterViewModel = ((GoogleMigrateImporterActivity) this.A00).A04;
                Number number = (Number) googleMigrateImporterViewModel.A01.A01();
                if (number != null) {
                    int intValue = number.intValue();
                    if (intValue == 0 || intValue == 1) {
                        if (!googleMigrateImporterViewModel.A06.A0B()) {
                            Log.i("GoogleMigrateImporterViewModel/no network access");
                            r12 = googleMigrateImporterViewModel.A02;
                            z = false;
                        } else {
                            r12 = googleMigrateImporterViewModel.A04;
                            z = true;
                        }
                        r12.A0B(z);
                        return;
                    } else if (intValue == 5) {
                        googleMigrateImporterViewModel.A07(1);
                        return;
                    } else if (intValue == 9) {
                        googleMigrateImporterViewModel.A05();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 34:
                GoogleMigrateImporterViewModel googleMigrateImporterViewModel2 = ((GoogleMigrateImporterActivity) this.A00).A04;
                Number number2 = (Number) googleMigrateImporterViewModel2.A01.A01();
                if (number2 != null) {
                    int intValue2 = number2.intValue();
                    if (intValue2 == 1) {
                        r1 = googleMigrateImporterViewModel2.A0D;
                        str = "google_migrate_rejected_permission";
                    } else if (intValue2 == 3) {
                        r1 = googleMigrateImporterViewModel2.A0D;
                        str = "google_migrate_import_started";
                    } else {
                        return;
                    }
                    r1.A01(str, "google_migrate_attempt_to_skip_import");
                    googleMigrateImporterViewModel2.A06(10);
                    return;
                }
                return;
            case 35:
                StickerSearchDialogFragment stickerSearchDialogFragment = (StickerSearchDialogFragment) this.A00;
                C12990iw.A1G(stickerSearchDialogFragment.A05);
                stickerSearchDialogFragment.A05.A04(false);
                return;
            case 37:
                PollCreatorActivity pollCreatorActivity = (PollCreatorActivity) this.A00;
                PollCreatorViewModel pollCreatorViewModel = pollCreatorActivity.A06;
                String trim = pollCreatorViewModel.A07.A00.trim();
                if (pollCreatorActivity.A04 != null && trim.length() > 0 && pollCreatorViewModel.A06(true)) {
                    PollCreatorViewModel pollCreatorViewModel2 = pollCreatorActivity.A06;
                    AbstractC14640lm r57 = pollCreatorActivity.A04;
                    ArrayList A0l6 = C12960it.A0l();
                    for (C861846a r013 : pollCreatorViewModel2.A0F) {
                        String str4 = r013.A00;
                        if (!str4.isEmpty()) {
                            A0l6.add(str4);
                        }
                    }
                    C16170oZ r8 = pollCreatorViewModel2.A03;
                    if (r8.A0x.A07(1394)) {
                        C20320vZ r214 = r8.A1G;
                        JniBridge jniBridge = r8.A1R;
                        C27671Iq r58 = new C27671Iq(r214.A07.A02(r57, true), r8.A0R.A00());
                        r58.A02 = trim;
                        List list3 = r58.A04;
                        list3.clear();
                        Iterator it6 = A0l6.iterator();
                        while (it6.hasNext()) {
                            NativeHolder nativeHolder = (NativeHolder) JniBridge.jvidispatchOOO(4, it6.next(), jniBridge.wajContext.get());
                            if (nativeHolder != null) {
                                AnonymousClass4I7 r014 = new AnonymousClass4I7(nativeHolder);
                                JniBridge.getInstance();
                                NativeHolder nativeHolder2 = r014.A00;
                                JniBridge.getInstance();
                                list3.add(new C27701Iu((String) JniBridge.jvidispatchOIO(1, (long) 64, nativeHolder2), Base64.encodeToString((byte[]) JniBridge.jvidispatchOIO(0, (long) 63, nativeHolder2), 2)));
                            }
                        }
                        r8.A0M(r58);
                        r8.A0f.A0S(r58);
                    }
                    pollCreatorActivity.finish();
                    pollCreatorActivity.overridePendingTransition(0, R.anim.slide_out_down);
                    return;
                }
                return;
            case 38:
                EULA eula = (EULA) this.A00;
                C12960it.A08(((ActivityC13810kN) eula).A09).putBoolean("is_ls_shown_during_reg", true).commit();
                eula.A0B.A00();
                ViewGroup viewGroup = (ViewGroup) eula.findViewById(R.id.eula_layout);
                AnonymousClass0G7 r215 = new AnonymousClass0G7();
                r215.A04(100);
                AnonymousClass073.A01(viewGroup, r215);
                viewGroup.removeAllViews();
                viewGroup.addView(C12960it.A0F(eula.getLayoutInflater(), viewGroup, R.layout.eula_with_ls));
                eula.A2e();
                eula.A2f();
                return;
            case 39:
                C38211ni.A06((Activity) this.A00, "com.whatsapp.w4b");
                return;
            case 40:
                Context context5 = (Context) this.A00;
                Intent A0A2 = C12970iu.A0A();
                A0A2.setClassName(context5.getPackageName(), "com.whatsapp.settings.Licenses");
                context5.startActivity(A0A2);
                return;
            case 41:
            case 42:
                Activity activity = (Activity) this.A00;
                AbstractC14640lm A01 = AbstractC14640lm.A01(activity.getIntent().getStringExtra("chat_jid"));
                Intent className3 = C12970iu.A0A().setClassName(activity.getPackageName(), "com.whatsapp.settings.chat.wallpaper.WallpaperCategoriesActivity");
                className3.putExtra("chat_jid", C15380n4.A03(A01));
                activity.startActivityForResult(className3, 199);
                return;
            case 43:
                ((AnonymousClass341) this.A00).A04();
                return;
            case 44:
                C622335s r015 = (C622335s) this.A00;
                C235512c r17 = r015.A06;
                String str5 = r015.A04.A0D;
                AnonymousClass15I r18 = r17.A0Q;
                if (r18.A01.containsKey(str5)) {
                    C12980iv.A1M((AbstractC16350or) r18.A00.get(str5));
                    return;
                }
                return;
            case 45:
                SetCodeFragment.A01((SetCodeFragment) this.A00);
                return;
            default:
                super.A04(view);
                return;
        }
    }

    public final void A05(String str) {
        AnonymousClass2y0 r5 = (AnonymousClass2y0) this.A00;
        C254919p r8 = r5.A01;
        Intent putExtra = C12990iw.A0E("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.EMAIL", new String[]{str}).putExtra("android.intent.extra.SUBJECT", r5.getContext().getString(R.string.tell_a_friend_email_subject));
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C12960it.A0X(r5.getContext(), "https://www.whatsapp.com/download/", new Object[1], 0, R.string.tell_a_friend_email_body));
        r8.A00(r5.getContext(), putExtra.putExtra("android.intent.extra.TEXT", C12960it.A0d("\n\n", A0h)), null, r5.getContext().getString(R.string.invite_via_email_title), true);
    }

    public final void A06(String str) {
        AnonymousClass2y0 r1 = (AnonymousClass2y0) this.A00;
        r1.A02.A00(AnonymousClass12P.A02(r1), Uri.parse(C12960it.A0d(str, C12960it.A0k("sms:"))), 18, C12960it.A0X(r1.getContext(), "https://whatsapp.com/dl/", C12970iu.A1b(), 0, R.string.tell_a_friend_sms));
    }
}
