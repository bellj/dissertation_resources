package com.whatsapp.util;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC42481vH;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass1IS;
import X.AnonymousClass1OY;
import X.AnonymousClass1VX;
import X.AnonymousClass1XV;
import X.C14960mK;
import X.C15380n4;
import X.C15580nU;
import X.C15650ng;
import X.C16700pc;
import X.C18120rw;
import X.C235812f;
import X.C28111Kr;
import X.C30061Vy;
import X.C30241Wq;
import X.C38211ni;
import X.C90794Pg;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import java.util.Stack;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0400000_I0 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public ViewOnClickCListenerShape2S0400000_I0(Object obj, Object obj2, Object obj3, Object obj4, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A03 = obj2;
        this.A01 = obj3;
        this.A02 = obj4;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        switch (this.A04) {
            case 0:
                AbstractC15340mz r3 = (AbstractC15340mz) this.A03;
                boolean z = r3 instanceof C30241Wq;
                AnonymousClass1OY r9 = (AnonymousClass1OY) this.A00;
                if (z) {
                    Context context = (Context) this.A01;
                    AbstractC14640lm r2 = r3.A0z.A00;
                    if (r2 != null) {
                        String str = ((C30241Wq) r3).A01;
                        C15580nU A04 = C15580nU.A04(r2.getRawString());
                        if (!(str == null || A04 == null || r9.A0r.A0C(A04))) {
                            String rawString = A04.getRawString();
                            Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity");
                            className.putExtra("subgroup_jid", rawString);
                            className.putExtra("parent_group_jid", str);
                            className.putExtra("display_type", 1);
                            r9.A0I.A06(context, className);
                            return;
                        }
                    }
                    if (r9.A0X.A09(r2) != null) {
                        r9.A0I.A08(context, new C14960mK().A0i(context, r2), "ConversationRow");
                        return;
                    }
                    return;
                }
                C15650ng r0 = r9.A0p;
                AnonymousClass1IS r1 = r3.A0z;
                AbstractC15340mz A03 = r0.A0K.A03(r1);
                if (A03 == null && r1.A02) {
                    C15650ng r5 = r9.A0p;
                    A03 = r5.A0K.A03(new AnonymousClass1IS(AnonymousClass1VX.A00, r1.A01, true));
                }
                if (r3 instanceof AnonymousClass1XV) {
                    AnonymousClass1XV r32 = (AnonymousClass1XV) r3;
                    if (r32.A0z.A01.startsWith("product_inquiry")) {
                        r9.A14(r9.A08.findViewById(R.id.quoted_thumb), r32, true);
                        return;
                    }
                }
                if (A03 != null) {
                    AnonymousClass1IS r52 = A03.A0z;
                    AbstractC14640lm r12 = r52.A00;
                    if (!C15380n4.A0N(r12)) {
                        AnonymousClass009.A05(r12);
                        if (!r12.equals(((AbstractC15340mz) this.A02).A0z.A00)) {
                            C14960mK r02 = new C14960mK();
                            Context context2 = (Context) this.A01;
                            Intent A0i = r02.A0i(context2, r12);
                            A0i.putExtra("row_id", A03.A11);
                            A0i.putExtra("sort_id", A03.A12);
                            A0i.putExtra("start_t", SystemClock.uptimeMillis());
                            r9.A1B.A00();
                            C38211ni.A00(A0i, r52);
                            r9.A0I.A08(context2, A0i, "ConversationRow");
                            return;
                        }
                        Conversation conversation = (Conversation) this.A01;
                        Stack stack = conversation.A55;
                        if (!stack.isEmpty() && ((C90794Pg) stack.peek()).A01.A11 != r9.getFMessage().A11) {
                            stack.clear();
                        }
                        stack.push(new C90794Pg(A03, r9.getFMessage(), r9.getTop()));
                        conversation.A3I(A03, conversation.getResources().getDimensionPixelSize(R.dimen.conversation_row_min_height));
                        if ((A03 instanceof C30061Vy) && C28111Kr.A00) {
                            AnonymousClass1OY A00 = conversation.A1g.A00(r52);
                            if (A00 instanceof AbstractC42481vH) {
                                ((AbstractC42481vH) A00).Ae9();
                                return;
                            }
                            return;
                        }
                        return;
                    } else if (!r9.A0w.A0H(A03)) {
                        Context context3 = (Context) this.A01;
                        Intent A0F = C14960mK.A0F(context3, A03.A0B(), Boolean.FALSE);
                        C38211ni.A00(A0F, r52);
                        context3.startActivity(A0F);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 1:
                ((C235812f) this.A03).A02(13);
                ActivityC13810kN r13 = (ActivityC13810kN) ((Activity) this.A01);
                C16700pc.A0E(r13, 0);
                C18120rw.A00(r13, (C18120rw) this.A02);
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
