package com.whatsapp.util;

import X.AbstractActivityC13750kH;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14940mI;
import X.AbstractC15160mf;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC28551Oa;
import X.AbstractC32741cf;
import X.AbstractC35731ia;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01T;
import X.AnonymousClass03U;
import X.AnonymousClass071;
import X.AnonymousClass073;
import X.AnonymousClass12P;
import X.AnonymousClass19Q;
import X.AnonymousClass1CY;
import X.AnonymousClass1IS;
import X.AnonymousClass1JV;
import X.AnonymousClass1KZ;
import X.AnonymousClass1OY;
import X.AnonymousClass1XF;
import X.AnonymousClass1XO;
import X.AnonymousClass1XP;
import X.AnonymousClass1s8;
import X.AnonymousClass2Dz;
import X.AnonymousClass2Uy;
import X.AnonymousClass2V3;
import X.AnonymousClass2V4;
import X.AnonymousClass2V5;
import X.AnonymousClass2V6;
import X.AnonymousClass2V7;
import X.AnonymousClass2V8;
import X.AnonymousClass2V9;
import X.AnonymousClass2VA;
import X.AnonymousClass2VC;
import X.AnonymousClass2VD;
import X.AnonymousClass2VE;
import X.AnonymousClass2VF;
import X.AnonymousClass2VH;
import X.AnonymousClass2VI;
import X.AnonymousClass2VJ;
import X.AnonymousClass2VK;
import X.AnonymousClass2VM;
import X.AnonymousClass2VN;
import X.AnonymousClass2VP;
import X.AnonymousClass2VR;
import X.AnonymousClass2VS;
import X.AnonymousClass2VT;
import X.C14320lF;
import X.C14330lG;
import X.C14900mE;
import X.C14930mH;
import X.C14960mK;
import X.C15330mx;
import X.C15370n3;
import X.C15380n4;
import X.C15650ng;
import X.C15670ni;
import X.C16150oX;
import X.C16170oZ;
import X.C16310on;
import X.C16440p1;
import X.C16700pc;
import X.C235512c;
import X.C244415n;
import X.C25991Bp;
import X.C26511Dt;
import X.C30041Vv;
import X.C35551iF;
import X.C35571iH;
import X.C38211ni;
import X.C38671oW;
import X.C41861uH;
import X.C42801vt;
import X.C43951xu;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.Conversation;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.SerializableLocation;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.biz.order.view.fragment.OrderDetailFragment;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.group.GroupAdminPickerActivity;
import com.whatsapp.group.NewGroup;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.StopLiveLocationDialogFragment;
import com.whatsapp.settings.chat.wallpaper.WallpaperCategoriesActivity;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPickerActivity;
import com.whatsapp.stickers.ConfirmPackDeleteDialogFragment;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.Voip;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape4S0200000_I0 extends AbstractView$OnClickListenerC34281fs {
    public Object A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape4S0200000_I0(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A01 = obj;
        this.A00 = obj2;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        String str;
        AnonymousClass2V4 r1;
        File file;
        switch (this.A02) {
            case 0:
                Conversation conversation = (Conversation) this.A00;
                AnonymousClass19Q r12 = conversation.A1I;
                UserJid userJid = (UserJid) this.A01;
                r12.A03(userJid, null, null, 42);
                ((ActivityC13830kP) conversation).A05.Ab2(new RunnableBRunnable0Shape14S0100000_I1(this, 12));
                conversation.Adm(CartFragment.A00(userJid, null, 4));
                return;
            case 1:
                Context context = (Context) this.A00;
                context.startActivity(C14960mK.A0H(context, ((AnonymousClass2V7) this.A01).A04));
                return;
            case 2:
                boolean z = ((AnonymousClass2V7) this.A01).A09;
                Conversation conversation2 = (Conversation) this.A00;
                if (z) {
                    conversation2.startActivity(conversation2.A14.A00(conversation2, null, null, null, "group-suspend-appeal", null, null, null, ((AbstractActivityC13750kH) conversation2).A0W.A00()));
                } else if (!C41861uH.A00(((ActivityC13810kN) conversation2).A0C, conversation2.A2s)) {
                    Intent intent = new Intent(conversation2, GroupAdminPickerActivity.class);
                    intent.putExtra("gid", C15380n4.A03(conversation2.A2c.A0D));
                    conversation2.startActivityForResult(intent, 42);
                }
                conversation2.overridePendingTransition(0, 0);
                return;
            case 3:
                ((AbstractC14940mI) this.A01).ASK();
                return;
            case 4:
                ((AbstractC14940mI) this.A01).AVh();
                return;
            case 5:
                return;
            case 6:
                C14930mH r5 = (C14930mH) this.A00;
                AnonymousClass071 r3 = new AnonymousClass071();
                TextView textView = r5.A05;
                r3.A06(textView);
                r3.A04(300);
                r3.A08(new AnonymousClass2V8((AbstractC14940mI) this.A01, r5));
                AnonymousClass073.A01((ViewGroup) textView.getParent(), r3);
                int dimensionPixelSize = textView.getContext().getResources().getDimensionPixelSize(R.dimen.space_loose);
                textView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                textView.setCompoundDrawablePadding(0);
                textView.setText("");
                return;
            case 7:
                RequestPermissionActivity requestPermissionActivity = (RequestPermissionActivity) this.A00;
                String str2 = requestPermissionActivity.A06;
                if (str2 != null) {
                    requestPermissionActivity.A05.A02(str2, "continue");
                }
                String[] strArr = (String[]) this.A01;
                requestPermissionActivity.A09 = true;
                RequestPermissionActivity.A0P(requestPermissionActivity.A03, strArr);
                AnonymousClass00T.A0E(requestPermissionActivity, strArr, 0);
                AnonymousClass00T.A05(requestPermissionActivity, R.id.permission_request_dialog).setVisibility(8);
                return;
            case 8:
                Runnable runnable = ((AnonymousClass2V9) this.A00).A01;
                if (runnable != null) {
                    runnable.run();
                }
                ((View.OnClickListener) this.A01).onClick(view);
                return;
            case 9:
                AnonymousClass2VA r6 = (AnonymousClass2VA) this.A00;
                ContactInfoActivity contactInfoActivity = r6.A0Y;
                Jid jid = ((C15370n3) this.A01).A0D;
                Integer num = r6.A0q;
                Intent intent2 = new Intent();
                intent2.setClassName(contactInfoActivity.getPackageName(), "com.whatsapp.biz.BusinessProfileExtraFieldsActivity");
                AnonymousClass009.A05(jid);
                intent2.putExtra("jid", jid.getRawString());
                intent2.putExtra("profile_entry_point", num);
                contactInfoActivity.startActivity(intent2);
                r6.A04(8);
                return;
            case 10:
            case 11:
                AnonymousClass2VA r7 = (AnonymousClass2VA) this.A00;
                Jid jid2 = (Jid) this.A01;
                r7.A04(9);
                if (r7.A0I.A0J()) {
                    r7.A0W.A01(r7.A0G, 1);
                }
                ContactInfoActivity contactInfoActivity2 = r7.A0Y;
                Intent intent3 = new Intent();
                intent3.setClassName(contactInfoActivity2.getPackageName(), "com.whatsapp.Conversation");
                Intent addFlags = intent3.putExtra("jid", jid2.getRawString()).putExtra("args_conversation_screen_entry_point", 1).addFlags(335544320);
                Integer num2 = r7.A0q;
                if (num2 != null) {
                    int intValue = num2.intValue();
                    if (intValue == 19) {
                        str = "custom_link";
                    } else if (intValue == 20) {
                        str = "custom_qr_code_link";
                    }
                    addFlags.putExtra("entry_point_conversion_source", str).putExtra("entry_point_conversion_app", "whatsapp");
                }
                r7.A0O.A08(contactInfoActivity2, addFlags, "ContactInfoActivity");
                return;
            case 12:
                AnonymousClass2VA r13 = (AnonymousClass2VA) this.A00;
                r13.A04(13);
                AnonymousClass2VA.A01(r13, 4);
                AnonymousClass12P r52 = r13.A0O;
                ContactInfoActivity contactInfoActivity3 = r13.A0Y;
                r52.A06(contactInfoActivity3, C14960mK.A0M(contactInfoActivity3, (Jid) this.A01, null, 9));
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                ((AbstractC15160mf) this.A01).A04(3);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                UserJid peerJid = Voip.getPeerJid();
                if (peerJid != null) {
                    C14960mK r14 = new C14960mK();
                    Context context2 = (Context) this.A01;
                    Intent A0l = r14.A0l(context2, peerJid, Boolean.FALSE);
                    A0l.putExtra("lobbyEntryPoint", 7);
                    context2.startActivity(A0l);
                    return;
                }
                return;
            case 15:
                AnonymousClass1s8 r0 = (AnonymousClass1s8) this.A01;
                r0.A0F(null, r0.A0Z, null, r0.A0v, null);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ContactInfoActivity contactInfoActivity4 = (ContactInfoActivity) this.A00;
                contactInfoActivity4.A38(1);
                Context applicationContext = contactInfoActivity4.getApplicationContext();
                Intent intent4 = new Intent();
                intent4.setClassName(applicationContext.getPackageName(), "com.whatsapp.Conversation");
                ((ActivityC13790kL) contactInfoActivity4).A00.A08(contactInfoActivity4, intent4.putExtra("jid", C15380n4.A03(((AnonymousClass2Dz) this.A01).A01)).addFlags(335544320), "ContactInfoActivity");
                return;
            case 17:
                AnonymousClass1IS r2 = ((AbstractC15340mz) this.A01).A0z;
                AbstractC14640lm r4 = r2.A00;
                AnonymousClass009.A05(r4);
                boolean z2 = r2.A02;
                AbstractC28551Oa r02 = (AbstractC28551Oa) this.A00;
                if (z2) {
                    ((ActivityC13810kN) AnonymousClass12P.A00(r02.getContext())).Adm(StopLiveLocationDialogFragment.A00(r4, r2.A01));
                    return;
                }
                C244415n r22 = r02.A0N;
                AnonymousClass009.A05(r22);
                Context context3 = r02.getContext();
                AnonymousClass009.A05(r4);
                r22.A08(context3, r4, null);
                return;
            case 18:
                AnonymousClass1OY r23 = (AnonymousClass1OY) this.A00;
                ActivityC13810kN r53 = (ActivityC13810kN) AbstractC35731ia.A01((Context) this.A01, ActivityC13810kN.class);
                AnonymousClass1XF r72 = (AnonymousClass1XF) ((AbstractC28551Oa) r23).A0O;
                if (!(r53 == null || r72.A03 == null || r72.A06 == null || r72.A08 == null)) {
                    r23.A0T.A00(8);
                    r23.A0T.A01(r72.A03, null, null, 44, null, null, null, r72.A06, null, null, 38);
                    AnonymousClass1IS r62 = r72.A0z;
                    UserJid userJid2 = r72.A03;
                    UserJid of = UserJid.of(r62.A00);
                    AnonymousClass009.A05(of);
                    String str3 = r72.A06;
                    String str4 = r72.A08;
                    C16700pc.A0E(userJid2, 1);
                    C16700pc.A0E(of, 2);
                    C16700pc.A0E(str3, 3);
                    C16700pc.A0E(str4, 4);
                    r53.Adm(OrderDetailFragment.A00(userJid2, of, r62, str3, str4));
                    return;
                }
                return;
            case 19:
                if (view instanceof AbstractC28551Oa) {
                    AbstractC15340mz fMessage = ((AbstractC28551Oa) view).getFMessage();
                    long A01 = C30041Vv.A01(fMessage);
                    long A02 = C30041Vv.A02(fMessage);
                    C14960mK r15 = new C14960mK();
                    Context context4 = (Context) this.A01;
                    AnonymousClass1IS r24 = fMessage.A0z;
                    Intent A0i = r15.A0i(context4, r24.A00);
                    A0i.putExtra("row_id", A01);
                    A0i.putExtra("sort_id", A02);
                    C38211ni.A00(A0i, r24);
                    ((AnonymousClass12P) this.A00).A07(context4, A0i);
                    return;
                }
                return;
            case C43951xu.A01:
                C16170oZ r63 = (C16170oZ) this.A01;
                AbstractC14640lm r73 = (AbstractC14640lm) this.A00;
                StringBuilder sb = new StringBuilder("UserActions/SetChatArchived; jid=");
                sb.append(r73);
                sb.append("; archive=");
                sb.append(false);
                Log.i(sb.toString());
                r63.A0a.A05(r73, false);
                String A0I = r63.A0U.A0I(new Object[]{1}, R.plurals.conversations_unarchived_confirmation, 1);
                C14900mE r25 = r63.A05;
                r25.A0E(A0I, 0);
                r25.A0H(new RunnableBRunnable0Shape0S0200000_I0(r63, 29, r73));
                r63.A1C.A04(r73, 4, 0, 0);
                return;
            case 21:
                ((C15330mx) this.A01).A02();
                return;
            case 22:
                NewGroup newGroup = (NewGroup) this.A00;
                if (newGroup.A07.getText().toString().trim().length() == 0) {
                    ((ActivityC13810kN) newGroup).A05.A07(R.string.new_group_info_prompt, 0);
                    return;
                }
                List list = (List) this.A01;
                String A04 = AbstractC32741cf.A04(newGroup.A07.getText().toString());
                int A00 = AnonymousClass2VC.A00(A04);
                int A022 = ((ActivityC13810kN) newGroup).A06.A02(AbstractC15460nI.A2A);
                if (A00 > A022) {
                    ((ActivityC13810kN) newGroup).A05.A0E(newGroup.getResources().getQuantityString(R.plurals.subject_reach_limit, A022, Integer.valueOf(A022)), 0);
                    return;
                } else if (!list.isEmpty() || newGroup.A0X) {
                    if (newGroup.A0X) {
                        File A002 = newGroup.A0A.A00(newGroup.A0b);
                        Uri uri = null;
                        if (A002 != null && A002.exists()) {
                            uri = Uri.fromFile(A002);
                        }
                        AnonymousClass1JV A07 = newGroup.A0O.A07();
                        Intent intent5 = new Intent();
                        intent5.putExtra("group_created", new AnonymousClass2VD(uri, A07, A04, newGroup.A00).A01());
                        newGroup.setResult(-1, intent5);
                    } else {
                        AnonymousClass1JV A072 = newGroup.A0O.A07();
                        newGroup.A0O.A0K(A072, list, true);
                        if (((ActivityC13810kN) newGroup).A07.A0B()) {
                            StringBuilder sb2 = new StringBuilder("newgroup/go create group:");
                            sb2.append(A072);
                            Log.i(sb2.toString());
                            newGroup.A2C(R.string.creating_group);
                            newGroup.A05 = new AnonymousClass01T(A072, new RunnableBRunnable0Shape0S1300000_I0(newGroup, A072, list, A04, 5));
                            newGroup.A0H.A0S(newGroup.A0T.A03(A072, A04, list, 2, newGroup.A00, ((ActivityC13790kL) newGroup).A05.A00()));
                            ((ActivityC13810kN) newGroup).A05.A0J(new RunnableBRunnable0Shape6S0100000_I0_6(newGroup, 47), 10000);
                            return;
                        }
                        Log.i("newgroup/no network access, fail to create group");
                        newGroup.A0H.A0S(newGroup.A0T.A03(A072, A04, list, 3, newGroup.A00, ((ActivityC13790kL) newGroup).A05.A00()));
                        File A003 = newGroup.A0A.A00(newGroup.A0b);
                        if (A003 != null && A003.exists()) {
                            try {
                                C42801vt A012 = newGroup.A0S.A01(A003);
                                newGroup.A0B.A01(newGroup.A08.A0B(A072), A012.A00, A012.A01);
                            } catch (IOException e) {
                                Log.e("newgroup/failed to update photo", e);
                            }
                        }
                        newGroup.setResult(-1);
                    }
                    newGroup.finish();
                    return;
                } else {
                    ((ActivityC13810kN) newGroup).A05.A07(R.string.no_valid_participant, 0);
                    return;
                }
            case 23:
                C38211ni.A05((Activity) this.A01);
                return;
            case 24:
                AnonymousClass2VE r16 = (AnonymousClass2VE) this.A00;
                C244415n r42 = r16.A03;
                Context context5 = r16.A00;
                SerializableLocation serializableLocation = ((InteractiveAnnotation) this.A01).serializableLocation;
                r42.A09(context5, serializableLocation.name, null, serializableLocation.latitude, serializableLocation.longitude);
                r16.A02.dismiss();
                return;
            case 25:
                ((AnonymousClass2VF) this.A00).A08.A06.A0B(Long.valueOf(((AnonymousClass2VH) this.A01).A00));
                return;
            case 26:
                AnonymousClass2VI r03 = (AnonymousClass2VI) this.A00;
                C244415n r17 = r03.A0B;
                Context context6 = r03.getContext();
                AnonymousClass1XO r04 = (AnonymousClass1XO) this.A01;
                r17.A09(context6, r04.A01, r04.A00, ((AnonymousClass1XP) r04).A00, ((AnonymousClass1XP) r04).A01);
                return;
            case 27:
                AnonymousClass2Uy r18 = (AnonymousClass2Uy) this.A00;
                Activity A004 = AnonymousClass12P.A00(r18.getContext());
                if (A004 instanceof ActivityC13810kN) {
                    C16440p1 r74 = (C16440p1) this.A01;
                    AnonymousClass1CY r8 = r18.A08;
                    C14900mE r54 = r18.A04;
                    AbstractC15710nm r32 = r18.A03;
                    AbstractC14440lR r9 = r18.A09;
                    AnonymousClass12P r26 = r18.A02;
                    C15670ni r64 = r18.A07;
                    ActivityC13810kN r43 = (ActivityC13810kN) A004;
                    if (RequestPermissionActivity.A0W(r43, r18.A06)) {
                        C16150oX r19 = ((AbstractC16130oV) r74).A02;
                        AnonymousClass009.A05(r19);
                        if ((r74.A0z.A02 || r19.A0P) && (file = r19.A0F) != null && file.exists()) {
                            C26511Dt.A06(r26, r32, r43, r54, r64, r74, r8, r9);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case 28:
                int A005 = ((AnonymousClass03U) this.A01).A00();
                AnonymousClass2VJ r05 = (AnonymousClass2VJ) this.A00;
                AnonymousClass2VK r33 = r05.A05;
                WallpaperCategoriesActivity.A02(r33.A00, ((Number) r05.A08.get(A005)).intValue(), r33.A01);
                return;
            case 29:
                int A006 = ((AnonymousClass03U) this.A01).A00();
                AnonymousClass2VM r27 = (AnonymousClass2VM) this.A00;
                if (r27.getItemViewType(A006) == 1) {
                    A006--;
                }
                AnonymousClass2VN r06 = r27.A01;
                ArrayList arrayList = new ArrayList();
                DownloadableWallpaperPickerActivity downloadableWallpaperPickerActivity = r06.A00;
                List<File> list2 = downloadableWallpaperPickerActivity.A07;
                AnonymousClass009.A05(list2);
                for (File file2 : list2) {
                    arrayList.add(Uri.fromFile(file2));
                }
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                AnonymousClass01T r07 = downloadableWallpaperPickerActivity.A02;
                if (r07 != null) {
                    Object obj = r07.A00;
                    AnonymousClass009.A05(obj);
                    arrayList2.addAll((Collection) obj);
                    Object obj2 = downloadableWallpaperPickerActivity.A02.A01;
                    AnonymousClass009.A05(obj2);
                    arrayList3.addAll((Collection) obj2);
                }
                AbstractC14640lm r44 = ((AnonymousClass2VP) downloadableWallpaperPickerActivity).A00;
                boolean z3 = ((AnonymousClass2VP) downloadableWallpaperPickerActivity).A01;
                Intent className = new Intent().setClassName(downloadableWallpaperPickerActivity.getPackageName(), "com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPreviewActivity");
                className.putExtra("STARTING_POSITION_KEY", A006);
                className.putExtra("THUMBNAIL_URIS_KEY", arrayList);
                className.putExtra("WHATSAPP_THUMBNAIL_RES_KEY", arrayList2);
                className.putExtra("WHATSAPP_FULL_RES_KEY", arrayList3);
                className.putExtra("chat_jid", C15380n4.A03(r44));
                className.putExtra("is_using_global_wallpaper", z3);
                downloadableWallpaperPickerActivity.startActivityForResult(className, 111);
                return;
            case C25991Bp.A0S:
                C35571iH r28 = (C35571iH) this.A01;
                r28.A06.setVisibility(0);
                r28.A06.setAlpha(1.0f);
                ((C35551iF) this.A00).A0R();
                return;
            case 31:
                AnonymousClass1KZ r55 = (AnonymousClass1KZ) this.A01;
                if (!r55.A05) {
                    StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = ((AnonymousClass2V3) this.A00).A01;
                    C235512c r34 = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0C;
                    r34.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r34, 4, r55));
                    ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0C.A0H(r55, null, 2, false);
                    return;
                }
                return;
            case 32:
                StickerStoreMyTabFragment stickerStoreMyTabFragment = ((AnonymousClass2VT) this.A00).A00;
                if (stickerStoreMyTabFragment.A0H != null) {
                    ConfirmPackDeleteDialogFragment.A00((AnonymousClass1KZ) this.A01).A1F(stickerStoreMyTabFragment.A0H, "confirm_delete");
                    return;
                }
                return;
            case 33:
                AnonymousClass1KZ r45 = (AnonymousClass1KZ) this.A01;
                if (!r45.A05) {
                    ((C38671oW) this.A00).A01.A0C.A0H(r45, null, 2, true);
                    return;
                }
                return;
            case 34:
                TextStatusComposerActivity textStatusComposerActivity = (TextStatusComposerActivity) this.A00;
                C14320lF r08 = textStatusComposerActivity.A0D;
                if (!(r08 == null || (r1 = r08.A06) == null || r1.A02 == null)) {
                    String str5 = r1.A01;
                    if ("video/mp4".equals(str5) || "image/gif".equals(str5)) {
                        textStatusComposerActivity.A0V.setImageProgressBarVisibility(true);
                        ((View) this.A01).setVisibility(8);
                        AbstractC14440lR r65 = ((ActivityC13830kP) textStatusComposerActivity).A05;
                        C14330lG r56 = ((ActivityC13810kN) textStatusComposerActivity).A04;
                        AnonymousClass2V4 r09 = textStatusComposerActivity.A0D.A06;
                        r65.Aaz(new AnonymousClass2VS(r56, new AnonymousClass2VR(this), r09.A02, r09.A01), new String[0]);
                        return;
                    }
                    return;
                }
                return;
            case 35:
                AnonymousClass2V5 r66 = (AnonymousClass2V5) this.A01;
                AbstractC14640lm r29 = r66.A00;
                if (r29 == null) {
                    C15650ng r10 = r66.A03.A0C;
                    long j = r66.A01;
                    C16310on A013 = r10.A0t.get();
                    try {
                        Cursor A09 = A013.A03.A09("SELECT chat_row_id FROM message_view WHERE _id = ?", new String[]{Long.toString(j)});
                        if (A09.moveToNext()) {
                            r29 = r10.A0N.A06(A09);
                            A09.close();
                            A013.close();
                        } else {
                            A09.close();
                            A013.close();
                            r29 = null;
                        }
                        r66.A00 = r29;
                        if (r29 == null) {
                            return;
                        }
                    } catch (Throwable th) {
                        try {
                            A013.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                C15370n3 A0A = r66.A03.A03.A0A(r29);
                if (A0A != null) {
                    C14960mK r110 = new C14960mK();
                    ViewSharedContactArrayActivity viewSharedContactArrayActivity = ((AnonymousClass2V6) this.A00).A01;
                    Intent A0g = r110.A0g(viewSharedContactArrayActivity, A0A);
                    long j2 = r66.A01;
                    A0g.putExtra("row_id", j2);
                    AbstractC15340mz A007 = viewSharedContactArrayActivity.A0C.A0K.A00(j2);
                    A0g.putExtra("sort_id", A007.A12);
                    C38211ni.A00(A0g, A007.A0z);
                    ((ActivityC13790kL) viewSharedContactArrayActivity).A00.A07(viewSharedContactArrayActivity, A0g);
                    return;
                }
                return;
            default:
                super.A04(view);
                return;
        }
    }
}
