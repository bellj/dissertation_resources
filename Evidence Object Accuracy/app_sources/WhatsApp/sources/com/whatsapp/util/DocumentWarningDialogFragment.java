package com.whatsapp.util;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AnonymousClass12P;
import X.C004802e;
import X.C14900mE;
import X.C15450nH;
import X.C15650ng;
import X.C15670ni;
import X.C16440p1;
import X.C38961p1;
import X.C38971p2;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.util.DocumentWarningDialogFragment;
import java.lang.ref.WeakReference;

/* loaded from: classes2.dex */
public class DocumentWarningDialogFragment extends Hilt_DocumentWarningDialogFragment {
    public AnonymousClass12P A00;
    public AbstractC15710nm A01;
    public C14900mE A02;
    public C15450nH A03;
    public C15650ng A04;
    public C15670ni A05;
    public AbstractC14440lR A06;

    public static DocumentWarningDialogFragment A00(int i, long j) {
        DocumentWarningDialogFragment documentWarningDialogFragment = new DocumentWarningDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("message_id", j);
        bundle.putInt("warning_id", i);
        documentWarningDialogFragment.A0U(bundle);
        return documentWarningDialogFragment;
    }

    public static /* synthetic */ void A01(DocumentWarningDialogFragment documentWarningDialogFragment) {
        C16440p1 r8 = (C16440p1) documentWarningDialogFragment.A04.A0K.A00(documentWarningDialogFragment.A03().getLong("message_id"));
        if (r8 != null && ((AbstractC16130oV) r8).A02 != null) {
            C14900mE r9 = documentWarningDialogFragment.A02;
            AbstractC15710nm r7 = documentWarningDialogFragment.A01;
            AbstractC14440lR r6 = documentWarningDialogFragment.A06;
            C15670ni r5 = documentWarningDialogFragment.A05;
            Context A0p = documentWarningDialogFragment.A0p();
            AnonymousClass12P r4 = documentWarningDialogFragment.A00;
            WeakReference weakReference = new WeakReference(A0p);
            r9.A06(0, R.string.loading_spinner);
            C38961p1 r2 = new C38961p1(r4, r9, r8, weakReference);
            C38971p2 r1 = new C38971p2(r7, r5, r8);
            r1.A01(r2, r9.A06);
            r6.Ab2(r1);
            ((AbstractC16130oV) r8).A02.A07 = 2;
            documentWarningDialogFragment.A04.A0W(r8);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e r3 = new C004802e(A0p());
        r3.A0A(A0I(A03().getInt("warning_id", R.string.warning_opening_document)));
        r3.setPositiveButton(R.string.open, new DialogInterface.OnClickListener() { // from class: X.4gK
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DocumentWarningDialogFragment.A01(DocumentWarningDialogFragment.this);
            }
        });
        r3.setNegativeButton(R.string.cancel, null);
        return r3.create();
    }
}
