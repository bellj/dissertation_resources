package com.whatsapp.catalogsearch.view.fragment;

import X.AbstractC14440lR;
import X.AbstractC16710pd;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass017;
import X.AnonymousClass018;
import X.AnonymousClass02M;
import X.AnonymousClass19Q;
import X.AnonymousClass1CR;
import X.AnonymousClass1M2;
import X.AnonymousClass1lK;
import X.AnonymousClass2fT;
import X.AnonymousClass419;
import X.AnonymousClass41B;
import X.AnonymousClass41C;
import X.AnonymousClass41J;
import X.AnonymousClass41L;
import X.AnonymousClass4A1;
import X.AnonymousClass4EC;
import X.AnonymousClass4EF;
import X.AnonymousClass4JC;
import X.AnonymousClass4KB;
import X.AnonymousClass4UV;
import X.AnonymousClass4Yq;
import X.AnonymousClass5TQ;
import X.C103744rF;
import X.C113805Jb;
import X.C113815Jc;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C16700pc;
import X.C16760pi;
import X.C21770xx;
import X.C22700zV;
import X.C25791Av;
import X.C34271fr;
import X.C42631vX;
import X.C48232Fc;
import X.C48882Ih;
import X.C71943dl;
import X.C71953dm;
import X.C71963dn;
import X.C71973do;
import X.C71993dq;
import X.C75013j9;
import X.C84463zL;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public final class CatalogSearchFragment extends Hilt_CatalogSearchFragment {
    public int A00;
    public MenuItem A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public TextView A09;
    public TextView A0A;
    public Toolbar A0B;
    public RecyclerView A0C;
    public C48882Ih A0D;
    public AnonymousClass4JC A0E;
    public C48232Fc A0F;
    public WaButton A0G;
    public C25791Av A0H;
    public C21770xx A0I;
    public AnonymousClass1CR A0J;
    public AnonymousClass19Q A0K;
    public AnonymousClass4EC A0L;
    public Button A0M;
    public C22700zV A0N;
    public AnonymousClass018 A0O;
    public UserJid A0P;
    public AbstractC14440lR A0Q;
    public boolean A0R;
    public boolean A0S;
    public final AnonymousClass4UV A0T = new C84463zL(this);
    public final AbstractC16710pd A0U = AnonymousClass4Yq.A00(new C71943dl(this));
    public final AbstractC16710pd A0V = AnonymousClass4Yq.A00(new C71953dm(this));
    public final AbstractC16710pd A0W = AnonymousClass4Yq.A00(new C71963dn(this));
    public final AbstractC16710pd A0X = AnonymousClass4Yq.A00(new C71973do(this));
    public final AbstractC16710pd A0Y = AnonymousClass4Yq.A00(new C113805Jb(this));
    public final AbstractC16710pd A0Z = AnonymousClass4Yq.A00(new C113815Jc(this));
    public final AbstractC16710pd A0a = AnonymousClass4Yq.A00(new C71993dq(this));

    public static final /* synthetic */ C34271fr A00(CatalogSearchFragment catalogSearchFragment, AnonymousClass4EF r5) {
        int i;
        if (r5 instanceof AnonymousClass41C) {
            i = R.string.catalog_search_snackbar_no_network_error_view_text;
        } else if (r5 instanceof AnonymousClass41B) {
            i = R.string.catalog_search_error_view_text;
        } else {
            throw C12990iw.A0v();
        }
        String A0I = catalogSearchFragment.A0I(i);
        C16700pc.A0B(A0I);
        if (catalogSearchFragment.A0L != null) {
            String A0I2 = catalogSearchFragment.A0I(R.string.ok);
            C16700pc.A0B(A0I2);
            C34271fr A00 = C34271fr.A00(catalogSearchFragment.A05(), A0I, 4000);
            A00.A07(A0I2, new ViewOnClickCListenerShape8S0100000_I1_2(A00, 1));
            return A00;
        }
        throw C16700pc.A06("config");
    }

    public static final List A01(AnonymousClass4KB r3) {
        List list = r3.A00;
        ArrayList A0l = C12960it.A0l();
        for (Object obj : list) {
            if (obj instanceof AnonymousClass419) {
                A0l.add(obj);
            }
        }
        ArrayList A0E = C16760pi.A0E(A0l);
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            A0E.add(((AnonymousClass419) it.next()).A00);
        }
        return A0E;
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        boolean A0N = C16700pc.A0N(menu, menuInflater);
        MenuItem findItem = menu.findItem(R.id.menuitem_search);
        C16700pc.A0B(findItem);
        this.A01 = findItem;
        findItem.setVisible(A0N);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        C16700pc.A0E(menuItem, 0);
        if (R.id.menuitem_search != menuItem.getItemId()) {
            return false;
        }
        View view = this.A02;
        if (view == null) {
            throw C16700pc.A06("containerSearch");
        }
        view.setVisibility(0);
        C48232Fc r0 = this.A0F;
        if (r0 == null) {
            throw C16700pc.A06("searchToolbarHelper");
        }
        r0.A01();
        CatalogSearchViewModel A0X = C12990iw.A0X(this);
        UserJid userJid = this.A0P;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        int i = this.A00;
        C12990iw.A0P(A0X.A06).A0B(new AnonymousClass41J(A0X.A02.A01.A07(1514)));
        AnonymousClass1CR.A00(A0X.A03, userJid, C12960it.A0V(), i);
        A0X.A04.A01.A0B("");
        View view2 = this.A05;
        if (view2 == null) {
            throw C16700pc.A06("searchMenuHolderView");
        }
        C12960it.A0y(view2.findViewById(R.id.search_back), this, 2);
        View view3 = this.A05;
        if (view3 == null) {
            throw C16700pc.A06("searchMenuHolderView");
        }
        C48232Fc.A00(view3);
        C48232Fc r02 = this.A0F;
        if (r02 == null) {
            throw C16700pc.A06("searchToolbarHelper");
        }
        TextView textView = (TextView) C16700pc.A02(r02.A02, R.id.search_src_text);
        textView.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(500)});
        C12960it.A0s(A01(), textView, R.color.search_text_color);
        textView.setHintTextColor(AnonymousClass00T.A00(A01(), R.color.hint_text));
        textView.setTextSize(0, A01().getResources().getDimension(R.dimen.catalog_search_hint_text_size));
        C22700zV r1 = this.A0N;
        if (r1 != null) {
            UserJid userJid2 = this.A0P;
            if (userJid2 == null) {
                throw C16700pc.A06("bizJid");
            }
            AnonymousClass1M2 A00 = r1.A00(userJid2);
            if (A00 != null) {
                textView.setHint(C12970iu.A0q(this, A00.A08, new Object[1], 0, R.string.search_text_hint));
            }
            C48232Fc r03 = this.A0F;
            if (r03 == null) {
                throw C16700pc.A06("searchToolbarHelper");
            }
            r03.A02.A08 = new View.OnFocusChangeListener() { // from class: X.4ml
                @Override // android.view.View.OnFocusChangeListener
                public final void onFocusChange(View view4, boolean z) {
                    CatalogSearchFragment catalogSearchFragment = CatalogSearchFragment.this;
                    C16700pc.A0E(catalogSearchFragment, 0);
                    if (z) {
                        ((CatalogSearchViewModel) catalogSearchFragment.A0a.getValue()).A05(catalogSearchFragment.A1A());
                    }
                }
            };
            return true;
        }
        throw C16700pc.A06("verifiedNameManager");
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.fragment_catalog_search, viewGroup, false);
        this.A02 = C16700pc.A03(inflate, R.id.container_catalog_search);
        this.A03 = C16700pc.A03(inflate, R.id.search_call_to_action);
        this.A09 = (TextView) C16700pc.A03(inflate, R.id.search_call_to_action_text);
        this.A0C = (RecyclerView) C16700pc.A03(inflate, R.id.list_search_result);
        this.A04 = C16700pc.A03(inflate, R.id.search_child_fragment_holder);
        this.A06 = C16700pc.A03(inflate, R.id.search_result_list_holder);
        this.A0M = (Button) C16700pc.A03(inflate, R.id.view_cart);
        this.A07 = C16700pc.A03(inflate, R.id.shadow_bottom_search_result_list);
        this.A08 = C16700pc.A03(inflate, R.id.search_results_error_view_holder);
        this.A0A = (TextView) C16700pc.A03(inflate, R.id.search_results_error_view_text);
        this.A0G = (WaButton) C16700pc.A03(inflate, R.id.search_results_error_view_retry_btn);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        C25791Av r1 = this.A0H;
        if (r1 != null) {
            r1.A04(this.A0T);
            super.A11();
            return;
        }
        throw C16700pc.A06("cartObservers");
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        ((AnonymousClass2fT) this.A0X.getValue()).A04.A00();
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        if (this.A0R) {
            this.A0R = false;
        } else {
            A1E(false);
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A0M();
        Parcelable parcelable = A03().getParcelable("category_biz_id");
        C16700pc.A0C(parcelable);
        C16700pc.A0B(parcelable);
        this.A0P = (UserJid) parcelable;
        this.A00 = A03().getInt("search_entry_point");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        View findViewById = A0C().findViewById(R.id.toolbar);
        C16700pc.A0B(findViewById);
        this.A0B = (Toolbar) findViewById;
        View findViewById2 = A0C().findViewById(R.id.search_holder);
        C16700pc.A0B(findViewById2);
        this.A05 = findViewById2;
        if (this.A0B == null) {
            throw C16700pc.A06("toolbarView");
        }
        ActivityC000900k A0C = A0C();
        AnonymousClass018 r6 = this.A0O;
        if (r6 != null) {
            View view2 = this.A05;
            if (view2 == null) {
                throw C16700pc.A06("searchMenuHolderView");
            }
            Toolbar toolbar = this.A0B;
            if (toolbar == null) {
                throw C16700pc.A06("toolbarView");
            }
            this.A0F = new C48232Fc(A0C, view2, new C103744rF(this), toolbar, r6);
            View view3 = this.A03;
            if (view3 == null) {
                throw C16700pc.A06("searchCallToActionView");
            }
            C12960it.A0y(view3, this, 4);
            View view4 = this.A03;
            if (view4 == null) {
                throw C16700pc.A06("searchCallToActionView");
            }
            C42631vX.A00(view4);
            RecyclerView recyclerView = this.A0C;
            if (recyclerView == null) {
                throw C16700pc.A06("searchResultList");
            }
            recyclerView.setAdapter((AnonymousClass02M) this.A0U.getValue());
            RecyclerView recyclerView2 = this.A0C;
            if (recyclerView2 == null) {
                throw C16700pc.A06("searchResultList");
            }
            recyclerView2.A0n(new C75013j9(this));
            RecyclerView recyclerView3 = this.A0C;
            if (recyclerView3 == null) {
                throw C16700pc.A06("searchResultList");
            }
            recyclerView3.setItemAnimator(null);
            AbstractC16710pd r3 = this.A0a;
            C12960it.A19(A0G(), (AnonymousClass017) C16700pc.A05(((CatalogSearchViewModel) r3.getValue()).A07), this, 55);
            C12960it.A19(A0G(), ((CatalogSearchViewModel) r3.getValue()).A00, this, 56);
            C12960it.A19(A0G(), ((CatalogSearchViewModel) r3.getValue()).A01, this, 54);
            AbstractC16710pd r32 = this.A0X;
            C12960it.A19(A0G(), ((AnonymousClass2fT) r32.getValue()).A01, this, 53);
            Button button = this.A0M;
            if (button == null) {
                throw C16700pc.A06("viewCartButton");
            }
            AbstractView$OnClickListenerC34281fs.A00(button, this, 40);
            C25791Av r1 = this.A0H;
            if (r1 != null) {
                r1.A03(this.A0T);
                C12960it.A19(A0G(), ((AnonymousClass2fT) r32.getValue()).A00, this, 57);
                WaButton waButton = this.A0G;
                if (waButton == null) {
                    throw C16700pc.A06("searchResultsErrorViewRetryButton");
                }
                C12960it.A0y(waButton, this, 3);
                return;
            }
            throw C16700pc.A06("cartObservers");
        }
        throw C16700pc.A06("whatsAppLocale");
    }

    public final String A1A() {
        String str = (String) C12990iw.A0X(this).A00.A01();
        if (str == null) {
            return "";
        }
        return str;
    }

    public final void A1B() {
        RecyclerView recyclerView = this.A0C;
        if (recyclerView == null) {
            throw C16700pc.A06("searchResultList");
        }
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (linearLayoutManager != null && C12980iv.A0A(linearLayoutManager) <= 4 && !((AnonymousClass1lK) this.A0U.getValue()).A0I() && !this.A0S) {
            CatalogSearchViewModel A0X = C12990iw.A0X(this);
            String A1A = A1A();
            UserJid userJid = this.A0P;
            if (userJid == null) {
                throw C16700pc.A06("bizJid");
            }
            C16700pc.A0E(A1A, 0);
            A0X.A05.A01(AnonymousClass4A1.A01, userJid, A1A);
        }
    }

    public final void A1C() {
        View view;
        int i;
        if (!((AnonymousClass1lK) this.A0U.getValue()).A05.isEmpty()) {
            RecyclerView recyclerView = this.A0C;
            if (recyclerView == null) {
                throw C16700pc.A06("searchResultList");
            } else if (recyclerView.canScrollVertically(1)) {
                view = this.A07;
                if (view == null) {
                    throw C16700pc.A06("searchResultListShadowBottom");
                }
                i = 0;
                view.setVisibility(i);
            }
        }
        view = this.A07;
        if (view == null) {
            throw C16700pc.A06("searchResultListShadowBottom");
        }
        i = 8;
        view.setVisibility(i);
    }

    public final void A1D(String str) {
        this.A0S = false;
        C48232Fc r0 = this.A0F;
        if (r0 == null) {
            throw C16700pc.A06("searchToolbarHelper");
        }
        r0.A02.clearFocus();
        AnonymousClass1lK.A00(this.A0U);
        AbstractC16710pd r4 = this.A0a;
        CatalogSearchViewModel catalogSearchViewModel = (CatalogSearchViewModel) r4.getValue();
        UserJid userJid = this.A0P;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        C16700pc.A0E(str, 0);
        catalogSearchViewModel.A04(AnonymousClass41L.A00);
        catalogSearchViewModel.A05.A01(AnonymousClass4A1.A02, userJid, str);
        CatalogSearchViewModel catalogSearchViewModel2 = (CatalogSearchViewModel) r4.getValue();
        UserJid userJid2 = this.A0P;
        if (userJid2 == null) {
            throw C16700pc.A06("bizJid");
        }
        AnonymousClass1CR.A00(catalogSearchViewModel2.A03, userJid2, C12970iu.A0g(), this.A00);
    }

    public final void A1E(boolean z) {
        View view = this.A02;
        if (view == null) {
            throw C16700pc.A06("containerSearch");
        }
        view.setVisibility(8);
        C48232Fc r0 = this.A0F;
        if (r0 == null) {
            throw C16700pc.A06("searchToolbarHelper");
        }
        r0.A04(z);
        CatalogSearchViewModel A0X = C12990iw.A0X(this);
        UserJid userJid = this.A0P;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        AnonymousClass1CR.A00(A0X.A03, userJid, 7, this.A00);
    }

    public final boolean A1F() {
        View view = this.A02;
        if (view == null) {
            throw C16700pc.A06("containerSearch");
        } else if (view.getVisibility() != 0) {
            return false;
        } else {
            A1E(true);
            ActivityC000900k A0C = A0C();
            if (A0C instanceof AnonymousClass5TQ) {
                ((AnonymousClass5TQ) A0C).ANm();
            }
            return true;
        }
    }
}
