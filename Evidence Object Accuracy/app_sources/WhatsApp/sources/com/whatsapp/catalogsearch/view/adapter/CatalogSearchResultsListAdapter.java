package com.whatsapp.catalogsearch.view.adapter;

import X.AbstractC001200n;
import X.AbstractC116525Vu;
import X.AbstractC59092tx;
import X.AbstractC75723kJ;
import X.AnonymousClass018;
import X.AnonymousClass03U;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass12P;
import X.AnonymousClass19Q;
import X.AnonymousClass19T;
import X.AnonymousClass1lK;
import X.AnonymousClass1lL;
import X.AnonymousClass23N;
import X.AnonymousClass5TV;
import X.C12960it;
import X.C12980iv;
import X.C14900mE;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C16700pc;
import X.C22700zV;
import X.C37071lG;
import X.C44691zO;
import X.C59302uQ;
import X.C84663zg;
import X.C84693zj;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public final class CatalogSearchResultsListAdapter extends AbstractC59092tx implements AnonymousClass054 {
    public final AbstractC001200n A00;
    public final AnonymousClass5TV A01;
    public final AbstractC116525Vu A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CatalogSearchResultsListAdapter(AbstractC001200n r17, AnonymousClass12P r18, C14900mE r19, C15570nT r20, AnonymousClass19Q r21, AnonymousClass19T r22, C37071lG r23, AnonymousClass5TV r24, AbstractC116525Vu r25, C15550nR r26, C22700zV r27, C15610nY r28, AnonymousClass018 r29, UserJid userJid) {
        super(r18, r19, r20, r21, r22, r23, r26, r27, r28, r29, userJid, null);
        C16700pc.A0J(r19, r20, r18, r22, r26);
        C16700pc.A0E(r28, 7);
        C16700pc.A0E(r29, 8);
        C16700pc.A0E(r27, 9);
        C16700pc.A0E(r21, 10);
        this.A02 = r25;
        this.A01 = r24;
        this.A00 = r17;
        List list = ((AnonymousClass1lL) this).A00;
        list.add(new C84663zg());
        A04(C12980iv.A0C(list));
        r17.ADr().A00(this);
    }

    @Override // X.AbstractC59092tx, X.AnonymousClass1lK
    public AbstractC75723kJ A0F(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        if (i != 5) {
            return super.A0F(viewGroup, i);
        }
        Context context = viewGroup.getContext();
        UserJid userJid = ((AnonymousClass1lK) this).A04;
        C15570nT r2 = ((AnonymousClass1lK) this).A01;
        AnonymousClass018 r9 = ((AbstractC59092tx) this).A05;
        C37071lG r4 = ((AnonymousClass1lK) this).A03;
        AnonymousClass19Q r3 = ((AbstractC59092tx) this).A01;
        AbstractC116525Vu r8 = this.A02;
        AnonymousClass5TV r7 = this.A01;
        View A0F = C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.business_product_catalog_list_product);
        AnonymousClass23N.A01(A0F);
        return new C59302uQ(A0F, r2, r3, r4, this, this, r7, r8, r9, userJid);
    }

    public final void A0M(List list) {
        List list2 = ((AnonymousClass1lL) this).A00;
        if (list2.isEmpty()) {
            A0J(null, list);
            return;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C44691zO r5 = (C44691zO) it.next();
            C16700pc.A0E(r5, 0);
            if (r5.A01()) {
                list2.add(C12980iv.A0C(list2), new C84693zj(r5, 5, A0E(r5.A0D)));
                A04(C12980iv.A0C(list2));
            }
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return A0F(viewGroup, i);
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
        C16700pc.A0E(r3, 1);
        if (r3.ordinal() == 5) {
            this.A00.ADr().A01(this);
            ((AnonymousClass1lK) this).A03.A00();
        }
    }
}
