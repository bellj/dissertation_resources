package com.whatsapp.catalogsearch.view.viewmodel;

import X.AbstractC16710pd;
import X.AnonymousClass015;
import X.AnonymousClass017;
import X.AnonymousClass03B;
import X.AnonymousClass1CR;
import X.AnonymousClass3IN;
import X.AnonymousClass41J;
import X.AnonymousClass41M;
import X.AnonymousClass4Q2;
import X.AnonymousClass4RX;
import X.AnonymousClass4Yq;
import X.AnonymousClass5J1;
import X.C113825Jd;
import X.C12990iw;
import X.C16700pc;
import X.C90094Mo;

/* loaded from: classes2.dex */
public final class CatalogSearchViewModel extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass017 A01;
    public final C90094Mo A02;
    public final AnonymousClass1CR A03;
    public final AnonymousClass4Q2 A04;
    public final AnonymousClass3IN A05;
    public final AbstractC16710pd A06 = AnonymousClass4Yq.A00(new AnonymousClass5J1());
    public final AbstractC16710pd A07 = AnonymousClass4Yq.A00(new C113825Jd(this));

    public CatalogSearchViewModel(C90094Mo r2, AnonymousClass1CR r3, AnonymousClass4Q2 r4, AnonymousClass3IN r5) {
        C16700pc.A0E(r3, 3);
        this.A05 = r5;
        this.A04 = r4;
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r5.A00;
        this.A00 = r4.A00;
    }

    public final void A04(AnonymousClass4RX r2) {
        C12990iw.A0P(this.A06).A0B(r2);
    }

    public final void A05(String str) {
        C16700pc.A0E(str, 0);
        if (str.length() == 0) {
            A04(new AnonymousClass41J(this.A02.A01.A07(1514)));
            return;
        }
        AnonymousClass4Q2 r2 = this.A04;
        r2.A01.A0B(AnonymousClass03B.A04(str).toString());
        A04(AnonymousClass41M.A00);
    }
}
