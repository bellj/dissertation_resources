package com.whatsapp;

import X.C17050qB;
import X.C22200yh;
import X.C39361pl;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* loaded from: classes2.dex */
public class GifHelper {
    public static native Mp4Ops.LibMp4OperationResult applyGifTag(String str, String str2);

    public static native boolean hasGifTag(String str);

    public static void A00(C17050qB r5, File file) {
        try {
            File A01 = r5.A01(file);
            Mp4Ops.LibMp4OperationResult applyGifTag = applyGifTag(file.getAbsolutePath(), A01.getAbsolutePath());
            if (applyGifTag == null) {
                Log.e("gif-helper/applyGifTag is null");
                throw new C39361pl(0, "result is null");
            } else if (!applyGifTag.success) {
                StringBuilder sb = new StringBuilder();
                sb.append("gif-helper/applyGifTag");
                sb.append(applyGifTag.errorMessage);
                Log.e(sb.toString());
                int i = applyGifTag.errorCode;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("invalid result, error_code: ");
                sb2.append(i);
                throw new C39361pl(i, sb2.toString());
            } else if (!C22200yh.A0S(r5, A01, file)) {
                Log.e("gif-helper/applyGifTag failed to apply tag properly.  Renaming marked file to original filepath unsuccessful");
                throw new C39361pl(0, "applyGifTag failed to apply tag properly.  Renaming marked file to original filepath unsuccessful");
            }
        } catch (IOException e) {
            Log.e("Could not access file or failed to move files properly", e);
            throw new C39361pl(0, "Could not access file or failed to move files properly");
        }
    }

    public static boolean A01(File file) {
        return file != null && hasGifTag(file.getAbsolutePath());
    }
}
