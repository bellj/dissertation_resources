package com.whatsapp.report;

import X.AbstractC14440lR;
import X.AnonymousClass014;
import X.AnonymousClass016;
import X.C14820m6;
import X.C14900mE;
import X.C26871Fd;
import X.C26891Ff;
import X.C44101yE;
import X.C44111yF;
import X.C44241ya;
import X.C44251yb;
import X.C48332Fp;
import X.C48352Fr;
import X.C49292Ke;
import android.app.Application;

/* loaded from: classes2.dex */
public class BusinessActivityReportViewModel extends AnonymousClass014 {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final AnonymousClass016 A01 = new AnonymousClass016(0);
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final C14900mE A03;
    public final C14820m6 A04;
    public final C26891Ff A05;
    public final C26871Fd A06;
    public final C44251yb A07;
    public final C44111yF A08;
    public final C48352Fr A09;
    public final C49292Ke A0A;
    public final C48332Fp A0B;
    public final C44101yE A0C;
    public final C44241ya A0D;
    public final AbstractC14440lR A0E;

    public BusinessActivityReportViewModel(Application application, C14900mE r6, C14820m6 r7, C26891Ff r8, C26871Fd r9, C48332Fp r10, C44101yE r11, C44241ya r12, AbstractC14440lR r13) {
        super(application);
        C44251yb r3 = new C44251yb(this);
        this.A07 = r3;
        C44111yF r2 = new C44111yF(this);
        this.A08 = r2;
        C48352Fr r1 = new C48352Fr(this);
        this.A09 = r1;
        C49292Ke r0 = new C49292Ke(this);
        this.A0A = r0;
        this.A03 = r6;
        this.A0E = r13;
        this.A04 = r7;
        this.A05 = r8;
        this.A0C = r11;
        this.A06 = r9;
        this.A0B = r10;
        this.A0D = r12;
        r12.A00 = r3;
        r10.A00 = r1;
        r11.A00 = r2;
        r9.A00 = r0;
    }

    public static /* synthetic */ void A00(BusinessActivityReportViewModel businessActivityReportViewModel) {
        businessActivityReportViewModel.A02.A0A(0);
        businessActivityReportViewModel.A05.A02();
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0C.A00 = null;
        this.A0B.A00 = null;
        this.A0D.A00 = null;
        this.A06.A00 = null;
    }
}
