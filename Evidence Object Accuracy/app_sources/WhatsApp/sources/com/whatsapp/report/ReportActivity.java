package com.whatsapp.report;

import X.AbstractActivityC37351mE;
import X.AbstractC116115Ue;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC37361mF;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass12H;
import X.AnonymousClass18U;
import X.AnonymousClass1MY;
import X.AnonymousClass1m5;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2GQ;
import X.AnonymousClass2eq;
import X.AnonymousClass374;
import X.C16150oX;
import X.C16440p1;
import X.C18360sK;
import X.C20660w7;
import X.C252018m;
import X.C26531Dv;
import X.C41691tw;
import X.C44891zj;
import X.C48322Fo;
import X.C52162aM;
import X.C58272oQ;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.report.ReportActivity;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.Date;

/* loaded from: classes2.dex */
public class ReportActivity extends AbstractActivityC37351mE implements AbstractC37361mF {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public ImageView A06;
    public ImageView A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public TextView A0B;
    public TextView A0C;
    public TextView A0D;
    public AnonymousClass18U A0E;
    public TextEmojiLabel A0F;
    public TextEmojiLabel A0G;
    public C18360sK A0H;
    public AnonymousClass018 A0I;
    public AnonymousClass12H A0J;
    public C20660w7 A0K;
    public BusinessActivityReportViewModel A0L;
    public C26531Dv A0M;
    public C48322Fo A0N;
    public AnonymousClass374 A0O;
    public AnonymousClass2GQ A0P;
    public C252018m A0Q;
    public final AbstractC18860tB A0R = new AnonymousClass1m5(this);

    public final String A2e(long j) {
        boolean equals = "sl".equals(this.A0I.A06());
        AnonymousClass018 r1 = this.A0I;
        if (equals) {
            return AnonymousClass1MY.A07(r1, 1).format(new Date(j));
        }
        return AnonymousClass1MY.A03(r1, j);
    }

    public final void A2f(TextEmojiLabel textEmojiLabel) {
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, ((ActivityC13810kN) this).A08));
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(getString(R.string.gdpr_report_header, this.A0Q.A03("26000110").toString())));
        URLSpan[] uRLSpanArr = (URLSpan[]) spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                spannableStringBuilder.setSpan(new C58272oQ(this, this.A0E, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A08, uRLSpan.getURL()), spanStart, spanEnd, spanFlags);
                spannableStringBuilder.setSpan(new TextAppearanceSpan(this, R.style.SettingsInlineLink), spanStart, spanEnd, spanFlags);
            }
            for (URLSpan uRLSpan2 : uRLSpanArr) {
                spannableStringBuilder.removeSpan(uRLSpan2);
            }
        }
        textEmojiLabel.setText(spannableStringBuilder);
    }

    @Override // X.AbstractC37361mF
    public void AZz() {
        TextView textView;
        String str;
        String A03;
        boolean z;
        long j;
        String A032;
        int A01 = this.A0M.A01();
        if (A01 != 0) {
            if (A01 != 1) {
                if (A01 == 2) {
                    C16440p1 A033 = this.A0M.A03();
                    if (A033 != null) {
                        C16150oX r0 = ((AbstractC16130oV) A033).A02;
                        AnonymousClass009.A05(r0);
                        z = r0.A0a;
                    } else {
                        z = false;
                    }
                    View view = this.A03;
                    if (z) {
                        view.setEnabled(false);
                        this.A03.setOnClickListener(null);
                        this.A07.setImageResource(R.drawable.ic_action_schedule);
                        AnonymousClass2GE.A07(this.A07, AnonymousClass00T.A00(this, R.color.gdpr_grey));
                        this.A0D.setText(R.string.gdpr_report_downloading);
                        this.A0D.setTextColor(AnonymousClass00T.A00(this, R.color.gdpr_grey));
                        this.A0C.setTextColor(AnonymousClass00T.A00(this, R.color.gdpr_grey));
                    } else {
                        view.setEnabled(true);
                        this.A03.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 17));
                        this.A07.setImageResource(R.drawable.ic_action_download);
                        AnonymousClass2GE.A07(this.A07, C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon));
                        this.A0D.setText(R.string.gdpr_report_download);
                        this.A0D.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_title_text));
                        this.A0C.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_subtitle_text));
                    }
                    this.A0C.setVisibility(0);
                    if (A033 != null) {
                        this.A0C.setText(getString(R.string.gdpr_report_info, AnonymousClass1MY.A03(this.A0I, this.A0M.A02()), C44891zj.A03(this.A0I, ((AbstractC16130oV) A033).A01)));
                    } else {
                        this.A0C.setText(AnonymousClass1MY.A03(this.A0I, this.A0M.A02()));
                    }
                    this.A04.setVisibility(8);
                    this.A05.setVisibility(8);
                } else if (A01 == 3) {
                    this.A03.setEnabled(true);
                    this.A03.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 18));
                    this.A07.setImageResource(R.drawable.ic_action_share);
                    AnonymousClass2GE.A07(this.A07, C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon));
                    this.A0D.setText(R.string.gdpr_report_share);
                    this.A0D.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_title_text));
                    this.A0C.setVisibility(0);
                    this.A0C.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_subtitle_text));
                    C16440p1 A034 = this.A0M.A03();
                    TextView textView2 = this.A0C;
                    if (A034 != null) {
                        A032 = getString(R.string.gdpr_report_info, AnonymousClass1MY.A03(this.A0I, this.A0M.A02()), C44891zj.A03(this.A0I, ((AbstractC16130oV) A034).A01));
                    } else {
                        A032 = AnonymousClass1MY.A03(this.A0I, this.A0M.A02());
                    }
                    textView2.setText(A032);
                    this.A04.setVisibility(0);
                    this.A05.setVisibility(0);
                } else {
                    return;
                }
                this.A0B.setVisibility(0);
                textView = this.A0B;
                Object[] objArr = new Object[1];
                C26531Dv r6 = this.A0M;
                synchronized (r6) {
                    j = r6.A0C.A00.getLong("gdpr_report_expiration_timestamp", 0);
                }
                objArr[0] = A2e(j);
                str = getString(R.string.gdpr_report_footer_available, objArr);
            } else {
                this.A03.setEnabled(false);
                this.A03.setOnClickListener(null);
                this.A07.setImageResource(R.drawable.ic_action_schedule);
                AnonymousClass2GE.A07(this.A07, AnonymousClass00T.A00(this, R.color.gdpr_grey));
                this.A0D.setText(R.string.gdpr_report_requested);
                this.A0D.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_title_text));
                this.A0C.setVisibility(0);
                long A02 = this.A0M.A02();
                boolean equals = "sl".equals(this.A0I.A06());
                AnonymousClass018 r2 = this.A0I;
                if (equals) {
                    A03 = AnonymousClass1MY.A07(r2, 2).format(new Date(A02));
                } else {
                    A03 = AnonymousClass1MY.A03(r2, A02);
                }
                this.A0C.setText(getString(R.string.gdpr_report_will_be_ready, A03));
                this.A0C.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_subtitle_text));
                this.A04.setVisibility(8);
                this.A05.setVisibility(8);
                this.A0B.setVisibility(0);
                int max = (int) Math.max(1L, (this.A0M.A02() - ((ActivityC13790kL) this).A05.A00()) / 86400000);
                textView = this.A0B;
                str = getResources().getQuantityString(R.plurals.gdpr_report_footer, max, Integer.valueOf(max));
            }
            textView.setText(str);
            this.A0G.setVisibility(8);
            return;
        }
        this.A03.setEnabled(true);
        this.A03.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 16));
        this.A07.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_settings_terms_policy), this.A0I));
        AnonymousClass2GE.A07(this.A07, C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon));
        boolean A07 = ((ActivityC13810kN) this).A0C.A07(455);
        TextView textView3 = this.A0D;
        int i = R.string.gdpr_report_request;
        if (A07) {
            i = R.string.gdpr_report_request_v2;
        }
        textView3.setText(i);
        this.A0D.setTextColor(AnonymousClass00T.A00(this, R.color.settings_item_title_text));
        this.A0C.setVisibility(8);
        this.A04.setVisibility(8);
        this.A05.setVisibility(8);
        this.A0B.setVisibility(8);
        this.A0B.setText(getResources().getQuantityString(R.plurals.gdpr_report_footer, 3, 3));
        if (((ActivityC13810kN) this).A0C.A07(455)) {
            this.A0G.setVisibility(0);
        }
    }

    public final void initGdprViews(View view) {
        this.A0D = (TextView) AnonymousClass028.A0D(view, R.id.report_button_title);
        this.A0C = (TextView) AnonymousClass028.A0D(view, R.id.report_button_subtitle);
        this.A07 = (ImageView) AnonymousClass028.A0D(view, R.id.report_button_icon);
        this.A03 = AnonymousClass028.A0D(view, R.id.report_button);
        this.A04 = AnonymousClass028.A0D(view, R.id.report_delete);
        this.A0G = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.report_item_header);
        this.A0B = (TextView) AnonymousClass028.A0D(view, R.id.report_item_footer);
        AnonymousClass2GE.A07(this.A07, C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon));
        this.A04.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 40));
        A2f(this.A0G);
    }

    public final void initP2BViews(View view) {
        if (!((ActivityC13810kN) this).A0C.A07(455)) {
            findViewById(R.id.request_p2b_report_container).setVisibility(8);
            findViewById(R.id.reports_divider).setVisibility(8);
            return;
        }
        this.A0A = (TextView) AnonymousClass028.A0D(view, R.id.report_button_title);
        this.A09 = (TextView) AnonymousClass028.A0D(view, R.id.report_button_subtitle);
        this.A06 = (ImageView) AnonymousClass028.A0D(view, R.id.report_button_icon);
        this.A00 = AnonymousClass028.A0D(view, R.id.report_button);
        this.A01 = AnonymousClass028.A0D(view, R.id.report_delete);
        this.A0F = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.report_item_header);
        this.A08 = (TextView) AnonymousClass028.A0D(view, R.id.report_item_footer);
        TextView textView = (TextView) AnonymousClass028.A0D(view, R.id.report_item_header);
        ((TextView) AnonymousClass028.A0D(view, R.id.report_button_title)).setText(getString(R.string.p2b_report_request));
        textView.setVisibility(0);
        textView.setText(getString(R.string.p2b_report_description));
        this.A02 = AnonymousClass028.A0D(view, R.id.report_delete_divider);
        AnonymousClass028.A0D(view, R.id.report_delete_divider).setVisibility(8);
        DeleteReportConfirmationDialogFragment deleteReportConfirmationDialogFragment = new DeleteReportConfirmationDialogFragment();
        deleteReportConfirmationDialogFragment.A01 = new AbstractC116115Ue() { // from class: X.3Zo
            @Override // X.AbstractC116115Ue
            public final void A8m() {
                boolean z;
                BusinessActivityReportViewModel businessActivityReportViewModel = ReportActivity.this.A0L;
                if (businessActivityReportViewModel.A03.A0M()) {
                    C12960it.A1A(businessActivityReportViewModel.A01, 1);
                    C48332Fp r8 = businessActivityReportViewModel.A0B;
                    if (r8.A04.A0B()) {
                        C17220qS r7 = r8.A05;
                        String A01 = r7.A01();
                        AnonymousClass1W9[] r2 = new AnonymousClass1W9[1];
                        boolean A1a = C12990iw.A1a("action", "delete", r2);
                        AnonymousClass1V8 r4 = new AnonymousClass1V8("p2b", r2);
                        AnonymousClass1W9[] r3 = new AnonymousClass1W9[6];
                        r3[A1a ? 1 : 0] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
                        C15570nT r0 = r8.A03;
                        r0.A08();
                        C27631Ih r02 = r0.A05;
                        AnonymousClass009.A05(r02);
                        C12960it.A1M("from", r02.getRawString(), r3, 1);
                        r3[2] = new AnonymousClass1W9("xmlns", "w:biz:p2b_report");
                        r3[3] = new AnonymousClass1W9("type", "set");
                        r3[4] = new AnonymousClass1W9("smax_id", "31");
                        r3[5] = new AnonymousClass1W9("id", A01);
                        r7.A0A(r8, new AnonymousClass1V8(r4, "iq", r3), A01, 266, 32000);
                        z = true;
                    } else {
                        z = false;
                    }
                    StringBuilder A0k = C12960it.A0k("app/sendDeleteReport success:");
                    A0k.append(z);
                    C12960it.A1F(A0k);
                }
            }
        };
        this.A01.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 47, deleteReportConfirmationDialogFragment));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0201, code lost:
        com.whatsapp.util.Log.e("BusinessActivityReportManager/validate-state/report-message-missing");
        r2.A03.A0K();
     */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c9 A[Catch: all -> 0x0226, TryCatch #0 {, blocks: (B:38:0x018b, B:44:0x0197, B:46:0x01a3, B:52:0x01b5, B:54:0x01bb, B:56:0x01c9, B:58:0x01d3, B:60:0x01db, B:61:0x01fb, B:63:0x0201, B:64:0x020c), top: B:80:0x018b }] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
        // Method dump skipped, instructions count: 754
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.report.ReportActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass374 r0 = this.A0O;
        if (r0 != null) {
            r0.A03(true);
        }
        AnonymousClass2GQ r02 = this.A0P;
        if (r02 != null) {
            r02.A03(true);
        }
        C48322Fo r03 = this.A0N;
        if (r03 != null) {
            r03.A03(true);
        }
        this.A0J.A04(this.A0R);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0H.A04(16, null);
        this.A0H.A04(32, null);
    }
}
