package com.whatsapp.report;

import X.AbstractC116115Ue;
import X.AnonymousClass018;
import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DeleteReportConfirmationDialogFragment extends Hilt_DeleteReportConfirmationDialogFragment {
    public AnonymousClass018 A00;
    public AbstractC116115Ue A01;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        A0O.A0A(Html.fromHtml(this.A00.A09(R.string.gdpr_delete_report_confirmation)));
        A0O.setNegativeButton(R.string.cancel, null);
        C12970iu.A1L(A0O, this, 61, R.string.delete);
        return A0O.create();
    }
}
