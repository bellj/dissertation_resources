package com.whatsapp.report;

import X.AbstractC116125Uf;
import X.AnonymousClass018;
import X.C004802e;
import X.C12970iu;
import X.C72463ee;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class ShareReportConfirmationDialogFragment extends Hilt_ShareReportConfirmationDialogFragment {
    public AnonymousClass018 A00;
    public AbstractC116125Uf A01;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        A0O.A06(R.string.gdpr_share_report_confirmation);
        C72463ee.A0S(A0O);
        C12970iu.A1L(A0O, this, 62, R.string.gdpr_share_report_button);
        return A0O.create();
    }
}
