package com.whatsapp.report;

import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.R;
import com.whatsapp.base.WaDialogFragment;

/* loaded from: classes2.dex */
public class DownloadReportFailedDialogFragment extends WaDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.download_failed);
        A0O.A06(R.string.gdpr_download_expired);
        A0O.setPositiveButton(R.string.ok, new IDxCListenerShape4S0000000_2_I1(16));
        return A0O.create();
    }
}
