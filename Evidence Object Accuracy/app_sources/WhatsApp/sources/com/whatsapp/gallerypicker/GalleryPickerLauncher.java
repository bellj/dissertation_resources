package com.whatsapp.gallerypicker;

import X.AbstractC009404s;
import X.ActivityC000900k;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass10Z;
import X.AnonymousClass2FH;
import X.AnonymousClass3OZ;
import X.C12970iu;
import X.C15890o4;
import X.C48572Gu;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* loaded from: classes2.dex */
public class GalleryPickerLauncher extends ActivityC000900k implements AnonymousClass004 {
    public C15890o4 A00;
    public AnonymousClass018 A01;
    public AnonymousClass10Z A02;
    public boolean A03;
    public final Object A04;
    public volatile AnonymousClass2FH A05;

    public GalleryPickerLauncher() {
        this(0);
    }

    public GalleryPickerLauncher(int i) {
        this.A04 = C12970iu.A0l();
        this.A03 = false;
        A0R(new AnonymousClass3OZ(this));
    }

    public final void A1U() {
        if (this.A00.A07()) {
            Intent intent = getIntent();
            int intExtra = intent.getIntExtra("max_items", 1);
            boolean booleanExtra = intent.getBooleanExtra("is_in_multi_select_mode_only", false);
            Uri fromFile = Uri.fromFile(this.A02.A01.A0M("tmpi"));
            Intent A0A = C12970iu.A0A();
            A0A.setClassName(getPackageName(), "com.whatsapp.gallerypicker.GalleryPicker");
            A0A.putExtra("include_media", 1);
            A0A.putExtra("max_items", intExtra);
            A0A.putExtra("is_in_multi_select_mode_only", booleanExtra);
            A0A.putExtra("preview", false);
            A0A.putExtra("output", fromFile);
            startActivityForResult(A0A, 1);
            return;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access;
        }
        RequestPermissionActivity.A0K(this, R.string.permission_storage_need_write_access_request, i2);
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A05 == null) {
            synchronized (this.A04) {
                if (this.A05 == null) {
                    this.A05 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A05.generatedComponent();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            if (i != 151) {
                super.onActivityResult(i, i2, intent);
                return;
            } else if (i2 == -1) {
                A1U();
                return;
            }
        } else if (getIntent() == null || !getIntent().getBooleanExtra("should_return_photo_source", false)) {
            setResult(i2, intent);
            if (intent != null) {
                intent.putExtra("chat_jid", getIntent().getStringExtra("chat_jid"));
                intent.putExtra("is_using_global_wallpaper", getIntent().getBooleanExtra("is_using_global_wallpaper", false));
            }
        } else {
            if (intent == null) {
                intent = C12970iu.A0A();
            }
            intent.putExtra("photo_source", 2);
            setResult(i2, intent);
        }
        finish();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.gallery_picker_label);
        if (bundle == null) {
            A1U();
        }
    }
}
