package com.whatsapp.gallerypicker;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass11P;
import X.AnonymousClass12P;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1A1;
import X.AnonymousClass1J1;
import X.AnonymousClass24W;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.C103084qB;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18720su;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C245115u;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C40691s6;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.GalleryPicker;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class GalleryPicker extends ActivityC13790kL {
    public int A00;
    public long A01;
    public View A02;
    public AnonymousClass01E A03;
    public C18720su A04;
    public C15550nR A05;
    public C15610nY A06;
    public AnonymousClass1J1 A07;
    public C21270x9 A08;
    public AnonymousClass19D A09;
    public AnonymousClass11P A0A;
    public C15890o4 A0B;
    public C245115u A0C;
    public AnonymousClass01H A0D;
    public AnonymousClass01H A0E;
    public boolean A0F;

    public GalleryPicker() {
        this(0);
        this.A00 = 7;
    }

    public GalleryPicker(int i) {
        this.A0F = false;
        A0R(new C103084qB(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A04 = (C18720su) r2.A2c.get();
            this.A0C = (C245115u) r2.A7s.get();
            this.A08 = (C21270x9) r2.A4A.get();
            this.A05 = (C15550nR) r2.A45.get();
            this.A06 = (C15610nY) r2.AMe.get();
            this.A0B = (C15890o4) r2.AN1.get();
            this.A09 = (AnonymousClass19D) r2.ABo.get();
            this.A0A = (AnonymousClass11P) r2.ABp.get();
            this.A0D = C18000rk.A00(r2.ADw);
            this.A0E = C18000rk.A00(r2.AID);
            this.A03 = (AnonymousClass01E) r1.A15.get();
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 90) {
            if (i != 91) {
                super.onActivityResult(i, i2, intent);
                return;
            } else if (i2 != -1) {
                return;
            } else {
                if (getIntent().getBooleanExtra("preview", true)) {
                    ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                    if (parcelableArrayListExtra == null) {
                        Uri data = intent.getData();
                        if (data != null) {
                            parcelableArrayListExtra = new ArrayList();
                            parcelableArrayListExtra.add(data);
                        } else {
                            return;
                        }
                    }
                    AnonymousClass24W r6 = new AnonymousClass24W(this);
                    r6.A0C = parcelableArrayListExtra;
                    r6.A08 = getIntent().getStringExtra("jid");
                    r6.A01 = 1;
                    r6.A02 = System.currentTimeMillis() - this.A01;
                    r6.A03 = getIntent().getLongExtra("picker_open_time", 0);
                    r6.A0G = true;
                    r6.A04 = getIntent().getLongExtra("quoted_message_row_id", 0);
                    r6.A09 = getIntent().getStringExtra("quoted_group_jid");
                    r6.A0D = getIntent().getBooleanExtra("number_from_url", false);
                    startActivityForResult(r6.A00(), 90);
                    return;
                }
            }
        } else if (i2 != -1) {
            if (i2 == 2) {
                setResult(2);
                finish();
            }
            return;
        }
        setResult(-1, intent);
        finish();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            this.A0D.get();
        }
        super.onBackPressed();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01a5, code lost:
        if (r1 == 2) goto L_0x01a7;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r13) {
        /*
        // Method dump skipped, instructions count: 451
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.gallerypicker.GalleryPicker.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        Intent intent;
        String str;
        int i = this.A00;
        if (i == 4) {
            intent = new Intent("android.intent.action.PICK", MediaStore.Video.Media.INTERNAL_CONTENT_URI);
            str = "video/*";
        } else {
            Uri uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            if (i == 2) {
                intent = new Intent("android.intent.action.PICK", uri);
                str = "image/gif";
            } else {
                intent = new Intent("android.intent.action.PICK", uri);
                str = "image/*";
            }
        }
        intent.setType(str);
        PackageManager packageManager = getPackageManager();
        int i2 = 0;
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        int size = queryIntentActivities.size();
        if (size <= 0) {
            return true;
        }
        getMenuInflater().inflate(R.menu.gallery_picker, menu);
        SubMenu subMenu = menu.findItem(R.id.more).getSubMenu();
        subMenu.clear();
        subMenu.setIcon(AnonymousClass2GE.A01(this, R.drawable.ic_more_teal, R.color.lightActionBarItemDrawableTint));
        menu.findItem(R.id.default_item).setVisible(false);
        Drawable A04 = AnonymousClass00T.A04(this, R.mipmap.icon);
        AnonymousClass009.A05(A04);
        ArrayList arrayList = new ArrayList(size);
        int i3 = Integer.MIN_VALUE;
        int intrinsicHeight = A04.getIntrinsicHeight();
        int i4 = 0;
        do {
            Drawable loadIcon = queryIntentActivities.get(i4).loadIcon(packageManager);
            i3 = Math.max(loadIcon.getIntrinsicHeight(), i3);
            arrayList.add(loadIcon);
            i4++;
        } while (i4 < size);
        int min = Math.min(intrinsicHeight, i3);
        do {
            ResolveInfo resolveInfo = queryIntentActivities.get(i2);
            Drawable drawable = (Drawable) arrayList.get(i2);
            Resources resources = getResources();
            if (drawable instanceof BitmapDrawable) {
                drawable = new BitmapDrawable(resources, Bitmap.createScaledBitmap(((BitmapDrawable) drawable).getBitmap(), min, min, false));
            }
            MenuItem add = subMenu.add(resolveInfo.loadLabel(packageManager));
            add.setIcon(drawable);
            add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener(intent, resolveInfo, this) { // from class: X.3MT
                public final /* synthetic */ Intent A00;
                public final /* synthetic */ ResolveInfo A01;
                public final /* synthetic */ GalleryPicker A02;

                {
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    GalleryPicker galleryPicker = this.A02;
                    Intent intent2 = this.A00;
                    ActivityInfo activityInfo = this.A01.activityInfo;
                    intent2.setComponent(new ComponentName(activityInfo.packageName, activityInfo.name));
                    galleryPicker.startActivityForResult(intent2, 91);
                    return false;
                }
            });
            i2++;
        } while (i2 < size);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A04.A02().A00.A06(-1);
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A02(this.A02, this.A0A);
            AnonymousClass1J1 r0 = this.A07;
            if (r0 != null) {
                r0.A00();
                this.A07 = null;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A07(this.A0A);
            ((AnonymousClass1A1) this.A0D.get()).A02(((ActivityC13810kN) this).A00);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            boolean z = ((AnonymousClass1A1) this.A0D.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r15 = ((ActivityC13810kN) this).A0C;
                C14900mE r13 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
                C21270x9 r10 = this.A08;
                C15550nR r9 = this.A05;
                C15610nY r8 = this.A06;
                AnonymousClass018 r7 = ((ActivityC13830kP) this).A01;
                Pair A00 = C40691s6.A00(this, view, this.A02, r13, r12, r9, r8, this.A07, r10, this.A09, this.A0A, ((ActivityC13810kN) this).A09, r7, r15, r11, this.A0D, this.A0E, "gallery-picker-activity");
                this.A02 = (View) A00.first;
                this.A07 = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A0A, this.A0D);
            }
            ((AnonymousClass1A1) this.A0D.get()).A01();
        }
    }
}
