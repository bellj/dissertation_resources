package com.whatsapp.gallerypicker;

import X.AbstractActivityC58482pY;
import X.AbstractC009504t;
import X.AbstractC454421p;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01V;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C41691tw;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class MediaPicker extends AbstractActivityC58482pY {
    public AnonymousClass01E A00;

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r3) {
        super.AXD(r3);
        C41691tw.A07(getWindow(), false);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        AnonymousClass01E A07 = A0V().A07(R.id.content);
        if (A07 != null) {
            A07.A0t(i, i2, intent);
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        A1b(5);
        if (AbstractC454421p.A00) {
            Window window = getWindow();
            window.addFlags(Integer.MIN_VALUE);
            window.clearFlags(67108864);
            window.requestFeature(13);
            window.requestFeature(12);
            Transition inflateTransition = TransitionInflater.from(this).inflateTransition(17760259);
            inflateTransition.excludeTarget(16908335, true);
            inflateTransition.excludeTarget(16908336, true);
            window.setEnterTransition(inflateTransition);
            Transition inflateTransition2 = TransitionInflater.from(this).inflateTransition(17760258);
            inflateTransition2.excludeTarget(16908335, true);
            inflateTransition2.excludeTarget(16908336, true);
            window.setReturnTransition(inflateTransition2);
            A0c();
        }
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        super.onCreate(bundle);
        setTitle(R.string.gallery_label);
        A1U().A0M(true);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(R.id.content);
        if (bundle == null) {
            C004902f A0P = C12970iu.A0P(this);
            A0P.A06(this.A00, frameLayout.getId());
            A0P.A01();
            View view = new View(this);
            C12970iu.A18(this, view, R.color.divider_gray);
            view.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) Math.ceil((double) (C12960it.A01(this) / 2.0f))));
            frameLayout.addView(view);
        }
        setContentView(frameLayout);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        AnonymousClass00T.A08(this);
        return true;
    }
}
