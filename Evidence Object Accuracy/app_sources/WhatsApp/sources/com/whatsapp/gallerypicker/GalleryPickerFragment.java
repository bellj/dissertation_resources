package com.whatsapp.gallerypicker;

import X.AbstractC14440lR;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass2FO;
import X.AnonymousClass2ZJ;
import X.AnonymousClass383;
import X.AnonymousClass3G9;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C15450nH;
import X.C15690nk;
import X.C15890o4;
import X.C16590pI;
import X.C18720su;
import X.C19390u2;
import X.C457522x;
import X.C51792Yv;
import X.C54332gY;
import X.C91364Rl;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.SquareImageView;

/* loaded from: classes2.dex */
public class GalleryPickerFragment extends Hilt_GalleryPickerFragment {
    public static boolean A0O;
    public static final String A0P;
    public static final C91364Rl[] A0Q;
    public static final C91364Rl[] A0R;
    public int A00 = 1;
    public int A01;
    public int A02;
    public BroadcastReceiver A03;
    public ContentObserver A04;
    public Drawable A05;
    public View A06;
    public RecyclerView A07;
    public C15450nH A08;
    public C18720su A09;
    public C16590pI A0A;
    public C15890o4 A0B;
    public AnonymousClass018 A0C;
    public C14850m9 A0D;
    public AnonymousClass383 A0E;
    public C54332gY A0F;
    public AnonymousClass2FO A0G;
    public C457522x A0H;
    public C19390u2 A0I;
    public C15690nk A0J;
    public AbstractC14440lR A0K;
    public boolean A0L;
    public boolean A0M;
    public final Handler A0N = C12970iu.A0E();

    static {
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(Environment.getExternalStorageDirectory(), A0h);
        String valueOf = String.valueOf(C12960it.A0d("/DCIM/Camera", A0h).toLowerCase().hashCode());
        A0P = valueOf;
        A0Q = new C91364Rl[]{new C91364Rl(valueOf, 4, 1, R.string.gallery_camera_images_bucket_name), new C91364Rl(valueOf, 5, 4, R.string.gallery_camera_videos_bucket_name), new C91364Rl(valueOf, 6, 2, R.string.gallery_camera_images_bucket_name), new C91364Rl(null, 0, 1, R.string.all_images), new C91364Rl(null, 1, 4, R.string.all_videos), new C91364Rl(null, 2, 2, R.string.all_gifs)};
        A0R = new C91364Rl[]{new C91364Rl(valueOf, 7, 7, R.string.gallery_camera_bucket_name), new C91364Rl(null, 3, 7, R.string.all_media), new C91364Rl(null, 1, 4, R.string.all_videos)};
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.gallery_picker_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        ContentResolver contentResolver;
        super.A12();
        AnonymousClass383 r1 = this.A0E;
        if (r1 != null) {
            r1.A03(true);
            this.A0E = null;
        }
        C457522x r0 = this.A0H;
        if (r0 != null) {
            r0.A00();
            this.A0H = null;
        }
        BroadcastReceiver broadcastReceiver = this.A03;
        if (broadcastReceiver != null) {
            this.A0A.A00.unregisterReceiver(broadcastReceiver);
        }
        ActivityC000900k A0B = A0B();
        if (A0B == null) {
            contentResolver = null;
        } else {
            contentResolver = A0B.getContentResolver();
        }
        AnonymousClass009.A05(contentResolver);
        contentResolver.unregisterContentObserver(this.A04);
        for (int i = 0; i < this.A07.getChildCount(); i++) {
            View childAt = this.A07.getChildAt(i);
            if (childAt instanceof FrameLayout) {
                ViewGroup viewGroup = (ViewGroup) childAt;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    View childAt2 = viewGroup.getChildAt(i2);
                    if (childAt2 instanceof SquareImageView) {
                        ((ImageView) childAt2).setImageDrawable(null);
                    }
                }
            }
        }
        this.A0F = null;
        this.A07.setAdapter(null);
        this.A09.A02().A00.A06(-1);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        ContentResolver contentResolver;
        ContentResolver contentResolver2;
        A0O = this.A0D.A07(1857);
        this.A00 = super.A05.getInt("include");
        int A00 = AnonymousClass00T.A00(this.A0A.A00, R.color.gallery_cell);
        this.A01 = A00;
        this.A05 = new ColorDrawable(A00);
        this.A02 = A02().getDimensionPixelSize(R.dimen.gallery_picker_folder_thumb_size);
        RecyclerView recyclerView = (RecyclerView) A05().findViewById(R.id.albums);
        this.A07 = recyclerView;
        if (A0O) {
            recyclerView.setClipToPadding(false);
            this.A07.setPadding(0, AnonymousClass3G9.A01(view.getContext(), 2.0f), 0, 0);
        }
        this.A03 = new C51792Yv(this);
        Handler handler = this.A0N;
        this.A04 = new AnonymousClass2ZJ(handler, this);
        C54332gY r1 = new C54332gY(this);
        this.A0F = r1;
        this.A07.setAdapter(r1);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addDataScheme("file");
        this.A0A.A00.registerReceiver(this.A03, intentFilter);
        ActivityC000900k A0B = A0B();
        if (A0B == null) {
            contentResolver = null;
        } else {
            contentResolver = A0B.getContentResolver();
        }
        AnonymousClass009.A05(contentResolver);
        contentResolver.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, this.A04);
        C18720su r3 = this.A09;
        ActivityC000900k A0B2 = A0B();
        if (A0B2 == null) {
            contentResolver2 = null;
        } else {
            contentResolver2 = A0B2.getContentResolver();
        }
        this.A0H = new C457522x(contentResolver2, handler, r3, "gallery-picker-fragment");
        this.A0M = false;
        this.A0L = false;
        A1B();
    }

    public final void A1A() {
        if (this.A06 == null) {
            ViewGroup A0P2 = C12980iv.A0P(A05(), R.id.root);
            A0B().getLayoutInflater().inflate(R.layout.gallery_picker_no_images, A0P2);
            View findViewById = A0P2.findViewById(R.id.no_media);
            this.A06 = findViewById;
            TextView A0J = C12960it.A0J(findViewById, R.id.no_media_text);
            int i = this.A00;
            int i2 = R.string.image_gallery_NoImageView_text;
            if (i != 1) {
                i2 = R.string.image_gallery_NoGifView_text;
                if (i != 2) {
                    if (i == 4) {
                        i2 = R.string.image_gallery_NoVideoView_text;
                    }
                }
            }
            A0J.setText(i2);
        }
        this.A06.setVisibility(0);
    }

    public final void A1B() {
        AnonymousClass009.A0A("galleryFoldersTask must be cancelled", C12980iv.A1X(this.A0E));
        if (!this.A0B.A07()) {
            A1A();
            return;
        }
        Point point = new Point();
        C12970iu.A17(A0B(), point);
        int i = point.y * point.x;
        int i2 = this.A02;
        C16590pI r2 = this.A0A;
        AnonymousClass2FO r5 = this.A0G;
        AnonymousClass383 r1 = new AnonymousClass383(r2, this.A0C, this, r5, this.A0I, this.A00, (i / (i2 * i2)) + 1);
        this.A0E = r1;
        C12990iw.A1N(r1, this.A0K);
    }

    public final void A1C(boolean z, boolean z2) {
        StringBuilder A0k = C12960it.A0k("gallerypicker/");
        A0k.append(this.A00);
        A0k.append("/rebake unmounted:");
        A0k.append(z);
        A0k.append(" scanning:");
        A0k.append(z2);
        A0k.append(" oldunmounted:");
        A0k.append(this.A0M);
        A0k.append(" oldscanning:");
        A0k.append(this.A0L);
        C12960it.A1F(A0k);
        if (z != this.A0M || z2 != this.A0L) {
            this.A0M = z;
            this.A0L = z2;
            AnonymousClass383 r1 = this.A0E;
            if (r1 != null) {
                r1.A03(true);
                this.A0E = null;
            }
            if (this.A0M || !this.A0B.A07()) {
                A1A();
                return;
            }
            C12970iu.A1G(this.A06);
            A1B();
        }
    }
}
