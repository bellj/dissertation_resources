package com.whatsapp.gallerypicker;

import X.AbstractC009504t;
import X.AbstractC14640lm;
import X.AbstractC35611iN;
import X.AbstractC454421p;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass01T;
import X.AnonymousClass028;
import X.AnonymousClass02Q;
import X.AnonymousClass185;
import X.AnonymousClass1O4;
import X.AnonymousClass1Y6;
import X.AnonymousClass24W;
import X.AnonymousClass2G6;
import X.AnonymousClass2GE;
import X.AnonymousClass2T1;
import X.AnonymousClass2T3;
import X.AnonymousClass2TT;
import X.C018108l;
import X.C15380n4;
import X.C15450nH;
import X.C15690nk;
import X.C19390u2;
import X.C39341ph;
import X.C453421e;
import X.C51802Yw;
import X.C75083jH;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.R;
import com.whatsapp.StickyHeadersRecyclerView;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class MediaPickerFragment extends Hilt_MediaPickerFragment {
    public int A00;
    public int A01 = Integer.MAX_VALUE;
    public long A02;
    public BroadcastReceiver A03;
    public AnonymousClass02Q A04;
    public AbstractC009504t A05;
    public C15450nH A06;
    public AbstractC14640lm A07;
    public C19390u2 A08;
    public AnonymousClass185 A09;
    public C15690nk A0A;
    public boolean A0B;
    public boolean A0C = true;
    public final C453421e A0D = new C453421e();
    public final HashSet A0E = new LinkedHashSet();

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        ArrayList parcelableArrayListExtra;
        String str;
        super.A0m(bundle);
        this.A02 = System.currentTimeMillis();
        ActivityC000900k A0B = A0B();
        AnonymousClass009.A05(A0B);
        ActivityC000800j r4 = (ActivityC000800j) A0B;
        Intent intent = r4.getIntent();
        this.A01 = intent.getIntExtra("max_items", Integer.MAX_VALUE);
        this.A0C = intent.getBooleanExtra("preview", true);
        this.A0B = intent.getBooleanExtra("is_in_multi_select_mode_only", false);
        AnonymousClass2G6 r1 = new AnonymousClass2G6(A01(), this);
        this.A04 = r1;
        if (this.A0B) {
            this.A05 = r4.A1W(r1);
        }
        this.A07 = AbstractC14640lm.A01(intent.getStringExtra("jid"));
        this.A00 = 7;
        ActivityC13810kN r7 = (ActivityC13810kN) A0B();
        AnonymousClass009.A05(r7);
        Intent intent2 = r7.getIntent();
        if (intent2 != null) {
            String resolveType = intent2.resolveType(r7);
            if (resolveType != null) {
                if (resolveType.equals("vnd.android.cursor.dir/image") || resolveType.equals("image/*")) {
                    this.A00 = 1;
                    r7.setTitle(A0I(R.string.pick_photos_gallery_title));
                }
                if (resolveType.equals("vnd.android.cursor.dir/video") || resolveType.equals("video/*")) {
                    this.A00 = 4;
                    r7.setTitle(A0I(R.string.pick_videos_gallery_title));
                }
            }
            Bundle extras = intent2.getExtras();
            if (extras != null) {
                str = extras.getString("window_title");
            } else {
                str = null;
            }
            if (!TextUtils.isEmpty(str)) {
                r7.A2O(str);
            }
            if (extras != null) {
                this.A00 = 7 & extras.getInt("include_media", this.A00);
            }
        }
        if (bundle != null) {
            parcelableArrayListExtra = bundle.getParcelableArrayList("android.intent.extra.STREAM");
        } else {
            parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
        }
        if (parcelableArrayListExtra != null && !parcelableArrayListExtra.isEmpty()) {
            HashSet hashSet = this.A0E;
            hashSet.clear();
            hashSet.addAll(parcelableArrayListExtra);
            this.A05 = r4.A1W(this.A04);
            ((MediaGalleryFragmentBase) this).A06.A02();
        }
        A0M();
        A1H(false);
        AnonymousClass185 r2 = this.A09;
        StickyHeadersRecyclerView stickyHeadersRecyclerView = ((MediaGalleryFragmentBase) this).A08;
        r2.A02(stickyHeadersRecyclerView.getContext());
        stickyHeadersRecyclerView.A0n(new C75083jH(r2));
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        if (this.A03 != null) {
            A0C().unregisterReceiver(this.A03);
            this.A03 = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i == 1) {
            ActivityC000900k A0B = A0B();
            AnonymousClass009.A05(A0B);
            ActivityC000800j r2 = (ActivityC000800j) A0B;
            if (i2 == -1) {
                r2.setResult(-1, intent);
            } else if (i2 == 2) {
                r2.setResult(2);
            } else if (i2 == 1) {
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                HashSet hashSet = this.A0E;
                hashSet.clear();
                if (parcelableArrayListExtra != null) {
                    hashSet.addAll(parcelableArrayListExtra);
                }
                AbstractC009504t r0 = this.A05;
                if (r0 == null) {
                    this.A05 = r2.A1W(this.A04);
                } else {
                    r0.A06();
                }
                this.A0D.A01(intent.getExtras());
                ((MediaGalleryFragmentBase) this).A06.A02();
                return;
            } else if (i2 == 0 && !A1J()) {
                this.A0D.A00.clear();
                return;
            } else {
                return;
            }
            r2.finish();
        }
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putParcelableArrayList("android.intent.extra.STREAM", new ArrayList<>(this.A0E));
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        if (this.A01 > 1) {
            MenuItem add = menu.add(0, R.id.menuitem_select_multiple, 0, A0I(R.string.select_multiple));
            Context A01 = A01();
            boolean z = ((MediaGalleryFragmentBase) this).A0Q;
            int i = R.color.lightActionBarItemDrawableTint;
            if (z) {
                i = R.color.gallery_toolbar_icon;
            }
            add.setIcon(AnonymousClass2GE.A01(A01, R.drawable.ic_action_select_multiple_teal, i)).setShowAsAction(2);
        }
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_select_multiple) {
            return false;
        }
        ActivityC000900k A0B = A0B();
        AnonymousClass009.A05(A0B);
        this.A05 = ((ActivityC000800j) A0B).A1W(this.A04);
        ((MediaGalleryFragmentBase) this).A06.A02();
        return true;
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A04 = null;
        this.A05 = null;
        int childCount = ((MediaGalleryFragmentBase) this).A08.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = ((MediaGalleryFragmentBase) this).A08.getChildAt(i);
            if (childAt instanceof AnonymousClass2T1) {
                ((ImageView) childAt).setImageDrawable(null);
            }
        }
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A13() {
        super.A13();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addDataScheme("file");
        this.A03 = new C51802Yw(this);
        A0C().registerReceiver(this.A03, intentFilter);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r6, AnonymousClass2T3 r7) {
        if (this.A01 <= 1) {
            return false;
        }
        if (A1J()) {
            A1N(r6);
            return true;
        }
        HashSet hashSet = this.A0E;
        Uri AAE = r6.AAE();
        hashSet.add(AAE);
        this.A0D.A03(new C39341ph(AAE));
        ActivityC000900k A0B = A0B();
        AnonymousClass009.A05(A0B);
        this.A05 = ((ActivityC000800j) A0B).A1W(this.A04);
        ((MediaGalleryFragmentBase) this).A06.A02();
        A1F(hashSet.size());
        return true;
    }

    public void A1M() {
        this.A0E.clear();
        ((MediaGalleryFragmentBase) this).A06.A02();
    }

    public void A1N(AbstractC35611iN r8) {
        if (r8 == null) {
            return;
        }
        if (A1J()) {
            HashSet hashSet = this.A0E;
            Uri AAE = r8.AAE();
            if (hashSet.contains(AAE)) {
                hashSet.remove(AAE);
                this.A0D.A00.remove(AAE);
            } else if (hashSet.size() < this.A01) {
                hashSet.add(AAE);
                this.A0D.A03(new C39341ph(AAE));
            } else {
                ((MediaGalleryFragmentBase) this).A07.A0E(A01().getString(R.string.share_too_many_items_with_placeholder, Integer.valueOf(this.A01)), 0);
            }
            if (hashSet.isEmpty()) {
                boolean z = ((MediaGalleryFragmentBase) this).A0Q;
                AbstractC009504t r0 = this.A05;
                AnonymousClass009.A05(r0);
                if (z) {
                    r0.A06();
                } else {
                    r0.A05();
                }
            } else {
                AbstractC009504t r02 = this.A05;
                AnonymousClass009.A05(r02);
                r02.A06();
                ((MediaGalleryFragmentBase) this).A07.A0J(new RunnableBRunnable0Shape6S0100000_I0_6(this, 33), 300);
            }
            ((MediaGalleryFragmentBase) this).A06.A02();
            return;
        }
        HashSet hashSet2 = new HashSet();
        Uri AAE2 = r8.AAE();
        hashSet2.add(AAE2);
        this.A0D.A03(new C39341ph(AAE2));
        A1O(hashSet2);
    }

    public void A1O(Set set) {
        if (set != null && !set.isEmpty()) {
            ArrayList<? extends Parcelable> arrayList = new ArrayList<>(set);
            ActivityC000900k A0C = A0C();
            Uri uri = null;
            if (this.A0C) {
                int intExtra = A0C.getIntent().getIntExtra("origin", 1);
                AnonymousClass24W r4 = new AnonymousClass24W(A0C);
                r4.A0C = arrayList;
                r4.A08 = C15380n4.A03(this.A07);
                r4.A00 = this.A01;
                r4.A01 = intExtra;
                r4.A02 = System.currentTimeMillis() - this.A02;
                r4.A03 = A0C.getIntent().getLongExtra("picker_open_time", 0);
                r4.A0G = true;
                r4.A04 = A0C.getIntent().getLongExtra("quoted_message_row_id", 0);
                r4.A09 = A0C.getIntent().getStringExtra("quoted_group_jid");
                boolean z = false;
                if (intExtra != 20) {
                    z = true;
                }
                r4.A0F = z;
                r4.A0D = A0C.getIntent().getBooleanExtra("number_from_url", false);
                C453421e r2 = this.A0D;
                C39341ph A00 = r2.A00((Uri) arrayList.get(0));
                List A07 = C15380n4.A07(UserJid.class, A0C.getIntent().getStringArrayListExtra("mentions"));
                Iterator it = new ArrayList(r2.A00.values()).iterator();
                while (it.hasNext()) {
                    C39341ph r0 = (C39341ph) it.next();
                    r0.A0D(null);
                    r0.A0E(null);
                }
                if (!A07.isEmpty()) {
                    A00.A0E(AnonymousClass1Y6.A00(A07));
                }
                String stringExtra = A0C.getIntent().getStringExtra("android.intent.extra.TEXT");
                if (!TextUtils.isEmpty(stringExtra)) {
                    A00.A0D(stringExtra);
                }
                Bundle bundle = new Bundle();
                r2.A02(bundle);
                r4.A06 = bundle;
                if (!AbstractC454421p.A00 || arrayList.size() != 1 || ((AnonymousClass01E) this).A0A == null) {
                    AnonymousClass009.A05(A0C);
                } else {
                    Uri uri2 = (Uri) arrayList.get(0);
                    AnonymousClass2T3 A1A = A1A(uri2);
                    if (A1A != null) {
                        r4.A05 = uri2;
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(new AnonymousClass01T(A1A, uri2.toString()));
                        View findViewById = ((AnonymousClass01E) this).A0A.findViewById(R.id.header_transition);
                        arrayList2.add(new AnonymousClass01T(findViewById, AnonymousClass028.A0J(findViewById)));
                        View findViewById2 = ((AnonymousClass01E) this).A0A.findViewById(R.id.transition_clipper_bottom);
                        AnonymousClass028.A0k(findViewById2, new AnonymousClass2TT(A0C()).A00(R.string.transition_footer));
                        arrayList2.add(new AnonymousClass01T(findViewById2, AnonymousClass028.A0J(findViewById2)));
                        View findViewById3 = ((AnonymousClass01E) this).A0A.findViewById(R.id.gallery_filter_swipe_transition);
                        arrayList2.add(new AnonymousClass01T(findViewById3, AnonymousClass028.A0J(findViewById3)));
                        View findViewById4 = ((AnonymousClass01E) this).A0A.findViewById(R.id.gallery_send_button_transition);
                        arrayList2.add(new AnonymousClass01T(findViewById4, AnonymousClass028.A0J(findViewById4)));
                        Bitmap bitmap = A1A.A00;
                        if (bitmap != null) {
                            AnonymousClass1O4 A02 = ((MediaGalleryFragmentBase) this).A09.A02();
                            StringBuilder sb = new StringBuilder();
                            sb.append(uri2);
                            sb.append("-gallery_thumb");
                            A02.A03(sb.toString(), bitmap);
                        }
                        A0C.startActivityForResult(r4.A00(), 1, C018108l.A02(A0C, (AnonymousClass01T[]) arrayList2.toArray(new AnonymousClass01T[0])).A03());
                        return;
                    }
                }
                A0C.startActivityForResult(r4.A00(), 1);
                return;
            }
            Intent intent = new Intent();
            AnonymousClass009.A05(A0C);
            intent.putExtra("bucket_uri", A0C.getIntent().getData());
            intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
            if (arrayList.size() == 1) {
                uri = (Uri) arrayList.get(0);
            }
            intent.setData(uri);
            A0C.setResult(-1, intent);
            A0C.finish();
        }
    }
}
