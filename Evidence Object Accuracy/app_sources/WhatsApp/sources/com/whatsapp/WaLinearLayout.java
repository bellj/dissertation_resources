package com.whatsapp;

import X.AbstractC53222dI;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass2GZ;
import X.C015607k;
import X.C42941w9;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaLinearLayout extends AbstractC53222dI {
    public int A00 = 0;

    public WaLinearLayout(Context context) {
        super(context);
    }

    public WaLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01(context, attributeSet);
    }

    public WaLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(context, attributeSet);
    }

    public WaLinearLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01(context, attributeSet);
    }

    public final void A01(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0U);
            this.A00 = obtainStyledAttributes.getResourceId(1, 0);
            boolean z = obtainStyledAttributes.getBoolean(0, false);
            Drawable background = getBackground();
            if (!(background == null || this.A00 == 0)) {
                setBackground(background);
            }
            obtainStyledAttributes.recycle();
            if (z) {
                AnonymousClass028.A0c(this, 0);
                setTag(R.id.bidilayout_ignore, C42941w9.A00);
            }
        }
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        if (!(this.A00 == 0 || drawable == null)) {
            drawable = C015607k.A03(drawable);
            C015607k.A0A(drawable, AnonymousClass00T.A00(getContext(), this.A00));
        }
        super.setBackground(drawable);
    }
}
