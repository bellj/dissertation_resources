package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass00X;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import X.C53662eo;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.material.textfield.TextInputEditText;

/* loaded from: classes2.dex */
public class ClearableEditText extends TextInputEditText implements TextWatcher, View.OnTouchListener, AnonymousClass004 {
    public Drawable A00;
    public View.OnClickListener A01;
    public AnonymousClass018 A02;
    public AnonymousClass2P7 A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final Rect A08;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.widget.TextView, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public ClearableEditText(Context context) {
        super(context, null);
        A02();
        this.A07 = true;
        this.A08 = C12980iv.A0J();
        A03(context, null);
    }

    public ClearableEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A07 = true;
        this.A08 = C12980iv.A0J();
        A03(context, attributeSet);
    }

    public ClearableEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A07 = true;
        this.A08 = C12980iv.A0J();
        A03(context, attributeSet);
    }

    public ClearableEditText(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    public void A02() {
        if (!this.A06) {
            this.A06 = true;
            this.A02 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A03(Context context, AttributeSet attributeSet) {
        int i = R.drawable.ic_backup_cancel;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A07);
            this.A05 = obtainStyledAttributes.getBoolean(1, false);
            this.A07 = obtainStyledAttributes.getBoolean(3, true);
            this.A04 = obtainStyledAttributes.getBoolean(0, false);
            i = obtainStyledAttributes.getResourceId(2, R.drawable.ic_backup_cancel);
            obtainStyledAttributes.recycle();
        }
        this.A00 = AnonymousClass00X.A04(null, getResources(), i);
        setOnTouchListener(this);
        addTextChangedListener(this);
        AnonymousClass028.A0g(this, new C53662eo(this, this));
    }

    public final void A04(Editable editable) {
        if (this.A04 || (!TextUtils.isEmpty(editable) && isFocusable() && isEnabled())) {
            if (this.A00 != null) {
                boolean A00 = C28141Kv.A00(this.A02);
                Drawable drawable = this.A00;
                if (A00) {
                    setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable, (Drawable) null);
                }
            }
        } else if (getClearIconDrawable() != null) {
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        A04(editable);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    /* access modifiers changed from: private */
    public Rect getClearBounds() {
        int A08;
        int width;
        Drawable clearIconDrawable = getClearIconDrawable();
        if (clearIconDrawable == null) {
            return C12980iv.A0J();
        }
        if (C28141Kv.A00(this.A02)) {
            A08 = 0;
        } else {
            A08 = C12980iv.A08(this) - clearIconDrawable.getIntrinsicWidth();
        }
        if (C28141Kv.A00(this.A02)) {
            width = getPaddingLeft() + clearIconDrawable.getIntrinsicWidth();
        } else {
            width = getWidth();
        }
        int bottom = ((getBottom() - getTop()) >> 1) - (clearIconDrawable.getIntrinsicHeight() >> 1);
        int bottom2 = ((getBottom() - getTop()) >> 1) + (clearIconDrawable.getIntrinsicHeight() >> 1);
        Rect rect = this.A08;
        rect.left = A08;
        rect.right = width;
        rect.top = bottom;
        rect.bottom = bottom2;
        return rect;
    }

    public Drawable getClearIconDrawable() {
        boolean A00 = C28141Kv.A00(this.A02);
        Drawable[] compoundDrawables = getCompoundDrawables();
        char c = 2;
        if (A00) {
            c = 0;
        }
        return compoundDrawables[c];
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (this.A05 && keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            clearFocus();
        }
        return super.onKeyPreIme(i, keyEvent);
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (getClearIconDrawable() == null) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() != 1) {
            return false;
        }
        Rect clearBounds = getClearBounds();
        if (x < clearBounds.left || x > clearBounds.right || y < clearBounds.top || y > clearBounds.bottom) {
            return false;
        }
        View.OnClickListener onClickListener = this.A01;
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
        C12990iw.A1G(this);
        requestFocus();
        return this.A07;
    }

    public void setAlwaysShowClearIcon(boolean z) {
        if (z != this.A04) {
            this.A04 = z;
            invalidate();
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        A04(getText());
    }

    public void setOnClearIconClickedListener(View.OnClickListener onClickListener) {
        this.A01 = onClickListener;
    }
}
