package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass007;
import X.AnonymousClass008;

/* loaded from: classes.dex */
public class AppShell extends AbstractConsumerAppShell implements AnonymousClass004 {
    public final AnonymousClass008 componentManager;

    public AppShell() {
        this(0);
    }

    public AppShell(int i) {
        this.componentManager = new AnonymousClass008(new AnonymousClass007(this));
    }

    public final AnonymousClass008 componentManager() {
        return this.componentManager;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        return this.componentManager.generatedComponent();
    }

    @Override // X.AnonymousClass002, android.app.Application
    public void onCreate() {
        generatedComponent();
        super.onCreate();
    }
}
