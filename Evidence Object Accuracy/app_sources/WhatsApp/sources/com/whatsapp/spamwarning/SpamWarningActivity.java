package com.whatsapp.spamwarning;

import X.AbstractC18870tC;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass2FL;
import X.AnonymousClass539;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14960mK;
import X.C16240og;
import X.C252018m;
import X.CountDownTimerC51962Zv;
import android.os.Bundle;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S1100000_I1;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SpamWarningActivity extends ActivityC13790kL {
    public int A00;
    public AbstractC18870tC A01;
    public C16240og A02;
    public C252018m A03;
    public boolean A04;

    public SpamWarningActivity() {
        this(0);
    }

    public SpamWarningActivity(int i) {
        this.A04 = false;
        ActivityC13830kP.A1P(this, 123);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A03 = C12980iv.A0g(A1M);
            this.A02 = (C16240og) A1M.ANq.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        AnonymousClass12P.A03(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        setContentView(R.layout.activity_spam_warning);
        setTitle(R.string.spam_title);
        int intExtra = getIntent().getIntExtra("spam_warning_reason_key", 100);
        this.A00 = getIntent().getIntExtra("expiry_in_seconds", -1);
        String stringExtra = getIntent().getStringExtra("spam_warning_message_key");
        String stringExtra2 = getIntent().getStringExtra("faq_url_key");
        StringBuilder A0k = C12960it.A0k("SpamWarningActivity started with code ");
        A0k.append(intExtra);
        A0k.append(" and expiry (in seconds) ");
        A0k.append(this.A00);
        C12960it.A1F(A0k);
        switch (intExtra) {
            case 101:
                i = R.string.spam_too_many_messages;
                break;
            case 102:
                i = R.string.spam_too_many_blocks;
                break;
            case 103:
                i = R.string.spam_too_many_groups;
                break;
            case 104:
                i = R.string.spam_too_many_people;
                break;
            case 105:
            default:
                int i2 = this.A00;
                i = R.string.spam_generic;
                if (i2 == -1) {
                    i = R.string.spam_generic_unknown_time_left;
                    break;
                }
                break;
            case 106:
                i = R.string.spam_too_many_messages_broadcasted;
                break;
        }
        findViewById(R.id.btn_spam_warning_learn_more).setOnClickListener(new ViewOnClickCListenerShape1S1100000_I1(4, stringExtra2, this));
        TextView A0M = C12970iu.A0M(this, R.id.spam_warning_info_textview);
        if (stringExtra == null || stringExtra.isEmpty()) {
            A0M.setText(i);
        } else {
            A0M.setText(stringExtra);
        }
        if (this.A00 == -1) {
            C12970iu.A1N(this, R.id.progress_bar, 8);
            if (this.A02.A04 == 2 || this.A02.A04 == 1) {
                startActivity(C14960mK.A02(this));
                finish();
                return;
            }
            AnonymousClass539 r1 = new AnonymousClass539(this);
            this.A01 = r1;
            this.A02.A05(r1);
            return;
        }
        C12970iu.A1N(this, R.id.spam_warning_generic_data_connection_missing_textview, 8);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progress_bar);
        circularProgressBar.setVisibility(0);
        circularProgressBar.A0I = true;
        circularProgressBar.setMax(this.A00 * 1000);
        new CountDownTimerC51962Zv(circularProgressBar, this, (long) (this.A00 * 1000)).start();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        AbstractC18870tC r1 = this.A01;
        if (r1 != null) {
            this.A02.A04(r1);
            this.A01 = null;
        }
        super.onDestroy();
    }
}
