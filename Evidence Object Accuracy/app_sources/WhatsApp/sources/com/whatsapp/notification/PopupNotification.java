package com.whatsapp.notification;

import X.AbstractC116455Vm;
import X.AbstractC14020ki;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14670lq;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC32741cf;
import X.AbstractC33331dp;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10Y;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass13H;
import X.AnonymousClass13L;
import X.AnonymousClass14P;
import X.AnonymousClass14X;
import X.AnonymousClass193;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass19P;
import X.AnonymousClass1AB;
import X.AnonymousClass1FO;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass1KS;
import X.AnonymousClass1OY;
import X.AnonymousClass1US;
import X.AnonymousClass1XB;
import X.AnonymousClass1XF;
import X.AnonymousClass1XO;
import X.AnonymousClass1XP;
import X.AnonymousClass1m6;
import X.AnonymousClass243;
import X.AnonymousClass2D2;
import X.AnonymousClass2D6;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass2GV;
import X.AnonymousClass2IC;
import X.AnonymousClass2x5;
import X.AnonymousClass2y4;
import X.AnonymousClass361;
import X.AnonymousClass3JK;
import X.AnonymousClass3S4;
import X.AnonymousClass3UH;
import X.AnonymousClass5UA;
import X.C004802e;
import X.C103234qQ;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14910mF;
import X.C14960mK;
import X.C15270mq;
import X.C15330mx;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15860o1;
import X.C16030oK;
import X.C16170oZ;
import X.C16440p1;
import X.C16590pI;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19990v2;
import X.C20220vP;
import X.C20640w5;
import X.C20710wC;
import X.C20830wO;
import X.C21270x9;
import X.C21300xC;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22200yh;
import X.C22280yp;
import X.C22330yu;
import X.C22700zV;
import X.C23000zz;
import X.C231510o;
import X.C238013b;
import X.C239913u;
import X.C244215l;
import X.C250417w;
import X.C252718t;
import X.C253519b;
import X.C26511Dt;
import X.C27131Gd;
import X.C27531Hw;
import X.C28581Od;
import X.C28801Pb;
import X.C30051Vw;
import X.C30061Vy;
import X.C30341Xa;
import X.C30351Xb;
import X.C30411Xh;
import X.C30421Xi;
import X.C30721Yo;
import X.C30731Yp;
import X.C32401c6;
import X.C35191hP;
import X.C36021jC;
import X.C36861kl;
import X.C37011l7;
import X.C38131nZ;
import X.C39151pN;
import X.C42971wC;
import X.C44891zj;
import X.C52162aM;
import X.C55332iE;
import X.C60332wf;
import X.C60692yR;
import X.C60712yT;
import X.C70383bC;
import X.C858644l;
import X.C92994Ym;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.facebook.redex.ViewOnClickCListenerShape0S1100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.preference.WaFontListPreference;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class PopupNotification extends ActivityC13810kN {
    public float A00;
    public int A01;
    public Sensor A02;
    public SensorEventListener A03;
    public SensorManager A04;
    public PowerManager.WakeLock A05;
    public View.OnClickListener A06;
    public View A07;
    public View A08;
    public View A09;
    public Button A0A;
    public ImageButton A0B;
    public ImageButton A0C;
    public TextView A0D;
    public TextView A0E;
    public TextView A0F;
    public AnonymousClass12P A0G;
    public C253519b A0H;
    public AbstractC116455Vm A0I;
    public C20640w5 A0J;
    public AnonymousClass1FO A0K;
    public AnonymousClass11L A0L;
    public C28801Pb A0M;
    public C21740xu A0N;
    public C16170oZ A0O;
    public C14650lo A0P;
    public C238013b A0Q;
    public C22330yu A0R;
    public AnonymousClass2D6 A0S;
    public AnonymousClass130 A0T;
    public C15550nR A0U;
    public AnonymousClass10S A0V;
    public C22700zV A0W;
    public C15610nY A0X;
    public AnonymousClass1J1 A0Y;
    public C21270x9 A0Z;
    public AnonymousClass131 A0a;
    public C250417w A0b;
    public C37011l7 A0c;
    public AnonymousClass19D A0d;
    public AnonymousClass11P A0e;
    public AnonymousClass19P A0f;
    public C14830m7 A0g;
    public C16590pI A0h;
    public AnonymousClass018 A0i;
    public C19990v2 A0j;
    public C20830wO A0k;
    public C15680nj A0l;
    public C15600nX A0m;
    public AnonymousClass10Y A0n;
    public AnonymousClass12H A0o;
    public C15370n3 A0p;
    public C231510o A0q;
    public C15330mx A0r;
    public AnonymousClass193 A0s;
    public C20710wC A0t;
    public C244215l A0u;
    public AbstractC14640lm A0v;
    public C16030oK A0w;
    public AnonymousClass13H A0x;
    public C20220vP A0y;
    public AnonymousClass13L A0z;
    public PopupNotificationViewPager A10;
    public AnonymousClass14X A11;
    public C16630pM A12;
    public C21840y4 A13;
    public C14910mF A14;
    public C22280yp A15;
    public AbstractC15340mz A16;
    public C239913u A17;
    public C21820y2 A18;
    public C15860o1 A19;
    public AnonymousClass12F A1A;
    public AnonymousClass1AB A1B;
    public C23000zz A1C;
    public C21300xC A1D;
    public C252718t A1E;
    public AnonymousClass19O A1F;
    public AbstractC14670lq A1G;
    public AnonymousClass2IC A1H;
    public Integer A1I;
    public HashSet A1J;
    public HashSet A1K;
    public HashSet A1L;
    public List A1M;
    public boolean A1N;
    public boolean A1O;
    public boolean A1P;
    public final Handler A1Q;
    public final Handler A1R;
    public final AnonymousClass2Dn A1S;
    public final C27131Gd A1T;
    public final AbstractC18860tB A1U;
    public final AbstractC33331dp A1V;
    public final AnonymousClass2D2 A1W;
    public final Runnable A1X;
    public final Runnable A1Y;

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public boolean AJN() {
        return false;
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void AaN() {
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Adl(DialogFragment dialogFragment, String str) {
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Adm(DialogFragment dialogFragment) {
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Ady(int i, int i2) {
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void AfX(String str) {
    }

    public PopupNotification() {
        this(0);
        this.A1M = new ArrayList();
        this.A1L = new HashSet();
        this.A1K = new HashSet();
        this.A1J = new HashSet();
        this.A1W = new AnonymousClass2D2(this);
        this.A1U = new AnonymousClass1m6(this);
        this.A1T = new C36861kl(this);
        this.A1S = new C60332wf(this);
        this.A1V = new C858644l(this);
        this.A0I = new AnonymousClass3UH(this);
        this.A00 = 5.0f;
        this.A1Q = new Handler(Looper.getMainLooper());
        this.A1X = new RunnableBRunnable0Shape9S0100000_I0_9(this, 6);
        this.A1R = new Handler(Looper.getMainLooper());
        this.A1Y = new RunnableBRunnable0Shape9S0100000_I0_9(this, 7);
    }

    public PopupNotification(int i) {
        this.A1N = false;
        A0R(new C103234qQ(this));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v2, resolved type: android.text.SpannableStringBuilder */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ View A02(PopupNotification popupNotification, AbstractC15340mz r21) {
        SpannableStringBuilder A0I;
        ThumbnailButton thumbnailButton;
        String str;
        AnonymousClass2x5 r2;
        Bitmap bitmap;
        C30721Yo r0;
        byte[] bArr;
        String str2;
        byte b = r21.A0y;
        if (b != 0) {
            if (b != 1) {
                if (b != 2) {
                    try {
                        if (b == 3) {
                            AbstractC16130oV r8 = (AbstractC16130oV) r21;
                            AnonymousClass2x5 r22 = new AnonymousClass2x5(popupNotification);
                            r22.setId(R.id.popup_notification_message_video_message);
                            popupNotification.A2a(r22);
                            r22.A08 = popupNotification.getResources().getDrawable(R.drawable.mark_video);
                            AnonymousClass19O r7 = popupNotification.A1F;
                            str = null;
                            r7.A08(r22, r8, new C70383bC(r22, r7, R.drawable.media_video));
                            int i = r8.A00;
                            if (i != 0) {
                                str = C38131nZ.A04(popupNotification.A0i, (long) i);
                                r2 = r22;
                            } else {
                                long j = r8.A01;
                                r2 = r22;
                                if (j > 0) {
                                    str = C44891zj.A03(popupNotification.A0i, j);
                                    r2 = r22;
                                }
                            }
                        } else if (b == 4) {
                            C30411Xh r10 = (C30411Xh) r21;
                            TextView textView = new TextView(popupNotification);
                            textView.setText(AnonymousClass1US.A03(128, r10.A00));
                            textView.setTextSize(AnonymousClass1OY.A02(popupNotification.getResources(), popupNotification.A0i, WaFontListPreference.A00));
                            textView.setGravity(17);
                            textView.setTextColor(AnonymousClass00T.A00(popupNotification, R.color.primary_text));
                            C30731Yp A15 = r10.A15(popupNotification.A0P, popupNotification.A0U, popupNotification.A0h, popupNotification.A0i);
                            if (A15 == null || (r0 = A15.A01) == null || (bArr = r0.A09) == null) {
                                bitmap = null;
                            } else {
                                bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                            }
                            Drawable drawable = popupNotification.getResources().getDrawable(R.drawable.shared_contact_btn);
                            if (bitmap != null) {
                                drawable = new BitmapDrawable(popupNotification.getResources(), C22200yh.A0A(bitmap, popupNotification.getResources().getDimension(R.dimen.thumb_round_radius), drawable.getIntrinsicWidth()));
                            }
                            textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                            int dimensionPixelSize = popupNotification.getResources().getDimensionPixelSize(R.dimen.image_button_padding);
                            textView.setCompoundDrawablePadding(dimensionPixelSize);
                            textView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                            RelativeLayout relativeLayout = new RelativeLayout(popupNotification);
                            relativeLayout.setGravity(17);
                            relativeLayout.addView(textView);
                            thumbnailButton = relativeLayout;
                        } else if (b == 5) {
                            AnonymousClass1XO r13 = (AnonymousClass1XO) r21;
                            RelativeLayout relativeLayout2 = new RelativeLayout(popupNotification);
                            relativeLayout2.setId(R.id.popup_notification_message_location_message);
                            relativeLayout2.setGravity(17);
                            View inflate = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_location_popup, (ViewGroup) relativeLayout2, true);
                            TextView textView2 = (TextView) inflate.findViewById(R.id.place_name);
                            TextView textView3 = (TextView) inflate.findViewById(R.id.place_address);
                            if (TextUtils.isEmpty(r13.A01)) {
                                textView2.setVisibility(8);
                                textView3.setVisibility(8);
                            } else {
                                textView2.setVisibility(0);
                                if (TextUtils.isEmpty(r13.A02)) {
                                    StringBuilder sb = new StringBuilder("https://maps.google.com/maps?q=");
                                    sb.append(Uri.encode(r13.A17().replaceAll("\\s+", "+")));
                                    sb.append("&sll=");
                                    sb.append(((AnonymousClass1XP) r13).A00);
                                    sb.append(",");
                                    sb.append(((AnonymousClass1XP) r13).A01);
                                    str2 = sb.toString();
                                } else {
                                    str2 = r13.A02;
                                }
                                textView2.setAutoLinkMask(0);
                                StringBuilder sb2 = new StringBuilder("<a href=\"");
                                sb2.append(str2);
                                sb2.append("\">");
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(r13.A01);
                                sb2.append(Html.escapeHtml(sb3.toString()));
                                sb2.append("</a>");
                                textView2.setText(Html.fromHtml(sb2.toString()));
                                if (!TextUtils.isEmpty(r13.A00)) {
                                    textView3.setVisibility(0);
                                    textView3.setText(r13.A00);
                                } else {
                                    textView3.setVisibility(8);
                                }
                                ViewOnClickCListenerShape0S1100000_I0 viewOnClickCListenerShape0S1100000_I0 = new ViewOnClickCListenerShape0S1100000_I0(4, str2, popupNotification);
                                textView2.setOnClickListener(viewOnClickCListenerShape0S1100000_I0);
                                textView3.setOnClickListener(viewOnClickCListenerShape0S1100000_I0);
                            }
                            ThumbnailButton thumbnailButton2 = (ThumbnailButton) inflate.findViewById(R.id.thumb);
                            int dimensionPixelSize2 = popupNotification.getResources().getDimensionPixelSize(R.dimen.image_button_padding);
                            thumbnailButton2.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
                            thumbnailButton2.A02 = popupNotification.getResources().getDimension(R.dimen.thumb_round_radius);
                            thumbnailButton2.A01 = 1.0f;
                            thumbnailButton2.A03 = 1711276032;
                            AnonymousClass19O r72 = popupNotification.A1F;
                            r72.A08(thumbnailButton2, r13, new C70383bC(thumbnailButton2, r72, R.drawable.media_location));
                            thumbnailButton2.setOnClickListener(popupNotification.A06);
                            thumbnailButton = relativeLayout2;
                        } else if (b == 9) {
                            C16440p1 r9 = (C16440p1) r21;
                            RelativeLayout relativeLayout3 = new RelativeLayout(popupNotification);
                            relativeLayout3.setId(R.id.popup_notification_message_document_message);
                            relativeLayout3.setGravity(17);
                            View inflate2 = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_document_popup, (ViewGroup) relativeLayout3, true);
                            TextView textView4 = (TextView) inflate2.findViewById(R.id.title);
                            TextView textView5 = (TextView) inflate2.findViewById(R.id.info);
                            View findViewById = inflate2.findViewById(R.id.bullet_info);
                            TextView textView6 = (TextView) inflate2.findViewById(R.id.file_size);
                            View findViewById2 = inflate2.findViewById(R.id.bullet_file_size);
                            TextView textView7 = (TextView) inflate2.findViewById(R.id.file_type);
                            ((ImageView) inflate2.findViewById(R.id.icon)).setImageDrawable(C26511Dt.A02(popupNotification, r9));
                            if (TextUtils.isEmpty(r9.A15())) {
                                textView4.setText(R.string.untitled_document);
                            } else {
                                textView4.setText(r9.A15());
                            }
                            textView6.setVisibility(0);
                            findViewById2.setVisibility(0);
                            textView6.setText(C44891zj.A03(popupNotification.A0i, ((AbstractC16130oV) r9).A01));
                            if (r9.A00 != 0) {
                                textView5.setVisibility(0);
                                findViewById.setVisibility(0);
                                textView5.setText(C26511Dt.A05(popupNotification.A0i, r9));
                            } else {
                                textView5.setVisibility(8);
                                findViewById.setVisibility(8);
                            }
                            textView7.setText(AnonymousClass14P.A00(((AbstractC16130oV) r9).A06).toUpperCase(Locale.US));
                            relativeLayout3.setOnClickListener(popupNotification.A06);
                            thumbnailButton = relativeLayout3;
                        } else if (b == 20) {
                            C30061Vy r73 = (C30061Vy) r21;
                            View inflate3 = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_sticker_popup, (ViewGroup) null, false);
                            StickerView stickerView = (StickerView) inflate3.findViewById(R.id.popup_sticker_view);
                            thumbnailButton = inflate3;
                            if (stickerView != null) {
                                popupNotification.A2c(r73, stickerView);
                                thumbnailButton = inflate3;
                            }
                        } else if (!(b == 37 || b == 23)) {
                            if (b != 24) {
                                switch (b) {
                                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                        AbstractC16130oV r82 = (AbstractC16130oV) r21;
                                        AnonymousClass2x5 r23 = new AnonymousClass2x5(popupNotification);
                                        r23.setId(R.id.popup_notification_message_gif_message);
                                        popupNotification.A2a(r23);
                                        r23.A08 = popupNotification.getResources().getDrawable(R.drawable.mark_gif);
                                        AnonymousClass19O r74 = popupNotification.A1F;
                                        str = null;
                                        r74.A08(r23, r82, new C70383bC(r23, r74, R.drawable.media_video));
                                        int i2 = r82.A00;
                                        if (i2 == 0) {
                                            long j2 = r82.A01;
                                            r2 = r23;
                                            if (j2 > 0) {
                                                str = C44891zj.A03(popupNotification.A0i, j2);
                                                r2 = r23;
                                                break;
                                            }
                                        } else {
                                            str = C38131nZ.A04(popupNotification.A0i, (long) i2);
                                            r2 = r23;
                                            break;
                                        }
                                        break;
                                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                        TextView textView8 = new TextView(popupNotification);
                                        textView8.setText(C32401c6.A07(popupNotification.A0P, popupNotification.A0U, popupNotification.A0h, popupNotification.A0i, (C30351Xb) r21));
                                        textView8.setTextSize(AnonymousClass1OY.A02(popupNotification.getResources(), popupNotification.A0i, WaFontListPreference.A00));
                                        textView8.setGravity(17);
                                        textView8.setTextColor(AnonymousClass00T.A00(popupNotification, R.color.primary_text));
                                        textView8.setCompoundDrawablesWithIntrinsicBounds(popupNotification.getResources().getDrawable(R.drawable.shared_contact_btn), (Drawable) null, (Drawable) null, (Drawable) null);
                                        int dimensionPixelSize3 = popupNotification.getResources().getDimensionPixelSize(R.dimen.image_button_padding);
                                        textView8.setCompoundDrawablePadding(dimensionPixelSize3);
                                        textView8.setPadding(dimensionPixelSize3, dimensionPixelSize3, dimensionPixelSize3, dimensionPixelSize3);
                                        RelativeLayout relativeLayout4 = new RelativeLayout(popupNotification);
                                        relativeLayout4.setGravity(17);
                                        relativeLayout4.addView(textView8);
                                        thumbnailButton = relativeLayout4;
                                        break;
                                    case 15:
                                        TextEmojiLabel textEmojiLabel = new TextEmojiLabel(popupNotification);
                                        String string = popupNotification.getString(R.string.revoked_msg_incoming);
                                        textEmojiLabel.setTextSize(AnonymousClass1OY.A02(popupNotification.getResources(), popupNotification.A0i, WaFontListPreference.A00));
                                        textEmojiLabel.setGravity(17);
                                        textEmojiLabel.setTextColor(AnonymousClass00T.A00(popupNotification, R.color.primary_text));
                                        textEmojiLabel.setPadding(popupNotification.getResources().getDimensionPixelSize(R.dimen.card_h_padding), 0, popupNotification.getResources().getDimensionPixelSize(R.dimen.card_h_padding), 0);
                                        textEmojiLabel.A0F(string, null, 0, true);
                                        textEmojiLabel.A07 = new C52162aM();
                                        thumbnailButton = textEmojiLabel;
                                        break;
                                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                        C30341Xa r83 = (C30341Xa) r21;
                                        RelativeLayout relativeLayout5 = new RelativeLayout(popupNotification);
                                        relativeLayout5.setGravity(17);
                                        View inflate4 = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_live_location_popup, (ViewGroup) relativeLayout5, true);
                                        TextView textView9 = (TextView) inflate4.findViewById(R.id.live_location_caption);
                                        if (!TextUtils.isEmpty(r83.A03)) {
                                            textView9.setText(r83.A03);
                                        } else {
                                            textView9.setVisibility(8);
                                        }
                                        View findViewById3 = inflate4.findViewById(R.id.live_location_icon_1);
                                        View findViewById4 = inflate4.findViewById(R.id.live_location_icon_2);
                                        View findViewById5 = inflate4.findViewById(R.id.live_location_icon_3);
                                        TextView textView10 = (TextView) inflate4.findViewById(R.id.live_location_label);
                                        long A00 = popupNotification.A0g.A00();
                                        long A04 = popupNotification.A0w.A04(r83);
                                        if (A04 > A00) {
                                            findViewById3.setVisibility(0);
                                            findViewById4.setVisibility(0);
                                            findViewById5.setVisibility(0);
                                            textView10.setTextColor(popupNotification.getResources().getColor(R.color.conversation_row_date));
                                            textView10.setText(popupNotification.getString(R.string.live_location_live_until, AnonymousClass3JK.A00(popupNotification.A0i, A04)));
                                        } else {
                                            findViewById3.setVisibility(8);
                                            findViewById4.setVisibility(8);
                                            findViewById5.setVisibility(8);
                                            textView10.setTextColor(popupNotification.getResources().getColor(R.color.live_location_expired_text));
                                            textView10.setText(R.string.live_location_sharing_ended);
                                        }
                                        ThumbnailButton thumbnailButton3 = (ThumbnailButton) inflate4.findViewById(R.id.thumb);
                                        int dimensionPixelSize4 = popupNotification.getResources().getDimensionPixelSize(R.dimen.image_button_padding);
                                        thumbnailButton3.setPadding(dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4);
                                        thumbnailButton3.A02 = popupNotification.getResources().getDimension(R.dimen.thumb_round_radius);
                                        thumbnailButton3.A01 = 1.0f;
                                        thumbnailButton3.A03 = 1711276032;
                                        AnonymousClass19O r75 = popupNotification.A1F;
                                        r75.A08(thumbnailButton3, r83, new C70383bC(thumbnailButton3, r75, R.drawable.media_location));
                                        thumbnailButton3.setOnClickListener(popupNotification.A06);
                                        thumbnailButton = relativeLayout5;
                                        break;
                                    default:
                                        switch (b) {
                                            case 42:
                                            case 43:
                                                thumbnailButton = new C60692yR(popupNotification, popupNotification.A06, (AbstractC16130oV) r21);
                                                break;
                                            case 44:
                                                AnonymousClass1XF r92 = (AnonymousClass1XF) r21;
                                                View inflate5 = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_order_popup, (ViewGroup) null, false);
                                                ImageView imageView = (ImageView) inflate5.findViewById(R.id.thumb);
                                                TextView textView11 = (TextView) inflate5.findViewById(R.id.message_text);
                                                TextView textView12 = (TextView) inflate5.findViewById(R.id.order_subtitle);
                                                ((TextView) inflate5.findViewById(R.id.order_title)).setText(AnonymousClass243.A0Y(popupNotification.A0i, r92));
                                                String A0X = AnonymousClass243.A0X(popupNotification, popupNotification.A0i, r92);
                                                if (TextUtils.isEmpty(A0X)) {
                                                    textView12.setVisibility(8);
                                                } else {
                                                    textView12.setText(A0X);
                                                    textView12.setVisibility(0);
                                                }
                                                if (!TextUtils.isEmpty(r92.A05)) {
                                                    textView11.setVisibility(0);
                                                    textView11.setText(r92.A05);
                                                } else {
                                                    textView11.setVisibility(8);
                                                }
                                                AnonymousClass19O r76 = popupNotification.A1F;
                                                r76.A07(imageView, r92, new C70383bC(imageView, r76, R.drawable.ic_group_invite_link));
                                                inflate5.setOnClickListener(popupNotification.A06);
                                                thumbnailButton = inflate5;
                                                break;
                                            default:
                                                thumbnailButton = new TextView(popupNotification);
                                                break;
                                        }
                                }
                            } else {
                                C28581Od r93 = (C28581Od) r21;
                                View inflate6 = popupNotification.getLayoutInflater().inflate(R.layout.conversation_row_group_invite_popup, (ViewGroup) null, false);
                                ImageView imageView2 = (ImageView) inflate6.findViewById(R.id.avatar);
                                ((TextView) inflate6.findViewById(R.id.group_name)).setText(r93.A05);
                                AnonymousClass19O r77 = popupNotification.A1F;
                                r77.A07(imageView2, r93, new C70383bC(imageView2, r77, R.drawable.avatar_group));
                                inflate6.setOnClickListener(popupNotification.A06);
                                thumbnailButton = inflate6;
                            }
                        }
                        r2.A0A = str;
                    } catch (IllegalArgumentException unused) {
                    }
                    r2.setOnClickListener(popupNotification.A06);
                    thumbnailButton = r2;
                } else {
                    C30421Xi r78 = (C30421Xi) r21;
                    if (((AbstractC15340mz) r78).A08 == 1) {
                        AnonymousClass1J1 r1 = popupNotification.A0Y;
                        if (r1 == null) {
                            r1 = popupNotification.A0Z.A04(popupNotification, "popup-notification");
                            popupNotification.A0Y = r1;
                        }
                        thumbnailButton = new C60712yT(popupNotification, r1, popupNotification.A0e, popupNotification, r78);
                    } else {
                        thumbnailButton = new AnonymousClass2y4(popupNotification, popupNotification, r78);
                    }
                }
            }
            ThumbnailButton thumbnailButton4 = new ThumbnailButton(popupNotification);
            thumbnailButton4.setId(R.id.popup_notification_message_image_message);
            popupNotification.A2a(thumbnailButton4);
            AnonymousClass19O r79 = popupNotification.A1F;
            r79.A08(thumbnailButton4, r21, new C70383bC(thumbnailButton4, r79, R.drawable.media_image));
            thumbnailButton4.setOnClickListener(popupNotification.A06);
            thumbnailButton = thumbnailButton4;
        } else {
            TextEmojiLabel textEmojiLabel2 = new TextEmojiLabel(popupNotification);
            textEmojiLabel2.setId(R.id.popup_notification_message_view_undefined);
            if (r21.A0L != null) {
                A0I = popupNotification.A11.A0U(r21, true);
            } else if (r21 instanceof AnonymousClass1XB) {
                A0I = popupNotification.A0L.A0A((AnonymousClass1XB) r21, false);
            } else {
                A0I = r21.A0I();
            }
            if (r21.A0z()) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A0I);
                popupNotification.A0x.A02(popupNotification, spannableStringBuilder, r21.A0o);
                A0I = spannableStringBuilder;
            }
            textEmojiLabel2.setTextSize(AnonymousClass1OY.A02(popupNotification.getResources(), popupNotification.A0i, WaFontListPreference.A00));
            textEmojiLabel2.setGravity(17);
            textEmojiLabel2.setTextColor(AnonymousClass00T.A00(popupNotification, R.color.primary_text));
            textEmojiLabel2.setPadding(popupNotification.getResources().getDimensionPixelSize(R.dimen.card_h_padding), 0, popupNotification.getResources().getDimensionPixelSize(R.dimen.card_h_padding), 0);
            textEmojiLabel2.A0F(A0I, null, 0, true);
            textEmojiLabel2.A07 = new C52162aM();
            thumbnailButton = textEmojiLabel2;
        }
        boolean A19 = C30051Vw.A19(r21);
        if (!A19 && r21.A0E() == null) {
            return thumbnailButton;
        }
        ViewGroup viewGroup = (ViewGroup) popupNotification.getLayoutInflater().inflate(R.layout.quoted_message_in_popup, (ViewGroup) null, false);
        FrameLayout frameLayout = (FrameLayout) viewGroup.findViewById(R.id.quoted_message_frame);
        if (r21.A0E() != null) {
            frameLayout.setVisibility(0);
            frameLayout.setForeground(C92994Ym.A00(popupNotification));
            TextView textView13 = (TextView) frameLayout.findViewById(R.id.quoted_title);
            TextView textView14 = (TextView) frameLayout.findViewById(R.id.quoted_bullet_divider);
            TextView textView15 = (TextView) frameLayout.findViewById(R.id.quoted_subtitle);
            float A01 = AnonymousClass1OY.A01(popupNotification.getResources(), popupNotification.A0i);
            textView13.setTextSize(A01);
            textView14.setTextSize(A01);
            textView15.setTextSize(A01);
            C27531Hw.A06(textView13);
            C27531Hw.A06(textView14);
            C27531Hw.A06(textView15);
            AnonymousClass19P r15 = popupNotification.A0f;
            AbstractC14640lm r94 = r21.A0z.A00;
            AbstractC15340mz A0E = r21.A0E();
            AnonymousClass1AB r6 = popupNotification.A1B;
            AnonymousClass1J1 r02 = popupNotification.A0Y;
            if (r02 == null) {
                r02 = popupNotification.A0Z.A04(popupNotification, "popup-notification");
                popupNotification.A0Y = r02;
            }
            r15.A01(frameLayout, r02, r94, A0E, r6, false);
        } else {
            frameLayout.setVisibility(8);
        }
        if (A19) {
            int dimensionPixelSize5 = popupNotification.getResources().getDimensionPixelSize(R.dimen.conversation_top_attribute_text_padding_horizontal);
            int dimensionPixelSize6 = popupNotification.getResources().getDimensionPixelSize(R.dimen.conversation_top_attribute_text_indicator_padding_horizontal);
            int dimensionPixelSize7 = popupNotification.getResources().getDimensionPixelSize(R.dimen.conversation_top_attribute_text_padding_top);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.gravity = 3;
            LinearLayout linearLayout = new LinearLayout(popupNotification.getApplicationContext());
            linearLayout.setOrientation(0);
            StringBuilder sb4 = new StringBuilder();
            String str3 = AnonymousClass01V.A06;
            sb4.append(str3);
            sb4.append(popupNotification.getString(R.string.forwarded_message_header));
            sb4.append(str3);
            String obj = sb4.toString();
            TextEmojiLabel textEmojiLabel3 = new TextEmojiLabel(popupNotification);
            textEmojiLabel3.setText(obj);
            textEmojiLabel3.setTextColor(popupNotification.getResources().getColor(R.color.top_attribute_message_text));
            textEmojiLabel3.setTypeface(textEmojiLabel3.getTypeface(), 2);
            textEmojiLabel3.setSingleLine();
            textEmojiLabel3.setLines(1);
            textEmojiLabel3.setTextSize(AnonymousClass1OY.A01(popupNotification.getResources(), popupNotification.A0i));
            textEmojiLabel3.setEllipsize(TextUtils.TruncateAt.END);
            textEmojiLabel3.setCompoundDrawablePadding(dimensionPixelSize6);
            linearLayout.setPadding(dimensionPixelSize5, dimensionPixelSize7, dimensionPixelSize5, 0);
            linearLayout.addView(textEmojiLabel3, -2, -2);
            viewGroup.addView(linearLayout, 0, layoutParams);
            if (popupNotification.A0i.A04().A06) {
                textEmojiLabel3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_forward_message, 0);
            } else {
                textEmojiLabel3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_forward_message, 0, 0, 0);
            }
        }
        viewGroup.addView(thumbnailButton);
        return viewGroup;
    }

    public static /* synthetic */ void A03(PopupNotification popupNotification) {
        if (!popupNotification.A1M.isEmpty()) {
            popupNotification.A2Z(popupNotification.A10.getCurrentItem());
        }
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A1N) {
            this.A1N = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            super.A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            super.A0B = (AnonymousClass19M) r1.A6R.get();
            super.A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            super.A0D = (C18810t5) r1.AMu.get();
            super.A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            this.A0h = (C16590pI) r1.AMg.get();
            this.A0g = (C14830m7) r1.ALb.get();
            this.A0N = (C21740xu) r1.AM1.get();
            this.A1E = (C252718t) r1.A9K.get();
            this.A0x = (AnonymousClass13H) r1.ABY.get();
            this.A0f = (AnonymousClass19P) r1.ACQ.get();
            this.A0j = (C19990v2) r1.A3M.get();
            this.A0J = (C20640w5) r1.AHk.get();
            this.A0O = (C16170oZ) r1.AM4.get();
            this.A0q = (C231510o) r1.AHO.get();
            this.A0G = (AnonymousClass12P) r1.A0H.get();
            this.A0Z = (C21270x9) r1.A4A.get();
            this.A11 = (AnonymousClass14X) r1.AFM.get();
            this.A0T = (AnonymousClass130) r1.A41.get();
            this.A0U = (C15550nR) r1.A45.get();
            this.A0H = (C253519b) r1.A4C.get();
            this.A0i = (AnonymousClass018) r1.ANb.get();
            this.A0X = (C15610nY) r1.AMe.get();
            this.A14 = (C14910mF) r1.ACs.get();
            this.A15 = (C22280yp) r1.AFw.get();
            this.A1D = (C21300xC) r1.A0c.get();
            this.A0Q = (C238013b) r1.A1Z.get();
            this.A0t = (C20710wC) r1.A8m.get();
            this.A0n = (AnonymousClass10Y) r1.AAR.get();
            this.A1A = (AnonymousClass12F) r1.AJM.get();
            this.A19 = (C15860o1) r1.A3H.get();
            this.A17 = (C239913u) r1.AC1.get();
            this.A1F = (AnonymousClass19O) r1.ACO.get();
            this.A13 = (C21840y4) r1.ACr.get();
            this.A0R = (C22330yu) r1.A3I.get();
            this.A0K = (AnonymousClass1FO) r1.AJf.get();
            this.A0s = (AnonymousClass193) r1.A6S.get();
            this.A0y = (C20220vP) r1.AC3.get();
            this.A1C = (C23000zz) r1.ALg.get();
            this.A0W = (C22700zV) r1.AMN.get();
            this.A0l = (C15680nj) r1.A4e.get();
            this.A0P = (C14650lo) r1.A2V.get();
            this.A0w = (C16030oK) r1.AAd.get();
            this.A1B = (AnonymousClass1AB) r1.AKI.get();
            this.A0b = (C250417w) r1.A4b.get();
            this.A18 = (C21820y2) r1.AHx.get();
            this.A0m = (C15600nX) r1.A8x.get();
            this.A0z = (AnonymousClass13L) r1.AFp.get();
            this.A12 = (C16630pM) r1.AIc.get();
            this.A0L = (AnonymousClass11L) r1.ALG.get();
            this.A0a = (AnonymousClass131) r1.A49.get();
            this.A0d = (AnonymousClass19D) r1.ABo.get();
            this.A0e = (AnonymousClass11P) r1.ABp.get();
            this.A0k = (C20830wO) r1.A4W.get();
            this.A0u = (C244215l) r1.A8y.get();
            this.A1H = r2.A0N();
            this.A0o = (AnonymousClass12H) r1.AC5.get();
            this.A0V = (AnonymousClass10S) r1.A46.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2I(AnonymousClass2GV r2, int i, int i2, int i3) {
        Adr(new Object[0], i, i2);
    }

    @Override // X.ActivityC13810kN
    public void A2K(AnonymousClass2GV r2, int i, int i2, int i3, int i4) {
        Adr(new Object[0], i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (A2d() != false) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2T() {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.notification.PopupNotification.A2T():void");
    }

    public void A2U() {
        this.A0b.A02((AbstractC14640lm) this.A0p.A0B(AbstractC14640lm.class), true, true);
        InputMethodManager A0Q = ((ActivityC13810kN) this).A08.A0Q();
        if (A0Q != null && A0Q.isFullscreenMode()) {
            A0Q.hideSoftInputFromWindow(this.A0c.getWindowToken(), 0);
        }
        int currentItem = this.A10.getCurrentItem();
        StringBuilder sb = new StringBuilder("popupnotification/moveToNextMessageOrExit/ message_pos:");
        sb.append(currentItem);
        sb.append(" messages.size:");
        sb.append(this.A1M.size());
        Log.i(sb.toString());
        this.A1K.add(((AbstractC15340mz) this.A1M.get(currentItem)).A0z);
        if (this.A1M.size() == 1 || (this.A0v != null && this.A01 == 1)) {
            A2X();
            finish();
            return;
        }
        int i = currentItem + 1;
        if (currentItem == this.A1M.size() - 1) {
            i = currentItem - 1;
        }
        this.A1I = Integer.valueOf(currentItem);
        this.A10.A0F(i, true);
        if (this.A1M.size() == 1) {
            A2W();
        }
        AbstractC15340mz r0 = this.A16;
        if (r0 != null) {
            this.A1L.add(r0.A0z);
        }
        C15370n3 r02 = this.A0p;
        if (r02 != null) {
            this.A1J.add(r02.A0B(AbstractC14640lm.class));
        }
    }

    public void A2V() {
        C15370n3 r2 = this.A0p;
        if (r2 == null) {
            return;
        }
        if (this.A0Q.A0I((UserJid) r2.A0B(UserJid.class))) {
            C36021jC.A01(this, 106);
            return;
        }
        String trim = this.A0c.getText().toString().trim();
        if (trim.length() > 0) {
            if (C42971wC.A0C(((ActivityC13810kN) this).A08, this.A12, trim)) {
                this.A0O.A08(null, null, null, AbstractC32741cf.A04(trim), Collections.singletonList(this.A0p.A0B(AbstractC14640lm.class)), null, false, false);
                TextKeyListener.clear(this.A0c.getText());
            } else {
                ((ActivityC13810kN) this).A05.A07(R.string.cannot_send_empty_text_message, 1);
            }
            A2U();
            return;
        }
        StringBuilder sb = new StringBuilder("popupnotification/sendentry/empty text ");
        sb.append(this.A1M.size());
        Log.i(sb.toString());
    }

    public final void A2W() {
        findViewById(R.id.navigation_holder).setVisibility(8);
        findViewById(R.id.navigation_divider).setVisibility(8);
        this.A07.setVisibility(8);
        this.A08.setVisibility(8);
    }

    public final void A2X() {
        StringBuilder sb = new StringBuilder("popupnotification/clearnotifications:");
        HashSet hashSet = this.A1J;
        sb.append(hashSet.size());
        Log.i(sb.toString());
        this.A13.A01(true);
        this.A0v = null;
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r4 = (AbstractC14640lm) it.next();
            ArrayList arrayList = new ArrayList();
            int A00 = this.A0j.A00(r4);
            HashSet hashSet2 = this.A1L;
            Iterator it2 = hashSet2.iterator();
            while (it2.hasNext()) {
                AnonymousClass1IS r1 = (AnonymousClass1IS) it2.next();
                AbstractC14640lm r0 = r1.A00;
                if (r0 != null && r0.equals(r4)) {
                    arrayList.add(r1);
                }
            }
            StringBuilder sb2 = new StringBuilder("popupnotification/msg:");
            sb2.append(arrayList.size());
            sb2.append("/");
            sb2.append(A00);
            Log.i(sb2.toString());
            if (arrayList.size() == A00) {
                this.A0b.A02(r4, true, true);
                hashSet2.removeAll(arrayList);
            }
        }
        this.A0z.A00 = null;
        this.A0y.A07();
    }

    public final void A2Y() {
        C15370n3 r4 = this.A0p;
        if (r4 != null) {
            Bitmap A01 = this.A0a.A01(this, r4, getResources().getDimension(R.dimen.small_avatar_radius), getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
            if (A01 == null) {
                AnonymousClass130 r1 = this.A0T;
                A01 = r1.A03(this, r1.A01(this.A0p));
            }
            ((ImageView) findViewById(R.id.popup_thumb)).setImageBitmap(A01);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2Z(int r8) {
        /*
        // Method dump skipped, instructions count: 451
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.notification.PopupNotification.A2Z(int):void");
    }

    public final void A2a(ThumbnailButton thumbnailButton) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.popup_gallery_height);
        thumbnailButton.setMinimumHeight(dimensionPixelSize);
        thumbnailButton.setMaxHeight(dimensionPixelSize);
        thumbnailButton.setAdjustViewBounds(true);
        thumbnailButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.image_button_padding);
        thumbnailButton.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
        thumbnailButton.A02 = getResources().getDimension(R.dimen.thumb_round_radius);
        if (thumbnailButton instanceof AnonymousClass2x5) {
            AnonymousClass2x5 r4 = (AnonymousClass2x5) thumbnailButton;
            r4.A00 = ((float) dimensionPixelSize) / 7.0f;
            r4.A04 = 5;
        }
    }

    public void A2b(AbstractC14640lm r3) {
        StringBuilder sb = new StringBuilder("popupnotification/set-quick-reply-jid:");
        sb.append(r3);
        Log.i(sb.toString());
        this.A0v = r3;
    }

    public final void A2c(C30061Vy r11, StickerView stickerView) {
        AnonymousClass1KS A1C = r11.A1C();
        if (A1C.A08 == null) {
            stickerView.setImageResource(R.drawable.sticker_error_in_conversation);
        }
        int dimensionPixelSize = stickerView.getContext().getResources().getDimensionPixelSize(R.dimen.popup_notification_sticker_size);
        this.A1B.A04(stickerView, A1C, null, 1, dimensionPixelSize, dimensionPixelSize, false, false);
    }

    public final boolean A2d() {
        if (this.A0c.getText().toString().length() > 0 || this.A1G.A0P != null) {
            return true;
        }
        return false;
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Ado(int i) {
        ((ActivityC13810kN) this).A05.A07(i, 0);
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Adp(String str) {
        ((ActivityC13810kN) this).A05.A0E(str, 0);
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Adq(AnonymousClass2GV r1, Object[] objArr, int i, int i2, int i3) {
        Adr(objArr, i, i2);
    }

    @Override // X.ActivityC13810kN, X.AbstractC13860kS
    public void Adr(Object[] objArr, int i, int i2) {
        ((ActivityC13810kN) this).A05.A0E(getString(i2, objArr), 0);
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        C35191hP r0 = this.A0e.A00;
        return (r0 == null || !r0.A0R) && super.dispatchTouchEvent(motionEvent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2X();
        super.onBackPressed();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            getWindow().getDecorView().setLayoutDirection(3);
        }
        if (i >= 21) {
            getWindow().setStatusBarColor(AnonymousClass00T.A00(this, 17170445));
        }
        super.onCreate(bundle);
        boolean z = this.A18.A00;
        int i2 = R.color.popup_dim;
        if (z) {
            i2 = R.color.black;
        }
        getWindow().setBackgroundDrawable(new ColorDrawable(AnonymousClass00T.A00(this, i2)));
        SensorManager A0D = ((ActivityC13810kN) this).A08.A0D();
        AnonymousClass009.A05(A0D);
        this.A04 = A0D;
        this.A02 = A0D.getDefaultSensor(8);
        PowerManager A0I = ((ActivityC13810kN) this).A08.A0I();
        if (A0I == null) {
            Log.w("popupnotification/create pm=null");
        } else {
            this.A05 = C39151pN.A00(A0I, "popupnotification", 268435466);
        }
        setContentView(getLayoutInflater().inflate(R.layout.popup_notification, (ViewGroup) null, false));
        this.A10 = (PopupNotificationViewPager) findViewById(R.id.message_view_pager);
        C37011l7 r3 = (C37011l7) findViewById(R.id.entry);
        this.A0c = r3;
        r3.setFilters(new InputFilter[]{new InputFilter() { // from class: X.4mH
            @Override // android.text.InputFilter
            public final CharSequence filter(CharSequence charSequence, int i3, int i4, Spanned spanned, int i5, int i6) {
                if (PopupNotification.this.A1G.A0P != null) {
                    return "";
                }
                return null;
            }
        }});
        this.A0A = (Button) findViewById(R.id.popup_action_btn);
        this.A0M = new C28801Pb(this, (TextEmojiLabel) findViewById(R.id.popup_title), this.A0X, this.A1A);
        this.A0F = (TextView) findViewById(R.id.conversation_contact_status);
        this.A0D = (TextView) findViewById(R.id.popup_count);
        ImageView imageView = (ImageView) findViewById(R.id.next_btn);
        imageView.setImageDrawable(new AnonymousClass2GF(getResources().getDrawable(R.drawable.selector_media_next), this.A0i));
        this.A07 = findViewById(R.id.next_btn_ext);
        ImageView imageView2 = (ImageView) findViewById(R.id.prev_btn);
        imageView2.setImageDrawable(new AnonymousClass2GF(getResources().getDrawable(R.drawable.selector_media_prev), this.A0i));
        this.A08 = findViewById(R.id.prev_btn_ext);
        this.A0E = (TextView) findViewById(R.id.read_only_chat_info);
        this.A09 = findViewById(R.id.emoji_popup_anchor);
        this.A0B = (ImageButton) findViewById(R.id.send);
        ImageButton imageButton = (ImageButton) findViewById(R.id.voice_note_btn);
        this.A0C = imageButton;
        imageButton.setLongClickable(true);
        AnonymousClass2D6 r1 = new AnonymousClass2D6(new C55332iE(this));
        this.A0S = r1;
        this.A10.setAdapter(r1);
        this.A10.setOnTouchListener(new View.OnTouchListener() { // from class: X.4nP
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return PopupNotification.this.A2d();
            }
        });
        this.A10.A0G(new AnonymousClass3S4(this));
        findViewById(R.id.popup_ok_btn).setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 2));
        ViewOnClickCListenerShape3S0100000_I0_3 viewOnClickCListenerShape3S0100000_I0_3 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 5);
        this.A06 = viewOnClickCListenerShape3S0100000_I0_3;
        this.A0A.setOnClickListener(viewOnClickCListenerShape3S0100000_I0_3);
        ViewOnClickCListenerShape3S0100000_I0_3 viewOnClickCListenerShape3S0100000_I0_32 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 3);
        imageView.setOnClickListener(viewOnClickCListenerShape3S0100000_I0_32);
        this.A07.setOnClickListener(viewOnClickCListenerShape3S0100000_I0_32);
        ViewOnClickCListenerShape3S0100000_I0_3 viewOnClickCListenerShape3S0100000_I0_33 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 4);
        imageView2.setOnClickListener(viewOnClickCListenerShape3S0100000_I0_33);
        this.A08.setOnClickListener(viewOnClickCListenerShape3S0100000_I0_33);
        this.A0B.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.input_send), this.A0i));
        View findViewById = findViewById(R.id.input_layout);
        findViewById.setBackgroundResource(R.drawable.ib_new_round);
        findViewById.setPadding(0, 0, 0, 0);
        View A05 = AnonymousClass00T.A05(this, R.id.text_entry_layout);
        int max = Math.max(A05.getPaddingLeft(), A05.getPaddingRight());
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) A05.getLayoutParams();
        if (!this.A0i.A04().A06) {
            layoutParams.rightMargin = max;
        } else {
            layoutParams.leftMargin = max;
        }
        A05.setLayoutParams(layoutParams);
        this.A0c.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 6));
        getLayoutInflater().inflate(R.layout.voice_note_view, (ViewGroup) findViewById(R.id.voice_note_stub), true);
        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) findViewById(R.id.root_layout);
        this.A1G = this.A1H.A01(keyboardPopupLayout, new C14960mK(), this);
        this.A0C.setOnTouchListener(new View.OnTouchListener() { // from class: X.4nQ
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                PopupNotification popupNotification = PopupNotification.this;
                if (!popupNotification.A0r.A01()) {
                    popupNotification.A1G.A0W(motionEvent, popupNotification.A0C, false);
                }
                return false;
            }
        });
        this.A0C.setVisibility(0);
        this.A0B.setVisibility(8);
        this.A0c.addTextChangedListener(new AnonymousClass361(this));
        this.A0c.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.4pT
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i3, KeyEvent keyEvent) {
                PopupNotification popupNotification = PopupNotification.this;
                if (i3 != 4) {
                    return false;
                }
                popupNotification.A2V();
                return true;
            }
        });
        this.A0B.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 1));
        C252718t r0 = this.A1E;
        AbstractC15710nm r15 = ((ActivityC13810kN) this).A03;
        AnonymousClass19M r12 = super.A0B;
        C231510o r11 = this.A0q;
        C15270mq r10 = new C15270mq(this, (ImageButton) findViewById(R.id.emoji_picker_btn), r15, keyboardPopupLayout, this.A0c, ((ActivityC13810kN) this).A08, super.A09, this.A0i, r12, r11, this.A0s, this.A12, r0);
        r10.A0C(this.A0I);
        r10.A0A = new AnonymousClass5UA() { // from class: X.56W
            @Override // X.AnonymousClass5UA
            public final boolean AJX() {
                AbstractC14670lq r02 = PopupNotification.this.A1G;
                if (r02 == null || r02.A0P == null) {
                    return true;
                }
                return false;
            }
        };
        C15330mx r13 = new C15330mx(this, this.A0i, super.A0B, r10, this.A0q, (EmojiSearchContainer) findViewById(R.id.popup_search_container), this.A12);
        this.A0r = r13;
        r13.A00 = new AbstractC14020ki() { // from class: X.56l
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r32) {
                PopupNotification.this.A0I.APc(r32.A00);
            }
        };
        if (getIntent().getBooleanExtra("popup_notification_extra_dismiss_notification", false)) {
            this.A0y.A0D(false);
            this.A13.A01(true);
        }
        A2b(AbstractC14640lm.A01(getIntent().getStringExtra("popup_notification_extra_quick_reply_jid")));
        A2T();
        this.A0o.A03(this.A1U);
        this.A0V.A03(this.A1T);
        this.A0R.A03(this.A1S);
        this.A0u.A03(this.A1V);
        if (this.A0K.A02(this.A0J) > 0) {
            C36021jC.A01(this, 115);
        }
        if (this.A0v != null) {
            getWindow().setSoftInputMode(4);
        }
        this.A0z.A00 = this.A1W;
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 106) {
            C004802e r5 = new C004802e(this);
            r5.A0A(getString(R.string.cannot_send_to_blocked_contact_1, this.A0X.A04(this.A0p)));
            r5.setPositiveButton(R.string.unblock, new DialogInterface.OnClickListener() { // from class: X.3KF
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    PopupNotification popupNotification = PopupNotification.this;
                    popupNotification.A0Q.A0C(popupNotification, C15370n3.A04(popupNotification.A0p));
                    C36021jC.A00(popupNotification, 106);
                }
            });
            r5.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fw
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(PopupNotification.this, 106);
                }
            });
            return r5.create();
        } else if (i != 115) {
            return super.onCreateDialog(i);
        } else {
            Log.i("popupnotification/dialog-software-about-to-expire");
            return this.A0K.A03(this, this.A0J, this.A0N);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1AB r0 = this.A1B;
        if (r0 != null) {
            r0.A03();
        }
        AnonymousClass1J1 r02 = this.A0Y;
        if (r02 != null) {
            r02.A00();
            this.A0Y = null;
        }
        AbstractC14670lq r03 = this.A1G;
        if (r03 != null) {
            r03.A02();
        }
        this.A0v = null;
        this.A1Q.removeCallbacks(this.A1X);
        this.A1R.removeCallbacks(this.A1Y);
        PowerManager.WakeLock wakeLock = this.A05;
        if (wakeLock != null && wakeLock.isHeld()) {
            this.A05.release();
        }
        SensorEventListener sensorEventListener = this.A03;
        if (sensorEventListener != null) {
            this.A04.unregisterListener(sensorEventListener);
        }
        this.A0e.A06();
        this.A0o.A04(this.A1U);
        this.A0V.A04(this.A1T);
        this.A0R.A04(this.A1S);
        this.A0u.A04(this.A1V);
        this.A0z.A00 = null;
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        Log.i("popupnotification/new-intent");
        super.onNewIntent(intent);
        setIntent(intent);
        A2b(AbstractC14640lm.A01(getIntent().getStringExtra("popup_notification_extra_quick_reply_jid")));
        if (intent.getBooleanExtra("popup_notification_extra_dismiss_notification", false)) {
            this.A0y.A0D(false);
            this.A13.A01(true);
        }
        A2T();
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A1G.A0T(false, false);
        this.A13.A00();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A1O = true;
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A1O = false;
        this.A1G.A0T(false, false);
    }
}
