package com.whatsapp.notification;

import X.AnonymousClass01J;
import X.AnonymousClass11U;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C20220vP;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Locale;

/* loaded from: classes2.dex */
public class MessageNotificationDismissedReceiver extends BroadcastReceiver {
    public C14820m6 A00;
    public C20220vP A01;
    public AnonymousClass11U A02;
    public final Object A03;
    public volatile boolean A04;

    public MessageNotificationDismissedReceiver() {
        this(0);
    }

    public MessageNotificationDismissedReceiver(int i) {
        this.A04 = false;
        this.A03 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A04) {
            synchronized (this.A03) {
                if (!this.A04) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = (C20220vP) r1.AC3.get();
                    this.A00 = C12970iu.A0Z(r1);
                    this.A02 = (AnonymousClass11U) r1.AGi.get();
                    this.A04 = true;
                }
            }
        }
        if (intent.hasExtra("chat_jid")) {
            long longExtra = intent.getLongExtra("last_message_time", -1);
            String stringExtra = intent.getStringExtra("chat_jid");
            Locale locale = Locale.US;
            Object[] objArr = new Object[3];
            objArr[0] = "messagenotificationdismissedreceiver/onreceive";
            objArr[1] = stringExtra;
            C12980iv.A1U(objArr, 2, longExtra);
            String.format(locale, "%s child notification: chatJid=%s, last_message_time=%d", objArr);
            this.A02.AHy(intent);
            return;
        }
        String stringExtra2 = intent.getStringExtra("notification_hash");
        C12970iu.A1D(C12960it.A08(this.A00), "notification_hash", stringExtra2);
        String.format("%s summary notification: notification_hash=%s", "messagenotificationdismissedreceiver/onreceive", stringExtra2);
        this.A01.A06();
    }
}
