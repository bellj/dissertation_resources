package com.whatsapp.notification;

import X.AbstractC14440lR;
import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C14900mE;
import X.C15650ng;
import X.C22160yd;
import X.C250417w;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape0S2200000_I0;

/* loaded from: classes2.dex */
public class MessageOTPNotificationBroadcastReceiver extends BroadcastReceiver {
    public C14900mE A00;
    public C250417w A01;
    public C22160yd A02;
    public C15650ng A03;
    public AbstractC14440lR A04;
    public final Object A05;
    public volatile boolean A06;

    public MessageOTPNotificationBroadcastReceiver() {
        this(0);
    }

    public MessageOTPNotificationBroadcastReceiver(int i) {
        this.A06 = false;
        this.A05 = new Object();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A06) {
            synchronized (this.A05) {
                if (!this.A06) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = (C14900mE) r1.A8X.get();
                    this.A04 = (AbstractC14440lR) r1.ANe.get();
                    this.A02 = (C22160yd) r1.AC4.get();
                    this.A03 = (C15650ng) r1.A4m.get();
                    this.A01 = (C250417w) r1.A4b.get();
                    this.A06 = true;
                }
            }
        }
        String stringExtra = intent.getStringExtra("extra_remote_jid");
        String stringExtra2 = intent.getStringExtra("extra_message_key_id");
        if (stringExtra != null && stringExtra2 != null) {
            this.A04.Ab2(new RunnableBRunnable0Shape0S2200000_I0(this, context, stringExtra, stringExtra2, 0));
        }
    }
}
