package com.whatsapp.notification;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C16490p7;
import X.C20230vQ;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class MissedCallNotificationDismissedReceiver extends BroadcastReceiver {
    public C16490p7 A00;
    public C20230vQ A01;
    public final Object A02;
    public volatile boolean A03;

    public MissedCallNotificationDismissedReceiver() {
        this(0);
    }

    public MissedCallNotificationDismissedReceiver(int i) {
        this.A03 = false;
        this.A02 = new Object();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = (C16490p7) r1.ACJ.get();
                    this.A01 = (C20230vQ) r1.ACe.get();
                    this.A03 = true;
                }
            }
        }
        Log.i("missedcallnotification/dismiss");
        C16490p7 r0 = this.A00;
        r0.A04();
        if (r0.A01) {
            this.A01.A01();
        }
    }
}
