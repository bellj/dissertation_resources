package com.whatsapp.notification;

import X.AnonymousClass004;
import X.AnonymousClass01A;
import X.AnonymousClass2D6;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.viewpager.widget.ViewPager;

/* loaded from: classes2.dex */
public class PopupNotificationViewPager extends ViewPager implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public Integer A01;
    public boolean A02;
    public boolean A03;

    public PopupNotificationViewPager(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A02 = true;
        this.A01 = null;
    }

    public PopupNotificationViewPager(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A02 = true;
        this.A01 = null;
    }

    public PopupNotificationViewPager(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    @Override // androidx.viewpager.widget.ViewPager
    public void A0F(int i, boolean z) {
        A0O(i, z, !z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005a, code lost:
        if ((Integer.MAX_VALUE - r6) > r2) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0O(int r6, boolean r7, boolean r8) {
        /*
            r5 = this;
            X.01A r0 = r5.A0V
            int r0 = r0.A01()
            if (r0 <= 0) goto L_0x0035
            X.01A r1 = r5.A0V
            boolean r0 = r1 instanceof X.AnonymousClass2D6
            if (r0 == 0) goto L_0x0035
            X.2D6 r1 = (X.AnonymousClass2D6) r1
            X.01A r0 = r1.A00
            int r4 = r0.A01()
            if (r6 >= 0) goto L_0x001d
            r0 = -1
        L_0x0019:
            if (r6 >= 0) goto L_0x0021
            int r6 = r6 + r4
            goto L_0x0019
        L_0x001d:
            r0 = 0
            if (r6 < r4) goto L_0x0021
            r0 = 1
        L_0x0021:
            int r6 = r6 % r4
            r3 = 2147483647(0x7fffffff, float:NaN)
            if (r8 == 0) goto L_0x0039
            int r3 = r3 - r6
            int r0 = r5.getDefaultOffsetBlocks()
            int r0 = r0 * r4
            if (r3 <= r0) goto L_0x0035
            int r2 = r5.getDefaultOffsetBlocks()
            int r2 = r2 * r4
        L_0x0034:
            int r6 = r6 + r2
        L_0x0035:
            super.A0F(r6, r7)
            return
        L_0x0039:
            int r2 = r5.getCurrentOffsetBlocks()
            int r2 = r2 + r0
            X.01A r0 = r5.A0V
            int r1 = r0.A01()
            X.01A r0 = r5.A0V
            X.2D6 r0 = (X.AnonymousClass2D6) r0
            X.01A r0 = r0.A00
            int r0 = r0.A01()
            int r1 = r1 / r0
            if (r2 < 0) goto L_0x0053
            if (r2 < r1) goto L_0x0058
        L_0x0053:
            int r2 = r5.getDefaultOffsetBlocks()
            r7 = 0
        L_0x0058:
            int r3 = r3 - r6
            int r2 = r2 * r4
            if (r3 <= r2) goto L_0x0035
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.notification.PopupNotificationViewPager.A0O(int, boolean, boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // androidx.viewpager.widget.ViewPager
    public int getCurrentItem() {
        if (this.A0V.A01() > 0) {
            AnonymousClass01A r2 = this.A0V;
            if (r2 instanceof AnonymousClass2D6) {
                return this.A0A % ((AnonymousClass2D6) r2).A00.A01();
            }
        }
        return this.A0A;
    }

    private int getCurrentOffsetBlocks() {
        if (this.A0V.A01() <= 0) {
            return 0;
        }
        AnonymousClass01A r2 = this.A0V;
        if (r2 instanceof AnonymousClass2D6) {
            return this.A0A / ((AnonymousClass2D6) r2).A00.A01();
        }
        return 0;
    }

    private int getDefaultOffsetBlocks() {
        return (this.A0V.A01() <= 0 || !(this.A0V instanceof AnonymousClass2D6)) ? 0 : 5;
    }

    @Override // androidx.viewpager.widget.ViewPager, android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        this.A02 = true;
        super.onAttachedToWindow();
    }

    @Override // androidx.viewpager.widget.ViewPager, android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        AnonymousClass01A r1 = this.A0V;
        if (!(r1 instanceof AnonymousClass2D6) || ((AnonymousClass2D6) r1).A00.A01() > 1) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    @Override // androidx.viewpager.widget.ViewPager, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.A02 = false;
        Integer num = this.A01;
        if (num != null) {
            A0F(num.intValue(), true);
            this.A01 = null;
        }
    }

    @Override // androidx.viewpager.widget.ViewPager, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        AnonymousClass01A r1 = this.A0V;
        if (!(r1 instanceof AnonymousClass2D6) || ((AnonymousClass2D6) r1).A00.A01() > 1) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }

    @Override // androidx.viewpager.widget.ViewPager
    public void setAdapter(AnonymousClass01A r2) {
        this.A02 = true;
        super.setAdapter(r2);
    }

    @Override // androidx.viewpager.widget.ViewPager
    public void setCurrentItem(int i) {
        A0F(i, false);
    }
}
