package com.whatsapp.notification;

import X.AbstractIntentServiceC27641Ik;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass03l;
import X.AnonymousClass1U8;
import X.AnonymousClass1UY;
import X.C005602s;
import X.C007103o;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C16170oZ;
import X.C16630pM;
import X.C20220vP;
import X.C250417w;
import X.C42961wB;
import X.C42971wC;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.graphics.drawable.IconCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public final class AndroidWear extends AbstractIntentServiceC27641Ik {
    public static AnonymousClass1U8 A08;
    public static final String A09;
    public static final String A0A;
    public static final int[] A0B = {R.string.android_wear_smile_emoji, R.string.android_wear_yes, R.string.android_wear_no, R.string.android_wear_on_my_way, R.string.android_wear_ok, R.string.android_wear_see_you_soon, R.string.android_wear_haha, R.string.android_wear_lol, R.string.android_wear_nice, R.string.android_wear_cant_talk, R.string.android_wear_sad_emoji, R.string.android_wear_thanks};
    public C14900mE A00;
    public C16170oZ A01;
    public C15550nR A02;
    public C250417w A03;
    public AnonymousClass01d A04;
    public C20220vP A05;
    public C16630pM A06;
    public boolean A07 = false;

    static {
        StringBuilder sb = new StringBuilder("com.whatsapp");
        sb.append(".intent.action.MARK_AS_READ");
        A09 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("com.whatsapp");
        sb2.append(".intent.action.REPLY");
        A0A = sb2.toString();
    }

    public AndroidWear() {
        super("AndroidWear");
    }

    public static AnonymousClass03l A00(Context context, C15370n3 r11) {
        PendingIntent A03 = AnonymousClass1UY.A03(context, new Intent(A09, ContentUris.withAppendedId(C42961wB.A00, r11.A08()), context, AndroidWear.class), 134217728);
        String string = context.getString(R.string.mark_read);
        C007103o[] r8 = null;
        IconCompat A02 = IconCompat.A02(null, "", R.drawable.ic_notif_mark_read);
        Bundle bundle = new Bundle();
        CharSequence A00 = C005602s.A00(string);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        C007103o[] r7 = null;
        if (!arrayList.isEmpty()) {
            r8 = (C007103o[]) arrayList.toArray(new C007103o[arrayList.size()]);
        }
        if (!arrayList2.isEmpty()) {
            r7 = (C007103o[]) arrayList2.toArray(new C007103o[arrayList2.size()]);
        }
        return new AnonymousClass03l(A03, bundle, A02, A00, r7, r8, 2, true, false);
    }

    public static boolean A01() {
        return Build.VERSION.SDK_INT >= 18;
    }

    @Override // X.AbstractIntentServiceC27651Il, android.app.IntentService, android.app.Service
    public void onCreate() {
        Log.i("android-wear/onCreate");
        A00();
        super.onCreate();
    }

    @Override // android.app.IntentService, android.app.Service
    public void onDestroy() {
        Log.i("android-wear/onDestroy");
        super.onDestroy();
    }

    @Override // android.app.IntentService
    public void onHandleIntent(Intent intent) {
        C14900mE r2;
        int i;
        if (intent != null) {
            Bundle A00 = C007103o.A00(intent);
            String str = null;
            if (C42961wB.A00(intent.getData())) {
                C15550nR r22 = this.A02;
                Uri data = intent.getData();
                AnonymousClass009.A0E(C42961wB.A00(data));
                C15370n3 A06 = r22.A06(ContentUris.parseId(data));
                if (A06 != null) {
                    if (A00 != null) {
                        CharSequence charSequence = A00.getCharSequence("android_wear_voice_input");
                        if (charSequence != null) {
                            str = charSequence.toString().trim();
                        }
                        if (!C42971wC.A0C(this.A04, this.A06, str)) {
                            Log.i("androidwear/voiceinputfromandroidwear/message is empty");
                            r2 = this.A00;
                            i = 2;
                            r2.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this, i));
                        }
                        this.A00.A0H(new RunnableBRunnable0Shape0S1200000_I0(this, A06, str, 25));
                        return;
                    } else if (A09.equals(intent.getAction())) {
                        this.A00.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(this, 24, A06));
                        return;
                    } else {
                        return;
                    }
                }
            }
            r2 = this.A00;
            i = 3;
            r2.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this, i));
        }
    }
}
