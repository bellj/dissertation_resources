package com.whatsapp.notification;

import X.AbstractC14640lm;
import X.AbstractIntentServiceC27641Ik;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass03l;
import X.AnonymousClass12H;
import X.AnonymousClass1UY;
import X.AnonymousClass1m0;
import X.C005602s;
import X.C007003n;
import X.C007103o;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C16170oZ;
import X.C16630pM;
import X.C20220vP;
import X.C20230vQ;
import X.C250417w;
import X.C42961wB;
import X.C42971wC;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.graphics.drawable.IconCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S1400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.notification.DirectReplyService;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

/* loaded from: classes2.dex */
public class DirectReplyService extends AbstractIntentServiceC27641Ik {
    public C14900mE A00;
    public C16170oZ A01;
    public C15550nR A02;
    public C250417w A03;
    public AnonymousClass01d A04;
    public AnonymousClass12H A05;
    public C20220vP A06;
    public C20230vQ A07;
    public C16630pM A08;
    public boolean A09 = false;

    public DirectReplyService() {
        super("DirectReply");
    }

    public static AnonymousClass03l A00(Context context, C15370n3 r12, String str, int i, boolean z) {
        CharSequence[] charSequenceArr;
        boolean equals = "com.whatsapp.intent.action.DIRECT_REPLY_FROM_MISSED_CALL".equals(str);
        int i2 = R.string.notification_quick_reply;
        if (equals) {
            i2 = R.string.voip_missed_call_notification_message;
        }
        String string = context.getString(i2);
        C007003n r0 = new C007003n("direct_reply_input");
        r0.A00 = string;
        C007103o r4 = new C007103o(r0.A02, string, "direct_reply_input", r0.A03, r0.A01);
        Intent putExtra = new Intent(str, ContentUris.withAppendedId(C42961wB.A00, r12.A08()), context, DirectReplyService.class).putExtra("direct_reply_num_messages", i);
        CharSequence charSequence = r4.A01;
        int i3 = 134217728;
        if (AnonymousClass1UY.A01) {
            i3 = 167772160;
        }
        PendingIntent service = PendingIntent.getService(context, 0, putExtra, i3);
        C007103o[] r10 = null;
        IconCompat A02 = IconCompat.A02(null, "", R.drawable.ic_action_reply);
        Bundle bundle = new Bundle();
        CharSequence A00 = C005602s.A00(charSequence);
        ArrayList arrayList = new ArrayList();
        arrayList.add(r4);
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C007103o r3 = (C007103o) it.next();
            if (r3.A04 || (!((charSequenceArr = r3.A05) == null || charSequenceArr.length == 0) || r3.A03.isEmpty())) {
                arrayList3.add(r3);
            } else {
                arrayList2.add(r3);
            }
        }
        C007103o[] r9 = null;
        if (!arrayList2.isEmpty()) {
            r10 = (C007103o[]) arrayList2.toArray(new C007103o[arrayList2.size()]);
        }
        if (!arrayList3.isEmpty()) {
            r9 = (C007103o[]) arrayList3.toArray(new C007103o[arrayList3.size()]);
        }
        return new AnonymousClass03l(service, bundle, A02, A00, r9, r10, 1, z, false);
    }

    public static /* synthetic */ void A01(Intent intent, C15370n3 r6, AnonymousClass1m0 r7, DirectReplyService directReplyService, String str) {
        directReplyService.A05.A04(r7);
        if (Build.VERSION.SDK_INT >= 28 && !"com.whatsapp.intent.action.DIRECT_REPLY_FROM_MISSED_CALL".equals(str)) {
            C20220vP r2 = directReplyService.A06;
            AbstractC14640lm r3 = (AbstractC14640lm) r6.A0B(AbstractC14640lm.class);
            int intExtra = intent.getIntExtra("direct_reply_num_messages", 0);
            StringBuilder sb = new StringBuilder("messagenotification/posting reply update runnable for jid:");
            sb.append(r3);
            Log.i(sb.toString());
            r2.A01().post(r2.A04(r3, null, intExtra, true, true, false, true));
        }
    }

    public static /* synthetic */ void A02(C15370n3 r14, AnonymousClass1m0 r15, DirectReplyService directReplyService, String str, String str2) {
        directReplyService.A05.A03(r15);
        directReplyService.A01.A08(null, null, null, str, Collections.singletonList(r14.A0B(AbstractC14640lm.class)), null, false, false);
        if ("com.whatsapp.intent.action.DIRECT_REPLY_FROM_MISSED_CALL".equals(str2)) {
            directReplyService.A07.A02();
            return;
        }
        int i = Build.VERSION.SDK_INT;
        C250417w r1 = directReplyService.A03;
        AbstractC14640lm r0 = (AbstractC14640lm) r14.A0B(AbstractC14640lm.class);
        if (i < 28) {
            r1.A02(r0, true, true);
            directReplyService.A06.A07();
            return;
        }
        r1.A02(r0, true, false);
    }

    public static boolean A03() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @Override // X.AbstractIntentServiceC27651Il, android.app.IntentService, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
    }

    @Override // android.app.IntentService
    public void onHandleIntent(Intent intent) {
        String str;
        StringBuilder sb = new StringBuilder("directreplyservice/intent: ");
        sb.append(intent);
        sb.append(" num_message:");
        sb.append(intent.getIntExtra("direct_reply_num_messages", 0));
        Log.i(sb.toString());
        Bundle A00 = C007103o.A00(intent);
        if (A00 == null) {
            str = "directreplyservice/could not find remote input";
        } else {
            String str2 = null;
            if (C42961wB.A00(intent.getData())) {
                C15550nR r2 = this.A02;
                Uri data = intent.getData();
                AnonymousClass009.A0E(C42961wB.A00(data));
                C15370n3 A06 = r2.A06(ContentUris.parseId(data));
                if (A06 != null) {
                    CharSequence charSequence = A00.getCharSequence("direct_reply_input");
                    if (charSequence != null) {
                        str2 = charSequence.toString().trim();
                    }
                    if (!C42971wC.A0C(this.A04, this.A08, str2)) {
                        Log.i("directreplyservice/message is empty");
                        this.A00.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this, 4));
                        return;
                    }
                    String action = intent.getAction();
                    CountDownLatch countDownLatch = new CountDownLatch(1);
                    AnonymousClass1m0 r5 = new AnonymousClass1m0((AbstractC14640lm) A06.A0B(AbstractC14640lm.class), countDownLatch);
                    this.A00.A0H(new Runnable(A06, r5, this, str2, action) { // from class: X.3lM
                        public final /* synthetic */ C15370n3 A00;
                        public final /* synthetic */ AnonymousClass1m0 A01;
                        public final /* synthetic */ DirectReplyService A02;
                        public final /* synthetic */ String A03;
                        public final /* synthetic */ String A04;

                        {
                            this.A02 = r3;
                            this.A01 = r2;
                            this.A00 = r1;
                            this.A03 = r4;
                            this.A04 = r5;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            DirectReplyService directReplyService = this.A02;
                            DirectReplyService.A02(this.A00, this.A01, directReplyService, this.A03, this.A04);
                        }
                    });
                    try {
                        countDownLatch.await();
                    } catch (InterruptedException e) {
                        Log.e("Interrupted while waiting to add message", e);
                    }
                    this.A00.A0H(new RunnableBRunnable0Shape0S1400000_I0(this, r5, A06, intent, action, 1));
                    return;
                }
            }
            str = "directreplyservice/contact could not be found";
        }
        Log.i(str);
    }
}
