package com.whatsapp.accountsync;

import X.AbstractActivityC28171Kz;
import X.AbstractC15850o0;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass116;
import X.AnonymousClass19Z;
import X.AnonymousClass2FL;
import X.AnonymousClass2FR;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16490p7;
import X.C17050qB;
import X.C18350sJ;
import X.C18470sV;
import X.C19890uq;
import X.C20650w6;
import X.C20670w8;
import X.C20740wF;
import X.C20820wN;
import X.C20850wQ;
import X.C26041Bu;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* loaded from: classes2.dex */
public class CallContactLandingActivity extends ProfileActivity {
    public AnonymousClass19Z A00;
    public boolean A01;

    public CallContactLandingActivity() {
        this(0);
    }

    public CallContactLandingActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 9);
    }

    @Override // X.AnonymousClass2FY, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC28171Kz) this).A05 = (C20650w6) A1M.A3A.get();
            ((AbstractActivityC28171Kz) this).A09 = (C18470sV) A1M.AK8.get();
            ((AbstractActivityC28171Kz) this).A02 = (C20670w8) A1M.AMw.get();
            ((AbstractActivityC28171Kz) this).A03 = C12960it.A0O(A1M);
            ((AbstractActivityC28171Kz) this).A0B = (C19890uq) A1M.ABw.get();
            ((AbstractActivityC28171Kz) this).A0A = C12980iv.A0e(A1M);
            ((AbstractActivityC28171Kz) this).A0E = (AbstractC15850o0) A1M.ANA.get();
            ((AbstractActivityC28171Kz) this).A04 = (C17050qB) A1M.ABL.get();
            ((AbstractActivityC28171Kz) this).A0C = (C20740wF) A1M.AIA.get();
            ((AbstractActivityC28171Kz) this).A0D = (C18350sJ) A1M.AHX.get();
            ((AbstractActivityC28171Kz) this).A06 = (C20820wN) A1M.ACG.get();
            ((AbstractActivityC28171Kz) this).A08 = (C26041Bu) A1M.ACL.get();
            ((AbstractActivityC28171Kz) this).A00 = (AnonymousClass2FR) A1L.A0P.get();
            ((AbstractActivityC28171Kz) this).A07 = (C20850wQ) A1M.ACI.get();
            ((ProfileActivity) this).A00 = C12970iu.A0S(A1M);
            ((ProfileActivity) this).A06 = C12960it.A0T(A1M);
            ((ProfileActivity) this).A05 = ActivityC13830kP.A1N(A1M);
            ((ProfileActivity) this).A04 = (WhatsAppLibLoader) A1M.ANa.get();
            ((ProfileActivity) this).A02 = (AnonymousClass116) A1M.A3z.get();
            ((ProfileActivity) this).A03 = (C16490p7) A1M.ACJ.get();
            this.A00 = (AnonymousClass19Z) A1M.A2o.get();
        }
    }
}
