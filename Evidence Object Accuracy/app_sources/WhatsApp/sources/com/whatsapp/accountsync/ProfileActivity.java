package com.whatsapp.accountsync;

import X.AbstractActivityC28171Kz;
import X.AbstractC14440lR;
import X.AbstractC44141yJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass116;
import X.AnonymousClass12U;
import X.AnonymousClass2F9;
import X.AnonymousClass2FY;
import X.C14960mK;
import X.C15370n3;
import X.C15570nT;
import X.C16490p7;
import X.C36021jC;
import X.C48222Fb;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ProfileActivity extends AnonymousClass2FY {
    public C15570nT A00;
    public C48222Fb A01 = null;
    public AnonymousClass116 A02;
    public C16490p7 A03;
    public WhatsAppLibLoader A04;
    public AnonymousClass12U A05;
    public AbstractC14440lR A06;

    public final void A2h() {
        if (AJN()) {
            Log.w("sync profile activity already finishing, ignoring gotoActivity call");
        } else if (this.A02.A00()) {
            if (getIntent().getData() != null) {
                this.A00.A08();
                Cursor query = getContentResolver().query(getIntent().getData(), null, null, null, null);
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            String string = query.getString(query.getColumnIndexOrThrow("mimetype"));
                            UserJid nullable = UserJid.getNullable(query.getString(query.getColumnIndexOrThrow("data1")));
                            if (nullable != null) {
                                if (!(this instanceof CallContactLandingActivity)) {
                                    C15370n3 A0B = ((AbstractActivityC28171Kz) this).A03.A0B(nullable);
                                    if ("vnd.android.cursor.item/vnd.com.whatsapp.profile".equals(string)) {
                                        ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, A0B));
                                        finish();
                                        query.close();
                                        return;
                                    }
                                } else {
                                    CallContactLandingActivity callContactLandingActivity = (CallContactLandingActivity) this;
                                    C15370n3 A0B2 = ((AbstractActivityC28171Kz) callContactLandingActivity).A03.A0B(nullable);
                                    if ("vnd.android.cursor.item/vnd.com.whatsapp.voip.call".equals(string)) {
                                        callContactLandingActivity.A00.A01(callContactLandingActivity, A0B2, 14, false);
                                    } else if ("vnd.android.cursor.item/vnd.com.whatsapp.video.call".equals(string)) {
                                        callContactLandingActivity.A00.A01(callContactLandingActivity, A0B2, 14, true);
                                    }
                                    finish();
                                    query.close();
                                    return;
                                }
                            }
                        }
                        query.close();
                    } catch (Throwable th) {
                        try {
                            query.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            StringBuilder sb = new StringBuilder("failed to go anywhere from sync profile activity; intent=");
            sb.append(getIntent());
            Log.e(sb.toString());
            finish();
        } else if (!isFinishing()) {
            startActivityForResult(RequestPermissionActivity.A02(this, R.string.permission_contacts_access_request, R.string.permission_contacts_needed, true), 150);
        }
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 150) {
            if (i2 == -1) {
                A2h();
            } else {
                Log.w("profileactivity/contact access denied");
                finish();
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!this.A04.A03()) {
            Log.i("aborting due to native libraries missing");
        } else {
            C15570nT r0 = this.A00;
            r0.A08();
            if (r0.A00 == null || !((ActivityC13790kL) this).A0B.A01()) {
                ((ActivityC13810kN) this).A05.A07(R.string.finish_registration_first, 1);
            } else {
                C16490p7 r02 = this.A03;
                r02.A04();
                if (!r02.A01) {
                    AnonymousClass2F9 r03 = ((AbstractActivityC28171Kz) this).A01;
                    if (((AbstractC44141yJ) r03).A03.A03(r03.A06)) {
                        int A04 = ((ActivityC13790kL) this).A07.A04();
                        StringBuilder sb = new StringBuilder("profileactivity/create/backupfilesfound ");
                        sb.append(A04);
                        Log.i(sb.toString());
                        if (A04 > 0) {
                            C36021jC.A01(this, 105);
                            return;
                        } else {
                            A2g(false);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    A2e();
                    return;
                }
            }
        }
        finish();
    }
}
