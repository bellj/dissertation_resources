package com.whatsapp.accountsync;

import X.ActivityC13830kP;
import X.ActivityC58572qz;
import X.AnonymousClass01J;
import X.C12970iu;
import X.C12990iw;
import X.C14900mE;
import X.C15570nT;
import X.C623536u;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.Main;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class LoginActivity extends ActivityC58572qz {
    public C14900mE A00;
    public C15570nT A01;
    public boolean A02;

    public LoginActivity() {
        this(0);
    }

    public LoginActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 10);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            this.A00 = C12970iu.A0R(A1M);
            this.A01 = C12970iu.A0S(A1M);
        }
    }

    @Override // X.ActivityC58572qz, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.app_name);
        setContentView(R.layout.login);
        boolean z = false;
        for (Account account : AccountManager.get(this).getAccounts()) {
            if ("com.whatsapp".contains(account.type)) {
                z = true;
            }
        }
        if (z) {
            this.A00.A07(R.string.account_sync_acct_added, 1);
        } else {
            C15570nT r0 = this.A01;
            r0.A08();
            if (r0.A05 == null) {
                Intent A0D = C12990iw.A0D(this, Main.class);
                A0D.putExtra("show_registration_first_dlg", true);
                startActivity(A0D);
            } else {
                C12990iw.A1N(new C623536u(this, this), ((ActivityC13830kP) this).A05);
                return;
            }
        }
        finish();
    }
}
