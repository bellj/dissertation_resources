package com.whatsapp.accountsync;

import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass2YC;
import X.C12970iu;
import X.C71083cM;
import android.content.Intent;
import android.os.IBinder;

/* loaded from: classes2.dex */
public class AccountAuthenticatorService extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public static AnonymousClass2YC A03;
    public boolean A00;
    public final Object A01;
    public volatile C71083cM A02;

    public AccountAuthenticatorService() {
        this(0);
    }

    public AccountAuthenticatorService(int i) {
        this.A01 = C12970iu.A0l();
        this.A00 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    this.A02 = new C71083cM(this);
                }
            }
        }
        return this.A02.generatedComponent();
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        if (!"android.accounts.AccountAuthenticator".equals(intent.getAction())) {
            return null;
        }
        AnonymousClass2YC r0 = A03;
        if (r0 == null) {
            r0 = new AnonymousClass2YC(this);
            A03 = r0;
        }
        return r0.getIBinder();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A00) {
            this.A00 = true;
            generatedComponent();
        }
        super.onCreate();
    }
}
