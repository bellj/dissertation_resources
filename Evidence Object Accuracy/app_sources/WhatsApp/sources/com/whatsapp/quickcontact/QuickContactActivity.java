package com.whatsapp.quickcontact;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC33021d9;
import X.AbstractC454421p;
import X.AbstractC47972Dm;
import X.AbstractC51412Uo;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10S;
import X.AnonymousClass11A;
import X.AnonymousClass11F;
import X.AnonymousClass12F;
import X.AnonymousClass12G;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass14X;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass1A2;
import X.AnonymousClass1AR;
import X.AnonymousClass1IZ;
import X.AnonymousClass1J1;
import X.AnonymousClass1YS;
import X.AnonymousClass1YT;
import X.AnonymousClass27Q;
import X.AnonymousClass27S;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2TT;
import X.AnonymousClass37R;
import X.AnonymousClass422;
import X.AnonymousClass480;
import X.AnonymousClass5UJ;
import X.AnonymousClass5V9;
import X.C004802e;
import X.C103324qZ;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16030oK;
import X.C16120oU;
import X.C17900ra;
import X.C18470sV;
import X.C18640sm;
import X.C18750sx;
import X.C18810t5;
import X.C19990v2;
import X.C20710wC;
import X.C20730wE;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C236712o;
import X.C236812p;
import X.C244415n;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C254219i;
import X.C27131Gd;
import X.C33701ew;
import X.C48962Ip;
import X.C70913c3;
import X.C864547j;
import X.C92434Vw;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.quickcontact.QuickContactActivity;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.util.FloatingChildLayout;

/* loaded from: classes2.dex */
public class QuickContactActivity extends ActivityC13790kL implements AbstractC33021d9, AnonymousClass27Q {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public ImageView A07;
    public C48962Ip A08;
    public AnonymousClass1AR A09;
    public AnonymousClass130 A0A;
    public C15550nR A0B;
    public AnonymousClass10S A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public C21270x9 A0F;
    public AnonymousClass131 A0G;
    public C20730wE A0H;
    public C18750sx A0I;
    public C19990v2 A0J;
    public C15600nX A0K;
    public C236812p A0L;
    public C15370n3 A0M;
    public AnonymousClass11F A0N;
    public C16120oU A0O;
    public C20710wC A0P;
    public AnonymousClass11A A0Q;
    public C15580nU A0R;
    public C16030oK A0S;
    public C244415n A0T;
    public C17900ra A0U;
    public AnonymousClass1IZ A0V;
    public AnonymousClass1A2 A0W;
    public AnonymousClass14X A0X;
    public AbstractC51412Uo A0Y;
    public AnonymousClass12F A0Z;
    public StatusesViewModel A0a;
    public AnonymousClass198 A0b;
    public C254219i A0c;
    public FloatingChildLayout A0d;
    public AnonymousClass1YT A0e;
    public AnonymousClass12G A0f;
    public C21260x8 A0g;
    public AnonymousClass19Z A0h;
    public C864547j A0i;
    public AnonymousClass37R A0j;
    public AnonymousClass1YS A0k;
    public C21280xA A0l;
    public boolean A0m;
    public boolean A0n;
    public final C27131Gd A0o;
    public final AnonymousClass5UJ A0p;
    public final AbstractC47972Dm A0q;
    public final C236712o A0r;

    public QuickContactActivity() {
        this(0);
        this.A0o = new AnonymousClass422(this);
        this.A0p = new AnonymousClass5UJ() { // from class: X.57H
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r2) {
                AbstractC51412Uo r0 = QuickContactActivity.this.A0Y;
                r0.A00();
                r0.A03();
            }
        };
        this.A0q = new C70913c3(this);
        this.A0r = new AnonymousClass480(this);
    }

    public QuickContactActivity(int i) {
        this.A0n = false;
        A0R(new C103324qZ(this));
    }

    public static /* synthetic */ void A02(QuickContactActivity quickContactActivity) {
        if (Build.VERSION.SDK_INT >= 21) {
            quickContactActivity.getWindow().setStatusBarColor(0);
        }
        quickContactActivity.finish();
        quickContactActivity.overridePendingTransition(0, 0);
    }

    public static /* synthetic */ void A03(QuickContactActivity quickContactActivity) {
        quickContactActivity.A0d.invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            quickContactActivity.getWindow().setStatusBarColor(quickContactActivity.getIntent().getIntExtra("status_bar_color", AnonymousClass00T.A00(quickContactActivity, R.color.primary)));
        }
        FloatingChildLayout floatingChildLayout = quickContactActivity.A0d;
        floatingChildLayout.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass27S(floatingChildLayout, new RunnableBRunnable0Shape10S0100000_I0_10(quickContactActivity, 24)));
    }

    public static /* synthetic */ void A09(QuickContactActivity quickContactActivity) {
        AbstractC14640lm r4;
        C92434Vw A04;
        if (quickContactActivity.A0m) {
            return;
        }
        if (quickContactActivity.A0a == null || !((ActivityC13810kN) quickContactActivity).A0C.A07(1533) || (r4 = (AbstractC14640lm) quickContactActivity.A0M.A0B(AbstractC14640lm.class)) == null || (A04 = quickContactActivity.A0a.A04(UserJid.of(r4))) == null || !A04.A00()) {
            C15370n3 r2 = quickContactActivity.A0M;
            if (r2.A0X && r2.A0K()) {
                C15600nX r22 = quickContactActivity.A0K;
                C15580nU r1 = quickContactActivity.A0R;
                AnonymousClass009.A05(r1);
                if (!r22.A0C(r1)) {
                    quickContactActivity.Ado(R.string.failed_update_photo_not_authorized);
                    return;
                }
            }
            Jid jid = quickContactActivity.A0M.A0D;
            AnonymousClass009.A05(jid);
            if (!AbstractC454421p.A00) {
                quickContactActivity.startActivity(C14960mK.A0N(quickContactActivity.getApplicationContext(), jid, null, 0.0f, 0, 0, 0, 0, false));
            } else {
                String stringExtra = quickContactActivity.getIntent().getStringExtra("transition_name");
                if (stringExtra == null) {
                    stringExtra = new AnonymousClass2TT(quickContactActivity).A00(R.string.transition_photo);
                }
                boolean z = false;
                if (Build.VERSION.SDK_INT >= 24) {
                    z = true;
                }
                int intExtra = quickContactActivity.getIntent().getIntExtra("status_bar_color", AnonymousClass00T.A00(quickContactActivity, R.color.primary));
                int intExtra2 = quickContactActivity.getIntent().getIntExtra("navigation_bar_color", AnonymousClass00T.A00(quickContactActivity, R.color.lightNavigationBarBackgroundColor));
                AnonymousClass028.A0k(quickContactActivity.A07, stringExtra);
                Context applicationContext = quickContactActivity.getApplicationContext();
                float f = 0.0f;
                if (z) {
                    f = 0.5f;
                }
                quickContactActivity.startActivity(C14960mK.A0N(applicationContext, jid, stringExtra, f, quickContactActivity.getWindow().getStatusBarColor(), intExtra, quickContactActivity.getWindow().getNavigationBarColor(), intExtra2, false), AbstractC454421p.A05(quickContactActivity, quickContactActivity.A07, stringExtra));
                if (!z) {
                    new Handler(Looper.getMainLooper()).postDelayed(new RunnableBRunnable0Shape10S0100000_I0_10(quickContactActivity, 25), (long) quickContactActivity.getResources().getInteger(17694721));
                    return;
                }
            }
            quickContactActivity.A2g(false);
            return;
        }
        quickContactActivity.A0a.A08(r4, 7, null);
        quickContactActivity.startActivity(C14960mK.A0F(quickContactActivity, r4, Boolean.TRUE));
        quickContactActivity.A2g(false);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0n) {
            this.A0n = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0J = (C19990v2) r1.A3M.get();
            this.A0O = (C16120oU) r1.ANE.get();
            this.A09 = (AnonymousClass1AR) r1.ALM.get();
            this.A0h = (AnonymousClass19Z) r1.A2o.get();
            this.A0g = (C21260x8) r1.A2k.get();
            this.A0F = (C21270x9) r1.A4A.get();
            this.A0T = (C244415n) r1.AAg.get();
            this.A0X = (AnonymousClass14X) r1.AFM.get();
            this.A0l = (C21280xA) r1.AMU.get();
            this.A0A = (AnonymousClass130) r1.A41.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A0f = (AnonymousClass12G) r1.A2h.get();
            this.A0C = (AnonymousClass10S) r1.A46.get();
            this.A0P = (C20710wC) r1.A8m.get();
            this.A0Z = (AnonymousClass12F) r1.AJM.get();
            this.A0b = (AnonymousClass198) r1.A0J.get();
            this.A0I = (C18750sx) r1.A2p.get();
            this.A0c = (C254219i) r1.A0K.get();
            this.A0H = (C20730wE) r1.A4J.get();
            this.A0N = (AnonymousClass11F) r1.AE3.get();
            this.A0L = (C236812p) r1.AAC.get();
            this.A0S = (C16030oK) r1.AAd.get();
            this.A0U = (C17900ra) r1.AF5.get();
            this.A0K = (C15600nX) r1.A8x.get();
            this.A0Q = (AnonymousClass11A) r1.A8o.get();
            this.A0G = (AnonymousClass131) r1.A49.get();
            this.A08 = (C48962Ip) r2.A00.get();
            this.A0W = (AnonymousClass1A2) r1.AET.get();
        }
    }

    public final void A2e() {
        AnonymousClass1YS r0 = this.A0k;
        if (r0 != null) {
            AnonymousClass1YT A01 = this.A0I.A01(r0.A00);
            if (A01 != null) {
                this.A0e = A01;
                return;
            }
            C864547j r2 = new C864547j(this.A0I, new AnonymousClass5V9() { // from class: X.5Az
                @Override // X.AnonymousClass5V9
                public final void ANW(AnonymousClass1YT r22) {
                    QuickContactActivity.this.A0e = r22;
                }
            }, this.A0k.A00);
            this.A0i = r2;
            ((ActivityC13830kP) this).A05.Ab5(r2, new Void[0]);
        }
    }

    public final void A2f() {
        C15550nR r2 = this.A0B;
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        C15370n3 A0B = r2.A0B(A01);
        this.A0M = A0B;
        this.A0R = (C15580nU) A0B.A0B(C15580nU.class);
    }

    public final void A2g(boolean z) {
        if (z) {
            FloatingChildLayout floatingChildLayout = this.A0d;
            if (floatingChildLayout.A02 == 1) {
                floatingChildLayout.A02 = 3;
                if (floatingChildLayout.A06.isRunning()) {
                    floatingChildLayout.A06.reverse();
                } else {
                    ValueAnimator ofInt = ValueAnimator.ofInt(127, 0);
                    floatingChildLayout.A06 = ofInt;
                    ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eZ
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                            FloatingChildLayout.this.setBackgroundColor(C12960it.A05(valueAnimator.getAnimatedValue()) << 24);
                        }
                    });
                    floatingChildLayout.A06.setDuration((long) floatingChildLayout.A01).start();
                }
            }
            FloatingChildLayout floatingChildLayout2 = this.A0d;
            RunnableBRunnable0Shape10S0100000_I0_10 runnableBRunnable0Shape10S0100000_I0_10 = new RunnableBRunnable0Shape10S0100000_I0_10(this, 23);
            int i = floatingChildLayout2.A03;
            if (i == 1 || i == 2) {
                floatingChildLayout2.A03 = 3;
                floatingChildLayout2.A09.invalidate();
                floatingChildLayout2.A00(runnableBRunnable0Shape10S0100000_I0_10, true);
                return;
            }
        }
        finish();
        overridePendingTransition(0, 0);
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r2) {
        this.A0a.A09(r2);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 1) {
            this.A0H.A06();
            this.A0b.A00();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2g(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01df, code lost:
        if (r6.A0X.A0a(r6, (com.whatsapp.jid.UserJid) r6.A0M.A0B(com.whatsapp.jid.UserJid.class), r7) == false) goto L_0x01e1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0361  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r22) {
        /*
        // Method dump skipped, instructions count: 1353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.quickcontact.QuickContactActivity.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 1) {
            return super.onCreateDialog(i);
        }
        C004802e r2 = new C004802e(this);
        r2.A06(R.string.activity_not_found);
        r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fy
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                C36021jC.A00(QuickContactActivity.this, 1);
            }
        });
        return r2.create();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0C.A04(this.A0o);
        this.A0E.A00();
        if (this.A0M.A0K()) {
            AnonymousClass11A r0 = this.A0Q;
            r0.A00.remove(this.A0p);
        }
        this.A0f.A04(this.A0q);
        this.A0g.A04(this.A0r);
        AnonymousClass37R r02 = this.A0j;
        if (r02 != null) {
            r02.A03(true);
            this.A0j = null;
        }
        C864547j r03 = this.A0i;
        if (r03 != null) {
            r03.A03(true);
            this.A0i = null;
        }
    }
}
