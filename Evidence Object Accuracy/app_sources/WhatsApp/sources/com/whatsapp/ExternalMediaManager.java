package com.whatsapp;

import X.AbstractC14440lR;
import X.AbstractServiceC003701q;
import X.AbstractServiceC003801r;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass5B7;
import X.C12960it;
import X.C12970iu;
import X.C17050qB;
import X.C21890y9;
import X.C22730zY;
import X.C32861cr;
import X.C58182oH;
import X.C71083cM;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ExternalMediaManager extends AbstractServiceC003701q implements AnonymousClass004 {
    public C22730zY A00;
    public C17050qB A01;
    public C21890y9 A02;
    public AbstractC14440lR A03;
    public boolean A04;
    public final Object A05;
    public volatile C71083cM A06;

    /* loaded from: classes2.dex */
    public class ExternalMediaStateReceiver extends BroadcastReceiver {
        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            if ("android.intent.action.MEDIA_BAD_REMOVAL".equals(intent.getAction()) || "android.intent.action.MEDIA_EJECT".equals(intent.getAction()) || "android.intent.action.MEDIA_MOUNTED".equals(intent.getAction()) || "android.intent.action.MEDIA_REMOVED".equals(intent.getAction()) || "android.intent.action.MEDIA_SHARED".equals(intent.getAction()) || "android.intent.action.MEDIA_UNMOUNTED".equals(intent.getAction())) {
                AbstractServiceC003801r.A00(context, intent.setClass(context, ExternalMediaManager.class), ExternalMediaManager.class, 5);
            }
        }
    }

    public ExternalMediaManager() {
        this(0);
    }

    public ExternalMediaManager(int i) {
        this.A05 = C12970iu.A0l();
        this.A04 = false;
    }

    @Override // X.AbstractServiceC003801r
    public void A05(Intent intent) {
        String str;
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            boolean equals = externalStorageState.equals("mounted_ro");
            C32861cr r1 = (C32861cr) this.A01.A05.get();
            boolean z = r1.A00;
            if (!equals) {
                if (!z) {
                    r1.A00 = true;
                    r1.A01 = true;
                    Log.i(C12960it.A0d(Environment.getExternalStorageState(), C12960it.A0k("media-state-manager/external/unavailable ")));
                }
            } else if (z || !r1.A01) {
                r1.A00 = false;
                r1.A01 = true;
                str = "media-state-manager/read-only";
                Log.i(str);
                this.A02.A01(true, false);
            }
        } else {
            C32861cr r12 = (C32861cr) this.A01.A05.get();
            if (r12.A00 || r12.A01) {
                r12.A00 = false;
                r12.A01 = false;
                str = "media-state-manager/external/available";
                Log.i(str);
                this.A02.A01(true, false);
            }
        }
        this.A00.A08(externalStorageState);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A06 == null) {
            synchronized (this.A05) {
                if (this.A06 == null) {
                    this.A06 = new C71083cM(this);
                }
            }
        }
        return this.A06.generatedComponent();
    }

    @Override // X.AbstractServiceC003801r, android.app.Service
    public void onCreate() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A03 = C12960it.A0T(r1);
            this.A01 = (C17050qB) r1.ABL.get();
            this.A02 = (C21890y9) r1.ABM.get();
            this.A00 = (C22730zY) r1.A8Y.get();
        }
        super.onCreate();
    }
}
