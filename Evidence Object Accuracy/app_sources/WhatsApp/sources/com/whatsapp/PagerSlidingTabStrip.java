package com.whatsapp;

import X.AbstractC14800m4;
import X.AnonymousClass01A;
import X.AnonymousClass028;
import X.AnonymousClass070;
import X.AnonymousClass2M0;
import X.AnonymousClass2QM;
import X.AnonymousClass2QN;
import X.AnonymousClass2QO;
import X.AnonymousClass2QQ;
import X.AnonymousClass2QS;
import X.AnonymousClass2QT;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import java.util.Locale;

/* loaded from: classes2.dex */
public class PagerSlidingTabStrip extends AnonymousClass2M0 {
    public static final int[] A0R = {16842901, 16842904};
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public Paint A0H;
    public Paint A0I;
    public LinearLayout.LayoutParams A0J;
    public LinearLayout.LayoutParams A0K;
    public LinearLayout A0L;
    public AnonymousClass070 A0M;
    public ViewPager A0N;
    public Locale A0O;
    public boolean A0P;
    public final AnonymousClass2QM A0Q;

    public PagerSlidingTabStrip(Context context) {
        this(context, null);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        LinearLayout r0;
        this.A0Q = new AnonymousClass2QM(this);
        this.A01 = 0;
        this.A00 = 0.0f;
        this.A05 = -10066330;
        this.A0F = 436207616;
        this.A02 = 436207616;
        this.A0P = true;
        this.A08 = 52;
        this.A06 = 3;
        this.A0G = 1;
        this.A03 = 12;
        this.A0B = 8;
        this.A04 = 1;
        this.A0D = 12;
        this.A0C = -10066330;
        this.A0E = 1;
        this.A07 = 0;
        this.A09 = R.drawable.background_tab;
        setFillViewport(true);
        setWillNotDraw(false);
        if (!(this instanceof HomePagerSlidingTabStrip)) {
            r0 = new LinearLayout(context);
        } else {
            r0 = new AnonymousClass2QO(context);
        }
        this.A0L = r0;
        r0.setOrientation(0);
        this.A0L.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(this.A0L);
        AnonymousClass028.A0a(this.A0L, 2);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.A08 = (int) TypedValue.applyDimension(1, (float) this.A08, displayMetrics);
        this.A06 = (int) TypedValue.applyDimension(1, (float) this.A06, displayMetrics);
        this.A0G = (int) TypedValue.applyDimension(1, (float) this.A0G, displayMetrics);
        this.A03 = (int) TypedValue.applyDimension(1, (float) this.A03, displayMetrics);
        this.A0B = (int) TypedValue.applyDimension(1, (float) this.A0B, displayMetrics);
        this.A04 = (int) TypedValue.applyDimension(1, (float) this.A04, displayMetrics);
        this.A0D = (int) TypedValue.applyDimension(2, (float) this.A0D, displayMetrics);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, A0R);
        this.A0D = obtainStyledAttributes.getDimensionPixelSize(0, this.A0D);
        this.A0C = obtainStyledAttributes.getColor(1, this.A0C);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0E);
        this.A05 = obtainStyledAttributes2.getColor(1, this.A05);
        this.A02 = obtainStyledAttributes2.getColor(0, this.A02);
        this.A0B = obtainStyledAttributes2.getDimensionPixelSize(2, this.A0B);
        obtainStyledAttributes2.recycle();
        Paint paint = new Paint();
        this.A0I = paint;
        paint.setAntiAlias(true);
        this.A0I.setStyle(Paint.Style.FILL);
        Paint paint2 = new Paint();
        this.A0H = paint2;
        paint2.setAntiAlias(true);
        this.A0H.setStrokeWidth((float) this.A04);
        this.A0J = new LinearLayout.LayoutParams(-2, -1);
        this.A0K = new LinearLayout.LayoutParams(0, -1, 1.0f);
        if (this.A0O == null) {
            this.A0O = getResources().getConfiguration().locale;
        }
    }

    public static /* synthetic */ void A00(PagerSlidingTabStrip pagerSlidingTabStrip, int i, int i2) {
        if (pagerSlidingTabStrip.A0A != 0) {
            int left = pagerSlidingTabStrip.A0L.getChildAt(i).getLeft() + i2;
            if (i > 0 || i2 > 0) {
                left -= pagerSlidingTabStrip.A08;
            }
            if (left != pagerSlidingTabStrip.A07) {
                pagerSlidingTabStrip.A07 = left;
                pagerSlidingTabStrip.scrollTo(left, 0);
            }
        }
    }

    public final void A01() {
        for (int i = 0; i < this.A0A; i++) {
            View childAt = this.A0L.getChildAt(i);
            childAt.setBackgroundResource(this.A09);
            if (childAt instanceof TextView) {
                TextView textView = (TextView) childAt;
                textView.setTextSize(0, (float) this.A0D);
                textView.setTypeface(null, this.A0E);
                textView.setTextColor(this.A0C);
                textView.setAllCaps(true);
            }
        }
    }

    public void A02(View view, int i) {
        LinearLayout.LayoutParams layoutParams;
        view.setFocusable(true);
        view.setOnClickListener(new ViewOnClickCListenerShape0S0101000_I0(this, i, 1));
        int i2 = this.A0B;
        view.setPadding(i2, 0, i2, 0);
        AnonymousClass028.A0g(view, new AnonymousClass2QQ(this));
        LinearLayout linearLayout = this.A0L;
        if (this.A0P) {
            layoutParams = this.A0K;
        } else {
            layoutParams = this.A0J;
        }
        linearLayout.addView(view, i, layoutParams);
    }

    public int getTextColor() {
        return this.A0C;
    }

    public int getTextSize() {
        return this.A0D;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        super.onDraw(canvas);
        if (!(isInEditMode() || this.A0A == 0)) {
            int height = getHeight();
            Paint paint = this.A0I;
            paint.setColor(this.A05);
            LinearLayout linearLayout = this.A0L;
            View childAt = linearLayout.getChildAt(this.A01);
            float left = (float) childAt.getLeft();
            float right = (float) childAt.getRight();
            if (this.A00 > 0.0f && (i = this.A01) < this.A0A - 1) {
                View childAt2 = linearLayout.getChildAt(i + 1);
                float left2 = (float) childAt2.getLeft();
                float right2 = (float) childAt2.getRight();
                float f = this.A00;
                float f2 = 1.0f - f;
                left = (left2 * f) + (f2 * left);
                right = (right2 * f) + (f2 * right);
            }
            float f3 = (float) height;
            canvas.drawRect(left, (float) (height - this.A06), right, f3, paint);
            paint.setColor(this.A0F);
            canvas.drawRect(0.0f, (float) (height - this.A0G), (float) linearLayout.getWidth(), f3, paint);
            Paint paint2 = this.A0H;
            paint2.setColor(this.A02);
            for (int i2 = 0; i2 < this.A0A - 1; i2++) {
                View childAt3 = linearLayout.getChildAt(i2);
                int i3 = this.A03;
                canvas.drawLine((float) childAt3.getRight(), (float) i3, (float) childAt3.getRight(), (float) (height - i3), paint2);
            }
        }
    }

    @Override // android.widget.HorizontalScrollView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        AnonymousClass2QS r2 = (AnonymousClass2QS) parcelable;
        super.onRestoreInstanceState(r2.getSuperState());
        this.A01 = r2.A00;
        requestLayout();
    }

    @Override // android.widget.HorizontalScrollView, android.view.View
    public Parcelable onSaveInstanceState() {
        AnonymousClass2QS r1 = new AnonymousClass2QS(super.onSaveInstanceState());
        r1.A00 = this.A01;
        return r1;
    }

    public void setOnPageChangeListener(AnonymousClass070 r1) {
        this.A0M = r1;
    }

    public void setShouldExpand(boolean z) {
        this.A0P = z;
        requestLayout();
    }

    public void setTextColor(int i) {
        this.A0C = i;
        A01();
    }

    public void setTextSize(int i) {
        this.A0D = i;
        A01();
    }

    public void setViewPager(ViewPager viewPager) {
        this.A0N = viewPager;
        if (viewPager.A0V != null) {
            viewPager.A0W = this.A0Q;
            this.A0L.removeAllViews();
            this.A0A = this.A0N.A0V.A01();
            for (int i = 0; i < this.A0A; i++) {
                AnonymousClass01A r1 = this.A0N.A0V;
                if (r1 instanceof AbstractC14800m4) {
                    A02(((AbstractC14800m4) r1).AEv(i), i);
                } else {
                    String charSequence = r1.A04(i).toString();
                    TextView textView = new TextView(getContext());
                    textView.setText(charSequence);
                    textView.setGravity(17);
                    textView.setSingleLine();
                    A02(textView, i);
                }
            }
            A01();
            getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass2QT(this));
            return;
        }
        throw new IllegalStateException("ViewPager does not have adapter instance.");
    }
}
