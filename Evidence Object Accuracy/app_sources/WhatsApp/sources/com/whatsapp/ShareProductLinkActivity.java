package com.whatsapp;

import X.AbstractActivityC42891w4;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass19Q;
import X.AnonymousClass2FL;
import X.AnonymousClass2V9;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C248917h;
import X.C27631Ih;
import X.C83663xe;
import X.C83673xf;
import X.C83683xg;
import android.os.Bundle;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ShareProductLinkActivity extends AbstractActivityC42891w4 {
    public AnonymousClass19Q A00;
    public boolean A01;

    public ShareProductLinkActivity() {
        this(0);
    }

    public ShareProductLinkActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 3);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = C12980iv.A0Z(A1M);
        }
    }

    @Override // X.AbstractActivityC42891w4, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        A2i();
        UserJid A0V = ActivityC13790kL.A0V(getIntent(), "jid");
        if (!(A0V instanceof C27631Ih)) {
            Log.e("share-product-link-activity/invalid-jid");
            finish();
            return;
        }
        String stringExtra = getIntent().getStringExtra("product_id");
        AnonymousClass009.A05(stringExtra);
        String format = String.format("%s/p/%s/%s", "https://wa.me", stringExtra, C248917h.A03(A0V));
        setTitle(R.string.product_share_title);
        TextView textView = ((AbstractActivityC42891w4) this).A01;
        if (textView != null) {
            textView.setText(format);
        }
        C12970iu.A0M(this, R.id.share_link_description).setText(R.string.product_share_description);
        if (((ActivityC13790kL) this).A01.A0F(A0V)) {
            str = C12960it.A0X(this, format, new Object[1], 0, R.string.product_share_text_template);
        } else {
            str = format;
        }
        C83673xf A2h = A2h();
        A2h.A00 = str;
        A2h.A01 = new RunnableBRunnable0Shape1S1200000_I1(A0V, this, stringExtra, 4);
        C83663xe A2f = A2f();
        A2f.A00 = format;
        A2f.A01 = new RunnableBRunnable0Shape1S1200000_I1(A0V, this, stringExtra, 5);
        C83683xg A2g = A2g();
        A2g.A02 = str;
        A2g.A00 = getString(R.string.share);
        A2g.A01 = getString(R.string.product_share_email_subject);
        ((AnonymousClass2V9) A2g).A01 = new RunnableBRunnable0Shape1S1200000_I1(A0V, this, stringExtra, 3);
    }
}
