package com.whatsapp.devicetype;

import X.C14820m6;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.os.Build;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DeviceTypeModule {
    public static Boolean A00(Application application, C14820m6 r4) {
        Context context = application;
        if (Build.VERSION.SDK_INT >= 17) {
            context = application.createDisplayContext(((DisplayManager) application.getSystemService("display")).getDisplay(0));
        }
        boolean z = context.getResources().getBoolean(R.bool.is_tablet);
        if (z) {
            SharedPreferences sharedPreferences = r4.A00;
            if (!sharedPreferences.getBoolean("detect_device_tablet", false)) {
                sharedPreferences.edit().putBoolean("detect_device_tablet", true).apply();
            }
        }
        return Boolean.valueOf(z);
    }
}
