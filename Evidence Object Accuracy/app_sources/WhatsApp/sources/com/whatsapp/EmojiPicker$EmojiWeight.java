package com.whatsapp;

import X.AbstractC38871oq;
import java.io.Serializable;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class EmojiPicker$EmojiWeight implements AbstractC38871oq, Serializable {
    public static final long serialVersionUID = 1;
    @Deprecated
    public int code;
    public int[] emoji;
    @Deprecated
    public int modifier;
    public float weight;

    public EmojiPicker$EmojiWeight(int[] iArr, float f) {
        this.emoji = iArr;
        this.weight = f;
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ boolean A7R(Object obj) {
        return Arrays.equals(this.emoji, (int[]) obj);
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ Object ADD() {
        return this.emoji;
    }

    @Override // X.AbstractC38871oq
    public float AHi() {
        return this.weight;
    }

    @Override // X.AbstractC38871oq
    public void AdB(float f) {
        this.weight = f;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            EmojiPicker$EmojiWeight emojiPicker$EmojiWeight = (EmojiPicker$EmojiWeight) obj;
            if (Float.compare(emojiPicker$EmojiWeight.weight, this.weight) != 0 || !Arrays.equals(this.emoji, emojiPicker$EmojiWeight.emoji)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (Float.valueOf(this.weight).hashCode() * 31 * 31) + Arrays.hashCode(this.emoji);
    }
}
