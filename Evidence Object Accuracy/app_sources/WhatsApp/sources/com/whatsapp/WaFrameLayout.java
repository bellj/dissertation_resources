package com.whatsapp;

import X.AbstractC53062cR;
import X.AnonymousClass00T;
import X.AnonymousClass2GZ;
import X.C015607k;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaFrameLayout extends AbstractC53062cR {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public boolean A04;

    public WaFrameLayout(Context context) {
        super(context);
        AbstractC53062cR.A00(this);
    }

    public WaFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        AbstractC53062cR.A00(this);
        A02(context, attributeSet);
    }

    public WaFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        AbstractC53062cR.A00(this);
        A02(context, attributeSet);
    }

    public WaFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        AbstractC53062cR.A00(this);
    }

    public final void A02(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            this.A04 = isPressed();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0R);
            this.A01 = obtainStyledAttributes.getResourceId(1, 0);
            this.A00 = obtainStyledAttributes.getResourceId(0, 0);
            if (!(getBackground() == null || this.A01 == 0)) {
                setBackgroundDrawable(getBackground());
            }
            this.A03 = obtainStyledAttributes.getResourceId(3, 0);
            this.A02 = obtainStyledAttributes.getResourceId(2, 0);
            if (!(getForeground() == null || this.A03 == 0)) {
                setForeground(getForeground());
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (isPressed() != this.A04) {
            this.A04 = isPressed();
            setBackgroundDrawable(getBackground());
            setForeground(getForeground());
        }
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        int i;
        if (!(this.A01 == 0 || drawable == null)) {
            drawable = C015607k.A03(drawable);
            if (!isPressed() || (i = this.A00) == 0) {
                i = this.A01;
            }
            C015607k.A0A(drawable, AnonymousClass00T.A00(getContext(), i));
        }
        super.setBackgroundDrawable(drawable);
    }

    @Override // android.view.View
    public void setForeground(Drawable drawable) {
        int i;
        if (!(this.A03 == 0 || drawable == null)) {
            drawable = C015607k.A03(drawable);
            if (!isPressed() || (i = this.A02) == 0) {
                i = this.A03;
            }
            C015607k.A0A(drawable, AnonymousClass00T.A00(getContext(), i));
        }
        super.setForeground(drawable);
    }
}
