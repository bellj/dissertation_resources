package com.whatsapp;

import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass0OC;
import X.AnonymousClass12P;
import X.AnonymousClass18U;
import X.AnonymousClass1AO;
import X.AnonymousClass4VJ;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15550nR;
import X.C20730wE;
import X.C254219i;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class PhoneHyperLinkDialogFragment extends Hilt_PhoneHyperLinkDialogFragment {
    public AnonymousClass12P A00;
    public AnonymousClass18U A01;
    public C15550nR A02;
    public C20730wE A03;
    public AnonymousClass1AO A04;
    public AnonymousClass018 A05;
    public UserJid A06;
    public C254219i A07;
    public String A08;
    public String A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;

    public static PhoneHyperLinkDialogFragment A00(UserJid userJid, String str, String str2, boolean z, boolean z2, boolean z3) {
        PhoneHyperLinkDialogFragment phoneHyperLinkDialogFragment = new PhoneHyperLinkDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("isSyncFailure", z);
        A0D.putBoolean("isWAAccount", z2);
        A0D.putBoolean("isPhoneNumberOwner", z3);
        A0D.putString("phoneNumber", str);
        A0D.putParcelable("jid", userJid);
        A0D.putString("url", str2);
        phoneHyperLinkDialogFragment.A0U(A0D);
        return phoneHyperLinkDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i == 1000) {
            this.A03.A06();
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        this.A0B = A03.getBoolean("isSyncFailure");
        this.A0C = A03.getBoolean("isWAAccount");
        this.A0A = A03.getBoolean("isPhoneNumberOwner");
        AnonymousClass018 r1 = this.A05;
        String string = A03.getString("phoneNumber");
        AnonymousClass009.A05(string);
        this.A08 = r1.A0G(string);
        this.A06 = (UserJid) A03.getParcelable("jid");
        String string2 = A03.getString("url");
        AnonymousClass009.A05(string2);
        this.A09 = string2;
        C004802e A0K = C12960it.A0K(this);
        TextView textView = (TextView) C12980iv.A0O(A04(), R.layout.phone_hyperlink_dialog_title);
        if (!this.A0B) {
            boolean z = this.A0C;
            int i = R.string.phone_number_is_not_on_whatsapp;
            if (z) {
                i = R.string.phone_number_is_on_whatsapp;
            }
            textView.setText(i);
            A0K.A01.A0B = textView;
        }
        ArrayList A0l = C12960it.A0l();
        if (this.A0C) {
            A0l.add(new AnonymousClass4VJ(C12970iu.A0q(this, this.A08, new Object[1], 0, R.string.chat_with), 1));
        }
        A0l.add(new AnonymousClass4VJ(C12970iu.A0q(this, this.A08, new Object[1], 0, R.string.dial), 2));
        A0l.add(new AnonymousClass4VJ(A0I(R.string.add_to_contacts), 3));
        ArrayAdapter arrayAdapter = new ArrayAdapter(A01(), (int) R.layout.phone_hyperlink_dialog_list_item, A0l);
        IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I1 = new IDxCListenerShape3S0200000_1_I1(A0l, 0, this);
        AnonymousClass0OC r0 = A0K.A01;
        r0.A0D = arrayAdapter;
        r0.A05 = iDxCListenerShape3S0200000_1_I1;
        return A0K.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        this.A04.A00(Boolean.valueOf(this.A0A), Boolean.valueOf(this.A0C), 8);
    }
}
