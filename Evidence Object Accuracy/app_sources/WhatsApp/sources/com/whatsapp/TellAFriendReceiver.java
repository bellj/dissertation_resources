package com.whatsapp;

import X.AnonymousClass01J;
import X.AnonymousClass1US;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C16120oU;
import X.C21240x6;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/* loaded from: classes2.dex */
public class TellAFriendReceiver extends BroadcastReceiver {
    public C21240x6 A00;
    public C16120oU A01;
    public final Object A02;
    public volatile boolean A03;

    public TellAFriendReceiver() {
        this(0);
    }

    public TellAFriendReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        ComponentName componentName;
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = C12970iu.A0b(r1);
                    this.A00 = (C21240x6) r1.AA7.get();
                    this.A03 = true;
                }
            }
        }
        if (Build.VERSION.SDK_INT >= 22 && (componentName = (ComponentName) intent.getParcelableExtra("android.intent.extra.CHOSEN_COMPONENT")) != null) {
            String packageName = componentName.getPackageName();
            if (!AnonymousClass1US.A0C(packageName)) {
                this.A00.A01(Integer.valueOf(intent.getIntExtra("extra_invite_source", 0)), packageName, 2);
            }
        }
    }
}
