package com.whatsapp.net.tls13;

import X.AnonymousClass3JS;
import java.io.Serializable;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class WtCachedPsk implements Serializable {
    public static final long serialVersionUID = 84430101;
    public final byte certsID;
    public final String cipher;
    public final long maxEarlyDataSize;
    public final byte[] pskVal;
    public final String sni;
    public final byte[] ticket;
    public final long ticketAgeAdd;
    public final long ticketIssuedTime;
    public final long ticketLifetime;
    public boolean useTestTime = false;

    public WtCachedPsk(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte b, long j) {
        this.pskVal = bArr;
        this.cipher = "TLS_AES_128_GCM_SHA256";
        this.maxEarlyDataSize = j;
        this.ticketAgeAdd = AnonymousClass3JS.A02(bArr2);
        long A02 = AnonymousClass3JS.A02(bArr3);
        this.ticketLifetime = (A02 > 172800 ? 172800 : A02) * 1000;
        this.ticket = bArr4;
        this.ticketIssuedTime = System.currentTimeMillis();
        this.sni = str;
        this.certsID = b;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.ticket, ((WtCachedPsk) obj).ticket);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(this.ticket);
    }
}
