package com.whatsapp;

import X.AnonymousClass009;
import X.AnonymousClass2p9;
import X.AnonymousClass3J7;
import X.AnonymousClass4RL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.RollingCounterView;

/* loaded from: classes2.dex */
public class RollingCounterView extends FrameLayout {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public AnonymousClass4RL A05;
    public AnonymousClass4RL A06;
    public TextEmojiLabel A07;
    public TextEmojiLabel A08;
    public final ValueAnimator A09 = ValueAnimator.ofFloat(0.0f, 1.0f);
    public final ValueAnimator A0A = ValueAnimator.ofFloat(0.0f, 1.0f);
    public final Rect A0B = C12980iv.A0J();

    public RollingCounterView(Context context) {
        super(context);
        A00();
    }

    public RollingCounterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public RollingCounterView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    private void A00() {
        this.A07 = new TextEmojiLabel(getContext());
        this.A08 = new TextEmojiLabel(getContext());
        super.addView(this.A07);
        super.addView(this.A08);
        C12990iw.A0w(this.A09, this, 2);
    }

    public final void A01() {
        String A02;
        AnonymousClass4RL r0 = this.A05;
        if (r0 == null) {
            AnonymousClass009.A0D(C12960it.A0U("finishedAnimationl called when currentAnimationInfo is null! This should never occur."));
            return;
        }
        int i = r0.A00;
        this.A00 = i;
        TextEmojiLabel textEmojiLabel = this.A07;
        if (!(this instanceof AnonymousClass2p9)) {
            A02 = Integer.toString(i);
        } else {
            AnonymousClass2p9 r2 = (AnonymousClass2p9) this;
            A02 = AnonymousClass3J7.A02(r2.getContext(), r2.A00, i);
        }
        textEmojiLabel.setText(A02);
        this.A07.requestLayout();
        this.A08.requestLayout();
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this);
        A0H.leftMargin = this.A01;
        A0H.rightMargin = this.A02;
        setLayoutParams(A0H);
        AnonymousClass4RL r1 = this.A06;
        if (r1 != null) {
            this.A06 = null;
            A02(r1);
            return;
        }
        this.A05 = null;
    }

    public final void A02(AnonymousClass4RL r9) {
        String A02;
        ValueAnimator valueAnimator;
        ValueAnimator valueAnimator2;
        this.A05 = r9;
        TextEmojiLabel textEmojiLabel = this.A08;
        int i = r9.A00;
        if (!(this instanceof AnonymousClass2p9)) {
            A02 = Integer.toString(i);
        } else {
            AnonymousClass2p9 r2 = (AnonymousClass2p9) this;
            A02 = AnonymousClass3J7.A02(r2.getContext(), r2.A00, i);
        }
        textEmojiLabel.setText(A02);
        C12970iu.A1F(this.A07);
        C12970iu.A1F(this.A08);
        long j = r9.A02;
        if (j == 0 && r9.A03 == 0) {
            int measuredWidth = this.A08.getMeasuredWidth();
            int measuredHeight = this.A08.getMeasuredHeight();
            int i2 = this.A01;
            int i3 = this.A02;
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this);
            A0H.width = measuredWidth;
            A0H.height = measuredHeight;
            A0H.leftMargin = i2;
            A0H.rightMargin = i3;
            this.A0B.set(0, 0, measuredWidth, measuredHeight);
            setLayoutParams(A0H);
            A01();
            return;
        }
        int measuredWidth2 = this.A08.getMeasuredWidth() - this.A07.getMeasuredWidth();
        AnimatorSet animatorSet = new AnimatorSet();
        if (measuredWidth2 != 0) {
            setupWidthAnimator(this.A08.getMeasuredWidth());
            Animator[] animatorArr = new Animator[2];
            if (measuredWidth2 > 0) {
                animatorArr[0] = this.A0A;
                valueAnimator2 = this.A09;
                valueAnimator = valueAnimator2;
            } else {
                valueAnimator = this.A09;
                animatorArr[0] = valueAnimator;
                valueAnimator2 = this.A0A;
            }
            animatorArr[1] = valueAnimator2;
            animatorSet.playSequentially(animatorArr);
        } else {
            valueAnimator = this.A09;
            animatorSet.play(valueAnimator);
        }
        this.A0A.setDuration(r9.A03);
        valueAnimator.setDuration(j);
        animatorSet.start();
    }

    @Override // android.view.ViewGroup
    @Deprecated
    public void addView(View view) {
        super.addView(view);
    }

    @Override // android.view.ViewGroup
    @Deprecated
    public void addView(View view, int i) {
        super.addView(view, i);
    }

    @Override // android.view.ViewGroup
    @Deprecated
    public void addView(View view, int i, int i2) {
        super.addView(view, i, i2);
    }

    @Override // android.view.ViewGroup
    @Deprecated
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    @Deprecated
    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, layoutParams);
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        float f;
        if (view == this.A07 || view == this.A08) {
            AnonymousClass4RL r0 = this.A05;
            if (r0 == null) {
                i = 0;
            } else {
                i = r0.A01;
            }
            ValueAnimator valueAnimator = this.A09;
            if (valueAnimator.isRunning()) {
                f = C12960it.A00(valueAnimator);
            } else {
                f = 0.0f;
            }
            if (view == this.A08) {
                i = -i;
                f = 1.0f - f;
            }
            int measuredHeight = getMeasuredHeight();
            Rect rect = this.A0B;
            if (rect.isEmpty()) {
                rect.set(0, 0, getMeasuredWidth(), measuredHeight);
            }
            canvas.save();
            canvas.clipRect(rect);
            canvas.translate(0.0f, ((float) (-measuredHeight)) * f * ((float) i));
            view.draw(canvas);
            canvas.restore();
            if (valueAnimator.isRunning()) {
                invalidate();
                return true;
            }
        } else {
            AnonymousClass009.A0D(C12970iu.A0f("drawChild given something other than primary/secondary textview"));
        }
        return false;
    }

    public String getPrimaryText() {
        if (TextUtils.isEmpty(this.A07.getText())) {
            return null;
        }
        return C12980iv.A0q(this.A07);
    }

    public void setAnimationInterpolator(TimeInterpolator timeInterpolator) {
        this.A09.setInterpolator(timeInterpolator);
        this.A0A.setInterpolator(timeInterpolator);
    }

    public void setTextColor(int i) {
        this.A07.setTextColor(i);
        this.A08.setTextColor(i);
    }

    private void setupWidthAnimator(int i) {
        int measuredWidth = this.A07.getMeasuredWidth();
        int measuredHeight = this.A07.getMeasuredHeight();
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this);
        int i2 = A0H.leftMargin;
        int i3 = A0H.rightMargin;
        ValueAnimator valueAnimator = this.A0A;
        valueAnimator.removeAllUpdateListeners();
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(i - measuredWidth, measuredWidth, measuredHeight, i2, i3) { // from class: X.3K2
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ int A02;
            public final /* synthetic */ int A03;
            public final /* synthetic */ int A04;

            {
                this.A00 = r2;
                this.A01 = r3;
                this.A02 = r4;
                this.A03 = r5;
                this.A04 = r6;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                RollingCounterView rollingCounterView = RollingCounterView.this;
                int i4 = this.A00;
                int i5 = this.A01;
                int i6 = this.A02;
                int i7 = this.A03;
                int i8 = this.A04;
                float A00 = C12960it.A00(valueAnimator2);
                int i9 = ((int) (((float) i4) * A00)) + i5;
                ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(rollingCounterView);
                A0H2.width = i9;
                A0H2.height = i6;
                A0H2.leftMargin = Math.round(((float) (rollingCounterView.A01 - i7)) * A00) + i7;
                A0H2.rightMargin = Math.round(A00 * ((float) (rollingCounterView.A02 - i8))) + i8;
                rollingCounterView.A0B.set(0, 0, i9, i6);
                rollingCounterView.setLayoutParams(A0H2);
            }
        });
    }
}
