package com.whatsapp;

import X.AnonymousClass00X;
import X.AnonymousClass2QN;
import X.AnonymousClass5TG;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C73683gd;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.List;

/* loaded from: classes2.dex */
public class StarRatingBar extends LinearLayout implements View.OnClickListener {
    public int A00;
    public AnonymousClass5TG A01;
    public final int A02;
    public final Drawable A03;
    public final Drawable A04;

    public StarRatingBar(Context context) {
        this(context, null, 0);
    }

    public StarRatingBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public StarRatingBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A00 = 0;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0L, 0, 0);
        try {
            this.A02 = obtainStyledAttributes.getInt(1, 5);
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            this.A03 = drawable == null ? AnonymousClass00X.A04(null, getResources(), R.drawable.message_rating_star) : drawable;
            Drawable drawable2 = obtainStyledAttributes.getDrawable(2);
            this.A04 = drawable2 == null ? AnonymousClass00X.A04(null, getResources(), R.drawable.message_rating_star_border) : drawable2;
            obtainStyledAttributes.recycle();
            setSaveEnabled(true);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final void A00() {
        if (getChildCount() == 0) {
            int i = 0;
            while (i < this.A02) {
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                i++;
                Object valueOf = Integer.valueOf(i);
                imageView.setTag(valueOf);
                Resources resources = getResources();
                Object[] A1b = C12970iu.A1b();
                A1b[0] = valueOf;
                imageView.setContentDescription(resources.getQuantityString(R.plurals.feedback_rating_value, i, A1b));
                imageView.setImageDrawable(this.A04);
                imageView.setOnClickListener(this);
                addView(imageView);
            }
            return;
        }
        int i2 = 0;
        while (i2 < this.A02) {
            ImageView imageView2 = (ImageView) getChildAt(i2);
            i2++;
            imageView2.setImageDrawable(i2 <= this.A00 ? this.A03 : this.A04);
        }
    }

    public int getRating() {
        return this.A00;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        int A05 = C12960it.A05(view.getTag());
        this.A00 = A05;
        A00();
        sendAccessibilityEvent(16384);
        AnonymousClass5TG r0 = this.A01;
        if (r0 != null) {
            r0.AUZ(A05, true);
        }
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        A00();
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C73683gd r2 = (C73683gd) parcelable;
        super.onRestoreInstanceState(r2.getSuperState());
        this.A00 = r2.A00;
        A00();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        C73683gd r1 = new C73683gd(super.onSaveInstanceState());
        r1.A00 = this.A00;
        return r1;
    }

    @Override // android.view.View, android.view.accessibility.AccessibilityEventSource
    public void sendAccessibilityEvent(int i) {
        if (((AccessibilityManager) getContext().getSystemService("accessibility")).isTouchExplorationEnabled()) {
            super.sendAccessibilityEvent(i);
            AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
            List<CharSequence> text = obtain.getText();
            Resources A09 = C12960it.A09(this);
            Object[] A1a = C12980iv.A1a();
            C12960it.A1P(A1a, this.A00, 0);
            text.add(C12990iw.A0o(A09, Integer.valueOf(this.A02), A1a, 1, R.string.feedback_rating_confirmation));
            obtain.setEnabled(true);
            ((AccessibilityManager) getContext().getSystemService("accessibility")).sendAccessibilityEvent(obtain);
        }
    }

    public void setOnRatingChangeListener(AnonymousClass5TG r1) {
        this.A01 = r1;
    }

    public void setRating(int i) {
        this.A00 = i;
        A00();
        sendAccessibilityEvent(16384);
        AnonymousClass5TG r0 = this.A01;
        if (r0 != null) {
            r0.AUZ(i, false);
        }
    }
}
