package com.whatsapp;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15680nj;
import X.C15860o1;
import X.C16170oZ;
import X.C17050qB;
import X.C18330sH;
import X.C252018m;
import X.C255319t;
import X.C255719x;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import androidx.preference.PreferenceFragmentCompat;
import com.whatsapp.settings.Hilt_SettingsChatHistoryFragment;
import com.whatsapp.settings.Hilt_SettingsJidNotificationFragment;
import com.whatsapp.settings.SettingsChatHistoryFragment;
import com.whatsapp.settings.SettingsJidNotificationFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_WaPreferenceFragment extends PreferenceFragmentCompat implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1A();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A1A()
            r3.A19()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.Hilt_WaPreferenceFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1A();
        A19();
    }

    public void A19() {
        if (this instanceof Hilt_SettingsJidNotificationFragment) {
            Hilt_SettingsJidNotificationFragment hilt_SettingsJidNotificationFragment = (Hilt_SettingsJidNotificationFragment) this;
            if (!hilt_SettingsJidNotificationFragment.A02) {
                hilt_SettingsJidNotificationFragment.A02 = true;
                SettingsJidNotificationFragment settingsJidNotificationFragment = (SettingsJidNotificationFragment) hilt_SettingsJidNotificationFragment;
                AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) hilt_SettingsJidNotificationFragment.generatedComponent())).A0Y;
                settingsJidNotificationFragment.A03 = (AnonymousClass19M) r1.A6R.get();
                settingsJidNotificationFragment.A00 = (AnonymousClass12P) r1.A0H.get();
                settingsJidNotificationFragment.A06 = (C252018m) r1.A7g.get();
                settingsJidNotificationFragment.A05 = (C15860o1) r1.A3H.get();
                settingsJidNotificationFragment.A01 = (C18330sH) r1.AN4.get();
                settingsJidNotificationFragment.A02 = (AnonymousClass018) r1.ANb.get();
            }
        } else if (this instanceof Hilt_SettingsChatHistoryFragment) {
            Hilt_SettingsChatHistoryFragment hilt_SettingsChatHistoryFragment = (Hilt_SettingsChatHistoryFragment) this;
            if (!hilt_SettingsChatHistoryFragment.A02) {
                hilt_SettingsChatHistoryFragment.A02 = true;
                SettingsChatHistoryFragment settingsChatHistoryFragment = (SettingsChatHistoryFragment) hilt_SettingsChatHistoryFragment;
                AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_SettingsChatHistoryFragment.generatedComponent())).A0Y;
                settingsChatHistoryFragment.A01 = (C15570nT) r2.AAr.get();
                settingsChatHistoryFragment.A0B = (AbstractC14440lR) r2.ANe.get();
                settingsChatHistoryFragment.A02 = (C15450nH) r2.AII.get();
                settingsChatHistoryFragment.A03 = (C16170oZ) r2.AM4.get();
                settingsChatHistoryFragment.A07 = (C14950mJ) r2.AKf.get();
                settingsChatHistoryFragment.A04 = (C15550nR) r2.A45.get();
                settingsChatHistoryFragment.A05 = (C255319t) r2.A6s.get();
                settingsChatHistoryFragment.A06 = (C17050qB) r2.ABL.get();
                settingsChatHistoryFragment.A08 = (C15680nj) r2.A4e.get();
                settingsChatHistoryFragment.A00 = (C14900mE) r2.A8X.get();
                settingsChatHistoryFragment.A0A = (C255719x) r2.A5X.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public final void A1A() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
