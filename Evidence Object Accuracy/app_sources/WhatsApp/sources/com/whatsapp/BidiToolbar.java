package com.whatsapp;

import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2Q3;
import X.C42941w9;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes2.dex */
public class BidiToolbar extends AnonymousClass2Q3 {
    public AnonymousClass018 A00;

    public BidiToolbar(Context context) {
        super(context);
        if (!isInEditMode()) {
            AnonymousClass028.A0c(this, this.A00.A04().A06 ? 1 : 0);
        }
    }

    public BidiToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (!isInEditMode()) {
            AnonymousClass028.A0c(this, this.A00.A04().A06 ? 1 : 0);
        }
    }

    public BidiToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!isInEditMode()) {
            AnonymousClass028.A0c(this, this.A00.A04().A06 ? 1 : 0);
        }
    }

    public final void A0K(View view, int i) {
        if ((view instanceof ViewGroup) && view.getId() != R.id.custom_view) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                childAt.layout(i - childAt.getRight(), childAt.getTop(), i - childAt.getLeft(), childAt.getBottom());
                A0K(childAt, childAt.getRight() - childAt.getLeft());
            }
        }
    }

    @Override // androidx.appcompat.widget.Toolbar, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (!isInEditMode() && !C42941w9.A01 && this.A00.A04().A06) {
            A0K(this, i3 - i);
        }
    }
}
