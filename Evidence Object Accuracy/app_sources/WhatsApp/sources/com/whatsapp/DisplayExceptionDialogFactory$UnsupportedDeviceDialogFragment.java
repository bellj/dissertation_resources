package com.whatsapp;

import X.ActivityC000900k;
import X.C004802e;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment extends Hilt_DisplayExceptionDialogFactory_UnsupportedDeviceDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e r2 = new C004802e(A0C());
        r2.A07(R.string.app_name);
        r2.A06(R.string.device_unsupported);
        r2.A0B(false);
        r2.setPositiveButton(R.string.ok, null);
        return r2.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.finish();
        }
    }
}
