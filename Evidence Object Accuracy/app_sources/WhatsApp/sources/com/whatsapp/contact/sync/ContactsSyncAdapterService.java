package com.whatsapp.contact.sync;

import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass1DQ;
import X.AnonymousClass5B7;
import X.C12970iu;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import android.os.IBinder;

/* loaded from: classes2.dex */
public class ContactsSyncAdapterService extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public AnonymousClass1DQ A00;
    public boolean A01;
    public final Object A02;
    public volatile C71083cM A03;

    public ContactsSyncAdapterService() {
        this(0);
    }

    public ContactsSyncAdapterService(int i) {
        this.A02 = C12970iu.A0l();
        this.A01 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    this.A03 = new C71083cM(this);
                }
            }
        }
        return this.A03.generatedComponent();
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.A00.getSyncAdapterBinder();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A01) {
            this.A01 = true;
            this.A00 = (AnonymousClass1DQ) ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01.A4E.get();
        }
        super.onCreate();
    }
}
