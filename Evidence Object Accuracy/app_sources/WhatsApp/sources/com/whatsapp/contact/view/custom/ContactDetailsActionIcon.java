package com.whatsapp.contact.view.custom;

import X.AnonymousClass00T;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12970iu;
import X.C42971wC;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class ContactDetailsActionIcon extends LinearLayout {
    public int A00;
    public int A01;
    public ImageView A02;
    public WaTextView A03;

    public ContactDetailsActionIcon(Context context) {
        super(context);
        A01(context, null);
    }

    public ContactDetailsActionIcon(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01(context, attributeSet);
    }

    public ContactDetailsActionIcon(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(context, attributeSet);
    }

    public void A00(int i, String str) {
        this.A02.setImageResource(i);
        this.A03.setText(C42971wC.A00(getContext(), str), TextView.BufferType.SPANNABLE);
    }

    public final void A01(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate(R.layout.contact_details_action_icon, (ViewGroup) this, true);
        this.A02 = C12970iu.A0K(this, R.id.action_icon);
        this.A03 = C12960it.A0N(this, R.id.action_title);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A06);
            int resourceId = obtainStyledAttributes.getResourceId(2, 0);
            Context context2 = getContext();
            if (resourceId == 0) {
                resourceId = R.color.ui_refresh_contact_info_action_text_color;
            }
            this.A01 = AnonymousClass00T.A00(context2, resourceId);
            this.A00 = AnonymousClass00T.A00(getContext(), R.color.disabled_text);
            this.A03.setTextColor(this.A01);
            A00(obtainStyledAttributes.getResourceId(0, 0), context.getString(obtainStyledAttributes.getResourceId(1, 0)));
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.A02.setEnabled(z);
        this.A03.setTextColor(z ? this.A01 : this.A00);
    }
}
