package com.whatsapp.contact;

import X.ActivityC13790kL;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ContactFormActivity extends ActivityC13790kL {
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_contact_form);
        ((TextInputLayout) findViewById(R.id.name_input_layout)).setHint(getResources().getString(R.string.contact_form_name_field_hint));
    }
}
