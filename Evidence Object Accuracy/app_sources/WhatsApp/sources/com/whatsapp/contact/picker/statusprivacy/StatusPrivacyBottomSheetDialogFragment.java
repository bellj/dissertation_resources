package com.whatsapp.contact.picker.statusprivacy;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass05J;
import X.AnonymousClass05L;
import X.AnonymousClass06W;
import X.AnonymousClass1US;
import X.AnonymousClass2FW;
import X.AnonymousClass3EA;
import X.C12960it;
import X.C12970iu;
import X.C18470sV;
import X.C32731ce;
import X.C53292dk;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.contact.picker.statusprivacy.StatusPrivacyBottomSheetDialogFragment;

/* loaded from: classes2.dex */
public class StatusPrivacyBottomSheetDialogFragment extends Hilt_StatusPrivacyBottomSheetDialogFragment {
    public AnonymousClass3EA A00;
    public AnonymousClass2FW A01;
    public C53292dk A02;
    public AnonymousClass018 A03;
    public C32731ce A04;
    public C18470sV A05;
    public AnonymousClass01H A06;
    public final AnonymousClass05L A07 = A06(new AnonymousClass05J() { // from class: X.3Oi
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            StatusPrivacyBottomSheetDialogFragment statusPrivacyBottomSheetDialogFragment = StatusPrivacyBottomSheetDialogFragment.this;
            C06830Vg r5 = (C06830Vg) obj;
            if (r5.A00 == -1) {
                Intent intent = r5.A01;
                AnonymousClass009.A05(intent);
                Parcelable parcelableExtra = intent.getParcelableExtra("status_distribution");
                AnonymousClass009.A05(parcelableExtra);
                C32731ce r0 = (C32731ce) parcelableExtra;
                statusPrivacyBottomSheetDialogFragment.A04 = r0;
                statusPrivacyBottomSheetDialogFragment.A00.A01(r0.A01.size(), statusPrivacyBottomSheetDialogFragment.A04.A02.size());
            }
            statusPrivacyBottomSheetDialogFragment.A00.A00(statusPrivacyBottomSheetDialogFragment.A04.A00);
        }
    }, new AnonymousClass06W());
    public final AnonymousClass05L A08 = A06(new AnonymousClass05J() { // from class: X.3Oh
        @Override // X.AnonymousClass05J
        public final void ALs(Object obj) {
            StatusPrivacyBottomSheetDialogFragment statusPrivacyBottomSheetDialogFragment = StatusPrivacyBottomSheetDialogFragment.this;
            C32731ce r0 = new C32731ce(statusPrivacyBottomSheetDialogFragment.A05.A06(), statusPrivacyBottomSheetDialogFragment.A05.A07(), statusPrivacyBottomSheetDialogFragment.A05.A03.A00("status_distribution", 0), false);
            statusPrivacyBottomSheetDialogFragment.A04 = r0;
            statusPrivacyBottomSheetDialogFragment.A00.A00(r0.A00);
            statusPrivacyBottomSheetDialogFragment.A00.A01(statusPrivacyBottomSheetDialogFragment.A04.A01.size(), statusPrivacyBottomSheetDialogFragment.A04.A02.size());
        }
    }, new AnonymousClass06W());

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A01 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Parcelable parcelable = A03().getParcelable("status_distribution");
        AnonymousClass009.A05(parcelable);
        this.A04 = (C32731ce) parcelable;
        this.A02 = new C53292dk(A01());
        AnonymousClass3EA r5 = new AnonymousClass3EA(A01(), this.A02, this.A03);
        this.A00 = r5;
        C32731ce r0 = this.A04;
        int i = r0.A00;
        int size = r0.A01.size();
        int size2 = this.A04.A02.size();
        r5.A00(i);
        r5.A01(size, size2);
        AnonymousClass018 r4 = r5.A02;
        Object[] A1b = C12970iu.A1b();
        A1b[0] = AnonymousClass1US.A05(r5.A00, R.color.accent_light);
        Spanned fromHtml = Html.fromHtml(r4.A0B(R.string.privacy_settings_footer_text, A1b));
        C53292dk r2 = r5.A01;
        r2.setFooterText(fromHtml);
        AbstractView$OnClickListenerC34281fs.A02(r2.A03, r2, this, 28);
        AbstractView$OnClickListenerC34281fs.A02(r2.A02, r2, this, 29);
        AbstractView$OnClickListenerC34281fs.A02(r2.A01, r2, this, 30);
        AbstractView$OnClickListenerC34281fs.A02(r2.A08, r2, this, 31);
        AbstractView$OnClickListenerC34281fs.A02(r2.A04, r2, this, 32);
        AbstractView$OnClickListenerC34281fs.A02(r2.A06, r2, this, 33);
        AbstractView$OnClickListenerC34281fs.A02(r2.A05, r2, this, 34);
        return this.A02;
    }

    @Override // com.whatsapp.contact.picker.statusprivacy.Hilt_StatusPrivacyBottomSheetDialogFragment, com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AnonymousClass2FW) {
            this.A01 = (AnonymousClass2FW) context;
            return;
        }
        throw C12960it.A0U(C12960it.A0d("StatusPrivacyBottomSheetDialogListener", C12960it.A0k("Activity must implement ")));
    }

    public void A1M(int i) {
        C32731ce r0 = this.A04;
        this.A04 = new C32731ce(r0.A01, r0.A02, i, r0.A03);
    }

    public final void A1N(boolean z) {
        AnonymousClass05L r4 = this.A07;
        Context A01 = A01();
        C32731ce r3 = this.A04;
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(A01.getPackageName(), "com.whatsapp.status.StatusTemporalRecipientsActivity");
        A0A.putExtra("is_black_list", z);
        A0A.putExtra("status_distribution", r3);
        r4.A00(null, A0A);
    }
}
