package com.whatsapp.contact.picker.invite;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass04S;
import X.AnonymousClass1US;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15450nH;
import X.C15550nR;
import X.C15610nY;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class InviteToGroupCallConfirmationFragment extends Hilt_InviteToGroupCallConfirmationFragment {
    public C15450nH A00;
    public C15550nR A01;
    public C15610nY A02;

    public static InviteToGroupCallConfirmationFragment A00(UserJid userJid) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("peer_id", userJid.getRawString());
        InviteToGroupCallConfirmationFragment inviteToGroupCallConfirmationFragment = new InviteToGroupCallConfirmationFragment();
        inviteToGroupCallConfirmationFragment.A0U(A0D);
        return inviteToGroupCallConfirmationFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        UserJid nullable = UserJid.getNullable(A03().getString("peer_id"));
        AnonymousClass009.A06(nullable, "null peer jid");
        ActivityC000900k A0B = A0B();
        C004802e A0S = C12980iv.A0S(A0B);
        A0S.setTitle(C12970iu.A0q(this, C15610nY.A01(this.A02, this.A01.A0B(nullable)), new Object[1], 0, R.string.invite_to_group_call_confirmation_title));
        A0S.A0A(Html.fromHtml(C12970iu.A0q(this, AnonymousClass1US.A05(A0B, R.color.accent_light), new Object[1], 0, R.string.invite_to_group_call_confirmation_description)));
        AnonymousClass04S A0L = C12960it.A0L(new IDxCListenerShape3S0200000_1_I1(nullable, 4, this), A0S, R.string.invite_to_group_call_confirmation_positive_button_label);
        A0L.setCanceledOnTouchOutside(true);
        return A0L;
    }
}
