package com.whatsapp.contact.picker;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass116;
import X.AnonymousClass11P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass19D;
import X.AnonymousClass1A1;
import X.AnonymousClass1AR;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass37W;
import X.AnonymousClass38K;
import X.AnonymousClass3DQ;
import X.AnonymousClass3FO;
import X.AnonymousClass3HC;
import X.AnonymousClass4TL;
import X.C102264or;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14650lo;
import X.C14850m9;
import X.C14900mE;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15890o4;
import X.C16590pI;
import X.C18000rk;
import X.C21270x9;
import X.C22680zT;
import X.C28141Kv;
import X.C30721Yo;
import X.C38121nY;
import X.C40691s6;
import X.C41341tN;
import X.C41351tO;
import X.C41421tV;
import X.C48232Fc;
import X.C51492Vb;
import X.C52682bV;
import X.C54162gH;
import X.C55262i6;
import X.C66763Ov;
import X.C74903iy;
import X.C90634Oq;
import X.MenuItem$OnActionExpandListenerC100774mS;
import X.animation.Animation$AnimationListenerC66523Nx;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.EmptyTellAFriendView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.contact.picker.PhoneContactsSelector;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class PhoneContactsSelector extends ActivityC13770kJ {
    public MenuItem A00;
    public View A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public ListView A05;
    public RecyclerView A06;
    public C22680zT A07;
    public C48232Fc A08;
    public AnonymousClass1AR A09;
    public C14650lo A0A;
    public AnonymousClass116 A0B;
    public AnonymousClass130 A0C;
    public C15550nR A0D;
    public C15610nY A0E;
    public AnonymousClass1J1 A0F;
    public AnonymousClass1J1 A0G;
    public C21270x9 A0H;
    public C52682bV A0I;
    public AnonymousClass37W A0J;
    public AnonymousClass38K A0K;
    public AnonymousClass19D A0L;
    public AnonymousClass11P A0M;
    public C16590pI A0N;
    public C15890o4 A0O;
    public AnonymousClass018 A0P;
    public AbstractC14640lm A0Q;
    public AnonymousClass12U A0R;
    public AnonymousClass01H A0S;
    public AnonymousClass01H A0T;
    public String A0U;
    public ArrayList A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public final C54162gH A0Z;
    public final ArrayList A0a;
    public final ArrayList A0b;
    public final List A0c;

    public PhoneContactsSelector() {
        this(0);
        this.A0b = C12960it.A0l();
        this.A0a = C12960it.A0l();
        this.A0c = C12960it.A0l();
        this.A0Z = new C54162gH(this);
    }

    public PhoneContactsSelector(int i) {
        this.A0X = false;
        ActivityC13830kP.A1P(this, 57);
    }

    public static String A02(C22680zT r32, C14650lo r33, C15550nR r34, C51492Vb r35, C16590pI r36, AnonymousClass018 r37) {
        String str;
        AnonymousClass009.A00();
        C30721Yo r0 = new C30721Yo(r33, r34, r36, r37);
        String obj = Long.valueOf(r35.A04).toString();
        AnonymousClass3DQ r6 = r0.A08;
        r6.A01 = r35.A06;
        Context context = r0.A0C.A00;
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data2", "data3", "data5", "data4", "data6", "data7", "data9"}, "contact_id = ? AND mimetype=?", new String[]{obj, "vnd.android.cursor.item/name"}, null);
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    r6.A02 = C12970iu.A0n(query, "data2");
                    r6.A00 = C12970iu.A0n(query, "data3");
                    r6.A03 = C12970iu.A0n(query, "data5");
                    r6.A06 = C12970iu.A0n(query, "data4");
                    r6.A07 = C12970iu.A0n(query, "data6");
                    r6.A04 = C12970iu.A0n(query, "data7");
                    r6.A05 = C12970iu.A0n(query, "data9");
                } finally {
                    try {
                        query.close();
                    } catch (Throwable unused) {
                    }
                }
            }
            query.close();
        }
        Cursor query2 = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data2", "data1", "data3", "is_primary", "raw_contact_id"}, "contact_id =?", new String[]{obj}, null);
        try {
            Map A03 = C30721Yo.A03(context, obj);
            if (query2 != null) {
                while (query2.moveToNext()) {
                    r0.A0A((UserJid) A03.get(C12970iu.A0n(query2, "raw_contact_id")), C12970iu.A0n(query2, "data1"), C12970iu.A0n(query2, "data3"), C12990iw.A06(query2, "data2"), C12960it.A1V(C12990iw.A06(query2, "is_primary"), 1));
                }
                r0.A09();
                query2.close();
            }
            Cursor query3 = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, "contact_id = ?", new String[]{obj}, null);
            if (query3 != null) {
                while (query3.moveToNext()) {
                    try {
                        r0.A0C(C12970iu.A0n(query3, "data1"), C12970iu.A0n(query3, "data3"), C12990iw.A06(query3, "data2"), C12960it.A1V(C12990iw.A06(query3, "is_primary"), 1));
                    } finally {
                        try {
                            query3.close();
                        } catch (Throwable unused2) {
                        }
                    }
                }
                query3.close();
            }
            String format = String.format("%s =? AND %s =?", "contact_id", "mimetype");
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = ContactsContract.Data.CONTENT_URI;
            Cursor query4 = contentResolver.query(uri, new String[]{"data2", "data1"}, format, new String[]{obj, "vnd.android.cursor.item/website"}, null);
            if (query4 != null) {
                while (query4.moveToNext()) {
                    try {
                        int A06 = C12990iw.A06(query4, "data2");
                        String A0n = C12970iu.A0n(query4, "data1");
                        List list = r0.A06;
                        if (list == null) {
                            list = C12960it.A0l();
                            r0.A06 = list;
                        }
                        C90634Oq r7 = new C90634Oq();
                        r7.A00 = A06;
                        AnonymousClass009.A05(A0n);
                        r7.A01 = A0n;
                        list.add(r7);
                    } finally {
                        try {
                            query4.close();
                        } catch (Throwable unused3) {
                        }
                    }
                }
                query4.close();
            }
            Cursor query5 = context.getContentResolver().query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, "contact_id = ?", new String[]{obj}, null);
            if (query5 != null) {
                while (query5.moveToNext()) {
                    try {
                        if (r0.A02 == null) {
                            r0.A02 = C12960it.A0l();
                        }
                        AnonymousClass4TL r11 = new AnonymousClass4TL();
                        r11.A01 = ContactsContract.CommonDataKinds.StructuredPostal.class;
                        r11.A00 = C12990iw.A06(query5, "data2");
                        r11.A02 = C12970iu.A0n(query5, "data1");
                        r11.A04 = new AnonymousClass3HC();
                        String A0n2 = C12970iu.A0n(query5, "data4");
                        if (A0n2 != null) {
                            r11.A04.A03 = A0n2.replaceAll("(\r\n|\r|\n|\n\r)", " ");
                        }
                        r11.A04.A00 = C12970iu.A0n(query5, "data7");
                        r11.A04.A02 = C12970iu.A0n(query5, "data8");
                        r11.A04.A04 = C12970iu.A0n(query5, "data9");
                        r11.A04.A01 = C12970iu.A0n(query5, "data10");
                        r11.A03 = C12970iu.A0n(query5, "data3");
                        r11.A05 = C12970iu.A1W(C12990iw.A06(query5, "is_primary"));
                        r0.A02.add(r11);
                    } finally {
                        try {
                            query5.close();
                        } catch (Throwable unused4) {
                        }
                    }
                }
                query5.close();
            }
            Cursor query6 = context.getContentResolver().query(uri, null, "contact_id = ? AND mimetype = ?", new String[]{obj, "vnd.android.cursor.item/organization"}, null);
            if (query6 != null) {
                try {
                    if (query6.moveToFirst()) {
                        String A0n3 = C12970iu.A0n(query6, "data1");
                        String A0n4 = C12970iu.A0n(query6, "data5");
                        StringBuilder A0j = C12960it.A0j(A0n3);
                        if (A0n4 == null || A0n4.length() == 0) {
                            str = "";
                        } else {
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append(";");
                            str = C12960it.A0d(A0n4, A0h);
                        }
                        String A0d = C12960it.A0d(str, A0j);
                        String A0n5 = C12970iu.A0n(query6, "data4");
                        query6.getInt(query6.getColumnIndexOrThrow("is_primary"));
                        r0.A0B(A0d, A0n5);
                    }
                    query6.close();
                } finally {
                }
            }
            query6 = context.getContentResolver().query(uri, new String[]{"data15"}, "contact_id = ? AND mimetype = ? ", new String[]{obj, "vnd.android.cursor.item/photo"}, null);
            if (query6 != null) {
                try {
                    if (query6.moveToFirst()) {
                        r0.A09 = query6.getBlob(0);
                    }
                    query6.close();
                } finally {
                }
            }
            Cursor query7 = context.getContentResolver().query(uri, null, "contact_id = ? AND mimetype = ?", new String[]{obj, "vnd.android.cursor.item/nickname"}, null);
            if (query7 != null) {
                try {
                    if (query7.moveToFirst()) {
                        AnonymousClass3FO r62 = new AnonymousClass3FO();
                        r62.A01 = "NICKNAME";
                        r62.A02 = C12970iu.A0n(query7, "data1");
                        r0.A0D(r62);
                    }
                    query7.close();
                } finally {
                    try {
                        query7.close();
                    } catch (Throwable unused5) {
                    }
                }
            }
            query6 = context.getContentResolver().query(uri, null, "contact_id = ? AND mimetype = ? AND data2 =? ", new String[]{obj, "vnd.android.cursor.item/contact_event", String.valueOf(3)}, null);
            if (query6 != null) {
                try {
                    if (query6.moveToFirst()) {
                        AnonymousClass3FO r9 = new AnonymousClass3FO();
                        r9.A01 = "BDAY";
                        String A0n6 = C12970iu.A0n(query6, "data1");
                        if (A0n6 == null) {
                            A0n6 = null;
                        } else {
                            try {
                                A0n6 = ((DateFormat) C38121nY.A02.A01()).format(((DateFormat) C38121nY.A00.A01()).parse(A0n6));
                            } catch (ParseException e) {
                                StringBuilder A0k = C12960it.A0k("Date string '");
                                A0k.append(A0n6);
                                Log.e(C12960it.A0d("' not in format of <MMM dd, yyyy>", A0k), e);
                            }
                        }
                        r9.A02 = A0n6;
                        r0.A0D(r9);
                    }
                    query6.close();
                } finally {
                    try {
                        query6.close();
                    } catch (Throwable unused6) {
                    }
                }
            }
            Cursor query8 = context.getContentResolver().query(uri, null, "contact_id = ? AND mimetype = ? ", new String[]{obj, "vnd.android.cursor.item/im"}, null);
            if (query8 != null) {
                while (query8.moveToNext()) {
                    try {
                        int A062 = C12990iw.A06(query8, "data5");
                        AnonymousClass3FO r5 = new AnonymousClass3FO();
                        r5.A02 = C12970iu.A0n(query8, "data1");
                        AnonymousClass018 r8 = r0.A0D;
                        String string = r8.A00.getResources().getString(ContactsContract.CommonDataKinds.Im.getProtocolLabelResource(A062));
                        Iterator A0s = C12990iw.A0s(C30721Yo.A0F);
                        while (A0s.hasNext()) {
                            Map.Entry A15 = C12970iu.A15(A0s);
                            if (((String) A15.getValue()).equalsIgnoreCase(string)) {
                                r5.A01 = C12990iw.A0r(A15);
                            }
                        }
                        r5.A04.add(r8.A00.getResources().getString(ContactsContract.CommonDataKinds.Im.getTypeLabelResource(A062)).toUpperCase());
                        r0.A0D(r5);
                    } finally {
                        try {
                            query8.close();
                        } catch (Throwable unused7) {
                        }
                    }
                }
                query8.close();
            }
            C41421tV.A00(r34, r0);
            try {
                return new C41421tV(r32, r37).A01(r0);
            } catch (C41341tN e2) {
                Log.e("Could not create VCard", new C41351tO(e2));
                return null;
            }
        } catch (Throwable th) {
            if (query2 != null) {
                try {
                    query2.close();
                } catch (Throwable unused8) {
                }
            }
            throw th;
        }
    }

    public static /* synthetic */ void A03(PhoneContactsSelector phoneContactsSelector) {
        AnonymousClass37W r1 = phoneContactsSelector.A0J;
        if (r1 != null) {
            r1.A03(true);
            phoneContactsSelector.A0J = null;
        }
        AnonymousClass37W r12 = new AnonymousClass37W(phoneContactsSelector, phoneContactsSelector.A0P, phoneContactsSelector.A0V, phoneContactsSelector.A0b);
        phoneContactsSelector.A0J = r12;
        C12960it.A1E(r12, ((ActivityC13830kP) phoneContactsSelector).A05);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0X) {
            this.A0X = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0N = C12970iu.A0X(A1M);
            this.A0R = ActivityC13830kP.A1N(A1M);
            this.A09 = (AnonymousClass1AR) A1M.ALM.get();
            this.A0H = C12970iu.A0W(A1M);
            this.A0C = C12990iw.A0Y(A1M);
            this.A0D = C12960it.A0O(A1M);
            this.A0E = C12960it.A0P(A1M);
            this.A0P = C12960it.A0R(A1M);
            this.A07 = (C22680zT) A1M.AGW.get();
            this.A0B = (AnonymousClass116) A1M.A3z.get();
            this.A0O = C12970iu.A0Y(A1M);
            this.A0A = C12980iv.A0Y(A1M);
            this.A0L = (AnonymousClass19D) A1M.ABo.get();
            this.A0M = (AnonymousClass11P) A1M.ABp.get();
            this.A0S = C18000rk.A00(A1M.ADw);
            this.A0T = C18000rk.A00(A1M.AID);
        }
    }

    public final void A2g() {
        View view;
        int i;
        this.A02.setVisibility(4);
        if (this.A02.getVisibility() == 0 || !this.A0Y) {
            view = this.A03;
            i = 8;
        } else {
            view = this.A03;
            i = 0;
        }
        view.setVisibility(i);
        if (!this.A0Y) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-getResources().getDimensionPixelSize(R.dimen.selected_contacts_layout_height)));
            translateAnimation.setDuration(240);
            translateAnimation.setAnimationListener(new animation.Animation$AnimationListenerC66523Nx(this, 0));
            this.A05.startAnimation(translateAnimation);
        }
    }

    public final void A2h(int i) {
        AbstractC005102i A1U = A1U();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1P(A1b, i, 0);
        A1U.A0H(this.A0P.A0I(A1b, R.plurals.n_contacts_selected, (long) i));
    }

    public final void A2i(C51492Vb r11) {
        boolean z;
        SelectionCheckView selectionCheckView = (SelectionCheckView) this.A05.findViewWithTag(r11);
        if (r11.A03) {
            z = false;
            r11.A03 = false;
        } else if (this.A0c.size() == 257) {
            C14900mE r5 = ((ActivityC13810kN) this).A05;
            AnonymousClass018 r4 = this.A0P;
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 257, 0);
            r5.A0E(r4.A0I(objArr, R.plurals.contact_array_message_reach_limit, 257), 1);
            if (selectionCheckView != null) {
                selectionCheckView.A04(false, false);
                return;
            }
            return;
        } else {
            TextView A0J = C12960it.A0J(findViewById(R.id.search_holder), R.id.search_src_text);
            if (A0J != null) {
                C12990iw.A1G(A0J);
            }
            z = true;
            r11.A03 = true;
        }
        List list = this.A0c;
        if (!z) {
            int indexOf = list.indexOf(r11);
            if (list.remove(r11)) {
                this.A0Z.A05(indexOf);
            }
        } else if (list.add(r11)) {
            this.A0Z.A04(C12990iw.A09(list, 1));
        }
        if (selectionCheckView != null) {
            selectionCheckView.A04(r11.A03, false);
        }
        if (list.isEmpty()) {
            A2g();
        } else if (this.A02.getVisibility() != 0) {
            if (this.A03.getVisibility() != 0) {
                int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.selected_contacts_layout_height);
                this.A02.setVisibility(0);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) dimensionPixelSize);
                translateAnimation.setDuration(240);
                translateAnimation.setAnimationListener(new animation.Animation$AnimationListenerC66523Nx(this, dimensionPixelSize));
                this.A05.startAnimation(translateAnimation);
            } else {
                this.A03.setVisibility(8);
                this.A02.setVisibility(0);
            }
        } else if (r11.A03) {
            this.A06.A0Y(C12990iw.A09(list, 1));
        }
        A2h(list.size());
        if (r11.A02 == null) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape11S0200000_I1_1(this, 15, r11));
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 8) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            setResult(-1, intent);
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (ActivityC13810kN.A1I(this)) {
            this.A0S.get();
        }
        if (this.A0W) {
            this.A0W = false;
            AnonymousClass009.A01();
            ArrayList arrayList = this.A0a;
            arrayList.clear();
            arrayList.addAll(this.A0b);
            C52682bV r0 = this.A0I;
            if (r0 != null) {
                r0.notifyDataSetChanged();
            }
            this.A08.A04(true);
            return;
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ListView listView;
        int dimensionPixelSize;
        Resources resources;
        int i;
        View view;
        int i2;
        super.onCreate(bundle);
        setContentView(R.layout.multiple_contact_picker);
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0N(true);
        this.A0F = this.A0H.A04(this, "phone-contacts-selector");
        this.A08 = new C48232Fc(this, findViewById(R.id.search_holder), new C66763Ov(this), A0Q, this.A0P);
        setTitle(R.string.contacts_to_send);
        this.A0Q = C12970iu.A0c(this);
        ListView A2e = A2e();
        this.A05 = A2e;
        A2e.setFastScrollAlwaysVisible(true);
        this.A05.setScrollBarStyle(33554432);
        List list = this.A0c;
        list.clear();
        this.A06 = (RecyclerView) findViewById(R.id.selected_items);
        this.A06.A0l(new C74903iy(this, getResources().getDimensionPixelSize(R.dimen.selected_contacts_top_offset)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(0);
        this.A06.setLayoutManager(linearLayoutManager);
        this.A06.setAdapter(this.A0Z);
        this.A06.setItemAnimator(new C55262i6());
        this.A05.setOnScrollListener(new C102264or(this));
        this.A05.setFastScrollEnabled(true);
        this.A05.setScrollbarFadingEnabled(true);
        boolean A00 = C28141Kv.A00(this.A0P);
        ListView listView2 = this.A05;
        if (A00) {
            listView2.setVerticalScrollbarPosition(1);
            listView = this.A05;
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_right);
            resources = getResources();
            i = R.dimen.contact_list_padding_left;
        } else {
            listView2.setVerticalScrollbarPosition(2);
            listView = this.A05;
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_left);
            resources = getResources();
            i = R.dimen.contact_list_padding_right;
        }
        listView.setPadding(dimensionPixelSize, 0, resources.getDimensionPixelSize(i), 0);
        this.A05.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4oz
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view2, int i3, long j) {
                PhoneContactsSelector phoneContactsSelector = PhoneContactsSelector.this;
                View findViewById = view2.findViewById(R.id.selection_check);
                if (findViewById != null) {
                    phoneContactsSelector.A2i((C51492Vb) findViewById.getTag());
                }
            }
        });
        A2h(list.size());
        this.A02 = findViewById(R.id.selected_list);
        if (list.isEmpty()) {
            this.A02.setVisibility(4);
        }
        this.A03 = findViewById(R.id.warning);
        TextView A0M = C12970iu.A0M(this, R.id.warning_text);
        C12990iw.A1G(A0M);
        this.A0Y = !TextUtils.isEmpty(A0M.getText());
        if (this.A02.getVisibility() == 0 || !this.A0Y) {
            view = this.A03;
            i2 = 8;
        } else {
            view = this.A03;
            i2 = 0;
        }
        view.setVisibility(i2);
        C52682bV r0 = new C52682bV(this, this, this.A0a);
        this.A0I = r0;
        A2f(r0);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.next_btn);
        this.A04 = imageView;
        AnonymousClass2GF.A01(this, imageView, this.A0P, R.drawable.ic_fab_next);
        C12960it.A0r(this, this.A04, R.string.next);
        this.A04.setVisibility(0);
        AbstractView$OnClickListenerC34281fs.A00(this.A04, this, 48);
        ((EmptyTellAFriendView) findViewById(R.id.contacts_empty)).setInviteButtonClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(this, 30));
        AbstractView$OnClickListenerC34281fs.A00(findViewById(R.id.button_open_permission_settings), this, 49);
        registerForContextMenu(this.A05);
        if (bundle == null && !this.A0B.A00()) {
            RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_new_group_request, R.string.permission_contacts_access_on_new_group);
        }
        if (this.A0B.A00()) {
            C12970iu.A1N(this, R.id.init_contacts_progress, 0);
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search);
        this.A00 = icon;
        icon.setShowAsAction(10);
        this.A00.setOnActionExpandListener(new MenuItem$OnActionExpandListenerC100774mS(this));
        this.A00.setVisible(!this.A0b.isEmpty());
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass37W r0 = this.A0J;
        if (r0 != null) {
            r0.A03(true);
            this.A0J = null;
        }
        AnonymousClass38K r02 = this.A0K;
        if (r02 != null) {
            r02.A03(true);
            this.A0K = null;
        }
        this.A0b.clear();
        this.A0a.clear();
        this.A0F.A00();
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A02(this.A01, this.A0M);
            AnonymousClass1J1 r03 = this.A0G;
            if (r03 != null) {
                r03.A00();
                this.A0G = null;
            }
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_search) {
            onSearchRequested();
            return true;
        } else if (itemId != 16908332) {
            return true;
        } else {
            finish();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A07(this.A0M);
            ((AnonymousClass1A1) this.A0S.get()).A02(((ActivityC13810kN) this).A00);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        AnonymousClass38K r0 = this.A0K;
        if (r0 != null) {
            r0.A03(true);
        }
        AnonymousClass37W r02 = this.A0J;
        if (r02 != null) {
            r02.A03(true);
            this.A0J = null;
        }
        AnonymousClass38K r1 = new AnonymousClass38K(this, ((ActivityC13810kN) this).A08, this.A0N, this.A0O);
        this.A0K = r1;
        C12960it.A1E(r1, ((ActivityC13830kP) this).A05);
        if (this.A0B.A00()) {
            this.A04.setVisibility(0);
        }
        if (ActivityC13810kN.A1I(this)) {
            boolean z = ((AnonymousClass1A1) this.A0S.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r15 = ((ActivityC13810kN) this).A0C;
                C14900mE r13 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
                C21270x9 r10 = this.A0H;
                C15550nR r9 = this.A0D;
                C15610nY r8 = this.A0E;
                AnonymousClass018 r7 = this.A0P;
                Pair A00 = C40691s6.A00(this, view, this.A01, r13, r12, r9, r8, this.A0G, r10, this.A0L, this.A0M, ((ActivityC13810kN) this).A09, r7, r15, r11, this.A0S, this.A0T, "phone-contacts-selector-activity");
                this.A01 = (View) A00.first;
                this.A0G = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A0M, this.A0S);
            }
            ((AnonymousClass1A1) this.A0S.get()).A01();
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A08.A01();
        this.A0W = true;
        return false;
    }
}
