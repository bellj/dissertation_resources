package com.whatsapp.contact.picker.invite;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC36921ks;
import X.AbstractC36931kt;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass02P;
import X.AnonymousClass10S;
import X.AnonymousClass116;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass19M;
import X.AnonymousClass1AR;
import X.AnonymousClass1AW;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2xS;
import X.AnonymousClass312;
import X.AnonymousClass3AX;
import X.C102994q2;
import X.C103774rI;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21240x6;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C36881kn;
import X.C36941ku;
import X.C36951kv;
import X.C48232Fc;
import X.C53952fr;
import X.MenuItem$OnActionExpandListenerC100804mV;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.contact.picker.PhoneNumberSelectionDialog;
import com.whatsapp.contact.picker.invite.InviteNonWhatsAppContactPickerActivity;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class InviteNonWhatsAppContactPickerActivity extends ActivityC13770kJ implements AbstractC36921ks, AbstractC36931kt {
    public MenuItem A00;
    public View A01;
    public View A02;
    public View A03;
    public ViewGroup A04;
    public ViewGroup A05;
    public Button A06;
    public TextView A07;
    public Toolbar A08;
    public C48232Fc A09;
    public AnonymousClass1AR A0A;
    public AnonymousClass116 A0B;
    public AnonymousClass130 A0C;
    public C15550nR A0D;
    public AnonymousClass10S A0E;
    public C15610nY A0F;
    public AnonymousClass1J1 A0G;
    public C21270x9 A0H;
    public C21240x6 A0I;
    public AnonymousClass2xS A0J;
    public C36941ku A0K;
    public AnonymousClass018 A0L;
    public AnonymousClass1AW A0M;
    public boolean A0N;
    public final C27131Gd A0O;

    public InviteNonWhatsAppContactPickerActivity() {
        this(0);
        this.A0O = new C36881kn(this);
    }

    public InviteNonWhatsAppContactPickerActivity(int i) {
        this.A0N = false;
        A0R(new C102994q2(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0N) {
            this.A0N = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0H = (C21270x9) r1.A4A.get();
            this.A0A = (AnonymousClass1AR) r1.ALM.get();
            this.A0C = (AnonymousClass130) r1.A41.get();
            this.A0D = (C15550nR) r1.A45.get();
            this.A0M = (AnonymousClass1AW) r1.AAX.get();
            this.A0F = (C15610nY) r1.AMe.get();
            this.A0L = (AnonymousClass018) r1.ANb.get();
            this.A0E = (AnonymousClass10S) r1.A46.get();
            this.A0I = (C21240x6) r1.AA7.get();
            this.A0B = (AnonymousClass116) r1.A3z.get();
        }
    }

    public final View A2g() {
        View inflate = getLayoutInflater().inflate(R.layout.contact_picker_row_small, (ViewGroup) null, false);
        AnonymousClass3AX.A00(inflate, R.drawable.ic_action_share, 0, R.drawable.green_circle, R.string.share_link);
        inflate.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 5));
        return inflate;
    }

    public final Integer A2h() {
        int intExtra = getIntent().getIntExtra("invite_source", 0);
        if (intExtra == 0) {
            return null;
        }
        return Integer.valueOf(intExtra);
    }

    public final void A2i(boolean z) {
        this.A05.addView(A2g());
        this.A05.setVisibility(0);
        View inflate = getLayoutInflater().inflate(R.layout.list_section, (ViewGroup) null, false);
        ((TextView) AnonymousClass028.A0D(inflate, R.id.title)).setText(R.string.from_contacts);
        this.A04.addView(inflate);
        this.A04.setVisibility(0);
        this.A01.setVisibility(0);
        if (z) {
            this.A07.setText(R.string.no_known_contacts_invite);
            this.A06.setVisibility(8);
            return;
        }
        C21240x6 r3 = this.A0I;
        Integer A2h = A2h();
        AnonymousClass312 r1 = new AnonymousClass312();
        r1.A03 = 1;
        r1.A04 = A2h;
        r1.A00 = Boolean.TRUE;
        r3.A03.A07(r1);
        this.A07.setText(R.string.permission_contacts_needed);
        this.A06.setVisibility(0);
    }

    @Override // X.AbstractC36921ks
    public void AT9(String str) {
        this.A0K.A0D.A0B(str);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass016 r1 = this.A0K.A07;
        if (r1.A01() == null || !((Boolean) r1.A01()).booleanValue()) {
            super.onBackPressed();
        } else {
            this.A0K.A07.A0B(false);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.invite_non_whatsapp_contact_picker);
        setTitle(R.string.tell_a_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.A08 = toolbar;
        A1e(toolbar);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0N(true);
        AnonymousClass018 r10 = this.A0L;
        this.A09 = new C48232Fc(this, findViewById(R.id.search_holder), new C103774rI(this), this.A08, r10);
        AnonymousClass1J1 A04 = this.A0H.A04(this, "invite-non-wa-contact-picker");
        this.A0G = A04;
        AnonymousClass2xS r5 = new AnonymousClass2xS(this, this.A0C, A04, this.A0L, new ArrayList());
        this.A0J = r5;
        ListView A2e = A2e();
        View A2g = A2g();
        this.A02 = A2g;
        this.A03 = A2g;
        A2e.addHeaderView(A2g);
        A2e.setAdapter((ListAdapter) r5);
        registerForContextMenu(A2e);
        A2e.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.3O8
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = InviteNonWhatsAppContactPickerActivity.this;
                Object itemAtPosition = adapterView.getItemAtPosition(i);
                if (itemAtPosition instanceof AnonymousClass3WI) {
                    AnonymousClass3WI r6 = (AnonymousClass3WI) itemAtPosition;
                    List list = r6.A01;
                    if (list.size() > 1) {
                        ArrayList A0l = C12960it.A0l();
                        Iterator it = list.iterator();
                        while (it.hasNext()) {
                            C15370n3 A0a = C12970iu.A0a(it);
                            String A01 = C248917h.A01(A0a);
                            AnonymousClass009.A05(A01);
                            A0l.add(new C65983Lv((String) C15610nY.A00(inviteNonWhatsAppContactPickerActivity, inviteNonWhatsAppContactPickerActivity.A0L, A0a), A01));
                        }
                        C21240x6 r3 = inviteNonWhatsAppContactPickerActivity.A0I;
                        Integer A2h = inviteNonWhatsAppContactPickerActivity.A2h();
                        AnonymousClass312 r1 = new AnonymousClass312();
                        r1.A03 = 1;
                        r1.A04 = A2h;
                        Boolean bool = Boolean.TRUE;
                        r1.A02 = bool;
                        r1.A01 = bool;
                        r3.A03.A07(r1);
                        inviteNonWhatsAppContactPickerActivity.Adl(PhoneNumberSelectionDialog.A00(C12960it.A0X(inviteNonWhatsAppContactPickerActivity, r6.A00, new Object[1], 0, R.string.message_contact_name), A0l), null);
                        return;
                    }
                    C36941ku r2 = inviteNonWhatsAppContactPickerActivity.A0K;
                    String A012 = C248917h.A01(r6.ABZ());
                    AnonymousClass009.A05(A012);
                    r2.A0D.A0B(A012);
                }
            }
        });
        View A05 = AnonymousClass00T.A05(this, R.id.init_contacts_progress);
        this.A01 = AnonymousClass00T.A05(this, R.id.empty_view);
        this.A05 = (ViewGroup) AnonymousClass00T.A05(this, R.id.share_link_header);
        this.A04 = (ViewGroup) AnonymousClass00T.A05(this, R.id.contacts_section);
        this.A07 = (TextView) AnonymousClass00T.A05(this, R.id.invite_empty_description);
        Button button = (Button) AnonymousClass00T.A05(this, R.id.button_open_permission_settings);
        this.A06 = button;
        button.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 4));
        C36941ku r52 = (C36941ku) new AnonymousClass02A(new C53952fr(this), this).A00(C36941ku.class);
        this.A0K = r52;
        r52.A08.A0B(0);
        AnonymousClass016 r3 = r52.A06;
        r3.A0B(new ArrayList());
        AnonymousClass1AW r1 = r52.A0C;
        AnonymousClass02P r2 = r52.A02;
        r1.A00(new C36951kv(r52), r3, r2);
        r52.A03.A0D(r2, new AnonymousClass02B() { // from class: X.3QI
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass016 r12;
                int i;
                C36941ku r22 = C36941ku.this;
                List list = (List) obj;
                if (list.isEmpty()) {
                    r12 = r22.A08;
                    i = 1;
                } else if (C12980iv.A0o(list) instanceof AnonymousClass551) {
                    r12 = r22.A08;
                    i = 3;
                } else {
                    if (r22.A01) {
                        AnonymousClass016 r13 = r22.A04;
                        if (r13.A01() == null) {
                            r13.A0B(Boolean.TRUE);
                        }
                    }
                    r12 = r22.A08;
                    i = 2;
                }
                C12970iu.A1Q(r12, i);
                r22.A03.A0B(list);
            }
        });
        this.A0K.A0D.A05(this, new AnonymousClass02B() { // from class: X.3QG
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = InviteNonWhatsAppContactPickerActivity.this;
                inviteNonWhatsAppContactPickerActivity.A0A.A00(inviteNonWhatsAppContactPickerActivity, Uri.parse(C12960it.A0d((String) obj, C12960it.A0k("sms:"))), inviteNonWhatsAppContactPickerActivity.A2h(), C12960it.A0X(inviteNonWhatsAppContactPickerActivity, "https://whatsapp.com/dl/", C12970iu.A1b(), 0, R.string.tell_a_friend_sms));
            }
        });
        this.A0K.A08.A05(this, new AnonymousClass02B(A05, this) { // from class: X.3RH
            public final /* synthetic */ View A00;
            public final /* synthetic */ InviteNonWhatsAppContactPickerActivity A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = this.A01;
                View view = this.A00;
                int A052 = C12960it.A05(obj);
                if (A052 == 0) {
                    view.setVisibility(0);
                } else if (A052 == 1) {
                    view.setVisibility(8);
                    inviteNonWhatsAppContactPickerActivity.A2i(inviteNonWhatsAppContactPickerActivity.A0B.A00());
                } else if (A052 == 2) {
                    view.setVisibility(8);
                    ListView A2e2 = inviteNonWhatsAppContactPickerActivity.A2e();
                    if (A2e2.getHeaderViewsCount() == 0) {
                        A2e2.addHeaderView(inviteNonWhatsAppContactPickerActivity.A03);
                    }
                    A2e2.removeFooterView(inviteNonWhatsAppContactPickerActivity.A02);
                } else if (A052 == 3) {
                    view.setVisibility(8);
                    ListView A2e3 = inviteNonWhatsAppContactPickerActivity.A2e();
                    if (A2e3.getFooterViewsCount() == 0) {
                        A2e3.addFooterView(inviteNonWhatsAppContactPickerActivity.A02);
                    }
                    A2e3.removeHeaderView(inviteNonWhatsAppContactPickerActivity.A03);
                }
            }
        });
        this.A0K.A07.A05(this, new AnonymousClass02B() { // from class: X.4t1
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = InviteNonWhatsAppContactPickerActivity.this;
                boolean A1Y = C12970iu.A1Y(obj);
                C48232Fc r12 = inviteNonWhatsAppContactPickerActivity.A09;
                if (A1Y) {
                    r12.A01();
                } else {
                    r12.A04(true);
                }
            }
        });
        this.A0K.A05.A05(this, new AnonymousClass02B() { // from class: X.4t0
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity.this.A2i(C12970iu.A1Y(obj));
            }
        });
        this.A0K.A04.A05(this, new AnonymousClass02B() { // from class: X.3QF
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = InviteNonWhatsAppContactPickerActivity.this;
                C21240x6 r32 = inviteNonWhatsAppContactPickerActivity.A0I;
                Integer A2h = inviteNonWhatsAppContactPickerActivity.A2h();
                AnonymousClass312 r12 = new AnonymousClass312();
                r12.A03 = C12960it.A0V();
                r12.A04 = A2h;
                r12.A02 = Boolean.TRUE;
                r32.A03.A07(r12);
            }
        });
        this.A0E.A03(this.A0O);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C48232Fc r3 = this.A09;
        MenuItem icon = menu.add(0, R.id.menuitem_search, 0, r3.A05.getString(R.string.search)).setIcon(R.drawable.ic_action_search);
        icon.setShowAsAction(10);
        icon.setOnActionExpandListener(new MenuItem$OnActionExpandListenerC100804mV(this, r3));
        this.A00 = icon;
        this.A0K.A03.A05(this, new AnonymousClass02B() { // from class: X.3QH
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = InviteNonWhatsAppContactPickerActivity.this;
                List list = (List) obj;
                inviteNonWhatsAppContactPickerActivity.A00.setVisible(!list.isEmpty());
                AnonymousClass2xS r1 = inviteNonWhatsAppContactPickerActivity.A0J;
                r1.A01 = list;
                r1.A02 = list;
                r1.A00 = C12980iv.A0z(inviteNonWhatsAppContactPickerActivity.A0K.A06);
                inviteNonWhatsAppContactPickerActivity.A0J.notifyDataSetChanged();
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0E.A04(this.A0O);
        AnonymousClass1J1 r0 = this.A0G;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_search) {
            this.A0K.A07.A0B(true);
            return true;
        } else if (itemId != 16908332) {
            return false;
        } else {
            finish();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0K.A05.A0B(Boolean.valueOf(this.A0B.A00()));
    }
}
