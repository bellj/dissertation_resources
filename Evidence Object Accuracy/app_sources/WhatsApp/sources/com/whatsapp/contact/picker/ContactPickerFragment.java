package com.whatsapp.contact.picker;

import X.AbstractC009504t;
import X.AbstractC116005Tt;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC33331dp;
import X.AbstractC36671kL;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02Q;
import X.AnonymousClass10S;
import X.AnonymousClass116;
import X.AnonymousClass118;
import X.AnonymousClass11I;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass16P;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1AN;
import X.AnonymousClass1AR;
import X.AnonymousClass1BF;
import X.AnonymousClass1BK;
import X.AnonymousClass1CY;
import X.AnonymousClass1DP;
import X.AnonymousClass1E5;
import X.AnonymousClass1GY;
import X.AnonymousClass1J1;
import X.AnonymousClass1L2;
import X.AnonymousClass2Dn;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass37V;
import X.AnonymousClass3AX;
import X.AnonymousClass3CP;
import X.AnonymousClass3UA;
import X.C004802e;
import X.C004902f;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15220ml;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15690nk;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16490p7;
import X.C16630pM;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C18000rk;
import X.C18330sH;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C19780uf;
import X.C19990v2;
import X.C20710wC;
import X.C20730wE;
import X.C21240x6;
import X.C21270x9;
import X.C21550xb;
import X.C22050yP;
import X.C22190yg;
import X.C22330yu;
import X.C22650zQ;
import X.C22680zT;
import X.C22700zV;
import X.C238013b;
import X.C239613r;
import X.C244215l;
import X.C248917h;
import X.C251118d;
import X.C252018m;
import X.C253318z;
import X.C255019q;
import X.C25951Bl;
import X.C26311Cv;
import X.C26501Ds;
import X.C27131Gd;
import X.C32701cb;
import X.C32721cd;
import X.C32731ce;
import X.C36021jC;
import X.C36591kA;
import X.C42941w9;
import X.C48232Fc;
import X.C627538l;
import X.C628138r;
import X.C628538v;
import X.C628738x;
import X.C63273Ay;
import X.C65983Lv;
import X.C66663Ol;
import X.C853042f;
import X.C91544Sd;
import X.MenuItem$OnActionExpandListenerC100764mR;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.countrygating.viewmodel.CountryGatingViewModel;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.tosgating.viewmodel.ToSGatingViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class ContactPickerFragment extends Hilt_ContactPickerFragment {
    public static boolean A2d;
    public byte A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public MenuItem A05;
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public View A0A;
    public ImageView A0B;
    public ImageView A0C;
    public ListView A0D;
    public ProgressBar A0E;
    public AnonymousClass02Q A0F;
    public AbstractC009504t A0G;
    public AnonymousClass17S A0H;
    public AnonymousClass12P A0I;
    public C22680zT A0J;
    public C14900mE A0K;
    public C15570nT A0L;
    public C48232Fc A0M;
    public C239613r A0N;
    public C15450nH A0O;
    public C18790t3 A0P;
    public AnonymousClass1AR A0Q;
    public TextEmojiLabel A0R;
    public C16170oZ A0S;
    public C18330sH A0T;
    public WaTextView A0U;
    public C238013b A0V;
    public C251118d A0W;
    public C16430p0 A0X;
    public AnonymousClass11I A0Y;
    public AnonymousClass2Dn A0Z;
    public C22330yu A0a;
    public AnonymousClass1BF A0b;
    public AnonymousClass116 A0c;
    public C15550nR A0d;
    public C27131Gd A0e;
    public AnonymousClass10S A0f;
    public C26311Cv A0g;
    public C22700zV A0h;
    public C15610nY A0i;
    public AnonymousClass1J1 A0j;
    public C21270x9 A0k;
    public C36591kA A0l;
    public AnonymousClass3UA A0m;
    public AnonymousClass37V A0n;
    public C628538v A0o;
    public C628138r A0p;
    public C628738x A0q;
    public C627538l A0r;
    public C21240x6 A0s;
    public AnonymousClass118 A0t;
    public C17010q7 A0u;
    public C253318z A0v;
    public C20730wE A0w;
    public AnonymousClass1GY A0x;
    public AnonymousClass16P A0y;
    public AnonymousClass1DP A0z;
    public C15220ml A10;
    public C18640sm A11;
    public AnonymousClass01d A12;
    public C14830m7 A13;
    public C15890o4 A14;
    public C14820m6 A15;
    public AnonymousClass018 A16;
    public CountryGatingViewModel A17;
    public AnonymousClass3CP A18;
    public C32701cb A19 = new C32701cb(null, null, false, false, false, false, false, false);
    public C21550xb A1A;
    public C255019q A1B;
    public C19990v2 A1C;
    public C15680nj A1D;
    public C19780uf A1E;
    public C15600nX A1F;
    public C16490p7 A1G;
    public AnonymousClass1BK A1H;
    public C32731ce A1I;
    public C18470sV A1J;
    public C15370n3 A1K;
    public C25951Bl A1L;
    public AnonymousClass1AN A1M;
    public AnonymousClass19M A1N;
    public C14850m9 A1O;
    public C22050yP A1P;
    public C16120oU A1Q;
    public C20710wC A1R;
    public AbstractC33331dp A1S;
    public C244215l A1T;
    public AnonymousClass1E5 A1U;
    public AbstractC14640lm A1V;
    public C17220qS A1W;
    public C17070qD A1X;
    public C16630pM A1Y;
    public C15860o1 A1Z;
    public AnonymousClass17R A1a;
    public AnonymousClass12F A1b;
    public C252018m A1c;
    public ToSGatingViewModel A1d;
    public AnonymousClass1CY A1e;
    public AnonymousClass198 A1f;
    public C22650zQ A1g;
    public C15690nk A1h;
    public C22190yg A1i;
    public AbstractC14440lR A1j;
    public C26501Ds A1k;
    public AnonymousClass01H A1l;
    public Long A1m;
    public String A1n;
    public String A1o;
    public String A1p;
    public String A1q;
    public String A1r;
    public String A1s;
    public String A1t;
    public String A1u = "";
    public String A1v;
    public String A1w;
    public String A1x;
    public String A1y;
    public ArrayList A1z;
    public ArrayList A20;
    public ArrayList A21;
    public List A22 = new ArrayList();
    public List A23 = new ArrayList();
    public List A24 = new ArrayList();
    public List A25;
    public List A26 = new ArrayList();
    public boolean A27;
    public boolean A28;
    public boolean A29;
    public boolean A2A = false;
    public boolean A2B;
    public boolean A2C;
    public boolean A2D;
    public boolean A2E;
    public boolean A2F;
    public boolean A2G;
    public boolean A2H;
    public boolean A2I;
    public boolean A2J;
    public boolean A2K;
    public boolean A2L;
    public boolean A2M;
    public boolean A2N;
    public boolean A2O;
    public boolean A2P;
    public boolean A2Q;
    public boolean A2R;
    public boolean A2S;
    public boolean A2T;
    public boolean A2U;
    public boolean A2V;
    public final Handler A2W = new Handler(Looper.getMainLooper());
    public final Runnable A2X;
    public final HashSet A2Y = new HashSet();
    public final List A2Z = new ArrayList();
    public final Map A2a = new LinkedHashMap();
    public final Set A2b = new HashSet();
    public final Set A2c;

    public String A1E(C15370n3 r2) {
        return null;
    }

    public String A1F(C15370n3 r2) {
        return null;
    }

    public void A1d(List list) {
    }

    public boolean A1e() {
        return true;
    }

    public boolean A1k() {
        return false;
    }

    public boolean A1m() {
        return false;
    }

    public boolean A1q(C15370n3 r2) {
        return false;
    }

    public ContactPickerFragment() {
        HashSet hashSet = new HashSet();
        this.A2c = hashSet;
        this.A2X = new RunnableBRunnable0Shape0S0100000_I0(hashSet, 21);
    }

    public static Bundle A00(Intent intent) {
        Bundle bundle = new Bundle();
        if (intent.getExtras() != null) {
            bundle.putAll(intent.getExtras());
            bundle.remove("perf_origin");
            bundle.remove("perf_start_time_ns");
            bundle.remove("key_perf_tracked");
        }
        if (intent.hasExtra("android.intent.extra.shortcut.ID")) {
            bundle.putString("jid", intent.getStringExtra("android.intent.extra.shortcut.ID"));
        }
        Bundle bundle2 = new Bundle();
        bundle2.putString("action", intent.getAction());
        bundle2.putString("type", intent.getType());
        bundle2.putBundle("extras", bundle);
        return bundle2;
    }

    public static /* synthetic */ void A01(ContactPickerFragment contactPickerFragment) {
        Intent intent = new Intent();
        ArrayList<String> arrayList = new ArrayList<>(1);
        Jid jid = contactPickerFragment.A1K.A0D;
        AnonymousClass009.A05(jid);
        arrayList.add(jid.getRawString());
        intent.putStringArrayListExtra("jids", arrayList);
        intent.putExtra("file_path", contactPickerFragment.A1B().getString("file_path"));
        contactPickerFragment.A0m.A01(intent);
        C36021jC.A00(contactPickerFragment.A0B(), 2);
        contactPickerFragment.A1P.A0F(false, 1);
        contactPickerFragment.A0m.A00();
    }

    public static /* synthetic */ void A02(ContactPickerFragment contactPickerFragment) {
        Intent intent = new Intent();
        Jid jid = contactPickerFragment.A1K.A0D;
        AnonymousClass009.A05(jid);
        intent.putExtra("contact", jid.getRawString());
        intent.putExtra("message_row_id", contactPickerFragment.A1B().getLong("message_row_id"));
        contactPickerFragment.A0m.A01(intent);
        C36021jC.A00(contactPickerFragment.A0B(), 3);
        contactPickerFragment.A0m.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00cc, code lost:
        if (A1B().getBoolean("request_out_contact_sync", false) == false) goto L_0x00ce;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0m(android.os.Bundle r9) {
        /*
        // Method dump skipped, instructions count: 312
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A0m(android.os.Bundle):void");
    }

    @Override // X.AnonymousClass01E
    public boolean A0o(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        ListView listView = this.A0D;
        if (listView == null) {
            listView = (ListView) this.A09.findViewById(16908298);
            this.A0D = listView;
        }
        C15370n3 ABZ = ((AbstractC116005Tt) listView.getItemAtPosition(adapterContextMenuInfo.position)).ABZ();
        if (ABZ == null || menuItem.getItemId() != 0) {
            return false;
        }
        C238013b r2 = this.A0V;
        ActivityC000900k A0B = A0B();
        Jid A0B2 = ABZ.A0B(UserJid.class);
        AnonymousClass009.A05(A0B2);
        r2.A0C(A0B, (UserJid) A0B2);
        return true;
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    this.A1f.A00();
                    if (Build.VERSION.SDK_INT >= 30) {
                        A1M();
                    }
                } else if (i != 151) {
                    super.A0t(i, i2, intent);
                } else if (i2 == -1) {
                    A1a(null);
                }
            } else if (i2 == -1) {
                this.A0m.A00();
            }
        } else if (this.A1J.A0F()) {
            View view = this.A0A;
            if (view != null) {
                view.setVisibility(8);
                this.A0A = null;
            }
            A1N();
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        Jid A0B;
        bundle.putBoolean("request_sync", this.A2H);
        C15370n3 r1 = this.A1K;
        if (!(r1 == null || (A0B = r1.A0B(AbstractC14640lm.class)) == null)) {
            bundle.putString("jid", A0B.getRawString());
        }
        Map map = this.A2a;
        if (!map.isEmpty()) {
            bundle.putStringArrayList("selected_jids", C15380n4.A06(map.keySet()));
        }
        this.A0M.A03(bundle);
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search);
        this.A05 = icon;
        icon.setShowAsAction(10);
        this.A05.setOnActionExpandListener(new MenuItem$OnActionExpandListenerC100764mR(this));
        this.A05.setVisible(!this.A22.isEmpty());
        if (A1i()) {
            menu.add(0, R.id.menuitem_tell_friend, 0, R.string.tell_a_friend);
            this.A0L.A08();
            menu.add(0, R.id.menuitem_contacts, 0, R.string.menuitem_contacts);
            menu.add(0, R.id.menuitem_refresh, 0, R.string.menuitem_refresh);
            menu.add(0, R.id.menuitem_contacts_help, 0, R.string.settings_help);
        }
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_refresh) {
            this.A0m.A00.A1g(true);
            A1M();
        } else if (itemId == R.id.menuitem_contacts) {
            PackageManager packageManager = A0p().getPackageManager();
            Intent intent = new Intent("android.intent.action.VIEW", ContactsContract.Contacts.CONTENT_URI);
            intent.setComponent(intent.resolveActivity(packageManager));
            if (intent.getComponent() != null) {
                A0v(intent);
                return true;
            }
            try {
                Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage("com.android.contacts");
                if (launchIntentForPackage == null) {
                    this.A0K.A07(R.string.view_contact_unsupport, 0);
                    return true;
                }
                A0v(launchIntentForPackage);
                return true;
            } catch (ActivityNotFoundException e) {
                Log.w("contact_picker/options/system contacts app could not found", e);
                this.A0K.A07(R.string.view_contact_unsupport, 0);
                return true;
            }
        } else if (itemId == R.id.menuitem_tell_friend) {
            AnonymousClass1AR r3 = this.A0Q;
            ActivityC000900k A0B = A0B();
            int i = 4;
            if (this.A2J) {
                i = 7;
            }
            r3.A01(A0B, Integer.valueOf(i));
            return true;
        } else if (itemId == R.id.menuitem_search) {
            this.A0M.A01();
            return true;
        } else if (itemId == R.id.menuitem_contacts_help) {
            A1P();
            return true;
        } else if (itemId == 16908332) {
            this.A0m.A00();
            return true;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009a, code lost:
        if (r5.A1O.A07(1267) == false) goto L_0x009c;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A10(android.os.Bundle r6, android.view.LayoutInflater r7, android.view.ViewGroup r8) {
        /*
            r5 = this;
            boolean r1 = r5.A2C
            r0 = 2131558682(0x7f0d011a, float:1.8742687E38)
            if (r1 == 0) goto L_0x000a
            r0 = 2131559321(0x7f0d0399, float:1.8743983E38)
        L_0x000a:
            r2 = 0
            android.view.View r3 = r7.inflate(r0, r8, r2)
            r5.A09 = r3
            boolean r1 = r5.A2C
            r0 = 2131365858(0x7f0a0fe2, float:1.8351593E38)
            if (r1 == 0) goto L_0x001b
            r0 = 2131365828(0x7f0a0fc4, float:1.8351532E38)
        L_0x001b:
            android.view.View r4 = r3.findViewById(r0)
            android.widget.ImageView r4 = (android.widget.ImageView) r4
            r5.A0B = r4
            X.018 r3 = r5.A16
            android.content.Context r1 = r5.A0p()
            r0 = 2131232325(0x7f080645, float:1.8080756E38)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r1, r0)
            X.2GF r0 = new X.2GF
            r0.<init>(r1, r3)
            r4.setImageDrawable(r0)
            android.widget.ImageView r1 = r5.A0B
            X.36U r0 = new X.36U
            r0.<init>(r5)
            r1.setOnClickListener(r0)
            android.view.View r1 = r5.A09
            r0 = 2131365469(0x7f0a0e5d, float:1.8350804E38)
            android.view.View r0 = r1.findViewById(r0)
            com.whatsapp.TextEmojiLabel r0 = (com.whatsapp.TextEmojiLabel) r0
            r5.A0R = r0
            android.view.View r1 = r5.A09
            r0 = 2131365471(0x7f0a0e5f, float:1.8350808E38)
            android.view.View r1 = r1.findViewById(r0)
            r5.A08 = r1
            boolean r0 = r5.A2C
            if (r0 != 0) goto L_0x007c
            r0 = 2131365470(0x7f0a0e5e, float:1.8350806E38)
            android.view.View r4 = r1.findViewById(r0)
            android.widget.ImageView r4 = (android.widget.ImageView) r4
            X.018 r3 = r5.A16
            android.content.Context r1 = r5.A0p()
            r0 = 2131231297(0x7f080241, float:1.8078671E38)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r1, r0)
            X.2GF r0 = new X.2GF
            r0.<init>(r1, r3)
            r4.setImageDrawable(r0)
        L_0x007c:
            boolean r0 = r5.A2C
            if (r0 == 0) goto L_0x00df
            android.view.View r1 = r5.A09
            r0 = 2131366102(0x7f0a10d6, float:1.8352088E38)
            android.view.View r0 = X.AnonymousClass028.A0D(r1, r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
        L_0x008b:
            r5.A0C = r0
            boolean r0 = r5.A2C
            if (r0 == 0) goto L_0x009c
            X.0m9 r1 = r5.A1O
            r0 = 1267(0x4f3, float:1.775E-42)
            boolean r1 = r1.A07(r0)
            r0 = 1
            if (r1 != 0) goto L_0x009d
        L_0x009c:
            r0 = 0
        L_0x009d:
            r5.A2F = r0
            android.os.Bundle r1 = r5.A1B()
            java.lang.String r0 = "status_chip_clicked"
            boolean r1 = r1.getBoolean(r0, r2)
            boolean r0 = r5.A2F
            if (r0 == 0) goto L_0x00dc
            android.os.Bundle r0 = r5.A1B()
            java.lang.String r3 = "status_distribution"
            android.os.Parcelable r0 = r0.getParcelable(r3)
            X.1ce r0 = (X.C32731ce) r0
            if (r0 == 0) goto L_0x00bf
            r5.A1I = r0
        L_0x00bf:
            if (r1 == 0) goto L_0x00dc
            X.1ce r2 = r5.A1I
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            com.whatsapp.contact.picker.statusprivacy.StatusPrivacyBottomSheetDialogFragment r1 = new com.whatsapp.contact.picker.statusprivacy.StatusPrivacyBottomSheetDialogFragment
            r1.<init>()
            r0.putParcelable(r3, r2)
            r1.A0U(r0)
            X.00k r0 = r5.A0C()
            X.0kN r0 = (X.ActivityC13810kN) r0
            r0.Adm(r1)
        L_0x00dc:
            android.view.View r0 = r5.A09
            return r0
        L_0x00df:
            r0 = 0
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A10(android.os.Bundle, android.view.LayoutInflater, android.view.ViewGroup):android.view.View");
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        C27131Gd r1 = this.A0e;
        if (r1 != null) {
            this.A0f.A04(r1);
            this.A0e = null;
        }
        AnonymousClass2Dn r12 = this.A0Z;
        if (r12 != null) {
            this.A0a.A04(r12);
            this.A0Z = null;
        }
        AnonymousClass1GY r13 = this.A0x;
        if (r13 != null) {
            this.A0y.A04(r13);
            this.A0x = null;
        }
        AbstractC33331dp r14 = this.A1S;
        if (r14 != null) {
            this.A1T.A04(r14);
            this.A1S = null;
        }
        this.A0j.A00();
        AnonymousClass37V r0 = this.A0n;
        if (r0 != null) {
            r0.A03(true);
            this.A0n = null;
        }
        C627538l r02 = this.A0r;
        if (r02 != null) {
            r02.A03(true);
            this.A0r = null;
        }
        C628738x r03 = this.A0q;
        if (r03 != null) {
            r03.A03(true);
            this.A0q = null;
        }
        C628538v r04 = this.A0o;
        if (r04 != null) {
            r04.A03(true);
            this.A0o = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (!A2d) {
            this.A0l.notifyDataSetChanged();
            A2d = false;
        }
    }

    @Override // com.whatsapp.contact.picker.Hilt_ContactPickerFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A0m = ((AnonymousClass1L2) context).ADO();
        } catch (ClassCastException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append(context);
            sb.append(" must implement ContactPickerFragment$HostProvider");
            throw new ClassCastException(sb.toString());
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        AnonymousClass01J r4 = (AnonymousClass01J) AnonymousClass01M.A00(A01(), AnonymousClass01J.class);
        this.A1d = (ToSGatingViewModel) new AnonymousClass02A(this).A00(ToSGatingViewModel.class);
        this.A17 = (CountryGatingViewModel) new AnonymousClass02A(this).A00(CountryGatingViewModel.class);
        if (bundle != null) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(bundle.getString("jid"));
            if (A01 != null) {
                this.A1K = this.A0d.A0B(A01);
            }
            ArrayList<String> stringArrayList = bundle.getStringArrayList("selected_jids");
            if (stringArrayList != null) {
                List<AbstractC14640lm> A07 = C15380n4.A07(AbstractC14640lm.class, stringArrayList);
                if (!A07.isEmpty()) {
                    Map map = this.A2a;
                    map.clear();
                    for (AbstractC14640lm r1 : A07) {
                        C15370n3 A0A = this.A0d.A0A(r1);
                        if (A0A != null) {
                            map.put(r1, A0A);
                        }
                    }
                }
            }
        }
        A0M();
        this.A2C = this.A1O.A07(815);
        this.A2D = this.A1O.A07(1283);
        this.A1l = C18000rk.A00(r4.A4v);
        this.A1I = new C32731ce(this.A1J.A06(), this.A1J.A07(), this.A1J.A03.A00("status_distribution", 0), false);
    }

    public Dialog A1A(int i) {
        ActivityC000900k A0B = A0B();
        if (i == 1) {
            Map map = this.A2a;
            C15370n3 r11 = this.A1K;
            C15610nY r10 = this.A0i;
            ArrayList arrayList = this.A20;
            return C63273Ay.A00(A0B, new DialogInterface.OnCancelListener() { // from class: X.4f0
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    ContactPickerFragment contactPickerFragment = ContactPickerFragment.this;
                    C36021jC.A00(contactPickerFragment.A0B(), 1);
                    AbstractC009504t r0 = contactPickerFragment.A0G;
                    if (r0 != null) {
                        r0.A05();
                    }
                }
            }, new DialogInterface.OnClickListener() { // from class: X.4fV
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ContactPickerFragment contactPickerFragment = ContactPickerFragment.this;
                    C36021jC.A00(contactPickerFragment.A0B(), 1);
                    contactPickerFragment.A1a(null);
                }
            }, new DialogInterface.OnClickListener() { // from class: X.4fW
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ContactPickerFragment contactPickerFragment = ContactPickerFragment.this;
                    C36021jC.A00(contactPickerFragment.A0B(), 1);
                    AbstractC009504t r0 = contactPickerFragment.A0G;
                    if (r0 != null) {
                        r0.A05();
                    }
                }
            }, r10, r11, this.A1N, arrayList, map);
        } else if (i == 2) {
            boolean A0K = this.A1K.A0K();
            int i2 = R.string.confirm_forward_msg;
            if (A0K) {
                i2 = R.string.group_confirm_forward_msg;
            }
            String A0J = A0J(i2, this.A0i.A04(this.A1K));
            C004802e r2 = new C004802e(A0p());
            r2.A0A(AbstractC36671kL.A05(A0B, this.A1N, A0J));
            r2.A0B(true);
            r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fX
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    C36021jC.A00(ContactPickerFragment.this.A0B(), 2);
                }
            });
            r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fa
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    ContactPickerFragment.A01(ContactPickerFragment.this);
                }
            });
            r2.A08(new DialogInterface.OnCancelListener() { // from class: X.4f1
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    C36021jC.A00(ContactPickerFragment.this.A0B(), 2);
                }
            });
            return r2.create();
        } else if (i != 3) {
            return null;
        } else {
            String A0J2 = A0J(R.string.group_confirm_set_icon, this.A0i.A04(this.A1K));
            C004802e r22 = new C004802e(A0p());
            r22.A0A(AbstractC36671kL.A05(A0B, this.A1N, A0J2));
            r22.A0B(true);
            r22.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fY
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    C36021jC.A00(ContactPickerFragment.this.A0B(), 3);
                }
            });
            r22.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fZ
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    ContactPickerFragment.A02(ContactPickerFragment.this);
                }
            });
            r22.A08(new DialogInterface.OnCancelListener() { // from class: X.4ez
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    C36021jC.A00(ContactPickerFragment.this.A0B(), 3);
                }
            });
            return r22.create();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r2 == null) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.Bundle A1B() {
        /*
            r3 = this;
            android.os.Bundle r1 = r3.A05
            if (r1 == 0) goto L_0x001b
            java.lang.String r0 = "extras"
            android.os.Bundle r2 = r1.getBundle(r0)
            r1 = 1
            if (r2 != 0) goto L_0x000e
        L_0x000d:
            r1 = 0
        L_0x000e:
            java.lang.String r0 = "extras should not be null (it should be an empty bundle if there are nothing)"
            X.AnonymousClass009.A0A(r0, r1)
            if (r2 != 0) goto L_0x001a
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
        L_0x001a:
            return r2
        L_0x001b:
            r2 = 0
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1B():android.os.Bundle");
    }

    public final View A1C(int i, int i2) {
        View inflate = A04().inflate(R.layout.contact_picker_row_small, (ViewGroup) this.A09, false);
        AnonymousClass3AX.A00(inflate, i, 0, R.drawable.gray_circle, i2);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.contactpicker_row_photo);
        if (imageView != null) {
            AnonymousClass2GE.A07(imageView, inflate.getResources().getColor(R.color.wds_cool_gray_400));
        }
        return inflate;
    }

    public final View A1D(View.OnClickListener onClickListener, int i, int i2, int i3, int i4) {
        View inflate = A04().inflate(R.layout.contact_picker_row_small, (ViewGroup) null, false);
        AnonymousClass3AX.A00(inflate, i, i2, i3, i4);
        inflate.setOnClickListener(onClickListener);
        FrameLayout frameLayout = new FrameLayout(A0p());
        frameLayout.addView(inflate);
        this.A2Z.add(inflate);
        AnonymousClass028.A0a(frameLayout, 2);
        return frameLayout;
    }

    public final List A1G() {
        Map map = this.A2a;
        ArrayList arrayList = new ArrayList(map.size());
        for (C15370n3 r1 : map.values()) {
            arrayList.add(r1.A0B(AbstractC14640lm.class));
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x008b, code lost:
        if (r14.A2T != false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ea, code lost:
        if (r14.A1O.A07(691) == false) goto L_0x00ec;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1H() {
        /*
        // Method dump skipped, instructions count: 901
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1H():void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:106|(2:108|(1:110)(4:132|134|147|148))(2:112|(1:116))|111|282|117|(1:119)(3:120|(4:123|(3:299|127|(2:131|278)(1:305))|302|121)|298)|135|(4:137|(3:139|(1:141)|142)|143|(1:145)(2:211|220))(1:(3:150|(1:152)(6:154|(4:158|(1:160)(1:163)|161|(2:285|164))|133|134|147|148)|153)(4:166|(2:168|(1:170))(3:204|(2:207|205)|311)|171|(1:210)(5:175|(2:176|(4:178|(2:287|180)|194|(2:308|196)(2:198|(1:203)(2:307|202)))(5:306|181|(1:189)(1:193)|190|(1:192)(3:269|(6:272|283|273|314|313|270)|312)))|197|147|148)))|146|147|148) */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x06ea  */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x0708  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1I() {
        /*
        // Method dump skipped, instructions count: 2233
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1I():void");
    }

    public void A1J() {
        if (this.A0G == null) {
            AnonymousClass02Q r1 = this.A0F;
            if (r1 == null) {
                r1 = new C66663Ol(this);
                this.A0F = r1;
            }
            this.A0G = this.A0m.A00.A1W(r1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0074, code lost:
        if (r49.A28 != false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00f7, code lost:
        if (r49.A17.A02.A07(1105) != false) goto L_0x008f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1K() {
        /*
        // Method dump skipped, instructions count: 250
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1K():void");
    }

    public final void A1L() {
        C627538l r0 = this.A0r;
        if (r0 != null) {
            r0.A03(true);
        }
        C628738x r02 = this.A0q;
        if (r02 != null) {
            r02.A03(true);
            this.A0q = null;
        }
        HashSet hashSet = this.A2Y;
        AbstractC14640lm r03 = this.A1V;
        boolean z = this.A2L;
        boolean z2 = this.A2I;
        boolean z3 = this.A2V;
        boolean z4 = this.A2K;
        boolean z5 = this.A2N;
        boolean z6 = this.A2U;
        boolean z7 = this.A2J;
        boolean z8 = this.A2M;
        boolean z9 = this.A2Q;
        boolean z10 = this.A2T;
        boolean z11 = this.A2S;
        boolean z12 = this.A28;
        C14850m9 r7 = this.A1O;
        C15550nR r6 = this.A0d;
        C17070qD r5 = this.A1X;
        C627538l r2 = new C627538l(this.A0T, this.A0V, r6, this, this.A1E, this.A1F, r7, r03, r5, hashSet, z, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12);
        this.A0r = r2;
        this.A1j.Ab5(r2, new Void[0]);
    }

    public final void A1M() {
        C15570nT r0 = this.A0L;
        r0.A08();
        if (r0.A00 == null) {
            this.A0K.A07(R.string.finish_registration_first, 1);
            return;
        }
        this.A0L.A08();
        this.A0m.A00.A1g(true);
        AnonymousClass37V r02 = this.A0n;
        if (r02 != null) {
            r02.A03(true);
        }
        AnonymousClass37V r2 = new AnonymousClass37V(this.A0d, this, this.A0w, this.A1D);
        this.A0n = r2;
        this.A1j.Aaz(r2, new Void[0]);
    }

    public final void A1N() {
        String A04;
        Map map = this.A2a;
        ArrayList arrayList = new ArrayList(map.size());
        int i = 0;
        if (this.A2C) {
            C15610nY r5 = this.A0i;
            List A1G = A1G();
            arrayList = r5.A0H(A01(), this.A1I, A1G);
        } else {
            for (C15370n3 r1 : map.values()) {
                if (C15380n4.A0N(r1.A0D)) {
                    A04 = A0I(R.string.my_status);
                } else {
                    A04 = this.A0i.A04(r1);
                }
                if (A04 != null) {
                    arrayList.add(0, A04);
                }
            }
        }
        this.A0R.A0G(null, C32721cd.A00(this.A0i.A04, arrayList, false));
        C42941w9.A0D((HorizontalScrollView) this.A08.findViewById(R.id.recipients_scroller), this.A16);
        if (!TextUtils.isEmpty(this.A1u)) {
            this.A0B.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(A0p(), R.drawable.ic_action_arrow_next), this.A16));
        }
        if (this.A2C && this.A0C != null) {
            Iterator it = map.values().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (C15380n4.A0N(((C15370n3) it.next()).A0D)) {
                        break;
                    }
                } else {
                    i = 8;
                    break;
                }
            }
            this.A0C.setVisibility(i);
        }
    }

    public final void A1O() {
        String A0I;
        if (this.A2C) {
            int size = this.A2a.size();
            if (size == 0) {
                int i = R.string.select_contacts;
                if (this.A2P) {
                    i = R.string.dm_select_chats;
                }
                A0I = A01().getString(i);
            } else {
                int i2 = R.plurals.n_contacts_selected;
                if (this.A2P) {
                    i2 = R.plurals.n_chats_selected;
                }
                A0I = this.A16.A0I(new Object[]{Integer.valueOf(size)}, i2, (long) size);
            }
            this.A0m.A00.A1U().A0I(A0I);
        }
    }

    public final void A1P() {
        if (this.A1O.A07(674)) {
            this.A10.A00(A0B(), "missingcontacts");
            return;
        }
        Context A0p = A0p();
        Intent intent = new Intent();
        intent.setClassName(A0p.getPackageName(), "com.whatsapp.contact.picker.ContactPickerHelp");
        A0v(intent);
    }

    public final void A1Q() {
        if (this.A04 + 3500 < SystemClock.elapsedRealtime()) {
            this.A04 = SystemClock.elapsedRealtime();
            AnonymousClass1CY r0 = this.A1e;
            A0p();
            r0.A00();
        }
    }

    public final void A1R() {
        if (this.A0G != null) {
            Map map = this.A2a;
            boolean isEmpty = map.isEmpty();
            AbstractC009504t r3 = this.A0G;
            if (isEmpty) {
                r3.A05();
            } else {
                r3.A0B(this.A16.A0J().format((long) map.size()));
            }
        }
    }

    public void A1S(int i) {
        if (A0p() == null) {
            return;
        }
        if (i == R.string.directly_entered_number_not_whatsappable || i == R.string.directly_entered_number_invalid || i == R.string.directly_entered_number_sync_failed || i == R.string.directly_entered_number_not_checked || i == R.string.directly_entered_number_is_missing_country_code || i == R.string.directly_entered_number_invalid_length || i == R.string.directly_entered_number_invalid_length_without_country_name || i == R.string.directly_entered_number_too_long || i == R.string.directly_entered_number_too_long_without_country_name || i == R.string.directly_entered_number_too_short || i == R.string.directly_entered_number_too_short_without_country_name || i == R.string.directly_entered_number_too_short_without_country_code) {
            A0v(C14960mK.A04(A0p()));
            this.A0m.A00();
        }
    }

    public final void A1T(int i) {
        if (this.A1O.A07(913)) {
            ActivityC000900k A0C = A0C();
            Intent intent = new Intent();
            intent.setClassName(A0C.getPackageName(), "com.whatsapp.contact.ContactFormActivity");
            A0v(intent);
            return;
        }
        Intent intent2 = new Intent("android.intent.action.INSERT");
        intent2.setType("vnd.android.cursor.dir/contact");
        try {
            startActivityForResult(intent2, 3);
            this.A1f.A02(true, i);
        } catch (ActivityNotFoundException unused) {
            this.A0K.A07(R.string.unimplemented, 0);
        }
    }

    public final void A1U(Intent intent) {
        if (this.A1O.A07(508) && !TextUtils.isEmpty(this.A1s)) {
            intent.putExtra("entry_point_conversion_source", this.A1s);
            if (!TextUtils.isEmpty(this.A1r)) {
                intent.putExtra("entry_point_conversion_app", this.A1r);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0119, code lost:
        if (r12.getQueryParameter("src") != null) goto L_0x011b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1V(android.net.Uri r12, android.util.Pair r13, X.C32701cb r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 564
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1V(android.net.Uri, android.util.Pair, X.1cb, java.lang.String, java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01f1, code lost:
        if (r4 > 0) goto L_0x01f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0214, code lost:
        if (r19.A2A == false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0226, code lost:
        if (r7 != false) goto L_0x0228;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1W(android.view.View r20, X.C15370n3 r21) {
        /*
        // Method dump skipped, instructions count: 1120
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1W(android.view.View, X.0n3):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
        if (r13.A2N != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f6, code lost:
        if (X.AbstractActivityC452520u.A09(r13.A0J, r4, r5.substring(r4.length())) == 1) goto L_0x00f8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1X(X.AnonymousClass4NP r14) {
        /*
        // Method dump skipped, instructions count: 398
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1X(X.4NP):void");
    }

    public void A1Y(C91544Sd r3) {
        List list;
        this.A22 = r3.A00;
        this.A26 = r3.A03;
        this.A23 = r3.A01;
        this.A24 = r3.A02;
        if (this.A2S && (list = r3.A04) != null) {
            A1d(list);
        }
        MenuItem menuItem = this.A05;
        if (menuItem != null) {
            menuItem.setVisible(!this.A22.isEmpty());
        }
        A1K();
    }

    public void A1Z(C15370n3 r5) {
        if (this.A0t.A04.A07(1666)) {
            C17010q7 r1 = this.A0u;
            r1.A00.A08();
            AbstractC14440lR r3 = r1.A07;
            RunnableBRunnable0Shape2S0200000_I0_2 runnableBRunnable0Shape2S0200000_I0_2 = new RunnableBRunnable0Shape2S0200000_I0_2(r1, 43, r5);
            StringBuilder sb = new StringBuilder("ContactDiscoverySyncHelper/syncOutContact");
            sb.append(r5.A0D);
            r3.Ab4(runnableBRunnable0Shape2S0200000_I0_2, sb.toString());
        }
        A1b(C248917h.A01(r5));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x029c, code lost:
        if (r16 != null) goto L_0x0070;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1a(X.C15370n3 r16) {
        /*
        // Method dump skipped, instructions count: 805
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1a(X.0n3):void");
    }

    public final void A1b(String str) {
        AnonymousClass1AR r6 = this.A0Q;
        Activity A00 = AnonymousClass12P.A00(A0p());
        StringBuilder sb = new StringBuilder("sms:");
        sb.append(str);
        Uri parse = Uri.parse(sb.toString());
        String A0J = A0J(R.string.tell_a_friend_sms, "https://whatsapp.com/dl/");
        int i = 14;
        if (this.A2J) {
            i = 15;
        }
        r6.A00(A00, parse, Integer.valueOf(i), A0J);
    }

    public void A1c(String str, List list) {
        String str2;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C15370n3 r3 = (C15370n3) it.next();
            CharSequence A00 = C15610nY.A00(A0p(), this.A16, r3);
            if (A00 != null) {
                str2 = A00.toString();
            } else {
                str2 = null;
            }
            arrayList.add(new C65983Lv(str2, C248917h.A01(r3)));
        }
        if (!arrayList.isEmpty()) {
            PhoneNumberSelectionDialog A002 = PhoneNumberSelectionDialog.A00(A0J(R.string.message_contact_name, str), arrayList);
            C004902f r1 = new C004902f(A0E());
            r1.A09(A002, "phone_number_selection_dialog");
            r1.A02();
        }
    }

    public boolean A1f() {
        if (!this.A2Q && !this.A2J) {
            return false;
        }
        this.A0L.A08();
        return true;
    }

    public boolean A1g() {
        return this.A2Q;
    }

    public boolean A1h() {
        return this.A2Q || this.A2J;
    }

    public boolean A1i() {
        return this.A2Q || this.A2J;
    }

    public boolean A1j() {
        return this.A2I || this.A2J || (this.A2Q && !this.A1O.A07(691)) || this.A2M;
    }

    public boolean A1l() {
        if (this.A2Q && this.A0t.A04.A07(1666)) {
            AnonymousClass118 r0 = this.A0t;
            r0.A05.A07(new C853042f());
        }
        Intent intent = new Intent();
        intent.putExtra("jids", C15380n4.A06(A1G()));
        intent.putExtra("status_distribution", this.A1I);
        if (this.A2P) {
            intent.putExtra("all_contacts_count", this.A22.size());
        }
        this.A0m.A00.setResult(0, intent);
        C48232Fc r02 = this.A0M;
        if (r02 == null || !r02.A05()) {
            if (this.A2N) {
                this.A1P.A0F(true, this.A2a.size());
            }
            return false;
        }
        this.A0M.A04(true);
        return true;
    }

    public boolean A1n() {
        return this.A2I || this.A2U || this.A2J || this.A2Q || this.A2M;
    }

    public boolean A1o() {
        return this.A2Q || this.A2J || this.A2I;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        if (r1.A0C((com.whatsapp.jid.GroupJid) r0) == false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c2, code lost:
        if (r6.A27 != false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cb, code lost:
        if (r6.A21 == null) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1p(android.content.Intent r7, X.C15370n3 r8) {
        /*
        // Method dump skipped, instructions count: 338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.ContactPickerFragment.A1p(android.content.Intent, X.0n3):boolean");
    }

    @Override // X.AnonymousClass01E, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        C15370n3 ABZ;
        AbstractC116005Tt r0 = (AbstractC116005Tt) ((AdapterView) view).getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (r0 != null && (ABZ = r0.ABZ()) != null && this.A0V.A0I((UserJid) ABZ.A0B(UserJid.class))) {
            contextMenu.add(0, 0, 0, A0J(R.string.block_list_menu_unblock, this.A0i.A04(ABZ)));
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        }
    }
}
