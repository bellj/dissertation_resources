package com.whatsapp.contact.picker;

import X.AbstractActivityC36611kC;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass02A;
import X.AnonymousClass1AW;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14650lo;
import X.C16590pI;
import X.C22680zT;
import X.C53812fG;
import X.C53942fq;
import android.content.Intent;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class ContactsAttachmentSelector extends AbstractActivityC36611kC {
    public C22680zT A00;
    public C14650lo A01;
    public C53812fG A02;
    public C16590pI A03;
    public AnonymousClass1AW A04;
    public boolean A05;

    public ContactsAttachmentSelector() {
        this(0);
    }

    public ContactsAttachmentSelector(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 55);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A03 = C12970iu.A0X(A1M);
            this.A04 = (AnonymousClass1AW) A1M.AAX.get();
            this.A00 = (C22680zT) A1M.AGW.get();
            this.A01 = C12980iv.A0Y(A1M);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 8) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            setResult(-1, intent);
            finish();
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C53812fG r0 = (C53812fG) new AnonymousClass02A(new C53942fq(this), this).A00(C53812fG.class);
        this.A02 = r0;
        C12960it.A17(this, r0.A03, 37);
        C12960it.A18(this, this.A02.A00, 73);
    }
}
