package com.whatsapp.contact.picker;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12970iu;
import android.os.Bundle;
import android.view.MenuItem;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ContactPickerHelp extends ActivityC13790kL {
    public boolean A00;

    public ContactPickerHelp() {
        this(0);
    }

    public ContactPickerHelp(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 54);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.contacts_help);
        C12970iu.A0N(this).A0M(true);
        setContentView(R.layout.contact_picker_help);
        findViewById(R.id.scroll_view).post(new RunnableBRunnable0Shape15S0100000_I1_1(this, 22));
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }
}
