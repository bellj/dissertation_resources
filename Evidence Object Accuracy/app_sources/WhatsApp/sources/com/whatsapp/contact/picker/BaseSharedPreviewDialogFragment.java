package com.whatsapp.contact.picker;

import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass2FV;
import X.AnonymousClass2GF;
import X.C14330lG;
import X.C14900mE;
import X.C15380n4;
import X.C15550nR;
import X.C15610nY;
import X.C22190yg;
import X.C32721cd;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class BaseSharedPreviewDialogFragment extends Hilt_BaseSharedPreviewDialogFragment {
    public View A00;
    public View A01;
    public ViewGroup A02;
    public ImageButton A03;
    public LinearLayout A04;
    public LinearLayout A05;
    public RelativeLayout A06;
    public C14330lG A07;
    public C14900mE A08;
    public C15550nR A09;
    public C15610nY A0A;
    public AnonymousClass2FV A0B;
    public AnonymousClass01d A0C;
    public AnonymousClass018 A0D;
    public EmojiSearchContainer A0E;
    public C22190yg A0F;
    public WebPagePreviewView A0G;
    public List A0H;

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        super.A0m(bundle);
        Toolbar toolbar = (Toolbar) this.A00.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AnonymousClass00T.A00(A0C(), R.color.primary));
        toolbar.A0C(A0B(), 2131952341);
        toolbar.setTitle(R.string.send_to);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass00T.A04(A0B(), R.drawable.ic_back), this.A0D));
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 21));
        toolbar.setNavigationContentDescription(R.string.back);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = ((DialogFragment) this).A03.getWindow();
            AnonymousClass009.A05(window);
            window.clearFlags(67108864);
            window.setStatusBarColor(AnonymousClass00T.A00(A0B(), R.color.primary_dark));
        }
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            Window window = dialog.getWindow();
            AnonymousClass009.A05(window);
            window.setLayout(-1, -1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(-16777216));
            dialog.getWindow().getAttributes().windowAnimations = R.style.FullScreenDialogFragmentAnimation;
        }
        super.A0s();
    }

    @Override // X.AnonymousClass01E
    public void A0x(Menu menu) {
        menu.findItem(R.id.menuitem_search).setVisible(false);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        A1B();
        return true;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String A04;
        this.A00 = A0C().getLayoutInflater().inflate(R.layout.base_shared_preview_fragment, viewGroup, false);
        A0M();
        A1D(2, 0);
        this.A04 = (LinearLayout) this.A00.findViewById(R.id.top_layout);
        this.A05 = (LinearLayout) this.A00.findViewById(R.id.view_placeholder);
        this.A06 = (RelativeLayout) this.A00.findViewById(R.id.footer);
        this.A03 = (ImageButton) this.A00.findViewById(R.id.send);
        this.A02 = (ViewGroup) this.A00.findViewById(R.id.web_page_preview_container);
        this.A01 = this.A00.findViewById(R.id.link_preview_divider);
        this.A02.setVisibility(8);
        this.A01.setVisibility(8);
        EmojiSearchContainer emojiSearchContainer = (EmojiSearchContainer) this.A00.findViewById(R.id.emoji_search_container);
        this.A0E = emojiSearchContainer;
        emojiSearchContainer.setVisibility(8);
        ArrayList arrayList = new ArrayList();
        for (AbstractC14640lm r2 : this.A0H) {
            if (C15380n4.A0N(r2)) {
                A04 = A0I(R.string.my_status);
            } else {
                A04 = this.A0A.A04(this.A09.A0B(r2));
            }
            arrayList.add(0, A04);
        }
        ((TextEmojiLabel) this.A00.findViewById(R.id.recipients)).A0G(null, C32721cd.A00(this.A0A.A04, arrayList, false));
        A1K();
        return this.A00;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        if (!(A0B() instanceof AnonymousClass2FV)) {
            throw new RuntimeException("Activity must implement BaseSharedPreviewDialogFragment.Host");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ArrayList<String> stringArrayList = A03().getStringArrayList("jids");
        AnonymousClass009.A06(stringArrayList, "null jids");
        this.A0H = C15380n4.A07(AbstractC14640lm.class, stringArrayList);
        AnonymousClass2FV r0 = (AnonymousClass2FV) A0B();
        this.A0B = r0;
        if (r0 != null) {
            ((ContactPicker) r0).A03 = this;
        }
        A1D(0, R.style.FullScreenDialogNoFloating);
        return super.A1A(bundle);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r0 != 0) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1K() {
        /*
            r3 = this;
            android.widget.RelativeLayout r0 = r3.A06
            android.view.ViewGroup$LayoutParams r2 = r0.getLayoutParams()
            com.whatsapp.webpagepreview.WebPagePreviewView r0 = r3.A0G
            if (r0 == 0) goto L_0x0013
            int r0 = r0.getVisibility()
            r1 = 2131166876(0x7f07069c, float:1.794801E38)
            if (r0 == 0) goto L_0x0016
        L_0x0013:
            r1 = 2131166877(0x7f07069d, float:1.7948012E38)
        L_0x0016:
            android.content.res.Resources r0 = r3.A02()
            int r1 = r0.getDimensionPixelSize(r1)
            r2.height = r1
            android.widget.RelativeLayout r0 = r3.A06
            int r0 = r0.getHeight()
            if (r1 == r0) goto L_0x002d
            android.widget.RelativeLayout r0 = r3.A06
            r0.setLayoutParams(r2)
        L_0x002d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment.A1K():void");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        AnonymousClass2FV r1 = this.A0B;
        if (r1 != null) {
            ((ContactPicker) r1).A03 = null;
        }
        super.onDismiss(dialogInterface);
    }
}
