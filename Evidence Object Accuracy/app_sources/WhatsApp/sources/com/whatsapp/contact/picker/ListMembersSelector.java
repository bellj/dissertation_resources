package com.whatsapp.contact.picker;

import X.AbstractActivityC36611kC;
import X.AbstractC005102i;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12U;
import X.AnonymousClass161;
import X.AnonymousClass2FL;
import X.C12980iv;
import X.C15680nj;
import X.C20710wC;
import X.C22230yk;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ListMembersSelector extends AbstractActivityC36611kC {
    public C15680nj A00;
    public C20710wC A01;
    public C22230yk A02;
    public AnonymousClass161 A03;
    public AnonymousClass12U A04;
    public boolean A05;

    public ListMembersSelector() {
        this(0);
    }

    public ListMembersSelector(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 56);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A04 = ActivityC13830kP.A1N(A1M);
            this.A02 = (C22230yk) A1M.ANT.get();
            this.A01 = C12980iv.A0e(A1M);
            this.A00 = (C15680nj) A1M.A4e.get();
            this.A03 = (AnonymousClass161) A1M.AJ4.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 150) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 != -1) {
            Log.i("listmembersselector/permissions denied");
            finish();
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        A1U.A0M(true);
        A1U.A0A(R.string.new_list);
        if (bundle == null && !((AbstractActivityC36611kC) this).A0I.A00()) {
            RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_new_broadcast_request, R.string.permission_contacts_access_on_new_broadcast);
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
    }
}
