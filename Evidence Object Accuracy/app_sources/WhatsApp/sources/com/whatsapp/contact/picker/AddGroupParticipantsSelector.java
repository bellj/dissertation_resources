package com.whatsapp.contact.picker;

import X.AbstractActivityC36611kC;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass04v;
import X.AnonymousClass11A;
import X.AnonymousClass2FL;
import X.AnonymousClass5UJ;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15580nU;
import X.C15600nX;
import X.C253118x;
import X.C28801Pb;
import X.C64043Ea;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.contact.picker.AddGroupParticipantsSelector;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
public class AddGroupParticipantsSelector extends AbstractActivityC36611kC {
    public C15600nX A00;
    public AnonymousClass11A A01;
    public C15580nU A02;
    public C253118x A03;
    public boolean A04;
    public final AnonymousClass5UJ A05;
    public final Set A06;

    @Override // X.AbstractActivityC36611kC
    public void A2y(int i) {
    }

    public AddGroupParticipantsSelector() {
        this(0);
        this.A06 = C12970iu.A12();
        this.A05 = new AnonymousClass5UJ() { // from class: X.3Xd
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r4) {
                AddGroupParticipantsSelector addGroupParticipantsSelector = AddGroupParticipantsSelector.this;
                if (addGroupParticipantsSelector.A02.equals(r4) && ((ActivityC13810kN) addGroupParticipantsSelector).A0C.A07(1863)) {
                    addGroupParticipantsSelector.A39((TextEmojiLabel) addGroupParticipantsSelector.findViewById(R.id.multiple_contact_picker_warning_text), addGroupParticipantsSelector.A02);
                }
            }
        };
    }

    public AddGroupParticipantsSelector(int i) {
        this.A04 = false;
        ActivityC13830kP.A1P(this, 53);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A03 = (C253118x) A1M.AAW.get();
            this.A01 = (AnonymousClass11A) A1M.A8o.get();
            this.A00 = C12980iv.A0d(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A30(C64043Ea r7, C15370n3 r8) {
        super.A30(r7, r8);
        boolean A07 = C15370n3.A07(r8, this.A06);
        boolean A0I = ((AbstractActivityC36611kC) this).A0F.A0I((UserJid) r8.A0B(UserJid.class));
        View view = r7.A00;
        AnonymousClass028.A0g(view, new AnonymousClass04v());
        if (A07 || A0I) {
            TextEmojiLabel textEmojiLabel = r7.A02;
            int i = R.string.tap_unblock;
            if (A07) {
                i = R.string.contact_already_in_group;
            }
            textEmojiLabel.setText(i);
            r7.A01.setEnabled(false);
            textEmojiLabel.setTypeface(null, 2);
            textEmojiLabel.setVisibility(0);
            C28801Pb.A00(this, r7.A03, R.color.list_item_disabled);
            if (A07) {
                view.setOnClickListener(null);
                view.setClickable(false);
                view.setFocusable(true);
                return;
            }
            return;
        }
        r7.A02.setTypeface(null, 0);
        C28801Pb.A00(this, r7.A03, R.color.list_item_title);
    }

    @Override // X.AbstractActivityC36611kC
    public void A32(C15370n3 r3) {
        if (!C15370n3.A07(r3, this.A06)) {
            super.A32(r3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1 != 1) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A39(com.whatsapp.TextEmojiLabel r5, X.C15580nU r6) {
        /*
            r4 = this;
            X.0m9 r2 = r4.A0C
            X.0nR r0 = r4.A0J
            X.0n3 r1 = r0.A0B(r6)
            r0 = 1863(0x747, float:2.61E-42)
            boolean r0 = r2.A07(r0)
            r2 = 1
            if (r0 == 0) goto L_0x0018
            int r1 = r1.A03
            r0 = 2131886238(0x7f12009e, float:1.940705E38)
            if (r1 == r2) goto L_0x001b
        L_0x0018:
            r0 = 2131886237(0x7f12009d, float:1.9407047E38)
        L_0x001b:
            java.lang.String r3 = r4.getString(r0)
            X.18x r2 = r4.A03
            r0 = 14
            com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1 r1 = new com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1
            r1.<init>(r4, r0, r6)
            java.lang.String r0 = "edit_group_settings"
            android.text.SpannableStringBuilder r0 = r2.A02(r4, r1, r3, r0)
            r5.setText(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.AddGroupParticipantsSelector.A39(com.whatsapp.TextEmojiLabel, X.0nU):void");
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.A02 = C15580nU.A04(getIntent().getStringExtra("gid"));
        super.onCreate(bundle);
        C15580nU r1 = this.A02;
        if (r1 != null) {
            this.A06.addAll(new HashSet(this.A00.A02(r1).A06().A00));
            AnonymousClass11A r0 = this.A01;
            r0.A00.add(this.A05);
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass11A r0 = this.A01;
        r0.A00.remove(this.A05);
    }
}
