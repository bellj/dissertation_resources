package com.whatsapp.contact.picker;

import X.AnonymousClass018;
import X.C102244op;
import X.C252718t;
import X.C28141Kv;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.collections.observablelistview.ObservableListView;

/* loaded from: classes2.dex */
public class BidiContactListView extends ObservableListView {
    public AnonymousClass018 A00;
    public C252718t A01;
    public boolean A02;

    public BidiContactListView(Context context) {
        super(context);
        A01();
        A00();
    }

    public BidiContactListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
        A00();
    }

    public BidiContactListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        A00();
    }

    public BidiContactListView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A01();
    }

    private void A00() {
        int dimensionPixelSize;
        Resources resources;
        int i;
        if (C28141Kv.A00(this.A00)) {
            setVerticalScrollbarPosition(1);
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_right);
            resources = getResources();
            i = R.dimen.contact_list_padding_left;
        } else {
            setVerticalScrollbarPosition(2);
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_left);
            resources = getResources();
            i = R.dimen.contact_list_padding_right;
        }
        setPadding(dimensionPixelSize, 0, resources.getDimensionPixelSize(i), 0);
        setFastScrollAlwaysVisible(true);
        setScrollBarStyle(33554432);
        setFastScrollEnabled(true);
        setScrollbarFadingEnabled(true);
        this.A08 = new C102244op(this);
    }
}
