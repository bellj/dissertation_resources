package com.whatsapp.contact.picker;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass116;
import X.AnonymousClass118;
import X.AnonymousClass11I;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass16P;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1AN;
import X.AnonymousClass1AR;
import X.AnonymousClass1BF;
import X.AnonymousClass1BK;
import X.AnonymousClass1CY;
import X.AnonymousClass1DP;
import X.AnonymousClass1E5;
import X.AnonymousClass3CP;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15220ml;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15690nk;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16490p7;
import X.C16630pM;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C18330sH;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C19780uf;
import X.C19990v2;
import X.C20710wC;
import X.C20730wE;
import X.C21240x6;
import X.C21270x9;
import X.C21550xb;
import X.C22050yP;
import X.C22190yg;
import X.C22330yu;
import X.C22650zQ;
import X.C22680zT;
import X.C22700zV;
import X.C238013b;
import X.C239613r;
import X.C244215l;
import X.C251118d;
import X.C252018m;
import X.C253318z;
import X.C255019q;
import X.C25951Bl;
import X.C26311Cv;
import X.C26501Ds;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_ContactPickerFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A19()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.Hilt_ContactPickerFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            ContactPickerFragment contactPickerFragment = (ContactPickerFragment) this;
            C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r2 = r3.A0Y;
            ((WaFragment) contactPickerFragment).A00 = (AnonymousClass182) r2.A94.get();
            ((WaFragment) contactPickerFragment).A01 = (AnonymousClass180) r2.ALt.get();
            contactPickerFragment.A13 = (C14830m7) r2.ALb.get();
            contactPickerFragment.A1O = (C14850m9) r2.A04.get();
            contactPickerFragment.A1e = (AnonymousClass1CY) r2.ABO.get();
            contactPickerFragment.A0K = (C14900mE) r2.A8X.get();
            contactPickerFragment.A0L = (C15570nT) r2.AAr.get();
            contactPickerFragment.A0N = (C239613r) r2.AI9.get();
            contactPickerFragment.A1j = (AbstractC14440lR) r2.ANe.get();
            contactPickerFragment.A1C = (C19990v2) r2.A3M.get();
            contactPickerFragment.A0H = (AnonymousClass17S) r2.A0F.get();
            contactPickerFragment.A1Q = (C16120oU) r2.ANE.get();
            contactPickerFragment.A0P = (C18790t3) r2.AJw.get();
            contactPickerFragment.A1N = (AnonymousClass19M) r2.A6R.get();
            contactPickerFragment.A0O = (C15450nH) r2.AII.get();
            contactPickerFragment.A1J = (C18470sV) r2.AK8.get();
            contactPickerFragment.A0Q = (AnonymousClass1AR) r2.ALM.get();
            contactPickerFragment.A0S = (C16170oZ) r2.AM4.get();
            contactPickerFragment.A1k = (C26501Ds) r2.AML.get();
            contactPickerFragment.A1g = (C22650zQ) r2.A4n.get();
            contactPickerFragment.A0k = (C21270x9) r2.A4A.get();
            contactPickerFragment.A1W = (C17220qS) r2.ABt.get();
            contactPickerFragment.A1a = (AnonymousClass17R) r2.AIz.get();
            contactPickerFragment.A1R = (C20710wC) r2.A8m.get();
            contactPickerFragment.A0I = (AnonymousClass12P) r2.A0H.get();
            contactPickerFragment.A0d = (C15550nR) r2.A45.get();
            contactPickerFragment.A1c = (C252018m) r2.A7g.get();
            contactPickerFragment.A1i = (C22190yg) r2.AB6.get();
            contactPickerFragment.A12 = (AnonymousClass01d) r2.ALI.get();
            contactPickerFragment.A0i = (C15610nY) r2.AMe.get();
            contactPickerFragment.A1X = (C17070qD) r2.AFC.get();
            contactPickerFragment.A16 = (AnonymousClass018) r2.ANb.get();
            contactPickerFragment.A1H = (AnonymousClass1BK) r2.AFZ.get();
            contactPickerFragment.A0V = (C238013b) r2.A1Z.get();
            contactPickerFragment.A0f = (AnonymousClass10S) r2.A46.get();
            contactPickerFragment.A0v = (C253318z) r2.A4B.get();
            contactPickerFragment.A0J = (C22680zT) r2.AGW.get();
            contactPickerFragment.A1b = (AnonymousClass12F) r2.AJM.get();
            contactPickerFragment.A0s = (C21240x6) r2.AA7.get();
            contactPickerFragment.A1f = (AnonymousClass198) r2.A0J.get();
            contactPickerFragment.A1Z = (C15860o1) r2.A3H.get();
            contactPickerFragment.A1P = (C22050yP) r2.A7v.get();
            contactPickerFragment.A0T = (C18330sH) r2.AN4.get();
            contactPickerFragment.A1U = (AnonymousClass1E5) r2.AKs.get();
            contactPickerFragment.A0a = (C22330yu) r2.A3I.get();
            contactPickerFragment.A0w = (C20730wE) r2.A4J.get();
            contactPickerFragment.A1B = (C255019q) r2.A50.get();
            contactPickerFragment.A0c = (AnonymousClass116) r2.A3z.get();
            contactPickerFragment.A1G = (C16490p7) r2.ACJ.get();
            contactPickerFragment.A0h = (C22700zV) r2.AMN.get();
            contactPickerFragment.A14 = (C15890o4) r2.AN1.get();
            contactPickerFragment.A15 = (C14820m6) r2.AN3.get();
            contactPickerFragment.A0b = (AnonymousClass1BF) r2.A3b.get();
            contactPickerFragment.A0y = (AnonymousClass16P) r2.A4K.get();
            contactPickerFragment.A1D = (C15680nj) r2.A4e.get();
            contactPickerFragment.A1E = (C19780uf) r2.A8E.get();
            contactPickerFragment.A10 = (C15220ml) r2.A4V.get();
            contactPickerFragment.A1h = (C15690nk) r2.A74.get();
            contactPickerFragment.A0W = (C251118d) r2.A2D.get();
            contactPickerFragment.A1M = (AnonymousClass1AN) r2.A5T.get();
            contactPickerFragment.A0X = (C16430p0) r2.A5y.get();
            contactPickerFragment.A1F = (C15600nX) r2.A8x.get();
            contactPickerFragment.A18 = new AnonymousClass3CP((C14830m7) r3.A0V.A1E.ALb.get());
            contactPickerFragment.A1Y = (C16630pM) r2.AIc.get();
            contactPickerFragment.A11 = (C18640sm) r2.A3u.get();
            contactPickerFragment.A0g = (C26311Cv) r2.AAQ.get();
            contactPickerFragment.A1A = (C21550xb) r2.AAj.get();
            contactPickerFragment.A0u = (C17010q7) r2.A43.get();
            contactPickerFragment.A0z = (AnonymousClass1DP) r2.A4N.get();
            contactPickerFragment.A1T = (C244215l) r2.A8y.get();
            contactPickerFragment.A0t = (AnonymousClass118) r2.A42.get();
            contactPickerFragment.A0Y = (AnonymousClass11I) r2.A2F.get();
            contactPickerFragment.A1L = (C25951Bl) r2.A2O.get();
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
