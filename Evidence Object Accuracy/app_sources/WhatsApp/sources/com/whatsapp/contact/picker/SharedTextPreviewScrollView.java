package com.whatsapp.contact.picker;

import X.AbstractC116015Tu;
import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12980iv;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/* loaded from: classes2.dex */
public class SharedTextPreviewScrollView extends ScrollView implements AnonymousClass004 {
    public AbstractC116015Tu A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;

    public SharedTextPreviewScrollView(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public SharedTextPreviewScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public SharedTextPreviewScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public SharedTextPreviewScrollView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // android.widget.ScrollView
    public void fling(int i) {
        super.fling(i);
        this.A03 = true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public AbstractC116015Tu getOnEndScrollListener() {
        return this.A00;
    }

    @Override // android.view.View
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (!this.A03 && this.A04) {
            return;
        }
        if (((float) C12980iv.A05(i2, i4)) < 1.0f || i2 >= getMeasuredHeight() || i2 == 0) {
            AbstractC116015Tu r0 = this.A00;
            if (r0 != null) {
                r0.APi();
            }
            this.A03 = false;
        }
    }

    @Override // android.widget.ScrollView, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == 1) {
            this.A04 = false;
            if (!this.A03) {
                this.A00.APi();
            }
        } else if (motionEvent.getActionMasked() == 2) {
            this.A04 = true;
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        return true;
    }

    public void setOnEndScrollListener(AbstractC116015Tu r1) {
        this.A00 = r1;
    }
}
