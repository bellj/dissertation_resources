package com.whatsapp.contact.picker;

import X.AbstractC009504t;
import X.AbstractC14640lm;
import X.AbstractC36921ks;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass01V;
import X.AnonymousClass1L2;
import X.AnonymousClass2FU;
import X.AnonymousClass2FV;
import X.AnonymousClass2FW;
import X.AnonymousClass3UA;
import X.C004902f;
import X.C14320lF;
import X.C14960mK;
import X.C15570nT;
import X.C16170oZ;
import X.C20640w5;
import X.C239613r;
import X.C28391Mz;
import X.C32731ce;
import X.C33771f3;
import X.C35741ib;
import X.C41691tw;
import X.C49132Jk;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import com.whatsapp.DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment;
import com.whatsapp.R;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes2.dex */
public class ContactPicker extends AnonymousClass2FU implements AnonymousClass2FV, AnonymousClass1L2, AbstractC36921ks, AnonymousClass2FW {
    public C20640w5 A00;
    public C239613r A01;
    public C16170oZ A02;
    public BaseSharedPreviewDialogFragment A03;
    public AnonymousClass3UA A04;
    public ContactPickerFragment A05;
    public WhatsAppLibLoader A06;

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment != null) {
            contactPickerFragment.A1S(i);
        }
    }

    public ContactPickerFragment A2h() {
        return new ContactPickerFragment();
    }

    @Override // X.AnonymousClass1L2
    public AnonymousClass3UA ADO() {
        AnonymousClass3UA r0 = this.A04;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass3UA r02 = new AnonymousClass3UA(this);
        this.A04 = r02;
        return r02;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC36921ks
    public void AT9(String str) {
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment != null && contactPickerFragment.A2Q && contactPickerFragment.A1O.A07(691)) {
            contactPickerFragment.A1b(str);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A02(this, R.color.primary);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        super.AXD(r2);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.AnonymousClass2FV
    public void Abd(Bundle bundle, String str, List list) {
        C14320lF r1;
        Intent A03;
        Boolean valueOf = Boolean.valueOf(bundle.getBoolean("load_preview"));
        AnonymousClass009.A05(valueOf);
        C32731ce r2 = null;
        if (valueOf.booleanValue()) {
            String A01 = C33771f3.A01(str);
            if (A01 == null) {
                r1 = null;
            } else {
                r1 = (C14320lF) C49132Jk.A00.get(A01);
            }
        } else {
            r1 = null;
        }
        Boolean valueOf2 = Boolean.valueOf(bundle.getBoolean("has_text_from_url"));
        AnonymousClass009.A05(valueOf2);
        boolean booleanValue = valueOf2.booleanValue();
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment != null) {
            r2 = contactPickerFragment.A1I;
        }
        this.A02.A08(r1, r2, null, str, list, null, false, booleanValue);
        ADO().A00.A2a(list);
        if (list.size() == 1) {
            A03 = new C14960mK().A0i(this, (AbstractC14640lm) list.get(0));
            C35741ib.A00(A03, "ContactPicker:getPostSendIntent");
        } else {
            A03 = C14960mK.A03(this);
        }
        startActivity(A03);
        finish();
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment == null || !contactPickerFragment.A1l()) {
            super.onBackPressed();
        }
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!this.A06.A03()) {
            Log.i("aborting due to native libraries missing");
        } else {
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            if (r0.A00 == null || !((ActivityC13790kL) this).A0B.A01()) {
                ((ActivityC13810kN) this).A05.A07(R.string.finish_registration_first, 1);
                startActivity(C14960mK.A04(this));
            } else {
                if (C20640w5.A00()) {
                    Log.w("contactpicker/device-not-supported");
                    Adm(new DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment());
                }
                if ("android.intent.action.CREATE_SHORTCUT".equals(getIntent().getAction())) {
                    setTitle(R.string.conversation_shortcut);
                }
                setContentView(R.layout.contact_picker_activity);
                if (C28391Mz.A02()) {
                    getWindow().addFlags(Integer.MIN_VALUE);
                }
                ContactPickerFragment contactPickerFragment = (ContactPickerFragment) A0V().A0A("ContactPickerFragment");
                this.A05 = contactPickerFragment;
                if (contactPickerFragment == null) {
                    this.A05 = A2h();
                    this.A05.A0U(ContactPickerFragment.A00(getIntent()));
                    C004902f r2 = new C004902f(A0V());
                    r2.A0A(this.A05, "ContactPickerFragment", R.id.fragment);
                    if (!r2.A0E) {
                        r2.A0F = false;
                        r2.A0J.A0d(r2, false);
                        return;
                    }
                    throw new IllegalStateException("This transaction is already being added to the back stack");
                }
                return;
            }
        }
        finish();
    }

    @Override // X.AbstractActivityC28171Kz, android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A1A;
        ContactPickerFragment contactPickerFragment = this.A05;
        return (contactPickerFragment == null || (A1A = contactPickerFragment.A1A(i)) == null) ? super.onCreateDialog(i) : A1A;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            BaseSharedPreviewDialogFragment baseSharedPreviewDialogFragment = this.A03;
            if (baseSharedPreviewDialogFragment != null) {
                baseSharedPreviewDialogFragment.A1B();
                return true;
            }
            ContactPickerFragment contactPickerFragment = this.A05;
            if (contactPickerFragment != null && contactPickerFragment.A1l()) {
                return true;
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment == null) {
            return false;
        }
        contactPickerFragment.A0M.A01();
        return true;
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested(SearchEvent searchEvent) {
        ContactPickerFragment contactPickerFragment = this.A05;
        if (contactPickerFragment == null) {
            return false;
        }
        contactPickerFragment.A0M.A01();
        return true;
    }
}
