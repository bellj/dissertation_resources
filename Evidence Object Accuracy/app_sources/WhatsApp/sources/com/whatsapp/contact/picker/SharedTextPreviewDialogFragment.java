package com.whatsapp.contact.picker;

import X.AbstractC116015Tu;
import X.AbstractC116455Vm;
import X.AbstractC14020ki;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC32741cf;
import X.AbstractC36671kL;
import X.AbstractC49122Jj;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass12P;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass2V4;
import X.AnonymousClass369;
import X.C1096452m;
import X.C14320lF;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15270mq;
import X.C15330mx;
import X.C15380n4;
import X.C16630pM;
import X.C18790t3;
import X.C22640zP;
import X.C231510o;
import X.C252718t;
import X.C33771f3;
import X.C49132Jk;
import X.C58012nz;
import X.C58022o0;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.List;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public class SharedTextPreviewDialogFragment extends Hilt_SharedTextPreviewDialogFragment {
    public View A00;
    public ImageButton A01;
    public AnonymousClass12P A02;
    public AbstractC15710nm A03;
    public C18790t3 A04;
    public C14320lF A05;
    public C22640zP A06;
    public SharedTextPreviewScrollView A07;
    public C14820m6 A08;
    public AnonymousClass19M A09;
    public C15270mq A0A;
    public C231510o A0B;
    public AnonymousClass193 A0C;
    public C14850m9 A0D;
    public MentionableEntry A0E;
    public C16630pM A0F;
    public C252718t A0G;
    public AbstractC14440lR A0H;
    public Runnable A0I = null;
    public String A0J;
    public String A0K;
    public String A0L;
    public boolean A0M;
    public boolean A0N = false;
    public boolean A0O = true;
    public final Handler A0P = new Handler(Looper.getMainLooper());
    public final AbstractC116455Vm A0Q = new C1096452m(this);

    public static SharedTextPreviewDialogFragment A00(String str, List list, boolean z) {
        SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = new SharedTextPreviewDialogFragment();
        BaseSharedPreviewDialogFragment baseSharedPreviewDialogFragment = new BaseSharedPreviewDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("jids", C15380n4.A06(list));
        baseSharedPreviewDialogFragment.A0U(bundle);
        Bundle A03 = baseSharedPreviewDialogFragment.A03();
        A03.putString("message", str);
        A03.putBoolean("has_text_from_url", z);
        sharedTextPreviewDialogFragment.A0U(A03);
        return sharedTextPreviewDialogFragment;
    }

    public static /* synthetic */ void A01(SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment) {
        String trim = AbstractC32741cf.A04(sharedTextPreviewDialogFragment.A0E.getText().toString()).trim();
        boolean z = false;
        if (trim.length() <= 0) {
            ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A08.A07(R.string.no_empty_message, 0);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putBoolean("has_text_from_url", sharedTextPreviewDialogFragment.A0M);
        WebPagePreviewView webPagePreviewView = ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G;
        if (webPagePreviewView != null && webPagePreviewView.getVisibility() == 0) {
            z = true;
        }
        bundle.putBoolean("load_preview", z);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0B.Abd(bundle, trim, ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0H);
        sharedTextPreviewDialogFragment.A1B();
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 27 && i2 == -1) {
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                this.A02.A06(A0B(), C14960mK.A02(A0B));
                A0B().finish();
            }
            A1B();
        }
    }

    @Override // com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment, X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            if (this.A0A.isShowing()) {
                this.A0A.dismiss();
            }
            A0C().getWindow().setSoftInputMode(2);
        }
        return super.A0z(menuItem);
    }

    @Override // com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        boolean z;
        String replaceFirst;
        super.A10(bundle, layoutInflater, viewGroup);
        int i = 0;
        ((BaseSharedPreviewDialogFragment) this).A05.addView(A0C().getLayoutInflater().inflate(R.layout.shared_text_preview_relative_layout, (ViewGroup) null, false));
        this.A07 = (SharedTextPreviewScrollView) ((BaseSharedPreviewDialogFragment) this).A05.findViewById(R.id.subject_layout);
        this.A0E = (MentionableEntry) ((BaseSharedPreviewDialogFragment) this).A05.findViewById(R.id.mentionable_entry);
        this.A00 = ((BaseSharedPreviewDialogFragment) this).A05.findViewById(R.id.stub);
        AnonymousClass018 r0 = ((BaseSharedPreviewDialogFragment) this).A0D;
        MentionableEntry mentionableEntry = this.A0E;
        if (r0.A04().A06) {
            mentionableEntry.setPadding(2, mentionableEntry.getPaddingTop(), mentionableEntry.getPaddingRight(), mentionableEntry.getPaddingBottom());
        } else {
            mentionableEntry.setPadding(mentionableEntry.getPaddingLeft(), mentionableEntry.getPaddingTop(), 2, mentionableEntry.getPaddingBottom());
        }
        this.A0E.addTextChangedListener(new AnonymousClass369(this));
        this.A0E.setInputType(131073);
        ImageButton imageButton = (ImageButton) ((BaseSharedPreviewDialogFragment) this).A05.findViewById(R.id.emoji_btn);
        this.A01 = imageButton;
        ActivityC000900k A0B = A0B();
        C252718t r14 = this.A0G;
        AbstractC15710nm r11 = this.A03;
        AnonymousClass19M r10 = this.A09;
        C231510o r6 = this.A0B;
        C15270mq r15 = new C15270mq(A0B, imageButton, r11, (KeyboardPopupLayout) ((BaseSharedPreviewDialogFragment) this).A00.findViewById(R.id.emoji_edit_text_layout), this.A0E, ((BaseSharedPreviewDialogFragment) this).A0C, this.A08, ((BaseSharedPreviewDialogFragment) this).A0D, r10, r6, this.A0C, this.A0F, r14);
        this.A0A = r15;
        C15330mx r102 = new C15330mx(A0B(), ((BaseSharedPreviewDialogFragment) this).A0D, this.A09, r15, this.A0B, ((BaseSharedPreviewDialogFragment) this).A0E, this.A0F);
        r102.A00 = new AbstractC14020ki() { // from class: X.56d
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                SharedTextPreviewDialogFragment.this.A0Q.APc(r3.A00);
            }
        };
        C15270mq r2 = this.A0A;
        r2.A0C(this.A0Q);
        r2.A0E = new RunnableBRunnable0Shape2S0200000_I0_2(this, 39, r102);
        String A01 = C33771f3.A01(this.A0L);
        if (A01 == null || (replaceFirst = this.A0L.replaceFirst(Pattern.quote(A01), "")) == null || !replaceFirst.trim().isEmpty()) {
            z = true;
        } else {
            StringBuilder sb = new StringBuilder("\n\n");
            sb.append(this.A0L);
            this.A0L = sb.toString();
            z = false;
        }
        A1K();
        this.A0E.setText(AbstractC36671kL.A05(A0B(), this.A09, this.A0L));
        A1N(this.A0E.getText(), true);
        this.A0E.requestFocus();
        Window window = ((DialogFragment) this).A03.getWindow();
        AnonymousClass009.A05(window);
        window.setSoftInputMode(5);
        MentionableEntry mentionableEntry2 = this.A0E;
        if (z) {
            i = mentionableEntry2.getText().length();
        }
        mentionableEntry2.setSelection(i);
        SharedTextPreviewScrollView sharedTextPreviewScrollView = this.A07;
        sharedTextPreviewScrollView.A00 = new AbstractC116015Tu() { // from class: X.3WJ
            @Override // X.AbstractC116015Tu
            public final void APi() {
                SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = SharedTextPreviewDialogFragment.this;
                int selectionStart = sharedTextPreviewDialogFragment.A0E.getSelectionStart();
                if (selectionStart == sharedTextPreviewDialogFragment.A0E.getSelectionEnd() && sharedTextPreviewDialogFragment.A0O) {
                    MentionableEntry mentionableEntry3 = sharedTextPreviewDialogFragment.A0E;
                    int offsetForPosition = mentionableEntry3.getOffsetForPosition(mentionableEntry3.getX() + C12990iw.A02(sharedTextPreviewDialogFragment.A0E), (float) sharedTextPreviewDialogFragment.A07.getScrollY());
                    int A05 = C12980iv.A05(sharedTextPreviewDialogFragment.A0E.getLayout().getLineTop(0), sharedTextPreviewDialogFragment.A0E.getLayout().getLineBottom(0));
                    MentionableEntry mentionableEntry4 = sharedTextPreviewDialogFragment.A0E;
                    int offsetForPosition2 = mentionableEntry4.getOffsetForPosition(mentionableEntry4.getX() + C12990iw.A02(sharedTextPreviewDialogFragment.A0E), (float) ((sharedTextPreviewDialogFragment.A07.getScrollY() + sharedTextPreviewDialogFragment.A07.getHeight()) - A05));
                    if (selectionStart < offsetForPosition) {
                        sharedTextPreviewDialogFragment.A0E.setSelection(offsetForPosition);
                    } else if (selectionStart > offsetForPosition2) {
                        sharedTextPreviewDialogFragment.A0E.setSelection(offsetForPosition2);
                    }
                } else if (!sharedTextPreviewDialogFragment.A0O) {
                    sharedTextPreviewDialogFragment.A0O = true;
                }
                sharedTextPreviewDialogFragment.A1L();
            }
        };
        sharedTextPreviewScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nh
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                SharedTextPreviewDialogFragment.this.A1L();
            }
        });
        this.A07.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oZ
            @Override // android.view.ViewTreeObserver.OnScrollChangedListener
            public final void onScrollChanged() {
                SharedTextPreviewDialogFragment.this.A1L();
            }
        });
        this.A07.setOverScrollMode(2);
        ((BaseSharedPreviewDialogFragment) this).A03.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 33));
        ((DialogFragment) this).A03.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: X.4hP
            @Override // android.content.DialogInterface.OnKeyListener
            public final boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
                SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = SharedTextPreviewDialogFragment.this;
                if (i2 != 4 || !sharedTextPreviewDialogFragment.A0A.isShowing()) {
                    return false;
                }
                sharedTextPreviewDialogFragment.A0A.dismiss();
                return true;
            }
        });
        A1L();
        return ((BaseSharedPreviewDialogFragment) this).A00;
    }

    @Override // com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        String string = A03.getString("message");
        AnonymousClass009.A06(string, "null message");
        this.A0L = string;
        Boolean valueOf = Boolean.valueOf(A03.getBoolean("has_text_from_url"));
        AnonymousClass009.A06(valueOf, "null hasTextFromUrl");
        this.A0M = valueOf.booleanValue();
        return super.A1A(bundle);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r0 != 0) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1L() {
        /*
            r5 = this;
            com.whatsapp.webpagepreview.WebPagePreviewView r0 = r5.A0G
            if (r0 == 0) goto L_0x000d
            int r0 = r0.getVisibility()
            r1 = 2131166874(0x7f07069a, float:1.7948006E38)
            if (r0 == 0) goto L_0x0010
        L_0x000d:
            r1 = 2131166875(0x7f07069b, float:1.7948008E38)
        L_0x0010:
            X.00k r0 = r5.A0C()
            android.content.res.Resources r0 = r0.getResources()
            int r3 = r0.getDimensionPixelSize(r1)
            com.whatsapp.contact.picker.SharedTextPreviewScrollView r0 = r5.A07
            int r0 = r0.getPaddingBottom()
            if (r0 == r3) goto L_0x0039
            com.whatsapp.contact.picker.SharedTextPreviewScrollView r4 = r5.A07
            int r2 = r4.getPaddingLeft()
            com.whatsapp.contact.picker.SharedTextPreviewScrollView r0 = r5.A07
            int r1 = r0.getPaddingTop()
            com.whatsapp.contact.picker.SharedTextPreviewScrollView r0 = r5.A07
            int r0 = r0.getPaddingRight()
            r4.setPadding(r2, r1, r0, r3)
        L_0x0039:
            r0 = 2
            int[] r2 = new int[r0]
            int[] r1 = new int[r0]
            android.view.View r0 = r5.A00
            r0.getLocationOnScreen(r2)
            android.widget.RelativeLayout r0 = r5.A06
            r0.getLocationOnScreen(r1)
            r0 = 1
            r1 = r1[r0]
            r0 = r2[r0]
            int r1 = r1 - r0
            if (r1 >= r3) goto L_0x0055
            r0 = 0
            int r3 = java.lang.Math.max(r0, r1)
        L_0x0055:
            android.widget.ImageButton r0 = r5.A01
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            r2.<init>(r0)
            r2.bottomMargin = r3
            r0 = 9
            r2.addRule(r0)
            r1 = 8
            r0 = 2131366237(0x7f0a115d, float:1.8352362E38)
            r2.addRule(r1, r0)
            android.widget.ImageButton r0 = r5.A01
            r0.setLayoutParams(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contact.picker.SharedTextPreviewDialogFragment.A1L():void");
    }

    public final void A1M() {
        ViewGroup viewGroup;
        if (((BaseSharedPreviewDialogFragment) this).A0G != null && (viewGroup = ((BaseSharedPreviewDialogFragment) this).A02) != null && viewGroup.getVisibility() == 0 && !this.A0N) {
            this.A0N = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) ((BaseSharedPreviewDialogFragment) this).A02.getHeight());
            translateAnimation.setDuration(150);
            translateAnimation.setAnimationListener(new C58022o0(this));
            ((BaseSharedPreviewDialogFragment) this).A0G.startAnimation(translateAnimation);
        }
    }

    public final void A1N(Editable editable, boolean z) {
        String A01 = C33771f3.A01(editable.toString());
        this.A0K = A01;
        if (A01 == null || A01.equals(this.A0J)) {
            A1O(null);
            return;
        }
        this.A0J = null;
        C14320lF r0 = this.A05;
        if (r0 == null || !TextUtils.equals(r0.A0K, A01)) {
            A1O((C14320lF) C49132Jk.A00.get(A01));
            if (this.A05 == null) {
                Runnable runnable = this.A0I;
                if (runnable != null) {
                    this.A0P.removeCallbacks(runnable);
                    this.A0I = null;
                }
                if (z) {
                    C14900mE r02 = ((BaseSharedPreviewDialogFragment) this).A08;
                    AbstractC14440lR r4 = this.A0H;
                    C49132Jk.A00(r02, this.A04, new AbstractC49122Jj() { // from class: X.535
                        @Override // X.AbstractC49122Jj
                        public final void ATN(C14320lF r2, boolean z2) {
                            SharedTextPreviewDialogFragment.this.A1O(r2);
                        }
                    }, ((BaseSharedPreviewDialogFragment) this).A0D, r4, A01);
                    return;
                }
                RunnableBRunnable0Shape0S1100000_I0 runnableBRunnable0Shape0S1100000_I0 = new RunnableBRunnable0Shape0S1100000_I0(22, A01, this);
                this.A0I = runnableBRunnable0Shape0S1100000_I0;
                this.A0P.postDelayed(runnableBRunnable0Shape0S1100000_I0, 700);
            }
        }
    }

    public final void A1O(C14320lF r11) {
        AnonymousClass2V4 r1;
        if (A0B() != null) {
            if (r11 != null) {
                if (!TextUtils.equals(this.A0K, r11.A0K)) {
                    return;
                }
                if (r11.A0B()) {
                    this.A05 = r11;
                    if (((BaseSharedPreviewDialogFragment) this).A0G == null) {
                        WebPagePreviewView webPagePreviewView = new WebPagePreviewView(A0C());
                        ((BaseSharedPreviewDialogFragment) this).A0G = webPagePreviewView;
                        webPagePreviewView.setForeground(null);
                        ((BaseSharedPreviewDialogFragment) this).A0G.setMinimumHeight(A02().getDimensionPixelSize(R.dimen.share_preview_footer_max_height));
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageContentBackgroundResource(0);
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageContentEnabled(false);
                        ((BaseSharedPreviewDialogFragment) this).A02.addView(((BaseSharedPreviewDialogFragment) this).A0G);
                        ((BaseSharedPreviewDialogFragment) this).A0G.A01();
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageProgressBarVisibility(false);
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageContentMinimumHeight(A02().getDimensionPixelSize(R.dimen.link_preview_min_height));
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageCancelClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 2));
                        C14320lF r0 = this.A05;
                        if (!(r0 == null || (r1 = r0.A06) == null || r1.A02 == null)) {
                            String str = r1.A01;
                            if ("video/mp4".equals(str) || "image/gif".equals(str)) {
                                ((BaseSharedPreviewDialogFragment) this).A0G.setImageContentEnabled(true);
                            }
                        }
                        ((BaseSharedPreviewDialogFragment) this).A0G.setImageContentClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 3));
                    }
                    A1L();
                    if (!(((BaseSharedPreviewDialogFragment) this).A02.getVisibility() == 0 || ((BaseSharedPreviewDialogFragment) this).A0G == null || this.A0N)) {
                        this.A0N = true;
                        int[] iArr = {0, 0};
                        this.A0E.getLocationOnScreen(iArr);
                        int height = iArr[1] + this.A0E.getHeight();
                        int[] iArr2 = {0, 0};
                        ((BaseSharedPreviewDialogFragment) this).A00.findViewById(R.id.recipients_container).getLocationOnScreen(iArr2);
                        int i = iArr2[1];
                        int abs = Math.abs(height - i);
                        int dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.share_preview_footer_max_height) - A02().getDimensionPixelSize(R.dimen.share_preview_footer_min_height);
                        if (abs > dimensionPixelSize || (i == 0 && height == 0)) {
                            A1K();
                            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) ((BaseSharedPreviewDialogFragment) this).A02.getHeight(), 0.0f);
                            translateAnimation.setDuration(150);
                            translateAnimation.setDuration(150);
                            ((BaseSharedPreviewDialogFragment) this).A02.setVisibility(0);
                            ((BaseSharedPreviewDialogFragment) this).A01.setVisibility(0);
                            ((BaseSharedPreviewDialogFragment) this).A0G.startAnimation(translateAnimation);
                        } else {
                            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-dimensionPixelSize));
                            translateAnimation2.setDuration(150);
                            translateAnimation2.setAnimationListener(new C58012nz(this));
                            this.A07.startAnimation(translateAnimation2);
                        }
                        this.A0N = false;
                    }
                    this.A0E.requestFocus();
                    WebPagePreviewView webPagePreviewView2 = ((BaseSharedPreviewDialogFragment) this).A0G;
                    AnonymousClass009.A03(webPagePreviewView2);
                    webPagePreviewView2.A0A(r11, null, false, this.A06.A07());
                    return;
                }
            }
            this.A05 = null;
            A1M();
        }
    }

    @Override // com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment, androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.getWindow().setSoftInputMode(3);
        }
        super.onDismiss(dialogInterface);
    }
}
