package com.whatsapp.contact.picker;

import X.AbstractC36921ks;
import X.AnonymousClass009;
import X.AnonymousClass47Y;
import X.C12970iu;
import X.C52702bX;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.base.WaDialogFragment;
import com.whatsapp.contact.picker.PhoneNumberSelectionDialog;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class PhoneNumberSelectionDialog extends WaDialogFragment {
    public AbstractC36921ks A00;

    public static PhoneNumberSelectionDialog A00(String str, ArrayList arrayList) {
        PhoneNumberSelectionDialog phoneNumberSelectionDialog = new PhoneNumberSelectionDialog();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("displayName", str);
        A0D.putParcelableArrayList("phoneNumberSelectionInfoList", arrayList);
        phoneNumberSelectionDialog.A0U(A0D);
        return phoneNumberSelectionDialog;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A00 = null;
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC36921ks) {
            this.A00 = (AbstractC36921ks) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        String string = A03.getString("displayName");
        ArrayList parcelableArrayList = A03.getParcelableArrayList("phoneNumberSelectionInfoList");
        AnonymousClass009.A05(parcelableArrayList);
        Context A01 = A01();
        C52702bX r4 = new C52702bX(A01, parcelableArrayList);
        AlertDialog create = new AlertDialog.Builder(A01).setTitle(string).setAdapter(r4, null).setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener(r4, this, parcelableArrayList) { // from class: X.3Kd
            public final /* synthetic */ C52702bX A00;
            public final /* synthetic */ PhoneNumberSelectionDialog A01;
            public final /* synthetic */ ArrayList A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PhoneNumberSelectionDialog phoneNumberSelectionDialog = this.A01;
                ArrayList arrayList = this.A02;
                C52702bX r0 = this.A00;
                AbstractC36921ks r1 = phoneNumberSelectionDialog.A00;
                if (r1 != null) {
                    r1.AT9(((C65983Lv) arrayList.get(r0.A00)).A00);
                }
                phoneNumberSelectionDialog.A1B();
            }
        }).setNegativeButton(R.string.cancel, (DialogInterface.OnClickListener) null).setCancelable(true).create();
        create.getListView().setOnItemClickListener(new AnonymousClass47Y(r4, this));
        return create;
    }
}
