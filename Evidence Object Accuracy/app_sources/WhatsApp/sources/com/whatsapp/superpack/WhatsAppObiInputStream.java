package com.whatsapp.superpack;

import java.io.InputStream;

/* loaded from: classes2.dex */
public class WhatsAppObiInputStream extends InputStream {
    public final byte[] oneByte = new byte[1];
    public long ptr;

    public static native void closeNative(long j);

    public static native long openBytesNative(byte[] bArr, int i, int i2);

    public static native long openInputStreamNative(InputStream inputStream, int i);

    public static native int readNative(long j, byte[] bArr, int i, int i2);

    public WhatsAppObiInputStream(long j) {
        if (j != 0) {
            this.ptr = j;
            return;
        }
        throw null;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        closeNative(this.ptr);
    }

    public static WhatsAppObiInputStream openBytes(byte[] bArr) {
        return openBytes(bArr, 0, bArr.length);
    }

    public static WhatsAppObiInputStream openBytes(byte[] bArr, int i, int i2) {
        if (i < 0 || i2 <= 0 || i + i2 > bArr.length) {
            throw new IllegalArgumentException("Invalid byte array offset/length");
        }
        long openBytesNative = openBytesNative(bArr, i, i2);
        if (openBytesNative != 0) {
            return new WhatsAppObiInputStream(openBytesNative);
        }
        throw new IllegalStateException("Failed to open OBI input stream");
    }

    public static WhatsAppObiInputStream openStream(InputStream inputStream, int i) {
        long openInputStreamNative = openInputStreamNative(inputStream, i);
        if (openInputStreamNative != 0) {
            return new WhatsAppObiInputStream(openInputStreamNative);
        }
        throw new IllegalStateException("Failed to open OBI input stream");
    }

    @Override // java.io.InputStream
    public synchronized int read() {
        int i;
        int read = read(this.oneByte);
        i = -1;
        if (read != -1) {
            if (read == 1) {
                byte b = this.oneByte[0];
                i = b;
                if (b < 0) {
                    i = b + 256;
                }
            } else {
                throw new IllegalStateException("Unexpected number of bytes read");
            }
        }
        return i == 1 ? 1 : 0;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public synchronized int read(byte[] bArr, int i, int i2) {
        if (i >= 0 && i2 >= 0) {
            if (i2 + i <= bArr.length) {
                if (readNative(this.ptr, bArr, i, i2) <= 0) {
                    return -1;
                }
                return i2;
            }
        }
        throw new IndexOutOfBoundsException();
    }
}
