package com.whatsapp.checkbox;

import X.AnonymousClass018;
import X.C12990iw;
import X.C28141Kv;
import X.C73113fh;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;

/* loaded from: classes2.dex */
public class RtlCheckBox extends AppCompatCheckBox {
    public int A00;
    public Drawable A01;
    public AnonymousClass018 A02;

    public RtlCheckBox(Context context) {
        this(context, null);
    }

    public RtlCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public RtlCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public final void A00() {
        if (this.A02 == null) {
            this.A02 = C12990iw.A0S(getContext()).Ag8();
        }
        if (!isInEditMode() && C28141Kv.A00(this.A02)) {
            setBackgroundDrawable(null);
            this.A00 = getPaddingLeft();
            int min = Math.min(this.A00, getPaddingRight());
            setPadding(min, getPaddingTop(), min, getPaddingBottom());
        }
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.widget.TextView, android.widget.CompoundButton
    public int getCompoundPaddingLeft() {
        if (isInEditMode() || C28141Kv.A01(this.A02)) {
            return super.getCompoundPaddingLeft();
        }
        return super.getPaddingLeft();
    }

    @Override // android.widget.TextView, android.widget.CompoundButton
    public int getCompoundPaddingRight() {
        if (isInEditMode() || C28141Kv.A01(this.A02)) {
            return super.getCompoundPaddingRight();
        }
        int paddingRight = super.getPaddingRight();
        Drawable drawable = this.A01;
        return drawable != null ? paddingRight + Math.max(this.A00, drawable.getIntrinsicWidth()) : paddingRight;
    }

    @Override // android.widget.TextView, android.widget.CompoundButton, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode() && C28141Kv.A00(this.A02) && this.A01 != null) {
            int gravity = getGravity() & 112;
            int intrinsicHeight = this.A01.getIntrinsicHeight();
            int intrinsicWidth = this.A01.getIntrinsicWidth();
            int i = 0;
            if (gravity == 16) {
                i = (getHeight() - intrinsicHeight) >> 1;
            } else if (gravity == 80) {
                i = getHeight() - intrinsicHeight;
            }
            int width = getWidth() - intrinsicWidth;
            int width2 = getWidth();
            this.A01.setBounds(width, i, width2, intrinsicHeight + i);
            this.A01.draw(canvas);
        }
    }

    @Override // androidx.appcompat.widget.AppCompatCheckBox, android.widget.CompoundButton
    public void setButtonDrawable(Drawable drawable) {
        if (this.A02 == null) {
            this.A02 = C12990iw.A0S(getContext()).Ag8();
        }
        if (!isInEditMode() && !C28141Kv.A01(this.A02)) {
            this.A01 = drawable;
            drawable = new C73113fh(drawable, this);
        }
        super.setButtonDrawable(drawable);
    }
}
