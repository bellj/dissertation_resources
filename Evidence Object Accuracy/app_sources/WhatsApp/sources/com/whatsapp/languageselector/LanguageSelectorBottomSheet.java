package com.whatsapp.languageselector;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass07F;
import X.AnonymousClass1C8;
import X.AnonymousClass3J4;
import X.AnonymousClass5UL;
import X.AnonymousClass5W3;
import X.C04230Kw;
import X.C04240Kx;
import X.C102304ov;
import X.C12970iu;
import X.C15570nT;
import X.C28391Mz;
import X.C52742bb;
import X.C56772le;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.BottomSheetListView;
import com.whatsapp.R;
import com.whatsapp.languageselector.LanguageSelectorBottomSheet;

/* loaded from: classes2.dex */
public class LanguageSelectorBottomSheet extends Hilt_LanguageSelectorBottomSheet {
    public C15570nT A00;
    public AnonymousClass01d A01;
    public AnonymousClass018 A02;
    public AnonymousClass5UL A03;
    public AnonymousClass5W3 A04;
    public AnonymousClass1C8 A05;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        AnonymousClass5W3 r0 = this.A04;
        if (r0 != null) {
            r0.ARi();
        }
        this.A03 = null;
        this.A04 = null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        Window window;
        super.A0s();
        AnonymousClass5W3 r0 = this.A04;
        if (r0 != null) {
            r0.ARk();
        }
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null && (window = dialog.getWindow()) != null) {
            if (Build.VERSION.SDK_INT >= 30) {
                C04240Kx.A00(window, false);
            } else {
                C04230Kw.A00(window, false);
            }
            dialog.findViewById(R.id.container).setFitsSystemWindows(false);
            dialog.findViewById(R.id.coordinator).setFitsSystemWindows(false);
            AnonymousClass028.A0h(dialog.findViewById(R.id.container), new AnonymousClass07F() { // from class: X.3PL
                @Override // X.AnonymousClass07F
                public final C018408o AMH(View view, C018408o r10) {
                    int i;
                    LanguageSelectorBottomSheet languageSelectorBottomSheet = LanguageSelectorBottomSheet.this;
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                    int i2 = r10.A00.A05(7).A00;
                    int i3 = layoutParams.leftMargin;
                    int identifier = languageSelectorBottomSheet.A02().getIdentifier("status_bar_height", "dimen", "android");
                    if (identifier > 0) {
                        i = languageSelectorBottomSheet.A02().getDimensionPixelSize(identifier);
                    } else {
                        i = 25;
                    }
                    layoutParams.setMargins(i3, i, layoutParams.rightMargin, i2);
                    view.setLayoutParams(layoutParams);
                    return r10;
                }
            });
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.language_selector_bottomsheet, viewGroup);
        AbstractView$OnClickListenerC34281fs.A01(AnonymousClass028.A0D(inflate, R.id.closeButton), this, 30);
        BottomSheetListView bottomSheetListView = (BottomSheetListView) AnonymousClass028.A0D(inflate, R.id.languageSelectorListView);
        ActivityC000900k A0B = A0B();
        AnonymousClass018 r3 = this.A02;
        bottomSheetListView.setAdapter((ListAdapter) new C52742bb(A0B, r3, AnonymousClass3J4.A01(this.A00, this.A01, r3), AnonymousClass3J4.A02()));
        bottomSheetListView.setOnItemClickListener(new AdapterView.OnItemClickListener(bottomSheetListView, this) { // from class: X.3OD
            public final /* synthetic */ BottomSheetListView A00;
            public final /* synthetic */ LanguageSelectorBottomSheet A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                LanguageSelectorBottomSheet languageSelectorBottomSheet = this.A01;
                languageSelectorBottomSheet.A02.A0R(((C90284Nh) this.A00.getAdapter().getItem(i)).A01);
                languageSelectorBottomSheet.A1B();
            }
        });
        if (C28391Mz.A02()) {
            A1M(AnonymousClass028.A0D(inflate, R.id.divider), bottomSheetListView);
        }
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A14() {
        super.A14();
        AnonymousClass5W3 r0 = this.A04;
        if (r0 != null) {
            r0.ARi();
        }
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        A00.A0E = new C56772le(A00, this);
        Point point = new Point();
        C12970iu.A17(A0C(), point);
        A00.A0L((int) (((double) point.y) * 0.5d));
    }

    public final void A1M(View view, BottomSheetListView bottomSheetListView) {
        bottomSheetListView.setOnScrollListener(new C102304ov(view, bottomSheetListView, this, A02().getDimensionPixelSize(R.dimen.language_selector_list_elevation)));
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AnonymousClass5UL r0 = this.A03;
        if (r0 != null) {
            r0.ARj();
        }
        AnonymousClass5W3 r02 = this.A04;
        if (r02 != null) {
            r02.ARi();
        }
    }
}
