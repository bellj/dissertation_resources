package com.whatsapp.search;

import X.AbstractC001200n;
import X.AbstractC05270Ox;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14740ly;
import X.AbstractC15710nm;
import X.AbstractC33021d9;
import X.AbstractC33331dp;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01H;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass04v;
import X.AnonymousClass078;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Y;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass132;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass15Q;
import X.AnonymousClass17S;
import X.AnonymousClass198;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1AM;
import X.AnonymousClass1BK;
import X.AnonymousClass1CY;
import X.AnonymousClass1J1;
import X.AnonymousClass1Q8;
import X.AnonymousClass1SF;
import X.AnonymousClass2Cu;
import X.AnonymousClass2Dn;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass32P;
import X.AnonymousClass3G9;
import X.AnonymousClass4JF;
import X.AnonymousClass4L6;
import X.AnonymousClass5U3;
import X.C009804x;
import X.C105294tp;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15670ni;
import X.C15680nj;
import X.C15860o1;
import X.C15890o4;
import X.C16030oK;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16590pI;
import X.C16630pM;
import X.C17070qD;
import X.C17220qS;
import X.C18330sH;
import X.C18850tA;
import X.C19990v2;
import X.C20220vP;
import X.C20650w6;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21270x9;
import X.C21280xA;
import X.C21320xE;
import X.C21400xM;
import X.C22230yk;
import X.C22330yu;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C241714m;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C244415n;
import X.C246716k;
import X.C250417w;
import X.C253018w;
import X.C253519b;
import X.C254219i;
import X.C255719x;
import X.C25641Ae;
import X.C26501Ds;
import X.C27131Gd;
import X.C27151Gf;
import X.C32901cv;
import X.C33701ew;
import X.C34271fr;
import X.C36411jq;
import X.C36911kq;
import X.C39091pH;
import X.C41691tw;
import X.C42571vR;
import X.C42581vS;
import X.C44091yC;
import X.C48232Fc;
import X.C48962Ip;
import X.C54612h0;
import X.C54682h7;
import X.C54692h8;
import X.C63193Aq;
import X.C67323Rb;
import X.C72593er;
import X.C75063jF;
import X.C83403xB;
import X.C84403zF;
import X.C851541j;
import X.View$OnLayoutChangeListenerC101064mv;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.google.android.material.chip.Chip;
import com.whatsapp.BidiToolbar;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.search.views.ProgressView;
import com.whatsapp.search.views.TokenizedSearchInput;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.text.FinalBackspaceAwareEntry;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class SearchFragment extends Hilt_SearchFragment implements AbstractC33021d9 {
    public ValueAnimator A00;
    public View A01;
    public ViewGroup A02;
    public RecyclerView A03;
    public C34271fr A04;
    public AnonymousClass17S A05;
    public AnonymousClass12P A06;
    public BidiToolbar A07;
    public C253519b A08;
    public AbstractC15710nm A09;
    public C48962Ip A0A;
    public AnonymousClass4JF A0B;
    public C14900mE A0C;
    public C15570nT A0D;
    public C15450nH A0E;
    public AnonymousClass11L A0F;
    public C16170oZ A0G;
    public C18330sH A0H;
    public C14650lo A0I;
    public AnonymousClass10A A0J;
    public C246716k A0K;
    public C238013b A0L;
    public C16430p0 A0M;
    public C22330yu A0N;
    public C243915i A0O;
    public C18850tA A0P;
    public AnonymousClass130 A0Q;
    public C15550nR A0R;
    public AnonymousClass10S A0S;
    public C15610nY A0T;
    public AnonymousClass10T A0U;
    public AnonymousClass1J1 A0V;
    public C21270x9 A0W;
    public C20730wE A0X;
    public C250417w A0Y;
    public AnonymousClass19D A0Z;
    public AnonymousClass11P A0a;
    public C42571vR A0b;
    public AnonymousClass01d A0c;
    public C14830m7 A0d;
    public C16590pI A0e;
    public C15890o4 A0f;
    public C14820m6 A0g;
    public AnonymousClass018 A0h;
    public C20650w6 A0i;
    public C19990v2 A0j;
    public C20830wO A0k;
    public C21320xE A0l;
    public C15680nj A0m;
    public C241714m A0n;
    public AnonymousClass15Q A0o;
    public C25641Ae A0p;
    public C15600nX A0q;
    public C236812p A0r;
    public AnonymousClass10Y A0s;
    public AnonymousClass1BK A0t;
    public C242114q A0u;
    public AnonymousClass132 A0v;
    public C21400xM A0w;
    public C15670ni A0x;
    public AnonymousClass19M A0y;
    public C14850m9 A0z;
    public C16120oU A10;
    public C20710wC A11;
    public C244215l A12;
    public AbstractC14640lm A13;
    public C16030oK A14;
    public C244415n A15;
    public AnonymousClass13H A16;
    public C17220qS A17;
    public C22230yk A18;
    public C20220vP A19;
    public C22710zW A1A;
    public C17070qD A1B;
    public AnonymousClass14X A1C;
    public C16630pM A1D;
    public IteratingPlayer A1E;
    public C36911kq A1F;
    public SearchViewModel A1G;
    public ProgressView A1H;
    public TokenizedSearchInput A1I;
    public C15860o1 A1J;
    public AnonymousClass1AM A1K;
    public AnonymousClass12F A1L;
    public C253018w A1M;
    public AnonymousClass12U A1N;
    public StatusesViewModel A1O;
    public AnonymousClass1CY A1P;
    public AnonymousClass198 A1Q;
    public C254219i A1R;
    public C255719x A1S;
    public AbstractC14440lR A1T;
    public C39091pH A1U;
    public C26501Ds A1V;
    public C21280xA A1W;
    public AnonymousClass01H A1X;
    public Runnable A1Y;
    public boolean A1Z;
    public final AbstractC05270Ox A1a = new C75063jF(this);
    public final AnonymousClass2Cu A1b = new C84403zF(this);
    public final AnonymousClass2Dn A1c = new C851541j(this);
    public final C27131Gd A1d = new C36411jq(this);
    public final C27151Gf A1e = new C32901cv(this);
    public final AbstractC33331dp A1f = new AnonymousClass32P(this);

    public static SearchFragment A00(Integer num, Integer num2, int i, int i2) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("x", i);
        bundle.putInt("y", i2);
        if (num != null) {
            bundle.putInt("enter_duration_ms", num.intValue());
        }
        if (num2 != null) {
            bundle.putInt("exit_duration_ms", num2.intValue());
        }
        searchFragment.A0U(bundle);
        return searchFragment;
    }

    public static /* synthetic */ void A01(AbstractC14640lm r4, SearchFragment searchFragment) {
        C36911kq r3 = searchFragment.A1F;
        if (r3 != null) {
            int i = 0;
            while (true) {
                C44091yC r1 = r3.A0q;
                if (i < r1.size()) {
                    if (r4.equals(r1.get(i).A01)) {
                        r3.A03(i);
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public static /* synthetic */ void A02(SearchFragment searchFragment) {
        if (searchFragment.A1G != null && searchFragment.A0p() != null) {
            searchFragment.A1G.A0P(3);
            searchFragment.A01.setVisibility(8);
            int i = Build.VERSION.SDK_INT;
            BidiToolbar bidiToolbar = searchFragment.A07;
            if (i >= 21) {
                bidiToolbar.setElevation(searchFragment.A02().getDimension(R.dimen.actionbar_elevation));
            } else {
                C48232Fc.A00(bidiToolbar);
            }
        }
    }

    public static /* synthetic */ void A03(SearchFragment searchFragment, Boolean bool) {
        View view;
        if (bool != null && bool.booleanValue() && (view = ((AnonymousClass01E) searchFragment).A0A) != null && (view.getParent() instanceof LinearLayout)) {
            ValueAnimator valueAnimator = searchFragment.A00;
            if (valueAnimator != null) {
                valueAnimator.cancel();
                searchFragment.A00 = null;
            }
            SearchViewModel searchViewModel = searchFragment.A1G;
            if (searchViewModel != null) {
                searchViewModel.A0P(2);
            }
            ((View) ((AnonymousClass01E) searchFragment).A0A.getParent()).setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
            searchFragment.A01.setVisibility(0);
            int i = Build.VERSION.SDK_INT;
            BidiToolbar bidiToolbar = searchFragment.A07;
            if (i >= 21) {
                bidiToolbar.setElevation(0.0f);
            } else {
                bidiToolbar.setBackgroundResource(0);
                searchFragment.A07.setBackgroundColor(AnonymousClass00T.A00(searchFragment.A0p(), R.color.neutral_primary));
            }
            searchFragment.A02.forceLayout();
        }
    }

    @Override // X.AnonymousClass01E
    public void A0k(Bundle bundle) {
        super.A0k(bundle);
        SearchViewModel searchViewModel = this.A1G;
        ActivityC000900k A0C = A0C();
        searchViewModel.A0G.A05(A0C, new AnonymousClass02B() { // from class: X.4tf
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0K();
            }
        });
        searchViewModel.A0E.A05(A0C, new AnonymousClass02B() { // from class: X.4td
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0K();
            }
        });
        searchViewModel.A0D.A05(A0C, new AnonymousClass02B() { // from class: X.3Qt
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel2 = SearchViewModel.this;
                String str = (String) obj;
                if (!C29941Vi.A00(str, searchViewModel2.A0a)) {
                    searchViewModel2.A0a = str;
                    if (!TextUtils.isEmpty(str)) {
                        C49512La r6 = searchViewModel2.A0z;
                        int A05 = searchViewModel2.A05();
                        Runnable runnable = r6.A02;
                        if (runnable != null) {
                            r6.A03.removeCallbacks(runnable);
                        }
                        RunnableBRunnable0Shape0S0201000_I0 runnableBRunnable0Shape0S0201000_I0 = new RunnableBRunnable0Shape0S0201000_I0((Object) r6, (Object) 4, A05, 28);
                        r6.A02 = runnableBRunnable0Shape0S0201000_I0;
                        r6.A03.postDelayed(runnableBRunnable0Shape0S0201000_I0, 500);
                    }
                    searchViewModel2.A0H();
                }
            }
        });
        searchViewModel.A05.A05(A0C, new AnonymousClass02B() { // from class: X.4tb
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel2 = SearchViewModel.this;
                Integer num = (Integer) obj;
                if (!C29941Vi.A00(num, searchViewModel2.A0X)) {
                    searchViewModel2.A0X = num;
                    searchViewModel2.A0H();
                }
            }
        });
        searchViewModel.A04.A05(A0C, new AnonymousClass02B() { // from class: X.4tc
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel2 = SearchViewModel.this;
                UserJid userJid = (UserJid) obj;
                if (!C29941Vi.A00(userJid, searchViewModel2.A0J)) {
                    searchViewModel2.A0J = userJid;
                    searchViewModel2.A0H();
                }
            }
        });
        searchViewModel.A06.A05(A0C, new AnonymousClass02B() { // from class: X.4te
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel2 = SearchViewModel.this;
                C49662Lr r3 = (C49662Lr) obj;
                if (!C29941Vi.A00(r3, searchViewModel2.A0N)) {
                    searchViewModel2.A0N = r3;
                    searchViewModel2.A0H();
                }
            }
        });
    }

    @Override // X.AnonymousClass01E
    public boolean A0o(MenuItem menuItem) {
        if (!this.A0b.A03(menuItem, this, A0C())) {
            return false;
        }
        A19();
        return true;
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        this.A0b.A02(i);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        this.A0o.A01(bundle);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        StringBuilder sb = new StringBuilder("SearchFragment/onCreateView ");
        sb.append(this);
        Log.i(sb.toString());
        try {
            AnonymousClass1Q8.A00(this.A0e.A00);
        } catch (IOException unused) {
        }
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(R.layout.search_fragment, viewGroup, false);
        this.A02 = viewGroup2;
        this.A03 = (RecyclerView) viewGroup2.findViewById(R.id.result_list);
        this.A01 = this.A02.findViewById(R.id.divider);
        SearchViewModel searchViewModel = this.A1G;
        try {
            searchViewModel.A15.Ab2(new RunnableBRunnable0Shape11S0100000_I0_11(searchViewModel.A0y, 28));
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("SearchViewModel/warmContacts/");
            sb2.append(searchViewModel.A15);
            sb2.append("/");
            sb2.append(searchViewModel.A0y);
            Log.e(sb2.toString(), e);
        }
        if (!this.A1Z) {
            if (this.A02 != null) {
                SearchViewModel searchViewModel2 = this.A1G;
                if (searchViewModel2 != null) {
                    searchViewModel2.A0P(1);
                }
                this.A1Y = this.A1T.AbK(new RunnableBRunnable0Shape11S0100000_I0_11(this, 24), "SearchFragment/setupAnimation", 50);
                this.A02.addOnLayoutChangeListener(new View$OnLayoutChangeListenerC101064mv(this));
            }
            this.A1Z = true;
        }
        IteratingPlayer iteratingPlayer = new IteratingPlayer(this.A03, this.A0C);
        this.A1E = iteratingPlayer;
        super.A0K.A00(iteratingPlayer);
        super.A0K.A00(this.A1G);
        C16590pI r6 = this.A0e;
        C26501Ds r5 = this.A1V;
        C39091pH r13 = new C39091pH(this.A0I, this.A0R, this.A0U, r6, this.A0h, r5, C39091pH.A00(this.A1T));
        this.A1U = r13;
        C14830m7 r0 = this.A0d;
        C14850m9 r02 = this.A0z;
        ActivityC000900k A0B = A0B();
        AnonymousClass13H r03 = this.A16;
        C009804x r04 = super.A0K;
        AnonymousClass1CY r05 = this.A1P;
        C14900mE r06 = this.A0C;
        AbstractC15710nm r07 = this.A09;
        AbstractC14440lR r08 = this.A1T;
        C15570nT r09 = this.A0D;
        C16590pI r010 = this.A0e;
        C19990v2 r011 = this.A0j;
        AnonymousClass19M r012 = this.A0y;
        C15450nH r013 = this.A0E;
        AnonymousClass12P r014 = this.A06;
        C244415n r015 = this.A15;
        AnonymousClass14X r016 = this.A1C;
        AnonymousClass130 r017 = this.A0Q;
        C15550nR r018 = this.A0R;
        C253519b r019 = this.A08;
        C241714m r020 = this.A0n;
        AnonymousClass01d r021 = this.A0c;
        C15610nY r022 = this.A0T;
        AnonymousClass018 r023 = this.A0h;
        C17070qD r024 = this.A1B;
        AnonymousClass1BK r025 = this.A0t;
        C238013b r026 = this.A0L;
        C20710wC r027 = this.A11;
        AnonymousClass10Y r028 = this.A0s;
        AnonymousClass12F r029 = this.A1L;
        C15860o1 r030 = this.A1J;
        C15670ni r031 = this.A0x;
        C15890o4 r032 = this.A0f;
        AnonymousClass1J1 r033 = this.A0V;
        C21400xM r034 = this.A0w;
        C14820m6 r035 = this.A0g;
        C236812p r15 = this.A0r;
        C22710zW r14 = this.A1A;
        C14650lo r10 = this.A0I;
        C15600nX r9 = this.A0q;
        C16630pM r8 = this.A1D;
        AnonymousClass11L r7 = this.A0F;
        SearchViewModel searchViewModel3 = this.A1G;
        C20830wO r52 = this.A0k;
        IteratingPlayer iteratingPlayer2 = this.A1E;
        C36911kq r036 = new C36911kq(A0B, r04, r014, r019, r07, this.A0B, r06, r09, r013, r7, r10, r026, this.A0M, r017, r018, r022, r033, r021, r0, r010, r032, r035, r023, r011, r52, r020, r9, r15, r028, r025, r034, r031, r012, r02, r027, this.A14, r015, r03, r14, r024, r016, r8, iteratingPlayer2, searchViewModel3, r030, r029, r05, r08, r13);
        this.A1F = r036;
        if (this.A0z.A07(1533)) {
            StatusesViewModel statusesViewModel = (StatusesViewModel) new AnonymousClass02A(new C67323Rb(this.A0A, true), this).A00(StatusesViewModel.class);
            this.A1O = statusesViewModel;
            statusesViewModel.A00 = this;
            statusesViewModel.A05.A05(A0G(), new AnonymousClass02B(r036, this) { // from class: X.3RP
                public final /* synthetic */ C36911kq A00;
                public final /* synthetic */ SearchFragment A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    ArrayList A0x;
                    Object obj2;
                    SearchFragment searchFragment = this.A01;
                    C36911kq r53 = this.A00;
                    Map map = (Map) obj;
                    Set set = searchFragment.A1O.A0G;
                    synchronized (set) {
                        A0x = C12980iv.A0x(set);
                        set.clear();
                    }
                    HashSet hashSet = new HashSet(A0x);
                    r53.A03 = map;
                    int i = 0;
                    while (true) {
                        C44091yC r1 = r53.A0q;
                        if (i < r1.size()) {
                            AnonymousClass2Vu r2 = r1.get(i);
                            int i2 = r2.A00;
                            if (i2 == 3) {
                                Object obj3 = r2.A01;
                                if (obj3 instanceof C15370n3) {
                                    obj2 = ((C15370n3) obj3).A0D;
                                    if (obj2 != null && hashSet.contains(obj2)) {
                                        r53.A03(i);
                                    }
                                    i++;
                                } else {
                                    i++;
                                }
                            } else {
                                if (i2 == 2) {
                                    obj2 = r2.A01;
                                    if (!(obj2 instanceof C27631Ih)) {
                                    }
                                    if (obj2 != null) {
                                        r53.A03(i);
                                    }
                                }
                                i++;
                            }
                        } else {
                            return;
                        }
                    }
                }
            });
            super.A0K.A00(this.A1O);
        }
        this.A03.A0l(new C54682h7(this.A1F, AnonymousClass00T.A00(A0p(), R.color.divider_gray), AnonymousClass3G9.A01(A0p(), 0.5f)));
        Context A01 = A01();
        SearchGridLayoutManager searchGridLayoutManager = new SearchGridLayoutManager(A01, this.A1F);
        RecyclerView recyclerView = this.A03;
        recyclerView.A0h = true;
        recyclerView.setLayoutManager(searchGridLayoutManager);
        this.A03.setAdapter(this.A1F);
        RecyclerView recyclerView2 = this.A03;
        IteratingPlayer iteratingPlayer3 = this.A1E;
        List list = recyclerView2.A0a;
        if (list == null) {
            list = new ArrayList();
            recyclerView2.A0a = list;
        }
        list.add(iteratingPlayer3);
        this.A03.A0l(new C54612h0(this.A0h, A02().getDimensionPixelSize(R.dimen.search_grid_padding)));
        RecyclerView recyclerView3 = this.A03;
        recyclerView3.A0l(new C54692h8(A01, recyclerView3, this.A1F, null));
        this.A03.setItemAnimator(null);
        C14830m7 r037 = this.A0d;
        C14900mE r038 = this.A0C;
        AbstractC14440lR r039 = this.A1T;
        C20650w6 r040 = this.A0i;
        C15450nH r53 = this.A0E;
        C18850tA r4 = this.A0P;
        C15550nR r041 = this.A0R;
        C22230yk r3 = this.A18;
        C20710wC r042 = this.A11;
        C15860o1 r043 = this.A1J;
        AnonymousClass132 r2 = this.A0v;
        C242114q r1 = this.A0u;
        C255719x r044 = this.A1S;
        C15600nX r045 = this.A0q;
        C42581vS r27 = new C42581vS(A01, r038, r53, r4, r041, new AnonymousClass5U3() { // from class: X.3WU
            @Override // X.AnonymousClass5U3
            public final void A5X(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
                SearchFragment searchFragment = SearchFragment.this;
                View view = ((AnonymousClass01E) searchFragment).A0A;
                if (searchFragment.A0p() != null && view != null) {
                    C34271fr A00 = C34271fr.A00(view.findViewById(R.id.search_fragment), charSequence, 0);
                    A00.A07(charSequence2, onClickListener);
                    A00.A06(AnonymousClass00T.A00(searchFragment.A0p(), R.color.snackbarButton));
                    C80703so r12 = new C80703so(searchFragment);
                    List list2 = ((AbstractC15160mf) A00).A01;
                    if (list2 == null) {
                        list2 = C12960it.A0l();
                        ((AbstractC15160mf) A00).A01 = list2;
                    }
                    list2.add(r12);
                    searchFragment.A04 = A00;
                    A00.A03();
                }
            }
        }, r037, r040, r045, r1, r2, r042, r3, r043, r044, r039);
        AnonymousClass01F r046 = super.A0H;
        C253018w r047 = this.A1M;
        C15570nT r048 = this.A0D;
        C19990v2 r049 = this.A0j;
        AnonymousClass12U r050 = this.A1N;
        C16170oZ r152 = this.A0G;
        AnonymousClass018 r142 = this.A0h;
        AnonymousClass12F r12 = this.A1L;
        AnonymousClass198 r102 = this.A1Q;
        C18330sH r92 = this.A0H;
        C254219i r82 = this.A1R;
        this.A0b = new C42571vR(A01, r046, r038, r048, r152, r92, this.A0I, r041, this.A0X, this.A0Y, r27, r037, this.A0g, r142, r040, r049, this.A0m, r045, r042, this.A19, r043, r12, r047, r050, r102, r82, r044, r039, 1);
        this.A03.setOnCreateContextMenuListener(this);
        BidiToolbar bidiToolbar = (BidiToolbar) this.A02.findViewById(R.id.toolbar);
        this.A07 = bidiToolbar;
        bidiToolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(A01, R.drawable.ic_back, R.color.searchNavigateBackTint), this.A0h));
        this.A07.setNavigationContentDescription(R.string.back);
        this.A07.setNavigationOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 42));
        this.A07.setContentInsetStartWithNavigation(A02().getDimensionPixelSize(R.dimen.search_input_offset));
        TokenizedSearchInput tokenizedSearchInput = (TokenizedSearchInput) this.A07.findViewById(R.id.search_input_layout);
        this.A1I = tokenizedSearchInput;
        AbstractC001200n A0G = A0G();
        tokenizedSearchInput.A0C = this.A1G;
        tokenizedSearchInput.A06 = A0G;
        Chip chip = tokenizedSearchInput.A0R;
        chip.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(tokenizedSearchInput, 43));
        Chip chip2 = tokenizedSearchInput.A0P;
        chip2.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(tokenizedSearchInput, 46));
        Chip chip3 = tokenizedSearchInput.A0Q;
        chip3.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(tokenizedSearchInput, 47));
        AnonymousClass04v r051 = tokenizedSearchInput.A05;
        AnonymousClass028.A0g(chip, r051);
        AnonymousClass028.A0g(chip2, r051);
        AnonymousClass028.A0g(chip3, r051);
        tokenizedSearchInput.A0C.A05.A05(A0G, new AnonymousClass02B() { // from class: X.4ti
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput tokenizedSearchInput2 = TokenizedSearchInput.this;
                Integer num = (Integer) obj;
                if (tokenizedSearchInput2.A0C != null && !C29941Vi.A00(tokenizedSearchInput2.A0G, num)) {
                    tokenizedSearchInput2.A0G = num;
                    tokenizedSearchInput2.A0F();
                    tokenizedSearchInput2.A0C();
                }
            }
        });
        tokenizedSearchInput.A0C.A04.A05(A0G, new AnonymousClass02B() { // from class: X.3Qu
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput tokenizedSearchInput2 = TokenizedSearchInput.this;
                UserJid userJid = (UserJid) obj;
                if (tokenizedSearchInput2.A0C != null && tokenizedSearchInput2.A0B != userJid) {
                    tokenizedSearchInput2.A0B = userJid;
                    if (userJid != null) {
                        tokenizedSearchInput2.A0P.setText(tokenizedSearchInput2.A08.A04(tokenizedSearchInput2.A07.A0B(userJid)));
                    }
                    tokenizedSearchInput2.A0D();
                    tokenizedSearchInput2.A0C();
                }
            }
        });
        tokenizedSearchInput.A0C.A0D.A05(A0G, new AnonymousClass02B() { // from class: X.4tj
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput.this.A0I((String) obj);
            }
        });
        tokenizedSearchInput.A0C.A0H.A05(A0G, new AnonymousClass02B() { // from class: X.4tg
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput.A07(TokenizedSearchInput.this, (Boolean) obj);
            }
        });
        tokenizedSearchInput.A0C.A06.A05(A0G, new AnonymousClass02B() { // from class: X.4tk
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput tokenizedSearchInput2 = TokenizedSearchInput.this;
                C49662Lr r32 = (C49662Lr) obj;
                if (tokenizedSearchInput2.A0C != null && !C29941Vi.A00(tokenizedSearchInput2.A0D, r32)) {
                    tokenizedSearchInput2.A0D = r32;
                    tokenizedSearchInput2.A0E();
                    tokenizedSearchInput2.A0C();
                }
            }
        });
        FinalBackspaceAwareEntry finalBackspaceAwareEntry = tokenizedSearchInput.A0U;
        finalBackspaceAwareEntry.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(tokenizedSearchInput, 44));
        if (!tokenizedSearchInput.A0A.A04().A06) {
            finalBackspaceAwareEntry.setSingleLine(true);
        }
        finalBackspaceAwareEntry.setFilters(new InputFilter[]{new InputFilter.LengthFilter(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)});
        AnonymousClass4L6 r16 = tokenizedSearchInput.A0E;
        List list2 = finalBackspaceAwareEntry.A01;
        if (list2 == null) {
            list2 = new ArrayList();
            finalBackspaceAwareEntry.A01 = list2;
        }
        list2.add(r16);
        C105294tp r22 = new AnonymousClass02B() { // from class: X.4tp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                FinalBackspaceAwareEntry.A00(FinalBackspaceAwareEntry.this, (String) obj);
            }
        };
        TokenizedSearchInput tokenizedSearchInput2 = r16.A00;
        SearchViewModel searchViewModel4 = tokenizedSearchInput2.A0C;
        if (searchViewModel4 != null) {
            searchViewModel4.A0D.A05(tokenizedSearchInput2.A06, r22);
        }
        finalBackspaceAwareEntry.setOnFocusChangeListener(tokenizedSearchInput.A0N);
        finalBackspaceAwareEntry.setInputEnterAction(3);
        finalBackspaceAwareEntry.setOnEditorActionListener(tokenizedSearchInput.A04);
        finalBackspaceAwareEntry.setOnKeyListener(tokenizedSearchInput.A03);
        if (!tokenizedSearchInput.A0M) {
            LayoutTransition layoutTransition = new LayoutTransition();
            layoutTransition.setAnimator(2, null);
            layoutTransition.setDuration(100);
            ((ViewGroup) AnonymousClass028.A0D(tokenizedSearchInput, R.id.input_layout)).setLayoutTransition(layoutTransition);
        }
        tokenizedSearchInput.A0T.setOnClickListener(tokenizedSearchInput.A02);
        tokenizedSearchInput.A0G = Integer.valueOf(tokenizedSearchInput.A0C.A05());
        tokenizedSearchInput.A0B = tokenizedSearchInput.A0C.A08();
        tokenizedSearchInput.A0J = tokenizedSearchInput.A0C.A0C();
        tokenizedSearchInput.A0D = tokenizedSearchInput.A0C.A0B();
        tokenizedSearchInput.A0F();
        tokenizedSearchInput.A0E();
        UserJid userJid = tokenizedSearchInput.A0B;
        if (userJid != null) {
            chip2.setText(tokenizedSearchInput.A08.A04(tokenizedSearchInput.A07.A0B(userJid)));
        }
        tokenizedSearchInput.A0D();
        tokenizedSearchInput.A0C();
        AnonymousClass009.A05(tokenizedSearchInput.A0C);
        tokenizedSearchInput.A09();
        tokenizedSearchInput.A0S.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(tokenizedSearchInput, 45));
        tokenizedSearchInput.A0C.A0A.A05(tokenizedSearchInput.A06, new AnonymousClass02B() { // from class: X.4th
            /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
                if (r4.booleanValue() == false) goto L_0x000d;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r4) {
                /*
                    r3 = this;
                    com.whatsapp.search.views.TokenizedSearchInput r2 = com.whatsapp.search.views.TokenizedSearchInput.this
                    java.lang.Boolean r4 = (java.lang.Boolean) r4
                    if (r4 == 0) goto L_0x000d
                    boolean r1 = r4.booleanValue()
                    r0 = 1
                    if (r1 != 0) goto L_0x000e
                L_0x000d:
                    r0 = 0
                L_0x000e:
                    r2.A0K = r0
                    r2.A09()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C105214th.ANq(java.lang.Object):void");
            }
        });
        tokenizedSearchInput.A0C.A09.A05(tokenizedSearchInput.A06, new AnonymousClass02B() { // from class: X.4tl
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TokenizedSearchInput tokenizedSearchInput3 = TokenizedSearchInput.this;
                Boolean bool = (Boolean) obj;
                int i = 0;
                if (bool != null && bool.booleanValue()) {
                    i = 1;
                }
                tokenizedSearchInput3.A01 = i;
                tokenizedSearchInput3.A09();
            }
        });
        this.A1H = (ProgressView) AnonymousClass028.A0D(this.A02, R.id.progress);
        this.A03.A0n(this.A1a);
        if (!AnonymousClass1SF.A0P(this.A0z) || !C21280xA.A01()) {
            C41691tw.A03(A0C(), R.color.searchStatusBar);
        }
        return this.A02;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        StringBuilder sb = new StringBuilder("SearchFragment/onDestroy ");
        sb.append(this);
        Log.i(sb.toString());
        if (super.A0g) {
            this.A0p.A03(null, getClass().getName());
        }
        this.A0S.A04(this.A1d);
        this.A0N.A04(this.A1c);
        this.A0l.A04(this.A1e);
        this.A0J.A04(this.A1b);
        this.A12.A04(this.A1f);
        super.A11();
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        StringBuilder sb = new StringBuilder("SearchFragment/onDestroyView ");
        sb.append(this);
        Log.i(sb.toString());
        Runnable runnable = this.A1Y;
        if (runnable != null) {
            this.A1T.AaP(runnable);
        }
        ValueAnimator valueAnimator = this.A00;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        A19();
        TokenizedSearchInput tokenizedSearchInput = this.A1I;
        FinalBackspaceAwareEntry finalBackspaceAwareEntry = tokenizedSearchInput.A0U;
        AnonymousClass4L6 r1 = tokenizedSearchInput.A0E;
        List list = finalBackspaceAwareEntry.A01;
        if (list != null) {
            list.remove(r1);
        }
        finalBackspaceAwareEntry.setOnFocusChangeListener(null);
        finalBackspaceAwareEntry.setOnEditorActionListener(null);
        finalBackspaceAwareEntry.setOnKeyListener(null);
        RecyclerView recyclerView = this.A03;
        IteratingPlayer iteratingPlayer = this.A1E;
        List list2 = recyclerView.A0a;
        if (list2 != null) {
            list2.remove(iteratingPlayer);
        }
        this.A03.A0o(this.A1a);
        super.A0K.A01(this.A1E);
        this.A0V.A00();
        this.A1U.A06();
        this.A02 = null;
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        StringBuilder sb = new StringBuilder("SearchFragment/onResume ");
        sb.append(this);
        Log.i(sb.toString());
        super.A13();
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        StringBuilder sb = new StringBuilder("SearchFragment/onStop ");
        sb.append(this);
        Log.i(sb.toString());
        super.A14();
    }

    @Override // com.whatsapp.search.Hilt_SearchFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A0V = this.A0W.A04(context.getApplicationContext(), "search-fragment");
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        StringBuilder sb = new StringBuilder("SearchFragment/onCreate ");
        sb.append(this);
        Log.i(sb.toString());
        super.A16(bundle);
        boolean z = false;
        if (bundle != null) {
            z = true;
        }
        this.A1Z = z;
        AbstractC14740ly r3 = (AbstractC14740ly) A0B();
        if (r3 != null && !r3.isFinishing()) {
            this.A0o.A00(bundle);
            this.A0p.A02(null, getClass().getName());
            SearchViewModel AGU = r3.AGU(this.A0V);
            this.A1G = AGU;
            AGU.A0E.A05(this, new AnonymousClass02B() { // from class: X.3Qs
                /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
                @Override // X.AnonymousClass02B
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void ANq(java.lang.Object r9) {
                    /*
                        r8 = this;
                        com.whatsapp.search.SearchFragment r4 = com.whatsapp.search.SearchFragment.this
                        X.1yC r9 = (X.C44091yC) r9
                        X.1kq r6 = r4.A1F
                        com.whatsapp.search.SearchViewModel r0 = r4.A1G
                        java.lang.String r7 = r0.A0C()
                        X.0pI r1 = r6.A0R
                        X.018 r0 = r6.A0U
                        X.1yC r5 = new X.1yC
                        r5.<init>(r1, r0)
                        r5.addAll(r9)
                        boolean r0 = r5.isEmpty()
                        if (r0 != 0) goto L_0x0063
                        androidx.recyclerview.widget.RecyclerView r1 = r6.A00
                        if (r1 == 0) goto L_0x0083
                        r0 = -1
                        boolean r0 = r1.canScrollVertically(r0)
                        if (r0 != 0) goto L_0x0083
                        com.whatsapp.search.SearchViewModel r0 = r6.A0r
                        X.016 r1 = r0.A0F
                        java.lang.Object r0 = r1.A01()
                        if (r0 == 0) goto L_0x003e
                        java.lang.Object r0 = r1.A01()
                        int r1 = X.C12960it.A05(r0)
                        r0 = 6
                        if (r1 > r0) goto L_0x0083
                    L_0x003e:
                        r3 = 1
                    L_0x003f:
                        X.1yC r2 = r6.A0q
                        java.lang.String r1 = r6.A01
                        X.2g6 r0 = new X.2g6
                        r0.<init>(r2, r5, r1, r7)
                        X.0SZ r1 = X.AnonymousClass0RD.A00(r0)
                        r2.clear()
                        r2.addAll(r5)
                        r6.A01 = r7
                        X.3Rz r0 = r6.A0p
                        r1.A01(r0)
                        if (r3 == 0) goto L_0x0063
                        com.whatsapp.search.SearchViewModel r0 = r6.A0r
                        r1 = 0
                        X.1It r0 = r0.A0T
                        X.C12960it.A1A(r0, r1)
                    L_0x0063:
                        X.1kq r3 = r4.A1F
                        r2 = 0
                    L_0x0066:
                        X.1yC r1 = r3.A0q
                        int r0 = r1.size()
                        if (r2 >= r0) goto L_0x0085
                        java.lang.Object r0 = r1.get(r2)
                        X.2Vu r0 = (X.AnonymousClass2Vu) r0
                        int r1 = r0.A00
                        r0 = 1
                        if (r1 == r0) goto L_0x007d
                        r0 = 12
                        if (r1 != r0) goto L_0x0080
                    L_0x007d:
                        r3.A03(r2)
                    L_0x0080:
                        int r2 = r2 + 1
                        goto L_0x0066
                    L_0x0083:
                        r3 = 0
                        goto L_0x003f
                    L_0x0085:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C67233Qs.ANq(java.lang.Object):void");
                }
            });
            SearchViewModel searchViewModel = this.A1G;
            searchViewModel.A08.A05(this, new AnonymousClass02B() { // from class: X.3Qq
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    C36911kq r32 = SearchFragment.this.A1F;
                    int i = 0;
                    while (true) {
                        C44091yC r1 = r32.A0q;
                        if (i >= r1.size()) {
                            return;
                        }
                        if (obj.equals(r1.get(i).A01)) {
                            r32.A03(i);
                            return;
                        }
                        i++;
                    }
                }
            });
            this.A1G.A0B.A05(this, new AnonymousClass02B() { // from class: X.3Ql
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x0010, code lost:
                    continue;
                 */
                @Override // X.AnonymousClass02B
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void ANq(java.lang.Object r7) {
                    /*
                        r6 = this;
                        com.whatsapp.search.SearchFragment r0 = com.whatsapp.search.SearchFragment.this
                        java.util.Collection r7 = (java.util.Collection) r7
                        X.1kq r5 = r0.A1F
                        if (r7 != 0) goto L_0x000c
                        r5.A02()
                    L_0x000b:
                        return
                    L_0x000c:
                        java.util.Iterator r4 = r7.iterator()
                    L_0x0010:
                        boolean r0 = r4.hasNext()
                        if (r0 == 0) goto L_0x000b
                        java.lang.Object r3 = r4.next()
                        if (r3 == 0) goto L_0x0010
                        r2 = 0
                    L_0x001d:
                        X.1yC r1 = r5.A0q
                        int r0 = r1.size()
                        if (r2 >= r0) goto L_0x0010
                        java.lang.Object r0 = r1.get(r2)
                        X.2Vu r0 = (X.AnonymousClass2Vu) r0
                        java.lang.Object r0 = r0.A01
                        boolean r0 = r3.equals(r0)
                        if (r0 == 0) goto L_0x0037
                        r5.A03(r2)
                        return
                    L_0x0037:
                        int r2 = r2 + 1
                        goto L_0x001d
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C67163Ql.ANq(java.lang.Object):void");
                }
            });
            this.A0S.A03(this.A1d);
            this.A0N.A03(this.A1c);
            this.A0l.A03(this.A1e);
            this.A0J.A03(this.A1b);
            this.A12.A03(this.A1f);
            this.A1G.A03.A05(this, new AnonymousClass02B() { // from class: X.4tV
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    ActivityC000900k A0B;
                    ViewGroup viewGroup;
                    SearchFragment searchFragment = SearchFragment.this;
                    Number number = (Number) obj;
                    if (number != null && (A0B = searchFragment.A0B()) != null && !A0B.isFinishing()) {
                        int intValue = number.intValue();
                        int i = 4;
                        if (intValue != 0) {
                            if (intValue == 1 || intValue == 2 || intValue == 3 || intValue == 4) {
                                viewGroup = searchFragment.A02;
                                i = 0;
                                viewGroup.setVisibility(i);
                            } else if (intValue != 5) {
                                return;
                            }
                        }
                        viewGroup = searchFragment.A02;
                        viewGroup.setVisibility(i);
                    }
                }
            });
            this.A1G.A0C.A05(this, new AnonymousClass02B() { // from class: X.3Qk
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    int i;
                    long j;
                    ProgressView progressView = SearchFragment.this.A1H;
                    if (progressView != null) {
                        boolean A1S = C12960it.A1S(Boolean.TRUE.equals(obj) ? 1 : 0);
                        AnimatorSet animatorSet = progressView.A00;
                        if (animatorSet != null) {
                            animatorSet.cancel();
                        }
                        progressView.A00 = new AnimatorSet();
                        float f = 0.0f;
                        float f2 = 1.0f;
                        if (A1S) {
                            f2 = 0.0f;
                            f = 1.0f;
                        }
                        if (A1S) {
                            i = progressView.A03;
                        } else {
                            i = 0;
                        }
                        ValueAnimator A00 = C63193Aq.A00(progressView, null, i);
                        CircularProgressBar circularProgressBar = progressView.A04;
                        progressView.A00.playTogether(A00, ObjectAnimator.ofFloat(circularProgressBar, "scaleX", f2, f), ObjectAnimator.ofFloat(circularProgressBar, "scaleY", f2, f));
                        AnimatorSet animatorSet2 = progressView.A00;
                        if (A1S) {
                            j = 800;
                        } else {
                            j = 0;
                        }
                        animatorSet2.setStartDelay(j);
                        progressView.A00.setDuration(800L);
                        progressView.A00.start();
                    }
                }
            });
            SearchViewModel searchViewModel2 = this.A1G;
            searchViewModel2.A0W.A05(this, new AnonymousClass02B() { // from class: X.3Qp
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment searchFragment = SearchFragment.this;
                    AbstractC15340mz r5 = (AbstractC15340mz) obj;
                    searchFragment.A19();
                    if (searchFragment.A0z.A07(931)) {
                        ((AnonymousClass1A1) searchFragment.A1X.get()).A03(r5.A0z.A00);
                    }
                    C51122Sx.A00(new C14960mK().A0m(searchFragment.A01(), r5, searchFragment.A1G.A0C()), searchFragment);
                }
            });
            SearchViewModel searchViewModel3 = this.A1G;
            searchViewModel3.A0U.A05(this, new AnonymousClass02B() { // from class: X.3Qr
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    View view;
                    C004902f r1;
                    SearchFragment searchFragment = SearchFragment.this;
                    AbstractC16130oV r7 = (AbstractC16130oV) obj;
                    AnonymousClass01F A0V = searchFragment.A0C().A0V();
                    int A07 = searchFragment.A1G.A07(r7);
                    if (A07 == -2) {
                        searchFragment.A0C.A07(R.string.gallery_media_not_exist, 0);
                        return;
                    }
                    AnonymousClass03U A0C = searchFragment.A03.A0C(A07);
                    if (A0C != null) {
                        view = A0C.A0H.findViewById(R.id.thumb_view);
                    } else {
                        view = null;
                    }
                    searchFragment.A1G.A0P(4);
                    ActivityC000900k A0C2 = searchFragment.A0C();
                    AnonymousClass2TT r2 = new AnonymousClass2TT(searchFragment.A0C());
                    RunnableBRunnable0Shape11S0100000_I0_11 runnableBRunnable0Shape11S0100000_I0_11 = new RunnableBRunnable0Shape11S0100000_I0_11(searchFragment, 26);
                    AnonymousClass1IS r10 = r7.A0z;
                    MediaViewFragment A01 = MediaViewFragment.A01(AbstractC454421p.A04(A0C2, view), null, r10, 5, 1, 2, SystemClock.uptimeMillis(), false, false, false);
                    boolean z2 = AbstractC454421p.A00;
                    if (z2) {
                        A01.A07().A0F = true;
                    }
                    ((MediaViewBaseFragment) A01).A0D = runnableBRunnable0Shape11S0100000_I0_11;
                    ActivityC000800j r32 = (ActivityC000800j) A0C2;
                    String A0W = AbstractC28561Ob.A0W(r10);
                    if (!z2 || view == null) {
                        r1 = new C004902f(A0V);
                    } else {
                        r1 = C454321o.A00(view, r32, A0V, r2, A0W);
                    }
                    r1.A0H = true;
                    r1.A0B(A01, "media_view_fragment", R.id.media_fragment_holder);
                    r1.A0F("media_view_fragment");
                    r1.A01();
                }
            });
            SearchViewModel searchViewModel4 = this.A1G;
            searchViewModel4.A0O.A05(this, new AnonymousClass02B() { // from class: X.3Qn
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment searchFragment = SearchFragment.this;
                    AbstractC14640lm r6 = (AbstractC14640lm) obj;
                    searchFragment.A19();
                    Intent putExtra = new C14960mK().A0i(searchFragment.A01(), r6).putExtra("start_t", SystemClock.uptimeMillis());
                    if (searchFragment.A0z.A07(931)) {
                        ((AnonymousClass1A1) searchFragment.A1X.get()).A03(r6);
                    }
                    C51122Sx.A00(putExtra, searchFragment);
                }
            });
            SearchViewModel searchViewModel5 = this.A1G;
            searchViewModel5.A0Q.A05(this, new AnonymousClass02B() { // from class: X.3Qo
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    View findViewById;
                    C92434Vw A04;
                    SearchFragment searchFragment = SearchFragment.this;
                    AbstractC14640lm r6 = (AbstractC14640lm) obj;
                    searchFragment.A0a.A06();
                    AnonymousClass03U A0C = searchFragment.A03.A0C(searchFragment.A1G.A06(r6));
                    if (A0C != null) {
                        if (A0C instanceof ViewHolder) {
                            findViewById = ((ViewHolder) A0C).A08;
                        } else {
                            findViewById = A0C.A0H.findViewById(R.id.contact_photo);
                        }
                        if (findViewById != null) {
                            StatusesViewModel statusesViewModel = searchFragment.A1O;
                            if (statusesViewModel == null || (A04 = statusesViewModel.A04(UserJid.of(r6))) == null || !A04.A00() || A04.A01 <= 0) {
                                AnonymousClass3DG r1 = new AnonymousClass3DG(searchFragment.A0z, r6, 17);
                                r1.A02 = AnonymousClass028.A0J(findViewById);
                                r1.A00(searchFragment.A0B(), findViewById);
                                return;
                            }
                            searchFragment.A1O.A08(r6, 9, null);
                            searchFragment.A0v(C14960mK.A0F(searchFragment.A01(), r6, Boolean.TRUE));
                        }
                    }
                }
            });
            SearchViewModel searchViewModel6 = this.A1G;
            searchViewModel6.A0P.A05(this, new AnonymousClass02B() { // from class: X.4tZ
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment searchFragment = SearchFragment.this;
                    AbstractC14640lm r4 = (AbstractC14640lm) obj;
                    searchFragment.A0O.A01 = 4;
                    AnonymousClass03U A0C = searchFragment.A03.A0C(searchFragment.A1G.A06(r4));
                    if (A0C != null) {
                        searchFragment.A13 = r4;
                        searchFragment.A03.showContextMenuForChild(A0C.A0H);
                    }
                }
            });
            SearchViewModel searchViewModel7 = this.A1G;
            searchViewModel7.A0T.A05(this, new AnonymousClass02B() { // from class: X.4tX
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment searchFragment = SearchFragment.this;
                    if (obj != null) {
                        searchFragment.A0C.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(searchFragment, 40, obj));
                    }
                }
            });
            SearchViewModel searchViewModel8 = this.A1G;
            searchViewModel8.A05.A05(this, new AnonymousClass02B() { // from class: X.4tW
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment.this.A1C(obj);
                }
            });
            SearchViewModel searchViewModel9 = this.A1G;
            searchViewModel9.A0D.A05(this, new AnonymousClass02B() { // from class: X.4tY
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment.this.A1C(obj);
                }
            });
            SearchViewModel searchViewModel10 = this.A1G;
            searchViewModel10.A04.A05(this, new AnonymousClass02B() { // from class: X.4ta
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment.this.A1C(obj);
                }
            });
            SearchViewModel searchViewModel11 = this.A1G;
            searchViewModel11.A06.A05(this, new AnonymousClass02B() { // from class: X.4tT
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment.this.A1C(obj);
                }
            });
            SearchViewModel searchViewModel12 = this.A1G;
            searchViewModel12.A0R.A05(this, new AnonymousClass02B() { // from class: X.3Qm
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment searchFragment = SearchFragment.this;
                    searchFragment.A0M.A00(3, 6);
                    Intent className = C12990iw.A0E("android.intent.action.VIEW").setClassName(searchFragment.A01().getPackageName(), "com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity");
                    className.putExtra("INITIAL_CATEGORY", (C30211Wn) obj);
                    searchFragment.A06.A06(searchFragment.A01(), className);
                }
            });
            SearchViewModel searchViewModel13 = this.A1G;
            searchViewModel13.A0V.A05(this, new AnonymousClass02B() { // from class: X.4tU
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SearchFragment.A03(SearchFragment.this, (Boolean) obj);
                }
            });
        }
    }

    public final void A19() {
        if (!this.A0z.A07(931) || !this.A0a.A0C()) {
            this.A0a.A06();
        }
    }

    public final void A1A() {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) this.A03.getLayoutManager();
        if (linearLayoutManager != null) {
            SearchViewModel searchViewModel = this.A1G;
            Integer valueOf = Integer.valueOf(linearLayoutManager.A19() + 6);
            AnonymousClass016 r1 = searchViewModel.A0F;
            if (!valueOf.equals(r1.A01())) {
                r1.A0A(valueOf);
            }
            SearchViewModel searchViewModel2 = this.A1G;
            Integer valueOf2 = Integer.valueOf(linearLayoutManager.A1B());
            AnonymousClass016 r12 = searchViewModel2.A0G;
            if (!valueOf2.equals(r12.A01())) {
                r12.A0A(valueOf2);
            }
        }
    }

    public final void A1B(Bundle bundle, Interpolator interpolator, Runnable runnable, int i, int i2, int i3, int i4, int i5, boolean z) {
        if (this.A02 != null) {
            int i6 = bundle.getInt("x", 0);
            int i7 = bundle.getInt("y", 0);
            float hypot = (float) Math.hypot((double) (i3 - i), (double) (i4 - i2));
            float f = 0.0f;
            if (!z) {
                f = hypot;
                hypot = 0.0f;
            }
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this.A02, i6, i7, f, hypot);
            createCircularReveal.setDuration((long) i5);
            createCircularReveal.setInterpolator(interpolator);
            createCircularReveal.addListener(new C72593er(this, runnable));
            createCircularReveal.start();
        }
    }

    public final void A1C(Object obj) {
        View view;
        LinearLayout linearLayout;
        ValueAnimator valueAnimator;
        A19();
        if (obj != null && !obj.equals(0) && !obj.equals("")) {
            Integer num = 2;
            if (num.equals(this.A1G.A03.A01()) && (view = super.A0A) != null && (linearLayout = (LinearLayout) view.getParent()) != null) {
                RunnableBRunnable0Shape11S0100000_I0_11 runnableBRunnable0Shape11S0100000_I0_11 = new RunnableBRunnable0Shape11S0100000_I0_11(this, 25);
                View view2 = (View) linearLayout.getParent();
                if (view2 != null) {
                    valueAnimator = C63193Aq.A00(linearLayout, new RunnableBRunnable0Shape1S0201000_I1(linearLayout, runnableBRunnable0Shape11S0100000_I0_11), view2.getHeight());
                } else {
                    valueAnimator = null;
                }
                this.A00 = valueAnimator;
                if (valueAnimator != null) {
                    valueAnimator.start();
                }
            }
        }
    }

    public final void A1D(Runnable runnable, int i, int i2, int i3, int i4, boolean z) {
        int i5;
        String str;
        Bundle bundle = super.A05;
        if (bundle != null) {
            if (z) {
                i5 = 500;
                str = "enter_duration_ms";
            } else {
                i5 = 250;
                str = "exit_duration_ms";
            }
            int i6 = bundle.getInt(str, i5);
            AnonymousClass078 r6 = new AnonymousClass078();
            if (Build.VERSION.SDK_INT >= 21) {
                A1B(bundle, r6, runnable, i, i2, i3, i4, i6, z);
                return;
            }
            ViewGroup viewGroup = this.A02;
            if (viewGroup != null) {
                float f = -((float) viewGroup.getHeight());
                float f2 = 0.0f;
                if (!z) {
                    f2 = f;
                    f = 0.0f;
                }
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, f, f2);
                translateAnimation.setInterpolator(r6);
                translateAnimation.setDuration((long) i6);
                translateAnimation.setAnimationListener(new C83403xB(this, runnable));
                this.A02.startAnimation(translateAnimation);
            }
        }
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r2) {
        StatusesViewModel statusesViewModel = this.A1O;
        if (statusesViewModel != null) {
            statusesViewModel.A09(r2);
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (!AnonymousClass1SF.A0P(this.A0z) || !C21280xA.A01()) {
            C41691tw.A03(A0C(), R.color.searchStatusBar);
        }
    }

    @Override // X.AnonymousClass01E, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        this.A0b.A00(contextMenu, this.A13, true, true);
    }
}
