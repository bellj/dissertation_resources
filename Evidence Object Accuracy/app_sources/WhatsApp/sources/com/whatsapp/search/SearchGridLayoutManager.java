package com.whatsapp.search;

import X.AnonymousClass02M;
import X.AnonymousClass0QS;
import X.C05480Ps;
import X.C74703iX;
import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import com.whatsapp.util.Log;

/* loaded from: classes3.dex */
public class SearchGridLayoutManager extends GridLayoutManager {
    public final AnonymousClass02M A00;

    public SearchGridLayoutManager(Context context, AnonymousClass02M r3) {
        super(6);
        this.A00 = r3;
        ((GridLayoutManager) this).A01 = new C74703iX(context, this);
    }

    @Override // androidx.recyclerview.widget.GridLayoutManager, androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public void A0u(AnonymousClass0QS r2, C05480Ps r3) {
        try {
            super.A0u(r2, r3);
        } catch (IndexOutOfBoundsException e) {
            Log.e(e);
        }
    }
}
