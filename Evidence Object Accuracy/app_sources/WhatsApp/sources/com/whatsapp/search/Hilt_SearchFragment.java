package com.whatsapp.search;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Y;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass132;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass15Q;
import X.AnonymousClass17S;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass198;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1AM;
import X.AnonymousClass1BK;
import X.AnonymousClass1CY;
import X.AnonymousClass4JF;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15670ni;
import X.C15680nj;
import X.C15860o1;
import X.C15890o4;
import X.C16030oK;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16590pI;
import X.C16630pM;
import X.C17070qD;
import X.C17220qS;
import X.C18000rk;
import X.C18330sH;
import X.C18850tA;
import X.C19990v2;
import X.C20220vP;
import X.C20650w6;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21270x9;
import X.C21280xA;
import X.C21320xE;
import X.C21400xM;
import X.C22230yk;
import X.C22330yu;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C241714m;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C244415n;
import X.C246716k;
import X.C250417w;
import X.C253018w;
import X.C253519b;
import X.C254219i;
import X.C255719x;
import X.C25641Ae;
import X.C26501Ds;
import X.C48572Gu;
import X.C48962Ip;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_SearchFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A04() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A04();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A04()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.Hilt_SearchFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A04();
        A18();
    }

    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            SearchFragment searchFragment = (SearchFragment) this;
            C51112Sw r2 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r3 = r2.A0Y;
            ((WaFragment) searchFragment).A00 = (AnonymousClass182) r3.A94.get();
            ((WaFragment) searchFragment).A01 = (AnonymousClass180) r3.ALt.get();
            searchFragment.A0d = (C14830m7) r3.ALb.get();
            searchFragment.A0z = (C14850m9) r3.A04.get();
            searchFragment.A1P = (AnonymousClass1CY) r3.ABO.get();
            searchFragment.A1M = (C253018w) r3.AJS.get();
            searchFragment.A0C = (C14900mE) r3.A8X.get();
            searchFragment.A16 = (AnonymousClass13H) r3.ABY.get();
            searchFragment.A09 = (AbstractC15710nm) r3.A4o.get();
            searchFragment.A0D = (C15570nT) r3.AAr.get();
            searchFragment.A0e = (C16590pI) r3.AMg.get();
            searchFragment.A1T = (AbstractC14440lR) r3.ANe.get();
            searchFragment.A05 = (AnonymousClass17S) r3.A0F.get();
            searchFragment.A0j = (C19990v2) r3.A3M.get();
            searchFragment.A1N = (AnonymousClass12U) r3.AJd.get();
            searchFragment.A10 = (C16120oU) r3.ANE.get();
            searchFragment.A0i = (C20650w6) r3.A3A.get();
            searchFragment.A0y = (AnonymousClass19M) r3.A6R.get();
            searchFragment.A0E = (C15450nH) r3.AII.get();
            searchFragment.A0P = (C18850tA) r3.AKx.get();
            searchFragment.A0G = (C16170oZ) r3.AM4.get();
            searchFragment.A1V = (C26501Ds) r3.AML.get();
            searchFragment.A06 = (AnonymousClass12P) r3.A0H.get();
            searchFragment.A0W = (C21270x9) r3.A4A.get();
            searchFragment.A15 = (C244415n) r3.AAg.get();
            searchFragment.A17 = (C17220qS) r3.ABt.get();
            searchFragment.A1C = (AnonymousClass14X) r3.AFM.get();
            searchFragment.A1W = (C21280xA) r3.AMU.get();
            searchFragment.A0Q = (AnonymousClass130) r3.A41.get();
            searchFragment.A0R = (C15550nR) r3.A45.get();
            searchFragment.A08 = (C253519b) r3.A4C.get();
            searchFragment.A0n = (C241714m) r3.A4l.get();
            searchFragment.A0c = (AnonymousClass01d) r3.ALI.get();
            searchFragment.A0T = (C15610nY) r3.AMe.get();
            searchFragment.A18 = (C22230yk) r3.ANT.get();
            searchFragment.A0h = (AnonymousClass018) r3.ANb.get();
            searchFragment.A1B = (C17070qD) r3.AFC.get();
            searchFragment.A0t = (AnonymousClass1BK) r3.AFZ.get();
            searchFragment.A0L = (C238013b) r3.A1Z.get();
            searchFragment.A0O = (C243915i) r3.A37.get();
            searchFragment.A11 = (C20710wC) r3.A8m.get();
            searchFragment.A0s = (AnonymousClass10Y) r3.AAR.get();
            searchFragment.A1L = (AnonymousClass12F) r3.AJM.get();
            searchFragment.A1Q = (AnonymousClass198) r3.A0J.get();
            searchFragment.A1J = (C15860o1) r3.A3H.get();
            searchFragment.A0H = (C18330sH) r3.AN4.get();
            searchFragment.A1R = (C254219i) r3.A0K.get();
            searchFragment.A0N = (C22330yu) r3.A3I.get();
            searchFragment.A0U = (AnonymousClass10T) r3.A47.get();
            searchFragment.A0X = (C20730wE) r3.A4J.get();
            searchFragment.A0v = (AnonymousClass132) r3.ALx.get();
            searchFragment.A0x = (C15670ni) r3.AIb.get();
            searchFragment.A0u = (C242114q) r3.AJq.get();
            searchFragment.A0w = (C21400xM) r3.ABd.get();
            searchFragment.A19 = (C20220vP) r3.AC3.get();
            searchFragment.A0g = (C14820m6) r3.AN3.get();
            searchFragment.A0K = (C246716k) r3.A2X.get();
            searchFragment.A0m = (C15680nj) r3.A4e.get();
            searchFragment.A0r = (C236812p) r3.AAC.get();
            searchFragment.A1K = (AnonymousClass1AM) r3.AJG.get();
            searchFragment.A1A = (C22710zW) r3.AF7.get();
            searchFragment.A0I = (C14650lo) r3.A2V.get();
            searchFragment.A0Y = (C250417w) r3.A4b.get();
            searchFragment.A0J = (AnonymousClass10A) r3.A2W.get();
            searchFragment.A1S = (C255719x) r3.A5X.get();
            searchFragment.A0q = (C15600nX) r3.A8x.get();
            searchFragment.A1D = (C16630pM) r3.AIc.get();
            searchFragment.A12 = (C244215l) r3.A8y.get();
            searchFragment.A0f = (C15890o4) r3.AN1.get();
            searchFragment.A14 = (C16030oK) r3.AAd.get();
            searchFragment.A0F = (AnonymousClass11L) r3.ALG.get();
            searchFragment.A0k = (C20830wO) r3.A4W.get();
            searchFragment.A0o = (AnonymousClass15Q) r3.A6g.get();
            searchFragment.A0p = (C25641Ae) r3.A6i.get();
            searchFragment.A0Z = (AnonymousClass19D) r3.ABo.get();
            searchFragment.A0a = (AnonymousClass11P) r3.ABp.get();
            searchFragment.A0S = (AnonymousClass10S) r3.A46.get();
            searchFragment.A0l = (C21320xE) r3.A4Y.get();
            searchFragment.A0M = (C16430p0) r3.A5y.get();
            searchFragment.A0B = (AnonymousClass4JF) r2.A0L.get();
            searchFragment.A1X = C18000rk.A00(r3.ADw);
            searchFragment.A0A = (C48962Ip) r2.A0V.A00.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
