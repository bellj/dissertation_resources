package com.whatsapp.search;

import X.AbstractC05270Ox;
import X.AbstractC51682Vy;
import X.AnonymousClass03H;
import X.AnonymousClass045;
import X.AnonymousClass074;
import X.C14900mE;
import X.C75053jE;
import android.view.View;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;

/* loaded from: classes.dex */
public class IteratingPlayer implements AnonymousClass045, AnonymousClass03H {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public boolean A03;
    public final AbstractC05270Ox A04 = new C75053jE(this);
    public final RecyclerView A05;
    public final C14900mE A06;
    public final Runnable A07 = new RunnableBRunnable0Shape11S0100000_I0_11(this, 23);

    public IteratingPlayer(RecyclerView recyclerView, C14900mE r4) {
        this.A06 = r4;
        this.A05 = recyclerView;
    }

    public final void A00() {
        if (this.A03) {
            this.A06.A0J(this.A07, 2000);
        }
    }

    public final void A01() {
        this.A03 = false;
        A04(this.A00, false);
        this.A06.A0G(this.A07);
    }

    public final void A02() {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) this.A05.getLayoutManager();
        if (linearLayoutManager != null) {
            this.A01 = linearLayoutManager.A19();
            this.A02 = linearLayoutManager.A1B();
        } else if (!this.A03) {
            this.A03 = true;
            A00();
        }
    }

    public final void A03(int i) {
        if (this.A01 > 0 || this.A02 > 0) {
            A04(this.A00, false);
            int min = Math.min(Math.max(i, this.A01), this.A02);
            int i2 = min;
            while (!A05(i2)) {
                i2++;
                int i3 = this.A02;
                if (i2 > i3) {
                    i2 = this.A01;
                }
                if (min != i2) {
                    if (i2 <= i3) {
                        if (i2 < this.A01) {
                        }
                    }
                }
            }
            A04(i2, true);
            this.A00 = i2;
            return;
        }
        A01();
    }

    public final void A04(int i, boolean z) {
        AbstractC51682Vy r0 = (AbstractC51682Vy) this.A05.A0D(i, false);
        if (r0 != null) {
            r0.A0C(z);
        }
    }

    public final boolean A05(int i) {
        AbstractC51682Vy r0 = (AbstractC51682Vy) this.A05.A0D(i, false);
        if (r0 != null) {
            return r0.A0D();
        }
        return false;
    }

    @Override // X.AnonymousClass045
    public void ANv(View view) {
        A02();
        if (!this.A03) {
            this.A03 = true;
            A00();
        }
    }

    @Override // X.AnonymousClass045
    public void ANw(View view) {
        A02();
    }

    @OnLifecycleEvent(AnonymousClass074.ON_START)
    public void onStart() {
        if (!this.A03) {
            this.A03 = true;
            A00();
        }
    }

    @OnLifecycleEvent(AnonymousClass074.ON_STOP)
    public void onStop() {
        A01();
    }
}
