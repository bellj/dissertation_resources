package com.whatsapp.search;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC35161hL;
import X.AbstractC35521iA;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass018;
import X.AnonymousClass02B;
import X.AnonymousClass02P;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass07E;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass1AM;
import X.AnonymousClass1AW;
import X.AnonymousClass1AY;
import X.AnonymousClass1J1;
import X.AnonymousClass2Vv;
import X.C14850m9;
import X.C14900mE;
import X.C15240mn;
import X.C15550nR;
import X.C15610nY;
import X.C15680nj;
import X.C16590pI;
import X.C20030v6;
import X.C20040v7;
import X.C20830wO;
import X.C21230x5;
import X.C22050yP;
import X.C240514a;
import X.C251118d;
import X.C25601Aa;
import X.C27691It;
import X.C29941Vi;
import X.C35151hK;
import X.C35231hU;
import X.C35531iB;
import X.C36911kq;
import X.C43951xu;
import X.C44091yC;
import X.C49512La;
import X.C49642Lp;
import X.C49652Lq;
import X.C49662Lr;
import X.C51532Vf;
import X.C51552Vh;
import X.C51562Vi;
import X.ExecutorC27271Gr;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.SparseIntArray;
import androidx.lifecycle.OnLifecycleEvent;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.search.SearchViewModel;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes.dex */
public class SearchViewModel extends AnonymousClass015 implements AbstractC35161hL, AnonymousClass03H {
    public long A00 = 0;
    public Handler A01 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.2Ve
        @Override // android.os.Handler.Callback
        public final boolean handleMessage(Message message) {
            SearchViewModel searchViewModel = SearchViewModel.this;
            if (message.what != 0) {
                return false;
            }
            searchViewModel.A0T(new C35531iB());
            return true;
        }
    });
    public SparseIntArray A02 = new SparseIntArray();
    public AnonymousClass017 A03;
    public AnonymousClass017 A04;
    public AnonymousClass017 A05;
    public AnonymousClass017 A06;
    public AnonymousClass017 A07;
    public AnonymousClass02P A08 = new AnonymousClass02P();
    public AnonymousClass02P A09 = new AnonymousClass02P();
    public AnonymousClass02P A0A = new AnonymousClass02P();
    public AnonymousClass02P A0B = new AnonymousClass02P();
    public AnonymousClass02P A0C = new AnonymousClass02P();
    public AnonymousClass02P A0D = new AnonymousClass02P();
    public AnonymousClass02P A0E = new AnonymousClass02P();
    public AnonymousClass016 A0F = new AnonymousClass016();
    public AnonymousClass016 A0G = new AnonymousClass016();
    public AnonymousClass016 A0H = new AnonymousClass016();
    public AbstractC18860tB A0I = new C35151hK(this);
    public UserJid A0J;
    public C35531iB A0K = C35531iB.A00();
    public C51532Vf A0L = new C51532Vf(this);
    public C49642Lp A0M = new C49642Lp();
    public C49662Lr A0N;
    public C27691It A0O = new C27691It();
    public C27691It A0P = new C27691It();
    public C27691It A0Q = new C27691It();
    public C27691It A0R = new C27691It();
    public C27691It A0S = new C27691It();
    public C27691It A0T = new C27691It();
    public C27691It A0U = new C27691It();
    public C27691It A0V = new C27691It();
    public C27691It A0W = new C27691It();
    public Integer A0X;
    public Runnable A0Y;
    public Runnable A0Z;
    public String A0a;
    public List A0b = new ArrayList();
    public List A0c = new ArrayList();
    public List A0d = new ArrayList();
    public AtomicBoolean A0e = new AtomicBoolean();
    public boolean A0f = true;
    public final AnonymousClass07E A0g;
    public final AbstractC15710nm A0h;
    public final C14900mE A0i;
    public final C251118d A0j;
    public final C25601Aa A0k;
    public final C15610nY A0l;
    public final AnonymousClass1J1 A0m;
    public final C16590pI A0n;
    public final AnonymousClass018 A0o;
    public final C15680nj A0p;
    public final C20040v7 A0q;
    public final AnonymousClass12H A0r;
    public final C20030v6 A0s;
    public final C14850m9 A0t;
    public final C22050yP A0u;
    public final C21230x5 A0v;
    public final C51552Vh A0w;
    public final C51562Vi A0x;
    public final C35231hU A0y;
    public final C49512La A0z;
    public final AnonymousClass1AY A10;
    public final AnonymousClass1AM A11;
    public final C240514a A12;
    public final AnonymousClass12F A13;
    public final ExecutorC27271Gr A14;
    public final AbstractC14440lR A15;
    public final AtomicBoolean A16 = new AtomicBoolean();

    public SearchViewModel(AnonymousClass07E r37, AbstractC15710nm r38, C14900mE r39, C251118d r40, C25601Aa r41, C15550nR r42, C15610nY r43, AnonymousClass1J1 r44, C16590pI r45, AnonymousClass018 r46, C20830wO r47, C15680nj r48, C15240mn r49, C20040v7 r50, AnonymousClass12H r51, C20030v6 r52, C14850m9 r53, C22050yP r54, C21230x5 r55, C49512La r56, AnonymousClass1AY r57, AnonymousClass1AM r58, C240514a r59, AnonymousClass12F r60, AnonymousClass1AW r61, AbstractC14440lR r62) {
        this.A0n = r45;
        this.A0t = r53;
        this.A0i = r39;
        this.A15 = r62;
        this.A0h = r38;
        this.A0q = r50;
        this.A0l = r43;
        this.A0o = r46;
        this.A0r = r51;
        this.A0g = r37;
        this.A13 = r60;
        this.A0u = r54;
        this.A12 = r59;
        this.A0m = r44;
        this.A0j = r40;
        this.A0p = r48;
        this.A11 = r58;
        this.A10 = r57;
        this.A0z = r56;
        this.A0s = r52;
        this.A0v = r55;
        this.A0k = r41;
        this.A14 = new ExecutorC27271Gr(r62, true);
        this.A03 = r37.A02("current_screen");
        this.A0D.A0D(r37.A02("query_text"), new AnonymousClass02B() { // from class: X.2Vg
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0D.A0B(obj);
            }
        });
        this.A05 = r37.A02("search_type");
        this.A04 = r37.A02("search_jid");
        this.A06 = r37.A02("smart_filter");
        this.A07 = r37.A02("user_grid_view_choice");
        C51552Vh r6 = new C51552Vh(this.A0D, this.A05, this.A04, this.A06, r43, r46, r47, r48, r55, r57, r61);
        this.A0w = r6;
        C51562Vi r5 = new C51562Vi(this.A0D, this.A05, this.A04, this.A06, r43, r46, r47, r48, r55, r57, r61);
        this.A0x = r5;
        C35231hU r11 = new C35231hU(this.A0D, this.A05, this.A04, this.A06, r42, r43, r46, r49, r53, r55, r57, r61);
        this.A0y = r11;
        A0J();
        this.A0E.A0D(r6.A00(), new AnonymousClass02B() { // from class: X.2Vj
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel = SearchViewModel.this;
                ArrayList arrayList = new ArrayList();
                Iterator it = new ArrayList((Collection) obj).iterator();
                while (it.hasNext()) {
                    C15370n3 r1 = (C15370n3) it.next();
                    if (r1 != null) {
                        Jid A0B = r1.A0B(AbstractC14640lm.class);
                        AnonymousClass009.A05(A0B);
                        arrayList.add(new AnonymousClass2WJ((AbstractC14640lm) A0B));
                    }
                }
                searchViewModel.A0b = arrayList;
                searchViewModel.A0I();
            }
        });
        this.A0E.A0D(r5.A00(), new AnonymousClass02B() { // from class: X.2Vk
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel = SearchViewModel.this;
                ArrayList arrayList = new ArrayList();
                Iterator it = new ArrayList((Collection) obj).iterator();
                while (it.hasNext()) {
                    C15370n3 r1 = (C15370n3) it.next();
                    if (r1 != null) {
                        arrayList.add(new AnonymousClass2WK(r1));
                    }
                }
                searchViewModel.A0d = arrayList;
                searchViewModel.A0I();
            }
        });
        this.A0E.A0D(r11.A06(), new AnonymousClass02B() { // from class: X.2Vl
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0T((C35531iB) obj);
            }
        });
        this.A0E.A0D(r11.A01(), new AnonymousClass02B() { // from class: X.2Vm
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel = SearchViewModel.this;
                List list = (List) obj;
                if (searchViewModel.A06.A01() != null) {
                    list = new ArrayList();
                }
                searchViewModel.A0c = list;
                searchViewModel.A0I();
            }
        });
        this.A0E.A0D(r11.A02(), new AnonymousClass02B() { // from class: X.2Vn
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel = SearchViewModel.this;
                SparseIntArray sparseIntArray = (SparseIntArray) obj;
                if (sparseIntArray == null) {
                    sparseIntArray = new SparseIntArray();
                    sparseIntArray.put(105, 1);
                }
                synchronized (searchViewModel) {
                    if (searchViewModel.A06.A01() != null) {
                        sparseIntArray = new SparseIntArray();
                    }
                    searchViewModel.A02 = sparseIntArray;
                }
                searchViewModel.A0I();
            }
        });
        this.A0E.A0D(this.A07, new AnonymousClass02B() { // from class: X.2Vo
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0I();
            }
        });
        this.A0C.A0D(r11.A00(), new AnonymousClass02B() { // from class: X.2Vp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(r11.A03(), new AnonymousClass02B() { // from class: X.2Vp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(r11.A04(), new AnonymousClass02B() { // from class: X.2Vp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(r6.A01(), new AnonymousClass02B() { // from class: X.2Vp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(r5.A01(), new AnonymousClass02B() { // from class: X.2Vp
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(this.A0G, new AnonymousClass02B() { // from class: X.2Vq
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0C.A0D(this.A0E, new AnonymousClass02B() { // from class: X.2Vr
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel.this.A0N();
            }
        });
        this.A0D.A0D(r11.A05(), new AnonymousClass02B(r37, this) { // from class: X.2Vs
            public final /* synthetic */ AnonymousClass07E A00;
            public final /* synthetic */ SearchViewModel A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SearchViewModel searchViewModel = this.A01;
                AnonymousClass07E r510 = this.A00;
                String str = (String) obj;
                if (str == null) {
                    str = "";
                }
                if (!str.equals(searchViewModel.A0C())) {
                    searchViewModel.A0M.A00(new C49652Lq(searchViewModel.A08(), Integer.valueOf(searchViewModel.A05()), str, 3));
                    r510.A04("query_text", str);
                }
            }
        });
        this.A0E.A0B(A09());
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0m.A00();
        A0M();
    }

    public int A04() {
        Number number = (Number) this.A0g.A02.get("last_nav_type");
        if (number == null) {
            return 0;
        }
        return number.intValue();
    }

    public int A05() {
        Number number = (Number) this.A0g.A01(0, "search_type").A01();
        if (number != null) {
            return number.intValue();
        }
        return 0;
    }

    public int A06(AbstractC14640lm r6) {
        C44091yC A0A = A0A();
        int i = -2;
        for (int i2 = 0; i2 < A0A.size(); i2++) {
            if ((A0A.get(i2).A00() == 3 || A0A.get(i2).A00() == 2) && C29941Vi.A00(A0A.A01(i2).ADg(), r6)) {
                i = i2;
            }
        }
        return i;
    }

    public int A07(AbstractC16130oV r6) {
        int i = -2;
        if (this.A0K.A01.contains(r6)) {
            C44091yC A0A = A0A();
            for (int i2 = 0; i2 < A0A.size(); i2++) {
                int A00 = A0A.A00(i2);
                if ((C36911kq.A00(A00) || A00 == 17 || A00 == 18 || A00 == 16 || A00 == 14) && C29941Vi.A00(A0A.A02(i2), r6)) {
                    i = i2;
                }
            }
        }
        return i;
    }

    public UserJid A08() {
        return (UserJid) this.A0g.A01(null, "search_jid").A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b9, code lost:
        if (A0Y() == false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f2, code lost:
        if (r8.A03.size() <= 0) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x010c, code lost:
        if (A0c() != false) goto L_0x010e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C44091yC A09() {
        /*
        // Method dump skipped, instructions count: 447
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.SearchViewModel.A09():X.1yC");
    }

    public final C44091yC A0A() {
        AnonymousClass02P r1 = this.A0E;
        if (r1.A01() == null) {
            return A09();
        }
        return (C44091yC) r1.A01();
    }

    public C49662Lr A0B() {
        return (C49662Lr) this.A0g.A01(null, "smart_filter").A01();
    }

    public String A0C() {
        String str = (String) this.A0g.A01("", "query_text").A01();
        return str != null ? str : "";
    }

    public void A0D() {
        A0R(null);
    }

    public void A0E() {
        A0U(null);
    }

    public void A0F() {
        A0Q(0);
        A0D();
        A0E();
        A0X(false);
        A0V("");
        this.A0g.A04("user_grid_view_choice", null);
        this.A0S.A0B(null);
        this.A0y.A09();
        this.A0x.A02();
        this.A0w.A02();
        this.A0d = new ArrayList();
        this.A0b = new ArrayList();
        this.A0c = new ArrayList();
        this.A0K = C35531iB.A00();
        Runnable runnable = this.A0Z;
        if (runnable != null) {
            runnable.run();
        }
        this.A0M = new C49642Lp();
        A0I();
    }

    public void A0G() {
        this.A0w.A03();
        this.A0x.A03();
    }

    public final void A0H() {
        this.A0e.set(true);
        if (TextUtils.isEmpty(A0C()) && A05() == 0 && A08() == null && A0B() == null) {
            this.A16.set(true);
            this.A0f = true;
        } else if (this.A0f) {
            this.A00 = SystemClock.uptimeMillis();
            this.A0f = false;
        }
    }

    public final void A0I() {
        this.A0Y = new RunnableBRunnable0Shape11S0100000_I0_11(this, 29);
        ExecutorC27271Gr r1 = this.A14;
        r1.A00();
        r1.execute(this.A0Y);
    }

    public final void A0J() {
        AnonymousClass12H r1 = this.A0r;
        r1.A03(this.A0y.A00);
        r1.A03(this.A0I);
    }

    public final void A0K() {
        int intValue;
        int size = A0A().size();
        AnonymousClass016 r1 = this.A0G;
        if (r1.A01() == null) {
            intValue = 0;
        } else {
            intValue = ((Number) r1.A01()).intValue();
        }
        if (size - intValue < 300) {
            this.A0y.A08();
        }
    }

    public final void A0L() {
        synchronized (this) {
            if (this.A0t.A07(1608)) {
                this.A02.put(117, 7);
            }
            SparseIntArray sparseIntArray = this.A02;
            sparseIntArray.put(105, 6);
            sparseIntArray.put(118, 5);
            sparseIntArray.put(C43951xu.A03, 4);
            sparseIntArray.put(103, 3);
            sparseIntArray.put(97, 2);
            sparseIntArray.put(100, 1);
            sparseIntArray.put(0, 1);
        }
    }

    public final void A0M() {
        AnonymousClass12H r1 = this.A0r;
        r1.A04(this.A0y.A00);
        r1.A04(this.A0I);
    }

    public final void A0N() {
        int size;
        int intValue;
        AbstractCollection abstractCollection = (AbstractCollection) this.A0E.A01();
        boolean z = false;
        if (abstractCollection == null) {
            size = 0;
        } else {
            size = abstractCollection.size();
        }
        AnonymousClass016 r1 = this.A0G;
        if (r1.A01() == null) {
            intValue = 0;
        } else {
            intValue = ((Number) r1.A01()).intValue();
        }
        if (A0b() && intValue > size - 3) {
            z = true;
        }
        Boolean valueOf = Boolean.valueOf(z);
        AnonymousClass02P r12 = this.A0C;
        if (!C29941Vi.A00(valueOf, r12.A01())) {
            r12.A0B(valueOf);
        }
    }

    public void A0O(int i) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        AnonymousClass07E r2 = this.A0g;
        r2.A04("last_nav_time", Long.valueOf(elapsedRealtime));
        r2.A04("last_nav_type", Integer.valueOf(i));
    }

    public void A0P(int i) {
        Integer valueOf = Integer.valueOf(i);
        if (!C29941Vi.A00(valueOf, this.A03.A01())) {
            this.A0g.A04("current_screen", valueOf);
        }
    }

    public void A0Q(int i) {
        if (A0B() == null && i != A05()) {
            this.A0z.A00(2, i);
            A0E();
            C49642Lp r4 = this.A0M;
            Integer valueOf = Integer.valueOf(i);
            r4.A00(new C49652Lq(A08(), valueOf, A0C(), 2));
            this.A0g.A04("search_type", valueOf);
        }
    }

    public void A0R(UserJid userJid) {
        if (!C29941Vi.A00(userJid, A08())) {
            this.A0M.A00(new C49652Lq(userJid, Integer.valueOf(A05()), A0C(), 3));
            this.A0g.A04("search_jid", userJid);
        }
    }

    public void A0S(AbstractC15340mz r3) {
        if (r3 != null && 1 == r3.A06()) {
            this.A0s.A00(r3);
        }
        A0X(false);
        A0O(2);
        this.A0W.A0B(r3);
    }

    public final void A0T(C35531iB r3) {
        this.A0K = r3.A01();
        Runnable runnable = this.A0Z;
        if (runnable != null) {
            runnable.run();
        }
        this.A01.removeMessages(0);
        A0I();
    }

    public void A0U(C49662Lr r4) {
        if (A05() == 0 && !C29941Vi.A00(r4, A0B())) {
            this.A0M.A00(new C49652Lq(r4, A0C()));
            this.A0g.A04("smart_filter", r4);
        }
    }

    public void A0V(String str) {
        if (!str.equals(A0C())) {
            this.A0M.A00(new C49652Lq(A08(), Integer.valueOf(A05()), str, 1));
            this.A0g.A04("query_text", str);
        }
    }

    public void A0W(boolean z) {
        A0F();
        A0P(1);
        A0O(4);
        this.A0S.A0A(Boolean.valueOf(z));
    }

    public void A0X(boolean z) {
        Boolean valueOf = Boolean.valueOf(z);
        AnonymousClass016 r1 = this.A0H;
        if (!valueOf.equals(r1.A01())) {
            r1.A0B(valueOf);
        }
    }

    public boolean A0Y() {
        return A05() == 103 || A05() == 105 || A05() == 118;
    }

    public boolean A0Z() {
        C49662Lr A0B = A0B();
        if (A0B == null || 2 != A0B.A01) {
            return false;
        }
        return true;
    }

    public final boolean A0a() {
        if (this.A0K.A02.size() == 0) {
            return false;
        }
        Boolean bool = (Boolean) this.A0g.A02.get("user_grid_view_choice");
        if (bool != null) {
            return bool.booleanValue();
        }
        if (!A0Y() || !A0C().isEmpty()) {
            return false;
        }
        return true;
    }

    public final boolean A0b() {
        Boolean bool = Boolean.TRUE;
        if (!bool.equals(this.A0w.A01().A01()) && !bool.equals(this.A0x.A01().A01())) {
            C35231hU r1 = this.A0y;
            if (!bool.equals(r1.A00().A01()) && !bool.equals(r1.A04().A01()) && !bool.equals(r1.A03().A01())) {
                return false;
            }
        }
        return true;
    }

    public final boolean A0c() {
        return (!Boolean.TRUE.equals(this.A0y.A04().A01()) || this.A0K.A02.size() > 0) && this.A02.size() > 0;
    }

    public final boolean A0d() {
        return this.A0t.A07(1287);
    }

    public final boolean A0e(long j) {
        long longValue;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        Number number = (Number) this.A0g.A02.get("last_nav_time");
        if (number == null) {
            longValue = 0;
        } else {
            longValue = number.longValue();
        }
        return elapsedRealtime - longValue > j;
    }

    @Override // X.AbstractC35161hL
    public AbstractC35521iA A8R(MediaViewFragment mediaViewFragment, AbstractC16130oV r3) {
        return new AnonymousClass2Vv(this);
    }

    @OnLifecycleEvent(AnonymousClass074.ON_PAUSE)
    public void onPause() {
        A04();
        if (A04() != 2 && A04() != 1 && A04() != 4) {
            if (A04() != 0 || A0e(500)) {
                A0O(3);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        if (A0e(300000) != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        if (r1 != 3) goto L_0x0016;
     */
    @androidx.lifecycle.OnLifecycleEvent(X.AnonymousClass074.ON_RESUME)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r5 = this;
            r5.A04()
            int r1 = r5.A04()
            r0 = 1
            r4 = 0
            if (r1 == 0) goto L_0x0016
            if (r1 == r0) goto L_0x0023
            r0 = 2
            r2 = 300000(0x493e0, double:1.482197E-318)
            if (r1 == r0) goto L_0x001a
            r0 = 3
            if (r1 == r0) goto L_0x0037
        L_0x0016:
            r5.A0O(r4)
            return
        L_0x001a:
            r0 = 500(0x1f4, double:2.47E-321)
            boolean r0 = r5.A0e(r0)
            if (r0 != 0) goto L_0x0037
            return
        L_0x0023:
            X.0m9 r1 = r5.A0t
            r0 = 1608(0x648, float:2.253E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x003d
            boolean r0 = r5.A0Z()
            if (r0 == 0) goto L_0x003d
            r5.A0G()
            goto L_0x0016
        L_0x0037:
            boolean r0 = r5.A0e(r2)
            if (r0 == 0) goto L_0x0016
        L_0x003d:
            r5.A04()
            r5.A0W(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.SearchViewModel.onResume():void");
    }
}
