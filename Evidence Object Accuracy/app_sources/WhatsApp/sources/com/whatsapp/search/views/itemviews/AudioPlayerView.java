package com.whatsapp.search.views.itemviews;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass2GF;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass3OU;
import X.C14850m9;
import X.C38131nZ;
import X.C42941w9;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import com.whatsapp.voicerecorder.VoiceNoteSeekBar;
import java.util.List;

/* loaded from: classes2.dex */
public class AudioPlayerView extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public ImageButton A02;
    public CircularProgressBar A03;
    public VoiceVisualizer A04;
    public AnonymousClass018 A05;
    public C14850m9 A06;
    public VoiceNoteSeekBar A07;
    public AnonymousClass2P7 A08;
    public boolean A09;

    public AudioPlayerView(Context context) {
        super(context);
        A01();
        A02(context, null);
    }

    public AudioPlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        A02(context, attributeSet);
    }

    public AudioPlayerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A01();
    }

    public void A00() {
        this.A04.setVisibility(8);
        this.A04.setEnabled(false);
        this.A07.setVisibility(0);
        this.A07.setProgress(this.A01);
        VoiceNoteSeekBar voiceNoteSeekBar = this.A07;
        voiceNoteSeekBar.A09 = false;
        voiceNoteSeekBar.invalidate();
    }

    public void A01() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A06 = (C14850m9) r1.A04.get();
            this.A05 = (AnonymousClass018) r1.ANb.get();
        }
    }

    public final void A02(Context context, AttributeSet attributeSet) {
        LinearLayout.inflate(context, R.layout.message_audio_player, this);
        setOrientation(0);
        setGravity(17);
        setClipChildren(false);
        setClipToPadding(false);
        this.A02 = (ImageButton) AnonymousClass028.A0D(this, R.id.control_btn);
        this.A07 = (VoiceNoteSeekBar) AnonymousClass028.A0D(this, R.id.audio_seekbar);
        this.A04 = (VoiceVisualizer) AnonymousClass028.A0D(this, R.id.audio_visualizer);
        CircularProgressBar circularProgressBar = (CircularProgressBar) AnonymousClass028.A0D(this, R.id.progress_bar_1);
        this.A03 = circularProgressBar;
        circularProgressBar.setMax(100);
        this.A03.A0C = AnonymousClass00T.A00(context, R.color.media_message_progress_determinate);
        this.A03.A0B = AnonymousClass00T.A00(context, R.color.circular_progress_bar_background);
        this.A03.setVisibility(8);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A02);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(5, 0);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(6, 0);
            int dimensionPixelSize3 = obtainStyledAttributes.getDimensionPixelSize(3, 0);
            int dimensionPixelSize4 = obtainStyledAttributes.getDimensionPixelSize(4, 0);
            int dimensionPixelSize5 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            int dimensionPixelSize6 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
            int dimensionPixelSize7 = obtainStyledAttributes.getDimensionPixelSize(2, 0);
            obtainStyledAttributes.recycle();
            View A0D = AnonymousClass028.A0D(this, R.id.controls);
            C42941w9.A0A(A0D, this.A05, A0D.getPaddingLeft(), A0D.getPaddingTop(), dimensionPixelSize3, A0D.getPaddingBottom());
            View A0D2 = AnonymousClass028.A0D(this, R.id.audio_seekbar);
            A0D2.setPadding(A0D2.getPaddingLeft(), dimensionPixelSize2, A0D2.getPaddingRight(), dimensionPixelSize);
            if (dimensionPixelSize4 > 0) {
                C42941w9.A07(A0D2, this.A05, dimensionPixelSize4, ((ViewGroup.MarginLayoutParams) A0D2.getLayoutParams()).rightMargin);
            }
            View A0D3 = AnonymousClass028.A0D(this, R.id.control_button_container);
            if (dimensionPixelSize5 > 0) {
                ViewGroup.LayoutParams layoutParams = A0D3.getLayoutParams();
                layoutParams.height = dimensionPixelSize5;
                layoutParams.width = dimensionPixelSize5;
                A0D3.setLayoutParams(layoutParams);
            }
            if (dimensionPixelSize6 > 0) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) A0D3.getLayoutParams();
                C42941w9.A07(A0D3, this.A05, dimensionPixelSize6, marginLayoutParams.rightMargin);
                A0D3.setLayoutParams(marginLayoutParams);
            }
            if (dimensionPixelSize7 > 0) {
                ViewGroup.LayoutParams layoutParams2 = this.A03.getLayoutParams();
                layoutParams2.height = dimensionPixelSize7;
                layoutParams2.width = dimensionPixelSize7;
                this.A03.setLayoutParams(layoutParams2);
            }
        }
    }

    public void A03(List list) {
        this.A04.setEnabled(true);
        this.A04.setVisibility(0);
        this.A04.A02(list, (((float) this.A01) * 1.0f) / ((float) this.A00));
        VoiceNoteSeekBar voiceNoteSeekBar = this.A07;
        voiceNoteSeekBar.A09 = true;
        voiceNoteSeekBar.invalidate();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public ProgressBar getProgressBar() {
        return this.A03;
    }

    public int getSeekbarProgress() {
        return this.A07.getProgress();
    }

    public void setOnControlButtonClickListener(View.OnClickListener onClickListener) {
        this.A02.setOnClickListener(onClickListener);
    }

    public void setOnControlButtonLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.A02.setOnLongClickListener(onLongClickListener);
    }

    public void setPlayButtonState(int i) {
        ImageButton imageButton;
        Context context;
        int i2;
        if (i == 0) {
            Drawable A04 = AnonymousClass00T.A04(getContext(), R.drawable.inline_audio_play);
            ImageButton imageButton2 = this.A02;
            if (!this.A06.A07(1117)) {
                A04 = new AnonymousClass2GF(A04, this.A05);
            }
            imageButton2.setImageDrawable(A04);
            imageButton = this.A02;
            context = getContext();
            i2 = R.string.play;
        } else if (i == 1) {
            this.A02.setImageResource(R.drawable.inline_audio_pause);
            imageButton = this.A02;
            context = getContext();
            i2 = R.string.pause;
        } else if (i == 2) {
            this.A02.setImageResource(R.drawable.inline_audio_upload);
            imageButton = this.A02;
            context = getContext();
            i2 = R.string.button_upload;
        } else if (i == 3) {
            this.A02.setImageResource(R.drawable.inline_audio_download);
            imageButton = this.A02;
            context = getContext();
            i2 = R.string.button_download;
        } else if (i == 4) {
            this.A02.setImageResource(R.drawable.inline_audio_cancel);
            imageButton = this.A02;
            context = getContext();
            i2 = R.string.cancel;
        } else {
            StringBuilder sb = new StringBuilder("setPlayButtonState: Did not handle playstate: ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        imageButton.setContentDescription(context.getString(i2));
    }

    public void setPlaybackListener(AnonymousClass3OU r2) {
        this.A07.setOnSeekBarChangeListener(r2);
    }

    public void setSeekbarColor(int i) {
        this.A07.setProgressColor(i);
    }

    public void setSeekbarContentDescription(long j) {
        this.A07.setContentDescription(getContext().getString(R.string.voice_message_time_elapsed, C38131nZ.A06(this.A05, j)));
    }

    public void setSeekbarLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.A07.setOnLongClickListener(onLongClickListener);
    }

    public void setSeekbarMax(int i) {
        this.A07.setMax(i);
        this.A00 = i;
    }

    public void setSeekbarProgress(int i) {
        this.A01 = i;
        this.A07.setProgress(i);
    }
}
