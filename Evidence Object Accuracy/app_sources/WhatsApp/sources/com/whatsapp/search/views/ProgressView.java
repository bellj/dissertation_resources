package com.whatsapp.search.views;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.AnonymousClass3G9;
import android.animation.AnimatorSet;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ProgressView extends FrameLayout implements AnonymousClass004 {
    public AnimatorSet A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final int A03;
    public final CircularProgressBar A04;

    public ProgressView(Context context) {
        this(context, null);
    }

    public ProgressView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, -1);
    }

    public ProgressView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        FrameLayout.inflate(context, R.layout.search_progress, this);
        this.A04 = (CircularProgressBar) AnonymousClass028.A0D(this, R.id.progress_bar);
        this.A03 = AnonymousClass3G9.A01(getContext(), 40.0f);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }
}
