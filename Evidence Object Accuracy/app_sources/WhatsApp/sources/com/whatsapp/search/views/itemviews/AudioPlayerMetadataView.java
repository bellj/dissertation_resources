package com.whatsapp.search.views.itemviews;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C42941w9;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class AudioPlayerMetadataView extends LinearLayout implements AnonymousClass004 {
    public TextView A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public AudioPlayerMetadataView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
        A00(context, null);
    }

    public AudioPlayerMetadataView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        A00(context, attributeSet);
    }

    public AudioPlayerMetadataView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A00(Context context, AttributeSet attributeSet) {
        ViewGroup viewGroup;
        LinearLayout.inflate(context, R.layout.message_audio_metadata, this);
        setOrientation(0);
        setGravity(17);
        View A0D = AnonymousClass028.A0D(this, R.id.date_wrapper);
        View A0D2 = AnonymousClass028.A0D(this, R.id.status);
        this.A00 = C12960it.A0I(this, R.id.description);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A01);
            boolean z = obtainStyledAttributes.getBoolean(2, true);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, 0);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            obtainStyledAttributes.recycle();
            C42941w9.A08(A0D, this.A01, A0D.getPaddingLeft(), dimensionPixelSize2);
            C42941w9.A07(A0D, this.A01, dimensionPixelSize, ((LinearLayout.LayoutParams) A0D.getLayoutParams()).rightMargin);
            if (!z && (viewGroup = (ViewGroup) A0D2.getParent()) != null) {
                viewGroup.removeView(A0D2);
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public void setDescription(String str) {
        this.A00.setText(str);
    }
}
