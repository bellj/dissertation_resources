package com.whatsapp.search.views;

import X.AbstractC001200n;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass04v;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3G9;
import X.AnonymousClass3GL;
import X.AnonymousClass4L6;
import X.AnonymousClass4TG;
import X.C016907y;
import X.C102654pU;
import X.C15550nR;
import X.C15610nY;
import X.C29941Vi;
import X.C49662Lr;
import X.C53602ei;
import X.C620734i;
import X.View$OnFocusChangeListenerC100984mn;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaImageView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.search.views.TokenizedSearchInput;
import com.whatsapp.text.FinalBackspaceAwareEntry;

/* loaded from: classes2.dex */
public class TokenizedSearchInput extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public View.OnClickListener A02;
    public View.OnKeyListener A03;
    public TextView.OnEditorActionListener A04;
    public AnonymousClass04v A05;
    public AbstractC001200n A06;
    public C15550nR A07;
    public C15610nY A08;
    public AnonymousClass01d A09;
    public AnonymousClass018 A0A;
    public UserJid A0B;
    public SearchViewModel A0C;
    public C49662Lr A0D;
    public AnonymousClass4L6 A0E;
    public AnonymousClass2P7 A0F;
    public Integer A0G;
    public Runnable A0H;
    public Runnable A0I;
    public String A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public final View.OnFocusChangeListener A0N;
    public final View A0O;
    public final Chip A0P;
    public final Chip A0Q;
    public final Chip A0R;
    public final WaImageButton A0S;
    public final WaImageView A0T;
    public final FinalBackspaceAwareEntry A0U;

    public TokenizedSearchInput(Context context) {
        this(context, null);
    }

    public TokenizedSearchInput(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TokenizedSearchInput(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A07 = (C15550nR) r1.A45.get();
            this.A08 = (C15610nY) r1.AMe.get();
            this.A0A = (AnonymousClass018) r1.ANb.get();
            this.A09 = (AnonymousClass01d) r1.ALI.get();
        }
        this.A0M = false;
        this.A0J = "";
        this.A0G = 0;
        this.A00 = 0;
        this.A01 = 0;
        this.A0E = new AnonymousClass4L6(this);
        this.A04 = new C102654pU(this);
        this.A03 = new View.OnKeyListener() { // from class: X.4mt
            @Override // android.view.View.OnKeyListener
            public final boolean onKey(View view, int i2, KeyEvent keyEvent) {
                TokenizedSearchInput tokenizedSearchInput = TokenizedSearchInput.this;
                if (tokenizedSearchInput.A0C == null || keyEvent == null) {
                    return false;
                }
                if ((keyEvent.getKeyCode() != 66 && keyEvent.getKeyCode() != 160) || keyEvent.getAction() != 1) {
                    return false;
                }
                tokenizedSearchInput.A0C.A0X(false);
                return true;
            }
        };
        this.A02 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 48);
        this.A05 = new C53602ei(this);
        this.A0N = new View$OnFocusChangeListenerC100984mn(this);
        LayoutInflater from = LayoutInflater.from(context);
        setOrientation(0);
        setGravity(16);
        from.inflate(R.layout.search_input, (ViewGroup) this, true);
        this.A0R = (Chip) AnonymousClass028.A0D(this, R.id.type_token);
        this.A0P = (Chip) AnonymousClass028.A0D(this, R.id.chat_token);
        this.A0Q = (Chip) AnonymousClass028.A0D(this, R.id.smart_filter_token);
        this.A0U = (FinalBackspaceAwareEntry) AnonymousClass028.A0D(this, R.id.search_input);
        this.A0T = (WaImageView) AnonymousClass028.A0D(this, R.id.search_clear_btn);
        this.A0O = AnonymousClass028.A0D(this, R.id.focus_dummy);
        this.A0S = (WaImageButton) AnonymousClass028.A0D(this, R.id.grid_list_toggle);
    }

    public static /* synthetic */ void A00(View view, TokenizedSearchInput tokenizedSearchInput) {
        SearchViewModel searchViewModel = tokenizedSearchInput.A0C;
        if (searchViewModel != null) {
            if (view == tokenizedSearchInput.A0R) {
                searchViewModel.A0Q(0);
            } else if (view == tokenizedSearchInput.A0P) {
                searchViewModel.A0D();
            } else if (view == tokenizedSearchInput.A0Q) {
                searchViewModel.A0E();
            }
        }
        tokenizedSearchInput.postDelayed(new RunnableBRunnable0Shape11S0100000_I0_11(tokenizedSearchInput, 32), 100);
    }

    public static /* synthetic */ void A05(TokenizedSearchInput tokenizedSearchInput) {
        SearchViewModel searchViewModel = tokenizedSearchInput.A0C;
        if (searchViewModel != null) {
            FinalBackspaceAwareEntry finalBackspaceAwareEntry = tokenizedSearchInput.A0U;
            finalBackspaceAwareEntry.setSelection(searchViewModel.A0C().length());
            finalBackspaceAwareEntry.sendAccessibilityEvent(8);
            tokenizedSearchInput.setFocus(0);
        }
    }

    public static /* synthetic */ void A07(TokenizedSearchInput tokenizedSearchInput, Boolean bool) {
        if (bool == null) {
            return;
        }
        if (bool.booleanValue()) {
            tokenizedSearchInput.setFocus(0);
            FinalBackspaceAwareEntry finalBackspaceAwareEntry = tokenizedSearchInput.A0U;
            finalBackspaceAwareEntry.requestFocus();
            finalBackspaceAwareEntry.A04(false);
            return;
        }
        tokenizedSearchInput.setFocus(4);
        tokenizedSearchInput.A0U.clearFocus();
        tokenizedSearchInput.A0O.requestFocus();
        InputMethodManager A0Q = tokenizedSearchInput.A09.A0Q();
        if (A0Q != null) {
            A0Q.hideSoftInputFromWindow(tokenizedSearchInput.getWindowToken(), 2);
        }
    }

    public ColorStateList A08(boolean z) {
        int A00 = AnonymousClass00T.A00(getContext(), R.color.search_input_token_selected);
        A00 = AnonymousClass00T.A00(getContext(), R.color.search_input_token);
        if (!z) {
        }
        return ColorStateList.valueOf(A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r3.A0D != null) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09() {
        /*
            r3 = this;
            com.whatsapp.WaImageView r2 = r3.A0T
            com.whatsapp.jid.UserJid r0 = r3.A0B
            if (r0 != 0) goto L_0x001b
            java.lang.String r0 = r3.A0J
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x001b
            java.lang.Integer r0 = r3.A0G
            int r0 = r0.intValue()
            if (r0 != 0) goto L_0x001b
            X.2Lr r1 = r3.A0D
            r0 = 1
            if (r1 == 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r0 = r0 ^ 1
            r2.setEnabled(r0)
            com.whatsapp.WaImageButton r1 = r3.A0S
            boolean r0 = r3.A0K
            r1.setEnabled(r0)
            boolean r0 = r3.A0M
            if (r0 == 0) goto L_0x0030
            r3.A0A()
            return
        L_0x0030:
            java.lang.Runnable r0 = r3.A0I
            if (r0 == 0) goto L_0x0037
            r3.removeCallbacks(r0)
        L_0x0037:
            java.lang.Runnable r0 = r3.A0H
            if (r0 == 0) goto L_0x003e
            r3.removeCallbacks(r0)
        L_0x003e:
            r0 = 30
            com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11 r2 = new com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11
            r2.<init>(r3, r0)
            r3.A0I = r2
            r0 = 50
            r3.postDelayed(r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.TokenizedSearchInput.A09():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
        if (r1 == 8) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        r5.setVisibility(4);
        A0B();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
        if (r1 == 8) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0A() {
        /*
            r6 = this;
            com.whatsapp.search.SearchViewModel r0 = r6.A0C
            if (r0 == 0) goto L_0x0051
            int r1 = r6.A01
            if (r1 == 0) goto L_0x0063
            r0 = 1
            if (r1 != r0) goto L_0x007a
            com.whatsapp.WaImageButton r5 = r6.A0S
            r0 = 2131232172(0x7f0805ac, float:1.8080446E38)
            r5.setImageResource(r0)
            android.content.Context r1 = r6.getContext()
            r0 = 2131892128(0x7f1217a0, float:1.9418996E38)
        L_0x001a:
            java.lang.String r0 = r1.getString(r0)
            r5.setContentDescription(r0)
            int r4 = r5.getVisibility()
            boolean r0 = r6.A0K
            r3 = 4
            r2 = 8
            if (r0 == 0) goto L_0x0057
            r1 = 0
        L_0x002d:
            boolean r0 = r6.A0M
            if (r0 == 0) goto L_0x0037
            r5.setVisibility(r1)
            r6.A0B()
        L_0x0037:
            if (r4 != r2) goto L_0x0052
            if (r1 == r2) goto L_0x0073
            if (r1 != r2) goto L_0x0043
        L_0x003d:
            r5.setVisibility(r3)
            r6.A0B()
        L_0x0043:
            r0 = 24
            com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0 r2 = new com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0
            r2.<init>(r6, r1, r0)
            r6.A0H = r2
            r0 = 50
            r6.postDelayed(r2, r0)
        L_0x0051:
            return
        L_0x0052:
            if (r4 == r2) goto L_0x0073
            if (r1 != r2) goto L_0x0073
            goto L_0x003d
        L_0x0057:
            com.whatsapp.search.SearchViewModel r0 = r6.A0C
            boolean r0 = r0.A0Y()
            r1 = 8
            if (r0 == 0) goto L_0x002d
            r1 = 4
            goto L_0x002d
        L_0x0063:
            com.whatsapp.WaImageButton r5 = r6.A0S
            r0 = 2131232171(0x7f0805ab, float:1.8080444E38)
            r5.setImageResource(r0)
            android.content.Context r1 = r6.getContext()
            r0 = 2131892127(0x7f12179f, float:1.9418994E38)
            goto L_0x001a
        L_0x0073:
            r5.setVisibility(r1)
            r6.A0B()
            return
        L_0x007a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.TokenizedSearchInput.A0A():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        if (r3.A0D != null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B() {
        /*
            r3 = this;
            com.whatsapp.jid.UserJid r0 = r3.A0B
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = r3.A0J
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0019
            java.lang.Integer r0 = r3.A0G
            int r0 = r0.intValue()
            if (r0 != 0) goto L_0x0019
            X.2Lr r0 = r3.A0D
            r2 = 1
            if (r0 == 0) goto L_0x001a
        L_0x0019:
            r2 = 0
        L_0x001a:
            com.whatsapp.WaImageView r1 = r3.A0T
            r0 = 0
            if (r2 == 0) goto L_0x0020
            r0 = 4
        L_0x0020:
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.TokenizedSearchInput.A0B():void");
    }

    public final void A0C() {
        FinalBackspaceAwareEntry finalBackspaceAwareEntry;
        String string;
        if (this.A0B == null || this.A0G.intValue() == 0) {
            finalBackspaceAwareEntry = this.A0U;
            string = getContext().getString(R.string.search_hint);
        } else {
            finalBackspaceAwareEntry = this.A0U;
            string = "";
        }
        finalBackspaceAwareEntry.setHint(string);
    }

    public final void A0D() {
        UserJid userJid = this.A0B;
        Chip chip = this.A0P;
        if (userJid == null) {
            chip.setVisibility(8);
        } else if (chip.getVisibility() == 8) {
            A0G(chip);
        } else {
            boolean z = true;
            boolean z2 = false;
            if (this.A00 == 2) {
                z2 = true;
            }
            chip.setChipBackgroundColor(A08(z2));
            if (this.A00 != 2) {
                z = false;
            }
            A0H(chip, z);
        }
    }

    public final void A0E() {
        C49662Lr r0 = this.A0D;
        if (r0 == null) {
            this.A0Q.setVisibility(8);
            return;
        }
        Chip chip = this.A0Q;
        chip.setText(r0.A02);
        C620734i.A00(getContext(), chip, this.A0D.A00, R.color.white);
        boolean z = true;
        boolean z2 = false;
        if (this.A00 == 3) {
            z2 = true;
        }
        chip.setChipBackgroundColor(A08(z2));
        if (this.A00 != 3) {
            z = false;
        }
        A0H(chip, z);
        if (chip.getVisibility() == 8) {
            A0G(chip);
        }
    }

    public final void A0F() {
        AnonymousClass4TG r0 = (AnonymousClass4TG) AnonymousClass3GL.A00().get(this.A0G.intValue());
        if (r0 == null) {
            this.A0R.setVisibility(8);
            return;
        }
        Chip chip = this.A0R;
        chip.setText(r0.A05);
        AnonymousClass3GL.A01(getContext(), chip, this.A0G.intValue(), R.color.white);
        boolean z = false;
        boolean z2 = false;
        if (this.A00 == 1) {
            z2 = true;
        }
        chip.setChipBackgroundColor(A08(z2));
        if (this.A00 == 1) {
            z = true;
        }
        A0H(chip, z);
        if (chip.getVisibility() == 8) {
            A0G(chip);
        }
    }

    public final void A0G(Chip chip) {
        int color = getResources().getColor(R.color.black_alpha_20);
        int color2 = getResources().getColor(R.color.search_input_token);
        int A05 = C016907y.A05(color, color2);
        if (this.A0M) {
            chip.setScaleX(1.0f);
            chip.setScaleY(1.0f);
            chip.setAlpha(1.0f);
            chip.setVisibility(0);
            chip.setChipBackgroundColor(ColorStateList.valueOf(color2));
            return;
        }
        ColorStateList valueOf = ColorStateList.valueOf(A05);
        chip.setScaleX(0.92f);
        chip.setScaleY(0.92f);
        chip.setAlpha(0.0f);
        chip.setChipBackgroundColor(valueOf);
        chip.setVisibility(0);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(chip, "scaleX", 0.92f, 1.0f);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(chip, "scaleY", 0.92f, 1.0f);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(chip, "alpha", 0.0f, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ofFloat).with(ofFloat2).with(ofFloat3);
        animatorSet.setDuration(100L);
        ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(A05), Integer.valueOf(color2));
        ofObject.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eT
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                Chip.this.setChipBackgroundColor(ColorStateList.valueOf(C12960it.A05(valueAnimator.getAnimatedValue())));
            }
        });
        ofObject.setDuration(100L);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.play(ofObject).after(animatorSet);
        animatorSet2.start();
    }

    public void A0H(Chip chip, boolean z) {
        Context context;
        float f;
        if (z) {
            chip.setChipStrokeColor(ColorStateList.valueOf(AnonymousClass00T.A00(getContext(), R.color.search_input_token_selected_stroke)));
            context = getContext();
            f = 1.0f;
        } else {
            chip.setChipStrokeColor(null);
            context = getContext();
            f = 0.0f;
        }
        chip.setChipStrokeWidth((float) AnonymousClass3G9.A01(context, f));
    }

    public void A0I(String str) {
        if (!C29941Vi.A00(this.A0J, str)) {
            if (this.A00 != 0 && !TextUtils.isEmpty(str)) {
                setFocus(0);
            }
            this.A0J = str;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0F;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0F = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        int max = Math.max(getResources().getDimensionPixelSize(R.dimen.search_token_min_max), (int) Math.floor(((double) getWidth()) * 0.3d));
        this.A0R.setMaxWidth(max);
        this.A0P.setMaxWidth(max);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0020, code lost:
        if (r4 != 4) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFocus(int r4) {
        /*
            r3 = this;
            com.whatsapp.search.SearchViewModel r0 = r3.A0C
            if (r0 == 0) goto L_0x0032
            int r0 = r3.A00
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            boolean r0 = X.C29941Vi.A00(r1, r0)
            if (r0 != 0) goto L_0x0032
            r2 = 1
            if (r4 == 0) goto L_0x003d
            if (r4 == r2) goto L_0x0033
            r0 = 2
            if (r4 == r0) goto L_0x0033
            r0 = 3
            if (r4 == r0) goto L_0x0033
        L_0x001f:
            r0 = 4
            if (r4 == r0) goto L_0x0027
        L_0x0022:
            com.whatsapp.search.SearchViewModel r0 = r3.A0C
            r0.A0X(r2)
        L_0x0027:
            r3.A00 = r4
            r3.A0F()
            r3.A0D()
            r3.A0E()
        L_0x0032:
            return
        L_0x0033:
            com.whatsapp.text.FinalBackspaceAwareEntry r1 = r3.A0U
            r0 = 0
            r1.setSelection(r0)
            r1.setCursorVisible(r0)
            goto L_0x001f
        L_0x003d:
            com.whatsapp.text.FinalBackspaceAwareEntry r0 = r3.A0U
            r0.setCursorVisible(r2)
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.TokenizedSearchInput.setFocus(int):void");
    }

    public void setNoAnimateForTestsOnly(boolean z) {
        this.A0M = z;
    }
}
