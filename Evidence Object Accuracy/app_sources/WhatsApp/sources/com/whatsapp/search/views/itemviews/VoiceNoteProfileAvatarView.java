package com.whatsapp.search.views.itemviews;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2GE;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C42941w9;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* loaded from: classes2.dex */
public class VoiceNoteProfileAvatarView extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public ColorStateList A01;
    public ColorStateList A02;
    public ImageView A03;
    public ImageView A04;
    public ImageView A05;
    public WaButton A06;
    public AnonymousClass018 A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public boolean A0A;

    public VoiceNoteProfileAvatarView(Context context) {
        super(context);
        if (!this.A09) {
            this.A09 = true;
            this.A07 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A00 = 0;
        A03(context, null);
    }

    public VoiceNoteProfileAvatarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A00 = 0;
        A03(context, attributeSet);
    }

    public VoiceNoteProfileAvatarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A09) {
            this.A09 = true;
            this.A07 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public static final void A00(View view) {
        AlphaAnimation A0J = C12970iu.A0J();
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(A0J);
        animationSet.setDuration(250);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        view.startAnimation(animationSet);
    }

    public static final void A01(View view) {
        AlphaAnimation A0R = C12980iv.A0R();
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(A0R);
        animationSet.setDuration(250);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        view.startAnimation(animationSet);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(int r15, boolean r16, boolean r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView.A02(int, boolean, boolean, boolean):void");
    }

    public final void A03(Context context, AttributeSet attributeSet) {
        FrameLayout.inflate(context, R.layout.voice_note_profile_avatar, this);
        this.A05 = C12970iu.A0K(this, R.id.picture);
        this.A03 = C12970iu.A0K(this, R.id.picture_in_group);
        this.A04 = C12970iu.A0K(this, R.id.mic_overlay);
        this.A06 = (WaButton) AnonymousClass028.A0D(this, R.id.fast_playback_overlay);
        this.A02 = AnonymousClass00T.A03(context, R.color.voiceNoteOutgoingPlaybackTint);
        this.A01 = AnonymousClass00T.A03(context, R.color.voiceNoteIncomingPlaybackTint);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0P);
            View A0D = AnonymousClass028.A0D(this, R.id.picture_frame);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(5, 0);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(6, 0);
            int dimensionPixelSize3 = obtainStyledAttributes.getDimensionPixelSize(4, 0);
            int dimensionPixelSize4 = obtainStyledAttributes.getDimensionPixelSize(8, 0);
            int dimensionPixelSize5 = obtainStyledAttributes.getDimensionPixelSize(7, Integer.MIN_VALUE);
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(1);
            int dimensionPixelSize6 = obtainStyledAttributes.getDimensionPixelSize(3, 0);
            int dimensionPixelSize7 = obtainStyledAttributes.getDimensionPixelSize(2, 0);
            obtainStyledAttributes.recycle();
            C42941w9.A0A(A0D, this.A07, dimensionPixelSize, dimensionPixelSize2, A0D.getPaddingRight(), dimensionPixelSize3);
            C42941w9.A0A(this.A05, this.A07, dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4);
            C42941w9.A0A(this.A03, this.A07, dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4, dimensionPixelSize4);
            if (dimensionPixelSize5 != Integer.MIN_VALUE) {
                ViewGroup.LayoutParams layoutParams = this.A05.getLayoutParams();
                layoutParams.height = dimensionPixelSize5;
                layoutParams.width = dimensionPixelSize5;
                this.A05.setLayoutParams(layoutParams);
                this.A03.setLayoutParams(layoutParams);
            }
            this.A04.setBackground(drawable);
            AnonymousClass028.A0M(colorStateList, this.A04);
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this.A04);
            C42941w9.A09(this.A04, this.A07, dimensionPixelSize6, A0H.topMargin, A0H.rightMargin, dimensionPixelSize7);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public int getFastPlaybackViewState() {
        return this.A00;
    }

    public ImageView getGroupProfileImageView() {
        return this.A03;
    }

    public ImageView getProfileImageView() {
        return this.A05;
    }

    public void setIsForwardedByNonAuthorPttUi(boolean z) {
        int i = 8;
        this.A0A = z;
        ImageView imageView = this.A05;
        int i2 = R.drawable.avatar_contact;
        if (z) {
            i2 = R.drawable.audio_ptt_forwarded_icon;
        }
        imageView.setImageResource(i2);
        ImageView imageView2 = this.A04;
        if (!z) {
            i = 0;
        }
        imageView2.setVisibility(i);
    }

    public void setMicColorTint(int i) {
        this.A04.setImageDrawable(AnonymousClass2GE.A01(getContext(), R.drawable.mic_played, i));
    }

    public void setOnFastPlaybackButtonClickListener(View.OnClickListener onClickListener) {
        setOnClickListener(onClickListener);
        this.A06.setOnClickListener(onClickListener);
        if (this.A06.getVisibility() == 8 || this.A06.getVisibility() == 4) {
            setClickable(false);
        }
    }

    public void setupMicBackgroundColor(int i) {
        Context context = getContext();
        Drawable mutate = C12970iu.A0C(context, R.drawable.mic_background_incoming).mutate();
        ColorStateList A03 = AnonymousClass00T.A03(context, i);
        this.A04.setBackground(mutate);
        AnonymousClass028.A0M(A03, this.A04);
    }
}
