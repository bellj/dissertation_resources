package com.whatsapp.search.views;

import X.AbstractC16130oV;
import X.AbstractC41521tf;
import X.AnonymousClass028;
import X.AnonymousClass04v;
import X.AnonymousClass19O;
import X.AnonymousClass1X2;
import X.AnonymousClass1X7;
import X.AnonymousClass1XG;
import X.AnonymousClass1XI;
import X.AnonymousClass1XR;
import X.AnonymousClass23N;
import X.C30041Vv;
import X.C70423bG;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes2.dex */
public class MessageThumbView extends WaImageView {
    public int A00;
    public AbstractC16130oV A01;
    public AnonymousClass19O A02;
    public boolean A03;
    public final AbstractC41521tf A04;

    public MessageThumbView(Context context) {
        this(context, null);
    }

    public MessageThumbView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A04 = new C70423bG(this);
    }

    public MessageThumbView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A04 = new C70423bG(this);
    }

    public MessageThumbView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    private int getNotDownloadedContentDescription() {
        AbstractC16130oV r1 = this.A01;
        if ((r1 instanceof AnonymousClass1X7) || (r1 instanceof AnonymousClass1XI)) {
            return R.string.conversation_row_image_not_downloaded_content_description;
        }
        if (r1 instanceof AnonymousClass1XR) {
            return R.string.conversation_row_gif_not_downloaded_content_description;
        }
        if ((r1 instanceof AnonymousClass1X2) || (r1 instanceof AnonymousClass1XG)) {
            return R.string.conversation_row_video_not_downloaded_content_description;
        }
        return -1;
    }

    public void setMessage(AbstractC16130oV r3) {
        if (this.A02 != null) {
            this.A01 = r3;
            AbstractC41521tf r1 = this.A04;
            r1.Adu(this);
            this.A02.A07(this, r3, r1);
        }
    }

    public void setRadius(int i) {
        this.A00 = i;
    }

    public void setStatus(int i) {
        Resources resources;
        int i2;
        if (((WaImageView) this).A00 != null && this.A01 != null) {
            AnonymousClass028.A0g(this, new AnonymousClass04v());
            if (i == 0 || i == 1) {
                resources = getResources();
                i2 = R.string.image_transfer_in_progress;
            } else if (i == 2 || i == 3) {
                resources = getResources();
                i2 = R.string.action_open_image;
            } else {
                AnonymousClass23N.A02(this, R.string.button_download);
                setOnClickListener(null);
                int notDownloadedContentDescription = getNotDownloadedContentDescription();
                if (notDownloadedContentDescription != -1) {
                    setContentDescription(getResources().getString(notDownloadedContentDescription, C30041Vv.A0B(((WaImageView) this).A00, this.A01.A01)));
                    return;
                }
                return;
            }
            setContentDescription(resources.getString(i2));
            setOnClickListener(null);
        }
    }
}
