package com.whatsapp.search.views.itemviews;

import X.AbstractC115385Ri;
import X.AbstractC14440lR;
import X.AbstractC16130oV;
import X.AnonymousClass004;
import X.AnonymousClass1XR;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass59G;
import X.C44051y7;
import X.C621534v;
import X.C98114iA;
import X.C98134iC;
import X.TextureView$SurfaceTextureListenerC100924mh;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;

/* loaded from: classes2.dex */
public class MessageGifVideoPlayer extends TextureView implements AnonymousClass004 {
    public MediaPlayer.OnErrorListener A00;
    public MediaPlayer.OnPreparedListener A01;
    public MediaPlayer A02;
    public Surface A03;
    public C44051y7 A04;
    public AnonymousClass1XR A05;
    public AbstractC115385Ri A06;
    public AbstractC14440lR A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public final TextureView.SurfaceTextureListener A0G;

    public MessageGifVideoPlayer(Context context) {
        super(context);
        A02();
        this.A0F = false;
        this.A0E = false;
        this.A0G = new TextureView$SurfaceTextureListenerC100924mh(this);
        this.A01 = new C98134iC(this);
        this.A00 = C98114iA.A00;
    }

    public MessageGifVideoPlayer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A0F = false;
        this.A0E = false;
        this.A0G = new TextureView$SurfaceTextureListenerC100924mh(this);
        this.A01 = new C98134iC(this);
        this.A00 = C98114iA.A00;
    }

    public MessageGifVideoPlayer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        if (r3 < r2) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
        if (r4 > r6) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
        r1 = r3 / r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0053, code lost:
        r2 = 1.0f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(com.whatsapp.search.views.itemviews.MessageGifVideoPlayer r8) {
        /*
            X.1y7 r1 = r8.A04
            if (r1 == 0) goto L_0x003f
            int r0 = r1.A02
            float r7 = (float) r0
            int r0 = r1.A01
            float r6 = (float) r0
            r1 = 0
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x003f
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x003f
            int r0 = r8.getWidth()
            float r5 = (float) r0
            int r0 = r8.getHeight()
            float r4 = (float) r0
            float r3 = r7 / r6
            float r2 = r5 / r4
            r1 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0040
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0040
        L_0x002b:
            int r0 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0051
        L_0x002f:
            float r2 = r2 / r3
        L_0x0030:
            r0 = 1073741824(0x40000000, float:2.0)
            float r5 = r5 / r0
            float r4 = r4 / r0
            android.graphics.Matrix r0 = new android.graphics.Matrix
            r0.<init>()
            r0.setScale(r1, r2, r5, r4)
            r8.setTransform(r0)
        L_0x003f:
            return
        L_0x0040:
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0049
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0049
            goto L_0x002b
        L_0x0049:
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 > 0) goto L_0x002f
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0053
        L_0x0051:
            float r3 = r3 / r2
            r1 = r3
        L_0x0053:
            r2 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.search.views.itemviews.MessageGifVideoPlayer.A00(com.whatsapp.search.views.itemviews.MessageGifVideoPlayer):void");
    }

    public static /* synthetic */ boolean A01() {
        return false;
    }

    public void A02() {
        if (!this.A0B) {
            this.A0B = true;
            this.A07 = (AbstractC14440lR) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANe.get();
        }
    }

    public final void A03() {
        if (this.A0F && this.A02 != null && this.A0C) {
            AbstractC115385Ri r0 = this.A06;
            if (r0 != null) {
                C621534v.A00(((AnonymousClass59G) r0).A00, false);
            }
            setVisibility(0);
            this.A02.start();
            this.A0D = true;
        }
    }

    public final void A04() {
        this.A0C = false;
        if (this.A0E) {
            this.A0A = true;
        } else if (!this.A0F) {
            this.A09 = true;
        } else {
            setSurfaceTextureListener(this.A0G);
            this.A07.Ab2(new RunnableBRunnable0Shape11S0100000_I0_11(this, 34));
        }
    }

    public final void A05() {
        MediaPlayer mediaPlayer;
        setVisibility(8);
        AbstractC115385Ri r0 = this.A06;
        if (r0 != null) {
            C621534v.A00(((AnonymousClass59G) r0).A00, true);
        }
        if (this.A0D && (mediaPlayer = this.A02) != null) {
            mediaPlayer.pause();
            this.A02.seekTo(0);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.TextureView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        A03();
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A05();
    }

    public void setMessage(AnonymousClass1XR r2) {
        if ((this.A05 != r2 || this.A02 == null) && ((AbstractC16130oV) r2).A02 != null) {
            this.A05 = r2;
            A04();
        }
    }

    public void setPlayingListener(AbstractC115385Ri r1) {
        this.A06 = r1;
    }

    public void setScrolling(boolean z) {
        this.A0E = z;
        if (!z && this.A0A) {
            this.A0A = false;
            A04();
        }
    }

    public void setShouldPlay(boolean z) {
        if (this.A0F != z) {
            this.A0F = z;
            if (!z) {
                if (this.A0D && this.A02 != null) {
                    A05();
                }
            } else if (this.A09) {
                A04();
            } else if (this.A0C) {
                A03();
            }
        }
    }
}
