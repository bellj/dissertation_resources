package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C15570nT;
import X.C27631Ih;
import X.C32891cu;
import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Mac;

/* loaded from: classes2.dex */
public class DefaultCryptoCallback implements CryptoCallback {
    public static final int E2E_CALL_KEY_LENGTH = 32;
    public static final int E2E_EXTENDED_V2_KEY_LENGTH = 46;
    public static final int HMAC_SHA256_DIGEST_LENGTH = 32;
    public static final int SECURE_SSRC_LENGTH = 4;
    public final C15570nT meManager;
    public final SecureRandom secureRandom = new SecureRandom();

    public DefaultCryptoCallback(C15570nT r2) {
        this.meManager = r2;
    }

    private byte[] expandCallKey(byte[] bArr, String str) {
        if (bArr.length == 32) {
            if (str == null) {
                C15570nT r0 = this.meManager;
                r0.A08();
                C27631Ih r02 = r0.A05;
                AnonymousClass009.A05(r02);
                str = r02.getRawString();
            }
            byte[] A00 = C32891cu.A00(bArr, str.getBytes(), 46);
            if (A00.length == 46) {
                return A00;
            }
            throw C12960it.A0U("split byte counts do not match");
        }
        throw C12970iu.A0f("callKey should be 32 bytes");
    }

    @Override // com.whatsapp.voipcalling.CryptoCallback
    public boolean generateE2EKeysV2(byte[] bArr, byte[] bArr2, String str) {
        if (bArr == null || bArr2 == null || bArr.length != 32 || bArr2.length != 46) {
            return false;
        }
        System.arraycopy(expandCallKey(bArr, str), 0, bArr2, 0, 46);
        return true;
    }

    @Override // com.whatsapp.voipcalling.CryptoCallback
    public boolean generateRandomBytes(byte[] bArr) {
        this.secureRandom.nextBytes(bArr);
        return true;
    }

    public static byte[] generateSecureSsrc(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        byte[] A01 = C32891cu.A01(bArr, bArr3, bArr2, 4);
        if (A01.length == 4) {
            return A01;
        }
        throw C12960it.A0U("split byte counts do not match");
    }

    @Override // com.whatsapp.voipcalling.CryptoCallback
    public boolean getSecureSsrc(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        if (bArr == null || bArr2 == null || bArr3 == null || bArr4 == null || bArr.length == 0 || bArr2.length == 0 || bArr3.length != 4) {
            return false;
        }
        System.arraycopy(generateSecureSsrc(bArr, bArr2, bArr3), 0, bArr4, 0, 4);
        return true;
    }

    @Override // com.whatsapp.voipcalling.CryptoCallback
    public boolean hkdfSha256(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        int length = bArr4.length;
        System.arraycopy(C32891cu.A01(bArr2, bArr, bArr3, length), 0, bArr4, 0, length);
        return true;
    }

    @Override // com.whatsapp.voipcalling.CryptoCallback
    public boolean hmacSha256(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            C12990iw.A1S(DefaultCrypto.HMAC_SHA256, instance, bArr2);
            byte[] doFinal = instance.doFinal(bArr);
            if (doFinal.length != 32) {
                return false;
            }
            System.arraycopy(doFinal, 0, bArr3, 0, 32);
            return true;
        } catch (InvalidKeyException | NoSuchAlgorithmException unused) {
            return false;
        }
    }
}
