package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public final class CallParticipant {
    public final UserJid jid;
    public final String state;

    public CallParticipant(UserJid userJid, String str) {
        this.jid = userJid;
        this.state = str;
    }

    public static CallParticipant create(String str, String str2) {
        UserJid nullable = UserJid.getNullable(str);
        if (nullable != null) {
            return new CallParticipant(nullable, str2);
        }
        StringBuilder sb = new StringBuilder("UserJid was null. Parsed: ");
        sb.append(str);
        AnonymousClass009.A07(sb.toString());
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallParticipant{jid=");
        sb.append(this.jid);
        sb.append(", state=");
        sb.append(this.state);
        sb.append('}');
        return sb.toString();
    }
}
