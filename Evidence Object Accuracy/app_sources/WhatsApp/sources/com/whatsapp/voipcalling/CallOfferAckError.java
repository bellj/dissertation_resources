package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CallOfferAckError {
    public final int errorCode;
    public final UserJid errorJid;

    public CallOfferAckError(String str, int i) {
        UserJid nullable = UserJid.getNullable(str);
        AnonymousClass009.A05(nullable);
        this.errorJid = nullable;
        this.errorCode = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallOfferAckError {errorJid=");
        sb.append(this.errorJid);
        sb.append(", errorCode=");
        sb.append(this.errorCode);
        sb.append('}');
        return sb.toString();
    }
}
