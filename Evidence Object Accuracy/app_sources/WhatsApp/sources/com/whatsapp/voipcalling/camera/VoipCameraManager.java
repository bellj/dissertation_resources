package com.whatsapp.voipcalling.camera;

import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass2NN;
import X.AnonymousClass2NP;
import X.AnonymousClass2NQ;
import X.AnonymousClass2NR;
import X.AnonymousClass2NT;
import X.AnonymousClass2NW;
import X.AnonymousClass2NY;
import X.C14850m9;
import X.C17140qK;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.SparseArray;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pjsip.PjCamera;
import org.pjsip.PjCameraInfo;

/* loaded from: classes2.dex */
public final class VoipCameraManager {
    public final C14850m9 abProps;
    public final AnonymousClass2NP cameraEventsListener = new AnonymousClass2NN(this);
    public Integer currentApiVersion;
    public volatile VoipPhysicalCamera currentCamera;
    public volatile Point lastAdjustedCameraPreviewSize;
    public final SparseArray rawCameraInfoCache = new SparseArray();
    public final AnonymousClass01d systemServices;
    public final C17140qK voipSharedPreferences;
    public final AbstractC14440lR waWorkers;

    public VoipCameraManager(AnonymousClass01d r2, C14850m9 r3, C17140qK r4, AbstractC14440lR r5) {
        this.systemServices = r2;
        this.abProps = r3;
        this.voipSharedPreferences = r4;
        this.waWorkers = r5;
    }

    public void addCameraErrorListener(AnonymousClass2NP r2) {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            voipPhysicalCamera.addCameraEventsListener(r2);
        }
    }

    /* access modifiers changed from: private */
    public void clearStoredRawCameraInfo(int i, int i2) {
        this.voipSharedPreferences.A01().edit().remove(C17140qK.A00(i, i2)).apply();
    }

    /* access modifiers changed from: private */
    public synchronized void closeCurrentCamera(VoipPhysicalCamera voipPhysicalCamera) {
        VoipPhysicalCamera voipPhysicalCamera2 = this.currentCamera;
        boolean z = false;
        if (voipPhysicalCamera2 == voipPhysicalCamera) {
            z = true;
        }
        AnonymousClass009.A0A("attempted to close orphaned camera", z);
        if (!(voipPhysicalCamera2 == null || voipPhysicalCamera2 == voipPhysicalCamera)) {
            voipPhysicalCamera2.removeCameraEventsListener(this.cameraEventsListener);
            voipPhysicalCamera2.close(true);
        }
        voipPhysicalCamera.removeCameraEventsListener(this.cameraEventsListener);
        this.lastAdjustedCameraPreviewSize = voipPhysicalCamera.getAdjustedPreviewSize();
        this.currentCamera = null;
    }

    public synchronized VoipCamera createCamera(int i, int i2, int i3, int i4, int i5, long j) {
        VoipCamera voipCamera;
        VoipPhysicalCamera r6;
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        voipCamera = null;
        voipCamera = null;
        VoipPhysicalCamera voipPhysicalCamera2 = null;
        if (voipPhysicalCamera != null) {
            AnonymousClass2NT cameraInfo = voipPhysicalCamera.getCameraInfo();
            int i6 = cameraInfo.A05;
            if (i6 == i2 && cameraInfo.A02 == i3 && cameraInfo.A00 == i4 && cameraInfo.A03 == i) {
                voipCamera = new VoipCamera(voipPhysicalCamera, j);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("voip/VoipCameraManager/createCamera camera info doesn't match. Current cam: w/h: ");
                sb.append(i6);
                sb.append("/");
                sb.append(cameraInfo.A02);
                sb.append(", format: ");
                sb.append(cameraInfo.A00);
                sb.append(", idx: ");
                sb.append(cameraInfo.A03);
                sb.append(". New cam: w/h: ");
                sb.append(i2);
                sb.append("/");
                sb.append(i3);
                sb.append(", format: ");
                sb.append(i4);
                sb.append(", idx: ");
                sb.append(i);
                Log.e(sb.toString());
            }
        } else {
            try {
                AnonymousClass2NQ rawCameraInfo = getRawCameraInfo(i);
                if (rawCameraInfo != null) {
                    if (rawCameraInfo.A00 == 1) {
                        r6 = new PjCamera(i, i2, i3, i4, i5, this.systemServices, this.abProps);
                        r6.passiveMode = this.voipSharedPreferences.A01().getBoolean("force_passive_capture_dev_stream_role", false);
                    } else {
                        r6 = new AnonymousClass2NW(this.systemServices, this.abProps, this.waWorkers, i, i2, i3, i4, i5);
                        r6.passiveMode = false;
                    }
                    r6.addCameraEventsListener(this.cameraEventsListener);
                    voipPhysicalCamera2 = r6;
                } else {
                    Log.e("voip/VoipCameraManager/createCamera couldn't get camera info");
                }
                this.currentCamera = voipPhysicalCamera2;
                voipCamera = new VoipCamera(this.currentCamera, j);
            } catch (RuntimeException e) {
                Log.e("voip/VoipCameraManager/createCamera error while starting camera", e);
            }
        }
        return voipCamera;
    }

    public Point getAdjustedCameraPreviewSize() {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            return voipPhysicalCamera.getAdjustedPreviewSize();
        }
        return null;
    }

    public int getCachedCam2HardwareLevel() {
        int i = this.voipSharedPreferences.A01().getInt("lowest_camera_hardware_support_level", -1);
        if (i == -1) {
            i = AnonymousClass2NW.A00(this.systemServices);
            this.voipSharedPreferences.A01().edit().putInt("lowest_camera_hardware_support_level", i).apply();
        }
        StringBuilder sb = new StringBuilder("voip/VoipCameraManager/getCachedCam2HardwareLevel got:");
        sb.append(i);
        Log.i(sb.toString());
        return i;
    }

    public synchronized int getCameraCount() {
        int i;
        int currentApiVersion = getCurrentApiVersion();
        if (currentApiVersion == 1) {
            i = Camera.getNumberOfCameras();
        } else {
            if (Build.VERSION.SDK_INT >= 21 && currentApiVersion == 2) {
                CameraManager A0E = this.systemServices.A0E();
                if (A0E == null) {
                    Log.w("voip/VoipCameraManager/getCameraCount, unable to acquire camera manager");
                } else {
                    try {
                        i = A0E.getCameraIdList().length;
                    } catch (Exception unused) {
                        Log.w("voip/VoipCameraManager/getCameraCount, unable to connect to cameras!");
                    }
                }
            }
            return 0;
        }
        return i;
    }

    public synchronized PjCameraInfo getCameraInfo(int i) {
        PjCameraInfo pjCameraInfo;
        pjCameraInfo = null;
        if (i >= 0) {
            if (i < getCameraCount()) {
                AnonymousClass2NQ rawCameraInfo = getRawCameraInfo(i);
                if (rawCameraInfo != null) {
                    pjCameraInfo = PjCameraInfo.createFromRawInfo(rawCameraInfo, this.voipSharedPreferences);
                    StringBuilder sb = new StringBuilder();
                    sb.append("voip/VoipCameraManager/getCameraInfo camera ");
                    sb.append(i);
                    sb.append(" info: ");
                    sb.append(pjCameraInfo);
                    Log.i(sb.toString());
                }
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("voip/VoipCameraManager/getCameraInfo bad idx: ");
        sb2.append(i);
        Log.e(sb2.toString());
        return pjCameraInfo;
    }

    private int getCameraInfoCacheKey(int i, int i2) {
        int i3 = i + i2;
        return ((i3 * (i3 + 1)) >> 1) + i2;
    }

    public int getCameraStartMode() {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            return voipPhysicalCamera.getCameraStartMode();
        }
        return -1;
    }

    public synchronized int getCurrentApiVersion() {
        Integer num;
        if (this.currentApiVersion == null) {
            this.currentApiVersion = 1;
            String A02 = this.voipSharedPreferences.A02();
            if (Build.VERSION.SDK_INT >= 21 && !TextUtils.isEmpty(A02) && AnonymousClass2NW.A06(A02, getCachedCam2HardwareLevel())) {
                this.currentApiVersion = 2;
            }
        }
        num = this.currentApiVersion;
        AnonymousClass009.A05(num);
        return num.intValue();
    }

    public Point getLastAdjustedCameraPreviewSize() {
        return this.lastAdjustedCameraPreviewSize;
    }

    public AnonymousClass2NY getLastCachedFrame() {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            return voipPhysicalCamera.getLastCachedFrame();
        }
        return null;
    }

    public synchronized AnonymousClass2NQ getRawCameraInfo(int i) {
        AnonymousClass2NQ r12;
        JSONObject jSONObject;
        int i2;
        AnonymousClass2NR r13;
        JSONArray jSONArray;
        JSONArray jSONArray2;
        JSONArray jSONArray3;
        JSONArray jSONArray4;
        String obj;
        int currentApiVersion = getCurrentApiVersion();
        StringBuilder sb = new StringBuilder();
        sb.append("voip/VoipCameraManager/getRawCameraInfo camera: ");
        sb.append(i);
        sb.append(" enabled camera version: ");
        sb.append(currentApiVersion);
        Log.i(sb.toString());
        int i3 = i + currentApiVersion;
        int i4 = ((i3 * (i3 + 1)) >> 1) + currentApiVersion;
        r12 = (AnonymousClass2NQ) this.rawCameraInfoCache.get(i4);
        if (r12 == null) {
            String string = this.voipSharedPreferences.A01().getString(C17140qK.A00(i, currentApiVersion), null);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("voip/VoipCameraManager/getRawCameraInfo, stored info for camera ");
            sb2.append(i);
            sb2.append(": ");
            sb2.append(string);
            Log.i(sb2.toString());
            ArrayList arrayList = null;
            if (!TextUtils.isEmpty(string)) {
                try {
                    jSONObject = new JSONObject(string);
                    i2 = jSONObject.getInt("version");
                } catch (JSONException e) {
                    Log.e(e);
                }
                if (i2 != 1) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("voip/RawCameraInfo/createFromJson, skip mismatched json version ");
                    sb3.append(i2);
                    sb3.append(", required ");
                    sb3.append(1);
                    Log.i(sb3.toString());
                } else {
                    int i5 = jSONObject.getInt("apiVersion");
                    if (i5 == 1 || i5 == 2) {
                        boolean z = jSONObject.getBoolean("isFrontCamera");
                        int i6 = jSONObject.getInt("orientation");
                        JSONArray jSONArray5 = jSONObject.getJSONArray("supportFormats");
                        if (jSONArray5 == null) {
                            Log.e("voip/RawCameraInfo/createFromJson, cannot find formats");
                        } else {
                            int[] iArr = new int[jSONArray5.length()];
                            for (int i7 = 0; i7 < jSONArray5.length(); i7++) {
                                iArr[i7] = jSONArray5.getInt(i7);
                            }
                            if (!jSONObject.has("preferredSize") || (jSONArray2 = jSONObject.getJSONArray("preferredSize")) == null) {
                                r13 = null;
                            } else if (jSONArray2.length() != 2) {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("voip/RawCameraInfo createFromJson bad preferred size ");
                                sb4.append(jSONArray2);
                                Log.e(sb4.toString());
                            } else {
                                r13 = new AnonymousClass2NR(jSONArray2.getInt(0), jSONArray2.getInt(1));
                            }
                            if (jSONObject.has("previewSizes") && (jSONArray = jSONObject.getJSONArray("previewSizes")) != null) {
                                int length = jSONArray.length();
                                if (length % 2 == 0) {
                                    arrayList = new ArrayList(length >> 1);
                                    for (int i8 = 0; i8 < length; i8 += 2) {
                                        arrayList.add(new AnonymousClass2NR(jSONArray.getInt(i8), jSONArray.getInt(i8 + 1)));
                                    }
                                } else {
                                    throw new JSONException("length is not even");
                                }
                            }
                            r12 = new AnonymousClass2NQ(r13, arrayList, iArr, i5, i6, z);
                            if (!isRawCameraInfoValid(i, r12)) {
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("voip/VoipCameraManager/getRawCameraInfo, stored raw info is outdated ");
                                sb5.append(r12);
                                Log.w(sb5.toString());
                                clearStoredRawCameraInfo(i, r12.A00);
                            }
                            this.rawCameraInfoCache.put(i4, r12);
                        }
                    } else {
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("voip/RawCameraInfo/createFromJson, skip unsupported api version ");
                        sb6.append(i5);
                        Log.i(sb6.toString());
                    }
                }
            }
            r12 = loadFromCameraService(i);
            if (r12 != null) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    jSONObject2.put("version", 1);
                    jSONObject2.put("apiVersion", r12.A00);
                    jSONObject2.put("isFrontCamera", r12.A04);
                    jSONObject2.put("orientation", r12.A01);
                    JSONArray jSONArray6 = new JSONArray();
                    for (int i9 : r12.A05) {
                        jSONArray6.put(i9);
                    }
                    jSONObject2.put("supportFormats", jSONArray6);
                    AnonymousClass2NR r2 = r12.A02;
                    if (r2 != null) {
                        jSONArray3 = new JSONArray();
                        jSONArray3.put(r2.A01);
                        jSONArray3.put(r2.A00);
                    } else {
                        jSONArray3 = null;
                    }
                    jSONObject2.put("preferredSize", jSONArray3);
                    List<AnonymousClass2NR> list = r12.A03;
                    if (list != null) {
                        jSONArray4 = new JSONArray();
                        for (AnonymousClass2NR r1 : list) {
                            jSONArray4.put(r1.A01);
                            jSONArray4.put(r1.A00);
                        }
                    } else {
                        jSONArray4 = null;
                    }
                    jSONObject2.put("previewSizes", jSONArray4);
                    obj = jSONObject2.toString();
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                if (!TextUtils.isEmpty(obj)) {
                    this.voipSharedPreferences.A01().edit().putString(C17140qK.A00(i, r12.A00), obj).apply();
                }
            } else {
                clearStoredRawCameraInfo(i, currentApiVersion);
            }
            this.rawCameraInfoCache.put(i4, r12);
        }
        return r12;
    }

    public boolean isCameraTextureApiFailed() {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        return voipPhysicalCamera != null && voipPhysicalCamera.textureApiFailed;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v5, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    private boolean isRawCameraInfoValid(int i, AnonymousClass2NQ r8) {
        boolean z;
        boolean z2;
        int i2 = r8.A00;
        if (i2 == 1) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            try {
                Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.orientation != r8.A01) {
                    return false;
                }
                int i3 = cameraInfo.facing;
                z2 = r8.A04;
                z = i3;
            } catch (RuntimeException e) {
                Log.e(e);
                return false;
            }
        } else {
            if (i2 == 2) {
                try {
                    CameraManager A0E = this.systemServices.A0E();
                    if (A0E != null) {
                        CameraCharacteristics cameraCharacteristics = A0E.getCameraCharacteristics(Integer.toString(i));
                        Integer num = (Integer) cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                        Integer num2 = (Integer) cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                        if (num == null || num2 == null) {
                            Log.w("voip/VoipCameraManager/isRawCameraInfoValid metadata returned null values, invalidating cache");
                            return false;
                        } else if (r8.A01 != num.intValue()) {
                            return false;
                        } else {
                            boolean z3 = r8.A04;
                            z2 = false;
                            z = z3;
                            if (num2.intValue() == 0) {
                                z2 = true;
                                z = z3;
                            }
                        }
                    }
                } catch (Exception e2) {
                    Log.w("voip/VoipCameraManager/isRawCameraInfoValid, camera is unavailable, invalidating info", e2);
                }
            }
            return false;
        }
        if (z == z2) {
            return true;
        }
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 6, insn: 0x012d: IF  (r6 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:53:0x0132, block:B:51:0x012d
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    private X.AnonymousClass2NQ loadFromCameraService(
/*
[307] Method generation error in method: com.whatsapp.voipcalling.camera.VoipCameraManager.loadFromCameraService(int):X.2NQ, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r16v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public void removeCameraErrorListener(AnonymousClass2NP r2) {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            voipPhysicalCamera.removeCameraEventsListener(r2);
        }
    }

    public synchronized void setCurrentApiVersion(int i) {
        this.currentApiVersion = Integer.valueOf(i);
    }

    public void setRequestedCamera2SupportLevel(String str) {
        int currentApiVersion = getCurrentApiVersion();
        int i = 1;
        if (Build.VERSION.SDK_INT >= 21 && str != null && AnonymousClass2NW.A06(str, getCachedCam2HardwareLevel())) {
            i = 2;
        }
        setCurrentApiVersion(i);
        if (currentApiVersion != getCurrentApiVersion()) {
            Voip.refreshVideoDevice();
        }
    }

    public void updateCameraPreviewOrientation() {
        VoipPhysicalCamera voipPhysicalCamera = this.currentCamera;
        if (voipPhysicalCamera != null) {
            voipPhysicalCamera.updatePreviewOrientation();
        }
    }
}
