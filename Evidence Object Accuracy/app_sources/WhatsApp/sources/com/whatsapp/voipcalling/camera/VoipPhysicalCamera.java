package com.whatsapp.voipcalling.camera;

import X.AbstractC1311461l;
import X.AnonymousClass009;
import X.AnonymousClass2NP;
import X.AnonymousClass2NT;
import X.AnonymousClass2NY;
import X.AnonymousClass660;
import X.C14850m9;
import X.C92444Vx;
import X.C92704Xc;
import X.HandlerC52032a8;
import X.HandlerThreadC73393gA;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.camera.VoipCamera;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;

/* loaded from: classes2.dex */
public abstract class VoipPhysicalCamera {
    public static final int CAMERA_MODE_CONSERVATIVE;
    public static final int CAMERA_MODE_DEFAULT;
    public static final int CAMERA_MODE_NO_FPS_RANGE;
    public static final int ERROR_CAMERA_PROCESSOR_SETUP_ERROR;
    public static final int ERROR_CAMERA_SESSION_CONFIGURING;
    public static final int ERROR_EXCEPTION_IN_CAMERA;
    public static final int ERROR_INVALID_STATE;
    public static final int ERROR_NO_CAMERA_AFTER_OPEN;
    public static final int ERROR_NO_CAMERA_IN_STOP;
    public static final int ERROR_OPEN_CAMERA;
    public static final int ERROR_POST_TO_LOOPER;
    public static final int ERROR_SETUP_PREVIEW;
    public static final int ERROR_SET_PARAMETERS;
    public static final int ERROR_START_FINAL_FAILED;
    public static final int ERROR_SWITCH_SURFACE_VIEW;
    public static final int MESSAGE_LAST_CAMERA_CALLBACK_CHECK;
    public static final int MESSAGE_ON_FRAME_AVAILABLE;
    public static final int SUCCESS;
    public static final String TAG;
    public final C14850m9 abProps;
    public long cameraCallbackCount;
    public final C92704Xc cameraEventsDispatcher = new C92704Xc(this);
    public AbstractC1311461l cameraProcessor;
    public HandlerThread cameraThread;
    public final Handler cameraThreadHandler;
    public long lastCameraCallbackTs;
    public boolean passiveMode;
    public volatile boolean textureApiFailed;
    public C92444Vx textureHolder;
    public final long thresholdRestartCameraMillis = 2000;
    public long totalElapsedCameraCallbackTime;
    public VideoPort videoPort;
    public final Map virtualCameras;

    public abstract void closeOnCameraThread();

    public abstract Point getAdjustedPreviewSize();

    public abstract AnonymousClass2NT getCameraInfo();

    public abstract int getCameraStartMode();

    public abstract AnonymousClass2NY getLastCachedFrame();

    public abstract int getLatestFrame(ByteBuffer byteBuffer);

    public abstract void onFrameAvailableOnCameraThread();

    public abstract int setVideoPortOnCameraThread(VideoPort videoPort);

    public abstract int startOnCameraThread();

    public abstract int stopOnCameraThread();

    public abstract void updatePreviewOrientation();

    public VoipPhysicalCamera(C14850m9 r3) {
        this.abProps = r3;
        this.virtualCameras = new HashMap();
        HandlerThreadC73393gA r0 = new HandlerThreadC73393gA(this);
        this.cameraThread = r0;
        r0.start();
        this.cameraThreadHandler = new HandlerC52032a8(this.cameraThread.getLooper(), this);
        if (Build.VERSION.SDK_INT >= 21 && r3.A07(1402)) {
            this.cameraProcessor = new AnonymousClass660();
        }
    }

    public final void addCameraEventsListener(AnonymousClass2NP r3) {
        C92704Xc r1 = this.cameraEventsDispatcher;
        synchronized (r1) {
            r1.A00.add(r3);
        }
    }

    private void clearFrameAvailableMessages() {
        this.cameraThreadHandler.removeMessages(2);
    }

    public final synchronized void close(boolean z) {
        HandlerThread handlerThread;
        Log.i("voip/video/VoipCamera/close Enter");
        if (((Boolean) syncRunOnCameraThread(new Callable(z) { // from class: X.5Dq
            public final /* synthetic */ boolean A01;

            {
                this.A01 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return VoipPhysicalCamera.this.lambda$close$3(this.A01);
            }
        }, Boolean.FALSE)).booleanValue() && (handlerThread = this.cameraThread) != null) {
            handlerThread.quit();
            this.cameraThread = null;
        }
        Log.i("voip/video/VoipCamera/close Exit");
    }

    public void createTexture(int i, int i2) {
        boolean z = false;
        if (this.videoPort != null) {
            z = true;
        }
        AnonymousClass009.A0A("videoport should not be null in createTexture", z);
        C92444Vx r0 = this.textureHolder;
        if (r0 == null) {
            r0 = this.videoPort.createSurfaceTexture();
            this.textureHolder = r0;
            if (r0 == null) {
                Log.e("voip/video/VoipCamera/createSurfaceTexture failed to create SurfaceTexture");
                this.textureApiFailed = true;
                return;
            }
        }
        r0.A01.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() { // from class: X.4hi
            @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
            public final void onFrameAvailable(SurfaceTexture surfaceTexture) {
                VoipPhysicalCamera.this.notifyFrameAvailable();
            }
        });
        this.textureHolder.A01.setDefaultBufferSize(i, i2);
        AbstractC1311461l r1 = this.cameraProcessor;
        if (r1 != null) {
            r1.Acb(this.textureHolder.A01, i, i2);
        }
    }

    private Object exchange(Exchanger exchanger, Object obj) {
        try {
            return exchanger.exchange(obj);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static int fpsRangeScore(int i, int i2, int i3) {
        int i4;
        if (i <= 5000) {
            i4 = -(5000 - i);
        } else {
            i4 = (-(i - 5000)) << 2;
        }
        return (i4 - Math.abs(i2 - i3)) / 1000;
    }

    public final int getAverageCaptureFps() {
        long j = this.totalElapsedCameraCallbackTime;
        if (j <= 0) {
            return 0;
        }
        return (int) ((this.cameraCallbackCount * 1000) / j);
    }

    public final boolean isPassiveMode() {
        return this.passiveMode;
    }

    public boolean isTextureApiFailed() {
        return this.textureApiFailed;
    }

    public /* synthetic */ Boolean lambda$close$3(boolean z) {
        if (z) {
            this.virtualCameras.clear();
        }
        if (this.virtualCameras.size() != 0) {
            return Boolean.FALSE;
        }
        closeOnCameraThread();
        return Boolean.TRUE;
    }

    private /* synthetic */ void lambda$createTexture$6(SurfaceTexture surfaceTexture) {
        notifyFrameAvailable();
    }

    public /* synthetic */ Integer lambda$registerVirtualCamera$4(VoipCamera voipCamera) {
        if (!this.virtualCameras.containsKey(Long.valueOf(voipCamera.userIdentity))) {
            this.virtualCameras.put(Long.valueOf(voipCamera.userIdentity), voipCamera);
        }
        return 0;
    }

    private /* synthetic */ Integer lambda$setVideoPort$1(VideoPort videoPort) {
        return Integer.valueOf(setVideoPortOnCameraThread(videoPort));
    }

    public /* synthetic */ Integer lambda$stop$2() {
        int i;
        Iterator it = this.virtualCameras.entrySet().iterator();
        boolean z = true;
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            } else if (((VoipCamera) ((Map.Entry) it.next()).getValue()).started) {
                z = false;
            }
        }
        if (z) {
            i = stopOnCameraThread();
        }
        return Integer.valueOf(i);
    }

    public /* synthetic */ void lambda$syncRunOnCameraThread$0(Exchanger exchanger, Callable callable) {
        try {
            exchange(exchanger, callable.call());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public /* synthetic */ Integer lambda$unregisterVirtualCamera$5(VoipCamera voipCamera) {
        int i;
        this.virtualCameras.remove(Long.valueOf(voipCamera.userIdentity));
        if (this.virtualCameras.size() == 0) {
            i = stopOnCameraThread();
        } else {
            i = 0;
        }
        return Integer.valueOf(i);
    }

    public void notifyFrameAvailable() {
        this.cameraThreadHandler.sendEmptyMessage(2);
    }

    public int registerVirtualCamera(VoipCamera voipCamera) {
        StringBuilder sb = new StringBuilder("voip/video/VoipCamera/Add new virtual camera with user identity ");
        sb.append(voipCamera.userIdentity);
        Log.i(sb.toString());
        return ((Number) syncRunOnCameraThread(new Callable(voipCamera, this) { // from class: X.5Dp
            public final /* synthetic */ VoipCamera A00;
            public final /* synthetic */ VoipPhysicalCamera A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return this.A01.lambda$registerVirtualCamera$4(this.A00);
            }
        }, -1)).intValue();
    }

    public void releaseTexture() {
        AbstractC1311461l r0 = this.cameraProcessor;
        boolean z = false;
        if (r0 != null) {
            r0.Acb(null, 0, 0);
        }
        if (this.textureHolder != null) {
            if (this.videoPort != null) {
                z = true;
            }
            AnonymousClass009.A0A("video port should not be null in releaseTexture", z);
            this.textureHolder.A01.setOnFrameAvailableListener(null);
            clearFrameAvailableMessages();
            this.videoPort.releaseSurfaceTexture(this.textureHolder);
            this.textureHolder = null;
        }
    }

    public final void removeCameraEventsListener(AnonymousClass2NP r3) {
        C92704Xc r1 = this.cameraEventsDispatcher;
        synchronized (r1) {
            r1.A00.remove(r3);
        }
    }

    public final void setPassiveMode(boolean z) {
        this.passiveMode = z;
    }

    public final synchronized int setVideoPort(VideoPort videoPort) {
        int intValue;
        Log.i("voip/video/VoipCamera/setVideoPort Enter");
        intValue = ((Integer) syncRunOnCameraThread(new Callable(videoPort, this) { // from class: X.5Dn
            public final /* synthetic */ VideoPort A00;
            public final /* synthetic */ VoipPhysicalCamera A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return Integer.valueOf(this.A01.setVideoPortOnCameraThread(this.A00));
            }
        }, -100)).intValue();
        StringBuilder sb = new StringBuilder();
        sb.append("voip/video/VoipCamera/setVideoPort Exit with ");
        sb.append(intValue);
        Log.i(sb.toString());
        return intValue;
    }

    public final synchronized int start() {
        String str;
        int intValue;
        StringBuilder sb = new StringBuilder();
        sb.append("voip/video/VoipCamera/start Enter in ");
        if (this.passiveMode) {
            str = "passive ";
        } else {
            str = "active ";
        }
        sb.append(str);
        sb.append("mode");
        Log.i(sb.toString());
        intValue = ((Integer) syncRunOnCameraThread(new Callable() { // from class: X.5Dg
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return Integer.valueOf(VoipPhysicalCamera.this.startOnCameraThread());
            }
        }, -100)).intValue();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("voip/video/VoipCamera/start Exit with ");
        sb2.append(intValue);
        Log.i(sb2.toString());
        return intValue;
    }

    public final void startPeriodicCameraCallbackCheck() {
        stopPeriodicCameraCallbackCheck();
        this.cameraThreadHandler.sendEmptyMessageDelayed(1, 1000);
    }

    public final synchronized void stop() {
        Log.i("voip/video/VoipCamera/stop Enter");
        int intValue = ((Integer) syncRunOnCameraThread(new Callable() { // from class: X.5Dh
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return VoipPhysicalCamera.this.lambda$stop$2();
            }
        }, -100)).intValue();
        StringBuilder sb = new StringBuilder();
        sb.append("voip/video/VoipCamera/stop Exit with ");
        sb.append(intValue);
        Log.i(sb.toString());
    }

    public final void stopPeriodicCameraCallbackCheck() {
        this.cameraThreadHandler.removeMessages(1);
        this.lastCameraCallbackTs = SystemClock.elapsedRealtime();
    }

    public final Object syncRunOnCameraThread(Callable callable, Object obj) {
        Exchanger exchanger = new Exchanger();
        return this.cameraThreadHandler.post(new RunnableBRunnable0Shape2S0300000_I0_2(this, exchanger, callable, 6)) ? exchange(exchanger, null) : obj;
    }

    public int unregisterVirtualCamera(VoipCamera voipCamera) {
        StringBuilder sb = new StringBuilder("voip/video/VoipCamera/Remove virtual camera with user identity ");
        sb.append(voipCamera.userIdentity);
        Log.i(sb.toString());
        return ((Number) syncRunOnCameraThread(new Callable(voipCamera, this) { // from class: X.5Do
            public final /* synthetic */ VoipCamera A00;
            public final /* synthetic */ VoipPhysicalCamera A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return this.A01.lambda$unregisterVirtualCamera$5(this.A00);
            }
        }, -1)).intValue();
    }

    public final void updateCameraCallbackCheck() {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.totalElapsedCameraCallbackTime += elapsedRealtime - this.lastCameraCallbackTs;
        this.lastCameraCallbackTs = elapsedRealtime;
        this.cameraCallbackCount++;
    }
}
