package com.whatsapp.voipcalling;

import X.AbstractActivityC28171Kz;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass01N;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass04S;
import X.AnonymousClass0OC;
import X.AnonymousClass10S;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass1AT;
import X.AnonymousClass1BA;
import X.AnonymousClass1J1;
import X.AnonymousClass1L1;
import X.AnonymousClass1L2;
import X.AnonymousClass1L3;
import X.AnonymousClass1L5;
import X.AnonymousClass1L6;
import X.AnonymousClass1L7;
import X.AnonymousClass1S6;
import X.AnonymousClass1S7;
import X.AnonymousClass1SF;
import X.AnonymousClass28F;
import X.AnonymousClass2CT;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2FR;
import X.AnonymousClass2ID;
import X.AnonymousClass2NP;
import X.AnonymousClass2SU;
import X.AnonymousClass2xD;
import X.AnonymousClass37Q;
import X.AnonymousClass3DO;
import X.AnonymousClass3L8;
import X.AnonymousClass3UA;
import X.AnonymousClass482;
import X.AnonymousClass4GV;
import X.AnonymousClass4LP;
import X.AnonymousClass5V8;
import X.C004802e;
import X.C004902f;
import X.C103674r8;
import X.C1102854y;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16210od;
import X.C17050qB;
import X.C17140qK;
import X.C18350sJ;
import X.C18360sK;
import X.C18470sV;
import X.C18640sm;
import X.C18750sx;
import X.C18810t5;
import X.C19890uq;
import X.C20230vQ;
import X.C20650w6;
import X.C20670w8;
import X.C20710wC;
import X.C20740wF;
import X.C20820wN;
import X.C20850wQ;
import X.C20970wc;
import X.C21250x7;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C236312k;
import X.C236712o;
import X.C236812p;
import X.C236912q;
import X.C237512w;
import X.C238013b;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C26041Bu;
import X.C26391De;
import X.C27131Gd;
import X.C29631Ua;
import X.C29941Vi;
import X.C36161jQ;
import X.C48232Fc;
import X.C48782Ht;
import X.C49902Nf;
import X.C50102Ob;
import X.C53572ef;
import X.C60232wG;
import X.C60242wH;
import X.C64363Fg;
import X.C64523Fw;
import X.C65293Iy;
import X.C83343x5;
import X.C852241s;
import X.C91984Tz;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0001000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.ViewOnClickCListenerShape1S0200000_I0_1;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.calling.CallDetailsLayout;
import com.whatsapp.calling.CallPictureGrid;
import com.whatsapp.calling.callgrid.view.CallGrid;
import com.whatsapp.calling.callgrid.view.FocusViewContainer;
import com.whatsapp.calling.callgrid.view.MenuBottomSheet;
import com.whatsapp.calling.callgrid.view.PipViewContainer;
import com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.calling.callgrid.viewmodel.InCallBannerViewModel;
import com.whatsapp.calling.callgrid.viewmodel.MenuBottomSheetViewModel;
import com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel;
import com.whatsapp.calling.callheader.viewmodel.CallHeaderViewModel;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout;
import com.whatsapp.calling.views.AppSettingsWarningDialogFragment;
import com.whatsapp.calling.views.VoipCallFooter;
import com.whatsapp.calling.views.VoipContactPickerDialogFragment;
import com.whatsapp.components.AnimatingArrowsLayout;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.filter.FilterUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* loaded from: classes2.dex */
public class VoipActivityV2 extends AbstractActivityC28171Kz implements AnonymousClass1L1, AnonymousClass1L2, AnonymousClass1L3, ViewTreeObserver.OnGlobalLayoutListener, AnonymousClass1L5, AnonymousClass1L6, AnonymousClass1L7 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public long A0A;
    public Drawable A0B;
    public Drawable A0C;
    public Drawable A0D;
    public Handler A0E;
    public View.OnClickListener A0F;
    public View.OnClickListener A0G;
    public View.OnClickListener A0H;
    public View A0I;
    public View A0J;
    public View A0K;
    public View A0L;
    public View A0M;
    public View A0N;
    public View A0O;
    public View A0P;
    public View A0Q;
    public View A0R;
    public ImageButton A0S;
    public ImageButton A0T;
    public ImageView A0U;
    public ImageView A0V;
    public TextView A0W;
    public TextView A0X;
    public TextView A0Y;
    public TextView A0Z;
    public TextView A0a;
    public TextView A0b;
    public DialogFragment A0c;
    public DialogFragment A0d;
    public DialogFragment A0e;
    public C16210od A0f;
    public C236912q A0g;
    public WaImageView A0h;
    public C238013b A0i;
    public CallDetailsLayout A0j;
    public CallPictureGrid A0k;
    public CallGrid A0l;
    public MenuBottomSheet A0m;
    public AnonymousClass3DO A0n;
    public VoipInCallNotifBanner A0o;
    public CallGridViewModel A0p;
    public InCallBannerViewModel A0q;
    public MenuBottomSheetViewModel A0r;
    public OrientationViewModel A0s;
    public CallHeaderViewModel A0t;
    public AnonymousClass2ID A0u;
    public C29631Ua A0v;
    public C20970wc A0w;
    public MaximizedParticipantVideoDialogFragment A0x;
    public VideoCallParticipantView A0y;
    public VideoCallParticipantViewLayout A0z;
    public AnonymousClass2SU A10;
    public AppSettingsWarningDialogFragment A11;
    public C27131Gd A12;
    public AnonymousClass10S A13;
    public C15610nY A14;
    public AnonymousClass1J1 A15;
    public C21270x9 A16;
    public AnonymousClass3UA A17;
    public ContactPickerFragment A18;
    public C18360sK A19;
    public C15890o4 A1A;
    public C18750sx A1B;
    public C236812p A1C;
    public C21250x7 A1D;
    public C16120oU A1E;
    public C20230vQ A1F;
    public AnonymousClass1AT A1G;
    public AnonymousClass1BA A1H;
    public C21260x8 A1I;
    public AnonymousClass19Z A1J;
    public AnonymousClass37Q A1K;
    public C237512w A1L;
    public AnonymousClass2CT A1M;
    public VoipCallControlBottomSheetV2 A1N;
    public C21280xA A1O;
    public C17140qK A1P;
    public VoipCameraManager A1Q;
    public C236312k A1R;
    public String A1S;
    public String A1T;
    public Map A1U;
    public AnonymousClass01N A1V;
    public boolean A1W;
    public boolean A1X;
    public boolean A1Y;
    public boolean A1Z;
    public boolean A1a;
    public boolean A1b;
    public boolean A1c;
    public boolean A1d;
    public boolean A1e;
    public boolean A1f;
    public boolean A1g;
    public boolean A1h;
    public boolean A1i;
    public boolean A1j;
    public boolean A1k;
    public boolean A1l;
    public boolean A1m;
    public boolean A1n;
    public boolean A1o;
    public boolean A1p;
    public final AnonymousClass28F A1q;
    public final C236712o A1r;
    public final AnonymousClass5V8 A1s;
    public final AnonymousClass4LP A1t;
    public final AnonymousClass2NP A1u;

    @Override // X.ActivityC13790kL
    public boolean A2d() {
        return false;
    }

    public VoipActivityV2() {
        this(0);
        this.A1p = false;
        this.A00 = -1.0f;
        this.A1r = new AnonymousClass482(this);
        this.A1u = new C49902Nf(this);
        this.A1t = new AnonymousClass4LP(this);
        this.A1n = true;
        this.A01 = 3;
        this.A1s = new AnonymousClass5V8() { // from class: X.5Aw
            @Override // X.AnonymousClass5V8
            public final void ANW(AnonymousClass1YT r4) {
                VoipActivityV2 voipActivityV2 = VoipActivityV2.this;
                voipActivityV2.finish();
                if (r4 != null) {
                    voipActivityV2.A1J.A06(voipActivityV2, r4, 4);
                }
                voipActivityV2.A1K = null;
            }
        };
        this.A1q = new C1102854y(this);
        this.A12 = null;
    }

    public VoipActivityV2(int i) {
        this.A1e = false;
        A0R(new C103674r8(this));
    }

    public static final List A02(CallInfo callInfo, boolean z) {
        int i;
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1S6 r2 : callInfo.participants.values()) {
            if (!r2.A0F && (z || ((i = r2.A01) != 11 && i == 1))) {
                arrayList.add(r2.A06);
            }
        }
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/getPeerParticipantJids ");
        sb.append(arrayList);
        Log.i(sb.toString());
        return arrayList;
    }

    public static /* synthetic */ void A03(Pair pair, VoipActivityV2 voipActivityV2) {
        C29631Ua r3;
        if (pair != null) {
            AbstractC14640lm r4 = (AbstractC14640lm) pair.first;
            int intValue = ((Number) pair.second).intValue();
            MenuBottomSheet menuBottomSheet = voipActivityV2.A0m;
            if (menuBottomSheet != null) {
                menuBottomSheet.A1C();
                voipActivityV2.A0m = null;
            }
            if (intValue == 1) {
                ((ActivityC13790kL) voipActivityV2).A00.A07(voipActivityV2, new C14960mK().A0g(voipActivityV2, ((AbstractActivityC28171Kz) voipActivityV2).A03.A0B(r4)));
                if (Build.VERSION.SDK_INT >= 26) {
                    voipActivityV2.A3V(voipActivityV2.A2i());
                }
            } else if (intValue == 2 && (r3 = voipActivityV2.A0v) != null) {
                r3.A0x.execute(new RunnableBRunnable0Shape1S0200000_I0_1(r3, 38, r4));
            }
        }
    }

    public static void A09(View view) {
        StringBuilder sb = new StringBuilder("calling/VoipUiUtils/animateButtonIn delay:");
        sb.append(100);
        Log.i(sb.toString());
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new OvershootInterpolator(1.0f));
        scaleAnimation.setDuration(300);
        scaleAnimation.setStartOffset((long) 100);
        view.startAnimation(scaleAnimation);
    }

    public static /* synthetic */ void A0A(UserJid userJid, VoipActivityV2 voipActivityV2) {
        C29631Ua r3;
        CallInfo A2i = voipActivityV2.A2i();
        if (A2i != null && A2i.callState != Voip.CallState.NONE && (r3 = voipActivityV2.A0v) != null) {
            r3.A0x.execute(new RunnableBRunnable0Shape1S0200000_I0_1(r3, 37, userJid));
        }
    }

    public static /* synthetic */ void A0B(VoipActivityV2 voipActivityV2) {
        if (Build.VERSION.SDK_INT < 26 || !((ActivityC13810kN) voipActivityV2).A08.A0S("android.software.picture_in_picture") || !voipActivityV2.A3T() || !voipActivityV2.A3V(voipActivityV2.A2i())) {
            voipActivityV2.finish();
        }
        if (voipActivityV2.getIntent().getBooleanExtra("isTaskRoot", true)) {
            voipActivityV2.startActivity(C14960mK.A02(voipActivityV2));
        }
    }

    public static /* synthetic */ boolean A0D(View.OnClickListener onClickListener, View view, VoipActivityV2 voipActivityV2) {
        Drawable drawable;
        AnonymousClass2SU r1;
        Log.i("voip/VoipActivityV2/videoParticipantView/onLongClick");
        if (!(view instanceof VideoCallParticipantView)) {
            AnonymousClass009.A07("long click on non VideoCallParticipantView");
            return false;
        }
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        VideoCallParticipantView videoCallParticipantView = (VideoCallParticipantView) view;
        UserJid userJid = videoCallParticipantView.A0O;
        CallInfo A2i = voipActivityV2.A2i();
        boolean z = true;
        if (userJid != null) {
            if (A2i != null && videoCallParticipantView.A08()) {
                MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = voipActivityV2.A0x;
                if (maximizedParticipantVideoDialogFragment != null) {
                    maximizedParticipantVideoDialogFragment.A1L(true);
                }
                AnonymousClass3L8 r15 = new DialogInterface.OnDismissListener(userJid, voipActivityV2) { // from class: X.3L8
                    public final /* synthetic */ UserJid A00;
                    public final /* synthetic */ VoipActivityV2 A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        VoipActivityV2 voipActivityV22 = this.A01;
                        UserJid userJid2 = this.A00;
                        MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment2 = voipActivityV22.A0x;
                        if (maximizedParticipantVideoDialogFragment2 != null && ((DialogFragment) maximizedParticipantVideoDialogFragment2).A03 == dialogInterface) {
                            voipActivityV22.A0x = null;
                        }
                        if (voipActivityV22.A1U.containsKey(userJid2)) {
                            ((AnonymousClass2SU) voipActivityV22.A1U.get(userJid2)).A05();
                        }
                    }
                };
                int[] iArr = new int[2];
                videoCallParticipantView.getLocationOnScreen(iArr);
                int i = iArr[0];
                int i2 = iArr[1];
                int width = videoCallParticipantView.getWidth();
                int height = videoCallParticipantView.getHeight();
                if (AnonymousClass4GV.A00 || Build.VERSION.SDK_INT < 21) {
                    drawable = new ColorDrawable(-16777216);
                } else {
                    Bitmap cachedViewBitmap = voipActivityV2.A0z.getCachedViewBitmap();
                    Canvas canvas = new Canvas(cachedViewBitmap);
                    Paint paint = new Paint();
                    paint.setColorFilter(new LightingColorFilter(-10066330, 0));
                    for (AnonymousClass2SU r2 : voipActivityV2.A1U.values()) {
                        Bitmap A00 = r2.A00();
                        if (!(A00 == null || r2.A00 == null)) {
                            int[] iArr2 = new int[2];
                            voipActivityV2.A0z.getLocationOnScreen(iArr2);
                            VideoCallParticipantView videoCallParticipantView2 = r2.A00;
                            videoCallParticipantView2.getLocationOnScreen(r0);
                            int i3 = r0[1] - iArr2[1];
                            int[] iArr3 = {iArr3[0] - iArr2[0], i3};
                            int i4 = iArr3[0];
                            canvas.drawBitmap(A00, (Rect) null, new Rect(i4, i3, i4 + videoCallParticipantView2.getWidth(), iArr3[1] + videoCallParticipantView2.getHeight()), paint);
                            A00.recycle();
                        }
                    }
                    FilterUtils.blurNative(cachedViewBitmap, voipActivityV2.getResources().getDimensionPixelSize(R.dimen.maximized_video_call_background_blur_radius), 2);
                    drawable = new BitmapDrawable(voipActivityV2.getResources(), cachedViewBitmap);
                }
                MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment2 = new MaximizedParticipantVideoDialogFragment(r15, drawable, onClickListener, new RunnableBRunnable0Shape13S0100000_I0_13(voipActivityV2, 36), i, i2, width, height);
                voipActivityV2.A0x = maximizedParticipantVideoDialogFragment2;
                if (userJid != voipActivityV2.A10.A04) {
                    r1 = new C60242wH(maximizedParticipantVideoDialogFragment2, userJid, voipActivityV2);
                } else {
                    r1 = new C60232wG(maximizedParticipantVideoDialogFragment2, userJid, voipActivityV2, "max_preview");
                }
                maximizedParticipantVideoDialogFragment2.A08 = r1;
                if (voipActivityV2.A1n) {
                    voipActivityV2.A1n = false;
                    if (voipActivityV2.A1N == null) {
                        View decorView = voipActivityV2.getWindow().getDecorView();
                        decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() | 4);
                        voipActivityV2.getWindow().setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                    }
                    voipActivityV2.A2t();
                } else {
                    MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment3 = voipActivityV2.A0x;
                    if (!maximizedParticipantVideoDialogFragment3.A0c()) {
                        maximizedParticipantVideoDialogFragment3.A1F(voipActivityV2.A0V(), "maximized_video");
                        return true;
                    }
                }
                return true;
            }
        } else if (A2i != null) {
            z = false;
        }
        AnonymousClass009.A0A("null jid or callinfo on long clicked VideoCallParticipantView", z);
        return false;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A1e) {
            this.A1e = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC28171Kz) this).A05 = (C20650w6) r1.A3A.get();
            ((AbstractActivityC28171Kz) this).A09 = (C18470sV) r1.AK8.get();
            ((AbstractActivityC28171Kz) this).A02 = (C20670w8) r1.AMw.get();
            ((AbstractActivityC28171Kz) this).A03 = (C15550nR) r1.A45.get();
            ((AbstractActivityC28171Kz) this).A0B = (C19890uq) r1.ABw.get();
            ((AbstractActivityC28171Kz) this).A0A = (C20710wC) r1.A8m.get();
            ((AbstractActivityC28171Kz) this).A0E = (AbstractC15850o0) r1.ANA.get();
            ((AbstractActivityC28171Kz) this).A04 = (C17050qB) r1.ABL.get();
            ((AbstractActivityC28171Kz) this).A0C = (C20740wF) r1.AIA.get();
            ((AbstractActivityC28171Kz) this).A0D = (C18350sJ) r1.AHX.get();
            ((AbstractActivityC28171Kz) this).A06 = (C20820wN) r1.ACG.get();
            ((AbstractActivityC28171Kz) this).A08 = (C26041Bu) r1.ACL.get();
            ((AbstractActivityC28171Kz) this).A00 = (AnonymousClass2FR) r2.A0P.get();
            ((AbstractActivityC28171Kz) this).A07 = (C20850wQ) r1.ACI.get();
            this.A1E = (C16120oU) r1.ANE.get();
            this.A1D = (C21250x7) r1.AJh.get();
            this.A1J = (AnonymousClass19Z) r1.A2o.get();
            this.A1R = (C236312k) r1.AMY.get();
            this.A1I = (C21260x8) r1.A2k.get();
            this.A16 = (C21270x9) r1.A4A.get();
            this.A1O = (C21280xA) r1.AMU.get();
            this.A14 = (C15610nY) r1.AMe.get();
            this.A0i = (C238013b) r1.A1Z.get();
            this.A13 = (AnonymousClass10S) r1.A46.get();
            this.A1B = (C18750sx) r1.A2p.get();
            this.A1Q = (VoipCameraManager) r1.AMV.get();
            this.A0u = r2.A04();
            this.A0w = r1.A2X();
            this.A1A = (C15890o4) r1.AN1.get();
            this.A1C = (C236812p) r1.AAC.get();
            this.A1H = (AnonymousClass1BA) r1.A2e.get();
            this.A1P = (C17140qK) r1.AMZ.get();
            this.A19 = (C18360sK) r1.AN0.get();
            this.A1F = (C20230vQ) r1.ACe.get();
            this.A0g = (C236912q) r1.A1n.get();
            this.A0f = (C16210od) r1.A0Y.get();
            this.A1G = (AnonymousClass1AT) r1.AG2.get();
            this.A1L = (C237512w) r1.AAD.get();
            this.A1V = r1.AGf;
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        ContactPickerFragment contactPickerFragment = this.A18;
        if (contactPickerFragment != null) {
            contactPickerFragment.A1S(i);
        }
    }

    public final AnonymousClass2SU A2h(UserJid userJid) {
        AnonymousClass2SU r1 = (AnonymousClass2SU) this.A1U.get(userJid);
        if (r1 != null) {
            return r1;
        }
        C60242wH r12 = new C60242wH(this.A0z, userJid, this);
        this.A1U.put(userJid, r12);
        return r12;
    }

    public final CallInfo A2i() {
        if (AnonymousClass1SF.A0O(((ActivityC13810kN) this).A0C)) {
            Voip.CallState currentCallState = Voip.getCurrentCallState();
            int currentCallLinkState = Voip.getCurrentCallLinkState();
            if (currentCallState == Voip.CallState.LINK && currentCallLinkState != 4) {
                CallLinkInfo callLinkInfo = Voip.getCallLinkInfo();
                if (callLinkInfo != null) {
                    return CallInfo.convertCallLinkInfoToCallInfo(callLinkInfo);
                }
                return null;
            }
        }
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo == null) {
            return null;
        }
        String str = this.A1T;
        if (str == null || !str.equals(callInfo.callWaitingInfo.A04)) {
            return callInfo;
        }
        return CallInfo.convertCallWaitingInfoToCallInfo(callInfo);
    }

    public final Voip.CallState A2j(CallInfo callInfo) {
        Voip.CallState callState = callInfo.callState;
        if (Voip.A0A(callState) && this.A1X) {
            return Voip.CallState.ACCEPT_SENT;
        }
        if (!callInfo.hasOutgoingParticipantInActiveOneToOneCall()) {
            return callState;
        }
        AnonymousClass1S6 defaultPeerInfo = callInfo.getDefaultPeerInfo();
        AnonymousClass009.A05(defaultPeerInfo);
        if (defaultPeerInfo.A01 == 2) {
            return Voip.CallState.CALLING;
        }
        if (callInfo.getDefaultPeerInfo().A01 == 3) {
            return Voip.CallState.PRE_ACCEPT_RECEIVED;
        }
        return callState;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002b, code lost:
        if (r0.A2e == 7) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A2k(com.whatsapp.jid.UserJid r6, int r7) {
        /*
            r5 = this;
            r0 = 2
            r1 = 2131892914(0x7f121ab2, float:1.942059E38)
            if (r7 == r0) goto L_0x001f
            r0 = 17
            if (r7 == r0) goto L_0x002d
            r0 = 4
            r1 = 2131893010(0x7f121b12, float:1.9420784E38)
            if (r7 == r0) goto L_0x001f
            r0 = 5
            if (r7 == r0) goto L_0x0031
            r0 = 9
            r2 = 0
            if (r7 == r0) goto L_0x0024
            r0 = 10
            if (r7 != r0) goto L_0x004b
            r1 = 2131892895(0x7f121a9f, float:1.9420551E38)
        L_0x001f:
            java.lang.String r0 = r5.getString(r1)
            return r0
        L_0x0024:
            X.1Ua r0 = r5.A0v
            if (r0 == 0) goto L_0x004b
            int r1 = r0.A2e
            r0 = 7
            if (r1 != r0) goto L_0x004b
        L_0x002d:
            r1 = 2131893042(0x7f121b32, float:1.942085E38)
            goto L_0x001f
        L_0x0031:
            r4 = 2131890786(0x7f121262, float:1.9416274E38)
            r0 = 1
            java.lang.Object[] r3 = new java.lang.Object[r0]
            r2 = 0
            X.0nY r1 = r5.A14
            X.0nR r0 = r5.A03
            X.0n3 r0 = r0.A0B(r6)
            java.lang.String r0 = r1.A04(r0)
            r3[r2] = r0
            java.lang.String r0 = r5.getString(r4, r3)
            return r0
        L_0x004b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A2k(com.whatsapp.jid.UserJid, int):java.lang.String");
    }

    public final String A2l(CallInfo callInfo, AnonymousClass1S6 r8) {
        int i;
        if (!this.A1j || CallGridViewModel.A00(r8) == 0) {
            String A0A = this.A14.A0A(((AbstractActivityC28171Kz) this).A03.A0B(r8.A06), -1);
            if (!r8.A0F && callInfo.callState == Voip.CallState.ACTIVE) {
                int i2 = r8.A04;
                if (i2 == 3) {
                    return getString(R.string.voip_requested_upgrade_to_video_new, A0A);
                }
                if (callInfo.isGroupCall()) {
                    int i3 = r8.A01;
                    i = R.string.calling;
                    if (i3 != 2) {
                        if (i3 == 3) {
                            i = R.string.ringing;
                        }
                    }
                    return getString(i);
                }
                AnonymousClass1S6 r0 = callInfo.self;
                if ((r0 != null && r0.A09) || r8.A09) {
                    i = R.string.voip_on_hold;
                } else if (r8.A0D) {
                    i = R.string.voip_reconnecting;
                } else if (callInfo.videoEnabled && i2 != 6 && !r8.A0K && i2 != 2) {
                    i = R.string.voip_connecting;
                }
                return getString(i);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r8.A0C != false) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r9 == false) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        r1 = com.whatsapp.R.string.voip_peer_muted;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0078, code lost:
        if (r7.getConnectedParticipantsCount() == 2) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x007b, code lost:
        r0 = com.whatsapp.R.string.voip_pip_peer_muted;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A2m(com.whatsapp.voipcalling.CallInfo r7, X.AnonymousClass1S6 r8, boolean r9) {
        /*
            r6 = this;
            boolean r0 = r6.A1j
            r5 = 0
            if (r0 == 0) goto L_0x000c
            int r0 = com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel.A00(r8)
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            return r5
        L_0x000c:
            com.whatsapp.jid.UserJid r1 = r8.A06
            X.0nY r2 = r6.A14
            X.0nR r0 = r6.A03
            X.0n3 r1 = r0.A0B(r1)
            r0 = -1
            java.lang.String r4 = r2.A0A(r1, r0)
            boolean r0 = r7.videoEnabled
            r3 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0033
            boolean r0 = r8.A0C
            if (r0 == 0) goto L_0x000b
        L_0x0025:
            if (r9 == 0) goto L_0x007b
            r1 = 2131893023(0x7f121b1f, float:1.942081E38)
        L_0x002a:
            java.lang.Object[] r0 = new java.lang.Object[r2]
            r0[r3] = r4
            java.lang.String r0 = r6.getString(r1, r0)
            return r0
        L_0x0033:
            int r1 = r8.A04
            r0 = 6
            if (r1 != r0) goto L_0x004e
            boolean r0 = r8.A0C
            if (r0 == 0) goto L_0x0048
            boolean r0 = r7.isGroupCall()
            if (r0 != 0) goto L_0x0048
            if (r9 == 0) goto L_0x0094
            r1 = 2131893024(0x7f121b20, float:1.9420813E38)
            goto L_0x002a
        L_0x0048:
            if (r9 == 0) goto L_0x007f
            r1 = 2131893029(0x7f121b25, float:1.9420823E38)
            goto L_0x002a
        L_0x004e:
            r0 = 2
            if (r1 != r0) goto L_0x0067
            boolean r0 = r8.A0C
            if (r0 == 0) goto L_0x0061
            boolean r0 = r7.isGroupCall()
            if (r0 != 0) goto L_0x0061
            if (r9 == 0) goto L_0x0083
            r1 = 2131893025(0x7f121b21, float:1.9420815E38)
            goto L_0x002a
        L_0x0061:
            if (r9 == 0) goto L_0x0087
            r1 = 2131893028(0x7f121b24, float:1.942082E38)
            goto L_0x002a
        L_0x0067:
            boolean r0 = r8.A0I
            if (r0 != 0) goto L_0x008b
            boolean r0 = r8.A0H
            if (r0 != 0) goto L_0x008b
            boolean r0 = r8.A0C
            if (r0 == 0) goto L_0x000b
            int r1 = r7.getConnectedParticipantsCount()
            r0 = 2
            if (r1 != r0) goto L_0x000b
            goto L_0x0025
        L_0x007b:
            r0 = 2131893031(0x7f121b27, float:1.9420827E38)
            goto L_0x0097
        L_0x007f:
            r0 = 2131893034(0x7f121b2a, float:1.9420833E38)
            goto L_0x0097
        L_0x0083:
            r0 = 2131893026(0x7f121b22, float:1.9420817E38)
            goto L_0x0097
        L_0x0087:
            r0 = 2131893033(0x7f121b29, float:1.9420831E38)
            goto L_0x0097
        L_0x008b:
            r0 = 2131891042(0x7f121362, float:1.9416793E38)
            if (r9 == 0) goto L_0x0097
            r0 = 2131892731(0x7f1219fb, float:1.9420219E38)
            goto L_0x0097
        L_0x0094:
            r0 = 2131893032(0x7f121b28, float:1.942083E38)
        L_0x0097:
            java.lang.String r0 = r6.getString(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A2m(com.whatsapp.voipcalling.CallInfo, X.1S6, boolean):java.lang.String");
    }

    public void A2n() {
        DialogFragment dialogFragment = this.A0e;
        if (dialogFragment != null) {
            dialogFragment.A1C();
            this.A0e = null;
            this.A1S = null;
        }
        DialogFragment dialogFragment2 = (DialogFragment) A0V().A0A(VoipErrorDialogFragment.class.getName());
        if (dialogFragment2 != null) {
            dialogFragment2.A1C();
        }
    }

    public void A2o() {
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/showCallFailedMessage");
        sb.append(this.A1S);
        Log.i(sb.toString());
        if (this.A1S != null) {
            A2n();
            DialogFragment A00 = MessageDialogFragment.A00(this.A1S);
            this.A0e = A00;
            A00.A1F(A0V(), null);
            return;
        }
        AnonymousClass009.A07("call failed message not defined");
    }

    public final void A2p() {
        this.A0Y.setText("");
        this.A0N.setVisibility(8);
    }

    public final void A2q() {
        boolean z = false;
        if (this.A18 != null) {
            z = true;
        }
        AnonymousClass009.A0A("contact picker fragment should not be null", z);
        ContactPickerFragment contactPickerFragment = this.A18;
        if (contactPickerFragment != null) {
            C48232Fc r0 = contactPickerFragment.A0M;
            if (r0 != null) {
                r0.A04(false);
            }
            this.A18 = null;
            A2v();
            A2M("VoipContactPickerDialogFragment");
        }
    }

    public final void A2r() {
        AnonymousClass01E A0A = A0V().A0A("permission_request");
        if (A0A != null) {
            C004902f r0 = new C004902f(A0V());
            r0.A05(A0A);
            r0.A03();
        }
    }

    public final void A2s() {
        if (this.A0I != null) {
            Log.i("voip/VoipActivityV2/hideAnswerCallView");
            View view = this.A0I;
            if (view != null) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration(125);
                alphaAnimation.setStartOffset((long) 0);
                alphaAnimation.setAnimationListener(new C83343x5(view));
                view.startAnimation(alphaAnimation);
            }
        }
        if (!this.A1i) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.A0P.getLayoutParams();
            if (this.A1N != null && layoutParams.bottomMargin == 0) {
                layoutParams.bottomMargin = this.A02 - getResources().getDimensionPixelSize(R.dimen.call_control_bottom_sheet_rounded_corner_radius);
                this.A0P.setLayoutParams(layoutParams);
            }
        }
    }

    public final void A2t() {
        C64523Fw r1;
        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = this.A1N;
        if (voipCallControlBottomSheetV2 != null && (r1 = voipCallControlBottomSheetV2.A0F) != null && r1.A0F.A0J) {
            r1.A02(5);
            voipCallControlBottomSheetV2.A1L();
        }
    }

    public final void A2u() {
        Log.i("voip/VoipActivityV2/hideInCallControls");
        this.A1n = false;
        this.A0R.setVisibility(8);
        this.A0R.setTranslationY(0.0f);
        this.A0N.setVisibility(8);
        A2M("CallControlSheet");
        VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
        for (int i = 0; i < videoCallParticipantViewLayout.A01; i++) {
            videoCallParticipantViewLayout.A01(i).A01();
            videoCallParticipantViewLayout.A01(i).A07(false, false);
        }
        View findViewById = findViewById(R.id.debug_views);
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
    }

    public final void A2v() {
        if (this.A1h) {
            ContactPickerFragment contactPickerFragment = this.A18;
            if (contactPickerFragment == null || ((AnonymousClass01E) contactPickerFragment).A0g) {
                ((ActivityC13790kL) this).A0D.A01(this.A0Q);
            }
        }
    }

    public final void A2w() {
        Dialog dialog;
        if (this.A18 != null) {
            Log.i("contact picker is already shown, ignore new events");
            return;
        }
        CallInfo A2i = A2i();
        if (A2i != null) {
            Map map = A2i.participants;
            KeyguardManager A07 = ((ActivityC13810kN) this).A08.A07();
            if (A07 != null && (A07.isKeyguardLocked() || A07.inKeyguardRestrictedInputMode())) {
                getWindow().clearFlags(524288);
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = this.A1N;
                if (!(voipCallControlBottomSheetV2 == null || (dialog = ((DialogFragment) voipCallControlBottomSheetV2).A03) == null || dialog.getWindow() == null)) {
                    ((DialogFragment) voipCallControlBottomSheetV2).A03.getWindow().clearFlags(524288);
                }
            }
            VoipContactPickerDialogFragment A00 = VoipContactPickerDialogFragment.A00(map, A2i.videoEnabled);
            this.A18 = A00.A00;
            Adl(A00, "VoipContactPickerDialogFragment");
        }
    }

    public final void A2x() {
        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = this.A1N;
        if (voipCallControlBottomSheetV2 != null && voipCallControlBottomSheetV2.A0c()) {
            voipCallControlBottomSheetV2.A1C();
        }
        this.A1N = null;
    }

    public final void A2y() {
        int i;
        C50102Ob r0;
        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2;
        CallInfo A2i = A2i();
        if (A2i == null) {
            return;
        }
        if (A2i.groupJid == null || (voipCallControlBottomSheetV2 = this.A1N) == null) {
            if (((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1O) == A2i.participants.size()) {
                i = 4;
                r0 = new C50102Ob();
            } else if (A2i.isCallFull()) {
                i = 5;
                r0 = new C852241s(this);
            } else {
                A2w();
                return;
            }
            VoipErrorDialogFragment.A01(r0, i).A1F(A0V(), null);
            return;
        }
        C64523Fw r1 = voipCallControlBottomSheetV2.A0F;
        if (r1 != null) {
            r1.A02(3);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2z() {
        /*
            r4 = this;
            com.whatsapp.voipcalling.CallInfo r2 = r4.A2i()
            if (r2 == 0) goto L_0x0048
            com.whatsapp.voipcalling.Voip$CallState r1 = r2.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.NONE
            if (r1 == r0) goto L_0x0048
            boolean r0 = r4.A1n
            if (r0 == 0) goto L_0x0048
            boolean r0 = r4.A1l
            if (r0 == 0) goto L_0x0048
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACTIVE
            if (r1 != r0) goto L_0x0048
            boolean r0 = r2.videoEnabled
            if (r0 == 0) goto L_0x0048
            java.util.Map r0 = r2.participants
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0026:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0048
            java.lang.Object r1 = r2.next()
            X.1S6 r1 = (X.AnonymousClass1S6) r1
            boolean r0 = r1.A0K
            if (r0 != 0) goto L_0x003b
            int r1 = r1.A04
            r0 = 6
            if (r1 != r0) goto L_0x0026
        L_0x003b:
            android.os.Handler r0 = r4.A0E
            r3 = 3
            r0.removeMessages(r3)
            android.os.Handler r2 = r4.A0E
            r0 = 5000(0x1388, double:2.4703E-320)
            r2.sendEmptyMessageDelayed(r3, r0)
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A2z():void");
    }

    public final void A30() {
        if (this.A1N == null) {
            VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
            boolean z = false;
            if (!(videoCallParticipantViewLayout == null || videoCallParticipantViewLayout.getPaddingBottom() == 0)) {
                this.A0z.setPadding(0, 0, 0, 0);
            }
            CallInfo A2i = A2i();
            if (A2i != null && A2i.videoEnabled) {
                z = true;
            }
            this.A1N = VoipCallControlBottomSheetV2.A01(z);
            this.A0R.setTranslationY(0.0f);
            if (A2i != null && !A2i.isPeerRequestingUpgrade()) {
                if (A2i.isGroupCall() || !Voip.A0A(A2i.callState)) {
                    A31();
                    VoipInCallNotifBanner voipInCallNotifBanner = this.A0o;
                    if (voipInCallNotifBanner != null) {
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) voipInCallNotifBanner.getLayoutParams();
                        Log.i("VoipCallNewParticipantBanner/resetBannerYPosition");
                        marginLayoutParams.bottomMargin = (int) voipInCallNotifBanner.getResources().getDimension(R.dimen.call_new_participant_banner_bottom_margin);
                        voipInCallNotifBanner.setLayoutParams(marginLayoutParams);
                    }
                    A2s();
                }
            }
        }
    }

    public final void A31() {
        if (this.A1N != null && !isFinishing()) {
            this.A1N.A1F(A0V(), "CallControlSheet");
        }
    }

    public final void A32() {
        Log.i("voip/VoipActivityV2/showInCallControls");
        this.A1n = true;
        if (this.A1N != null) {
            A31();
            A37();
            VoipInCallNotifBanner voipInCallNotifBanner = this.A0o;
            if (voipInCallNotifBanner != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) voipInCallNotifBanner.getLayoutParams();
                Log.i("VoipCallNewParticipantBanner/resetBannerYPosition");
                marginLayoutParams.bottomMargin = (int) voipInCallNotifBanner.getResources().getDimension(R.dimen.call_new_participant_banner_bottom_margin);
                voipInCallNotifBanner.setLayoutParams(marginLayoutParams);
            }
        }
        this.A0R.setVisibility(0);
        this.A0R.setTranslationY(0.0f);
        if (this.A0Y.length() > 0) {
            this.A0N.setVisibility(0);
        }
        View findViewById = findViewById(R.id.debug_views);
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
    }

    public final void A33() {
        if (this.A1j) {
            CallGridViewModel callGridViewModel = this.A0p;
            AnonymousClass009.A05(callGridViewModel);
            callGridViewModel.A0C.A0A(null);
            return;
        }
        this.A1Q.removeCameraErrorListener(this.A1u);
        this.A0E.removeMessages(6);
        Voip.setVideoPreviewPort(null, this.A1T);
        Voip.setVideoPreviewSize(0, 0);
    }

    public final void A34() {
        Log.i("VoipActivityV2 vm unbindService");
        try {
            this.A0w.A00.obtainMessage(5, this).sendToTarget();
        } catch (IllegalArgumentException e) {
            Log.e(e);
        }
        C29631Ua r0 = this.A0v;
        if (r0 != null) {
            r0.A0t(this);
            this.A0v.A0S = null;
            this.A0v = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0200, code lost:
        if (r6.isCallLinkLobbyState != false) goto L_0x0202;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d9, code lost:
        if (r6.isInLonelyState() != false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e7, code lost:
        if (r1 == 1) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0100, code lost:
        if (r6.isInLonelyState() != false) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x013e, code lost:
        if (com.whatsapp.voipcalling.Voip.A0A(r6.callState) == false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        if (r11.A1X != false) goto L_0x0020;
     */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x025b  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x016e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A35() {
        /*
        // Method dump skipped, instructions count: 679
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A35():void");
    }

    public final void A36() {
        TextView textView = (TextView) findViewById(R.id.debug_tx_network_conditioner_param_text_view);
        if (textView != null) {
            if (Voip.isTxNetworkConditionerOn()) {
                StringBuilder sb = new StringBuilder("Tx network conditioner is ON !!!\n");
                sb.append(Voip.getCurrentTxNetworkConditionerParameters());
                textView.setText(sb.toString());
                textView.setVisibility(0);
            } else {
                textView.setVisibility(8);
            }
        }
        TextView textView2 = (TextView) findViewById(R.id.debug_rx_network_conditioner_param_text_view);
        if (textView2 == null) {
            return;
        }
        if (Voip.isRxNetworkConditionerOn()) {
            StringBuilder sb2 = new StringBuilder("Rx network conditioner is ON !!!\n");
            sb2.append(Voip.getCurrentRxNetworkConditionerParameters());
            textView2.setText(sb2.toString());
            textView2.setVisibility(0);
            return;
        }
        textView2.setVisibility(8);
    }

    public final void A37() {
        CallInfo A2i = A2i();
        if (A2i != null && A2i.videoEnabled) {
            if (this.A1j) {
                A38();
                return;
            }
            for (AnonymousClass2SU r2 : this.A1U.values()) {
                VideoCallParticipantView videoCallParticipantView = r2.A00;
                if (videoCallParticipantView != null && videoCallParticipantView.A03 == 1) {
                    A38();
                    AnonymousClass1S6 infoByJid = A2i.getInfoByJid(r2.A04);
                    r2.A09(infoByJid);
                    r2.A08(A2i, infoByJid);
                    return;
                }
            }
        }
    }

    public final void A38() {
        int i;
        if (this.A1j) {
            this.A0n.A00();
            return;
        }
        int i2 = this.A04;
        VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
        float f = 0.225f;
        if (this.A1g) {
            f = 0.4f;
        }
        videoCallParticipantViewLayout.A00 = f;
        boolean z = this.A1n;
        if (!z || this.A1N == null) {
            i = 0;
        } else {
            i = this.A02;
        }
        videoCallParticipantViewLayout.A02 = i;
        if (!z) {
            i2 = 0;
        }
        videoCallParticipantViewLayout.A04 = i2;
    }

    public final void A39(float f) {
        CallGrid callGrid;
        if (f <= 0.0f && (callGrid = this.A0l) != null && callGrid.getHeight() != 0) {
            this.A00 = f;
            if (this.A1f) {
                f = -1.0f;
            }
            float f2 = f + 1.0f;
            float height = 1.0f - ((((float) (this.A07 + this.A02)) / ((float) this.A0l.getHeight())) * f2);
            this.A0l.setScaleY(height);
            this.A0l.setScaleX(height);
            this.A0l.setTranslationY(f2 * (((float) (this.A07 - this.A02)) / 2.0f));
        }
    }

    public final void A3A(int i) {
        int i2;
        Log.i("voip/VoipActivityV2/call/accept");
        CallInfo A2i = A2i();
        if (A2i != null && A2i.callState != Voip.CallState.NONE) {
            if (!this.A1X) {
                A2s();
                A31();
                if (!AnonymousClass1SF.A0P(((ActivityC13810kN) this).A0C)) {
                    CallDetailsLayout callDetailsLayout = this.A0j;
                    callDetailsLayout.A04(A2i.callState, A2i.videoEnabled, callDetailsLayout.A09(A2i));
                }
            } else {
                Log.w("callAccepted is true when calling acceptCall()");
            }
            boolean z = true;
            this.A1X = true;
            if (this.A0v != null) {
                if (Build.VERSION.SDK_INT < 23) {
                    boolean z2 = false;
                    if (this.A1A.A00.A00.checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") == 0) {
                        z2 = true;
                    }
                    boolean z3 = !z2;
                    if (!A2i.videoEnabled || this.A1A.A00.A00.checkCallingOrSelfPermission("android.permission.CAMERA") == 0) {
                        z = false;
                    }
                    if (z3 || z) {
                        StringBuilder sb = new StringBuilder("voip/service/accept noRecordPermission = ");
                        sb.append(z3);
                        sb.append(", noCameraPermission = ");
                        sb.append(z);
                        Log.w(sb.toString());
                        if (z) {
                            i2 = R.string.can_not_start_video_call_without_camera_permission;
                            if (z3) {
                                i2 = R.string.can_not_start_video_call_without_mic_and_camera_permission;
                            }
                        } else {
                            i2 = R.string.can_not_start_voip_call_without_record_permission;
                        }
                        this.A0v.A0Y(29, getString(i2));
                        return;
                    }
                }
                this.A0v.A0N();
                UserJid peerJid = A2i.getPeerJid();
                AnonymousClass009.A05(peerJid);
                boolean z4 = A2i.videoEnabled;
                int i3 = -1;
                if (A2i.isPeerRequestingUpgrade()) {
                    i3 = 2;
                }
                if (!A3U(peerJid, i3, z4)) {
                    return;
                }
                if (Voip.A0A(A2i.callState)) {
                    this.A0v.A0y(this.A1T, i);
                } else if (A2i.isPeerRequestingUpgrade()) {
                    C29631Ua r2 = this.A0v;
                    r2.A0X();
                    r2.A2T.setRequestedCamera2SupportLevel(r2.A2R.A02());
                    r2.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(3));
                }
            } else {
                Log.e("voip/VoipActivityV2/call/accept voiceService is null");
            }
        }
    }

    public final void A3B(int i) {
        Log.i("voip/VoipActivityV2/call/reject");
        CallInfo A2i = A2i();
        if (A2i != null && A2i.callState != Voip.CallState.NONE) {
            A2s();
            C29631Ua r2 = this.A0v;
            if (r2 == null) {
                return;
            }
            if (Voip.A0A(A2i.callState)) {
                r2.A0z(A2i.callId, null, i);
            } else if (A2i.isPeerRequestingUpgrade()) {
                this.A0v.A0x.execute(new RunnableBRunnable0Shape0S0001000_I0(0, 2));
            }
        }
    }

    public final void A3C(int i, int i2) {
        VideoCallParticipantView videoCallParticipantView;
        CallGridViewModel callGridViewModel;
        AnonymousClass1S6 r0;
        VoipCallFooter voipCallFooter;
        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = this.A1N;
        if (!(voipCallControlBottomSheetV2 == null || (voipCallFooter = voipCallControlBottomSheetV2.A0M) == null)) {
            float f = (float) i2;
            voipCallFooter.A03.setRotation(f);
            voipCallFooter.A06.setRotation(f);
            voipCallFooter.A05.setRotation(f);
            voipCallFooter.A07.setRotation(f);
            voipCallFooter.A04.setRotation(f);
        }
        float f2 = (float) i2;
        this.A0S.setRotation(f2);
        this.A0T.setRotation(f2);
        this.A0N.setRotation(f2);
        if (!this.A1j || (callGridViewModel = this.A0p) == null) {
            VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
            for (int i3 = 0; i3 < videoCallParticipantViewLayout.A01; i3++) {
                VideoCallParticipantView A01 = videoCallParticipantViewLayout.A01(i3);
                A01.A0G.setRotation(f2);
                A01.A0E.setRotation(f2);
                A01.A0C.setRotation(f2);
            }
        } else {
            callGridViewModel.A00 = i;
            if (!(callGridViewModel.A02 == null || (r0 = (AnonymousClass1S6) callGridViewModel.A0C.A05().A00.get(callGridViewModel.A02)) == null)) {
                callGridViewModel.A0A(r0);
            }
            LinkedHashMap linkedHashMap = callGridViewModel.A0P;
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                C64363Fg r1 = (C64363Fg) entry.getValue();
                new Pair(-1, -1);
                UserJid userJid = r1.A0S;
                C15370n3 r02 = r1.A0R;
                Pair pair = r1.A08;
                boolean z = r1.A0G;
                boolean z2 = r1.A0E;
                boolean z3 = r1.A0A;
                boolean z4 = r1.A0H;
                boolean z5 = r1.A0F;
                int i4 = r1.A01;
                boolean z6 = r1.A0B;
                int i5 = r1.A00;
                boolean z7 = r1.A0N;
                boolean z8 = r1.A0K;
                boolean z9 = r1.A0J;
                int i6 = r1.A05;
                boolean z10 = r1.A0M;
                boolean z11 = r1.A0O;
                boolean z12 = r1.A09;
                Bitmap bitmap = r1.A07;
                boolean z13 = r1.A0L;
                boolean z14 = r1.A0D;
                boolean z15 = r1.A0P;
                int i7 = r1.A02;
                boolean z16 = r1.A0I;
                int i8 = r1.A04;
                boolean z17 = r1.A0Q;
                int i9 = r1.A06;
                boolean z18 = r1.A0C;
                C64363Fg r12 = new C64363Fg(r02, userJid);
                r12.A08 = pair;
                r12.A0G = z;
                r12.A0E = z2;
                r12.A0A = z3;
                r12.A0H = z4;
                r12.A0F = z5;
                r12.A01 = i4;
                r12.A0B = z6;
                r12.A00 = i5;
                r12.A0N = z7;
                r12.A0K = z8;
                r12.A0J = z9;
                r12.A05 = i6;
                r12.A0M = z10;
                r12.A0O = z11;
                r12.A09 = z12;
                r12.A03 = callGridViewModel.A00 * -90;
                r12.A07 = bitmap;
                r12.A0L = z13;
                r12.A0D = z14;
                r12.A0P = z15;
                r12.A02 = i7;
                r12.A0I = z16;
                r12.A04 = i8;
                r12.A0Q = z17;
                r12.A06 = i9;
                r12.A0C = z18;
                linkedHashMap.put(entry.getKey(), r12);
            }
            callGridViewModel.A06();
        }
        MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A0x;
        if (!(maximizedParticipantVideoDialogFragment == null || (videoCallParticipantView = maximizedParticipantVideoDialogFragment.A07) == null)) {
            videoCallParticipantView.A0G.setRotation(f2);
            videoCallParticipantView.A0E.setRotation(f2);
            videoCallParticipantView.A0C.setRotation(f2);
        }
    }

    public final void A3D(Intent intent, CallInfo callInfo) {
        AnonymousClass009.A0A("should only be called if intent action is ACTION_ACCEPT_INCOMING_CALL", "com.whatsapp.intent.action.ACCEPT_CALL".equals(intent.getAction()));
        String stringExtra = intent.getStringExtra("call_id");
        if (Voip.A09(callInfo)) {
            if (!callInfo.callId.equals(stringExtra)) {
                AnonymousClass1S7 r1 = callInfo.callWaitingInfo;
                if (r1.A01 != 1 || !r1.A04.equals(stringExtra)) {
                    return;
                }
            }
            this.A1X = true;
            this.A05 = 0;
            if (this.A0v != null) {
                A3A(intent.getIntExtra("call_ui_action", 0));
            } else {
                this.A1W = true;
            }
        }
    }

    public final void A3E(Intent intent, CallInfo callInfo) {
        AnonymousClass009.A0A("should only be called if intent action is ACTION_SHOW_ALERT_MESSAGE_IN_ACTIVE_CALL", "com.whatsapp.intent.action.SHOW_ALERT_MESSAGE_IN_ACTIVE_CALL".equals(intent.getAction()));
        if (callInfo.callState == Voip.CallState.ACTIVE) {
            NonActivityDismissDialogFragment.A00(intent.getStringExtra("alertMessage"), false).A1F(A0V(), "VoipAlertDialog");
            return;
        }
        Log.i("voip/VoipActivityV2/new-intent call is gone, ignore the request to show alert message");
        finish();
    }

    public final void A3F(View.OnClickListener onClickListener, CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Drawable drawable;
        this.A0Y.setText(charSequence);
        this.A0N.setVisibility(0);
        TextView textView = this.A0Y;
        Drawable drawable2 = null;
        if (z) {
            drawable = this.A0D;
        } else {
            drawable = null;
        }
        textView.setCompoundDrawables(null, drawable, null, null);
        if (charSequence2 == null) {
            View view = this.A0N;
            if (z) {
                drawable2 = this.A0C;
            }
            view.setBackground(drawable2);
            this.A0M.setVisibility(8);
            this.A0X.setVisibility(8);
            return;
        }
        boolean z2 = false;
        if (onClickListener != null) {
            z2 = true;
        }
        AnonymousClass009.A0A("buttonOnClickListener must be set together with buttonText", z2);
        this.A0N.setBackground(this.A0B);
        this.A0M.setVisibility(0);
        this.A0X.setVisibility(0);
        this.A0X.setText(charSequence2);
        this.A0X.setOnClickListener(new ViewOnClickCListenerShape1S0200000_I0_1(this, 2, onClickListener));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r6.A00 != 2) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3G(X.AnonymousClass3HT r6) {
        /*
            r5 = this;
            r4 = 1
            r1 = 0
            if (r6 == 0) goto L_0x000a
            int r3 = r6.A00
            r0 = 2
            r2 = 1
            if (r3 == r0) goto L_0x000b
        L_0x000a:
            r2 = 0
        L_0x000b:
            r5.A1f = r2
            X.3DO r0 = r5.A0n
            if (r0 == 0) goto L_0x0014
            r4 = r4 ^ r2
            r0.A02 = r4
        L_0x0014:
            if (r6 == 0) goto L_0x0058
            android.view.View r0 = r5.A0L
            if (r0 == 0) goto L_0x0058
            android.view.ViewParent r4 = r0.getParent()
            android.view.View r4 = (android.view.View) r4
            android.content.res.Resources r2 = r5.getResources()
            r0 = 2131165415(0x7f0700e7, float:1.7945046E38)
            int r3 = r2.getDimensionPixelSize(r0)
            android.view.View r0 = r5.A0L
            android.view.ViewGroup$LayoutParams r2 = r0.getLayoutParams()
            android.view.ViewGroup$MarginLayoutParams r2 = (android.view.ViewGroup.MarginLayoutParams) r2
            boolean r0 = r5.A1f
            if (r0 == 0) goto L_0x0041
            int r1 = r4.getHeight()
            android.graphics.Rect r0 = r6.A02
            int r0 = r0.top
            int r1 = r1 - r0
            int r1 = r1 + r3
        L_0x0041:
            r2.bottomMargin = r1
            android.view.View r0 = r5.A0L
            r0.setLayoutParams(r2)
            boolean r0 = r5.A1Y
            if (r0 == 0) goto L_0x0058
            android.view.View r2 = r5.A0L
            r1 = 39
            com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13 r0 = new com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13
            r0.<init>(r5, r1)
            r2.post(r0)
        L_0x0058:
            com.whatsapp.voipcalling.CallInfo r1 = r5.A2i()
            boolean r0 = r5.A1f
            r5.A3S(r0)
            r5.A3M(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3G(X.3HT):void");
    }

    public final void A3H(AnonymousClass2SU r6, AnonymousClass1S6 r7) {
        if (this.A1j) {
            CallGridViewModel callGridViewModel = this.A0p;
            AnonymousClass009.A05(callGridViewModel);
            C48782Ht r1 = callGridViewModel.A0C;
            VideoPort videoPort = r1.A05;
            if (videoPort == null) {
                Log.i("voip/CallDatasource/startCameraPreview selfVideoPort not set");
            } else {
                r1.A0A(videoPort);
            }
        } else {
            this.A0E.removeMessages(6);
            CallInfo A2i = A2i();
            if (A2i == null) {
                return;
            }
            if ((A2i.callState != Voip.CallState.RECEIVED_CALL || !this.A1o) && this.A1l && this.A1A.A02("android.permission.CAMERA") == 0) {
                StringBuilder sb = new StringBuilder("voip/VoipActivityV2/startCameraPreview/setting preview surface to presenter ");
                sb.append(r6.A07);
                sb.append(" retry: ");
                int i = this.A05;
                this.A05 = i + 1;
                sb.append(i);
                Log.i(sb.toString());
                if (Voip.setVideoPreviewPort(r6.A01, A2i.callId) == 0) {
                    VideoPort videoPort2 = r6.A01;
                    Point point = new Point(0, 0);
                    if (videoPort2 != null) {
                        point = videoPort2.getWindowSize();
                    }
                    Voip.setVideoPreviewSize(point.x, point.y);
                    this.A05 = 0;
                    r6.A09(r7);
                    this.A1Q.addCameraErrorListener(this.A1u);
                } else if (this.A05 >= 10) {
                    C29631Ua r2 = this.A0v;
                    if (r2 != null) {
                        r2.A0m(null, null, 15);
                    }
                } else {
                    this.A0E.sendEmptyMessageDelayed(6, 500);
                }
            }
        }
    }

    public final void A3I(CallInfo callInfo) {
        AnonymousClass1S6 r0;
        C29631Ua r1 = this.A0v;
        if (r1 != null && r1.A14(callInfo.callId) && callInfo.callState == Voip.CallState.ACTIVE && (r0 = callInfo.self) != null && r0.A09) {
            Log.i("voip/VoipActivityV2/checkToShowResumeCallButton");
            A3F(this.A0H, getString(R.string.voip_on_hold), getString(R.string.voip_resume), false);
        }
    }

    public final void A3J(CallInfo callInfo) {
        boolean z = false;
        if (callInfo.videoEnabled) {
            z = true;
        }
        AnonymousClass009.A0A("can be called only for video call", z);
        for (AnonymousClass2SU r2 : this.A1U.values()) {
            AnonymousClass1S6 infoByJid = callInfo.getInfoByJid(r2.A04);
            r2.A09(infoByJid);
            r2.A08(callInfo, infoByJid);
            MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A0x;
            if (maximizedParticipantVideoDialogFragment != null) {
                maximizedParticipantVideoDialogFragment.A1K(callInfo, infoByJid);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x015b, code lost:
        if (r0 != false) goto L_0x015d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3K(com.whatsapp.voipcalling.CallInfo r11) {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3K(com.whatsapp.voipcalling.CallInfo):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r10.A1Y != false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3L(com.whatsapp.voipcalling.CallInfo r11) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3L(com.whatsapp.voipcalling.CallInfo):void");
    }

    public final void A3M(CallInfo callInfo) {
        if (callInfo != null) {
            boolean z = callInfo.videoEnabled;
            OrientationViewModel orientationViewModel = this.A0s;
            if (z) {
                if (orientationViewModel != null) {
                    orientationViewModel.A07(this);
                }
            } else if (orientationViewModel != null && orientationViewModel.A08()) {
                A3C(0, 0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0257, code lost:
        if (r17.A1N != null) goto L_0x0259;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x02ab, code lost:
        if (r18.isEitherSideRequestingUpgrade() == false) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x048c, code lost:
        if (r8 == false) goto L_0x0342;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00fa, code lost:
        if (r18.isGroupCallEnabled == false) goto L_0x00fc;
     */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02b6  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02d8  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0449  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3N(com.whatsapp.voipcalling.CallInfo r18) {
        /*
        // Method dump skipped, instructions count: 1234
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3N(com.whatsapp.voipcalling.CallInfo):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v3, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0132, code lost:
        if (r4 > 1) goto L_0x026a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x029d  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x02cc  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02de A[LOOP:12: B:157:0x02d8->B:159:0x02de, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3O(com.whatsapp.voipcalling.CallInfo r14) {
        /*
        // Method dump skipped, instructions count: 850
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3O(com.whatsapp.voipcalling.CallInfo):void");
    }

    public final void A3P(String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            setTaskDescription(new ActivityManager.TaskDescription(str));
        }
    }

    public final void A3Q(boolean z) {
        int i = this.A06;
        this.A04 = i;
        this.A03 = 0;
        if (!z || !this.A1j) {
            this.A04 = i + this.A07;
            this.A03 = 0 + ((this.A02 - getResources().getDimensionPixelSize(R.dimen.call_control_bottom_sheet_padding_top)) - (getResources().getDimensionPixelSize(R.dimen.call_grid_border_width_for_speaker) >> 1));
        }
    }

    public final void A3R(boolean z) {
        String string;
        int i;
        ImageButton imageButton = this.A0S;
        if (z) {
            string = getString(R.string.voip_joinable_expand_participant_list_description);
            i = R.string.voip_joinable_expand_participant_list_click_action_description;
        } else {
            string = getString(R.string.voip_joinable_add_participant_description);
            i = R.string.voip_joinable_add_participant_click_action_description;
        }
        AnonymousClass028.A0g(imageButton, new C53572ef(imageButton, string, getString(i), true));
    }

    public final void A3S(boolean z) {
        int i;
        Rect rect;
        C36161jQ r0;
        if (this.A0p != null) {
            boolean z2 = this.A1g;
            Resources resources = getResources();
            if (z2) {
                int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.call_grid_picture_in_picture_margin);
                CallGridViewModel callGridViewModel = this.A0p;
                rect = new Rect(dimensionPixelSize, dimensionPixelSize, 0, 0);
                if (!C29941Vi.A00(callGridViewModel.A01, rect)) {
                    callGridViewModel.A01 = rect;
                    r0 = callGridViewModel.A0K;
                } else {
                    return;
                }
            } else {
                int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.voip_call_grid_margin);
                if (z) {
                    i = 0;
                } else {
                    i = this.A03;
                }
                CallGridViewModel callGridViewModel2 = this.A0p;
                rect = new Rect(dimensionPixelSize2, this.A04, 0, i);
                if (!C29941Vi.A00(callGridViewModel2.A01, rect)) {
                    callGridViewModel2.A01 = rect;
                    r0 = callGridViewModel2.A0K;
                } else {
                    return;
                }
            }
            r0.A0B(rect);
        }
    }

    public final boolean A3T() {
        AppOpsManager A06 = ((ActivityC13810kN) this).A08.A06();
        AnonymousClass009.A05(A06);
        try {
        } catch (SecurityException e) {
            StringBuilder sb = new StringBuilder("voip/VoipActivityV2/isPictureInPictureAllowed");
            sb.append(e);
            Log.w(sb.toString());
        }
        if (A06.checkOp("android:picture_in_picture", Process.myUid(), getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0032, code lost:
        if (r2 != false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r6.A1A.A02("android.permission.CAMERA") == 0) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A3U(com.whatsapp.jid.UserJid r7, int r8, boolean r9) {
        /*
            r6 = this;
            r6.A2r()
            X.0o4 r1 = r6.A1A
            java.lang.String r0 = "android.permission.RECORD_AUDIO"
            int r0 = r1.A02(r0)
            r5 = 1
            r4 = 0
            r3 = 0
            if (r0 == 0) goto L_0x0011
            r3 = 1
        L_0x0011:
            if (r9 == 0) goto L_0x001e
            X.0o4 r1 = r6.A1A
            java.lang.String r0 = "android.permission.CAMERA"
            int r0 = r1.A02(r0)
            r2 = 1
            if (r0 != 0) goto L_0x001f
        L_0x001e:
            r2 = 0
        L_0x001f:
            X.0o4 r0 = r6.A1A
            boolean r1 = r0.A09()
            if (r2 != 0) goto L_0x002c
            if (r3 != 0) goto L_0x002c
            if (r1 != 0) goto L_0x002c
            return r5
        L_0x002c:
            r0 = -1
            if (r8 != r0) goto L_0x0035
            if (r3 != 0) goto L_0x0034
            r8 = 3
            if (r2 == 0) goto L_0x0035
        L_0x0034:
            r8 = 0
        L_0x0035:
            com.whatsapp.calling.views.PermissionDialogFragment r2 = com.whatsapp.calling.views.PermissionDialogFragment.A00(r7, r8, r3, r2, r1)
            X.01F r0 = r6.A0V()
            X.02f r1 = new X.02f
            r1.<init>(r0)
            java.lang.String r0 = "permission_request"
            r1.A09(r2, r0)
            r1.A03()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3U(com.whatsapp.jid.UserJid, int, boolean):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r8.videoEnabled == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0130, code lost:
        if (r3 != null) goto L_0x008f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A3V(com.whatsapp.voipcalling.CallInfo r8) {
        /*
        // Method dump skipped, instructions count: 359
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.A3V(com.whatsapp.voipcalling.CallInfo):boolean");
    }

    public final boolean A3W(CallInfo callInfo) {
        Voip.CallState callState;
        boolean A07 = ((ActivityC13810kN) this).A0C.A07(1071);
        Voip.CallState callState2 = callInfo.callState;
        Voip.CallState callState3 = Voip.CallState.ACTIVE;
        if (A07) {
            if (!(callState2 == callState3 || callState2 == Voip.CallState.CONNECTED_LONELY || callState2 == Voip.CallState.CALLING)) {
                callState = Voip.CallState.PRE_ACCEPT_RECEIVED;
                if (callState2 == callState && !this.A1X) {
                    return false;
                }
            }
            return true;
        }
        if (callState2 != callState3) {
            callState = Voip.CallState.CONNECTED_LONELY;
            if (callState2 == callState) {
            }
        }
        return true;
    }

    @Override // X.AnonymousClass1L2
    public AnonymousClass3UA ADO() {
        AnonymousClass3UA r0 = this.A17;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass2xD r02 = new AnonymousClass2xD(this, this);
        this.A17 = r02;
        return r02;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A01;
    }

    @Override // X.AnonymousClass1L1
    public void ATh(int i) {
        C29631Ua r2;
        String str;
        A2r();
        if (this.A0v != null) {
            int intExtra = getIntent().getIntExtra("call_ui_action", 0);
            if (i == 0) {
                r2 = this.A0v;
                str = this.A1T;
            } else if (i == 1) {
                return;
            } else {
                if (i == 2) {
                    this.A0v.A0x.execute(new RunnableBRunnable0Shape0S0001000_I0(0, 2));
                    return;
                } else if (i != 3) {
                    AnonymousClass009.A0A("Unknown request code", false);
                    return;
                } else {
                    r2 = this.A0v;
                    str = this.A1T;
                    r2.A1B = true;
                }
            }
            r2.A0z(str, null, intExtra);
        }
    }

    @Override // X.AnonymousClass1L1
    public void ATi(String[] strArr, int i) {
        C29631Ua r5;
        boolean z;
        A2r();
        CallInfo A2i = A2i();
        if (Voip.A08(A2i) && (r5 = this.A0v) != null) {
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z = false;
                    break;
                } else if ("android.permission.CAMERA".equals(strArr[i2])) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (i == 0) {
                if (A2i.videoEnabled && z) {
                    Voip.refreshVideoDevice();
                    A3H(this.A10, A2i.self);
                }
                if (Voip.A0A(A2i.callState)) {
                    UserJid peerJid = A2i.getPeerJid();
                    AnonymousClass009.A05(peerJid);
                    if (A3U(peerJid, -1, A2i.videoEnabled)) {
                        this.A0v.A0y(this.A1T, 1);
                    }
                }
            } else if (i == 1) {
                Log.i("voip/VoipActivityV2/onPermissionsGranted switching to video call");
                this.A0v.A0M();
            } else if (i != 2) {
                if (i != 3) {
                    AnonymousClass009.A0A("Unknown request code", false);
                } else if (Voip.A0A(A2i.callState)) {
                    String str = this.A1T;
                    if (!r5.A14(str)) {
                        int A0A = r5.A0A();
                        if (A0A != 0) {
                            StringBuilder sb = new StringBuilder("voip/acceptCall/cellularCallInProgress ");
                            sb.append(A0A);
                            Log.w(sb.toString());
                            r5.A0z(str, "busy", 1);
                            return;
                        }
                        TelephonyManager telephonyManager = r5.A1Q;
                        if (telephonyManager != null) {
                            telephonyManager.listen(r5.A0Q, 32);
                        }
                    }
                    r5.A0y(str, 1);
                }
            } else if (A2i.isPeerRequestingUpgrade()) {
                Voip.refreshVideoDevice();
                A3H(this.A10, A2i.self);
                C29631Ua r2 = this.A0v;
                r2.A0X();
                r2.A2T.setRequestedCamera2SupportLevel(r2.A2R.A02());
                r2.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(3));
            }
        }
    }

    @Override // X.AnonymousClass1L3
    public void AfP(CallInfo callInfo) {
        boolean z;
        Voip.CallState callState;
        VoipCallFooter voipCallFooter;
        int i;
        Voip.CallState A2j = A2j(callInfo);
        if (A2j != Voip.CallState.NONE && !Voip.A0A(A2j)) {
            VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = this.A1N;
            int i2 = 0;
            if (!(voipCallControlBottomSheetV2 == null || (voipCallFooter = voipCallControlBottomSheetV2.A0M) == null)) {
                C29631Ua r0 = this.A0v;
                if (r0 != null) {
                    i = r0.A2P.A00;
                } else {
                    i = 0;
                }
                voipCallFooter.A02(callInfo, i, this.A0g.A08());
            }
            if (this.A0S.getVisibility() == 0) {
                ImageButton imageButton = this.A0S;
                if (callInfo.groupJid != null || (((callState = callInfo.callState) == Voip.CallState.ACTIVE || callState == Voip.CallState.CONNECTED_LONELY) && !callInfo.isEitherSideRequestingUpgrade())) {
                    z = true;
                } else {
                    z = false;
                }
                C65293Iy.A04(imageButton, z);
            }
            ImageButton imageButton2 = this.A0T;
            Voip.CallState callState2 = callInfo.callState;
            if (!(callState2 == Voip.CallState.ACTIVE || callState2 == Voip.CallState.CONNECTED_LONELY || this.A1X) || !callInfo.self.A07) {
                i2 = 8;
            }
            imageButton2.setVisibility(i2);
        }
    }

    @Override // X.AnonymousClass1L4
    public void Afh(UserJid userJid) {
        AnonymousClass009.A01();
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/videoRenderStarted ");
        sb.append(userJid);
        Log.i(sb.toString());
        A2h(userJid).A05();
        A3N(A2i());
        A2z();
    }

    @Override // X.AnonymousClass1L4
    public void Afi(CallInfo callInfo) {
        Object valueOf;
        if (callInfo.callId.equals(this.A1T)) {
            this.A1d = false;
            this.A0E.removeMessages(10);
            if (callInfo.callState != Voip.CallState.NONE && !callInfo.callEnding) {
                StringBuilder sb = new StringBuilder("voip/VoipActivityV2/videoStateChanged self_video_state: ");
                sb.append(callInfo.self.A04);
                sb.append(", peer_video_state: ");
                if (callInfo.getDefaultPeerInfo() == null) {
                    valueOf = "null";
                } else {
                    valueOf = Integer.valueOf(callInfo.getDefaultPeerInfo().A04);
                }
                sb.append(valueOf);
                Log.i(sb.toString());
                this.A0j.clearAnimation();
                if (callInfo.isEitherSideRequestingUpgrade()) {
                    this.A1X = false;
                }
                A3M(callInfo);
                A3N(callInfo);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:93:0x018d, code lost:
        if (r1 == 1) goto L_0x018f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0283  */
    @Override // X.AnonymousClass1L4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callStateChanged(com.whatsapp.voipcalling.Voip.CallState r12, com.whatsapp.voipcalling.CallInfo r13) {
        /*
        // Method dump skipped, instructions count: 776
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.callStateChanged(com.whatsapp.voipcalling.Voip$CallState, com.whatsapp.voipcalling.CallInfo):void");
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        C29631Ua r1;
        if (Build.VERSION.SDK_INT >= 21 || (r1 = this.A0v) == null || r1.A2P.A00 == 1 || !r1.A13) {
            return super.dispatchTouchEvent(motionEvent);
        }
        Log.i("voip/VoipActivityV2/dispatchTouchEvent Touch event ignored");
        return true;
    }

    public void hideView(View view) {
        view.clearAnimation();
        view.setVisibility(4);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        ContactPickerFragment contactPickerFragment = this.A18;
        if (contactPickerFragment == null) {
            CallGridViewModel callGridViewModel = this.A0p;
            if (callGridViewModel != null) {
                AnonymousClass016 r1 = callGridViewModel.A07;
                if (r1.A01() != null) {
                    callGridViewModel.A08((UserJid) r1.A01());
                    return;
                }
            }
            if (Build.VERSION.SDK_INT < 26 || !((ActivityC13810kN) this).A08.A0S("android.software.picture_in_picture") || !A3T() || !A3V(A2i())) {
                super.onBackPressed();
            }
        } else if (!contactPickerFragment.A1l()) {
            A2q();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.A0p != null && ((Boolean) this.A1V.get()).booleanValue()) {
            CallGridViewModel callGridViewModel = this.A0p;
            boolean z = false;
            if (configuration.orientation == 2) {
                z = true;
            }
            C36161jQ r1 = callGridViewModel.A0I;
            if (((Boolean) r1.A01()).booleanValue() != z) {
                r1.A0B(Boolean.valueOf(z));
                callGridViewModel.A07(callGridViewModel.A0C.A05(), true);
            }
        }
        OrientationViewModel orientationViewModel = this.A0s;
        if (orientationViewModel != null) {
            orientationViewModel.A02 = ((Boolean) orientationViewModel.A08.get()).booleanValue();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0270, code lost:
        if (r2.equalsIgnoreCase("OP4A57") == false) goto L_0x0272;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02df, code lost:
        if (r2.startsWith("PD1818") == false) goto L_0x02e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x02f9, code lost:
        if (r1.equalsIgnoreCase("davinciin") == false) goto L_0x02fd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x040e A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x046e A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x04df  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0517 A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0539 A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x05e1 A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x060e A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0627  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0628 A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x064b  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x065e A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x03ac A[Catch: all -> 0x075a, TryCatch #0 {all -> 0x075a, blocks: (B:4:0x0015, B:5:0x001f, B:6:0x002d, B:8:0x0033, B:9:0x003d, B:12:0x0053, B:15:0x005b, B:17:0x0061, B:24:0x0085, B:25:0x008d, B:26:0x0097, B:28:0x00a6, B:30:0x00aa, B:32:0x00b0, B:33:0x00b9, B:35:0x00c8, B:36:0x00cb, B:38:0x0110, B:39:0x0118, B:44:0x015c, B:46:0x0163, B:48:0x0167, B:49:0x0220, B:51:0x023c, B:53:0x0246, B:55:0x0250, B:57:0x0258, B:59:0x0262, B:61:0x026a, B:63:0x0272, B:65:0x027a, B:67:0x0284, B:69:0x028c, B:71:0x0296, B:73:0x029f, B:75:0x02a9, B:77:0x02b1, B:79:0x02b9, B:81:0x02c1, B:83:0x02c9, B:85:0x02d1, B:87:0x02d9, B:89:0x02e1, B:91:0x02e9, B:93:0x02f3, B:97:0x02fe, B:99:0x03ac, B:100:0x03b1, B:102:0x040e, B:103:0x041d, B:105:0x046e, B:108:0x0489, B:109:0x048d, B:112:0x04a0, B:113:0x04b1, B:116:0x04e0, B:118:0x0517, B:119:0x051a, B:121:0x0539, B:124:0x053f, B:127:0x0543, B:129:0x0549, B:131:0x054d, B:132:0x0551, B:134:0x05e1, B:135:0x05fd, B:137:0x060e, B:138:0x0611, B:141:0x0628, B:143:0x0630, B:144:0x0634, B:146:0x063c, B:148:0x0641, B:153:0x0651, B:154:0x0656, B:156:0x065e, B:157:0x0679, B:159:0x0685, B:161:0x0689, B:162:0x068e, B:163:0x06a8, B:165:0x06b7, B:166:0x06d5, B:168:0x06db, B:169:0x06eb, B:170:0x0708, B:172:0x071a, B:174:0x0734, B:177:0x0746), top: B:186:0x0015 }] */
    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r26) {
        /*
        // Method dump skipped, instructions count: 1891
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipActivityV2.onCreate(android.os.Bundle):void");
    }

    @Override // X.AbstractActivityC28171Kz, android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog A1A;
        ContactPickerFragment contactPickerFragment = this.A18;
        return (contactPickerFragment == null || (A1A = contactPickerFragment.A1A(i)) == null) ? super.onCreateDialog(i) : A1A;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        AnonymousClass1J1 r0;
        super.onDestroy();
        CallGrid callGrid = this.A0l;
        if (callGrid != null) {
            ((ActivityC001000l) this).A06.A01(callGrid.A0E);
        }
        Handler handler = this.A0E;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        CallDetailsLayout callDetailsLayout = this.A0j;
        if (!(callDetailsLayout == null || (r0 = callDetailsLayout.A09.A05) == null)) {
            r0.A00();
        }
        C27131Gd r1 = this.A12;
        if (r1 != null) {
            this.A13.A04(r1);
        }
        C29631Ua r02 = this.A0v;
        if (r02 != null) {
            r02.A0t(this);
            this.A0v.A0S = null;
        }
        OrientationViewModel orientationViewModel = this.A0s;
        if (orientationViewModel != null) {
            orientationViewModel.A08();
        }
        AnonymousClass1J1 r03 = this.A15;
        if (r03 != null) {
            r03.A00();
        }
        VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
        if (videoCallParticipantViewLayout != null) {
            Map map = videoCallParticipantViewLayout.A0R;
            for (VideoPort videoPort : map.values()) {
                videoPort.release();
            }
            map.clear();
        }
        A2x();
        VoipInCallNotifBanner voipInCallNotifBanner = this.A0o;
        if (voipInCallNotifBanner != null) {
            voipInCallNotifBanner.A01();
            this.A0o.A0I.A00();
        }
        this.A1I.A04(this.A1r);
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        VideoCallParticipantViewLayout videoCallParticipantViewLayout;
        int i;
        if (this.A0Q.getWidth() != this.A09 || this.A0Q.getHeight() != this.A08) {
            StringBuilder sb = new StringBuilder("voip/VoipActivityV2/onGlobalLayout size: ");
            sb.append(this.A0Q.getWidth());
            sb.append("x");
            sb.append(this.A0Q.getHeight());
            sb.append(", orientation: ");
            sb.append(getResources().getConfiguration().orientation);
            Log.i(sb.toString());
            this.A09 = this.A0Q.getWidth();
            this.A08 = this.A0Q.getHeight();
            this.A0u.onGlobalLayout();
            CallInfo A2i = A2i();
            A3N(A2i);
            if (this.A1l && A2i != null && A2i.videoEnabled) {
                if (A2i.isGroupCall() && (i = (videoCallParticipantViewLayout = this.A0z).A01) > 2) {
                    videoCallParticipantViewLayout.A06(i, true);
                }
                this.A1Q.updateCameraPreviewOrientation();
                if (!this.A1j) {
                    A3J(A2i);
                    return;
                }
            } else if (!this.A1i) {
                return;
            }
            CallGrid callGrid = this.A0l;
            callGrid.A03.A02();
            callGrid.A04.A02();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Voip.CallState callState;
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/onKeyDown ");
        sb.append(keyEvent);
        Log.i(sb.toString());
        CallInfo A2i = A2i();
        C29631Ua r4 = this.A0v;
        if (!(r4 == null || A2i == null || (callState = A2i.callState) == Voip.CallState.NONE)) {
            if (callState == Voip.CallState.RECEIVED_CALL) {
                if (i == 24 || i == 25 || i == 91 || i == 164) {
                    r4.A0N();
                    return true;
                } else if (i == 5 || i == 126 || i == 79 || i == 85) {
                    Log.i("voip/VoipActivityV2/onKeyDown accept call from remote control");
                    A3A(8);
                    return true;
                } else if (i == 6 || i == 86) {
                    Log.i("voip/VoipActivityV2/onKeyDown reject call from remote control");
                    A3B(9);
                    return true;
                }
            } else if (i == 6 || i == 86 || ((i == 79 || i == 85) && keyEvent.getRepeatCount() == 0)) {
                Log.i("voip/VoipActivityV2/onKeyDown end call from remote control");
                Log.i("voip/VoipActivityV2/call/end");
                C29631Ua r1 = this.A0v;
                if (r1 != null) {
                    r1.A0m(null, null, 1);
                }
                this.A1a = true;
                return true;
            } else {
                boolean z = false;
                if (i == 24 || i == 25) {
                    C29631Ua r2 = this.A0v;
                    if (i == 24) {
                        z = true;
                    }
                    try {
                        AudioManager A0G = r2.A1n.A0G();
                        if (A0G != null) {
                            int streamMaxVolume = A0G.getStreamMaxVolume(0);
                            int streamVolume = A0G.getStreamVolume(0);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("voip/audio_route/adjustVoipStackAudioLevel direction ");
                            sb2.append(z ? " UP" : "DOWN");
                            sb2.append(", volume ");
                            sb2.append(streamVolume);
                            sb2.append(", max volume ");
                            sb2.append(streamMaxVolume);
                            Log.i(sb2.toString());
                            if (!z) {
                                short s = r2.A0y;
                                if (s > r2.A08) {
                                    short s2 = (short) (s - 32);
                                    r2.A0y = s2;
                                    Voip.adjustAudioLevel(s2);
                                    return true;
                                }
                            } else if (streamVolume == streamMaxVolume) {
                                short s3 = r2.A0y;
                                if (s3 >= r2.A08 + 192) {
                                    Log.i("voip/audio_route/adjustVoipStackAudioLevel no-op, auido level is 192");
                                } else {
                                    short s4 = (short) (s3 + 32);
                                    r2.A0y = s4;
                                    Voip.adjustAudioLevel(s4);
                                }
                            }
                        }
                    } catch (Throwable th) {
                        Log.e(th);
                    }
                } else if (i == 131) {
                    AnonymousClass009.A0A("Should be used for automation only", false);
                    AnonymousClass009.A0A("it can only be used in smoke or automation", false);
                } else if (i == 4 && A2i.callState == Voip.CallState.REJOINING) {
                    A3B(2);
                }
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        String str;
        C29631Ua r1;
        String action = intent.getAction();
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/onNewIntent ");
        sb.append(intent);
        sb.append(", action ");
        sb.append(action);
        sb.append(", is finishing ");
        sb.append(isFinishing());
        Log.i(sb.toString());
        super.onNewIntent(intent);
        setIntent(intent);
        this.A1d = false;
        this.A1W = false;
        this.A1T = intent.getStringExtra("call_id");
        CallInfo A2i = A2i();
        if (A2i != null) {
            String str2 = this.A1T;
            if (str2 == null) {
                str2 = A2i.callId;
                this.A1T = str2;
            }
            CallGridViewModel callGridViewModel = this.A0p;
            if (callGridViewModel != null) {
                callGridViewModel.A0C.A0B(str2);
            }
            if (this.A0v == null) {
                this.A0w.A00.obtainMessage(4, this).sendToTarget();
            }
            A3M(A2i);
            if ("com.whatsapp.intent.action.ACCEPT_CALL".equals(action)) {
                A3D(intent, A2i);
            } else if ("com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN".equals(action) || "join_call".equals(action)) {
                this.A1R.A00(A2i.callId);
                this.A1X = false;
                this.A1n = true;
                if (this.A18 != null) {
                    A2q();
                }
                A2x();
                A30();
                Voip.stopVideoCaptureStream();
                A3N(A2i);
                this.A0w.A00(new C26391De("refresh_notification"));
            } else if ("com.whatsapp.intent.action.SHOW_END_CALL_CONFIRMATION".equals(action)) {
                String stringExtra = intent.getStringExtra("confirmationString");
                if (this.A0d == null && C21280xA.A00()) {
                    Log.w("voip/VoipActivityV2/showEndCallConfirmationDialog.");
                    DialogFragment A00 = EndCallConfirmationDialogFragment.A00(stringExtra);
                    this.A0d = A00;
                    A00.A1F(A0V(), null);
                }
            } else {
                int i = 2;
                if ("com.whatsapp.intent.action.END_CALL_AFTER_CONFIRMATION".equals(action)) {
                    C29631Ua r12 = this.A0v;
                    if (r12 != null) {
                        r12.A0m(null, null, 2);
                    }
                } else if ("com.whatsapp.intent.action.SHOW_ALERT_MESSAGE_IN_ACTIVE_CALL".equals(action)) {
                    A3E(intent, A2i);
                } else if ("ACTION_AUTOMATION_BRING_TO_FRONT".equals(action)) {
                    AnonymousClass009.A0A("it can only be used in smoke or automation", false);
                } else if ("com.whatsapp.intent.action.CALL_BACK".equals(action) && C21280xA.A00()) {
                    ((ActivityC13810kN) this).A05.A07(R.string.error_call_disabled_during_call, 1);
                } else if ("com.whatsapp.intent.action.REJECT_CALL_FROM_VOIP_UI".equals(action)) {
                    if (intent.getBooleanExtra("pendingCall", false)) {
                        i = 7;
                    }
                    if (A2i.isCallLinkLobbyState) {
                        C29631Ua r13 = this.A0v;
                        if (r13 != null) {
                            r13.A0v(A2i.callId);
                        }
                        finish();
                    } else {
                        A3B(i);
                    }
                } else if (!"com.whatsapp.intent.action.JOIN_CALL_LINK".equals(action)) {
                    if (isFinishing()) {
                        str = "voip/VoipActivityV2/new-intent activity is finishing, do nothing";
                    } else if (Voip.A09(A2i)) {
                        str = "voip/VoipActivityV2/new-intent the WhatsApp call is not active, do nothing";
                    } else {
                        A3N(A2i);
                        if (intent.getBooleanExtra("newCall", false)) {
                            Log.i("voip/VoipActivityV2/onNewIntent/NewCall clearing states");
                            this.A1X = intent.getBooleanExtra("callAccepted", false);
                            this.A05 = 0;
                            this.A0E.removeMessages(9);
                            A2n();
                        }
                    }
                    Log.e(str);
                    return;
                } else if (AnonymousClass1SF.A0O(((ActivityC13810kN) this).A0C) && (r1 = this.A0v) != null) {
                    Log.i("voip/call/join call link");
                    r1.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(4));
                }
            }
            this.A19.A04(7, null);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (this.A18 == null || menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2q();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A1h = false;
        this.A1a = false;
        if (this.A01 == 0) {
            A2u();
        }
        AppSettingsWarningDialogFragment appSettingsWarningDialogFragment = this.A11;
        if (appSettingsWarningDialogFragment == null) {
            return;
        }
        if (appSettingsWarningDialogFragment.A0c()) {
            A2M(AppSettingsWarningDialogFragment.class.getName());
        } else {
            this.A11 = null;
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPictureInPictureModeChanged(boolean z) {
        super.onPictureInPictureModeChanged(z);
        if (z) {
            this.A1g = true;
            if (this.A01 != 0) {
                A2u();
                this.A0z.A03();
            }
            this.A01 = 1;
            C29631Ua r2 = this.A0v;
            if (r2 != null) {
                Log.i("VoiceService:onEnterPictureInPicture");
                r2.A0C = System.currentTimeMillis();
            }
        } else {
            this.A1g = false;
            this.A01 = 2;
            C29631Ua r0 = this.A0v;
            if (r0 != null) {
                r0.A0L();
            }
        }
        CallInfo A2i = A2i();
        if (A2i != null && A2i.videoEnabled) {
            if (!this.A1j || this.A0p == null) {
                A38();
                A3J(A2i);
            } else {
                A3S(this.A1f);
                CallGridViewModel callGridViewModel = this.A0p;
                callGridViewModel.A04 = z;
                AnonymousClass016 r22 = callGridViewModel.A0B;
                Object A01 = r22.A01();
                AnonymousClass009.A05(A01);
                C91984Tz r1 = (C91984Tz) A01;
                r1.A07 = z;
                float f = 0.225f;
                if (z) {
                    f = 0.33f;
                }
                r1.A00 = f;
                r22.A0B(r1);
                Rect rect = callGridViewModel.A01;
                if (rect != null) {
                    callGridViewModel.A0K.A0B(rect);
                }
                callGridViewModel.A07(callGridViewModel.A0C.A05(), false);
                AnonymousClass3DO r12 = this.A0n;
                r12.A01 = 0.0f;
                r12.A00();
            }
            Voip.processPipModeChange(z);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public void onRestart() {
        if (((ActivityC13810kN) this).A0C.A07(1807)) {
            ((ActivityC13830kP) this).A02.A05(this.A0R, new RunnableBRunnable0Shape13S0100000_I0_13(this, 38), "VoipActivityV2", 4);
        }
        super.onRestart();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        CallGrid callGrid;
        CallGridViewModel callGridViewModel;
        C29631Ua r0;
        boolean A07 = ((ActivityC13810kN) this).A0C.A07(1807);
        if (A07) {
            try {
                ((ActivityC13830kP) this).A02.A08("VoipActivityV2_onResume");
            } finally {
                if (A07) {
                    ((ActivityC13830kP) this).A02.A07("VoipActivityV2_onResume");
                }
            }
        }
        super.onResume();
        if (this.A0Q != null) {
            this.A1h = true;
            this.A1k = true;
            CallInfo A2i = A2i();
            if (!(A2i == null || A2i.callState == Voip.CallState.NONE)) {
                A2v();
                this.A0E.sendEmptyMessageDelayed(1, 500);
                if (A2i.videoEnabled) {
                    if (A2i.self.A04 != 6) {
                        if (!A2i.videoPreviewReady && !this.A1j) {
                            this.A10.A05();
                        }
                        if (A2i.videoCaptureStarted) {
                            Log.i("voip/VoipActivityV2/videoCaptureStarted.");
                        }
                        UserJid peerJid = A2i.getPeerJid();
                        if (!A2i.isCallLinkLobbyState) {
                            AnonymousClass009.A05(peerJid);
                            AnonymousClass1S6 infoByJid = A2i.getInfoByJid(peerJid);
                            if (infoByJid != null && infoByJid.A0K) {
                                Afh(peerJid);
                            }
                        }
                        A3N(A2i);
                    } else if (this.A1p && (r0 = this.A0v) != null) {
                        r0.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(7));
                    }
                }
                if (!(!this.A1j || (callGrid = this.A0l) == null || (callGridViewModel = this.A0p) == null)) {
                    MenuBottomSheetViewModel menuBottomSheetViewModel = this.A0r;
                    if (callGrid.A05 == null) {
                        callGrid.A05 = callGridViewModel;
                        callGridViewModel.A0A.A05(this, new AnonymousClass02B() { // from class: X.4sn
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                CallGrid callGrid2 = CallGrid.this;
                                List list = (List) obj;
                                if (list != null) {
                                    callGrid2.A05(list, false);
                                }
                            }
                        });
                        callGrid.A05.A08.A05(this, new AnonymousClass02B() { // from class: X.3Q5
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                int i;
                                CallGrid callGrid2 = CallGrid.this;
                                List list = (List) obj;
                                if (list == null || list.isEmpty()) {
                                    callGrid2.A04.A0I(C12960it.A0l());
                                    return;
                                }
                                callGrid2.A05(list, true);
                                int size = list.size();
                                RecyclerView recyclerView = callGrid2.A0I;
                                int width = recyclerView.getWidth();
                                if (size == 3) {
                                    i = width / 3;
                                } else {
                                    i = (int) (((double) width) / 3.25d);
                                }
                                if (i != recyclerView.getHeight()) {
                                    callGrid2.A04.A02();
                                }
                            }
                        });
                        callGrid.A05.A0B.A05(this, new AnonymousClass02B() { // from class: X.4so
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                PipViewContainer pipViewContainer = PipViewContainer.this;
                                pipViewContainer.A06 = (C91984Tz) obj;
                                pipViewContainer.A02();
                            }
                        });
                        AnonymousClass016 r1 = callGrid.A05.A06;
                        FocusViewContainer focusViewContainer = callGrid.A0O;
                        r1.A05(this, new AnonymousClass02B() { // from class: X.3Q6
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                FocusViewContainer focusViewContainer2 = FocusViewContainer.this;
                                AnonymousClass4ND r8 = (AnonymousClass4ND) obj;
                                int i = r8.A01;
                                int i2 = r8.A00;
                                if (i > 0 && i2 > 0) {
                                    FrameLayout frameLayout = focusViewContainer2.A01;
                                    AnonymousClass064 r3 = (AnonymousClass064) frameLayout.getLayoutParams();
                                    Locale locale = Locale.ENGLISH;
                                    Object[] A1a = C12980iv.A1a();
                                    C12960it.A1P(A1a, i, 0);
                                    C12960it.A1P(A1a, i2, 1);
                                    r3.A0t = String.format(locale, "H,%d:%d", A1a);
                                    frameLayout.setLayoutParams(r3);
                                }
                            }
                        });
                        callGrid.A05.A07.A05(this, new AnonymousClass02B() { // from class: X.4sl
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                CallGrid callGrid2 = CallGrid.this;
                                int i = 4;
                                if (obj == null) {
                                    i = 1;
                                }
                                AnonymousClass028.A0a(callGrid2.A0J, i);
                                AnonymousClass028.A0a(callGrid2.A0I, i);
                            }
                        });
                        callGrid.A05.A0J.A05(this, new AnonymousClass02B() { // from class: X.3Q4
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                CallGrid callGrid2 = CallGrid.this;
                                boolean A1Y = C12970iu.A1Y(obj);
                                StringBuilder A0k = C12960it.A0k("CallGrid/onAvSwitched, isVideoEnabled: ");
                                A0k.append(A1Y);
                                C12960it.A1F(A0k);
                                callGrid2.A0B = A1Y;
                                callGrid2.A0N.A06 = A1Y;
                                callGrid2.A0M.A0D = A1Y;
                            }
                        });
                        callGrid.A05.A0I.A05(this, new AnonymousClass02B() { // from class: X.4sm
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                CallGrid callGrid2 = CallGrid.this;
                                boolean A1Y = C12970iu.A1Y(obj);
                                callGrid2.A0A = A1Y;
                                callGrid2.A0N.A05 = A1Y;
                            }
                        });
                        callGrid.A05.A0K.A05(this, new AnonymousClass02B() { // from class: X.4sk
                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                CallGrid.this.setMargins((Rect) obj);
                            }
                        });
                        callGrid.A03.A04 = callGridViewModel;
                        callGrid.A04.A04 = callGridViewModel;
                        focusViewContainer.setMenuViewModel(this, menuBottomSheetViewModel);
                    }
                }
                if (this.A1p) {
                    this.A1p = false;
                }
                if (this.A01 == 2) {
                    this.A01 = 3;
                    this.A0z.A04();
                    if (!"com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN".equals(getIntent().getAction())) {
                        A32();
                    }
                }
                AppSettingsWarningDialogFragment appSettingsWarningDialogFragment = this.A11;
                if (appSettingsWarningDialogFragment != null && !appSettingsWarningDialogFragment.A0c()) {
                    Adm(appSettingsWarningDialogFragment);
                }
            }
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A0x;
        if (maximizedParticipantVideoDialogFragment != null) {
            maximizedParticipantVideoDialogFragment.A1C();
        }
        if (this.A18 != null) {
            A2q();
        }
        super.onSaveInstanceState(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        ContactPickerFragment contactPickerFragment = this.A18;
        if (contactPickerFragment == null) {
            return false;
        }
        contactPickerFragment.A0M.A01();
        return true;
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested(SearchEvent searchEvent) {
        ContactPickerFragment contactPickerFragment = this.A18;
        if (contactPickerFragment == null) {
            return false;
        }
        contactPickerFragment.A0M.A01();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        View view = this.A0Q;
        if (view != null) {
            this.A1l = true;
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
            C29631Ua r0 = this.A0v;
            if (r0 != null) {
                r0.A1H = false;
            }
            this.A0u.AWH(this.A0L);
            CallInfo A2i = A2i();
            if (A2i != null && A2i.callState != Voip.CallState.NONE) {
                if (A2i.isPeerRequestingUpgrade()) {
                    this.A1X = false;
                }
                A3M(A2i);
                A3N(A2i);
                if (this.A1c || ("join_call".equals(getIntent().getAction()) && Voip.A0A(A2i.callState))) {
                    this.A0w.A00(new C26391De("refresh_notification"));
                    this.A1c = false;
                }
                if (A2i.videoEnabled) {
                    if (!"com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN".equals(getIntent().getAction()) && A2i.self.A04 != 6) {
                        Voip.startVideoCaptureStream();
                    }
                    if (!this.A1j) {
                        this.A10.A05();
                    }
                    C29631Ua r2 = this.A0v;
                    if (r2 != null && this.A1g) {
                        Log.i("VoiceService:onEnterPictureInPicture");
                        r2.A0C = System.currentTimeMillis();
                    }
                }
                Log.i("voip/VoipActivityV2/bindService");
                this.A0w.A00.obtainMessage(4, this).sendToTarget();
            } else if (getIntent().hasExtra("showCallFailedMessage")) {
                A2o();
            } else {
                finish();
                Log.e("voip/VoipActivityV2/onStart call_not_active, finishing");
            }
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        Voip.CallState callState;
        Voip.CallState callState2;
        super.onStop();
        AnonymousClass37Q r0 = this.A1K;
        if (r0 != null) {
            r0.A03(true);
        }
        this.A0u.AWn();
        if (!getIntent().getBooleanExtra("joinable", false)) {
            OrientationViewModel orientationViewModel = this.A0s;
            if (orientationViewModel != null) {
                orientationViewModel.A08();
            }
            this.A1l = false;
            View view = this.A0Q;
            if (view != null) {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
            MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A0x;
            if (maximizedParticipantVideoDialogFragment != null) {
                maximizedParticipantVideoDialogFragment.A1L(true);
            }
            VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0z;
            if (videoCallParticipantViewLayout != null) {
                C17140qK r4 = this.A1P;
                if (videoCallParticipantViewLayout.A0O) {
                    int i = 0;
                    videoCallParticipantViewLayout.A0O = false;
                    int i2 = !videoCallParticipantViewLayout.A0N ? 1 : 0;
                    if (!videoCallParticipantViewLayout.A0M) {
                        i = 2;
                    }
                    r4.A01().edit().putInt("video_call_pip_position", i2 + i).apply();
                }
            }
            Handler handler = this.A0E;
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
            CallInfo A2i = A2i();
            if (!(A2i == null || (callState = A2i.callState) == (callState2 = Voip.CallState.NONE))) {
                if (callState == Voip.CallState.RECEIVED_CALL && Build.VERSION.SDK_INT >= 21 && !((ActivityC13790kL) this).A0C.A00 && !this.A1X) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("notification_type", 1);
                    StringBuilder sb = new StringBuilder("voip/VoipActivityV2/onStop post ");
                    sb.append("NOTIFICATION_HEADS_UP");
                    Log.i(sb.toString());
                    this.A0w.A00(new C26391De("refresh_notification", bundle));
                    this.A1c = true;
                }
                C29631Ua r1 = this.A0v;
                if (r1 != null && this.A1g) {
                    r1.A0L();
                }
                if (A2i.videoEnabled) {
                    Voip.stopVideoCaptureStream();
                    if (A2i.callState != callState2 && Build.VERSION.SDK_INT < 19 && this.A0y.A03 == 1) {
                        Log.i("voip/VoipActivityV2/onStop finish current activity, will recreate on foreground");
                        C29631Ua r02 = this.A0v;
                        if (r02 != null) {
                            r02.A0t(this);
                            this.A0v.A0S = null;
                        }
                        finish();
                    }
                }
                if (this.A0v != null && !this.A0f.A00) {
                    Log.i("voip/VoipActivityV2/onStop. App is put to background, mark to show VoipActivity again when foregrounded.");
                    this.A0v.A1H = true;
                }
            }
            A34();
        }
    }

    @Override // android.app.Activity
    public void onUserInteraction() {
        this.A1k = true;
    }

    @Override // android.app.Activity
    public void onUserLeaveHint() {
        Log.i("voip/VoipActivityV2/onUserLeaveHint");
        if (this.A18 != null || Build.VERSION.SDK_INT < 26 || !A3V(A2i())) {
            this.A1k = false;
        }
    }

    @Override // android.app.Activity
    public void recreate() {
        if (this.A01 == 3) {
            super.recreate();
        }
    }

    public void showView(View view) {
        view.setVisibility(0);
        ((AnimatingArrowsLayout) view).A03.start();
    }

    /* loaded from: classes2.dex */
    public class E2EEInfoDialogFragment extends Hilt_VoipActivityV2_E2EEInfoDialogFragment {
        public int A00 = 11;
        public AnonymousClass12P A01;
        public C14850m9 A02;
        public C16120oU A03;
        public AnonymousClass1AT A04;
        public C252018m A05;

        public static E2EEInfoDialogFragment A00(int i) {
            E2EEInfoDialogFragment e2EEInfoDialogFragment = new E2EEInfoDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("entry_point", i);
            e2EEInfoDialogFragment.A0U(A0D);
            return e2EEInfoDialogFragment;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int i;
            C004802e A0K = C12960it.A0K(this);
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            if (!(bundle2 == null || (i = bundle2.getInt("entry_point", -1)) == -1)) {
                this.A00 = i;
                if (this.A02.A07(1071)) {
                    this.A04.A00(i, 1);
                }
            }
            boolean A07 = this.A02.A07(1071);
            int i2 = R.string.encryption_description;
            if (A07) {
                i2 = R.string.built_call_private_summary;
            }
            A0K.A06(i2);
            C12970iu.A1K(A0K, this, 74, R.string.ok);
            A0K.A00(R.string.learn_more, new IDxCListenerShape8S0100000_1_I1(this, 37));
            return A0K.create();
        }
    }

    /* loaded from: classes2.dex */
    public class EndCallConfirmationDialogFragment extends Hilt_VoipActivityV2_EndCallConfirmationDialogFragment {
        public static DialogFragment A00(String str) {
            EndCallConfirmationDialogFragment endCallConfirmationDialogFragment = new EndCallConfirmationDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("message", str);
            endCallConfirmationDialogFragment.A0U(A0D);
            return endCallConfirmationDialogFragment;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            String string = A03().getString("message");
            C004802e A0K = C12960it.A0K(this);
            A0K.A0A(string);
            A0K.A0B(true);
            C12970iu.A1L(A0K, this, 75, R.string.btn_continue);
            return C12990iw.A0O(A0K, this, 38, R.string.hang_up);
        }
    }

    /* loaded from: classes2.dex */
    public class NonActivityDismissDialogFragment extends Hilt_VoipActivityV2_NonActivityDismissDialogFragment {
        public static DialogFragment A00(String str, boolean z) {
            NonActivityDismissDialogFragment nonActivityDismissDialogFragment = new NonActivityDismissDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("text", str);
            A0D.putBoolean("dismiss", z);
            nonActivityDismissDialogFragment.A0U(A0D);
            return nonActivityDismissDialogFragment;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            Context A01 = A01();
            Bundle A03 = A03();
            C004802e A0S = C12980iv.A0S(A01);
            A0S.A0A(A03.getString("text"));
            A0S.A0B(true);
            if (A03.getBoolean("dismiss", false)) {
                C12970iu.A1L(A0S, this, 77, R.string.ok);
            }
            return A0S.create();
        }
    }

    /* loaded from: classes2.dex */
    public class ReplyWithMessageDialogFragment extends Hilt_VoipActivityV2_ReplyWithMessageDialogFragment {
        public static final int[] A03 = {R.string.incomming_call_reply_0, R.string.incomming_call_reply_1, R.string.incomming_call_reply_2, R.string.incomming_call_reply_3, R.string.incomming_call_reply_custom};
        public C14830m7 A00;
        public AnonymousClass018 A01;
        public final UserJid A02;

        public ReplyWithMessageDialogFragment(UserJid userJid) {
            this.A02 = userJid;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e A0K = C12960it.A0K(this);
            String[] A0S = this.A01.A0S(A03);
            IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I1 = new IDxCListenerShape3S0200000_1_I1(A0S, 13, this);
            AnonymousClass0OC r0 = A0K.A01;
            r0.A0M = A0S;
            r0.A05 = iDxCListenerShape3S0200000_1_I1;
            AnonymousClass04S create = A0K.create();
            create.setCanceledOnTouchOutside(true);
            return create;
        }
    }

    /* loaded from: classes2.dex */
    public class SwitchConfirmationFragment extends Hilt_VoipActivityV2_SwitchConfirmationFragment {
        public C14820m6 A00;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e A0K = C12960it.A0K(this);
            A0K.A06(R.string.voip_requesting_upgrade_to_video_confirmation_text);
            AnonymousClass04S A0L = C12960it.A0L(new IDxCListenerShape8S0100000_1_I1(this, 39), A0K, R.string.voip_requesting_upgrade_to_video_confirmation_positive_button_label);
            A0L.setCanceledOnTouchOutside(true);
            return A0L;
        }
    }

    /* loaded from: classes3.dex */
    public class MessageDialogFragment extends Hilt_VoipActivityV2_MessageDialogFragment {
        public C21280xA A00;

        public static DialogFragment A00(String str) {
            MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("message", str);
            messageDialogFragment.A0U(A0D);
            return messageDialogFragment;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            Context A01 = A01();
            String string = A03().getString("message");
            C004802e r2 = new C004802e(A01);
            r2.A0A(string);
            r2.A0B(true);
            C12970iu.A1L(r2, this, 76, R.string.ok);
            return r2.create();
        }

        @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
        public void onDismiss(DialogInterface dialogInterface) {
            if (A0B() != null && !C21280xA.A00()) {
                A0B().finish();
            }
        }
    }
}
