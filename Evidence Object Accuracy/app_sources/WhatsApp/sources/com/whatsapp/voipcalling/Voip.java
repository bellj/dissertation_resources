package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import X.AnonymousClass2KU;
import X.C12960it;
import X.C14330lG;
import android.graphics.Bitmap;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AutomaticGainControl;
import android.media.audiofx.NoiseSuppressor;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.protocol.VoipStanzaChildNode;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/* loaded from: classes2.dex */
public final class Voip {
    public static CryptoCallback A00;
    public static final AnonymousClass2KU A01 = new AnonymousClass2KU();
    public static final SimpleDateFormat A02 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
    public static volatile SignalingXmppCallback A03;

    /* loaded from: classes2.dex */
    public enum CallState {
        NONE,
        CALLING,
        PRE_ACCEPT_RECEIVED,
        RECEIVED_CALL,
        ACCEPT_SENT,
        ACCEPT_RECEIVED,
        ACTIVE,
        ACTIVE_ELSEWHERE,
        RECEIVED_CALL_WITHOUT_OFFER,
        REJOINING,
        LINK,
        CONNECTED_LONELY
    }

    /* loaded from: classes2.dex */
    public enum DebugTapType {
        RECEIVED_AND_DECODED,
        CAPTURED_AND_POST_PROCESSED,
        OUTGOING_ENCODED,
        RAW_CAPTURED,
        RAW_PLAYBACK
    }

    public static native void acceptCall();

    public static native int acceptVideoUpgrade();

    public static native void adjustAudioLevel(int i);

    public static native int cancelInviteToGroupCall(UserJid userJid);

    public static native int cancelVideoUpgrade(int i);

    public static native void checkOngoingCalls(String[] strArr, DeviceJid[] deviceJidArr);

    public static native void clearVoipParam(String str);

    public static native int createCallLink(boolean z);

    public static native void debugAdjustAECMParams(short s, short s2);

    public static native boolean dumpLastVideoFrame(String str, Bitmap bitmap);

    public static native void endCall(boolean z);

    public static native void endCallAndAcceptPendingCall(String str);

    public static native CallInfo getCallInfo();

    public static native CallLinkInfo getCallLinkInfo();

    public static native int getCameraCount();

    public static native String getCurrentCallId();

    public static native int getCurrentCallLinkState();

    public static native CallState getCurrentCallState();

    public static native String getCurrentRxNetworkConditionerParameters();

    public static native String getCurrentTxNetworkConditionerParameters();

    public static native UserJid getPeerJid();

    public static native String getStreamStatistics();

    public static native String getStreamStatisticsShort();

    public static native String getVoipParam(String str);

    public static native int getVoipParamElemCount(String str);

    public static native String getVoipParamForCall(String str, String str2);

    public static native int inviteToGroupCall(CallParticipantJid callParticipantJid);

    public static native boolean isRxNetworkConditionerOn();

    public static native boolean isTxNetworkConditionerOn();

    public static native int joinCallLink();

    public static native int joinOngoingCall(String str, UserJid userJid, DeviceJid deviceJid, boolean z, CallParticipantJid[] callParticipantJidArr, boolean z2, GroupJid groupJid, int i, String str2);

    public static native void muteCall(boolean z);

    public static native int nativeHandleIncomingSignalingXmpp(Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, long j, long j2, boolean z);

    public static native int nativeHandleIncomingSignalingXmppAck(Jid jid, String str, int i, VoipStanzaChildNode[] voipStanzaChildNodeArr);

    public static native int nativeHandleIncomingSignalingXmppReceipt(Jid jid, VoipStanzaChildNode voipStanzaChildNode);

    public static native int nativeHandleIncomingXmppOffer(Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, long j, long j2, boolean z, boolean z2, int i, boolean z3, String str3);

    public static native int nativeHandleWebClientMessage(String str, String str2, int i, VoipStanzaChildNode voipStanzaChildNode);

    public static native int nativeParseXmppOffer(CallOfferInfo[] callOfferInfoArr, Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, long j, long j2, boolean z);

    public static native void nativeRegisterCryptoCallback(CryptoCallback cryptoCallback);

    public static native void nativeRegisterEventCallback(VoipEventCallback voipEventCallback);

    public static native int nativeRegisterJNIUtils(JNIUtils jNIUtils);

    public static native void nativeRegisterMultiNetworkCallback(MultiNetworkCallback multiNetworkCallback);

    public static native void nativeRegisterSignalingXmppCallback(SignalingXmppCallback signalingXmppCallback);

    public static native void nativeUnregisterCryptoCallback();

    public static native void nativeUnregisterEventCallback();

    public static native void nativeUnregisterJNIUtils();

    public static native void nativeUnregisterMultiNetworkCallback();

    public static native void nativeUnregisterSignalingXmppCallback();

    public static native void notifyAudioRouteChange(int i);

    public static native void notifyDeviceIdentityChanged(DeviceJid deviceJid);

    public static native void notifyDeviceIdentityDeleted(DeviceJid deviceJid);

    public static native int notifyFailureToCreateAlternativeSocket(boolean z);

    public static native int notifyLostOfAlternativeNetwork();

    public static native void onCallInterrupted(boolean z, boolean z2);

    public static native int previewCallLink(String str, boolean z);

    public static native void processPipModeChange(boolean z);

    public static native int refreshVideoDevice();

    public static native void rejectCall(String str, String str2);

    public static native void rejectCallWithoutCallContext(String str, boolean z, DeviceJid deviceJid, DeviceJid deviceJid2, String str2, int i, int i2);

    public static native void rejectPendingCall(String str);

    public static native int rejectVideoUpgrade(int i);

    public static native int requestVideoUpgrade();

    public static native void resendOfferOnDecryptionFailure(DeviceJid deviceJid, String str);

    public static native void saveCallMetrics(String str);

    public static native int sendMutePeerRequestInGroupCall(UserJid userJid);

    public static native void sendRekeyRequest(DeviceJid deviceJid, int i);

    public static native int setBatteryState(float f, float f2, boolean z);

    public static native int setScreenSize(int i, int i2);

    public static native int setVideoDisplayPort(String str, VideoPort videoPort);

    public static native int setVideoPreviewPort(VideoPort videoPort, String str);

    public static native int setVideoPreviewSize(int i, int i2);

    public static native int startCall(String str, CallParticipantJid callParticipantJid, boolean z);

    public static native boolean startCallRecording(RecordingInfo[] recordingInfoArr);

    public static native int startGroupCall(String str, CallParticipantJid[] callParticipantJidArr, boolean z, GroupJid groupJid);

    public static native int startTestNetworkConditionWithAlternativeSocket(int i, String str, int i2);

    public static native void startVideoCaptureStream();

    public static native void startVideoRenderStream(String str);

    public static native boolean stopCallRecording();

    public static native void stopVideoCaptureStream();

    public static native void stopVideoRenderStream(String str);

    public static native int switchCamera();

    public static native int switchNetworkWithAlternativeSocket(int i, String str, int i2);

    public static native void timeoutPendingCall(String str);

    public static native void transitionToRejoining();

    public static native int turnCameraOff();

    public static native int turnCameraOn();

    public static native void updateNetworkMedium(int i, int i2);

    public static native void updateNetworkRestrictions(boolean z);

    public static native int updateParticipantsRxSubscription(UserJid[] userJidArr);

    public static native void videoOrientationChanged(int i);

    public static Boolean A00(String str) {
        Integer A012 = A01(str);
        if (A012 == null) {
            return null;
        }
        int intValue = A012.intValue();
        boolean z = false;
        if (intValue != 0) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    public static Integer A01(String str) {
        String A07 = A07(str);
        if (A07 == null || A07.isEmpty()) {
            StringBuilder sb = new StringBuilder("No value found for param ");
            sb.append(str);
            Log.i(sb.toString());
            return null;
        }
        try {
            return Integer.valueOf(A07);
        } catch (NumberFormatException e) {
            StringBuilder sb2 = new StringBuilder("Wrong format for param ");
            sb2.append(str);
            sb2.append(", value ");
            sb2.append(A07);
            Log.e(sb2.toString(), e);
            return null;
        }
    }

    public static Object A02(int i, boolean z) {
        boolean z2;
        try {
            z2 = AcousticEchoCanceler.isAvailable();
        } catch (Throwable th) {
            Log.e(th);
            z2 = false;
        }
        if (!z2) {
            return null;
        }
        if (i < 0) {
            return null;
        }
        for (int i2 = 0; i2 <= 50; i2++) {
            int i3 = i + i2;
            if (i2 == 0) {
                i3 = i + 3;
            }
            try {
                AcousticEchoCanceler create = AcousticEchoCanceler.create(i3);
                if (create != null) {
                    create.setEnabled(z);
                    StringBuilder sb = new StringBuilder();
                    sb.append("voip/hackBuiltInAec/enabled ");
                    sb.append(create.getEnabled());
                    sb.append(" on session id ");
                    sb.append(i3);
                    sb.append(" with previous session id ");
                    sb.append(i);
                    Log.i(sb.toString());
                    return create;
                }
            } catch (Throwable th2) {
                Log.e(th2);
            }
        }
        StringBuilder sb2 = new StringBuilder("voip/hackBuiltInAec/failed previousAudioSessionId = ");
        sb2.append(i);
        sb2.append(", range = ");
        sb2.append(50);
        Log.i(sb2.toString());
        return null;
    }

    public static Object A03(int i, boolean z) {
        boolean z2;
        try {
            z2 = AutomaticGainControl.isAvailable();
        } catch (Throwable th) {
            Log.e(th);
            z2 = false;
        }
        if (!z2) {
            return null;
        }
        if (i < 0) {
            return null;
        }
        for (int i2 = 0; i2 <= 50; i2++) {
            int i3 = i + i2;
            if (i2 == 0) {
                i3 = i + 3;
            }
            try {
                AutomaticGainControl create = AutomaticGainControl.create(i3);
                if (create != null) {
                    create.setEnabled(z);
                    StringBuilder sb = new StringBuilder();
                    sb.append("voip/hackBuiltInAgc/enabled ");
                    sb.append(create.getEnabled());
                    sb.append(" on session id ");
                    sb.append(i3);
                    sb.append(" with previous session id ");
                    sb.append(i);
                    Log.i(sb.toString());
                    return create;
                }
            } catch (Throwable th2) {
                Log.e(th2);
            }
        }
        StringBuilder sb2 = new StringBuilder("voip/hackBuiltInAgc/failed previousAudioSessionId = ");
        sb2.append(i);
        sb2.append(", range = ");
        sb2.append(50);
        Log.i(sb2.toString());
        return null;
    }

    public static Object A04(int i, boolean z) {
        boolean z2;
        try {
            z2 = NoiseSuppressor.isAvailable();
        } catch (Throwable th) {
            Log.e(th);
            z2 = false;
        }
        if (!z2) {
            return null;
        }
        if (i < 0) {
            return null;
        }
        for (int i2 = 0; i2 <= 50; i2++) {
            int i3 = i + i2;
            if (i2 == 0) {
                i3 = i + 3;
            }
            try {
                NoiseSuppressor create = NoiseSuppressor.create(i3);
                if (create != null) {
                    create.setEnabled(z);
                    StringBuilder sb = new StringBuilder();
                    sb.append("voip/hackBuiltInNs/enabled ");
                    sb.append(create.getEnabled());
                    sb.append(" on session id ");
                    sb.append(i3);
                    sb.append(" with previous session id ");
                    sb.append(i);
                    Log.i(sb.toString());
                    return create;
                }
            } catch (Throwable th2) {
                Log.e(th2);
            }
        }
        StringBuilder sb2 = new StringBuilder("voip/hackBuiltInNs/failed previousAudioSessionId = ");
        sb2.append(i);
        sb2.append(", range = ");
        sb2.append(50);
        Log.i(sb2.toString());
        return null;
    }

    public static String A05(int i) {
        if (i == 0) {
            return "kAudOutputDefault";
        }
        if (i == 1) {
            return "kAudOutputSpeaker";
        }
        if (i == 2) {
            return "kAudOutputEarpiece";
        }
        if (i == 3) {
            return "kAudOutputBluetooth";
        }
        if (i == 4) {
            return "kAudOutputHeadset";
        }
        AnonymousClass009.A07("UNKNOWN AudioRoute");
        return "UNKNOWN AudioRoute";
    }

    public static String A06(int i) {
        if (i == 0) {
            return "kCallLinkStateNone";
        }
        if (i == 1) {
            return "kCallLinkStateQuerySent";
        }
        if (i == 2) {
            return "kCallLinkStateQueryAcked";
        }
        if (i == 3) {
            return "kCallLinkStateJoinSent";
        }
        if (i == 4) {
            return "kCallLinkStateJoinAcked";
        }
        AnonymousClass009.A07("UNKNOWN CallLinkState");
        return "UNKNOWN CallLinkState";
    }

    public static String A07(String str) {
        String voipParam = getVoipParam(str);
        if (voipParam == null || voipParam.isEmpty()) {
            return null;
        }
        return voipParam;
    }

    public static boolean A08(CallInfo callInfo) {
        return (callInfo == null || callInfo.callState == CallState.NONE) ? false : true;
    }

    public static boolean A09(CallInfo callInfo) {
        return A08(callInfo) && callInfo.callState != CallState.ACTIVE_ELSEWHERE;
    }

    public static boolean A0A(CallState callState) {
        return callState == CallState.RECEIVED_CALL || callState == CallState.REJOINING;
    }

    /* loaded from: classes2.dex */
    public class CallLogInfo {
        public final int callLogResultType;
        public Map groupCallLogs;
        public final UserJid initialPeerJid;
        public final long rxTotalBytes;
        public final long txTotalBytes;

        public CallLogInfo(UserJid userJid, int i, long j, long j2) {
            this.callLogResultType = i;
            this.txTotalBytes = j;
            this.rxTotalBytes = j2;
            this.initialPeerJid = userJid;
        }

        public void addGroupCallLog(String str, int i) {
            Map map = this.groupCallLogs;
            if (map == null) {
                map = new LinkedHashMap();
                this.groupCallLogs = map;
            }
            map.put(str, Integer.valueOf(i));
        }

        public Map getGroupCallLogs() {
            return this.groupCallLogs;
        }
    }

    /* loaded from: classes2.dex */
    public class JidHelper {
        public static UserJid convertToUserJid(Jid jid) {
            if (jid instanceof UserJid) {
                return (UserJid) jid;
            }
            if (jid instanceof DeviceJid) {
                return ((DeviceJid) jid).getUserJid();
            }
            return null;
        }

        public static int getAgent(Jid jid) {
            return jid.getAgent();
        }

        public static int getDevice(Jid jid) {
            return jid.getDevice();
        }

        public static String getDomain(Jid jid) {
            return jid.getServer();
        }

        public static String getIdentifier(Jid jid) {
            return jid.user;
        }

        public static Jid getNullable(String str) {
            return Jid.getNullable(str);
        }

        public static int getType(Jid jid) {
            return jid.getType();
        }
    }

    /* loaded from: classes2.dex */
    public class RecordingInfo {
        public File outputFile;
        public OutputStream outputStream;

        public RecordingInfo(C14330lG r7, DebugTapType debugTapType) {
            String str;
            switch (debugTapType.ordinal()) {
                case 0:
                    str = "received.decoded";
                    break;
                case 1:
                    str = "record.processed";
                    break;
                case 2:
                    str = "record.encoded";
                    break;
                case 3:
                    str = "record.raw";
                    break;
                case 4:
                    str = "playback.raw";
                    break;
                default:
                    throw C12960it.A0U(C12960it.A0b("unknown debug tap type: ", debugTapType));
            }
            String format = Voip.A02.format(new Date(System.currentTimeMillis()));
            r7.A04();
            StringBuilder A0j = C12960it.A0j(format);
            A0j.append(".");
            A0j.append(str);
            File file = new File((File) null, C12960it.A0d(".wav.gz", A0j));
            this.outputFile = file;
            try {
                this.outputStream = new GZIPOutputStream(new FileOutputStream(file, true));
            } catch (IOException e) {
                Log.e(e);
                this.outputStream = null;
            }
        }

        public File getOutputFile() {
            return this.outputFile;
        }

        public OutputStream getOutputStream() {
            return this.outputStream;
        }
    }
}
