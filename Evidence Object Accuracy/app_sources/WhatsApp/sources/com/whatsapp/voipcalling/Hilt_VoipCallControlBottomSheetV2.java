package com.whatsapp.voipcalling;

import X.AbstractC009404s;
import X.AbstractC15710nm;
import X.AbstractC18030rn;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12F;
import X.AnonymousClass130;
import X.C14820m6;
import X.C14850m9;
import X.C15550nR;
import X.C15610nY;
import X.C17140qK;
import X.C18360sK;
import X.C20710wC;
import X.C21250x7;
import X.C21270x9;
import X.C236312k;
import X.C36811kg;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_VoipCallControlBottomSheetV2 extends BottomSheetDialogFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1J();
        return this.A00;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A1J()
            r3.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.Hilt_VoipCallControlBottomSheetV2.A0u(android.app.Activity):void");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1J();
        A1I();
    }

    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = (VoipCallControlBottomSheetV2) this;
            C51112Sw r2 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r3 = r2.A0Y;
            voipCallControlBottomSheetV2.A0U = (C14850m9) r3.A04.get();
            voipCallControlBottomSheetV2.A0Y = (C236312k) r3.AMY.get();
            voipCallControlBottomSheetV2.A0T = (C21250x7) r3.AJh.get();
            voipCallControlBottomSheetV2.A0V = (C20710wC) r3.A8m.get();
            voipCallControlBottomSheetV2.A0N = (C15550nR) r3.A45.get();
            voipCallControlBottomSheetV2.A0P = (AnonymousClass01d) r3.ALI.get();
            voipCallControlBottomSheetV2.A0O = (C15610nY) r3.AMe.get();
            voipCallControlBottomSheetV2.A0S = (AnonymousClass018) r3.ANb.get();
            voipCallControlBottomSheetV2.A0X = (C17140qK) r3.AMZ.get();
            voipCallControlBottomSheetV2.A0R = (C14820m6) r3.AN3.get();
            voipCallControlBottomSheetV2.A0Q = (C18360sK) r3.AN0.get();
            C36811kg r4 = new C36811kg(AbstractC18030rn.A00(r3.AO3), (AnonymousClass130) r3.A41.get(), (C21270x9) r3.A4A.get(), (C14820m6) r3.AN3.get(), (C14850m9) r3.A04.get());
            r4.A01 = (AbstractC15710nm) r3.A4o.get();
            r4.A06 = (C21250x7) r3.AJh.get();
            r4.A03 = (C15550nR) r3.A45.get();
            r4.A05 = (AnonymousClass01d) r3.ALI.get();
            r4.A04 = (C15610nY) r3.AMe.get();
            r4.A07 = (C20710wC) r3.A8m.get();
            r4.A09 = (AnonymousClass12F) r3.AJM.get();
            voipCallControlBottomSheetV2.A0G = r4;
            voipCallControlBottomSheetV2.A0K = r2.A0V.A04();
        }
    }

    public final void A1J() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
