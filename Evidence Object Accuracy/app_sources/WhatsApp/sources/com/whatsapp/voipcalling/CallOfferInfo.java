package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CallOfferInfo {
    public final CallGroupInfo callGroupInfo;
    public final String callId;
    public final String callLinkToken;
    public final long epochTimeMillis;
    public final UserJid fromJid;
    public final GroupJid groupJid;
    public final boolean isJoinableCall;
    public final boolean isVideoCall;
    public final boolean resume;
    public final boolean uploadFieldStat;

    public CallOfferInfo(UserJid userJid, GroupJid groupJid, String str, long j, boolean z, CallGroupInfo callGroupInfo, boolean z2, boolean z3, boolean z4, String str2) {
        this.fromJid = userJid;
        this.groupJid = groupJid;
        this.callId = str;
        this.epochTimeMillis = j;
        this.isVideoCall = z;
        this.callGroupInfo = callGroupInfo;
        this.resume = z2;
        this.uploadFieldStat = z3;
        this.isJoinableCall = z4;
        this.callLinkToken = str2;
    }

    public static CallOfferInfo create(UserJid userJid, GroupJid groupJid, String str, long j, boolean z, CallGroupInfo callGroupInfo, boolean z2, boolean z3, boolean z4, String str2) {
        if (str != null) {
            return new CallOfferInfo(userJid, groupJid, str, j, z, callGroupInfo, z2, z3, z4, str2);
        }
        AnonymousClass009.A07("callId shouldn't be null");
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        if (r2.equals(r3.groupJid) == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.whatsapp.jid.GroupJid getGroupJid(X.C14850m9 r5) {
        /*
            r4 = this;
            com.whatsapp.voipcalling.CallInfo r3 = com.whatsapp.voipcalling.Voip.getCallInfo()
            com.whatsapp.jid.GroupJid r2 = r4.groupJid
            if (r2 == 0) goto L_0x0019
            if (r3 == 0) goto L_0x0019
            com.whatsapp.voipcalling.Voip$CallState r1 = r3.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.CALLING
            if (r1 == r0) goto L_0x0019
            com.whatsapp.jid.GroupJid r0 = r3.groupJid
            boolean r0 = r2.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r1 = 0
        L_0x001a:
            boolean r0 = X.AnonymousClass1SF.A0P(r5)
            if (r0 == 0) goto L_0x0025
            if (r1 != 0) goto L_0x0025
            com.whatsapp.jid.GroupJid r0 = r4.groupJid
            return r0
        L_0x0025:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.CallOfferInfo.getGroupJid(X.0m9):com.whatsapp.jid.GroupJid");
    }

    public boolean isJoinableGroupCall() {
        return this.isJoinableCall && this.callGroupInfo != null;
    }
}
