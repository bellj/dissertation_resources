package com.whatsapp.voipcalling;

import X.AnonymousClass009;
import X.AnonymousClass1S6;
import X.AnonymousClass1S7;
import X.C15380n4;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.Voip;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class CallInfo {
    public long ECMode;
    public long audioDuration;
    public long bytesReceived;
    public long bytesSent;
    public long callActiveTime;
    public long callDuration;
    public boolean callEnding;
    public final String callId;
    public UserJid callLinkCreatorJid;
    public String callLinkToken;
    public int callResult;
    public int callSetupErrorType;
    public final Voip.CallState callState;
    public AnonymousClass1S7 callWaitingInfo;
    public int connectedLimit;
    public final UserJid creatorJid;
    public final GroupJid groupJid;
    public int initialGroupTransactionId;
    public final UserJid initialPeerJid;
    public boolean isCallLinkLobbyState;
    public boolean isCaller;
    public boolean isEndedByMe;
    public boolean isGroupCall;
    public boolean isGroupCallCreatedOnServer;
    public boolean isGroupCallEnabled;
    public final boolean isJoinableGroupCall;
    public final Map participants = new LinkedHashMap();
    public final UserJid peerJid;
    public AnonymousClass1S6 self;
    public final String tsLogCallId;
    public boolean videoCaptureStarted;
    public long videoDuration;
    public boolean videoEnabled;
    public boolean videoPreviewReady;

    public CallInfo(Voip.CallState callState, String str, String str2, UserJid userJid, UserJid userJid2, UserJid userJid3, GroupJid groupJid, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, int i, int i2, boolean z7, boolean z8, boolean z9, long j, long j2, long j3, long j4, long j5, long j6, long j7, int i3, int i4, boolean z10, String str3) {
        this.callState = callState;
        this.callId = str;
        this.tsLogCallId = str2;
        this.peerJid = userJid;
        this.initialPeerJid = userJid2;
        this.creatorJid = userJid3;
        this.groupJid = groupJid;
        this.isCaller = z;
        this.isGroupCall = z2;
        this.isGroupCallEnabled = z3;
        this.isGroupCallCreatedOnServer = z4;
        this.videoEnabled = z5;
        this.isEndedByMe = z6;
        this.callResult = i;
        this.callSetupErrorType = i2;
        this.callEnding = z7;
        this.videoPreviewReady = z8;
        this.videoCaptureStarted = z9;
        this.callActiveTime = j;
        this.callDuration = j2;
        this.audioDuration = j3;
        this.videoDuration = j4;
        this.bytesSent = j5;
        this.bytesReceived = j6;
        this.ECMode = j7;
        this.initialGroupTransactionId = i3;
        this.connectedLimit = i4;
        this.isJoinableGroupCall = z10;
        this.callLinkToken = str3;
    }

    private void addParticipantInfo(AnonymousClass1S6 r3) {
        this.participants.put(r3.A06, r3);
        if (r3.A0F) {
            this.self = r3;
        }
    }

    private void addParticipantInfo(UserJid userJid, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, int i2, boolean z6, boolean z7, boolean z8, boolean z9, int i3, int i4, int i5, boolean z10, boolean z11, boolean z12, boolean z13, boolean z14, boolean z15, int i6) {
        boolean z16 = true;
        if (i < 1 || (i > 7 && i != 11)) {
            z16 = false;
        }
        StringBuilder sb = new StringBuilder("invalid participant state ");
        sb.append(i);
        AnonymousClass009.A0A(sb.toString(), z16);
        AnonymousClass1S6 r2 = new AnonymousClass1S6(userJid, i, i2, i3, i4, i5, i6, z, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z15);
        this.participants.put(userJid, r2);
        if (r2.A0F) {
            this.self = r2;
        }
    }

    public static CallInfo convertCallLinkInfoToCallInfo(CallLinkInfo callLinkInfo) {
        AnonymousClass1S6 r1 = callLinkInfo.self;
        CallInfo callInfo = new CallInfo(Voip.CallState.LINK, CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, null, null, null, null, null, false, false, false, false, callLinkInfo.videoEnabled, false, 0, 0, false, r1.A0K, false, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, callLinkInfo.token);
        callInfo.addParticipantInfo(r1);
        callInfo.callLinkCreatorJid = callLinkInfo.creatorJid;
        callInfo.isCallLinkLobbyState = true;
        return callInfo;
    }

    public static CallInfo convertCallWaitingInfoToCallInfo(CallInfo callInfo) {
        Voip.CallState callState;
        AnonymousClass1S7 r3 = callInfo.callWaitingInfo;
        boolean z = true;
        if (r3.A01 == 1) {
            callState = Voip.CallState.RECEIVED_CALL;
        } else {
            callState = Voip.CallState.ACTIVE;
        }
        UserJid userJid = r3.A03.initialPeerJid;
        AnonymousClass009.A05(userJid);
        String str = r3.A04;
        List<UserJid> list = r3.A06;
        UserJid userJid2 = (UserJid) list.get(0);
        GroupJid groupJid = r3.A02;
        if (r3.A00 <= 1) {
            z = false;
        }
        boolean z2 = callInfo.isGroupCallEnabled;
        boolean z3 = r3.A08;
        CallInfo callInfo2 = new CallInfo(callState, str, null, userJid, userJid, userJid2, groupJid, false, z, z2, false, z3, false, 0, 0, false, false, false, 0, 0, 0, 0, 0, 0, 0, -1, 0, r3.A07, r3.A05);
        AnonymousClass1S6 r0 = callInfo.self;
        callInfo2.addParticipantInfo(r0.A06, r0.A01, true, false, r0.A09, r0.A0D, r0.A0A, 1, r0.A0K, r0.A0J, r0.A0I, r0.A0H, r0.A05, r0.A02, r0.A03 * 90, r0.A07, r0.A08, r0.A0G, false, false, false, 0);
        for (UserJid userJid3 : list) {
            callInfo2.addParticipantInfo(userJid3, 2, false, false, false, false, false, z3 ? 1 : 0, false, false, false, false, 0, 0, 0, false, false, false, false, false, false, 0);
        }
        callInfo2.setCallWaitingInfo(false, 0, "", 0, new String[0], null, false, false, 0, 0, false, null, false, null);
        return callInfo2;
    }

    public boolean enableAudioVideoSwitch() {
        return this.self.A07;
    }

    public long getAudioDuration() {
        return this.audioDuration;
    }

    public long getBytesReceived() {
        return this.bytesReceived;
    }

    public long getBytesSent() {
        return this.bytesSent;
    }

    public long getCallActiveTime() {
        return this.callActiveTime;
    }

    public long getCallDuration() {
        return this.callDuration;
    }

    public String getCallId() {
        return this.callId;
    }

    public UserJid getCallLinkCreatorJid() {
        return this.callLinkCreatorJid;
    }

    public String getCallLinkToken() {
        return this.callLinkToken;
    }

    public int getCallResult() {
        return this.callResult;
    }

    public int getCallSetupErrorType() {
        return this.callSetupErrorType;
    }

    public Voip.CallState getCallState() {
        return this.callState;
    }

    public AnonymousClass1S7 getCallWaitingInfo() {
        return this.callWaitingInfo;
    }

    public int getConnectedParticipantsCount() {
        int i = 0;
        for (Map.Entry entry : this.participants.entrySet()) {
            if (((AnonymousClass1S6) entry.getValue()).A01 == 1) {
                i++;
            }
        }
        return i;
    }

    public UserJid getCreatorJid() {
        boolean z = this.isCallLinkLobbyState;
        UserJid userJid = this.creatorJid;
        if (!z) {
            AnonymousClass009.A05(userJid);
        }
        return userJid;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1S6 getDefaultPeerInfo() {
        /*
            r5 = this;
            boolean r0 = r5.isGroupCall
            r4 = 0
            if (r0 != 0) goto L_0x002a
            java.util.Map r0 = r5.participants
            java.util.Collection r0 = r0.values()
            java.util.Iterator r3 = r0.iterator()
        L_0x000f:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x002a
            java.lang.Object r2 = r3.next()
            X.1S6 r2 = (X.AnonymousClass1S6) r2
            boolean r0 = r2.A0F
            if (r0 == 0) goto L_0x0029
            com.whatsapp.jid.UserJid r1 = r2.A06
            com.whatsapp.jid.UserJid r0 = r5.peerJid
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000f
        L_0x0029:
            return r2
        L_0x002a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.CallInfo.getDefaultPeerInfo():X.1S6");
    }

    public long getECMode() {
        return this.ECMode;
    }

    public GroupJid getGroupJid() {
        return this.groupJid;
    }

    public AnonymousClass1S6 getInfoByJid(UserJid userJid) {
        return (AnonymousClass1S6) this.participants.get(userJid);
    }

    public UserJid getInitialPeerJid() {
        if (this.isCallLinkLobbyState) {
            return null;
        }
        UserJid userJid = this.initialPeerJid;
        AnonymousClass009.A05(userJid);
        return userJid;
    }

    public Set getParticipantJids() {
        return this.participants.keySet();
    }

    public Map getParticipants() {
        return this.participants;
    }

    public UserJid getPeerJid() {
        if (this.isCallLinkLobbyState) {
            return null;
        }
        UserJid userJid = this.peerJid;
        AnonymousClass009.A05(userJid);
        return userJid;
    }

    public AnonymousClass1S6 getSelfInfo() {
        return this.self;
    }

    public String getTSLogCallId() {
        return this.tsLogCallId;
    }

    public long getVideoDuration() {
        return this.videoDuration;
    }

    public boolean hasOutgoingParticipantInActiveOneToOneCall() {
        if (this.callState != Voip.CallState.ACTIVE || getDefaultPeerInfo() == null) {
            return false;
        }
        int i = getDefaultPeerInfo().A01;
        return i == 2 || i == 3;
    }

    public int initialGroupTransactionId() {
        return this.initialGroupTransactionId;
    }

    public boolean isCallEnding() {
        return this.callEnding;
    }

    public boolean isCallFull() {
        return this.connectedLimit > 0 && getConnectedParticipantsCount() >= this.connectedLimit;
    }

    public boolean isCallLinkLobbyState() {
        return this.isCallLinkLobbyState;
    }

    public boolean isCallOnHold() {
        if (!this.isCallLinkLobbyState && !isInLonelyState()) {
            if (!this.self.A09) {
                for (AnonymousClass1S6 r1 : this.participants.values()) {
                    if (r1.A0F || r1.A09) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public boolean isCaller() {
        return this.isCaller;
    }

    public boolean isEitherSideRequestingUpgrade() {
        AnonymousClass1S6 r0;
        if (isGroupCall() || this.callState == Voip.CallState.NONE || (r0 = this.self) == null) {
            return false;
        }
        if (r0.A04 == 3 || isPeerRequestingUpgrade()) {
            return true;
        }
        return false;
    }

    public boolean isEndedByMe() {
        return this.isEndedByMe;
    }

    public boolean isGroupCall() {
        return this.isGroupCall || this.callLinkToken != null;
    }

    public boolean isGroupCallCreatedOnServer() {
        return this.isGroupCallCreatedOnServer;
    }

    public boolean isGroupCallEnabled() {
        return this.isGroupCallEnabled;
    }

    public boolean isInLonelyState() {
        Voip.CallState callState = this.callState;
        if (callState != Voip.CallState.CONNECTED_LONELY) {
            if (callState == Voip.CallState.ACTIVE) {
                for (AnonymousClass1S6 r1 : this.participants.values()) {
                    if (r1.A01 != 1 || r1.A0F) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    public boolean isJoinableGroupCall() {
        return this.isJoinableGroupCall;
    }

    public boolean isPeerRequestingUpgrade() {
        AnonymousClass1S6 defaultPeerInfo = getDefaultPeerInfo();
        if (defaultPeerInfo == null || defaultPeerInfo.A04 != 3) {
            return false;
        }
        return true;
    }

    public boolean isSelfCallOnHold() {
        return this.self.A09;
    }

    public boolean isSelfRequestingUpgrade() {
        AnonymousClass1S6 r2;
        if (this.isGroupCall || (r2 = this.self) == null || this.callState == Voip.CallState.NONE || r2.A04 != 3) {
            return false;
        }
        return true;
    }

    public boolean isStartedFromCallLink() {
        return this.callLinkToken != null;
    }

    public boolean isVideoCaptureStarted() {
        return this.videoCaptureStarted;
    }

    public boolean isVideoEnabled() {
        return this.videoEnabled;
    }

    public boolean isVideoPreviewReady() {
        return this.videoPreviewReady;
    }

    private void setCallLinkCreatorJid(UserJid userJid) {
        this.callLinkCreatorJid = userJid;
    }

    private void setCallWaitingInfo(boolean z, int i, String str, int i2, String[] strArr, String str2, boolean z2, boolean z3, int i3, int i4, boolean z4, Voip.CallLogInfo callLogInfo, boolean z5, String str3) {
        this.callWaitingInfo = new AnonymousClass1S7(callLogInfo, str, str2, str3, C15380n4.A07(UserJid.class, Arrays.asList(strArr)), i, i2, z2, z5);
    }

    private void setIsCallLinkLobbyState(boolean z) {
        this.isCallLinkLobbyState = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallId: ");
        sb.append(this.callId);
        sb.append(", peerJid: ");
        sb.append(this.peerJid);
        sb.append(", callState: ");
        sb.append(this.callState);
        return sb.toString();
    }
}
