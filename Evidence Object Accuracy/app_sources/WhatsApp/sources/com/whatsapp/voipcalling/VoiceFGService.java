package com.whatsapp.voipcalling;

import X.AnonymousClass1JK;
import X.C20970wc;
import X.C26391De;
import android.app.Notification;
import android.content.Intent;
import android.os.IBinder;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class VoiceFGService extends AnonymousClass1JK {
    public static volatile Notification A02;
    public C20970wc A00;
    public boolean A01 = false;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public VoiceFGService() {
        super("voicefgservice", true);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        Log.i("voicefgservice/onCreate");
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("voicefgservice/onDestroy");
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        StringBuilder sb = new StringBuilder("voicefgservice/onStartCommand:");
        sb.append(intent);
        Log.i(sb.toString());
        if (intent != null) {
            String action = intent.getAction();
            if ("hangup_call".equals(action) || "reject_call".equals(action)) {
                this.A00.A00(new C26391De(action, intent.getExtras()));
                return 2;
            } else if (!"com.whatsapp.service.VoiceFgService.START".equals(action) || A02 == null) {
                StringBuilder sb2 = new StringBuilder("voicefgservice/onStartCommand service started with unknown action:");
                sb2.append(intent.getAction());
                Log.e(sb2.toString());
                return 2;
            } else {
                if (intent.getBooleanExtra("com.whatsapp.service.VoiceFgService.EXTRA_STOP_FOREGROUND_STATE", false)) {
                    stopForeground(true);
                }
                A01(i2, A02, intent.getIntExtra("com.whatsapp.service.VoiceFgService.EXTRA_NOTIFICATION_ID", 23));
            }
        }
        return 2;
    }
}
