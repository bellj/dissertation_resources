package com.whatsapp.voipcalling;

import X.AnonymousClass004;
import X.AnonymousClass5B7;
import X.C12960it;
import X.C12970iu;
import X.C237612x;
import X.C71083cM;
import android.content.Intent;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.DisconnectCause;
import android.telecom.PhoneAccountHandle;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class SelfManagedConnectionService extends ConnectionService implements AnonymousClass004 {
    public C237612x A00;
    public boolean A01;
    public final Object A02;
    public volatile C71083cM A03;

    public SelfManagedConnectionService() {
        this(0);
    }

    public SelfManagedConnectionService(int i) {
        this.A02 = C12970iu.A0l();
        this.A01 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    this.A03 = new C71083cM(this);
                }
            }
        }
        return this.A03.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A01) {
            this.A01 = true;
            ((AnonymousClass5B7) generatedComponent()).A01(this);
        }
        super.onCreate();
        Log.i("voip/SelfManagedConnectionService/onCreate");
    }

    @Override // android.telecom.ConnectionService
    public Connection onCreateIncomingConnection(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onCreateIncomingConnection ", connectionRequest));
        Connection A02 = this.A00.A02(connectionRequest, false);
        if (A02 == null) {
            return Connection.createFailedConnection(new DisconnectCause(1));
        }
        return A02;
    }

    @Override // android.telecom.ConnectionService
    public void onCreateIncomingConnectionFailed(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onCreateIncomingConnectionFailed ", connectionRequest));
        this.A00.A06(connectionRequest);
    }

    @Override // android.telecom.ConnectionService
    public Connection onCreateOutgoingConnection(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onCreateOutgoingConnection ", connectionRequest));
        Connection A02 = this.A00.A02(connectionRequest, true);
        if (A02 == null) {
            return Connection.createFailedConnection(new DisconnectCause(1));
        }
        return A02;
    }

    @Override // android.telecom.ConnectionService
    public void onCreateOutgoingConnectionFailed(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onCreateOutgoingConnectionFailed ", connectionRequest));
        this.A00.A07(connectionRequest);
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        Log.i("voip/SelfManagedConnectionService/onDestroy");
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onStartCommand ", intent));
        return super.onStartCommand(intent, i, i2);
    }

    @Override // android.telecom.ConnectionService, android.app.Service
    public boolean onUnbind(Intent intent) {
        Log.i(C12960it.A0b("voip/SelfManagedConnectionService/onUnbind ", intent));
        return super.onUnbind(intent);
    }
}
