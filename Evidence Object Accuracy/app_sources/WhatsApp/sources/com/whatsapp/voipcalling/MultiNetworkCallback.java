package com.whatsapp.voipcalling;

import X.AnonymousClass2CS;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;

/* loaded from: classes3.dex */
public class MultiNetworkCallback {
    public final AnonymousClass2CS provider;

    public MultiNetworkCallback(AnonymousClass2CS r1) {
        this.provider = r1;
    }

    public void closeAlternativeSocket(boolean z) {
        AnonymousClass2CS r3 = this.provider;
        r3.A05.execute(new RunnableBRunnable0Shape0S0110000_I0(r3, 21, z));
    }

    public void createAlternativeSocket(boolean z, boolean z2) {
        AnonymousClass2CS r2 = this.provider;
        r2.A05.execute(new Runnable(z, z2) { // from class: X.3lA
            public final /* synthetic */ boolean A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass2CS.A06(AnonymousClass2CS.this, this.A01, this.A02);
            }
        });
    }
}
