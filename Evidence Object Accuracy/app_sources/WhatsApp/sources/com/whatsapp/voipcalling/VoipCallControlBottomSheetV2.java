package com.whatsapp.voipcalling;

import X.AbstractC001400p;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass0KS;
import X.AnonymousClass1L7;
import X.AnonymousClass1SF;
import X.AnonymousClass2CT;
import X.AnonymousClass2ID;
import X.AnonymousClass3DO;
import X.AnonymousClass3NE;
import X.AnonymousClass4LR;
import X.C004902f;
import X.C14820m6;
import X.C14850m9;
import X.C14960mK;
import X.C15550nR;
import X.C15610nY;
import X.C17140qK;
import X.C18360sK;
import X.C20710wC;
import X.C21250x7;
import X.C236312k;
import X.C36811kg;
import X.C42941w9;
import X.C48782Ht;
import X.C50102Ob;
import X.C52562bJ;
import X.C64523Fw;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.ViewOnClickCListenerShape0S0110000_I0;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.google.android.material.button.MaterialButton;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.calling.controls.viewmodel.BottomSheetViewModel;
import com.whatsapp.calling.controls.viewmodel.CallControlButtonsViewModel;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout;
import com.whatsapp.calling.views.VoipCallControlBottomSheetDragIndicator;
import com.whatsapp.calling.views.VoipCallFooter;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;
import java.util.List;

/* loaded from: classes2.dex */
public class VoipCallControlBottomSheetV2 extends Hilt_VoipCallControlBottomSheetV2 {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public FrameLayout A0A;
    public NestedScrollView A0B;
    public RecyclerView A0C;
    public MaterialButton A0D;
    public MaterialButton A0E;
    public C64523Fw A0F;
    public C36811kg A0G;
    public BottomSheetViewModel A0H;
    public CallControlButtonsViewModel A0I;
    public ParticipantsListViewModel A0J;
    public AnonymousClass2ID A0K;
    public VoipCallControlBottomSheetDragIndicator A0L;
    public VoipCallFooter A0M;
    public C15550nR A0N;
    public C15610nY A0O;
    public AnonymousClass01d A0P;
    public C18360sK A0Q;
    public C14820m6 A0R;
    public AnonymousClass018 A0S;
    public C21250x7 A0T;
    public C14850m9 A0U;
    public C20710wC A0V;
    public AnonymousClass2CT A0W;
    public C17140qK A0X;
    public C236312k A0Y;
    public boolean A0Z;
    public boolean A0a;

    public static /* synthetic */ int A00(VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2) {
        ActivityC000900k A0B;
        if (Build.VERSION.SDK_INT >= 24 && (A0B = voipCallControlBottomSheetV2.A0B()) != null && A0B.isInMultiWindowMode()) {
            return 0;
        }
        int identifier = voipCallControlBottomSheetV2.A02().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return voipCallControlBottomSheetV2.A02().getDimensionPixelSize(identifier);
        }
        return 25;
    }

    public static VoipCallControlBottomSheetV2 A01(boolean z) {
        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = new VoipCallControlBottomSheetV2();
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_video_call", z);
        voipCallControlBottomSheetV2.A0U(bundle);
        return voipCallControlBottomSheetV2;
    }

    public static /* synthetic */ void A02(DialogInterface dialogInterface, VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2) {
        CallInfo callInfo;
        VoipActivityV2 voipActivityV2;
        CallInfo A2i;
        AnonymousClass2CT r0 = voipCallControlBottomSheetV2.A0W;
        if (!(r0 == null || (A2i = (voipActivityV2 = r0.A00).A2i()) == null || A2i.isCallLinkLobbyState)) {
            UserJid peerJid = A2i.getPeerJid();
            AnonymousClass009.A05(peerJid);
            boolean z = A2i.videoEnabled;
            int i = -1;
            if (A2i.isPeerRequestingUpgrade()) {
                i = 2;
            }
            voipActivityV2.A3U(peerJid, i, z);
        }
        View view = voipCallControlBottomSheetV2.A09;
        AnonymousClass009.A03(view);
        view.setVisibility(0);
        Dialog dialog = (Dialog) dialogInterface;
        View A00 = AnonymousClass0KS.A00(dialog, R.id.design_bottom_sheet);
        voipCallControlBottomSheetV2.A06 = A00;
        voipCallControlBottomSheetV2.A0F = new C64523Fw(A00, voipCallControlBottomSheetV2.A08, voipCallControlBottomSheetV2);
        voipCallControlBottomSheetV2.A0K.Abq(voipCallControlBottomSheetV2.A06);
        voipCallControlBottomSheetV2.A0K.AB7().A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R7
            /* JADX WARNING: Code restructure failed: missing block: B:17:0x004f, code lost:
                if (r2 != 2) goto L_0x0051;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r8) {
                /*
                // Method dump skipped, instructions count: 295
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3R7.ANq(java.lang.Object):void");
            }
        });
        voipCallControlBottomSheetV2.A06.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nu
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                AnonymousClass2ID.this.onGlobalLayout();
            }
        });
        voipCallControlBottomSheetV2.A0J.A05.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R5
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                List list = (List) obj;
                ActivityC000900k A0B = voipCallControlBottomSheetV22.A0B();
                if (A0B == null || !C36021jC.A03(A0B)) {
                    voipCallControlBottomSheetV22.A0G.A0F(list);
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A06.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R8
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                boolean A1Y = C12970iu.A1Y(obj);
                StringBuilder A0k = C12960it.A0k("voip/VoipCallControlBottomSheetV2 onBottomSheetHidableChanged ");
                A0k.append(A1Y);
                C12960it.A1F(A0k);
                C64523Fw r02 = voipCallControlBottomSheetV22.A0F;
                if (r02 != null) {
                    r02.A0F.A0J = A1Y;
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A09.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R3
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                VoipCallControlBottomSheetDragIndicator voipCallControlBottomSheetDragIndicator;
                int i2;
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                boolean A1Y = C12970iu.A1Y(obj);
                StringBuilder A0k = C12960it.A0k("voip/VoipCallControlBottomSheetV2 isInLobbyChanged ");
                A0k.append(A1Y);
                C12960it.A1F(A0k);
                Dialog dialog2 = ((DialogFragment) voipCallControlBottomSheetV22).A03;
                if (!(dialog2 == null || dialog2.getWindow() == null)) {
                    View decorView = ((DialogFragment) voipCallControlBottomSheetV22).A03.getWindow().getDecorView();
                    if (A1Y) {
                        i2 = voipCallControlBottomSheetV22.A04;
                    } else {
                        i2 = voipCallControlBottomSheetV22.A03;
                    }
                    decorView.setBackgroundColor(i2);
                    voipCallControlBottomSheetV22.A1N(0.0f);
                }
                C64523Fw r1 = voipCallControlBottomSheetV22.A0F;
                if (r1 != null) {
                    r1.A0A = A1Y;
                    r1.A02(4);
                    r1.A00();
                    r1.A01();
                }
                NestedScrollView nestedScrollView = voipCallControlBottomSheetV22.A0B;
                if (nestedScrollView != null) {
                    nestedScrollView.scrollTo(0, 0);
                }
                if (!A1Y && (voipCallControlBottomSheetDragIndicator = voipCallControlBottomSheetV22.A0L) != null && !voipCallControlBottomSheetV22.A0H.A02) {
                    voipCallControlBottomSheetDragIndicator.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(voipCallControlBottomSheetV22, 42), 750);
                }
                AnonymousClass2CT r12 = voipCallControlBottomSheetV22.A0W;
                if (r12 != null) {
                    r12.A00(true);
                    AnonymousClass2CT r5 = voipCallControlBottomSheetV22.A0W;
                    StringBuilder A0k2 = C12960it.A0k("voip/VoipActivityV2/onIsInLobbyChanged/isInLobby ");
                    A0k2.append(A1Y);
                    C12960it.A1F(A0k2);
                    VoipActivityV2 voipActivityV22 = r5.A00;
                    CallInfo A2i2 = voipActivityV22.A2i();
                    if (A2i2 != null && A2i2.videoEnabled && !A2i2.isSelfRequestingUpgrade() && !A1Y) {
                        voipActivityV22.A0O.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(r5, 40), 750);
                    } else if (!voipActivityV22.A1i) {
                        voipActivityV22.A0j.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(r5, 41), 750);
                    }
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A0A.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R4
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                boolean A1Y = C12970iu.A1Y(obj);
                if (voipCallControlBottomSheetV22.A08 != null && voipCallControlBottomSheetV22.A0F != null && voipCallControlBottomSheetV22.A0L != null) {
                    StringBuilder A0k = C12960it.A0k("voip/VoipCallControlBottomSheetV2 updateUiForAVSwitch ");
                    A0k.append(A1Y);
                    C12960it.A1F(A0k);
                    int i2 = 0;
                    if (A1Y != C12960it.A1T(voipCallControlBottomSheetV22.A08.getVisibility())) {
                        View view2 = voipCallControlBottomSheetV22.A08;
                        if (!A1Y) {
                            i2 = 8;
                        }
                        view2.setVisibility(i2);
                        C64523Fw r02 = voipCallControlBottomSheetV22.A0F;
                        r02.A0B = A1Y;
                        r02.A00();
                        if (AnonymousClass23N.A05(voipCallControlBottomSheetV22.A0P.A0P()) && A1Y) {
                            voipCallControlBottomSheetV22.A0F.A02(4);
                            voipCallControlBottomSheetV22.A08.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(voipCallControlBottomSheetV22, 43), 750);
                        }
                        AnonymousClass2CT r03 = voipCallControlBottomSheetV22.A0W;
                        if (r03 != null) {
                            r03.A00(true);
                        }
                    }
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A03.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.4u0
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                boolean A1Y = C12970iu.A1Y(obj);
                VoipCallControlBottomSheetDragIndicator voipCallControlBottomSheetDragIndicator = voipCallControlBottomSheetV22.A0L;
                if (voipCallControlBottomSheetDragIndicator != null) {
                    voipCallControlBottomSheetDragIndicator.setVisibility(C12960it.A02(A1Y ? 1 : 0));
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A05.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.4tz
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C64523Fw r02;
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                boolean A1Y = C12970iu.A1Y(obj);
                RecyclerView recyclerView = voipCallControlBottomSheetV22.A0C;
                if (recyclerView != null) {
                    recyclerView.setVisibility(C12960it.A02(A1Y ? 1 : 0));
                    if (!A1Y && (r02 = voipCallControlBottomSheetV22.A0F) != null) {
                        r02.A00();
                        voipCallControlBottomSheetV22.A0F.A01();
                    }
                }
            }
        });
        voipCallControlBottomSheetV2.A0H.A04.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.4u1
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C64523Fw r1 = C64523Fw.this;
                r1.A09 = C12970iu.A1Y(obj);
                r1.A00();
                r1.A01();
            }
        });
        voipCallControlBottomSheetV2.A0I.A01.A05(voipCallControlBottomSheetV2, new AnonymousClass02B() { // from class: X.3R6
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                int i2;
                MaterialButton materialButton;
                int i3;
                CallInfo A2i2;
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV22 = VoipCallControlBottomSheetV2.this;
                AnonymousClass3ES r11 = (AnonymousClass3ES) obj;
                ActivityC000900k A0B = voipCallControlBottomSheetV22.A0B();
                if ((A0B == null || !C36021jC.A03(A0B)) && voipCallControlBottomSheetV22.A0A != null && voipCallControlBottomSheetV22.A0W != null && voipCallControlBottomSheetV22.A09 != null) {
                    StringBuilder A0k = C12960it.A0k("voip/VoipCallControlBottomSheetV2 setCallControlButtons ");
                    int i4 = r11.A01;
                    A0k.append(i4);
                    C12960it.A1F(A0k);
                    voipCallControlBottomSheetV22.A0A.removeAllViews();
                    Object tag = voipCallControlBottomSheetV22.A0A.getTag();
                    int i5 = r11.A00;
                    Integer valueOf = Integer.valueOf(i5);
                    boolean z2 = false;
                    if (!C29941Vi.A00(tag, valueOf)) {
                        voipCallControlBottomSheetV22.A0A.setFocusable(false);
                        voipCallControlBottomSheetV22.A0A.setVisibility(8);
                        voipCallControlBottomSheetV22.A0A.getParent().requestLayout();
                        boolean A1V = C12960it.A1V(i5, 1);
                        View view2 = voipCallControlBottomSheetV22.A09;
                        int i6 = R.id.call_controls_btns_container;
                        if (A1V) {
                            i6 = R.id.call_controls_btns_container_landscape_flex;
                        }
                        voipCallControlBottomSheetV22.A0A = (FrameLayout) AnonymousClass028.A0D(view2, i6);
                        View view3 = voipCallControlBottomSheetV22.A07;
                        AnonymousClass009.A03(view3);
                        AnonymousClass064 r1 = (AnonymousClass064) view3.getLayoutParams();
                        float f = 1.0f;
                        if (A1V) {
                            f = 0.5f;
                        }
                        r1.A01 = f;
                        voipCallControlBottomSheetV22.A07.setLayoutParams(r1);
                        voipCallControlBottomSheetV22.A0A.setTag(valueOf);
                        voipCallControlBottomSheetV22.A0A.setFocusable(true);
                        voipCallControlBottomSheetV22.A0A.setVisibility(0);
                    }
                    if (i4 == 1) {
                        i2 = R.layout.voip_call_control_lobby_btns;
                    } else if (i4 == 2) {
                        i2 = R.layout.voip_call_control_in_call_btns;
                    } else if (i4 == 3) {
                        i2 = R.layout.call_link_lobby;
                    } else {
                        return;
                    }
                    View inflate = voipCallControlBottomSheetV22.A04().inflate(i2, (ViewGroup) voipCallControlBottomSheetV22.A0A, false);
                    inflate.setTag(r11);
                    voipCallControlBottomSheetV22.A0A.addView(inflate, new FrameLayout.LayoutParams(-1, -1, 17));
                    if (i4 == 1 || i4 == 3) {
                        voipCallControlBottomSheetV22.A0D = (MaterialButton) inflate.findViewById(R.id.negative_button);
                        MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.positive_button);
                        voipCallControlBottomSheetV22.A0E = materialButton2;
                        AnonymousClass04D.A09(materialButton2, 13, 16, 1, 2);
                        AnonymousClass04D.A09(voipCallControlBottomSheetV22.A0D, 13, 16, 1, 2);
                        AnonymousClass4WR r02 = r11.A03;
                        AnonymousClass009.A05(r02);
                        boolean z3 = r02.A00;
                        boolean equals = "com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN".equals(voipCallControlBottomSheetV22.A0W.A00.getIntent().getAction());
                        if (i4 == 3) {
                            z2 = true;
                        }
                        if (voipCallControlBottomSheetV22.A0E != null && (materialButton = voipCallControlBottomSheetV22.A0D) != null) {
                            int i7 = R.string.voip_joinable_ignore;
                            if (z2) {
                                i7 = R.string.call_link_leave_lobby;
                            }
                            materialButton.setText(i7);
                            voipCallControlBottomSheetV22.A0D.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(voipCallControlBottomSheetV22, 38));
                            voipCallControlBottomSheetV22.A0E.setIcon(null);
                            voipCallControlBottomSheetV22.A0E.setMaxLines(2);
                            MaterialButton materialButton3 = voipCallControlBottomSheetV22.A0E;
                            if (z3) {
                                i3 = R.string.voip_joinable_call_full_button;
                            } else if (equals) {
                                i3 = R.string.voip_joinable_group_call_waiting_end_and_join;
                            } else {
                                materialButton3.setMaxLines(1);
                                MaterialButton materialButton4 = voipCallControlBottomSheetV22.A0E;
                                String A0I = voipCallControlBottomSheetV22.A0I(R.string.voip_joinable_join);
                                Dialog dialog2 = ((DialogFragment) voipCallControlBottomSheetV22).A03;
                                if (!(dialog2 == null || dialog2.getContext() == null)) {
                                    TypedValue typedValue = new TypedValue();
                                    dialog2.getContext().getTheme().resolveAttribute(R.attr.voipLobbyPositiveButtonIcon, typedValue, true);
                                    Drawable A03 = C015607k.A03(AnonymousClass00T.A04(dialog2.getContext(), typedValue.resourceId));
                                    C015607k.A0A(A03, AnonymousClass00T.A00(dialog2.getContext(), R.color.white));
                                    materialButton4.setText(A0I);
                                    materialButton4.setIcon(A03);
                                }
                                voipCallControlBottomSheetV22.A0E.setOnClickListener(new ViewOnClickCListenerShape0S0110000_I0(voipCallControlBottomSheetV22, 1, z2));
                                return;
                            }
                            materialButton3.setText(i3);
                            voipCallControlBottomSheetV22.A0E.setOnClickListener(new ViewOnClickCListenerShape0S0110000_I0(voipCallControlBottomSheetV22, 1, z2));
                            return;
                        }
                        return;
                    }
                    voipCallControlBottomSheetV22.A0E = null;
                    voipCallControlBottomSheetV22.A0D = null;
                    C92604Wp r6 = r11.A02;
                    AnonymousClass009.A05(r6);
                    VoipCallFooter voipCallFooter = (VoipCallFooter) AnonymousClass028.A0D(inflate, R.id.footer);
                    voipCallControlBottomSheetV22.A0M = voipCallFooter;
                    ActivityC000900k A0B2 = voipCallControlBottomSheetV22.A0B();
                    if (A0B2 instanceof AnonymousClass1L3) {
                        AnonymousClass1L3 r2 = (AnonymousClass1L3) A0B2;
                        voipCallFooter.setMuteButtonClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r2, 25));
                        voipCallFooter.setToggleVideoButtonClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r2, 26));
                        voipCallFooter.setSpeakerButtonClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r2, 29));
                        voipCallFooter.setBluetoothButtonClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r2, 31));
                        voipCallFooter.setEndCallButtonClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r2, 33));
                        boolean z4 = r6.A01;
                        VoipCallFooter voipCallFooter2 = voipCallControlBottomSheetV22.A0M;
                        if (z4) {
                            voipCallFooter2.A01();
                        } else {
                            voipCallFooter2.A00();
                        }
                        voipCallControlBottomSheetV22.A0M.A01.setVisibility(0);
                        AnonymousClass2CT r03 = voipCallControlBottomSheetV22.A0W;
                        if (r03 != null && (A2i2 = r03.A00.A2i()) != null) {
                            r2.AfP(A2i2);
                            return;
                        }
                        return;
                    }
                    Log.w("voip/VoipCallControlBottomSheetV2/failed to get voip activity");
                    voipCallControlBottomSheetV22.A1C();
                }
            }
        });
        AnonymousClass0KS.A00(dialog, R.id.touch_outside).setOnTouchListener(new AnonymousClass3NE(voipCallControlBottomSheetV2));
        if (Build.VERSION.SDK_INT >= 21) {
            voipCallControlBottomSheetV2.A1K();
        }
        AnonymousClass2CT r02 = voipCallControlBottomSheetV2.A0W;
        if (r02 != null) {
            callInfo = r02.A00.A2i();
        } else {
            callInfo = null;
        }
        AnonymousClass2CT r1 = voipCallControlBottomSheetV2.A0W;
        if (r1 != null) {
            r1.A00(true);
        }
        if (callInfo != null) {
            voipCallControlBottomSheetV2.A0Y.A02(callInfo.callId, "voip_call_control_bottom_sheet_onshown");
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        Log.i("voip/VoipCallControlBottomSheetV2 onDetach");
        this.A0Z = false;
        RecyclerView recyclerView = this.A0C;
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        this.A0F = null;
        this.A06 = null;
        this.A0W = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00d8, code lost:
        if (r1 == false) goto L_0x00da;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A10(android.os.Bundle r8, android.view.LayoutInflater r9, android.view.ViewGroup r10) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipCallControlBottomSheetV2.A10(android.os.Bundle, android.view.LayoutInflater, android.view.ViewGroup):android.view.View");
    }

    @Override // com.whatsapp.voipcalling.Hilt_VoipCallControlBottomSheetV2, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        CallInfo A2i;
        super.A15(context);
        Log.i("voip/VoipCallControlBottomSheetV2 onAttach");
        try {
            this.A02 = (float) ViewConfiguration.get(context).getScaledTouchSlop();
            VoipActivityV2 voipActivityV2 = (VoipActivityV2) ((AnonymousClass1L7) context);
            AnonymousClass2CT r0 = voipActivityV2.A1M;
            if (r0 == null) {
                r0 = new AnonymousClass2CT(voipActivityV2);
                voipActivityV2.A1M = r0;
            }
            this.A0W = r0;
            AbstractC001400p r2 = (AbstractC001400p) context;
            ParticipantsListViewModel participantsListViewModel = (ParticipantsListViewModel) new AnonymousClass02A(r2).A00(ParticipantsListViewModel.class);
            this.A0J = participantsListViewModel;
            participantsListViewModel.A01 = this.A0W;
            if (!participantsListViewModel.A03) {
                participantsListViewModel.A03 = true;
                C48782Ht r02 = participantsListViewModel.A07;
                r02.A03(participantsListViewModel);
                participantsListViewModel.A04(r02.A05());
            }
            this.A0H = (BottomSheetViewModel) new AnonymousClass02A(r2).A00(BottomSheetViewModel.class);
            this.A0I = (CallControlButtonsViewModel) new AnonymousClass02A(r2).A00(CallControlButtonsViewModel.class);
            C36811kg r1 = this.A0G;
            r1.A0A = new AnonymousClass4LR(this);
            r1.A02 = this.A0J;
            AnonymousClass2CT r03 = this.A0W;
            if (r03 != null && (A2i = r03.A00.A2i()) != null) {
                this.A0Y.A02(A2i.callId, "voip_call_control_bottom_sheet_onattach");
            }
        } catch (ClassCastException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append(context);
            sb.append(" must implement VoipCallControlBottomSheet$HostProvider");
            throw new ClassCastException(sb.toString());
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        AnonymousClass009.A05(bundle2);
        boolean z = bundle2.getBoolean("is_video_call", false);
        int i = R.style.VoipCallControlVoiceBottomSheetM4;
        if (z) {
            i = R.style.VoipCallControlVideoBottomSheet;
        }
        A1D(0, i);
        this.A0a = !AnonymousClass1SF.A0N(this.A0R, this.A0U);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r4, String str) {
        CallGridViewModel callGridViewModel;
        AnonymousClass2CT r0 = this.A0W;
        if (r0 != null) {
            VoipActivityV2 voipActivityV2 = r0.A00;
            if (!(!voipActivityV2.A1m && voipActivityV2.A18 == null && ((callGridViewModel = voipActivityV2.A0p) == null || callGridViewModel.A07.A01() == null))) {
                Log.i("voip/VoipCallControlBottomSheetV2 bottom sheet action is disabled");
                return;
            }
        }
        if (this.A0Z) {
            Log.i("voip/VoipCallControlBottomSheetV2 show after attached");
            C64523Fw r02 = this.A0F;
            if (r02 != null && r02.A09()) {
                Dialog dialog = ((DialogFragment) this).A03;
                if (!(dialog == null || dialog.getWindow() == null)) {
                    View decorView = ((DialogFragment) this).A03.getWindow().getDecorView();
                    decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & -5);
                }
                this.A0F.A02(4);
                this.A0F.A06(true);
                this.A0F.A00();
                this.A0F.A01();
                return;
            }
            return;
        }
        this.A0Z = true;
        Log.i("voip/VoipCallControlBottomSheetV2 attaching bottom sheet");
        C004902f r03 = new C004902f(r4);
        r03.A09(this, str);
        r03.A03();
    }

    public final void A1K() {
        View view = this.A06;
        if (view != null && view.getContext() != null) {
            View view2 = this.A06;
            view2.setElevation(view2.getContext().getResources().getDimension(R.dimen.call_control_bottom_sheet_elevation));
            this.A06.setClipToOutline(true);
            this.A06.setOutlineProvider(new C52562bJ(this));
        }
    }

    public final void A1L() {
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null && dialog.getWindow() != null && this.A0a) {
            View decorView = ((DialogFragment) this).A03.getWindow().getDecorView();
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() | 256 | 4);
        }
    }

    public final void A1M() {
        C64523Fw r1;
        int i;
        boolean z;
        C64523Fw r12 = this.A0F;
        if (r12 != null) {
            if (r12.A08()) {
                z = false;
            } else if (!r12.A0F.A0J) {
                return;
            } else {
                if (r12.A09()) {
                    Dialog dialog = ((DialogFragment) this).A03;
                    if (!(dialog == null || dialog.getWindow() == null)) {
                        View decorView = ((DialogFragment) this).A03.getWindow().getDecorView();
                        decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & -5);
                    }
                    r12 = this.A0F;
                    z = true;
                } else {
                    A1L();
                    r1 = this.A0F;
                    if (r1.A0F.A0J) {
                        i = 5;
                        r1.A02(i);
                    }
                    return;
                }
            }
            r12.A06(z);
            r1 = this.A0F;
            i = 4;
            r1.A02(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000d, code lost:
        if (r0 == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1N(float r6) {
        /*
            r5 = this;
            X.3Fw r1 = r5.A0F
            r4 = 1
            r3 = 0
            if (r1 == 0) goto L_0x000f
            boolean r0 = r1.A06
            if (r0 == 0) goto L_0x000f
            boolean r0 = r1.A08
            r1 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            android.app.Dialog r0 = r5.A03
            if (r0 == 0) goto L_0x004a
            android.view.Window r0 = r0.getWindow()
            if (r0 == 0) goto L_0x004a
            if (r1 == 0) goto L_0x004e
            r0 = 0
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x004e
            com.whatsapp.calling.controls.viewmodel.BottomSheetViewModel r0 = r5.A0H
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x004e
        L_0x0027:
            android.app.Dialog r0 = r5.A03
            android.view.Window r0 = r0.getWindow()
            android.view.View r0 = r0.getDecorView()
            android.graphics.drawable.Drawable r2 = r0.getBackground()
            if (r4 == 0) goto L_0x0047
            boolean r0 = r5.A1R()
            if (r0 == 0) goto L_0x004b
            float r1 = r5.A01
        L_0x003f:
            r0 = 1132396544(0x437f0000, float:255.0)
            float r1 = r1 * r0
            float r1 = r1 * r6
            int r3 = java.lang.Math.round(r1)
        L_0x0047:
            r2.setAlpha(r3)
        L_0x004a:
            return
        L_0x004b:
            float r1 = r5.A00
            goto L_0x003f
        L_0x004e:
            r4 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipCallControlBottomSheetV2.A1N(float):void");
    }

    public final void A1O(int i) {
        CallInfo A2i;
        NetworkInfo networkInfo;
        AnonymousClass2CT r0 = this.A0W;
        if (r0 == null || (A2i = r0.A00.A2i()) == null || this.A0W == null || A0p() == null) {
            Log.w("voip/VoipCallControlBottomSheetV2/failed to get call info when build call action intent");
            return;
        }
        boolean equals = "com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN".equals(this.A0W.A00.getIntent().getAction());
        Intent intent = null;
        if (i != 0) {
            if (i == 1) {
                Context A0p = A0p();
                String str = A2i.callId;
                intent = new Intent();
                intent.setClassName(A0p.getPackageName(), "com.whatsapp.voipcalling.VoipActivityV2");
                intent.setAction("com.whatsapp.intent.action.REJECT_CALL_FROM_VOIP_UI");
                intent.putExtra("pendingCall", equals);
                intent.putExtra("call_id", str);
            } else if (i == 2) {
                Context A0p2 = A0p();
                intent = new Intent();
                intent.setClassName(A0p2.getPackageName(), "com.whatsapp.voipcalling.VoipActivityV2");
                intent.setAction("com.whatsapp.intent.action.JOIN_CALL_LINK");
            }
            intent.setFlags(268435456);
        } else {
            ConnectivityManager A0H = this.A0P.A0H();
            if (A0H != null) {
                networkInfo = A0H.getActiveNetworkInfo();
            } else {
                networkInfo = null;
            }
            int i2 = 7;
            if (!A2i.isCallFull()) {
                i2 = 3;
                if (networkInfo != null) {
                    int i3 = 3;
                    if (equals) {
                        i3 = 10;
                    }
                    intent = new C14960mK().A0n(A0p(), A2i.callId, i3, true);
                }
            }
            VoipErrorDialogFragment A01 = VoipErrorDialogFragment.A01(new C50102Ob(), i2);
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                A01.A1F(A0B.A0V(), null);
            }
        }
        this.A0Q.A04(27, A2i.callId);
        ActivityC000900k A0B2 = A0B();
        if (A0B2 != null && intent != null) {
            A0B2.startActivity(intent);
        }
    }

    public void A1P(int i, float f) {
        View view;
        int translationY;
        int i2;
        int i3;
        AnonymousClass2CT r1 = this.A0W;
        if (r1 != null) {
            float f2 = 0.0f;
            if (f <= 0.0f) {
                VoipActivityV2 voipActivityV2 = r1.A00;
                f2 = ((float) voipActivityV2.A0R.getHeight()) * f;
                if (!voipActivityV2.A1j) {
                    VideoCallParticipantViewLayout videoCallParticipantViewLayout = voipActivityV2.A0z;
                    if (videoCallParticipantViewLayout.A0M) {
                        translationY = i;
                    } else {
                        translationY = (int) (f2 - voipActivityV2.A0R.getTranslationY());
                    }
                    VideoCallParticipantView videoCallParticipantView = videoCallParticipantViewLayout.A0Q;
                    if (videoCallParticipantView.A03 == 1) {
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) videoCallParticipantView.getLayoutParams();
                        StringBuilder sb = new StringBuilder("VideoCallParticipantViewLayout/movePiPViewWithOutAnimation xOffset: ");
                        sb.append(0);
                        sb.append(", yOffset: ");
                        sb.append(translationY);
                        Log.i(sb.toString());
                        AnonymousClass018 r9 = videoCallParticipantViewLayout.A0H;
                        if (!r9.A04().A06) {
                            i2 = marginLayoutParams.leftMargin;
                        } else {
                            i2 = marginLayoutParams.rightMargin;
                        }
                        int i4 = i2 + 0;
                        int i5 = marginLayoutParams.topMargin;
                        if (!r9.A04().A06) {
                            i3 = marginLayoutParams.rightMargin;
                        } else {
                            i3 = marginLayoutParams.leftMargin;
                        }
                        C42941w9.A09(videoCallParticipantView, r9, i4, i5, i3, marginLayoutParams.bottomMargin);
                        marginLayoutParams.topMargin += translationY;
                        videoCallParticipantView.setLayoutParams(marginLayoutParams);
                    }
                } else if (voipActivityV2.A1Y) {
                    voipActivityV2.A39(f);
                } else if (voipActivityV2.A01 == 3) {
                    AnonymousClass3DO r0 = voipActivityV2.A0n;
                    r0.A01 = f;
                    r0.A00();
                }
                VoipInCallNotifBanner voipInCallNotifBanner = voipActivityV2.A0o;
                if (voipInCallNotifBanner != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) voipInCallNotifBanner.getLayoutParams();
                    StringBuilder sb2 = new StringBuilder("VoipCallNewParticipantBanner/moveBannerYPosition yOffset: ");
                    sb2.append(i);
                    Log.i(sb2.toString());
                    marginLayoutParams2.bottomMargin -= i;
                    voipInCallNotifBanner.setLayoutParams(marginLayoutParams2);
                }
                view = voipActivityV2.A0R;
            } else {
                view = r1.A00.A0R;
            }
            view.setTranslationY(f2);
        }
    }

    public boolean A1Q() {
        C64523Fw r0;
        return this.A0Z && (r0 = this.A0F) != null && r0.A08();
    }

    public boolean A1R() {
        BottomSheetViewModel bottomSheetViewModel = this.A0H;
        return bottomSheetViewModel != null && ((Boolean) bottomSheetViewModel.A09.A01()).booleanValue();
    }

    public boolean A1S() {
        C64523Fw r1;
        int i;
        if (!this.A0Z || (r1 = this.A0F) == null) {
            return false;
        }
        if (r1.A08 || (i = r1.A02) == 0) {
            i = r1.A0F.A0B;
        }
        if (i == 2 || i == 1) {
            return true;
        }
        return false;
    }

    public boolean A1T() {
        int A18 = A18();
        if (A18 == 0) {
            Bundle bundle = ((AnonymousClass01E) this).A05;
            AnonymousClass009.A05(bundle);
            if (bundle.getBoolean("is_video_call", false)) {
                return true;
            }
            return false;
        } else if (A18 == R.style.VoipCallControlVideoBottomSheet) {
            return true;
        } else {
            return false;
        }
    }
}
