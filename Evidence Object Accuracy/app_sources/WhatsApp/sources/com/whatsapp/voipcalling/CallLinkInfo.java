package com.whatsapp.voipcalling;

import X.AnonymousClass1S6;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CallLinkInfo {
    public static final String DEFAULT_CALL_LINK_CALL_ID = "default";
    public final UserJid creatorJid;
    public final int linkState;
    public AnonymousClass1S6 self;
    public final String token;
    public final boolean videoEnabled;

    public CallLinkInfo(int i, UserJid userJid, String str, boolean z) {
        this.linkState = i;
        this.creatorJid = userJid;
        this.token = str;
        this.videoEnabled = z;
    }

    public UserJid getCreatorJid() {
        return this.creatorJid;
    }

    public int getLinkState() {
        return this.linkState;
    }

    public AnonymousClass1S6 getSelfInfo() {
        return this.self;
    }

    public String getToken() {
        return this.token;
    }

    public boolean isVideoEnabled() {
        return this.videoEnabled;
    }

    private void setSelfParticipantInfo(UserJid userJid, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, int i3, int i4, int i5, boolean z5, boolean z6) {
        this.self = new AnonymousClass1S6(userJid, i, i2, i3, i4, i5, 0, true, false, false, false, false, z, z2, z3, z4, z5, z6, false, false, false);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("token: ");
        sb.append(this.token);
        sb.append(", videoEnabled: ");
        sb.append(this.videoEnabled);
        sb.append(", linkState: ");
        sb.append(Voip.A06(this.linkState));
        return sb.toString();
    }
}
