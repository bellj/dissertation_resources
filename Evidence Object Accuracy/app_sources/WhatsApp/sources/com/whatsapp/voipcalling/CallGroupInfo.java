package com.whatsapp.voipcalling;

import java.util.Arrays;

/* loaded from: classes2.dex */
public final class CallGroupInfo {
    public final CallParticipant[] participants;
    public final int transactionId;

    public CallGroupInfo(int i, CallParticipant[] callParticipantArr) {
        this.transactionId = i;
        this.participants = callParticipantArr;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallGroupInfo{transactionId=");
        sb.append(this.transactionId);
        sb.append(", participants=");
        sb.append(Arrays.toString(this.participants));
        sb.append('}');
        return sb.toString();
    }
}
