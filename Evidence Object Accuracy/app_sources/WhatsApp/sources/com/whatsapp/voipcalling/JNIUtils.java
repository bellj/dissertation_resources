package com.whatsapp.voipcalling;

import X.AbstractC14440lR;
import X.AbstractC15460nI;
import X.AnonymousClass01N;
import X.AnonymousClass01d;
import X.AnonymousClass1SF;
import X.C14330lG;
import X.C14820m6;
import X.C14840m8;
import X.C14850m9;
import X.C15380n4;
import X.C15450nH;
import X.C15570nT;
import X.C15890o4;
import X.C16590pI;
import X.C17140qK;
import X.C17170qN;
import android.content.SharedPreferences;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import java.io.File;
import java.util.Arrays;
import org.wawebrtc.MediaCodecVideoEncoder;

/* loaded from: classes2.dex */
public class JNIUtils {
    public static final String[] H264_BLACKLISTED_DEVICE_BOARD = {"sc7735s", "PXA19x8", "SC7727S", "sc7730s", "SC7715A", "full_oppo6750_15331", "mt6577", "hawaii", "java", "arima89_we_s_jb2", "arima82_w_s_kk", "capri", "mt6572", "P7-L10", "P7-L12"};
    public static final String[] H264_BLACKLISTED_DEVICE_HARDWARE = {"my70ds", "sc8830", "sc8830a", "samsungexynos7580"};
    public final C14850m9 abProps;
    public final C14330lG fMessageIO;
    public final AnonymousClass01N isVideoRotationSupportedProvider;
    public final C15570nT meManager;
    public final C14840m8 multiDeviceConfig;
    public int previousAudioSessionId = -1;
    public final C15450nH serverProps;
    public final AnonymousClass01d systemServices;
    public final VoipCameraManager voipCameraManager;
    public final C17140qK voipSharedPreferences;
    public final C16590pI waContext;
    public final C17170qN waDebugBuildSharedPreferences;
    public final C15890o4 waPermissionsHelper;
    public final C14820m6 waSharedPreferences;
    public final AbstractC14440lR waWorkers;

    private boolean isH265SwCodecSupported() {
        return false;
    }

    public void uploadCrashLog(String str) {
    }

    public JNIUtils(C14850m9 r2, C15570nT r3, C16590pI r4, AbstractC14440lR r5, C14330lG r6, C15450nH r7, AnonymousClass01d r8, C14840m8 r9, VoipCameraManager voipCameraManager, C15890o4 r11, C14820m6 r12, C17140qK r13, C17170qN r14, AnonymousClass01N r15) {
        this.abProps = r2;
        this.meManager = r3;
        this.waContext = r4;
        this.waWorkers = r5;
        this.fMessageIO = r6;
        this.serverProps = r7;
        this.systemServices = r8;
        this.multiDeviceConfig = r9;
        this.voipCameraManager = voipCameraManager;
        this.waPermissionsHelper = r11;
        this.waSharedPreferences = r12;
        this.voipSharedPreferences = r13;
        this.waDebugBuildSharedPreferences = r14;
        this.isVideoRotationSupportedProvider = r15;
    }

    public boolean allowAlternativeNetworkForAudioCall() {
        if (this.waSharedPreferences.A00.getBoolean("voip_low_data_usage", false) || this.waSharedPreferences.A00.getInt("autodownload_cellular_mask", 1) == 0) {
            return false;
        }
        return true;
    }

    public boolean allowAlternativeNetworkForVideoCall() {
        if (this.waSharedPreferences.A00.getBoolean("voip_low_data_usage", false) || (this.waSharedPreferences.A00.getInt("autodownload_cellular_mask", 1) & 4) == 0) {
            return false;
        }
        return true;
    }

    public synchronized int[] findAvailableAudioSamplingRate(int i) {
        return findAvailableAudioSamplingRate(new int[]{16000, 24000, 44100, 22050, 8000, 11025, 32000, 48000, 12000}, 2, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c6, code lost:
        if (r32 > 64000) goto L_0x00c8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02d4  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02f2  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0331 A[LOOP:5: B:115:0x032f->B:116:0x0331, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0347  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x034c  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0325 A[EDGE_INSN: B:143:0x0325->B:114:0x0325 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0340 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0312 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0142 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0122 A[LOOP:2: B:50:0x0120->B:51:0x0122, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int[] findAvailableAudioSamplingRate(int[] r30, int r31, int r32) {
        /*
        // Method dump skipped, instructions count: 898
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.JNIUtils.findAvailableAudioSamplingRate(int[], int, int):int[]");
    }

    public int getAudioLevelSpeakingThreshold() {
        return Math.min(this.abProps.A02(1213), 127);
    }

    public int getCallLinkMilestoneVersion() {
        return this.abProps.A02(1372);
    }

    public String getDebugDirectory() {
        this.fMessageIO.A04();
        return this.waContext.A00.getFilesDir().getAbsolutePath();
    }

    public boolean getDebugVoipRecordDecoderVideo() {
        return false;
    }

    public boolean getDebugVoipRecordEncoderVideo() {
        return false;
    }

    public boolean getDebugVoipRecordPreprocessedCaptureVideo() {
        return false;
    }

    public boolean getDebugVoipRecordRawCaptureVideo() {
        return false;
    }

    public boolean getDebugVoipRecordRawRenderVideo() {
        return false;
    }

    public int getGroupCallBufferProcessDelay() {
        return this.abProps.A02(1092);
    }

    public int getHeartbeatIntervalS() {
        return this.abProps.A02(1430);
    }

    public int getJoinableMilestoneVersion() {
        if (AnonymousClass1SF.A0M(this.waSharedPreferences, this.abProps)) {
            return 4;
        }
        return this.abProps.A02(643);
    }

    public int getLobbyTimeoutMin() {
        return this.abProps.A02(1565);
    }

    public static int getSamplingHash(int i, int[] iArr, int i2, int i3) {
        int length = iArr.length;
        int[] iArr2 = new int[length + 4];
        System.arraycopy(iArr, 0, iArr2, 0, length);
        iArr2[length] = i2;
        iArr2[length + 1] = i;
        iArr2[length + 2] = Build.VERSION.SDK_INT;
        iArr2[length + 3] = i3;
        return Arrays.hashCode(iArr2);
    }

    public String getSelfJid() {
        this.meManager.A08();
        C15570nT r0 = this.meManager;
        r0.A08();
        return C15380n4.A03(r0.A05);
    }

    public final String getTimeSeriesDirectory() {
        File A07 = AnonymousClass1SF.A07(this.waContext.A00);
        if (A07 != null) {
            return A07.getAbsolutePath();
        }
        Log.e("getTimeSeriesDirectory base time series directory is null");
        return "";
    }

    public int getUpdateSpeakerStatusIntervalMs() {
        return this.abProps.A02(1106);
    }

    public final String getVoipCacheDirectory() {
        String str;
        File cacheDir = this.waContext.A00.getCacheDir();
        if (cacheDir != null) {
            File file = new File(cacheDir, "voip");
            if (file.exists() || file.mkdirs()) {
                return file.getAbsolutePath();
            }
            str = "getVoipCacheDirectory could not init directory";
        } else {
            str = "getVoipCacheDirectory Cache Directory is null";
        }
        Log.e(str);
        return "";
    }

    public VoipCameraManager getVoipCameraManager() {
        return this.voipCameraManager;
    }

    public MediaCodecVideoEncoder initMediaCodecVideoEncoder() {
        return new MediaCodecVideoEncoder(this.voipSharedPreferences);
    }

    public boolean isFixedVideoOrientationEnabled() {
        return isVideoRotationEnabled();
    }

    public boolean isGcallCodecNegoFixEnabled() {
        return this.voipSharedPreferences.A01().getBoolean("enable_gcall_codec_nego_fix", false);
    }

    public boolean isGroupCallBufferEnabled() {
        return this.abProps.A07(1039);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        if (r1.equalsIgnoreCase("jfvelte") == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isH264HwCodecSupported() {
        /*
            r6 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r5 = 0
            r0 = 19
            if (r1 < r0) goto L_0x0029
            boolean r0 = X.C39011p7.A08()
            if (r0 == 0) goto L_0x0029
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            java.lang.String r0 = "5.0.1"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x002a
            java.lang.String r1 = android.os.Build.DEVICE
            java.lang.String r0 = "jflte"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x0029
            java.lang.String r0 = "jfvelte"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x002a
        L_0x0029:
            return r5
        L_0x002a:
            java.lang.String[] r4 = com.whatsapp.voipcalling.JNIUtils.H264_BLACKLISTED_DEVICE_BOARD
            int r3 = r4.length
            r2 = 0
        L_0x002e:
            if (r2 >= r3) goto L_0x003d
            r1 = r4[r2]
            java.lang.String r0 = android.os.Build.BOARD
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0029
            int r2 = r2 + 1
            goto L_0x002e
        L_0x003d:
            java.lang.String[] r4 = com.whatsapp.voipcalling.JNIUtils.H264_BLACKLISTED_DEVICE_HARDWARE
            int r3 = r4.length
            r2 = 0
        L_0x0041:
            if (r2 >= r3) goto L_0x0050
            r1 = r4[r2]
            java.lang.String r0 = android.os.Build.HARDWARE
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0029
            int r2 = r2 + 1
            goto L_0x0041
        L_0x0050:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.JNIUtils.isH264HwCodecSupported():boolean");
    }

    private boolean isH265HwCodecSupported() {
        return MediaCodecVideoEncoder.isH265HwSupported();
    }

    private synchronized H26xSupportResult isH26XCodecSupported() {
        return new H26xSupportResult(isH264HwCodecSupported(), false, MediaCodecVideoEncoder.isH265HwSupported(), false);
    }

    public synchronized H26xSupportResult isH26XCodecSupportedFromCache() {
        SharedPreferences A01;
        A01 = this.voipSharedPreferences.A01();
        return (!A01.contains("video_codec_h264_hw_supported") || !A01.contains("video_codec_h264_sw_supported") || !A01.contains("video_codec_h265_hw_supported") || !A01.contains("video_codec_h265_sw_supported")) ? null : new H26xSupportResult(A01.getBoolean("video_codec_h264_hw_supported", false), A01.getBoolean("video_codec_h264_sw_supported", false), A01.getBoolean("video_codec_h265_hw_supported", false), A01.getBoolean("video_codec_h265_sw_supported", false));
    }

    public boolean isLowDataUsageEnabled() {
        return this.waSharedPreferences.A00.getBoolean("voip_low_data_usage", false);
    }

    public boolean isMDCallEnabled() {
        return this.multiDeviceConfig.A05() && this.serverProps.A05(AbstractC15460nI.A0j);
    }

    public boolean isMuteParticipantEnabled() {
        return this.abProps.A07(1111) && AnonymousClass1SF.A0M(this.waSharedPreferences, this.abProps);
    }

    public boolean isReportCallRepalyerIdAllowed() {
        return this.abProps.A07(1834);
    }

    public boolean isVidQualityManagerEnabled() {
        return this.voipSharedPreferences.A01().getBoolean("enable_vid_quality_manager", false);
    }

    public boolean isVideoConverterMemoryLeakFixEnabled() {
        return this.voipSharedPreferences.A01.A07(MediaCodecVideoEncoder.MIN_ENCODER_WIDTH);
    }

    public boolean isVideoRotationEnabled() {
        return ((Boolean) this.isVideoRotationSupportedProvider.get()).booleanValue();
    }

    public boolean isVoipStanzaSmaxationEnabled() {
        return this.abProps.A07(1520);
    }

    public boolean isWamCallExtendedEnabled() {
        return this.abProps.A07(1939);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateH26XCodecSupported$0() {
        this.voipSharedPreferences.A04(isH26XCodecSupported());
    }

    public synchronized void updateH26XCodecSupported(boolean z) {
        if (z) {
            this.waWorkers.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(this, 33));
        } else {
            this.voipSharedPreferences.A04(isH26XCodecSupported());
        }
    }

    /* loaded from: classes2.dex */
    public class H26xSupportResult {
        public final boolean isH264HwSupported;
        public final boolean isH264SwSupported;
        public final boolean isH265HwSupported;
        public final boolean isH265SwSupported;

        public H26xSupportResult(boolean z, boolean z2, boolean z3, boolean z4) {
            this.isH264HwSupported = z;
            this.isH264SwSupported = z2;
            this.isH265HwSupported = z3;
            this.isH265SwSupported = z4;
        }
    }
}
