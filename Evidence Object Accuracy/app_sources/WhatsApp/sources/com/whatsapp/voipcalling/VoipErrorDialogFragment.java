package com.whatsapp.voipcalling;

import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.C15450nH;
import X.C15550nR;
import X.C15610nY;
import X.C32721cd;
import X.C43951xu;
import X.C50102Ob;
import android.os.Bundle;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class VoipErrorDialogFragment extends Hilt_VoipErrorDialogFragment {
    public int A00;
    public int A01;
    public C15450nH A02;
    public C50102Ob A03;
    public C15550nR A04;
    public C15610nY A05;
    public AnonymousClass018 A06;
    public ArrayList A07 = new ArrayList();

    public static VoipErrorDialogFragment A00(Bundle bundle, C50102Ob r3, int i) {
        Bundle bundle2 = new Bundle();
        bundle2.putAll(bundle);
        bundle2.putInt("error", i);
        VoipErrorDialogFragment voipErrorDialogFragment = new VoipErrorDialogFragment();
        voipErrorDialogFragment.A0U(bundle2);
        voipErrorDialogFragment.A03 = r3;
        return voipErrorDialogFragment;
    }

    public static VoipErrorDialogFragment A01(C50102Ob r2, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("error", i);
        VoipErrorDialogFragment voipErrorDialogFragment = new VoipErrorDialogFragment();
        voipErrorDialogFragment.A0U(bundle);
        voipErrorDialogFragment.A03 = r2;
        return voipErrorDialogFragment;
    }

    public static VoipErrorDialogFragment A02(List list, int i, boolean z) {
        VoipErrorDialogFragment voipErrorDialogFragment = new VoipErrorDialogFragment();
        Bundle bundle = new Bundle();
        int i2 = 1;
        if (z) {
            i2 = 2;
        }
        bundle.putInt("error", i2);
        bundle.putParcelableArrayList("user_jids", new ArrayList<>(list));
        bundle.putInt("call_size", i);
        voipErrorDialogFragment.A0U(bundle);
        voipErrorDialogFragment.A03 = new C50102Ob();
        return voipErrorDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            this.A01 = bundle2.getInt("error");
            this.A07 = bundle2.getParcelableArrayList("user_jids");
            this.A00 = bundle2.getInt("call_size");
        }
        if (this.A07 == null) {
            this.A07 = new ArrayList();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0017;
     */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r6) {
        /*
            r5 = this;
            java.lang.String r0 = r5.A1L()
            java.lang.String r1 = r5.A1K()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            r2 = 0
            r4 = 1
            if (r0 == 0) goto L_0x0017
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            r0 = 0
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            X.AnonymousClass009.A0E(r0)
            X.00k r0 = r5.A0C()
            X.02e r3 = new X.02e
            r3.<init>(r0)
            java.lang.String r0 = r5.A1L()
            r3.setTitle(r0)
            java.lang.String r0 = r5.A1K()
            r3.A0A(r0)
            r3.A0B(r4)
            int r0 = r5.A01
            switch(r0) {
                case 1: goto L_0x003f;
                case 2: goto L_0x003f;
                case 3: goto L_0x003f;
                case 4: goto L_0x003f;
                case 5: goto L_0x0099;
                case 6: goto L_0x003f;
                case 7: goto L_0x003f;
                case 8: goto L_0x003f;
                case 9: goto L_0x003f;
                case 10: goto L_0x003f;
                case 11: goto L_0x003f;
                case 12: goto L_0x003f;
                case 13: goto L_0x003f;
                case 14: goto L_0x003f;
                case 15: goto L_0x003f;
                case 16: goto L_0x003f;
                case 17: goto L_0x003f;
                case 18: goto L_0x003f;
                case 19: goto L_0x008d;
                case 20: goto L_0x008d;
                case 21: goto L_0x003f;
                case 22: goto L_0x003f;
                case 23: goto L_0x003f;
                case 24: goto L_0x003f;
                default: goto L_0x003a;
            }
        L_0x003a:
            java.lang.String r0 = "Unknown error"
            X.AnonymousClass009.A0A(r0, r2)
        L_0x003f:
            int r0 = r5.A01
            switch(r0) {
                case 1: goto L_0x0069;
                case 2: goto L_0x0069;
                case 3: goto L_0x0069;
                case 4: goto L_0x0069;
                case 5: goto L_0x0075;
                case 6: goto L_0x0069;
                case 7: goto L_0x0069;
                case 8: goto L_0x0069;
                case 9: goto L_0x0069;
                case 10: goto L_0x0069;
                case 11: goto L_0x0081;
                case 12: goto L_0x0069;
                case 13: goto L_0x0069;
                case 14: goto L_0x0069;
                case 15: goto L_0x0069;
                case 16: goto L_0x0069;
                case 17: goto L_0x0069;
                case 18: goto L_0x0069;
                case 19: goto L_0x0075;
                case 20: goto L_0x0075;
                case 21: goto L_0x0081;
                case 22: goto L_0x0081;
                case 23: goto L_0x0081;
                case 24: goto L_0x0081;
                default: goto L_0x0044;
            }
        L_0x0044:
            java.lang.String r0 = "Unknown error"
            X.AnonymousClass009.A0A(r0, r2)
        L_0x0049:
            android.os.Bundle r1 = r5.A05
            if (r1 == 0) goto L_0x005f
            java.lang.String r0 = "finish"
            boolean r0 = r1.getBoolean(r0, r2)
            if (r0 == 0) goto L_0x005f
            r2 = 1
            X.4hS r1 = new X.4hS
            r1.<init>()
            X.0OC r0 = r3.A01
            r0.A08 = r1
        L_0x005f:
            X.04S r1 = r3.create()
            r0 = r2 ^ 1
            r1.setCanceledOnTouchOutside(r0)
            return r1
        L_0x0069:
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            X.4gO r0 = new X.4gO
            r0.<init>()
            r3.setNegativeButton(r1, r0)
            goto L_0x0049
        L_0x0075:
            r1 = 2131886925(0x7f12034d, float:1.9408443E38)
            X.4gM r0 = new X.4gM
            r0.<init>()
            r3.setNegativeButton(r1, r0)
            goto L_0x0049
        L_0x0081:
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            X.4gP r0 = new X.4gP
            r0.<init>()
            r3.setNegativeButton(r1, r0)
            goto L_0x0049
        L_0x008d:
            r1 = 2131886887(0x7f120327, float:1.9408366E38)
            X.4gN r0 = new X.4gN
            r0.<init>()
            r3.setPositiveButton(r1, r0)
            goto L_0x003f
        L_0x0099:
            r1 = 2131892924(0x7f121abc, float:1.942061E38)
            X.4gL r0 = new X.4gL
            r0.<init>()
            r3.setPositiveButton(r1, r0)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipErrorDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }

    public final String A1K() {
        AnonymousClass018 r5;
        int i;
        int i2;
        switch (this.A01) {
            case 1:
                r5 = this.A06;
                i = R.plurals.voip_error_group_call_size_not_supported;
                int i3 = this.A00;
                return r5.A0I(new Object[]{Integer.valueOf(i3)}, i, (long) i3);
            case 2:
                ArrayList arrayList = this.A07;
                int size = arrayList.size();
                r5 = this.A06;
                i = R.plurals.voip_error_multiple_participant_group_call_size_not_supported;
                if (size == 1) {
                    return r5.A0I(new Object[]{A1M(arrayList), Integer.valueOf(this.A00)}, R.plurals.voip_error_one_participant_group_call_size_not_supported, (long) this.A00);
                }
                int i3 = this.A00;
                return r5.A0I(new Object[]{Integer.valueOf(i3)}, i, (long) i3);
            case 3:
                i2 = R.string.voip_joinable_no_internet_connection_body;
                return A0I(i2);
            case 4:
                return A0J(R.string.voip_joinable_max_participants_invited_body, Integer.valueOf(this.A02.A02(AbstractC15460nI.A1O)));
            case 5:
                i2 = R.string.voip_joinable_call_full_add_participant_body;
                return A0I(i2);
            case 6:
                i2 = R.string.voip_joinable_call_full_rering_body;
                return A0I(i2);
            case 7:
            case 24:
                i2 = R.string.voip_joinable_call_full_join_body;
                return A0I(i2);
            case 8:
                i2 = R.string.voip_joinable_call_ended_while_joining_body;
                return A0I(i2);
            case 9:
                i2 = R.string.voip_joinable_call_full_info_body;
                return A0I(i2);
            case 10:
                i2 = R.string.linked_group_call_membership_mismatch;
                return A0I(i2);
            case 11:
                i2 = R.string.linked_group_call_duplicate;
                return A0I(i2);
            case 12:
                i2 = R.string.linked_group_call_ended;
                return A0I(i2);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i2 = R.string.linked_group_call_not_group_member;
                return A0I(i2);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                i2 = R.string.linked_group_call_not_admin_in_announcement_group;
                return A0I(i2);
            case 15:
                i2 = R.string.linked_group_call_exceed_limit;
                return A0I(i2);
            case GlVideoRenderer.CAP_RENDER_I420:
                i2 = R.string.linked_group_call_only_one_participant;
                return A0I(i2);
            case 17:
                i2 = R.string.suspended_group_error_message;
                return A0I(i2);
            case 18:
                return A0J(R.string.voip_feature_not_available, A1M(this.A07));
            case 19:
                i2 = R.string.call_link_error_message;
                return A0I(i2);
            case C43951xu.A01:
                i2 = R.string.check_connectivity;
                return A0I(i2);
            case 21:
                i2 = R.string.call_timed_out;
                return A0I(i2);
            case 22:
                i2 = R.string.unable_to_connect;
                return A0I(i2);
            case 23:
                i2 = R.string.please_try_again_later;
                return A0I(i2);
            default:
                AnonymousClass009.A0A("Unknown error", false);
                return "";
        }
    }

    public final String A1L() {
        int i;
        switch (this.A01) {
            case 1:
                ArrayList arrayList = this.A07;
                int size = arrayList.size();
                AnonymousClass018 r5 = this.A06;
                if (size <= 3) {
                    return r5.A0I(new Object[]{A1M(arrayList)}, R.plurals.voip_error_add_participant_failed_dialog_title, (long) arrayList.size());
                }
                return r5.A0I(new Object[]{A1M(arrayList.subList(0, 1)), Integer.valueOf(this.A07.size() - 1)}, R.plurals.voip_error_add_four_or_more_participant_failed_dialog_title, (long) (arrayList.size() - 1));
            case 2:
                i = R.string.voip_error_select_fewer_participants;
                return A0I(i);
            case 3:
            case C43951xu.A01:
                i = R.string.voip_joinable_no_internet_connection_title;
                return A0I(i);
            case 4:
                i = R.string.voip_joinable_max_participants_invited_title;
                return A0I(i);
            case 5:
            case 6:
            case 7:
            case 24:
                i = R.string.voip_joinable_call_full_title;
                return A0I(i);
            case 8:
            case 12:
                i = R.string.voip_joinable_call_ended_while_joining_title;
                return A0I(i);
            case 9:
                i = R.string.voip_joinable_call_full_info_title;
                return A0I(i);
            case 10:
            case 11:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420:
            case 17:
                i = R.string.voip_call_failed;
                return A0I(i);
            case 18:
            case 21:
            case 22:
                return "";
            case 19:
                i = R.string.call_link_error_title;
                return A0I(i);
            case 23:
                i = R.string.unable_to_connect;
                return A0I(i);
            default:
                AnonymousClass009.A0A("Unknown error", false);
                return "";
        }
    }

    public final String A1M(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(this.A05.A0A(this.A04.A0B((AbstractC14640lm) it.next()), -1));
        }
        if (arrayList.size() <= 3) {
            return C32721cd.A00(this.A05.A04, arrayList, true);
        }
        int size = arrayList.size() - 1;
        return this.A06.A0I(new Object[]{arrayList.get(0), Integer.valueOf(size)}, R.plurals.group_voip_call_participants_label, (long) size);
    }
}
