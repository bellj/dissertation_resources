package com.whatsapp.voipcalling;

import X.AbstractC92124Uq;
import X.AnonymousClass009;
import X.AnonymousClass4W9;
import X.C12960it;
import X.C78583p9;
import X.C80573sU;
import android.content.Context;
import com.whatsapp.util.Log;

/* loaded from: classes3.dex */
public final class VoipFaceDetector {
    public static final String TAG = "voip/video/VoipFaceDetector/";
    public final boolean detectBounds;
    public final AbstractC92124Uq detector;
    public final int maxDetections;
    public volatile int nextFrameId;
    public volatile boolean released;

    public VoipFaceDetector(AbstractC92124Uq r1, int i, boolean z) {
        this.detector = r1;
        this.maxDetections = i;
        this.detectBounds = z;
    }

    public static VoipFaceDetector create(Context context, int i, boolean z) {
        AnonymousClass009.A0E(C12960it.A1U(i));
        Context applicationContext = context.getApplicationContext();
        C78583p9 r1 = new C78583p9();
        r1.A01 = 0;
        r1.A02 = 0;
        r1.A03 = 0;
        r1.A04 = true;
        r1.A05 = true;
        r1.A00 = -1.0f;
        return new VoipFaceDetector(new C80573sU(new AnonymousClass4W9(applicationContext, r1)), i, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (r23 <= 0) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass4LS detect(java.nio.ByteBuffer r21, int r22, int r23, int r24, int r25) {
        /*
        // Method dump skipped, instructions count: 649
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipFaceDetector.detect(java.nio.ByteBuffer, int, int, int, int):X.4LS");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        if (((X.C80573sU) r2.detector).A01.A00() == null) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isOperational() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.released     // Catch: all -> 0x0015
            if (r0 != 0) goto L_0x0012
            X.4Uq r0 = r2.detector     // Catch: all -> 0x0015
            X.3sU r0 = (X.C80573sU) r0     // Catch: all -> 0x0015
            X.4W9 r0 = r0.A01     // Catch: all -> 0x0015
            java.lang.Object r1 = r0.A00()     // Catch: all -> 0x0015
            r0 = 1
            if (r1 != 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            monitor-exit(r2)
            return r0
        L_0x0015:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.voipcalling.VoipFaceDetector.isOperational():boolean");
    }

    public synchronized void release() {
        if (!this.released) {
            Log.i("voip/video/VoipFaceDetector/Releasing face detector");
            this.released = true;
            this.detector.A00();
        }
    }
}
