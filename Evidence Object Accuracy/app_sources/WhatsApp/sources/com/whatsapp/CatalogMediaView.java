package com.whatsapp;

import X.AbstractC14720lw;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C004902f;
import X.C12970iu;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.biz.catalog.CatalogMediaViewFragment;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* loaded from: classes2.dex */
public class CatalogMediaView extends ActivityC13790kL implements AbstractC14720lw {
    public boolean A00;

    @Override // X.AbstractC14720lw
    public void APJ() {
    }

    @Override // X.AbstractC14720lw
    public void ASg() {
    }

    @Override // X.AbstractC14720lw
    public void AXE() {
    }

    @Override // X.AbstractC14720lw
    public boolean Add() {
        return true;
    }

    public CatalogMediaView() {
        this(0);
    }

    public CatalogMediaView(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 1);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.AbstractC14720lw
    public void ASf() {
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ActivityC13790kL.A0a(this);
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent != null) {
            setContentView(R.layout.media_view_activity);
            AnonymousClass01F A0V = A0V();
            AnonymousClass01E A0A = A0V.A0A("catalog_media_view_fragment");
            if (A0A == null) {
                A0A = new CatalogMediaViewFragment();
            }
            Bundle A0D = C12970iu.A0D();
            A0D.putParcelable("product", intent.getParcelableExtra("product"));
            A0D.putInt("target_image_index", intent.getIntExtra("target_image_index", 0));
            A0D.putString("cached_jid", intent.getStringExtra("cached_jid"));
            A0D.putBundle("animation_bundle", intent.getBundleExtra("animation_bundle"));
            A0A.A0U(A0D);
            C004902f r1 = new C004902f(A0V);
            r1.A0B(A0A, "catalog_media_view_fragment", R.id.media_view_fragment_container);
            r1.A01();
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        MediaViewBaseFragment.A06((Activity) this, true);
    }
}
