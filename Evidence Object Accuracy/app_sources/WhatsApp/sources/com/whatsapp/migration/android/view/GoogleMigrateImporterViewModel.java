package com.whatsapp.migration.android.view;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass116;
import X.AnonymousClass3YW;
import X.AnonymousClass5XP;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15890o4;
import X.C16590pI;
import X.C18350sJ;
import X.C18640sm;
import X.C22900zp;
import X.C25661Ag;
import X.C26031Bt;
import X.C26061Bw;
import android.content.Context;
import com.whatsapp.migration.android.integration.service.GoogleMigrateService;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class GoogleMigrateImporterViewModel extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();
    public AnonymousClass016 A02 = C12980iv.A0T();
    public AnonymousClass016 A03 = C12980iv.A0T();
    public AnonymousClass016 A04 = C12980iv.A0T();
    public final AnonymousClass116 A05;
    public final C18640sm A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C26061Bw A09;
    public final AnonymousClass5XP A0A;
    public final C26031Bt A0B;
    public final C18350sJ A0C;
    public final C25661Ag A0D;
    public final C22900zp A0E;

    public GoogleMigrateImporterViewModel(AnonymousClass116 r3, C18640sm r4, C16590pI r5, C15890o4 r6, C26061Bw r7, C26031Bt r8, C18350sJ r9, C25661Ag r10, C22900zp r11) {
        AnonymousClass3YW r0 = new AnonymousClass3YW(this);
        this.A0A = r0;
        this.A07 = r5;
        this.A0D = r10;
        this.A0E = r11;
        this.A05 = r3;
        this.A08 = r6;
        this.A0C = r9;
        this.A0B = r8;
        this.A06 = r4;
        this.A09 = r7;
        r8.A03(r0);
        int A06 = r7.A06();
        if (A06 == 0) {
            Log.i("GoogleMigrateImporterViewModel/onCreate/REQUEST_FOR_PERMISSION state");
            A06(0);
            return;
        }
        Log.i(C12960it.A0W(A06, "GoogleMigrateImporterViewModel/onCreate/already has state. Current screen = "));
        A06(A06);
        if (A06 == 2) {
            A07(2);
        } else if (A06 == 3) {
            C12970iu.A1Q(this.A03, r7.A05());
            A05();
        } else if (A06 == 18) {
            A04();
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0B.A04(this.A0A);
    }

    public void A04() {
        this.A0C.A01();
        A06(18);
        this.A00.A0A(C12970iu.A0h());
        this.A09.A0C();
        Context context = this.A07.A00;
        C22900zp r1 = this.A0E;
        Log.i("GoogleMigrateService/stopImport()");
        r1.A01(context, GoogleMigrateService.class);
    }

    public void A05() {
        this.A0D.A03("google_migrate_import_started", null, 0);
        Context context = this.A07.A00;
        C22900zp r2 = this.A0E;
        Log.i("GoogleMigrateService/startImport()");
        r2.A03(context, C12990iw.A0E("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_START_IMPORT"), GoogleMigrateService.class);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(int r6) {
        /*
            r5 = this;
            java.lang.String r0 = "GoogleMigrateImporterViewModel/currentScreen/setCurrentScreen: "
            java.lang.String r0 = X.C12960it.A0W(r6, r0)
            com.whatsapp.util.Log.i(r0)
            java.lang.String r4 = "google_migrate_recoverable_error"
            java.lang.String r0 = "unknown"
            r3 = 0
            switch(r6) {
                case 0: goto L_0x0033;
                case 1: goto L_0x0036;
                case 2: goto L_0x001a;
                case 3: goto L_0x001a;
                case 4: goto L_0x0012;
                case 5: goto L_0x0039;
                case 6: goto L_0x003c;
                case 7: goto L_0x001a;
                case 8: goto L_0x001a;
                case 9: goto L_0x003c;
                case 10: goto L_0x0041;
                case 11: goto L_0x0044;
                case 12: goto L_0x0047;
                case 13: goto L_0x004a;
                case 14: goto L_0x004d;
                case 15: goto L_0x0050;
                case 16: goto L_0x0053;
                case 17: goto L_0x001a;
                case 18: goto L_0x001a;
                default: goto L_0x0012;
            }
        L_0x0012:
            r4 = r0
        L_0x0013:
            X.1Ag r2 = r5.A0D
            r0 = 0
            r2.A03(r4, r3, r0)
        L_0x001a:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
            X.016 r1 = r5.A01
            java.lang.Object r0 = r1.A01()
            boolean r0 = X.C29941Vi.A00(r2, r0)
            if (r0 != 0) goto L_0x0032
            java.lang.String r0 = "GoogleMigrateImporterViewModel/currentScreen/post"
            com.whatsapp.util.Log.i(r0)
            r1.A0A(r2)
        L_0x0032:
            return
        L_0x0033:
            java.lang.String r4 = "google_migrate_permission"
            goto L_0x0013
        L_0x0036:
            java.lang.String r4 = "google_migrate_rejected_permission"
            goto L_0x0013
        L_0x0039:
            java.lang.String r4 = "google_migrate_import_complete"
            goto L_0x0013
        L_0x003c:
            java.lang.String r4 = "google_migrate_unrecoverable_error"
            java.lang.String r3 = "generic_unrecoverable"
            goto L_0x0013
        L_0x0041:
            java.lang.String r4 = "google_migrate_cancel_import_dialog"
            goto L_0x0013
        L_0x0044:
            java.lang.String r3 = "jabber_id_not_found"
            goto L_0x0013
        L_0x0047:
            java.lang.String r3 = "generic_recoverable"
            goto L_0x0013
        L_0x004a:
            java.lang.String r3 = "encryption_key_mismatch"
            goto L_0x0013
        L_0x004d:
            java.lang.String r3 = "encryption_key_not_found"
            goto L_0x0013
        L_0x0050:
            java.lang.String r3 = "encryption_timed_out"
            goto L_0x0013
        L_0x0053:
            java.lang.String r3 = "encryption_no_connection"
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel.A06(int):void");
    }

    public void A07(Integer num) {
        int intValue = num.intValue();
        int i = 2;
        if (intValue != 2) {
            if (intValue == 1) {
                this.A0D.A02("google_migrate_import_complete", "google_migrate_import_complete_next");
                i = 8;
            }
            this.A00.A0A(num);
            this.A09.A0C();
            Context context = this.A07.A00;
            C22900zp r1 = this.A0E;
            Log.i("GoogleMigrateService/stopImport()");
            r1.A01(context, GoogleMigrateService.class);
        }
        A06(i);
        this.A00.A0A(num);
        this.A09.A0C();
        Context context = this.A07.A00;
        C22900zp r1 = this.A0E;
        Log.i("GoogleMigrateService/stopImport()");
        r1.A01(context, GoogleMigrateService.class);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006b, code lost:
        if (r5.A05.A00() == false) goto L_0x006d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(boolean r6) {
        /*
            r5 = this;
            java.lang.String r2 = "google_migrate_confirm_continue_import"
            X.1Ag r1 = r5.A0D
            if (r6 == 0) goto L_0x0057
            java.lang.String r0 = "google_migrate_recoverable_error"
            r1.A01(r0, r2)
            X.016 r1 = r5.A01
            java.lang.Object r0 = r1.A01()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r1.A01()
            int r1 = X.C12960it.A05(r0)
            r0 = 6
            if (r1 == r0) goto L_0x0029
            r0 = 9
            if (r1 == r0) goto L_0x0055
            r0 = 11
            if (r1 == r0) goto L_0x0052
            switch(r1) {
                case 13: goto L_0x0046;
                case 14: goto L_0x0049;
                case 15: goto L_0x004c;
                case 16: goto L_0x004f;
                default: goto L_0x0029;
            }
        L_0x0029:
            r4 = 1
        L_0x002a:
            X.0pI r0 = r5.A07
            android.content.Context r3 = r0.A00
            X.0zp r2 = r5.A0E
            java.lang.String r0 = "GoogleMigrateService/prepareBeforeRetry()"
            com.whatsapp.util.Log.i(r0)
            java.lang.String r0 = "com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_PREPARE_BEFORE_RETRY"
            android.content.Intent r1 = X.C12990iw.A0E(r0)
            java.lang.String r0 = "migration_error_code"
            r1.putExtra(r0, r4)
            java.lang.Class<com.whatsapp.migration.android.integration.service.GoogleMigrateService> r0 = com.whatsapp.migration.android.integration.service.GoogleMigrateService.class
            r2.A03(r3, r1, r0)
            return
        L_0x0046:
            r4 = 104(0x68, float:1.46E-43)
            goto L_0x002a
        L_0x0049:
            r4 = 101(0x65, float:1.42E-43)
            goto L_0x002a
        L_0x004c:
            r4 = 103(0x67, float:1.44E-43)
            goto L_0x002a
        L_0x004f:
            r4 = 102(0x66, float:1.43E-43)
            goto L_0x002a
        L_0x0052:
            r4 = 301(0x12d, float:4.22E-43)
            goto L_0x002a
        L_0x0055:
            r4 = 2
            goto L_0x002a
        L_0x0057:
            java.lang.String r0 = "google_migrate_cancel_import_dialog"
            r1.A01(r0, r2)
            X.0o4 r0 = r5.A08
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x006d
            X.116 r0 = r5.A05
            boolean r1 = r0.A00()
            r0 = 3
            if (r1 != 0) goto L_0x006e
        L_0x006d:
            r0 = 1
        L_0x006e:
            r5.A06(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel.A08(boolean):void");
    }
}
