package com.whatsapp.migration.android.integration.service;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AnonymousClass01d;
import X.AnonymousClass1JK;
import X.AnonymousClass3YV;
import X.AnonymousClass5XP;
import X.C005602s;
import X.C25661Ag;
import X.C26031Bt;
import X.C26061Bw;
import X.C26451Dk;
import android.content.Intent;
import android.os.IBinder;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class GoogleMigrateService extends AnonymousClass1JK {
    public AbstractC15710nm A00;
    public AnonymousClass01d A01;
    public C26061Bw A02;
    public C26031Bt A03;
    public C26451Dk A04;
    public C25661Ag A05;
    public AbstractC14440lR A06;
    public boolean A07 = false;
    public final AnonymousClass5XP A08 = new AnonymousClass3YV(this);

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public GoogleMigrateService() {
        super("GoogleMigrateService", true);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
        this.A03.A03(this.A08);
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("GoogleMigrateService/onDestroy()");
        super.onDestroy();
        stopForeground(true);
        this.A03.A04(this.A08);
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        String str;
        super.onStartCommand(intent, i, i2);
        if (intent == null) {
            str = "GoogleMigrateService/onStartCommand()/intent is null";
        } else if ("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_START_IMPORT".equals(intent.getAction()) && this.A02.A0H()) {
            str = "GoogleMigrateService/onStartCommand()/import in progress";
        } else if ("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_CANCEL_IMPORT".equals(intent.getAction())) {
            Log.i("GoogleMigrateService/onStartCommand()/action_cancel_import");
            C26451Dk r1 = this.A04;
            C005602s A00 = r1.A00(false);
            A00.A0A(r1.A00.A00.getResources().getString(R.string.google_migrate_notification_cancelling_import));
            A01(i2, A00.A01(), 31);
            this.A06.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 23, new RunnableBRunnable0Shape8S0100000_I0_8(this, 43)));
            return 1;
        } else if ("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_START_IMPORT".equals(intent.getAction())) {
            Log.i("GoogleMigrateService/onStartCommand()/action_start_import");
            C26451Dk r12 = this.A04;
            C005602s A002 = r12.A00(false);
            A002.A0A(r12.A00.A00.getResources().getString(R.string.google_migrate_notification_importing));
            A01(i2, A002.A01(), 31);
            this.A06.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 23, new RunnableBRunnable0Shape8S0100000_I0_8(this, 42)));
            return 1;
        } else {
            if ("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_PREPARE_BEFORE_RETRY".equals(intent.getAction())) {
                Log.i("GoogleMigrateService/onStartCommand()/prepare_before_retry");
                int intExtra = intent.getIntExtra("migration_error_code", 1);
                C26451Dk r13 = this.A04;
                C005602s A003 = r13.A00(false);
                A003.A0A(r13.A00.A00.getResources().getString(R.string.retry));
                A01(i2, A003.A01(), 31);
                this.A06.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 23, new RunnableBRunnable0Shape0S0101000_I0(this, intExtra, 12)));
                return 1;
            }
            return 1;
        }
        Log.i(str);
        return 1;
    }
}
