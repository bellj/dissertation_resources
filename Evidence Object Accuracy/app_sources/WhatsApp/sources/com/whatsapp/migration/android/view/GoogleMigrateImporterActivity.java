package com.whatsapp.migration.android.view;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass29H;
import X.AnonymousClass2FL;
import X.C002601e;
import X.C013606j;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C22900zp;
import X.C25661Ag;
import X.C35751ig;
import X.C42941w9;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.facebook.redex.IDxProviderShape16S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.backup.google.PromptDialogFragment;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.migration.android.integration.service.GoogleMigrateService;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class GoogleMigrateImporterActivity extends ActivityC13790kL implements AnonymousClass29H {
    public static final int A0C = 11;
    public static final int A0D = -1;
    public static final String A0E = "GoogleMigrateImporterActivity/";
    public static final String A0F = "com.whatsapp.migration.ACTION_OPENED_VIA_NOTIFICATION";
    public ViewTreeObserver.OnGlobalLayoutListener A00;
    public WaImageView A01;
    public WaTextView A02;
    public WaTextView A03;
    public GoogleMigrateImporterViewModel A04;
    public View A05;
    public WaButton A06;
    public WaButton A07;
    public WaTextView A08;
    public WaTextView A09;
    public RoundCornerProgressBar A0A;
    public boolean A0B;

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
    }

    public GoogleMigrateImporterActivity() {
        this(0);
        this.A00 = new IDxLListenerShape13S0100000_1_I1(this, 8);
    }

    public GoogleMigrateImporterActivity(int i) {
        this.A0B = false;
        A0B();
    }

    /* access modifiers changed from: private */
    public C013606j A0A(int i) {
        C013606j A01 = C013606j.A01(null, getResources(), i);
        AnonymousClass009.A06(A01, C12960it.A0W(i, "GoogleMigrateImporterActivity/getVectorDrawable/drawableId is invalid/drawableId = "));
        return A01;
    }

    private void A0B() {
        ActivityC13830kP.A1P(this, 88);
    }

    public static /* synthetic */ void A0D() {
    }

    public static /* synthetic */ void A0K() {
    }

    public static /* synthetic */ void A0L() {
    }

    public static /* synthetic */ void A0M() {
    }

    private void A0N(int i, int i2) {
        A0O(i, R.string.google_migrate_recoverable_import_error_title, i2, R.string.retry, R.string.google_migrate_skip_import);
    }

    private void A0O(int i, int i2, int i3, int i4, int i5) {
        String string;
        String string2;
        String num = Integer.toString(i);
        if (A0V().A0A(num) != null) {
            Log.i(C12960it.A0d(num, C12960it.A0k("GoogleMigrateImporterActivity/showDialog/dialog is already shown/dialogId = ")));
            return;
        }
        Bundle A0D2 = C12970iu.A0D();
        A0D2.putInt("dialog_id", i);
        String str = null;
        if (i2 == -1) {
            string = null;
        } else {
            string = getString(i2);
        }
        A0D2.putString("title", string);
        A0D2.putCharSequence("message", getString(i3));
        A0D2.putBoolean("cancelable", false);
        if (i4 == -1) {
            string2 = null;
        } else {
            string2 = getString(i4);
        }
        A0D2.putString("positive_button", string2);
        if (i5 != -1) {
            str = getString(i5);
        }
        A0D2.putString("negative_button", str);
        PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
        promptDialogFragment.A0U(A0D2);
        Adl(promptDialogFragment, num);
    }

    public static void A0Z(Context context) {
        C12990iw.A0D(context, GoogleMigrateImporterActivity.class).addFlags(335544320);
    }

    public static void A0p(GoogleMigrateImporterActivity googleMigrateImporterActivity, int i) {
        String str;
        googleMigrateImporterActivity.setResult(i);
        if (googleMigrateImporterActivity.getIntent() != null && A0F.equals(googleMigrateImporterActivity.getIntent().getAction())) {
            Intent A0D2 = C12990iw.A0D(googleMigrateImporterActivity, RegisterName.class);
            if (i == 2) {
                str = "google_migrate_import_canceled";
            } else {
                if (i == 1) {
                    str = "google_migrate_import_success";
                }
                googleMigrateImporterActivity.startActivity(A0D2);
            }
            A0D2.putExtra(str, true);
            googleMigrateImporterActivity.startActivity(A0D2);
        }
        googleMigrateImporterActivity.finish();
    }

    public static void A1J(GoogleMigrateImporterActivity googleMigrateImporterActivity, int i) {
        if (i == 100) {
            googleMigrateImporterActivity.A08.setVisibility(8);
            googleMigrateImporterActivity.A0A.setVisibility(8);
            return;
        }
        RoundCornerProgressBar roundCornerProgressBar = googleMigrateImporterActivity.A0A;
        if (i == -1) {
            roundCornerProgressBar.setVisibility(0);
            googleMigrateImporterActivity.A0A.setProgress(0);
            googleMigrateImporterActivity.A08.setVisibility(0);
            googleMigrateImporterActivity.A08.setText(R.string.loading_spinner);
        } else if (i >= 0) {
            roundCornerProgressBar.setVisibility(0);
            googleMigrateImporterActivity.A0A.setProgress(i);
            googleMigrateImporterActivity.A08.setVisibility(0);
            googleMigrateImporterActivity.A08.setText(C12960it.A0X(googleMigrateImporterActivity, C12970iu.A0r(((ActivityC13830kP) googleMigrateImporterActivity).A01, i), C12970iu.A1b(), 0, R.string.google_migrate_importing_percentage));
        } else {
            roundCornerProgressBar.setVisibility(8);
            googleMigrateImporterActivity.A08.setVisibility(8);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static void A1U(GoogleMigrateImporterActivity googleMigrateImporterActivity, Integer num) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        String string;
        String string2;
        String str;
        String string3;
        String str2;
        if (num == null) {
            Log.e("GoogleMigrateImporterActivity/currentScreen is null");
            return;
        }
        String string4 = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_header);
        String str3 = null;
        C002601e r4 = new C002601e(null, new IDxProviderShape16S0100000_2_I1(googleMigrateImporterActivity, 11));
        googleMigrateImporterActivity.A0A.setVisibility(8);
        googleMigrateImporterActivity.A08.setVisibility(8);
        switch (num.intValue()) {
            case 0:
                string = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_sub_title_need_permission);
                str2 = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_warning_need_permission);
                str = googleMigrateImporterActivity.getString(R.string.google_migrate_start_import);
                string3 = googleMigrateImporterActivity.getString(R.string.accessibility_drawable_google_migrate_start_import);
                string2 = null;
                str3 = str2;
                break;
            case 1:
                string4 = googleMigrateImporterActivity.getString(R.string.google_migrate_permissions_needed);
                string = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_sub_title_need_permission);
                str2 = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_warning_need_permission);
                str = googleMigrateImporterActivity.getString(R.string.google_migrate_start_import);
                string2 = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_do_not_import_permission_denied);
                r4 = new C002601e(null, new IDxProviderShape16S0100000_2_I1(googleMigrateImporterActivity, 10));
                string3 = googleMigrateImporterActivity.getString(R.string.accessibility_drawable_google_migrate_permission_rejected);
                str3 = str2;
                break;
            case 2:
            case 4:
            case 8:
            default:
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 3:
                string = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_sub_title_in_progress);
                string2 = googleMigrateImporterActivity.getString(R.string.cancel);
                googleMigrateImporterActivity.A0A.setVisibility(0);
                googleMigrateImporterActivity.AaN();
                string3 = googleMigrateImporterActivity.getString(R.string.accessibility_drawable_google_migrate_in_progress);
                str = null;
                break;
            case 5:
                string4 = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_title_import_complete);
                str = googleMigrateImporterActivity.getString(R.string.next);
                r4 = new C002601e(null, new IDxProviderShape16S0100000_2_I1(googleMigrateImporterActivity, 9));
                string3 = googleMigrateImporterActivity.getString(R.string.accessibility_drawable_google_migrate_completed);
                string = null;
                string2 = null;
                break;
            case 6:
            case 9:
                i7 = 1;
                i6 = R.string.google_migrate_unrecoverable_import_error_title;
                i5 = R.string.google_migrate_unrecoverable_import_error_message;
                i4 = R.string.google_migrate_skip_import;
                i3 = -1;
                googleMigrateImporterActivity.A0O(i7, i6, i5, i4, i3);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 7:
                string = googleMigrateImporterActivity.getString(R.string.google_migrate_importer_sub_title_in_progress);
                string2 = googleMigrateImporterActivity.getString(R.string.cancel);
                googleMigrateImporterActivity.A2C(R.string.google_migrate_cancelling);
                str = null;
                string3 = null;
                break;
            case 10:
                i7 = 3;
                i6 = R.string.import_cancel_dialog_title;
                i5 = R.string.import_cancel_dialog_message;
                i4 = R.string.import_cancel_dialog_cancel_button;
                i3 = R.string.google_migrate_resume;
                googleMigrateImporterActivity.A0O(i7, i6, i5, i4, i3);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 11:
                googleMigrateImporterActivity.A04.A08(true);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 12:
                i = 2;
                i2 = R.string.google_migrate_recoverable_import_error_message;
                googleMigrateImporterActivity.A0N(i, i2);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i7 = 8;
                i6 = R.string.google_migrate_encryption_key_wrong_phone_number_popup_title;
                i5 = R.string.google_migrate_encryption_key_wrong_phone_number;
                i4 = R.string.google_migrate_encryption_key_wrong_phone_number_try_another_number;
                i3 = R.string.google_migrate_skip_import;
                googleMigrateImporterActivity.A0O(i7, i6, i5, i4, i3);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                i7 = 9;
                i6 = R.string.google_migrate_recoverable_import_error_title;
                i5 = R.string.google_migrate_encryption_key_not_found;
                i4 = R.string.google_migrate_encryption_key_wrong_phone_number_try_another_number;
                i3 = R.string.google_migrate_skip_import;
                googleMigrateImporterActivity.A0O(i7, i6, i5, i4, i3);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 15:
                i = 6;
                i2 = R.string.google_migrate_encryption_key_timed_out;
                googleMigrateImporterActivity.A0N(i, i2);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                i = 7;
                i2 = R.string.check_connectivity;
                googleMigrateImporterActivity.A0N(i, i2);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
            case 17:
                googleMigrateImporterActivity.A2C(R.string.loading_spinner);
                string = null;
                str = null;
                string2 = null;
                string3 = null;
                break;
        }
        googleMigrateImporterActivity.A1a(r4, string4, string, str3, str, string2, string3);
    }

    public static void A1W(GoogleMigrateImporterActivity googleMigrateImporterActivity, boolean z) {
        if (!z) {
            googleMigrateImporterActivity.A0O(5, R.string.check_for_internet_connection, R.string.migration_failed_to_migrate_internet_reason, R.string.ok, -1);
        }
    }

    public static void A1X(GoogleMigrateImporterActivity googleMigrateImporterActivity, boolean z) {
        if (z) {
            int i = Build.VERSION.SDK_INT;
            int i2 = R.string.permission_storage_contacts_setting_on_import_from_google_migrate_v30;
            if (i < 30) {
                i2 = R.string.permission_storage_contacts_setting_on_import_from_google_migrate;
            }
            if (!googleMigrateImporterActivity.isFinishing()) {
                C35751ig r4 = new C35751ig(googleMigrateImporterActivity);
                r4.A09 = new int[]{R.drawable.permission_contacts, R.drawable.permission_plus, R.drawable.permission_storage};
                r4.A0C = new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
                r4.A02 = R.string.permission_storage_contacts_on_import_from_google_migrate;
                r4.A0A = null;
                r4.A03 = i2;
                r4.A08 = null;
                r4.A06 = false;
                googleMigrateImporterActivity.startActivityForResult(r4.A00(), 11);
            }
        }
    }

    private void A1a(C002601e r8, String str, String str2, String str3, String str4, String str5, String str6) {
        if (!TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str4) || !TextUtils.isEmpty(str5)) {
            this.A01.setImageDrawable((Drawable) r8.get());
            if (str6 != null) {
                this.A01.setFocusable(true);
            }
            this.A01.setContentDescription(str6);
            WaTextView waTextView = this.A02;
            int i = 0;
            int i2 = 8;
            if (str2 != null) {
                i2 = 0;
            }
            waTextView.setVisibility(i2);
            WaTextView waTextView2 = this.A09;
            int i3 = 8;
            if (str3 != null) {
                i3 = 0;
            }
            waTextView2.setVisibility(i3);
            WaButton waButton = this.A06;
            int i4 = 8;
            if (str4 != null) {
                i4 = 0;
            }
            waButton.setVisibility(i4);
            WaButton waButton2 = this.A07;
            if (str5 == null) {
                i = 8;
            }
            waButton2.setVisibility(i);
            boolean isEmpty = TextUtils.isEmpty(str5);
            Resources resources = getResources();
            int i5 = R.dimen.xp_migrate_importer_btn_margin_bottom;
            if (isEmpty) {
                i5 = R.dimen.xp_migrate_single_btn_bottom_margin;
            }
            int dimensionPixelSize = resources.getDimensionPixelSize(i5);
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this.A06);
            C42941w9.A09(this.A06, ((ActivityC13830kP) this).A01, A0H.leftMargin, A0H.topMargin, A0H.rightMargin, dimensionPixelSize);
            this.A03.setText(str);
            this.A02.setText(str2);
            this.A09.setText(str3);
            this.A06.setText(str4);
            this.A07.setText(str5);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel;
        boolean z;
        switch (i) {
            case 2:
            case 6:
            case 7:
            case 8:
            case 9:
                this.A04.A0D.A01("google_migrate_recoverable_error", "google_migrate_attempt_to_skip_import");
                A0O(4, R.string.import_cancel_dialog_title, R.string.import_cancel_dialog_message, R.string.import_cancel_dialog_cancel_button, R.string.google_migrate_resume);
                return;
            case 3:
                googleMigrateImporterViewModel = this.A04;
                z = false;
                break;
            case 4:
                googleMigrateImporterViewModel = this.A04;
                z = true;
                break;
            case 5:
            default:
                return;
        }
        googleMigrateImporterViewModel.A08(z);
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel;
        String str;
        boolean z = true;
        switch (i) {
            case 1:
                googleMigrateImporterViewModel = this.A04;
                z = false;
                break;
            case 2:
            case 6:
            case 7:
            case 8:
            case 9:
                this.A04.A08(true);
                return;
            case 3:
            case 4:
                googleMigrateImporterViewModel = this.A04;
                break;
            case 5:
            default:
                return;
        }
        C25661Ag r2 = googleMigrateImporterViewModel.A0D;
        if (z) {
            str = "google_migrate_cancel_import_dialog";
        } else {
            str = "google_migrate_unrecoverable_error";
        }
        r2.A02(str, "google_migrate_confirm_skip_import");
        Context context = googleMigrateImporterViewModel.A07.A00;
        C22900zp r22 = googleMigrateImporterViewModel.A0E;
        Log.i("GoogleMigrateService/cancelImport()");
        r22.A03(context, C12990iw.A0E("com.whatsapp.migration.android.integration.service.GoogleMigrateService.ACTION_CANCEL_IMPORT"), GoogleMigrateService.class);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        super.onActivityResult(i, i2, intent);
        if (i == 11 && i2 == -1) {
            GoogleMigrateImporterViewModel googleMigrateImporterViewModel = this.A04;
            if (googleMigrateImporterViewModel.A09.A0G()) {
                Log.i("GoogleMigrateImporterViewModel/onPermissionGranted/import already successfully finished, skipping import again");
                return;
            }
            googleMigrateImporterViewModel.A0D.A02("google_migrate_permission", "google_migrate_accepted_permission");
            googleMigrateImporterViewModel.A05();
            return;
        }
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel2 = this.A04;
        boolean A07 = googleMigrateImporterViewModel2.A08.A07();
        boolean A00 = googleMigrateImporterViewModel2.A05.A00();
        if (!A07) {
            str = !A00 ? "google_migrate_rejected_contact_and_storage_permission" : "google_migrate_rejected_storage_permission";
        } else {
            str = !A00 ? "google_migrate_rejected_contact_permission" : "unknown";
        }
        googleMigrateImporterViewModel2.A0D.A02("google_migrate_permission", str);
        googleMigrateImporterViewModel2.A06(1);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass12P.A03(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.google_migrate_importer_view);
        this.A03 = (WaTextView) AnonymousClass00T.A05(this, R.id.google_migrate_title);
        this.A02 = (WaTextView) AnonymousClass00T.A05(this, R.id.google_migrate_sub_title);
        this.A09 = (WaTextView) AnonymousClass00T.A05(this, R.id.google_migrate_warning);
        this.A06 = (WaButton) AnonymousClass00T.A05(this, R.id.google_migrate_main_action);
        this.A07 = (WaButton) AnonymousClass00T.A05(this, R.id.google_migrate_sub_action);
        this.A01 = (WaImageView) AnonymousClass00T.A05(this, R.id.google_migrate_image_view);
        this.A0A = (RoundCornerProgressBar) AnonymousClass00T.A05(this, R.id.google_migrate_progress_bar);
        this.A08 = (WaTextView) AnonymousClass00T.A05(this, R.id.google_migrate_progress_description);
        View findViewById = findViewById(R.id.google_migrate_importer_view_layout);
        this.A05 = findViewById;
        if (findViewById != null) {
            findViewById.getViewTreeObserver().addOnGlobalLayoutListener(this.A00);
        }
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel = (GoogleMigrateImporterViewModel) C13000ix.A02(this).A00(GoogleMigrateImporterViewModel.class);
        this.A04 = googleMigrateImporterViewModel;
        C12960it.A17(this, googleMigrateImporterViewModel.A01, 50);
        C12960it.A17(this, this.A04.A03, 49);
        C12960it.A17(this, this.A04.A00, 48);
        C12960it.A17(this, this.A04.A04, 46);
        C12960it.A17(this, this.A04.A02, 47);
        AbstractView$OnClickListenerC34281fs.A01(this.A06, this, 33);
        AbstractView$OnClickListenerC34281fs.A01(this.A07, this, 34);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AaN();
    }
}
