package com.whatsapp.migration.android.api;

import X.AnonymousClass00E;
import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.AnonymousClass30H;
import X.C12960it;
import X.C12970iu;
import X.C14850m9;
import X.C15570nT;
import X.C16120oU;
import X.C21390xL;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DeferredRestoreBroadcastReceiver extends BroadcastReceiver {
    public C15570nT A00;
    public C21390xL A01;
    public C14850m9 A02;
    public C16120oU A03;
    public final Object A04;
    public volatile boolean A05;

    public DeferredRestoreBroadcastReceiver() {
        this(0);
    }

    public DeferredRestoreBroadcastReceiver(int i) {
        this.A05 = false;
        this.A04 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        if (!this.A05) {
            synchronized (this.A04) {
                if (!this.A05) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A02 = C12960it.A0S(r1);
                    this.A00 = C12970iu.A0S(r1);
                    this.A03 = C12970iu.A0b(r1);
                    this.A01 = (C21390xL) r1.AGQ.get();
                    this.A05 = true;
                }
            }
        }
        Log.i("DeferredRestoreBroadcastReceiver/on-receive");
        if (intent != null && "com.google.android.apps.pixelmigrate.IOS_APP_DATA_AVAILABLE".equals(intent.getAction())) {
            if (!this.A02.A07(835)) {
                str = "DeferredRestoreBroadcastReceiver/sendWamEventIfApplicable/did not send data because ab prop is not enabled";
            } else {
                AnonymousClass30H r4 = new AnonymousClass30H();
                C15570nT r0 = this.A00;
                r0.A08();
                boolean z = false;
                r4.A01 = Boolean.valueOf(C12960it.A1W(r0.A00));
                try {
                    if (this.A01.A00("cross_platform_migration_completed", 0) != 0) {
                        z = true;
                    }
                    r4.A00 = Boolean.valueOf(z);
                } catch (RuntimeException e) {
                    Log.e("DeferredRestoreBroadcastReceiver/sendWamEventIfApplicable/", e);
                    r4.A00 = Boolean.FALSE;
                }
                this.A03.A0B(r4, new AnonymousClass00E(1, 1), true);
                str = "DeferredRestoreBroadcastReceiver/sendWamEventIfApplicable/sent wam event";
            }
            Log.i(str);
        }
    }
}
