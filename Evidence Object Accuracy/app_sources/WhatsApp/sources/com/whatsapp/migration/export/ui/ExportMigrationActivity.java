package com.whatsapp.migration.export.ui;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10J;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1Tv;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103214qO;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15730no;
import X.C15750nq;
import X.C15780nt;
import X.C15790nu;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16590pI;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22670zS;
import X.C22730zY;
import X.C22900zp;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C44891zj;
import X.C53312dp;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.MenuItem;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.migration.export.service.MessagesExporterService;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ExportMigrationActivity extends ActivityC13790kL {
    public C21740xu A00;
    public WaButton A01;
    public WaButton A02;
    public WaImageView A03;
    public WaTextView A04;
    public WaTextView A05;
    public WaTextView A06;
    public WaTextView A07;
    public WaTextView A08;
    public C22730zY A09;
    public AnonymousClass10J A0A;
    public RoundCornerProgressBar A0B;
    public C16590pI A0C;
    public C14950mJ A0D;
    public C22050yP A0E;
    public C16120oU A0F;
    public C15750nq A0G;
    public C15730no A0H;
    public ExportMigrationViewModel A0I;
    public C15780nt A0J;
    public C22900zp A0K;
    public String A0L;
    public boolean A0M;

    public ExportMigrationActivity() {
        this(0);
    }

    public ExportMigrationActivity(int i) {
        this.A0M = false;
        A0R(new C103214qO(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A00 = (C21740xu) r1.AM1.get();
            this.A0C = (C16590pI) r1.AMg.get();
            this.A0F = (C16120oU) r1.ANE.get();
            this.A0D = (C14950mJ) r1.AKf.get();
            this.A0K = (C22900zp) r1.A7t.get();
            this.A0H = (C15730no) r1.A6r.get();
            this.A0E = (C22050yP) r1.A7v.get();
            this.A0G = (C15750nq) r1.ACZ.get();
            this.A0J = (C15780nt) r1.A6z.get();
            this.A09 = (C22730zY) r1.A8Y.get();
            this.A0A = (AnonymousClass10J) r1.A8b.get();
        }
    }

    public final void A2e(int i) {
        Context context = this.A0C.A00;
        Log.i("xpm-export-service-cancelExport()");
        Intent intent = new Intent("ACTION_CANCEL_EXPORT");
        intent.setClass(context, MessagesExporterService.class);
        AnonymousClass1Tv.A00(context, intent);
        StringBuilder sb = new StringBuilder("ExportMigrationActivity/cancelMigrationAndReturn/resultCode: ");
        sb.append(i);
        Log.i(sb.toString());
        setResult(i);
        finish();
    }

    public final void A2f(long j) {
        String string = getString(R.string.move_chats_insufficient_space_title);
        String A04 = C44891zj.A04(((ActivityC13830kP) this).A01, j);
        AnonymousClass018 r4 = ((ActivityC13830kP) this).A01;
        runOnUiThread(new Runnable(string, r4.A0I(new Object[]{r4.A0F(A04)}, R.plurals.move_chats_insufficient_space_dialog, j), j) { // from class: X.2io
            public final /* synthetic */ long A00;
            public final /* synthetic */ String A02;
            public final /* synthetic */ String A03;

            {
                this.A02 = r2;
                this.A03 = r3;
                this.A00 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                String str = this.A02;
                String str2 = this.A03;
                long j2 = this.A00;
                C53312dp r42 = new C53312dp(exportMigrationActivity);
                r42.setTitle(str);
                r42.A0A(str2);
                r42.A0B(false);
                C12970iu.A1M(r42, exportMigrationActivity, 24, R.string.move_chats_manage_storage);
                r42.setNegativeButton(R.string.cancel, 
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0027: INVOKE  
                      (r4v0 'r42' X.2dp)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.cancel int)
                      (wrap: X.4ge : 0x0024: CONSTRUCTOR  (r0v3 X.4ge A[REMOVE]) = (r5v0 'exportMigrationActivity' com.whatsapp.migration.export.ui.ExportMigrationActivity), (r2v0 'j2' long) call: X.4ge.<init>(com.whatsapp.migration.export.ui.ExportMigrationActivity, long):void type: CONSTRUCTOR)
                     type: VIRTUAL call: X.02e.setNegativeButton(int, android.content.DialogInterface$OnClickListener):X.02e in method: X.2io.run():void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0024: CONSTRUCTOR  (r0v3 X.4ge A[REMOVE]) = (r5v0 'exportMigrationActivity' com.whatsapp.migration.export.ui.ExportMigrationActivity), (r2v0 'j2' long) call: X.4ge.<init>(com.whatsapp.migration.export.ui.ExportMigrationActivity, long):void type: CONSTRUCTOR in method: X.2io.run():void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.4ge, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    com.whatsapp.migration.export.ui.ExportMigrationActivity r5 = com.whatsapp.migration.export.ui.ExportMigrationActivity.this
                    java.lang.String r1 = r6.A02
                    java.lang.String r0 = r6.A03
                    long r2 = r6.A00
                    X.2dp r4 = new X.2dp
                    r4.<init>(r5)
                    r4.setTitle(r1)
                    r4.A0A(r0)
                    r0 = 0
                    r4.A0B(r0)
                    r1 = 2131889446(0x7f120d26, float:1.9413556E38)
                    r0 = 24
                    X.C12970iu.A1M(r4, r5, r0, r1)
                    r1 = 2131886925(0x7f12034d, float:1.9408443E38)
                    X.4ge r0 = new X.4ge
                    r0.<init>(r5, r2)
                    r4.setNegativeButton(r1, r0)
                    r4.A05()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55572io.run():void");
            }
        });
    }

    public final void A2g(Runnable runnable) {
        String string = getString(R.string.move_chats_mid_transfer_dialog);
        if (!this.A0H.A08()) {
            this.A0E.A0C(this.A0L, 15);
        }
        C53312dp r2 = new C53312dp(this);
        r2.A0A(string);
        r2.A03(new DialogInterface.OnClickListener() { // from class: X.4ft
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                exportMigrationActivity.A0E.A0C(exportMigrationActivity.A0L, 9);
            }
        }, getString(R.string.move_chats_cancel_transfer_positive_label));
        r2.A01(new DialogInterface.OnClickListener(runnable) { // from class: X.4gf
            public final /* synthetic */ Runnable A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                Runnable runnable2 = this.A01;
                exportMigrationActivity.A2e(0);
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        }, getString(R.string.move_chats_cancel_transfer_negative_label));
        r2.A05();
    }

    public final void A2h(Runnable runnable, Runnable runnable2, boolean z) {
        String string = getString(R.string.move_chats_cancel_transfer_title);
        String string2 = getString(R.string.move_chats_cancel_transfer_dialog);
        C53312dp r2 = new C53312dp(this);
        r2.setTitle(string);
        r2.A0A(string2);
        r2.A0B(z);
        r2.A03(new DialogInterface.OnClickListener(runnable) { // from class: X.4fu
            public final /* synthetic */ Runnable A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                this.A00.run();
            }
        }, getString(R.string.move_chats_cancel_transfer_positive_label));
        r2.A01(new DialogInterface.OnClickListener(runnable2) { // from class: X.4fv
            public final /* synthetic */ Runnable A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                this.A00.run();
            }
        }, getString(R.string.move_chats_cancel_transfer_negative_label));
        r2.A05();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2g(new RunnableBRunnable0Shape8S0100000_I0_8(this, 47));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String A02 = this.A0G.A02();
        this.A0L = A02;
        this.A0E.A0C(A02, 11);
        if (!((ActivityC13810kN) this).A0C.A07(843)) {
            this.A0E.A0D(this.A0L, 18);
            Log.e("ExportMigrationActivity/verifyActivityStartRequest; disabled: platform migration feature is disabled");
            setResult(102);
        } else {
            try {
                C15780nt r2 = this.A0J;
                synchronized (r2.A00) {
                }
                if (!r2.A02.A00("com.apple.movetoios")) {
                    this.A0E.A0D(this.A0L, 19);
                    Log.e("ExportMigrationActivity/onCreate/security: Permission validation failed");
                    ((ActivityC13810kN) this).A03.AaV("xpm-export-activity-permission-denied", null, false);
                    setResult(104);
                    this.A0G.A03();
                    super.finish();
                    return;
                }
                if (!this.A0H.A08()) {
                    C15570nT r0 = ((ActivityC13790kL) this).A01;
                    r0.A08();
                    if (r0.A05 == null) {
                        this.A0E.A0D(this.A0L, 21);
                        Log.e("ExportMigrationActivity/onCreate/user: no user logged in, skipping.");
                        startActivity(C14960mK.A05(this));
                        setResult(105);
                    }
                }
                if (this.A0H.A08()) {
                    C15790nu r1 = this.A0H.A0A;
                    if (!r1.A05()) {
                        r1.A03();
                    }
                    Log.i("ExportMigrationActivity/providerReady/finishing");
                    this.A0E.A0D(this.A0L, 24);
                    Log.i("ExportMigrationActivity/activateContentProviderAndFinishActivity");
                    setResult(100);
                    finish();
                    return;
                }
                this.A0E.A0C(this.A0L, 1);
                setContentView(R.layout.export_migration_view);
                setTitle(getString(R.string.move_chats_ios));
                AbstractC005102i A1U = A1U();
                if (A1U != null) {
                    A1U.A0M(true);
                }
                this.A07 = (WaTextView) AnonymousClass00T.A05(this, R.id.export_migrate_title);
                this.A06 = (WaTextView) AnonymousClass00T.A05(this, R.id.export_migrate_sub_title);
                this.A08 = (WaTextView) AnonymousClass00T.A05(this, R.id.export_migrate_warning);
                this.A04 = (WaTextView) AnonymousClass00T.A05(this, R.id.export_migrate_change_number_action);
                this.A01 = (WaButton) AnonymousClass00T.A05(this, R.id.export_migrate_main_action);
                this.A02 = (WaButton) AnonymousClass00T.A05(this, R.id.export_migrate_sub_action);
                this.A03 = (WaImageView) AnonymousClass00T.A05(this, R.id.export_migrate_image_view);
                this.A0B = (RoundCornerProgressBar) AnonymousClass00T.A05(this, R.id.export_migrate_progress_bar);
                this.A05 = (WaTextView) AnonymousClass00T.A05(this, R.id.export_migrate_progress_description);
                ExportMigrationViewModel exportMigrationViewModel = (ExportMigrationViewModel) new AnonymousClass02A(this).A00(ExportMigrationViewModel.class);
                this.A0I = exportMigrationViewModel;
                exportMigrationViewModel.A02.A05(this, new AnonymousClass02B() { // from class: X.3QY
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                        if (obj == null) {
                            Log.e("ExportMigrationActivity/onCurrentScreenChanged/screen is null");
                            return;
                        }
                        Log.i(C12960it.A0b("ExportMigrationActivity/onCurrentScreenChanged/screen=", obj));
                        AnonymousClass4U6 r5 = exportMigrationActivity.A0I.A05;
                        int i = r5.A03;
                        int i2 = r5.A06;
                        int i3 = r5.A00;
                        int i4 = r5.A04;
                        int i5 = r5.A0A;
                        exportMigrationActivity.A07.setText(r5.A08);
                        exportMigrationActivity.A06.setText(r5.A07);
                        if (i3 == 0) {
                            SpannableStringBuilder A0J = C12990iw.A0J(Html.fromHtml(exportMigrationActivity.getString(R.string.move_chats_edit_phone_number)));
                            URLSpan[] uRLSpanArr = (URLSpan[]) A0J.getSpans(0, A0J.length(), URLSpan.class);
                            if (uRLSpanArr != null) {
                                for (URLSpan uRLSpan : uRLSpanArr) {
                                    if ("edit-number".equals(uRLSpan.getURL())) {
                                        int spanStart = A0J.getSpanStart(uRLSpan);
                                        int spanEnd = A0J.getSpanEnd(uRLSpan);
                                        int spanFlags = A0J.getSpanFlags(uRLSpan);
                                        A0J.removeSpan(uRLSpan);
                                        A0J.setSpan(new C52222aS(exportMigrationActivity), spanStart, spanEnd, spanFlags);
                                    }
                                }
                            }
                            exportMigrationActivity.A04.setText(A0J);
                            exportMigrationActivity.A04.setLinkTextColor(AnonymousClass00T.A00(exportMigrationActivity, R.color.link_color));
                            exportMigrationActivity.A04.setMovementMethod(new LinkMovementMethod());
                        }
                        exportMigrationActivity.A04.setVisibility(i3);
                        if (i == 0) {
                            exportMigrationActivity.A01.setText(r5.A02);
                            exportMigrationActivity.A01.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(exportMigrationActivity, 37, obj));
                        }
                        exportMigrationActivity.A01.setVisibility(i);
                        if (i2 == 0) {
                            exportMigrationActivity.A02.setText(r5.A05);
                            exportMigrationActivity.A02.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(exportMigrationActivity, 38, obj));
                        }
                        exportMigrationActivity.A02.setVisibility(i2);
                        WaImageView waImageView = exportMigrationActivity.A03;
                        int i6 = r5.A01;
                        C013606j A01 = C013606j.A01(null, exportMigrationActivity.getResources(), i6);
                        AnonymousClass009.A06(A01, C12960it.A0W(i6, "ExportMigrationActivity/getVectorDrawable/drawableId is invalid/drawableId = "));
                        waImageView.setImageDrawable(A01);
                        exportMigrationActivity.A0B.setVisibility(i4);
                        exportMigrationActivity.A05.setVisibility(i4);
                        if (i4 == 0) {
                            exportMigrationActivity.A0B.setProgress(0);
                        }
                        exportMigrationActivity.A08.setVisibility(i5);
                        if (i5 == 0) {
                            exportMigrationActivity.A08.setText(r5.A09);
                        }
                    }
                });
                this.A0I.A00.A05(this, new AnonymousClass02B() { // from class: X.3QZ
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                        if (C12960it.A05(obj) == 1) {
                            String string = exportMigrationActivity.getString(R.string.move_chats_unknown_error);
                            C53312dp r22 = new C53312dp(exportMigrationActivity);
                            r22.A0A(string);
                            r22.A0B(false);
                            C12970iu.A1L(r22, exportMigrationActivity, 42, R.string.ok);
                            r22.A05();
                        }
                    }
                });
                this.A0I.A01.A05(this, new AnonymousClass02B() { // from class: X.3QX
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        ExportMigrationActivity exportMigrationActivity = ExportMigrationActivity.this;
                        int A05 = C12960it.A05(obj);
                        exportMigrationActivity.A0B.setProgress(A05);
                        exportMigrationActivity.A05.setText(C12960it.A0X(exportMigrationActivity, C12970iu.A0r(((ActivityC13830kP) exportMigrationActivity).A01, A05), C12970iu.A1b(), 0, R.string.move_chats_preparing_progress));
                    }
                });
                return;
            } catch (SecurityException e) {
                this.A0E.A0D(this.A0L, 20);
                Log.e("ExportMigrationActivity/onCreate/security: Permission validation failed", e);
                ((ActivityC13810kN) this).A03.A02("xpm-export-activity-permission-denied", null, e);
                setResult(104);
                this.A0G.A03();
                super.finish();
                return;
            }
        }
        this.A0G.A03();
        super.finish();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        A2g(null);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r2.A01 != null) goto L_0x001e;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r3 = this;
            super.onResume()
            X.0no r0 = r3.A0H
            boolean r0 = r0.A09()
            if (r0 == 0) goto L_0x0012
            com.whatsapp.migration.export.ui.ExportMigrationViewModel r1 = r3.A0I
            r0 = 5
        L_0x000e:
            r1.A04(r0)
            return
        L_0x0012:
            X.0no r2 = r3.A0H
            monitor-enter(r2)
            android.os.CancellationSignal r0 = r2.A00     // Catch: all -> 0x0033
            if (r0 != 0) goto L_0x001e
            java.util.concurrent.CountDownLatch r1 = r2.A01     // Catch: all -> 0x0033
            r0 = 0
            if (r1 == 0) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            monitor-exit(r2)
            if (r0 == 0) goto L_0x0026
            com.whatsapp.migration.export.ui.ExportMigrationViewModel r1 = r3.A0I
            r0 = 1
            goto L_0x000e
        L_0x0026:
            X.0lR r2 = r3.A05
            r1 = 49
            com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8 r0 = new com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8
            r0.<init>(r3, r1)
            r2.Ab2(r0)
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.migration.export.ui.ExportMigrationActivity.onResume():void");
    }
}
