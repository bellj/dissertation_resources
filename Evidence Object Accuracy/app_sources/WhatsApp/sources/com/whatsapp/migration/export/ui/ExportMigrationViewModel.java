package com.whatsapp.migration.export.ui;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass3YZ;
import X.AnonymousClass4U6;
import X.C12960it;
import X.C12980iv;
import X.C14850m9;
import X.C20170vK;
import X.C29941Vi;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ExportMigrationViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final C20170vK A03;
    public final AnonymousClass3YZ A04;
    public final AnonymousClass4U6 A05 = new AnonymousClass4U6();

    public ExportMigrationViewModel(C14850m9 r2, C20170vK r3) {
        int i;
        this.A03 = r3;
        AnonymousClass3YZ r0 = new AnonymousClass3YZ(this);
        this.A04 = r0;
        r3.A03(r0);
        if (r2.A07(881)) {
            Log.e("ExportMigrationViewModel/disabled: app version for platform migration is not supported");
            i = 4;
        } else {
            i = 0;
        }
        A04(i);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A03.A04(this.A04);
    }

    public void A04(int i) {
        int i2;
        Log.i(C12960it.A0W(i, "ExportMigrationViewModel/setScreen: "));
        Integer valueOf = Integer.valueOf(i);
        AnonymousClass016 r1 = this.A02;
        if (!C29941Vi.A00(valueOf, r1.A01())) {
            AnonymousClass4U6 r3 = this.A05;
            r3.A0A = 8;
            r3.A00 = 8;
            r3.A03 = 8;
            r3.A06 = 8;
            r3.A04 = 8;
            if (i != 0) {
                if (i != 1) {
                    if (i == 2) {
                        r3.A08 = R.string.move_chats_almost_done;
                        r3.A07 = R.string.move_chats_redirect_move_to_ios;
                        r3.A02 = R.string.next;
                        r3.A03 = 0;
                    } else if (i == 4) {
                        r3.A08 = R.string.update_whatsapp;
                        r3.A07 = R.string.move_chats_update_whatsapp_subtitle;
                        r3.A02 = R.string.upgrade;
                        r3.A03 = 0;
                        r3.A05 = R.string.not_now;
                        r3.A06 = 0;
                        r3.A0A = 8;
                        i2 = R.drawable.android_to_ios_error;
                    } else if (i == 5) {
                        r3.A08 = R.string.move_chats_cancelling;
                        r3.A07 = R.string.move_chats_cancellation_in_progress;
                        r3.A06 = 8;
                        r3.A04 = 8;
                    } else {
                        return;
                    }
                    r3.A0A = 8;
                } else {
                    r3.A08 = R.string.move_chats_preparing;
                    r3.A07 = R.string.move_chats_in_progress;
                    r3.A0A = 8;
                    r3.A06 = 0;
                    r3.A05 = R.string.cancel;
                    r3.A04 = 0;
                }
                i2 = R.drawable.android_to_ios_in_progress;
            } else {
                r3.A08 = R.string.move_chats_ios;
                r3.A07 = R.string.move_chats_ios_subtitle;
                r3.A00 = 0;
                r3.A02 = R.string.move_chats_start;
                r3.A03 = 0;
                r3.A09 = R.string.move_chats_ios_skip_warning;
                r3.A0A = 0;
                i2 = R.drawable.android_to_ios_start;
            }
            r3.A01 = i2;
            Log.i(C12960it.A0W(i, "ExportMigrationViewModel/setScreen/post="));
            r1.A0A(valueOf);
        }
    }
}
