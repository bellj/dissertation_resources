package com.whatsapp.migration.export.ui;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C013606j;
import X.C103224qP;
import X.C14330lG;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15730no;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C53312dp;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.migration.export.ui.ExportMigrationDataExportedActivity;

/* loaded from: classes2.dex */
public class ExportMigrationDataExportedActivity extends ActivityC13810kN {
    public C15730no A00;
    public boolean A01;

    public ExportMigrationDataExportedActivity() {
        this(0);
    }

    public ExportMigrationDataExportedActivity(int i) {
        this.A01 = false;
        A0R(new C103224qP(this));
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            this.A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            this.A0B = (AnonymousClass19M) r1.A6R.get();
            this.A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            this.A0D = (C18810t5) r1.AMu.get();
            this.A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            this.A00 = (C15730no) r1.A6r.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.export_migration_view);
        setTitle(getString(R.string.move_chats_ios));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.export_migrate_main_action);
        View A05 = AnonymousClass00T.A05(this, R.id.export_migrate_sub_action);
        textView.setVisibility(0);
        textView.setText(R.string.next);
        A05.setVisibility(8);
        C013606j A01 = C013606j.A01(null, getResources(), R.drawable.android_to_ios_in_progress);
        AnonymousClass009.A06(A01, "ExportMigrationDataExportedActivity/getVectorDrawable/drawableId is invalid");
        ((ImageView) AnonymousClass00T.A05(this, R.id.export_migrate_image_view)).setImageDrawable(A01);
        textView.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 0));
        ((TextView) AnonymousClass00T.A05(this, R.id.export_migrate_title)).setText(R.string.move_chats_almost_done);
        ((TextView) AnonymousClass00T.A05(this, R.id.export_migrate_sub_title)).setText(R.string.move_chats_finish_registering);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String string = getString(R.string.move_chats_mid_transfer_dialog);
        C53312dp r2 = new C53312dp(this);
        r2.A0A(string);
        r2.A03(null, getString(R.string.move_chats_cancel_transfer_positive_label));
        r2.A01(new DialogInterface.OnClickListener() { // from class: X.3KE
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ExportMigrationDataExportedActivity exportMigrationDataExportedActivity = ExportMigrationDataExportedActivity.this;
                ((ActivityC13830kP) exportMigrationDataExportedActivity).A05.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(exportMigrationDataExportedActivity, 0));
                exportMigrationDataExportedActivity.startActivity(C14960mK.A04(exportMigrationDataExportedActivity));
                exportMigrationDataExportedActivity.finish();
            }
        }, getString(R.string.move_chats_cancel_transfer_negative_label));
        r2.A05();
        return true;
    }
}
