package com.whatsapp.migration.export.encryption;

import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass042;
import X.AnonymousClass043;
import X.AnonymousClass0GL;
import X.AnonymousClass114;
import X.C006503b;
import X.C12990iw;
import android.content.Context;
import android.os.CancellationSignal;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/* loaded from: classes2.dex */
public class ExportEncryptionManager$KeyPrefetchWorker extends Worker {
    public final AbstractC15710nm A00;
    public final AnonymousClass114 A01;

    public ExportEncryptionManager$KeyPrefetchWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J A0S = C12990iw.A0S(context.getApplicationContext());
        this.A00 = A0S.A7p();
        this.A01 = (AnonymousClass114) A0S.A6p.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        try {
            this.A01.A01(new CancellationSignal());
            return new AnonymousClass0GL(C006503b.A01);
        } catch (Exception e) {
            this.A00.A02("xpm-export-prefetch-key", e.toString(), e);
            return new AnonymousClass042();
        }
    }
}
