package com.whatsapp.migration.export.service;

import X.AbstractServiceC621634y;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass3F9;
import X.AnonymousClass3YY;
import X.AnonymousClass5B7;
import X.C12960it;
import X.C12970iu;
import X.C15730no;
import X.C18360sK;
import X.C20170vK;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import android.os.IBinder;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class MessagesExporterService extends AbstractServiceC621634y implements AnonymousClass004 {
    public C15730no A00;
    public AnonymousClass3F9 A01;
    public C20170vK A02;
    public AnonymousClass3YY A03;
    public boolean A04 = false;
    public final Object A05 = C12970iu.A0l();
    public volatile C71083cM A06;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A06 == null) {
            synchronized (this.A05) {
                if (this.A06 == null) {
                    this.A06 = new C71083cM(this);
                }
            }
        }
        return this.A06.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            ((AbstractServiceC621634y) this).A01 = C12970iu.A0Q(r1);
            super.A02 = C12960it.A0T(r1);
            this.A00 = (C15730no) r1.A6r.get();
            this.A02 = (C20170vK) r1.ACS.get();
            this.A01 = new AnonymousClass3F9(C12970iu.A0X(r1), (C18360sK) r1.AN0.get(), C12960it.A0R(r1));
        }
        super.onCreate();
        AnonymousClass3YY r12 = new AnonymousClass3YY(this);
        this.A03 = r12;
        this.A02.A03(r12);
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.i("xpm-export-service-onDestroy()");
        super.onDestroy();
        this.A02.A04(this.A03);
        stopForeground(false);
    }
}
