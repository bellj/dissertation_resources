package com.whatsapp.jid;

import X.AbstractC14640lm;
import X.AnonymousClass1MW;
import X.AnonymousClass1US;
import X.C29961Vk;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* loaded from: classes2.dex */
public abstract class UserJid extends AbstractC14640lm implements Parcelable {
    public static final C29961Vk JID_FACTORY = C29961Vk.A00();

    @Override // com.whatsapp.jid.Jid
    public abstract String getServer();

    @Override // com.whatsapp.jid.Jid
    public abstract int getType();

    public UserJid(Parcel parcel) {
        super(parcel);
    }

    public UserJid(String str) {
        super(str);
    }

    public static UserJid get(String str) {
        Jid jid = Jid.get(str);
        if (jid instanceof UserJid) {
            return (UserJid) jid;
        }
        throw new AnonymousClass1MW(str);
    }

    public static UserJid getNullable(String str) {
        UserJid userJid = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            userJid = get(str);
            return userJid;
        } catch (AnonymousClass1MW unused) {
            return userJid;
        }
    }

    @Override // com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass1US.A02(4, this.user));
        sb.append('@');
        sb.append(getServer());
        return sb.toString();
    }

    public DeviceJid getPrimaryDevice() {
        try {
            return DeviceJid.getFromUserJidAndDeviceId(this, 0);
        } catch (AnonymousClass1MW e) {
            throw new IllegalStateException(e);
        }
    }

    public static UserJid of(Jid jid) {
        if (jid instanceof UserJid) {
            return (UserJid) jid;
        }
        return null;
    }
}
