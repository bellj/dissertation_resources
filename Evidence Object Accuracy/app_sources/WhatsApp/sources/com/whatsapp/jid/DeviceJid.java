package com.whatsapp.jid;

import X.AnonymousClass009;
import X.AnonymousClass1MW;
import X.AnonymousClass1US;
import X.C29831Uv;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* loaded from: classes2.dex */
public abstract class DeviceJid extends Jid implements Parcelable {
    public final byte device;
    public final UserJid userJid;

    @Override // com.whatsapp.jid.Jid, android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // com.whatsapp.jid.Jid
    public abstract String getServer();

    @Override // com.whatsapp.jid.Jid
    public abstract int getType();

    public DeviceJid(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(UserJid.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.userJid = (UserJid) readParcelable;
        this.device = parcel.readByte();
    }

    public DeviceJid(UserJid userJid, int i) {
        super(userJid.user);
        if (userJid != C29831Uv.A00) {
            this.userJid = userJid;
            if (i < 0 || i > 99) {
                StringBuilder sb = new StringBuilder("Invalid device: ");
                sb.append(i);
                throw new AnonymousClass1MW(sb.toString());
            }
            this.device = (byte) i;
            return;
        }
        throw new AnonymousClass1MW(userJid);
    }

    public static String buildRawString(String str, String str2, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('.');
        sb.append(i);
        sb.append(':');
        sb.append(i2);
        sb.append('@');
        sb.append(str2);
        return sb.toString();
    }

    @Override // com.whatsapp.jid.Jid, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && super.equals(obj)) {
            DeviceJid deviceJid = (DeviceJid) obj;
            if (this.device == deviceJid.device) {
                return this.userJid.equals(deviceJid.userJid);
            }
        }
        return false;
    }

    public static DeviceJid get(String str) {
        DeviceJid of;
        Jid jid = Jid.get(str);
        if (jid instanceof DeviceJid) {
            return (DeviceJid) jid;
        }
        if ((jid instanceof UserJid) && (of = of(jid)) != null) {
            return of;
        }
        throw new AnonymousClass1MW(str);
    }

    @Override // com.whatsapp.jid.Jid
    public int getDevice() {
        return this.device;
    }

    public static DeviceJid getFromUserJidAndDeviceId(UserJid userJid, int i) {
        return get(buildRawString(userJid.user, userJid.getServer(), userJid.getAgent(), i));
    }

    public static DeviceJid getNullable(String str) {
        DeviceJid deviceJid = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            deviceJid = get(str);
            return deviceJid;
        } catch (AnonymousClass1MW unused) {
            return deviceJid;
        }
    }

    @Override // com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass1US.A02(4, this.user));
        sb.append(':');
        sb.append((int) this.device);
        sb.append('@');
        sb.append(getServer());
        return sb.toString();
    }

    @Override // com.whatsapp.jid.Jid
    public String getRawString() {
        return buildRawString(this.user, getServer(), getAgent(), this.device);
    }

    public UserJid getUserJid() {
        return this.userJid;
    }

    @Override // com.whatsapp.jid.Jid, java.lang.Object
    public int hashCode() {
        return (((super.hashCode() * 31) + this.userJid.hashCode()) * 31) + this.device;
    }

    public boolean isPrimary() {
        return this.device == 0;
    }

    public static DeviceJid of(Jid jid) {
        if (jid instanceof DeviceJid) {
            return (DeviceJid) jid;
        }
        if (jid instanceof UserJid) {
            return ((UserJid) jid).getPrimaryDevice();
        }
        return null;
    }

    @Override // com.whatsapp.jid.Jid, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.userJid, i);
        parcel.writeByte(this.device);
    }
}
