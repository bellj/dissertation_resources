package com.whatsapp.jid;

import X.AnonymousClass1MW;
import X.C29941Vi;
import X.C29961Vk;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Arrays;

/* loaded from: classes2.dex */
public abstract class Jid implements Comparable, Parcelable {
    public static final C29961Vk JID_FACTORY = C29961Vk.A00();
    public final String user;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Deprecated
    public int getAgent() {
        return 0;
    }

    public int getDevice() {
        return 0;
    }

    public abstract String getServer();

    public abstract int getType();

    public Jid(Parcel parcel) {
        this.user = parcel.readString();
    }

    public Jid(String str) {
        this.user = str;
    }

    public static String buildRawString(String str, String str2) {
        if (str.isEmpty()) {
            return str2;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('@');
        sb.append(str2);
        return sb.toString();
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return getRawString().compareTo(((Jid) obj).getRawString());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Jid)) {
            return false;
        }
        Jid jid = (Jid) obj;
        if (!C29941Vi.A00(this.user, jid.user) || !getServer().equals(jid.getServer()) || getType() != jid.getType()) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007f, code lost:
        if (r7.equals("lid") == false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.whatsapp.jid.Jid get(java.lang.String r10) {
        /*
        // Method dump skipped, instructions count: 386
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.jid.Jid.get(java.lang.String):com.whatsapp.jid.Jid");
    }

    public static Jid getNullable(String str) {
        Jid jid = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            jid = get(str);
            return jid;
        } catch (AnonymousClass1MW unused) {
            return jid;
        }
    }

    public String getObfuscatedString() {
        return getRawString();
    }

    public String getRawString() {
        return buildRawString(this.user, getServer());
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.user, getServer(), Integer.valueOf(getType())});
    }

    public boolean isProtocolCompliant() {
        int type = getType();
        return (type == 2 || type == 9 || type == 11 || type == 8) ? false : true;
    }

    @Override // java.lang.Object
    public final String toString() {
        return getObfuscatedString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.user);
    }
}
