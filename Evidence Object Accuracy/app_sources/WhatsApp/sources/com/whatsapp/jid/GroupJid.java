package com.whatsapp.jid;

import X.AbstractC15590nW;
import X.AnonymousClass1MW;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* loaded from: classes2.dex */
public abstract class GroupJid extends AbstractC15590nW implements Parcelable {
    public GroupJid(Parcel parcel) {
        super(parcel);
    }

    public GroupJid(String str) {
        super(str);
    }

    public static GroupJid getNullable(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Jid jid = Jid.get(str);
            if (jid instanceof GroupJid) {
                return (GroupJid) jid;
            }
            throw new AnonymousClass1MW(str);
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }

    public static GroupJid of(Jid jid) {
        if (jid instanceof GroupJid) {
            return (GroupJid) jid;
        }
        return null;
    }
}
