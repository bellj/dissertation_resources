package com.whatsapp.businesscollection.view.activity;

import X.AbstractActivityC59392ue;
import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass11G;
import X.AnonymousClass19T;
import X.AnonymousClass1CQ;
import X.AnonymousClass1FZ;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass5TP;
import X.AnonymousClass5TQ;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C19840ul;
import X.C19850um;
import X.C21770xx;
import X.C25791Av;
import X.C25811Ax;
import X.C37071lG;
import X.C48882Ih;
import android.os.Bundle;
import android.view.Menu;
import android.view.ViewStub;
import com.whatsapp.R;
import com.whatsapp.businesscollection.view.activity.CollectionProductListActivity;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CollectionProductListActivity extends AbstractActivityC59392ue implements AnonymousClass5TQ {
    public AnonymousClass1CQ A00;
    public boolean A01;

    public CollectionProductListActivity() {
        this(0);
    }

    public CollectionProductListActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 23);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(r2, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC59392ue) this).A0L = (C19840ul) A1M.A1Q.get();
            ((AbstractActivityC59392ue) this).A06 = (C21770xx) A1M.A2s.get();
            ((AbstractActivityC59392ue) this).A05 = (C25791Av) A1M.A2t.get();
            ((AbstractActivityC59392ue) this).A0C = (AnonymousClass19T) A1M.A2y.get();
            ((AbstractActivityC59392ue) this).A0G = C12960it.A0O(A1M);
            ((AbstractActivityC59392ue) this).A0I = C12960it.A0P(A1M);
            ((AbstractActivityC59392ue) this).A0J = (AnonymousClass11G) A1M.AKq.get();
            ((AbstractActivityC59392ue) this).A09 = (C19850um) A1M.A2v.get();
            ((AbstractActivityC59392ue) this).A0H = C12980iv.A0a(A1M);
            ((AbstractActivityC59392ue) this).A0B = C12980iv.A0Z(A1M);
            ((AbstractActivityC59392ue) this).A03 = (C48882Ih) r2.A0Q.get();
            ((AbstractActivityC59392ue) this).A0D = new C37071lG(C12990iw.A0V(A1M));
            ((AbstractActivityC59392ue) this).A08 = (AnonymousClass1FZ) A1M.AGJ.get();
            ((AbstractActivityC59392ue) this).A0A = (C25811Ax) A1M.A2w.get();
            this.A00 = r2.A02();
        }
    }

    @Override // X.AnonymousClass5TQ
    public void ANm() {
        ((AbstractActivityC59392ue) this).A0E.A03.A00();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass01E A0A = A0V().A0A("CatalogSearchFragmentTag");
        if (A0A == null || !(A0A instanceof CatalogSearchFragment) || !((CatalogSearchFragment) A0A).A1F()) {
            super.onBackPressed();
        }
    }

    @Override // X.AbstractActivityC59392ue, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((ViewStub) findViewById(R.id.stub_toolbar_search)).inflate();
        A1e(ActivityC13790kL.A0Q(this));
        String str = this.A0P;
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            if (str != null) {
                A1U.A0I(str);
            }
        }
        this.A00.A00(new AnonymousClass5TP() { // from class: X.3VJ
            @Override // X.AnonymousClass5TP
            public final void AQG(UserJid userJid) {
                C004902f A0P = C12970iu.A0P(CollectionProductListActivity.this);
                A0P.A0B(AnonymousClass3AU.A00(userJid, 2), "CatalogSearchFragmentTag", R.id.catalog_search_host);
                A0P.A01();
            }
        }, ((AbstractActivityC59392ue) this).A0K);
    }

    @Override // X.AbstractActivityC59392ue, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.collection_product_list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
