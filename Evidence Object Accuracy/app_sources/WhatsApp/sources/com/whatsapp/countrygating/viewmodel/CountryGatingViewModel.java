package com.whatsapp.countrygating.viewmodel;

import X.AnonymousClass015;
import X.C14850m9;
import X.C22700zV;
import X.C28061Kl;
import com.whatsapp.jid.UserJid;

/* loaded from: classes3.dex */
public class CountryGatingViewModel extends AnonymousClass015 {
    public boolean A00;
    public final C22700zV A01;
    public final C14850m9 A02;

    public CountryGatingViewModel(C22700zV r1, C14850m9 r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    public boolean A04(UserJid userJid) {
        return C28061Kl.A01(this.A01, this.A02, userJid);
    }
}
