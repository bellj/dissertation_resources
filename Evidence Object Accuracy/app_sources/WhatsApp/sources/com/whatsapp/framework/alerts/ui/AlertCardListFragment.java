package com.whatsapp.framework.alerts.ui;

import X.AnonymousClass015;
import X.AnonymousClass01Y;
import X.AnonymousClass02A;
import X.AnonymousClass0RD;
import X.AnonymousClass0SZ;
import X.AnonymousClass4JE;
import X.AnonymousClass5UD;
import X.C105554uF;
import X.C12960it;
import X.C16700pc;
import X.C18050rp;
import X.C54222gN;
import X.C74463i4;
import X.C74553iI;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public final class AlertCardListFragment extends Hilt_AlertCardListFragment implements AnonymousClass5UD {
    public RecyclerView A00;
    public AnonymousClass4JE A01;
    public C18050rp A02;
    public C54222gN A03;
    public C74463i4 A04;

    public static /* synthetic */ void A00(AlertCardListFragment alertCardListFragment, List list) {
        C16700pc.A0E(alertCardListFragment, 0);
        C54222gN r3 = alertCardListFragment.A03;
        if (r3 == null) {
            throw C16700pc.A06("alertsListAdapter");
        }
        C16700pc.A0B(list);
        List A07 = AnonymousClass01Y.A07(AnonymousClass01Y.A05(list));
        List list2 = r3.A01;
        AnonymousClass0SZ A00 = AnonymousClass0RD.A00(new C74553iI(list2, A07));
        list2.clear();
        list2.addAll(A07);
        A00.A02(r3);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        return layoutInflater.inflate(R.layout.alert_list_fragment, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        C74463i4 r0 = this.A04;
        if (r0 == null) {
            throw C16700pc.A06("alertListViewModel");
        }
        C12960it.A17(this, r0.A00, 43);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        AnonymousClass015 A00 = new AnonymousClass02A(new C105554uF(this), A0C()).A00(C74463i4.class);
        C16700pc.A0B(A00);
        C74463i4 r0 = (C74463i4) A00;
        this.A04 = r0;
        if (r0 == null) {
            throw C16700pc.A06("alertListViewModel");
        }
        r0.A00.A0A(r0.A01.A02());
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        this.A00 = (RecyclerView) C16700pc.A03(view, R.id.alert_card_list);
        C54222gN r1 = new C54222gN(this, C12960it.A0l());
        this.A03 = r1;
        RecyclerView recyclerView = this.A00;
        if (recyclerView == null) {
            throw C16700pc.A06("alertsList");
        }
        recyclerView.setAdapter(r1);
    }

    public final C18050rp A1K() {
        C18050rp r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("alertStorage");
    }

    public final void A1L() {
        if (this.A01 == null) {
            throw C16700pc.A06("alertListViewModelFactory");
        }
    }
}
