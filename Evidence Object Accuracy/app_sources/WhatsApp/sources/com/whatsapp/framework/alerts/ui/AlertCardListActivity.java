package com.whatsapp.framework.alerts.ui;

import X.AbstractActivityC119195d4;
import X.AbstractC005102i;
import X.AbstractC16710pd;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass4Yq;
import X.C004902f;
import X.C113855Jg;
import X.C12970iu;
import X.C12990iw;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class AlertCardListActivity extends AbstractActivityC119195d4 {
    public final AbstractC16710pd A00 = AnonymousClass4Yq.A00(new C113855Jg(this));

    public static final /* synthetic */ AlertCardListFragment A02() {
        return new AlertCardListFragment();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.alert_card_list_layout);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I(getString(R.string.alert_card_list_activity_title));
        }
        AbstractC005102i A1U2 = A1U();
        if (A1U2 != null) {
            A1U2.A0M(true);
        }
        AbstractC005102i A1U3 = A1U();
        if (A1U3 != null) {
            A1U3.A0D(AnonymousClass00T.A04(this, R.drawable.ic_back));
        }
        Bundle bundle2 = new Bundle(C12990iw.A0H(this));
        AbstractC16710pd r1 = this.A00;
        ((AnonymousClass01E) r1.getValue()).A0U(bundle2);
        C004902f A0P = C12970iu.A0P(this);
        A0P.A0A((AnonymousClass01E) r1.getValue(), null, R.id.alert_list_fragment_container);
        A0P.A01();
    }
}
