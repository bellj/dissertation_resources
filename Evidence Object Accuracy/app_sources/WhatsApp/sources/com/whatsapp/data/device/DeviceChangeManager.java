package com.whatsapp.data.device;

import X.AbstractC14640lm;
import X.AbstractC15590nW;
import X.AnonymousClass009;
import X.AnonymousClass1JO;
import X.AnonymousClass1XB;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C15380n4;
import X.C15570nT;
import X.C15600nX;
import X.C15650ng;
import X.C15680nj;
import X.C15990oG;
import X.C18240s8;
import X.C18770sz;
import X.C20870wS;
import X.C22130yZ;
import X.C22140ya;
import X.C27631Ih;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
public class DeviceChangeManager {
    public final C15570nT A00;
    public final C20870wS A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C15990oG A04;
    public final C18240s8 A05;
    public final C15680nj A06;
    public final C15650ng A07;
    public final C15600nX A08;
    public final C22130yZ A09;
    public final C18770sz A0A;
    public final C14850m9 A0B;
    public final C14840m8 A0C;
    public final C22140ya A0D;

    public DeviceChangeManager(C15570nT r1, C20870wS r2, C14830m7 r3, C14820m6 r4, C15990oG r5, C18240s8 r6, C15680nj r7, C15650ng r8, C15600nX r9, C22130yZ r10, C18770sz r11, C14850m9 r12, C14840m8 r13, C22140ya r14) {
        this.A02 = r3;
        this.A0B = r12;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A07 = r8;
        this.A0C = r13;
        this.A04 = r5;
        this.A0A = r11;
        this.A03 = r4;
        this.A09 = r10;
        this.A06 = r7;
        this.A0D = r14;
        this.A08 = r9;
    }

    public final Set A00(UserJid userJid) {
        HashSet hashSet = new HashSet();
        C15570nT r4 = this.A00;
        r4.A08();
        C27631Ih r0 = r4.A05;
        AnonymousClass009.A05(r0);
        Set A01 = A01(r0);
        for (AbstractC15590nW r2 : A01(userJid)) {
            if (A01.contains(r2)) {
                Set set = this.A08.A02(r2).A06().A00;
                if (set.contains(userJid)) {
                    r4.A08();
                    if (set.contains(r4.A05) || C15380n4.A0F(r2)) {
                        hashSet.add(r2);
                    }
                }
            }
        }
        return hashSet;
    }

    public final Set A01(UserJid userJid) {
        if (this.A00.A0F(userJid)) {
            return new HashSet(this.A06.A06());
        }
        return this.A08.A05(userJid);
    }

    public void A02(AnonymousClass1JO r17, AnonymousClass1JO r18, AnonymousClass1JO r19, UserJid userJid, boolean z) {
        AnonymousClass1XB A02;
        AnonymousClass1XB A022;
        boolean z2 = this.A03.A00.getBoolean("security_notifications", false);
        boolean z3 = true;
        boolean z4 = !this.A09.A0C.A07(903);
        if (!z4 && !z) {
            z3 = false;
        }
        if (this.A0A.A06.A05() && z2 && z3) {
            r18.toString();
            r19.toString();
            C15570nT r5 = this.A00;
            if (r5.A0F(userJid)) {
                for (AbstractC14640lm r10 : this.A06.A04()) {
                    if (!r5.A0F(r10) && z4) {
                        this.A07.A0p(this.A0D.A01(r10, userJid, r18.A00.size(), r19.A00.size(), this.A02.A00()));
                    }
                }
            } else if (!r17.A00.isEmpty()) {
                if (this.A06.A0D(userJid)) {
                    C15650ng r6 = this.A07;
                    if (z4) {
                        A022 = this.A0D.A01(userJid, userJid, r18.A00.size(), r19.A00.size(), this.A02.A00());
                    } else {
                        A022 = this.A0D.A02(userJid, userJid, this.A02.A00());
                    }
                    r6.A0p(A022);
                }
                for (AbstractC14640lm r102 : A00(userJid)) {
                    C15650ng r62 = this.A07;
                    if (z4) {
                        A02 = this.A0D.A01(r102, userJid, r18.A00.size(), r19.A00.size(), this.A02.A00());
                    } else {
                        A02 = this.A0D.A02(r102, userJid, this.A02.A00());
                    }
                    r62.A0p(A02);
                }
            }
        }
    }
}
