package com.whatsapp.data;

import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractFutureC44231yX;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass4NY;
import X.C005602s;
import X.C05360Pg;
import X.C15600nX;
import X.C15650ng;
import X.C16510p9;
import X.C18360sK;
import X.C19990v2;
import X.C21320xE;
import X.C21600xg;
import X.C21610xh;
import X.C22630zO;
import X.C81353ts;
import android.app.Notification;
import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* loaded from: classes2.dex */
public class ConversationDeleteWorker extends Worker {
    public static final ConcurrentHashMap A0B = new ConcurrentHashMap();
    public static final AtomicInteger A0C = new AtomicInteger();
    public static final AtomicInteger A0D = new AtomicInteger();
    public static final AtomicInteger A0E = new AtomicInteger();
    public static final AtomicLong A0F = new AtomicLong();
    public final Context A00;
    public final AbstractC15710nm A01;
    public final C18360sK A02;
    public final AnonymousClass018 A03;
    public final C16510p9 A04;
    public final C19990v2 A05;
    public final C21320xE A06;
    public final C15650ng A07;
    public final C21610xh A08;
    public final C15600nX A09;
    public final C21600xg A0A;

    public ConversationDeleteWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = context;
        this.A04 = (C16510p9) r1.A3J.get();
        this.A01 = r1.A7p();
        this.A05 = (C19990v2) r1.A3M.get();
        this.A03 = r1.Ag8();
        this.A07 = (C15650ng) r1.A4m.get();
        this.A08 = (C21610xh) r1.A5Z.get();
        this.A0A = (C21600xg) r1.AKc.get();
        this.A06 = (C21320xE) r1.A4Y.get();
        this.A02 = (C18360sK) r1.AN0.get();
        this.A09 = (C15600nX) r1.A8x.get();
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A00() {
        Context context = this.A00;
        String string = context.getString(R.string.delete_wait_progress);
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "other_notifications@1";
        A00.A03 = -1;
        C18360sK.A01(A00, R.drawable.notifybar);
        if (Build.VERSION.SDK_INT >= 21) {
            A00.A0I = "progress";
            A00.A06 = -1;
        }
        A00.A03(100, 0, true);
        A00.A0D(false);
        A00.A0E(true);
        A00.A0A(string);
        A00.A09("");
        Notification A01 = A00.A01();
        C81353ts r2 = new C81353ts();
        r2.A04(new C05360Pg(13, A01, 0));
        return r2;
    }

    @Override // androidx.work.ListenableWorker
    public void A03() {
        A0D.addAndGet(-1);
        A05();
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0201  */
    @Override // androidx.work.Worker
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass043 A04() {
        /*
        // Method dump skipped, instructions count: 548
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.data.ConversationDeleteWorker.A04():X.043");
    }

    public final void A05() {
        AtomicInteger atomicInteger = A0D;
        if (atomicInteger.get() <= 0) {
            A0C.set(0);
            A0E.set(0);
            A0F.set(0);
            atomicInteger.set(0);
            A0B.clear();
            this.A02.A04(13, null);
        }
    }

    public void A06(AbstractC14640lm r13, int i) {
        int max;
        AnonymousClass4NY r2 = (AnonymousClass4NY) A0B.get(r13);
        synchronized (r2) {
            int i2 = r2.A00;
            max = Math.max(0, i - i2);
            r2.A00 = i2 + max;
            r2.A01 -= max;
        }
        AtomicInteger atomicInteger = A0C;
        atomicInteger.addAndGet(max);
        AtomicInteger atomicInteger2 = A0E;
        if (atomicInteger2.get() == 0 || atomicInteger.get() > atomicInteger2.get()) {
            Log.w("conversation-delete-worker/delete-progress/totalMessagesAllJids not updated.");
            return;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        AtomicLong atomicLong = A0F;
        if (uptimeMillis - atomicLong.get() >= 250) {
            atomicLong.set(uptimeMillis);
            int i3 = (atomicInteger.get() * 100) / atomicInteger2.get();
            Context context = this.A00;
            String string = context.getString(R.string.delete_wait_progress);
            String string2 = context.getString(R.string.delete_wait_progress_text_with_percentage, Integer.valueOf(atomicInteger.get()), Integer.valueOf(atomicInteger2.get()), this.A03.A0K().format(((double) i3) / 100.0d));
            C005602s A00 = C22630zO.A00(context);
            A00.A0J = "other_notifications@1";
            A00.A03 = -1;
            C18360sK.A01(A00, R.drawable.notifybar);
            if (Build.VERSION.SDK_INT >= 21) {
                A00.A0I = "progress";
                A00.A06 = -1;
            }
            A00.A03(100, i3, false);
            A00.A0D(false);
            A00.A0E(true);
            A00.A0A(string);
            A00.A09(string2);
            this.A02.A03(13, A00.A01());
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 8, insn: 0x016d: IGET  (r0 I:X.0xh) = (r8 I:com.whatsapp.data.ConversationDeleteWorker) com.whatsapp.data.ConversationDeleteWorker.A08 X.0xh, block:B:36:0x0168
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final boolean A07(
/*
[787] Method generation error in method: com.whatsapp.data.ConversationDeleteWorker.A07(X.1Hl):boolean, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r38v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/
}
