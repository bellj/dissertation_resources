package com.whatsapp;

import X.AbstractC116435Vk;
import X.AnonymousClass00X;
import X.AnonymousClass028;
import X.AnonymousClass3MH;
import X.AnonymousClass3U5;
import X.AnonymousClass5TB;
import X.C14280l9;
import X.C42941w9;
import X.C51282Tp;
import X.C53522ea;
import X.C72733f5;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class CodeInputField extends WaEditText {
    public static Typeface A08;
    public static Typeface A09;
    public char A00;
    public char A01;
    public int A02;
    public ValueAnimator A03;
    public AnonymousClass3MH A04;
    public boolean A05;
    public boolean A06;
    public final Context A07;

    public CodeInputField(Context context) {
        super(context);
        A02();
        this.A07 = context;
    }

    public CodeInputField(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A07 = context;
    }

    public CodeInputField(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A07 = context;
    }

    public CodeInputField(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    public void A05() {
        ValueAnimator valueAnimator = this.A03;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        } else {
            float x = getX();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(x, ((float) getResources().getDimensionPixelSize(R.dimen.error_wiggle_animation_offset)) + x);
            ofFloat.setInterpolator(new LinearInterpolator());
            ofFloat.setRepeatCount(3);
            ofFloat.setRepeatMode(2);
            ofFloat.setDuration(50L);
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eK
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    CodeInputField.this.setX(C12960it.A00(valueAnimator2));
                }
            });
            ofFloat.addListener(new C72733f5(this, x));
            this.A03 = ofFloat;
        }
        this.A03.start();
        C51282Tp.A01(((WaEditText) this).A02);
    }

    public void A06(AbstractC116435Vk r9, int i) {
        A08(r9, new AnonymousClass5TB() { // from class: X.3U2
            @Override // X.AnonymousClass5TB
            public final SpannableStringBuilder AGq(String str) {
                CodeInputField codeInputField = CodeInputField.this;
                SpannableStringBuilder A0J = C12990iw.A0J(str);
                for (int i2 = 0; i2 < A0J.length(); i2++) {
                    if (A0J.charAt(i2) == codeInputField.A01) {
                        A0J.setSpan(C12980iv.A0M(codeInputField.getContext(), R.color.code_input_hint_color), i2, i2 + 1, 33);
                    }
                }
                return A0J;
            }
        }, null, null, 8211, 8226, i);
    }

    public void A07(AbstractC116435Vk r11, int i, int i2) {
        AnonymousClass3U5 r4 = new AnonymousClass5TB(i2) { // from class: X.3U5
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // X.AnonymousClass5TB
            public final SpannableStringBuilder AGq(String str) {
                int A00;
                int A002;
                CodeInputField codeInputField = CodeInputField.this;
                int i3 = this.A00;
                SpannableStringBuilder A0J = C12990iw.A0J(str);
                for (int i4 = 0; i4 < A0J.length(); i4++) {
                    if (A0J.charAt(i4) == ')') {
                        int i5 = i4 + 1;
                        A0J.setSpan(new RelativeSizeSpan(0.9f), i4, i5, 33);
                        if (i3 != 0) {
                            A002 = i3;
                        } else {
                            A002 = AnonymousClass00T.A00(codeInputField.getContext(), R.color.accent_dark);
                        }
                        A0J.setSpan(new C52272aX(codeInputField.A07, A002), i4, i5, 33);
                    } else if (A0J.charAt(i4) != 160) {
                        if (i3 != 0) {
                            A00 = i3;
                        } else {
                            A00 = AnonymousClass00T.A00(codeInputField.getContext(), R.color.accent_dark);
                        }
                        A0J.setSpan(new C52272aX(codeInputField.A07, A00), i4, i4 + 1, 33);
                    }
                }
                return A0J;
            }
        };
        setPasswordTransformationEnabled(true);
        setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 0));
        setCursorVisible(false);
        A08(r11, r4, "pin_font", null, ')', '(', i);
    }

    public void A08(AbstractC116435Vk r3, AnonymousClass5TB r4, String str, String str2, char c, char c2, int i) {
        Typeface typeface;
        this.A02 = i;
        this.A01 = c;
        this.A00 = c2;
        AnonymousClass3MH r0 = new AnonymousClass3MH(r3, r4, this);
        this.A04 = r0;
        addTextChangedListener(r0);
        setCode("");
        if (TextUtils.equals(str, "pin_font")) {
            typeface = A08;
            if (typeface == null) {
                typeface = AnonymousClass00X.A02(getContext());
                A08 = typeface;
            }
        } else {
            typeface = A09;
            if (typeface == null) {
                typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/RobotoMono-Regular.ttf");
                A09 = typeface;
            }
        }
        setTypeface(typeface);
        C42941w9.A03(this);
        if (str2 != null) {
            AnonymousClass028.A0g(this, new C53522ea(this, str2));
        }
    }

    public String getCode() {
        return getText().toString().replaceAll("[^0-9]", "");
    }

    public boolean getErrorState() {
        return this.A05;
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        removeTextChangedListener(this.A04);
        super.onDetachedFromWindow();
    }

    @Override // android.widget.TextView
    public void onSelectionChanged(int i, int i2) {
        int indexOf;
        if (i == i2 && (indexOf = getText().toString().indexOf(this.A01)) > -1 && i > indexOf) {
            setSelection(indexOf);
        }
        super.onSelectionChanged(i, i2);
    }

    public void setCode(String str) {
        StringBuilder sb = new StringBuilder(str);
        int length = str.length();
        while (true) {
            int i = this.A02;
            if (length < i + 1) {
                sb.append(this.A01);
                length++;
            } else {
                sb.insert(i >> 1, (char) 160);
                this.A04.A01 = true;
                setText(sb);
                setSelection(length + 1);
                this.A04.A01 = false;
                return;
            }
        }
    }

    public void setErrorState(boolean z) {
        if (this.A05 != z) {
            this.A05 = z;
            setCode("");
        }
    }

    public void setPasswordTransformationEnabled(boolean z) {
        setTransformationMethod(z ? new C14280l9(this) : null);
    }

    public void setRegistrationVoiceCodeLength(int i) {
        this.A02 = i;
    }
}
