package com.whatsapp.businessupsell;

import X.AbstractC28491Nn;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.ActivityC60092vu;
import X.AnonymousClass01J;
import X.AnonymousClass18U;
import X.AnonymousClass2FL;
import X.AnonymousClass2I7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C252018m;
import X.C58272oQ;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes2.dex */
public class BusinessProfileEducation extends ActivityC60092vu {
    public AnonymousClass18U A00;
    public C252018m A01;
    public AnonymousClass2I7 A02;
    public boolean A03;

    public BusinessProfileEducation() {
        this(0);
    }

    public BusinessProfileEducation(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 29);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((ActivityC60092vu) this).A00 = C12970iu.A0b(A1M);
            this.A00 = C12990iw.A0T(A1M);
            this.A01 = C12980iv.A0g(A1M);
            this.A02 = A1L.A0L();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        Object[] objArr;
        super.onCreate(bundle);
        setContentView(R.layout.business_profile_education);
        C12960it.A0z(findViewById(R.id.close), this, 39);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.business_account_info_description);
        AbstractC28491Nn.A03(textEmojiLabel);
        String stringExtra = getIntent().getStringExtra("key_extra_business_name");
        if (!C12960it.A1V(getIntent().getIntExtra("key_extra_verified_level", -1), 3) || stringExtra == null) {
            i = R.string.biz_profile_education_business_account;
            objArr = new Object[]{C252018m.A00(this.A01, "26000089")};
        } else {
            i = R.string.biz_profile_education_official_business_account;
            objArr = C12980iv.A1a();
            objArr[0] = Html.escapeHtml(stringExtra);
            objArr[1] = C252018m.A00(this.A01, "26000089");
        }
        SpannableStringBuilder A0J = C12990iw.A0J(Html.fromHtml(getString(i, objArr)));
        URLSpan[] uRLSpanArr = (URLSpan[]) A0J.getSpans(0, A0J.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                A0J.setSpan(new C58272oQ(this, this.A00, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A08, uRLSpan.getURL()), A0J.getSpanStart(uRLSpan), A0J.getSpanEnd(uRLSpan), A0J.getSpanFlags(uRLSpan));
            }
        }
        AbstractC28491Nn.A04(textEmojiLabel, ((ActivityC13810kN) this).A08);
        textEmojiLabel.setText(A0J, TextView.BufferType.SPANNABLE);
        C12960it.A0z(findViewById(R.id.upsell_button), this, 40);
        A2e(1, 11, true);
    }
}
