package com.whatsapp.businessupsell;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.ActivityC60092vu;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2I7;
import X.C12960it;
import X.C12970iu;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class BusinessAppEducation extends ActivityC60092vu {
    public AnonymousClass2I7 A00;
    public boolean A01;

    public BusinessAppEducation() {
        this(0);
    }

    public BusinessAppEducation(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 28);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((ActivityC60092vu) this).A00 = C12970iu.A0b(A1M);
            this.A00 = A1L.A0L();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.business_app_education);
        C12960it.A0z(findViewById(R.id.close), this, 37);
        C12960it.A0z(findViewById(R.id.install_smb_google_play), this, 38);
        A2e(1, 12, false);
    }
}
