package com.whatsapp.mediaview;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass109;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Z;
import X.AnonymousClass11P;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass13H;
import X.AnonymousClass17S;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass18U;
import X.AnonymousClass199;
import X.AnonymousClass19O;
import X.AnonymousClass1CH;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15670ni;
import X.C15680nj;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16590pI;
import X.C16630pM;
import X.C17040qA;
import X.C17050qB;
import X.C17070qD;
import X.C17170qN;
import X.C17220qS;
import X.C18640sm;
import X.C18720su;
import X.C18850tA;
import X.C20710wC;
import X.C20830wO;
import X.C22050yP;
import X.C22100yW;
import X.C22180yf;
import X.C22190yg;
import X.C22260yn;
import X.C22370yy;
import X.C22410z2;
import X.C22610zM;
import X.C22700zV;
import X.C22710zW;
import X.C239613r;
import X.C242114q;
import X.C244415n;
import X.C250918b;
import X.C251118d;
import X.C252018m;
import X.C253218y;
import X.C253318z;
import X.C255419u;
import X.C26311Cv;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_MediaViewFragment extends MediaViewBaseFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1P();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A1P()
            r3.A1O()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediaview.Hilt_MediaViewFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1P();
        A1O();
    }

    public void A1O() {
        if (!this.A02) {
            this.A02 = true;
            MediaViewFragment mediaViewFragment = (MediaViewFragment) this;
            AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            ((WaFragment) mediaViewFragment).A00 = (AnonymousClass182) r2.A94.get();
            ((WaFragment) mediaViewFragment).A01 = (AnonymousClass180) r2.ALt.get();
            mediaViewFragment.A0e = (C14830m7) r2.ALb.get();
            mediaViewFragment.A0P = (C18720su) r2.A2c.get();
            mediaViewFragment.A0w = (C14850m9) r2.A04.get();
            mediaViewFragment.A0H = (C14900mE) r2.A8X.get();
            mediaViewFragment.A1A = (AnonymousClass13H) r2.ABY.get();
            mediaViewFragment.A0F = (AbstractC15710nm) r2.A4o.get();
            mediaViewFragment.A0J = (C15570nT) r2.AAr.get();
            mediaViewFragment.A0K = (C239613r) r2.AI9.get();
            mediaViewFragment.A0f = (C16590pI) r2.AMg.get();
            mediaViewFragment.A0D = (AnonymousClass17S) r2.A0F.get();
            mediaViewFragment.A0I = (AnonymousClass18U) r2.AAU.get();
            mediaViewFragment.A0k = (C14950mJ) r2.AKf.get();
            mediaViewFragment.A1B = (C17220qS) r2.ABt.get();
            mediaViewFragment.A0G = (C22260yn) r2.A5I.get();
            mediaViewFragment.A1E = (C17070qD) r2.AFC.get();
            mediaViewFragment.A0X = (C253318z) r2.A4B.get();
            mediaViewFragment.A0z = (C20710wC) r2.A8m.get();
            mediaViewFragment.A0U = (C22700zV) r2.AMN.get();
            mediaViewFragment.A0n = (C15680nj) r2.A4e.get();
            mediaViewFragment.A1D = (C22710zW) r2.AF7.get();
            mediaViewFragment.A1C = (C22410z2) r2.ANH.get();
            mediaViewFragment.A0O = (C251118d) r2.A2D.get();
            mediaViewFragment.A0T = (C26311Cv) r2.AAQ.get();
            mediaViewFragment.A0l = (C22610zM) r2.A6b.get();
            mediaViewFragment.A0g = (C17170qN) r2.AMt.get();
            mediaViewFragment.A0N = (C250918b) r2.A2B.get();
            mediaViewFragment.A0V = (C15610nY) r2.AMe.get();
            mediaViewFragment.A0b = (C17050qB) r2.ABL.get();
            mediaViewFragment.A0m = (C20830wO) r2.A4W.get();
            mediaViewFragment.A1T = (AbstractC14440lR) r2.ANe.get();
            mediaViewFragment.A1Q = (AnonymousClass199) r2.A0h.get();
            mediaViewFragment.A0y = (C16120oU) r2.ANE.get();
            mediaViewFragment.A0L = (C15450nH) r2.AII.get();
            mediaViewFragment.A0Q = (C18850tA) r2.AKx.get();
            mediaViewFragment.A0M = (C16170oZ) r2.AM4.get();
            mediaViewFragment.A0E = (AnonymousClass12P) r2.A0H.get();
            mediaViewFragment.A12 = (C244415n) r2.AAg.get();
            mediaViewFragment.A0R = (C15550nR) r2.A45.get();
            mediaViewFragment.A0v = (C22180yf) r2.A5U.get();
            mediaViewFragment.A1N = (C252018m) r2.A7g.get();
            mediaViewFragment.A1R = (C22190yg) r2.AB6.get();
            mediaViewFragment.A0c = (AnonymousClass01d) r2.ALI.get();
            mediaViewFragment.A0j = (AnonymousClass018) r2.ANb.get();
            mediaViewFragment.A0p = (C253218y) r2.AAF.get();
            mediaViewFragment.A0S = (AnonymousClass10S) r2.A46.get();
            mediaViewFragment.A0o = (C15650ng) r2.A4m.get();
            mediaViewFragment.A0r = (AnonymousClass12H) r2.AC5.get();
            mediaViewFragment.A1M = (AbstractC15850o0) r2.ANA.get();
            mediaViewFragment.A0x = (C22050yP) r2.A7v.get();
            mediaViewFragment.A0q = (C15660nh) r2.ABE.get();
            mediaViewFragment.A1S = (AnonymousClass19O) r2.ACO.get();
            mediaViewFragment.A0d = (C15410nB) r2.ALJ.get();
            mediaViewFragment.A0W = (AnonymousClass10T) r2.A47.get();
            mediaViewFragment.A14 = (C17040qA) r2.AAx.get();
            mediaViewFragment.A1G = (AnonymousClass10Z) r2.AGM.get();
            mediaViewFragment.A0u = (C15670ni) r2.AIb.get();
            mediaViewFragment.A0s = (C242114q) r2.AJq.get();
            mediaViewFragment.A0h = (C15890o4) r2.AN1.get();
            mediaViewFragment.A0i = (C14820m6) r2.AN3.get();
            mediaViewFragment.A16 = (C22370yy) r2.AB0.get();
            mediaViewFragment.A0t = (C22100yW) r2.A3g.get();
            mediaViewFragment.A0Z = (C255419u) r2.AJp.get();
            mediaViewFragment.A13 = (AnonymousClass109) r2.AI8.get();
            mediaViewFragment.A1F = (C16630pM) r2.AIc.get();
            mediaViewFragment.A0Y = (AnonymousClass11P) r2.ABp.get();
            mediaViewFragment.A15 = (AnonymousClass1CH) r2.AAy.get();
            mediaViewFragment.A0a = (C18640sm) r2.A3u.get();
        }
    }

    public final void A1P() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
