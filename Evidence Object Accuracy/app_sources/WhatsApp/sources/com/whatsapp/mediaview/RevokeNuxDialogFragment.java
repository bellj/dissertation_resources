package com.whatsapp.mediaview;

import X.ActivityC000900k;
import X.AnonymousClass12P;
import X.AnonymousClass3AJ;
import X.AnonymousClass3UM;
import X.AnonymousClass3UN;
import X.C14820m6;
import X.C14850m9;
import X.C252018m;
import android.app.Dialog;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class RevokeNuxDialogFragment extends Hilt_RevokeNuxDialogFragment {
    public AnonymousClass12P A00;
    public C14820m6 A01;
    public C14850m9 A02;
    public C252018m A03;
    public final int A04;

    public RevokeNuxDialogFragment(int i) {
        this.A04 = i;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        boolean z;
        boolean z2;
        int i = this.A04;
        ActivityC000900k A0B = A0B();
        C14850m9 r1 = this.A02;
        AnonymousClass12P r5 = this.A00;
        C252018m r4 = this.A03;
        C14820m6 r3 = this.A01;
        switch (i) {
            case 23:
                z = true;
                return AnonymousClass3AJ.A00(A0B, r5, new AnonymousClass3UN(A0B, r3, i, r1.A07(1333)), r4, z);
            case 24:
                z = false;
                return AnonymousClass3AJ.A00(A0B, r5, new AnonymousClass3UN(A0B, r3, i, r1.A07(1333)), r4, z);
            case 25:
                z2 = true;
                return AnonymousClass3AJ.A00(A0B, r5, new AnonymousClass3UM(A0B, r3, i, r1.A07(1333)), r4, z2);
            default:
                z2 = false;
                return AnonymousClass3AJ.A00(A0B, r5, new AnonymousClass3UM(A0B, r3, i, r1.A07(1333)), r4, z2);
        }
    }
}
