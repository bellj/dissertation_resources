package com.whatsapp.mediaview;

import X.AbstractActivityC13840kQ;
import X.AbstractC116105Ud;
import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AbstractC14640lm;
import X.AbstractC14720lw;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC16130oV;
import X.AbstractC18860tB;
import X.AbstractC28651Ol;
import X.AbstractC35161hL;
import X.AbstractC35501i8;
import X.AbstractC35511i9;
import X.AbstractC35521iA;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01F;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02N;
import X.AnonymousClass04S;
import X.AnonymousClass109;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Z;
import X.AnonymousClass11P;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass13H;
import X.AnonymousClass17S;
import X.AnonymousClass18U;
import X.AnonymousClass199;
import X.AnonymousClass19O;
import X.AnonymousClass1CH;
import X.AnonymousClass1IS;
import X.AnonymousClass1Q5;
import X.AnonymousClass1X7;
import X.AnonymousClass1XH;
import X.AnonymousClass21S;
import X.AnonymousClass21T;
import X.AnonymousClass23N;
import X.AnonymousClass2AC;
import X.AnonymousClass2B2;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2JY;
import X.AnonymousClass2VE;
import X.AnonymousClass2YJ;
import X.AnonymousClass315;
import X.AnonymousClass39C;
import X.AnonymousClass39D;
import X.AnonymousClass3OR;
import X.AnonymousClass3YO;
import X.AnonymousClass43Q;
import X.AnonymousClass5U5;
import X.C004802e;
import X.C015007d;
import X.C015607k;
import X.C1110457w;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15670ni;
import X.C15680nj;
import X.C15890o4;
import X.C16120oU;
import X.C16150oX;
import X.C16170oZ;
import X.C16590pI;
import X.C16630pM;
import X.C17040qA;
import X.C17050qB;
import X.C17070qD;
import X.C17170qN;
import X.C17220qS;
import X.C18640sm;
import X.C18720su;
import X.C18850tA;
import X.C20710wC;
import X.C20830wO;
import X.C20870wS;
import X.C22050yP;
import X.C22100yW;
import X.C22180yf;
import X.C22190yg;
import X.C22200yh;
import X.C22260yn;
import X.C22370yy;
import X.C22410z2;
import X.C22610zM;
import X.C22700zV;
import X.C22710zW;
import X.C239613r;
import X.C242114q;
import X.C244415n;
import X.C250918b;
import X.C251118d;
import X.C252018m;
import X.C253218y;
import X.C253318z;
import X.C255419u;
import X.C26311Cv;
import X.C27131Gd;
import X.C27621Ig;
import X.C30041Vv;
import X.C32731ce;
import X.C35291ha;
import X.C36051jF;
import X.C37111lV;
import X.C38131nZ;
import X.C38211ni;
import X.C38241nl;
import X.C38971p2;
import X.C42641vY;
import X.C51122Sx;
import X.C58362oh;
import X.C623837a;
import X.C625237o;
import X.C70523bQ;
import X.C70613bZ;
import X.C70623ba;
import X.C89564Kl;
import X.DialogInterface$OnClickListenerC65743Kv;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0400000_I1;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape1S0500000_I1;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.R;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.ephemeral.ViewOnceNUXDialog;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.storage.StorageUsageDeleteMessagesDialogFragment;
import com.whatsapp.support.ReportSpamDialogFragment;
import com.whatsapp.ui.media.MediaCaptionTextView;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import com.whatsapp.voicerecorder.VoiceNoteSeekBar;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class MediaViewFragment extends Hilt_MediaViewFragment implements AbstractC35511i9 {
    public static final boolean A1p = (!C38241nl.A02());
    public static final boolean A1q;
    public int A00;
    public int A01 = 0;
    public int A02;
    public int A03 = 0;
    public int A04;
    public long A05;
    public Handler A06;
    public View A07;
    public ImageButton A08;
    public TextView A09;
    public DialogFragment A0A;
    public DialogFragment A0B;
    public RunnableBRunnable0Shape0S0310000_I0 A0C;
    public AnonymousClass17S A0D;
    public AnonymousClass12P A0E;
    public AbstractC15710nm A0F;
    public C22260yn A0G;
    public C14900mE A0H;
    public AnonymousClass18U A0I;
    public C15570nT A0J;
    public C239613r A0K;
    public C15450nH A0L;
    public C16170oZ A0M;
    public C250918b A0N;
    public C251118d A0O;
    public C18720su A0P;
    public C18850tA A0Q;
    public C15550nR A0R;
    public AnonymousClass10S A0S;
    public C26311Cv A0T;
    public C22700zV A0U;
    public C15610nY A0V;
    public AnonymousClass10T A0W;
    public C253318z A0X;
    public AnonymousClass11P A0Y;
    public C255419u A0Z;
    public C18640sm A0a;
    public C17050qB A0b;
    public AnonymousClass01d A0c;
    public C15410nB A0d;
    public C14830m7 A0e;
    public C16590pI A0f;
    public C17170qN A0g;
    public C15890o4 A0h;
    public C14820m6 A0i;
    public AnonymousClass018 A0j;
    public C14950mJ A0k;
    public C22610zM A0l;
    public C20830wO A0m;
    public C15680nj A0n;
    public C15650ng A0o;
    public C253218y A0p;
    public C15660nh A0q;
    public AnonymousClass12H A0r;
    public C242114q A0s;
    public C22100yW A0t;
    public C15670ni A0u;
    public C22180yf A0v;
    public C14850m9 A0w;
    public C22050yP A0x;
    public C16120oU A0y;
    public C20710wC A0z;
    public AbstractC14640lm A10;
    public GroupJid A11;
    public C244415n A12;
    public AnonymousClass109 A13;
    public C17040qA A14;
    public AnonymousClass1CH A15;
    public C22370yy A16;
    public AnonymousClass2VE A17;
    public AbstractC35521iA A18;
    public AbstractC35161hL A19;
    public AnonymousClass13H A1A;
    public C17220qS A1B;
    public C22410z2 A1C;
    public C22710zW A1D;
    public C17070qD A1E;
    public C16630pM A1F;
    public AnonymousClass10Z A1G;
    public AnonymousClass1IS A1H;
    public AbstractC16130oV A1I;
    public AbstractC16130oV A1J;
    public AbstractC16130oV A1K;
    public AnonymousClass2JY A1L;
    public AbstractC15850o0 A1M;
    public C252018m A1N;
    public MediaCaptionTextView A1O;
    public AbstractC28651Ol A1P;
    public AnonymousClass199 A1Q;
    public C22190yg A1R;
    public AnonymousClass19O A1S;
    public AbstractC14440lR A1T;
    public AnonymousClass21S A1U = null;
    public VoiceNoteSeekBar A1V;
    public boolean A1W;
    public boolean A1X;
    public boolean A1Y = false;
    public boolean A1Z = false;
    public boolean A1a;
    public boolean A1b = true;
    public boolean A1c = true;
    public boolean A1d;
    public boolean A1e;
    public boolean A1f;
    public boolean A1g;
    public boolean A1h;
    public final C27131Gd A1i = new C37111lV(this);
    public final AbstractC18860tB A1j = new C35291ha(this);
    public final AbstractC116105Ud A1k = new AbstractC116105Ud() { // from class: X.592
        @Override // X.AbstractC116105Ud
        public final void AVK(String str, int i) {
            AnonymousClass2JY r2;
            MediaViewFragment mediaViewFragment = MediaViewFragment.this;
            if (i == 2 && (r2 = mediaViewFragment.A1L) != null) {
                AnonymousClass009.A05(str);
                r2.A02(str, 3, false, false);
            }
        }
    };
    public final Runnable A1l = new RunnableBRunnable0Shape8S0100000_I0_8(this, 14);
    public final HashMap A1m = new HashMap();
    public final Map A1n = new HashMap();
    public final Map A1o = new HashMap();

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT > 23) {
            z = true;
        }
        A1q = z;
    }

    public static int A00(int i) {
        if (i == 1) {
            return R.string.gallery_image_notready_warning;
        }
        if (i == 2) {
            return R.string.gallery_audio_notready_warning;
        }
        if (i != 3) {
            if (i == 9) {
                return R.string.gallery_document_notready_warning;
            }
            if (i == 13) {
                return R.string.gallery_gif_notready_warning;
            }
            if (i == 42) {
                return R.string.gallery_image_notready_warning;
            }
            if (i != 43) {
                return R.string.gallery_notready_warning;
            }
        }
        return R.string.gallery_video_notready_warning;
    }

    public static MediaViewFragment A01(Bundle bundle, AbstractC14640lm r5, AnonymousClass1IS r6, int i, int i2, int i3, long j, boolean z, boolean z2, boolean z3) {
        MediaViewFragment mediaViewFragment = new MediaViewFragment();
        Bundle bundle2 = new Bundle();
        C38211ni.A08(bundle2, r6, "");
        if (r5 != null) {
            bundle2.putString("jid", r5.getRawString());
        }
        bundle2.putBoolean("gallery", z);
        bundle2.putBoolean("nogallery", z2);
        bundle2.putInt("video_play_origin", i);
        bundle2.putLong("start_t", j);
        bundle2.putBundle("animation_bundle", bundle);
        bundle2.putInt("navigator_type", i3);
        bundle2.putInt("menu_style", i2);
        bundle2.putBoolean("menu_set_wallpaper", z3);
        mediaViewFragment.A0U(bundle2);
        return mediaViewFragment;
    }

    public static final void A02(Menu menu, int i, boolean z) {
        MenuItem findItem = menu.findItem(i);
        if (findItem != null) {
            findItem.setVisible(z);
        }
    }

    public static /* synthetic */ void A03(InteractiveAnnotation interactiveAnnotation, MediaViewFragment mediaViewFragment, PhotoView photoView) {
        AnonymousClass2VE r1 = new AnonymousClass2VE(photoView.getContext(), (ViewGroup) photoView.getRootView(), mediaViewFragment.A12);
        mediaViewFragment.A17 = r1;
        r1.A00(null, interactiveAnnotation, photoView);
    }

    public static /* synthetic */ void A04(MediaViewFragment mediaViewFragment) {
        if (mediaViewFragment.A1g && mediaViewFragment.A1X) {
            if (mediaViewFragment.A1J != null && mediaViewFragment.A0h.A07()) {
                AbstractC16130oV r2 = mediaViewFragment.A1J;
                mediaViewFragment.A1J = null;
                C1110457w r1 = new AbstractC35501i8(r2) { // from class: X.57w
                    public final /* synthetic */ AbstractC16130oV A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC35501i8
                    public final void AXp(boolean z) {
                        MediaViewFragment mediaViewFragment2 = MediaViewFragment.this;
                        AbstractC16130oV r3 = this.A01;
                        C14900mE r22 = mediaViewFragment2.A0H;
                        r22.A02.post(new RunnableBRunnable0Shape0S0210000_I0(mediaViewFragment2, r3, 16, z));
                    }
                };
                if (!((MediaViewBaseFragment) mediaViewFragment).A0E) {
                    r1.AXp(true);
                } else {
                    ((MediaViewBaseFragment) mediaViewFragment).A0A = r1;
                }
            }
            if (mediaViewFragment.A1N()) {
                mediaViewFragment.A0C().A0d();
            } else {
                mediaViewFragment.A0N();
            }
        }
    }

    public static /* synthetic */ void A05(MediaViewFragment mediaViewFragment, ExoPlaybackControlView exoPlaybackControlView, AnonymousClass21S r6, int i) {
        ExoPlaybackControlView exoPlaybackControlView2;
        ExoPlaybackControlView exoPlaybackControlView3;
        if (!r6.A0G) {
            return;
        }
        if ((i & 4) == 0) {
            mediaViewFragment.A1M(true, false);
            for (AnonymousClass21S r0 : mediaViewFragment.A1o.values()) {
                if (!(r0 == r6 || (exoPlaybackControlView3 = r0.A0C) == null)) {
                    exoPlaybackControlView3.A0F.setVisibility(0);
                    if (exoPlaybackControlView3.A09) {
                        exoPlaybackControlView3.A0E.setVisibility(0);
                    }
                    exoPlaybackControlView3.A04();
                    exoPlaybackControlView3.A03();
                    exoPlaybackControlView3.A05();
                }
            }
            if (Build.VERSION.SDK_INT < 19 && exoPlaybackControlView.getVisibility() != 0) {
                exoPlaybackControlView.A01();
                exoPlaybackControlView.A06(3000);
                return;
            }
            return;
        }
        mediaViewFragment.A1M(false, false);
        for (AnonymousClass21S r02 : mediaViewFragment.A1o.values()) {
            if (!(r02 == r6 || (exoPlaybackControlView2 = r02.A0C) == null)) {
                exoPlaybackControlView2.A02();
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        AbstractC35521iA r0;
        super.A0r();
        if (!A1q && this.A1U != null) {
            this.A0H.A0G(this.A1l);
            this.A1U.A08();
            AnonymousClass2B2 r02 = this.A1U.A0D;
            if (r02 != null) {
                r02.A01();
            }
        }
        if (A0C().isFinishing() && (r0 = this.A18) != null) {
            r0.AeQ();
        }
        AnonymousClass2VE r03 = this.A17;
        if (r03 != null) {
            r03.A02.dismiss();
        }
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        AnonymousClass21S r0;
        C36051jF r3 = ((AbstractActivityC13840kQ) ((AbstractC14720lw) A0C())).A00;
        r3.A01.A08("on_fragment_start");
        super.A0s();
        if (A1q && (r0 = this.A1U) != null) {
            r0.A0F();
            AnonymousClass2B2 r1 = this.A1U.A0D;
            if (r1 != null && !(r1 instanceof AnonymousClass39C)) {
                AnonymousClass39D r12 = (AnonymousClass39D) r1;
                if (!r12.A06) {
                    r12.A0B.A02();
                }
            }
        }
        ((MediaViewBaseFragment) this).A06.setAlpha(1.0f);
        r3.A01.A07("on_fragment_start");
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        List list;
        C14900mE r1;
        int i3;
        ProgressDialogFragment progressDialogFragment;
        AnonymousClass01F r12;
        String str;
        C32731ce r8 = null;
        AbstractC14640lm r82 = null;
        r8 = null;
        switch (i) {
            case 0:
                if (i2 == -1 && intent != null && intent.getData() != null) {
                    Uri data = intent.getData();
                    AbstractC16130oV A1Q = A1Q(this.A03);
                    if (A1Q != null) {
                        r82 = A1Q.A0z.A00;
                    }
                    Context A01 = A01();
                    this.A1M.A05(A01, data, r82, true);
                    C22200yh.A0P(A01, data);
                    return;
                }
                return;
            case 1:
                if (i2 == -1) {
                    AnonymousClass10Z r13 = this.A1G;
                    C15570nT r0 = this.A0J;
                    r0.A08();
                    C27621Ig r02 = r0.A01;
                    AnonymousClass009.A05(r02);
                    if (r13.A0A(r02)) {
                        progressDialogFragment = ProgressDialogFragment.A00(0, R.string.updating_profile_photo_dialog_title);
                        this.A0B = progressDialogFragment;
                        r12 = A0E();
                        str = "photo_progress_fragment";
                        progressDialogFragment.A1F(r12, str);
                        return;
                    }
                    return;
                }
                if (i2 == 0 && intent != null) {
                    this.A1G.A03(intent, (ActivityC13810kN) A0C());
                    return;
                }
                return;
            case 2:
                if (i2 == -1 && intent != null) {
                    this.A11 = GroupJid.getNullable(intent.getStringExtra("contact"));
                    AbstractC16130oV r03 = (AbstractC16130oV) this.A0o.A0K.A00(intent.getLongExtra("message_row_id", -1));
                    Intent intent2 = new Intent();
                    if (r03 != null) {
                        C16150oX r04 = r03.A02;
                        AnonymousClass009.A05(r04);
                        intent2.setData(Uri.fromFile(r04.A0F));
                        AnonymousClass10Z r4 = this.A1G;
                        if (A0p() != null) {
                            r4.A05(intent2, (ActivityC13810kN) A0C(), this, null, 3);
                            return;
                        }
                        return;
                    }
                    Log.e("mediaview/no-message-for-group-icon");
                    r1 = this.A0H;
                    i3 = R.string.failed_update_photo;
                    r1.A07(i3, 0);
                    return;
                }
                return;
            case 3:
                if (i2 == -1) {
                    GroupJid groupJid = this.A11;
                    if (groupJid != null && this.A1G.A0A(this.A0R.A0B(groupJid))) {
                        progressDialogFragment = ProgressDialogFragment.A00(0, R.string.updating_group_icon_dialog_title);
                        this.A0A = progressDialogFragment;
                        r12 = A0E();
                        str = "group_progress_fragment";
                        progressDialogFragment.A1F(r12, str);
                        return;
                    }
                    return;
                }
                if (i2 == 0) {
                    return;
                }
                return;
            case 4:
                if (i2 == -1 && intent != null) {
                    AnonymousClass1IS A02 = C38211ni.A02(intent);
                    AbstractC15340mz A1Q2 = A1Q(this.A03);
                    if ((A1Q2 == null || A1Q2.A0z != A02) && (A1Q2 = this.A0o.A0K.A03(A02)) == null) {
                        Log.w("mediaview/forward/failed");
                        r1 = this.A0H;
                        i3 = R.string.message_forward_failed;
                        r1.A07(i3, 0);
                        return;
                    }
                    list = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                    if (this.A1d && C15380n4.A0P(list)) {
                        r8 = (C32731ce) intent.getParcelableExtra("status_distribution");
                    }
                    this.A0M.A06(this.A0K, r8, A1Q2, list);
                    if (list.size() == 1 || C15380n4.A0N((Jid) list.get(0))) {
                        ((ActivityC13790kL) A0C()).A2a(list);
                        return;
                    } else {
                        C51122Sx.A00(new C14960mK().A0g(A01(), this.A0R.A0B((AbstractC14640lm) list.get(0))), this);
                        return;
                    }
                } else {
                    return;
                }
            case 5:
                if (i2 == -1 && intent != null) {
                    list = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                    if (list.size() == 1) {
                        break;
                    }
                    ((ActivityC13790kL) A0C()).A2a(list);
                    return;
                }
                return;
            case 6:
                if (i2 == -1 && intent != null && intent.getData() != null) {
                    Uri data2 = intent.getData();
                    AbstractC14640lm A012 = AbstractC14640lm.A01(intent.getStringExtra("chat_jid"));
                    Context A013 = A01();
                    this.A1M.A05(A013, data2, A012, true);
                    C22200yh.A0P(A013, data2);
                    A1E();
                    return;
                }
                return;
            default:
                super.A0t(i, i2, intent);
                return;
        }
    }

    @Override // X.AnonymousClass01E
    public void A0v(Intent intent) {
        AbstractC35521iA r0 = this.A18;
        if (r0 != null) {
            r0.ASu();
        }
        super.A0v(intent);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        GroupJid groupJid = this.A11;
        if (groupJid != null) {
            bundle.putString("gid", groupJid.getRawString());
        }
        bundle.putBoolean("is_different_video", this.A1b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        if (X.C30041Vv.A0m(r8) != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007d, code lost:
        if (r1 != false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007f, code lost:
        r13 = r8.A0C();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0083, code lost:
        if (r13 != null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0085, code lost:
        r13 = r19.A10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0087, code lost:
        if (r13 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0089, code lost:
        r10 = A0J(com.whatsapp.R.string.report_spam_contact, r19.A0V.A0A(r19.A0R.A0B(r13), -1));
        r0 = r20.findItem(15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a9, code lost:
        if (r0 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ab, code lost:
        r0.setTitle(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c1, code lost:
        if (r0 != false) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d7, code lost:
        if (r17 == false) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x013c, code lost:
        if (X.C30041Vv.A0W(r19.A0w, r8) == false) goto L_0x013e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x011a  */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0x(android.view.Menu r20) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediaview.MediaViewFragment.A0x(android.view.Menu):void");
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        MenuItem add;
        int i;
        menu.clear();
        if (this.A02 == 3) {
            menu.add(0, 14, 1, R.string.view_once_info).setIcon(AnonymousClass2GE.A01(A01(), R.drawable.ic_viewonce, R.color.white)).setShowAsAction(2);
        } else {
            menu.add(0, 10, 0, R.string.add_star).setIcon(R.drawable.ic_media_unstarred).setShowAsAction(2);
            menu.add(0, 11, 0, R.string.remove_star).setIcon(R.drawable.ic_media_starred).setShowAsAction(2);
            if (this.A02 == 2) {
                add = menu.add(0, 6, 0, R.string.delete);
                i = R.drawable.ic_action_delete;
            } else {
                add = menu.add(0, 9, 0, R.string.conversation_menu_forward);
                i = R.drawable.ic_media_forward;
            }
            add.setIcon(i).setShowAsAction(2);
            C15370n3 A0B = this.A0R.A0B(this.A10);
            if (!A0B.A0K() || !this.A0z.A0Y(A0B)) {
                Drawable A03 = C015607k.A03(C015007d.A01(A01(), R.drawable.ic_text_status_compose));
                C015607k.A0A(A03, -1);
                menu.add(0, 13, 0, R.string.edit).setIcon(A03).setShowAsAction(1);
            }
            menu.add(0, 7, 0, R.string.all_media).setIcon(R.drawable.ic_action_all_media).setShowAsAction(0);
            menu.add(0, 12, 0, R.string.view_in_chat).setShowAsAction(0);
            menu.add(0, 8, 0, R.string.share).setIcon(R.drawable.ic_action_share);
            if (this.A0w.A07(942)) {
                this.A1Z = this.A0w.A07(1050);
                menu.add(0, 16, 0, R.string.save_to_storage);
            }
            SubMenu addSubMenu = menu.addSubMenu(1, 0, 0, R.string.set_as);
            addSubMenu.clearHeader();
            addSubMenu.add(1, 4, 0, R.string.set_as_profile_photo_wa_gallery);
            addSubMenu.add(1, 5, 0, R.string.set_as_group_icon_wa_gallery);
            if (this.A1e) {
                addSubMenu.add(1, 1, 0, R.string.use_as_wallpaper);
            }
            menu.add(1, 2, 0, R.string.view_in_gallery);
            menu.add(1, 3, 0, R.string.rotate);
            if (this.A02 == 2) {
                return;
            }
        }
        menu.add(0, 6, 0, R.string.delete);
        menu.add(0, 15, 0, R.string.report_spam);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        String str;
        long j;
        AbstractC14440lR r2;
        int i;
        File file;
        AbstractC16130oV A1Q = A1Q(this.A03);
        boolean z = false;
        switch (menuItem.getItemId()) {
            case 1:
                if (A1Q != null) {
                    C16150oX r0 = A1Q.A02;
                    AnonymousClass009.A05(r0);
                    Uri fromFile = Uri.fromFile(r0.A0F);
                    AbstractC14640lm r4 = A1Q.A0z.A00;
                    ActivityC000900k A0C = A0C();
                    Uri A04 = this.A1M.A04();
                    Intent className = new Intent().setClassName(A0C.getPackageName(), "com.whatsapp.settings.chat.wallpaper.GalleryWallpaperPreview");
                    className.setData(fromFile);
                    className.putExtra("output", A04);
                    className.putExtra("chat_jid", C15380n4.A03(r4));
                    className.putExtra("is_using_global_wallpaper", true);
                    startActivityForResult(className, 6);
                    return true;
                }
                str = "mediaview/no-message-to-set-as-wallpaper";
                Log.e(str);
                return true;
            case 2:
                if (A1Q != null) {
                    this.A0H.A06(0, R.string.loading_spinner);
                    C14900mE r5 = this.A0H;
                    AbstractC15710nm r42 = this.A0F;
                    AbstractC14440lR r3 = this.A1T;
                    C15670ni r02 = this.A0u;
                    C70623ba r22 = new AbstractC14590lg(A1Q) { // from class: X.3ba
                        public final /* synthetic */ AbstractC16130oV A01;

                        {
                            this.A01 = r2;
                        }

                        /* JADX WARNING: Removed duplicated region for block: B:15:0x0033  */
                        @Override // X.AbstractC14590lg
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public final void accept(java.lang.Object r6) {
                            /*
                                r5 = this;
                                com.whatsapp.mediaview.MediaViewFragment r4 = com.whatsapp.mediaview.MediaViewFragment.this
                                X.0oV r1 = r5.A01
                                android.net.Uri r6 = (android.net.Uri) r6
                                X.0mE r0 = r4.A0H
                                r0.A03()
                                java.lang.String r0 = "android.intent.action.VIEW"
                                android.content.Intent r3 = X.C12990iw.A0E(r0)
                                byte r2 = r1.A0y
                                r1 = 1
                                if (r2 == r1) goto L_0x0040
                                r0 = 3
                                if (r2 == r0) goto L_0x003c
                                r0 = 13
                                if (r2 == r0) goto L_0x003c
                                r0 = 42
                                if (r2 == r0) goto L_0x003b
                                r0 = 43
                                if (r2 == r0) goto L_0x003b
                                r3.setData(r6)
                            L_0x0028:
                                r3.setFlags(r1)
                                android.content.Context r1 = r4.A01()
                                X.1iA r0 = r4.A18
                                if (r0 == 0) goto L_0x0036
                                r0.ASu()
                            L_0x0036:
                                X.12P r0 = r4.A0E
                                r0.A06(r1, r3)
                            L_0x003b:
                                return
                            L_0x003c:
                                java.lang.String r0 = "video/*"
                                goto L_0x0042
                            L_0x0040:
                                java.lang.String r0 = "image/*"
                            L_0x0042:
                                r3.setDataAndType(r6, r0)
                                goto L_0x0028
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.C70623ba.accept(java.lang.Object):void");
                        }
                    };
                    C38971p2 r1 = new C38971p2(r42, r02, A1Q);
                    r1.A01(r22, r5.A06);
                    r3.Ab2(r1);
                    return true;
                }
                str = "mediaview/no-message-to-view-in-gallery";
                Log.e(str);
                return true;
            case 3:
                if (A1Q(this.A03) instanceof AnonymousClass1X7) {
                    AbstractC16130oV A1Q2 = A1Q(this.A03);
                    AnonymousClass009.A05(A1Q2);
                    this.A1T.Aaz(new C625237o(this.A0q, this, (AnonymousClass1X7) A1Q2, this.A1R, this.A1S), new Void[0]);
                    return true;
                }
                return true;
            case 4:
                if (A1Q != null) {
                    Intent intent = new Intent();
                    C16150oX r03 = A1Q.A02;
                    AnonymousClass009.A05(r03);
                    intent.setData(Uri.fromFile(r03.A0F));
                    AnonymousClass10Z r52 = this.A1G;
                    if (A0p() != null) {
                        r52.A05(intent, (ActivityC13810kN) A0C(), this, null, 1);
                        return true;
                    }
                    return true;
                }
                str = "mediaview/no-message-to-set-as-profile-photo";
                Log.e(str);
                return true;
            case 5:
                C42641vY r23 = new C42641vY(A01());
                r23.A0E = true;
                r23.A0K = Long.valueOf(A1Q(this.A03).A11);
                startActivityForResult(r23.A00(), 2);
                return true;
            case 6:
                AbstractC16130oV A1Q3 = A1Q(this.A03);
                if (A1Q3 != null) {
                    List singletonList = Collections.singletonList(A1Q3);
                    if (this.A02 == 2) {
                        this.A1T.Aaz(new C623837a(new AnonymousClass02N(), new AnonymousClass5U5(singletonList) { // from class: X.568
                            public final /* synthetic */ List A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // X.AnonymousClass5U5
                            public final void APY(Collection collection) {
                                MediaViewFragment mediaViewFragment = MediaViewFragment.this;
                                List list = this.A01;
                                StorageUsageDeleteMessagesDialogFragment storageUsageDeleteMessagesDialogFragment = new StorageUsageDeleteMessagesDialogFragment();
                                storageUsageDeleteMessagesDialogFragment.A05 = list;
                                storageUsageDeleteMessagesDialogFragment.A04 = collection;
                                storageUsageDeleteMessagesDialogFragment.A02 = null;
                                storageUsageDeleteMessagesDialogFragment.A1F(mediaViewFragment.A0E(), null);
                            }
                        }, this.A0q, singletonList), new Void[0]);
                        return true;
                    }
                    DeleteMessagesDialogFragment.A00(this.A10, singletonList).A1F(A0E(), null);
                    return true;
                }
                return true;
            case 7:
                A1H();
                return true;
            case 8:
                this.A0M.A02(A0C(), this.A0E, A1Q(this.A03));
                return true;
            case 9:
                if (A1Q != null) {
                    C42641vY r43 = new C42641vY(A01());
                    r43.A06 = true;
                    r43.A02 = this.A10;
                    byte b = A1Q.A0y;
                    r43.A0R = new ArrayList(Collections.singleton(Integer.valueOf(Byte.valueOf(b).intValue())));
                    if (b == 3) {
                        j = ((long) A1Q.A00) * 1000;
                    } else {
                        j = 0;
                    }
                    r43.A0L = Long.valueOf(j);
                    Intent A00 = r43.A00();
                    C38211ni.A00(A00, A1Q.A0z);
                    startActivityForResult(A00, 4);
                    return true;
                }
                str = "mediaview/no-message-for-forward";
                Log.e(str);
                return true;
            case 10:
                if (A1Q != null) {
                    AnonymousClass43Q r12 = new AnonymousClass43Q();
                    r12.A00 = Integer.valueOf(C20870wS.A01(this.A0v, A1Q));
                    r12.A01 = 1;
                    this.A0y.A07(r12);
                    r2 = this.A1T;
                    i = 4;
                    r2.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, i, A1Q));
                    return true;
                }
                str = "mediaview/no-message-for-star";
                Log.e(str);
                return true;
            case 11:
                if (A1Q != null) {
                    r2 = this.A1T;
                    i = 5;
                    r2.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, i, A1Q));
                    return true;
                }
                str = "mediaview/no-message-for-unstar";
                Log.e(str);
                return true;
            case 12:
                if (A1Q != null) {
                    long A01 = C30041Vv.A01(A1Q);
                    long A02 = C30041Vv.A02(A1Q);
                    C14960mK r7 = new C14960mK();
                    Context A012 = A01();
                    AnonymousClass1IS r6 = A1Q.A0z;
                    Intent A0i = r7.A0i(A012, r6.A00);
                    A0i.putExtra("row_id", A01);
                    A0i.putExtra("sort_id", A02);
                    C38211ni.A00(A0i, r6);
                    C51122Sx.A00(A0i, this);
                    return true;
                }
                str = "mediaview/no-message-to-view-in-chat";
                Log.e(str);
                return true;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                if (A1Q != null) {
                    ((MediaViewBaseFragment) this).A06.animate().alpha(0.0f).setDuration(100).setListener(new AnonymousClass2YJ(this, A1Q));
                    return true;
                }
                str = "mediaview/no-message-for-edit";
                Log.e(str);
                return true;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                ViewOnceNUXDialog.A00(A0F(), (AbstractC15340mz) ((AnonymousClass1XH) A1Q), true);
                return true;
            case 15:
                if (!(A1Q == null || this.A10 == null)) {
                    this.A0t.A05().A00(new AbstractC14590lg(A1Q) { // from class: X.3bb
                        public final /* synthetic */ AbstractC16130oV A01;

                        {
                            this.A01 = r2;
                        }

                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            MediaViewFragment mediaViewFragment = MediaViewFragment.this;
                            AbstractC16130oV r24 = this.A01;
                            Boolean bool = (Boolean) obj;
                            ActivityC000900k A0B = mediaViewFragment.A0B();
                            if (A0B != null && !C36021jC.A03(A0B)) {
                                ReportSpamDialogFragment A002 = ReportSpamDialogFragment.A00(mediaViewFragment.A10, r24.A0C(), 
                                /*  JADX ERROR: Method code generation error
                                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0027: INVOKE  (r1v0 'A002' com.whatsapp.support.ReportSpamDialogFragment) = 
                                      (wrap: X.0lm : 0x0012: IGET  (r4v0 X.0lm A[REMOVE]) = (r3v0 'mediaViewFragment' com.whatsapp.mediaview.MediaViewFragment) com.whatsapp.mediaview.MediaViewFragment.A10 X.0lm)
                                      (wrap: com.whatsapp.jid.UserJid : 0x001a: INVOKE  (r5v0 com.whatsapp.jid.UserJid A[REMOVE]) = (r2v0 'r24' X.0oV) type: VIRTUAL call: X.0mz.A0C():com.whatsapp.jid.UserJid)
                                      (wrap: X.59d : 0x0020: CONSTRUCTOR  (r6v0 X.59d A[REMOVE]) = (r3v0 'mediaViewFragment' com.whatsapp.mediaview.MediaViewFragment) call: X.59d.<init>(com.whatsapp.mediaview.MediaViewFragment):void type: CONSTRUCTOR)
                                      ("media_viewer")
                                      (0 int)
                                      (wrap: boolean : 0x0014: INVOKE  (r9v0 boolean A[REMOVE]) = (r14v1 'bool' java.lang.Boolean) type: VIRTUAL call: java.lang.Boolean.booleanValue():boolean)
                                      false
                                      false
                                      false
                                     type: STATIC call: com.whatsapp.support.ReportSpamDialogFragment.A00(X.0lm, com.whatsapp.jid.UserJid, X.1eV, java.lang.String, int, boolean, boolean, boolean, boolean):com.whatsapp.support.ReportSpamDialogFragment in method: X.3bb.accept(java.lang.Object):void, file: classes2.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0020: CONSTRUCTOR  (r6v0 X.59d A[REMOVE]) = (r3v0 'mediaViewFragment' com.whatsapp.mediaview.MediaViewFragment) call: X.59d.<init>(com.whatsapp.mediaview.MediaViewFragment):void type: CONSTRUCTOR in method: X.3bb.accept(java.lang.Object):void, file: classes2.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                    	... 23 more
                                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.59d, state: NOT_LOADED
                                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                    	... 29 more
                                    */
                                /*
                                    this = this;
                                    com.whatsapp.mediaview.MediaViewFragment r3 = com.whatsapp.mediaview.MediaViewFragment.this
                                    X.0oV r2 = r13.A01
                                    java.lang.Boolean r14 = (java.lang.Boolean) r14
                                    X.00k r0 = r3.A0B()
                                    if (r0 == 0) goto L_0x0034
                                    boolean r0 = X.C36021jC.A03(r0)
                                    if (r0 != 0) goto L_0x0034
                                    X.0lm r4 = r3.A10
                                    boolean r9 = r14.booleanValue()
                                    java.lang.String r7 = "media_viewer"
                                    com.whatsapp.jid.UserJid r5 = r2.A0C()
                                    X.59d r6 = new X.59d
                                    r6.<init>(r3)
                                    r8 = 0
                                    r10 = 0
                                    r11 = 0
                                    r12 = 0
                                    com.whatsapp.support.ReportSpamDialogFragment r1 = com.whatsapp.support.ReportSpamDialogFragment.A00(r4, r5, r6, r7, r8, r9, r10, r11, r12)
                                    r1.A07 = r2
                                    X.01F r0 = r3.A0E()
                                    X.C42791vs.A00(r1, r0)
                                L_0x0034:
                                    return
                                */
                                throw new UnsupportedOperationException("Method not decompiled: X.C70633bb.accept(java.lang.Object):void");
                            }
                        });
                        return true;
                    }
                    return true;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    if (A1Q != null) {
                        C16150oX r04 = A1Q.A02;
                        if (r04 == null || (file = r04.A0F) == null) {
                            if (this.A1Z) {
                                str = "mediasave/no-message-media-to-save-to-storage";
                                Log.e(str);
                                return true;
                            }
                            return true;
                        }
                        this.A1T.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(this, A1Q, file, 11));
                        return true;
                    }
                    if (this.A1Z) {
                        str = "mediasave/no-message-to-save-to-storage";
                        Log.e(str);
                        return true;
                    }
                    return true;
                case 17:
                case 18:
                case 19:
                    if (!(A1Q == null || A0B() == null)) {
                        C253218y r62 = this.A0p;
                        if (menuItem.getItemId() == 17) {
                            z = true;
                        }
                        ActivityC000900k A0B = A0B();
                        C16170oZ r122 = this.A0M;
                        C89564Kl r32 = new C89564Kl(this);
                        AnonymousClass009.A0F(false);
                        Long l = A1Q.A0Y;
                        if (!r62.A03.A0B()) {
                            r62.A00.A07(R.string.keep_in_chat_offline_error, 1);
                            return true;
                        }
                        if (l != null) {
                            C14850m9 r13 = r62.A06;
                            Long l2 = A1Q.A0Y;
                            if (l2 != null) {
                                if (System.currentTimeMillis() > l2.longValue() + ((long) (r13.A02(1698) * 1000)) && !z) {
                                    DialogInterface$OnClickListenerC65743Kv r53 = new DialogInterface.OnClickListener(A0B, r62.A00, r122, A1Q, r62.A08) { // from class: X.3Kv
                                        public final /* synthetic */ Activity A00;
                                        public final /* synthetic */ C14900mE A01;
                                        public final /* synthetic */ C16170oZ A02;
                                        public final /* synthetic */ AbstractC15340mz A03;
                                        public final /* synthetic */ AbstractC14440lR A04;

                                        {
                                            this.A03 = r4;
                                            this.A04 = r5;
                                            this.A02 = r3;
                                            this.A01 = r2;
                                            this.A00 = r1;
                                        }

                                        @Override // android.content.DialogInterface.OnClickListener
                                        public final void onClick(DialogInterface dialogInterface, int i2) {
                                            AbstractC15340mz r14 = this.A03;
                                            AbstractC14440lR r05 = this.A04;
                                            C16170oZ r24 = this.A02;
                                            C14900mE r44 = this.A01;
                                            Activity activity = this.A00;
                                            HashSet A12 = C12970iu.A12();
                                            A12.add(r14);
                                            r05.Ab2(new RunnableBRunnable0Shape1S0400000_I1(r24, A12, r44, activity, 1));
                                            dialogInterface.dismiss();
                                        }
                                    };
                                    C004802e r24 = new C004802e(A0B);
                                    r24.A06(R.string.disabled_undo_keep_in_chat);
                                    r24.setPositiveButton(R.string.revoke_delete_for_me, r53);
                                    r24.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fk
                                        @Override // android.content.DialogInterface.OnClickListener
                                        public final void onClick(DialogInterface dialogInterface, int i2) {
                                            C89564Kl.this.A00.A0C().invalidateOptionsMenu();
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    r24.create().show();
                                    return true;
                                }
                            }
                            if (l.longValue() <= System.currentTimeMillis()) {
                                if (z) {
                                    r62.A00.A07(R.string.keep_in_chat_expired_warning, 0);
                                    r32.A00.A0C().invalidateOptionsMenu();
                                    return true;
                                }
                                C14900mE r10 = r62.A00;
                                AbstractC14440lR r11 = r62.A08;
                                View inflate = LayoutInflater.from(A0B).inflate(R.layout.mtrl_alert_dialog_actions_vertical, (ViewGroup) null);
                                C004802e r14 = new C004802e(A0B);
                                r14.A07(R.string.warn_undo_keep_in_chat_expired_title);
                                r14.A06(R.string.warn_undo_keep_in_chat_expired);
                                r14.setView(inflate);
                                AnonymousClass04S create = r14.create();
                                TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.button1);
                                TextView textView2 = (TextView) AnonymousClass028.A0D(inflate, R.id.button2);
                                textView.setText(R.string.warn_undo_keep_in_chat_expired_positive_button);
                                textView.setTextColor(AnonymousClass00T.A00(A0B, R.color.status_error));
                                textView.setOnClickListener(new ViewOnClickCListenerShape1S0500000_I1(r10, r11, r122, A1Q, create, 1));
                                textView2.setText(R.string.cancel);
                                textView2.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(r32, 6, create));
                                create.show();
                                return true;
                            }
                        }
                        if (!A1Q.A0z.A02 || z) {
                            r62.A08.Ab2(new RunnableBRunnable0Shape0S0210000_I0(r122, A1Q, 12, z));
                            r32.A00.A0C().invalidateOptionsMenu();
                            return true;
                        }
                        C14900mE r54 = r62.A00;
                        AbstractC14440lR r15 = r62.A08;
                        C004802e r25 = new C004802e(A0B);
                        r25.A06(R.string.kic_unkeep_your_message_description);
                        r25.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(r122, r32, A1Q, r15) { // from class: X.3Ku
                            public final /* synthetic */ C16170oZ A01;
                            public final /* synthetic */ C89564Kl A02;
                            public final /* synthetic */ AbstractC15340mz A03;
                            public final /* synthetic */ AbstractC14440lR A04;

                            {
                                this.A04 = r5;
                                this.A01 = r2;
                                this.A03 = r4;
                                this.A02 = r3;
                            }

                            @Override // android.content.DialogInterface.OnClickListener
                            public final void onClick(DialogInterface dialogInterface, int i2) {
                                AbstractC14440lR r55 = this.A04;
                                C16170oZ r44 = this.A01;
                                AbstractC15340mz r33 = this.A03;
                                C89564Kl r26 = this.A02;
                                r55.Ab2(new RunnableBRunnable0Shape11S0200000_I1_1(r44, 27, r33));
                                dialogInterface.dismiss();
                                r26.A00.A0C().invalidateOptionsMenu();
                            }
                        });
                        r25.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fl
                            @Override // android.content.DialogInterface.OnClickListener
                            public final void onClick(DialogInterface dialogInterface, int i2) {
                                C89564Kl r05 = C89564Kl.this;
                                dialogInterface.dismiss();
                                r05.A00.A0C().invalidateOptionsMenu();
                                dialogInterface.dismiss();
                            }
                        });
                        r25.create().show();
                        return true;
                    }
                    return true;
                default:
                    return true;
            }
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
        public void A11() {
            A1U();
            RunnableBRunnable0Shape0S0310000_I0 runnableBRunnable0Shape0S0310000_I0 = this.A0C;
            if (runnableBRunnable0Shape0S0310000_I0 != null) {
                runnableBRunnable0Shape0S0310000_I0.A03 = true;
                ((Thread) runnableBRunnable0Shape0S0310000_I0.A02).interrupt();
                this.A0C = null;
            }
            AnonymousClass21S r0 = this.A1U;
            if (r0 != null) {
                r0.A06();
                this.A1U = null;
                AbstractC16130oV r2 = this.A1K;
                if (r2 != null) {
                    this.A16.A0C(r2, false, false);
                }
            }
            this.A1K = null;
            this.A0S.A04(this.A1i);
            this.A0r.A04(this.A1j);
            AbstractC35521iA r02 = this.A18;
            if (r02 != null) {
                r02.close();
            }
            this.A1T.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0d, 37));
            AnonymousClass2VE r03 = this.A17;
            if (r03 != null) {
                r03.A02.dismiss();
            }
            if (this.A1O != null) {
                this.A1O = null;
            }
            super.A11();
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
        public void A13() {
            C36051jF r3 = ((AbstractActivityC13840kQ) ((AbstractC14720lw) A0C())).A00;
            r3.A01.A08("on_fragment_resume");
            super.A13();
            AnonymousClass21S r0 = this.A1U;
            if (r0 != null) {
                r0.A0F();
                AnonymousClass2B2 r1 = this.A1U.A0D;
                if (r1 != null && !(r1 instanceof AnonymousClass39C)) {
                    AnonymousClass39D r12 = (AnonymousClass39D) r1;
                    if (!r12.A06) {
                        r12.A0B.A02();
                    }
                }
            }
            r3.A01.A07("on_fragment_resume");
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
        public void A14() {
            super.A14();
            if (A1q && this.A1U != null) {
                this.A0H.A0G(this.A1l);
                this.A1U.A08();
                AnonymousClass2B2 r0 = this.A1U.A0D;
                if (r0 != null) {
                    r0.A01();
                }
            }
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
        public void A16(Bundle bundle) {
            C36051jF r5 = ((AbstractActivityC13840kQ) ((AbstractC14720lw) A0C())).A00;
            r5.A01.A08("on_fragment_create");
            this.A1T.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0d, 37));
            super.A16(bundle);
            RunnableBRunnable0Shape0S0310000_I0 runnableBRunnable0Shape0S0310000_I0 = new RunnableBRunnable0Shape0S0310000_I0(this);
            this.A0C = runnableBRunnable0Shape0S0310000_I0;
            ((Thread) runnableBRunnable0Shape0S0310000_I0.A02).start();
            boolean z = false;
            boolean z2 = false;
            if (bundle != null) {
                z2 = true;
            }
            this.A1W = z2;
            A0M();
            r5.A01.A07("on_fragment_create");
            if (this.A0w.A07(815) && this.A0w.A07(1267)) {
                z = true;
            }
            this.A1d = z;
            C14830m7 r0 = this.A0e;
            C14850m9 r02 = this.A0w;
            C14900mE r03 = this.A0H;
            C15570nT r04 = this.A0J;
            AbstractC14440lR r05 = this.A1T;
            C16120oU r06 = this.A0y;
            AnonymousClass17S r07 = this.A0D;
            C15450nH r08 = this.A0L;
            AnonymousClass18U r09 = this.A0I;
            C17220qS r010 = this.A1B;
            C15550nR r011 = this.A0R;
            AnonymousClass01d r012 = this.A0c;
            C15610nY r013 = this.A0V;
            C22260yn r014 = this.A0G;
            C17070qD r14 = this.A1E;
            C253318z r13 = this.A0X;
            C22700zV r11 = this.A0U;
            C15680nj r10 = this.A0n;
            C22710zW r9 = this.A1D;
            C22410z2 r8 = this.A1C;
            C251118d r7 = this.A0O;
            C18640sm r6 = this.A0a;
            C26311Cv r52 = this.A0T;
            C22610zM r4 = this.A0l;
            C17170qN r3 = this.A0g;
            this.A1L = new AnonymousClass2JY(r07, r014, (ActivityC13810kN) A0C(), r03, r09, r04, r08, this.A0N, r7, r011, r52, r11, r013, r13, r6, r012, r0, r3, r4, r10, r02, r06, r010, r8, r9, r14, r05, null, this.A0w.A07(611), false);
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E
        public void A17(Bundle bundle, View view) {
            String str;
            String obj;
            C36051jF r7 = ((AbstractActivityC13840kQ) ((AbstractC14720lw) A0C())).A00;
            r7.A01.A08("on_fragment_view_created");
            super.A17(bundle, view);
            if (((MediaViewBaseFragment) this).A0C != null) {
                StringBuilder sb = new StringBuilder("mediaview/oncreate/oom/heap size:");
                sb.append(Debug.getNativeHeapAllocatedSize() / 1024);
                sb.append(" kB");
                Log.e(sb.toString());
                AnonymousClass10T r3 = this.A0W;
                StringBuilder sb2 = new StringBuilder("native heap size:");
                sb2.append(Debug.getNativeHeapAllocatedSize() / 1024);
                sb2.append(" kB");
                Log.e(sb2.toString());
                r3.A02.A01().A00.A06(0);
                AnonymousClass2AC r1 = new AnonymousClass2AC(new Object[0], R.string.error_low_on_memory);
                r1.A00 = 101;
                r1.A01().A1F(A0E(), "oom_fragment");
                return;
            }
            Bundle A03 = A03();
            if (this.A0h.A07()) {
                AnonymousClass1IS A032 = C38211ni.A03(A03, "");
                AnonymousClass009.A05(A032);
                this.A1H = A032;
                this.A10 = AbstractC14640lm.A01(A03.getString("jid"));
                this.A1f = A03.getBoolean("nogallery", false);
                this.A05 = A03.getLong("start_t", 0);
                this.A1a = A03.getBoolean("gallery", false);
                int i = A03.getInt("video_play_origin");
                int i2 = 1;
                if (i != 1) {
                    i2 = 2;
                    if (i != 2) {
                        i2 = 3;
                        if (i != 3) {
                            i2 = 4;
                            if (i != 4) {
                                i2 = 5;
                            }
                        }
                    }
                }
                this.A04 = i2;
                int i3 = A03.getInt("navigator_type");
                this.A02 = A03.getInt("menu_style");
                this.A1e = A03.getBoolean("menu_set_wallpaper", false);
                if (i3 == 1) {
                    if (this.A10 == null) {
                        obj = "mediaview/oncreate/jid navigatorfactory with null jid";
                        Log.e(obj);
                    } else {
                        this.A19 = new AbstractC35161hL() { // from class: X.57v
                            @Override // X.AbstractC35161hL
                            public final AbstractC35521iA A8R(MediaViewFragment mediaViewFragment, AbstractC16130oV r10) {
                                MediaViewFragment mediaViewFragment2 = MediaViewFragment.this;
                                AbstractC14440lR r72 = mediaViewFragment2.A1T;
                                return new AnonymousClass2Am(mediaViewFragment2.A0o, mediaViewFragment2.A0q, mediaViewFragment2.A10, mediaViewFragment, r10, mediaViewFragment2.A1L, r72);
                            }
                        };
                    }
                } else if (i3 == 2) {
                    SearchViewModel searchViewModel = (SearchViewModel) new AnonymousClass02A(A0C()).A00(SearchViewModel.class);
                    this.A19 = searchViewModel;
                    super.A0K.A00(searchViewModel);
                }
                if (this.A19 == null) {
                    obj = "mediaview/oncreate/null navigatorfactory";
                } else {
                    if (this.A1f) {
                        ((MediaViewBaseFragment) this).A02.setVisibility(8);
                    }
                    if (bundle != null) {
                        this.A1b = bundle.getBoolean("is_different_video", true);
                    }
                    StringBuilder sb3 = new StringBuilder("mediaview/found-key ");
                    sb3.append(this.A1H.A00);
                    sb3.append(" me:");
                    AnonymousClass1IS r12 = this.A1H;
                    sb3.append(r12.A02);
                    sb3.append(" id:");
                    sb3.append(r12.A01);
                    Log.i(sb3.toString());
                    C15650ng r0 = this.A0o;
                    AbstractC16130oV r9 = (AbstractC16130oV) r0.A0K.A03(this.A1H);
                    if (r9 == null) {
                        StringBuilder sb4 = new StringBuilder("mediaview/cannot find message for ");
                        sb4.append(this.A1H);
                        obj = sb4.toString();
                    } else {
                        byte b = r9.A0y;
                        if (b == 2 || C30041Vv.A0H(b) || b == 9 || C30041Vv.A0F(b)) {
                            this.A1J = r9;
                        }
                        StringBuilder sb5 = new StringBuilder("mediaview/view message:");
                        sb5.append(this.A1H);
                        Log.i(sb5.toString());
                        AbstractC35521iA A8R = this.A19.A8R(this, r9);
                        this.A18 = A8R;
                        A8R.Ac4(new RunnableBRunnable0Shape8S0100000_I0_8(this, 13));
                        C58362oh r13 = new C58362oh(new AnonymousClass3YO(this), this);
                        ((MediaViewBaseFragment) this).A08 = r13;
                        ((MediaViewBaseFragment) this).A09.setAdapter(r13);
                        ((MediaViewBaseFragment) this).A09.A0F(0, false);
                        int AFq = this.A18.AFq(this.A1H);
                        this.A18.getCount();
                        this.A03 = AFq;
                        A1F();
                        ((MediaViewBaseFragment) this).A09.A0F(AFq, false);
                        A0C().invalidateOptionsMenu();
                        ((MediaViewBaseFragment) this).A02.setVisibility(8);
                        if (!this.A1f) {
                            this.A18.AeB();
                        }
                        this.A1K = r9;
                        if (b == 3 || b == 1) {
                            this.A1T.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 6, r9));
                        } else if (C30041Vv.A0I(b)) {
                            C14820m6 r14 = this.A0i;
                            AnonymousClass01F A0F = A0F();
                            AbstractC15340mz r2 = (AbstractC15340mz) ((AnonymousClass1XH) r9);
                            if (!A0F.A0m()) {
                                SharedPreferences sharedPreferences = r14.A00;
                                if (r2 == null) {
                                    str = "view_once_nux";
                                } else {
                                    str = "view_once_receiver_nux";
                                }
                                if (!sharedPreferences.getBoolean(str, false) && A0F.A0A("view_once_nux") == null) {
                                    ViewOnceNUXDialog.A00(A0F, r2, false);
                                }
                            }
                        }
                        this.A06 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.3Le
                            @Override // android.os.Handler.Callback
                            public final boolean handleMessage(Message message) {
                                MediaViewFragment mediaViewFragment = MediaViewFragment.this;
                                if (mediaViewFragment.A1P != null) {
                                    if (mediaViewFragment.A01 == 1) {
                                        if (mediaViewFragment.A1V.getMax() > 0) {
                                            int min = Math.min(mediaViewFragment.A1P.A02(), mediaViewFragment.A1V.getMax());
                                            int i4 = min / 1000;
                                            if (mediaViewFragment.A00 / 1000 != i4) {
                                                C38131nZ.A0C(mediaViewFragment.A09, mediaViewFragment.A0j, (long) i4);
                                                mediaViewFragment.A00 = min;
                                            }
                                            mediaViewFragment.A1V.setProgress(min);
                                        } else {
                                            ((ActivityC13810kN) mediaViewFragment.A0C()).Ado(R.string.error_zero_audio_length);
                                        }
                                    }
                                    if (!mediaViewFragment.A0C().isFinishing() && mediaViewFragment.A01 == 1 && mediaViewFragment.A1P.A0D()) {
                                        C12990iw.A17(mediaViewFragment.A06);
                                    } else if (mediaViewFragment.A01 != 2) {
                                        Log.i("mediaview/audio/set to stop status");
                                        VoiceNoteSeekBar voiceNoteSeekBar = mediaViewFragment.A1V;
                                        voiceNoteSeekBar.setProgress(voiceNoteSeekBar.getMax());
                                        C38131nZ.A0C(mediaViewFragment.A09, mediaViewFragment.A0j, (long) (mediaViewFragment.A1P.A03() / 1000));
                                        mediaViewFragment.A1R();
                                        return true;
                                    }
                                }
                                return true;
                            }
                        });
                        if (this.A1W) {
                            this.A1J = null;
                        }
                        this.A1K = this.A1J;
                        A1V(this.A03);
                        if (!this.A1W) {
                            this.A1I = r9;
                            Bundle bundle2 = ((MediaViewBaseFragment) this).A01;
                            if (bundle2 != null) {
                                ((MediaViewBaseFragment) this).A0E = true;
                                ((MediaViewBaseFragment) this).A0B.A0C(bundle2, this);
                            }
                        }
                        if (bundle != null) {
                            this.A11 = GroupJid.getNullable(bundle.getString("gid"));
                        }
                        this.A0S.A03(this.A1i);
                        this.A0r.A03(this.A1j);
                        AnonymousClass1Q5 r4 = r7.A01;
                        r4.A07("on_fragment_view_created");
                        r4.A0A("media_type", String.valueOf((int) b), true);
                        int log10 = (int) Math.log10((double) r9.A01);
                        if (log10 <= 17) {
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append((long) Math.pow(10.0d, (double) log10));
                            sb6.append("_");
                            sb6.append((long) Math.pow(10.0d, (double) (log10 + 1)));
                            String obj2 = sb6.toString();
                            if (obj2 != null) {
                                r4.A0A("file_size", obj2, true);
                            }
                        }
                        ((AbstractC14720lw) A0C()).ASg();
                        return;
                    }
                }
                Log.e(obj);
            }
            A1E();
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment
        public void A1D() {
            View findViewWithTag;
            AbstractC16130oV A1Q = A1Q(this.A03);
            if (!(A1Q == null || (findViewWithTag = ((MediaViewBaseFragment) this).A09.findViewWithTag(A1Q.A0z)) == null)) {
                View findViewById = findViewWithTag.findViewById(R.id.thumbnail);
                if (findViewById != null) {
                    findViewById.setVisibility(0);
                    findViewById.setAlpha(1.0f);
                }
                View findViewById2 = findViewWithTag.findViewById(R.id.video_view);
                if (findViewById2 != null) {
                    findViewById2.setVisibility(8);
                }
            }
            super.A1D();
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment
        public void A1E() {
            super.A1E();
            Map map = this.A1n;
            for (AnonymousClass21T r0 : map.values()) {
                r0.A08();
            }
            map.clear();
        }

        public final AbstractC16130oV A1Q(int i) {
            AbstractC35521iA r0 = this.A18;
            if (r0 == null) {
                return null;
            }
            return r0.AEG(i);
        }

        public final void A1R() {
            AbstractC28651Ol r2 = this.A1P;
            if (r2 != null && this.A01 != 2) {
                r2.A04();
                A1T();
                this.A01 = 2;
            }
        }

        public final void A1S() {
            ImageButton imageButton = this.A08;
            if (imageButton != null) {
                imageButton.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(A01(), R.drawable.mviewer_pause), this.A0j));
                this.A08.setContentDescription(A0I(R.string.pause));
            }
            View view = this.A07;
            if (view != null) {
                AnonymousClass23N.A02(view, R.string.pause);
            }
        }

        public final void A1T() {
            ImageButton imageButton = this.A08;
            if (imageButton != null) {
                imageButton.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(A01(), R.drawable.mviewer_play), this.A0j));
                this.A08.setContentDescription(A0I(R.string.play));
            }
            View view = this.A07;
            if (view != null) {
                AnonymousClass23N.A02(view, R.string.play);
            }
        }

        public final void A1U() {
            AbstractC28651Ol r0 = this.A1P;
            if (r0 != null) {
                r0.A06();
                this.A1P = null;
                this.A01 = 0;
            }
            VoiceNoteSeekBar voiceNoteSeekBar = this.A1V;
            if (voiceNoteSeekBar != null) {
                voiceNoteSeekBar.setProgress(0);
            }
            A1T();
            TextView textView = this.A09;
            if (textView != null) {
                textView.setText(C38131nZ.A04(this.A0j, 0));
            }
        }

        public final void A1V(int i) {
            String A04;
            int i2;
            AbstractC16130oV A1Q = A1Q(i);
            if (A1Q != null) {
                AbstractC35521iA r0 = this.A18;
                if (r0 != null) {
                    r0.AfO(i);
                }
                if (A1Q.A0z.A02) {
                    i2 = R.string.you;
                } else {
                    AbstractC14640lm A0C = A1Q.A0C();
                    if (A0C == null && (A0C = this.A10) == null) {
                        Log.e("mediaview/no sender and no jid");
                        this.A0F.AaV("MediaViewFragment/setSenderAndDateTime", "null_jid_no_sender", true);
                        i2 = R.string.unknown;
                    } else {
                        A04 = this.A0V.A04(this.A0R.A0B(A0C));
                        ((MediaViewBaseFragment) this).A07.setText(A04);
                        ((MediaViewBaseFragment) this).A05.setText(C38131nZ.A01(this.A0j, this.A0e.A02(A1Q.A0I)).toString());
                        A0C().invalidateOptionsMenu();
                    }
                }
                A04 = A0I(i2);
                ((MediaViewBaseFragment) this).A07.setText(A04);
                ((MediaViewBaseFragment) this).A05.setText(C38131nZ.A01(this.A0j, this.A0e.A02(A1Q.A0I)).toString());
                A0C().invalidateOptionsMenu();
            }
        }

        public final void A1W(AbstractC16130oV r7) {
            View findViewWithTag;
            StringBuilder sb = new StringBuilder("mediaview/prepareaudioplayback/");
            AnonymousClass1IS r1 = r7.A0z;
            sb.append(r1.A01);
            Log.i(sb.toString());
            if (this.A0h.A07() && (findViewWithTag = ((MediaViewBaseFragment) this).A09.findViewWithTag(r1)) != null) {
                this.A09 = (TextView) findViewWithTag.findViewById(R.id.progress_tv);
                VoiceNoteSeekBar voiceNoteSeekBar = (VoiceNoteSeekBar) findViewWithTag.findViewById(R.id.audio_seekbar);
                this.A1V = voiceNoteSeekBar;
                voiceNoteSeekBar.setOnSeekBarChangeListener(new AnonymousClass3OR(this));
                ImageButton imageButton = (ImageButton) findViewWithTag.findViewById(R.id.audio_control_btn);
                this.A08 = imageButton;
                ViewOnClickCListenerShape0S0200000_I0 viewOnClickCListenerShape0S0200000_I0 = new ViewOnClickCListenerShape0S0200000_I0(this.A1V, 36, this);
                imageButton.setOnClickListener(viewOnClickCListenerShape0S0200000_I0);
                View findViewById = findViewWithTag.findViewById(R.id.audio_icon);
                this.A07 = findViewById;
                if (((MediaViewBaseFragment) this).A0G) {
                    findViewById.setOnClickListener(viewOnClickCListenerShape0S0200000_I0);
                } else {
                    findViewById.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 35, viewOnClickCListenerShape0S0200000_I0));
                }
                AbstractC28651Ol r0 = this.A1P;
                if (r0 != null) {
                    r0.A06();
                    this.A1P = null;
                }
                try {
                    C16150oX r02 = r7.A02;
                    AnonymousClass009.A05(r02);
                    File file = r02.A0F;
                    if (file != null) {
                        AbstractC28651Ol A00 = AbstractC28651Ol.A00(null, null, file, 3);
                        this.A1P = A00;
                        A00.A0B(new MediaPlayer.OnErrorListener() { // from class: X.4i7
                            @Override // android.media.MediaPlayer.OnErrorListener
                            public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                                StringBuilder A0k = C12960it.A0k("mediaview/error: what:");
                                A0k.append(i);
                                Log.e(C12960it.A0e("  extra:", A0k, i2));
                                return false;
                            }
                        });
                        this.A1P.A05();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("mediaview/audio duration:");
                        sb2.append(this.A1P.A03());
                        Log.i(sb2.toString());
                        this.A01 = 2;
                        this.A09.setText(C38131nZ.A04(this.A0j, (long) (this.A1P.A03() / 1000)));
                        this.A1V.setMax(this.A1P.A03());
                    } else {
                        Log.e("mediaview/ audio file is null");
                        ((ActivityC13810kN) A0C()).Ado(R.string.gallery_audio_cannot_load);
                    }
                } catch (IOException e) {
                    Log.e("mediaview/prepare fail", e);
                    AbstractC28651Ol r03 = this.A1P;
                    if (r03 != null) {
                        r03.A06();
                        this.A1P = null;
                    }
                    ((ActivityC13810kN) A0C()).Ado(R.string.gallery_audio_cannot_load);
                }
                this.A1V.setProgress(0);
                A1T();
            }
        }

        public final void A1X(AbstractC16130oV r12, int i, boolean z) {
            int i2;
            AnonymousClass21S r1;
            this.A0Y.A04();
            boolean z2 = A1p;
            byte b = r12.A0y;
            boolean A0H = C30041Vv.A0H(b);
            if (z2) {
                if ((A0H || C30041Vv.A0F(b)) && z && (r1 = this.A1U) != null) {
                    r1.A04 = this.A04;
                    r1.A08();
                    this.A1U.A09(i);
                    this.A1U.A07();
                    A0C().invalidateOptionsMenu();
                    return;
                }
            } else if (A0H) {
                this.A0H.A06(0, R.string.loading_spinner);
                C14900mE r7 = this.A0H;
                AbstractC15710nm r6 = this.A0F;
                AbstractC14440lR r5 = this.A1T;
                C15670ni r0 = this.A0u;
                C70523bQ r4 = new AbstractC14590lg() { // from class: X.3bQ
                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        List<ResolveInfo> queryIntentActivities;
                        MediaViewFragment mediaViewFragment = MediaViewFragment.this;
                        mediaViewFragment.A0H.A03();
                        Intent A0E = C12990iw.A0E("android.intent.action.VIEW");
                        A0E.setDataAndType((Uri) obj, "video/*");
                        A0E.setFlags(1);
                        if (Build.MANUFACTURER.startsWith("Sony") && (queryIntentActivities = mediaViewFragment.A0C().getPackageManager().queryIntentActivities(A0E, 0)) != null) {
                            for (ResolveInfo resolveInfo : queryIntentActivities) {
                                StringBuilder A0k = C12960it.A0k("mediaview/share");
                                ActivityInfo activityInfo = resolveInfo.activityInfo;
                                A0k.append(activityInfo.packageName);
                                A0k.append(" | ");
                                Log.i(C12960it.A0d(activityInfo.name, A0k));
                                if (resolveInfo.activityInfo.name.equals("com.sonyericsson.gallery.MovieView")) {
                                    A0E.setClassName("com.sonyericsson.gallery", "com.sonyericsson.gallery.MovieView");
                                }
                            }
                        }
                        Context A01 = mediaViewFragment.A01();
                        AbstractC35521iA r02 = mediaViewFragment.A18;
                        if (r02 != null) {
                            r02.ASu();
                        }
                        mediaViewFragment.A0E.A06(A01, A0E);
                    }
                };
                C38971p2 r13 = new C38971p2(r6, r0, r12);
                r13.A01(r4, r7.A06);
                r5.Ab2(r13);
                C16150oX r42 = r12.A02;
                AnonymousClass009.A05(r42);
                if (this.A1b) {
                    C22050yP r62 = this.A0x;
                    int i3 = 1;
                    if (r12.A0z.A02) {
                        i3 = 3;
                    }
                    if (z) {
                        i2 = this.A04;
                    } else {
                        i2 = 4;
                    }
                    long j = (long) r12.A00;
                    File file = r42.A0F;
                    if (file != null) {
                        AnonymousClass315 r52 = new AnonymousClass315();
                        r52.A05 = Long.valueOf(j);
                        r52.A04 = Long.valueOf((System.currentTimeMillis() - file.lastModified()) / 1000);
                        r52.A03 = Integer.valueOf(i3);
                        r52.A02 = 1;
                        r52.A01 = Integer.valueOf(i2);
                        r52.A00 = Double.valueOf((double) file.length());
                        r62.A0G.A07(r52);
                    }
                }
                this.A1b = false;
                return;
            } else if (C30041Vv.A0F(b)) {
                AnonymousClass21T r02 = (AnonymousClass21T) this.A1n.get(r12.A0z);
                if (r02 != null) {
                    r02.A07();
                    return;
                }
                return;
            }
            if (b == 2) {
                A1W(r12);
                if (this.A1P != null) {
                    this.A1Q.A02();
                    try {
                        this.A1P.A08();
                        if (i > 0) {
                            this.A1P.A0A(i);
                            this.A1V.setProgress(this.A1P.A02());
                        }
                        this.A01 = 1;
                        this.A06.sendEmptyMessage(0);
                        A1S();
                    } catch (IOException e) {
                        Log.e("mediaview/playMedia failed to start", e);
                        ((ActivityC13810kN) A0C()).Ado(R.string.gallery_audio_cannot_load);
                    }
                }
            } else if (b == 9) {
                Log.i("mediaview/playMedia trying to open document");
                this.A0H.A06(0, R.string.loading_spinner);
                C14900mE r53 = this.A0H;
                AbstractC15710nm r43 = this.A0F;
                AbstractC14440lR r3 = this.A1T;
                C15670ni r03 = this.A0u;
                C70613bZ r2 = new AbstractC14590lg(r12) { // from class: X.3bZ
                    public final /* synthetic */ AbstractC16130oV A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        MediaViewFragment mediaViewFragment = MediaViewFragment.this;
                        AbstractC16130oV r14 = this.A01;
                        mediaViewFragment.A0H.A03();
                        Intent A0E = C12990iw.A0E("android.intent.action.VIEW");
                        A0E.setDataAndType((Uri) obj, r14.A06);
                        A0E.setFlags(1);
                        Context A01 = mediaViewFragment.A01();
                        AbstractC35521iA r04 = mediaViewFragment.A18;
                        if (r04 != null) {
                            r04.ASu();
                        }
                        mediaViewFragment.A0E.A06(A01, A0E);
                    }
                };
                C38971p2 r14 = new C38971p2(r43, r03, r12);
                r14.A01(r2, r53.A06);
                r3.Ab2(r14);
            }
        }

        @Override // X.AbstractC35511i9
        public void AOx() {
            A1U();
            AnonymousClass21S r1 = this.A1U;
            if (!(r1 == null || this.A1K == null)) {
                r1.A08();
                this.A1U.A06();
                this.A1o.remove(this.A1K.A0z);
                this.A1m.remove(this.A1K.A0z);
                this.A1U = null;
            }
            AbstractC35521iA r0 = this.A18;
            if (r0 == null || r0.getCount() == 1) {
                A1E();
            }
        }

        @Override // com.whatsapp.mediaview.MediaViewBaseFragment, X.AnonymousClass01E, android.content.ComponentCallbacks
        public void onConfigurationChanged(Configuration configuration) {
            super.onConfigurationChanged(configuration);
            AnonymousClass2VE r0 = this.A17;
            if (r0 != null) {
                r0.A02.dismiss();
            }
        }
    }
