package com.whatsapp.mediaview;

import X.AbstractC14640lm;
import X.AbstractC14720lw;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00E;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass1IS;
import X.AnonymousClass1Q6;
import X.AnonymousClass2FL;
import X.C004902f;
import X.C38211ni;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class MediaViewActivity extends ActivityC13790kL implements AbstractC14720lw {
    public MediaViewFragment A00;
    public boolean A01;

    @Override // X.AbstractActivityC13840kQ
    public int A1m() {
        return 703923716;
    }

    @Override // X.AbstractC14720lw
    public void APJ() {
    }

    @Override // X.AbstractC14720lw
    public void AXE() {
    }

    @Override // X.AbstractC14720lw
    public boolean Add() {
        return true;
    }

    public MediaViewActivity() {
        this(0);
    }

    public MediaViewActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 86);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.AbstractActivityC13840kQ
    public AnonymousClass1Q6 A1n() {
        AnonymousClass1Q6 A1n = super.A1n();
        A1n.A03 = true;
        return A1n;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A01;
    }

    @Override // X.AbstractC14720lw
    public void ASf() {
        finish();
    }

    @Override // X.AbstractC14720lw
    public void ASg() {
        A1u();
    }

    @Override // android.app.Activity
    public void finish() {
        super.finish();
        MediaViewFragment mediaViewFragment = this.A00;
        if (mediaViewFragment != null) {
            ((MediaViewBaseFragment) mediaViewFragment).A0B.A0A();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        MediaViewFragment mediaViewFragment = this.A00;
        if (mediaViewFragment != null) {
            mediaViewFragment.A1G();
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ActivityC13790kL.A0a(this);
        super.onCreate(bundle);
        A1w("on_activity_create");
        setContentView(R.layout.media_view_activity);
        AnonymousClass01F A0V = A0V();
        MediaViewFragment mediaViewFragment = (MediaViewFragment) A0V.A0A("media_view_fragment");
        this.A00 = mediaViewFragment;
        if (mediaViewFragment == null) {
            Intent intent = getIntent();
            AnonymousClass1IS A02 = C38211ni.A02(intent);
            if (A02 == null) {
                Log.e("mediaview/message key parameter is missing");
                finish();
                return;
            }
            AbstractC14640lm A01 = AbstractC14640lm.A01(intent.getStringExtra("jid"));
            boolean booleanExtra = intent.getBooleanExtra("gallery", false);
            boolean booleanExtra2 = intent.getBooleanExtra("nogallery", false);
            this.A00 = MediaViewFragment.A01(intent.getBundleExtra("animation_bundle"), A01, A02, intent.getIntExtra("video_play_origin", 5), intent.getIntExtra("menu_style", 1), 1, intent.getLongExtra("start_t", 0), booleanExtra, booleanExtra2, intent.getBooleanExtra("menu_set_wallpaper", false));
        }
        C004902f r2 = new C004902f(A0V);
        r2.A0B(this.A00, "media_view_fragment", R.id.media_view_fragment_container);
        r2.A01();
        A1v("on_activity_create");
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        MediaViewBaseFragment.A06((Activity) this, true);
    }
}
