package com.whatsapp.mediaview;

import X.AbstractView$OnClickListenerC76323lS;
import X.AnonymousClass0MU;
import X.AnonymousClass2D4;
import X.AnonymousClass3GA;
import X.AnonymousClass45S;
import X.C52382ai;
import X.C70333b7;
import X.RunnableC55562in;
import X.RunnableC76143l8;
import X.RunnableC76313lR;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape0S0120102_I0;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.InteractiveAnnotation;

/* loaded from: classes2.dex */
public class PhotoView extends AnonymousClass2D4 implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
    public float A00;
    public float A01;
    public float A02 = Float.MAX_VALUE;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08 = 0.8f;
    public int A09 = 0;
    public Matrix A0A;
    public Matrix A0B = new Matrix();
    public Matrix A0C = new Matrix();
    public Paint A0D = new Paint();
    public PointF A0E = new PointF();
    public Rect A0F = new Rect();
    public RectF A0G = new RectF();
    public RectF A0H = new RectF();
    public RectF A0I = new RectF();
    public BitmapDrawable A0J;
    public Drawable A0K;
    public ScaleGestureDetector A0L;
    public View.OnClickListener A0M;
    public AnonymousClass0MU A0N;
    public RunnableBRunnable0Shape0S0120102_I0 A0O;
    public RunnableBRunnable0Shape0S0120102_I0 A0P;
    public RunnableC55562in A0Q;
    public RunnableC76313lR A0R;
    public RunnableC76143l8 A0S;
    public boolean A0T = false;
    public boolean A0U;
    public boolean A0V = true;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public final Runnable A0Z = new RunnableBRunnable0Shape8S0100000_I0_8(this, 19);

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public PhotoView(Context context) {
        super(context);
        A02();
    }

    public PhotoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public PhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }

    public static void A00(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                A00(viewGroup.getChildAt(i));
            }
        } else if (view instanceof PhotoView) {
            ((PhotoView) view).A01();
        }
    }

    public void A01() {
        this.A0N = null;
        this.A0L = null;
        this.A0J = null;
        setImageDrawable(null);
        RunnableC76313lR r1 = this.A0R;
        if (r1 != null) {
            r1.A06 = false;
            r1.A07 = true;
        }
        this.A0R = null;
        RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0 = this.A0P;
        if (runnableBRunnable0Shape0S0120102_I0 != null) {
            runnableBRunnable0Shape0S0120102_I0.A04 = false;
            runnableBRunnable0Shape0S0120102_I0.A05 = true;
        }
        this.A0P = null;
        RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I02 = this.A0O;
        if (runnableBRunnable0Shape0S0120102_I02 != null) {
            runnableBRunnable0Shape0S0120102_I02.A04 = false;
            runnableBRunnable0Shape0S0120102_I02.A05 = true;
        }
        this.A0O = null;
        RunnableC55562in r0 = this.A0Q;
        if (r0 != null) {
            r0.A02 = true;
            PhotoView photoView = r0.A03;
            photoView.A07 = (float) Math.round(photoView.A07);
            photoView.A08(true);
            photoView.requestLayout();
            photoView.invalidate();
        }
        this.A0Q = null;
        RunnableC76143l8 r12 = this.A0S;
        if (r12 != null) {
            r12.A01 = true;
        }
        this.A0S = null;
        this.A0M = null;
        this.A0M = null;
    }

    public final void A02() {
        Context context = getContext();
        this.A0N = new AnonymousClass0MU(context, this);
        C52382ai r2 = new C52382ai(context, this, this, getScaledMinScalingSpan());
        this.A0L = r2;
        if (Build.VERSION.SDK_INT >= 19) {
            r2.setQuickScaleEnabled(false);
        }
        this.A0R = new RunnableC76313lR(this);
        this.A0P = new RunnableBRunnable0Shape0S0120102_I0(this, 1);
        this.A0O = new RunnableBRunnable0Shape0S0120102_I0(this, 0);
        this.A0Q = new RunnableC55562in(this);
        this.A0S = new RunnableC76143l8(this);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public final void A03(float f, float f2) {
        View.OnClickListener onClickListener = this.A0M;
        if (onClickListener == null) {
            return;
        }
        if (onClickListener instanceof AbstractView$OnClickListenerC76323lS) {
            AnonymousClass45S r8 = (AnonymousClass45S) ((AbstractView$OnClickListenerC76323lS) onClickListener);
            C70333b7 r6 = r8.A00;
            PhotoView photoView = r6.A01;
            Bitmap photo = photoView.getPhoto();
            if (photo != null) {
                Matrix matrix = new Matrix();
                photoView.getImageMatrix().invert(matrix);
                float[] fArr = {f, f2};
                float[] fArr2 = {(float) photo.getWidth(), (float) photo.getHeight()};
                matrix.mapPoints(fArr);
                InteractiveAnnotation A01 = AnonymousClass3GA.A01(r8.A01, fArr, fArr2);
                if (A01 != null) {
                    MediaViewFragment.A03(A01, r6.A00, photoView);
                    return;
                }
            }
            MediaViewFragment mediaViewFragment = r6.A00;
            mediaViewFragment.A1M(!((MediaViewBaseFragment) mediaViewFragment).A0G, true);
            return;
        }
        onClickListener.onClick(this);
    }

    public final void A04(float f, float f2, float f3) {
        float min = Math.min(Math.max(f, this.A04 * this.A08), this.A03);
        float f4 = min / this.A00;
        Matrix matrix = this.A0B;
        matrix.postRotate(-this.A07, (float) (getWidth() >> 1), (float) (getHeight() >> 1));
        matrix.postScale(f4, f4, f2, f3);
        this.A00 = min;
        matrix.postRotate(this.A07, (float) (getWidth() >> 1), (float) (getHeight() >> 1));
        A09(true);
        setImageMatrix(matrix);
    }

    public void A05(Bitmap bitmap) {
        A06(bitmap == null ? null : new BitmapDrawable(getResources(), bitmap));
    }

    public void A06(BitmapDrawable bitmapDrawable) {
        BitmapDrawable bitmapDrawable2 = this.A0J;
        if (bitmapDrawable != bitmapDrawable2) {
            boolean z = false;
            if (bitmapDrawable2 != null) {
                if (!(bitmapDrawable != null && bitmapDrawable2.getIntrinsicWidth() == bitmapDrawable.getIntrinsicWidth() && this.A0J.getIntrinsicHeight() == bitmapDrawable.getIntrinsicHeight())) {
                    z = true;
                }
                this.A04 = 0.0f;
            }
            this.A0J = bitmapDrawable;
            setImageDrawable(bitmapDrawable);
            A08(z);
            invalidate();
        }
    }

    public void A07(boolean z) {
        this.A0Y = z;
        if (!z) {
            Matrix matrix = this.A0B;
            matrix.set(this.A0C);
            this.A00 = this.A06;
            setImageMatrix(matrix);
        }
    }

    public final void A08(boolean z) {
        float f;
        float f2;
        float min;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        BitmapDrawable bitmapDrawable = this.A0J;
        if (bitmapDrawable != null && this.A0W) {
            this.A0J.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), this.A0J.getIntrinsicHeight());
            if (z || (this.A04 == 0.0f && this.A0J != null && this.A0W)) {
                float intrinsicWidth = (float) this.A0J.getIntrinsicWidth();
                float intrinsicHeight = (float) this.A0J.getIntrinsicHeight();
                float width = (float) ((getWidth() - getPaddingLeft()) - getPaddingRight());
                float height = (float) ((getHeight() - getPaddingTop()) - getPaddingBottom());
                this.A05 = 0.0f;
                Matrix matrix = this.A0B;
                matrix.reset();
                this.A0H.set(0.0f, 0.0f, intrinsicWidth, intrinsicHeight);
                this.A0G.set(0.0f, 0.0f, width, height);
                float f9 = intrinsicWidth / 2.0f;
                float f10 = intrinsicHeight / 2.0f;
                matrix.setTranslate((width / 2.0f) - f9, (height / 2.0f) - f10);
                boolean z2 = this.A0T;
                float abs = Math.abs(this.A07 % 180.0f);
                int i = (abs > 90.0f ? 1 : (abs == 90.0f ? 0 : -1));
                if (z2) {
                    if (i == 0) {
                        f7 = width / intrinsicHeight;
                        f8 = height / intrinsicWidth;
                    } else {
                        f7 = width / intrinsicWidth;
                        f8 = height / intrinsicHeight;
                    }
                    min = Math.max(f7, f8);
                } else {
                    if (i == 0) {
                        f = width / intrinsicHeight;
                        f2 = height / intrinsicWidth;
                    } else {
                        f = width / intrinsicWidth;
                        f2 = height / intrinsicHeight;
                    }
                    min = Math.min(f, f2);
                }
                this.A04 = min;
                float f11 = this.A02;
                float min2 = Math.min(min, f11);
                this.A04 = min2;
                int i2 = this.A09;
                if (i2 == 3) {
                    if (abs == 90.0f) {
                        f5 = width / intrinsicHeight;
                        f6 = height / intrinsicWidth;
                    } else {
                        f5 = width / intrinsicWidth;
                        f6 = height / intrinsicHeight;
                    }
                    min2 = Math.max(f5, f6);
                } else if (i2 == 1) {
                    min2 = abs == 90.0f ? width / intrinsicHeight : width / intrinsicWidth;
                } else if (i2 == 2) {
                    min2 = abs == 90.0f ? height / intrinsicWidth : height / intrinsicHeight;
                }
                if (abs == 90.0f) {
                    f3 = width / intrinsicHeight;
                    f4 = height / intrinsicWidth;
                } else {
                    f3 = width / intrinsicWidth;
                    f4 = height / intrinsicHeight;
                }
                if (Math.abs((f3 / f4) - 1.0f) < this.A01) {
                    min2 = Math.max(f3, f4);
                    this.A05 = min2;
                }
                float min3 = Math.min(min2, f11);
                this.A00 = min3;
                this.A05 = Math.min(this.A05, f11);
                matrix.preScale(min3, min3, f9, f10);
                this.A03 = Math.max(this.A04 * 8.0f, 8.0f);
                matrix.postRotate(this.A07, (float) (getWidth() / 2), (float) (getHeight() / 2));
                this.A06 = this.A00;
                this.A0C.set(matrix);
            }
            Matrix matrix2 = this.A0B;
            this.A0A = matrix2;
            setImageMatrix(matrix2);
        }
    }

    public final void A09(boolean z) {
        float f;
        RectF rectF = this.A0I;
        rectF.set(this.A0H);
        Matrix matrix = this.A0B;
        matrix.mapRect(rectF);
        float width = (float) getWidth();
        float f2 = rectF.left;
        float f3 = rectF.right;
        float f4 = 0.0f;
        float f5 = width - 0.0f;
        if (f3 - f2 < f5) {
            f = ((f5 - (f3 + f2)) / 2.0f) + 0.0f;
        } else {
            f = f2 > 0.0f ? 0.0f - f2 : f3 < width ? width - f3 : 0.0f;
        }
        float height = (float) getHeight();
        float f6 = rectF.top;
        float f7 = rectF.bottom;
        float f8 = height - 0.0f;
        if (f7 - f6 < f8) {
            f4 = 0.0f + ((f8 - (f7 + f6)) / 2.0f);
        } else if (f6 > 0.0f) {
            f4 = 0.0f - f6;
        } else if (f7 < height) {
            f4 = height - f7;
        }
        if ((Math.abs(f) > 20.0f || Math.abs(f4) > 20.0f) && !z) {
            RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0 = this.A0O;
            if (runnableBRunnable0Shape0S0120102_I0 != null && !runnableBRunnable0Shape0S0120102_I0.A04) {
                runnableBRunnable0Shape0S0120102_I0.A02 = -1;
                runnableBRunnable0Shape0S0120102_I0.A00 = f;
                runnableBRunnable0Shape0S0120102_I0.A01 = f4;
                runnableBRunnable0Shape0S0120102_I0.A05 = false;
                runnableBRunnable0Shape0S0120102_I0.A04 = true;
                ((View) runnableBRunnable0Shape0S0120102_I0.A03).postDelayed(runnableBRunnable0Shape0S0120102_I0, 250);
                return;
            }
            return;
        }
        matrix.postTranslate(f, f4);
        setImageMatrix(matrix);
    }

    public boolean A0A() {
        if (this.A0Y) {
            RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0 = this.A0P;
            if (runnableBRunnable0Shape0S0120102_I0 != null && runnableBRunnable0Shape0S0120102_I0.A04) {
                return true;
            }
            float f = this.A05;
            int i = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
            float f2 = this.A00;
            if (i == 0) {
                if (f2 != this.A04) {
                    return true;
                }
            } else if (f2 > f) {
                return true;
            }
        }
        return false;
    }

    public final boolean A0B(float f, float f2) {
        float max;
        float max2;
        RectF rectF = this.A0I;
        rectF.set(this.A0H);
        Matrix matrix = this.A0B;
        matrix.mapRect(rectF);
        float width = (float) getWidth();
        float f3 = rectF.left;
        float f4 = rectF.right;
        float f5 = width - 0.0f;
        if (f4 - f3 < f5) {
            max = ((f5 - (f4 + f3)) / 2.0f) + 0.0f;
        } else {
            max = Math.max(width - f4, Math.min(0.0f - f3, f));
        }
        float height = (float) getHeight();
        float f6 = rectF.top;
        float f7 = rectF.bottom;
        float f8 = height - 0.0f;
        if (f7 - f6 < f8) {
            max2 = ((f8 - (f7 + f6)) / 2.0f) + 0.0f;
        } else {
            max2 = Math.max(height - f7, Math.min(0.0f - f6, f2));
        }
        matrix.postTranslate(max, max2);
        setImageMatrix(matrix);
        return max == f && max2 == f2;
    }

    public Bitmap getFullViewCroppedBitmap() {
        if (!this.A0T) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Matrix matrix = new Matrix(this.A0A);
        if (this.A0J == null) {
            return createBitmap;
        }
        canvas.concat(matrix);
        this.A0J.draw(canvas);
        return createBitmap;
    }

    public float getMinScale() {
        return this.A04;
    }

    public float getOriginalScale() {
        return this.A06;
    }

    public Bitmap getPhoto() {
        BitmapDrawable bitmapDrawable = this.A0J;
        if (bitmapDrawable != null) {
            return bitmapDrawable.getBitmap();
        }
        return null;
    }

    public float getScale() {
        return this.A00;
    }

    private int getScaledMinScalingSpan() {
        Resources resources = getContext().getResources();
        try {
            return resources.getDimensionPixelSize(resources.getIdentifier("config_minScalingSpan", "dimen", "android"));
        } catch (Resources.NotFoundException unused) {
            return (int) TypedValue.applyDimension(5, 27.0f, resources.getDisplayMetrics());
        }
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        float x;
        float y;
        if (!this.A0V || !this.A0Y) {
            return false;
        }
        if (!this.A0U) {
            float f = this.A00;
            float f2 = this.A04;
            float f3 = f2 * 2.0f;
            if (f == f3) {
                f3 = f2;
            }
            float min = Math.min(this.A03, Math.max(f2, f3));
            RunnableC76313lR r4 = this.A0R;
            if (r4 != null) {
                if (min == f2) {
                    x = (float) (getWidth() >> 1);
                    y = (float) (getHeight() >> 1);
                } else {
                    x = motionEvent.getX();
                    y = motionEvent.getY();
                }
                r4.A00(f, min, x, y, 200);
            }
        }
        this.A0U = false;
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        if (!this.A0Y) {
            return true;
        }
        RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0 = this.A0P;
        if (runnableBRunnable0Shape0S0120102_I0 != null) {
            runnableBRunnable0Shape0S0120102_I0.A04 = false;
            runnableBRunnable0Shape0S0120102_I0.A05 = true;
        }
        RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I02 = this.A0O;
        if (runnableBRunnable0Shape0S0120102_I02 == null) {
            return true;
        }
        runnableBRunnable0Shape0S0120102_I02.A04 = false;
        runnableBRunnable0Shape0S0120102_I02.A05 = true;
        return true;
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.A0J != null && this.A0K != null) {
            int width = (getWidth() - this.A0K.getIntrinsicWidth()) >> 1;
            int height = (getHeight() - this.A0K.getIntrinsicHeight()) >> 1;
            Drawable drawable = this.A0K;
            drawable.setBounds(width, height, drawable.getIntrinsicWidth() + width, this.A0K.getIntrinsicHeight() + height);
            this.A0K.draw(canvas);
        }
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0;
        if (!this.A0Y || (runnableBRunnable0Shape0S0120102_I0 = this.A0P) == null || runnableBRunnable0Shape0S0120102_I0.A04) {
            return true;
        }
        runnableBRunnable0Shape0S0120102_I0.A02 = -1;
        runnableBRunnable0Shape0S0120102_I0.A00 = f;
        runnableBRunnable0Shape0S0120102_I0.A01 = f2;
        runnableBRunnable0Shape0S0120102_I0.A05 = false;
        runnableBRunnable0Shape0S0120102_I0.A04 = true;
        ((View) runnableBRunnable0Shape0S0120102_I0.A03).post(runnableBRunnable0Shape0S0120102_I0);
        return true;
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.A0W = true;
        Matrix matrix = this.A0A;
        if (matrix == null || matrix.equals(getImageMatrix())) {
            A08(z);
        }
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        if (this.A0Y) {
            this.A0X = false;
            A04(this.A00 * scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
        }
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        if (!this.A0Y) {
            return false;
        }
        RunnableC76313lR r1 = this.A0R;
        if (r1 != null) {
            r1.A06 = false;
            r1.A07 = true;
        }
        this.A0X = true;
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        RunnableC76313lR r1;
        if (this.A0Y && this.A0X) {
            this.A0U = true;
            Matrix matrix = this.A0B;
            matrix.set(this.A0C);
            this.A00 = this.A06;
            setImageMatrix(matrix);
        }
        float f = this.A00;
        float f2 = this.A04;
        if (f < f2 && (r1 = this.A0R) != null) {
            r1.A00(f, f2, (float) (getWidth() >> 1), (float) (getHeight() >> 1), 100);
        }
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (!this.A0Y) {
            return true;
        }
        A0B(-f, -f2);
        return true;
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        if (this.A0M != null && !this.A0X && this.A0V) {
            A03(motionEvent.getX(), motionEvent.getY());
        }
        this.A0X = false;
        return true;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!(this.A0L == null || this.A0N == null)) {
            if (!isEnabled()) {
                return false;
            }
            this.A0L.onTouchEvent(motionEvent);
            this.A0N.A00.AXZ(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            int pointerCount = motionEvent.getPointerCount();
            if (actionMasked != 1) {
                if (actionMasked != 3) {
                    return true;
                }
            } else if (this.A0M != null && !this.A0X && pointerCount == 1 && !this.A0V) {
                this.A0E.set(motionEvent.getX(), motionEvent.getY());
                post(this.A0Z);
            }
            RunnableBRunnable0Shape0S0120102_I0 runnableBRunnable0Shape0S0120102_I0 = this.A0P;
            if (runnableBRunnable0Shape0S0120102_I0 != null && !runnableBRunnable0Shape0S0120102_I0.A04) {
                A09(false);
            }
        }
        return true;
    }

    public void setAllowFullViewCrop(boolean z) {
        if (z != this.A0T) {
            this.A0T = z;
            requestLayout();
            invalidate();
        }
    }

    public void setDoubleTapToZoomEnabled(boolean z) {
        this.A0V = z;
    }

    public void setInitialFitTolerance(float f) {
        this.A01 = f;
    }

    public void setInitialScaleType(int i) {
        this.A09 = i;
    }

    public void setIsLongpressEnabled(boolean z) {
        this.A0N.A00.AcG(z);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.A0M = onClickListener;
    }

    public void setOverlay(Drawable drawable) {
        this.A0K = drawable;
    }

    public void setUnderscaleAmount(float f) {
        this.A08 = f;
    }
}
