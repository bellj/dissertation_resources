package com.whatsapp.mediaview;

import X.AbstractC14720lw;
import X.AbstractC16130oV;
import X.AbstractC35501i8;
import X.AbstractC35521iA;
import X.AbstractC454421p;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass0B5;
import X.AnonymousClass12P;
import X.AnonymousClass19N;
import X.AnonymousClass2TT;
import X.AnonymousClass33Z;
import X.AnonymousClass3XJ;
import X.AnonymousClass443;
import X.C06260Su;
import X.C14960mK;
import X.C15890o4;
import X.C454321o;
import X.C58362oh;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.base.WaFragment;
import com.whatsapp.biz.catalog.CatalogMediaViewFragment;
import com.whatsapp.gesture.VerticalSwipeDismissBehavior;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public abstract class MediaViewBaseFragment extends WaFragment implements AbstractC35501i8 {
    public static final boolean A0H;
    public Rect A00;
    public Bundle A01;
    public View A02;
    public View A03;
    public ViewGroup A04;
    public TextView A05;
    public Toolbar A06;
    public TextEmojiLabel A07;
    public C58362oh A08;
    public AnonymousClass443 A09;
    public AbstractC35501i8 A0A;
    public AbstractC454421p A0B;
    public OutOfMemoryError A0C;
    public Runnable A0D;
    public boolean A0E = false;
    public boolean A0F = false;
    public boolean A0G = true;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 21) {
            z = true;
        }
        A0H = z;
    }

    public static void A06(Activity activity, boolean z) {
        int i = 1280;
        if (!z) {
            i = 1285;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            i |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        if (A0H) {
            i |= 512;
            if (!z) {
                i |= 2;
            }
        }
        activity.getWindow().getDecorView().setSystemUiVisibility(i);
    }

    public static /* synthetic */ void A07(WindowInsets windowInsets, MediaViewBaseFragment mediaViewBaseFragment) {
        Rect rect = new Rect();
        mediaViewBaseFragment.A00 = rect;
        rect.set(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        mediaViewBaseFragment.A1I();
    }

    public static /* synthetic */ boolean A08(MediaViewBaseFragment mediaViewBaseFragment) {
        PhotoView A19 = mediaViewBaseFragment.A19(mediaViewBaseFragment.A1C(mediaViewBaseFragment.A09.getCurrentItem()));
        return A19 != null && A19.A0A();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        try {
            return layoutInflater.inflate(R.layout.media_view, viewGroup, false);
        } catch (OutOfMemoryError e) {
            this.A0C = e;
            return null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        if (this.A09 != null) {
            for (int i = 0; i < this.A09.getChildCount(); i++) {
                View childAt = this.A09.getChildAt(i);
                if (childAt instanceof FrameLayout) {
                    int i2 = 0;
                    while (true) {
                        ViewGroup viewGroup = (ViewGroup) childAt;
                        if (i2 < viewGroup.getChildCount()) {
                            View childAt2 = viewGroup.getChildAt(i2);
                            if (childAt2 instanceof PhotoView) {
                                ((PhotoView) childAt2).A01();
                            }
                            i2++;
                        }
                    }
                }
            }
        }
        super.A11();
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A04.removeView(this.A09);
        this.A04 = null;
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        C15890o4 r0;
        super.A13();
        if (!this.A0F) {
            Context A01 = A01();
            if (!(this instanceof MediaViewFragment)) {
                r0 = ((CatalogMediaViewFragment) this).A06;
            } else {
                r0 = ((MediaViewFragment) this).A0h;
            }
            if (!RequestPermissionActivity.A0W(A01, r0)) {
                this.A0F = true;
                A1E();
            }
        }
        A1M(true, true);
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        A1M(true, true);
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        AnonymousClass12P.A05(A0C().getWindow());
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        AbstractC454421p r0;
        C15890o4 r02;
        if (!this.A0F) {
            Context A01 = A01();
            if (!(this instanceof MediaViewFragment)) {
                r02 = ((CatalogMediaViewFragment) this).A06;
            } else {
                r02 = ((MediaViewFragment) this).A0h;
            }
            if (!RequestPermissionActivity.A0W(A01, r02)) {
                this.A0F = true;
                A1E();
            }
        }
        super.A16(bundle);
        AnonymousClass2TT r1 = new AnonymousClass2TT(A0C());
        if (AbstractC454421p.A00) {
            r0 = new C454321o(r1, this);
        } else {
            r0 = new AnonymousClass33Z(this);
        }
        this.A0B = r0;
        this.A09 = new AnonymousClass443(A01(), this);
        Bundle bundle2 = super.A05;
        if (bundle2 == null) {
            A1E();
        } else {
            this.A01 = bundle2.getBundle("animation_bundle");
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        boolean z = A0H;
        View decorView = A0C().getWindow().getDecorView();
        if (z) {
            decorView.setSystemUiVisibility(1792);
            view.findViewById(R.id.media_view_layout).setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() { // from class: X.4mi
                @Override // android.view.View.OnApplyWindowInsetsListener
                public final WindowInsets onApplyWindowInsets(View view2, WindowInsets windowInsets) {
                    MediaViewBaseFragment.A07(windowInsets, MediaViewBaseFragment.this);
                    return windowInsets;
                }
            });
        } else {
            decorView.setSystemUiVisibility(1280);
        }
        Toolbar toolbar = (Toolbar) AnonymousClass028.A0D(A05(), R.id.toolbar);
        this.A06 = toolbar;
        toolbar.A07();
        ((ActivityC000800j) A0C()).A1e(this.A06);
        this.A06.setBackground(new ColorDrawable(AnonymousClass00T.A00(A01(), R.color.media_view_action_bar_background)));
        ((ActivityC000800j) A0C()).A1U().A0P(false);
        ((ActivityC000800j) A0C()).A1U().A0M(true);
        this.A06.setNavigationOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 47));
        View inflate = LayoutInflater.from(((ActivityC000800j) A0C()).A1U().A02()).inflate(R.layout.media_view_actionbar, (ViewGroup) null, false);
        View findViewById = inflate.findViewById(R.id.title_holder);
        findViewById.setClickable(true);
        findViewById.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 48));
        this.A07 = (TextEmojiLabel) findViewById.findViewById(R.id.contact_name);
        this.A05 = (TextView) findViewById.findViewById(R.id.date_time);
        this.A02 = inflate.findViewById(R.id.progress_bar);
        ((ActivityC000800j) A0C()).A1U().A0N(true);
        ((ActivityC000800j) A0C()).A1U().A0F(inflate);
        this.A03 = view.findViewById(R.id.title_protection);
        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(view, R.id.pager_container);
        this.A04 = viewGroup;
        viewGroup.addView(this.A09);
        View findViewById2 = view.findViewById(R.id.background);
        findViewById2.setBackground(new ColorDrawable(-16777216));
        AnonymousClass1 r1 = new VerticalSwipeDismissBehavior(A01()) { // from class: com.whatsapp.mediaview.MediaViewBaseFragment.1
            @Override // com.whatsapp.gesture.VerticalSwipeDismissBehavior, X.AbstractC009304r
            public void A0B(View view2, View view3, CoordinatorLayout coordinatorLayout, int[] iArr, int i, int i2, int i3) {
                MediaViewBaseFragment mediaViewBaseFragment = this;
                if (!(mediaViewBaseFragment instanceof MediaViewFragment) || !((MediaViewFragment) mediaViewBaseFragment).A1Y) {
                    super.A0B(view2, view3, coordinatorLayout, iArr, i, i2, i3);
                }
            }

            @Override // com.whatsapp.gesture.VerticalSwipeDismissBehavior, X.AbstractC009304r
            public boolean A0C(MotionEvent motionEvent, View view2, CoordinatorLayout coordinatorLayout) {
                if (motionEvent.getPointerCount() <= 1) {
                    MediaViewBaseFragment mediaViewBaseFragment = this;
                    if (!MediaViewBaseFragment.A08(mediaViewBaseFragment) && (!(mediaViewBaseFragment instanceof MediaViewFragment) || !((MediaViewFragment) mediaViewBaseFragment).A1Y)) {
                        return super.A0C(motionEvent, view2, coordinatorLayout);
                    }
                }
                C06260Su r0 = this.A04;
                if (r0 == null) {
                    return false;
                }
                r0.A02();
                return false;
            }
        };
        r1.A01 = 0.5f;
        r1.A0A = true;
        r1.A05 = new AnonymousClass3XJ(findViewById2, this);
        ((AnonymousClass0B5) view.findViewById(R.id.pager_container).getLayoutParams()).A00(r1);
        onConfigurationChanged(A02().getConfiguration());
    }

    public PhotoView A18(ViewGroup viewGroup) {
        PhotoView A18;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof PhotoView) {
                return (PhotoView) childAt;
            }
            if ((childAt instanceof ViewGroup) && (A18 = A18((ViewGroup) childAt)) != null) {
                return A18;
            }
        }
        return null;
    }

    public PhotoView A19(Object obj) {
        if (obj != null) {
            View findViewWithTag = this.A09.findViewWithTag(obj);
            if (findViewWithTag instanceof ViewGroup) {
                return A18((ViewGroup) findViewWithTag);
            }
        }
        return null;
    }

    public Object A1A() {
        if (!(this instanceof MediaViewFragment)) {
            return ((CatalogMediaViewFragment) this).A08;
        }
        AbstractC16130oV r0 = ((MediaViewFragment) this).A1I;
        if (r0 == null) {
            return null;
        }
        return r0.A0z;
    }

    public Object A1B() {
        if (this instanceof MediaViewFragment) {
            return ((MediaViewFragment) this).A1H;
        }
        CatalogMediaViewFragment catalogMediaViewFragment = (CatalogMediaViewFragment) this;
        return AnonymousClass19N.A00(catalogMediaViewFragment.A00, catalogMediaViewFragment.A02.A0D);
    }

    public Object A1C(int i) {
        if (!(this instanceof MediaViewFragment)) {
            return AnonymousClass19N.A00(i, ((CatalogMediaViewFragment) this).A02.A0D);
        }
        AbstractC16130oV A1Q = ((MediaViewFragment) this).A1Q(i);
        if (A1Q != null) {
            return A1Q.A0z;
        }
        return null;
    }

    public void A1D() {
        AbstractC14720lw r0 = (AbstractC14720lw) A0B();
        if (r0 != null) {
            r0.APJ();
        }
        Bundle bundle = this.A01;
        if (bundle != null) {
            this.A0E = true;
            this.A0B.A0B(bundle);
            return;
        }
        A1E();
    }

    public void A1E() {
        ActivityC000900k A0B = A0B();
        if (A0B != null && !A0B.isFinishing()) {
            if (A0C() instanceof AbstractC14720lw) {
                ((AbstractC14720lw) A0C()).ASf();
                return;
            }
            StringBuilder sb = new StringBuilder("mediaview/finish called from non-host activity: ");
            sb.append(A0C().getLocalClassName());
            Log.e(sb.toString());
            A0C().finish();
        }
    }

    public void A1F() {
        C58362oh r0;
        if (A0B() != null && (r0 = this.A08) != null) {
            r0.A06();
        }
    }

    public void A1G() {
        if (!(this instanceof MediaViewFragment)) {
            A1D();
            return;
        }
        MediaViewFragment mediaViewFragment = (MediaViewFragment) this;
        RunnableBRunnable0Shape0S0310000_I0 runnableBRunnable0Shape0S0310000_I0 = mediaViewFragment.A0C;
        if (runnableBRunnable0Shape0S0310000_I0 != null) {
            runnableBRunnable0Shape0S0310000_I0.A03 = true;
            ((Thread) runnableBRunnable0Shape0S0310000_I0.A02).interrupt();
            mediaViewFragment.A0C = null;
        }
        AbstractC35521iA r0 = mediaViewFragment.A18;
        if (r0 != null) {
            r0.AeQ();
        }
        mediaViewFragment.A1D();
    }

    public void A1H() {
        if (this instanceof MediaViewFragment) {
            MediaViewFragment mediaViewFragment = (MediaViewFragment) this;
            if (mediaViewFragment.A10 == null || (mediaViewFragment.A1a && mediaViewFragment.A1I != null)) {
                mediaViewFragment.A1G();
                return;
            }
            mediaViewFragment.A1I = null;
            mediaViewFragment.A0v(C14960mK.A0C(mediaViewFragment.A01(), mediaViewFragment.A10));
            mediaViewFragment.A1E();
        }
    }

    public final void A1I() {
        if (this.A09 != null) {
            for (int i = 0; i < this.A09.getChildCount(); i++) {
                A1K(this.A09.getChildAt(i));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0054, code lost:
        if (r3 != null) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1J(int r7) {
        /*
        // Method dump skipped, instructions count: 325
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediaview.MediaViewBaseFragment.A1J(int):void");
    }

    public void A1K(View view) {
        View view2;
        if (this.A00 != null) {
            ViewGroup viewGroup = (ViewGroup) view.findViewById(R.id.footerView);
            if (viewGroup != null || (viewGroup = (ViewGroup) view.findViewById(R.id.footer)) != null) {
                View findViewById = viewGroup.findViewById(R.id.caption);
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) viewGroup.getLayoutParams();
                int i = A02().getConfiguration().orientation;
                if (findViewById != null) {
                    View findViewById2 = view.findViewById(R.id.exo_player_navbar_padding_view);
                    if (findViewById2 != null) {
                        ViewGroup.LayoutParams layoutParams2 = findViewById2.getLayoutParams();
                        if (i == 1) {
                            layoutParams2.height = this.A00.bottom;
                        } else if (i == 2) {
                            layoutParams2.height = 0;
                        }
                        findViewById2.setLayoutParams(layoutParams2);
                    }
                    if (i == 1) {
                        layoutParams.bottomMargin = this.A00.bottom;
                    } else if (i == 2) {
                        layoutParams.bottomMargin = 0;
                    }
                } else {
                    viewGroup.setPadding(viewGroup.getPaddingLeft(), viewGroup.getPaddingTop(), viewGroup.getPaddingRight(), 0);
                    View findViewWithTag = viewGroup.findViewWithTag("navigation_protection");
                    if (i == 1) {
                        if (findViewWithTag == null) {
                            view2 = new View(A01());
                        } else {
                            view2 = findViewWithTag;
                        }
                        view2.setBackgroundColor(AnonymousClass00T.A00(A01(), R.color.media_view_footer_background));
                        view2.setTag("navigation_protection");
                        view2.setLayoutParams(new LinearLayout.LayoutParams(-1, this.A00.bottom));
                        if (findViewWithTag == null) {
                            viewGroup.addView(view2);
                        }
                    } else if (findViewWithTag != null) {
                        viewGroup.removeViewInLayout(findViewWithTag);
                    }
                    View findViewById3 = viewGroup.findViewById(R.id.footer_padding_bottom);
                    if (findViewById3 != null) {
                        findViewById3.setVisibility(8);
                    }
                }
                Rect rect = this.A00;
                layoutParams.leftMargin = rect.left;
                layoutParams.rightMargin = rect.right;
                layoutParams.topMargin = rect.top;
                viewGroup.setLayoutParams(layoutParams);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1L(boolean r10, int r11) {
        /*
            r9 = this;
            X.443 r0 = r9.A09
            int r8 = r0.getChildCount()
            r7 = 0
            r6 = 0
        L_0x0008:
            if (r6 >= r8) goto L_0x0042
            X.443 r0 = r9.A09
            android.view.View r1 = r0.getChildAt(r6)
            r0 = 2131363589(0x7f0a0705, float:1.8346991E38)
            android.view.View r5 = r1.findViewById(r0)
            r4 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            int r1 = r5.getVisibility()
            if (r10 == 0) goto L_0x0036
            if (r1 == 0) goto L_0x0033
            android.view.animation.AlphaAnimation r2 = new android.view.animation.AlphaAnimation
            r2.<init>(r4, r3)
            r5.setVisibility(r7)
        L_0x002a:
            if (r11 <= 0) goto L_0x0033
            long r0 = (long) r11
            r2.setDuration(r0)
            r5.startAnimation(r2)
        L_0x0033:
            int r6 = r6 + 1
            goto L_0x0008
        L_0x0036:
            r0 = 4
            if (r1 == r0) goto L_0x0033
            android.view.animation.AlphaAnimation r2 = new android.view.animation.AlphaAnimation
            r2.<init>(r3, r4)
            r5.setVisibility(r0)
            goto L_0x002a
        L_0x0042:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediaview.MediaViewBaseFragment.A1L(boolean, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r5.A06.getVisibility() != 0) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        r5.A06.setVisibility(r2);
        r5.A06.startAnimation(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0043, code lost:
        if (r7 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0045, code lost:
        r1 = A0B();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0049, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004b, code lost:
        A06(r1, r5.A0G);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        if (r5.A06.getVisibility() != 4) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1M(boolean r6, boolean r7) {
        /*
            r5 = this;
            boolean r0 = r5.A0E
            if (r0 != 0) goto L_0x0050
            boolean r0 = r5.A0G
            if (r0 == r6) goto L_0x0050
            r5.A0G = r6
            r0 = 400(0x190, float:5.6E-43)
            r5.A1L(r6, r0)
            r2 = 250(0xfa, double:1.235E-321)
            r4 = 0
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r6 == 0) goto L_0x0051
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r1.<init>(r4, r0)
            r1.setDuration(r2)
            android.view.View r0 = r5.A03
            int r0 = r0.getVisibility()
            r2 = 0
            if (r0 == 0) goto L_0x0031
            android.view.View r0 = r5.A03
            r0.setVisibility(r2)
            android.view.View r0 = r5.A03
            r0.startAnimation(r1)
        L_0x0031:
            androidx.appcompat.widget.Toolbar r0 = r5.A06
            int r0 = r0.getVisibility()
            if (r0 == 0) goto L_0x0043
        L_0x0039:
            androidx.appcompat.widget.Toolbar r0 = r5.A06
            r0.setVisibility(r2)
            androidx.appcompat.widget.Toolbar r0 = r5.A06
            r0.startAnimation(r1)
        L_0x0043:
            if (r7 == 0) goto L_0x0050
            X.00k r1 = r5.A0B()
            if (r1 == 0) goto L_0x0050
            boolean r0 = r5.A0G
            A06(r1, r0)
        L_0x0050:
            return
        L_0x0051:
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r1.<init>(r0, r4)
            r1.setDuration(r2)
            android.view.View r0 = r5.A03
            int r0 = r0.getVisibility()
            r2 = 4
            if (r0 == r2) goto L_0x006c
            android.view.View r0 = r5.A03
            r0.setVisibility(r2)
            android.view.View r0 = r5.A03
            r0.startAnimation(r1)
        L_0x006c:
            androidx.appcompat.widget.Toolbar r0 = r5.A06
            int r0 = r0.getVisibility()
            if (r0 == r2) goto L_0x0043
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediaview.MediaViewBaseFragment.A1M(boolean, boolean):void");
    }

    public boolean A1N() {
        ActivityC000900k A0B = A0B();
        return (A0B instanceof AbstractC14720lw) && ((AbstractC14720lw) A0B).Add();
    }

    @Override // X.AbstractC35501i8
    public void AXp(boolean z) {
        Runnable runnable = this.A0D;
        if (runnable != null && z) {
            runnable.run();
        }
        this.A0E = false;
        AbstractC35501i8 r0 = this.A0A;
        if (r0 != null) {
            r0.AXp(z);
            this.A0A = null;
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        Window window;
        Context A01;
        int i;
        super.onConfigurationChanged(configuration);
        if (A0H) {
            A0C().getWindow().setStatusBarColor(AnonymousClass00T.A00(A01(), R.color.media_view_status_bar_background));
            int i2 = configuration.orientation;
            if (i2 == 2) {
                window = A0C().getWindow();
                A01 = A01();
                i = R.color.media_view_footer_background;
            } else {
                if (i2 == 1) {
                    window = A0C().getWindow();
                    A01 = A01();
                    i = 17170445;
                }
                A0C().getWindow().addFlags(Integer.MIN_VALUE);
            }
            window.setNavigationBarColor(AnonymousClass00T.A00(A01, i));
            A0C().getWindow().addFlags(Integer.MIN_VALUE);
        }
    }
}
