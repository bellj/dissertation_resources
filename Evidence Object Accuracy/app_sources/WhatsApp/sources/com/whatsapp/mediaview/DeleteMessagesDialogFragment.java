package com.whatsapp.mediaview;

import X.AbstractC116445Vl;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC35511i9;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass11G;
import X.AnonymousClass19J;
import X.AnonymousClass19M;
import X.AnonymousClass1IS;
import X.AnonymousClass3U6;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15550nR;
import X.C15600nX;
import X.C15610nY;
import X.C16170oZ;
import X.C16370ot;
import X.C20710wC;
import X.C38211ni;
import X.C64993Hs;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.whatsapp.mediaview.DeleteMessagesDialogFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

/* loaded from: classes2.dex */
public class DeleteMessagesDialogFragment extends Hilt_DeleteMessagesDialogFragment {
    public AbstractC116445Vl A00 = new AnonymousClass3U6(this);
    public AbstractC35511i9 A01 = new AbstractC35511i9() { // from class: X.52X
        @Override // X.AbstractC35511i9
        public final void AOx() {
            AnonymousClass01E r1 = ((AnonymousClass01E) DeleteMessagesDialogFragment.this).A0D;
            if (r1 instanceof AbstractC35511i9) {
                ((AbstractC35511i9) r1).AOx();
            }
        }
    };
    public C14900mE A02;
    public C16170oZ A03;
    public C15550nR A04;
    public C15610nY A05;
    public C14830m7 A06;
    public C14820m6 A07;
    public AnonymousClass018 A08;
    public C16370ot A09;
    public C15600nX A0A;
    public AnonymousClass19M A0B;
    public C14850m9 A0C;
    public C20710wC A0D;
    public AnonymousClass11G A0E;
    public AnonymousClass19J A0F;
    public AbstractC14440lR A0G;

    public static DeleteMessagesDialogFragment A00(AbstractC14640lm r6, List list) {
        DeleteMessagesDialogFragment deleteMessagesDialogFragment = new DeleteMessagesDialogFragment();
        Bundle A0D = C12970iu.A0D();
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A0l.add(C12980iv.A0f(it).A0z);
        }
        C38211ni.A09(A0D, A0l);
        if (r6 != null) {
            A0D.putString("jid", r6.getRawString());
        }
        A0D.putBoolean("is_revokable", true);
        deleteMessagesDialogFragment.A0U(A0D);
        return deleteMessagesDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        List<AnonymousClass1IS> A04;
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (!(bundle2 == null || A0p() == null || (A04 = C38211ni.A04(bundle2)) == null)) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (AnonymousClass1IS r1 : A04) {
                linkedHashSet.add(this.A09.A03(r1));
            }
            AbstractC14640lm A01 = AbstractC14640lm.A01(bundle2.getString("jid"));
            boolean z = bundle2.getBoolean("is_revokable");
            String A012 = C64993Hs.A01(A0p(), this.A04, this.A05, A01, linkedHashSet);
            Context A0p = A0p();
            C14850m9 r0 = this.A0C;
            C14900mE r02 = this.A02;
            C14830m7 r15 = this.A06;
            AbstractC14440lR r14 = this.A0G;
            AnonymousClass19M r13 = this.A0B;
            Dialog A00 = C64993Hs.A00(A0p, this.A00, this.A01, r02, this.A03, this.A04, this.A05, null, r15, this.A07, this.A08, this.A0A, r13, r0, this.A0D, this.A0E, this.A0F, r14, A012, linkedHashSet, z);
            if (A00 != null) {
                return A00;
            }
        }
        A1C();
        return super.A1A(bundle);
    }
}
