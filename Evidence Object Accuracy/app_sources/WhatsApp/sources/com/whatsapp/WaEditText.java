package com.whatsapp;

import X.AbstractActivityC452520u;
import X.AbstractC14290lC;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass1US;
import X.AnonymousClass2GZ;
import X.AnonymousClass4C8;
import X.AnonymousClass534;
import X.AnonymousClass5RH;
import X.C20920wX;
import X.C71133cR;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.Selection;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.components.PhoneNumberEntry;

/* loaded from: classes2.dex */
public class WaEditText extends AbstractC14290lC {
    public Rect A00;
    public AnonymousClass5RH A01;
    public AnonymousClass01d A02;
    public AnonymousClass018 A03;
    public boolean A04;
    public final Runnable A05 = new RunnableBRunnable0Shape1S0100000_I0_1(this, 34);

    public WaEditText(Context context) {
        super(context);
    }

    public WaEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet);
    }

    public WaEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet);
    }

    private void A00(Context context, AttributeSet attributeSet) {
        if (attributeSet != null && !isInEditMode()) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0Q);
            int resourceId = obtainStyledAttributes.getResourceId(3, 0);
            if (resourceId != 0) {
                setContentDescription(this.A03.A09(resourceId));
            }
            int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
            if (resourceId2 != 0) {
                setHint(context.getString(resourceId2));
            }
            int resourceId3 = obtainStyledAttributes.getResourceId(2, 0);
            if (resourceId3 != 0) {
                setImeActionLabel(this.A03.A09(resourceId3), getImeActionId());
            }
            int resourceId4 = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId4 != 0) {
                setText(this.A03.A09(resourceId4));
            }
            obtainStyledAttributes.recycle();
        }
    }

    public void A03() {
        InputMethodManager A0Q = this.A02.A0Q();
        this.A04 = false;
        removeCallbacks(this.A05);
        if (A0Q != null) {
            A0Q.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    public void A04(boolean z) {
        InputMethodManager A0Q = this.A02.A0Q();
        if (A0Q == null) {
            return;
        }
        if (A0Q.isFullscreenMode() && !z) {
            return;
        }
        if (A0Q.isActive(this)) {
            this.A04 = false;
            removeCallbacks(this.A05);
            A0Q.showSoftInput(this, 0);
            return;
        }
        requestFocus();
        this.A04 = true;
    }

    @Override // X.AnonymousClass011, android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        if (this.A04) {
            Runnable runnable = this.A05;
            removeCallbacks(runnable);
            post(runnable);
        }
        return onCreateInputConnection;
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
        } catch (IndexOutOfBoundsException unused) {
            setText(AnonymousClass1US.A01(getText()));
        }
    }

    @Override // X.AnonymousClass011, android.widget.TextView
    public boolean onTextContextMenuItem(int i) {
        ClipboardManager A0B;
        ClipData primaryClip;
        String str;
        String num;
        String A01;
        AnonymousClass5RH r0 = this.A01;
        if (r0 != null) {
            PhoneNumberEntry phoneNumberEntry = ((AnonymousClass534) r0).A00;
            if (!((i != 16908322 && i != 16908337) || (A0B = phoneNumberEntry.A05.A0B()) == null || (primaryClip = A0B.getPrimaryClip()) == null || primaryClip.getItemCount() == 0)) {
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                if (itemAt == null || itemAt.getText() == null) {
                    str = "";
                } else {
                    str = itemAt.getText().toString();
                }
                if (str.startsWith("+")) {
                    try {
                        C71133cR A0E = C20920wX.A00().A0E(str, null);
                        num = Integer.toString(A0E.countryCode_);
                        A01 = C20920wX.A01(A0E);
                    } catch (AnonymousClass4C8 unused) {
                    }
                    if (AbstractActivityC452520u.A03(phoneNumberEntry.A01, num, A01) == 1) {
                        phoneNumberEntry.A02.setText(num);
                        phoneNumberEntry.A03.setText(A01);
                    }
                }
            }
        }
        return super.onTextContextMenuItem(i);
    }

    public void setCursorPosition_internal(int i, int i2) {
        int length = getText().length();
        Selection.setSelection(getText(), Math.min(i, length), Math.min(i2, length));
    }

    public void setOnContextMenuListener(AnonymousClass5RH r1) {
        this.A01 = r1;
    }

    public void setSpan_internal(Object obj, int i, int i2, int i3) {
        getText().setSpan(obj, i, Math.min(i2, getText().length()), i3);
    }

    public void setVisibleBounds(Rect rect) {
        this.A00 = rect;
    }
}
