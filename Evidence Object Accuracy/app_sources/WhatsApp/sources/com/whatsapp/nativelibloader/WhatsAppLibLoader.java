package com.whatsapp.nativelibloader;

import X.AbstractC15710nm;
import X.AnonymousClass009;
import X.AnonymousClass129;
import X.AnonymousClass1HP;
import X.AnonymousClass1Q8;
import X.C14820m6;
import X.C14950mJ;
import X.C21030wi;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.facebook.superpack.AssetDecompressor;
import com.whatsapp.AbstractAppShellDelegate;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* loaded from: classes2.dex */
public class WhatsAppLibLoader {
    public static final String[] A07 = new String[0];
    public static final String[] A08 = {"vlc", "whatsapp", "curve25519"};
    public Boolean A00 = null;
    public Map A01 = null;
    public final AbstractC15710nm A02;
    public final AnonymousClass129 A03;
    public final C14820m6 A04;
    public final C14950mJ A05;
    public final C21030wi A06;

    public static native String getJNICodeVersion();

    public static native void testLibraryUsable(byte[] bArr);

    public WhatsAppLibLoader(AbstractC15710nm r2, AnonymousClass129 r3, C14820m6 r4, C14950mJ r5, C21030wi r6) {
        this.A02 = r2;
        this.A05 = r5;
        this.A04 = r4;
        this.A06 = r6;
        this.A03 = r3;
    }

    public static final boolean A00() {
        UnsatisfiedLinkError e;
        String str;
        byte[] bArr = new byte[3];
        try {
            testLibraryUsable(bArr);
            if (!Arrays.equals(new byte[]{31, 41, 59}, bArr)) {
                Log.w("whatsapplibloader/usable test array does not match");
                return false;
            }
            try {
                String jNICodeVersion = getJNICodeVersion();
                StringBuilder sb = new StringBuilder();
                sb.append("whatsapplibloader/usable jniVersion: ");
                sb.append(jNICodeVersion);
                Log.i(sb.toString());
                if (!"2.22.17.70".equals(jNICodeVersion)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("whatsapplibloader/usable version does not match. JAVA version: ");
                    sb2.append("2.22.17.70");
                    sb2.append(", JNI version: ");
                    sb2.append(jNICodeVersion);
                    Log.w(sb2.toString());
                    return false;
                }
                Log.i("whatsapplibloader/usable isLibraryUsable: True");
                return true;
            } catch (UnsatisfiedLinkError e2) {
                e = e2;
                str = "whatsapplibloader/usable error while testing library usability getJNICodeVersion";
                Log.w(str, e);
                return false;
            }
        } catch (UnsatisfiedLinkError e3) {
            e = e3;
            str = "whatsapplibloader/usable error while testing library usability testLibraryUsable";
        }
    }

    public final void A01(Context context, String str) {
        StringBuilder sb = new StringBuilder("whatsapplibloader/system-load-library-with-install start, loading: ");
        sb.append(str);
        Log.i(sb.toString());
        try {
            System.loadLibrary(str);
        } catch (UnsatisfiedLinkError e) {
            Log.w("whatsapplibloader/system-load-library-with-install error", e);
            A02(context, Arrays.asList(str));
        }
        Log.i("whatsapplibloader/system-load-library-with-install end");
    }

    public synchronized void A02(Context context, List list) {
        StringBuilder sb = new StringBuilder();
        sb.append("whatsapplibloader/try-install start, loading: ");
        sb.append(list.size());
        Log.i(sb.toString());
        String A02 = AnonymousClass1HP.A02();
        String str = "x86";
        if (A02.startsWith("armeabi-v7")) {
            str = "armeabi-v7a";
        } else if (A02.startsWith("arm64-v8a")) {
            str = "arm64-v8a";
        } else if (A02.startsWith("x86_64")) {
            str = "x86_64";
        } else if (!A02.startsWith(str)) {
            StringBuilder sb2 = new StringBuilder("can not find lib folder for ABI ");
            sb2.append(A02);
            throw new UnsatisfiedLinkError(sb2.toString());
        }
        StringBuilder sb3 = new StringBuilder("whatsapplibloader/arch resolved to ");
        sb3.append(str);
        Log.i(sb3.toString());
        try {
            ZipFile zipFile = new ZipFile(context.getPackageCodePath());
            try {
                if (this.A01 == null) {
                    HashMap hashMap = new HashMap(8);
                    StringBuilder sb4 = new StringBuilder("lib/");
                    sb4.append(str);
                    sb4.append("/lib");
                    String obj = sb4.toString();
                    byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements()) {
                        ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                        String name = zipEntry.getName();
                        if (name.endsWith(".so")) {
                            StringBuilder sb5 = new StringBuilder("whatsapplibloader/extractLibs found ");
                            sb5.append(name);
                            Log.i(sb5.toString());
                            if (name.startsWith(obj)) {
                                String[] split = name.split("/");
                                String str2 = split[split.length - 1];
                                File file = new File(context.getFilesDir(), str2);
                                if ("libunwindstack.so".equals(str2)) {
                                    file.delete();
                                } else {
                                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                                    while (true) {
                                        try {
                                            int read = inputStream.read(bArr);
                                            if (read <= 0) {
                                                break;
                                            }
                                            fileOutputStream.write(bArr, 0, read);
                                        } catch (Throwable th) {
                                            try {
                                                fileOutputStream.close();
                                            } catch (Throwable unused) {
                                            }
                                            throw th;
                                        }
                                    }
                                    fileOutputStream.close();
                                    inputStream.close();
                                    StringBuilder sb6 = new StringBuilder("whatsapplibloader/extractLibs copied ");
                                    sb6.append(file.getAbsolutePath());
                                    sb6.append(" from apk");
                                    Log.i(sb6.toString());
                                    hashMap.put(str2.substring(3, str2.length() - 3), file);
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    this.A01 = hashMap;
                } else {
                    Log.i("whatsapplibloader/try-install No need to extract libs again");
                }
                Map map = this.A01;
                ArrayList arrayList = new ArrayList(list);
                for (String str3 : map.keySet()) {
                    arrayList.remove(str3);
                }
                if (arrayList.isEmpty()) {
                    Map map2 = this.A01;
                    LinkedList linkedList = new LinkedList();
                    for (Object obj2 : list) {
                        linkedList.add(map2.get(obj2));
                    }
                    Iterator it = linkedList.iterator();
                    while (it.hasNext()) {
                        String absolutePath = ((File) it.next()).getAbsolutePath();
                        System.load(absolutePath);
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("whatsapplibloader/try-install loaded: ");
                        sb7.append(absolutePath);
                        Log.i(sb7.toString());
                    }
                    zipFile.close();
                } else {
                    StringBuilder sb8 = new StringBuilder("Libraries not found: ");
                    sb8.append(arrayList.toString());
                    throw new UnsatisfiedLinkError(sb8.toString());
                }
            } catch (Throwable th2) {
                try {
                    zipFile.close();
                } catch (Throwable unused2) {
                }
                throw th2;
            }
        } catch (IOException e) {
            Log.e("whatsapplibloader/try-install ioerror", e);
            throw new UnsatisfiedLinkError("IOException when install native library");
        }
    }

    public synchronized boolean A03() {
        boolean z;
        if (this.A00 == null) {
            Log.e("whatsapplibloader/is-loaded: isLoaded() was called before load was attempted");
        }
        z = false;
        if (this.A00 == Boolean.TRUE) {
            z = true;
        }
        return z;
    }

    public boolean A04(Context context) {
        try {
            A01(context, "superpack");
            byte[] bArr = new byte[3];
            try {
                AssetDecompressor.testDecompressorLibraryUsable(bArr);
                if (!Arrays.equals(new byte[]{71, 119, 83}, bArr)) {
                    Log.w("whatsappassetdecompressor/usable compressor test array does not match");
                    return false;
                }
                Log.i("whatsappassetdecompressor/decompressor-usable isLibraryUsable: True");
                return true;
            } catch (UnsatisfiedLinkError e) {
                Log.w("whatsappassetdecompressor/decompressor-usable error while testing compressor library usability testLibraryUsable", e);
                return false;
            }
        } catch (UnsatisfiedLinkError e2) {
            StringBuilder sb = new StringBuilder("whatsapplibloader/compression library is corrupt/");
            sb.append(e2);
            Log.i(sb.toString());
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            StringBuilder sb2 = new StringBuilder("whatsapplibloader/load-startup-libs: install source ");
            sb2.append(installerPackageName);
            Log.i(sb2.toString());
            return false;
        }
    }

    public synchronized boolean A05(Context context) {
        Boolean bool;
        AnonymousClass009.A05(context);
        try {
            bool = this.A00;
        } catch (UnsatisfiedLinkError e) {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            StringBuilder sb = new StringBuilder("whatsapplibloader/load-startup-libs: install source ");
            sb.append(installerPackageName);
            Log.i(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("whatsapplibloader/load-startup-libs: available internal storage: ");
            sb2.append(this.A05.A02());
            Log.i(sb2.toString());
            Log.e("WhatsAppLibLoader/loadStartupLibs", e);
            C14820m6 r3 = this.A04;
            if (r3.A1J("corrupt_installation_reported_timestamp", 86400000)) {
                this.A02.AaV("WhatsAppLibLoader/loadStartupLibs", "native libraries are missing", true);
                r3.A0k("corrupt_installation_reported_timestamp");
            }
            new Handler(Looper.getMainLooper()).post(new RunnableBRunnable0Shape9S0100000_I0_9(context, 1));
        }
        if (bool == null) {
            this.A00 = Boolean.FALSE;
            C21030wi r7 = this.A06;
            if (r7.A02(context, AbstractAppShellDelegate.COMPRESSED_LIBS_ARCHIVE_NAME)) {
                try {
                    AnonymousClass1Q8.A00(context);
                    String[] strArr = A08;
                    for (String str : strArr) {
                        if (!r7.A03(context, str)) {
                            A01(context, str);
                        }
                    }
                    String[] strArr2 = A07;
                    for (String str2 : strArr2) {
                        if (!r7.A03(context, str2)) {
                            Log.i("whatsapplibloader/system-load-optional-library start");
                            try {
                                System.loadLibrary(str2);
                            } catch (UnsatisfiedLinkError e2) {
                                Log.w("whatsapplibloader/load-optional-library error", e2);
                            }
                            Log.i("whatsapplibloader/system-load-optional-library end");
                        }
                    }
                    if (!A00()) {
                        Log.w("whatsapplibloader/load-startup-libs unable to use loaded libraries; trying install direct from apk");
                        A02(context, Arrays.asList(strArr));
                        Log.i("whatsapplibloader/load-startup-libs install direct from apk worked; retesting library usability");
                        if (!A00()) {
                            Log.w("whatsapplibloader/load-startup-libs library usability still broken; throwing to corrupt installation activity");
                            throw new UnsatisfiedLinkError("unable to use libraries despite successful install directly from apk");
                        }
                    }
                    this.A00 = Boolean.TRUE;
                } catch (IOException unused) {
                }
            }
            Log.i("whatsappsoloader/decompression failed");
            new Handler(Looper.getMainLooper()).post(new RunnableBRunnable0Shape9S0100000_I0_9(context, 1));
            return false;
        } else if (!bool.booleanValue()) {
            throw new UnsatisfiedLinkError();
        }
        return true;
    }
}
