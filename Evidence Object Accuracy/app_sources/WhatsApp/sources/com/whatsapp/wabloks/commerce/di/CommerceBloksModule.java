package com.whatsapp.wabloks.commerce.di;

import X.AnonymousClass01Y;
import X.AnonymousClass01b;
import X.C127185u5;
import X.C127195u6;
import X.C127535ue;
import X.C14820m6;
import X.C16660pY;
import X.C16670pZ;
import X.C16680pa;
import X.C16690pb;
import X.C16700pc;
import X.C16790pl;
import X.C16800pm;
import X.C16810pn;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public final class CommerceBloksModule {
    public static final Set A00(C14820m6 r3, C16660pY r4, C16670pZ r5, C16680pa r6, C16690pb r7) {
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(r3, 4);
        HashSet hashSet = new HashSet();
        Iterable<String> iterable = (Iterable) r4.A02.getValue();
        C16700pc.A0B(iterable);
        ArrayList arrayList = new ArrayList();
        for (String str : iterable) {
            Pattern compile = Pattern.compile(str);
            if (compile != null) {
                arrayList.add(compile);
            }
        }
        hashSet.add(new C16790pl(AnonymousClass01Y.A08(arrayList), new C127185u5(r6, r7)));
        List<String> singletonList = Collections.singletonList("com.bloks.www.whatsapp.commerce.galaxy_message");
        C16700pc.A0B(singletonList);
        ArrayList arrayList2 = new ArrayList();
        for (String str2 : singletonList) {
            Pattern compile2 = Pattern.compile(str2);
            if (compile2 != null) {
                arrayList2.add(compile2);
            }
        }
        hashSet.add(new C16790pl(AnonymousClass01Y.A08(arrayList2), new C127185u5(r5, r7)));
        return hashSet;
    }

    public static final Set A01(C16660pY r6, C16800pm r7, C16810pn r8) {
        C16700pc.A0E(r6, 2);
        HashSet hashSet = new HashSet();
        hashSet.add(new C16790pl(new AnonymousClass01b(Arrays.asList("com.bloks.www.whatsapp.commerce.galaxy_message")), new C127535ue(null, r7, new C127195u6(null, 5068499733235945L))));
        hashSet.add(new C16790pl(r6.A02.getValue(), new C127535ue(null, r8, new C127195u6(null, 5656824251012211L))));
        return hashSet;
    }
}
