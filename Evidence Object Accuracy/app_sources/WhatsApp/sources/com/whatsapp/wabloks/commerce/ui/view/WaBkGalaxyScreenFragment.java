package com.whatsapp.wabloks.commerce.ui.view;

import X.AnonymousClass015;
import X.AnonymousClass01E;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C130355zH;
import X.C16700pc;
import X.C17120qI;
import X.C34271fr;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.wabloks.base.BkFragment;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;

/* loaded from: classes2.dex */
public final class WaBkGalaxyScreenFragment extends Hilt_WaBkGalaxyScreenFragment {
    public View A00;
    public FrameLayout A01;
    public FrameLayout A02;
    public C34271fr A03;
    public WaTextView A04;
    public C17120qI A05;
    public WaGalaxyNavBarViewModel A06;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.wa_galaxy_bloks_fragment, viewGroup, false);
        this.A03 = C34271fr.A00(inflate, "", -1);
        return inflate;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        ((WaBkGalaxyLayoutViewModel) ((BkFragment) this).A05).A02.A04(A0G());
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        AnonymousClass015 A00 = C13000ix.A02(A0C()).A00(WaGalaxyNavBarViewModel.class);
        C16700pc.A0B(A00);
        this.A06 = (WaGalaxyNavBarViewModel) A00;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        this.A00 = C16700pc.A02(view, R.id.bloks_dialogfragment_progressbar);
        this.A01 = (FrameLayout) C16700pc.A02(view, R.id.bloks_dialogfragment);
        this.A02 = (FrameLayout) C16700pc.A02(view, R.id.extensions_container);
        this.A04 = (WaTextView) C16700pc.A02(view, R.id.extensions_error_text);
        FrameLayout frameLayout = this.A01;
        if (frameLayout == null) {
            throw C16700pc.A06("bloksFrameLayout");
        }
        frameLayout.setVisibility(8);
        View view2 = this.A00;
        if (view2 == null) {
            throw C16700pc.A06("progressBar");
        }
        view2.setVisibility(0);
        C12960it.A19(A0G(), ((WaBkGalaxyLayoutViewModel) ((BkFragment) this).A05).A02, this, 100);
        C12960it.A19(A0G(), ((WaBkGalaxyLayoutViewModel) ((BkFragment) this).A05).A03, this, 101);
        C12970iu.A1P(A0G(), ((WaBkGalaxyLayoutViewModel) ((BkFragment) this).A05).A01, this, 62);
        super.A17(bundle, view);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public void A1A() {
        A1I();
        if (((AnonymousClass01E) this).A05 != null) {
            String string = A03().getString("qpl_params");
            C17120qI r1 = this.A05;
            if (r1 != null) {
                C130355zH.A01(r1, string, "openScreen", null);
                return;
            }
            throw C16700pc.A06("uiObserversLazy");
        }
    }

    public final void A1I() {
        View view = this.A00;
        if (view == null) {
            throw C16700pc.A06("progressBar");
        }
        view.setVisibility(8);
        FrameLayout frameLayout = this.A01;
        if (frameLayout == null) {
            throw C16700pc.A06("bloksFrameLayout");
        }
        frameLayout.setVisibility(0);
    }

    public final void A1J(String str) {
        WaTextView waTextView = this.A04;
        if (waTextView == null) {
            throw C16700pc.A06("extensionsErrorText");
        }
        waTextView.setVisibility(0);
        waTextView.setText(str);
        FrameLayout frameLayout = this.A02;
        if (frameLayout == null) {
            throw C16700pc.A06("extensionsFrameLayout");
        }
        frameLayout.setVisibility(8);
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = this.A06;
        if (waGalaxyNavBarViewModel == null) {
            throw C16700pc.A06("waGalaxyNavBarViewModel");
        }
        waGalaxyNavBarViewModel.A03.A0B(false);
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel2 = this.A06;
        if (waGalaxyNavBarViewModel2 == null) {
            throw C16700pc.A06("waGalaxyNavBarViewModel");
        }
        waGalaxyNavBarViewModel2.A02.A0B(false);
        A1I();
    }
}
