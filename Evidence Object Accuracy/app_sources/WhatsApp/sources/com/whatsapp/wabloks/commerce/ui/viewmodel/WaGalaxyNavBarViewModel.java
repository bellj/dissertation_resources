package com.whatsapp.wabloks.commerce.ui.viewmodel;

import X.AbstractC28681Oo;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C12980iv;
import X.C130795zz;
import X.C16700pc;

/* loaded from: classes2.dex */
public final class WaGalaxyNavBarViewModel extends AnonymousClass015 {
    public AbstractC28681Oo A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final C130795zz A04;

    public WaGalaxyNavBarViewModel(C130795zz r2) {
        C16700pc.A0E(r2, 1);
        this.A04 = r2;
    }
}
