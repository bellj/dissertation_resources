package com.whatsapp.wabloks.commerce.ui.view;

import X.AbstractC115815Ta;
import X.AbstractC28681Oo;
import X.AbstractC50192Om;
import X.AbstractC50222Op;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass1AI;
import X.AnonymousClass1US;
import X.AnonymousClass2FL;
import X.AnonymousClass3BN;
import X.AnonymousClass546;
import X.C12970iu;
import X.C13000ix;
import X.C16700pc;
import X.C18580sg;
import X.C28391Mz;
import X.C48912Ik;
import X.C64173En;
import X.C89264Jh;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;
import java.util.Map;

/* loaded from: classes2.dex */
public class WaExtensionsBottomsheetModalActivity extends ActivityC13810kN implements AbstractC50192Om, AbstractC50222Op {
    public C48912Ik A00;
    public C64173En A01;
    public C18580sg A02;
    public WaGalaxyNavBarViewModel A03;
    public Map A04;
    public boolean A05;

    @Override // X.AbstractC50222Op
    public void AfT(AbstractC115815Ta r2, boolean z) {
    }

    public WaExtensionsBottomsheetModalActivity() {
        this(0);
    }

    public WaExtensionsBottomsheetModalActivity(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 133);
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            this.A02 = A1M.A2U();
            this.A00 = (C48912Ik) A1L.A19.get();
            this.A04 = A1M.A4d();
        }
    }

    @Override // X.AbstractC50192Om
    public C18580sg AAw() {
        return this.A02;
    }

    @Override // X.AbstractC50192Om
    public C64173En AHe() {
        return this.A01;
    }

    @Override // X.AbstractC50222Op
    public void AfS(AbstractC115815Ta r6) {
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = this.A03;
        try {
            AnonymousClass3BN r3 = new AnonymousClass3BN(r6.AAL().A0F(40));
            if (r3.A00 != null) {
                waGalaxyNavBarViewModel.A00 = new AbstractC28681Oo() { // from class: X.54L
                    @Override // X.AbstractC28681Oo
                    public final AbstractC14200l1 AAN() {
                        return AnonymousClass3BN.this.A00;
                    }
                };
            }
            String A0I = r6.AAL().A0I(36);
            if (AnonymousClass1US.A0C(A0I)) {
                A0I = r3.A02;
            }
            if (A0I != null) {
                waGalaxyNavBarViewModel.A04.A00(new AnonymousClass546(waGalaxyNavBarViewModel), A0I);
            }
        } catch (ClassCastException e) {
            Log.e(C16700pc.A08("Bloks: Invalid navigation bar type", e));
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AbstractC28681Oo r1 = this.A03.A00;
        if (r1 != null) {
            AnonymousClass1AI.A08(this.A01, r1);
        } else {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (C28391Mz.A02()) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent));
        }
        this.A01 = this.A00.A00(this, A0V(), new C89264Jh(this.A04));
        this.A03 = (WaGalaxyNavBarViewModel) C13000ix.A02(this).A00(WaGalaxyNavBarViewModel.class);
        Intent intent = getIntent();
        C16700pc.A0E(intent, 0);
        GalaxyBottomsheetBaseContainer galaxyBottomsheetBaseContainer = new GalaxyBottomsheetBaseContainer();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("screen_name", intent.getStringExtra("screen_name"));
        A0D.putString("screen_params", intent.getStringExtra("screen_params"));
        A0D.putParcelable("screen_cache_config", intent.getParcelableExtra("screen_cache_config"));
        galaxyBottomsheetBaseContainer.A0U(A0D);
        AnonymousClass01F A0V = A0V();
        AnonymousClass009.A05(A0V);
        galaxyBottomsheetBaseContainer.A1F(A0V, "fds_bottom_sheet_container");
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
