package com.whatsapp.wabloks.commerce.ui.view;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass61W;
import X.C16700pc;
import X.C17120qI;
import X.C18000rk;
import X.C18840t8;
import X.C48912Ik;
import X.C65963Lt;
import android.content.Intent;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* loaded from: classes2.dex */
public final class WaBkGalaxyActivity extends WaBloksActivity {
    public boolean A00;

    public WaBkGalaxyActivity() {
        this(0);
    }

    public WaBkGalaxyActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 132);
    }

    @Override // X.AbstractActivityC119315dW, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(r2, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            AnonymousClass61W.A04(this, C18000rk.A00(A1M.A1W));
            AnonymousClass61W.A03((C18840t8) A1M.A1V.get(), this);
            AnonymousClass61W.A06(this, A1M.A4e());
            AnonymousClass61W.A00((C48912Ik) r2.A19.get(), this);
            AnonymousClass61W.A02((C17120qI) A1M.ALs.get(), this);
            AnonymousClass61W.A05(this, A1M.A4d());
            AnonymousClass61W.A01(A1M.A2U(), this);
        }
    }

    @Override // com.whatsapp.wabloks.ui.WaBloksActivity
    public AnonymousClass01E A2e(Intent intent) {
        String stringExtra = intent.getStringExtra("screen_name");
        C16700pc.A0C(stringExtra);
        String stringExtra2 = intent.getStringExtra("screen_params");
        String stringExtra3 = intent.getStringExtra("qpl_param_map");
        C16700pc.A0E(stringExtra, 0);
        WaBkGalaxyScreenFragment waBkGalaxyScreenFragment = new WaBkGalaxyScreenFragment();
        waBkGalaxyScreenFragment.A1F(stringExtra);
        waBkGalaxyScreenFragment.A1D(stringExtra2);
        waBkGalaxyScreenFragment.A1C((C65963Lt) intent.getParcelableExtra("screen_cache_config"));
        waBkGalaxyScreenFragment.A1E(stringExtra3);
        return waBkGalaxyScreenFragment;
    }
}
