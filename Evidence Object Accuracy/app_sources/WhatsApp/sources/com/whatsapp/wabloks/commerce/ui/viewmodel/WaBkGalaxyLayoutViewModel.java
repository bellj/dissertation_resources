package com.whatsapp.wabloks.commerce.ui.viewmodel;

import X.AbstractC74513iC;
import X.AnonymousClass009;
import X.AnonymousClass01H;
import X.C12960it;
import X.C13000ix;
import X.C16700pc;
import X.C18640sm;
import X.C27691It;
import X.C91064Qh;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class WaBkGalaxyLayoutViewModel extends AbstractC74513iC {
    public final C18640sm A00;
    public final C27691It A01 = C13000ix.A03();
    public final C27691It A02 = C13000ix.A03();
    public final C27691It A03 = C13000ix.A03();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WaBkGalaxyLayoutViewModel(C18640sm r2, AnonymousClass01H r3) {
        super(r3);
        C16700pc.A0G(r2, r3);
        this.A00 = r2;
    }

    @Override // X.AbstractC74513iC
    public boolean A04(C91064Qh r5) {
        int i = r5.A00;
        if (i == 1 || i == 3 || i == 4 || i == 6 || i == 7) {
            C27691It r2 = this.A02;
            boolean A0B = this.A00.A0B();
            int i2 = R.string.extensions_error_no_network_error_view_text;
            if (A0B) {
                i2 = R.string.extensions_error_server_generic_error;
            }
            C12960it.A1A(r2, i2);
            return false;
        }
        AnonymousClass009.A07("BkLayoutViewModel: invalid error status");
        return false;
    }
}
