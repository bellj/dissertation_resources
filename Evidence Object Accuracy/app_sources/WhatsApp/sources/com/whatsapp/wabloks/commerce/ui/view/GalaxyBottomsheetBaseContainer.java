package com.whatsapp.wabloks.commerce.ui.view;

import X.AbstractC005102i;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.AnonymousClass015;
import X.AnonymousClass01E;
import X.C004902f;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C130795zz;
import X.C16700pc;
import X.C65963Lt;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape12S0100000_I1_6;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;

/* loaded from: classes2.dex */
public final class GalaxyBottomsheetBaseContainer extends Hilt_GalaxyBottomsheetBaseContainer {
    public Toolbar A00;
    public WaImageView A01;
    public C130795zz A02;
    public WaGalaxyNavBarViewModel A03;

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        boolean A0N = C16700pc.A0N(menu, menuInflater);
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = this.A03;
        if (waGalaxyNavBarViewModel == null) {
            throw C16700pc.A06("waGalaxyNavBarViewModel");
        } else if (C16700pc.A0O(waGalaxyNavBarViewModel.A03.A01(), Boolean.FALSE)) {
            menu.add(0, A0N ? 1 : 0, 0, A0I(R.string.learn_more));
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View A01 = C16700pc.A01(layoutInflater, viewGroup, R.layout.wa_extension_bottom_sheet);
        Toolbar toolbar = (Toolbar) C16700pc.A02(A01, R.id.bk_bottom_sheet_toolbar);
        this.A00 = toolbar;
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            ActivityC000800j r0 = (ActivityC000800j) A0B;
            if (toolbar == null) {
                throw C16700pc.A06("headerView");
            }
            r0.A1e(toolbar);
            AbstractC005102i A1U = r0.A1U();
            if (A1U != null) {
                A1U.A0P(false);
            }
            WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = this.A03;
            if (waGalaxyNavBarViewModel != null) {
                C12990iw.A1J(waGalaxyNavBarViewModel.A03, false);
                A0M();
                WaImageView waImageView = (WaImageView) C16700pc.A02(A01, R.id.bk_branding_image);
                C16700pc.A0E(waImageView, 0);
                this.A01 = waImageView;
                Toolbar toolbar2 = this.A00;
                if (toolbar2 == null) {
                    throw C16700pc.A06("headerView");
                }
                toolbar2.setVisibility(0);
                Toolbar toolbar3 = this.A00;
                if (toolbar3 == null) {
                    throw C16700pc.A06("headerView");
                }
                toolbar3.setNavigationOnClickListener(new ViewOnClickCListenerShape12S0100000_I1_6(this, 2));
                View A02 = C16700pc.A02(A01, R.id.wa_fcs_bottom_sheet_fragment_container);
                C004902f r5 = new C004902f(A0E());
                Bundle bundle2 = ((AnonymousClass01E) this).A05;
                if (bundle2 != null) {
                    String string = bundle2.getString("screen_name");
                    C16700pc.A0C(string);
                    String string2 = bundle2.getString("screen_params");
                    String string3 = bundle2.getString("qpl_param_map");
                    C16700pc.A0E(string, 0);
                    WaBkGalaxyScreenFragment waBkGalaxyScreenFragment = new WaBkGalaxyScreenFragment();
                    waBkGalaxyScreenFragment.A1F(string);
                    waBkGalaxyScreenFragment.A1D(string2);
                    waBkGalaxyScreenFragment.A1C((C65963Lt) bundle2.getParcelable("screen_cache_config"));
                    waBkGalaxyScreenFragment.A1E(string3);
                    r5.A0B(waBkGalaxyScreenFragment, "BK_FRAGMENT", A02.getId());
                    r5.A01();
                }
                return A01;
            }
            throw C16700pc.A06("waGalaxyNavBarViewModel");
        }
        throw C12980iv.A0n("null cannot be cast to non-null type androidx.appcompat.app.AppCompatActivity");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, 2131952346);
        AnonymousClass015 A00 = C13000ix.A02(A0C()).A00(WaGalaxyNavBarViewModel.class);
        C16700pc.A0B(A00);
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = (WaGalaxyNavBarViewModel) A00;
        C16700pc.A0E(waGalaxyNavBarViewModel, 0);
        this.A03 = waGalaxyNavBarViewModel;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16700pc.A0E(view, 0);
        WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = this.A03;
        if (waGalaxyNavBarViewModel != null) {
            C12960it.A18(this, waGalaxyNavBarViewModel.A01, 97);
            WaGalaxyNavBarViewModel waGalaxyNavBarViewModel2 = this.A03;
            if (waGalaxyNavBarViewModel2 != null) {
                C12960it.A18(this, waGalaxyNavBarViewModel2.A02, 99);
                WaGalaxyNavBarViewModel waGalaxyNavBarViewModel3 = this.A03;
                if (waGalaxyNavBarViewModel3 != null) {
                    C12960it.A18(this, waGalaxyNavBarViewModel3.A03, 98);
                    return;
                }
                throw C16700pc.A06("waGalaxyNavBarViewModel");
            }
            throw C16700pc.A06("waGalaxyNavBarViewModel");
        }
        throw C16700pc.A06("waGalaxyNavBarViewModel");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        C16700pc.A0E(dialogInterface, 0);
        super.onDismiss(dialogInterface);
        A0C().finish();
    }
}
