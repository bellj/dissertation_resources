package com.whatsapp.wabloks.base;

import X.AbstractC009404s;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.C117295Zj;
import X.C12970iu;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;

/* loaded from: classes4.dex */
public abstract class Hilt_BkScreenFragment extends BkFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1H();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1H()
            r2.A1G()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wabloks.base.Hilt_BkScreenFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1H();
        A1G();
    }

    public void A1G() {
        if (this instanceof Hilt_BkFcsPreloadingScreenFragment) {
            Hilt_BkFcsPreloadingScreenFragment hilt_BkFcsPreloadingScreenFragment = (Hilt_BkFcsPreloadingScreenFragment) this;
            if (!hilt_BkFcsPreloadingScreenFragment.A02) {
                hilt_BkFcsPreloadingScreenFragment.A02 = true;
                BkFcsPreloadingScreenFragment bkFcsPreloadingScreenFragment = (BkFcsPreloadingScreenFragment) hilt_BkFcsPreloadingScreenFragment;
                C51112Sw r1 = (C51112Sw) ((AbstractC51092Su) hilt_BkFcsPreloadingScreenFragment.generatedComponent());
                AnonymousClass01J r0 = r1.A0Y;
                C117295Zj.A0w(r1, r0, bkFcsPreloadingScreenFragment);
                bkFcsPreloadingScreenFragment.A00 = r0.A4D();
            }
        } else if (!this.A02) {
            this.A02 = true;
            C51112Sw r2 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            C117295Zj.A0w(r2, r2.A0Y, (BkScreenFragment) this);
        }
    }

    public final void A1H() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
