package com.whatsapp.wabloks.base;

import X.AbstractC50172Ok;
import X.AnonymousClass01E;
import X.AnonymousClass6E3;
import X.AnonymousClass6E4;
import X.AnonymousClass6E6;
import X.AnonymousClass6EB;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C17120qI;
import X.C50132Og;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;

/* loaded from: classes4.dex */
public class FdsContentFragmentManager extends Hilt_FdsContentFragmentManager {
    public LinearLayout A00;
    public C17120qI A01;
    public Runnable A02;
    public boolean A03 = true;

    public static FdsContentFragmentManager A00(String str) {
        FdsContentFragmentManager fdsContentFragmentManager = new FdsContentFragmentManager();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("fds_observer_id", str);
        fdsContentFragmentManager.A0U(A0D);
        return fdsContentFragmentManager;
    }

    public static /* synthetic */ void A01(FdsContentFragmentManager fdsContentFragmentManager, AnonymousClass6EB r6) {
        AnonymousClass01E r4 = r6.A00;
        String str = r6.A01;
        if (fdsContentFragmentManager.A03) {
            fdsContentFragmentManager.A19(r4, str);
        } else if (!fdsContentFragmentManager.A0C().isFinishing()) {
            fdsContentFragmentManager.A01.A02(fdsContentFragmentManager.A03().getString("fds_observer_id")).A01(new AnonymousClass6E3());
            fdsContentFragmentManager.A02 = new Runnable(r4, fdsContentFragmentManager, str) { // from class: X.6Jm
                public final /* synthetic */ AnonymousClass01E A00;
                public final /* synthetic */ FdsContentFragmentManager A01;
                public final /* synthetic */ String A02;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                    this.A02 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.A01.A19(this.A00, this.A02);
                }
            };
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.wa_fds_modal_container);
        this.A00 = (LinearLayout) A0F.findViewById(R.id.wa_fcs_modal_fragment_container);
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        try {
            this.A01.A02(this.A05.getString("fds_observer_id")).A03(this);
        } catch (Exception unused) {
        }
        this.A00 = null;
        this.A02 = null;
        super.A11();
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C50132Og A02 = this.A01.A02(this.A05.getString("fds_observer_id"));
        A02.A00(new AbstractC50172Ok() { // from class: X.6Du
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                FdsContentFragmentManager.A01(FdsContentFragmentManager.this, (AnonymousClass6EB) obj);
            }
        }, AnonymousClass6EB.class, this);
        A02.A00(new AbstractC50172Ok() { // from class: X.6Dt
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                int i;
                FdsContentFragmentManager fdsContentFragmentManager = FdsContentFragmentManager.this;
                String str = ((AnonymousClass6E6) obj).A00;
                AnonymousClass01F A0E = fdsContentFragmentManager.A0E();
                if (str != null) {
                    AnonymousClass01F A0E2 = fdsContentFragmentManager.A0E();
                    i = 0;
                    while (true) {
                        if (i < A0E2.A03()) {
                            if (((C004902f) ((AbstractC005002h) A0E2.A0E.get(i))).A0A.equals(str)) {
                                break;
                            }
                            i++;
                        } else {
                            i = -1;
                            break;
                        }
                    }
                } else {
                    int A03 = A0E.A03();
                    if (A03 != 1) {
                        i = A03 - 2;
                    } else {
                        return;
                    }
                }
                A0E.A0M(i);
            }
        }, AnonymousClass6E6.class, this);
        A02.A01(new AnonymousClass6E4());
    }

    public void A19(AnonymousClass01E r6, String str) {
        C004902f r4 = new C004902f(A0E());
        r4.A0F(str);
        r4.A02 = R.anim.enter_from_right;
        r4.A03 = R.anim.exit_to_left;
        r4.A05 = R.anim.enter_from_left;
        r4.A06 = R.anim.exit_to_right;
        r4.A0B(r6, str, this.A00.getId());
        r4.A01();
    }
}
