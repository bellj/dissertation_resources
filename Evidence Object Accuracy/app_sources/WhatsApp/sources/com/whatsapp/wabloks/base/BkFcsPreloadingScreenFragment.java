package com.whatsapp.wabloks.base;

import X.AbstractC50172Ok;
import X.AnonymousClass01E;
import X.AnonymousClass3JI;
import X.AnonymousClass6ED;
import X.C19600uN;
import X.C65963Lt;
import X.C91064Qh;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.wabloks.base.BkFcsPreloadingScreenFragment;
import java.util.ArrayList;
import java.util.Map;

/* loaded from: classes4.dex */
public class BkFcsPreloadingScreenFragment extends Hilt_BkFcsPreloadingScreenFragment {
    public C19600uN A00;
    public Map A01;
    public boolean A02 = false;
    public final String A03;
    public final String A04;

    public BkFcsPreloadingScreenFragment(String str, String str2) {
        this.A04 = str;
        this.A03 = str2;
    }

    public static BkFcsPreloadingScreenFragment A00(C65963Lt r1, String str, String str2, String str3, String str4) {
        BkFcsPreloadingScreenFragment bkFcsPreloadingScreenFragment = new BkFcsPreloadingScreenFragment(str, str4);
        bkFcsPreloadingScreenFragment.A1F(str);
        bkFcsPreloadingScreenFragment.A1D(str2);
        bkFcsPreloadingScreenFragment.A1C(r1);
        bkFcsPreloadingScreenFragment.A1E(str3);
        return bkFcsPreloadingScreenFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        this.A02 = false;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A13() {
        super.A13();
        this.A02 = true;
    }

    @Override // com.whatsapp.wabloks.base.BkScreenFragment, com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        C91064Qh A00;
        AnonymousClass3JI r0;
        if (!(((AnonymousClass01E) this).A05 == null || (A00 = this.A00.A00(this.A04)) == null || (r0 = A00.A01) == null)) {
            ((BkFragment) this).A02 = r0;
        }
        super.A16(bundle);
        ((BkScreenFragment) this).A04.A02(this.A03).A00(new AbstractC50172Ok() { // from class: X.6Ds
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                BkFcsPreloadingScreenFragment bkFcsPreloadingScreenFragment = BkFcsPreloadingScreenFragment.this;
                AnonymousClass6ED r5 = (AnonymousClass6ED) obj;
                if (bkFcsPreloadingScreenFragment.A02 && bkFcsPreloadingScreenFragment.A01 != null && TextUtils.equals(r5.A02, bkFcsPreloadingScreenFragment.A04)) {
                    ArrayList A0l = C12960it.A0l();
                    A0l.add(r5.A01);
                    String str = r5.A00;
                    if ("onLoadingFailure".equals(str)) {
                        A0l.add(C12970iu.A11());
                    }
                    bkFcsPreloadingScreenFragment.A1B((AbstractC28681Oo) bkFcsPreloadingScreenFragment.A01.get(str), A0l);
                }
            }
        }, AnonymousClass6ED.class, this);
    }
}
