package com.whatsapp.wabloks.base;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.C18000rk;
import X.C48912Ik;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import X.C72453ed;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaDialogFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.Hilt_PrivacyNoticeDialogFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_BkDialogFragment extends WaDialogFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1J();
        return this.A00;
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1J()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wabloks.base.Hilt_BkDialogFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1J();
        A1I();
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment
    public void A1I() {
        if (this instanceof Hilt_PrivacyNoticeDialogFragment) {
            Hilt_PrivacyNoticeDialogFragment hilt_PrivacyNoticeDialogFragment = (Hilt_PrivacyNoticeDialogFragment) this;
            if (!hilt_PrivacyNoticeDialogFragment.A02) {
                hilt_PrivacyNoticeDialogFragment.A02 = true;
                PrivacyNoticeDialogFragment privacyNoticeDialogFragment = (PrivacyNoticeDialogFragment) hilt_PrivacyNoticeDialogFragment;
                C51112Sw r2 = (C51112Sw) ((AbstractC51092Su) hilt_PrivacyNoticeDialogFragment.generatedComponent());
                AnonymousClass01J r1 = r2.A0Y;
                C72453ed.A18(r1, privacyNoticeDialogFragment);
                privacyNoticeDialogFragment.A00 = (C48912Ik) r2.A0V.A19.get();
                privacyNoticeDialogFragment.A05 = r1.A4d();
                privacyNoticeDialogFragment.A01 = r1.A2U();
                privacyNoticeDialogFragment.A03 = C18000rk.A00(r2.A00);
                privacyNoticeDialogFragment.A04 = C18000rk.A00(r1.ALs);
            }
        } else if (!this.A02) {
            this.A02 = true;
            C72453ed.A18(((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y, this);
        }
    }

    public final void A1J() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }
}
