package com.whatsapp.wabloks.base;

import X.AbstractC50192Om;
import X.AbstractC74513iC;
import X.ActivityC000800j;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass1Q8;
import X.C117295Zj;
import X.C12960it;
import X.C12970iu;
import X.C130355zH;
import X.C17120qI;
import X.C18580sg;
import X.C48912Ik;
import X.C64173En;
import X.C89264Jh;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes4.dex */
public class BkScreenFragment extends Hilt_BkScreenFragment implements AbstractC50192Om {
    public View A00;
    public FrameLayout A01;
    public C48912Ik A02;
    public C18580sg A03;
    public C17120qI A04;
    public Map A05;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.fragment_bloks);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        GenericBkLayoutViewModel genericBkLayoutViewModel = (GenericBkLayoutViewModel) ((BkFragment) this).A05;
        if (((AbstractC74513iC) genericBkLayoutViewModel).A01) {
            genericBkLayoutViewModel.A01.A04(A0G());
            this.A00 = null;
            return;
        }
        throw C12960it.A0U("BkLayoutViewModel must be initialized");
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        try {
            AnonymousClass1Q8.A00(A0C().getApplicationContext());
        } catch (IOException unused) {
        }
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A00 = AnonymousClass028.A0D(view, R.id.bloks_dialogfragment_progressbar);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(view, R.id.bloks_dialogfragment);
        this.A01 = frameLayout;
        C12970iu.A1G(frameLayout);
        View view2 = this.A00;
        if (view2 != null) {
            view2.setVisibility(0);
        }
        GenericBkLayoutViewModel genericBkLayoutViewModel = (GenericBkLayoutViewModel) ((BkFragment) this).A05;
        if (((AbstractC74513iC) genericBkLayoutViewModel).A01) {
            C117295Zj.A0s(A0G(), genericBkLayoutViewModel.A01, this, 163);
            super.A17(bundle, view);
            return;
        }
        throw C12960it.A0U("BkLayoutViewModel must be initialized");
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public int A18() {
        return R.id.bloks_container;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public Class A19() {
        return GenericBkLayoutViewModel.class;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public void A1A() {
        C12970iu.A1G(this.A00);
        FrameLayout frameLayout = this.A01;
        if (frameLayout != null) {
            frameLayout.setVisibility(0);
        }
        Bundle bundle = ((AnonymousClass01E) this).A05;
        if (bundle != null) {
            C130355zH.A01(this.A04, bundle.getString("qpl_params"), "openScreen", null);
        }
    }

    @Override // X.AbstractC50192Om
    public C18580sg AAw() {
        return this.A03;
    }

    @Override // X.AbstractC50192Om
    public C64173En AHe() {
        return this.A02.A00((ActivityC000800j) A0B(), A0F(), new C89264Jh(this.A05));
    }
}
