package com.whatsapp.wabloks.base;

import X.AbstractC001200n;
import X.AnonymousClass009;
import X.AnonymousClass01F;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.C004902f;
import X.C12960it;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.wabloks.ext.WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragment;

/* loaded from: classes4.dex */
public abstract class BkDialogFragment extends Hilt_BkDialogFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        BkFragment privacyNoticeFragment;
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.bk_dialog_fragment);
        View findViewById = A0F.findViewById(R.id.wa_bloks_dialog_fragment_container);
        AnonymousClass01F A0E = A0E();
        boolean z = this instanceof PrivacyNoticeDialogFragment;
        if (!z) {
            str = "FRAGMENT_CONTENT";
        } else {
            str = "TOSFragment";
        }
        if (A0E.A0A(str) == null) {
            C004902f r6 = new C004902f(A0E);
            int id = findViewById.getId();
            if (!z) {
                WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1 waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1 = (WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1) this;
                String str2 = waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1.A02;
                String str3 = waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1.A01;
                privacyNoticeFragment = new BkScreenFragment();
                privacyNoticeFragment.A1F(str2);
                privacyNoticeFragment.A1D(str3);
                privacyNoticeFragment.A1C(null);
                privacyNoticeFragment.A1E(null);
            } else {
                privacyNoticeFragment = new PrivacyNoticeFragment();
                privacyNoticeFragment.A0K.A00(new AnonymousClass054() { // from class: com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment$$ExternalSyntheticLambda0
                    @Override // X.AnonymousClass054
                    public final void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
                        PrivacyNoticeDialogFragment privacyNoticeDialogFragment = PrivacyNoticeDialogFragment.this;
                        if (r3.equals(AnonymousClass074.ON_DESTROY)) {
                            privacyNoticeDialogFragment.A1C();
                        }
                    }
                });
            }
            r6.A0A(privacyNoticeFragment, str, id);
            r6.A01();
        }
        return A0F;
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int i = configuration.orientation;
        if (i == 2) {
            AnonymousClass009.A05(this);
            AnonymousClass009.A05(((DialogFragment) this).A03.getWindow());
            ((DialogFragment) this).A03.getWindow().setLayout((int) (((double) A0C().getWindowManager().getDefaultDisplay().getWidth()) * 0.8d), -2);
        } else if (i == 1) {
            Dialog dialog = ((DialogFragment) this).A03;
            AnonymousClass009.A05(dialog);
            AnonymousClass009.A05(dialog.getWindow());
            ((DialogFragment) this).A03.getWindow().setLayout(-2, (int) (((double) A0C().getWindowManager().getDefaultDisplay().getHeight()) * 0.85d));
        }
    }
}
