package com.whatsapp.wabloks.base;

import X.AbstractC16850pr;
import X.AbstractC17450qp;
import X.AbstractC28681Oo;
import X.AbstractC50192Om;
import X.AbstractC74513iC;
import X.ActivityC000900k;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass02P;
import X.AnonymousClass3JI;
import X.AnonymousClass3JV;
import X.AnonymousClass5B5;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13000ix;
import X.C14220l3;
import X.C14230l4;
import X.C14250l6;
import X.C64173En;
import X.C64893Hi;
import X.C65093Ic;
import X.C65963Lt;
import X.C70983cC;
import X.C91064Qh;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import com.facebook.rendercore.RootHostView;
import com.whatsapp.R;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class BkFragment extends AnonymousClass01E {
    public RootHostView A00;
    public C64893Hi A01;
    public AnonymousClass3JI A02;
    public C64173En A03;
    public AbstractC50192Om A04;
    public AbstractC74513iC A05;
    public AnonymousClass01H A06;

    public void A1A() {
    }

    @Override // X.AnonymousClass01E
    public void A0U(Bundle bundle) {
        if (super.A05 == null) {
            super.A0U(bundle);
            return;
        }
        throw C12960it.A0U("arguments already set");
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        C64893Hi r1 = this.A01;
        if (r1 != null) {
            r1.A04();
            this.A01 = null;
        }
        this.A00 = null;
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        this.A04.AAw().A00(A0B(), (AbstractC17450qp) this.A06.get(), this.A03);
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        AnonymousClass01E r2 = this.A0D;
        ActivityC000900k A0B = A0B();
        if (r2 instanceof AbstractC50192Om) {
            this.A04 = (AbstractC50192Om) r2;
        } else if (A0B instanceof AbstractC50192Om) {
            this.A04 = (AbstractC50192Om) A0B;
        } else {
            A0B.finish();
        }
        C64173En AHe = this.A04.AHe();
        this.A03 = AHe;
        this.A04.AAw().A00(A0B(), (AbstractC17450qp) this.A06.get(), AHe);
        AbstractC74513iC r3 = (AbstractC74513iC) C13000ix.A02(this).A00(A19());
        this.A05 = r3;
        AnonymousClass3JI r22 = this.A02;
        if (r22 != null) {
            if (!r3.A01) {
                r3.A01 = true;
                AnonymousClass016 A0T = C12980iv.A0T();
                r3.A00 = A0T;
                AnonymousClass5B5 r32 = new AnonymousClass5B5(r22);
                C70983cC r23 = new C70983cC(A0T, null);
                C91064Qh r1 = new C91064Qh();
                r1.A01 = r32.A00;
                r1.A00 = 5;
                r23.AVH(r1);
                return;
            }
            throw C12960it.A0U("BkLayoutViewModel was already initialized");
        } else if (A03().containsKey("screen_name")) {
            String string = A03().getString("screen_params");
            String string2 = A03().getString("qpl_params");
            AbstractC74513iC r4 = this.A05;
            C64173En r33 = this.A03;
            String string3 = A03().getString("screen_name");
            if (string3 != null) {
                C65963Lt r5 = (C65963Lt) A03().getParcelable("screen_cache_config");
                if (!r4.A01) {
                    r4.A01 = true;
                    AnonymousClass02P r24 = new AnonymousClass02P();
                    AnonymousClass016 A0T2 = C12980iv.A0T();
                    r24.A0D(A0T2, new AnonymousClass02B(r24, r4) { // from class: X.4u8
                        public final /* synthetic */ AnonymousClass02P A00;
                        public final /* synthetic */ AbstractC74513iC A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AnonymousClass02B
                        public final void ANq(Object obj) {
                            AbstractC74513iC r34 = this.A01;
                            AnonymousClass02P r25 = this.A00;
                            C91064Qh r52 = (C91064Qh) obj;
                            if (r52.A00 != 5) {
                                r34.A04(r52);
                            } else {
                                r25.A0B(r52);
                            }
                        }
                    });
                    r4.A00 = r24;
                    AbstractC16850pr r42 = (AbstractC16850pr) r4.A02.get();
                    r42.A02(r5, new C70983cC(A0T2, r33), null, string3, string, string2, r42.A00.contains(string3));
                    return;
                }
                throw C12960it.A0U("BkLayoutViewModel was already initialized");
            }
            throw C12960it.A0U("BkFragment is missing screen name");
        } else if (bundle != null) {
            A0C().onBackPressed();
        } else {
            throw C12960it.A0U("data missing for init");
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A00 = (RootHostView) AnonymousClass028.A0D(view, A18());
        AbstractC74513iC r1 = this.A05;
        if (r1.A01) {
            C12960it.A19(A0G(), r1.A00, this, 96);
            return;
        }
        throw C12960it.A0U("BkLayoutViewModel must be initialized");
    }

    public int A18() {
        return R.id.bloks_container;
    }

    public Class A19() {
        return WaBkGalaxyLayoutViewModel.class;
    }

    public final void A1B(AbstractC28681Oo r7, List list) {
        C14220l3 r4;
        if (r7 != null && r7.AAN() != null) {
            C64173En r5 = this.A03;
            if (list == null) {
                r4 = C14220l3.A01;
            } else {
                r4 = new C14220l3(list);
            }
            C14250l6.A00(C14230l4.A00(AnonymousClass3JV.A01(C65093Ic.A00().A00, new SparseArray(), null, r5, null), null), r4, r7.AAN());
        }
    }

    public void A1C(C65963Lt r3) {
        if (super.A05 == null) {
            A0U(C12970iu.A0D());
        }
        A03().putParcelable("screen_cache_config", r3);
    }

    public void A1D(String str) {
        if (super.A05 == null) {
            A0U(C12970iu.A0D());
        }
        A03().putSerializable("screen_params", str);
    }

    public void A1E(String str) {
        if (super.A05 == null) {
            A0U(C12970iu.A0D());
        }
        A03().putSerializable("qpl_params", str);
    }

    public void A1F(String str) {
        if (super.A05 == null) {
            A0U(C12970iu.A0D());
        }
        A03().putString("screen_name", str);
    }
}
