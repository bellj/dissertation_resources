package com.whatsapp.wabloks.ui.PrivacyNotice;

import X.AnonymousClass01H;
import X.C13000ix;
import X.C18640sm;
import X.C27691It;
import X.C91064Qh;
import com.whatsapp.shops.ShopsBkLayoutViewModel;

/* loaded from: classes4.dex */
public class PrivacyNoticeFragmentViewModel extends ShopsBkLayoutViewModel {
    public final C27691It A00 = C13000ix.A03();

    public PrivacyNoticeFragmentViewModel(C18640sm r2, AnonymousClass01H r3) {
        super(r2, r3);
    }

    @Override // com.whatsapp.shops.ShopsBkLayoutViewModel, X.AbstractC74513iC
    public boolean A04(C91064Qh r3) {
        int i = r3.A00;
        if (i != 1 && i != 3 && i != 4 && i != 6 && i != 7) {
            return super.A04(r3);
        }
        this.A00.A0B(null);
        return false;
    }
}
