package com.whatsapp.wabloks.ui;

import X.AbstractC50172Ok;
import X.AbstractC50192Om;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass6E7;
import X.C117295Zj;
import X.C12990iw;
import X.C17120qI;
import X.C18580sg;
import X.C28391Mz;
import X.C48912Ik;
import X.C64173En;
import X.C89264Jh;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaFcsBottomsheetModalActivity;
import java.util.Map;

/* loaded from: classes4.dex */
public class WaFcsBottomsheetModalActivity extends ActivityC13810kN implements AbstractC50192Om {
    public C48912Ik A00;
    public C18580sg A01;
    public C17120qI A02;
    public FcsBottomsheetBaseContainer A03;
    public Map A04;
    public boolean A05;

    public WaFcsBottomsheetModalActivity() {
        this(0);
    }

    public WaFcsBottomsheetModalActivity(int i) {
        this.A05 = false;
        C117295Zj.A0p(this, 120);
    }

    public static Intent A02(Context context, String str, String str2, String str3, String str4, String str5) {
        return C12990iw.A0D(context, WaFcsBottomsheetModalActivity.class).putExtra("screen_params", str).putExtra("fds_observer_id", str2).putExtra("fds_on_back", str3).putExtra("fds_button_style", str4).putExtra("fds_state_name", str5);
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            this.A01 = A1M.A2U();
            this.A00 = (C48912Ik) A09.A19.get();
            this.A02 = (C17120qI) A1M.ALs.get();
            this.A04 = A1M.A4d();
        }
    }

    @Override // X.AbstractC50192Om
    public C18580sg AAw() {
        return this.A01;
    }

    @Override // X.AbstractC50192Om
    public C64173En AHe() {
        return this.A00.A00(this, A0V(), new C89264Jh(this.A04));
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (C28391Mz.A02()) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent));
        }
        this.A02.A02(getIntent().getStringExtra("fds_observer_id")).A00(new AbstractC50172Ok() { // from class: X.6E0
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                WaFcsBottomsheetModalActivity waFcsBottomsheetModalActivity = WaFcsBottomsheetModalActivity.this;
                if (!((AnonymousClass6E7) obj).A00.contains(waFcsBottomsheetModalActivity.getIntent().getStringExtra("fds_state_name"))) {
                    waFcsBottomsheetModalActivity.A03.A04 = Boolean.FALSE;
                    waFcsBottomsheetModalActivity.finish();
                }
            }
        }, AnonymousClass6E7.class, this);
        Intent intent = getIntent();
        FcsBottomsheetBaseContainer A00 = FcsBottomsheetBaseContainer.A00(intent.getStringExtra("fds_observer_id"), intent.getStringExtra("fds_on_back"), intent.getStringExtra("fds_button_style"), intent.getStringExtra("fds_state_name"));
        this.A03 = A00;
        AnonymousClass01F A0V = A0V();
        AnonymousClass009.A05(A0V);
        A00.A1F(A0V, "fds_bottom_sheet_container");
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
