package com.whatsapp.wabloks.ui;

import X.AbstractActivityC119315dW;
import X.AbstractC115815Ta;
import X.AbstractC119405dv;
import X.AbstractC119415dw;
import X.AbstractC36791ka;
import X.AbstractC50192Om;
import X.AbstractC50222Op;
import X.ActivityC001000l;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass05I;
import X.C12970iu;
import X.C12990iw;
import X.C17120qI;
import X.C18580sg;
import X.C18840t8;
import X.C48912Ik;
import X.C64173En;
import X.C65963Lt;
import X.C87934Dp;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.wabloks.base.BkFcsPreloadingScreenFragment;
import com.whatsapp.wabloks.base.BkScreenFragment;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;
import java.util.Map;
import java.util.Set;

/* loaded from: classes4.dex */
public class WaBloksActivity extends AbstractActivityC119315dW implements AbstractC50192Om, AbstractC50222Op {
    public AnonymousClass01E A00;
    public C48912Ik A01;
    public C64173En A02;
    public C18580sg A03;
    public C17120qI A04;
    public C18840t8 A05;
    public AbstractC119405dv A06;
    public AbstractC119415dw A07;
    public AnonymousClass01H A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public Map A0D;
    public Map A0E;
    public final Set A0F = C12970iu.A12();
    public final Set A0G = C12970iu.A12();

    public static Intent A09(Context context, String str, String str2) {
        return C12990iw.A0D(context, WaBloksActivity.class).putExtra("screen_name", str).putExtra("screen_params", str2).putExtra("screen_cache_config", (Parcelable) null);
    }

    public AnonymousClass01E A2e(Intent intent) {
        if (this instanceof WaFcsPreloadedBloksActivity) {
            return BkFcsPreloadingScreenFragment.A00((C65963Lt) intent.getParcelableExtra("screen_cache_config"), intent.getStringExtra("screen_name"), intent.getStringExtra("screen_params"), intent.getStringExtra("qpl_param_map"), intent.getStringExtra("fds_observer_id"));
        } else if (!(this instanceof WaFcsModalActivity)) {
            String stringExtra = intent.getStringExtra("screen_name");
            String stringExtra2 = intent.getStringExtra("screen_params");
            String stringExtra3 = intent.getStringExtra("qpl_param_map");
            BkScreenFragment bkScreenFragment = new BkScreenFragment();
            bkScreenFragment.A1F(stringExtra);
            bkScreenFragment.A1D(stringExtra2);
            bkScreenFragment.A1C((C65963Lt) intent.getParcelableExtra("screen_cache_config"));
            bkScreenFragment.A1E(stringExtra3);
            return bkScreenFragment;
        } else {
            FdsContentFragmentManager A00 = FdsContentFragmentManager.A00(intent.getStringExtra("fds_observer_id"));
            ((WaFcsModalActivity) this).A00 = A00;
            return A00;
        }
    }

    @Override // X.AbstractC50192Om
    public C18580sg AAw() {
        return this.A03;
    }

    @Override // X.AbstractC50192Om
    public C64173En AHe() {
        return this.A02;
    }

    @Override // X.AbstractC50222Op
    public void AfS(AbstractC115815Ta r3) {
        if (((ActivityC001000l) this).A06.A02.compareTo(AnonymousClass05I.CREATED) >= 0) {
            this.A06.A04(r3);
        }
    }

    @Override // X.AbstractC50222Op
    public void AfT(AbstractC115815Ta r3, boolean z) {
        AbstractC119415dw r0;
        if (((ActivityC001000l) this).A06.A02.compareTo(AnonymousClass05I.CREATED) >= 0 && (r0 = this.A07) != null) {
            r0.A00(r3);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AbstractC119405dv r1 = this.A06;
        if (r1.A02()) {
            r1.A01();
        } else if (A0V().A03() <= 1) {
            setResult(0, C87934Dp.A00(getIntent()));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00d5 A[LOOP:0: B:13:0x00cf->B:15:0x00d5, LOOP_END] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
        // Method dump skipped, instructions count: 297
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wabloks.ui.WaBloksActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        for (AbstractC36791ka r0 : this.A0G) {
            r0.AOk(menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        if (isFinishing() && getIntent().getStringExtra("wa_screen_options") != null) {
            this.A05.A04("wa_screen_options", getIntent().getStringExtra("wa_screen_options"));
        }
        try {
            synchronized (this.A04.A02(this.A0B)) {
                this.A04.A02(this.A0B).A03(this);
            }
        } catch (Exception unused) {
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        for (AbstractC36791ka r0 : this.A0G) {
            if (r0.ATH(menuItem)) {
                return true;
            }
        }
        return false;
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        for (AbstractC36791ka r0 : this.A0G) {
            r0.AUA(menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
