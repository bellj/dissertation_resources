package com.whatsapp.wabloks.ui.PrivacyNotice;

import X.AnonymousClass01H;
import X.C12960it;
import X.C17120qI;
import X.C50132Og;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.wabloks.base.BkFragment;

/* loaded from: classes4.dex */
public class PrivacyNoticeFragment extends Hilt_PrivacyNoticeFragment {
    public View A00;
    public FrameLayout A01;
    public C50132Og A02;
    public AnonymousClass01H A03;
    public AnonymousClass01H A04;

    public PrivacyNoticeFragment() {
        A1F("com.bloks.www.minishops.whatsapp.privacy_notice");
        A1D(null);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.fragment_bloks);
    }

    @Override // com.whatsapp.shops.ShopsBkFragment, com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        ((PrivacyNoticeFragmentViewModel) ((BkFragment) this).A05).A00.A04(A0G());
        this.A00 = null;
    }

    @Override // com.whatsapp.wabloks.ui.PrivacyNotice.Hilt_PrivacyNoticeFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        C50132Og A01 = ((C17120qI) this.A04.get()).A01(context);
        C50132Og r0 = this.A02;
        if (!(r0 == null || r0 == A01)) {
            r0.A03(this);
        }
        this.A02 = A01;
    }

    @Override // com.whatsapp.shops.ShopsBkFragment, com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        this.A00 = view.findViewById(R.id.bloks_dialogfragment_progressbar);
        this.A01 = (FrameLayout) view.findViewById(R.id.bloks_dialogfragment);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public int A18() {
        return R.id.bloks_container;
    }
}
