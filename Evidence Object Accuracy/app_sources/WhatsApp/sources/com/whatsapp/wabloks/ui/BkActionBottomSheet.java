package com.whatsapp.wabloks.ui;

import X.AbstractC115815Ta;
import X.AbstractC50172Ok;
import X.AnonymousClass01H;
import X.AnonymousClass6E5;
import X.C123975oF;
import X.C12960it;
import X.C12970iu;
import X.C17120qI;
import X.C18840t8;
import X.C48912Ik;
import X.C50132Og;
import X.C94824cb;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.BkActionBottomSheet;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public class BkActionBottomSheet extends Hilt_BkActionBottomSheet {
    public C48912Ik A00;
    public C50132Og A01;
    public C17120qI A02;
    public AnonymousClass01H A03;
    public Map A04;

    public static BkActionBottomSheet A00(C18840t8 r5, String str, String str2, List list) {
        Bundle A0D = C12970iu.A0D();
        String A0f = C12960it.A0f(C12960it.A0k("action_sheet_buttons"), list.hashCode());
        A0D.putString("action_sheet_buttons", A0f);
        A0D.putString("action_sheet_title", str);
        A0D.putString("action_sheet_message", str2);
        A0D.putBoolean("action_sheet_has_buttons", true);
        r5.A02(new C94824cb(list), "action_sheet_buttons", A0f);
        BkActionBottomSheet bkActionBottomSheet = new BkActionBottomSheet();
        bkActionBottomSheet.A0U(A0D);
        return bkActionBottomSheet;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        List<AbstractC115815Ta> list;
        C50132Og A01 = this.A02.A01(A01());
        this.A01 = A01;
        A01.A00(new AbstractC50172Ok() { // from class: X.6Dv
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                BkActionBottomSheet.this.A1B();
            }
        }, AnonymousClass6E5.class, this);
        Bundle A03 = A03();
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(R.layout.action_bottom_sheet, viewGroup, false);
        TextView A0J = C12960it.A0J(viewGroup2, R.id.bloks_action_sheet_title);
        TextView A0J2 = C12960it.A0J(viewGroup2, R.id.bloks_action_sheet_description);
        String string = A03.getString("action_sheet_title", "");
        String string2 = A03.getString("action_sheet_message", "");
        if (!TextUtils.isEmpty(string)) {
            A0J.setVisibility(0);
            A0J.setText(A03.getString("action_sheet_title"));
        }
        if (!TextUtils.isEmpty(string2)) {
            A0J2.setVisibility(0);
            A0J2.setText(A03.getString("action_sheet_message"));
        }
        if (A03.getBoolean("action_sheet_has_buttons")) {
            boolean z = A03.getBoolean("action_sheet_has_buttons", false);
            String string3 = A03.getString("action_sheet_buttons", "");
            if (!z || (list = (List) ((C18840t8) this.A03.get()).A01("action_sheet_buttons", string3)) == null) {
                A1B();
            } else {
                for (AbstractC115815Ta r3 : list) {
                    TextView textView = (TextView) layoutInflater.inflate(R.layout.action_sheet_button, viewGroup, false);
                    textView.setText(r3.AAL().A0I(36));
                    textView.setOnClickListener(new C123975oF(r3, this));
                    viewGroup2.addView(textView);
                }
            }
        }
        return viewGroup2;
    }
}
