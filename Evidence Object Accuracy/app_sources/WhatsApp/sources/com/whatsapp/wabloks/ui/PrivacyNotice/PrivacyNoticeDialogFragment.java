package com.whatsapp.wabloks.ui.PrivacyNotice;

import X.AbstractC50172Ok;
import X.AbstractC50192Om;
import X.ActivityC000800j;
import X.AnonymousClass01H;
import X.AnonymousClass6E5;
import X.C17120qI;
import X.C18580sg;
import X.C48912Ik;
import X.C50132Og;
import X.C50142Oh;
import X.C64173En;
import X.C89264Jh;
import android.content.Context;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;
import java.util.Map;

/* loaded from: classes4.dex */
public class PrivacyNoticeDialogFragment extends Hilt_PrivacyNoticeDialogFragment implements AbstractC50192Om {
    public C48912Ik A00;
    public C18580sg A01;
    public C50132Og A02;
    public AnonymousClass01H A03;
    public AnonymousClass01H A04;
    public Map A05;

    @Override // com.whatsapp.wabloks.ui.PrivacyNotice.Hilt_PrivacyNoticeDialogFragment, com.whatsapp.wabloks.base.Hilt_BkDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        C50132Og A01 = ((C17120qI) this.A04.get()).A01(context);
        C50132Og r0 = this.A02;
        if (!(r0 == null || r0 == A01)) {
            r0.A03(this);
        }
        this.A02 = A01;
        A01.A00(new AbstractC50172Ok() { // from class: X.6Dy
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                PrivacyNoticeDialogFragment.this.A1C();
            }
        }, AnonymousClass6E5.class, this);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1C() {
        this.A02.A01(new C50142Oh(3));
        super.A1C();
    }

    @Override // X.AbstractC50192Om
    public C18580sg AAw() {
        return this.A01;
    }

    @Override // X.AbstractC50192Om
    public C64173En AHe() {
        return this.A00.A00((ActivityC000800j) A0C(), A0F(), new C89264Jh(this.A05));
    }
}
