package com.whatsapp.wabloks.ui;

import X.AbstractC50172Ok;
import X.ActivityC13810kN;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass3EV;
import X.AnonymousClass4JG;
import X.AnonymousClass6E9;
import X.AnonymousClass6EA;
import X.AnonymousClass6EC;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C134476Et;
import X.C17120qI;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;
import com.whatsapp.wabloks.ui.FcsBottomsheetBaseContainer;

/* loaded from: classes4.dex */
public class FcsBottomsheetBaseContainer extends Hilt_FcsBottomsheetBaseContainer {
    public Toolbar A00;
    public AnonymousClass4JG A01;
    public C17120qI A02;
    public AnonymousClass3EV A03;
    public Boolean A04 = Boolean.TRUE;
    public String A05;
    public String A06;
    public String A07;
    public String A08;

    public static FcsBottomsheetBaseContainer A00(String str, String str2, String str3, String str4) {
        FcsBottomsheetBaseContainer fcsBottomsheetBaseContainer = new FcsBottomsheetBaseContainer();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("fds_observer_id", str);
        A0D.putString("fds_on_back", str2);
        A0D.putString("fds_button_style", str3);
        A0D.putString("fds_state_name", str4);
        fcsBottomsheetBaseContainer.A0U(A0D);
        return fcsBottomsheetBaseContainer;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putString("fds_state_name", this.A08);
        bundle.putString("fds_on_back", this.A06);
        bundle.putString("fds_button_style", this.A05);
        bundle.putString("fds_observer_id", this.A07);
        super.A0w(bundle);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        this.A08 = A03().getString("fds_state_name");
        this.A06 = A03().getString("fds_on_back");
        this.A07 = A03().getString("fds_observer_id");
        this.A05 = A03().getString("fds_button_style");
        this.A02.A02(this.A07).A00(new AbstractC50172Ok() { // from class: X.6Dw
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                FcsBottomsheetBaseContainer fcsBottomsheetBaseContainer = FcsBottomsheetBaseContainer.this;
                AnonymousClass6EC r7 = (AnonymousClass6EC) obj;
                String str = r7.A01;
                String str2 = r7.A00;
                fcsBottomsheetBaseContainer.A06 = str;
                fcsBottomsheetBaseContainer.A05 = str2;
                fcsBottomsheetBaseContainer.A00.setVisibility(0);
                fcsBottomsheetBaseContainer.A03.A01(fcsBottomsheetBaseContainer.A00, new C134476Et(fcsBottomsheetBaseContainer), fcsBottomsheetBaseContainer.A08, fcsBottomsheetBaseContainer.A05);
            }
        }, AnonymousClass6EC.class, this);
        this.A02.A01(A0p()).A00(new AbstractC50172Ok() { // from class: X.6Dx
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                FcsBottomsheetBaseContainer fcsBottomsheetBaseContainer = FcsBottomsheetBaseContainer.this;
                fcsBottomsheetBaseContainer.A00.setTitle(((AnonymousClass6E9) obj).A00);
            }
        }, AnonymousClass6E9.class, this);
        this.A03 = new AnonymousClass3EV((ActivityC13810kN) A0B(), C12960it.A0R(this.A01.A00.A04));
        View inflate = layoutInflater.inflate(R.layout.wa_fcs_bottom_sheet, viewGroup, false);
        Toolbar toolbar = (Toolbar) AnonymousClass028.A0D(inflate, R.id.bk_bottom_sheet_toolbar);
        this.A00 = toolbar;
        toolbar.setVisibility(0);
        this.A03.A01(this.A00, new C134476Et(this), this.A08, this.A05);
        View A0D = AnonymousClass028.A0D(inflate, R.id.wa_fcs_bottom_sheet_fragment_container);
        C004902f r3 = new C004902f(A0E());
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2 != null) {
            r3.A0A(FdsContentFragmentManager.A00(bundle2.getString("fds_observer_id")), "fds_content_manager", A0D.getId());
            r3.A01();
        }
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, 2131952423);
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment
    public int A18() {
        return R.style.RoundedFcsBottomSheetDialogTheme;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Bundle bundle = ((AnonymousClass01E) this).A05;
        if (bundle != null && this.A04.booleanValue()) {
            this.A02.A02(bundle.getString("fds_observer_id")).A01(new AnonymousClass6EA(null, false));
        }
        super.onDismiss(dialogInterface);
    }
}
