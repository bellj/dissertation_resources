package com.whatsapp.wabloks.ui;

import X.C12990iw;
import X.C130355zH;
import X.C65963Lt;
import android.content.Context;
import android.content.Intent;

/* loaded from: classes4.dex */
public class WaFcsPreloadedBloksActivity extends WaBloksActivity {
    public static Intent A03(Context context, C65963Lt r3, Integer num, String str, String str2, String str3, String str4, String str5, String str6) {
        return C12990iw.A0D(context, WaFcsPreloadedBloksActivity.class).putExtra("screen_name", str).putExtra("screen_params", str2).putExtra("fds_on_back", str4).putExtra("fds_button_style", str5).putExtra("fds_state_name", str6).putExtra("fds_observer_id", str3).putExtra("qpl_param_map", C130355zH.A00(num)).putExtra("screen_cache_config", r3);
    }
}
