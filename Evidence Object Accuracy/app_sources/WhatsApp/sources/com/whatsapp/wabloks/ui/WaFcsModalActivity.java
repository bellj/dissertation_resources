package com.whatsapp.wabloks.ui;

import X.AbstractActivityC119315dW;
import X.AbstractC136146Le;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C12990iw;
import X.C130355zH;
import X.C65963Lt;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;

/* loaded from: classes4.dex */
public class WaFcsModalActivity extends WaBloksActivity implements AbstractC136146Le {
    public FdsContentFragmentManager A00;
    public boolean A01;

    public WaFcsModalActivity() {
        this(0);
    }

    public WaFcsModalActivity(int i) {
        this.A01 = false;
        C117295Zj.A0p(this, 121);
    }

    public static Intent A03(Context context, C65963Lt r2, Integer num, String str, String str2, String str3, String str4, String str5, String str6) {
        return C12990iw.A0D(context, WaFcsModalActivity.class).putExtra("screen_name", str).putExtra("screen_params", str2).putExtra("screen_cache_config", r2).putExtra("fds_observer_id", str3).putExtra("fds_on_back", str4).putExtra("fds_button_style", str5).putExtra("fds_state_name", str6).putExtra("qpl_param_map", C130355zH.A00(num));
    }

    @Override // X.ActivityC000900k
    public void A0Y() {
        FdsContentFragmentManager fdsContentFragmentManager = this.A00;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = true;
            Runnable runnable = fdsContentFragmentManager.A02;
            if (runnable != null) {
                runnable.run();
                fdsContentFragmentManager.A02 = null;
            }
        }
        super.A0Y();
    }

    @Override // X.AbstractActivityC119315dW, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A09 = C117295Zj.A09(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A09, this);
            ActivityC13810kN.A10(A1M, this);
            AbstractActivityC119315dW.A02(A09, A1M, ActivityC13790kL.A0S(A09, A1M, this, ActivityC13790kL.A0Y(A1M, this)), this);
        }
    }

    @Override // com.whatsapp.wabloks.ui.WaBloksActivity, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        try {
            ((WaBloksActivity) this).A04.A02(((WaBloksActivity) this).A0B).A03(this);
        } catch (Exception unused) {
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        FdsContentFragmentManager fdsContentFragmentManager = this.A00;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = false;
        }
        super.onPause();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        FdsContentFragmentManager fdsContentFragmentManager = this.A00;
        if (fdsContentFragmentManager != null) {
            fdsContentFragmentManager.A03 = false;
        }
        super.onSaveInstanceState(bundle);
    }
}
