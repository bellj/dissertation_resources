package com.whatsapp.wabloks.ui.bottomsheet;

import X.AbstractC28681Oo;
import X.AnonymousClass009;
import X.AnonymousClass01H;
import X.AnonymousClass028;
import X.AnonymousClass3JI;
import X.AnonymousClass67K;
import X.C117305Zk;
import X.C125555rR;
import X.C12960it;
import X.C12970iu;
import X.C18840t8;
import X.C94824cb;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.base.BkFragment;
import com.whatsapp.wabloks.base.GenericBkLayoutViewModel;
import com.whatsapp.wabloks.ui.bottomsheet.BkBottomSheetContentFragment;

/* loaded from: classes4.dex */
public class BkBottomSheetContentFragment extends Hilt_BkBottomSheetContentFragment {
    public View A00;
    public Toolbar A01;
    public C125555rR A02;
    public AbstractC28681Oo A03;
    public AnonymousClass01H A04;
    public String A05;
    public String A06;
    public boolean A07;

    public static BkBottomSheetContentFragment A00(C125555rR r4, C18840t8 r5, String str, boolean z) {
        Bundle A0D = C12970iu.A0D();
        String A0f = C12960it.A0f(C12960it.A0k("bk_bottom_sheet_content_fragment"), r4.hashCode());
        A0D.putString("bottom_sheet_fragment_tag", str);
        A0D.putBoolean("bottom_sheet_back_stack", z);
        A0D.putString("bk_bottom_sheet_content_fragment", A0f);
        r5.A02(new C94824cb(r4), "bk_bottom_sheet_content_fragment", A0f);
        BkBottomSheetContentFragment bkBottomSheetContentFragment = new BkBottomSheetContentFragment();
        bkBottomSheetContentFragment.A0U(A0D);
        ((BkFragment) bkBottomSheetContentFragment).A02 = (AnonymousClass3JI) r4.A00.A02.get(35);
        return bkBottomSheetContentFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.bk_bottom_sheet_fragment);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        AbstractC28681Oo r1 = this.A03;
        if (!(r1 == null || this.A02 == null)) {
            try {
                A1B(r1, null);
            } catch (NullPointerException e) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(getClass().getName());
                Log.e(C12960it.A0d("Failed to execute onContentDismiss Expression: ", A0h), e);
            }
        }
        if (this.A0g && this.A02 != null) {
            ((C18840t8) this.A04.get()).A04("bk_bottom_sheet_content_fragment", C12960it.A0f(C12960it.A0k("bk_bottom_sheet_content_fragment"), this.A02.hashCode()));
        }
        super.A11();
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A12() {
        this.A01 = null;
        this.A00 = null;
        super.A12();
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        C125555rR r0 = (C125555rR) ((C18840t8) this.A04.get()).A01("bk_bottom_sheet_content_fragment", A03().getString("bk_bottom_sheet_content_fragment", ""));
        this.A02 = r0;
        if (r0 != null) {
            ((BkFragment) this).A02 = (AnonymousClass3JI) r0.A00.A02.get(35);
        }
        super.A16(bundle);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass67K r0;
        Bundle A03 = A03();
        this.A00 = view.findViewById(R.id.bloks_dialogfragment_progressbar);
        this.A01 = (Toolbar) AnonymousClass028.A0D(view, R.id.bk_bottom_sheet_toolbar);
        this.A05 = A03.getString("bottom_sheet_fragment_tag");
        this.A07 = A03.getBoolean("bottom_sheet_back_stack");
        C125555rR r02 = this.A02;
        if (r02 != null) {
            String A0I = r02.A00.A0I(36);
            this.A06 = A0I;
            if (!TextUtils.isEmpty(A0I)) {
                this.A01.setVisibility(0);
                this.A01.setTitle(this.A06);
            }
            if (this.A02.A00.A0G(38) == null) {
                r0 = null;
            } else {
                r0 = new AbstractC28681Oo() { // from class: X.67K
                    @Override // X.AbstractC28681Oo
                    public final AbstractC14200l1 AAN() {
                        return BkBottomSheetContentFragment.this.A02.A00.A0G(38);
                    }
                };
            }
            this.A03 = r0;
            boolean z = this.A07;
            Toolbar toolbar = this.A01;
            if (z) {
                toolbar.setVisibility(0);
                this.A01.getNavigationIcon().setVisible(true, true);
                this.A01.setNavigationOnClickListener(C117305Zk.A0A(this, 200));
            } else {
                Drawable navigationIcon = toolbar.getNavigationIcon();
                AnonymousClass009.A05(navigationIcon);
                navigationIcon.setVisible(false, false);
            }
        }
        super.A17(bundle, view);
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public int A18() {
        return R.id.bloks_container;
    }

    @Override // com.whatsapp.wabloks.base.BkFragment
    public Class A19() {
        return GenericBkLayoutViewModel.class;
    }
}
