package com.whatsapp.wabloks.ui.bottomsheet;

import X.AbstractC28681Oo;
import X.AbstractC35731ia;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01T;
import X.AnonymousClass1AI;
import X.C004902f;
import X.C117305Zk;
import X.C12960it;
import X.C130435zP;
import X.C130735zt;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* loaded from: classes4.dex */
public class BkBottomSheetContainerFragment extends Hilt_BkBottomSheetContainerFragment {
    public LinearLayout A00;
    public AnonymousClass01T A01;
    public AbstractC28681Oo A02;
    public AnonymousClass01H A03;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        C004902f r0 = new C004902f(A0C().A0V());
        r0.A05(this);
        r0.A02();
        super.A0w(bundle);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Object obj;
        Object obj2;
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.wa_bloks_bottom_sheet);
        this.A00 = C117305Zk.A07(A0F, R.id.wa_bloks_bottom_sheet_fragment_container);
        AnonymousClass01T r0 = this.A01;
        if (!(r0 == null || (obj = r0.A00) == null || (obj2 = r0.A01) == null)) {
            C004902f r1 = new C004902f(A0E());
            r1.A0B((AnonymousClass01E) obj, (String) obj2, this.A00.getId());
            r1.A01();
        }
        return A0F;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        if (this.A02 != null) {
            WaBloksActivity waBloksActivity = (WaBloksActivity) A0C();
            AbstractC28681Oo r1 = this.A02;
            if (!(r1 == null || r1.AAN() == null)) {
                AnonymousClass1AI.A08(waBloksActivity.A02, r1);
            }
        }
        ((C130735zt) this.A03.get()).A00(AbstractC35731ia.A00(A0p()));
        C130435zP.A01.pop();
        super.onDismiss(dialogInterface);
    }
}
