package com.whatsapp.wabloks.ui.bottomsheet;

import X.AbstractC51092Su;
import X.C117295Zj;
import X.C18000rk;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.RoundedBottomSheetDialogFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_BkBottomSheetContainerFragment extends RoundedBottomSheetDialogFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    private void A00() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A00();
        return this.A00;
    }

    @Override // com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A00()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wabloks.ui.bottomsheet.Hilt_BkBottomSheetContainerFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A00();
        A1I();
    }

    @Override // com.whatsapp.Hilt_RoundedBottomSheetDialogFragment
    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            BkBottomSheetContainerFragment bkBottomSheetContainerFragment = (BkBottomSheetContainerFragment) this;
            bkBottomSheetContainerFragment.A03 = C18000rk.A00(C117295Zj.A0A((C51112Sw) ((AbstractC51092Su) generatedComponent()), bkBottomSheetContainerFragment).A1S);
        }
    }
}
