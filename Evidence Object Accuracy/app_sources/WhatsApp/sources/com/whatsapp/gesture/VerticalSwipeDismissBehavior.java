package com.whatsapp.gesture;

import X.AbstractC009304r;
import X.AnonymousClass028;
import X.AnonymousClass04X;
import X.AnonymousClass5X1;
import X.C06260Su;
import X.C12970iu;
import X.C12990iw;
import X.C53722ev;
import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.facebook.redex.RunnableBRunnable0Shape1S0210000_I1;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* loaded from: classes2.dex */
public class VerticalSwipeDismissBehavior extends AbstractC009304r {
    public float A00;
    public float A01 = 0.0f;
    public int A02;
    public VelocityTracker A03;
    public C06260Su A04;
    public AnonymousClass5X1 A05;
    public WeakReference A06;
    public boolean A07 = true;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final AnonymousClass04X A0C = new C53722ev(this);

    public VerticalSwipeDismissBehavior(Context context) {
        this.A00 = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    @Override // X.AbstractC009304r
    public void A0A(View view, View view2, CoordinatorLayout coordinatorLayout, int i) {
        float yVelocity;
        WeakReference weakReference = this.A06;
        if (weakReference != null && view2 == weakReference.get() && this.A09) {
            VelocityTracker velocityTracker = this.A03;
            if (velocityTracker == null) {
                yVelocity = 0.0f;
            } else {
                velocityTracker.computeCurrentVelocity(1000, this.A00);
                yVelocity = this.A03.getYVelocity(this.A02);
            }
            if (A0J(view, yVelocity, 0)) {
                this.A05.APG(view);
            } else if (this.A04.A0H(view, view.getLeft(), 0)) {
                view.postOnAnimation(new RunnableBRunnable0Shape1S0210000_I1(this, view, 1, false));
            }
            this.A09 = false;
        }
    }

    @Override // X.AbstractC009304r
    public void A0B(View view, View view2, CoordinatorLayout coordinatorLayout, int[] iArr, int i, int i2, int i3) {
        int max;
        if (view2 == this.A06.get()) {
            int top = view.getTop();
            if (i2 > 0) {
                if (view2.canScrollVertically(1)) {
                    if (top > 0) {
                        max = Math.min(i2, top);
                        iArr[1] = max;
                        AnonymousClass028.A0Y(view, -max);
                    }
                }
                iArr[1] = i2;
                AnonymousClass028.A0Y(view, -i2);
                this.A09 = true;
            } else if (i2 < 0) {
                if (view2.canScrollVertically(-1)) {
                    if (top < 0) {
                        max = Math.max(i2, top);
                        iArr[1] = max;
                        AnonymousClass028.A0Y(view, -max);
                    }
                }
                iArr[1] = i2;
                AnonymousClass028.A0Y(view, -i2);
                this.A09 = true;
            }
            if (this.A09) {
                this.A05.AWA(view, Math.min(1.0f, (((float) Math.abs(view.getTop())) * 1.0f) / C12990iw.A03(view)));
            }
        }
    }

    @Override // X.AbstractC009304r
    public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        View view2;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.A02 = -1;
            VelocityTracker velocityTracker = this.A03;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.A03 = null;
            }
        }
        VelocityTracker velocityTracker2 = this.A03;
        if (velocityTracker2 == null) {
            velocityTracker2 = VelocityTracker.obtain();
            this.A03 = velocityTracker2;
        }
        velocityTracker2.addMovement(motionEvent);
        boolean z = this.A08;
        if (actionMasked == 0) {
            z = coordinatorLayout.A0I(view, (int) motionEvent.getX(), (int) motionEvent.getY());
            this.A08 = z;
            WeakReference weakReference = this.A06;
            if (!(weakReference == null || (view2 = (View) weakReference.get()) == null || !coordinatorLayout.A0I(view2, (int) motionEvent.getX(), (int) motionEvent.getY()))) {
                this.A02 = motionEvent.getPointerId(motionEvent.getActionIndex());
                this.A0B = true;
            }
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.A0B = false;
            this.A08 = false;
            this.A02 = -1;
        }
        if (!z) {
            return false;
        }
        C06260Su r3 = this.A04;
        if (r3 == null) {
            if (this.A0A) {
                float f = this.A01;
                r3 = new C06260Su(coordinatorLayout.getContext(), coordinatorLayout, this.A0C);
                r3.A06 = (int) (((float) r3.A06) * (1.0f / f));
            } else {
                r3 = new C06260Su(coordinatorLayout.getContext(), coordinatorLayout, this.A0C);
            }
            this.A04 = r3;
        }
        return r3.A0E(motionEvent);
    }

    @Override // X.AbstractC009304r
    public boolean A0D(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        if (motionEvent.getActionMasked() == 0) {
            this.A02 = -1;
            VelocityTracker velocityTracker = this.A03;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.A03 = null;
            }
        }
        VelocityTracker velocityTracker2 = this.A03;
        if (velocityTracker2 == null) {
            velocityTracker2 = VelocityTracker.obtain();
            this.A03 = velocityTracker2;
        }
        velocityTracker2.addMovement(motionEvent);
        C06260Su r0 = this.A04;
        if (r0 == null) {
            return false;
        }
        try {
            r0.A07(motionEvent);
            return true;
        } catch (IllegalArgumentException e) {
            Log.e("VerticalSwipeDismissBehavior/onTouchEvent", e);
            return false;
        }
    }

    @Override // X.AbstractC009304r
    public boolean A0E(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i, int i2) {
        this.A09 = false;
        return (i & 2) != 0;
    }

    @Override // X.AbstractC009304r
    public boolean A0F(View view, View view2, CoordinatorLayout coordinatorLayout, float f, float f2) {
        this.A06.get();
        return false;
    }

    @Override // X.AbstractC009304r
    public boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
        int top = view.getTop();
        coordinatorLayout.A0D(view, i);
        if (this.A09) {
            AnonymousClass028.A0Y(view, top - view.getTop());
        }
        this.A06 = C12970iu.A10(A0I(view));
        return true;
    }

    public final View A0I(View view) {
        if (AnonymousClass028.A0s(view)) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View A0I = A0I(viewGroup.getChildAt(i));
                if (A0I != null) {
                    return A0I;
                }
            }
        }
        return null;
    }

    public final boolean A0J(View view, float f, int i) {
        if (Math.abs(f) > ((float) (view.getHeight() << 1)) || Math.abs(view.getTop() - i) >= Math.round(C12990iw.A03(view) * 0.2f)) {
            return true;
        }
        return false;
    }
}
