package com.whatsapp.insufficientstoragespace;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass2FL;
import X.AnonymousClass30R;
import X.AnonymousClass3FC;
import X.AnonymousClass3GM;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C16120oU;
import X.C44891zj;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S1100000_I1;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Locale;

/* loaded from: classes2.dex */
public class InsufficientStorageSpaceActivity extends ActivityC13790kL {
    public long A00;
    public ScrollView A01;
    public C16120oU A02;
    public AnonymousClass3FC A03;
    public boolean A04;

    @Override // X.ActivityC13790kL
    public void A2W() {
    }

    public InsufficientStorageSpaceActivity() {
        this(0);
    }

    public InsufficientStorageSpaceActivity(int i) {
        this.A04 = false;
        ActivityC13830kP.A1P(this, 81);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = C12970iu.A0b(A1M);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass12P.A03(this);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A03.A00();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        boolean z;
        int i;
        int i2;
        String A0o;
        View.OnClickListener viewOnClickCListenerShape9S0100000_I1_3;
        super.onCreate(bundle);
        String A00 = AnonymousClass3GM.A00(this.A02, 6);
        setContentView(R.layout.activity_insufficient_storage_space);
        this.A01 = (ScrollView) findViewById(R.id.insufficient_storage_scroll_view);
        TextView A0N = C12990iw.A0N(this, R.id.btn_storage_settings);
        TextView A0N2 = C12990iw.A0N(this, R.id.insufficient_storage_title_textview);
        TextView A0N3 = C12990iw.A0N(this, R.id.insufficient_storage_description_textview);
        long longExtra = getIntent().getLongExtra("spaceNeededInBytes", -1);
        this.A00 = longExtra;
        long A02 = (longExtra - ((ActivityC13790kL) this).A06.A02()) + SearchActionVerificationClientService.MS_TO_NS;
        if (getIntent() == null || !getIntent().getBooleanExtra("allowSkipKey", false)) {
            z = false;
            i = R.string.insufficient_internal_storage_settings_button;
            i2 = R.string.insufficient_internal_storage_space_title_enhanced;
            A0o = C12990iw.A0o(getResources(), C44891zj.A03(((ActivityC13830kP) this).A01, A02), new Object[1], 0, R.string.insufficient_internal_storage_space_description_enhanced);
        } else {
            z = true;
            i = R.string.insufficient_internal_storage_settings_button_almost_full_enhanced;
            i2 = R.string.insufficient_internal_storage_space_title_almost_full_enhanced;
            A0o = getResources().getString(R.string.insufficient_internal_storage_space_description_almost_full_enhanced);
        }
        A0N2.setText(i2);
        A0N3.setText(A0o);
        A0N.setText(i);
        if (z) {
            viewOnClickCListenerShape9S0100000_I1_3 = new ViewOnClickCListenerShape1S1100000_I1(3, A00, this);
        } else {
            viewOnClickCListenerShape9S0100000_I1_3 = new ViewOnClickCListenerShape9S0100000_I1_3(this, 31);
        }
        A0N.setOnClickListener(viewOnClickCListenerShape9S0100000_I1_3);
        if (z) {
            View findViewById = findViewById(R.id.btn_skip_storage_settings);
            findViewById.setVisibility(0);
            C12960it.A0x(findViewById, this, 32);
        }
        AnonymousClass3FC r0 = new AnonymousClass3FC(this.A01, findViewById(R.id.bottom_button_container), getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation));
        this.A03 = r0;
        r0.A00();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        long A02 = ((ActivityC13790kL) this).A06.A02();
        Locale locale = Locale.ENGLISH;
        Object[] A1a = C12980iv.A1a();
        boolean z = false;
        A1a[0] = Long.valueOf(A02);
        A1a[1] = Long.valueOf(this.A00);
        C12990iw.A1R("insufficient-storage-activity/internal-storage available: %,d required: %,d", locale, A1a);
        if (A02 > this.A00) {
            Log.i("insufficient-storage-activity/space-available/finishing-the-activity");
            long j = this.A00;
            if (j > 0) {
                AnonymousClass30R r1 = new AnonymousClass30R();
                r1.A02 = Long.valueOf(j);
                if (findViewById(R.id.btn_skip_storage_settings).getVisibility() == 0) {
                    z = true;
                }
                r1.A00 = Boolean.valueOf(z);
                r1.A01 = 1;
                this.A02.A06(r1);
            }
            finish();
        }
    }
}
