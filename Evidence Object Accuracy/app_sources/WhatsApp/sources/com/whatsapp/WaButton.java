package com.whatsapp;

import X.AbstractC629139c;
import X.AnonymousClass2GZ;
import X.C12960it;
import X.C12970iu;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaButton extends AbstractC629139c {
    public WaButton(Context context) {
        super(context);
    }

    public WaButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A05(context, attributeSet);
    }

    public WaButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A05(context, attributeSet);
    }

    public final void A05(Context context, AttributeSet attributeSet) {
        if (!this.A0E && attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0P);
            int resourceId = obtainStyledAttributes.getResourceId(3, 0);
            if (resourceId != 0) {
                C12960it.A0r(context, this, resourceId);
            }
            int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
            if (resourceId2 != 0) {
                setHint(context.getString(resourceId2));
            }
            int resourceId3 = obtainStyledAttributes.getResourceId(2, 0);
            if (resourceId3 != 0) {
                setImeActionLabel(context.getString(resourceId3), getImeActionId());
            }
            int resourceId4 = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId4 != 0) {
                C12970iu.A19(context, this, resourceId4);
            }
            obtainStyledAttributes.recycle();
        }
    }
}
