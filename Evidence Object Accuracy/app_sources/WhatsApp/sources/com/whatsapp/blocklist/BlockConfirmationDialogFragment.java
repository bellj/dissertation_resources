package com.whatsapp.blocklist;

import X.AbstractC14030kj;
import X.AbstractC14440lR;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass028;
import X.AnonymousClass04S;
import X.AnonymousClass3L0;
import X.C004802e;
import X.C14900mE;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15610nY;
import X.C16120oU;
import X.C16170oZ;
import X.C238013b;
import X.C254119h;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class BlockConfirmationDialogFragment extends Hilt_BlockConfirmationDialogFragment {
    public C14900mE A00;
    public C15450nH A01;
    public C16170oZ A02;
    public AbstractC14030kj A03;
    public C238013b A04;
    public C15550nR A05;
    public C15610nY A06;
    public C254119h A07;
    public C16120oU A08;
    public AbstractC14440lR A09;

    public static BlockConfirmationDialogFragment A00(UserJid userJid, String str, boolean z, boolean z2, boolean z3) {
        BlockConfirmationDialogFragment blockConfirmationDialogFragment = new BlockConfirmationDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", userJid.getRawString());
        bundle.putString("entryPoint", str);
        bundle.putBoolean("fromSpamPanel", z);
        bundle.putBoolean("showSuccessToast", z2);
        bundle.putBoolean("showReportAndBlock", z3);
        blockConfirmationDialogFragment.A0U(bundle);
        return blockConfirmationDialogFragment;
    }

    @Override // com.whatsapp.blocklist.Hilt_BlockConfirmationDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC14030kj) {
            this.A03 = (AbstractC14030kj) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        CheckBox checkBox;
        Bundle A03 = A03();
        ActivityC13810kN r9 = (ActivityC13810kN) A0B();
        AnonymousClass009.A05(r9);
        AnonymousClass009.A05(A03);
        String string = A03.getString("jid", null);
        String string2 = A03.getString("entryPoint", null);
        boolean z = A03.getBoolean("fromSpamPanel", false);
        boolean z2 = A03.getBoolean("showSuccessToast", false);
        boolean z3 = A03.getBoolean("showReportAndBlock", false);
        UserJid nullable = UserJid.getNullable(string);
        AnonymousClass009.A05(nullable);
        C15370n3 A0B = this.A05.A0B(nullable);
        C004802e r3 = new C004802e(r9);
        if (z3) {
            View inflate = LayoutInflater.from(A0p()).inflate(R.layout.dialog_with_checkbox, (ViewGroup) null, false);
            checkBox = (CheckBox) AnonymousClass028.A0D(inflate, R.id.checkbox);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.dialog_message)).setText(R.string.block_spam_dialog_message);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.checkbox_header)).setText(R.string.report_contact);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.checkbox_message)).setText(R.string.reporting_flow_general_group);
            AnonymousClass028.A0D(inflate, R.id.checkbox_container).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(checkBox, 44));
            r3.setView(inflate);
        } else {
            checkBox = null;
        }
        AnonymousClass3L0 r7 = new DialogInterface.OnClickListener(checkBox, r9, this, A0B, string2, z, z2) { // from class: X.3L0
            public final /* synthetic */ CheckBox A00;
            public final /* synthetic */ ActivityC13810kN A01;
            public final /* synthetic */ BlockConfirmationDialogFragment A02;
            public final /* synthetic */ C15370n3 A03;
            public final /* synthetic */ String A04;
            public final /* synthetic */ boolean A05;
            public final /* synthetic */ boolean A06;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
                this.A03 = r4;
                this.A04 = r5;
                this.A05 = r6;
                this.A06 = r7;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BlockConfirmationDialogFragment blockConfirmationDialogFragment = this.A02;
                CheckBox checkBox2 = this.A00;
                ActivityC13810kN r4 = this.A01;
                C15370n3 r92 = this.A03;
                String str = this.A04;
                boolean z4 = this.A05;
                boolean z5 = this.A06;
                if (checkBox2 == null || !checkBox2.isChecked()) {
                    if (z4) {
                        C12990iw.A1N(new C627138h(r4, r4, blockConfirmationDialogFragment.A02, null, blockConfirmationDialogFragment.A07, r92, null, null, str, true, false), blockConfirmationDialogFragment.A09);
                        return;
                    }
                    blockConfirmationDialogFragment.A04.A08(r4, null, r92, null, null, str, true, z5);
                } else if (blockConfirmationDialogFragment.A07.A02(r4)) {
                    blockConfirmationDialogFragment.A00.A0C(null);
                    AbstractC14030kj r0 = blockConfirmationDialogFragment.A03;
                    if (r0 != null) {
                        C15360n1 r02 = ((Conversation) r0).A20;
                        r02.A03 = 0;
                        r02.A01 = 0;
                        r02.A02 = 0;
                        r02.A05();
                    }
                    blockConfirmationDialogFragment.A09.Ab2(new RunnableBRunnable0Shape0S1300000_I0(blockConfirmationDialogFragment, r4, r92, str, 1));
                }
            }
        };
        r3.setTitle(A0J(R.string.block_spam_dialog_header, this.A06.A04(A0B)));
        r3.setPositiveButton(R.string.block, r7);
        r3.setNegativeButton(R.string.cancel, null);
        AnonymousClass04S create = r3.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
