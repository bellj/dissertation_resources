package com.whatsapp.blocklist;

import X.AbstractC33331dp;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass130;
import X.AnonymousClass18N;
import X.AnonymousClass1J1;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass41P;
import X.AnonymousClass424;
import X.AnonymousClass44T;
import X.AnonymousClass540;
import X.AnonymousClass541;
import X.AnonymousClass542;
import X.AnonymousClass5TX;
import X.AnonymousClass5US;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C17070qD;
import X.C18610sj;
import X.C18640sm;
import X.C21270x9;
import X.C21860y6;
import X.C22330yu;
import X.C22710zW;
import X.C238013b;
import X.C244215l;
import X.C27131Gd;
import X.C42641vY;
import X.C52252aV;
import X.C52732ba;
import X.C71333cl;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.blocklist.BlockList;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class BlockList extends ActivityC13770kJ {
    public C52732ba A00;
    public C238013b A01;
    public C22330yu A02;
    public AnonymousClass130 A03;
    public C15550nR A04;
    public AnonymousClass10S A05;
    public C15610nY A06;
    public AnonymousClass1J1 A07;
    public C21270x9 A08;
    public C244215l A09;
    public C21860y6 A0A;
    public AnonymousClass18N A0B;
    public C18610sj A0C;
    public C22710zW A0D;
    public C17070qD A0E;
    public AnonymousClass12F A0F;
    public boolean A0G;
    public final AnonymousClass2Dn A0H;
    public final C27131Gd A0I;
    public final AbstractC33331dp A0J;
    public final ArrayList A0K;
    public final ArrayList A0L;

    public BlockList() {
        this(0);
        this.A0L = C12960it.A0l();
        this.A0K = C12960it.A0l();
        this.A0I = new AnonymousClass424(this);
        this.A0H = new AnonymousClass41P(this);
        this.A0J = new AnonymousClass44T(this);
    }

    public BlockList(int i) {
        this.A0G = false;
        ActivityC13830kP.A1P(this, 22);
    }

    public static /* synthetic */ void A02(BlockList blockList) {
        blockList.A2g();
        blockList.A00.notifyDataSetChanged();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0G) {
            this.A0G = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A08 = C12970iu.A0W(A1M);
            this.A03 = C12990iw.A0Y(A1M);
            this.A04 = C12960it.A0O(A1M);
            this.A06 = C12960it.A0P(A1M);
            this.A0E = (C17070qD) A1M.AFC.get();
            this.A01 = (C238013b) A1M.A1Z.get();
            this.A0F = C12990iw.A0f(A1M);
            this.A02 = (C22330yu) A1M.A3I.get();
            this.A0A = (C21860y6) A1M.AE6.get();
            this.A0D = (C22710zW) A1M.AF7.get();
            this.A0C = (C18610sj) A1M.AF0.get();
            this.A09 = (C244215l) A1M.A8y.get();
            this.A05 = C12990iw.A0Z(A1M);
        }
    }

    public final void A2g() {
        ArrayList arrayList = this.A0K;
        arrayList.clear();
        ArrayList arrayList2 = this.A0L;
        arrayList2.clear();
        Iterator it = this.A01.A03().iterator();
        while (it.hasNext()) {
            arrayList2.add(this.A04.A0B(C12990iw.A0b(it)));
        }
        Collections.sort(arrayList2, new C71333cl(this.A06, ((ActivityC13830kP) this).A01));
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        ArrayList A0l3 = C12960it.A0l();
        Iterator it2 = arrayList2.iterator();
        while (it2.hasNext()) {
            C15370n3 A0a = C12970iu.A0a(it2);
            if (A0a.A0J()) {
                A0l2.add(new AnonymousClass540(A0a));
            } else {
                A0l.add(new AnonymousClass540(A0a));
            }
        }
        AnonymousClass18N r0 = this.A0B;
        if (r0 != null && r0.AJQ()) {
            ArrayList A0x = C12980iv.A0x(this.A0B.AAs());
            Collections.sort(A0x);
            Iterator it3 = A0x.iterator();
            while (it3.hasNext()) {
                A0l3.add(new AnonymousClass542(C12970iu.A0x(it3)));
            }
        }
        if (!A0l.isEmpty()) {
            arrayList.add(new AnonymousClass541(0));
        }
        arrayList.addAll(A0l);
        if (!A0l2.isEmpty()) {
            arrayList.add(new AnonymousClass541(1));
            arrayList.addAll(A0l2);
        }
        if (!A0l3.isEmpty()) {
            arrayList.add(new AnonymousClass541(2));
        }
        arrayList.addAll(A0l3);
    }

    public final void A2h() {
        TextView A0M = C12970iu.A0M(this, R.id.block_list_primary_text);
        TextView A0M2 = C12970iu.A0M(this, R.id.block_list_help);
        View findViewById = findViewById(R.id.block_list_info);
        if (this.A01.A0H.A00.getLong("block_list_receive_time", 0) != 0) {
            A0M2.setVisibility(0);
            findViewById.setVisibility(0);
            Drawable A0C = C12970iu.A0C(this, R.drawable.ic_add_person_tip);
            A0M.setText(R.string.no_blocked_contacts);
            String string = getString(R.string.block_list_help);
            A0M2.setText(C52252aV.A00(A0M2.getPaint(), AnonymousClass2GE.A03(this, A0C, R.color.add_person_to_block_tint), string));
            return;
        }
        A0M2.setVisibility(8);
        findViewById.setVisibility(8);
        A0M.setText(C18640sm.A01(this));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 10) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            this.A01.A08(this, null, this.A04.A0B(ActivityC13790kL.A0V(intent, "contact")), null, null, null, true, true);
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        AnonymousClass18N r3;
        AnonymousClass5TX r1 = (AnonymousClass5TX) A2e().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        if (menuItem.getItemId() != 0) {
            return super.onContextItemSelected(menuItem);
        }
        int ADe = r1.ADe();
        if (ADe != 0) {
            if (ADe == 1 && (r3 = this.A0B) != null) {
                r3.Afe(this, new AnonymousClass5US() { // from class: X.586
                    @Override // X.AnonymousClass5US
                    public final void AVD(C452120p r32) {
                        BlockList blockList = BlockList.this;
                        if (r32 == null) {
                            blockList.A2g();
                            blockList.A00.notifyDataSetChanged();
                            return;
                        }
                        blockList.Ado(R.string.payment_unblock_error);
                    }
                }, this.A0C, ((AnonymousClass542) r1).A00, false);
            }
            return true;
        }
        C15370n3 r12 = ((AnonymousClass540) r1).A00;
        C238013b r0 = this.A01;
        AnonymousClass009.A05(r12);
        r0.A0B(this, r12, true);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.block_list_header);
        C12970iu.A0N(this).A0M(true);
        setContentView(R.layout.block_list);
        this.A07 = this.A08.A04(this, "block-list-activity");
        if (this.A0D.A07() && this.A0A.A0C()) {
            AnonymousClass18N ABo = this.A0E.A02().ABo();
            this.A0B = ABo;
            if (ABo != null && ABo.AdQ()) {
                this.A0B.A9p(new AnonymousClass5US() { // from class: X.585
                    @Override // X.AnonymousClass5US
                    public final void AVD(C452120p r2) {
                        BlockList blockList = BlockList.this;
                        if (r2 == null) {
                            blockList.A2g();
                            blockList.A00.notifyDataSetChanged();
                        }
                    }
                }, this.A0C);
            }
        }
        A2g();
        A2h();
        C14850m9 r8 = ((ActivityC13810kN) this).A0C;
        C52732ba r2 = new C52732ba(this, this.A03, this.A06, this.A07, ((ActivityC13830kP) this).A01, r8, this.A0F, this.A0K);
        this.A00 = r2;
        A2f(r2);
        A2e().setEmptyView(findViewById(R.id.block_list_empty));
        A2e().setDivider(null);
        A2e().setClipToPadding(false);
        registerForContextMenu(A2e());
        A2e().setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4ow
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                BlockList.this.openContextMenu(view);
            }
        });
        this.A05.A03(this.A0I);
        this.A02.A03(this.A0H);
        this.A09.A03(this.A0J);
        this.A01.A0F(null);
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        String A04;
        AnonymousClass5TX r2 = (AnonymousClass5TX) A2e().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        int ADe = r2.ADe();
        if (ADe != 0) {
            if (ADe == 1) {
                A04 = ((AnonymousClass542) r2).A00;
            }
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        }
        A04 = this.A06.A04(((AnonymousClass540) r2).A00);
        contextMenu.add(0, 0, 0, C12960it.A0X(this, A04, new Object[1], 0, R.string.block_list_menu_unblock));
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_settings_add_blocked_contact, 0, R.string.menuitem_add).setIcon(R.drawable.ic_action_add_person).setShowAsAction(2);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A07.A00();
        this.A05.A04(this.A0I);
        this.A02.A04(this.A0H);
        this.A09.A04(this.A0J);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_settings_add_blocked_contact) {
            ArrayList A0l = C12960it.A0l();
            Iterator it = this.A0L.iterator();
            while (it.hasNext()) {
                Jid jid = C12970iu.A0a(it).A0D;
                AnonymousClass009.A05(jid);
                A0l.add(jid.getRawString());
            }
            C42641vY r1 = new C42641vY(this);
            r1.A03 = true;
            r1.A0Q = A0l;
            r1.A03 = Boolean.TRUE;
            startActivityForResult(r1.A00(), 10);
            return true;
        }
        if (menuItem.getItemId() == 16908332) {
            finish();
        }
        return true;
    }
}
