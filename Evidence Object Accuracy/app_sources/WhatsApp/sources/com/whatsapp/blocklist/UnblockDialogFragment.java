package com.whatsapp.blocklist;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.AnonymousClass5TY;
import X.C004802e;
import X.C12970iu;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class UnblockDialogFragment extends Hilt_UnblockDialogFragment {
    public AnonymousClass5TY A00;
    public AnonymousClass018 A01;
    public boolean A02;

    public static UnblockDialogFragment A00(AnonymousClass5TY r3, String str, int i, boolean z) {
        UnblockDialogFragment unblockDialogFragment = new UnblockDialogFragment();
        unblockDialogFragment.A00 = r3;
        unblockDialogFragment.A02 = z;
        Bundle A0D = C12970iu.A0D();
        A0D.putString("message", str);
        A0D.putInt("title", i);
        unblockDialogFragment.A0U(A0D);
        return unblockDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1;
        ActivityC000900k A0B = A0B();
        String string = A03().getString("message");
        AnonymousClass009.A05(string);
        int i = A03().getInt("title");
        if (this.A00 == null) {
            iDxCListenerShape9S0100000_2_I1 = null;
        } else {
            iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(this, 19);
        }
        IDxCListenerShape4S0200000_2_I1 iDxCListenerShape4S0200000_2_I1 = new IDxCListenerShape4S0200000_2_I1(A0B, 0, this);
        C004802e r2 = new C004802e(A0B);
        r2.A0A(string);
        if (i != 0) {
            r2.A07(i);
        }
        r2.setPositiveButton(R.string.unblock, iDxCListenerShape9S0100000_2_I1);
        r2.setNegativeButton(R.string.cancel, iDxCListenerShape4S0200000_2_I1);
        if (this.A02) {
            r2.A01.A08 = new DialogInterface.OnKeyListener(A0B) { // from class: X.4hM
                public final /* synthetic */ Activity A00;

                {
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnKeyListener
                public final boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
                    Activity activity = this.A00;
                    if (i2 != 4 || keyEvent.getAction() != 1) {
                        return false;
                    }
                    activity.finish();
                    return true;
                }
            };
        }
        AnonymousClass04S create = r2.create();
        create.setCanceledOnTouchOutside(!this.A02);
        return create;
    }
}
