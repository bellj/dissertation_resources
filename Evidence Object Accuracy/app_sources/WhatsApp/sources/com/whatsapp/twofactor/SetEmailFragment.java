package com.whatsapp.twofactor;

import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass23M;
import X.AnonymousClass2eq;
import X.AnonymousClass3KI;
import X.AnonymousClass47M;
import X.C004802e;
import X.C52162aM;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextWatcher;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.ViewOnClickCListenerShape16S0100000_I0_3;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class SetEmailFragment extends Hilt_SetEmailFragment {
    public int A00;
    public TextWatcher A01 = new AnonymousClass47M(this);
    public Button A02;
    public EditText A03;
    public TextView A04;
    public AnonymousClass01d A05;
    public TwoFactorAuthActivity A06;

    public static SetEmailFragment A00(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("type", i);
        SetEmailFragment setEmailFragment = new SetEmailFragment();
        setEmailFragment.A0U(bundle);
        return setEmailFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.fragment_two_factor_auth_email, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A04 = null;
        this.A03 = null;
        this.A02 = null;
        this.A06 = null;
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        String str;
        super.A13();
        EditText editText = this.A03;
        TextWatcher textWatcher = this.A01;
        editText.removeTextChangedListener(textWatcher);
        EditText editText2 = this.A03;
        int i = this.A00;
        TwoFactorAuthActivity twoFactorAuthActivity = this.A06;
        if (i == 1) {
            str = twoFactorAuthActivity.A04;
        } else {
            str = twoFactorAuthActivity.A05;
        }
        editText2.setText(str);
        this.A03.addTextChangedListener(textWatcher);
        A1A();
        this.A03.requestFocus();
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A00 = A03().getInt("type", 1);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        int i;
        TwoFactorAuthActivity twoFactorAuthActivity;
        this.A06 = (TwoFactorAuthActivity) A0B();
        Button button = (Button) view.findViewById(R.id.submit);
        this.A02 = button;
        button.setOnClickListener(new ViewOnClickCListenerShape16S0100000_I0_3(this, 2));
        this.A03 = (EditText) view.findViewById(R.id.email);
        this.A04 = (TextView) view.findViewById(R.id.error);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) view.findViewById(R.id.description);
        int i2 = this.A00;
        int i3 = 1;
        if (i2 == 1) {
            if (this.A06.A08[0] != 2) {
                textEmojiLabel.A07 = new C52162aM();
                textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, this.A05));
                String A0I = A0I(R.string.two_factor_auth_email_info_with_skip);
                int A00 = AnonymousClass00T.A00(A0p(), R.color.link_color);
                TextAppearanceSpan textAppearanceSpan = new TextAppearanceSpan(A0p(), R.style.SettingsInlineLink);
                RunnableBRunnable0Shape12S0100000_I0_12 runnableBRunnable0Shape12S0100000_I0_12 = new RunnableBRunnable0Shape12S0100000_I0_12(this, 47);
                HashMap hashMap = new HashMap();
                hashMap.put("skip", runnableBRunnable0Shape12S0100000_I0_12);
                textEmojiLabel.setText(AnonymousClass23M.A07(textAppearanceSpan, A0I, hashMap, A00));
            } else {
                textEmojiLabel.setText(R.string.two_factor_auth_email_info);
            }
            this.A02.setText(R.string.next);
        } else if (i2 == 2) {
            textEmojiLabel.setText(R.string.two_factor_auth_email_confirmation);
            this.A02.setText(R.string.two_factor_auth_submit);
            i = 1;
            twoFactorAuthActivity = this.A06;
            if (twoFactorAuthActivity.A2h(this) || twoFactorAuthActivity.A08.length == 1) {
                i3 = i;
            }
            twoFactorAuthActivity.A2f(view, i3);
        }
        i = 0;
        twoFactorAuthActivity = this.A06;
        if (twoFactorAuthActivity.A2h(this)) {
        }
        i3 = i;
        twoFactorAuthActivity.A2f(view, i3);
    }

    public final void A1A() {
        Button button = this.A02;
        if (button != null) {
            String trim = this.A03.getText().toString().trim();
            int indexOf = trim.indexOf(64);
            boolean z = true;
            if (indexOf <= 0 || indexOf >= trim.length() - 1 || indexOf != trim.lastIndexOf(64)) {
                z = false;
            }
            button.setEnabled(z);
        }
    }

    /* loaded from: classes2.dex */
    public class ConfirmSkipEmailDialog extends Hilt_SetEmailFragment_ConfirmSkipEmailDialog {
        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e r2 = new C004802e(A0p());
            r2.A06(R.string.two_factor_auth_email_skip_confirm);
            r2.setPositiveButton(R.string.ok, new AnonymousClass3KI(this));
            r2.setNegativeButton(R.string.cancel, null);
            return r2.create();
        }
    }
}
