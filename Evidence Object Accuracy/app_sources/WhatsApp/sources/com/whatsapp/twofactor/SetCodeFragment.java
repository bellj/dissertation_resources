package com.whatsapp.twofactor;

import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14920mG;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SetCodeFragment extends Hilt_SetCodeFragment {
    public int A00;
    public Button A01;
    public TextView A02;
    public CodeInputField A03;
    public TwoFactorAuthActivity A04;
    public C14920mG A05;

    public static SetCodeFragment A00(int i) {
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("type", i);
        SetCodeFragment setCodeFragment = new SetCodeFragment();
        setCodeFragment.A0U(A0D);
        return setCodeFragment;
    }

    public static /* synthetic */ void A01(SetCodeFragment setCodeFragment) {
        int i = setCodeFragment.A00;
        if (i == 1) {
            setCodeFragment.A04.A2g(A00(2), true);
        } else if (i == 2) {
            TwoFactorAuthActivity twoFactorAuthActivity = setCodeFragment.A04;
            if (twoFactorAuthActivity.A2h(setCodeFragment)) {
                twoFactorAuthActivity.A2e();
            } else {
                twoFactorAuthActivity.A2g(SetEmailFragment.A00(1), true);
            }
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.fragment_two_factor_auth_code);
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A04 = null;
        this.A01 = null;
        this.A03 = null;
        this.A02 = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r4.A04.A2h(r4) == false) goto L_0x0011;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A13() {
        /*
            r4 = this;
            super.A13()
            int r0 = r4.A00
            r3 = 2
            if (r0 != r3) goto L_0x0011
            com.whatsapp.twofactor.TwoFactorAuthActivity r0 = r4.A04
            boolean r0 = r0.A2h(r4)
            r2 = 1
            if (r0 != 0) goto L_0x0012
        L_0x0011:
            r2 = 0
        L_0x0012:
            android.widget.Button r1 = r4.A01
            r0 = 2131889573(0x7f120da5, float:1.9413813E38)
            if (r2 == 0) goto L_0x001c
            r0 = 2131892399(0x7f1218af, float:1.9419545E38)
        L_0x001c:
            r1.setText(r0)
            int r0 = r4.A00
            if (r0 != r3) goto L_0x0037
            com.whatsapp.twofactor.TwoFactorAuthActivity r0 = r4.A04
            java.lang.String r1 = r0.A03
            if (r1 == 0) goto L_0x0037
            com.whatsapp.CodeInputField r0 = r4.A03
            r0.setCode(r1)
            com.whatsapp.CodeInputField r0 = r4.A03
            java.lang.String r0 = r0.getCode()
            r4.A1B(r0)
        L_0x0037:
            r4.A1A()
            com.whatsapp.CodeInputField r0 = r4.A03
            r0.requestFocus()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.twofactor.SetCodeFragment.A13():void");
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A00 = A03().getInt("type", 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0070  */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r14, android.view.View r15) {
        /*
            r13 = this;
            X.00k r0 = r13.A0B()
            com.whatsapp.twofactor.TwoFactorAuthActivity r0 = (com.whatsapp.twofactor.TwoFactorAuthActivity) r0
            r13.A04 = r0
            r0 = 2131366239(0x7f0a115f, float:1.8352366E38)
            android.view.View r1 = r15.findViewById(r0)
            android.widget.Button r1 = (android.widget.Button) r1
            r13.A01 = r1
            r0 = 45
            X.AbstractView$OnClickListenerC34281fs.A01(r1, r13, r0)
            r0 = 2131363390(0x7f0a063e, float:1.8346587E38)
            android.widget.TextView r0 = X.C12960it.A0J(r15, r0)
            r13.A02 = r0
            r0 = 2131362706(0x7f0a0392, float:1.83452E38)
            android.view.View r0 = r15.findViewById(r0)
            com.whatsapp.CodeInputField r0 = (com.whatsapp.CodeInputField) r0
            r13.A03 = r0
            X.3U3 r7 = new X.3U3
            r7.<init>()
            r1 = 2131886165(0x7f120055, float:1.9406901E38)
            r3 = 1
            java.lang.Object[] r0 = new java.lang.Object[r3]
            r12 = 6
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)
            r2 = 0
            java.lang.String r9 = X.C12970iu.A0q(r13, r4, r0, r2, r1)
            com.whatsapp.CodeInputField r5 = r13.A03
            X.3U1 r6 = new X.3U1
            r6.<init>(r13)
            r10 = 42
            r8 = 0
            r11 = 42
            r5.A08(r6, r7, r8, r9, r10, r11, r12)
            int r1 = r13.A00
            if (r1 == r3) goto L_0x007e
            r0 = 2
            if (r1 == r0) goto L_0x0075
            r0 = 2131892402(0x7f1218b2, float:1.9419551E38)
            java.lang.String r1 = r13.A0I(r0)
        L_0x005e:
            r4 = 0
        L_0x005f:
            r0 = 2131362707(0x7f0a0393, float:1.8345202E38)
            android.widget.TextView r0 = X.C12960it.A0J(r15, r0)
            r0.setText(r1)
            com.whatsapp.twofactor.TwoFactorAuthActivity r1 = r13.A04
            int[] r0 = r1.A08
            int r0 = r0.length
            if (r0 != r3) goto L_0x0071
            r2 = r4
        L_0x0071:
            r1.A2f(r15, r2)
            return
        L_0x0075:
            r0 = 2131892373(0x7f121895, float:1.9419492E38)
            java.lang.String r1 = r13.A0I(r0)
            r4 = 1
            goto L_0x005f
        L_0x007e:
            r1 = 2131892368(0x7f121890, float:1.9419482E38)
            java.lang.Object[] r0 = new java.lang.Object[r3]
            java.lang.String r1 = X.C12970iu.A0q(r13, r4, r0, r2, r1)
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.twofactor.SetCodeFragment.A17(android.os.Bundle, android.view.View):void");
    }

    public final void A1A() {
        String str;
        if (this.A01 != null) {
            boolean z = true;
            if (this.A00 != 1 ? (str = this.A04.A02) == null || !str.contentEquals(this.A03.getCode()) : this.A03.getCode().length() != 6) {
                z = false;
            }
            this.A01.setEnabled(z);
        }
    }

    public final boolean A1B(CharSequence charSequence) {
        C12990iw.A1G(this.A02);
        if (charSequence.length() == 6) {
            int i = this.A00;
            if (i != 1) {
                if (i == 2) {
                    String str = this.A04.A02;
                    if (str == null || !str.contentEquals(this.A03.getCode())) {
                        this.A02.setText(R.string.two_factor_auth_code_mismatch_error);
                    }
                }
                this.A03.requestFocus();
            }
            return true;
        }
        return false;
    }
}
