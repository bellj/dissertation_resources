package com.whatsapp.twofactor;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1e1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C004902f;
import X.C016307r;
import X.C103654r6;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14920mG;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public class TwoFactorAuthActivity extends ActivityC13790kL implements AnonymousClass1e1 {
    public static final int[] A0B = {R.id.page_indicator_1, R.id.page_indicator_2, R.id.page_indicator_3};
    public AbstractC005102i A00;
    public C14920mG A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;
    public int[] A08;
    public final Handler A09;
    public final Runnable A0A;

    public TwoFactorAuthActivity() {
        this(0);
        this.A09 = new Handler(Looper.getMainLooper());
        this.A0A = new RunnableBRunnable0Shape13S0100000_I0_13(this, 0);
    }

    public TwoFactorAuthActivity(int i) {
        this.A07 = false;
        A0R(new C103654r6(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A01 = (C14920mG) r1.ALr.get();
        }
    }

    public void A2e() {
        A2C(R.string.two_factor_auth_submitting);
        this.A09.postDelayed(this.A0A, C14920mG.A0D);
        ((ActivityC13830kP) this).A05.Ab6(new RunnableBRunnable0Shape13S0100000_I0_13(this, 3));
    }

    public void A2f(View view, int i) {
        int i2 = 0;
        while (i2 < i) {
            i2++;
            C016307r.A00(ColorStateList.valueOf(AnonymousClass00T.A00(this, R.color.pageIndicatorSelected)), (ImageView) view.findViewById(A0B[i2]));
        }
        int length = this.A08.length;
        while (true) {
            length++;
            int[] iArr = A0B;
            if (length < iArr.length) {
                view.findViewById(iArr[length]).setVisibility(8);
            } else {
                return;
            }
        }
    }

    public void A2g(AnonymousClass01E r6, boolean z) {
        C004902f r4 = new C004902f(A0V());
        r4.A02 = R.anim.slide_in_right;
        r4.A03 = R.anim.slide_out_left;
        r4.A05 = R.anim.slide_in_left;
        r4.A06 = R.anim.slide_out_right;
        r4.A07(r6, R.id.container);
        if (z) {
            r4.A0F(null);
        }
        r4.A01();
    }

    public boolean A2h(AnonymousClass01E r4) {
        return this.A08.length == 1 || r4.getClass() == SetEmailFragment.class;
    }

    @Override // X.AnonymousClass1e1
    public void AXu() {
        Handler handler = this.A09;
        handler.removeCallbacks(this.A0A);
        handler.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(this, 1), 700);
    }

    @Override // X.AnonymousClass1e1
    public void AXv() {
        Handler handler = this.A09;
        handler.removeCallbacks(this.A0A);
        handler.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(this, 2), 700);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass01E A00;
        super.onCreate(bundle);
        getWindow().addFlags(DefaultCrypto.BUFFER_SIZE);
        setTitle(R.string.settings_two_factor_auth);
        AbstractC005102i A1U = A1U();
        this.A00 = A1U;
        boolean z = true;
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.activity_two_factor_auth);
        int[] intArrayExtra = getIntent().getIntArrayExtra("workflows");
        AnonymousClass009.A05(intArrayExtra);
        this.A08 = intArrayExtra;
        if (intArrayExtra.length <= 0) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        String stringExtra = getIntent().getStringExtra("primaryCTA");
        AnonymousClass009.A05(stringExtra);
        this.A06 = stringExtra;
        C004902f r4 = new C004902f(A0V());
        int i = this.A08[0];
        if (i == 1) {
            A00 = SetCodeFragment.A00(1);
        } else if (i == 2) {
            A00 = SetEmailFragment.A00(1);
        } else {
            StringBuilder sb = new StringBuilder("Invalid work flow:");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        r4.A07(A00, R.id.container);
        r4.A01();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass01F A0V = A0V();
            if (A0V.A03() > 0) {
                A0V.A0H();
                return true;
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        List list = this.A01.A0B;
        AnonymousClass009.A0F(list.contains(this));
        list.remove(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        List list = this.A01.A0B;
        AnonymousClass009.A0F(!list.contains(this));
        list.add(this);
    }
}
