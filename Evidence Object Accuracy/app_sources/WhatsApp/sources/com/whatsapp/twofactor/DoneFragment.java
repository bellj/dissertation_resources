package com.whatsapp.twofactor;

import X.AnonymousClass028;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DoneFragment extends Hilt_DoneFragment {
    public String A00;

    public static DoneFragment A00(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("primaryCTA", str);
        DoneFragment doneFragment = new DoneFragment();
        doneFragment.A0U(bundle);
        return doneFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.fragment_two_factor_auth_done, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A00 = A03().getString("primaryCTA", "DONE");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        boolean equals = this.A00.equals("CONTINUE");
        int i = R.string.encrypted_backup_done_button;
        if (equals) {
            i = R.string.delete_chat_clear_chat_nux_button_text;
        }
        String A0I = A0I(i);
        TextView textView = (TextView) AnonymousClass028.A0D(view, R.id.done_button);
        textView.setText(A0I);
        textView.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 7));
        TwoFactorAuthActivity twoFactorAuthActivity = (TwoFactorAuthActivity) A0B();
        twoFactorAuthActivity.A2f(view, twoFactorAuthActivity.A08.length);
    }
}
