package com.whatsapp.twofactor;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1e1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass3KJ;
import X.C004802e;
import X.C103644r5;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14920mG;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C41691tw;
import X.ViewTreeObserver$OnPreDrawListenerC101944oL;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;
import java.util.List;

/* loaded from: classes2.dex */
public class SettingsTwoFactorAuthActivity extends ActivityC13790kL implements AnonymousClass1e1 {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public ScrollView A05;
    public TextView A06;
    public TextView A07;
    public C14920mG A08;
    public boolean A09;
    public final Handler A0A;
    public final Runnable A0B;

    public SettingsTwoFactorAuthActivity() {
        this(0);
        this.A0A = new Handler(Looper.getMainLooper());
        this.A0B = new RunnableBRunnable0Shape12S0100000_I0_12(this, 48);
    }

    public SettingsTwoFactorAuthActivity(int i) {
        this.A09 = false;
        A0R(new C103644r5(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A08 = (C14920mG) r1.ALr.get();
        }
    }

    public final void A2e() {
        float f;
        boolean canScrollVertically = this.A05.canScrollVertically(1);
        View view = this.A03;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public final void A2f() {
        this.A05.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101944oL(this));
    }

    public final void A2g(int... iArr) {
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.twofactor.TwoFactorAuthActivity");
        intent.putExtra("primaryCTA", "DONE");
        intent.putExtra("workflows", iArr);
        startActivity(intent);
    }

    @Override // X.AnonymousClass1e1
    public void AXu() {
        this.A0A.removeCallbacks(this.A0B);
        AaN();
        Ado(R.string.two_factor_auth_save_error);
        ((ActivityC13830kP) this).A05.Ab6(new RunnableBRunnable0Shape12S0100000_I0_12(this, 49));
    }

    @Override // X.AnonymousClass1e1
    public void AXv() {
        this.A0A.removeCallbacks(this.A0B);
        AaN();
        ((ActivityC13830kP) this).A05.Ab6(new RunnableBRunnable0Shape12S0100000_I0_12(this, 49));
        ((ActivityC13810kN) this).A05.A07(R.string.two_factor_auth_disabled, 1);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (Build.VERSION.SDK_INT >= 21) {
            A2f();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_two_factor_auth);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.settings_two_factor_auth);
        this.A05 = (ScrollView) findViewById(R.id.scroll_view);
        this.A04 = (ImageView) findViewById(R.id.logo);
        this.A03 = findViewById(R.id.enable_panel);
        this.A02 = findViewById(R.id.disable_panel_divider);
        this.A01 = findViewById(R.id.disable_panel);
        this.A07 = (TextView) findViewById(R.id.description);
        TextView textView = (TextView) findViewById(R.id.disable_button);
        TextView textView2 = (TextView) findViewById(R.id.change_code_button);
        this.A06 = (TextView) findViewById(R.id.change_email_button);
        findViewById(R.id.enable_button).setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 8));
        textView.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 9));
        textView2.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 11));
        this.A06.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 10));
        int i = Build.VERSION.SDK_INT;
        if (i < 23) {
            int A00 = C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon);
            AnonymousClass2GE.A08(textView, A00);
            AnonymousClass2GE.A08(textView2, A00);
            AnonymousClass2GE.A08(this.A06, A00);
        }
        if (i >= 21) {
            this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
            this.A05.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4of
                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    SettingsTwoFactorAuthActivity.this.A2e();
                }
            });
            A2f();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        List list = this.A08.A0B;
        AnonymousClass009.A0F(list.contains(this));
        list.remove(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            currentFocus.clearFocus();
        }
        List list = this.A08.A0B;
        AnonymousClass009.A0F(!list.contains(this));
        list.add(this);
        ((ActivityC13830kP) this).A05.Ab6(new RunnableBRunnable0Shape12S0100000_I0_12(this, 49));
    }

    /* loaded from: classes2.dex */
    public class ConfirmDisableDialog extends Hilt_SettingsTwoFactorAuthActivity_ConfirmDisableDialog {
        public AnonymousClass018 A00;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e r2 = new C004802e(A0p());
            r2.A06(R.string.settings_two_factor_auth_disable_confirm);
            r2.setPositiveButton(R.string.settings_two_factor_auth_disable, new AnonymousClass3KJ(this));
            r2.setNegativeButton(R.string.cancel, null);
            return r2.create();
        }
    }
}
