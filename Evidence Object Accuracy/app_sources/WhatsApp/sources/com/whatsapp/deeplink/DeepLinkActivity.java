package com.whatsapp.deeplink;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass11G;
import X.AnonymousClass11J;
import X.AnonymousClass12P;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass19M;
import X.AnonymousClass19Q;
import X.AnonymousClass19T;
import X.AnonymousClass19Y;
import X.AnonymousClass19Z;
import X.AnonymousClass1AN;
import X.AnonymousClass1BK;
import X.AnonymousClass1BL;
import X.AnonymousClass2B8;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2I7;
import X.AnonymousClass3CP;
import X.C103064q9;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16430p0;
import X.C17070qD;
import X.C17220qS;
import X.C18350sJ;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19850um;
import X.C20800wL;
import X.C21470xT;
import X.C21550xb;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22180yf;
import X.C22300yr;
import X.C22610zM;
import X.C22670zS;
import X.C22700zV;
import X.C249317l;
import X.C250918b;
import X.C251118d;
import X.C252718t;
import X.C252818u;
import X.C253318z;
import X.C255019q;
import X.C25831Az;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape0S1210000_I0;
import com.whatsapp.R;
import com.whatsapp.deeplink.DeepLinkActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DeepLinkActivity extends ActivityC13790kL implements AnonymousClass2B8 {
    public Handler A00;
    public AnonymousClass17S A01;
    public AnonymousClass19Y A02;
    public C21740xu A03;
    public C25831Az A04;
    public C19850um A05;
    public AnonymousClass19Q A06;
    public AnonymousClass19T A07;
    public C250918b A08;
    public C251118d A09;
    public C16430p0 A0A;
    public C15550nR A0B;
    public C22700zV A0C;
    public C253318z A0D;
    public AnonymousClass11J A0E;
    public C15890o4 A0F;
    public C22610zM A0G;
    public AnonymousClass3CP A0H;
    public C21550xb A0I;
    public C255019q A0J;
    public C21470xT A0K;
    public AnonymousClass1BK A0L;
    public C22300yr A0M;
    public C22180yf A0N;
    public AnonymousClass1AN A0O;
    public AnonymousClass11G A0P;
    public C17220qS A0Q;
    public C17070qD A0R;
    public AnonymousClass1BL A0S;
    public C20800wL A0T;
    public C18350sJ A0U;
    public AnonymousClass17R A0V;
    public AnonymousClass2I7 A0W;
    public AnonymousClass19Z A0X;
    public String A0Y;
    public boolean A0Z;
    public final C14960mK A0a;

    public DeepLinkActivity() {
        this(0);
        this.A0a = new C14960mK();
    }

    public DeepLinkActivity(int i) {
        this.A0Z = false;
        A0R(new C103064q9(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0Z) {
            this.A0Z = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A03 = (C21740xu) r2.AM1.get();
            this.A01 = (AnonymousClass17S) r2.A0F.get();
            this.A0K = (C21470xT) r2.ALe.get();
            this.A0X = (AnonymousClass19Z) r2.A2o.get();
            this.A02 = (AnonymousClass19Y) r2.AI6.get();
            this.A0Q = (C17220qS) r2.ABt.get();
            this.A0V = (AnonymousClass17R) r2.AIz.get();
            this.A07 = (AnonymousClass19T) r2.A2y.get();
            this.A0N = (C22180yf) r2.A5U.get();
            this.A0B = (C15550nR) r2.A45.get();
            this.A0R = (C17070qD) r2.AFC.get();
            this.A0L = (AnonymousClass1BK) r2.AFZ.get();
            this.A0D = (C253318z) r2.A4B.get();
            this.A0M = (C22300yr) r2.AMv.get();
            this.A0J = (C255019q) r2.A50.get();
            this.A0P = (AnonymousClass11G) r2.AKq.get();
            this.A05 = (C19850um) r2.A2v.get();
            this.A04 = (C25831Az) r2.A30.get();
            this.A0U = (C18350sJ) r2.AHX.get();
            this.A0C = (C22700zV) r2.AMN.get();
            this.A0F = (C15890o4) r2.AN1.get();
            this.A0W = r1.A0L();
            this.A06 = (AnonymousClass19Q) r2.A2u.get();
            this.A0S = (AnonymousClass1BL) r2.AFa.get();
            this.A09 = (C251118d) r2.A2D.get();
            this.A0O = (AnonymousClass1AN) r2.A5T.get();
            this.A0T = (C20800wL) r2.AHW.get();
            this.A0H = new AnonymousClass3CP((C14830m7) r2.ALb.get());
            this.A0I = (C21550xb) r2.AAj.get();
            this.A0G = (C22610zM) r2.A6b.get();
            this.A0E = (AnonymousClass11J) r2.A3m.get();
            this.A0A = (C16430p0) r2.A5y.get();
            this.A08 = (C250918b) r2.A2B.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        finish();
        overridePendingTransition(0, 0);
    }

    public final void A2e(UserJid userJid, String str, boolean z) {
        if (((ActivityC13810kN) this).A0C.A07(508)) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1210000_I0(this, userJid, str, 1, z));
        }
    }

    public final void A2f(String str) {
        AlertDialog create = new AlertDialog.Builder(this).setTitle(R.string.invalid_deep_link).setMessage(R.string.invalid_deep_link_for_consumer).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fi
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeepLinkActivity deepLinkActivity = DeepLinkActivity.this;
                deepLinkActivity.finish();
                deepLinkActivity.overridePendingTransition(0, 0);
            }
        }).setPositiveButton(R.string.open_smb_app, new DialogInterface.OnClickListener(str) { // from class: X.3KS
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                Intent intent;
                PackageManager packageManager;
                DeepLinkActivity deepLinkActivity = DeepLinkActivity.this;
                String str2 = this.A01;
                try {
                    packageManager = deepLinkActivity.getPackageManager();
                } catch (PackageManager.NameNotFoundException | RuntimeException e) {
                    Log.e("Failed to get package info", e);
                }
                if (packageManager != null) {
                    if (packageManager.getPackageInfo("com.whatsapp.w4b", 0) != null) {
                        intent = C12970iu.A0B(Uri.parse(str2));
                        ((ActivityC13790kL) deepLinkActivity).A00.A06(deepLinkActivity, intent);
                        deepLinkActivity.finish();
                        deepLinkActivity.overridePendingTransition(0, 0);
                    }
                }
                intent = deepLinkActivity.A0W.A00("smb_linking_back2wa");
                ((ActivityC13790kL) deepLinkActivity).A00.A06(deepLinkActivity, intent);
                deepLinkActivity.finish();
                deepLinkActivity.overridePendingTransition(0, 0);
            }
        }).create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: X.4hD
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                DeepLinkActivity deepLinkActivity = DeepLinkActivity.this;
                deepLinkActivity.finish();
                deepLinkActivity.overridePendingTransition(0, 0);
            }
        });
        create.show();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:248:0x08a1, code lost:
        if (X.AnonymousClass4G7.A00.contains(r1) != false) goto L_0x08a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x09b1, code lost:
        if (((X.ActivityC13790kL) r28).A0B.A01() == false) goto L_0x09b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x024c, code lost:
        if (r2 < 1) goto L_0x024e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02a6, code lost:
        if (r6.AJs(r4) != false) goto L_0x02a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x03ac, code lost:
        if (r3.A03.A07(1439) != false) goto L_0x03ae;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r29) {
        /*
        // Method dump skipped, instructions count: 2604
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.deeplink.DeepLinkActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A00.removeMessages(1);
    }
}
