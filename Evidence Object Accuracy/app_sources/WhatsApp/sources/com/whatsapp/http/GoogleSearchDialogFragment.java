package com.whatsapp.http;

import X.AbstractC14440lR;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass01V;
import X.AnonymousClass04S;
import X.AnonymousClass12P;
import X.AnonymousClass1X7;
import X.AnonymousClass38T;
import X.C004802e;
import X.C14900mE;
import X.C15450nH;
import X.C16120oU;
import X.C18790t3;
import X.C28861Ph;
import X.C853442j;
import X.DialogInterface$OnClickListenerC96664fo;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.http.GoogleSearchDialogFragment;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class GoogleSearchDialogFragment extends Hilt_GoogleSearchDialogFragment {
    public AnonymousClass12P A00;
    public C14900mE A01;
    public C15450nH A02;
    public C18790t3 A03;
    public C16120oU A04;
    public AbstractC14440lR A05;

    public static void A00(ActivityC13810kN r4, C15450nH r5, AbstractC15340mz r6) {
        if (!(r6 instanceof AnonymousClass1X7) && (r6 instanceof C28861Ph) && r5.A05(AbstractC15460nI.A13)) {
            String A0I = r6.A0I();
            Bundle bundle = new Bundle();
            bundle.putInt("search_query_type", 0);
            bundle.putString("search_query_text", A0I);
            GoogleSearchDialogFragment googleSearchDialogFragment = new GoogleSearchDialogFragment();
            googleSearchDialogFragment.A0U(bundle);
            r4.Adm(googleSearchDialogFragment);
        }
    }

    public static /* synthetic */ void A01(GoogleSearchDialogFragment googleSearchDialogFragment, int i) {
        String str;
        String replace;
        if (i == -1) {
            Bundle A03 = googleSearchDialogFragment.A03();
            ActivityC000900k A0C = googleSearchDialogFragment.A0C();
            if (!(A0C instanceof ActivityC13810kN)) {
                AnonymousClass009.A07("GoogleSearchDialogFragment does not have a DialogActivity as a host");
            } else if (((ActivityC13810kN) A0C).A2S(R.string.quick_message_search_no_internet)) {
                return;
            }
            int i2 = A03.getInt("search_query_type");
            if (i2 == 0) {
                String string = googleSearchDialogFragment.A03().getString("search_query_text");
                Uri.Builder appendQueryParameter = new Uri.Builder().scheme("https").authority("www.google.com").path("search").appendQueryParameter("ctx", "wa");
                int length = (2000 - appendQueryParameter.build().toString().getBytes().length) - 3;
                Charset charset = AnonymousClass01V.A0A;
                byte[] array = charset.encode(string).array();
                try {
                    replace = URLEncoder.encode(string, AnonymousClass01V.A08).replace("+", "%20");
                } catch (UnsupportedEncodingException unused) {
                    Log.e("UrlUtils/truncateParameterForPercentEncoding UTF-8 encoding not supported");
                }
                if (replace.getBytes().length > length) {
                    int i3 = 0;
                    int i4 = 0;
                    while (i3 < length && i4 < array.length) {
                        i3 = replace.charAt(i3) == '%' ? i3 + 3 : i3 + 1;
                        if (i3 > length) {
                            break;
                        }
                        i4++;
                    }
                    while ((array[i4] & 192) == 128 && i4 > 0) {
                        i4--;
                    }
                    string = new String(Arrays.copyOfRange(array, 0, i4), charset);
                } else if (string == null) {
                    str = "Failed to encode URI in UTF-8, this should not happen";
                }
                Uri build = appendQueryParameter.appendQueryParameter("q", string).build();
                if (build != null) {
                    Intent intent = new Intent("android.intent.action.VIEW", build);
                    ActivityC000900k A0B = googleSearchDialogFragment.A0B();
                    if (A0B != null && !A0B.isFinishing()) {
                        googleSearchDialogFragment.A04.A07(new C853442j());
                        googleSearchDialogFragment.A00.A06(A0B, intent);
                        return;
                    }
                    return;
                }
                str = "Failed to encode URI in UTF-8, this should not happen";
            } else if (i2 == 1) {
                String string2 = googleSearchDialogFragment.A03().getString("image_file");
                ActivityC000900k A0B2 = googleSearchDialogFragment.A0B();
                if (A0B2 != null && !A0B2.isFinishing()) {
                    if (A0B2 instanceof ActivityC13810kN) {
                        googleSearchDialogFragment.A05.Aaz(new AnonymousClass38T((ActivityC13810kN) A0B2, googleSearchDialogFragment.A01, googleSearchDialogFragment.A03, googleSearchDialogFragment.A04, new File(string2)), new Void[0]);
                        return;
                    }
                    str = "GoogleSearchDialogFragment does not have a DialogActivity as a host";
                } else {
                    return;
                }
            } else {
                return;
            }
            AnonymousClass009.A07(str);
        }
    }

    @Override // com.whatsapp.http.Hilt_GoogleSearchDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (!(AnonymousClass12P.A00(context) instanceof ActivityC13810kN)) {
            AnonymousClass009.A07("GoogleSearchDialogFragment does not have a DialogActivity as a host");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        DialogInterface$OnClickListenerC96664fo r1 = new DialogInterface.OnClickListener() { // from class: X.4fo
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                GoogleSearchDialogFragment.A01(GoogleSearchDialogFragment.this, i);
            }
        };
        C004802e r2 = new C004802e(A0C);
        r2.setPositiveButton(R.string.action_search_web, r1);
        r2.setNegativeButton(R.string.cancel, null);
        r2.A06(R.string.quick_message_search_confirmation);
        AnonymousClass04S create = r2.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
