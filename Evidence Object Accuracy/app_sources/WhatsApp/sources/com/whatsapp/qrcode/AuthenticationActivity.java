package com.whatsapp.qrcode;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02N;
import X.AnonymousClass19M;
import X.AnonymousClass21K;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103294qW;
import X.C14330lG;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C22670zS;
import X.C26061Bw;
import X.C83843xz;
import android.os.Bundle;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintView;
import com.whatsapp.util.Log;
import java.security.Signature;

/* loaded from: classes2.dex */
public class AuthenticationActivity extends ActivityC13810kN implements AnonymousClass21K {
    public AnonymousClass02N A00;
    public C22670zS A01;
    public FingerprintView A02;
    public Runnable A03;
    public boolean A04;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public AuthenticationActivity() {
        this(0);
    }

    public AuthenticationActivity(int i) {
        this.A04 = false;
        A0R(new C103294qW(this));
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            this.A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            this.A0B = (AnonymousClass19M) r1.A6R.get();
            this.A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            this.A0D = (C18810t5) r1.AMu.get();
            this.A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            this.A01 = (C22670zS) r1.A0V.get();
        }
    }

    public final void A2T() {
        Log.i("AuthenticationActivity/start-listening");
        this.A02.removeCallbacks(this.A03);
        AnonymousClass02N r2 = new AnonymousClass02N();
        this.A00 = r2;
        C22670zS r1 = this.A01;
        AnonymousClass009.A0F(r1.A04());
        r1.A01.A6I(r2, this);
        FingerprintView fingerprintView = this.A02;
        fingerprintView.A02(fingerprintView.A06);
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        Log.i("AuthenticationActivity/fingerprint-error");
        if (i == 7) {
            Log.i("AuthenticationActivity/fingerprint-error-too-many-attempts");
            charSequence = getString(R.string.fingerprint_lockout_error, 30);
            this.A02.removeCallbacks(this.A03);
            this.A02.postDelayed(this.A03, C26061Bw.A0L);
        }
        this.A02.A03(charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        Log.i("AuthenticationActivity/fingerprint-failed");
        FingerprintView fingerprintView = this.A02;
        fingerprintView.A04(fingerprintView.getContext().getString(R.string.fingerprint_not_recognized));
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        Log.i("AuthenticationActivity/fingerprint-help");
        this.A02.A04(charSequence.toString());
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        Log.i("AuthenticationActivity/fingerprint-success");
        this.A02.A01();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        setResult(0);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!this.A01.A02()) {
            Log.i("AuthenticationActivity/onCreate: setting not enabled");
            setResult(-1);
            finish();
            overridePendingTransition(0, 17432577);
            return;
        }
        setContentView(R.layout.activity_authentication);
        ((TextView) findViewById(R.id.auth_title)).setText(getIntent().getStringExtra("extra_auth_title"));
        FingerprintView fingerprintView = (FingerprintView) findViewById(R.id.fingerprint_view);
        this.A02 = fingerprintView;
        fingerprintView.A00 = new C83843xz(this);
        this.A03 = new RunnableBRunnable0Shape10S0100000_I0_10(this, 1);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        FingerprintView fingerprintView = this.A02;
        if (fingerprintView != null) {
            fingerprintView.A00 = null;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:13:0x0013 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: X.02N */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [com.whatsapp.authentication.FingerprintView, android.view.View] */
    /* JADX WARN: Type inference failed for: r1v2, types: [X.02N] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        Log.i("AuthenticationActivity/stop-listening");
        AnonymousClass02N r1 = this.A02;
        r1.removeCallbacks(this.A03);
        AnonymousClass02N r0 = this.A00;
        if (r0 != null) {
            try {
                r1 = 0;
                r1 = 0;
                r1 = 0;
                try {
                    r0.A01();
                } catch (NullPointerException e) {
                    e.getMessage();
                }
            } finally {
                this.A00 = r1;
            }
        }
    }

    @Override // X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (!this.A01.A02()) {
            Log.i("AuthenticationActivity/not-enrolled");
            setResult(-1);
            finish();
            return;
        }
        A2T();
    }
}
