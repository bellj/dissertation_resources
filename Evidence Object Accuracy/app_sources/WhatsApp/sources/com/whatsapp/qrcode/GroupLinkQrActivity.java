package com.whatsapp.qrcode;

import X.AbstractC14440lR;
import X.AbstractC42911w6;
import X.AbstractC42921w7;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass3ZQ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14330lG;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C17220qS;
import X.C20710wC;
import X.C625337p;
import X.C63803Cz;
import X.C63883Dh;
import X.C88144El;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.R;
import com.whatsapp.RevokeLinkConfirmationDialogFragment;
import com.whatsapp.growthlock.InviteLinkUnavailableDialogFragment;
import com.whatsapp.qrcode.contactqr.ContactQrContactCardView;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class GroupLinkQrActivity extends ActivityC13790kL implements AbstractC42911w6, AbstractC42921w7 {
    public C15550nR A00;
    public AnonymousClass018 A01;
    public C15370n3 A02;
    public C20710wC A03;
    public C15580nU A04;
    public C17220qS A05;
    public C63883Dh A06;
    public ContactQrContactCardView A07;
    public String A08;
    public boolean A09;

    public GroupLinkQrActivity() {
        this(0);
    }

    public GroupLinkQrActivity(int i) {
        this.A09 = false;
        ActivityC13830kP.A1P(this, 96);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A05 = C12990iw.A0c(A1M);
            this.A00 = C12960it.A0O(A1M);
            this.A01 = C12960it.A0R(A1M);
            this.A03 = C12980iv.A0e(A1M);
        }
    }

    public final void A2e(boolean z) {
        if (z) {
            Ady(0, R.string.contact_qr_wait);
        }
        AnonymousClass3ZQ r1 = new AnonymousClass3ZQ(((ActivityC13810kN) this).A05, this.A05, this, z);
        C15580nU r0 = this.A04;
        AnonymousClass009.A05(r0);
        r1.A00(r0);
    }

    @Override // X.AbstractC42921w7
    public void ARo(int i, String str, boolean z) {
        String A0d;
        AaN();
        if (str != null) {
            StringBuilder A0k = C12960it.A0k("invitelink/gotcode/");
            A0k.append(str);
            A0k.append(" recreate:");
            A0k.append(z);
            C12960it.A1F(A0k);
            C20710wC r0 = this.A03;
            r0.A0x.put(this.A04, str);
            this.A08 = str;
            ContactQrContactCardView contactQrContactCardView = this.A07;
            if (TextUtils.isEmpty(str)) {
                A0d = null;
            } else {
                A0d = C12960it.A0d(str, C12960it.A0k("https://chat.whatsapp.com/"));
            }
            contactQrContactCardView.setQrCode(A0d);
            if (z) {
                Ado(R.string.reset_link_complete);
                return;
            }
            return;
        }
        Log.i(C12960it.A0W(i, "invitelink/failed/"));
        if (i == 436) {
            Adm(InviteLinkUnavailableDialogFragment.A00(true, true));
            C20710wC r02 = this.A03;
            r02.A0x.remove(this.A04);
            return;
        }
        ((ActivityC13810kN) this).A05.A07(C88144El.A00(i, this.A03.A0b(this.A04)), 0);
        if (TextUtils.isEmpty(this.A08)) {
            finish();
        }
    }

    @Override // X.AbstractC42911w6
    public void Aat() {
        A2e(true);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String A0d;
        super.onCreate(bundle);
        setContentView(R.layout.group_qr_code);
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        ActivityC13790kL.A0c(this, A0Q, this.A01);
        A0Q.setTitle(R.string.contact_qr_title);
        A0Q.setNavigationOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 16));
        A1e(A0Q);
        setTitle(R.string.settings_qr);
        C15580nU A0U = ActivityC13790kL.A0U(getIntent(), "jid");
        this.A04 = A0U;
        this.A02 = this.A00.A0B(A0U);
        ContactQrContactCardView contactQrContactCardView = (ContactQrContactCardView) findViewById(R.id.group_qr_card);
        this.A07 = contactQrContactCardView;
        contactQrContactCardView.A02(this.A02, true);
        this.A07.setStyle(0);
        boolean A0b = this.A03.A0b(this.A04);
        ContactQrContactCardView contactQrContactCardView2 = this.A07;
        int i = R.string.group_link_qr_prompt;
        if (A0b) {
            i = R.string.parent_group_link_qr_prompt;
        }
        contactQrContactCardView2.setPrompt(getString(i));
        this.A06 = new C63883Dh();
        String A0t = C12970iu.A0t(this.A04, this.A03.A0x);
        this.A08 = A0t;
        if (!TextUtils.isEmpty(A0t)) {
            String str = this.A08;
            ContactQrContactCardView contactQrContactCardView3 = this.A07;
            if (TextUtils.isEmpty(str)) {
                A0d = null;
            } else {
                A0d = C12960it.A0d(str, C12960it.A0k("https://chat.whatsapp.com/"));
            }
            contactQrContactCardView3.setQrCode(A0d);
        }
        A2e(false);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        ActivityC13790kL.A0b(this, menu);
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String A0d;
        String A0d2;
        if (menuItem.getItemId() == R.id.menuitem_contactqr_share) {
            if (this.A08 == null) {
                A2e(false);
                ((ActivityC13810kN) this).A05.A07(R.string.share_failed, 0);
                return true;
            }
            boolean A0b = this.A03.A0b(this.A04);
            A2C(R.string.contact_qr_wait);
            AbstractC14440lR r3 = ((ActivityC13830kP) this).A05;
            C14900mE r12 = ((ActivityC13810kN) this).A05;
            C15570nT r13 = ((ActivityC13790kL) this).A01;
            C14330lG r11 = ((ActivityC13810kN) this).A04;
            int i = R.string.group_qr_email_body_with_link;
            if (A0b) {
                i = R.string.parent_group_qr_email_body_with_link;
            }
            Object[] objArr = new Object[1];
            String str = this.A08;
            if (TextUtils.isEmpty(str)) {
                A0d = null;
            } else {
                A0d = C12960it.A0d(str, C12960it.A0k("https://chat.whatsapp.com/"));
            }
            C625337p r9 = new C625337p(this, r11, r12, r13, C12960it.A0X(this, A0d, objArr, 0, i));
            Bitmap[] bitmapArr = new Bitmap[1];
            C15370n3 r6 = this.A02;
            String str2 = this.A08;
            if (TextUtils.isEmpty(str2)) {
                A0d2 = null;
            } else {
                A0d2 = C12960it.A0d(str2, C12960it.A0k("https://chat.whatsapp.com/"));
            }
            int i2 = R.string.group_link_qr_share_prompt;
            if (A0b) {
                i2 = R.string.parent_group_link_qr_share_prompt;
            }
            bitmapArr[0] = new C63803Cz(r6, getString(i2), A0d2, true).A00(this);
            r3.Aaz(r9, bitmapArr);
            return true;
        } else if (menuItem.getItemId() != R.id.menuitem_contactqr_revoke) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            Adm(RevokeLinkConfirmationDialogFragment.A00(this.A04, true));
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A06.A01(getWindow(), ((ActivityC13810kN) this).A08);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        this.A06.A00(getWindow());
        super.onStop();
    }
}
