package com.whatsapp.qrcode;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C013606j;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C73863gv;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class QrScannerOverlay extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public Paint A05;
    public Drawable A06;
    public C73863gv A07;
    public AnonymousClass2P7 A08;
    public String A09;
    public boolean A0A;
    public final float A0B;
    public final float A0C;
    public final int A0D;
    public final int A0E;
    public final Paint A0F;
    public final Rect A0G;
    public final RectF A0H;

    public QrScannerOverlay(Context context) {
        this(context, null);
    }

    public QrScannerOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public QrScannerOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0A) {
            this.A0A = true;
            generatedComponent();
        }
        this.A0F = C12990iw.A0G(1);
        this.A0G = C12980iv.A0J();
        this.A0H = C12980iv.A0K();
        this.A01 = 0.0f;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0H);
        this.A04 = obtainStyledAttributes.getInt(0, 1);
        int resourceId = obtainStyledAttributes.getResourceId(1, 0);
        if (resourceId != 0) {
            this.A09 = context.getString(resourceId);
            this.A02 = obtainStyledAttributes.getDimensionPixelSize(2, 0);
            this.A03 = obtainStyledAttributes.getDimensionPixelSize(3, 0);
        }
        obtainStyledAttributes.recycle();
        this.A0C = getResources().getDimension(R.dimen.autofocus_stroke_size);
        this.A0B = getResources().getDimension(R.dimen.contact_qr_corner_radius);
        AnonymousClass00T.A00(context, R.color.white_alpha_20);
        this.A0E = AnonymousClass00T.A00(context, R.color.qr_scanline);
        this.A0D = AnonymousClass00T.A00(context, R.color.qr_scanner_overlay_gray);
        if (this.A04 == 1) {
            this.A00 = 0.0125f;
            C013606j A01 = C013606j.A01(null, getResources(), R.drawable.ic_qr_frame);
            AnonymousClass009.A05(A01);
            this.A06 = A01;
            A01.setBounds(0, 0, A01.getIntrinsicWidth(), this.A06.getIntrinsicHeight());
        } else {
            this.A00 = 0.01f;
        }
        Paint A0G = C12990iw.A0G(1);
        this.A05 = A0G;
        C12990iw.A14(A0G, PorterDuff.Mode.CLEAR);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        C73863gv r2 = new C73863gv(this);
        this.A07 = r2;
        r2.setDuration(2000);
        this.A07.setRepeatCount(-1);
        this.A07.setRepeatMode(2);
        startAnimation(this.A07);
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnimation();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int A04 = C12960it.A04(this, getWidth());
        int A03 = C12960it.A03(this);
        int min = (Math.min(A04, A03) * 3) >> 2;
        int paddingLeft = ((A04 - min) >> 1) + getPaddingLeft();
        int paddingTop = ((A03 - min) >> 1) + getPaddingTop();
        int i = paddingLeft + min;
        int i2 = min + paddingTop;
        float f = this.A01;
        float f2 = this.A00;
        float f3 = f + f2;
        this.A01 = f3;
        if (f3 > 1.0f || f3 < 0.0f) {
            if (f3 > 1.0f) {
                this.A01 = 1.0f;
            } else {
                this.A01 = 0.0f;
            }
            this.A00 = -f2;
        }
        canvas.drawColor(this.A0D);
        RectF rectF = this.A0H;
        float f4 = (float) paddingLeft;
        float f5 = (float) paddingTop;
        float f6 = (float) i2;
        rectF.set(f4, f5, (float) i, f6);
        if (this.A04 == 0) {
            canvas.drawArc(rectF, 0.0f, 360.0f, true, this.A05);
            Paint paint = this.A0F;
            C12990iw.A13(paint);
            paint.setColor(this.A0E);
            paint.setAlpha(127);
            float f7 = this.A0C * 2.0f;
            paint.setStrokeWidth(f7);
            int i3 = (int) (f5 + f7);
            i2 = (int) (f6 - f7);
            float f8 = this.A01;
            float f9 = (f8 * 2.0f) - 1.0f;
            float f10 = (float) ((paddingLeft + i) >> 1);
            float sqrt = (((float) (i - paddingLeft)) * ((float) Math.sqrt((double) (1.0f - (f9 * f9))))) / 2.0f;
            float f11 = (((float) (i2 - i3)) * f8) + ((float) i3);
            canvas.drawLine(f10 - sqrt, f11, sqrt + f10, f11, paint);
        } else {
            float f12 = this.A0B;
            canvas.drawRoundRect(rectF, f12, f12, this.A05);
            Drawable drawable = this.A06;
            drawable.setAlpha((int) (this.A01 * 255.0f));
            canvas.save();
            canvas.translate(f4, f5);
            drawable.draw(canvas);
            canvas.translate((float) (i - paddingLeft), 0.0f);
            canvas.rotate(90.0f);
            drawable.draw(canvas);
            canvas.rotate(-90.0f);
            canvas.translate(0.0f, (float) (i2 - paddingTop));
            canvas.rotate(180.0f);
            drawable.draw(canvas);
            canvas.rotate(-180.0f);
            canvas.translate((float) (paddingLeft - i), 0.0f);
            canvas.rotate(270.0f);
            drawable.draw(canvas);
            canvas.restore();
        }
        String str = this.A09;
        if (str != null) {
            Paint paint2 = this.A0F;
            C12970iu.A16(-1, paint2);
            paint2.setTextSize((float) this.A02);
            int length = str.length();
            Rect rect = this.A0G;
            paint2.getTextBounds(str, 0, length, rect);
            canvas.drawText(str, ((float) (A04 - rect.width())) / 2.0f, (float) (i2 + this.A03 + (rect.height() >> 1)), paint2);
        }
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        C73863gv r0;
        if (getVisibility() != 0) {
            clearAnimation();
        } else if (getAnimation() == null && (r0 = this.A07) != null) {
            startAnimation(r0);
        }
    }
}
