package com.whatsapp.qrcode;

import X.AbstractActivityC41101su;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass13R;
import X.AnonymousClass16S;
import X.AnonymousClass16T;
import X.AnonymousClass19M;
import X.AnonymousClass1E2;
import X.AnonymousClass1F2;
import X.AnonymousClass1GD;
import X.AnonymousClass27K;
import X.AnonymousClass27L;
import X.AnonymousClass27W;
import X.AnonymousClass2CH;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103304qX;
import X.C1102254s;
import X.C1102354t;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14890mD;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C15990oG;
import X.C16590pI;
import X.C17220qS;
import X.C18240s8;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21280xA;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22230yk;
import X.C22420z3;
import X.C22670zS;
import X.C233411h;
import X.C245716a;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C25931Bj;
import X.C25941Bk;
import X.C41091ss;
import X.C69443Zg;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.R;
import com.whatsapp.qrcode.DevicePairQrScannerActivity;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class DevicePairQrScannerActivity extends AbstractActivityC41101su {
    public static final long A0R;
    public static final long A0S;
    public C21740xu A00;
    public C25931Bj A01;
    public AnonymousClass16S A02;
    public AnonymousClass16T A03;
    public AnonymousClass13R A04;
    public AnonymousClass27K A05;
    public C25941Bk A06;
    public C233411h A07;
    public C16590pI A08;
    public C15990oG A09;
    public C18240s8 A0A;
    public C22100yW A0B;
    public C245716a A0C;
    public AnonymousClass1E2 A0D;
    public C17220qS A0E;
    public C22420z3 A0F;
    public C22230yk A0G;
    public AnonymousClass27L A0H;
    public DevicePairQrScannerViewModel A0I;
    public AnonymousClass1F2 A0J;
    public C14890mD A0K;
    public C14860mA A0L;
    public Runnable A0M;
    public boolean A0N;
    public final AnonymousClass1GD A0O;
    public final AnonymousClass27W A0P;
    public final Runnable A0Q;

    static {
        TimeUnit timeUnit = TimeUnit.SECONDS;
        A0R = timeUnit.toMillis(6) + 32000;
        A0S = timeUnit.toMillis(4);
    }

    public DevicePairQrScannerActivity() {
        this(0);
        this.A0Q = new RunnableBRunnable0Shape10S0100000_I0_10(this, 4);
        this.A0P = new C69443Zg(this);
        this.A0O = new C41091ss(this);
    }

    public DevicePairQrScannerActivity(int i) {
        this.A0N = false;
        A0R(new C103304qX(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0N) {
            this.A0N = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC41101su) this).A04 = (C21280xA) r1.AMU.get();
            ((AbstractActivityC41101su) this).A02 = (C15890o4) r1.AN1.get();
            this.A08 = (C16590pI) r1.AMg.get();
            this.A00 = (C21740xu) r1.AM1.get();
            this.A0K = (C14890mD) r1.ANL.get();
            this.A0L = (C14860mA) r1.ANU.get();
            this.A0E = (C17220qS) r1.ABt.get();
            this.A0G = (C22230yk) r1.ANT.get();
            this.A0A = (C18240s8) r1.AIt.get();
            this.A07 = (C233411h) r1.AKz.get();
            this.A09 = (C15990oG) r1.AIs.get();
            this.A0D = (AnonymousClass1E2) r1.A0N.get();
            this.A0C = (C245716a) r1.AJR.get();
            this.A0F = (C22420z3) r1.ANM.get();
            this.A0B = (C22100yW) r1.A3g.get();
            this.A02 = (AnonymousClass16S) r1.AL4.get();
            this.A03 = (AnonymousClass16T) r1.AL5.get();
            this.A01 = (C25931Bj) r1.AJQ.get();
            this.A04 = (AnonymousClass13R) r1.ACl.get();
            this.A06 = (C25941Bk) r1.A5k.get();
            this.A0J = (AnonymousClass1F2) r1.A5l.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.network_required_airplane_on || i == R.string.network_required || i == R.string.error_log_in_device) {
            ((AbstractActivityC41101su) this).A03.Aab();
        } else if (i == 1000) {
            finish();
        }
    }

    public final void A2i() {
        Runnable runnable = this.A0M;
        if (runnable != null) {
            ((ActivityC13810kN) this).A00.removeCallbacks(runnable);
        }
        if (this.A0B.A0L.A03()) {
            AaN();
        } else {
            A1g(false);
        }
    }

    public final void A2j() {
        A2i();
        Vibrator A0K = ((ActivityC13810kN) this).A08.A0K();
        AnonymousClass009.A05(A0K);
        A0K.vibrate(75);
        finish();
    }

    @Override // X.AbstractActivityC41101su, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass27K r0;
        super.onCreate(bundle);
        C25941Bk r1 = this.A06;
        if (r1.A02.A0L.A03()) {
            r0 = new C1102354t(r1.A00, r1.A01, r1.A03, r1.A04);
        } else {
            r0 = new C1102254s();
        }
        this.A05 = r0;
        C16590pI r02 = this.A08;
        C14830m7 r03 = ((ActivityC13790kL) this).A05;
        C14850m9 r04 = ((ActivityC13810kN) this).A0C;
        C14900mE r05 = ((ActivityC13810kN) this).A05;
        AbstractC15710nm r06 = ((ActivityC13810kN) this).A03;
        AbstractC14440lR r07 = ((ActivityC13830kP) this).A05;
        C14890mD r15 = this.A0K;
        C14860mA r14 = this.A0L;
        C17220qS r13 = this.A0E;
        C18240s8 r12 = this.A0A;
        C22230yk r11 = this.A0G;
        C233411h r10 = this.A07;
        C15990oG r9 = this.A09;
        C14820m6 r8 = ((ActivityC13810kN) this).A09;
        C22420z3 r7 = this.A0F;
        this.A0H = new AnonymousClass27L(r06, r05, this.A02, this.A03, r10, r03, r02, r8, r9, r12, this.A0B, r04, r13, r7, r11, this.A0P, this.A0J, r07, r15, r14);
        TextView textView = (TextView) findViewById(R.id.hint);
        textView.setVisibility(0);
        textView.setText(Html.fromHtml(getString(R.string.qr_code_hint, "web.whatsapp.com")));
        this.A0B.A03(this.A0O);
        AnonymousClass13R r08 = this.A04;
        synchronized (r08.A04) {
            r08.A00 = false;
        }
        getIntent().getIntExtra("entry_point", 1);
        getIntent().getStringExtra("agent_id");
        DevicePairQrScannerViewModel devicePairQrScannerViewModel = (DevicePairQrScannerViewModel) new AnonymousClass02A(this).A00(DevicePairQrScannerViewModel.class);
        this.A0I = devicePairQrScannerViewModel;
        devicePairQrScannerViewModel.A03.A05(this, new AnonymousClass02B() { // from class: X.4tN
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                DevicePairQrScannerActivity.this.A2j();
                throw C12970iu.A0z();
            }
        });
        this.A0I.A04.A05(this, new AnonymousClass02B() { // from class: X.4tO
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                DevicePairQrScannerActivity.this.A2j();
            }
        });
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0B.A04(this.A0O);
        AnonymousClass2CH r1 = this.A0H.A01;
        if (r1 != null) {
            C14860mA r0 = r1.A08;
            r0.A0R.remove(r1.A07);
        }
        AnonymousClass13R r2 = this.A04;
        synchronized (r2.A04) {
            r2.A00 = true;
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }
}
