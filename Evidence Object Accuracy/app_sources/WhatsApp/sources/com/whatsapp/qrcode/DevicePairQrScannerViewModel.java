package com.whatsapp.qrcode;

import X.AbstractC14440lR;
import X.AnonymousClass014;
import X.AnonymousClass1E2;
import X.C245716a;
import X.C26221Cm;
import X.C27691It;
import android.app.Application;

/* loaded from: classes3.dex */
public class DevicePairQrScannerViewModel extends AnonymousClass014 {
    public final C245716a A00;
    public final AnonymousClass1E2 A01;
    public final C26221Cm A02;
    public final C27691It A03 = new C27691It();
    public final C27691It A04 = new C27691It();
    public final AbstractC14440lR A05;

    public DevicePairQrScannerViewModel(Application application, C245716a r3, AnonymousClass1E2 r4, C26221Cm r5, AbstractC14440lR r6) {
        super(application);
        this.A05 = r6;
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
    }
}
