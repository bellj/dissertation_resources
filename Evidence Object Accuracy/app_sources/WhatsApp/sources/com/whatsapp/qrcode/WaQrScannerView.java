package com.whatsapp.qrcode;

import X.AnonymousClass004;
import X.AnonymousClass27M;
import X.AnonymousClass27N;
import X.AnonymousClass27Z;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C14850m9;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import java.util.Map;

/* loaded from: classes2.dex */
public class WaQrScannerView extends FrameLayout implements AnonymousClass27N, AnonymousClass004 {
    public C14850m9 A00;
    public AnonymousClass27N A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public WaQrScannerView(Context context) {
        super(context);
        A00();
        A01();
    }

    public WaQrScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        A00();
        A01();
    }

    public WaQrScannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01();
    }

    public void A00() {
        if (!this.A03) {
            this.A03 = true;
            this.A00 = (C14850m9) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.A04.get();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [com.whatsapp.qrcode.WaQrScannerView, android.view.View, android.view.ViewGroup] */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.whatsapp.qrcode.QrScannerViewV2] */
    public final void A01() {
        AnonymousClass27M r0;
        boolean A07 = this.A00.A07(349);
        Context context = getContext();
        if (A07) {
            r0 = new QrScannerViewV2(context);
        } else {
            r0 = new AnonymousClass27M(context);
        }
        addView(r0);
        this.A01 = r0;
    }

    @Override // X.AnonymousClass27N
    public boolean AK9() {
        return this.A01.AK9();
    }

    @Override // X.AnonymousClass27N
    public void Aab() {
        this.A01.Aab();
    }

    @Override // X.AnonymousClass27N
    public void Aao() {
        this.A01.Aao();
    }

    @Override // X.AnonymousClass27N
    public boolean Aee() {
        return this.A01.Aee();
    }

    @Override // X.AnonymousClass27N
    public void Af2() {
        this.A01.Af2();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // X.AnonymousClass27N
    public void setQrDecodeHints(Map map) {
        this.A01.setQrDecodeHints(map);
    }

    @Override // X.AnonymousClass27N
    public void setQrScannerCallback(AnonymousClass27Z r2) {
        this.A01.setQrScannerCallback(r2);
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        ((View) this.A01).setVisibility(i);
    }
}
