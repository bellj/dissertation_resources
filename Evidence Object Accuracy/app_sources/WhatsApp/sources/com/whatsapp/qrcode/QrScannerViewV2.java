package com.whatsapp.qrcode;

import X.AbstractC467527b;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass0MU;
import X.AnonymousClass1s9;
import X.AnonymousClass27N;
import X.AnonymousClass27X;
import X.AnonymousClass27Z;
import X.AnonymousClass2BK;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2U2;
import X.C14850m9;
import X.C16630pM;
import X.C467427a;
import X.C73593gU;
import X.View$OnTouchListenerC101434nW;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.util.Log;
import java.util.Map;

/* loaded from: classes2.dex */
public class QrScannerViewV2 extends FrameLayout implements AnonymousClass27N, AnonymousClass004 {
    public AbstractC467527b A00;
    public AnonymousClass1s9 A01;
    public AnonymousClass01d A02;
    public C14850m9 A03;
    public C16630pM A04;
    public AnonymousClass27Z A05;
    public AnonymousClass2P7 A06;
    public boolean A07;
    public final Handler A08;

    @Override // X.AnonymousClass27N
    public void Aab() {
    }

    @Override // X.AnonymousClass27N
    public void Aao() {
    }

    public QrScannerViewV2(Context context) {
        super(context);
        A00();
        this.A08 = new Handler(Looper.getMainLooper());
        this.A00 = new C467427a(this);
        A01();
    }

    public QrScannerViewV2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A08 = new Handler(Looper.getMainLooper());
        this.A00 = new C467427a(this);
        A01();
    }

    public QrScannerViewV2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A08 = new Handler(Looper.getMainLooper());
        this.A00 = new C467427a(this);
        A01();
    }

    public QrScannerViewV2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A03 = (C14850m9) r1.A04.get();
            this.A02 = (AnonymousClass01d) r1.ALI.get();
            this.A04 = (C16630pM) r1.AIc.get();
        }
    }

    public final void A01() {
        AnonymousClass1s9 r1;
        Context context = getContext();
        if (this.A03.A07(125)) {
            r1 = AnonymousClass2U2.A00(context, AnonymousClass2BK.A02(this.A02, this.A04));
            if (r1 != null) {
                Log.i("QrScannerViewV2/LiteCameraView");
                this.A01 = r1;
                r1.setQrScanningEnabled(true);
                AnonymousClass1s9 r12 = this.A01;
                r12.setCameraCallback(this.A00);
                View view = (View) r12;
                setupTapToFocus(view);
                addView(view);
            }
        }
        Log.i("QrScannerViewV2/CameraView");
        r1 = new AnonymousClass27X(context, null);
        this.A01 = r1;
        r1.setQrScanningEnabled(true);
        AnonymousClass1s9 r12 = this.A01;
        r12.setCameraCallback(this.A00);
        View view = (View) r12;
        setupTapToFocus(view);
        addView(view);
    }

    @Override // X.AnonymousClass27N
    public boolean AK9() {
        return this.A01.AK9();
    }

    @Override // X.AnonymousClass27N
    public boolean Aee() {
        return this.A01.Aee();
    }

    @Override // X.AnonymousClass27N
    public void Af2() {
        this.A01.Af2();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        AnonymousClass1s9 r0 = this.A01;
        if (i == 0) {
            r0.Aar();
            this.A01.A7E();
            return;
        }
        r0.pause();
    }

    @Override // X.AnonymousClass27N
    public void setQrDecodeHints(Map map) {
        this.A01.setQrDecodeHints(map);
    }

    @Override // X.AnonymousClass27N
    public void setQrScannerCallback(AnonymousClass27Z r1) {
        this.A05 = r1;
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        ((View) this.A01).setVisibility(i);
    }

    private void setupTapToFocus(View view) {
        view.setOnTouchListener(new View$OnTouchListenerC101434nW(new AnonymousClass0MU(getContext(), new C73593gU(this)), this));
    }
}
