package com.whatsapp.qrcode.contactqr;

import X.AbstractC14730lx;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass17S;
import X.AnonymousClass2FL;
import X.AnonymousClass2GV;
import X.AnonymousClass34O;
import X.AnonymousClass34P;
import X.C1095652e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15680nj;
import X.C17070qD;
import X.C22190yg;
import X.C22260yn;
import X.C22410z2;
import X.C22610zM;
import X.C22710zW;
import X.C250918b;
import X.C253318z;
import X.C26311Cv;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ContactQrActivity extends AnonymousClass34O implements AbstractC14730lx {
    public boolean A00;

    public ContactQrActivity() {
        this(0);
    }

    public ContactQrActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 99);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass34P) this).A0J = C12970iu.A0b(A1M);
            ((AnonymousClass34P) this).A03 = (AnonymousClass17S) A1M.A0F.get();
            ((AnonymousClass34P) this).A05 = C12990iw.A0T(A1M);
            ((AnonymousClass34P) this).A09 = C12960it.A0O(A1M);
            this.A0T = (C22190yg) A1M.AB6.get();
            ((AnonymousClass34P) this).A0C = C12960it.A0P(A1M);
            ((AnonymousClass34P) this).A04 = (C22260yn) A1M.A5I.get();
            ((AnonymousClass34P) this).A0N = (C17070qD) A1M.AFC.get();
            ((AnonymousClass34P) this).A0D = (C253318z) A1M.A4B.get();
            ((AnonymousClass34P) this).A0K = C12990iw.A0c(A1M);
            ((AnonymousClass34P) this).A0G = C12960it.A0R(A1M);
            ((AnonymousClass34P) this).A0B = C12980iv.A0a(A1M);
            ((AnonymousClass34P) this).A0F = C12970iu.A0Y(A1M);
            ((AnonymousClass34P) this).A0I = (C15680nj) A1M.A4e.get();
            ((AnonymousClass34P) this).A0M = (C22710zW) A1M.AF7.get();
            ((AnonymousClass34P) this).A0L = (C22410z2) A1M.ANH.get();
            ((AnonymousClass34P) this).A08 = C12970iu.A0V(A1M);
            ((AnonymousClass34P) this).A0A = (C26311Cv) A1M.AAQ.get();
            ((AnonymousClass34P) this).A0H = (C22610zM) A1M.A6b.get();
            ((AnonymousClass34P) this).A07 = (C250918b) A1M.A2B.get();
            ((AnonymousClass34P) this).A0E = C12990iw.A0a(A1M);
        }
    }

    @Override // X.AnonymousClass34P
    public void A2e() {
        super.A2e();
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(1);
        }
        this.A0U = C12980iv.A0p(((ActivityC13810kN) this).A09.A00, "contact_qr_code");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        ActivityC13790kL.A0b(this, menu);
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_contactqr_share) {
            A2f();
            return true;
        } else if (menuItem.getItemId() != R.id.menuitem_contactqr_revoke) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            A2L(new AnonymousClass2GV() { // from class: X.52f
                @Override // X.AnonymousClass2GV
                public final void AO1() {
                    AnonymousClass34O.this.A2g(true);
                }
            }, new C1095652e(this), R.string.contact_qr_revoke_title, R.string.contact_qr_revoke_subtitle, R.string.contact_qr_revoke_ok_button, R.string.contact_qr_revoke_cancel_button);
            return true;
        }
    }
}
