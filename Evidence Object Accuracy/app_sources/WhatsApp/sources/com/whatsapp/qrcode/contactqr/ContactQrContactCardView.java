package com.whatsapp.qrcode.contactqr;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass12F;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass1M2;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4AB;
import X.AnonymousClass4CK;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15370n3;
import X.C15570nT;
import X.C15580nU;
import X.C15610nY;
import X.C20710wC;
import X.C22700zV;
import X.C27531Hw;
import X.C28801Pb;
import X.C49152Jn;
import X.EnumC49142Jm;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.QrImageView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.util.Log;
import java.util.EnumMap;

/* loaded from: classes2.dex */
public class ContactQrContactCardView extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public C15570nT A02;
    public QrImageView A03;
    public C28801Pb A04;
    public C28801Pb A05;
    public C28801Pb A06;
    public WaTextView A07;
    public ThumbnailButton A08;
    public AnonymousClass130 A09;
    public C22700zV A0A;
    public C15610nY A0B;
    public AnonymousClass131 A0C;
    public AnonymousClass018 A0D;
    public C20710wC A0E;
    public AnonymousClass12F A0F;
    public AnonymousClass2P7 A0G;
    public boolean A0H;

    public ContactQrContactCardView(Context context) {
        super(context);
        A00();
        A01(context);
    }

    public ContactQrContactCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(context);
    }

    public ContactQrContactCardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(context);
    }

    public ContactQrContactCardView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A02 = C12970iu.A0S(A00);
            this.A09 = C12990iw.A0Y(A00);
            this.A0B = C12960it.A0P(A00);
            this.A0D = C12960it.A0R(A00);
            this.A0E = C12980iv.A0e(A00);
            this.A0F = C12990iw.A0f(A00);
            this.A0A = C12980iv.A0a(A00);
            this.A0C = (AnonymousClass131) A00.A49.get();
        }
    }

    public final void A01(Context context) {
        LinearLayout.inflate(context, R.layout.contact_qr_contact_card, this);
        this.A08 = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.profile_picture);
        this.A06 = new C28801Pb(this, this.A0B, this.A0F, (int) R.id.title);
        this.A04 = new C28801Pb(this, this.A0B, this.A0F, (int) R.id.custom_url);
        this.A05 = new C28801Pb(this, this.A0B, this.A0F, (int) R.id.subtitle);
        this.A00 = AnonymousClass028.A0D(this, R.id.qr_code_container);
        this.A03 = (QrImageView) AnonymousClass028.A0D(this, R.id.qr_code);
        this.A07 = C12960it.A0N(this, R.id.prompt);
        this.A01 = AnonymousClass028.A0D(this, R.id.qr_shadow);
    }

    public void A02(C15370n3 r6, boolean z) {
        C28801Pb r2;
        Context context;
        int i;
        if (!r6.A0X || !z) {
            this.A09.A06(this.A08, r6);
        } else {
            this.A08.setImageBitmap(this.A0C.A00(getContext(), r6, (float) getResources().getDimensionPixelSize(R.dimen.contact_qr_avatar_radius), getResources().getDimensionPixelSize(R.dimen.contact_qr_avatar_size)));
        }
        if (r6.A0K()) {
            this.A06.A08(this.A0B.A04(r6));
            boolean A0b = this.A0E.A0b((C15580nU) r6.A0B(C15580nU.class));
            r2 = this.A05;
            context = getContext();
            i = R.string.group_qr_share_subtitle;
            if (A0b) {
                i = R.string.parent_group_qr_share_subtitle;
            }
        } else if (!r6.A0J()) {
            this.A06.A08(r6.A0U);
            r2 = this.A05;
            context = getContext();
            i = R.string.contact_qr_share_subtitle;
        } else {
            AnonymousClass1M2 A00 = this.A0A.A00(C15370n3.A05(r6));
            if (r6.A0L() || (A00 != null && A00.A03 == 3)) {
                this.A06.A08(r6.A0U);
                this.A06.A05(1);
                r2 = this.A05;
                context = getContext();
                i = R.string.business_info_official_business_account;
            } else {
                this.A06.A08(r6.A0U);
                r2 = this.A05;
                context = getContext();
                i = R.string.message_qr_whatsapp_business_account;
            }
        }
        r2.A08(context.getString(i));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0G;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0G = r0;
        }
        return r0.generatedComponent();
    }

    public void setCustomUrl(String str) {
        this.A04.A08(str);
    }

    public void setCustomUrlVisible(boolean z) {
        C28801Pb r0 = this.A04;
        r0.A01.setVisibility(C12960it.A02(z ? 1 : 0));
    }

    public void setPrompt(String str) {
        this.A07.setText(str);
    }

    public void setQrCode(String str) {
        try {
            this.A03.setQrCode(C49152Jn.A00(EnumC49142Jm.A03, str, new EnumMap(AnonymousClass4AB.class)), null);
            this.A03.invalidate();
        } catch (AnonymousClass4CK e) {
            Log.e("ContactQrContactCardView/failed to set QR code", e);
        }
    }

    public void setStyle(int i) {
        C27531Hw.A06(this.A06.A01);
        if (i == 1) {
            C12970iu.A18(getContext(), this, R.color.contact_qr_share_card_background_color);
            setPadding(0, getResources().getDimensionPixelOffset(R.dimen.contact_qr_share_card_padding_top), 0, getPaddingBottom());
            C12970iu.A0H(this.A07).setMargins(0, this.A07.getResources().getDimensionPixelSize(R.dimen.contact_qr_share_card_prompt_margin_top), 0, 0);
            WaTextView waTextView = this.A07;
            waTextView.setTextSize(0, (float) waTextView.getResources().getDimensionPixelSize(R.dimen.contact_qr_share_card_prompt_text_size));
            C12960it.A0s(getContext(), this.A07, R.color.white_alpha_54);
            this.A01.setVisibility(0);
            return;
        }
        C12960it.A0r(getContext(), this.A00, R.string.accessibility_my_qr_code);
    }
}
