package com.whatsapp.qrcode.contactqr;

import X.AbstractC14440lR;
import X.AbstractC14730lx;
import X.AbstractC36671kL;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass1lT;
import X.C14830m7;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15890o4;
import X.C16120oU;
import X.C20730wE;
import X.C21270x9;
import X.C22700zV;
import X.C248917h;
import X.C254219i;
import X.C26311Cv;
import X.C27131Gd;
import X.C28801Pb;
import X.C28811Pc;
import X.C43661xO;
import X.C48182Et;
import X.C51122Sx;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ScannedCodeDialogFragment extends Hilt_ScannedCodeDialogFragment {
    public int A00;
    public View.OnClickListener A01 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 17);
    public View.OnClickListener A02 = new ViewOnClickCListenerShape3S0100000_I0_3(this, 19);
    public ImageView A03;
    public C15570nT A04;
    public C15450nH A05;
    public C15550nR A06;
    public AnonymousClass10S A07;
    public C26311Cv A08;
    public C22700zV A09;
    public C15610nY A0A;
    public AnonymousClass1J1 A0B;
    public C21270x9 A0C;
    public C20730wE A0D;
    public AnonymousClass01d A0E;
    public C14830m7 A0F;
    public C15890o4 A0G;
    public AnonymousClass018 A0H;
    public C15370n3 A0I;
    public AnonymousClass19M A0J;
    public C16120oU A0K;
    public UserJid A0L;
    public AbstractC14730lx A0M;
    public AnonymousClass12F A0N;
    public AnonymousClass198 A0O;
    public C254219i A0P;
    public AbstractC14440lR A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public final C27131Gd A0U = new AnonymousClass1lT(this);

    public static ScannedCodeDialogFragment A00(C43661xO r4, C48182Et r5) {
        String str;
        ScannedCodeDialogFragment scannedCodeDialogFragment = new ScannedCodeDialogFragment();
        Bundle bundle = new Bundle();
        int i = r5.A01;
        int i2 = 0;
        if (i != 0) {
            i2 = 1;
            if (i != 1) {
                i2 = 2;
                if (i != 2) {
                    throw new IllegalArgumentException("Unhandled type");
                }
            }
        }
        bundle.putInt("ARG_TYPE", i2);
        UserJid userJid = r5.A02;
        if (userJid != null) {
            str = userJid.getRawString();
        } else {
            str = null;
        }
        bundle.putString("ARG_JID", str);
        bundle.putString("ARG_MESSAGE", r5.A03);
        bundle.putString("ARG_SOURCE", r4.A03);
        bundle.putString("ARG_QR_CODE_ID", r4.A02);
        scannedCodeDialogFragment.A0U(bundle);
        return scannedCodeDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A07.A04(this.A0U);
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i == 1) {
            if (i2 == -1) {
                this.A0D.A06();
                A0v(C14960mK.A02(A0C()).addFlags(603979776));
                Intent A0i = new C14960mK().A0i(A01(), this.A0L);
                A0i.putExtra("added_by_qr_code", true);
                C51122Sx.A00(A0i, this);
            }
            A1B();
            this.A0O.A00();
            return;
        }
        super.A0t(i, i2, intent);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0D;
        int i;
        Bundle A03 = A03();
        this.A00 = A03.getInt("ARG_TYPE");
        this.A0L = UserJid.getNullable(A03.getString("ARG_JID"));
        this.A0S = A03.getString("ARG_MESSAGE");
        this.A0R = A03.getString("ARG_SOURCE");
        this.A0T = A03.getString("ARG_QR_CODE_ID");
        C15550nR r1 = this.A06;
        UserJid userJid = this.A0L;
        AnonymousClass009.A05(userJid);
        this.A0I = r1.A0B(userJid);
        boolean A0F = this.A04.A0F(this.A0L);
        View inflate = A0C().getLayoutInflater().inflate(R.layout.view_scanned_qr_code, (ViewGroup) null);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.title);
        TextView textView2 = (TextView) AnonymousClass028.A0D(inflate, R.id.positive_button);
        this.A03 = (ImageView) AnonymousClass028.A0D(inflate, R.id.profile_picture);
        View A0D2 = AnonymousClass028.A0D(inflate, R.id.contact_info);
        TextView textView3 = (TextView) AnonymousClass028.A0D(inflate, R.id.result_title);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(inflate, R.id.result_subtitle);
        if (!this.A0I.A0H()) {
            textView3.setText(this.A0H.A0G(C248917h.A04(this.A0L)));
            String A06 = this.A0A.A06(this.A0I);
            if (A06 != null) {
                textEmojiLabel.A0G(null, A06);
            } else {
                textEmojiLabel.setVisibility(8);
            }
        } else {
            C28801Pb r4 = new C28801Pb(A0D2, this.A0A, this.A0N, (int) R.id.result_title);
            textView3.setText(AbstractC36671kL.A03(A0p(), textView3.getPaint(), this.A0J, this.A0I.A0D()));
            r4.A05(1);
            textEmojiLabel.setText(A0I(R.string.business_info_official_business_account));
        }
        this.A0B.A06(this.A03, this.A0I);
        int i2 = this.A00;
        if (i2 == 0) {
            textView.setText(A0I(R.string.qr_title_add_account));
            if (A0F) {
                textView2.setText(A0I(R.string.ok));
                textView2.setOnClickListener(this.A02);
                return inflate;
            }
            C28811Pc r12 = this.A0I.A0C;
            int i3 = R.string.contact_qr_add_contact_add;
            if (r12 != null) {
                i3 = R.string.contact_qr_contact_message;
            }
            textView2.setText(A0I(i3));
            textView2.setOnClickListener(this.A01);
            A0D = AnonymousClass028.A0D(inflate, R.id.details_row);
            i = 18;
        } else if (i2 == 1) {
            A1B();
            return inflate;
        } else if (i2 == 2) {
            textView.setText(A0I(R.string.qr_title_add_account));
            textView2.setText(R.string.message_qr_continue_to_chat);
            textView2.setOnClickListener(this.A01);
            A0D = AnonymousClass028.A0D(inflate, R.id.details_row);
            i = 16;
        } else {
            throw new IllegalArgumentException("Unhandled type");
        }
        A0D.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, i));
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        this.A0B.A00();
    }

    @Override // com.whatsapp.qrcode.contactqr.Hilt_ScannedCodeDialogFragment, com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC14730lx) {
            this.A0M = (AbstractC14730lx) context;
        }
        this.A07.A03(this.A0U);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A0B = this.A0C.A04(A01(), "scanned-code-dialog-fragment");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AbstractC14730lx r0 = this.A0M;
        if (r0 != null) {
            r0.AUT();
        }
    }
}
