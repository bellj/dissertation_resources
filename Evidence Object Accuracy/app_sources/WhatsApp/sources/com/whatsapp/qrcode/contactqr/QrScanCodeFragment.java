package com.whatsapp.qrcode.contactqr;

import X.AnonymousClass018;
import X.AnonymousClass23N;
import X.C14820m6;
import X.C14900mE;
import X.C21280xA;
import X.C42791vs;
import X.C69473Zj;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.qrcode.QrEducationDialogFragment;
import com.whatsapp.qrcode.QrScannerOverlay;
import com.whatsapp.qrcode.WaQrScannerView;

/* loaded from: classes2.dex */
public class QrScanCodeFragment extends Hilt_QrScanCodeFragment {
    public View A00;
    public ImageView A01;
    public C14900mE A02;
    public C14820m6 A03;
    public AnonymousClass018 A04;
    public QrScannerOverlay A05;
    public WaQrScannerView A06;
    public C21280xA A07;
    public String A08;
    public boolean A09 = false;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final Runnable A0D = new RunnableBRunnable0Shape10S0100000_I0_10(this, 16);
    public final Runnable A0E = new RunnableBRunnable0Shape10S0100000_I0_10(this, 17);

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        this.A02.A0G(this.A0D);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.contact_qr_scan_code, viewGroup, false);
        this.A06 = (WaQrScannerView) inflate.findViewById(R.id.qr_scanner_view);
        this.A05 = (QrScannerOverlay) inflate.findViewById(R.id.overlay);
        this.A00 = inflate.findViewById(R.id.qr_scan_from_gallery);
        this.A01 = (ImageView) inflate.findViewById(R.id.qr_scan_flash);
        this.A0A = this.A03.A00.getBoolean("contact_qr_education", true);
        this.A01.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 14));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 15));
        WaQrScannerView waQrScannerView = this.A06;
        waQrScannerView.setQrScannerCallback(new C69473Zj(this));
        waQrScannerView.setContentDescription(A0I(R.string.contact_qr_scan_a_qr_code));
        AnonymousClass23N.A02(this.A06, R.string.accessibility_action_camera_focus);
        this.A06.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 13));
        A1C();
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        this.A02.A0G(this.A0D);
        super.A11();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (this.A0C && !this.A0B && !this.A0A) {
            this.A02.A0J(this.A0D, 15000);
        }
    }

    public void A1A() {
        this.A02.A0G(this.A0E);
        this.A0C = true;
        A1C();
        C14900mE r0 = this.A02;
        Runnable runnable = this.A0D;
        r0.A0G(runnable);
        if (this.A0A) {
            if (A0e()) {
                C42791vs.A01(new QrEducationDialogFragment(), A0E());
                this.A09 = true;
            }
        } else if (!this.A0B) {
            this.A02.A0J(runnable, 15000);
        }
    }

    public final void A1B() {
        boolean Aee = this.A06.A01.Aee();
        ImageView imageView = this.A01;
        if (Aee) {
            imageView.setVisibility(0);
            boolean AK9 = this.A06.A01.AK9();
            ImageView imageView2 = this.A01;
            int i = R.drawable.flash_off;
            if (AK9) {
                i = R.drawable.flash_on;
            }
            imageView2.setImageResource(i);
            ImageView imageView3 = this.A01;
            int i2 = R.string.flash_off_action;
            if (!AK9) {
                i2 = R.string.flash_on_action;
            }
            imageView3.setContentDescription(A0I(i2));
            return;
        }
        imageView.setVisibility(8);
    }

    public final void A1C() {
        WaQrScannerView waQrScannerView = this.A06;
        if (waQrScannerView != null) {
            int i = 0;
            int i2 = 8;
            if (this.A0C) {
                i2 = 0;
            }
            waQrScannerView.setVisibility(i2);
            QrScannerOverlay qrScannerOverlay = this.A05;
            if (!this.A0C) {
                i = 8;
            }
            qrScannerOverlay.setVisibility(i);
        }
    }
}
