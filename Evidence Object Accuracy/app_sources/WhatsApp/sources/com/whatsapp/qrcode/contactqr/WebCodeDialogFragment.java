package com.whatsapp.qrcode.contactqr;

import X.AbstractC14730lx;
import X.AnonymousClass17R;
import X.C004802e;
import X.C14840m8;
import X.C15450nH;
import X.C245716a;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.WebCodeDialogFragment;

/* loaded from: classes2.dex */
public class WebCodeDialogFragment extends Hilt_WebCodeDialogFragment {
    public C15450nH A00;
    public C245716a A01;
    public C14840m8 A02;
    public AbstractC14730lx A03;
    public AnonymousClass17R A04;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        this.A03 = null;
        super.A0l();
    }

    @Override // com.whatsapp.qrcode.contactqr.Hilt_WebCodeDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC14730lx) {
            this.A03 = (AbstractC14730lx) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e r2 = new C004802e(A01());
        r2.A07(R.string.qr_dialog_title);
        r2.A06(R.string.qr_dialog_content);
        r2.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener() { // from class: X.4fx
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                WebCodeDialogFragment webCodeDialogFragment = WebCodeDialogFragment.this;
                webCodeDialogFragment.A0v(C65303Iz.A02(webCodeDialogFragment.A01()));
            }
        });
        r2.setNegativeButton(R.string.cancel, null);
        return r2.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AbstractC14730lx r0 = this.A03;
        if (r0 != null) {
            r0.AUT();
        }
    }
}
