package com.whatsapp.qrcode.contactqr;

import X.AbstractC14730lx;
import X.C004802e;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ErrorDialogFragment extends Hilt_ErrorDialogFragment {
    public AbstractC14730lx A00;

    public static ErrorDialogFragment A00(int i) {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ARG_ERROR_CODE", i);
        errorDialogFragment.A0U(bundle);
        return errorDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A00 = null;
    }

    @Override // com.whatsapp.qrcode.contactqr.Hilt_ErrorDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC14730lx) {
            this.A00 = (AbstractC14730lx) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        int i;
        int i2 = A03().getInt("ARG_ERROR_CODE");
        C004802e r4 = new C004802e(A01());
        r4.setPositiveButton(R.string.ok, null);
        switch (i2) {
            case 2:
                r4.A07(R.string.contact_qr_valid_unsupported_title);
                r4.A0A(A0J(R.string.contact_qr_valid_unsupported_subtitle_website, "https://whatsapp.com/android"));
                break;
            case 3:
                i = R.string.contact_qr_scan_no_connection;
                r4.A06(i);
                break;
            case 4:
                i = R.string.qr_scan_with_web_scanner;
                r4.A06(i);
                break;
            case 5:
                i = R.string.qr_scan_with_payments_scanner;
                r4.A06(i);
                break;
            case 6:
                i = R.string.contact_qr_scan_toast_no_valid_code;
                r4.A06(i);
                break;
            case 7:
                i = R.string.invalid_custom_url_dialog;
                r4.A06(i);
                break;
            default:
                i = R.string.contact_qr_scan_invalid_dialog;
                r4.A06(i);
                break;
        }
        return r4.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AbstractC14730lx r0 = this.A00;
        if (r0 != null) {
            r0.AUT();
        }
    }
}
