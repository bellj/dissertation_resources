package com.whatsapp.qrcode.contactqr;

import X.AbstractActivityC58502pa;
import X.AbstractC14440lR;
import X.AbstractC14730lx;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01d;
import X.AnonymousClass17S;
import X.AnonymousClass18U;
import X.AnonymousClass2JY;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15680nj;
import X.C16120oU;
import X.C17070qD;
import X.C17170qN;
import X.C17220qS;
import X.C18640sm;
import X.C22260yn;
import X.C22410z2;
import X.C22610zM;
import X.C22700zV;
import X.C22710zW;
import X.C250918b;
import X.C251118d;
import X.C253318z;
import X.C25951Bl;
import X.C26311Cv;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class QrSheetDeepLinkActivity extends AbstractActivityC58502pa implements AbstractC14730lx {
    public AnonymousClass17S A00;
    public C22260yn A01;
    public AnonymousClass18U A02;
    public C250918b A03;
    public C251118d A04;
    public C15550nR A05;
    public C26311Cv A06;
    public C22700zV A07;
    public C15610nY A08;
    public C253318z A09;
    public C17170qN A0A;
    public C22610zM A0B;
    public C15680nj A0C;
    public C25951Bl A0D;
    public C16120oU A0E;
    public C17220qS A0F;
    public C22410z2 A0G;
    public C22710zW A0H;
    public C17070qD A0I;
    public AnonymousClass2JY A0J;
    public String A0K;

    @Override // X.AbstractC14730lx
    public void AUT() {
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C14830m7 r0 = ((ActivityC13790kL) this).A05;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        C15570nT r04 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r05 = ((ActivityC13830kP) this).A05;
        C16120oU r06 = this.A0E;
        AnonymousClass17S r07 = this.A00;
        C15450nH r08 = ((ActivityC13810kN) this).A06;
        AnonymousClass18U r09 = this.A02;
        C17220qS r010 = this.A0F;
        C15550nR r011 = this.A05;
        AnonymousClass01d r012 = ((ActivityC13810kN) this).A08;
        C15610nY r013 = this.A08;
        C22260yn r15 = this.A01;
        C17070qD r14 = this.A0I;
        C253318z r13 = this.A09;
        C22700zV r11 = this.A07;
        C15680nj r10 = this.A0C;
        C22710zW r9 = this.A0H;
        C22410z2 r8 = this.A0G;
        C251118d r7 = this.A04;
        C18640sm r6 = ((ActivityC13810kN) this).A07;
        C26311Cv r5 = this.A06;
        C22610zM r4 = this.A0B;
        AnonymousClass2JY r014 = new AnonymousClass2JY(r07, r15, this, r03, r09, r04, r08, this.A03, r7, r011, r5, r11, r013, r13, r6, r012, r0, this.A0A, r4, r10, r02, r06, r010, r8, r9, r14, r05, null, false, false);
        this.A0J = r014;
        r014.A01 = getIntent().getStringExtra("extra_deep_link_session_id");
        this.A0J.A02 = true;
        this.A0K = getIntent().getStringExtra("qrcode");
        boolean booleanExtra = getIntent().getBooleanExtra("from_internal_deep_link_click", false);
        String str = this.A0K;
        if (str != null && !this.A0J.A0Z) {
            this.A0K = str;
            this.A0J.A02(str, 5, false, booleanExtra);
        }
    }
}
