package com.whatsapp.qrcode.contactqr;

import X.AnonymousClass028;
import X.C12960it;
import X.C15570nT;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ContactQrMyCodeFragment extends Hilt_ContactQrMyCodeFragment {
    public C15570nT A00;
    public ContactQrContactCardView A01;
    public String A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.contact_qr_my_code, viewGroup, false);
        ContactQrContactCardView contactQrContactCardView = (ContactQrContactCardView) AnonymousClass028.A0D(inflate, R.id.contact_qr_card);
        this.A01 = contactQrContactCardView;
        contactQrContactCardView.setStyle(0);
        ContactQrContactCardView contactQrContactCardView2 = this.A01;
        C15570nT r0 = this.A00;
        r0.A08();
        contactQrContactCardView2.A02(r0.A01, true);
        this.A01.setPrompt(A0I(R.string.contact_qr_prompt));
        ContactQrContactCardView contactQrContactCardView3 = this.A01;
        if (!(contactQrContactCardView3 == null || (str = this.A02) == null)) {
            contactQrContactCardView3.setQrCode(C12960it.A0d(str, C12960it.A0k("https://wa.me/qr/")));
        }
        return inflate;
    }
}
