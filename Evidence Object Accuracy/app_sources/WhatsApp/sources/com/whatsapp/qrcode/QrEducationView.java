package com.whatsapp.qrcode;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass2GE;
import X.AnonymousClass2P7;
import X.C12980iv;
import X.C12990iw;
import X.C65403Jl;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class QrEducationView extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public int A02;
    public ValueAnimator A03;
    public Paint A04;
    public RectF A05;
    public Drawable A06;
    public Drawable A07;
    public Drawable A08;
    public Drawable A09;
    public Drawable A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;

    public QrEducationView(Context context) {
        super(context);
        if (!this.A0C) {
            this.A0C = true;
            generatedComponent();
        }
        this.A0D = false;
        this.A0E = true;
        this.A05 = C12980iv.A0K();
        A02(context);
    }

    public QrEducationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A0D = false;
        this.A0E = true;
        this.A05 = C12980iv.A0K();
        A02(context);
    }

    public QrEducationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0C) {
            this.A0C = true;
            generatedComponent();
        }
        this.A0D = false;
        this.A0E = true;
        this.A05 = C12980iv.A0K();
        A02(context);
    }

    public QrEducationView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A0C) {
            this.A0C = true;
            generatedComponent();
        }
    }

    public static float A00(float f, float f2, float f3, float f4, float f5) {
        return f4 + ((f5 - f4) * (f3 <= f ? 0.0f : f3 >= f2 ? 1.0f : (f3 - f) / (f2 - f)));
    }

    public final void A01() {
        if (this.A03 == null) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.A03 = ofFloat;
            ofFloat.setDuration(8000L);
            this.A03.setRepeatCount(-1);
            this.A03.setInterpolator(new LinearInterpolator());
            this.A03.addUpdateListener(new C65403Jl(this));
        }
        this.A03.start();
    }

    public final void A02(Context context) {
        Resources resources = context.getResources();
        this.A0A = resources.getDrawable(R.drawable.anim_qr_normal);
        this.A09 = resources.getDrawable(R.drawable.anim_qr_blurred);
        this.A07 = resources.getDrawable(R.drawable.anim_laptop);
        this.A08 = resources.getDrawable(R.drawable.anim_phone);
        this.A06 = AnonymousClass2GE.A01(context, R.drawable.anim_frame, R.color.qr_scanner_frame_color);
        int A00 = AnonymousClass00T.A00(context, R.color.qrEducationAnimationBgColor);
        this.A02 = AnonymousClass00T.A00(context, R.color.qrEducationAnimationColor);
        Paint A0F = C12990iw.A0F();
        this.A04 = A0F;
        A0F.setAntiAlias(true);
        this.A04.setColor(A00);
        this.A01 = getResources().getDimension(R.dimen.contact_qr_corner_radius);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        A01();
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnimation();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        Drawable drawable;
        float A00;
        int i;
        int i2;
        int width = getWidth();
        int height = getHeight();
        canvas.translate((float) (width >> 1), (float) (height >> 1));
        if (this.A08.getIntrinsicHeight() + (this.A08.getIntrinsicWidth() / 3) > height) {
            float intrinsicHeight = ((float) height) / ((float) (this.A08.getIntrinsicHeight() + (this.A08.getIntrinsicWidth() / 3)));
            canvas.scale(intrinsicHeight, intrinsicHeight);
        }
        float f = this.A00;
        if (f < 0.14f) {
            f = (f * f) / 0.14f;
        } else if (f >= 0.2f && f < 0.3f) {
            f = ((float) (Math.sqrt((double) (f - 0.2f)) * Math.sqrt(0.10000000894069672d))) + 0.2f;
        }
        if (this.A0E) {
            this.A07.setAlpha(255);
            int intrinsicWidth = (int) ((((float) this.A07.getIntrinsicWidth()) * 1.0f) / 2.0f);
            int intrinsicHeight2 = (int) ((((float) this.A07.getIntrinsicHeight()) * 1.0f) / 2.0f);
            this.A07.setBounds(-intrinsicWidth, -intrinsicHeight2, intrinsicWidth, intrinsicHeight2);
            drawable = this.A07;
        } else {
            int intrinsicWidth2 = (int) ((((float) this.A0A.getIntrinsicWidth()) / 2.0f) * 1.3f);
            int intrinsicHeight3 = (int) ((((float) this.A0A.getIntrinsicHeight()) / 2.0f) * 1.3f);
            RectF rectF = this.A05;
            int i3 = intrinsicWidth2 << 1;
            rectF.left = (float) (-i3);
            rectF.top = (float) (intrinsicHeight3 * -2);
            rectF.bottom = (float) (intrinsicHeight3 << 1);
            rectF.right = (float) i3;
            float f2 = this.A01;
            canvas.drawRoundRect(rectF, f2, f2, this.A04);
            this.A0A.setBounds(-intrinsicWidth2, -intrinsicHeight3, intrinsicWidth2, intrinsicHeight3);
            this.A0A.setColorFilter(this.A02, PorterDuff.Mode.SRC_IN);
            this.A0A.setAlpha(255);
            drawable = this.A0A;
        }
        drawable.draw(canvas);
        float intrinsicWidth3 = (float) (this.A08.getIntrinsicWidth() >> 1);
        float intrinsicHeight4 = (float) (this.A08.getIntrinsicHeight() >> 1);
        if (f < 0.14f) {
            A00 = A00(0.0f, 0.14f, f, 0.0f, 1.2566371f);
        } else {
            A00 = A00(0.14f, 0.2f, f, 1.2566371f, 1.5707964f);
        }
        float f3 = (-intrinsicWidth3) - ((intrinsicWidth3 * 3.0f) / 4.0f);
        float sin = (((float) Math.sin((double) A00)) * intrinsicWidth3) + f3;
        float f4 = intrinsicWidth3 / 6.0f;
        int i4 = (int) intrinsicWidth3;
        int i5 = (int) sin;
        int i6 = (int) intrinsicHeight4;
        int i7 = (int) f4;
        this.A08.setBounds((-i4) - i5, (-i6) + i7, i4 - i5, i6 + i7);
        this.A08.draw(canvas);
        int i8 = this.A08.getBounds().left;
        int i9 = this.A08.getBounds().right;
        int i10 = (i9 - i8) / 7;
        canvas.clipRect(i8 + i10, this.A08.getBounds().top, i9 - i10, this.A08.getBounds().bottom);
        float intrinsicWidth4 = (float) (this.A0A.getIntrinsicWidth() >> 1);
        float intrinsicHeight5 = (float) (this.A0A.getIntrinsicHeight() >> 1);
        float intrinsicWidth5 = (float) (this.A09.getIntrinsicWidth() >> 1);
        float intrinsicHeight6 = (float) (this.A09.getIntrinsicHeight() >> 1);
        float sin2 = sin - ((sin - (f3 + (((float) Math.sin(1.5707963267948966d)) * intrinsicWidth3))) / 3.0f);
        int i11 = (int) intrinsicWidth4;
        int i12 = (int) sin2;
        int i13 = (int) intrinsicHeight5;
        int i14 = (int) (f4 - (intrinsicHeight4 / 8.0f));
        this.A0A.setBounds((-i11) - i12, (-i13) + i14, i11 - i12, i13 + i14);
        int i15 = (int) intrinsicWidth5;
        int i16 = (int) intrinsicHeight6;
        this.A09.setBounds((-i15) - i12, (-i16) + i14, i15 - i12, i16 + i14);
        if (f > 0.2f) {
            i2 = (int) A00(0.2f, 0.3f, f, 0.0f, 255.0f);
            i = (int) A00(0.2f, 0.3f, f, 255.0f, 0.0f);
        } else {
            i = 255;
            i2 = 0;
        }
        this.A0A.setColorFilter(null);
        this.A0A.setAlpha(i2);
        this.A09.setAlpha(i);
        this.A0A.draw(canvas);
        this.A09.draw(canvas);
        this.A06.setAlpha(((int) (Math.sin((double) (this.A00 * 30.0f)) * 127.0d)) + 127);
        int intrinsicWidth6 = (int) (((float) this.A06.getIntrinsicWidth()) / 2.0f);
        int intrinsicHeight7 = (int) (((float) this.A06.getIntrinsicHeight()) / 2.0f);
        this.A06.setBounds((-intrinsicWidth6) - i5, (-intrinsicHeight7) + i14, intrinsicWidth6 - i5, intrinsicHeight7 + i14);
        this.A06.draw(canvas);
        canvas.translate((float) ((-width) >> 1), (float) ((-height) >> 1));
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int min = Math.min(View.getDefaultSize(getSuggestedMinimumWidth(), i), View.getDefaultSize(getSuggestedMinimumHeight(), i2));
        setMeasuredDimension(min, min);
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        if (getVisibility() == 0) {
            A01();
            return;
        }
        ValueAnimator valueAnimator = this.A03;
        if (valueAnimator != null) {
            valueAnimator.end();
        }
        this.A03 = null;
    }

    public void setShowLaptop(boolean z) {
        this.A0E = z;
    }
}
