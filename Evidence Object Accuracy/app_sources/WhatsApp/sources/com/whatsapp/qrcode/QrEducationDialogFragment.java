package com.whatsapp.qrcode;

import X.AnonymousClass01E;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.QrScanCodeFragment;

/* loaded from: classes2.dex */
public class QrEducationDialogFragment extends Hilt_QrEducationDialogFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.qr_education_dialog_fragment, viewGroup, false);
        ((QrEducationView) inflate.findViewById(R.id.education)).A0E = false;
        inflate.findViewById(R.id.ok).setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 12));
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(2, 2131952352);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AnonymousClass01E r5 = ((AnonymousClass01E) this).A0D;
        if (r5 instanceof QrScanCodeFragment) {
            QrScanCodeFragment qrScanCodeFragment = (QrScanCodeFragment) r5;
            if (qrScanCodeFragment.A0A) {
                qrScanCodeFragment.A0A = false;
                qrScanCodeFragment.A03.A00.edit().putBoolean("contact_qr_education", false).apply();
                qrScanCodeFragment.A02.A0J(qrScanCodeFragment.A0D, 15000);
            }
            qrScanCodeFragment.A09 = false;
            qrScanCodeFragment.A06.Aab();
        }
    }
}
