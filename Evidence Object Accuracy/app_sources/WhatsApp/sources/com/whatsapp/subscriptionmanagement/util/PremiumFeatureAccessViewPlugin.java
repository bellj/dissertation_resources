package com.whatsapp.subscriptionmanagement.util;

import X.AnonymousClass03H;
import X.AnonymousClass074;
import androidx.lifecycle.OnLifecycleEvent;

/* loaded from: classes.dex */
public class PremiumFeatureAccessViewPlugin implements AnonymousClass03H {
    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    private void onDestroy() {
        throw new NullPointerException("iterator");
    }

    @OnLifecycleEvent(AnonymousClass074.ON_START)
    private void onStart() {
        throw new NullPointerException("isDcpEnabled");
    }
}
