package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P7;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/* loaded from: classes2.dex */
public class CircularProgressBar extends ProgressBar implements AnonymousClass004 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public long A0D;
    public Rect A0E;
    public AnonymousClass2P7 A0F;
    public String A0G;
    public boolean A0H;
    public boolean A0I;
    public final Paint A0J;
    public final RectF A0K;

    public CircularProgressBar(Context context) {
        super(context);
        if (!this.A0H) {
            this.A0H = true;
            generatedComponent();
        }
        this.A0K = new RectF();
        this.A0J = new Paint();
        this.A0G = null;
        this.A06 = 5.0f;
        this.A0I = false;
        this.A05 = 0.3f;
        this.A0E = new Rect();
        A00(context, null);
    }

    public CircularProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A0K = new RectF();
        this.A0J = new Paint();
        this.A0G = null;
        this.A06 = 5.0f;
        this.A0I = false;
        this.A05 = 0.3f;
        this.A0E = new Rect();
        A00(context, attributeSet);
    }

    public CircularProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0H) {
            this.A0H = true;
            generatedComponent();
        }
        this.A0K = new RectF();
        this.A0J = new Paint();
        this.A0G = null;
        this.A06 = 5.0f;
        this.A0I = false;
        this.A05 = 0.3f;
        this.A0E = new Rect();
        A00(context, attributeSet);
    }

    public CircularProgressBar(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A0H) {
            this.A0H = true;
            generatedComponent();
        }
    }

    public final void A00(Context context, AttributeSet attributeSet) {
        this.A08 = AnonymousClass00T.A00(context, R.color.circular_progress_bar_center_text);
        this.A0C = AnonymousClass00T.A00(context, R.color.circular_progress_bar_bar);
        this.A0B = AnonymousClass00T.A00(context, R.color.circular_progress_bar_background);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A06);
            this.A0C = obtainStyledAttributes.getInteger(1, this.A0C);
            this.A0B = obtainStyledAttributes.getInteger(0, this.A0B);
            this.A09 = obtainStyledAttributes.getInteger(2, this.A09);
            this.A06 = obtainStyledAttributes.getFloat(5, this.A06);
            this.A0A = obtainStyledAttributes.getInteger(3, this.A0A);
            this.A03 = obtainStyledAttributes.getDimension(4, this.A03);
            this.A08 = obtainStyledAttributes.getInteger(6, this.A08);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0F;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0F = r0;
        }
        return r0.generatedComponent();
    }

    public String getCenterText() {
        return this.A0G;
    }

    public boolean getKnobEnabled() {
        return this.A0I;
    }

    public float getPaintStrokeFactor() {
        return this.A06;
    }

    public int getProgressBarBackgroundColor() {
        return this.A0B;
    }

    public int getProgressBarColor() {
        return this.A0C;
    }

    @Override // android.widget.ProgressBar, android.view.View
    public void onDraw(Canvas canvas) {
        float f;
        Paint paint = this.A0J;
        paint.setAntiAlias(true);
        if (this.A09 != 0) {
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(this.A09);
            canvas.drawArc(this.A0K, 0.0f, 360.0f, true, paint);
        }
        paint.setStrokeCap(Paint.Cap.ROUND);
        if (isIndeterminate()) {
            long uptimeMillis = SystemClock.uptimeMillis();
            long j = this.A0D;
            if (j == 0) {
                this.A0D = uptimeMillis;
                j = uptimeMillis;
            }
            long j2 = uptimeMillis - j;
            float f2 = (((float) (j2 % 1333)) * 1.0f) / 1333.0f;
            if (f2 < 0.5f) {
                float sin = (float) Math.sin(((double) f2) * 3.141592653589793d);
                f = (((sin * sin) * sin) * sin) / 2.0f;
            } else {
                float sin2 = (float) Math.sin(((double) (f2 - 0.5f)) * 3.141592653589793d);
                f = ((((sin2 * sin2) * sin2) * sin2) / 2.0f) + 0.5f;
            }
            if (f < 0.5f) {
                this.A00 = f * 2.0f * 280.0f;
                this.A02 = -1.0f;
            } else {
                float f3 = this.A02;
                if (f3 < 0.0f) {
                    f3 = this.A01;
                    this.A02 = f3;
                }
                this.A00 = (1.0f - f) * 2.0f * 280.0f;
                this.A01 = f3 + ((f - 0.5f) * 2.0f * 280.0f);
            }
            canvas.rotate(((((float) (j2 % 2200)) * 1.0f) / 2200.0f) * 360.0f, (float) (getWidth() >> 1), (float) (getHeight() >> 1));
            paint.setAntiAlias(true);
            int i = this.A0B;
            if (i != 0) {
                paint.setColor(i);
                paint.setStyle(Paint.Style.STROKE);
                canvas.drawArc(this.A0K, 0.0f, 360.0f, false, paint);
            }
            int i2 = this.A0A;
            if (i2 != 0) {
                paint.setColor(i2);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth((this.A04 / this.A06) + (this.A03 * 2.0f));
                canvas.drawArc(this.A0K, this.A01, this.A00, false, paint);
            }
            paint.setColor(this.A0C);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(this.A04 / this.A06);
            canvas.drawArc(this.A0K, this.A01, this.A00, false, paint);
            invalidate();
            return;
        }
        paint.setStyle(Paint.Style.STROKE);
        if (this.A0B != 0) {
            paint.setStrokeWidth(this.A04 / this.A06);
            paint.setColor(this.A0B);
            canvas.drawArc(this.A0K, ((((float) getProgress()) * 360.0f) / ((float) getMax())) + 270.0f, 360.0f - ((((float) getProgress()) * 360.0f) / ((float) getMax())), false, paint);
        }
        int i3 = this.A0A;
        if (i3 != 0) {
            paint.setColor(i3);
            paint.setStrokeWidth((this.A04 / this.A06) + (this.A03 * 2.0f));
            canvas.drawArc(this.A0K, -90.0f, (((float) getProgress()) * 360.0f) / ((float) getMax()), false, paint);
        }
        paint.setStrokeWidth(this.A04 / this.A06);
        paint.setColor(this.A0C);
        RectF rectF = this.A0K;
        canvas.drawArc(rectF, -90.0f, (((float) getProgress()) * 360.0f) / ((float) getMax()), false, paint);
        if (this.A0I) {
            paint.setColor(this.A0B);
            paint.setStyle(Paint.Style.FILL);
            double progress = (double) (((float) getProgress()) * ((float) (6.283185307179586d / ((double) getMax()))));
            canvas.drawCircle((float) (((double) rectF.centerX()) + (((double) this.A04) * Math.sin(progress))), (float) (((double) rectF.centerY()) - (((double) this.A04) * Math.cos(progress))), 10.0f, paint);
        }
        if (this.A0G != null) {
            paint.setColor(this.A08);
            paint.setTextSize(this.A07);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTypeface(Typeface.create("sans-serif-light", 0));
            String str = this.A0G;
            int length = str.length();
            Rect rect = this.A0E;
            paint.getTextBounds(str, 0, length, rect);
            paint.setStyle(Paint.Style.FILL);
            String str2 = this.A0G;
            canvas.drawText(str2, 0, str2.length(), rectF.centerX(), rectF.centerY() + (((float) rect.height()) * 0.5f), paint);
        }
    }

    @Override // android.widget.ProgressBar, android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        int paddingLeft = getPaddingLeft();
        float min = ((float) Math.min((i - paddingLeft) - getPaddingRight(), (i2 - getPaddingTop()) - getPaddingBottom())) / 2.0f;
        this.A04 = min;
        float f = min - (this.A05 * min);
        this.A04 = f;
        float f2 = (float) (i >> 1);
        float f3 = (float) (i2 >> 1);
        this.A0K.set(f2 - f, f3 - f, f2 + f, f3 + f);
    }

    public void setCenterText(String str) {
        this.A0G = str;
        this.A07 = (float) getResources().getDimensionPixelSize(R.dimen.progress_bar_text_size);
    }

    public void setKnobEnabled(boolean z) {
        this.A0I = z;
    }

    public void setPaintStrokeFactor(float f) {
        this.A06 = f;
    }

    public void setProgressBarBackgroundColor(int i) {
        this.A0B = i;
    }

    public void setProgressBarColor(int i) {
        this.A0C = i;
    }

    public void setRadiusFactor(float f) {
        this.A05 = f;
    }
}
