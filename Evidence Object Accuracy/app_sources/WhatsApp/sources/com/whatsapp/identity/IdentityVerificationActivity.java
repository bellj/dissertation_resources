package com.whatsapp.identity;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.AbstractC22250ym;
import X.AbstractC27101Ga;
import X.AbstractC35861it;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02S;
import X.AnonymousClass02T;
import X.AnonymousClass02U;
import X.AnonymousClass12P;
import X.AnonymousClass18U;
import X.AnonymousClass19M;
import X.AnonymousClass1AT;
import X.AnonymousClass1GD;
import X.AnonymousClass1MW;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass38Q;
import X.AnonymousClass451;
import X.AnonymousClass4BB;
import X.AnonymousClass4V2;
import X.C103124qF;
import X.C14330lG;
import X.C14350lI;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20920wX;
import X.C21280xA;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22130yZ;
import X.C22670zS;
import X.C22820zh;
import X.C237112s;
import X.C237312u;
import X.C244915s;
import X.C248917h;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C27631Ih;
import X.C29941Vi;
import X.C31911bJ;
import X.C35751ig;
import X.C41021sl;
import X.C49162Jo;
import X.C617732f;
import X.C63623Ch;
import X.C69453Zh;
import X.C83263wx;
import X.C83393xA;
import X.ExecutorC27271Gr;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.BidiToolbar;
import com.whatsapp.R;
import com.whatsapp.identity.IdentityVerificationActivity;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.qrcode.WaQrScannerView;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class IdentityVerificationActivity extends ActivityC13790kL implements AbstractC27101Ga, AbstractC22250ym, AbstractC35861it {
    public int A00;
    public int A01;
    public MenuItem A02;
    public View A03;
    public Animation A04;
    public ImageView A05;
    public ProgressBar A06;
    public TextView A07;
    public TextView A08;
    public C49162Jo A09;
    public BidiToolbar A0A;
    public C22820zh A0B;
    public AnonymousClass18U A0C;
    public C15550nR A0D;
    public C15610nY A0E;
    public C237112s A0F;
    public C15890o4 A0G;
    public C31911bJ A0H;
    public C237312u A0I;
    public C15370n3 A0J;
    public C22100yW A0K;
    public C22130yZ A0L;
    public C16120oU A0M;
    public C244915s A0N;
    public C14840m8 A0O;
    public AnonymousClass1AT A0P;
    public WaQrScannerView A0Q;
    public C252018m A0R;
    public C21280xA A0S;
    public boolean A0T;
    public boolean A0U;
    public final AnonymousClass1GD A0V;
    public final AnonymousClass4V2 A0W;
    public final AnonymousClass4V2 A0X;
    public final Runnable A0Y;
    public final Map A0Z;

    public IdentityVerificationActivity() {
        this(0);
        this.A0Z = Collections.singletonMap(AnonymousClass4BB.A01, "ISO-8859-1");
        this.A0Y = new RunnableBRunnable0Shape7S0100000_I0_7(this, 6);
        this.A0V = new C41021sl(this);
        this.A0W = new AnonymousClass451(this);
        this.A0X = new C617732f(this);
    }

    public IdentityVerificationActivity(int i) {
        this.A0T = false;
        A0R(new C103124qF(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0M = (C16120oU) r1.ANE.get();
            this.A0C = (AnonymousClass18U) r1.AAU.get();
            this.A0S = (C21280xA) r1.AMU.get();
            this.A0D = (C15550nR) r1.A45.get();
            this.A0R = (C252018m) r1.A7g.get();
            this.A0E = (C15610nY) r1.AMe.get();
            this.A0N = (C244915s) r1.A86.get();
            this.A0O = (C14840m8) r1.ACi.get();
            this.A0B = (C22820zh) r1.A9H.get();
            this.A0G = (C15890o4) r1.AN1.get();
            this.A0L = (C22130yZ) r1.A5d.get();
            this.A0K = (C22100yW) r1.A3g.get();
            this.A0F = (C237112s) r1.A18.get();
            this.A0I = (C237312u) r1.AM6.get();
            this.A0P = (AnonymousClass1AT) r1.AG2.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == 101) {
            A2i();
            this.A0U = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0057, code lost:
        if (X.C31201aA.A02(r2, r3) == false) goto L_0x0059;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005e A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0066 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0070 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0092 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x009a A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00a5 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00c7 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00cf A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00d9 A[Catch: 1Pt -> 0x0103, TryCatch #0 {1Pt -> 0x0103, blocks: (B:3:0x0002, B:5:0x0013, B:7:0x0018, B:9:0x001c, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x002e, B:17:0x0030, B:19:0x003a, B:21:0x003e, B:22:0x0040, B:24:0x0046, B:25:0x0048, B:28:0x0052, B:31:0x005a, B:33:0x005e, B:34:0x0060, B:36:0x0066, B:37:0x0068, B:39:0x0070, B:41:0x0074, B:42:0x0076, B:44:0x007c, B:45:0x007e, B:47:0x0086, B:50:0x008e, B:52:0x0092, B:53:0x0094, B:55:0x009a, B:56:0x009c, B:58:0x00a5, B:60:0x00a9, B:61:0x00ab, B:63:0x00b1, B:64:0x00b3, B:66:0x00bb, B:69:0x00c3, B:71:0x00c7, B:72:0x00c9, B:74:0x00cf, B:75:0x00d1, B:77:0x00d9, B:79:0x00dd, B:80:0x00df, B:82:0x00e5, B:83:0x00e7, B:85:0x00ef), top: B:100:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x00fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A2e(byte[] r8) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.identity.IdentityVerificationActivity.A2e(byte[]):int");
    }

    public final void A2f() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        translateAnimation.setAnimationListener(new C83263wx(this));
        translateAnimation.setDuration(300);
        this.A03.startAnimation(translateAnimation);
    }

    public final void A2g() {
        int i;
        String charSequence;
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        int min = (Math.min(point.x, point.y) << 1) / 3;
        Bitmap createBitmap = Bitmap.createBitmap(min, min, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(-1);
        C63623Ch r14 = this.A09.A04;
        int i2 = r14.A01;
        int i3 = r14.A00;
        int i4 = min / 12;
        float f = ((float) (min - (i4 << 1))) * 1.0f;
        float f2 = f / ((float) i2);
        float f3 = f / ((float) i3);
        Paint paint = new Paint();
        paint.setColor(-16777216);
        int i5 = 0;
        while (true) {
            i = 1;
            if (i5 < i2) {
                int i6 = 0;
                while (i6 < i3) {
                    if (r14.A02[i6][i5] == i) {
                        float f4 = (float) i4;
                        canvas.drawRect((((float) i5) * f2) + f4, (((float) i6) * f3) + f4, (((float) (i5 + 1)) * f2) + f4, f4 + (((float) (i6 + 1)) * f3), paint);
                    }
                    i6++;
                    i = 1;
                }
                i5++;
            } else {
                try {
                    break;
                } catch (Throwable th) {
                    createBitmap.recycle();
                    throw th;
                }
            }
        }
        File A0M = ((ActivityC13810kN) this).A04.A0M("code.png");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(A0M);
            try {
                createBitmap.compress(Bitmap.CompressFormat.PNG, 0, fileOutputStream);
                fileOutputStream.close();
            } catch (Throwable th2) {
                try {
                    fileOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th2;
            }
        } catch (FileNotFoundException e) {
            Log.e("idverification/sharefailed", e);
            ((ActivityC13810kN) this).A05.A07(R.string.share_failed, 0);
            createBitmap.recycle();
            return;
        } catch (IOException e2) {
            Log.e(e2);
        }
        createBitmap.recycle();
        String A00 = this.A0H.A00.A00();
        StringBuilder sb = new StringBuilder();
        int length = A00.length();
        while (i <= length) {
            sb.append(A00.charAt(i - 1));
            if (i != length) {
                if (i % 20 == 0) {
                    sb.append('\n');
                } else if (i % 5 == 0) {
                    sb.append(" ");
                }
            }
            i++;
        }
        Intent intent = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        C27631Ih r9 = r0.A05;
        AnonymousClass009.A05(r9);
        intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.identity_code_email_subject, ((ActivityC13830kP) this).A01.A0F(((ActivityC13790kL) this).A01.A05()), ((ActivityC13830kP) this).A01.A0G(C248917h.A00(C20920wX.A00(), r9.user))));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(getString(R.string.identity_code_email_body));
        sb2.append("\n");
        AnonymousClass018 r1 = ((ActivityC13830kP) this).A01;
        String obj = sb.toString();
        String[] split = obj.split("\n");
        AnonymousClass02S A03 = r1.A03();
        int length2 = obj.length();
        int length3 = split.length;
        StringBuilder sb3 = new StringBuilder(length2 + (length3 << 2));
        for (String str : split) {
            AnonymousClass02T r02 = AnonymousClass02U.A04;
            if (str == null) {
                charSequence = null;
            } else {
                charSequence = A03.A02(r02, str).toString();
            }
            sb3.append(charSequence);
            sb3.append('\n');
        }
        sb2.append(sb3.toString());
        intent.putExtra("android.intent.extra.TEXT", sb2.toString());
        intent.putExtra("android.intent.extra.STREAM", C14350lI.A01(getApplicationContext(), A0M));
        intent.setType("image/png");
        intent.addFlags(524288);
        startActivity(Intent.createChooser(intent, null));
    }

    public final void A2h() {
        WaQrScannerView waQrScannerView = this.A0Q;
        if (waQrScannerView != null && waQrScannerView.getVisibility() == 0) {
            return;
        }
        if (this.A0G.A02("android.permission.CAMERA") != 0) {
            C35751ig r2 = new C35751ig(this);
            r2.A01 = R.drawable.permission_cam;
            r2.A02 = R.string.permission_cam_access_on_verify_identity_request;
            r2.A03 = R.string.permission_cam_access_on_verify_identity;
            r2.A0C = new String[]{"android.permission.CAMERA"};
            startActivityForResult(r2.A00(), 1);
            return;
        }
        findViewById(R.id.overlay).setVisibility(0);
        this.A0Q.setVisibility(0);
        this.A07.setVisibility(8);
        ((ActivityC13810kN) this).A05.A0G(this.A0Y);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(18:6|(1:28)(1:9)|10|(1:12)(14:19|(1:(2:22|23)(1:26))(1:(2:25|23)(1:27))|14|(5:16|(1:18)|60|(1:30)|61)|31|58|32|35|(3:37|(1:64)(4:39|(2:(1:42)|43)(2:46|(2:48|65)(1:49))|44|63)|45)|62|50|(2:55|51)|56|57)|13|14|(0)|31|58|32|35|(0)|62|50|(3:53|55|51)|66|56|57) */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x013a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x013b, code lost:
        com.whatsapp.util.Log.w("idverification/", r1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0155  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2i() {
        /*
        // Method dump skipped, instructions count: 481
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.identity.IdentityVerificationActivity.A2i():void");
    }

    public final void A2j(Intent intent) {
        C14900mE r2;
        int i;
        NdefMessage ndefMessage = (NdefMessage) intent.getParcelableArrayExtra("android.nfc.extra.NDEF_MESSAGES")[0];
        byte[] payload = ndefMessage.getRecords()[0].getPayload();
        UserJid nullable = UserJid.getNullable(new String(ndefMessage.getRecords()[0].getId(), Charset.forName("US-ASCII")));
        if (nullable != null) {
            C15370n3 A0B = this.A0D.A0B(nullable);
            this.A0J = A0B;
            String A0A = this.A0E.A0A(A0B, -1);
            A2N(getString(R.string.verify_identity_names, A0A));
            A2m(false);
            if (this.A0H == null) {
                Log.w("idverification/ndef/no-fingerprint");
                return;
            }
            int A2e = A2e(payload);
            if (A2e == -3) {
                r2 = ((ActivityC13810kN) this).A05;
                i = R.string.verify_identity_result_wrong_you;
            } else if (A2e != -2) {
                if (A2e != 1) {
                    if (A2e == 2) {
                        A2o(false);
                    } else {
                        return;
                    }
                }
                A2o(true);
                return;
            } else {
                r2 = ((ActivityC13810kN) this).A05;
                i = R.string.verify_identity_result_wrong_contact;
            }
            r2.A0E(getString(i, A0A), 1);
        }
    }

    public final void A2k(UserJid userJid) {
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (userJid.equals(r0.A05) || userJid.equals(this.A0J.A0B(UserJid.class))) {
            runOnUiThread(new RunnableBRunnable0Shape7S0100000_I0_7(this, 10));
        }
    }

    public final void A2l(Runnable runnable) {
        WaQrScannerView waQrScannerView = this.A0Q;
        if (waQrScannerView != null && waQrScannerView.getVisibility() == 0) {
            findViewById(R.id.main_layout).setVisibility(0);
            findViewById(R.id.scan_code).setVisibility(0);
            findViewById(R.id.verify_identity_qr_tip).setVisibility(8);
            findViewById(R.id.overlay).setVisibility(8);
            this.A07.setVisibility(8);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setDuration((long) getResources().getInteger(17694721));
            translateAnimation.setAnimationListener(new C83393xA(this, runnable));
            findViewById(R.id.main_layout).startAnimation(translateAnimation);
        }
    }

    public final void A2m(boolean z) {
        AnonymousClass4V2 r5;
        AaN();
        if (z) {
            r5 = this.A0X;
        } else {
            r5 = this.A0W;
        }
        C244915s r4 = this.A0N;
        ExecutorC27271Gr r3 = r4.A05;
        r3.A00();
        ((AbstractC16350or) new AnonymousClass38Q(r4, r5, (UserJid) this.A0J.A0B(UserJid.class))).A02.executeOnExecutor(r3, new Void[0]);
    }

    public final void A2n(boolean z) {
        MenuItem menuItem = this.A02;
        if (menuItem != null) {
            menuItem.setVisible(z);
        }
        View findViewById = findViewById(R.id.footer);
        int i = 0;
        int i2 = 8;
        if (z) {
            i2 = 0;
        }
        findViewById.setVisibility(i2);
        View findViewById2 = findViewById(R.id.verify_identity_tip);
        int i3 = 8;
        if (z) {
            i3 = 0;
        }
        findViewById2.setVisibility(i3);
        View findViewById3 = findViewById(R.id.qr_code_group);
        if (!z) {
            i = 8;
        }
        findViewById3.setVisibility(i);
    }

    public final void A2o(boolean z) {
        this.A05.setVisibility(0);
        ImageView imageView = this.A05;
        int i = R.drawable.red_circle;
        if (z) {
            i = R.drawable.green_circle;
        }
        imageView.setBackgroundResource(i);
        ImageView imageView2 = this.A05;
        int i2 = R.string.identity_not_verified;
        if (z) {
            i2 = R.string.identity_verified;
        }
        imageView2.setContentDescription(getString(i2));
        ImageView imageView3 = this.A05;
        int i3 = R.drawable.ill_verification_failure;
        if (z) {
            i3 = R.drawable.ill_verification_success;
        }
        imageView3.setImageResource(i3);
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setInterpolator(new OvershootInterpolator());
        animationSet.setDuration((long) getResources().getInteger(17694721));
        this.A05.startAnimation(animationSet);
        this.A05.setFocusable(true);
        this.A05.setFocusableInTouchMode(true);
        this.A05.requestFocus();
        ((ActivityC13810kN) this).A05.A0J(this.A0Y, 4000);
    }

    @Override // X.AbstractC22250ym
    public void AMm(List list) {
        UserJid userJid;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (deviceJid == null) {
                userJid = null;
            } else {
                userJid = deviceJid.getUserJid();
            }
            if (C29941Vi.A00(this.A0J.A0B(UserJid.class), userJid)) {
                A2m(false);
            }
        }
    }

    @Override // X.AbstractC27101Ga
    public void AQx(DeviceJid deviceJid, int i) {
        runOnUiThread(new RunnableBRunnable0Shape5S0200000_I0_5(this, 22, deviceJid));
    }

    @Override // X.AbstractC27101Ga
    public void ARG(DeviceJid deviceJid) {
        A2k(deviceJid.getUserJid());
    }

    @Override // X.AbstractC27101Ga
    public void ARH(DeviceJid deviceJid) {
        A2k(deviceJid.getUserJid());
    }

    @Override // X.AbstractC27101Ga
    public void ARI(DeviceJid deviceJid) {
        A2k(deviceJid.getUserJid());
    }

    @Override // X.AbstractC35861it
    public void AY9(UserJid userJid, Set set, Set set2) {
        if (!set.isEmpty() || !set2.isEmpty()) {
            A2k(userJid);
        }
    }

    @Override // android.app.Activity
    public void finish() {
        WaQrScannerView waQrScannerView = this.A0Q;
        if (waQrScannerView != null && waQrScannerView.getVisibility() == 0 && findViewById(R.id.main_layout).getVisibility() == 8) {
            A2l(null);
        } else {
            super.finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            A2h();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            this.A0J = this.A0D.A0B(UserJid.get(getIntent().getStringExtra("jid")));
            setContentView(R.layout.identity_verification);
            setTitle(R.string.verify_identity);
            BidiToolbar bidiToolbar = (BidiToolbar) AnonymousClass00T.A05(this, R.id.toolbar);
            this.A0A = bidiToolbar;
            bidiToolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A04(getResources().getDrawable(R.drawable.ic_back), getResources().getColor(R.color.homeActivityToolbarContent)), ((ActivityC13830kP) this).A01));
            this.A0A.setTitle(R.string.verify_identity);
            this.A0A.setSubtitle(getString(R.string.verify_identity_names, this.A0E.A0A(this.A0J, -1)));
            this.A0A.setBackgroundResource(R.color.primary);
            this.A0A.A0C(this, 2131952341);
            this.A0A.setNavigationOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 18));
            A1e(this.A0A);
            this.A08 = (TextView) findViewById(R.id.identity_text);
            this.A06 = (ProgressBar) findViewById(R.id.progress_bar);
            this.A07 = (TextView) findViewById(R.id.error_indicator);
            this.A0Q = (WaQrScannerView) findViewById(R.id.qr_scanner_view);
            this.A03 = findViewById(R.id.header);
            if (!((ActivityC13810kN) this).A09.A00.getBoolean("security_notifications", false) && ((ActivityC13810kN) this).A09.A1J("security_notifications_alert_timestamp", 2592000000L)) {
                this.A03.postDelayed(new RunnableBRunnable0Shape7S0100000_I0_7(this, 13), 1000);
            }
            findViewById(R.id.enable).setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 17));
            findViewById(R.id.close).setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 0));
            WaQrScannerView waQrScannerView = this.A0Q;
            waQrScannerView.setQrDecodeHints(this.A0Z);
            waQrScannerView.setQrScannerCallback(new C69453Zh(this));
            A2n(false);
            A2m(false);
            this.A05 = (ImageView) AnonymousClass00T.A05(this, R.id.result);
            AnonymousClass00T.A05(this, R.id.scan_code).setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 19));
            if (this.A0G.A02("android.permission.NFC") == 0) {
                NfcAdapter defaultAdapter = NfcAdapter.getDefaultAdapter(this);
                if (defaultAdapter != null) {
                    try {
                        defaultAdapter.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback() { // from class: X.3LY
                            @Override // android.nfc.NfcAdapter.CreateNdefMessageCallback
                            public final NdefMessage createNdefMessage(NfcEvent nfcEvent) {
                                IdentityVerificationActivity identityVerificationActivity = IdentityVerificationActivity.this;
                                if (identityVerificationActivity.A0H == null) {
                                    Log.w("idverification/createndef/no-fingerprint");
                                    return null;
                                }
                                byte[] bytes = "application/com.whatsapp.identity".getBytes(Charset.forName("US-ASCII"));
                                C15570nT r0 = ((ActivityC13790kL) identityVerificationActivity).A01;
                                r0.A08();
                                C27631Ih r02 = r0.A05;
                                AnonymousClass009.A05(r02);
                                return new NdefMessage(new NdefRecord[]{new NdefRecord(2, bytes, r02.getRawString().getBytes(Charset.forName("US-ASCII")), identityVerificationActivity.A0H.A01.A02()), NdefRecord.createApplicationRecord("com.whatsapp")});
                            }
                        }, this, new Activity[0]);
                    } catch (IllegalStateException | SecurityException e) {
                        Log.w("idverification/ ", e);
                    }
                }
                if ("android.nfc.action.NDEF_DISCOVERED".equals(getIntent().getAction())) {
                    A2j(getIntent());
                }
            }
            this.A0F.A03(this);
            this.A0B.A03(this);
            this.A0I.A03(this);
            this.A0K.A03(this.A0V);
        } catch (AnonymousClass1MW e2) {
            Log.e("idverification/finishing due to invalid jid", e2);
            finish();
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem icon = menu.add(0, R.id.menuitem_share, 0, R.string.share).setIcon(R.drawable.ic_action_share);
        this.A02 = icon;
        icon.setShowAsAction(2);
        MenuItem menuItem = this.A02;
        boolean z = false;
        if (this.A0H != null) {
            z = true;
        }
        menuItem.setVisible(z);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0F.A04(this);
        this.A0B.A04(this);
        this.A0I.A04(this);
        this.A0K.A04(this.A0V);
        ((ActivityC13810kN) this).A05.A0G(this.A0Y);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if ("android.nfc.action.NDEF_DISCOVERED".equals(intent.getAction())) {
            A2j(intent);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_share) {
            WaQrScannerView waQrScannerView = this.A0Q;
            if (waQrScannerView == null || waQrScannerView.getVisibility() != 0) {
                A2g();
                return true;
            }
            A2l(new RunnableBRunnable0Shape7S0100000_I0_7(this, 7));
            return true;
        } else if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            finish();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (this.A0Q.getVisibility() == 0) {
            this.A0Q.setVisibility(4);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0Q.getVisibility() == 4) {
            this.A0Q.setVisibility(0);
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape7S0100000_I0_7(this, 11));
    }
}
