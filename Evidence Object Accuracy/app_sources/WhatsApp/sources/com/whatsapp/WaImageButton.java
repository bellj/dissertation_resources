package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass08i;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C28141Kv;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaImageButton extends AnonymousClass08i implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;

    public WaImageButton(Context context) {
        super(context, null);
        A00();
    }

    public WaImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(context, attributeSet);
    }

    public WaImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(context, attributeSet);
    }

    public WaImageButton(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A01(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0S);
            int resourceId = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId != 0) {
                setContentDescription(this.A00.A09(resourceId));
            }
            this.A03 = obtainStyledAttributes.getBoolean(1, false);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        boolean z;
        if (this.A03) {
            z = C28141Kv.A00(this.A00);
            if (z) {
                canvas.save();
                canvas.scale(-1.0f, 1.0f, ((float) super.getWidth()) * 0.5f, ((float) super.getHeight()) * 0.5f);
            }
        } else {
            z = false;
        }
        super.onDraw(canvas);
        if (this.A03 && z) {
            canvas.restore();
        }
    }
}
