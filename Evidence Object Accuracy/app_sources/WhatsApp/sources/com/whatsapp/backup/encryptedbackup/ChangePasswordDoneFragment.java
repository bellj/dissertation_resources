package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C12960it;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ChangePasswordDoneFragment extends Hilt_ChangePasswordDoneFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_change_password_done);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        int i;
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.change_password_done_done_button), this, encBackupViewModel, 1);
        TextView A0I = C12960it.A0I(view, R.id.change_password_done_title);
        if (encBackupViewModel.A04() == 6) {
            i = R.string.encrypted_backup_add_password_done_title;
        } else if (encBackupViewModel.A04() == 7 || encBackupViewModel.A04() == 9) {
            i = R.string.encrypted_backup_validate_password_done_title;
        } else {
            return;
        }
        A0I.setText(i);
    }
}
