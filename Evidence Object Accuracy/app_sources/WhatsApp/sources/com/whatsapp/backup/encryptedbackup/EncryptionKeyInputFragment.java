package com.whatsapp.backup.encryptedbackup;

import X.AbstractC15710nm;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass5U7;
import X.C004902f;
import X.C102604pP;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14850m9;
import X.C14900mE;
import X.C252818u;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.backup.encryptedbackup.EncryptionKeyFragment;
import com.whatsapp.backup.encryptedbackup.EncryptionKeyInputFragment;
import com.whatsapp.deviceauth.BiometricAuthPlugin;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* loaded from: classes2.dex */
public class EncryptionKeyInputFragment extends Hilt_EncryptionKeyInputFragment {
    public Button A00;
    public RelativeLayout A01;
    public AnonymousClass12P A02;
    public AbstractC15710nm A03;
    public C14900mE A04;
    public C252818u A05;
    public EncBackupViewModel A06;
    public EncryptionKeyFragment A07;
    public AnonymousClass01d A08;
    public BiometricAuthPlugin A09;
    public C14850m9 A0A;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_encryption_key_input);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Resources A02;
        int i;
        Object[] objArr;
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        this.A06 = encBackupViewModel;
        int A04 = encBackupViewModel.A04();
        TextView A0I = C12960it.A0I(view, R.id.enc_backup_encryption_key_input_instructional);
        View A0D = AnonymousClass028.A0D(view, R.id.enc_backup_encryption_key_input_forgot);
        int i2 = 64;
        if (A04 == 6 || A04 == 4) {
            C14850m9 r9 = this.A0A;
            this.A09 = new BiometricAuthPlugin(A0C(), this.A03, this.A04, this.A08, new AnonymousClass5U7() { // from class: X.56M
                @Override // X.AnonymousClass5U7
                public final void AMd(int i3) {
                    EncryptionKeyInputFragment encryptionKeyInputFragment = EncryptionKeyInputFragment.this;
                    if (i3 == -1 || i3 == 4) {
                        encryptionKeyInputFragment.A06.A09(6);
                        encryptionKeyInputFragment.A06.A0D(true);
                    }
                }
            }, r9, R.string.encrypted_backup_biometric_auth_plugin_title, R.string.encrypted_backup_biometric_auth_plugin_subtitle);
            AbstractView$OnClickListenerC34281fs.A00(A0D, this, 8);
            C12960it.A19(A0G(), this.A06.A04, this, 8);
            if (A04 == 6) {
                A02 = A02();
                i = R.plurals.encrypted_backup_verify_password_add_password_instruction;
                objArr = new Object[]{64};
                i2 = 64;
                C12980iv.A15(A02, A0I, objArr, i, i2);
                C004902f r3 = new C004902f(A0E());
                EncryptionKeyFragment encryptionKeyFragment = new EncryptionKeyFragment();
                this.A07 = encryptionKeyFragment;
                r3.A07(encryptionKeyFragment, R.id.encryption_key_input_encryption_key_container);
                r3.A01();
                this.A00 = (Button) AnonymousClass028.A0D(view, R.id.encryption_key_input_next_button);
                this.A01 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.enc_key_background);
                A1A(false);
                C12970iu.A1P(A0G(), this.A06.A02, this, 1);
            }
            i2 = 64;
            A02 = A02();
            i = R.plurals.encrypted_backup_verify_password_disable_from_key_instruction;
        } else {
            if (A04 == 2) {
                AbstractView$OnClickListenerC34281fs.A00(A0D, this, 9);
                A02 = A02();
                i = R.plurals.encrypted_backup_restore_encryption_key_instruction;
            }
            C004902f r3 = new C004902f(A0E());
            EncryptionKeyFragment encryptionKeyFragment = new EncryptionKeyFragment();
            this.A07 = encryptionKeyFragment;
            r3.A07(encryptionKeyFragment, R.id.encryption_key_input_encryption_key_container);
            r3.A01();
            this.A00 = (Button) AnonymousClass028.A0D(view, R.id.encryption_key_input_next_button);
            this.A01 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.enc_key_background);
            A1A(false);
            C12970iu.A1P(A0G(), this.A06.A02, this, 1);
        }
        objArr = new Object[]{64};
        C12980iv.A15(A02, A0I, objArr, i, i2);
        C004902f r3 = new C004902f(A0E());
        EncryptionKeyFragment encryptionKeyFragment = new EncryptionKeyFragment();
        this.A07 = encryptionKeyFragment;
        r3.A07(encryptionKeyFragment, R.id.encryption_key_input_encryption_key_container);
        r3.A01();
        this.A00 = (Button) AnonymousClass028.A0D(view, R.id.encryption_key_input_next_button);
        this.A01 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.enc_key_background);
        A1A(false);
        C12970iu.A1P(A0G(), this.A06.A02, this, 1);
    }

    public void A1A(boolean z) {
        ViewOnClickCListenerShape17S0100000_I1 viewOnClickCListenerShape17S0100000_I1;
        C102604pP r0;
        this.A00.setEnabled(z);
        Button button = this.A00;
        if (z) {
            viewOnClickCListenerShape17S0100000_I1 = new ViewOnClickCListenerShape17S0100000_I1(this, 10);
        } else {
            viewOnClickCListenerShape17S0100000_I1 = null;
        }
        button.setOnClickListener(viewOnClickCListenerShape17S0100000_I1);
        RelativeLayout relativeLayout = this.A01;
        int i = R.drawable.enc_backup_enc_key_bg_disabled;
        if (z) {
            i = R.drawable.enc_backup_enc_key_bg;
        }
        relativeLayout.setBackgroundResource(i);
        EncryptionKeyFragment encryptionKeyFragment = this.A07;
        CodeInputField[] codeInputFieldArr = encryptionKeyFragment.A04;
        if (codeInputFieldArr != null) {
            CodeInputField codeInputField = codeInputFieldArr[codeInputFieldArr.length - 1];
            if (z) {
                r0 = new TextView.OnEditorActionListener() { // from class: X.4pP
                    @Override // android.widget.TextView.OnEditorActionListener
                    public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                        EncryptionKeyFragment encryptionKeyFragment2 = EncryptionKeyFragment.this;
                        if (i2 != 0) {
                            return false;
                        }
                        encryptionKeyFragment2.A01.A07();
                        return false;
                    }
                };
            } else {
                r0 = null;
            }
            codeInputField.setOnEditorActionListener(r0);
            Context A0p = encryptionKeyFragment.A0p();
            if (A0p != null) {
                int i2 = R.color.settings_title_accent;
                if (z) {
                    i2 = R.color.primary_light;
                }
                for (CodeInputField codeInputField2 : encryptionKeyFragment.A04) {
                    C12960it.A0s(A0p, codeInputField2, i2);
                }
            }
        }
    }
}
