package com.whatsapp.backup.encryptedbackup;

import X.C12980iv;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConfirmPasswordFragment extends Hilt_ConfirmPasswordFragment {
    public String A00;

    @Override // com.whatsapp.backup.encryptedbackup.PasswordInputFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        this.A00 = (String) ((PasswordInputFragment) this).A07.A05.A01();
        int i = ((PasswordInputFragment) this).A00;
        TextView textView = ((PasswordInputFragment) this).A04;
        int i2 = R.string.encrypted_backup_confirm_password_title_change_password;
        if (i == 1) {
            i2 = R.string.encrypted_backup_confirm_password_title_enable;
        }
        C12980iv.A1I(textView, this, i2);
        C12980iv.A1I(((PasswordInputFragment) this).A03, this, R.string.encrypted_backup_confirm_password_instruction);
        C12980iv.A1I(((PasswordInputFragment) this).A08, this, R.string.encrypted_backup_confirm_password_action_button);
        A1E(true);
        A1B();
    }
}
