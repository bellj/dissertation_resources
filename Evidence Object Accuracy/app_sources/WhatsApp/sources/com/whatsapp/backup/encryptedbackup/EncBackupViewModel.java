package com.whatsapp.backup.encryptedbackup;

import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass1OM;
import X.AnonymousClass1ON;
import X.AnonymousClass1OP;
import X.AnonymousClass1US;
import X.AnonymousClass432;
import X.AnonymousClass43K;
import X.AnonymousClass53C;
import X.C14820m6;
import X.C15820nx;
import X.C16120oU;
import X.C17220qS;
import X.C18350sJ;
import X.C20670w8;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.jobqueue.job.DeleteAccountFromHsmServerJob;
import com.whatsapp.jobqueue.requirement.ChatConnectionRequirement;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.LinkedList;
import org.whispersystems.jobqueue.JobParameters;

/* loaded from: classes2.dex */
public class EncBackupViewModel extends AnonymousClass015 {
    public CountDownTimer A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final AnonymousClass016 A04 = new AnonymousClass016(1);
    public final AnonymousClass016 A05 = new AnonymousClass016();
    public final AnonymousClass016 A06 = new AnonymousClass016(0);
    public final AnonymousClass016 A07 = new AnonymousClass016();
    public final AnonymousClass016 A08 = new AnonymousClass016(0L);
    public final AnonymousClass016 A09 = new AnonymousClass016();
    public final AnonymousClass016 A0A;
    public final C15820nx A0B;
    public final AnonymousClass01d A0C;
    public final C14820m6 A0D;
    public final AnonymousClass018 A0E;
    public final C16120oU A0F;
    public final C17220qS A0G;
    public final C18350sJ A0H;
    public final AbstractC14440lR A0I;

    public EncBackupViewModel(C15820nx r3, AnonymousClass01d r4, C14820m6 r5, AnonymousClass018 r6, C16120oU r7, C17220qS r8, C18350sJ r9, AbstractC14440lR r10) {
        Boolean bool = Boolean.FALSE;
        this.A0A = new AnonymousClass016(bool);
        this.A01 = new AnonymousClass016(bool);
        this.A0I = r10;
        this.A0F = r7;
        this.A0G = r8;
        this.A0C = r4;
        this.A0E = r6;
        this.A0B = r3;
        this.A0H = r9;
        this.A0D = r5;
    }

    public static /* synthetic */ void A00(EncBackupViewModel encBackupViewModel, int i) {
        AnonymousClass016 r1;
        int i2;
        if (i == 0) {
            encBackupViewModel.A04.A0A(3);
            if (encBackupViewModel.A04() == 1) {
                Log.i("EncBackupViewModel/enabled encrypted backup");
                encBackupViewModel.A08(5);
                r1 = encBackupViewModel.A07;
                i2 = -1;
            } else {
                Log.i("EncBackupViewModel/successfully re-registered with the hsm");
                r1 = encBackupViewModel.A03;
                i2 = 502;
            }
        } else if (i == 3) {
            Log.e("EncBackupViewModel/failed to enable encrypted backup due to a connection error");
            r1 = encBackupViewModel.A04;
            i2 = 8;
        } else {
            Log.e("EncBackupViewModel/failed to enable encrypted backup due to a server error");
            r1 = encBackupViewModel.A04;
            i2 = 4;
        }
        r1.A0A(Integer.valueOf(i2));
    }

    public int A04() {
        Object A01 = this.A09.A01();
        AnonymousClass009.A05(A01);
        return ((Number) A01).intValue();
    }

    public void A05() {
        C15820nx r3 = this.A0B;
        r3.A07.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r3, 9));
        if (!r3.A03.A00.getBoolean("encrypted_backup_using_encryption_key", false)) {
            C20670w8 r4 = r3.A00;
            LinkedList linkedList = new LinkedList();
            linkedList.add(new ChatConnectionRequirement());
            r4.A00(new DeleteAccountFromHsmServerJob(new JobParameters("DeleteAccountFromHsmServerJob", linkedList, true)));
        }
        Log.i("EncBackupViewModel/encrypted backup disabled");
        this.A03.A0B(402);
    }

    public void A06() {
        AnonymousClass016 r1 = this.A01;
        if (r1.A01() == null || !((Boolean) r1.A01()).booleanValue()) {
            this.A04.A0A(2);
            C15820nx r12 = this.A0B;
            Object A01 = this.A05.A01();
            AnonymousClass009.A05(A01);
            AnonymousClass1OP r2 = new AnonymousClass1OP(this);
            JniBridge jniBridge = r12.A08;
            AbstractC14440lR r6 = r12.A07;
            new AnonymousClass1ON(r12, r2, r12.A03, r12.A05, r12.A06, r6, jniBridge, (String) A01).A00();
            return;
        }
        C14820m6 r13 = this.A0B.A03;
        r13.A14(true);
        r13.A15(true);
        A08(5);
        this.A07.A0A(-1);
    }

    public void A07() {
        String str = (String) this.A02.A01();
        if (str == null) {
            return;
        }
        if (A04() == 2) {
            C15820nx r5 = this.A0B;
            AnonymousClass53C r4 = new AnonymousClass53C(this);
            boolean z = false;
            if (str.length() == 64) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            r5.A07.Ab2(new AnonymousClass1OM(r4, r5, null, AnonymousClass1US.A0E(str), true));
            return;
        }
        this.A04.A0B(2);
        this.A0I.Ab2(new RunnableBRunnable0Shape0S1100000_I0(4, str, this));
    }

    public void A08(int i) {
        AnonymousClass43K r1 = new AnonymousClass43K();
        r1.A00 = Integer.valueOf(i);
        this.A0F.A07(r1);
    }

    public void A09(int i) {
        AnonymousClass43K r1 = new AnonymousClass43K();
        r1.A01 = Integer.valueOf(i);
        this.A0F.A07(r1);
    }

    public void A0A(int i) {
        AnonymousClass432 r1 = new AnonymousClass432();
        r1.A00 = Integer.valueOf(i);
        this.A0F.A07(r1);
    }

    public void A0B(int i) {
        this.A03.A0B(Integer.valueOf(i));
    }

    public void A0C(Bundle bundle) {
        AnonymousClass009.A0B("getIntent().getExtras()[USER_ACTION_ARG] is required but is not present", bundle.containsKey("user_action"));
        int i = bundle.getInt("user_action");
        AnonymousClass016 r1 = this.A09;
        if (r1.A01() == null) {
            r1.A0B(Integer.valueOf(i));
        }
        AnonymousClass016 r2 = this.A03;
        if (r2.A01() == null) {
            int i2 = 100;
            if (i != 1) {
                i2 = 103;
                if (i != 2) {
                    i2 = 102;
                    if (i != 3) {
                        if (i == 7 || i == 9) {
                            i2 = 104;
                        } else {
                            return;
                        }
                    }
                }
            }
            r2.A0B(Integer.valueOf(i2));
        }
    }

    public void A0D(boolean z) {
        AnonymousClass016 r1;
        int i;
        if (z) {
            Log.i("EncBackupViewModel/successfully verified encryption key");
            this.A0A.A0A(Boolean.TRUE);
            this.A04.A0A(3);
            A09(4);
            if (A04() == 4) {
                r1 = this.A03;
                i = 302;
            } else if (A04() == 6) {
                r1 = this.A03;
                i = 300;
            } else {
                return;
            }
        } else {
            Log.i("EncBackupViewModel/invalid encryption key");
            r1 = this.A04;
            i = 5;
        }
        r1.A0A(Integer.valueOf(i));
    }

    public boolean A0E() {
        Object A01 = this.A0A.A01();
        AnonymousClass009.A05(A01);
        return ((Boolean) A01).booleanValue();
    }
}
