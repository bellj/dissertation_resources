package com.whatsapp.backup.encryptedbackup;

import X.AbstractC15710nm;
import X.C12960it;
import X.C14850m9;
import X.C14900mE;
import android.content.Intent;
import com.whatsapp.deviceauth.BiometricAuthPlugin;

/* loaded from: classes2.dex */
public class VerifyPasswordFragment extends Hilt_VerifyPasswordFragment {
    public AbstractC15710nm A00;
    public C14900mE A01;
    public BiometricAuthPlugin A02;
    public C14850m9 A03;

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 12345) {
            A1G(i2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a7  */
    @Override // com.whatsapp.backup.encryptedbackup.PasswordInputFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r14, android.view.View r15) {
        /*
            r13 = this;
            super.A17(r14, r15)
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            int r1 = r0.A04()
            r2 = 7
            r0 = 8
            if (r1 != r0) goto L_0x0015
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            X.016 r0 = r0.A09
            X.C12960it.A1A(r0, r2)
        L_0x0015:
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            int r1 = r0.A04()
            r0 = 10
            r3 = 9
            if (r1 != r0) goto L_0x0028
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            X.016 r0 = r0.A09
            X.C12960it.A1A(r0, r3)
        L_0x0028:
            X.0m9 r10 = r13.A03
            X.0mE r7 = r13.A01
            X.0nm r6 = r13.A00
            X.01d r8 = r13.A09
            X.00k r5 = r13.A0C()
            r11 = 2131887909(0x7f120725, float:1.9410438E38)
            r12 = 2131887908(0x7f120724, float:1.9410436E38)
            X.56N r9 = new X.56N
            r9.<init>()
            com.whatsapp.deviceauth.BiometricAuthPlugin r4 = new com.whatsapp.deviceauth.BiometricAuthPlugin
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12)
            r13.A02 = r4
            int r1 = r13.A00
            r0 = 4
            if (r1 != r0) goto L_0x0084
            android.widget.TextView r1 = r13.A04
            r0 = 2131887995(0x7f12077b, float:1.9410613E38)
            X.C12980iv.A1I(r1, r13, r0)
            android.widget.TextView r1 = r13.A03
            r0 = 2131887994(0x7f12077a, float:1.941061E38)
        L_0x0058:
            X.C12980iv.A1I(r1, r13, r0)
        L_0x005b:
            android.widget.TextView r1 = r13.A01
            r0 = 0
            r1.setVisibility(r0)
            com.whatsapp.components.Button r1 = r13.A08
            r0 = 2131887992(0x7f120778, float:1.9410607E38)
            X.C12980iv.A1I(r1, r13, r0)
            android.widget.TextView r1 = r13.A01
            r0 = 14
            X.AbstractView$OnClickListenerC34281fs.A00(r1, r13, r0)
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            X.0nx r0 = r0.A0B
            X.0o5 r0 = r0.A01
            X.0pF r0 = r0.A00()
            if (r0 == 0) goto L_0x00a7
            r0 = 1
            r13.A1E(r0)
            r13.A1B()
            return
        L_0x0084:
            r0 = 5
            if (r1 != r0) goto L_0x0095
            android.widget.TextView r1 = r13.A04
            r0 = 2131887995(0x7f12077b, float:1.9410613E38)
            X.C12980iv.A1I(r1, r13, r0)
            android.widget.TextView r1 = r13.A03
            r0 = 2131887993(0x7f120779, float:1.9410609E38)
            goto L_0x0058
        L_0x0095:
            if (r1 == r2) goto L_0x0099
            if (r1 != r3) goto L_0x005b
        L_0x0099:
            android.widget.TextView r1 = r13.A04
            r0 = 2131887985(0x7f120771, float:1.9410593E38)
            X.C12980iv.A1I(r1, r13, r0)
            android.widget.TextView r1 = r13.A03
            r0 = 2131887984(0x7f120770, float:1.941059E38)
            goto L_0x0058
        L_0x00a7:
            com.whatsapp.backup.encryptedbackup.EncBackupViewModel r0 = r13.A07
            X.016 r0 = r0.A04
            X.C12960it.A1A(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.encryptedbackup.VerifyPasswordFragment.A17(android.os.Bundle, android.view.View):void");
    }

    public final void A1G(int i) {
        EncBackupViewModel encBackupViewModel;
        int i2;
        if (i == -1 || i == 4) {
            ((PasswordInputFragment) this).A07.A09(6);
            ((PasswordInputFragment) this).A07.A0A.A0A(true);
            int A04 = ((PasswordInputFragment) this).A07.A04();
            if (A04 == 4) {
                ((PasswordInputFragment) this).A07.A0B(302);
                return;
            }
            if (A04 != 5) {
                if (A04 == 7) {
                    encBackupViewModel = ((PasswordInputFragment) this).A07;
                    i2 = 8;
                } else if (A04 == 9) {
                    encBackupViewModel = ((PasswordInputFragment) this).A07;
                    i2 = 10;
                } else {
                    return;
                }
                C12960it.A1A(encBackupViewModel.A09, i2);
            }
            ((PasswordInputFragment) this).A07.A0B(300);
        }
    }
}
