package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass016;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.C12960it;
import X.C14820m6;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class EnableDoneFragment extends Hilt_EnableDoneFragment {
    public AnonymousClass01d A00;
    public C14820m6 A01;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_enable_done);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enable_done_create_button), this, encBackupViewModel, 5);
        AnonymousClass016 r3 = encBackupViewModel.A04;
        C12960it.A19(A0G(), r3, this, 5);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enable_done_cancel_button), this, encBackupViewModel, 6);
        C12960it.A19(A0G(), r3, this, 5);
    }
}
