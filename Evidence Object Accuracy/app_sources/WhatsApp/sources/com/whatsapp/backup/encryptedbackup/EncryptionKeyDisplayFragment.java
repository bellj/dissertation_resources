package com.whatsapp.backup.encryptedbackup;

import X.C12960it;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import com.whatsapp.R;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public class EncryptionKeyDisplayFragment extends WaFragment {
    public RelativeLayout A00;
    public EncBackupViewModel A01;

    @Override // X.AnonymousClass01E
    public boolean A0o(MenuItem menuItem) {
        this.A00.setBackgroundResource(R.drawable.enc_backup_enc_key_bg);
        if (menuItem.getItemId() != 0) {
            return true;
        }
        EncBackupViewModel encBackupViewModel = this.A01;
        ClipboardManager A0B = encBackupViewModel.A0C.A0B();
        String str = (String) encBackupViewModel.A02.A01();
        if (A0B == null || str == null) {
            return true;
        }
        A0B.setPrimaryClip(ClipData.newPlainText(str, str));
        return true;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A01 = (EncBackupViewModel) C12960it.A0M(this);
    }

    @Override // X.AnonymousClass01E, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        contextMenu.add(0, 0, 0, R.string.copy);
        this.A00.setBackgroundResource(R.drawable.enc_backup_enc_key_bg_pressed);
    }
}
