package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C004902f;
import X.C12960it;
import X.C12980iv;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class EncryptionKeyInfoFragment extends Hilt_EncryptionKeyInfoFragment {
    public Button A00;
    public Button A01;
    public FrameLayout A02;
    public TextView A03;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_encryption_key_info);
    }

    @Override // com.whatsapp.backup.encryptedbackup.EncryptionKeyDisplayFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        C004902f r2 = new C004902f(A0E());
        r2.A07(new EncryptionKeyFragment(), R.id.encryption_key_info_encryption_key_container);
        r2.A01();
        ((EncryptionKeyDisplayFragment) this).A00 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.enc_key_background);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(view, R.id.encryption_key_info_encryption_key_container);
        this.A02 = frameLayout;
        frameLayout.setVisibility(4);
        Button button = (Button) AnonymousClass028.A0D(view, R.id.encryption_key_info_middle_button);
        this.A01 = button;
        C12980iv.A15(A02(), button, new Object[]{64}, R.plurals.encrypted_backup_encryption_key_info_button_show, 64);
        AbstractView$OnClickListenerC34281fs.A00(this.A01, this, 5);
        Button button2 = (Button) AnonymousClass028.A0D(view, R.id.encryption_key_info_bottom_button);
        this.A00 = button2;
        C12980iv.A15(A02(), button2, new Object[]{64}, R.plurals.encrypted_backup_encryption_key_info_button_show, 64);
        AbstractView$OnClickListenerC34281fs.A00(this.A00, this, 6);
        this.A03 = C12960it.A0I(view, R.id.encryption_key_info_info);
    }
}
