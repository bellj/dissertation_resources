package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C12960it;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public class DisableDoneFragment extends WaFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_disable_done);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.disable_done_done_button), this, C12960it.A0M(this), 4);
    }
}
