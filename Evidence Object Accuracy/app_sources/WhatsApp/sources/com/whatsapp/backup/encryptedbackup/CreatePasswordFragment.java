package com.whatsapp.backup.encryptedbackup;

import X.C12980iv;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CreatePasswordFragment extends Hilt_CreatePasswordFragment {
    @Override // com.whatsapp.backup.encryptedbackup.PasswordInputFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        int i = ((PasswordInputFragment) this).A00;
        TextView textView = ((PasswordInputFragment) this).A04;
        int i2 = R.string.encrypted_backup_create_password_title_change_password;
        if (i == 1) {
            i2 = R.string.encrypted_backup_create_password_title_enable;
        }
        C12980iv.A1I(textView, this, i2);
        ((PasswordInputFragment) this).A03.setVisibility(4);
        ((PasswordInputFragment) this).A06.setHint(A0H(R.string.encrypted_backup_password_input_hint));
        C12980iv.A1I(((PasswordInputFragment) this).A08, this, R.string.encrypted_backup_create_password_action);
        A1E(true);
        A1B();
    }
}
