package com.whatsapp.backup.encryptedbackup;

import X.AbstractC009404s;
import X.AnonymousClass004;
import X.C12970iu;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_PasswordInputFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = C12970iu.A0l();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A19()
            r2.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.encryptedbackup.Hilt_PasswordInputFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v10, types: [com.whatsapp.backup.encryptedbackup.Hilt_PasswordInputFragment, com.whatsapp.backup.encryptedbackup.Hilt_ConfirmPasswordFragment] */
    /* JADX WARN: Type inference failed for: r2v12, types: [com.whatsapp.backup.encryptedbackup.PasswordInputFragment] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A18() {
        /*
            r3 = this;
            boolean r0 = r3 instanceof com.whatsapp.backup.encryptedbackup.Hilt_VerifyPasswordFragment
            if (r0 != 0) goto L_0x008c
            boolean r0 = r3 instanceof com.whatsapp.backup.encryptedbackup.Hilt_RestorePasswordInputFragment
            if (r0 != 0) goto L_0x0056
            boolean r0 = r3 instanceof com.whatsapp.backup.encryptedbackup.Hilt_CreatePasswordFragment
            if (r0 != 0) goto L_0x0045
            boolean r0 = r3 instanceof com.whatsapp.backup.encryptedbackup.Hilt_ConfirmPasswordFragment
            if (r0 != 0) goto L_0x0034
            boolean r0 = r3.A02
            if (r0 != 0) goto L_0x0033
            r0 = 1
            r3.A02 = r0
            java.lang.Object r0 = r3.generatedComponent()
            X.2Su r0 = (X.AbstractC51092Su) r0
            r2 = r3
            com.whatsapp.backup.encryptedbackup.PasswordInputFragment r2 = (com.whatsapp.backup.encryptedbackup.PasswordInputFragment) r2
        L_0x0020:
            X.2Sw r0 = (X.C51112Sw) r0
            X.01J r1 = r0.A0Y
            X.C12960it.A1B(r1, r2)
            X.01d r0 = X.C12960it.A0Q(r1)
            r2.A09 = r0
            X.018 r0 = X.C12960it.A0R(r1)
            r2.A0A = r0
        L_0x0033:
            return
        L_0x0034:
            r2 = r3
            com.whatsapp.backup.encryptedbackup.Hilt_ConfirmPasswordFragment r2 = (com.whatsapp.backup.encryptedbackup.Hilt_ConfirmPasswordFragment) r2
            boolean r0 = r2.A02
            if (r0 != 0) goto L_0x0033
            r0 = 1
            r2.A02 = r0
            java.lang.Object r0 = r2.generatedComponent()
            X.2Su r0 = (X.AbstractC51092Su) r0
            goto L_0x0020
        L_0x0045:
            r2 = r3
            com.whatsapp.backup.encryptedbackup.Hilt_CreatePasswordFragment r2 = (com.whatsapp.backup.encryptedbackup.Hilt_CreatePasswordFragment) r2
            boolean r0 = r2.A02
            if (r0 != 0) goto L_0x0033
            r0 = 1
            r2.A02 = r0
            java.lang.Object r0 = r2.generatedComponent()
            X.2Su r0 = (X.AbstractC51092Su) r0
            goto L_0x0020
        L_0x0056:
            r2 = r3
            com.whatsapp.backup.encryptedbackup.Hilt_RestorePasswordInputFragment r2 = (com.whatsapp.backup.encryptedbackup.Hilt_RestorePasswordInputFragment) r2
            boolean r0 = r2.A02
            if (r0 != 0) goto L_0x0033
            r0 = 1
            r2.A02 = r0
            java.lang.Object r0 = r2.generatedComponent()
            X.2Su r0 = (X.AbstractC51092Su) r0
            com.whatsapp.backup.encryptedbackup.RestorePasswordInputFragment r2 = (com.whatsapp.backup.encryptedbackup.RestorePasswordInputFragment) r2
            X.2Sw r0 = (X.C51112Sw) r0
            X.01J r1 = r0.A0Y
            X.C12960it.A1B(r1, r2)
            X.01d r0 = X.C12960it.A0Q(r1)
            r2.A09 = r0
            X.018 r0 = X.C12960it.A0R(r1)
            r2.A0A = r0
            X.12P r0 = X.C12980iv.A0W(r1)
            r2.A00 = r0
            X.01N r0 = r1.AMy
            java.lang.Object r0 = r0.get()
            X.18u r0 = (X.C252818u) r0
            r2.A01 = r0
            return
        L_0x008c:
            r2 = r3
            com.whatsapp.backup.encryptedbackup.Hilt_VerifyPasswordFragment r2 = (com.whatsapp.backup.encryptedbackup.Hilt_VerifyPasswordFragment) r2
            boolean r0 = r2.A02
            if (r0 != 0) goto L_0x0033
            r0 = 1
            r2.A02 = r0
            java.lang.Object r0 = r2.generatedComponent()
            X.2Su r0 = (X.AbstractC51092Su) r0
            com.whatsapp.backup.encryptedbackup.VerifyPasswordFragment r2 = (com.whatsapp.backup.encryptedbackup.VerifyPasswordFragment) r2
            X.2Sw r0 = (X.C51112Sw) r0
            X.01J r1 = r0.A0Y
            X.C12960it.A1B(r1, r2)
            X.01d r0 = X.C12960it.A0Q(r1)
            r2.A09 = r0
            X.018 r0 = X.C12960it.A0R(r1)
            r2.A0A = r0
            X.0m9 r0 = X.C12960it.A0S(r1)
            r2.A03 = r0
            X.0mE r0 = X.C12970iu.A0R(r1)
            r2.A01 = r0
            X.0nm r0 = X.C12970iu.A0Q(r1)
            r2.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.encryptedbackup.Hilt_PasswordInputFragment.A18():void");
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = C51052Sq.A01(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
