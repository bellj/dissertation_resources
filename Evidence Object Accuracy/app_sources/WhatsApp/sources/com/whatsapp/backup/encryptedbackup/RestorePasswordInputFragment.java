package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass12P;
import X.C12960it;
import X.C12980iv;
import X.C252818u;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class RestorePasswordInputFragment extends Hilt_RestorePasswordInputFragment {
    public AnonymousClass12P A00;
    public C252818u A01;

    @Override // com.whatsapp.backup.encryptedbackup.PasswordInputFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        C12980iv.A1I(((PasswordInputFragment) this).A04, this, R.string.encrypted_backup_restore_password_input_title);
        C12980iv.A1I(((PasswordInputFragment) this).A03, this, R.string.encrypted_backup_restore_password_input_instruction);
        ((PasswordInputFragment) this).A01.setVisibility(0);
        AbstractView$OnClickListenerC34281fs.A00(((PasswordInputFragment) this).A01, this, 12);
        C12980iv.A1I(((PasswordInputFragment) this).A08, this, R.string.encrypted_backup_restore_password_input_next_button);
        ((PasswordInputFragment) this).A05.setVisibility(0);
        ((PasswordInputFragment) this).A05.setText(R.string.encrypted_backup_used_encryption_key_link);
        AbstractView$OnClickListenerC34281fs.A00(((PasswordInputFragment) this).A05, this, 13);
        C12960it.A19(A0G(), ((PasswordInputFragment) this).A07.A08, this, 10);
    }
}
