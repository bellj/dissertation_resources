package com.whatsapp.backup.encryptedbackup;

import X.AbstractC005002h;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C74203hY;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.base.WaFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* loaded from: classes2.dex */
public class EncBackupMainActivity extends ActivityC13790kL {
    public AnonymousClass01F A00;
    public WaImageButton A01;
    public EncBackupViewModel A02;
    public boolean A03;

    public EncBackupMainActivity() {
        this(0);
    }

    public EncBackupMainActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 15);
    }

    public static /* synthetic */ void A02(EncBackupMainActivity encBackupMainActivity) {
        AnonymousClass01F r3 = encBackupMainActivity.A00;
        if (r3 == null) {
            return;
        }
        if (r3.A03() <= 1) {
            encBackupMainActivity.setResult(0, C12970iu.A0A());
            encBackupMainActivity.finish();
            return;
        }
        String str = ((C004902f) ((AbstractC005002h) r3.A0E.get(r3.A03() - 2))).A0A;
        if (str != null) {
            try {
                int parseInt = Integer.parseInt(str);
                if (encBackupMainActivity.A02.A0E()) {
                    AnonymousClass01F r2 = encBackupMainActivity.A00;
                    if (r2.A03() > 2 || parseInt == 202 || parseInt == 203) {
                        String str2 = ((C004902f) ((AbstractC005002h) r2.A0E.get(r2.A03() - 3))).A0A;
                        if (str2 != null) {
                            parseInt = Integer.parseInt(str2);
                        }
                    }
                }
                encBackupMainActivity.A02.A0B(parseInt);
            } catch (NumberFormatException unused) {
                Log.e("EncBackupMainActivity unable to set fragment request code to proper value after back navigation");
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    public final void A2e(WaFragment waFragment, int i, boolean z) {
        View.OnClickListener onClickListener;
        this.A01.setVisibility(C12960it.A02(z ? 1 : 0));
        WaImageButton waImageButton = this.A01;
        if (z) {
            onClickListener = new ViewOnClickCListenerShape17S0100000_I1(this, 4);
        } else {
            onClickListener = null;
        }
        waImageButton.setOnClickListener(onClickListener);
        ((ActivityC001000l) this).A04.A01(new C74203hY(this, z), this);
        String valueOf = String.valueOf(i);
        AnonymousClass01E A0A = this.A00.A0A(valueOf);
        if (this.A00 == null) {
            return;
        }
        if (A0A == null || A0A.A0e()) {
            C004902f r1 = new C004902f(this.A00);
            r1.A0B(waFragment, valueOf, R.id.fragment_container);
            r1.A0F(valueOf);
            r1.A02();
        }
    }

    @Override // android.app.Activity
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
        Object A01 = this.A02.A03.A01();
        if (A01 != null) {
            AnonymousClass01E A0A = this.A00.A0A(A01.toString());
            if (A0A instanceof EncryptionKeyDisplayFragment) {
                ((EncryptionKeyDisplayFragment) A0A).A00.setBackgroundResource(R.drawable.enc_backup_enc_key_bg);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.enc_backup_main_activity);
        WaImageButton waImageButton = (WaImageButton) AnonymousClass00T.A05(this, R.id.enc_backup_toolbar_button);
        this.A01 = waImageButton;
        AnonymousClass2GF.A01(this, waImageButton, ((ActivityC13830kP) this).A01, R.drawable.ic_back);
        this.A00 = A0V();
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C13000ix.A02(this).A00(EncBackupViewModel.class);
        this.A02 = encBackupViewModel;
        C12960it.A18(this, encBackupViewModel.A03, 6);
        C12960it.A18(this, this.A02.A04, 7);
        C12960it.A17(this, this.A02.A07, 0);
        this.A02.A0C(C12990iw.A0H(this));
    }
}
