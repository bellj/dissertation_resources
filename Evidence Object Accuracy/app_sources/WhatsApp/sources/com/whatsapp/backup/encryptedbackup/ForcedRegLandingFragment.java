package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C12960it;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ForcedRegLandingFragment extends Hilt_ForcedRegLandingFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_forced_reg_landing);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enc_backup_validate_password_continue_button), this, encBackupViewModel, 12);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enc_backup_validate_password_turn_off_button), this, encBackupViewModel, 13);
        if (encBackupViewModel.A04() == 9) {
            C12960it.A0I(view, R.id.enc_backup_validate_password_info_subtitle_info).setText(R.string.encrypted_backup_forced_re_registration_landing_info);
        }
    }
}
