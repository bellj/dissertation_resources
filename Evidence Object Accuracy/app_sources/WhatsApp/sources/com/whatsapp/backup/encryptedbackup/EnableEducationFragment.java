package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass015;
import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public class EnableEducationFragment extends WaFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_enable_education);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        AnonymousClass015 A0M = C12960it.A0M(this);
        TextView A0I = C12960it.A0I(view, R.id.enable_education_use_encryption_key_button);
        Resources A02 = A02();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1O(A1b, 64);
        C12980iv.A15(A02, A0I, A1b, R.plurals.encrypted_backup_use_encryption_key_button, 64);
        AbstractView$OnClickListenerC34281fs.A02(A0I, this, A0M, 7);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enable_education_create_password_button), this, A0M, 8);
    }
}
