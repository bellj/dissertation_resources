package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C004902f;
import X.C12960it;
import X.C12980iv;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConfirmEncryptionKeyFragment extends Hilt_ConfirmEncryptionKeyFragment {
    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_encryption_key_confirm);
    }

    @Override // com.whatsapp.backup.encryptedbackup.EncryptionKeyDisplayFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        C004902f r2 = new C004902f(A0E());
        r2.A07(new EncryptionKeyFragment(), R.id.encryption_key_confirm_encryption_key_container);
        r2.A01();
        ((EncryptionKeyDisplayFragment) this).A00 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.enc_key_background);
        C12980iv.A15(A02(), C12960it.A0I(view, R.id.encryption_key_confirm_title), new Object[]{64}, R.plurals.encrypted_backup_encryption_key_confirm_title, 64);
        TextView A0I = C12960it.A0I(view, R.id.encryption_key_confirm_button_confirm);
        C12980iv.A15(A02(), A0I, new Object[]{64}, R.plurals.encrypted_backup_encryption_key_confirm_button_confirm, 64);
        AbstractView$OnClickListenerC34281fs.A00(A0I, this, 2);
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass028.A0D(view, R.id.encryption_key_confirm_button_cancel), this, 3);
        ((EncryptionKeyDisplayFragment) this).A00.setOnCreateContextMenuListener(this);
    }
}
