package com.whatsapp.backup.encryptedbackup;

import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass3MC;
import X.AnonymousClass3MM;
import X.C12960it;
import X.C12970iu;
import X.C28391Mz;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.backup.encryptedbackup.EncryptionKeyFragment;

/* loaded from: classes2.dex */
public class EncryptionKeyFragment extends Hilt_EncryptionKeyFragment {
    public int A00;
    public EncBackupViewModel A01;
    public AnonymousClass01d A02;
    public AnonymousClass018 A03;
    public CodeInputField[] A04;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_encryption_key);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.whatsapp.CodeInputField[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        InputMethodManager A0Q;
        super.A16(bundle);
        this.A01 = (EncBackupViewModel) C12960it.A0M(this);
        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(view, R.id.encryption_key_vertical_layout);
        this.A04 = new CodeInputField[16];
        int i = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            ViewGroup viewGroup2 = (ViewGroup) viewGroup.getChildAt(i2);
            int i3 = 0;
            do {
                int i4 = (i2 << 2) + i3;
                this.A04[i4] = viewGroup2.getChildAt(i3);
                if (Build.VERSION.SDK_INT >= 21) {
                    this.A04[i4].setLetterSpacing(0.15f);
                }
                if (this.A01.A04() != 1) {
                    CodeInputField codeInputField = this.A04[i4];
                    codeInputField.setEnabled(true);
                    codeInputField.setClickable(true);
                    codeInputField.setLongClickable(true);
                    codeInputField.setOnFocusChangeListener(new View.OnFocusChangeListener(i4) { // from class: X.3MZ
                        public final /* synthetic */ int A00;

                        {
                            this.A00 = r2;
                        }

                        @Override // android.view.View.OnFocusChangeListener
                        public final void onFocusChange(View view2, boolean z) {
                            EncryptionKeyFragment encryptionKeyFragment = EncryptionKeyFragment.this;
                            int i5 = this.A00;
                            if (z) {
                                encryptionKeyFragment.A00 = i5;
                                if (i5 > 0) {
                                    Editable text = encryptionKeyFragment.A04[i5 - 1].getText();
                                    AnonymousClass009.A05(text);
                                    if (text.length() < 4) {
                                        encryptionKeyFragment.A04[encryptionKeyFragment.A00 - 1].requestFocus();
                                    }
                                }
                            }
                        }
                    });
                    codeInputField.addTextChangedListener(new AnonymousClass3MC(codeInputField, this));
                    codeInputField.setOnKeyListener(new View.OnKeyListener() { // from class: X.3Md
                        @Override // android.view.View.OnKeyListener
                        public final boolean onKey(View view2, int i5, KeyEvent keyEvent) {
                            int i6;
                            EncryptionKeyFragment encryptionKeyFragment = EncryptionKeyFragment.this;
                            if (keyEvent.getAction() != 0 || i5 != 67 || (i6 = encryptionKeyFragment.A00) <= 0 || encryptionKeyFragment.A04[i6].getText() == null || encryptionKeyFragment.A04[encryptionKeyFragment.A00].getText().length() != 0) {
                                return false;
                            }
                            CodeInputField codeInputField2 = encryptionKeyFragment.A04[encryptionKeyFragment.A00 - 1];
                            Editable text = codeInputField2.getText();
                            AnonymousClass009.A05(text);
                            text.delete(codeInputField2.length() - 1, codeInputField2.length());
                            codeInputField2.requestFocus();
                            return true;
                        }
                    });
                    if (C28391Mz.A03()) {
                        codeInputField.setCustomInsertionActionModeCallback(new AnonymousClass3MM(this));
                    }
                }
                i3++;
            } while (i3 < 4);
        }
        String str = (String) this.A01.A02.A01();
        if (str != null) {
            A1A(str);
            i = str.length() >> 2;
        }
        if (!(this.A01.A04() == 1 || (A0Q = this.A02.A0Q()) == null)) {
            this.A04[i].requestFocus();
            A0Q.toggleSoftInput(1, 1);
        }
    }

    public void A1A(String str) {
        String str2;
        if (str != null) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                int i3 = i2 << 2;
                int length = str.length();
                String substring = str.substring(i << 2, Math.min(i3, length));
                if (this.A04[i].getText() != null) {
                    str2 = C12970iu.A0p(this.A04[i]);
                } else {
                    str2 = "";
                }
                if (!substring.equals(str2)) {
                    this.A04[i].setText(substring);
                    this.A04[i].setSelection(substring.length());
                }
                if (length >= i3 && i2 < 16) {
                    i = i2;
                } else {
                    return;
                }
            }
        }
    }
}
