package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C44891zj;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class EnableInfoFragment extends Hilt_EnableInfoFragment {
    public AnonymousClass018 A00;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_enable_info);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        long j;
        long j2;
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        TextView A0I = C12960it.A0I(view, R.id.enable_info_backup_size_message);
        C14820m6 r5 = encBackupViewModel.A0D;
        String A09 = r5.A09();
        if (A09 != null) {
            j = r5.A08(A09);
        } else {
            j = 0;
        }
        String A092 = r5.A09();
        if (A092 == null) {
            j2 = 0;
        } else if (TextUtils.isEmpty(A092)) {
            j2 = -1;
        } else {
            j2 = r5.A00.getLong(C12960it.A0d(A092, C12960it.A0k("gdrive_last_successful_backup_media_size:")), -1);
        }
        if (j > 0 || j == -1) {
            C12960it.A0I(view, R.id.enable_info_enc_backup_info).setText(R.string.encrypted_backup_enable_info_subtitle_gdrive);
        }
        if (j > 0 && j2 >= 0) {
            A0I.setVisibility(0);
            Object[] A1a = C12980iv.A1a();
            A1a[0] = C44891zj.A03(this.A00, j);
            A0I.setText(Html.fromHtml(C12970iu.A0q(this, C44891zj.A03(this.A00, j2), A1a, 1, R.string.encrypted_backup_enable_info_backup_size_message)));
        }
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enable_info_turn_on_button), this, encBackupViewModel, 9);
    }
}
