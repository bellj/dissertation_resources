package com.whatsapp.backup.encryptedbackup;

import X.AbstractC14440lR;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.C004802e;
import X.C100694mK;
import X.C102614pQ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16550pE;
import X.C51282Tp;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.backup.encryptedbackup.PasswordInputFragment;
import com.whatsapp.components.Button;
import com.whatsapp.util.Log;
import java.text.Normalizer;

/* loaded from: classes2.dex */
public abstract class PasswordInputFragment extends Hilt_PasswordInputFragment {
    public int A00;
    public TextView A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public TextView A05;
    public CodeInputField A06;
    public EncBackupViewModel A07;
    public Button A08;
    public AnonymousClass01d A09;
    public AnonymousClass018 A0A;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_password_input);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        this.A07 = encBackupViewModel;
        this.A00 = encBackupViewModel.A04();
        this.A04 = C12960it.A0I(view, R.id.enc_backup_password_input_title);
        this.A03 = C12960it.A0I(view, R.id.enc_backup_password_input_instruction);
        this.A01 = C12960it.A0I(view, R.id.enc_backup_password_input_forgot_password);
        this.A06 = (CodeInputField) AnonymousClass028.A0D(view, R.id.enc_backup_password_input);
        this.A02 = C12960it.A0I(view, R.id.enc_backup_password_input_requirement);
        this.A08 = (Button) AnonymousClass028.A0D(view, R.id.enc_backup_password_input_button);
        this.A05 = C12960it.A0I(view, R.id.enc_backup_password_input_use_encryption_key_button);
        this.A01.setVisibility(8);
        this.A05.setVisibility(8);
        this.A06.addTextChangedListener(new C100694mK(this));
        A1F(false);
        C12960it.A19(A0G(), this.A07.A04, this, 9);
    }

    public void A1A() {
        EncBackupViewModel encBackupViewModel;
        AbstractC14440lR r2;
        int i;
        String str;
        EncBackupViewModel encBackupViewModel2;
        int i2;
        if (this instanceof VerifyPasswordFragment) {
            Editable text = this.A06.getText();
            if (text != null) {
                EncBackupViewModel encBackupViewModel3 = this.A07;
                encBackupViewModel3.A05.A0B(Normalizer.normalize(text.toString().trim(), Normalizer.Form.NFKC));
                encBackupViewModel = this.A07;
                AnonymousClass016 r1 = encBackupViewModel.A04;
                C12960it.A1A(r1, 2);
                if (encBackupViewModel.A0B.A01.A00() != null) {
                    r2 = encBackupViewModel.A0I;
                    i = 11;
                } else {
                    Log.i("EncBackupViewModel/no attempts remaining");
                    C12970iu.A1Q(r1, 7);
                    return;
                }
            } else {
                return;
            }
        } else if (!(this instanceof RestorePasswordInputFragment)) {
            if (!(this instanceof CreatePasswordFragment)) {
                ConfirmPasswordFragment confirmPasswordFragment = (ConfirmPasswordFragment) this;
                Editable text2 = ((PasswordInputFragment) confirmPasswordFragment).A06.getText();
                if (text2 == null || !Normalizer.normalize(text2.toString().trim(), Normalizer.Form.NFKC).equals(confirmPasswordFragment.A00)) {
                    confirmPasswordFragment.A1D(confirmPasswordFragment.A0I(R.string.encrypted_backup_confirm_password_mismatch_error), true);
                    return;
                }
                int i3 = ((PasswordInputFragment) confirmPasswordFragment).A00;
                encBackupViewModel2 = ((PasswordInputFragment) confirmPasswordFragment).A07;
                if (i3 == 1) {
                    i2 = 500;
                } else {
                    encBackupViewModel2.A06();
                    return;
                }
            } else {
                Editable text3 = this.A06.getText();
                if (text3 != null) {
                    String normalize = Normalizer.normalize(text3.toString().trim(), Normalizer.Form.NFKC);
                    int A00 = C16550pE.A00(normalize);
                    if (A00 == 1) {
                        Resources A02 = A02();
                        Object[] objArr = new Object[1];
                        C12960it.A1P(objArr, 6, 0);
                        str = A02.getQuantityString(R.plurals.encrypted_backup_password_input_requirement_warning_too_short, 6, objArr);
                    } else if (A00 == 2) {
                        Resources A022 = A02();
                        Object[] objArr2 = new Object[1];
                        C12960it.A1P(objArr2, 1, 0);
                        str = A022.getQuantityString(R.plurals.encrypted_backup_password_input_requirement_warning_too_few_letters, 1, objArr2);
                    } else if (A00 == 3) {
                        str = A0I(R.string.encrypted_backup_password_input_requirement_warning_weak);
                    } else if (A00 == 4) {
                        this.A07.A05.A0B(normalize);
                        encBackupViewModel2 = this.A07;
                        i2 = 400;
                    } else {
                        return;
                    }
                    A1D(str, true);
                    return;
                }
                return;
            }
            encBackupViewModel2.A0B(i2);
            return;
        } else {
            Editable text4 = this.A06.getText();
            if (text4 != null) {
                EncBackupViewModel encBackupViewModel4 = this.A07;
                encBackupViewModel4.A05.A0B(Normalizer.normalize(text4.toString().trim(), Normalizer.Form.NFKC));
                encBackupViewModel = this.A07;
                boolean A0F = encBackupViewModel.A0H.A0F();
                AnonymousClass016 r12 = encBackupViewModel.A04;
                if (!A0F) {
                    C12960it.A1A(r12, 4);
                    return;
                }
                C12960it.A1A(r12, 2);
                r2 = encBackupViewModel.A0I;
                i = 10;
            } else {
                return;
            }
        }
        r2.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(encBackupViewModel, i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        if (r1 <= 1) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0096, code lost:
        if (r1 == 0) goto L_0x0098;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1B() {
        /*
            r9 = this;
            boolean r0 = r9 instanceof com.whatsapp.backup.encryptedbackup.VerifyPasswordFragment
            if (r0 != 0) goto L_0x0085
            boolean r0 = r9 instanceof com.whatsapp.backup.encryptedbackup.RestorePasswordInputFragment
            if (r0 != 0) goto L_0x0085
            boolean r1 = r9 instanceof com.whatsapp.backup.encryptedbackup.CreatePasswordFragment
            com.whatsapp.CodeInputField r0 = r9.A06
            android.text.Editable r0 = r0.getText()
            if (r1 != 0) goto L_0x0041
            r1 = 1
            if (r0 == 0) goto L_0x003f
            java.lang.String r0 = r0.toString()
            int r0 = X.C16550pE.A00(r0)
            if (r0 <= r1) goto L_0x003f
        L_0x001f:
            r9.A1F(r1)
            r1 = 2131887921(0x7f120731, float:1.9410463E38)
            android.content.Context r3 = r9.A0p()
            if (r3 == 0) goto L_0x003e
            r2 = 0
            android.widget.TextView r0 = r9.A02
            r0.setText(r1)
            android.widget.TextView r1 = r9.A02
            r0 = 2131100809(0x7f060489, float:1.781401E38)
            X.C12960it.A0s(r3, r1, r0)
            android.widget.TextView r0 = r9.A02
            r0.setVisibility(r2)
        L_0x003e:
            return
        L_0x003f:
            r1 = 0
            goto L_0x001f
        L_0x0041:
            r8 = 1
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = r0.toString()
            int r1 = X.C16550pE.A00(r0)
            r0 = 1
            if (r1 > r8) goto L_0x0050
        L_0x004f:
            r0 = 0
        L_0x0050:
            r9.A1F(r0)
            r7 = 2131755068(0x7f10003c, float:1.9141005E38)
            android.content.Context r6 = r9.A0p()
            if (r6 == 0) goto L_0x003e
            r5 = 0
            X.AnonymousClass009.A0E(r8)
            android.widget.TextView r4 = r9.A02
            android.content.res.Resources r3 = r9.A02()
            java.lang.Object[] r2 = X.C12980iv.A1a()
            r1 = 6
            X.C12960it.A1P(r2, r1, r5)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r2[r8] = r0
            X.C12980iv.A15(r3, r4, r2, r7, r1)
            android.widget.TextView r1 = r9.A02
            r0 = 2131100809(0x7f060489, float:1.781401E38)
            X.C12960it.A0s(r6, r1, r0)
            android.widget.TextView r0 = r9.A02
            r0.setVisibility(r5)
            return
        L_0x0085:
            com.whatsapp.CodeInputField r0 = r9.A06
            android.text.Editable r0 = r0.getText()
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = r0.toString()
            int r1 = X.C16550pE.A00(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0099
        L_0x0098:
            r0 = 0
        L_0x0099:
            r9.A1F(r0)
            android.widget.TextView r1 = r9.A02
            r0 = 8
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.encryptedbackup.PasswordInputFragment.A1B():void");
    }

    public final void A1C(DialogInterface.OnClickListener onClickListener, String str, boolean z) {
        C004802e A0S = C12980iv.A0S(A0C());
        A0S.A0A(str);
        A0S.setPositiveButton(R.string.ok, onClickListener);
        C12970iu.A1J(A0S);
        A1E(z);
        A1F(false);
        C51282Tp.A01(this.A09);
        Log.i(C12960it.A0d(str, C12960it.A0k("PasswordInputFragment/error modal shown with message: ")));
    }

    public void A1D(String str, boolean z) {
        Context A0p = A0p();
        if (A0p != null) {
            this.A02.setText(str);
            C12960it.A0s(A0p, this.A02, R.color.red_error);
            this.A02.setVisibility(0);
            A1E(z);
            A1F(false);
            C51282Tp.A01(this.A09);
            Log.i(C12960it.A0d(str, C12960it.A0k("PasswordInputFragment/error message shown: ")));
        }
    }

    public void A1E(boolean z) {
        this.A06.setEnabled(z);
        if (z) {
            InputMethodManager A0Q = this.A09.A0Q();
            if (A0Q != null && !A0Q.isAcceptingText()) {
                A0Q.toggleSoftInput(1, 1);
            }
            this.A06.requestFocus();
        }
    }

    public void A1F(boolean z) {
        C102614pQ r0;
        CodeInputField codeInputField;
        this.A08.setEnabled(z);
        Button button = this.A08;
        if (z) {
            AbstractView$OnClickListenerC34281fs.A00(button, this, 11);
            codeInputField = this.A06;
            r0 = new TextView.OnEditorActionListener() { // from class: X.4pQ
                @Override // android.widget.TextView.OnEditorActionListener
                public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    PasswordInputFragment passwordInputFragment = PasswordInputFragment.this;
                    if (i != 6) {
                        return false;
                    }
                    passwordInputFragment.A1A();
                    return false;
                }
            };
        } else {
            r0 = null;
            button.setOnClickListener(null);
            codeInputField = this.A06;
        }
        codeInputField.setOnEditorActionListener(r0);
    }
}
