package com.whatsapp.backup.encryptedbackup;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class EnabledLandingFragment extends Hilt_EnabledLandingFragment {
    public C14820m6 A00;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.enc_backup_enabled_landing);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A16(bundle);
        EncBackupViewModel encBackupViewModel = (EncBackupViewModel) C12960it.A0M(this);
        TextView A0I = C12960it.A0I(view, R.id.enc_backup_enabled_landing_password_button);
        C14820m6 r1 = encBackupViewModel.A0D;
        String A09 = r1.A09();
        if (A09 != null && r1.A08(A09) > 0) {
            C12960it.A0I(view, R.id.enc_backup_enabled_landing_privacy_notice).setText(R.string.encrypted_backup_privacy_notice_gdrive);
        }
        if (this.A00.A00.getBoolean("encrypted_backup_using_encryption_key", false)) {
            TextView A0I2 = C12960it.A0I(view, R.id.enc_backup_enabled_landing_restore_notice);
            Resources A02 = A02();
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, 64, 0);
            C12980iv.A15(A02, A0I2, A1b, R.plurals.encrypted_backup_restore_notice_encryption_key, 64);
            C12980iv.A1I(A0I, this, R.string.encrypted_backup_enabled_landing_button_create_password);
        }
        AbstractView$OnClickListenerC34281fs.A02(A0I, this, encBackupViewModel, 10);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(view, R.id.enc_backup_enabled_landing_disable_button), this, encBackupViewModel, 11);
    }
}
