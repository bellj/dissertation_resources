package com.whatsapp.backup.google;

import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractIntentServiceC27641Ik;
import X.AnonymousClass022;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10N;
import X.AnonymousClass1D6;
import X.C05450Pp;
import X.C12960it;
import X.C12970iu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16490p7;
import X.C16590pI;
import X.C17050qB;
import X.C17220qS;
import X.C19380u1;
import X.C20850wQ;
import X.C21630xj;
import X.C21710xr;
import X.C22660zR;
import X.C22730zY;
import X.C25721Am;
import X.C26551Dx;
import X.C26981Fo;
import X.C27051Fv;
import X.C44921zm;
import X.EnumC03840Ji;
import android.app.Notification;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.whatsapp.Me;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public final class GoogleBackupService extends AbstractIntentServiceC27641Ik {
    public int A00;
    public AbstractC15710nm A01;
    public C14330lG A02;
    public C14900mE A03;
    public C15570nT A04;
    public C15820nx A05;
    public C25721Am A06;
    public C22730zY A07;
    public C26551Dx A08;
    public AnonymousClass10N A09;
    public AnonymousClass10J A0A;
    public AnonymousClass10K A0B;
    public C27051Fv A0C;
    public AnonymousClass1D6 A0D;
    public C19380u1 A0E;
    public C15810nw A0F;
    public C17050qB A0G;
    public C14830m7 A0H;
    public C16590pI A0I;
    public C15890o4 A0J;
    public C14820m6 A0K;
    public C15880o3 A0L;
    public C20850wQ A0M;
    public C16490p7 A0N;
    public C21630xj A0O;
    public C14850m9 A0P;
    public C16120oU A0Q;
    public C17220qS A0R;
    public C22660zR A0S;
    public C26981Fo A0T;
    public AbstractC15850o0 A0U;
    public C21710xr A0V;
    public Map A0W;
    public Random A0X;
    public boolean A0Y = false;
    public boolean A0Z = false;
    public final Binder A0a = new Binder();
    public final Object A0b = C12970iu.A0l();
    public final ArrayList A0c = C12960it.A0l();
    public final AtomicBoolean A0d = new AtomicBoolean(false);

    public GoogleBackupService() {
        super(GoogleBackupService.class.getCanonicalName());
    }

    public final String A01() {
        C15570nT r0 = this.A04;
        r0.A08();
        Me me = r0.A00;
        if (me == null) {
            Log.i("gdrive-service/my-jid/me is null, can't proceed");
            return null;
        }
        String str = me.jabber_id;
        if (str != null) {
            return str;
        }
        Log.e("gdrive-service/my-jid/jidUser is null, fatal error.");
        return null;
    }

    @Override // android.app.IntentService, android.app.Service
    public IBinder onBind(Intent intent) {
        return this.A0a;
    }

    @Override // X.AbstractIntentServiceC27651Il, android.app.IntentService, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
        this.A0B.A04();
    }

    @Override // android.app.IntentService, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass10N r1 = this.A09;
        r1.A00 = -1;
        r1.A01 = -1;
        AnonymousClass10J r3 = this.A0A;
        r3.A06.set(0);
        r3.A05.set(0);
        r3.A04.set(0);
        r3.A07.set(0);
        r3.A03.set(0);
        this.A0B.A05();
        try {
            for (C05450Pp r0 : (List) ((AnonymousClass022) this.A0V.get()).A02().get()) {
                if (r0.A03 == EnumC03840Ji.RUNNING) {
                    break;
                }
            }
        } catch (InterruptedException | ExecutionException unused) {
        }
        this.A08.A02();
        C44921zm.A01();
        this.A07.A05();
        this.A07.A0j.set(false);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Multi-variable search result rejected for r10v37, resolved type: byte[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:562:0x0ee6, code lost:
        if (r1.A00() == false) goto L_0x11c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:563:0x0ee8, code lost:
        r14 = r7.A00;
        r5 = r7.A0F;
        r11.A0f(r5);
        r11.A0n(r5, r10.A05);
        r11.A0o(r5, r10.A04);
        r11.A0m(r5, r10.A03("mediaSize", -1));
        r11.A0p(r5, r10.A03("videoSize", -1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:565:0x0f14, code lost:
        if (r9 != null) goto L_0x0f18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:566:0x0f16, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:567:0x0f18, code lost:
        r1 = r9.optBoolean("encryptedBackupEnabled", false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:568:0x0f1e, code lost:
        r11.A0x(r5, r1);
        r1 = r10.A02("backupFrequency", -1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:569:0x0f28, code lost:
        if (r1 < 0) goto L_0x0f7d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:570:0x0f2a, code lost:
        r19 = r11.A1H(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:571:0x0f2e, code lost:
        r1 = r10.A02("backupNetworkSettings", -1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:572:0x0f34, code lost:
        if (r1 < 0) goto L_0x0f3c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:573:0x0f36, code lost:
        r19 = r19 & r13.A0C(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:575:0x0f3e, code lost:
        if (r9 == null) goto L_0x0f7b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:577:0x0f44, code lost:
        if (r9.has("includeVideosInBackup") == false) goto L_0x0f70;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:578:0x0f46, code lost:
        r1 = r9.optBoolean("includeVideosInBackup", true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:579:0x0f4a, code lost:
        r11.A11(r1);
        r11.A0R(r10.A02("backupQuotaWarningVisibility", 0));
        r11.A0q("backup_quota_user_notice_period_end_timestamp", r10.A03("backupQuotaUserNoticePeriodEndDate", 0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:580:0x0f67, code lost:
        if (r9 == null) goto L_0x0f85;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:582:0x0f6d, code lost:
        if (r9.has("localSettings") == false) goto L_0x0f85;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:584:0x0f70, code lost:
        r1 = r10.A06();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:585:0x0f74, code lost:
        if (r1 == null) goto L_0x0f7b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:586:0x0f76, code lost:
        r1 = r1.optBoolean("includeVideosInBackup", true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:587:0x0f7b, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:588:0x0f7d, code lost:
        r19 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:589:0x0f80, code lost:
        r1 = r9.getJSONObject("localSettings");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:590:0x0f85, code lost:
        r2 = r10.A06();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:591:0x0f89, code lost:
        if (r2 == null) goto L_0x0f9b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:593:0x0f8f, code lost:
        if (r2.has("localSettings") == false) goto L_0x0f9b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:594:0x0f91, code lost:
        r1 = r2.getJSONObject("localSettings");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:595:0x0f95, code lost:
        if (r1 == null) goto L_0x0fa5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:596:0x0f97, code lost:
        r11.A0y(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:597:0x0f9b, code lost:
        com.whatsapp.util.Log.w("gdrive-api-v2/backup/get-local-settings/localSettings-is-missing");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:598:0x0fa1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:599:0x0fa2, code lost:
        com.whatsapp.util.Log.e("gdrive-api-v2/backup/get-local-settings/failed to parse", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0224, code lost:
        if (r2 == 1) goto L_0x0226;
     */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x07d6 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x0801 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x0b5e A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:442:0x0b67 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:445:0x0b74 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:447:0x0bcc  */
    /* JADX WARNING: Removed duplicated region for block: B:451:0x0bda A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:468:0x0c59 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:471:0x0c62 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:481:0x0ca7 A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:482:0x0cab A[Catch: 3yY -> 0x0d16, 3yN -> 0x0ce9, 3yT -> 0x0cdb, 3yR -> 0x0ccd, 2AP -> 0x0cbf, 3yP -> 0x0cb1, all -> 0x0d31, TryCatch #12 {3yP -> 0x0cb1, blocks: (B:309:0x074e, B:311:0x0758, B:312:0x075c, B:313:0x0761, B:316:0x0771, B:318:0x0777, B:320:0x077d, B:324:0x078a, B:326:0x07a3, B:327:0x07a8, B:329:0x07d6, B:330:0x07e1, B:332:0x0801, B:333:0x0827, B:335:0x082d, B:337:0x0835, B:338:0x084b, B:340:0x0853, B:342:0x085b, B:344:0x085f, B:345:0x0860, B:347:0x086e, B:348:0x0874, B:350:0x087a, B:351:0x089a, B:353:0x08a0, B:354:0x08ae, B:356:0x08c2, B:357:0x08cf, B:358:0x08d4, B:359:0x08dd, B:361:0x08e3, B:363:0x08ed, B:364:0x090b, B:365:0x0911, B:366:0x0916, B:367:0x0917, B:369:0x091f, B:371:0x0923, B:372:0x0924, B:373:0x0929, B:374:0x092a, B:375:0x0932, B:377:0x0938, B:378:0x095c, B:380:0x0962, B:382:0x0970, B:383:0x0976, B:387:0x09ad, B:388:0x09b2, B:389:0x0a1f, B:391:0x0a27, B:393:0x0a31, B:394:0x0a50, B:396:0x0a5a, B:397:0x0a71, B:398:0x0a7a, B:399:0x0a82, B:401:0x0a8a, B:403:0x0a8e, B:405:0x0a92, B:407:0x0a96, B:409:0x0a9a, B:411:0x0a9e, B:413:0x0aa2, B:414:0x0aa3, B:415:0x0aa4, B:416:0x0aa5, B:417:0x0aa6, B:418:0x0aa7, B:419:0x0aa8, B:421:0x0aae, B:422:0x0add, B:424:0x0b11, B:426:0x0b19, B:428:0x0b21, B:429:0x0b31, B:431:0x0b37, B:434:0x0b46, B:436:0x0b4b, B:437:0x0b4e, B:439:0x0b57, B:441:0x0b5e, B:442:0x0b67, B:443:0x0b6e, B:445:0x0b74, B:446:0x0b7c, B:449:0x0bcf, B:451:0x0bda, B:453:0x0bef, B:454:0x0bf5, B:456:0x0c02, B:457:0x0c08, B:459:0x0c28, B:461:0x0c2e, B:462:0x0c32, B:463:0x0c4a, B:465:0x0c50, B:468:0x0c59, B:469:0x0c5c, B:471:0x0c62, B:473:0x0c6a, B:476:0x0c7a, B:477:0x0c91, B:479:0x0c97, B:481:0x0ca7, B:482:0x0cab, B:483:0x0cb0), top: B:850:0x074e, outer: #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01e7 A[Catch: all -> 0x0247, TRY_ENTER, TryCatch #20 {all -> 0x0247, blocks: (B:14:0x002a, B:16:0x0032, B:18:0x003c, B:20:0x0044, B:21:0x006d, B:23:0x0073, B:24:0x007d, B:25:0x00a5, B:27:0x00c2, B:73:0x01e1, B:75:0x01e7, B:76:0x01eb, B:78:0x01f1, B:81:0x01ff, B:82:0x0200, B:83:0x0219, B:85:0x021b, B:90:0x0227, B:91:0x0237, B:93:0x023d), top: B:853:0x002a, inners: #42 }] */
    /* JADX WARNING: Removed duplicated region for block: B:863:0x16e2 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x023d A[Catch: all -> 0x0247, LOOP:4: B:91:0x0237->B:93:0x023d, LOOP_END, TRY_LEAVE, TryCatch #20 {all -> 0x0247, blocks: (B:14:0x002a, B:16:0x0032, B:18:0x003c, B:20:0x0044, B:21:0x006d, B:23:0x0073, B:24:0x007d, B:25:0x00a5, B:27:0x00c2, B:73:0x01e1, B:75:0x01e7, B:76:0x01eb, B:78:0x01f1, B:81:0x01ff, B:82:0x0200, B:83:0x0219, B:85:0x021b, B:90:0x0227, B:91:0x0237, B:93:0x023d), top: B:853:0x002a, inners: #42 }] */
    /* JADX WARNING: Removed duplicated region for block: B:985:? A[RETURN, SYNTHETIC] */
    @Override // android.app.IntentService
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r53) {
        /*
        // Method dump skipped, instructions count: 5912
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.GoogleBackupService.onHandleIntent(android.content.Intent):void");
    }

    @Override // android.app.IntentService, android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        Notification notification;
        int onStartCommand = super.onStartCommand(intent, i, i2);
        if (intent == null) {
            return onStartCommand;
        }
        synchronized (this.A0b) {
            Notification A00 = this.A0B.A00(C16590pI.A00(this.A0I), intent.getAction());
            if (!(this.A00 == 0 || (notification = this.A0B.A0R) == null)) {
                A00 = notification;
            }
            this.A0B.A03();
            try {
                startForeground(5, A00);
                this.A00++;
            } catch (IllegalStateException e) {
                Log.w("Failed to start foreground service GoogleBackupService", e);
                stopSelf();
            }
        }
        return onStartCommand;
    }
}
