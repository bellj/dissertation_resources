package com.whatsapp.backup.google;

import X.AbstractC005102i;
import X.AbstractC116175Uk;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC32851cq;
import X.AbstractC453621g;
import X.AbstractC87974Dt;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass022;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass116;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass19M;
import X.AnonymousClass1D6;
import X.AnonymousClass1Tv;
import X.AnonymousClass29H;
import X.AnonymousClass29I;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2KB;
import X.AnonymousClass3FU;
import X.AnonymousClass3WX;
import X.C004902f;
import X.C05450Pp;
import X.C102834pm;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16590pI;
import X.C17050qB;
import X.C18260sA;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19500uD;
import X.C21710xr;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22730zY;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C25721Am;
import X.C25991Bp;
import X.C26061Bw;
import X.C26551Dx;
import X.C27631Ih;
import X.C28181Ma;
import X.C35751ig;
import X.C36021jC;
import X.C41691tw;
import X.C43951xu;
import X.C44771zW;
import X.C44891zj;
import X.C44981zs;
import X.C58892rw;
import X.C68653Wf;
import X.C84203ya;
import X.C84213yb;
import X.C84223yc;
import X.C84233yd;
import X.C84243ye;
import X.C84253yf;
import X.C88134Ek;
import X.C90054Mk;
import X.DialogC72873fJ;
import X.DialogInterface$OnCancelListenerC96164ex;
import X.ProgressDialogC48342Fq;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1100000_I0;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaLinearLayout;
import com.whatsapp.WaTextView;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.settings.SettingsChat;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class SettingsGoogleDrive extends ActivityC13790kL implements AnonymousClass29H, AnonymousClass29I {
    public View.OnClickListener A00;
    public View.OnClickListener A01;
    public View.OnClickListener A02;
    public View.OnClickListener A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public Button A0A;
    public ImageView A0B;
    public ImageView A0C;
    public ProgressBar A0D;
    public TextView A0E;
    public TextView A0F;
    public TextView A0G;
    public TextView A0H;
    public TextView A0I;
    public TextView A0J;
    public TextView A0K;
    public TextView A0L;
    public TextView A0M;
    public TextView A0N;
    public SwitchCompat A0O;
    public SwitchCompat A0P;
    public TextEmojiLabel A0Q;
    public WaLinearLayout A0R;
    public WaTextView A0S;
    public WaTextView A0T;
    public WaTextView A0U;
    public C19500uD A0V;
    public C15820nx A0W;
    public C25721Am A0X;
    public C22730zY A0Y;
    public C26551Dx A0Z;
    public AnonymousClass10J A0a;
    public AnonymousClass10K A0b;
    public DialogC72873fJ A0c;
    public AnonymousClass1D6 A0d;
    public SettingsGoogleDriveViewModel A0e;
    public AnonymousClass116 A0f;
    public AbstractC453621g A0g;
    public C17050qB A0h;
    public C16590pI A0i;
    public C15890o4 A0j;
    public C18260sA A0k;
    public C16120oU A0l;
    public AnonymousClass12U A0m;
    public C21710xr A0n;
    public boolean A0o;
    public boolean A0p;
    public String[] A0q;
    public final ConditionVariable A0r;
    public final AbstractC32851cq A0s;
    public volatile boolean A0t;

    public SettingsGoogleDrive() {
        this(0);
        this.A0s = new AnonymousClass3WX(this);
        this.A0r = new ConditionVariable(false);
    }

    public SettingsGoogleDrive(int i) {
        this.A0p = false;
        A0R(new C102834pm(this));
    }

    public static /* synthetic */ void A02(AccountManagerFuture accountManagerFuture, AuthRequestDialogFragment authRequestDialogFragment, SettingsGoogleDrive settingsGoogleDrive) {
        try {
            Log.i("settings-gdrive/show-accounts/waiting-for-add-account-activity-to-return");
            Bundle bundle = (Bundle) accountManagerFuture.getResult();
            if (!bundle.containsKey("authAccount")) {
                Log.e("settings-gdrive/error-during-add-account/account-manager-returned-with-no-account-name");
            } else {
                settingsGoogleDrive.A2m(authRequestDialogFragment, String.valueOf(bundle.get("authAccount")));
            }
        } catch (AuthenticatorException | IOException e) {
            Log.e("settings-gdrive/error-during-add-account", e);
        } catch (OperationCanceledException e2) {
            Log.i("settings-gdrive/user-canceled-add-account-operation", e2);
        }
    }

    public static void A03(View view, int i) {
        if (view.getVisibility() != i) {
            view.setVisibility(i);
        }
    }

    public static /* synthetic */ void A09(View view, SettingsGoogleDrive settingsGoogleDrive) {
        int i;
        if (view == settingsGoogleDrive.A04) {
            settingsGoogleDrive.A2i();
            return;
        }
        if (view == settingsGoogleDrive.A09) {
            Log.i("settings-gdrive/toggle-network-pref");
            if (!C44771zW.A0G(((ActivityC13810kN) settingsGoogleDrive).A09)) {
                if (C44771zW.A0H(((ActivityC13810kN) settingsGoogleDrive).A09)) {
                    i = R.string.settings_gdrive_please_wait_for_media_restore_to_finish_before_account_change;
                    settingsGoogleDrive.Ado(i);
                }
                settingsGoogleDrive.A0P.toggle();
                ((ActivityC13830kP) settingsGoogleDrive).A05.Ab2(new RunnableBRunnable0Shape0S0101000_I0(settingsGoogleDrive, settingsGoogleDrive.A0P.isChecked() ? 1 : 0, 4));
                return;
            }
        } else if (view == settingsGoogleDrive.A07) {
            Log.i("settings-gdrive/show-freq-pref");
            SingleChoiceListDialogFragment singleChoiceListDialogFragment = new SingleChoiceListDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 10);
            bundle.putString("title", settingsGoogleDrive.getString(R.string.settings_gdrive_backup_options_title));
            bundle.putStringArray("items", settingsGoogleDrive.A0q);
            int A01 = ((ActivityC13810kN) settingsGoogleDrive).A09.A01();
            int i2 = 0;
            while (true) {
                int[] iArr = SettingsGoogleDriveViewModel.A0i;
                if (i2 < iArr.length) {
                    if (iArr[i2] == A01) {
                        break;
                    }
                    i2++;
                } else {
                    StringBuilder sb = new StringBuilder("settings-gdrive/get-backup-freq-index/");
                    sb.append(A01);
                    Log.e(sb.toString());
                    i2 = 0;
                    break;
                }
            }
            bundle.putInt("selected_item_index", i2);
            singleChoiceListDialogFragment.A0U(bundle);
            if (!settingsGoogleDrive.A2p()) {
                C004902f r1 = new C004902f(settingsGoogleDrive.A0V());
                r1.A09(singleChoiceListDialogFragment, null);
                r1.A02();
                return;
            }
            return;
        } else if (view != settingsGoogleDrive.A08) {
            throw new IllegalArgumentException("Can't handle the click event for the pref view");
        } else if (!C44771zW.A0G(((ActivityC13810kN) settingsGoogleDrive).A09)) {
            if (C44771zW.A0H(((ActivityC13810kN) settingsGoogleDrive).A09)) {
                settingsGoogleDrive.Ado(R.string.settings_gdrive_please_wait_for_media_restore_to_finish_before_account_change);
            } else {
                settingsGoogleDrive.A0O.toggle();
            }
            SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = settingsGoogleDrive.A0e;
            boolean isChecked = settingsGoogleDrive.A0O.isChecked();
            settingsGoogleDriveViewModel.A0F.A0B(Boolean.valueOf(isChecked));
            settingsGoogleDriveViewModel.A0a.A11(isChecked);
            settingsGoogleDriveViewModel.A04();
            return;
        }
        i = R.string.settings_gdrive_please_wait_for_backup_to_finish_before_change;
        settingsGoogleDrive.Ado(i);
    }

    public static /* synthetic */ void A0A(AuthRequestDialogFragment authRequestDialogFragment, SettingsGoogleDrive settingsGoogleDrive, String str) {
        boolean z;
        String obj;
        Account[] accountsByType = AccountManager.get(settingsGoogleDrive).getAccountsByType("com.google");
        int length = accountsByType.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (TextUtils.equals(accountsByType[i].name, str)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        AnonymousClass009.A05(str);
        if (z) {
            settingsGoogleDrive.A2m(authRequestDialogFragment, str);
            return;
        }
        try {
            Bundle result = AccountManager.get(settingsGoogleDrive).addAccount("com.google", null, null, null, settingsGoogleDrive, null, null).getResult();
            if (!result.containsKey("authAccount")) {
                obj = "settings-gdrive/error-during-media-restore/account-manager-returned-with-no-account-name";
            } else if (!str.equals(result.get("authAccount"))) {
                StringBuilder sb = new StringBuilder();
                sb.append("settings-gdrive/error-during-media-restore/account-manager user added ");
                sb.append(result.get("authAccount"));
                sb.append(" instead of ");
                sb.append(str);
                obj = sb.toString();
            } else {
                settingsGoogleDrive.A0e.A06(false);
                settingsGoogleDrive.A2m(authRequestDialogFragment, str);
                return;
            }
            Log.e(obj);
        } catch (AuthenticatorException | OperationCanceledException | IOException e) {
            Log.e("settings-gdrive/error-during-media-restore", e);
        }
    }

    public static /* synthetic */ void A0B(SettingsGoogleDrive settingsGoogleDrive) {
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_restore_media_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_restore_media;
        }
        RequestPermissionActivity.A0K(settingsGoogleDrive, i2, R.string.permission_storage_need_write_access_on_restore_media_request);
    }

    public static /* synthetic */ void A0D(SettingsGoogleDrive settingsGoogleDrive) {
        if (((ActivityC13810kN) settingsGoogleDrive).A09.A00.getBoolean("encrypted_backup_enabled", false)) {
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 18);
            bundle.putString("title", settingsGoogleDrive.getString(R.string.encrypted_backup_cancel_backup_dialog_title));
            bundle.putCharSequence("message", settingsGoogleDrive.getString(R.string.encrypted_backup_cancel_backup_dialog_subtitle));
            bundle.putBoolean("cancelable", false);
            bundle.putString("positive_button", settingsGoogleDrive.getString(R.string.encrypted_backup_cancel_backup_dialog_positive_button));
            bundle.putString("negative_button", settingsGoogleDrive.getString(R.string.encrypted_backup_cancel_backup_dialog_negative_button));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            C004902f r1 = new C004902f(settingsGoogleDrive.A0V());
            r1.A09(promptDialogFragment, null);
            r1.A02();
            return;
        }
        settingsGoogleDrive.A2e();
    }

    public static /* synthetic */ void A0K(SettingsGoogleDrive settingsGoogleDrive) {
        Bundle bundle = new Bundle();
        bundle.putInt("dialog_id", 12);
        bundle.putCharSequence("message", settingsGoogleDrive.getString(R.string.gdrive_cancel_media_restore_message));
        bundle.putString("positive_button", settingsGoogleDrive.getString(R.string.skip));
        bundle.putString("negative_button", settingsGoogleDrive.getString(R.string.cancel));
        PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
        promptDialogFragment.A0U(bundle);
        C004902f r1 = new C004902f(settingsGoogleDrive.A0V());
        r1.A09(promptDialogFragment, null);
        r1.A02();
    }

    public static /* synthetic */ void A0L(SettingsGoogleDrive settingsGoogleDrive, AbstractC87974Dt r14) {
        int i;
        if (r14 instanceof C84233yd) {
            int i2 = ((C84233yd) r14).A00;
            if (i2 == 0) {
                String string = settingsGoogleDrive.getString(R.string.export_backup_preparing_description);
                ProgressDialogC48342Fq r0 = SettingsChat.A0T;
                if (r0 != null) {
                    r0.setTitle(R.string.export_backup_preparing_title);
                    SettingsChat.A0T.setMessage(string);
                    return;
                }
                return;
            } else if (i2 == 1) {
                SettingsChat.A0T = null;
                C36021jC.A00(settingsGoogleDrive, 605);
                Bundle bundle = new Bundle();
                bundle.putLong("backup_size", settingsGoogleDrive.A0X.A00);
                bundle.putInt("backup_state", 1);
                NotEnoughStorageDialogFragment notEnoughStorageDialogFragment = new NotEnoughStorageDialogFragment();
                notEnoughStorageDialogFragment.A0U(bundle);
                notEnoughStorageDialogFragment.A1F(settingsGoogleDrive.A0V(), null);
                return;
            } else if (i2 == 2) {
                SettingsChat.A0T = null;
                C36021jC.A00(settingsGoogleDrive, 605);
                i = R.string.space_check_fail;
            } else if (i2 == 3) {
                SettingsChat.A0T = null;
                C36021jC.A00(settingsGoogleDrive, 605);
                AnonymousClass1Tv.A00(settingsGoogleDrive.A0i.A00, C14960mK.A0U(settingsGoogleDrive, "action_backup_export"));
                return;
            } else {
                return;
            }
        } else if (r14 instanceof C84223yc) {
            settingsGoogleDrive.showDialog(606);
            String string2 = settingsGoogleDrive.getString(R.string.export_backup_dialog_message, C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, 0), C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, settingsGoogleDrive.A0X.A00), ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(0.0d));
            DialogC72873fJ r1 = settingsGoogleDrive.A0c;
            if (r1 != null) {
                ((TextView) r1.findViewById(R.id.backup_export_info)).setText(string2);
                settingsGoogleDrive.setProgress(0);
                return;
            }
            return;
        } else if (r14 instanceof C84253yf) {
            C84253yf r142 = (C84253yf) r14;
            long j = r142.A00;
            long j2 = r142.A01;
            int i3 = (int) ((100 * j) / j2);
            String string3 = settingsGoogleDrive.getString(R.string.export_backup_dialog_message, C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, j), C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, j2), ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(((double) i3) / 100.0d));
            if (settingsGoogleDrive.A0c == null) {
                settingsGoogleDrive.showDialog(606);
            }
            DialogC72873fJ r12 = settingsGoogleDrive.A0c;
            if (r12 != null) {
                ((TextView) r12.findViewById(R.id.backup_export_info)).setText(string3);
                ((ProgressBar) settingsGoogleDrive.A0c.findViewById(R.id.backup_export_progress)).setProgress(i3);
                return;
            }
            return;
        } else if (r14 instanceof C84243ye) {
            settingsGoogleDrive.removeDialog(606);
            settingsGoogleDrive.A0c = null;
            boolean z = ((C84243ye) r14).A00;
            i = R.string.export_backup_fail;
            if (z) {
                i = R.string.export_backup_success;
            }
        } else if ((r14 instanceof C84203ya) || (r14 instanceof C84213yb)) {
            settingsGoogleDrive.removeDialog(606);
            settingsGoogleDrive.A0c = null;
            return;
        } else {
            return;
        }
        settingsGoogleDrive.Ado(i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static /* synthetic */ void A0M(SettingsGoogleDrive settingsGoogleDrive, C90054Mk r14) {
        String str;
        int i;
        StringBuilder sb;
        String str2;
        int i2;
        long j;
        StringBuilder sb2 = new StringBuilder("settings-gdrive/media-restore-error/");
        int i3 = r14.A00;
        sb2.append(C44771zW.A04(i3));
        Log.i(sb2.toString());
        AnonymousClass009.A01();
        String A09 = ((ActivityC13810kN) settingsGoogleDrive).A09.A09();
        View.OnClickListener onClickListener = null;
        switch (i3) {
            case 10:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
                str = null;
                break;
            case 11:
            case 21:
            case 28:
            case 29:
            case C25991Bp.A0S:
            case 31:
                str = settingsGoogleDrive.getString(R.string.gdrive_media_restore_error_auth_failed_summary);
                i = 34;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
            case 12:
                str = settingsGoogleDrive.getString(R.string.gdrive_media_restore_error_account_not_present_on_the_device_anymore_summary, A09);
                onClickListener = new ViewOnClickCListenerShape0S1100000_I0(2, A09, settingsGoogleDrive);
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                sb = new StringBuilder();
                str2 = "settings-gdrive/display-media-restore-error/unexpected/";
                sb.append(str2);
                sb.append(C44771zW.A04(i3));
                Log.e(sb.toString());
                str = null;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                i2 = R.string.gdrive_media_restore_error_not_reachable_summary;
                str = settingsGoogleDrive.getString(i2);
                i = 37;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
            case 15:
                Bundle bundle = r14.A01;
                long j2 = -1;
                if (bundle != null) {
                    j = bundle.getLong("total_bytes_to_be_downloaded", -1);
                    j2 = bundle.getLong("total_bytes_downloaded", -1);
                    long j3 = j - j2;
                    if (j >= 0 && j3 > 0) {
                        str = settingsGoogleDrive.getString(R.string.gdrive_media_restore_error_internal_storage_full_summary, C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, j3));
                        onClickListener = new ViewOnClickCListenerShape0S0200000_I0(settingsGoogleDrive, 4, r14);
                        break;
                    }
                } else {
                    j = -1;
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("settings-gdrive/display-media-restore-error/");
                sb3.append(i3);
                sb3.append(" total: ");
                sb3.append(j);
                sb3.append(" download: ");
                sb3.append(j2);
                sb3.append(" downloaded cannot be more than total.");
                Log.e(sb3.toString());
                str = settingsGoogleDrive.getString(R.string.gdrive_media_restore_error_internal_storage_full_summary_unknown_size);
                onClickListener = new ViewOnClickCListenerShape0S0200000_I0(settingsGoogleDrive, 4, r14);
                break;
            case 18:
            case 19:
                i2 = R.string.gdrive_media_restore_error_google_drive_servers_are_not_working_properly_summary;
                str = settingsGoogleDrive.getString(i2);
                i = 37;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
            case C43951xu.A01:
                Log.e("settings-gdrive/display-media-restore-error/unexpected-error/failed-to-authenticate-with-whatsapp-servers");
                i2 = R.string.gdrive_media_restore_error_google_drive_servers_are_not_working_properly_summary;
                str = settingsGoogleDrive.getString(i2);
                i = 37;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
            case 22:
                StringBuilder sb4 = new StringBuilder("Unexpected error: ");
                sb4.append(i3);
                throw new IllegalStateException(sb4.toString());
            case 23:
                str = settingsGoogleDrive.getString(R.string.gdrive_media_restore_error_storage_permission_withdrawn);
                i = 27;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
            case 24:
            case 26:
            case 27:
            default:
                sb = new StringBuilder();
                str2 = "settings-gdrive/display-media-restore-error/unhandled-error/";
                sb.append(str2);
                sb.append(C44771zW.A04(i3));
                Log.e(sb.toString());
                str = null;
                break;
            case 25:
                i2 = R.string.gdrive_media_restore_error_google_backup_service_disabled_summary;
                str = settingsGoogleDrive.getString(i2);
                i = 37;
                onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i);
                break;
        }
        if (!settingsGoogleDrive.A2p()) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("settings-gdrive/display-media-restore-error/");
            sb5.append(C44771zW.A04(i3));
            Log.i(sb5.toString());
            View A05 = AnonymousClass00T.A05(settingsGoogleDrive, R.id.google_drive_backup_error_text_views);
            A05.setOnClickListener(onClickListener);
            boolean z = false;
            if (onClickListener != null) {
                z = true;
            }
            A05.setClickable(z);
            SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = settingsGoogleDrive.A0e;
            if (str != null) {
                settingsGoogleDriveViewModel.A06(true);
                ((TextView) AnonymousClass00T.A05(settingsGoogleDrive, R.id.google_drive_backup_error_info)).setText(str);
                AnonymousClass00T.A05(settingsGoogleDrive, R.id.cancel_error_view).setVisibility(0);
                AnonymousClass00T.A05(settingsGoogleDrive, R.id.cancel_error_view).setOnClickListener(settingsGoogleDrive.A01);
                return;
            }
            settingsGoogleDriveViewModel.A06(false);
            return;
        }
        StringBuilder sb6 = new StringBuilder("settings-gdrive/display-media-restore-error failed to display error ");
        sb6.append(C44771zW.A04(i3));
        sb6.append(" since Activity is about to finish.");
        Log.i(sb6.toString());
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0p) {
            this.A0p = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0i = (C16590pI) r1.AMg.get();
            this.A0l = (C16120oU) r1.ANE.get();
            this.A0m = (AnonymousClass12U) r1.AJd.get();
            this.A0n = (C21710xr) r1.ANg.get();
            this.A0W = (C15820nx) r1.A6U.get();
            this.A0V = (C19500uD) r1.A1H.get();
            this.A0h = (C17050qB) r1.ABL.get();
            this.A0d = (AnonymousClass1D6) r1.A1G.get();
            this.A0k = (C18260sA) r1.AAZ.get();
            this.A0X = (C25721Am) r1.A1B.get();
            this.A0f = (AnonymousClass116) r1.A3z.get();
            this.A0Z = (C26551Dx) r1.A8Z.get();
            this.A0j = (C15890o4) r1.AN1.get();
            this.A0Y = (C22730zY) r1.A8Y.get();
            this.A0b = (AnonymousClass10K) r1.A8c.get();
            this.A0a = (AnonymousClass10J) r1.A8b.get();
        }
    }

    public final void A2e() {
        Log.i("settings-gdrive/cancel-backup");
        this.A0e.A09.A0A(false);
        this.A0Z.A03();
        if (C44771zW.A0M(((ActivityC13810kN) this).A0C)) {
            try {
                for (C05450Pp r0 : (List) ((AnonymousClass022) this.A0n.get()).A02().get()) {
                    if (!r0.A03.A00()) {
                        ((AnonymousClass022) this.A0n.get()).A08("com.whatsapp.backup.google.google-backup-worker");
                        return;
                    }
                }
            } catch (InterruptedException | ExecutionException unused) {
                Log.e("settings-gdrive/cancel-backup couldn't get work info for BackupWorker.");
            }
        }
    }

    public final void A2f() {
        if (this.A0h.A04(this.A0s)) {
            this.A0Z.A05(10);
            this.A0e.A05.A0A(false);
            this.A0e.A0B.A0A(false);
            C44981zs r2 = new C44981zs();
            r2.A04 = Long.valueOf(System.currentTimeMillis());
            r2.A03 = 0;
            C18260sA r10 = this.A0k;
            C14950mJ r9 = ((ActivityC13790kL) this).A06;
            r10.A01(new C68653Wf(this, this, this.A0V, this.A0j, ((ActivityC13830kP) this).A01, r9, r10, new AbstractC116175Uk(r2) { // from class: X.3Zv
                public final /* synthetic */ C44981zs A01;

                {
                    this.A01 = r2;
                }

                /* JADX WARNING: Code restructure failed: missing block: B:47:0x0120, code lost:
                    if (r1 == 2) goto L_0x0122;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:48:0x0122, code lost:
                    if (r2 == 1) goto L_0x0124;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:53:0x0148, code lost:
                    com.whatsapp.util.Log.e(X.C12960it.A0W(r1, "settings-gdrive/perform-backup/unknown-network-type/"));
                 */
                @Override // X.AbstractC116175Uk
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void ASB(int r8) {
                    /*
                    // Method dump skipped, instructions count: 340
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C69593Zv.ASB(int):void");
                }
            }), 3000);
        }
    }

    public final void A2g() {
        String A09 = ((ActivityC13810kN) this).A09.A09();
        if (A09 == null) {
            A2i();
            return;
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, new AuthRequestDialogFragment(), A09, 5));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a8, code lost:
        if (r0 == false) goto L_0x00aa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2h() {
        /*
            r7 = this;
            X.1D6 r0 = r7.A0d
            X.0m9 r1 = r0.A08
            r0 = 932(0x3a4, float:1.306E-42)
            boolean r0 = r1.A07(r0)
            r5 = 1
            r3 = 0
            com.whatsapp.TextEmojiLabel r1 = r7.A0Q
            if (r0 == 0) goto L_0x006b
            r1.setVisibility(r3)
            r0 = 2131889088(0x7f120bc0, float:1.941283E38)
            java.lang.String r6 = r7.getString(r0)
            r2 = 2131891679(0x7f1215df, float:1.9418085E38)
            r0 = 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            X.1D6 r0 = r7.A0d
            java.lang.String r0 = r0.A03()
            r1[r3] = r0
            r1[r5] = r6
            java.lang.String r0 = r7.getString(r2, r1)
            android.text.SpannableStringBuilder r4 = new android.text.SpannableStringBuilder
            r4.<init>(r0)
            X.2aQ r3 = new X.2aQ
            r3.<init>(r7)
            int r2 = r0.length()
            int r0 = r6.length()
            int r1 = r2 - r0
            r0 = 33
            r4.setSpan(r3, r1, r2, r0)
            com.whatsapp.TextEmojiLabel r0 = r7.A0Q
            r0.setText(r4)
            com.whatsapp.TextEmojiLabel r0 = r7.A0Q
            r0.setLinksClickable(r5)
            com.whatsapp.TextEmojiLabel r1 = r7.A0Q
            android.text.method.MovementMethod r0 = android.text.method.LinkMovementMethod.getInstance()
            r1.setMovementMethod(r0)
            X.0nx r0 = r7.A0W
            boolean r0 = r0.A04()
            if (r0 == 0) goto L_0x009d
            com.whatsapp.WaTextView r3 = r7.A0U
            r2 = 2131891668(0x7f1215d4, float:1.9418063E38)
        L_0x0067:
            r3.setText(r2)
            return
        L_0x006b:
            r0 = 8
            r1.setVisibility(r0)
            X.0nx r0 = r7.A0W
            boolean r0 = r0.A05()
            if (r0 == 0) goto L_0x009a
            X.0nx r0 = r7.A0W
            boolean r1 = r0.A04()
            X.0m6 r0 = r7.A09
            int r0 = r0.A01()
            com.whatsapp.WaTextView r3 = r7.A0U
            if (r0 == 0) goto L_0x0091
            r2 = 2131891666(0x7f1215d2, float:1.9418058E38)
            if (r1 == 0) goto L_0x0067
            r2 = 2131891667(0x7f1215d3, float:1.941806E38)
            goto L_0x0067
        L_0x0091:
            r2 = 2131891664(0x7f1215d0, float:1.9418054E38)
            if (r1 == 0) goto L_0x0067
            r2 = 2131891665(0x7f1215d1, float:1.9418056E38)
            goto L_0x0067
        L_0x009a:
            com.whatsapp.WaTextView r3 = r7.A0U
            goto L_0x00aa
        L_0x009d:
            X.0nx r0 = r7.A0W
            boolean r0 = r0.A05()
            com.whatsapp.WaTextView r3 = r7.A0U
            r2 = 2131891660(0x7f1215cc, float:1.9418046E38)
            if (r0 != 0) goto L_0x0067
        L_0x00aa:
            r2 = 2131891663(0x7f1215cf, float:1.9418052E38)
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.SettingsGoogleDrive.A2h():void");
    }

    public final void A2i() {
        int i;
        AnonymousClass009.A01();
        if (!A2p()) {
            if (C44771zW.A0G(((ActivityC13810kN) this).A09)) {
                Log.i("settings-gdrive/account-selector/backup/running");
                i = R.string.settings_gdrive_please_wait_for_backup_to_finish_before_account_change;
            } else if (C44771zW.A0H(((ActivityC13810kN) this).A09)) {
                Log.i("settings-gdrive/account-selector/restore/running");
                i = R.string.settings_gdrive_please_wait_for_restore_to_finish_before_account_change;
            } else {
                int i2 = 0;
                if (this.A0j.A02("android.permission.GET_ACCOUNTS") != 0 || !this.A0f.A00()) {
                    C35751ig r3 = new C35751ig(this);
                    r3.A01 = R.drawable.permission_contacts_small;
                    r3.A0C = new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS"};
                    r3.A02 = R.string.permission_contacts_access_for_gdrive_backup_request;
                    r3.A03 = R.string.permission_contacts_access_for_gdrive_backup;
                    A2E(r3.A00(), 150);
                    return;
                }
                String A09 = ((ActivityC13810kN) this).A09.A09();
                Account[] accountsByType = AccountManager.get(this).getAccountsByType("com.google");
                int length = accountsByType.length;
                if (length > 0) {
                    StringBuilder sb = new StringBuilder("settings-gdrive/account-selector/starting-account-picker/num-accounts/");
                    sb.append(length);
                    Log.i(sb.toString());
                    int i3 = -1;
                    int i4 = length + 1;
                    String[] strArr = new String[i4];
                    do {
                        strArr[i2] = accountsByType[i2].name;
                        if (A09 != null && A09.equals(strArr[i2])) {
                            i3 = i2;
                        }
                        i2++;
                    } while (i2 < length);
                    strArr[i4 - 1] = getString(R.string.google_account_picker_add_account);
                    SingleChoiceListDialogFragment singleChoiceListDialogFragment = new SingleChoiceListDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("dialog_id", 17);
                    bundle.putString("title", getString(R.string.google_account_picker_title));
                    bundle.putInt("selected_item_index", i3);
                    bundle.putStringArray("multi_line_list_items_key", strArr);
                    singleChoiceListDialogFragment.A0U(bundle);
                    if (A0V().A0A("account-picker") == null) {
                        C004902f r0 = new C004902f(A0V());
                        r0.A09(singleChoiceListDialogFragment, "account-picker");
                        r0.A02();
                        return;
                    }
                    return;
                }
                Log.i("settings-gdrive/account-selector/no-account-found/start-add-account-activity");
                A2j();
                return;
            }
            Ado(i);
        }
    }

    public final void A2j() {
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0300000_I0(this, AccountManager.get(this).addAccount("com.google", null, null, null, this, null, null), new AuthRequestDialogFragment(), 14));
    }

    public final void A2k() {
        this.A0Z.A05(10);
        Intent A0U = C14960mK.A0U(this, "action_backup");
        A0U.putExtra("backup_mode", "user_initiated");
        AnonymousClass1Tv.A00(this, A0U);
    }

    public final void A2l() {
        A2h();
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A0e;
        AnonymousClass016 r1 = settingsGoogleDriveViewModel.A0N;
        C15820nx r2 = settingsGoogleDriveViewModel.A0Q;
        r1.A0B(Boolean.valueOf(r2.A05()));
        if (r2.A05()) {
            settingsGoogleDriveViewModel.A0A.A0B(Boolean.valueOf(r2.A04()));
        }
    }

    public final void A2m(AuthRequestDialogFragment authRequestDialogFragment, String str) {
        AnonymousClass009.A00();
        StringBuilder sb = new StringBuilder("settings-gdrive/auth-request account being used is ");
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        this.A0t = false;
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 0, authRequestDialogFragment));
        ConditionVariable conditionVariable = this.A0r;
        conditionVariable.close();
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, authRequestDialogFragment, str, 7));
        Log.i("settings-gdrive/auth-request blocking on tokenReceived");
        C28181Ma r3 = new C28181Ma("settings-gdrive/fetch-auth-token");
        conditionVariable.block(C26061Bw.A0L);
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 1, r3));
    }

    public final void A2n(String str) {
        StringBuilder sb = new StringBuilder("setting-gdrive/activity-result/account-picker accountName is ");
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        if (str != null) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, new AuthRequestDialogFragment(), str, 4));
        } else if (((ActivityC13810kN) this).A09.A09() == null) {
            Log.i("setting-gdrive/activity-result/account-picker accountName is null");
            this.A0e.A07(0);
        }
    }

    public final void A2o(String str, String str2) {
        this.A0r.open();
        DialogFragment dialogFragment = (DialogFragment) A0V().A0A("auth_request_dialog");
        if (dialogFragment != null) {
            dialogFragment.A1C();
        }
        if (str != null) {
            SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A0e;
            C14820m6 r1 = settingsGoogleDriveViewModel.A0a;
            if (!TextUtils.equals(r1.A09(), str2)) {
                r1.A0f(str2);
                C26551Dx r2 = settingsGoogleDriveViewModel.A0U;
                synchronized (r2.A0N) {
                    r2.A00 = null;
                }
                StringBuilder sb = new StringBuilder("gdrive-setting-view-model/update-account-name new accountName is ");
                sb.append(C44771zW.A0B(str2));
                Log.i(sb.toString());
                settingsGoogleDriveViewModel.A02.A0B(str2);
                settingsGoogleDriveViewModel.A05();
                Intent A0U = C14960mK.A0U(this, "action_fetch_backup_info");
                A0U.putExtra("account_name", str2);
                AnonymousClass1Tv.A00(this, A0U);
            } else {
                StringBuilder sb2 = new StringBuilder("gdrive-setting-view-model/update-account-name account unchanged, token received for ");
                sb2.append(C44771zW.A0B(str2));
                Log.i(sb2.toString());
            }
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 46));
    }

    public final boolean A2p() {
        return C36021jC.A03(this) || this.A0o;
    }

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
        String str;
        switch (i) {
            case 12:
                str = "settings-gdrive/cancel-media-restore-dialog/user-decided-not-to-cancel";
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                str = "settings-gdrive/perform-backup user declined to perform Google Drive backup over cellular (when the settings say Wi-Fi only)";
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 17:
            default:
                StringBuilder sb = new StringBuilder("unexpected dialog box: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            case 15:
                str = "settings-gdrive/user-declined-to-restore-media-over-cellular";
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                str = "settings-gdrive/user-declined-to-backup-over-cellular";
                break;
            case 18:
                str = "settings-gdrive/user-declined-to-cancel-encrypted-backup";
                break;
        }
        Log.i(str);
    }

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
        StringBuilder sb = new StringBuilder("unexpected dialog box: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        switch (i) {
            case 12:
                this.A0Z.A03();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                Log.i("settings-gdrive/perform-backup user decided to perform Google Drive backup over cellular (when the settings say Wi-Fi only)");
                this.A0Y.A01();
                A2k();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                Log.i("settings-gdrive/google-play-services-is-broken");
                this.A0e.A07(0);
                return;
            case 15:
                Log.i("settings-gdrive/user-confirmed-media-restore-over-cellular");
                this.A0Y.A02();
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                Log.i("settings-gdrive/user-confirmed-backup-over-cellular");
                this.A0Y.A01();
                return;
            case 17:
            default:
                StringBuilder sb = new StringBuilder("unexpected dialog box: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            case 18:
                Log.i("settings-gdrive/user-confirmed-cancel-encrypted-backup");
                A2e();
                return;
        }
    }

    @Override // X.AnonymousClass29I
    public void APF(int i) {
        StringBuilder sb = new StringBuilder("settings-gdrive/dialogId-");
        sb.append(i);
        sb.append("-dismissed");
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass29I
    public void AW6(String[] strArr, int i, int i2) {
        String str;
        if (i == 10) {
            int[] iArr = SettingsGoogleDriveViewModel.A0i;
            if (i2 > iArr.length) {
                StringBuilder sb = new StringBuilder("settings-gdrive/change-freq/unexpected-choice/");
                sb.append(i2);
                str = sb.toString();
            } else {
                StringBuilder sb2 = new StringBuilder("settings-gdrive/change-freq/index:");
                sb2.append(i2);
                sb2.append("/value:");
                sb2.append(iArr[i2]);
                Log.i(sb2.toString());
                int A01 = ((ActivityC13810kN) this).A09.A01();
                int i3 = iArr[i2];
                if (!this.A0e.A07(i3)) {
                    str = "settings-gdrive/change-freq failed to set the new frequency.";
                } else {
                    if (i3 != 0) {
                        if (A01 == 0 && !C44771zW.A0G(((ActivityC13810kN) this).A09) && !C44771zW.A0H(((ActivityC13810kN) this).A09)) {
                            this.A04.performClick();
                        }
                    } else if (((ActivityC13810kN) this).A09.A00.getLong("gdrive_next_prompt_for_setup_timestamp", -1) < System.currentTimeMillis() + 2592000000L) {
                        ((ActivityC13810kN) this).A09.A0Y(System.currentTimeMillis() + 2592000000L);
                    }
                    A2h();
                    return;
                }
            }
            Log.e(str);
        } else if (i != 17) {
            StringBuilder sb3 = new StringBuilder("unexpected dialog box: ");
            sb3.append(i);
            throw new IllegalStateException(sb3.toString());
        } else if (strArr[i2].equals(getString(R.string.google_account_picker_add_account))) {
            A2j();
        } else {
            A2n(strArr[i2]);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        StringBuilder sb = new StringBuilder("settings-gdrive/activity-result request: ");
        sb.append(i);
        sb.append(" result: ");
        sb.append(i2);
        Log.i(sb.toString());
        super.onActivityResult(i, i2, intent);
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    String str2 = null;
                    if (intent != null) {
                        str = intent.getStringExtra("authAccount");
                    } else {
                        str = null;
                    }
                    if (i2 == -1) {
                        str2 = str;
                    }
                    A2n(str2);
                    return;
                } else if (i != 150) {
                    if (i == 151 && i2 == -1) {
                        if (((ActivityC13810kN) this).A09.A03() == 23) {
                            this.A0Z.A05(10);
                        }
                        if (C44771zW.A0H(((ActivityC13810kN) this).A09) || C44771zW.A0G(((ActivityC13810kN) this).A09)) {
                            C22730zY r3 = this.A0Y;
                            r3.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r3, 21));
                            return;
                        }
                    } else {
                        return;
                    }
                } else if (i2 == -1) {
                    A2i();
                    return;
                } else {
                    return;
                }
            } else if (i2 == -1) {
                AnonymousClass009.A05(intent);
                A2o(intent.getStringExtra("authtoken"), intent.getStringExtra("authAccount"));
                return;
            } else {
                DialogFragment dialogFragment = (DialogFragment) A0V().A0A("auth_request_dialog");
                if (dialogFragment != null) {
                    dialogFragment.A1C();
                    return;
                }
                return;
            }
        } else if (i2 == -1) {
            A2l();
            String A09 = ((ActivityC13810kN) this).A09.A09();
            if (A09 == null || ((ActivityC13810kN) this).A09.A08(A09) == -1) {
                ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 41));
                return;
            } else if (((ActivityC13810kN) this).A09.A1I(A09) && !((ActivityC13810kN) this).A09.A00.getBoolean("encrypted_backup_enabled", false)) {
                C15570nT r0 = ((ActivityC13790kL) this).A01;
                r0.A08();
                C27631Ih r2 = r0.A05;
                if (r2 != null) {
                    this.A0a.A02(new C58892rw(this));
                    Intent A0U = C14960mK.A0U(this, "action_delete");
                    A0U.putExtra("account_name", ((ActivityC13810kN) this).A09.A09());
                    A0U.putExtra("jid_user", r2.user);
                    ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 46, A0U));
                    return;
                }
                return;
            } else if (((ActivityC13810kN) this).A09.A1I(A09) || !((ActivityC13810kN) this).A09.A00.getBoolean("encrypted_backup_enabled", false)) {
                return;
            }
        } else {
            return;
        }
        A2f();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (isTaskRoot()) {
            startActivity(C14960mK.A04(this));
        }
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        this.A0e = (SettingsGoogleDriveViewModel) new AnonymousClass02A(this).A00(SettingsGoogleDriveViewModel.class);
        this.A0g = new AbstractC453621g() { // from class: X.55w
            @Override // X.AbstractC453621g
            public final void AWT() {
                SettingsGoogleDrive.this.A0e.A05();
            }
        };
        setTitle(R.string.settings_backup);
        setContentView(R.layout.activity_settings_google_drive);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        this.A06 = AnonymousClass00T.A05(this, R.id.google_drive_backup_error_info_view);
        this.A04 = findViewById(R.id.settings_gdrive_change_account_view);
        this.A0E = (TextView) AnonymousClass00T.A05(this, R.id.settings_gdrive_account_name_summary);
        this.A0A = (Button) findViewById(R.id.google_drive_backup_now_btn);
        this.A0G = (TextView) findViewById(R.id.google_drive_backup_now_btn_info);
        this.A0H = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_general_info);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.google_drive_progress);
        this.A0D = progressBar;
        C88134Ek.A00(progressBar, AnonymousClass00T.A00(this, R.color.media_message_progress_determinate));
        this.A0B = (ImageView) findViewById(R.id.cancel_download);
        this.A0C = (ImageView) findViewById(R.id.resume_download);
        this.A07 = findViewById(R.id.settings_gdrive_change_frequency_view);
        this.A0F = (TextView) findViewById(R.id.settings_gdrive_backup_options_summary);
        this.A09 = findViewById(R.id.settings_gdrive_network_settings_view);
        this.A0P = (SwitchCompat) findViewById(R.id.gdrive_network_setting);
        this.A08 = findViewById(R.id.settings_gdrive_backup_include_video);
        this.A05 = findViewById(R.id.settings_gdrive_password_protect_backups);
        this.A0S = (WaTextView) findViewById(R.id.settings_gdrive_password_protect_backups_value);
        this.A0U = (WaTextView) findViewById(R.id.settings_gdrive_backup_encryption_info);
        this.A0Q = (TextEmojiLabel) findViewById(R.id.settings_gdrive_backup_quota_info);
        this.A0T = (WaTextView) findViewById(R.id.settings_encrypted_backup_info);
        this.A0R = (WaLinearLayout) findViewById(R.id.gdrive_backup_e2e_encrypted);
        this.A0O = (SwitchCompat) findViewById(R.id.include_video_setting);
        this.A0N = (TextView) AnonymousClass00T.A05(this, R.id.include_video_settings_summary);
        this.A0M = (TextView) AnonymousClass00T.A05(this, R.id.local_backup_time);
        this.A0L = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_time);
        this.A0J = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_quota_reached);
        this.A0I = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_media_cutoff_date);
        this.A0K = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_size);
        int A00 = C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.last_backup_icon), A00);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.gdrive_icon), A00);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.backup_settings_icon), A00);
        int[] iArr = SettingsGoogleDriveViewModel.A0h;
        int length = iArr.length;
        this.A0q = new String[length];
        for (int i = 0; i < length; i++) {
            int i2 = iArr[i];
            if (i2 == R.string.settings_gdrive_backup_frequency_option_manual) {
                this.A0q[i] = getString(R.string.settings_gdrive_backup_frequency_option_manual, getString(R.string.backup));
            } else {
                this.A0q[i] = getString(i2);
            }
        }
        this.A05.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 7));
        this.A0X.A0F.A07(1729);
        this.A0e.A0H.A05(this, new AnonymousClass02B() { // from class: X.3Pm
            /* JADX WARNING: Code restructure failed: missing block: B:31:0x00eb, code lost:
                if (r0 != 0) goto L_0x00aa;
             */
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00d1  */
            /* JADX WARNING: Removed duplicated region for block: B:47:0x0148  */
            /* JADX WARNING: Removed duplicated region for block: B:49:0x0159  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r18) {
                /*
                // Method dump skipped, instructions count: 394
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C66933Pm.ANq(java.lang.Object):void");
            }
        });
        this.A0e.A0d.A05(this, new AnonymousClass02B() { // from class: X.4sJ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive.A0L(SettingsGoogleDrive.this, (AbstractC87974Dt) obj);
            }
        });
        this.A0e.A0O.A05(this, new AnonymousClass02B() { // from class: X.3Pi
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass018 r4;
                int i3;
                long j;
                String A02;
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                AbstractC471529e r7 = (AbstractC471529e) obj;
                TextView textView = settingsGoogleDrive.A0N;
                if (r7 == null) {
                    textView.setVisibility(8);
                    return;
                }
                SettingsGoogleDrive.A03(textView, 0);
                if (r7 instanceof C84313yl) {
                    A02 = settingsGoogleDrive.getString(R.string.calculating);
                } else {
                    if (r7 instanceof C471429d) {
                        r4 = ((ActivityC13830kP) settingsGoogleDrive).A01;
                        i3 = R.plurals.settings_gdrive_video_size_to_be_uploaded_plural;
                        j = ((C471429d) r7).A00;
                    } else if (r7 instanceof C84323ym) {
                        r4 = ((ActivityC13830kP) settingsGoogleDrive).A01;
                        i3 = R.plurals.settings_gdrive_video_size_already_uploaded_plural;
                        j = ((C84323ym) r7).A00;
                    } else {
                        throw C12970iu.A0f(C12960it.A0b("Unexpected state ", r7));
                    }
                    A02 = C44891zj.A02(r4, i3, j, false);
                }
                settingsGoogleDrive.A0N.setText(A02);
            }
        });
        this.A0e.A0I.A05(this, new AnonymousClass02B() { // from class: X.4sY
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Number number = (Number) obj;
                int i3 = 0;
                if (number == null) {
                    number = 0;
                }
                int intValue = number.intValue();
                WaTextView waTextView = settingsGoogleDrive.A0T;
                if (intValue != 1) {
                    i3 = 8;
                } else {
                    waTextView.setText(R.string.settings_gdrive_e2e_encryption_in_progress);
                    waTextView = settingsGoogleDrive.A0T;
                }
                waTextView.setVisibility(i3);
            }
        });
        this.A0e.A0F.A05(this, new AnonymousClass02B() { // from class: X.4sW
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive.this.A0O.setChecked(C12970iu.A1Z(obj, Boolean.TRUE));
            }
        });
        this.A0e.A02.A05(this, new AnonymousClass02B() { // from class: X.4sZ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                String str = (String) obj;
                if (str == null) {
                    str = settingsGoogleDrive.getString(R.string.settings_gdrive_account_name_missing_value);
                }
                settingsGoogleDrive.A0E.setText(str);
            }
        });
        this.A0e.A04.A05(this, new AnonymousClass02B() { // from class: X.3Pl
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                TextView textView = settingsGoogleDrive.A0F;
                String[] strArr = settingsGoogleDrive.A0q;
                int intValue = ((Number) obj).intValue();
                int i3 = 0;
                while (true) {
                    int[] iArr2 = SettingsGoogleDriveViewModel.A0i;
                    if (i3 < iArr2.length) {
                        if (iArr2[i3] == intValue) {
                            break;
                        }
                        i3++;
                    } else {
                        Log.e(C12960it.A0W(intValue, "settings-gdrive/get-backup-freq-index/"));
                        i3 = 0;
                        break;
                    }
                }
                textView.setText(strArr[i3]);
            }
        });
        this.A0e.A0L.A05(this, new AnonymousClass02B() { // from class: X.4sT
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                ProgressBar progressBar2 = settingsGoogleDrive.A0D;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(progressBar2, i3);
            }
        });
        this.A0e.A0J.A05(this, new AnonymousClass02B() { // from class: X.4sS
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                boolean A1Z = C12970iu.A1Z(obj, Boolean.TRUE);
                if (A1Z != settingsGoogleDrive.A0D.isIndeterminate()) {
                    settingsGoogleDrive.A0D.setIndeterminate(A1Z);
                }
            }
        });
        this.A0e.A0K.A05(this, new AnonymousClass02B() { // from class: X.4sX
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                int intValue;
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Number number = (Number) obj;
                if (number != null && (intValue = number.intValue()) >= 0) {
                    settingsGoogleDrive.A0D.setProgress(intValue);
                }
            }
        });
        this.A0e.A09.A05(this, new AnonymousClass02B() { // from class: X.4sO
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                ImageView imageView = settingsGoogleDrive.A0B;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(imageView, i3);
            }
        });
        this.A0e.A0M.A05(this, new AnonymousClass02B() { // from class: X.4sU
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                ImageView imageView = settingsGoogleDrive.A0C;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(imageView, i3);
            }
        });
        this.A0e.A0B.A05(this, new AnonymousClass02B() { // from class: X.4sQ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                boolean A1Z = C12970iu.A1Z(obj, Boolean.TRUE);
                if (A1Z != settingsGoogleDrive.A05.isClickable()) {
                    settingsGoogleDrive.A05.setClickable(A1Z);
                }
            }
        });
        this.A0e.A06.A05(this, new AnonymousClass02B() { // from class: X.4sM
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                TextView textView = settingsGoogleDrive.A0G;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(textView, i3);
            }
        });
        this.A0e.A07.A05(this, new AnonymousClass02B() { // from class: X.4sN
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                Button button = settingsGoogleDrive.A0A;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(button, i3);
            }
        });
        this.A0e.A05.A05(this, new AnonymousClass02B() { // from class: X.4sL
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                boolean A1Z = C12970iu.A1Z(obj, Boolean.TRUE);
                if (A1Z != settingsGoogleDrive.A0A.isClickable()) {
                    settingsGoogleDrive.A0A.setClickable(A1Z);
                }
            }
        });
        this.A0e.A08.A05(this, new AnonymousClass02B() { // from class: X.3Ph
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                String str;
                TextView textView;
                TextView textView2;
                String A0X;
                int i3;
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                AbstractC87994Dv r15 = (AbstractC87994Dv) obj;
                if (r15 == null) {
                    textView2 = settingsGoogleDrive.A0G;
                    A0X = "";
                } else {
                    if (r15 instanceof C58922rz) {
                        switch (((C58922rz) r15).A00) {
                            case 0:
                                i3 = R.string.settings_gdrive_restore_pending_on_wifi_not_available_message;
                                break;
                            case 1:
                                i3 = R.string.settings_gdrive_restore_media_pending_on_data_network_not_available_message;
                                break;
                            case 2:
                                i3 = R.string.settings_gdrive_restore_media_pending_on_low_battery;
                                break;
                            case 3:
                                i3 = R.string.gdrive_restore_error_sdcard_unmounted_summary;
                                break;
                            case 4:
                                i3 = R.string.gdrive_restore_error_sdcard_missing_summary;
                                break;
                            case 5:
                                i3 = R.string.settings_gdrive_backup_preparation_message;
                                break;
                            case 6:
                                i3 = R.string.settings_gdrive_backup_pending_on_wifi_message_cellular_connection_available;
                                break;
                            case 7:
                                i3 = R.string.settings_gdrive_backup_pending_on_wifi_message_no_data_connection_available;
                                break;
                            case 8:
                                i3 = R.string.settings_gdrive_error_data_network_not_available_message;
                                break;
                            case 9:
                                i3 = R.string.settings_gdrive_backup_pending_on_low_battery;
                                break;
                            case 10:
                                i3 = R.string.msg_store_backup_skipped_due_to_unmounted_sdcard_summary;
                                break;
                            case 11:
                                i3 = R.string.gdrive_backup_error_sdcard_missing_summary;
                                break;
                            case 12:
                                i3 = R.string.settings_gdrive_backup_finishing_message;
                                break;
                            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                i3 = R.string.settings_gdrive_restore_media_preparation_message;
                                break;
                            default:
                                throw C12970iu.A0f(C12970iu.A0s(r15, C12960it.A0j("Unexpected message ")));
                        }
                        textView = settingsGoogleDrive.A0G;
                        str = settingsGoogleDrive.getString(i3);
                    } else if (r15 instanceof C84273yh) {
                        textView2 = settingsGoogleDrive.A0G;
                        A0X = C12960it.A0X(settingsGoogleDrive, ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(((double) ((C84273yh) r15).A00) / 100.0d), new Object[1], 0, R.string.settings_gdrive_backup_preparation_message_with_percentage_placeholder);
                    } else {
                        if (r15 instanceof C84293yj) {
                            C84293yj r152 = (C84293yj) r15;
                            AnonymousClass018 r2 = ((ActivityC13830kP) settingsGoogleDrive).A01;
                            long j = r152.A01;
                            String A03 = C44891zj.A03(r2, j);
                            long j2 = r152.A00;
                            Object[] objArr = new Object[3];
                            objArr[0] = A03;
                            objArr[1] = C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, j2);
                            str = C12960it.A0X(settingsGoogleDrive, ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(((double) ((int) ((j * 100) / j2))) / 100.0d), objArr, 2, R.string.settings_gdrive_backup_progress_message_with_percentage);
                        } else if (r15 instanceof C84283yi) {
                            str = C12960it.A0X(settingsGoogleDrive, ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(((double) ((C84283yi) r15).A00) / 100.0d), new Object[1], 0, R.string.settings_gdrive_restore_media_preparation_message_with_percentage_placeholder);
                        } else if (r15 instanceof C84303yk) {
                            C84303yk r153 = (C84303yk) r15;
                            AnonymousClass018 r22 = ((ActivityC13830kP) settingsGoogleDrive).A01;
                            long j3 = r153.A00;
                            Object[] objArr2 = new Object[3];
                            objArr2[0] = C44891zj.A03(r22, j3);
                            AnonymousClass018 r23 = ((ActivityC13830kP) settingsGoogleDrive).A01;
                            long j4 = r153.A01;
                            objArr2[1] = C44891zj.A03(r23, j4);
                            str = C12960it.A0X(settingsGoogleDrive, ((ActivityC13830kP) settingsGoogleDrive).A01.A0K().format(((double) j3) / ((double) j4)), objArr2, 2, R.string.settings_gdrive_restore_progress_message_with_percentage);
                        } else {
                            throw C12970iu.A0f(C12970iu.A0s(r15, C12960it.A0j("Unexpected message ")));
                        }
                        textView = settingsGoogleDrive.A0G;
                    }
                    textView.setText(str);
                    return;
                }
                textView2.setText(A0X);
            }
        });
        this.A0e.A0D.A05(this, new AnonymousClass02B() { // from class: X.3Pj
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                String str;
                View.OnClickListener onClickListener;
                int i3;
                int i4;
                int i5;
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                int A05 = C12960it.A05(obj);
                Log.i(C12960it.A0d(C44771zW.A04(A05), C12960it.A0k("settings-gdrive/backup-error/")));
                AnonymousClass009.A01();
                switch (A05) {
                    case 10:
                        str = null;
                        break;
                    case 11:
                    case 21:
                    case 28:
                    case 29:
                    case C25991Bp.A0S:
                    case 31:
                        str = settingsGoogleDrive.getString(R.string.gdrive_backup_error_auth_failed_summary);
                        i5 = 35;
                        onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i5);
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 12:
                        str = settingsGoogleDrive.getString(R.string.gdrive_backup_error_account_not_present_on_the_device_anymore_summary);
                        i5 = 31;
                        onClickListener = new ViewOnClickCListenerShape0S0100000_I0(settingsGoogleDrive, i5);
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        if (settingsGoogleDrive.A0d.A08.A07(932)) {
                            i3 = R.string.gdrive_error_full_summary_total_upload_unknown_size;
                            str = settingsGoogleDrive.getString(i3);
                            onClickListener = settingsGoogleDrive.A03;
                            settingsGoogleDrive.A02 = onClickListener;
                            break;
                        }
                        str = null;
                        break;
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    case 18:
                    case C43951xu.A01:
                        i4 = R.string.gdrive_backup_error_not_reachable_summary;
                        str = C12960it.A0X(settingsGoogleDrive, settingsGoogleDrive.getString(R.string.backup), new Object[1], 0, i4);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 15:
                        str = C12960it.A0X(settingsGoogleDrive, C44891zj.A03(((ActivityC13830kP) settingsGoogleDrive).A01, SearchActionVerificationClientService.MS_TO_NS), new Object[1], 0, R.string.gdrive_backup_error_internal_storage_full_summary);
                        break;
                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    case 27:
                        i4 = R.string.gdrive_error_local_backup_missing_summary;
                        str = C12960it.A0X(settingsGoogleDrive, settingsGoogleDrive.getString(R.string.backup), new Object[1], 0, i4);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 17:
                    case 22:
                        Log.i("settings-gdrive/display-backup-error/unexpected-error/file-not-found");
                        i4 = R.string.gdrive_backup_error_not_reachable_summary;
                        str = C12960it.A0X(settingsGoogleDrive, settingsGoogleDrive.getString(R.string.backup), new Object[1], 0, i4);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 19:
                        i3 = R.string.gdrive_backup_error_google_drive_servers_are_not_working_properly_summary;
                        str = settingsGoogleDrive.getString(i3);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 23:
                        i3 = R.string.gdrive_backup_error_storage_permission_denied_summary;
                        str = settingsGoogleDrive.getString(i3);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 24:
                        throw C12960it.A0U(C12960it.A0W(A05, "Unexpected error during Google Drive backup: "));
                    case 25:
                        i3 = R.string.gdrive_backup_error_google_backup_service_disabled_summary;
                        str = settingsGoogleDrive.getString(i3);
                        onClickListener = settingsGoogleDrive.A03;
                        settingsGoogleDrive.A02 = onClickListener;
                        break;
                    case 26:
                    default:
                        Log.e(C12960it.A0d(C44771zW.A04(A05), C12960it.A0k("settings-gdrive/display-backup-error/unhandled-error/")));
                        str = null;
                        break;
                }
                if (!settingsGoogleDrive.A2p()) {
                    Log.i(C12960it.A0d(C44771zW.A04(A05), C12960it.A0k("settings-gdrive/display-backup-error/")));
                    View A052 = AnonymousClass00T.A05(settingsGoogleDrive, R.id.google_drive_backup_error_text_views);
                    A052.setOnClickListener(settingsGoogleDrive.A02);
                    View A053 = AnonymousClass00T.A05(settingsGoogleDrive, R.id.cancel_error_view);
                    A053.setVisibility(0);
                    A053.setOnClickListener(settingsGoogleDrive.A00);
                    A052.setClickable(C12960it.A1W(settingsGoogleDrive.A02));
                    SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = settingsGoogleDrive.A0e;
                    if (str != null) {
                        settingsGoogleDriveViewModel.A06(true);
                        C12990iw.A0N(settingsGoogleDrive, R.id.google_drive_backup_error_info).setText(str);
                        return;
                    }
                    settingsGoogleDriveViewModel.A06(false);
                } else if (A05 != 10) {
                    StringBuilder A0k = C12960it.A0k("settings-gdrive/display-backup-error failed to display error ");
                    A0k.append(C44771zW.A04(A05));
                    Log.e(C12960it.A0d(" since Activity is about to finish.", A0k));
                }
            }
        });
        this.A0e.A0E.A05(this, new AnonymousClass02B() { // from class: X.4sK
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive.A0M(SettingsGoogleDrive.this, (C90054Mk) obj);
            }
        });
        this.A0e.A0C.A05(this, new AnonymousClass02B() { // from class: X.4sR
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                View view = settingsGoogleDrive.A06;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(view, i3);
            }
        });
        this.A0e.A0N.A05(this, new AnonymousClass02B() { // from class: X.4sV
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                View view = settingsGoogleDrive.A05;
                int i3 = 0;
                if (obj != bool) {
                    i3 = 8;
                }
                SettingsGoogleDrive.A03(view, i3);
            }
        });
        this.A0e.A0A.A05(this, new AnonymousClass02B() { // from class: X.4sP
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Boolean bool = Boolean.TRUE;
                WaTextView waTextView = settingsGoogleDrive.A0S;
                int i3 = R.string.settings_gdrive_backup_e2e_encrypted_value_disabled;
                if (obj == bool) {
                    i3 = R.string.settings_gdrive_backup_e2e_encrypted_value_enabled;
                }
                waTextView.setText(i3);
            }
        });
        SwitchCompat switchCompat = this.A0P;
        boolean z = false;
        if (((ActivityC13810kN) this).A09.A02() == 1) {
            z = true;
        }
        switchCompat.setChecked(z);
        TextView textView = this.A0H;
        boolean A002 = C14950mJ.A00();
        int i3 = R.string.settings_gdrive_backup_general_info_shared_storage_short;
        if (A002) {
            i3 = R.string.settings_gdrive_backup_general_info_sdcard_short;
        }
        textView.setText(i3);
        A2l();
        this.A03 = new ViewOnClickCListenerShape0S0100000_I0(this, 30);
        this.A00 = new ViewOnClickCListenerShape0S0100000_I0(this, 36);
        this.A01 = new ViewOnClickCListenerShape0S0100000_I0(this, 29);
        this.A0A.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 32));
        ViewOnClickCListenerShape0S0100000_I0 viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0100000_I0(this, 33);
        this.A0B.setOnClickListener(this.A00);
        this.A0C.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 28));
        this.A04.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A0e.A05();
        this.A09.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A07.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A08.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A0e.A03.A05(this, new AnonymousClass02B() { // from class: X.3Pk
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                DialogFragment dialogFragment;
                SettingsGoogleDrive settingsGoogleDrive = SettingsGoogleDrive.this;
                Number number = (Number) obj;
                if (number != null && number.intValue() == 1 && (dialogFragment = (DialogFragment) settingsGoogleDrive.A0V().A0A("13")) != null && dialogFragment.A0e()) {
                    Log.i("settings-gdrive-observer/wifi-connected user is waiting on \"backup on cellular\" prompt  and Wi-Fi is connected, start backup");
                    settingsGoogleDrive.A2k();
                    dialogFragment.A1C();
                }
            }
        });
        bindService(C14960mK.A0U(this, null), this.A0e.A00, 1);
        if (!this.A0Y.A09()) {
            Log.i("settings-gdrive/create google drive access not allowed.");
            finish();
        }
        if ((bundle == null || !bundle.getBoolean("intent_already_parsed", false)) && (intent = getIntent()) != null) {
            if (intent.getAction() != null) {
                onNewIntent(intent);
            }
            if (intent.getBooleanExtra("backup_quota_notification", false)) {
                AnonymousClass2KB r1 = new AnonymousClass2KB();
                r1.A04 = 3;
                this.A0l.A07(r1);
            }
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 600) {
            return SettingsChat.A02(this);
        }
        if (i == 602) {
            return SettingsChat.A03(this);
        }
        if (i == 605) {
            ProgressDialogC48342Fq r1 = new ProgressDialogC48342Fq(this);
            SettingsChat.A0T = r1;
            r1.setTitle(R.string.msg_store_backup_db_title);
            SettingsChat.A0T.setMessage(getString(R.string.settings_backup_db_now_message));
            SettingsChat.A0T.setIndeterminate(true);
            SettingsChat.A0T.setCancelable(false);
            return SettingsChat.A0T;
        } else if (i != 606) {
            return super.onCreateDialog(i);
        } else {
            DialogC72873fJ r12 = new DialogC72873fJ(this, this.A0e);
            this.A0c = r12;
            return r12;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0o = true;
        this.A0e.A0g.set(false);
        unbindService(this.A0e.A00);
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        AnonymousClass3FU r3;
        int i;
        super.onNewIntent(intent);
        String action = intent.getAction();
        StringBuilder sb = new StringBuilder("settings-gdrive/new-intent/action/");
        sb.append(action);
        Log.i(sb.toString());
        if (action != null) {
            String str = "action_perform_backup_over_cellular";
            if (!action.equals(str)) {
                str = "action_perform_media_restore_over_cellular";
                if (!action.equals(str)) {
                    StringBuilder sb2 = new StringBuilder("settings-gdrive/new-intent/unexpected-action/");
                    sb2.append(intent.getAction());
                    Log.e(sb2.toString());
                    return;
                }
                r3 = new AnonymousClass3FU(15);
                i = R.string.google_drive_confirm_media_restore_over_cellular_message;
            } else {
                r3 = new AnonymousClass3FU(16);
                i = R.string.google_drive_confirm_backup_over_cellular_message;
            }
            String string = getString(i);
            Bundle bundle = r3.A00;
            bundle.putCharSequence("message", string);
            r3.A00();
            r3.A02(getString(R.string.google_drive_resume_button_label));
            r3.A01(getString(R.string.not_now));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            C004902f r0 = new C004902f(A0V());
            r0.A09(promptDialogFragment, str);
            r0.A02();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        onBackPressed();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        C17050qB r0 = this.A0h;
        AbstractC453621g r1 = this.A0g;
        if (r1 != null) {
            r0.A06.remove(r1);
        }
        super.onPause();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C17050qB r0 = this.A0h;
        AbstractC453621g r1 = this.A0g;
        if (r1 != null) {
            r0.A06.add(r1);
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("intent_already_parsed", true);
    }

    /* loaded from: classes2.dex */
    public class AuthRequestDialogFragment extends Hilt_SettingsGoogleDrive_AuthRequestDialogFragment {
        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            ProgressDialogC48342Fq r2 = new ProgressDialogC48342Fq(A0p());
            r2.setTitle(R.string.settings_gdrive_authenticating_with_google_servers_title);
            r2.setIndeterminate(true);
            r2.setMessage(A0I(R.string.settings_gdrive_authenticating_with_google_servers_message));
            r2.setCancelable(true);
            r2.setOnCancelListener(new DialogInterface$OnCancelListenerC96164ex(this));
            return r2;
        }
    }
}
