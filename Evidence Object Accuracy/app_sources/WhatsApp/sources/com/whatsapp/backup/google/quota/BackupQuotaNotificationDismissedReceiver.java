package com.whatsapp.backup.google.quota;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.AnonymousClass2KB;
import X.C16120oU;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BackupQuotaNotificationDismissedReceiver extends BroadcastReceiver {
    public C16120oU A00;
    public final Object A01;
    public volatile boolean A02;

    public BackupQuotaNotificationDismissedReceiver() {
        this(0);
    }

    public BackupQuotaNotificationDismissedReceiver(int i) {
        this.A02 = false;
        this.A01 = new Object();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = (C16120oU) ((AnonymousClass01J) AnonymousClass22D.A00(context)).ANE.get();
                    this.A02 = true;
                }
            }
        }
        Log.i("BackupQuotaNotification/dismissed");
        AnonymousClass2KB r1 = new AnonymousClass2KB();
        r1.A05 = 3;
        this.A00.A07(r1);
    }
}
