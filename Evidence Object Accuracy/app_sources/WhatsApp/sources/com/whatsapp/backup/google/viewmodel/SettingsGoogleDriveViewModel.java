package com.whatsapp.backup.google.viewmodel;

import X.AbstractC14440lR;
import X.AbstractC20260vT;
import X.AbstractC87994Dv;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass01I;
import X.AnonymousClass10I;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10L;
import X.AnonymousClass10M;
import X.AnonymousClass1D6;
import X.AnonymousClass1I1;
import X.AnonymousClass3LH;
import X.AnonymousClass53D;
import X.C14330lG;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15820nx;
import X.C15880o3;
import X.C18640sm;
import X.C22730zY;
import X.C25721Am;
import X.C26551Dx;
import X.C27691It;
import X.C44771zW;
import X.C58922rz;
import X.C68153Uh;
import X.C84313yl;
import X.C84323ym;
import X.C90054Mk;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.ConditionVariable;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public class SettingsGoogleDriveViewModel extends AnonymousClass015 implements AbstractC20260vT {
    public static final int[] A0h = {R.string.settings_gdrive_backup_frequency_option_off, R.string.settings_gdrive_backup_frequency_option_manual, R.string.settings_gdrive_backup_frequency_option_daily, R.string.settings_gdrive_backup_frequency_option_weekly, R.string.settings_gdrive_backup_frequency_option_monthly};
    public static final int[] A0i = {0, 4, 1, 2, 3};
    public final ServiceConnection A00;
    public final ConditionVariable A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final AnonymousClass016 A04;
    public final AnonymousClass016 A05;
    public final AnonymousClass016 A06;
    public final AnonymousClass016 A07;
    public final AnonymousClass016 A08;
    public final AnonymousClass016 A09;
    public final AnonymousClass016 A0A;
    public final AnonymousClass016 A0B;
    public final AnonymousClass016 A0C;
    public final AnonymousClass016 A0D;
    public final AnonymousClass016 A0E;
    public final AnonymousClass016 A0F;
    public final AnonymousClass016 A0G = new AnonymousClass016(Boolean.FALSE);
    public final AnonymousClass016 A0H = new AnonymousClass016(0L);
    public final AnonymousClass016 A0I;
    public final AnonymousClass016 A0J;
    public final AnonymousClass016 A0K;
    public final AnonymousClass016 A0L;
    public final AnonymousClass016 A0M;
    public final AnonymousClass016 A0N;
    public final AnonymousClass016 A0O;
    public final C14330lG A0P;
    public final C15820nx A0Q;
    public final C25721Am A0R;
    public final AnonymousClass10M A0S;
    public final AnonymousClass10I A0T;
    public final C26551Dx A0U;
    public final AnonymousClass10J A0V;
    public final AnonymousClass10K A0W;
    public final AnonymousClass10L A0X;
    public final AnonymousClass1D6 A0Y;
    public final C18640sm A0Z;
    public final C14820m6 A0a;
    public final C15880o3 A0b;
    public final C14850m9 A0c;
    public final C27691It A0d;
    public final AbstractC14440lR A0e;
    public final AtomicBoolean A0f;
    public final AtomicBoolean A0g;

    public SettingsGoogleDriveViewModel(C14330lG r21, C14900mE r22, C15820nx r23, C25721Am r24, AnonymousClass10I r25, C22730zY r26, C26551Dx r27, AnonymousClass10J r28, AnonymousClass10K r29, AnonymousClass1D6 r30, C18640sm r31, C14820m6 r32, C15880o3 r33, C14850m9 r34, AbstractC14440lR r35) {
        AnonymousClass016 r8 = new AnonymousClass016();
        this.A0O = r8;
        AnonymousClass016 r2 = new AnonymousClass016();
        this.A0F = r2;
        this.A0I = new AnonymousClass016();
        AnonymousClass016 r7 = new AnonymousClass016();
        this.A02 = r7;
        AnonymousClass016 r6 = new AnonymousClass016();
        this.A04 = r6;
        this.A0L = new AnonymousClass016();
        this.A0J = new AnonymousClass016();
        this.A0K = new AnonymousClass016();
        this.A09 = new AnonymousClass016();
        this.A0M = new AnonymousClass016();
        this.A0C = new AnonymousClass016();
        this.A0B = new AnonymousClass016();
        this.A06 = new AnonymousClass016();
        this.A08 = new AnonymousClass016();
        AnonymousClass016 r5 = new AnonymousClass016();
        this.A07 = r5;
        this.A05 = new AnonymousClass016(Boolean.TRUE);
        this.A0D = new AnonymousClass016(10);
        this.A0E = new AnonymousClass016(new C90054Mk(10, null));
        this.A0d = new C27691It();
        this.A0N = new AnonymousClass016();
        this.A0A = new AnonymousClass016();
        this.A0g = new AtomicBoolean();
        this.A0f = new AtomicBoolean();
        boolean z = false;
        this.A01 = new ConditionVariable(false);
        this.A00 = new AnonymousClass3LH(this);
        this.A0c = r34;
        this.A0e = r35;
        this.A0P = r21;
        this.A0Q = r23;
        this.A0Y = r30;
        this.A0b = r33;
        this.A0R = r24;
        this.A0U = r27;
        this.A0a = r32;
        this.A0T = r25;
        this.A0Z = r31;
        this.A0W = r29;
        this.A0V = r28;
        this.A0X = new C68153Uh(r22, r29, this, r31, r32);
        this.A0S = new AnonymousClass53D(r24, this);
        r31.A03(this);
        r2.A0B(Boolean.valueOf(r32.A00.getBoolean("gdrive_include_videos_in_backup", false)));
        C14820m6 r1 = this.A0a;
        String A09 = r1.A09();
        if (!TextUtils.isEmpty(A09)) {
            SharedPreferences sharedPreferences = r1.A00;
            StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_video_size:");
            sb.append(A09);
            long j = sharedPreferences.getLong(sb.toString(), -1);
            if (j > 0) {
                r8.A0B(new C84323ym(j));
            }
        }
        r7.A0B(r32.A09());
        r6.A0B(Integer.valueOf(r32.A01()));
        if (!r26.A0c.get() && !C44771zW.A0H(r32)) {
            z = true;
        }
        r5.A0B(Boolean.valueOf(z));
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0Z.A04(this);
        AnonymousClass10J r0 = this.A0V;
        r0.A01.A04(this.A0X);
        AnonymousClass10I r02 = this.A0T;
        r02.A00.A04(this.A0S);
    }

    public void A04() {
        AnonymousClass016 r1;
        C84323ym r0;
        C14820m6 r12 = this.A0a;
        String A09 = r12.A09();
        if (!TextUtils.isEmpty(A09)) {
            SharedPreferences sharedPreferences = r12.A00;
            StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_video_size:");
            sb.append(A09);
            long j = sharedPreferences.getLong(sb.toString(), -1);
            if (j > 0) {
                r1 = this.A0O;
                r0 = new C84323ym(j);
                r1.A0B(r0);
                return;
            }
        }
        Object A01 = this.A0F.A01();
        Boolean bool = Boolean.TRUE;
        r1 = this.A0O;
        if (A01 != bool) {
            r0 = null;
            r1.A0B(r0);
            return;
        }
        r1.A0B(new C84313yl());
        this.A0e.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(this, 0));
    }

    public void A05() {
        this.A0e.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(this, 1));
        A04();
        C14820m6 r4 = this.A0a;
        String A09 = r4.A09();
        int i = 0;
        if (A09 != null) {
            boolean A1I = r4.A1I(A09);
            int A06 = r4.A06(A09);
            if (A1I || A06 == 0) {
                i = A06;
            } else {
                r4.A0l(A09, 0);
            }
        }
        this.A0I.A0B(Integer.valueOf(i));
    }

    public void A06(boolean z) {
        boolean A01 = AnonymousClass01I.A01();
        AnonymousClass016 r1 = this.A0C;
        Boolean valueOf = Boolean.valueOf(z);
        if (A01) {
            r1.A0B(valueOf);
        } else {
            r1.A0A(valueOf);
        }
    }

    public boolean A07(int i) {
        if (!this.A0a.A1H(i)) {
            return false;
        }
        this.A04.A0B(Integer.valueOf(i));
        return true;
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r5) {
        int A05 = this.A0Z.A05(true);
        this.A03.A0A(Integer.valueOf(A05));
        if (A05 == 0 || A05 == 2) {
            AbstractC87994Dv r1 = (AbstractC87994Dv) this.A08.A01();
            if (r1 instanceof C58922rz) {
                int i = ((C58922rz) r1).A00;
                if (i == 0) {
                    this.A0X.ASa(0, 0);
                } else if (i == 6 || i == 7) {
                    this.A0X.AN3(0, 0);
                }
            }
        }
    }
}
