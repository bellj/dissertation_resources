package com.whatsapp.backup.google.workers;

import X.AbstractC15710nm;
import X.AbstractFutureC44231yX;
import X.AnonymousClass01J;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10N;
import X.AnonymousClass1D6;
import X.C006503b;
import X.C05360Pg;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16240og;
import X.C16490p7;
import X.C16590pI;
import X.C17050qB;
import X.C17220qS;
import X.C19380u1;
import X.C21630xj;
import X.C22660zR;
import X.C22730zY;
import X.C26551Dx;
import X.C27051Fv;
import X.C44771zW;
import X.C44791zY;
import X.C44801zZ;
import X.C44921zm;
import X.C44931zn;
import X.C81353ts;
import android.content.Context;
import android.text.TextUtils;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class GoogleBackupWorker extends Worker {
    public final int A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C15570nT A03;
    public final C16240og A04;
    public final C15820nx A05;
    public final C22730zY A06;
    public final C26551Dx A07;
    public final AnonymousClass10N A08;
    public final AnonymousClass10J A09;
    public final C44801zZ A0A;
    public final AnonymousClass10K A0B;
    public final C27051Fv A0C;
    public final AnonymousClass1D6 A0D;
    public final C19380u1 A0E;
    public final C15810nw A0F;
    public final C17050qB A0G;
    public final C14830m7 A0H;
    public final C16590pI A0I;
    public final C15890o4 A0J;
    public final C14820m6 A0K;
    public final C15880o3 A0L;
    public final C16490p7 A0M;
    public final C21630xj A0N;
    public final C14850m9 A0O;
    public final C16120oU A0P;
    public final C44931zn A0Q;
    public final C17220qS A0R;
    public final C22660zR A0S;

    public GoogleBackupWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J A0S = C12990iw.A0S(context);
        this.A0H = A0S.Aen();
        this.A0O = A0S.A3L();
        this.A01 = A0S.A7p();
        this.A03 = A0S.A1w();
        this.A0I = C12970iu.A0X(A0S);
        this.A02 = (C14330lG) A0S.A7B.get();
        this.A0P = A0S.Ag5();
        this.A0F = (C15810nw) A0S.A73.get();
        this.A0S = (C22660zR) A0S.AAk.get();
        C17220qS A3P = A0S.A3P();
        this.A0R = A3P;
        this.A0E = (C19380u1) A0S.A1N.get();
        this.A05 = (C15820nx) A0S.A6U.get();
        C16240og r5 = (C16240og) A0S.ANq.get();
        this.A04 = r5;
        this.A0G = (C17050qB) A0S.ABL.get();
        this.A0N = (C21630xj) A0S.ACc.get();
        this.A0D = (AnonymousClass1D6) A0S.A1G.get();
        this.A0L = (C15880o3) A0S.ACF.get();
        this.A07 = (C26551Dx) A0S.A8Z.get();
        this.A0M = (C16490p7) A0S.ACJ.get();
        this.A0C = (C27051Fv) A0S.AHd.get();
        this.A0J = C12970iu.A0Y(A0S);
        this.A0K = A0S.Ag2();
        C22730zY r4 = (C22730zY) A0S.A8Y.get();
        this.A06 = r4;
        this.A08 = (AnonymousClass10N) A0S.A8a.get();
        this.A0B = (AnonymousClass10K) A0S.A8c.get();
        this.A09 = (AnonymousClass10J) A0S.A8b.get();
        C44931zn r7 = new C44931zn();
        this.A0Q = r7;
        r7.A0F = C12970iu.A0g();
        C006503b r3 = super.A01.A01;
        r7.A0G = Integer.valueOf(r3.A02("KEY_BACKUP_SCHEDULE", 0));
        r7.A0C = Integer.valueOf(r3.A02("KEY_BACKUP_NETWORK_SETTING", -1));
        this.A0A = new C44801zZ(r5, r4, A3P);
        this.A00 = r3.A02("KEY_MAX_NUMBER_OF_RETRIES", 0);
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A00() {
        C81353ts r4 = new C81353ts();
        r4.A04(new C05360Pg(5, this.A0B.A00(C16590pI.A00(this.A0I), null), 0));
        return r4;
    }

    @Override // androidx.work.ListenableWorker
    public void A03() {
        Log.i("google-backup-worker/onStopped");
        this.A0A.A06();
        this.A06.A0c.getAndSet(false);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 11, insn: 0x025b: IGET  (r0 I:X.10K) = (r11 I:com.whatsapp.backup.google.workers.GoogleBackupWorker) com.whatsapp.backup.google.workers.GoogleBackupWorker.A0B X.10K, block:B:88:0x025b
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // androidx.work.Worker
    public X.AnonymousClass043 A04() {
        /*
        // Method dump skipped, instructions count: 609
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.workers.GoogleBackupWorker.A04():X.043");
    }

    public final void A05() {
        this.A0E.A00(6, false);
        C22730zY r3 = this.A06;
        r3.A05();
        C14820m6 r2 = this.A0K;
        if (C44771zW.A0G(r2) || r3.A0c.get()) {
            r3.A0c.getAndSet(false);
            C26551Dx r1 = this.A07;
            C44791zY A00 = r1.A00();
            C19380u1 r12 = r1.A0E;
            if (A00 != null) {
                A00.A09(false);
            }
            r12.A00(2, false);
            C44921zm.A01();
            r3.A0G.open();
            r3.A0D.open();
            r3.A0A.open();
            r3.A04 = false;
            r2.A0V(0);
            r2.A0S(10);
        }
        AnonymousClass10N r13 = this.A08;
        r13.A00 = -1;
        r13.A01 = -1;
        AnonymousClass10J r32 = this.A09;
        r32.A06.set(0);
        r32.A05.set(0);
        r32.A04.set(0);
        r32.A07.set(0);
        r32.A03.set(0);
    }

    public final void A06(int i) {
        if (this.A0A.A05()) {
            String A04 = C44771zW.A04(i);
            if (i != 10) {
                TextUtils.join("\n", Thread.currentThread().getStackTrace());
                Log.e(C12960it.A0d(A04, C12960it.A0k("google-backup-worker/set-error/")));
            }
            this.A0K.A0S(i);
            C12990iw.A1M(this.A0Q, C44771zW.A00(i));
            this.A08.A07(i, this.A09.A00());
        }
    }
}
