package com.whatsapp.backup.google;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass116;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass19M;
import X.AnonymousClass1D6;
import X.AnonymousClass29U;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass3NM;
import X.C102404p5;
import X.C102814pk;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16590pI;
import X.C17050qB;
import X.C18260sA;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20660w7;
import X.C21710xr;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22730zY;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26551Dx;
import android.content.ActivityNotFoundException;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatSpinner;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1200000_I0;
import com.whatsapp.R;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public final class GoogleDriveNewUserSetupActivity extends AnonymousClass29U {
    public int A00;
    public View A01;
    public Button A02;
    public RadioGroup A03;
    public AppCompatSpinner A04;
    public List A05;
    public boolean A06;
    public RadioButton[] A07;
    public final ViewTreeObserver.OnGlobalLayoutListener A08;

    public GoogleDriveNewUserSetupActivity() {
        this(0);
        this.A00 = -1;
        this.A08 = new AnonymousClass3NM(this);
    }

    public GoogleDriveNewUserSetupActivity(int i) {
        this.A06 = false;
        A0R(new C102814pk(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AnonymousClass29U) this).A0N = (C16590pI) r1.AMg.get();
            this.A0Q = (C16120oU) r1.ANE.get();
            this.A0S = (AnonymousClass12U) r1.AJd.get();
            this.A0R = (C20660w7) r1.AIB.get();
            this.A0T = (C21710xr) r1.ANg.get();
            ((AnonymousClass29U) this).A0E = (C15820nx) r1.A6U.get();
            ((AnonymousClass29U) this).A0M = (C17050qB) r1.ABL.get();
            ((AnonymousClass29U) this).A0J = (AnonymousClass1D6) r1.A1G.get();
            this.A0P = (C18260sA) r1.AAZ.get();
            ((AnonymousClass29U) this).A0L = (AnonymousClass116) r1.A3z.get();
            ((AnonymousClass29U) this).A0G = (C26551Dx) r1.A8Z.get();
            ((AnonymousClass29U) this).A0O = (C15890o4) r1.AN1.get();
            ((AnonymousClass29U) this).A0F = (C22730zY) r1.A8Y.get();
            ((AnonymousClass29U) this).A0I = (AnonymousClass10K) r1.A8c.get();
            ((AnonymousClass29U) this).A0H = (AnonymousClass10J) r1.A8b.get();
        }
    }

    @Override // X.AnonymousClass29U
    public void A2e() {
        super.A2e();
        if (this.A00 != 0) {
            A2p(false);
            A2n();
            this.A00 = -1;
        }
    }

    public final void A2m() {
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        int dimensionPixelSize = point.x - getResources().getDimensionPixelSize(R.dimen.gdrive_view_left_padding);
        for (RadioButton radioButton : this.A07) {
            radioButton.setWidth(dimensionPixelSize);
        }
    }

    public final void A2n() {
        this.A03.clearCheck();
        this.A04.setSelection(this.A05.size() - 1, true);
    }

    public final void A2o(RadioButton radioButton, String str) {
        int i = 2;
        String.format("gdrive-new-user-setup/freq-option-changed item:%s radioBtn:%s", str, radioButton);
        if (getString(R.string.settings_gdrive_backup_frequency_option_daily).equals(str)) {
            i = 1;
        } else if (!getString(R.string.settings_gdrive_backup_frequency_option_weekly).equals(str)) {
            if (getString(R.string.settings_gdrive_backup_frequency_option_monthly).equals(str)) {
                i = 3;
            } else if (getString(R.string.settings_gdrive_backup_frequency_option_off).equals(str)) {
                i = 0;
            } else {
                StringBuilder sb = new StringBuilder("gdrive-new-user-setup/create/unexpected-backup-frequency/");
                sb.append(str);
                Log.i(sb.toString());
                i = -1;
            }
        }
        int i2 = this.A00;
        this.A00 = i;
        if (radioButton != null) {
            A2n();
            radioButton.toggle();
            radioButton.getText();
            this.A04.setSelection(this.A05.indexOf(radioButton.getText().toString()));
        }
        A2p(true);
        if ((i2 == -1 || i2 == 0 || ((ActivityC13810kN) this).A09.A09() == null) && i != 0 && i != -1) {
            this.A01.performClick();
        }
    }

    public final void A2p(boolean z) {
        int i;
        if (this.A02 == null) {
            Log.e("gdrive-new-user-setup/update-setup-btn/setup-btn-is-null");
            return;
        }
        AnonymousClass2GF r3 = new AnonymousClass2GF(getResources().getDrawable(R.drawable.chevron), ((ActivityC13830kP) this).A01);
        if (z) {
            this.A02.setTextColor(getResources().getColor(R.color.primary_light));
            r3.setColorFilter(getResources().getColor(R.color.primary_light), PorterDuff.Mode.SRC_ATOP);
            i = 255;
        } else {
            int color = getResources().getColor(R.color.settings_disabled_text);
            this.A02.setTextColor(color);
            r3.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            i = color >>> 24;
        }
        r3.setAlpha(i);
        boolean z2 = !((ActivityC13830kP) this).A01.A04().A06;
        Button button = this.A02;
        if (z2) {
            button.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, r3, (Drawable) null);
        } else {
            button.setCompoundDrawablesWithIntrinsicBounds(r3, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }

    @Override // X.AnonymousClass29U, X.AnonymousClass29H
    public void APA(int i) {
        if (i == 14) {
            this.A00 = 0;
            this.A02.performClick();
            return;
        }
        super.APA(i);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        try {
            AnonymousClass12P.A03(this);
        } catch (ActivityNotFoundException e) {
            Log.e("gdrive-new-user-setup/back-pressed", e);
            ((ActivityC13810kN) this).A05.A07(R.string.gdrive_new_user_setup_button_toast_no_freq_selected, 1);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        List list;
        int i;
        super.onConfigurationChanged(configuration);
        A2m();
        int i2 = this.A00;
        if (i2 == 0) {
            list = this.A05;
            i = R.string.settings_gdrive_backup_frequency_option_off;
        } else if (i2 == 1) {
            list = this.A05;
            i = R.string.settings_gdrive_backup_frequency_option_daily;
        } else if (i2 != 2) {
            if (i2 == 3) {
                list = this.A05;
                i = R.string.settings_gdrive_backup_frequency_option_monthly;
            }
            A2n();
            this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A08);
        } else {
            list = this.A05;
            i = R.string.settings_gdrive_backup_frequency_option_weekly;
        }
        int indexOf = list.indexOf(getString(i));
        if (indexOf >= 0) {
            RadioButton radioButton = this.A07[indexOf];
            radioButton.toggle();
            radioButton.getText();
            this.A04.setSelection(this.A05.indexOf(radioButton.getText().toString()));
            this.A04.setSelection(indexOf);
            this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A08);
        }
        A2n();
        this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A08);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v71, resolved type: android.widget.RadioButton[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AnonymousClass29U, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!((AnonymousClass29U) this).A0F.A0A()) {
            Log.i("gdrive-new-user-setup/create no need to display GoogleDriveNewUserSetupActivity, exiting.");
            setResult(-1);
            finish();
            return;
        }
        setTitle(R.string.gdrive_new_user_setup_title);
        A1U().A0M(false);
        findViewById(R.id.settings_gdrive_backup_info_box).setVisibility(8);
        findViewById(R.id.settings_gdrive_gdrive_category_title).setVisibility(8);
        findViewById(R.id.settings_gdrive_change_frequency_view).setVisibility(8);
        findViewById(R.id.settings_gdrive_network_settings_view).setVisibility(8);
        findViewById(R.id.include_video_settings_summary).setVisibility(8);
        findViewById(R.id.gdrive_new_user_setup_message).setVisibility(0);
        findViewById(R.id.gdrive_new_user_setup_select_frequency_message).setVisibility(0);
        TextView textView = (TextView) findViewById(R.id.gdrive_new_user_setup_footer_info);
        textView.setText(getString(R.string.gdrive_new_user_setup_footer_info, getString(R.string.settings_general), getString(R.string.settings_chat), getString(R.string.settings_backup)));
        textView.setVisibility(0);
        findViewById(R.id.backup_settings_icon).setVisibility(0);
        TextView textView2 = (TextView) findViewById(R.id.settings_gdrive_backup_now_category_title);
        textView2.setVisibility(0);
        textView2.setText(R.string.gdrive_new_user_setup_category_title);
        ((TextView) findViewById(R.id.settings_gdrive_change_account_title)).setText(R.string.gdrive_new_user_setup_account_title);
        this.A01 = findViewById(R.id.settings_gdrive_change_account_view);
        this.A03 = (RadioGroup) findViewById(R.id.gdrive_new_user_setup_freq_options);
        this.A05 = new ArrayList();
        int[] iArr = SettingsGoogleDriveViewModel.A0h;
        for (int i : iArr) {
            if (!(i == R.string.settings_gdrive_backup_frequency_option_manual || i == R.string.settings_gdrive_backup_frequency_option_off)) {
                this.A05.add(getString(i));
            }
        }
        this.A05.add(getString(R.string.settings_gdrive_backup_frequency_option_off));
        this.A05.add(getString(R.string.gdrive_new_user_setup_select_a_backup_frequency));
        this.A03.setVisibility(0);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.A05);
        arrayAdapter.setDropDownViewResource(17367049);
        AppCompatSpinner appCompatSpinner = (AppCompatSpinner) findViewById(R.id.gdrive_new_user_setup_freq_options_spinner);
        this.A04 = appCompatSpinner;
        appCompatSpinner.setAdapter((SpinnerAdapter) arrayAdapter);
        this.A04.setSelection(this.A05.size() - 1);
        this.A04.setOnItemSelectedListener(new C102404p5(this));
        LayoutInflater A01 = AnonymousClass01d.A01(this);
        AnonymousClass009.A05(A01);
        this.A07 = new RadioButton[this.A05.size() - 1];
        this.A03.addView(A01.inflate(R.layout.google_drive_new_user_setup_divider, (ViewGroup) null));
        for (int i2 = 0; i2 < this.A07.length; i2++) {
            String str = (String) this.A05.get(i2);
            TextView textView3 = (TextView) A01.inflate(R.layout.google_drive_new_user_setup_frequency_option, (ViewGroup) null);
            textView3.setText(str);
            this.A03.addView(textView3);
            this.A03.addView(A01.inflate(R.layout.google_drive_new_user_setup_divider, (ViewGroup) null));
            this.A07[i2] = textView3;
            textView3.setOnClickListener(new ViewOnClickCListenerShape0S1200000_I0(this, textView3, str, 0));
        }
        A2m();
        Button button = (Button) findViewById(R.id.gdrive_new_user_setup_btn);
        this.A02 = button;
        button.setVisibility(0);
        A2p(false);
        this.A02.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 20));
        this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A08);
    }
}
