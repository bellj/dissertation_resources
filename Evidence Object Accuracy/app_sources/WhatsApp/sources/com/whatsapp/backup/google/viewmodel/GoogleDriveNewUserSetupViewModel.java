package com.whatsapp.backup.google.viewmodel;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass1D6;
import X.C14820m6;
import X.C26551Dx;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class GoogleDriveNewUserSetupViewModel extends AnonymousClass015 {
    public static final int[] A06 = {R.string.settings_gdrive_backup_frequency_option_off, R.string.settings_gdrive_backup_frequency_option_manual, R.string.settings_gdrive_backup_frequency_option_daily, R.string.settings_gdrive_backup_frequency_option_weekly, R.string.settings_gdrive_backup_frequency_option_monthly};
    public static final int[] A07 = {0, 4, 1, 2, 3};
    public final AnonymousClass016 A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass016 A02;
    public final C26551Dx A03;
    public final AnonymousClass1D6 A04;
    public final C14820m6 A05;

    public GoogleDriveNewUserSetupViewModel(C26551Dx r7, AnonymousClass1D6 r8, C14820m6 r9) {
        AnonymousClass016 r5 = new AnonymousClass016();
        this.A02 = r5;
        AnonymousClass016 r4 = new AnonymousClass016();
        this.A00 = r4;
        AnonymousClass016 r3 = new AnonymousClass016();
        this.A01 = r3;
        this.A04 = r8;
        this.A03 = r7;
        this.A05 = r9;
        r5.A0B(Boolean.valueOf(r9.A00.getBoolean("gdrive_include_videos_in_backup", false)));
        r4.A0B(r9.A09());
        r3.A0B(Integer.valueOf(r9.A01()));
    }

    public boolean A04(int i) {
        if (!this.A05.A1H(i)) {
            return false;
        }
        this.A01.A0B(Integer.valueOf(i));
        return true;
    }
}
