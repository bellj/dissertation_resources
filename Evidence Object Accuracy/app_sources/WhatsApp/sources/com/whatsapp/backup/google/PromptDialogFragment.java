package com.whatsapp.backup.google;

import X.AnonymousClass01d;
import X.AnonymousClass29H;
import X.C12960it;
import X.C12970iu;
import android.content.Context;

/* loaded from: classes2.dex */
public class PromptDialogFragment extends Hilt_PromptDialogFragment {
    public AnonymousClass29H A00;
    public AnonymousClass01d A01;

    @Override // com.whatsapp.backup.google.Hilt_PromptDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A00 = (AnonymousClass29H) context;
        } catch (ClassCastException unused) {
            StringBuilder A0h = C12960it.A0h();
            C12970iu.A1V(context, A0h);
            throw new ClassCastException(C12960it.A0d(" must implement PromptDialogClickListener", A0h));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007c, code lost:
        if (r3.getBoolean("cancelable") != false) goto L_0x007e;
     */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r6) {
        /*
            r5 = this;
            android.os.Bundle r3 = r5.A03()
            java.lang.String r1 = "dialog_id"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x00a3
            int r4 = r3.getInt(r1)
            X.02e r2 = X.C12970iu.A0O(r5)
            java.lang.String r1 = "title"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x0024
            java.lang.String r0 = r3.getString(r1)
            r2.setTitle(r0)
        L_0x0024:
            java.lang.String r1 = "message"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x0033
            java.lang.CharSequence r0 = r3.getCharSequence(r1)
            r2.A0A(r0)
        L_0x0033:
            java.lang.String r1 = "neutral_button"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x0047
            java.lang.String r1 = r3.getString(r1)
            X.4gV r0 = new X.4gV
            r0.<init>(r4)
            r2.A02(r0, r1)
        L_0x0047:
            java.lang.String r1 = "positive_button"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x005b
            java.lang.String r1 = r3.getString(r1)
            X.4gT r0 = new X.4gT
            r0.<init>(r4)
            r2.A03(r0, r1)
        L_0x005b:
            java.lang.String r1 = "negative_button"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x006f
            java.lang.String r1 = r3.getString(r1)
            X.4gU r0 = new X.4gU
            r0.<init>(r4)
            r2.A01(r0, r1)
        L_0x006f:
            java.lang.String r1 = "cancelable"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x007e
            boolean r1 = r3.getBoolean(r1)
            r0 = 0
            if (r1 == 0) goto L_0x007f
        L_0x007e:
            r0 = 1
        L_0x007f:
            r2.A0B(r0)
            r5.A1G(r0)
            X.04S r2 = r2.create()
            r2.setCanceledOnTouchOutside(r0)
            java.lang.String r1 = "is_message_clickable"
            boolean r0 = r3.containsKey(r1)
            if (r0 == 0) goto L_0x00a2
            boolean r0 = r3.getBoolean(r1)
            if (r0 == 0) goto L_0x00a2
            X.3LE r0 = new X.3LE
            r0.<init>(r2, r5)
            r2.setOnShowListener(r0)
        L_0x00a2:
            return r2
        L_0x00a3:
            java.lang.String r0 = "dialog_id should be provided."
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.PromptDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }
}
