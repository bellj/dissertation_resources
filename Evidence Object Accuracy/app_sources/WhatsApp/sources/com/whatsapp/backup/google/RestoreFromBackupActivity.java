package com.whatsapp.backup.google;

import X.AbstractActivityC28171Kz;
import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC32851cq;
import X.AbstractC44761zV;
import X.AbstractC87984Du;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass01I;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10J;
import X.AnonymousClass10L;
import X.AnonymousClass10O;
import X.AnonymousClass116;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass19M;
import X.AnonymousClass1D6;
import X.AnonymousClass1Tv;
import X.AnonymousClass1UB;
import X.AnonymousClass29F;
import X.AnonymousClass29H;
import X.AnonymousClass29I;
import X.AnonymousClass29M;
import X.AnonymousClass29N;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2FR;
import X.AnonymousClass3HG;
import X.AnonymousClass3LG;
import X.AnonymousClass432;
import X.C004902f;
import X.C102824pl;
import X.C1104455o;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15650ng;
import X.C15810nw;
import X.C15820nx;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16490p7;
import X.C16590pI;
import X.C16630pM;
import X.C17050qB;
import X.C18350sJ;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C19350ty;
import X.C19890uq;
import X.C19930uu;
import X.C20650w6;
import X.C20670w8;
import X.C20710wC;
import X.C20740wF;
import X.C20820wN;
import X.C20850wQ;
import X.C21630xj;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22730zY;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25701Ak;
import X.C25721Am;
import X.C25771At;
import X.C25991Bp;
import X.C26041Bu;
import X.C26551Dx;
import X.C27051Fv;
import X.C29851Uy;
import X.C32811cm;
import X.C36021jC;
import X.C38131nZ;
import X.C38241nl;
import X.C41691tw;
import X.C42971wC;
import X.C43951xu;
import X.C44771zW;
import X.C44891zj;
import X.C44921zm;
import X.C471229a;
import X.C471329b;
import X.C58272oQ;
import X.C58902rx;
import X.C66873Pg;
import X.C68113Ud;
import X.C73833gs;
import X.C84033yJ;
import X.C84263yg;
import X.C88134Ek;
import X.EnumC16570pG;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Environment;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200100_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape0S0000000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0110000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1200000_I0;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.backup.google.viewmodel.RestoreFromBackupViewModel;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.settings.SettingsChat;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public final class RestoreFromBackupActivity extends AbstractActivityC28171Kz implements AnonymousClass29H, AnonymousClass29I {
    public int A00;
    public long A01;
    public long A02;
    public View A03;
    public Button A04;
    public ProgressBar A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public C18790t3 A09;
    public C21740xu A0A;
    public C15820nx A0B;
    public C25701Ak A0C;
    public AnonymousClass29F A0D;
    public C25721Am A0E;
    public AnonymousClass29N A0F;
    public C22730zY A0G;
    public C26551Dx A0H;
    public AnonymousClass10J A0I;
    public GoogleDriveRestoreAnimationView A0J;
    public C27051Fv A0K;
    public AnonymousClass1D6 A0L;
    public RestoreFromBackupViewModel A0M;
    public AnonymousClass116 A0N;
    public C16590pI A0O;
    public C15890o4 A0P;
    public C19350ty A0Q;
    public C15650ng A0R;
    public C29851Uy A0S;
    public C16490p7 A0T;
    public C21630xj A0U;
    public C25771At A0V;
    public C16120oU A0W;
    public C16630pM A0X;
    public AnonymousClass3HG A0Y;
    public AnonymousClass10O A0Z;
    public C25661Ag A0a;
    public AnonymousClass12U A0b;
    public C252018m A0c;
    public C19930uu A0d;
    public String A0e;
    public boolean A0f;
    public boolean A0g = false;
    public boolean A0h;
    public boolean A0i;
    public final ServiceConnection A0j;
    public final ConditionVariable A0k;
    public final ConditionVariable A0l;
    public final ConditionVariable A0m;
    public final AnonymousClass10L A0n;
    public final AnonymousClass29M A0o;
    public final AbstractC44761zV A0p;
    public final AbstractC32851cq A0q;
    public final List A0r;
    public final Set A0s;
    public final AtomicBoolean A0t;
    public final AtomicBoolean A0u;
    public final AtomicBoolean A0v;
    public final AtomicBoolean A0w;

    public RestoreFromBackupActivity() {
        super(true, false);
        A0R(new C102824pl(this));
        this.A0r = new ArrayList();
        this.A0s = Collections.newSetFromMap(new ConcurrentHashMap());
        this.A0l = new ConditionVariable(false);
        this.A0k = new ConditionVariable(false);
        this.A0v = new AtomicBoolean();
        this.A0m = new ConditionVariable(false);
        this.A0t = new AtomicBoolean(false);
        this.A0u = new AtomicBoolean(false);
        this.A0w = new AtomicBoolean(true);
        this.A0j = new AnonymousClass3LG(this);
        this.A0p = new C84033yJ(this);
        this.A0o = new AnonymousClass29M(this);
        this.A0n = new C58902rx(this);
        this.A0q = new C1104455o(this);
    }

    public static String A02(int i) {
        switch (i) {
            case 21:
                return "new";
            case 22:
                return "restore-from-gdrive";
            case 23:
                return "restore-from-local";
            case 24:
                return "restoring-from-gdrive";
            case 25:
                return "return-from-auth";
            case 26:
                return "msgstore-restored";
            case 27:
                return "restoring-from-local";
            case 28:
                return "restore-from-backup-file";
            case 29:
                return "restoring-from-backup-file";
            default:
                StringBuilder sb = new StringBuilder("Unknown state: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
    }

    public static /* synthetic */ void A03(AccountManagerFuture accountManagerFuture, RestoreFromBackupActivity restoreFromBackupActivity) {
        try {
            Bundle bundle = (Bundle) accountManagerFuture.getResult();
            if (!bundle.containsKey("authAccount")) {
                Log.e("gdrive-activity/error-during-add-account/account-manager-returned-with-no-account-name");
            } else {
                restoreFromBackupActivity.A36(String.valueOf(bundle.get("authAccount")), 4);
            }
        } catch (AuthenticatorException | OperationCanceledException | IOException e) {
            Log.e("gdrive-activity/error-during-add-account", e);
            ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupActivity, 35));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static /* synthetic */ void A0A(Bundle bundle, RestoreFromBackupActivity restoreFromBackupActivity, int i) {
        View.OnClickListener viewOnClickCListenerShape0S0100000_I0;
        int i2;
        Object[] objArr;
        StringBuilder sb;
        AnonymousClass009.A01();
        StringBuilder sb2 = new StringBuilder("gdrive-activity-observer/display-msgstore-download-error/");
        sb2.append(C44771zW.A04(i));
        Log.i(sb2.toString());
        String string = restoreFromBackupActivity.getString(R.string.retry);
        String A09 = ((ActivityC13810kN) restoreFromBackupActivity).A09.A09();
        AnonymousClass009.A05(A09);
        String str = null;
        switch (i) {
            case 10:
                return;
            case 11:
            case 21:
            case 28:
            case 29:
            case C25991Bp.A0S:
            case 31:
                str = restoreFromBackupActivity.getString(R.string.gdrive_message_store_download_error_auth_failed_summary, string);
                viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S1100000_I0(0, A09, restoreFromBackupActivity);
                break;
            case 12:
                str = restoreFromBackupActivity.getString(R.string.gdrive_message_store_download_error_account_not_present_on_the_device_anymore_summary, A09, string);
                viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S1100000_I0(1, A09, restoreFromBackupActivity);
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                sb = new StringBuilder("gdrive-activity/display-msgstore-download-error/unexpected/");
                sb.append(i);
                Log.e(sb.toString());
                viewOnClickCListenerShape0S0100000_I0 = null;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 17:
            case 18:
            case 19:
            case 22:
            case 25:
                str = restoreFromBackupActivity.getString(R.string.gdrive_message_store_download_error_not_reachable_summary, string);
                viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0100000_I0(restoreFromBackupActivity, 24);
                break;
            case 15:
                long j = -1;
                if (bundle != null) {
                    j = bundle.getLong("msgstore_bytes_to_be_downloaded", -1);
                    if (j > 0) {
                        i2 = R.string.gdrive_message_store_download_error_internal_storage_full_summary;
                        objArr = new Object[]{C44891zj.A03(((ActivityC13830kP) restoreFromBackupActivity).A01, j), string};
                        str = restoreFromBackupActivity.getString(i2, objArr);
                        viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0101000_I0(restoreFromBackupActivity);
                        break;
                    }
                }
                StringBuilder sb3 = new StringBuilder("gdrive-activity/display-msgstore-download-error/");
                sb3.append(i);
                sb3.append(" message store download size: ");
                sb3.append(j);
                sb3.append(" is invalid");
                Log.e(sb3.toString());
                i2 = R.string.gdrive_message_store_download_error_internal_storage_full_summary_unknown_size;
                objArr = new Object[]{string};
                str = restoreFromBackupActivity.getString(i2, objArr);
                viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0101000_I0(restoreFromBackupActivity);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case C43951xu.A01:
                throw new IllegalStateException("Unexpected error");
            case 23:
                viewOnClickCListenerShape0S0100000_I0 = null;
                break;
            case 24:
                Object[] objArr2 = new Object[1];
                boolean A03 = C38241nl.A03();
                int i3 = R.string.gdrive_message_store_download_error_older_version_of_app_action;
                if (A03) {
                    i3 = R.string.learn_more;
                }
                objArr2[0] = restoreFromBackupActivity.getString(i3);
                str = restoreFromBackupActivity.getString(R.string.gdrive_message_store_download_error_older_version_of_app_with_action, objArr2);
                viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0100000_I0(restoreFromBackupActivity, 21);
                boolean A032 = C38241nl.A03();
                int i4 = R.string.upgrade;
                if (A032) {
                    i4 = R.string.software_about_to_deprecate_title;
                }
                string = restoreFromBackupActivity.getString(i4);
                break;
            case 26:
            case 27:
            default:
                sb = new StringBuilder("gdrive-activity/display-msgstore-download-error/unhandled-error/");
                sb.append(C44771zW.A04(i));
                Log.e(sb.toString());
                viewOnClickCListenerShape0S0100000_I0 = null;
                break;
        }
        if (!restoreFromBackupActivity.A33()) {
            restoreFromBackupActivity.A05.setIndeterminate(false);
            restoreFromBackupActivity.A08.setText(R.string.activity_gdrive_restore_messages_paused_message);
            GoogleDriveRestoreAnimationView googleDriveRestoreAnimationView = restoreFromBackupActivity.A0J;
            if (googleDriveRestoreAnimationView == null) {
                googleDriveRestoreAnimationView = (GoogleDriveRestoreAnimationView) AnonymousClass00T.A05(restoreFromBackupActivity, R.id.google_drive_restore_animation_view);
                restoreFromBackupActivity.A0J = googleDriveRestoreAnimationView;
            }
            googleDriveRestoreAnimationView.A01 = 4;
            C73833gs r0 = googleDriveRestoreAnimationView.A0A;
            if (r0 != null) {
                r0.cancel();
            }
            View A05 = AnonymousClass00T.A05(restoreFromBackupActivity, R.id.google_drive_backup_error_info_view);
            if (str != null) {
                A05.setVisibility(0);
                ((TextView) AnonymousClass00T.A05(restoreFromBackupActivity, R.id.google_drive_backup_error_info)).setText(str);
            } else {
                A05.setVisibility(8);
            }
            AnonymousClass00T.A05(restoreFromBackupActivity, R.id.gdrive_restore_info).setVisibility(0);
            restoreFromBackupActivity.A32(((ActivityC13810kN) restoreFromBackupActivity).A09.A00.getBoolean("gdrive_last_restore_file_is_encrypted", false));
            AnonymousClass00T.A05(restoreFromBackupActivity, R.id.restore_actions_view).setVisibility(0);
            AnonymousClass00T.A05(restoreFromBackupActivity, R.id.google_drive_media_will_be_downloaded_later_notice).setVisibility(8);
            restoreFromBackupActivity.A0J.setVisibility(8);
            restoreFromBackupActivity.A05.setVisibility(8);
            restoreFromBackupActivity.A08.setVisibility(8);
            AnonymousClass00T.A05(restoreFromBackupActivity, R.id.dont_restore).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(restoreFromBackupActivity, 22));
            restoreFromBackupActivity.A04.setText(string);
            restoreFromBackupActivity.A04.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
            return;
        }
        StringBuilder sb4 = new StringBuilder("gdrive-activity/display-msgstore-download-error failed to display error ");
        sb4.append(i);
        sb4.append(" since Activity is about to finish.");
        Log.e(sb4.toString());
    }

    public static /* synthetic */ void A0B(AnonymousClass29N r10, RestoreFromBackupActivity restoreFromBackupActivity, String str) {
        restoreFromBackupActivity.A0a.A02("backup_found", "restore");
        if (restoreFromBackupActivity.A35()) {
            return;
        }
        if (((ActivityC13790kL) restoreFromBackupActivity).A06.A01() < restoreFromBackupActivity.A01) {
            StringBuilder sb = new StringBuilder("gdrive-activity/show-restore insufficient storage, available: ");
            sb.append(((ActivityC13790kL) restoreFromBackupActivity).A06.A01());
            sb.append(" required: ");
            sb.append(restoreFromBackupActivity.A01);
            Log.i(sb.toString());
            boolean A00 = C14950mJ.A00();
            int i = R.string.gdrive_insufficient_shared_storage_message;
            if (A00) {
                i = R.string.gdrive_insufficient_sdcard_storage_message;
            }
            String string = restoreFromBackupActivity.getString(i, C44891zj.A03(((ActivityC13830kP) restoreFromBackupActivity).A01, restoreFromBackupActivity.A01));
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 13);
            bundle.putString("title", restoreFromBackupActivity.getString(R.string.gdrive_insufficient_sdcard_storage_title));
            bundle.putCharSequence("message", string);
            String string2 = restoreFromBackupActivity.getString(R.string.btn_storage_settings);
            if (Build.VERSION.SDK_INT >= 26) {
                string2 = restoreFromBackupActivity.getString(R.string.btn_clear_space);
            }
            bundle.putString("positive_button", string2);
            bundle.putString("neutral_button", restoreFromBackupActivity.getString(R.string.ok));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            promptDialogFragment.A1F(restoreFromBackupActivity.A0V(), null);
            return;
        }
        StringBuilder sb2 = new StringBuilder("gdrive-activity/show-restore starting restore from ");
        sb2.append(C44771zW.A0B(str));
        Log.i(sb2.toString());
        Log.i("gdrive-activity/show-restore/stopping-approx-transfer-size-calc-thread");
        restoreFromBackupActivity.A0t.set(true);
        if (r10.A02) {
            restoreFromBackupActivity.A2q(2);
            restoreFromBackupActivity.startActivityForResult(C14960mK.A07(restoreFromBackupActivity, 2), 0);
            return;
        }
        restoreFromBackupActivity.A2j();
        ((ActivityC13830kP) restoreFromBackupActivity).A05.Ab2(new RunnableBRunnable0Shape0S1200100_I0(restoreFromBackupActivity, r10, r10.A05, 0, r10.A00));
    }

    public static /* synthetic */ void A0D(RestoreFromBackupActivity restoreFromBackupActivity) {
        AnonymousClass009.A01();
        if (!restoreFromBackupActivity.A33()) {
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 23);
            bundle.putString("title", restoreFromBackupActivity.getString(R.string.gdrive_message_restore_failed_storage_error));
            bundle.putCharSequence("message", restoreFromBackupActivity.getString(R.string.gdrive_message_restore_failed_media_card_not_found));
            bundle.putBoolean("cancelable", false);
            bundle.putString("positive_button", restoreFromBackupActivity.getString(R.string.ok));
            bundle.putString("negative_button", restoreFromBackupActivity.getString(R.string.learn_more));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            C004902f r1 = new C004902f(restoreFromBackupActivity.A0V());
            r1.A09(promptDialogFragment, null);
            r1.A02();
        }
    }

    public static /* synthetic */ void A0K(RestoreFromBackupActivity restoreFromBackupActivity) {
        Bundle bundle = new Bundle();
        bundle.putInt("dialog_id", 16);
        bundle.putCharSequence("message", restoreFromBackupActivity.getString(R.string.gdrive_one_time_setup_taking_too_long_message));
        bundle.putBoolean("cancelable", false);
        bundle.putString("positive_button", restoreFromBackupActivity.getString(R.string.ok));
        bundle.putString("negative_button", restoreFromBackupActivity.getString(R.string.skip));
        PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
        promptDialogFragment.A0U(bundle);
        if (!restoreFromBackupActivity.A33()) {
            C004902f r1 = new C004902f(restoreFromBackupActivity.A0V());
            r1.A09(promptDialogFragment, "one-time-setup-taking-too-long");
            r1.A02();
        }
    }

    public static /* synthetic */ void A0L(RestoreFromBackupActivity restoreFromBackupActivity) {
        Intent type = new Intent("android.intent.action.GET_CONTENT").setType("application/octet-stream");
        if (Build.VERSION.SDK_INT >= 26) {
            File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            if (!externalStoragePublicDirectory.exists()) {
                Log.e("restore-from-backup-activity/backup-file-picker/downloads-directory-does-not-exist");
                return;
            }
            type = type.putExtra("android.provider.extra.INITIAL_URI", Uri.fromFile(externalStoragePublicDirectory));
        }
        restoreFromBackupActivity.startActivityForResult(type, 9);
    }

    public static /* synthetic */ void A0M(RestoreFromBackupActivity restoreFromBackupActivity, AbstractC87984Du r7) {
        if (r7 instanceof C84263yg) {
            C84263yg r72 = (C84263yg) r7;
            int i = r72.A00;
            if (i != 0) {
                if (i == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("dialog_id", 24);
                    bundle.putString("title", restoreFromBackupActivity.getString(R.string.not_enough_storage));
                    bundle.putCharSequence("message", C44891zj.A02(((ActivityC13830kP) restoreFromBackupActivity).A01, R.plurals.import_backup_not_enough_space_message, restoreFromBackupActivity.A0E.A01, false));
                    bundle.putBoolean("cancelable", false);
                    bundle.putString("positive_button", restoreFromBackupActivity.getString(R.string.ok_short));
                    bundle.putString("negative_button", restoreFromBackupActivity.getString(R.string.permission_settings_open));
                    PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
                    promptDialogFragment.A0U(bundle);
                    promptDialogFragment.A1F(restoreFromBackupActivity.A0V(), null);
                } else if (i != 2) {
                    StringBuilder sb = new StringBuilder("restore-from-backup-activity/incorrect-backup-import-space-check-status/");
                    sb.append(i);
                    Log.e(sb.toString());
                }
            } else if (((AbstractActivityC28171Kz) restoreFromBackupActivity).A0D.A0F()) {
                Intent A0U = C14960mK.A0U(restoreFromBackupActivity, "action_backup_import");
                A0U.setData(r72.A01);
                AnonymousClass1Tv.A00(restoreFromBackupActivity.A0O.A00, A0U);
            }
        }
    }

    public static /* synthetic */ void A0N(RestoreFromBackupActivity restoreFromBackupActivity, String str) {
        Account[] accountArr;
        String obj;
        try {
            accountArr = AccountManager.get(restoreFromBackupActivity.A0O.A00).getAccountsByType("com.google");
        } catch (Exception e) {
            Log.e("gdrive-activity/get-google-accounts", e);
            accountArr = new Account[0];
        }
        for (Account account : accountArr) {
            if (TextUtils.equals(account.name, str)) {
                restoreFromBackupActivity.A36(str, 1);
                return;
            }
        }
        try {
            String string = AccountManager.get(restoreFromBackupActivity).addAccount("com.google", null, null, null, restoreFromBackupActivity, null, null).getResult().getString("authAccount");
            if (string == null) {
                obj = "gdrive-activity/error-during-msgstore-download/account-manager-returned-with-no-account-name";
            } else if (!string.equals(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("gdrive-activity/error-during-msgstore-download/account-manager user added ");
                sb.append(C44771zW.A0B(string));
                sb.append(" instead of ");
                sb.append(C44771zW.A0B(str));
                obj = sb.toString();
            } else {
                ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupActivity, 37));
                restoreFromBackupActivity.A36(str, 1);
                return;
            }
            Log.e(obj);
        } catch (AuthenticatorException | OperationCanceledException | IOException e2) {
            Log.e("gdrive-activity/error-during-msgstore-download", e2);
        }
    }

    public static /* synthetic */ void A0O(RestoreFromBackupActivity restoreFromBackupActivity, boolean z, boolean z2) {
        String str;
        String str2;
        C25661Ag r2 = restoreFromBackupActivity.A0a;
        if (z) {
            str = "restore_successful";
        } else {
            str = "restore_unsuccessful";
        }
        r2.A02(str, "next");
        if (!z2) {
            ((ActivityC13810kN) restoreFromBackupActivity).A09.A0V(0);
            ((ActivityC13810kN) restoreFromBackupActivity).A09.A0F();
            str2 = "gdrive-activity/msgstore-download-finish no media to restore, setting result of Google Drive activity to BACKUP_FOUND_AND_RESTORED.";
        } else if (((ActivityC13810kN) restoreFromBackupActivity).A07.A05(true) != 1) {
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 12);
            bundle.putCharSequence("message", restoreFromBackupActivity.getString(R.string.gdrive_messages_restore_succeeded_media_will_be_restored_on_wifi));
            bundle.putBoolean("cancelable", false);
            bundle.putString("positive_button", restoreFromBackupActivity.getString(R.string.ok));
            bundle.putString("negative_button", restoreFromBackupActivity.getString(R.string.gdrive_restore_now));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            promptDialogFragment.A1F(restoreFromBackupActivity.A0V(), null);
            return;
        } else {
            Log.i("gdrive-activity/msgstore-download-finish, Wi-Fi available, starting media restore.");
            Log.i("gdrive-activity/restore-media");
            AnonymousClass1Tv.A00(restoreFromBackupActivity, C14960mK.A0U(restoreFromBackupActivity, "action_restore_media"));
            str2 = "gdrive-activity/msgstore-download-finish setting result of Google Drive activity to BACKUP_FOUND_AND_RESTORED.";
        }
        Log.i(str2);
        restoreFromBackupActivity.setResult(3);
        restoreFromBackupActivity.finish();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0g) {
            this.A0g = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC28171Kz) this).A05 = (C20650w6) r1.A3A.get();
            ((AbstractActivityC28171Kz) this).A09 = (C18470sV) r1.AK8.get();
            ((AbstractActivityC28171Kz) this).A02 = (C20670w8) r1.AMw.get();
            ((AbstractActivityC28171Kz) this).A03 = (C15550nR) r1.A45.get();
            ((AbstractActivityC28171Kz) this).A0B = (C19890uq) r1.ABw.get();
            ((AbstractActivityC28171Kz) this).A0A = (C20710wC) r1.A8m.get();
            ((AbstractActivityC28171Kz) this).A0E = (AbstractC15850o0) r1.ANA.get();
            ((AbstractActivityC28171Kz) this).A04 = (C17050qB) r1.ABL.get();
            ((AbstractActivityC28171Kz) this).A0C = (C20740wF) r1.AIA.get();
            ((AbstractActivityC28171Kz) this).A0D = (C18350sJ) r1.AHX.get();
            ((AbstractActivityC28171Kz) this).A06 = (C20820wN) r1.ACG.get();
            ((AbstractActivityC28171Kz) this).A08 = (C26041Bu) r1.ACL.get();
            ((AbstractActivityC28171Kz) this).A00 = (AnonymousClass2FR) r2.A0P.get();
            ((AbstractActivityC28171Kz) this).A07 = (C20850wQ) r1.ACI.get();
            this.A0O = (C16590pI) r1.AMg.get();
            this.A0A = (C21740xu) r1.AM1.get();
            this.A0d = (C19930uu) r1.AM5.get();
            this.A0W = (C16120oU) r1.ANE.get();
            this.A0b = (AnonymousClass12U) r1.AJd.get();
            this.A09 = (C18790t3) r1.AJw.get();
            this.A0a = (C25661Ag) r1.A8G.get();
            this.A0c = (C252018m) r1.A7g.get();
            this.A0R = (C15650ng) r1.A4m.get();
            this.A0B = (C15820nx) r1.A6U.get();
            this.A0Q = (C19350ty) r1.A4p.get();
            this.A0U = (C21630xj) r1.ACc.get();
            this.A0L = (AnonymousClass1D6) r1.A1G.get();
            this.A0E = (C25721Am) r1.A1B.get();
            this.A0N = (AnonymousClass116) r1.A3z.get();
            this.A0V = (C25771At) r1.A7p.get();
            this.A0H = (C26551Dx) r1.A8Z.get();
            this.A0T = (C16490p7) r1.ACJ.get();
            this.A0K = (C27051Fv) r1.AHd.get();
            this.A0P = (C15890o4) r1.AN1.get();
            this.A0Z = (AnonymousClass10O) r1.AMM.get();
            this.A0C = (C25701Ak) r1.A1F.get();
            this.A0X = (C16630pM) r1.AIc.get();
            this.A0G = (C22730zY) r1.A8Y.get();
            this.A0I = (AnonymousClass10J) r1.A8b.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x019c, code lost:
        if (r0 == false) goto L_0x019e;
     */
    @Override // X.AbstractActivityC28171Kz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2f(X.C29851Uy r12) {
        /*
        // Method dump skipped, instructions count: 750
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.RestoreFromBackupActivity.A2f(X.1Uy):void");
    }

    public final Spannable A2h(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(str2, new C58272oQ(this, ((ActivityC13790kL) this).A00, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A08, ((ActivityC13790kL) this).A02.A00("https://faq.whatsapp.com/android/chats/how-to-restore-your-chat-history").toString()));
        return C42971wC.A01(str, hashMap);
    }

    public void A2i() {
        AnonymousClass009.A01();
        Log.i("restore-from-backup-activity/show-importing-view");
        ((TextView) findViewById(R.id.title_toolbar_text)).setText("");
        ((TextView) findViewById(R.id.activity_gdrive_backup_found_category)).setText(R.string.activity_google_drive_importing_title);
        findViewById(R.id.restore_actions_view).setVisibility(8);
        findViewById(R.id.restore_general_info).setVisibility(8);
        findViewById(R.id.import_general_info).setVisibility(8);
        findViewById(R.id.gdrive_restore_info).setVisibility(8);
        findViewById(R.id.calculating_progress_view).setVisibility(8);
        findViewById(R.id.google_drive_backup_error_info_view).setVisibility(8);
        findViewById(R.id.google_drive_restore_animation_view).setVisibility(0);
        this.A05.setVisibility(0);
        this.A05.setIndeterminate(true);
        C88134Ek.A00(this.A05, AnonymousClass00T.A00(this, R.color.media_message_progress_determinate));
        this.A08.setVisibility(0);
        this.A08.setText(getString(R.string.import_backup_progress));
        View findViewById = findViewById(R.id.restore_actions_view);
        AnonymousClass009.A03(findViewById);
        findViewById.setVisibility(8);
    }

    public void A2j() {
        AnonymousClass009.A01();
        Log.i("gdrive-activity/show-msgstore-downloading-view");
        AnonymousClass00T.A05(this, R.id.restore_actions_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.restore_general_info).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.import_general_info).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.calculating_progress_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_animation_view).setVisibility(0);
        this.A05.setVisibility(0);
        this.A05.setIndeterminate(true);
        C88134Ek.A00(this.A05, AnonymousClass00T.A00(this, R.color.media_message_progress_determinate));
        this.A08.setVisibility(0);
        this.A07 = (TextView) findViewById(R.id.google_drive_media_will_be_downloaded_later_notice);
        long j = this.A02;
        if (j == 0) {
            j = ((ActivityC13810kN) this).A09.A00.getLong("gdrive_approx_media_download_size", 0);
            this.A02 = j;
        }
        if (j > 0) {
            this.A07.setText(getString(R.string.activity_gdrive_media_will_be_downloaded_later_notice, C44891zj.A03(((ActivityC13830kP) this).A01, j)));
            this.A07.setVisibility(0);
        }
    }

    public void A2k() {
        Log.i("gdrive-activity/show-restore-for-no-gdrive-local-backup");
        AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(0);
        this.A0l.open();
        ((TextView) AnonymousClass00T.A05(this, R.id.title_toolbar_text)).setText("");
        AnonymousClass00T.A05(this, R.id.calculating_progress_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.gdrive_restore_size_info).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.calculating_transfer_size_progress_bar).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.gdrive_restore_info).setVisibility(8);
        ((TextView) AnonymousClass00T.A05(this, R.id.activity_gdrive_backup_found_category)).setText(R.string.activity_google_drive_import_title);
        ((TextView) AnonymousClass00T.A05(this, R.id.restore_general_info)).setText(R.string.import_general_info);
        AnonymousClass00T.A05(this, R.id.dont_restore).setOnClickListener(new ViewOnClickCListenerShape0S0000000_I0(0));
        this.A04.setText(R.string.import_button_text);
        this.A04.setAllCaps(true);
        this.A04.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 25));
    }

    public void A2l() {
        boolean z;
        Log.i("gdrive-activity/show-restore-for-local-backup");
        AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(0);
        this.A0l.open();
        setTitle(R.string.activity_google_drive_restore_title);
        AnonymousClass00T.A05(this, R.id.calculating_progress_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.gdrive_restore_size_info).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.calculating_transfer_size_progress_bar).setVisibility(8);
        ((TextView) AnonymousClass00T.A05(this, R.id.gdrive_restore_info)).setText(getString(R.string.local_restore_info_calculating, C38131nZ.A01(((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A07.A06()).toString()));
        this.A0M.A01.A05(this, new AnonymousClass02B() { // from class: X.3Pf
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                RestoreFromBackupActivity restoreFromBackupActivity = RestoreFromBackupActivity.this;
                long A0G = C12980iv.A0G(obj);
                String charSequence = C38131nZ.A01(((ActivityC13830kP) restoreFromBackupActivity).A01, ((ActivityC13790kL) restoreFromBackupActivity).A07.A06()).toString();
                String A03 = C44891zj.A03(((ActivityC13830kP) restoreFromBackupActivity).A01, A0G);
                TextView A0N = C12990iw.A0N(restoreFromBackupActivity, R.id.gdrive_restore_info);
                Object[] objArr = new Object[3];
                C12990iw.A1P(null, charSequence, objArr);
                A0N.setText(C12960it.A0X(restoreFromBackupActivity, A03, objArr, 2, R.string.local_restore_info));
            }
        });
        RestoreFromBackupViewModel restoreFromBackupViewModel = this.A0M;
        restoreFromBackupViewModel.A08.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupViewModel, 49));
        boolean z2 = false;
        try {
            File A09 = ((ActivityC13790kL) this).A07.A09();
            if (A09 != null) {
                if (C32811cm.A00(A09.getName()) == EnumC16570pG.A07) {
                    z2 = true;
                }
            }
            z = z2;
        } catch (IOException unused) {
            Log.e("Cannot determine whether local backup is encrypted");
            z = false;
        }
        A32(z2);
        if (this.A0J == null) {
            this.A0J = (GoogleDriveRestoreAnimationView) findViewById(R.id.google_drive_restore_animation_view);
        }
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.restore_general_info);
        boolean A00 = C14950mJ.A00();
        int i = R.string.shared_internal_storage_restore_general_info;
        if (A00) {
            i = R.string.sdcard_restore_general_info;
        }
        textView.setText(i);
        this.A0E.A0F.A07(1729);
        AnonymousClass00T.A05(this, R.id.dont_restore).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 26));
        this.A04.setOnClickListener(new ViewOnClickCListenerShape0S0110000_I0(this, 0, z));
        this.A0a.A00("backup_found");
    }

    public void A2m() {
        String A0B;
        StringBuilder sb = new StringBuilder("gdrive-activity/skip-restore user declined to restore backup from ");
        AnonymousClass29N r0 = this.A0F;
        if (r0 == null) {
            A0B = "<unset account>";
        } else {
            A0B = C44771zW.A0B(r0.A05);
        }
        sb.append(A0B);
        Log.i(sb.toString());
        Log.i("gdrive-activity/skip-restore/stopping-approx-transfer-size-calc-thread");
        AnonymousClass29N r02 = this.A0F;
        if (r02 != null && r02.A02) {
            A2q(6);
        }
        this.A0t.set(true);
        ((ActivityC13810kN) this).A09.A0V(0);
        ((ActivityC13810kN) this).A09.A0F();
        ((ActivityC13810kN) this).A09.A1H(0);
        ((ActivityC13810kN) this).A09.A11(false);
        RunnableBRunnable0Shape2S0100000_I0_2 runnableBRunnable0Shape2S0100000_I0_2 = new RunnableBRunnable0Shape2S0100000_I0_2(this, 33);
        if (AnonymousClass01I.A01()) {
            ((ActivityC13830kP) this).A05.Ab2(runnableBRunnable0Shape2S0100000_I0_2);
        } else {
            runnableBRunnable0Shape2S0100000_I0_2.run();
        }
        this.A0H.A05(10);
        String A09 = ((ActivityC13810kN) this).A09.A09();
        if (A09 != null) {
            Intent A0U = C14960mK.A0U(this, "action_remove_backup_info");
            A0U.putExtra("account_name", A09);
            A0U.putExtra("remove_account_name", true);
            AnonymousClass1Tv.A00(this, A0U);
        }
        setResult(2);
        A2o();
    }

    public final void A2n() {
        Log.i("gdrive-activity/set-skip-restore/");
        this.A0i = true;
        setResult(2);
    }

    public final void A2o() {
        Log.i("gdrive-activity/show-new-user-settings");
        A2n();
        A2g(false);
        ((ActivityC13810kN) this).A09.A0Y(System.currentTimeMillis() + 604800000);
    }

    public final void A2p() {
        Log.i("gdrive-activity/restore-messages");
        AnonymousClass29N r0 = this.A0F;
        if (r0 == null || !r0.A01) {
            AnonymousClass1Tv.A00(this, C14960mK.A0U(this, "action_restore"));
        } else {
            this.A0n.ASl(true);
        }
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(this, 31));
    }

    public final void A2q(int i) {
        AnonymousClass432 r1 = new AnonymousClass432();
        r1.A00 = Integer.valueOf(i);
        this.A0W.A07(r1);
    }

    public final void A2r(int i) {
        RequestPermissionActivity.A0Q(this, this.A0N, this.A0P, "google_backup", new int[]{R.drawable.ic_baseline_cloud_upload_48}, i, R.string.gdrive_backup_restore_permission_dialog_title, R.string.gdrive_backup_restore_permission_dialog_message, R.string.cancel);
    }

    public final void A2s(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("dialog_id", i);
        bundle.putCharSequence("message", getString(R.string.dont_restore_message));
        bundle.putBoolean("cancelable", true);
        bundle.putString("positive_button", getString(R.string.msg_store_do_not_restore));
        bundle.putString("negative_button", getString(R.string.cancel));
        PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
        promptDialogFragment.A0U(bundle);
        if (!A33()) {
            C004902f r1 = new C004902f(A0V());
            r1.A09(promptDialogFragment, null);
            r1.A02();
        }
    }

    public void A2t(long j, long j2) {
        String string;
        AnonymousClass009.A00();
        this.A01 = j;
        this.A02 = j2;
        ((ActivityC13810kN) this).A09.A00.edit().putLong("gdrive_approx_media_download_size", j2).apply();
        StringBuilder sb = new StringBuilder("washaredpreferences/save-gdrive-media-download-transfer-size/");
        sb.append(j2);
        Log.i(sb.toString());
        if (j <= 0) {
            string = getString(R.string.gdrive_backup_size_info_with_nothing_to_download);
        } else {
            string = getString(R.string.gdrive_backup_size_info, C44891zj.A03(((ActivityC13830kP) this).A01, j));
        }
        this.A0l.block();
        StringBuilder sb2 = new StringBuilder("gdrive-activity/update-restore-info/ total download size: ");
        sb2.append(j);
        sb2.append(" media download size: ");
        sb2.append(j2);
        Log.i(sb2.toString());
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape0S1100000_I0(6, string, this));
    }

    public void A2u(AnonymousClass29N r6) {
        AnonymousClass009.A01();
        AnonymousClass01E A0A = A0V().A0A("one-time-setup-taking-too-long");
        if (A0A != null) {
            ((DialogFragment) A0A).A1C();
        }
        if (A33()) {
            StringBuilder sb = new StringBuilder("gdrive-activity/one-time-setup background task finished but parent activity has already exited, therefore, stopping the task. Data: ");
            sb.append(r6);
            Log.i(sb.toString());
            return;
        }
        this.A0F = r6;
        if (r6 != null) {
            A2w(null, 22);
            A2v(r6);
            return;
        }
        StringBuilder sb2 = new StringBuilder("gdrive-activity/one-time-setup/num-of-local-backup-files/");
        sb2.append(((ActivityC13790kL) this).A07.A04());
        Log.i(sb2.toString());
        if (((ActivityC13790kL) this).A07.A04() > 0 && this.A0w.get()) {
            Log.i("gdrive-activity/one-time-setup no google drive backups found but local backup exists.");
            ((ActivityC13790kL) this).A07.A00 = 3;
            A2w(null, 23);
            A2l();
        } else if (C44771zW.A0J(((ActivityC13810kN) this).A09)) {
            A2o();
            setResult(1);
        } else {
            Log.i("gdrive-activity/one-time-setup user is an existing user but has no google drive backups found and no local backups exist either, warn the user.");
            ((ActivityC13790kL) this).A07.A00 = 4;
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 14);
            bundle.putCharSequence("message", getString(R.string.gdrive_no_google_account_found_message));
            bundle.putBoolean("cancelable", false);
            bundle.putString("positive_button", getString(R.string.gdrive_give_permission_button_label));
            bundle.putString("negative_button", getString(R.string.skip));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            if (!A33()) {
                C004902f r0 = new C004902f(A0V());
                r0.A09(promptDialogFragment, null);
                r0.A02();
            }
        }
    }

    public final void A2v(AnonymousClass29N r12) {
        int i;
        long j;
        StringBuilder sb = new StringBuilder("gdrive-activity/show-restore-for-gdrive-backup/");
        String str = r12.A05;
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        long j2 = r12.A04;
        long j3 = r12.A00;
        AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(0);
        this.A0l.open();
        setTitle(R.string.activity_google_drive_restore_title);
        if (this.A0J == null) {
            this.A0J = (GoogleDriveRestoreAnimationView) findViewById(R.id.google_drive_restore_animation_view);
        }
        boolean z = r12.A01;
        TextView textView = (TextView) AnonymousClass00T.A05(this, R.id.restore_general_info);
        if (z) {
            boolean A00 = C14950mJ.A00();
            i = R.string.shared_internal_storage_restore_general_info;
            if (A00) {
                i = R.string.sdcard_restore_general_info;
            }
        } else {
            i = R.string.gdrive_restore_general_info;
        }
        textView.setText(i);
        this.A0E.A0F.A07(1729);
        StringBuilder sb2 = new StringBuilder(getString(R.string.gdrive_backup_last_modified_date_unavailable));
        StringBuilder sb3 = new StringBuilder();
        if (j2 > 0) {
            sb2.setLength(0);
            sb2.append(C38131nZ.A01(((ActivityC13830kP) this).A01, j2));
        }
        if (r12.A01) {
            j = 0;
        } else {
            j = j3;
        }
        this.A01 = j;
        if (j3 >= 0) {
            sb3.setLength(0);
            sb3.append(C44891zj.A03(((ActivityC13830kP) this).A01, j3));
        }
        if (!r12.A03) {
            sb2.setLength(0);
            sb2.append(C38131nZ.A01(((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A07.A06()));
            Log.i("gdrive-activity/show-restore-for-gdrive-backup/local message backup will be used. showing local backup timestamp");
        }
        TextView textView2 = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_restore_info);
        boolean z2 = r12.A01;
        int i2 = R.string.gdrive_restore_info;
        if (z2) {
            i2 = R.string.local_restore_info;
        }
        textView2.setText(getString(i2, str, sb2.toString(), sb3.toString()));
        A32(r12.A02);
        AnonymousClass00T.A05(this, R.id.dont_restore).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 23));
        this.A04.setOnClickListener(new ViewOnClickCListenerShape0S1200000_I0(this, r12, str, 1));
        this.A0a.A00("backup_found");
    }

    public void A2w(C29851Uy r5, int i) {
        Integer num;
        this.A0M.A00 = i;
        this.A0S = r5;
        StringBuilder sb = new StringBuilder("gdrive-activity/state ");
        sb.append(A02(i));
        sb.append(" ");
        sb.append(r5);
        Log.i(sb.toString());
        C14820m6 r2 = ((ActivityC13810kN) this).A09;
        int i2 = this.A0M.A00;
        C29851Uy r0 = this.A0S;
        if (r0 != null) {
            num = Integer.valueOf(r0.A00);
        } else {
            num = null;
        }
        SharedPreferences.Editor edit = r2.A00.edit();
        edit.putInt("gdrive_activity_state", i2);
        if (num != null) {
            edit.putInt("gdrive_activity_msgstore_init_key", num.intValue());
        } else {
            edit.remove("gdrive_activity_msgstore_init_key");
        }
        edit.apply();
    }

    public final void A2x(boolean z) {
        StringBuilder sb;
        String str;
        AnonymousClass29N r0;
        if (z && (r0 = this.A0F) != null && r0.A02) {
            A2q(8);
        }
        setTitle(R.string.activity_google_drive_restore_title);
        if (((ActivityC13810kN) this).A09.A00.getBoolean("gdrive_restore_overwrite_local_files", false)) {
            sb = new StringBuilder();
            str = "gdrive-activity/msgstore-download/finished, success: ";
        } else {
            sb = new StringBuilder();
            str = "gdrive-activity/msgstore-download/not performed since we are using local, success: ";
        }
        sb.append(str);
        sb.append(z);
        sb.append(", now, restoring it.");
        Log.i(sb.toString());
        super.A2g(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
        if (r37.A0N.A00() == false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2y(boolean r38) {
        /*
        // Method dump skipped, instructions count: 231
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.RestoreFromBackupActivity.A2y(boolean):void");
    }

    public final void A2z(boolean z) {
        if (!this.A0P.A07()) {
            if (z) {
                A2r(6);
                return;
            }
        } else if (((ActivityC13790kL) this).A07.A04() > 0) {
            ((ActivityC13790kL) this).A07.A00 = 3;
            A2w(null, 23);
            A2l();
            return;
        }
        ((ActivityC13790kL) this).A07.A00 = 4;
        A2n();
        A2g(false);
    }

    public final void A30(boolean z) {
        AnonymousClass009.A01();
        AnonymousClass00T.A05(this, R.id.restore_actions_view).setVisibility(0);
        AnonymousClass00T.A05(this, R.id.restore_general_info).setVisibility(0);
        AnonymousClass00T.A05(this, R.id.calculating_progress_view).setVisibility(0);
        AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(0);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_animation_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_progress).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_progress_info).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.google_drive_media_will_be_downloaded_later_notice).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.msgrestore_result_box).setVisibility(8);
        AnonymousClass00T.A05(this, R.id.nextBtn).setVisibility(8);
        File databasePath = getDatabasePath("msgstore.db");
        if (databasePath.exists()) {
            boolean delete = databasePath.delete();
            StringBuilder sb = new StringBuilder();
            sb.append("gdrive-activity/show-msgstore-downloading-view/restore-failed ");
            if (!delete) {
                sb.append(databasePath);
                sb.append(" exists but cannot be deleted, message restore might fail");
                Log.w(sb.toString());
            } else {
                sb.append(databasePath);
                sb.append(" deleted");
                Log.i(sb.toString());
            }
        }
        ((AbstractActivityC28171Kz) this).A08.A00();
        A2y(!z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r0.A01 != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A31(boolean r11) {
        /*
            r10 = this;
            X.1Ag r1 = r10.A0a
            if (r11 == 0) goto L_0x00de
            java.lang.String r0 = "restore_successful"
        L_0x0006:
            r1.A00(r0)
            X.AnonymousClass009.A01()
            X.29N r0 = r10.A0F
            r7 = 1
            r6 = 0
            if (r0 == 0) goto L_0x0017
            boolean r0 = r0.A01
            r5 = 1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r5 = 0
        L_0x0018:
            com.whatsapp.backup.google.GoogleDriveRestoreAnimationView r0 = r10.A0J
            if (r0 != 0) goto L_0x0027
            r0 = 2131363738(0x7f0a079a, float:1.8347293E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r10, r0)
            com.whatsapp.backup.google.GoogleDriveRestoreAnimationView r0 = (com.whatsapp.backup.google.GoogleDriveRestoreAnimationView) r0
            r10.A0J = r0
        L_0x0027:
            r0.A04(r6)
            r0 = 2131365569(0x7f0a0ec1, float:1.8351007E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r10, r0)
            r1 = 8
            r0.setVisibility(r1)
            android.widget.ProgressBar r0 = r10.A05
            r0.setVisibility(r1)
            android.widget.TextView r0 = r10.A08
            r0.setVisibility(r1)
            android.widget.TextView r0 = r10.A07
            if (r0 != 0) goto L_0x004f
            r0 = 2131363734(0x7f0a0796, float:1.8347285E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r10, r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.A07 = r0
        L_0x004f:
            r0.setVisibility(r1)
            r0 = 2131364525(0x7f0a0aad, float:1.834889E38)
            android.view.View r4 = X.AnonymousClass00T.A05(r10, r0)
            android.widget.TextView r4 = (android.widget.TextView) r4
            r4.setVisibility(r6)
            if (r5 == 0) goto L_0x00bf
            X.0m6 r1 = r10.A09
            r0 = 2
            r1.A0V(r0)
            X.018 r9 = r10.A01
            r8 = 2131755131(0x7f10007b, float:1.9141133E38)
            X.0p7 r0 = r10.A0T
            int r0 = r0.A00()
            long r0 = (long) r0
            java.lang.Object[] r3 = new java.lang.Object[r7]
            X.0p7 r2 = r10.A0T
            int r2 = r2.A00()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r3[r6] = r2
            java.lang.String r2 = r9.A0I(r3, r8, r0)
        L_0x0084:
            java.lang.String r1 = "gdrive-activity/after-msgstore-verified/ "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            r4.setText(r2)
            r0 = 2131364595(0x7f0a0af3, float:1.8349032E38)
            android.view.View r1 = X.AnonymousClass00T.A05(r10, r0)
            r1.setVisibility(r6)
            X.01d r0 = r10.A08
            android.view.accessibility.AccessibilityManager r0 = r0.A0P()
            if (r0 == 0) goto L_0x00b6
            boolean r0 = r0.isTouchExplorationEnabled()
            if (r0 == 0) goto L_0x00b6
            r1.setFocusableInTouchMode(r7)
            r1.requestFocus()
        L_0x00b6:
            X.3lb r0 = new X.3lb
            r0.<init>(r11, r5)
            r1.setOnClickListener(r0)
            return
        L_0x00bf:
            X.018 r9 = r10.A01
            r8 = 2131755132(0x7f10007c, float:1.9141135E38)
            X.0p7 r0 = r10.A0T
            int r0 = r0.A00()
            long r1 = (long) r0
            java.lang.Object[] r3 = new java.lang.Object[r7]
            X.0p7 r0 = r10.A0T
            int r0 = r0.A00()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3[r6] = r0
            java.lang.String r2 = r9.A0I(r3, r8, r1)
            goto L_0x0084
        L_0x00de:
            java.lang.String r0 = "restore_unsuccessful"
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.RestoreFromBackupActivity.A31(boolean):void");
    }

    public final void A32(boolean z) {
        View view = this.A03;
        if (z) {
            view.setVisibility(0);
            A2q(1);
            return;
        }
        view.setVisibility(8);
    }

    public final boolean A33() {
        return C36021jC.A03(this) || this.A0f;
    }

    public final boolean A34() {
        return this.A0s.size() < this.A0r.size() || this.A0w.get();
    }

    public final boolean A35() {
        if (this.A0P.A07() || !RequestPermissionActivity.A0Y(((ActivityC13810kN) this).A09, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"})) {
            return false;
        }
        A2r(8);
        return true;
    }

    public final boolean A36(String str, int i) {
        AnonymousClass009.A00();
        StringBuilder sb = new StringBuilder("gdrive-activity/auth-request account being used is ");
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1101000_I0(this, str, i, 0));
        Log.i("gdrive-activity/auth-request blocking on tokenReceived");
        this.A0m.block(100000);
        return this.A0e != null;
    }

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
        String str;
        String str2;
        if (i == 10 || i == 11) {
            StringBuilder sb = new StringBuilder("gdrive-activity/dialog-negative-click/dialog-id/");
            sb.append(i);
            Log.i(sb.toString());
        } else if (i == 12) {
            Log.i("gdrive-activity/restore-media-on-cellular-dialog Wi-Fi unavailable and user agreed to restore media on cellular.");
            ((ActivityC13810kN) this).A09.A00.edit().putString("gdrive_media_restore_network_setting", String.valueOf(1)).apply();
            Log.i("gdrive-activity/restore-media");
            AnonymousClass1Tv.A00(this, C14960mK.A0U(this, "action_restore_media"));
            Log.i("gdrive-activity/msgstore-download-finish setting result of Google Drive activity to BACKUP_FOUND_AND_RESTORED.");
            setResult(3);
            finish();
        } else if (i == 14) {
            this.A0E.A0F.A07(1729);
            Log.i("gdrive-activity/no-local-or-gdrive-backup-found-dialog no google drive backups found and user is not interested in adding an account for that either.");
            A2o();
            setResult(1);
        } else if (i == 16) {
            Log.i("gdrive-activity/one-time-setup-is-taking-too-long/user-decided-to-cancel");
            C44921zm.A01();
            this.A0u.set(true);
            if (((ActivityC13790kL) this).A07.A04() > 0) {
                A2w(null, 23);
                A2l();
                return;
            }
            A2n();
            A2g(false);
        } else {
            if (i == 18) {
                Log.i("gdrive-activity/failed-to-restore-messages-from-selected-backup/user-decided-to-continue");
                A2n();
            } else {
                if (i == 19) {
                    str2 = "gdrive-activity/failed-to-restore-messages/internal-storage-out-of-free-space/user-clicked-ok";
                } else if (i == 20) {
                    if (A34()) {
                        str2 = "gdrive-activity/msgstore-jid-mismatch/restore-from-older";
                    } else {
                        Log.i("gdrive-activity/msgstore-jid-mismatch/skip");
                    }
                } else if (i == 22) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://faq.whatsapp.com/android/chats/how-to-restore-your-chat-history")));
                    return;
                } else if (i == 23) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://faq.whatsapp.com/android/chats/how-to-restore-your-chat-history")));
                    A30(true);
                    return;
                } else if (i == 24) {
                    boolean z = false;
                    if (Build.VERSION.SDK_INT >= 26) {
                        z = true;
                    }
                    if (z) {
                        str = "android.os.storage.action.MANAGE_STORAGE";
                    } else {
                        str = "android.settings.INTERNAL_STORAGE_SETTINGS";
                    }
                    startActivityForResult(new Intent(str), 7);
                    return;
                } else {
                    StringBuilder sb2 = new StringBuilder("unexpected dialog box: ");
                    sb2.append(i);
                    throw new IllegalStateException(sb2.toString());
                }
                Log.i(str2);
                A30(true);
                return;
            }
            A2g(false);
            A31(false);
        }
    }

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
        if (i == 13) {
            Log.i("gdrive-activity/insufficient-space-dialog/neutral-click");
            return;
        }
        StringBuilder sb = new StringBuilder("unexpected dialog box: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        String str;
        if (i == 10) {
            Log.i("gdrive-activity/show-restore user declined to restore from local backup");
            setResult(2);
            A2o();
        } else if (i == 11) {
            Log.i("gdrive-activity/user-confirmed-skip-restore");
            if (((ActivityC13790kL) this).A07.A04() <= 0 || !this.A0w.get()) {
                A2m();
                return;
            }
            A2w(null, 23);
            A2l();
        } else {
            if (i == 12) {
                Log.i("gdrive-activity/restore-media-on-cellular-dialog, Wi-Fi unavailable and user declined to restore media on cellular.");
                setResult(3);
            } else {
                if (i == 13) {
                    Log.i("gdrive-activity/insufficient-storage-for-restore/user-decided-to-visit-storage-settings");
                } else if (i == 14) {
                    Log.i("gdrive-activity/one-time-setup no google drive backups found and user decided to add an account or give permission to an existing one.");
                    Account[] accountsByType = AccountManager.get(this).getAccountsByType("com.google");
                    int length = accountsByType.length;
                    int i2 = length + 1;
                    String[] strArr = new String[i2];
                    for (int i3 = 0; i3 < length; i3++) {
                        strArr[i3] = accountsByType[i3].name;
                    }
                    int i4 = i2 - 1;
                    strArr[i4] = getString(R.string.google_account_picker_add_account);
                    String[] strArr2 = new String[i2];
                    boolean[] zArr = new boolean[i2];
                    List list = this.A0r;
                    list.clear();
                    for (int i5 = 0; i5 < length; i5++) {
                        list.add(accountsByType[i5]);
                        if (this.A0s.contains(accountsByType[i5])) {
                            strArr2[i5] = getString(R.string.google_drive_no_backup_found);
                            zArr[i5] = false;
                        } else {
                            strArr2[i5] = null;
                            zArr[i5] = true;
                        }
                    }
                    zArr[i4] = true;
                    SingleChoiceListDialogFragment singleChoiceListDialogFragment = new SingleChoiceListDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("dialog_id", 17);
                    bundle.putString("title", getString(R.string.google_account_picker_title));
                    bundle.putStringArray("multi_line_list_items_key", strArr);
                    bundle.putStringArray("multi_line_list_item_values_key", strArr2);
                    bundle.putBooleanArray("list_item_enabled_key", zArr);
                    bundle.putString("disabled_item_toast_key", getString(R.string.gdrive_no_backup_found));
                    singleChoiceListDialogFragment.A0U(bundle);
                    if (!A33()) {
                        singleChoiceListDialogFragment.A1F(A0V(), null);
                        return;
                    }
                    return;
                } else if (i == 15) {
                    Log.i("gdrive-activity/google-play-services-is-broken/user-decided-to-skip");
                    setResult(1);
                } else if (i == 16) {
                    Log.i("gdrive-activity/one-time-setup-taking-too-long/user-decided-to-wait");
                    return;
                } else {
                    if (i == 18) {
                        Log.i("gdrive-activity/failed-to-restore-from-selected-backup/restore-from-older");
                    } else if (i != 19) {
                        if (i == 20) {
                            Log.i("gdrive-activity/msgstore-jid-mistmatch/user-decided-to-reregister");
                            C16630pM r1 = this.A0X;
                            Log.i("register/phone/clear-reg-preferences");
                            SharedPreferences.Editor edit = r1.A01(C18350sJ.A00(this, RegisterPhone.class)).edit();
                            edit.clear();
                            if (!edit.commit()) {
                                Log.w("register/phone/failed-to-clear-reg-preferences");
                            }
                            ((AbstractActivityC28171Kz) this).A0D.A09();
                            Intent A04 = C14960mK.A04(this);
                            A04.setFlags(268435456);
                            startActivity(A04);
                            this.A0Q.A04("RestoreFromBackupActivity");
                            return;
                        } else if (i == 21) {
                            Log.i("gdrive-activity/failed-to-restore-from-selected-backup/re-enter-encryption-key");
                            A30(false);
                            return;
                        } else if (i != 22) {
                            if (!(i == 23 || i == 24)) {
                                StringBuilder sb = new StringBuilder("unexpected dialog box: ");
                                sb.append(i);
                                throw new IllegalStateException(sb.toString());
                            }
                        } else {
                            return;
                        }
                    }
                    A30(true);
                    return;
                }
                boolean z = false;
                if (Build.VERSION.SDK_INT >= 26) {
                    z = true;
                }
                if (z) {
                    str = "android.os.storage.action.MANAGE_STORAGE";
                } else {
                    str = "android.settings.INTERNAL_STORAGE_SETTINGS";
                }
                startActivityForResult(new Intent(str), 7);
                return;
            }
            finish();
        }
    }

    @Override // X.AnonymousClass29I
    public void APF(int i) {
        if (i == 17) {
            Log.i("gdrive-activity/user-dismissed-account-selector-dialog-dismissed");
            A2y(true);
            return;
        }
        StringBuilder sb = new StringBuilder("Unexpected dialog id:");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass29I
    public void AW6(String[] strArr, int i, int i2) {
        if (i != 17) {
            StringBuilder sb = new StringBuilder("Unexpected dialogId: ");
            sb.append(i);
            sb.append(" index:");
            sb.append(i2);
            throw new IllegalStateException(sb.toString());
        } else if (strArr[i2].equals(getString(R.string.google_account_picker_add_account))) {
            C44771zW.A00.execute(new RunnableBRunnable0Shape0S0200000_I0(this, 44, AccountManager.get(this).addAccount("com.google", null, null, null, this, null, null)));
            Log.i("gdrive-activity/show-accounts/waiting-for-add-account-activity-to-return");
        } else {
            Intent intent = new Intent();
            intent.putExtra("authAccount", strArr[i2]);
            onActivityResult(3, -1, intent);
        }
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 5) {
            StringBuilder sb = new StringBuilder("gdrive-activity/request-permissions/result/");
            sb.append(i2);
            Log.i(sb.toString());
        } else if (i == 6) {
            StringBuilder sb2 = new StringBuilder("gdrive-activity/request-permissions-non-gps/result/");
            sb2.append(i2);
            Log.i(sb2.toString());
            A2z(false);
            return;
        } else if (i == 2) {
            StringBuilder sb3 = new StringBuilder("gdrive-activity/request-to-fix-google-play-services/result/");
            sb3.append(i2);
            Log.i(sb3.toString());
            A2y(false);
            return;
        } else if (i == 1) {
            if (i2 == -1) {
                AnonymousClass009.A05(intent);
                this.A0e = intent.getStringExtra("authtoken");
                this.A0m.open();
                ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 36));
                return;
            }
            return;
        } else if (i == 3) {
            if (i2 == -1) {
                AnonymousClass009.A05(intent);
                AnonymousClass009.A05(intent.getExtras());
                String string = intent.getExtras().getString("authAccount");
                C44771zW.A0B(string);
                if (string == null) {
                    Log.e("gdrive-activity/activity-result/account-picker-returned-null-account");
                    return;
                } else {
                    ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1100000_I0(10, string, this));
                    return;
                }
            } else {
                StringBuilder sb4 = new StringBuilder("gdrive-activity/activity-result/account-picker-request/");
                sb4.append(i2);
                Log.e(sb4.toString());
                A2n();
                A2g(false);
                return;
            }
        } else if (i == 4) {
            StringBuilder sb5 = new StringBuilder("gdrive-activity/activity-result/account-added-request/");
            sb5.append(i2);
            Log.i(sb5.toString());
            Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.backup.google.RestoreFromBackupActivity");
            className.setAction("action_show_restore_one_time_setup");
            startActivity(className);
            return;
        } else if (i == 0) {
            StringBuilder sb6 = new StringBuilder("gdrive-activity/activity-result/password-input-activity/");
            sb6.append(i2);
            Log.i(sb6.toString());
            if (i2 == -1) {
                A2q(7);
                if (this.A0M.A00 == 23) {
                    A2w(null, 27);
                    A2j();
                    A2x(true);
                    return;
                }
                AnonymousClass29N r6 = this.A0F;
                A2j();
                ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200100_I0(this, r6, r6.A05, 0, r6.A00));
                return;
            }
            return;
        } else if (i == 7) {
            A30(true);
            return;
        } else if (i == 8) {
            if (!this.A0P.A07()) {
                AnonymousClass009.A01();
                if (!A33()) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("dialog_id", 22);
                    bundle.putString("title", getString(R.string.gdrive_message_restore_failed_storage_error));
                    bundle.putCharSequence("message", getString(R.string.gdrive_message_restore_storage_permission_necessary));
                    bundle.putBoolean("cancelable", false);
                    bundle.putString("positive_button", getString(R.string.ok));
                    bundle.putString("negative_button", getString(R.string.learn_more));
                    PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
                    promptDialogFragment.A0U(bundle);
                    C004902f r1 = new C004902f(A0V());
                    r1.A09(promptDialogFragment, null);
                    r1.A02();
                    return;
                }
                return;
            }
        } else if (i != 9) {
            super.onActivityResult(i, i2, intent);
            return;
        } else if (i2 == -1) {
            AnonymousClass009.A01();
            Log.i("gdrive-activity/show-impor-preparing-view");
            ((TextView) findViewById(R.id.title_toolbar_text)).setText("");
            ((TextView) findViewById(R.id.activity_gdrive_backup_found_category)).setText(R.string.activity_google_drive_importing_title);
            findViewById(R.id.restore_actions_view).setVisibility(8);
            findViewById(R.id.restore_general_info).setVisibility(8);
            findViewById(R.id.import_general_info).setVisibility(8);
            findViewById(R.id.gdrive_restore_info).setVisibility(8);
            findViewById(R.id.calculating_progress_view).setVisibility(8);
            findViewById(R.id.google_drive_backup_error_info_view).setVisibility(8);
            findViewById(R.id.google_drive_restore_animation_view).setVisibility(0);
            this.A05.setVisibility(0);
            this.A05.setIndeterminate(true);
            C88134Ek.A00(this.A05, AnonymousClass00T.A00(this, R.color.media_message_progress_determinate));
            this.A08.setVisibility(0);
            this.A08.setText(R.string.import_backup_preparing_description);
            View findViewById = findViewById(R.id.restore_actions_view);
            AnonymousClass009.A03(findViewById);
            findViewById.setVisibility(8);
            if (intent != null && intent.getData() != null) {
                Uri data = intent.getData();
                RestoreFromBackupViewModel restoreFromBackupViewModel = this.A0M;
                C68113Ud r0 = new C68113Ud(data, restoreFromBackupViewModel);
                C25721Am r3 = restoreFromBackupViewModel.A04;
                C471229a r12 = r3.A03;
                r12.A03(r0);
                r3.A0H.Aaz(new C471329b(data, r12, r3, r3.A08), new Void[0]);
                return;
            }
            return;
        } else {
            return;
        }
        A2y(true);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass12P.A03(this);
    }

    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C29851Uy r0;
        Bundle bundle2;
        int i;
        super.onCreate(bundle);
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        this.A0Y = new AnonymousClass3HG(this.A09, ((ActivityC13830kP) this).A01, this.A0V, ((ActivityC13810kN) this).A0D, this.A0c, ((ActivityC13830kP) this).A05);
        this.A0M = (RestoreFromBackupViewModel) new AnonymousClass02A(this).A00(RestoreFromBackupViewModel.class);
        if (!this.A0G.A09()) {
            Log.i("gdrive-activity/no-google-drive-access-possible");
            setResult(0);
        } else {
            setContentView(R.layout.activity_google_drive_restore);
            Toolbar toolbar = (Toolbar) findViewById(R.id.title_toolbar);
            if (!ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey()) {
                A1e(toolbar);
                AbstractC005102i A1U = A1U();
                if (A1U != null) {
                    A1U.A0M(false);
                    A1U.A0P(false);
                }
            }
            setTitle(R.string.activity_google_drive_title);
            C88134Ek.A00((ProgressBar) findViewById(R.id.gdrive_looking_for_backup_progress_bar), AnonymousClass00T.A00(this, R.color.primary_light));
            C88134Ek.A00((ProgressBar) findViewById(R.id.calculating_transfer_size_progress_bar), AnonymousClass00T.A00(this, R.color.primary_light));
            this.A05 = (ProgressBar) AnonymousClass00T.A05(this, R.id.google_drive_progress);
            this.A08 = (TextView) AnonymousClass00T.A05(this, R.id.google_drive_progress_info);
            this.A03 = AnonymousClass00T.A05(this, R.id.gdrive_restore_encrypted_backup);
            this.A04 = (Button) AnonymousClass00T.A05(this, R.id.perform_restore);
            this.A06 = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_restore_info);
            this.A0M.A02.A05(this, new C66873Pg(this));
            this.A0M.A07.A05(this, new AnonymousClass02B() { // from class: X.4sI
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    RestoreFromBackupActivity.A0M(RestoreFromBackupActivity.this, (AbstractC87984Du) obj);
                }
            });
            this.A0h = getApplicationContext().bindService(C14960mK.A0U(getApplicationContext(), null), this.A0j, 1);
            if (bundle == null) {
                this.A0M.A00 = 21;
                this.A0S = null;
                ((ActivityC13810kN) this).A09.A00.edit().remove("gdrive_activity_state").remove("gdrive_activity_msgstore_init_key").apply();
            } else {
                SharedPreferences sharedPreferences = ((ActivityC13810kN) this).A09.A00;
                Pair pair = new Pair(Integer.valueOf(sharedPreferences.getInt("gdrive_activity_state", -1)), Integer.valueOf(sharedPreferences.getInt("gdrive_activity_msgstore_init_key", -1)));
                RestoreFromBackupViewModel restoreFromBackupViewModel = this.A0M;
                int intValue = ((Number) pair.first).intValue();
                if (intValue == -1) {
                    intValue = 21;
                }
                restoreFromBackupViewModel.A00 = intValue;
                if (intValue == 26) {
                    int intValue2 = ((Number) pair.second).intValue();
                    if (intValue2 == -1) {
                        r0 = new C29851Uy(0);
                    } else if (intValue2 <= 7) {
                        r0 = new C29851Uy(intValue2);
                    } else {
                        StringBuilder sb = new StringBuilder("Initialization state is not recognized. State = ");
                        sb.append(intValue2);
                        throw new IllegalArgumentException(sb.toString());
                    }
                } else {
                    r0 = null;
                }
                this.A0S = r0;
            }
            if (this.A0M.A00 == 24 && !this.A0G.A0f.get()) {
                Log.i("gdrive-activity/create/it looks like restoring from gdrive has been completed but we missed it, let's try again");
                A2w(this.A0S, 22);
            }
            if (bundle == null) {
                bundle2 = null;
            } else {
                bundle2 = bundle.getBundle("restore_account_data");
            }
            StringBuilder sb2 = new StringBuilder("gdrive-activity/create/state/");
            sb2.append(A02(this.A0M.A00));
            Log.i(sb2.toString());
            int i2 = this.A0M.A00;
            switch (i2) {
                case 21:
                    Intent intent = getIntent();
                    if (intent.getAction() == null) {
                        Log.e("gdrive-activity/create no action provided.");
                        break;
                    } else {
                        onNewIntent(intent);
                        return;
                    }
                case 22:
                    if (bundle2 != null) {
                        this.A0F = AnonymousClass29N.A00(bundle2);
                        A2w(null, 22);
                        A2v(this.A0F);
                        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 45, bundle));
                        return;
                    }
                    throw new IllegalStateException("restore_account_data cannot be null");
                case 23:
                    A2w(null, 23);
                    A2l();
                    return;
                case 24:
                    if (bundle2 != null) {
                        this.A0F = AnonymousClass29N.A00(bundle2);
                        A2w(null, 22);
                        A2v(this.A0F);
                        A2w(null, 24);
                        A2j();
                        if (!C44771zW.A0I(((ActivityC13810kN) this).A09)) {
                            Log.i("gdrive-activity/create/gdrive-msgstore-download-not-pending");
                            A2x(true);
                            return;
                        }
                        Log.i("gdrive-activity/create/gdrive-msgstore-download-pending");
                        return;
                    }
                    throw new IllegalStateException("restore_account_data cannot be null");
                case 25:
                    if (bundle2 != null) {
                        this.A0F = AnonymousClass29N.A00(bundle2);
                        return;
                    }
                    return;
                case 26:
                    C29851Uy r2 = this.A0S;
                    if (bundle2 != null) {
                        this.A0F = AnonymousClass29N.A00(bundle2);
                        A2w(null, 22);
                        A2v(this.A0F);
                    } else {
                        A2w(null, 23);
                        A2l();
                    }
                    A2j();
                    StringBuilder sb3 = new StringBuilder("gdrive-activity/create/msgstore-init-status/");
                    sb3.append(r2);
                    Log.i(sb3.toString());
                    A2f(r2);
                    return;
                case 27:
                    A2w(null, 23);
                    A2l();
                    A2j();
                    A2x(true);
                    i = 27;
                    break;
                case 28:
                    A2w(null, 28);
                    A2k();
                    return;
                case 29:
                    A2k();
                    A2i();
                    i = 29;
                    break;
                default:
                    StringBuilder sb4 = new StringBuilder("Unknown state: ");
                    sb4.append(i2);
                    throw new IllegalStateException(sb4.toString());
            }
            A2w(null, i);
            return;
        }
        finish();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0f = true;
        if (this.A0v.compareAndSet(true, false)) {
            AnonymousClass10J r0 = this.A0I;
            r0.A01.A04(this.A0n);
            C25701Ak r02 = this.A0C;
            r02.A00.A04(this.A0o);
        }
        if (this.A0h) {
            getApplicationContext().unbindService(this.A0j);
        }
        this.A0Y.A01();
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getAction() == null) {
            Log.e("gdrive-activity/new-intent action is null");
        } else if (!intent.getAction().equals("action_show_restore_one_time_setup")) {
            StringBuilder sb = new StringBuilder("gdrive-activity/new-intent unexpected action: ");
            sb.append(intent.getAction());
            Log.e(sb.toString());
            finish();
        } else {
            Dialog A02 = C44771zW.A02(this, new DialogInterface.OnCancelListener() { // from class: X.4ew
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    RestoreFromBackupActivity restoreFromBackupActivity = RestoreFromBackupActivity.this;
                    Log.i("gdrive-activity/create user declined to install Google Play Services.");
                    restoreFromBackupActivity.A2z(true);
                }
            }, AnonymousClass1UB.A00(this.A0O.A00), 2, false);
            if (A02 != null && !A33() && !C44771zW.A0J(((ActivityC13810kN) this).A09)) {
                Log.i("gdrive-activity/google-play-service-unavailable/existing-user");
                A02.show();
            } else if (((ActivityC13810kN) this).A09.A09() == null) {
                A2y(false);
            } else if (C44771zW.A0I(((ActivityC13810kN) this).A09)) {
                Log.i("gdrive-activity/create/continue-msgstore-download");
                AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(8);
                AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(0);
                A2j();
                String A09 = ((ActivityC13810kN) this).A09.A09();
                AnonymousClass009.A05(A09);
                long A08 = ((ActivityC13810kN) this).A09.A08(A09);
                long A07 = ((ActivityC13810kN) this).A09.A07(A09);
                String string = getString(R.string.gdrive_backup_last_modified_date_unavailable);
                if (A07 > 0) {
                    string = C38131nZ.A01(((ActivityC13830kP) this).A01, A07).toString();
                }
                if (!((ActivityC13810kN) this).A09.A00.getBoolean("gdrive_restore_overwrite_local_files", false)) {
                    string = C38131nZ.A01(((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A07.A06()).toString();
                }
                Object A03 = C44891zj.A03(((ActivityC13830kP) this).A01, A08);
                if (this.A0F == null) {
                    this.A0F = new AnonymousClass29N(A09, A07, A08, ((ActivityC13810kN) this).A09.A00.getBoolean("gdrive_restore_overwrite_local_files", false), false, ((ActivityC13810kN) this).A09.A00.getBoolean("gdrive_last_restore_file_is_encrypted", false));
                }
                ((TextView) AnonymousClass00T.A05(this, R.id.gdrive_restore_info)).setText(getString(R.string.gdrive_restore_info, A09, string, A03));
                A32(this.A0F.A02);
                A2p();
            } else if (((ActivityC13810kN) this).A09.A01() != 0) {
                Log.i("gdrive-activity/create/msgstore-download-already-finished, restoring");
                AnonymousClass00T.A05(this, R.id.google_drive_looking_for_backup_view).setVisibility(8);
                AnonymousClass00T.A05(this, R.id.google_drive_restore_view).setVisibility(0);
                AnonymousClass016 r1 = this.A0M.A02;
                r1.A05(this, new C66873Pg(this));
                Number number = (Number) r1.A01();
                if (number != null) {
                    this.A06.setText(SettingsChat.A09(this, ((ActivityC13830kP) this).A01, number.longValue()));
                }
                RestoreFromBackupViewModel restoreFromBackupViewModel = this.A0M;
                restoreFromBackupViewModel.A08.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupViewModel, 48));
                A32(((ActivityC13810kN) this).A09.A00.getBoolean("gdrive_last_restore_file_is_encrypted", false));
                A2j();
                A2x(true);
            } else {
                A2o();
                setResult(2);
            }
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        StringBuilder sb;
        String str;
        StringBuilder sb2;
        String str2;
        if (menuItem.getItemId() != 0) {
            return super.onOptionsItemSelected(menuItem);
        }
        String str3 = "one-time-restore";
        this.A0Z.A01(str3);
        AnonymousClass3HG r5 = this.A0Y;
        AnonymousClass10O r4 = this.A0Z;
        C29851Uy r0 = this.A0S;
        if (r0 != null) {
            int i = r0.A00;
            if (i == 3) {
                sb2 = new StringBuilder();
                sb2.append(str3);
                str2 = "-jid-mismatch";
            } else if (i == 4) {
                sb2 = new StringBuilder();
                sb2.append(str3);
                str2 = "-integrity-check-failed";
            }
            sb2.append(str2);
            str3 = sb2.toString();
        }
        int A00 = AnonymousClass1UB.A00(this.A0O.A00);
        if (A00 != 0) {
            if (A00 == 1) {
                sb = new StringBuilder();
                sb.append(str3);
                str = "-no-gs";
            } else if (A00 != 2) {
                sb = new StringBuilder();
                if (A00 != 3) {
                    sb.append(str3);
                    str = "-gs-invalid";
                } else {
                    sb.append(str3);
                    str = "-gs-disabled";
                }
            } else {
                sb = new StringBuilder();
                sb.append(str3);
                str = "-update-gs";
            }
            sb.append(str);
            str3 = sb.toString();
        }
        r5.A02(this, r4, str3);
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        AnonymousClass29N r2 = this.A0F;
        if (r2 != null) {
            StringBuilder sb = new StringBuilder("gdrive-activity/save-state/restore-account-data/ ");
            sb.append(r2);
            Log.i(sb.toString());
            bundle.putBundle("restore_account_data", this.A0F.A01());
        }
        StringBuilder sb2 = new StringBuilder("gdrive-activity/save-state/total-download-size/");
        sb2.append(this.A01);
        Log.i(sb2.toString());
        bundle.putLong("total_download_size", this.A01);
        StringBuilder sb3 = new StringBuilder("gdrive-activity/save-state/media-download-size/");
        sb3.append(this.A02);
        Log.i(sb3.toString());
        bundle.putLong("media_download_size", this.A02);
    }

    @Override // android.app.Activity
    public void setTitle(int i) {
        ((TextView) AnonymousClass00T.A05(this, R.id.title_toolbar_text)).setText(i);
    }
}
