package com.whatsapp.backup.google;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass2GE;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C73833gs;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class GoogleDriveRestoreAnimationView extends View implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public Resources A05;
    public Paint A06;
    public Drawable A07;
    public Drawable A08;
    public Drawable A09;
    public C73833gs A0A;
    public AnonymousClass018 A0B;
    public AnonymousClass2P7 A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public final DecelerateInterpolator A0G;

    public GoogleDriveRestoreAnimationView(Context context) {
        super(context);
        A00();
        this.A0G = new DecelerateInterpolator();
        this.A01 = 0;
        A03(context, null);
    }

    public GoogleDriveRestoreAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0G = new DecelerateInterpolator();
        this.A01 = 0;
        A03(context, attributeSet);
    }

    public GoogleDriveRestoreAnimationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0G = new DecelerateInterpolator();
        this.A01 = 0;
        A03(context, attributeSet);
    }

    public GoogleDriveRestoreAnimationView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0E) {
            this.A0E = true;
            this.A0B = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    public void A01() {
        if (this.A0A == null) {
            A02();
        }
        this.A01 = 1;
        startAnimation(this.A0A);
    }

    public final void A02() {
        C73833gs r2 = new C73833gs(this);
        this.A0A = r2;
        r2.setDuration(2000);
        this.A0A.setRepeatCount(-1);
        this.A0A.setInterpolator(new LinearInterpolator());
        this.A0A.setFillAfter(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0089, code lost:
        if (r0 != null) goto L_0x008b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(android.content.Context r7, android.util.AttributeSet r8) {
        /*
            r6 = this;
            r3 = 0
            if (r8 == 0) goto L_0x0069
            android.content.Context r0 = r6.getContext()
            android.content.res.Resources$Theme r1 = r0.getTheme()
            int[] r0 = X.AnonymousClass2QN.A0A
            android.content.res.TypedArray r2 = r1.obtainStyledAttributes(r8, r0, r3, r3)
            r5 = 0
            r4 = 1
            X.018 r0 = r6.A0B     // Catch: all -> 0x0061
            X.1Kv r0 = r0.A04()     // Catch: all -> 0x0061
            boolean r1 = r0.A06     // Catch: all -> 0x0061
            r0 = 0
            if (r1 == 0) goto L_0x001f
            r0 = 3
        L_0x001f:
            android.graphics.drawable.Drawable r0 = r2.getDrawable(r0)     // Catch: all -> 0x0061
            r6.A09 = r0     // Catch: all -> 0x0061
            X.018 r0 = r6.A0B     // Catch: all -> 0x0061
            X.1Kv r0 = r0.A04()     // Catch: all -> 0x0061
            boolean r0 = r0.A06     // Catch: all -> 0x0061
            if (r0 != 0) goto L_0x0030
            r5 = 3
        L_0x0030:
            android.graphics.drawable.Drawable r0 = r2.getDrawable(r5)     // Catch: all -> 0x0061
            r6.A07 = r0     // Catch: all -> 0x0061
            r0 = 2
            boolean r0 = r2.getBoolean(r0, r3)     // Catch: all -> 0x0061
            r6.A0F = r0     // Catch: all -> 0x0061
            X.018 r0 = r6.A0B     // Catch: all -> 0x0061
            X.1Kv r0 = r0.A04()     // Catch: all -> 0x0061
            boolean r1 = r0.A06     // Catch: all -> 0x0061
            r0 = 1
            if (r1 == 0) goto L_0x0049
            r0 = 4
        L_0x0049:
            int r0 = r2.getColor(r0, r3)     // Catch: all -> 0x0061
            r6.A03 = r0     // Catch: all -> 0x0061
            X.018 r0 = r6.A0B     // Catch: all -> 0x0061
            X.1Kv r0 = r0.A04()     // Catch: all -> 0x0061
            boolean r0 = r0.A06     // Catch: all -> 0x0061
            if (r0 != 0) goto L_0x005a
            r4 = 4
        L_0x005a:
            int r0 = r2.getColor(r4, r3)     // Catch: all -> 0x0061
            r6.A02 = r0     // Catch: all -> 0x0061
            goto L_0x0066
        L_0x0061:
            r0 = move-exception
            r2.recycle()
            throw r0
        L_0x0066:
            r2.recycle()
        L_0x0069:
            android.content.res.Resources r2 = r7.getResources()
            r6.A05 = r2
            android.graphics.drawable.Drawable r0 = r6.A09
            if (r0 != 0) goto L_0x008b
            X.018 r0 = r6.A0B
            X.1Kv r0 = r0.A04()
            boolean r1 = r0.A06
            r0 = 2131232272(0x7f080610, float:1.8080649E38)
            if (r1 == 0) goto L_0x0083
            r0 = 2131232271(0x7f08060f, float:1.8080647E38)
        L_0x0083:
            android.graphics.drawable.Drawable r0 = r2.getDrawable(r0)
            r6.A09 = r0
            if (r0 == 0) goto L_0x008f
        L_0x008b:
            int r3 = r0.getIntrinsicWidth()
        L_0x008f:
            r6.A04 = r3
            android.graphics.drawable.Drawable r0 = r6.A07
            if (r0 != 0) goto L_0x00ad
            android.content.res.Resources r2 = r6.A05
            X.018 r0 = r6.A0B
            X.1Kv r0 = r0.A04()
            boolean r1 = r0.A06
            r0 = 2131232271(0x7f08060f, float:1.8080647E38)
            if (r1 == 0) goto L_0x00a7
            r0 = 2131232272(0x7f080610, float:1.8080649E38)
        L_0x00a7:
            android.graphics.drawable.Drawable r0 = r2.getDrawable(r0)
            r6.A07 = r0
        L_0x00ad:
            android.graphics.drawable.Drawable r1 = r6.A09
            int r0 = r6.A03
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A04(r1, r0)
            r6.A09 = r0
            android.graphics.drawable.Drawable r1 = r6.A07
            int r0 = r6.A02
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A04(r1, r0)
            r6.A07 = r0
            android.graphics.Paint r0 = new android.graphics.Paint
            r0.<init>()
            r6.A06 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.GoogleDriveRestoreAnimationView.A03(android.content.Context, android.util.AttributeSet):void");
    }

    public void A04(boolean z) {
        int i;
        if (this.A0A == null) {
            A02();
        }
        clearAnimation();
        Resources resources = this.A05;
        if (z) {
            this.A08 = AnonymousClass2GE.A04(resources.getDrawable(R.drawable.ic_restore_error), AnonymousClass00T.A00(getContext(), R.color.googleDriveRestoreAnimationErrorIconTint));
            i = 3;
        } else {
            this.A09 = AnonymousClass2GE.A04(resources.getDrawable(R.drawable.ill_restore_anim), this.A03);
            this.A08 = AnonymousClass2GE.A04(this.A05.getDrawable(R.drawable.ill_restore_success_checkmark), AnonymousClass00T.A00(getContext(), R.color.googleDriveRestoreAnimationSuccessIconTint));
            i = 2;
        }
        this.A01 = i;
        C73833gs r2 = this.A0A;
        if (r2 != null) {
            r2.setDuration(800);
            startAnimation(this.A0A);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0C;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0C = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        C73833gs r2 = new C73833gs(this);
        this.A0A = r2;
        r2.setDuration(2000);
        this.A0A.setRepeatCount(-1);
        this.A0A.setInterpolator(new LinearInterpolator());
        this.A0A.setFillAfter(true);
        startAnimation(this.A0A);
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnimation();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0036, code lost:
        if (r0 != 3) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01da, code lost:
        if (r4 < (r2 - 0.5d)) goto L_0x01dc;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r25) {
        /*
        // Method dump skipped, instructions count: 652
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.GoogleDriveRestoreAnimationView.onDraw(android.graphics.Canvas):void");
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        C73833gs r0;
        if (getVisibility() != 0) {
            clearAnimation();
        } else if (getAnimation() == null && (r0 = this.A0A) != null) {
            startAnimation(r0);
        }
    }
}
