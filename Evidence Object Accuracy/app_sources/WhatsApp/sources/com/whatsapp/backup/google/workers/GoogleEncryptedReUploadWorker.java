package com.whatsapp.backup.google.workers;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass021;
import X.AnonymousClass022;
import X.AnonymousClass023;
import X.AnonymousClass042;
import X.AnonymousClass043;
import X.AnonymousClass0GK;
import X.AnonymousClass10N;
import X.AnonymousClass1D6;
import X.AnonymousClass317;
import X.AnonymousClass3DX;
import X.C003901s;
import X.C004101u;
import X.C004201x;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15890o4;
import X.C16120oU;
import X.C16240og;
import X.C16590pI;
import X.C17050qB;
import X.C17220qS;
import X.C18360sK;
import X.C18640sm;
import X.C18790t3;
import X.C19380u1;
import X.C19930uu;
import X.C21710xr;
import X.C22660zR;
import X.C22730zY;
import X.C27051Fv;
import X.C44801zZ;
import X.C44841zd;
import X.EnumC004001t;
import X.EnumC007503u;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class GoogleEncryptedReUploadWorker extends Worker {
    public AnonymousClass3DX A00;
    public boolean A01 = false;
    public final AbstractC15710nm A02;
    public final C14330lG A03;
    public final C15570nT A04;
    public final C18790t3 A05;
    public final C15820nx A06;
    public final C22730zY A07;
    public final AnonymousClass10N A08;
    public final C44801zZ A09;
    public final C27051Fv A0A;
    public final AnonymousClass1D6 A0B;
    public final C19380u1 A0C;
    public final C18640sm A0D;
    public final C15810nw A0E;
    public final C17050qB A0F;
    public final C14830m7 A0G;
    public final C16590pI A0H;
    public final C18360sK A0I;
    public final C15890o4 A0J;
    public final C14820m6 A0K;
    public final C14850m9 A0L;
    public final C16120oU A0M;
    public final AnonymousClass317 A0N = new AnonymousClass317();
    public final C17220qS A0O;
    public final C22660zR A0P;
    public final C19930uu A0Q;
    public final AbstractC14440lR A0R;
    public final C21710xr A0S;
    public final ArrayList A0T = new ArrayList();
    public final Random A0U;

    public GoogleEncryptedReUploadWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A0U = new Random();
        this.A0G = r1.Aen();
        this.A0L = r1.A3L();
        this.A0R = r1.Ag3();
        this.A0Q = (C19930uu) r1.AM5.get();
        this.A02 = r1.A7p();
        this.A04 = r1.A1w();
        this.A0H = (C16590pI) r1.AMg.get();
        this.A03 = (C14330lG) r1.A7B.get();
        this.A05 = (C18790t3) r1.AJw.get();
        this.A0M = r1.Ag5();
        this.A0E = (C15810nw) r1.A73.get();
        this.A0P = (C22660zR) r1.AAk.get();
        C17220qS A3P = r1.A3P();
        this.A0O = A3P;
        this.A0C = (C19380u1) r1.A1N.get();
        this.A0S = (C21710xr) r1.ANg.get();
        this.A06 = (C15820nx) r1.A6U.get();
        this.A0F = (C17050qB) r1.ABL.get();
        this.A0B = (AnonymousClass1D6) r1.A1G.get();
        this.A0A = (C27051Fv) r1.AHd.get();
        this.A0J = (C15890o4) r1.AN1.get();
        this.A0K = r1.Ag2();
        this.A08 = (AnonymousClass10N) r1.A8a.get();
        this.A0D = r1.A7Z();
        this.A0I = (C18360sK) r1.AN0.get();
        C22730zY r2 = (C22730zY) r1.A8Y.get();
        this.A07 = r2;
        this.A09 = new C44841zd((C16240og) r1.ANq.get(), r2, this, A3P);
    }

    public static AnonymousClass021 A00(C14820m6 r4, long j) {
        EnumC004001t r0;
        C003901s r2 = new C003901s();
        r2.A02 = true;
        if (r4.A02() == 0) {
            r0 = EnumC004001t.UNMETERED;
        } else {
            r0 = EnumC004001t.NOT_ROAMING;
        }
        r2.A01 = r0;
        C004101u r1 = new C004101u(r2);
        C004201x r42 = new C004201x(GoogleEncryptedReUploadWorker.class);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        r42.A02(j, timeUnit);
        r42.A00.A09 = r1;
        r42.A03(EnumC007503u.LINEAR, timeUnit, 900000);
        return (AnonymousClass021) r42.A00();
    }

    public static void A01(AnonymousClass023 r8, C14820m6 r9, C21710xr r10, Random random, boolean z) {
        long j;
        Calendar instance = Calendar.getInstance();
        if (!z) {
            int A01 = r9.A01();
            long currentTimeMillis = System.currentTimeMillis() - r9.A07(r9.A09());
            if (A01 == 1 || (A01 != 2 ? !(A01 != 3 || currentTimeMillis < 2419200000L) : currentTimeMillis >= 432000000)) {
                Log.i("google-encrypted-re-upload-worker/scheduleNextRun doesn't schedule run because google drive backup will run in the next backup slot");
                return;
            }
            long timeInMillis = instance.getTimeInMillis();
            if (instance.get(11) >= 2) {
                instance.add(5, 1);
            }
            instance.set(14, 0);
            instance.set(13, 0);
            instance.set(12, 0);
            instance.set(11, 2);
            instance.add(13, random.nextInt((int) TimeUnit.SECONDS.convert(4, TimeUnit.HOURS)));
            j = instance.getTimeInMillis() - timeInMillis;
        } else {
            j = 0;
        }
        StringBuilder sb = new StringBuilder("google-encrypted-re-upload-worker/scheduleNextRun at ");
        sb.append(instance.getTime());
        sb.append(", immediately = ");
        sb.append(z);
        sb.append(", existingWorkPolicy = ");
        sb.append(r8);
        Log.i(sb.toString());
        ((AnonymousClass022) r10.get()).A05(r8, A00(r9, j), "com.whatsapp.backup.google.google-encrypted-re-upload-worker");
    }

    public static final void A02(String str, boolean z) {
        if (z) {
            StringBuilder sb = new StringBuilder("google-encrypted-re-upload-worker ");
            sb.append(str);
            sb.append(", work aborted");
            Log.w(sb.toString());
        }
    }

    @Override // androidx.work.ListenableWorker
    public void A03() {
        Log.i("google-encrypted-re-upload-worker/onStopped");
        this.A07.A0d.getAndSet(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x035a, code lost:
        if (r1.length() <= 0) goto L_0x035c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0381, code lost:
        if (r0.startsWith(r12.A05) == false) goto L_0x0383;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f8, code lost:
        if (r6.jabber_id == null) goto L_0x00fa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x05f1  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x05f3 A[Catch: all -> 0x06e9, TryCatch #3 {all -> 0x06e9, blocks: (B:3:0x0003, B:5:0x0032, B:6:0x0034, B:7:0x0070, B:10:0x00a1, B:12:0x00aa, B:14:0x00bb, B:17:0x00c3, B:19:0x00ca, B:21:0x00d5, B:23:0x00e0, B:26:0x00ec, B:29:0x00f5, B:32:0x00fb, B:34:0x0102, B:36:0x010d, B:39:0x011c, B:41:0x0123, B:42:0x012b, B:45:0x013d, B:47:0x0144, B:50:0x0150, B:51:0x0156, B:53:0x0167, B:54:0x016e, B:56:0x017a, B:57:0x0180, B:59:0x0184, B:63:0x0191, B:66:0x0199, B:67:0x019e, B:69:0x01d5, B:70:0x01e3, B:72:0x0233, B:73:0x023a, B:74:0x0246, B:76:0x024c, B:77:0x0250, B:78:0x025b, B:80:0x0263, B:83:0x0273, B:84:0x0278, B:86:0x0293, B:88:0x029e, B:91:0x02b7, B:92:0x02f0, B:94:0x02f6, B:96:0x02fe, B:98:0x0304, B:100:0x0316, B:102:0x031d, B:103:0x0338, B:105:0x033e, B:107:0x0344, B:109:0x0350, B:112:0x035e, B:114:0x0364, B:115:0x0376, B:117:0x037a, B:121:0x0386, B:123:0x038e, B:124:0x03a7, B:125:0x03ad, B:126:0x03b0, B:127:0x03b3, B:129:0x03b8, B:130:0x03bb, B:132:0x03c3, B:134:0x03c7, B:135:0x03c8, B:139:0x03d6, B:140:0x03db, B:141:0x03dc, B:142:0x03e1, B:143:0x03e2, B:145:0x03eb, B:147:0x0406, B:148:0x0424, B:150:0x042a, B:152:0x043a, B:153:0x0454, B:155:0x045a, B:156:0x0473, B:157:0x0486, B:158:0x0487, B:159:0x048f, B:161:0x0499, B:162:0x049f, B:164:0x04a6, B:166:0x04ae, B:167:0x04b3, B:168:0x04c6, B:169:0x04ca, B:171:0x04e1, B:174:0x04ee, B:176:0x04f5, B:177:0x04fa, B:179:0x050c, B:180:0x0520, B:182:0x0531, B:185:0x0540, B:186:0x0544, B:188:0x054c, B:189:0x05aa, B:190:0x05bc, B:192:0x05c2, B:193:0x05cc, B:195:0x05d7, B:196:0x05dd, B:198:0x05e3, B:202:0x05f3, B:206:0x05fe, B:209:0x0607, B:211:0x060d, B:214:0x061a, B:217:0x0623, B:219:0x0629, B:220:0x0632, B:222:0x064e, B:223:0x0651, B:224:0x0657, B:225:0x065e, B:227:0x067e, B:228:0x0681, B:229:0x0687, B:230:0x068e, B:232:0x0699, B:234:0x06a0, B:236:0x06a6, B:238:0x06b2, B:239:0x06be), top: B:251:0x0003, inners: #0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x064e A[Catch: all -> 0x06e9, TryCatch #3 {all -> 0x06e9, blocks: (B:3:0x0003, B:5:0x0032, B:6:0x0034, B:7:0x0070, B:10:0x00a1, B:12:0x00aa, B:14:0x00bb, B:17:0x00c3, B:19:0x00ca, B:21:0x00d5, B:23:0x00e0, B:26:0x00ec, B:29:0x00f5, B:32:0x00fb, B:34:0x0102, B:36:0x010d, B:39:0x011c, B:41:0x0123, B:42:0x012b, B:45:0x013d, B:47:0x0144, B:50:0x0150, B:51:0x0156, B:53:0x0167, B:54:0x016e, B:56:0x017a, B:57:0x0180, B:59:0x0184, B:63:0x0191, B:66:0x0199, B:67:0x019e, B:69:0x01d5, B:70:0x01e3, B:72:0x0233, B:73:0x023a, B:74:0x0246, B:76:0x024c, B:77:0x0250, B:78:0x025b, B:80:0x0263, B:83:0x0273, B:84:0x0278, B:86:0x0293, B:88:0x029e, B:91:0x02b7, B:92:0x02f0, B:94:0x02f6, B:96:0x02fe, B:98:0x0304, B:100:0x0316, B:102:0x031d, B:103:0x0338, B:105:0x033e, B:107:0x0344, B:109:0x0350, B:112:0x035e, B:114:0x0364, B:115:0x0376, B:117:0x037a, B:121:0x0386, B:123:0x038e, B:124:0x03a7, B:125:0x03ad, B:126:0x03b0, B:127:0x03b3, B:129:0x03b8, B:130:0x03bb, B:132:0x03c3, B:134:0x03c7, B:135:0x03c8, B:139:0x03d6, B:140:0x03db, B:141:0x03dc, B:142:0x03e1, B:143:0x03e2, B:145:0x03eb, B:147:0x0406, B:148:0x0424, B:150:0x042a, B:152:0x043a, B:153:0x0454, B:155:0x045a, B:156:0x0473, B:157:0x0486, B:158:0x0487, B:159:0x048f, B:161:0x0499, B:162:0x049f, B:164:0x04a6, B:166:0x04ae, B:167:0x04b3, B:168:0x04c6, B:169:0x04ca, B:171:0x04e1, B:174:0x04ee, B:176:0x04f5, B:177:0x04fa, B:179:0x050c, B:180:0x0520, B:182:0x0531, B:185:0x0540, B:186:0x0544, B:188:0x054c, B:189:0x05aa, B:190:0x05bc, B:192:0x05c2, B:193:0x05cc, B:195:0x05d7, B:196:0x05dd, B:198:0x05e3, B:202:0x05f3, B:206:0x05fe, B:209:0x0607, B:211:0x060d, B:214:0x061a, B:217:0x0623, B:219:0x0629, B:220:0x0632, B:222:0x064e, B:223:0x0651, B:224:0x0657, B:225:0x065e, B:227:0x067e, B:228:0x0681, B:229:0x0687, B:230:0x068e, B:232:0x0699, B:234:0x06a0, B:236:0x06a6, B:238:0x06b2, B:239:0x06be), top: B:251:0x0003, inners: #0 }] */
    @Override // androidx.work.Worker
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass043 A04() {
        /*
        // Method dump skipped, instructions count: 1794
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.backup.google.workers.GoogleEncryptedReUploadWorker.A04():X.043");
    }

    public final AnonymousClass043 A05(int i, int i2) {
        long j;
        C14820m6 r3 = this.A0K;
        String A09 = r3.A09();
        if (!TextUtils.isEmpty(A09)) {
            long currentTimeMillis = System.currentTimeMillis();
            if (TextUtils.isEmpty(A09)) {
                j = 0;
            } else {
                SharedPreferences sharedPreferences = r3.A00;
                StringBuilder sb = new StringBuilder("gdrive_old_media_encryption_start_time:");
                sb.append(A09);
                j = sharedPreferences.getLong(sb.toString(), 0);
            }
            AnonymousClass317 r2 = this.A0N;
            Long valueOf = Long.valueOf((currentTimeMillis - j) / 3600000);
            r2.A08 = valueOf;
            r2.A05 = valueOf;
        }
        AnonymousClass317 r1 = this.A0N;
        if (i < 6) {
            r1.A02 = Integer.valueOf(i2);
            this.A0M.A07(r1);
            return new AnonymousClass042();
        }
        r1.A02 = 7;
        this.A0M.A07(r1);
        return new AnonymousClass0GK();
    }
}
