package com.whatsapp.backup.google.viewmodel;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C14330lG;
import X.C15880o3;
import X.C25721Am;
import X.C26251Cp;
import X.C27691It;

/* loaded from: classes2.dex */
public class RestoreFromBackupViewModel extends AnonymousClass015 {
    public int A00 = 21;
    public final AnonymousClass016 A01 = new AnonymousClass016();
    public final AnonymousClass016 A02 = new AnonymousClass016(0L);
    public final C14330lG A03;
    public final C25721Am A04;
    public final C26251Cp A05;
    public final C15880o3 A06;
    public final C27691It A07 = new C27691It();
    public final AbstractC14440lR A08;

    public RestoreFromBackupViewModel(C14330lG r3, C25721Am r4, C26251Cp r5, C15880o3 r6, AbstractC14440lR r7) {
        this.A08 = r7;
        this.A03 = r3;
        this.A05 = r5;
        this.A06 = r6;
        this.A04 = r4;
    }
}
