package com.whatsapp.backup.google;

import X.AnonymousClass018;
import X.AnonymousClass2GV;
import X.AnonymousClass52Z;
import X.C004802e;
import X.C12970iu;
import X.C12990iw;
import X.C14820m6;
import X.C44891zj;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.R;
import com.whatsapp.backup.google.NotEnoughStorageDialogFragment;

/* loaded from: classes3.dex */
public class NotEnoughStorageDialogFragment extends Hilt_NotEnoughStorageDialogFragment {
    public C14820m6 A00;
    public AnonymousClass018 A01;

    public static /* synthetic */ void A00(NotEnoughStorageDialogFragment notEnoughStorageDialogFragment) {
        String str;
        if (C12990iw.A1X(Build.VERSION.SDK_INT, 26)) {
            str = "android.os.storage.action.MANAGE_STORAGE";
        } else {
            str = "android.settings.INTERNAL_STORAGE_SETTINGS";
        }
        notEnoughStorageDialogFragment.startActivityForResult(new Intent(str), 7);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        long j = A03.getLong("backup_size");
        int i = A03.getInt("backup_state");
        AnonymousClass52Z r6 = new AnonymousClass2GV() { // from class: X.52Z
            @Override // X.AnonymousClass2GV
            public final void AO1() {
                NotEnoughStorageDialogFragment.A00(NotEnoughStorageDialogFragment.this);
            }
        };
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.not_enough_storage);
        AnonymousClass018 r2 = this.A01;
        int i2 = R.plurals.import_backup_not_enough_space_message;
        if (i == 1) {
            i2 = R.plurals.export_backup_not_enough_space_message;
        }
        A0O.A0A(C44891zj.A02(r2, i2, j, false));
        A0O.setPositiveButton(R.string.ok_short, new IDxCListenerShape4S0000000_2_I1(5));
        C12970iu.A1K(A0O, r6, 12, R.string.permission_settings_open);
        return A0O.create();
    }
}
