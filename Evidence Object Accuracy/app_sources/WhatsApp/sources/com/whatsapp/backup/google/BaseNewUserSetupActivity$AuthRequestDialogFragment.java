package com.whatsapp.backup.google;

import X.ProgressDialogC48342Fq;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.backup.google.BaseNewUserSetupActivity$AuthRequestDialogFragment;

/* loaded from: classes2.dex */
public class BaseNewUserSetupActivity$AuthRequestDialogFragment extends Hilt_BaseNewUserSetupActivity_AuthRequestDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ProgressDialogC48342Fq r2 = new ProgressDialogC48342Fq(A0p());
        r2.setTitle(R.string.settings_gdrive_authenticating_with_google_servers_title);
        r2.setIndeterminate(true);
        r2.setMessage(A0I(R.string.settings_gdrive_authenticating_with_google_servers_message));
        r2.setCancelable(true);
        r2.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.4ev
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                ((AnonymousClass29U) C13010iy.A01(BaseNewUserSetupActivity$AuthRequestDialogFragment.this)).A0a = true;
            }
        });
        return r2;
    }
}
