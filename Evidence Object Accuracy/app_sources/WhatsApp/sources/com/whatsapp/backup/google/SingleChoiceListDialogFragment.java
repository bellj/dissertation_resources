package com.whatsapp.backup.google;

import X.AnonymousClass01E;
import X.AnonymousClass0OC;
import X.AnonymousClass29I;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C14900mE;
import X.C53302do;
import X.DialogInterface$OnClickListenerC65723Kt;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.backup.google.SingleChoiceListDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public class SingleChoiceListDialogFragment extends Hilt_SingleChoiceListDialogFragment {
    public C14900mE A00;
    public AnonymousClass29I A01;
    public final AtomicBoolean A02 = new AtomicBoolean(false);

    public static /* synthetic */ void A00(DialogInterface dialogInterface, Bundle bundle, SingleChoiceListDialogFragment singleChoiceListDialogFragment, int i, int i2) {
        singleChoiceListDialogFragment.A01.AW6(bundle.getStringArray("items"), i, i2);
        singleChoiceListDialogFragment.A02.set(true);
        dialogInterface.dismiss();
    }

    @Override // com.whatsapp.backup.google.Hilt_SingleChoiceListDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A01 = (AnonymousClass29I) context;
        } catch (ClassCastException unused) {
            StringBuilder A0h = C12960it.A0h();
            C12970iu.A1V(context, A0h);
            throw new ClassCastException(C12960it.A0d(" must implement SingleChoiceListListener", A0h));
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String str;
        int length;
        int length2;
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (bundle2.containsKey("dialog_id")) {
            int i = bundle2.getInt("dialog_id");
            this.A02.set(false);
            C004802e A0O = C12970iu.A0O(this);
            A0O.setTitle(bundle2.getString("title"));
            A0O.setPositiveButton(R.string.cancel, null);
            if (bundle2.containsKey("items") && bundle2.containsKey("multi_line_list_items_key")) {
                throw C12960it.A0U("Cannot provide both items and multi_line_list_items_key");
            } else if (bundle2.containsKey("items") || bundle2.containsKey("multi_line_list_items_key")) {
                int i2 = bundle2.getInt("selected_item_index", -1);
                if (bundle2.containsKey("items")) {
                    A0O.A09(new DialogInterface.OnClickListener(bundle2, this, i) { // from class: X.4gw
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ Bundle A01;
                        public final /* synthetic */ SingleChoiceListDialogFragment A02;

                        {
                            this.A02 = r2;
                            this.A00 = r3;
                            this.A01 = r1;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            SingleChoiceListDialogFragment.A00(dialogInterface, this.A01, this.A02, this.A00, i3);
                        }
                    }, bundle2.getStringArray("items"), i2);
                } else if (bundle2.containsKey("multi_line_list_items_key")) {
                    ArrayList A0l = C12960it.A0l();
                    String[] stringArray = bundle2.getStringArray("multi_line_list_items_key");
                    if (stringArray != null) {
                        String[] stringArray2 = bundle2.getStringArray("multi_line_list_item_values_key");
                        boolean[] booleanArray = bundle2.getBooleanArray("list_item_enabled_key");
                        String string = bundle2.getString("disabled_item_toast_key");
                        if (stringArray2 == null || (length = stringArray.length) == (length2 = stringArray2.length)) {
                            for (int i3 = 0; i3 < stringArray.length; i3++) {
                                HashMap A11 = C12970iu.A11();
                                A11.put("line1", stringArray[i3]);
                                if (stringArray2 != null) {
                                    str = stringArray2[i3];
                                } else {
                                    str = null;
                                }
                                A11.put("line2", str);
                                A0l.add(A11);
                            }
                            C53302do r9 = new C53302do(A0p(), this, A0l, new int[]{16908308, 16908309}, new String[]{"line1", "line2"}, stringArray, booleanArray, i2);
                            DialogInterface$OnClickListenerC65723Kt r1 = new DialogInterface.OnClickListener(string, stringArray, booleanArray, i) { // from class: X.3Kt
                                public final /* synthetic */ int A00;
                                public final /* synthetic */ String A02;
                                public final /* synthetic */ String[] A03;
                                public final /* synthetic */ boolean[] A04;

                                {
                                    this.A04 = r4;
                                    this.A00 = r5;
                                    this.A03 = r3;
                                    this.A02 = r2;
                                }

                                @Override // android.content.DialogInterface.OnClickListener
                                public final void onClick(DialogInterface dialogInterface, int i4) {
                                    SingleChoiceListDialogFragment singleChoiceListDialogFragment = SingleChoiceListDialogFragment.this;
                                    boolean[] zArr = this.A04;
                                    int i5 = this.A00;
                                    String[] strArr = this.A03;
                                    String str2 = this.A02;
                                    if (zArr == null || zArr[i4]) {
                                        singleChoiceListDialogFragment.A01.AW6(strArr, i5, i4);
                                        singleChoiceListDialogFragment.A02.set(true);
                                        dialogInterface.dismiss();
                                    } else if (str2 != null) {
                                        singleChoiceListDialogFragment.A00.A0E(str2, 0);
                                    }
                                }
                            };
                            AnonymousClass0OC r0 = A0O.A01;
                            r0.A0D = r9;
                            r0.A05 = r1;
                            r0.A00 = i2;
                            r0.A0L = true;
                        } else {
                            StringBuilder A0k = C12960it.A0k("keys.length = ");
                            A0k.append(length);
                            A0k.append(" ≠ ");
                            A0k.append(length2);
                            throw C12970iu.A0f(C12960it.A0d(" values.length", A0k));
                        }
                    } else {
                        throw C12970iu.A0f("Must provide multi_line_list_items_key");
                    }
                }
                return A0O.create();
            } else {
                throw C12960it.A0U("Must provide either items or multi_line_list_items_key");
            }
        } else {
            throw C12960it.A0U("dialog_id should be provided.");
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        Bundle bundle = ((AnonymousClass01E) this).A05;
        if (this.A01 != null && !this.A02.get() && bundle.containsKey("dialog_id")) {
            this.A01.APF(bundle.getInt("dialog_id"));
        }
    }
}
