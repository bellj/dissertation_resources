package com.whatsapp.registration;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass10O;
import X.AnonymousClass2FL;
import X.AnonymousClass3HG;
import X.AnonymousClass5TE;
import X.AnonymousClass5WF;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14960mK;
import X.C15890o4;
import X.C16590pI;
import X.C18350sJ;
import X.C18790t3;
import X.C252018m;
import X.C25771At;
import X.C35751ig;
import X.C42971wC;
import X.C58272oQ;
import X.C63183Ap;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.registration.PrimaryFlashCallEducationScreen;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class PrimaryFlashCallEducationScreen extends ActivityC13790kL implements AnonymousClass5WF {
    public long A00;
    public long A01;
    public C18790t3 A02;
    public C16590pI A03;
    public C15890o4 A04;
    public C25771At A05;
    public AnonymousClass3HG A06;
    public C18350sJ A07;
    public AnonymousClass10O A08;
    public C252018m A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;

    public PrimaryFlashCallEducationScreen() {
        this(0);
        this.A00 = 0;
        this.A01 = 0;
    }

    public PrimaryFlashCallEducationScreen(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 104);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A03 = C12970iu.A0X(A1M);
            this.A02 = C12990iw.A0U(A1M);
            this.A09 = C12980iv.A0g(A1M);
            this.A05 = (C25771At) A1M.A7p.get();
            this.A07 = (C18350sJ) A1M.AHX.get();
            this.A04 = C12970iu.A0Y(A1M);
            this.A08 = (AnonymousClass10O) A1M.AMM.get();
        }
    }

    public final SpannableString A2e(Typeface typeface, String str) {
        TypefaceSpan typefaceSpan;
        Spanned fromHtml = Html.fromHtml(str);
        String obj = fromHtml.toString();
        SpannableString spannableString = new SpannableString(obj);
        Object[] spans = fromHtml.getSpans(0, obj.length(), Object.class);
        for (Object obj2 : spans) {
            int spanStart = fromHtml.getSpanStart(obj2);
            int spanEnd = fromHtml.getSpanEnd(obj2);
            int spanFlags = fromHtml.getSpanFlags(obj2);
            if (Build.VERSION.SDK_INT >= 28) {
                typefaceSpan = new TypefaceSpan(typeface);
            } else {
                typefaceSpan = new TypefaceSpan("sans-serif-medium");
            }
            spannableString.setSpan(typefaceSpan, spanStart, spanEnd, spanFlags);
            spannableString.setSpan(C12980iv.A0M(this, R.color.flash_call_medium_weight_text_color), spanStart, spanEnd, spanFlags);
        }
        return spannableString;
    }

    public final void A2f() {
        Log.i("primaryflashcalleducationscreen/attempt-flash-call");
        this.A07.A0A(8);
        startActivity(C14960mK.A0X(this, null, -1, this.A00, this.A01, this.A0C, false, this.A0A, true));
        finish();
    }

    public final void A2g() {
        if (Build.VERSION.SDK_INT >= 28) {
            C12970iu.A1B(C12960it.A08(((ActivityC13810kN) this).A09), "pref_flash_call_manage_call_permission_granted", this.A04.A05() ? 1 : 0);
            C12970iu.A1B(C12960it.A08(((ActivityC13810kN) this).A09), "pref_flash_call_call_log_permission_granted", this.A04.A04() ? 1 : 0);
        }
    }

    public final void A2h(boolean z) {
        StringBuilder A0k = C12960it.A0k("primaryflashcalleducationscreen/startverifysms/usesmsretriever=");
        A0k.append(z);
        C12960it.A1F(A0k);
        this.A07.A0A(4);
        startActivity(C14960mK.A0X(this, null, -1, this.A00, this.A01, z, true, this.A0A, false));
        finish();
    }

    @Override // X.AnonymousClass5WF
    public void AZW() {
        this.A0C = false;
        boolean z = this.A0D;
        C15890o4 r0 = this.A04;
        if (z) {
            if (!r0.A06()) {
                Log.i("primaryflashcalleducationscreen/request-flash-call-permissions");
                RequestPermissionActivity.A0N(this, this.A04, 2, true);
                return;
            }
            A2f();
        } else if (r0.A02("android.permission.RECEIVE_SMS") == 0) {
            A2h(false);
        } else {
            C35751ig r2 = new C35751ig(this);
            r2.A01 = R.drawable.permission_sms;
            r2.A0C = new String[]{"android.permission.RECEIVE_SMS"};
            r2.A02 = R.string.permission_sms_request;
            r2.A06 = true;
            A2E(r2.A00(), 1);
        }
    }

    @Override // X.AnonymousClass5WF
    public void AeF() {
        this.A0C = true;
        if (!this.A0D) {
            A2h(true);
        } else if (!this.A04.A06()) {
            Log.i("primaryflashcalleducationscreen/request-flash-call-permissions");
            RequestPermissionActivity.A0N(this, this.A04, 2, true);
        } else {
            A2f();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        if (i == 1) {
            StringBuilder A0k = C12960it.A0k("primaryflashcalleducationscreen/activity-result/request-sms-permissions/");
            if (i2 == -1) {
                str = "granted";
            } else {
                str = "denied";
            }
            Log.i(C12960it.A0d(str, A0k));
            A2h(false);
        } else if (i != 2) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            Log.i("primaryflashcalleducationscreen/activity-result/request-flash-call-permissions/granted");
            A2g();
            A2f();
        } else {
            Log.i("primaryflashcalleducationscreen/activity-result/request-flash-call-permissions/denied");
            ((ActivityC13810kN) this).A09.A0g("primary_eligible");
            A2g();
            this.A0D = false;
            C63183Ap.A00(this.A03, this);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        Intent intent;
        if (this.A0A) {
            Log.i("primaryflashcalleducationscreen/back-pressed/go-to-change-number-screen");
            this.A07.A0A(3);
            if (!this.A07.A0E()) {
                finish();
                return;
            } else {
                intent = C12970iu.A0A();
                intent.setClassName(getPackageName(), "com.whatsapp.registration.ChangeNumber");
            }
        } else {
            Log.i("primaryflashcalleducationscreen/back-pressed/go-to-register-phone-screen");
            this.A07.A0A(1);
            intent = C14960mK.A05(this);
            intent.putExtra("com.whatsapp.registration.RegisterPhone.clear_phone_number", true);
        }
        A2G(intent, true);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Log.i("primaryflashcalleducationscreen/oncreate");
        super.onCreate(bundle);
        setContentView(R.layout.primary_flash_call_education_screen);
        C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "pref_flash_call_education_screen_displayed", true);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.verify_flash_call_title_toolbar);
        ActivityC13790kL.A0c(this, toolbar, ((ActivityC13830kP) this).A01);
        A1e(toolbar);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 26));
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0P(false);
        }
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        C12990iw.A0N(this, R.id.flash_call_education_screen_headline).setTypeface(createFromAsset, 0);
        C12990iw.A0N(this, R.id.make_and_manage_calls).setText(A2e(createFromAsset, getString(R.string.manage_call_permission_explanation)));
        C12990iw.A0N(this, R.id.access_phone_call_logs).setText(A2e(createFromAsset, getString(R.string.access_call_log_permission_explanation)));
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.flash_call_learn_more);
        String string = getString(R.string.learn_more_about_verify_with_flash_call);
        HashMap A11 = C12970iu.A11();
        A11.put("flash-call-faq-link", ((ActivityC13790kL) this).A02.A00("https://faq.whatsapp.com/android/verification/how-to-register-your-account-with-a-missed-call"));
        C42971wC.A09(this, ((ActivityC13790kL) this).A00, ((ActivityC13810kN) this).A05, textEmojiLabel, ((ActivityC13810kN) this).A08, string, A11);
        SpannableString spannableString = new SpannableString(textEmojiLabel.getText());
        ((C58272oQ[]) spannableString.getSpans(0, spannableString.length(), C58272oQ.class))[0].A02 = new AnonymousClass5TE() { // from class: X.3UL
            @Override // X.AnonymousClass5TE
            public final void A7H() {
                C12970iu.A1B(C12960it.A08(((ActivityC13810kN) PrimaryFlashCallEducationScreen.this).A09), "pref_flash_call_education_link_clicked", 1);
            }
        };
        AbstractC14440lR r14 = ((ActivityC13830kP) this).A05;
        this.A06 = new AnonymousClass3HG(this.A02, ((ActivityC13830kP) this).A01, this.A05, ((ActivityC13810kN) this).A0D, this.A09, r14);
        if (C12990iw.A0H(this) != null) {
            this.A00 = getIntent().getLongExtra("sms_retry_time", 0);
            this.A01 = getIntent().getLongExtra("voice_retry_time", 0);
            this.A0A = getIntent().getBooleanExtra("change_number", false);
        }
        C12960it.A12(AnonymousClass00T.A05(this, R.id.verify_with_sms_button), this, 25);
        C12960it.A12(AnonymousClass00T.A05(this, R.id.continue_button), this, 24);
        if (((ActivityC13810kN) this).A09.A00.getInt("pref_flash_call_education_link_clicked", -1) == -1) {
            C12960it.A0u(((ActivityC13810kN) this).A09.A00, "pref_flash_call_education_link_clicked", 0);
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 0) {
            Log.i("primaryflashcalleducationscreen/select-menu-option/help");
            this.A06.A02(this, this.A08, "verify-flash-call");
            return true;
        } else if (itemId != 1) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            Log.i("primaryflashcalleducationscreen/select-menu-option/reset");
            this.A07.A09();
            startActivity(C14960mK.A01(this));
            finishAffinity();
            return true;
        }
    }
}
