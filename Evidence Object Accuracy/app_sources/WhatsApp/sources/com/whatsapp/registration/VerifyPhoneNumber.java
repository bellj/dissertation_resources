package com.whatsapp.registration;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC20260vT;
import X.AbstractC44431yx;
import X.AbstractC44441yy;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass04S;
import X.AnonymousClass0U5;
import X.AnonymousClass105;
import X.AnonymousClass10O;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass1CE;
import X.AnonymousClass1I0;
import X.AnonymousClass1R9;
import X.AnonymousClass1RC;
import X.AnonymousClass1RD;
import X.AnonymousClass23M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2Z3;
import X.AnonymousClass3FQ;
import X.AnonymousClass3HG;
import X.AnonymousClass3J8;
import X.AnonymousClass4AY;
import X.AnonymousClass4KK;
import X.AnonymousClass5B8;
import X.C004802e;
import X.C103384qf;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15410nB;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16590pI;
import X.C16630pM;
import X.C18350sJ;
import X.C18360sK;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C20640w5;
import X.C20800wL;
import X.C20920wX;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22040yO;
import X.C22660zR;
import X.C22670zS;
import X.C22680zT;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25771At;
import X.C25961Bm;
import X.C36021jC;
import X.C38131nZ;
import X.C51822Yy;
import X.C626237y;
import X.C626938f;
import X.C627438k;
import X.C64323Fc;
import X.C64333Fd;
import X.C862946p;
import X.C863046q;
import X.CountDownTimerC51942Zt;
import X.HandlerC52092aE;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0100200_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.registration.report.BanReportViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class VerifyPhoneNumber extends ActivityC13790kL implements AbstractC44431yx, AbstractC44441yy {
    public static int A1D = 6;
    public static int A1E = 6;
    public static int A1F;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public long A0A;
    public ProgressDialog A0B;
    public CountDownTimer A0C;
    public CountDownTimer A0D;
    public View A0E;
    public View A0F;
    public View A0G;
    public ImageButton A0H;
    public ProgressBar A0I;
    public RelativeLayout A0J;
    public TextView A0K;
    public AnonymousClass04S A0L;
    public CodeInputField A0M;
    public C22680zT A0N;
    public C20640w5 A0O;
    public AnonymousClass19Y A0P;
    public C18790t3 A0Q;
    public TextEmojiLabel A0R;
    public C21740xu A0S;
    public AnonymousClass4KK A0T;
    public C15410nB A0U;
    public C16590pI A0V;
    public C18360sK A0W;
    public C15890o4 A0X;
    public C25771At A0Y;
    public AnonymousClass11G A0Z;
    public C16630pM A0a;
    public AnonymousClass3J8 A0b;
    public C25961Bm A0c;
    public AnonymousClass3FQ A0d;
    public AnonymousClass3FQ A0e;
    public C64333Fd A0f;
    public AnonymousClass1CE A0g;
    public C22660zR A0h;
    public C64323Fc A0i;
    public AnonymousClass2Z3 A0j;
    public AnonymousClass3HG A0k;
    public C20800wL A0l;
    public C18350sJ A0m;
    public C51822Yy A0n;
    public AnonymousClass10O A0o;
    public HandlerC52092aE A0p;
    public C862946p A0q;
    public C25661Ag A0r;
    public BanReportViewModel A0s;
    public AnonymousClass105 A0t;
    public AnonymousClass12U A0u;
    public C252018m A0v;
    public String A0w;
    public String A0x;
    public String A0y;
    public String A0z;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    public boolean A16;
    public boolean A17;
    public boolean A18;
    public boolean A19;
    public boolean A1A;
    public final C20920wX A1B;
    public final AbstractC20260vT A1C;

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
    }

    public VerifyPhoneNumber() {
        this(0);
        this.A00 = 0;
        this.A14 = false;
        this.A0A = 0;
        this.A09 = -1;
        this.A1B = C20920wX.A00();
        this.A1C = new AbstractC20260vT() { // from class: X.55i
            @Override // X.AbstractC20260vT
            public final void AOa(AnonymousClass1I1 r2) {
                VerifyPhoneNumber.this.A2p();
            }
        };
        this.A04 = -2;
    }

    public VerifyPhoneNumber(int i) {
        this.A13 = false;
        A0R(new C103384qf(this));
    }

    public static final String A02(Intent intent) {
        Uri data;
        String str = null;
        if ("whatsapp".equals(intent.getScheme())) {
            Uri data2 = intent.getData();
            if (data2 != null && "r".equals(data2.getHost())) {
                str = data2.getQueryParameter("c");
                StringBuilder sb = new StringBuilder();
                sb.append("verifyphonenumber/codefromverificationlink/code/");
                sb.append(str);
                Log.i(sb.toString());
            }
        } else if (("http".equals(intent.getScheme()) || "https".equals(intent.getScheme())) && (data = intent.getData()) != null && "v.whatsapp.com".equals(data.getHost())) {
            String path = data.getPath();
            str = path.substring(path.lastIndexOf("/") + 1);
            StringBuilder sb = new StringBuilder();
            sb.append("verifyphonenumber/codefromverificationlink/code/");
            sb.append(str);
            Log.i(sb.toString());
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A03(com.whatsapp.registration.VerifyPhoneNumber r6) {
        /*
            java.lang.String r0 = "verifyphonenumber/request-call/cc="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r6.A0x
            r1.append(r0)
            java.lang.String r0 = "/number="
            r1.append(r0)
            java.lang.String r0 = r6.A0y
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            int r0 = r6.A2e()
            boolean r0 = X.AnonymousClass3J8.A01(r0)
            if (r0 == 0) goto L_0x002c
            r0 = 2
            r6.A3A(r0)
        L_0x002c:
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 0
            r0 = 23
            if (r2 >= r0) goto L_0x0084
            java.lang.String r1 = "verifyphonenumber/is-eligible-for-flash-call-as-secondary-registration/api="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0042:
            com.whatsapp.util.Log.i(r0)
        L_0x0045:
            java.lang.String r0 = "verifyphonenumber/request-voice"
            com.whatsapp.util.Log.i(r0)
            X.0m6 r0 = r6.A09
            int r0 = r0.A05()
            X.46q r4 = new X.46q
            r4.<init>(r0)
            X.01d r0 = r6.A08
            android.telephony.TelephonyManager r0 = r0.A0N()
            r2 = 0
            if (r0 == 0) goto L_0x0067
            int r1 = r0.getSimState()
            r0 = 1
            if (r1 != r0) goto L_0x0067
            r2 = 1
        L_0x0067:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            r4.A01 = r0
            X.0m6 r0 = r6.A09
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "migrate_from_consumer_app_directly"
            boolean r0 = r1.getBoolean(r0, r3)
            r1 = 1
            if (r0 == 0) goto L_0x0080
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r4.A02 = r0
        L_0x0080:
            r6.A3K(r4, r1)
            return
        L_0x0084:
            X.01d r0 = r6.A08
            android.telephony.TelephonyManager r0 = r0.A0N()
            if (r0 == 0) goto L_0x0097
            int r1 = r0.getSimState()
            r0 = 1
            if (r1 != r0) goto L_0x0097
            java.lang.String r0 = "verifyphonenumber/is-eligible-for-flash-call-as-secondary-registration/sim absent"
            goto L_0x0042
        L_0x0097:
            X.0m6 r0 = r6.A09
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r4 = "is_first_flash_call_request"
            r0 = 1
            boolean r5 = r1.getBoolean(r4, r0)
            X.0m6 r0 = r6.A09
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "flash_call_eligible"
            int r2 = r1.getInt(r0, r3)
            java.lang.String r0 = "verifyphonenumber/is-eligible-for-flash-call-as-secondary-registration/isFirstSecondaryFlashCallRequest="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r0 = "/flashCallEligible="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            r0 = 1
            if (r5 == 0) goto L_0x0045
            if (r2 != r0) goto L_0x0045
            X.0m6 r1 = r6.A09
            java.lang.String r0 = "secondary_eligible"
            r1.A0j(r0)
            X.0m6 r0 = r6.A09
            android.content.SharedPreferences r0 = r0.A00
            android.content.SharedPreferences$Editor r0 = r0.edit()
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r4, r3)
            r0.apply()
            r6.A3l(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.A03(com.whatsapp.registration.VerifyPhoneNumber):void");
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A13) {
            this.A13 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0V = (C16590pI) r1.AMg.get();
            this.A0S = (C21740xu) r1.AM1.get();
            this.A0O = (C20640w5) r1.AHk.get();
            this.A0u = (AnonymousClass12U) r1.AJd.get();
            this.A0Q = (C18790t3) r1.AJw.get();
            this.A0r = (C25661Ag) r1.A8G.get();
            this.A0h = (C22660zR) r1.AAk.get();
            this.A0P = (AnonymousClass19Y) r1.AI6.get();
            this.A0v = (C252018m) r1.A7g.get();
            this.A0N = (C22680zT) r1.AGW.get();
            this.A0g = (AnonymousClass1CE) r1.A87.get();
            this.A0U = (C15410nB) r1.ALJ.get();
            this.A0Z = (AnonymousClass11G) r1.AKq.get();
            this.A0f = new C64333Fd(r2.A1B, (AnonymousClass018) r1.ANb.get(), (C22040yO) r1.A01.get());
            this.A0Y = (C25771At) r1.A7p.get();
            this.A0m = (C18350sJ) r1.AHX.get();
            this.A0X = (C15890o4) r1.AN1.get();
            this.A0o = (AnonymousClass10O) r1.AMM.get();
            this.A0W = (C18360sK) r1.AN0.get();
            this.A0T = AnonymousClass5B8.A00((C25961Bm) r1.A0o.get());
            this.A0t = (AnonymousClass105) r1.AJc.get();
            this.A0a = (C16630pM) r1.AIc.get();
            this.A0l = (C20800wL) r1.AHW.get();
            this.A0b = new AnonymousClass3J8();
        }
    }

    public final int A2e() {
        int i = this.A02;
        if (i == 3) {
            return 9;
        }
        return i == 1 ? 13 : 4;
    }

    public final long A2f() {
        long j = getPreferences(0).getLong("com.whatsapp.registration.VerifyPhoneNumber.sms_request_failed_retry_time", -1);
        System.currentTimeMillis();
        return j;
    }

    public final long A2g() {
        return getPreferences(0).getLong("com.whatsapp.registration.VerifyPhoneNumber.call_countdown_end_time", -1);
    }

    public final String A2h() {
        return AnonymousClass3J8.A01(A2e()) ? "verify_second_sms" : "verify_sms";
    }

    public final String A2i() {
        SharedPreferences preferences = getPreferences(0);
        String string = preferences.getString("com.whatsapp.registration.VerifyPhoneNumber.sms_cc", null);
        String string2 = preferences.getString("com.whatsapp.registration.VerifyPhoneNumber.sms_phone_number", null);
        if (!this.A0x.equals(string) || !this.A0y.equals(string2)) {
            return null;
        }
        return preferences.getString("com.whatsapp.registration.VerifyPhoneNumber.sms_code", null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 != false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A2j() {
        /*
            r2 = this;
            X.0nN r0 = r2.A0B
            int r0 = r0.A00()
            boolean r0 = X.AnonymousClass3J8.A01(r0)
            if (r0 == 0) goto L_0x0013
            boolean r1 = r2.A14
            r0 = 2131886173(0x7f12005d, float:1.9406917E38)
            if (r1 == 0) goto L_0x0016
        L_0x0013:
            r0 = 2131892700(0x7f1219dc, float:1.9420156E38)
        L_0x0016:
            java.lang.String r0 = r2.getString(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.A2j():java.lang.String");
    }

    public final String A2k() {
        int i;
        Object[] objArr;
        long A2g = A2g();
        long currentTimeMillis = System.currentTimeMillis();
        long j = -1;
        if (A2g != -1) {
            j = A2g - currentTimeMillis;
        }
        StringBuilder sb = new StringBuilder("verifyphonenumber/voice-retry-time/diff/");
        sb.append(j);
        Log.i(sb.toString());
        if (j > 0) {
            i = R.string.register_server_sms_next_method_with_wait_time;
            objArr = new Object[]{getString(R.string.verify_voice_call_button), C38131nZ.A08(((ActivityC13830kP) this).A01, j)};
        } else {
            i = R.string.register_server_sms_next_method;
            objArr = new Object[]{getString(R.string.verify_voice_call_button)};
        }
        return getString(i, objArr);
    }

    public final String A2l() {
        int i;
        Object[] objArr;
        long A2g = A2g();
        long j = -1;
        if (A2g != -1) {
            j = A2g - System.currentTimeMillis();
        }
        StringBuilder sb = new StringBuilder("verifyphonenumber/voice-retry-time/diff/");
        sb.append(j);
        Log.i(sb.toString());
        if (j > 0) {
            i = R.string.register_server_sms_too_many_tries_try_voice_with_wait_time;
            objArr = new Object[]{getString(R.string.verify_voice_call_button), C38131nZ.A08(((ActivityC13830kP) this).A01, j)};
        } else {
            i = R.string.register_server_sms_too_many_tries_try_voice;
            objArr = new Object[]{getString(R.string.verify_voice_call_button)};
        }
        return getString(i, objArr);
    }

    public final String A2m() {
        int i;
        Object[] objArr;
        long A2f = A2f();
        long currentTimeMillis = System.currentTimeMillis();
        long j = -1;
        if (A2f != -1) {
            j = A2f - currentTimeMillis;
        }
        StringBuilder sb = new StringBuilder("verifyphonenumber/sms-retry-time/diff/");
        sb.append(j);
        Log.i(sb.toString());
        if (j > 0) {
            i = R.string.register_server_voice_next_method_with_wait_time;
            objArr = new Object[]{A2j(), C38131nZ.A08(((ActivityC13830kP) this).A01, j)};
        } else {
            i = R.string.register_server_voice_next_method;
            objArr = new Object[]{A2j()};
        }
        return getString(i, objArr);
    }

    public final String A2n() {
        int i;
        Object[] objArr;
        long A2f = A2f();
        long currentTimeMillis = System.currentTimeMillis();
        long j = -1;
        if (A2f != -1) {
            j = A2f - currentTimeMillis;
        }
        StringBuilder sb = new StringBuilder("verifyphonenumber/sms-retry-time/diff/");
        sb.append(j);
        Log.i(sb.toString());
        if (A2f > currentTimeMillis) {
            i = R.string.register_server_voice_too_many_tries_try_sms_with_wait_time;
            objArr = new Object[]{A2j(), C38131nZ.A08(((ActivityC13830kP) this).A01, j)};
        } else {
            i = R.string.register_server_voice_too_many_tries_try_sms;
            objArr = new Object[]{A2j()};
        }
        return getString(i, objArr);
    }

    public final void A2o() {
        if (this.A0D != null) {
            Log.i("verifyphonenumber/cancel-primary-flash-call-timer");
            this.A0D.cancel();
            this.A0D = null;
        }
    }

    public final void A2p() {
        int i;
        this.A0U.A00();
        AnonymousClass1I0 A07 = ((ActivityC13810kN) this).A07.A07();
        StringBuilder sb = new StringBuilder("verifyphonenumber/network/active ");
        sb.append(A07);
        Log.i(sb.toString());
        if (A07 == null) {
            i = -1;
        } else if (A07.A04) {
            i = 0;
        } else {
            i = 99;
            if (A07.A06) {
                i = 1;
            }
        }
        int i2 = this.A04;
        if (i != i2) {
            StringBuilder sb2 = new StringBuilder("verifyphonenumber/network/switch old=");
            sb2.append(i2);
            sb2.append(" new=");
            sb2.append(i);
            Log.i(sb2.toString());
            this.A04 = i;
            if (i != -1 && this.A0p.hasMessages(1)) {
                Log.i("verifyphonenumber/network/switch/has-retry-pending");
                this.A0p.removeMessages(1);
                this.A01 = 0;
                String A2i = A2i();
                if (A2i != null) {
                    this.A0p.sendMessage(this.A0p.obtainMessage(1, A2i));
                    return;
                }
                Log.e("verifyphonenumber/network/switch/no-saved-code");
            }
        }
    }

    public final void A2q() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.code_verification_retry_time");
        if (!edit.commit()) {
            Log.e("verifyphonenumber/clear-code-verification-retry-time/error");
        }
    }

    public final void A2r() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.sms_code");
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.sms_cc");
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.sms_phone_number");
        if (!edit.commit()) {
            Log.w("verifyphonenumber/savedcode/clear/commit failed");
        }
    }

    public final void A2s() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.sms_request_failed_retry_time");
        if (!edit.commit()) {
            Log.e("verifyphonenumber/clear-sms-retry-time/error");
        }
    }

    public final void A2t() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.call_countdown_end_time");
        if (!edit.commit()) {
            Log.e("verifyphonenumber/clear-voice-retry-time/error");
        }
    }

    public final void A2u() {
        String str;
        C14820m6 r6 = ((ActivityC13810kN) this).A09;
        C25961Bm r5 = this.A0c;
        boolean z = this.A15;
        int i = r6.A00.getInt("autoconf_type", -1);
        StringBuilder sb = new StringBuilder("RegistrationHelper/shouldCreateAutoconfVerifier/AUTOCONF_ENABLED=");
        sb.append(true);
        sb.append(", autoconfSameDeviceCheck=");
        sb.append(z);
        sb.append(", autoconfManager=");
        if (r5 == null) {
            str = "null";
        } else {
            str = "nonnull";
        }
        sb.append(str);
        sb.append(", autoconfType=");
        sb.append(i);
        Log.i(sb.toString());
        if (i == 1) {
            r6.A0c("autoconf_server_enabled");
        }
        if (!z || r5 == null || i != 1) {
            Log.i("VerifyPhoneNumber/should not create autoconf verifier");
            return;
        }
        Log.i("VerifyPhoneNumber/attempt to create autoconf verifier");
        AnonymousClass04S r0 = this.A0L;
        if (r0 != null) {
            r0.dismiss();
            this.A0L = null;
        }
        AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
        String str2 = this.A0x;
        String str3 = this.A0y;
        C20800wL r52 = this.A0l;
        C14820m6 r3 = ((ActivityC13810kN) this).A09;
        C25961Bm r4 = this.A0c;
        AnonymousClass009.A05(r4);
        r1.Aaz(new C626237y(r3, r4, r52, str2, str3), new String[0]);
    }

    public final void A2v() {
        try {
            startActivity(new Intent("android.intent.action.VIEW", this.A0v.A01().appendPath("link").appendPath("verification.php").appendQueryParameter("platform", "android").appendQueryParameter("lc", ((ActivityC13830kP) this).A01.A05()).appendQueryParameter("lg", ((ActivityC13830kP) this).A01.A06()).build()));
        } catch (ActivityNotFoundException unused) {
            ((ActivityC13810kN) this).A05.A07(R.string.activity_not_found, 0);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2w() {
        /*
        // Method dump skipped, instructions count: 279
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.A2w():void");
    }

    public final void A2x() {
        if (this.A0i.A02 || AJN()) {
            AnonymousClass23M.A0G(this, this.A0W, -1);
        }
    }

    public final void A2y() {
        Log.i("VerifyPhoneNumber/removeProgressDialog");
        ProgressDialog progressDialog = this.A0B;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.A0B = null;
    }

    public final void A2z() {
        Log.i("verifyphonenumber/resetAllVerificationState");
        A3B(0);
        HandlerC52092aE r0 = this.A0p;
        if (r0 != null && r0.hasMessages(1)) {
            this.A0p.removeMessages(1);
        }
        A2t();
        A2s();
        A2q();
        A3A(0);
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.remove("com.whatsapp.registration.VerifyPhoneNumber.account_defence_sms_requested_once");
        if (!edit.commit()) {
            Log.e("AccountDefenceVerificationHelper/reset-is2ndOtpSMSRequestedOnce/error");
        }
        this.A14 = false;
        SharedPreferences.Editor edit2 = getPreferences(0).edit();
        edit2.remove("com.whatsapp.registration.VerifyPhoneNumber.account_defence_original_sms_wait_time");
        edit2.remove("com.whatsapp.registration.VerifyPhoneNumber.account_defence_original_voice_wait_time");
        if (!edit2.commit()) {
            Log.e("verifyphonenumber/reset-original-wait-time-diffs/error");
        }
    }

    public final void A30() {
        Intent A05;
        A2z();
        if (this.A10) {
            Log.i("verifyphonenumber/returntoregphone/changenumber/setregverified");
            this.A0m.A0A(3);
            if (!this.A0m.A0E()) {
                finish();
            }
            A05 = new Intent();
            A05.setClassName(getPackageName(), "com.whatsapp.registration.ChangeNumber");
        } else {
            this.A0m.A0A(1);
            A05 = C14960mK.A05(this);
            A05.putExtra("com.whatsapp.registration.RegisterPhone.clear_phone_number", true);
        }
        startActivity(A05);
        finish();
    }

    public final void A31() {
        if (((ActivityC13790kL) this).A0B.A00() != 8) {
            long A2f = A2f();
            long currentTimeMillis = System.currentTimeMillis();
            long j = -1;
            if (A2f != -1) {
                j = A2f - currentTimeMillis;
            }
            if (A2f > currentTimeMillis) {
                this.A0m.A0B(j);
            }
        }
    }

    public final void A32() {
        CountDownTimer countDownTimer = this.A0C;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A0C = null;
            A2q();
            this.A0I.setProgress(100);
            this.A0K.setText(getString(R.string.verify_description_bottom, Integer.valueOf(A1D)));
            this.A11 = false;
            if (this.A0f.A03()) {
                this.A0f.A02(true);
            } else {
                this.A0M.setEnabled(true);
            }
        }
    }

    public final void A33() {
        if (this.A1A) {
            if (this.A19) {
                unregisterReceiver(this.A0n);
                this.A19 = false;
            }
        } else if (this.A16) {
            unregisterReceiver(this.A0j);
            this.A16 = false;
        }
    }

    public final void A34() {
        int A00 = ((ActivityC13790kL) this).A0B.A00();
        C14820m6 r1 = ((ActivityC13810kN) this).A09;
        if (A00 == 8) {
            r1.A0g("primary_successful");
        } else {
            r1.A0j("secondary_successful");
        }
    }

    public final void A35() {
        this.A0K.setText(R.string.register_user_is_banned_bottom);
        this.A0R.setVisibility(8);
        this.A0F.setVisibility(8);
        this.A0G.setVisibility(8);
        this.A0J.setVisibility(8);
        C36021jC.A01(this, 124);
    }

    public final void A36(int i) {
        A2y();
        AIT();
        if (((ActivityC13790kL) this).A0B.A00() == 12 && AnonymousClass1CE.A00(((ActivityC13810kN) this).A08, i)) {
            Log.i("VerifyPhoneNumber/fall back to flash call");
            A2G(C14960mK.A0A(this, A2f(), A2g(), false), true);
        } else if (A3n()) {
            Log.i("VerifyPhoneNumber/fall back to sms");
            A3G(A2f(), A2g());
        }
    }

    public final void A37(int i) {
        if (this.A0i.A02 || AJN()) {
            AnonymousClass23M.A0G(this, this.A0W, i);
        } else {
            C36021jC.A01(this, i);
        }
    }

    public final void A38(int i) {
        String string = getString(i);
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/showProgressDialog/");
        sb.append(string);
        Log.i(sb.toString());
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(string);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        this.A0B = progressDialog;
    }

    public final void A39(int i) {
        if (!A3n()) {
            this.A0i.A01(i);
        }
    }

    public final void A3A(int i) {
        this.A00 = i;
        SharedPreferences preferences = getPreferences(0);
        StringBuilder sb = new StringBuilder("AccountDefenceVerificationHelper/save-acct-defence-state/");
        sb.append(i);
        Log.i(sb.toString());
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt("com.whatsapp.registration.VerifyPhoneNumber.account_defence_verification_state", i);
        if (!edit.commit()) {
            Log.e("AccountDefenceVerificationHelper/save-acct-defence-state/error");
        }
        this.A0R.setText(this.A0b.A02(this, ((ActivityC13830kP) this).A01.A0G(AnonymousClass23M.A0E(this.A0x, this.A0y)), this.A00));
    }

    public final void A3B(int i) {
        A1F = i;
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt("com.whatsapp.registration.VerifyPhoneNumber.verification_state", A1F);
        if (!edit.commit()) {
            Log.w("verifyphonenumber/savestate/commit failed");
        }
    }

    public final void A3C(long j) {
        StringBuilder sb = new StringBuilder("verifyphonenumber/save-sms-retry-time/");
        sb.append(j);
        Log.i(sb.toString());
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.sms_request_failed_retry_time", j);
        if (!edit.commit()) {
            Log.e("verifyphonenumber/save-sms-retry-time/error");
        }
    }

    public final void A3D(long j) {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.call_countdown_end_time", j);
        if (!edit.commit()) {
            Log.e("verifyphonenumber/save-voice-retry-time/error");
        }
    }

    public final void A3E(long j) {
        CountDownTimer countDownTimer = this.A0C;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A0C = null;
        }
        if (j < 1000) {
            A2q();
            return;
        }
        this.A11 = true;
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.code_verification_retry_time", System.currentTimeMillis() + j);
        if (!edit.commit()) {
            Log.e("verifyphonenumber/save-code-verification-retry-time/error");
        }
        if (this.A0f.A03()) {
            this.A0f.A02(false);
        } else {
            this.A0M.setEnabled(false);
        }
        this.A0I.setProgress(0);
        this.A0I.setVisibility(0);
        this.A0K.setText(R.string.verify_description_bottom_code_input_disable);
        this.A0C = new CountDownTimerC51942Zt(this, j, j).start();
    }

    public final void A3F(long j) {
        if (!A3n()) {
            this.A0m.A0B(j);
        }
    }

    public final void A3G(long j, long j2) {
        Log.i("verifyphonenumber/restartactivitywithsmsverification");
        A3B(0);
        if (((ActivityC13790kL) this).A0B.A00() == 8) {
            ((ActivityC13810kN) this).A09.A0g("primary_failed");
        }
        C36021jC.A01(this, 40);
        ((ActivityC13810kN) this).A05.A0J(new RunnableBRunnable0Shape0S0100200_I0(this, 1, j, j2), 1500);
    }

    public final void A3H(long j, long j2) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/updateSmsGroupUI/originalSmsWaitTime=");
        sb.append(j);
        sb.append(", remainingSmsWaitTime=");
        sb.append(j2);
        Log.i(sb.toString());
        if (j2 > 0) {
            if (((ActivityC13790kL) this).A0B.A00() != 8) {
                this.A0F.setVisibility(0);
            }
            this.A0e.A01(j2, true);
            A3C(j2 + System.currentTimeMillis());
        } else if (j < 0) {
            this.A0F.setVisibility(8);
        } else if (j == 0) {
            if (((ActivityC13790kL) this).A0B.A00() != 8) {
                this.A0F.setVisibility(0);
            }
            this.A0e.A02(true);
            if (AnonymousClass3J8.A01(A2e()) && this.A00 == 0) {
                A3A(1);
            }
            A2s();
        }
    }

    public final void A3I(long j, long j2) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/updateVoiceGroupUI/originalVoiceWaitTime=");
        sb.append(j);
        sb.append(", remainingVoiceWaitTime=");
        sb.append(j2);
        Log.i(sb.toString());
        if (j2 > 0) {
            int A00 = ((ActivityC13790kL) this).A0B.A00();
            if (!(((ActivityC13790kL) this).A0B.A00() == 8 || A00 == 9)) {
                this.A0G.setVisibility(0);
            }
            this.A0d.A01(j2, true);
            A3D(j2 + System.currentTimeMillis());
        } else if (j < 0) {
            this.A0G.setVisibility(8);
        } else if (j == 0) {
            int A002 = ((ActivityC13790kL) this).A0B.A00();
            if (!(((ActivityC13790kL) this).A0B.A00() == 8 || A002 == 9)) {
                this.A0G.setVisibility(0);
            }
            this.A0d.A02(true);
            if (AnonymousClass3J8.A01(A2e()) && this.A00 == 0) {
                A3A(1);
            }
            A2t();
        }
    }

    public final void A3J(C863046q r22, String str, String str2, String str3, String str4, String str5, boolean z) {
        String str6;
        int i;
        if (!AnonymousClass3J8.A01(A2e()) || (i = this.A00) == 2 || i == 3) {
            AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
            int i2 = ((ActivityC13810kN) this).A09.A00.getInt("pref_flash_call_education_link_clicked", -1);
            int i3 = ((ActivityC13810kN) this).A09.A00.getInt("pref_flash_call_manage_call_permission_granted", -1);
            int i4 = ((ActivityC13810kN) this).A09.A00.getInt("pref_flash_call_call_log_permission_granted", -1);
            C16590pI r4 = this.A0V;
            int i5 = this.A02;
            boolean z2 = true;
            if (i5 != 3) {
                z2 = false;
                if (i5 == 1) {
                    str6 = "account_defence";
                } else if (i5 != 3) {
                    str6 = null;
                }
                r1.Aaz(new C627438k(((ActivityC13810kN) this).A08, r4, ((ActivityC13810kN) this).A09, this.A0c, this.A0l, this, r22, str, str2, str3, str4, str5, str6, i2, i3, i4, z, z2), new String[0]);
                return;
            }
            str6 = "ban_appeal";
            r1.Aaz(new C627438k(((ActivityC13810kN) this).A08, r4, ((ActivityC13810kN) this).A09, this.A0c, this.A0l, this, r22, str, str2, str3, str4, str5, str6, i2, i3, i4, z, z2), new String[0]);
            return;
        }
        Log.w("VerifyPhoneNumber/executeRequestCodeTask should not request code yet, returning");
    }

    public final void A3K(C863046q r9, boolean z) {
        A3J(r9, this.A0x, this.A0y, "voice", null, null, z);
    }

    public final void A3L(C862946p r19, AnonymousClass4AY r20, String str, String str2, String str3, String str4, String str5) {
        String str6;
        AbstractC14440lR r4 = ((ActivityC13830kP) this).A05;
        C14820m6 r6 = ((ActivityC13810kN) this).A09;
        C20800wL r8 = this.A0l;
        C25961Bm r7 = this.A0c;
        int i = this.A02;
        boolean z = true;
        if (i != 3) {
            z = false;
            if (i == 1) {
                str6 = "account_defence";
            } else if (i != 3) {
                str6 = null;
            }
            r4.Aaz(new C626938f(r6, r7, r8, this, r19, r20, str2, str3, str4, str6, str5, z), str);
        }
        str6 = "ban_appeal";
        r4.Aaz(new C626938f(r6, r7, r8, this, r19, r20, str2, str3, str4, str6, str5, z), str);
    }

    public final void A3M(AnonymousClass1RD r10, String str) {
        StringBuilder sb = new StringBuilder("verify/request/");
        sb.append(str);
        sb.append("/code/ok");
        Log.i(sb.toString());
        ((ActivityC13810kN) this).A09.A12(r10.A0I);
        A3j(this.A0x, this.A0y, r10.A0A, null, -1, r10.A0I);
    }

    public final void A3N(AnonymousClass1RD r5, String str) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/2fa");
        Log.i(sb.toString());
        A3i(r5.A0H, r5.A0G, r5.A03);
    }

    public final void A3O(AnonymousClass1RD r8, String str) {
        long A02;
        String str2;
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/updateProgresses: method:");
        sb.append(str);
        Log.i(sb.toString());
        if (str.equals("sms")) {
            String str3 = r8.A0E;
            if (str3 == null) {
                str3 = r8.A0D;
            }
            A02 = AnonymousClass23M.A02(str3, -1) * 1000;
            str2 = r8.A0F;
        } else if (str.equals("voice") || str.equals("flash")) {
            A02 = AnonymousClass23M.A02(r8.A0E, -1) * 1000;
            str2 = r8.A0F;
            if (str2 == null) {
                str2 = r8.A0D;
            }
        } else {
            StringBuilder sb2 = new StringBuilder("Invalid method: ");
            sb2.append(str);
            throw new IllegalArgumentException(sb2.toString());
        }
        long A022 = AnonymousClass23M.A02(str2, -1) * 1000;
        if (AnonymousClass3J8.A01(A2e())) {
            AnonymousClass3J8.A00(getPreferences(0), A02, A022);
        }
        A3H(A02, A02);
        A3I(A022, A022);
    }

    public final void A3P(AnonymousClass1RD r3, String str, String str2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/bad-parameter/");
        sb.append(r3.A0C);
        Log.e(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        if ("number".equals(r3.A0C)) {
            A39(33);
            return;
        }
        A39(24);
        if (str.equals("sms")) {
            A2x();
        }
    }

    public final void A3Q(AnonymousClass1RD r3, String str, String str2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/next-method");
        Log.w(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        A3O(r3, str);
        A39(35);
        A31();
    }

    public final void A3R(AnonymousClass1RD r8, String str, String str2, int i, int i2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/provider-unroutable");
        Log.e(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        String str3 = r8.A0D;
        if (str3 == null) {
            A3c(getString(i));
            return;
        }
        try {
            long parseLong = Long.parseLong(str3) * 1000;
            A3F(parseLong);
            A3c(getString(i2, C38131nZ.A08(((ActivityC13830kP) this).A01, parseLong)));
            A3D(System.currentTimeMillis() + parseLong);
            this.A0d.A01(parseLong, true);
        } catch (NumberFormatException e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("verify");
            sb2.append(str);
            sb2.append("/request/");
            sb2.append(str);
            sb2.append("/unroutable/time-not-int");
            Log.w(sb2.toString(), e);
            A3c(getString(i));
        }
    }

    public final void A3S(AnonymousClass1RD r8, String str, String str2, String str3, int i, int i2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/no-routes");
        Log.w(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        String str4 = r8.A0D;
        if (str4 == null) {
            A3c(getString(i));
            this.A0o.A02(str3);
            return;
        }
        try {
            long parseLong = Long.parseLong(str4) * 1000;
            A3F(parseLong);
            A3c(getString(i2, C38131nZ.A08(((ActivityC13830kP) this).A01, parseLong)));
            A3D(System.currentTimeMillis() + parseLong);
            this.A0d.A01(parseLong, true);
            this.A0o.A02(str3);
        } catch (NumberFormatException e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("verify");
            sb2.append(str);
            sb2.append("/request/");
            sb2.append(str);
            sb2.append("/no-routes/time-not-int");
            Log.w(sb2.toString(), e);
            A3c(getString(i));
        }
    }

    public final void A3T(AnonymousClass1RC r3, String str, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        if (r3 == AnonymousClass1RC.ERROR_BAD_TOKEN) {
            str3 = "/bad-token";
        } else {
            str3 = "/invalid-skey";
        }
        sb.append(str3);
        Log.e(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        A39(38);
    }

    public final void A3U(AnonymousClass1R9 r8, String str) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/");
        sb.append(str);
        sb.append("/onAccountDefenceSecondCodeRequired");
        Log.i(sb.toString());
        if (str.equals("flash")) {
            A34();
        }
        A3e(str, r8.A02, r8.A03);
    }

    public final void A3V(AnonymousClass1R9 r5, String str) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/");
        sb.append(str);
        sb.append("/onCodeVerified");
        Log.i(sb.toString());
        Log.i("RegistrationUtils/showVerificationCompleteDialog");
        AnonymousClass04S r1 = null;
        if (!AnonymousClass12P.A00(this).isFinishing()) {
            View inflate = View.inflate(this, R.layout.dialog_verification_complete, null);
            C004802e r0 = new C004802e(this);
            r0.setView(inflate);
            r1 = r0.A05();
        }
        this.A0L = r1;
        RunnableBRunnable0Shape0S1200000_I0 runnableBRunnable0Shape0S1200000_I0 = new RunnableBRunnable0Shape0S1200000_I0(this, r5, str, 29);
        if (r1 == null) {
            runnableBRunnable0Shape0S1200000_I0.run();
        } else {
            ((ActivityC13810kN) this).A05.A0J(runnableBRunnable0Shape0S1200000_I0, 1000);
        }
    }

    public final void A3W(AnonymousClass1R9 r5, String str) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/");
        sb.append(str);
        sb.append("/onSecurityCodeRequired");
        Log.i(sb.toString());
        if (str.equals("flash")) {
            A34();
        }
        A2u();
        A3i(r5.A0B, r5.A0A, r5.A04);
    }

    public void A3X(String str) {
        this.A15 = true;
        A3b(str);
        this.A01 = 0;
        String A0B = ((ActivityC13810kN) this).A09.A0B();
        String A0C = ((ActivityC13810kN) this).A09.A0C();
        A3L(this.A0q, AnonymousClass4AY.AUTO_DETECTED, str, A0B, A0C, "sms", null);
    }

    public void A3Y(String str) {
        int length;
        int i;
        if (str != null && (length = str.length()) == (i = A1E)) {
            if (length == i) {
                for (int i2 = 0; i2 < length; i2++) {
                    if (Character.isDigit(str.charAt(i2))) {
                    }
                }
                String str2 = this.A0x;
                String str3 = this.A0y;
                AnonymousClass009.A05(str3);
                A3L(this.A0q, AnonymousClass4AY.TYPED, str, str2, str3, "voice", null);
                return;
            }
            C36021jC.A01(this, 29);
        }
    }

    public final void A3Z(String str) {
        if (this.A11) {
            Log.i("verifyphonenumber/verificationlink/voice/code-entry-blocked-retry-later");
            A3b(str);
            return;
        }
        StringBuilder sb = new StringBuilder("verifyphonenumber/verificationlink/voice/state ");
        sb.append(A1F);
        Log.i(sb.toString());
        if (this.A0f.A03()) {
            C64333Fd r4 = this.A0f;
            if (TextUtils.isEmpty(str)) {
                r4.A01();
                return;
            }
            int i = 0;
            while (true) {
                ArrayList arrayList = r4.A09;
                if (i < arrayList.size()) {
                    AnonymousClass009.A05(str);
                    ((TextView) arrayList.get(i)).setText(String.valueOf(str.charAt(i)));
                    i++;
                } else {
                    return;
                }
            }
        } else {
            this.A0M.setText(str);
        }
    }

    public final void A3a(String str) {
        StringBuilder sb = new StringBuilder("verify/");
        sb.append(str);
        sb.append("/registration-not-allowed-error");
        Log.e(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, "not-allowed");
        this.A0i.A01(42);
    }

    public final void A3b(String str) {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putString("com.whatsapp.registration.VerifyPhoneNumber.sms_code", str);
        edit.putString("com.whatsapp.registration.VerifyPhoneNumber.sms_cc", this.A0x);
        edit.putString("com.whatsapp.registration.VerifyPhoneNumber.sms_phone_number", this.A0y);
        if (!edit.commit()) {
            Log.w("verifyphonenumber/savedcode/save/commit failed");
        }
    }

    public final void A3c(String str) {
        if (!A3n()) {
            this.A0i.A03(str);
        }
    }

    public final void A3d(String str) {
        if (A1F == 12) {
            this.A0J.setVisibility(8);
            this.A0F.setVisibility(8);
            this.A0G.setVisibility(8);
            return;
        }
        if (!str.equals("flash")) {
            this.A0J.setVisibility(0);
        }
        long A2g = A2g();
        if (A2g != -1) {
            long currentTimeMillis = A2g - System.currentTimeMillis();
            if (currentTimeMillis > 0) {
                this.A0d.A01(currentTimeMillis, true);
            } else {
                A2t();
            }
        }
    }

    public final void A3e(String str, long j, long j2) {
        StringBuilder sb = new StringBuilder("VerifyPhoneNumber/");
        sb.append(str);
        sb.append("/launchAccountDefenceSecondCodeFlow smsWait: ");
        sb.append(j);
        sb.append(", voiceWait: ");
        sb.append(j2);
        Log.i(sb.toString());
        this.A0r.A02(A2h(), "successful");
        this.A0m.A0A(13);
        A3B(0);
        long currentTimeMillis = System.currentTimeMillis();
        long j3 = j * 1000;
        long j4 = j2 * 1000;
        long j5 = j3 + currentTimeMillis;
        long j6 = currentTimeMillis + j4;
        long min = Math.min(j5, j6);
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.code_verification_retry_time", min);
        if (!edit.commit()) {
            Log.e("verifyphonenumber/save-code-verification-retry-time/error");
        }
        AnonymousClass3J8.A00(getPreferences(0), j3, j4);
        A31();
        C36021jC.A01(this, 41);
        ((ActivityC13810kN) this).A05.A0J(new RunnableBRunnable0Shape0S0100200_I0(this, 2, j5, j6), 1500);
    }

    public final void A3f(String str, String str2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/blocked");
        Log.e(sb.toString());
        A3B(12);
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        A35();
    }

    public final void A3g(String str, String str2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/missing-parameter");
        Log.e(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        A39(25);
    }

    public final void A3h(String str, String str2) {
        StringBuilder sb = new StringBuilder("verify");
        sb.append(str);
        sb.append("/request/");
        sb.append(str);
        sb.append("/unspecified");
        Log.w(sb.toString());
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, str2);
        A39(109);
    }

    public final void A3i(String str, String str2, long j) {
        this.A0m.A0A(7);
        ((ActivityC13810kN) this).A09.A0w(str, str2, j, -1, -1, ((ActivityC13790kL) this).A05.A00());
        this.A0r.A02(A2h(), "successful");
        boolean z = this.A10;
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.registration.VerifyTwoFactorAuth");
        intent.putExtra("changenumber", z);
        A2G(intent, false);
        finish();
    }

    public final void A3j(String str, String str2, String str3, String str4, int i, boolean z) {
        A3B(0);
        this.A0p.removeMessages(1);
        A2r();
        if (this.A02 == 3) {
            this.A0m.A0A(10);
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.userban.ui.BanAppealActivity");
            intent.putExtra("appeal_request_token", str4);
            intent.putExtra("ban_violation_type", i);
            intent.putExtra("launch_source", 2);
            startActivity(intent);
        } else {
            Log.i("verifyphonenumber/registrationhasbeenverified");
            ((ActivityC13810kN) this).A09.A12(z);
            this.A0m.A0C(str, str2, str3);
            this.A0m.A04();
            if (!this.A10 || this.A0m.A0D()) {
                this.A0r.A02(A2h(), "successful");
                A2u();
                if (this.A0i.A02) {
                    AnonymousClass23M.A0H(this, this.A0W, this.A0m, this.A10);
                } else if (this.A10) {
                    startActivity(C14960mK.A04(this));
                }
                A2y();
                this.A0m.A0A(2);
                Intent intent2 = new Intent();
                intent2.setClassName(getPackageName(), "com.whatsapp.registration.RegisterName");
                startActivity(intent2);
                ((ActivityC13810kN) this).A09.A00.edit().remove("flash_call_eligible").remove("is_first_flash_call_request").remove("pref_flash_call_education_link_clicked").remove("pref_flash_call_manage_call_permission_granted").remove("pref_flash_call_call_log_permission_granted").remove("pref_flash_call_education_screen_displayed").remove("pref_prefer_sms_over_flash").apply();
                this.A0g.A01();
                this.A0h.A05(false);
            }
        }
        finish();
    }

    public final void A3k(boolean z) {
        int i;
        ImageButton imageButton = this.A0H;
        int i2 = 4;
        if (z) {
            i2 = 0;
        }
        imageButton.setVisibility(i2);
        if (this.A0f.A03()) {
            RelativeLayout relativeLayout = this.A0J;
            int i3 = 16;
            if (z) {
                i3 = 1;
            }
            relativeLayout.setGravity(i3);
            C64333Fd r4 = this.A0f;
            boolean z2 = !z;
            Iterator it = r4.A09.iterator();
            while (it.hasNext()) {
                TextView textView = (TextView) it.next();
                if (!z2) {
                    i = r4.A06;
                } else {
                    i = r4.A05;
                }
                textView.setWidth(i);
                textView.setHeight(!z2 ? r4.A02 : r4.A01);
            }
            if (z) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.A0H.getLayoutParams();
                layoutParams.setMargins(this.A05, this.A07, this.A06, 0);
                int i4 = this.A08;
                layoutParams.width = i4;
                layoutParams.height = i4;
                this.A0H.setLayoutParams(layoutParams);
            }
        }
    }

    public final void A3l(boolean z) {
        Log.i("verifyphonenumber/request-flash");
        if (!this.A0X.A06()) {
            Log.i("verifyphonenumber/request-flash/request-permission");
            RequestPermissionActivity.A0N(this, this.A0X, 700, z);
            return;
        }
        Log.i("verifyphonenumber/request-flash/has-permission");
        A2w();
    }

    public final void A3m(boolean z) {
        String str;
        TelephonyManager A0N = ((ActivityC13810kN) this).A08.A0N();
        if (A0N != null) {
            A0N.getNetworkOperator();
            A0N.getNetworkOperatorName();
            A0N.getSimOperator();
            A0N.getSimOperatorName();
        }
        ((ActivityC13810kN) this).A09.A0i(null);
        C863046q r4 = new C863046q(((ActivityC13810kN) this).A09.A05());
        if (((ActivityC13810kN) this).A09.A00.getBoolean("migrate_from_consumer_app_directly", false)) {
            r4.A02 = true;
        }
        String str2 = this.A0x;
        String str3 = this.A0y;
        C15890o4 r1 = this.A0X;
        if (this.A1A) {
            str = "2";
        } else {
            str = r1.A02("android.permission.RECEIVE_SMS") == 0 ? "1" : "0";
        }
        A3J(r4, str2, str3, "sms", null, str, z);
    }

    public final boolean A3n() {
        return ((ActivityC13790kL) this).A0B.A00() == 8 || ((ActivityC13790kL) this).A0B.A00() == 12;
    }

    @Override // X.AbstractC44441yy
    public void AIT() {
        if (((ActivityC13790kL) this).A0B.A00() == 8) {
            Log.i("verifyphonenumber/hide-automatically-verifying-progress-dialog");
            A2y();
            return;
        }
        Log.i("verifyphonenumber/hide-verifying-progress-dialog");
        C36021jC.A00(this, 23);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r27 == null) goto L_0x0a37;
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x042f  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0496  */
    /* JADX WARNING: Removed duplicated region for block: B:318:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC44431yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOI(X.AnonymousClass1RD r27, X.AnonymousClass1RC r28, java.lang.String r29) {
        /*
        // Method dump skipped, instructions count: 2654
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.AOI(X.1RD, X.1RC, java.lang.String):void");
    }

    @Override // android.app.Activity
    public SharedPreferences getPreferences(int i) {
        return this.A0a.A01(getLocalClassName());
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 700) {
            return;
        }
        if (i2 == -1) {
            Log.i("verifyphonenumber/activity-result/permission-accepted/request-flash");
            A2w();
            return;
        }
        Log.i("verifyphonenumber/activity-result/permission-declined/request-voice");
        C863046q r3 = new C863046q(((ActivityC13810kN) this).A09.A05());
        if (((ActivityC13810kN) this).A09.A00.getBoolean("migrate_from_consumer_app_directly", false)) {
            r3.A02 = true;
        }
        r3.A00 = false;
        A3K(r3, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0521, code lost:
        if (r2 != false) goto L_0x0523;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x07ab, code lost:
        if (((X.ActivityC13790kL) r27).A0B.A00() == 9) goto L_0x06ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x07bc, code lost:
        if (r27.A0X.A02("android.permission.RECEIVE_SMS") == 0) goto L_0x07be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0256, code lost:
        if (r6 != false) goto L_0x0258;
     */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x051c  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0596  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x05ea  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0682  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x06cf  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x06dd  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0716  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x076a  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x07a3  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x07b3  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x07c7  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x07e6  */
    /* JADX WARNING: Removed duplicated region for block: B:216:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r28) {
        /*
        // Method dump skipped, instructions count: 2151
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.onCreate(android.os.Bundle):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x008a  */
    @Override // android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog onCreateDialog(int r18) {
        /*
        // Method dump skipped, instructions count: 966
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.onCreateDialog(int):android.app.Dialog");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        if (!this.A10 && AnonymousClass3J8.A01(A2e())) {
            menu.add(0, 2, 0, R.string.register_again_menu_item_text);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        Log.i("verifyphonenumber/ondestroy");
        A33();
        this.A0g.A01();
        A2o();
        CountDownTimer countDownTimer = this.A0C;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A0C = null;
        }
        AnonymousClass3FQ r0 = this.A0d;
        if (r0 != null) {
            r0.A02(true);
        }
        AnonymousClass3FQ r02 = this.A0e;
        if (r02 != null) {
            r02.A02(true);
        }
        ((ActivityC13810kN) this).A07.A04(this.A1C);
        this.A0k.A01();
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        Log.i("verifyphonenumber/intent");
        super.onNewIntent(intent);
        String A02 = A02(intent);
        if (A02 == null) {
            int intExtra = intent.getIntExtra("com.whatsapp.verifynumber.dialog", 0);
            int i = 21;
            if (intExtra != 21) {
                i = 22;
                if (intExtra != 22) {
                    StringBuilder sb = new StringBuilder("verifyphonenumber/intent/unknown ");
                    sb.append(intExtra);
                    Log.i(sb.toString());
                    return;
                }
            }
            C36021jC.A01(this, i);
        } else if (this.A12) {
            A3Z(A02);
        } else {
            StringBuilder sb2 = new StringBuilder("verifyphonenumber/intent/defer-code/");
            sb2.append(A02);
            Log.i(sb2.toString());
            this.A0w = A02;
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent intent;
        int itemId = menuItem.getItemId();
        if (itemId != 0) {
            if (itemId == 1) {
                this.A0m.A09();
                A2z();
                intent = C14960mK.A01(this);
            } else if (itemId != 2) {
                return super.onOptionsItemSelected(menuItem);
            } else {
                this.A0m.A09();
                A2z();
                intent = new Intent().setClassName(getPackageName(), "com.whatsapp.registration.RegisterPhone");
                intent.putExtra("com.whatsapp.registration.RegisterPhone.resetstate", true);
                intent.putExtra("com.whatsapp.registration.RegisterPhone.clear_phone_number", true);
            }
            startActivity(intent);
            finishAffinity();
            return true;
        }
        AnonymousClass3HG r3 = this.A0k;
        AnonymousClass10O r2 = this.A0o;
        StringBuilder sb = new StringBuilder("verify-sms +");
        sb.append(this.A0x);
        sb.append(this.A0y);
        r3.A02(this, r2, sb.toString());
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        String code;
        StringBuilder sb = new StringBuilder("verifyphonenumber/pause ");
        sb.append(A1F);
        Log.i(sb.toString());
        super.onPause();
        C64323Fc r1 = this.A0i;
        r1.A02 = true;
        AnonymousClass23M.A0J(r1.A04, AnonymousClass23M.A00);
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt("com.whatsapp.registration.VerifyPhoneNumber.verification_state", A1F);
        if (!edit.commit()) {
            Log.w("verifyphonenumber/pause/commit failed");
        }
        if (this.A0f.A03()) {
            code = this.A0f.A00();
        } else {
            code = this.A0M.getCode();
        }
        if (!TextUtils.isEmpty(code)) {
            ((ActivityC13810kN) this).A09.A0i(code);
        }
    }

    @Override // android.app.Activity
    public void onPrepareDialog(int i, Dialog dialog) {
        AnonymousClass04S r4;
        String str;
        if (i != 26) {
            switch (i) {
                case 35:
                    r4 = (AnonymousClass04S) dialog;
                    str = A2k();
                    break;
                case 36:
                    r4 = (AnonymousClass04S) dialog;
                    str = A2m();
                    break;
                case 37:
                    r4 = (AnonymousClass04S) dialog;
                    str = A2l();
                    break;
                default:
                    return;
            }
        } else {
            r4 = (AnonymousClass04S) dialog;
            str = A2n();
        }
        AnonymousClass0U5 r0 = r4.A00;
        r0.A0Q = str;
        TextView textView = r0.A0K;
        if (textView != null) {
            textView.setText(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a7, code lost:
        if (((X.ActivityC13790kL) r9).A0B.A00() == 8) goto L_0x00a9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0111  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
        // Method dump skipped, instructions count: 474
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.VerifyPhoneNumber.onResume():void");
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("use_sms_retriever", this.A1A);
        super.onSaveInstanceState(bundle);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        AnonymousClass04S r0 = this.A0L;
        if (r0 != null) {
            r0.dismiss();
            this.A0L = null;
        }
    }
}
