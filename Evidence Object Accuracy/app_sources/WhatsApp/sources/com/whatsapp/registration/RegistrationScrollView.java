package com.whatsapp.registration;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.ViewTreeObserver$OnScrollChangedListenerC102124od;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.registration.RegistrationScrollView;

/* loaded from: classes2.dex */
public class RegistrationScrollView extends ScrollView implements AnonymousClass004 {
    public View A00;
    public View A01;
    public ViewTreeObserver.OnGlobalLayoutListener A02;
    public LinearLayout A03;
    public WaTextView A04;
    public AnonymousClass2P7 A05;
    public boolean A06;
    public boolean A07;
    public final float A08;
    public final ViewTreeObserver.OnScrollChangedListener A09;

    public RegistrationScrollView(Context context) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        this.A08 = getResources().getDimension(R.dimen.actionbar_elevation);
        this.A09 = new ViewTreeObserver$OnScrollChangedListenerC102124od(this);
    }

    public RegistrationScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A08 = getResources().getDimension(R.dimen.actionbar_elevation);
        this.A09 = new ViewTreeObserver$OnScrollChangedListenerC102124od(this);
    }

    public RegistrationScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        this.A08 = getResources().getDimension(R.dimen.actionbar_elevation);
        this.A09 = new ViewTreeObserver$OnScrollChangedListenerC102124od(this);
    }

    public RegistrationScrollView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
    }

    public static /* synthetic */ void A00(View view, View view2, LinearLayout linearLayout, WaTextView waTextView, RegistrationScrollView registrationScrollView) {
        if (!registrationScrollView.A06) {
            registrationScrollView.getViewTreeObserver().addOnScrollChangedListener(registrationScrollView.A09);
            registrationScrollView.A06 = true;
        }
        boolean canScrollVertically = registrationScrollView.canScrollVertically(1);
        boolean canScrollVertically2 = registrationScrollView.canScrollVertically(-1);
        if (canScrollVertically2 || canScrollVertically) {
            if (canScrollVertically) {
                if (Build.VERSION.SDK_INT < 21) {
                    view.setVisibility(0);
                }
                AnonymousClass028.A0V(waTextView, registrationScrollView.A08);
            }
            if (canScrollVertically2) {
                if (Build.VERSION.SDK_INT < 21) {
                    view2.setVisibility(0);
                }
                AnonymousClass028.A0V(linearLayout, registrationScrollView.A08);
                return;
            }
            return;
        }
        if (Build.VERSION.SDK_INT < 21) {
            view.setVisibility(8);
            view2.setVisibility(8);
        }
        AnonymousClass028.A0V(linearLayout, 0.0f);
        AnonymousClass028.A0V(waTextView, 0.0f);
    }

    public static /* synthetic */ void A01(RegistrationScrollView registrationScrollView) {
        float f;
        float f2;
        float f3;
        boolean canScrollVertically = registrationScrollView.canScrollVertically(1);
        if (registrationScrollView.canScrollVertically(-1) || canScrollVertically) {
            int bottom = registrationScrollView.getChildAt(registrationScrollView.getChildCount() - 1).getBottom();
            int height = registrationScrollView.getHeight();
            int scrollY = registrationScrollView.getScrollY();
            float height2 = (float) (registrationScrollView.getChildAt(0).getHeight() - height);
            float f4 = ((float) scrollY) / height2;
            float f5 = ((float) (bottom - (height + scrollY))) / height2;
            WaTextView waTextView = registrationScrollView.A04;
            View view = registrationScrollView.A01;
            int i = (f4 > 0.1f ? 1 : (f4 == 0.1f ? 0 : -1));
            int i2 = Build.VERSION.SDK_INT;
            if (i < 0) {
                if (i2 < 21) {
                    view.setVisibility(0);
                    view.setAlpha(f4 * 10.0f);
                }
                f2 = registrationScrollView.A08;
                f = f4 * 10.0f * f2;
            } else {
                if (i2 < 21) {
                    view.setVisibility(0);
                }
                f = registrationScrollView.A08;
                f2 = f;
            }
            AnonymousClass028.A0V(waTextView, f);
            LinearLayout linearLayout = registrationScrollView.A03;
            View view2 = registrationScrollView.A00;
            int i3 = (f5 > 0.1f ? 1 : (f5 == 0.1f ? 0 : -1));
            int i4 = Build.VERSION.SDK_INT;
            if (i3 < 0) {
                if (i4 < 21) {
                    view2.setVisibility(0);
                    view2.setAlpha(f5 * 10.0f);
                }
                f3 = f5 * 10.0f * f2;
            } else {
                if (i4 < 21) {
                    view2.setVisibility(0);
                }
                f3 = f2;
            }
            AnonymousClass028.A0V(linearLayout, f3);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    public void setTopAndBottomScrollingElevation(LinearLayout linearLayout, WaTextView waTextView, View view, View view2) {
        this.A03 = linearLayout;
        this.A04 = waTextView;
        this.A01 = view;
        this.A00 = view2;
        this.A02 = new ViewTreeObserver.OnGlobalLayoutListener(view2, view, linearLayout, waTextView, this) { // from class: X.4o7
            public final /* synthetic */ View A00;
            public final /* synthetic */ View A01;
            public final /* synthetic */ LinearLayout A02;
            public final /* synthetic */ WaTextView A03;
            public final /* synthetic */ RegistrationScrollView A04;

            {
                this.A04 = r5;
                this.A00 = r1;
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
            }

            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                RegistrationScrollView.A00(this.A00, this.A01, this.A02, this.A03, this.A04);
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(this.A02);
    }
}
