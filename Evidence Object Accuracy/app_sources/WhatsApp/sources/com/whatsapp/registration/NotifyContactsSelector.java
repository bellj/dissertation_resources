package com.whatsapp.registration;

import X.AbstractActivityC36611kC;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12U;
import X.AnonymousClass2FL;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class NotifyContactsSelector extends AbstractActivityC36611kC {
    public AnonymousClass12U A00;
    public boolean A01;

    public NotifyContactsSelector() {
        this(0);
    }

    public NotifyContactsSelector(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 103);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A00 = ActivityC13830kP.A1N(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A2y(int i) {
        if (i <= 0) {
            A1U().A09(R.string.add_contacts_to_notify_change_number);
        } else {
            super.A2y(i);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 150) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 != -1) {
            Log.i("listmembersselector/permissions denied");
            finish();
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null && !((AbstractActivityC36611kC) this).A0I.A00()) {
            RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_notify_contacts_change_number_request, R.string.permission_contacts_access_on_notify_contacts_change_number);
        }
    }
}
