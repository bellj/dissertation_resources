package com.whatsapp.registration.report;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass4JN;
import X.AnonymousClass4JO;
import X.C20330va;
import X.C26891Ff;
import X.C44331yk;

/* loaded from: classes2.dex */
public class BanReportViewModel extends AnonymousClass015 {
    public String A00;
    public final AnonymousClass016 A01 = new AnonymousClass016();
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final AnonymousClass4JN A03;
    public final AnonymousClass4JO A04;
    public final C44331yk A05;
    public final C20330va A06;
    public final C26891Ff A07;
    public final AbstractC14440lR A08;

    public BanReportViewModel(AnonymousClass4JN r2, AnonymousClass4JO r3, C44331yk r4, C20330va r5, C26891Ff r6, AbstractC14440lR r7) {
        this.A08 = r7;
        this.A03 = r2;
        this.A06 = r5;
        this.A04 = r3;
        this.A05 = r4;
        this.A07 = r6;
    }
}
