package com.whatsapp.registration.directmigration;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C26991Fp;
import X.C44021y2;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class MigrationRequesterBroadcastReceiver extends BroadcastReceiver {
    public C14820m6 A00;
    public C26991Fp A01;
    public final Object A02;
    public volatile boolean A03;

    public MigrationRequesterBroadcastReceiver() {
        this(0);
    }

    public MigrationRequesterBroadcastReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = C12970iu.A0Z(r1);
                    this.A01 = (C26991Fp) r1.A5r.get();
                    this.A03 = true;
                }
            }
        }
        if (intent != null) {
            Log.i("MigrationRequesterBroadcastReceiver/received-broadcast");
            if ("com.whatsapp.registration.directmigration.providerAppMigrationSpaceNeededAction".equals(intent.getAction())) {
                long longExtra = intent.getLongExtra("extra_min_storage_needed", 0);
                long longExtra2 = intent.getLongExtra("extra_msg_db_size", 0);
                C44021y2 r4 = this.A01.A01;
                r4.A03 = Double.valueOf((double) longExtra);
                r4.A02 = Double.valueOf((double) longExtra2);
                C12970iu.A1C(C12960it.A08(this.A00), "registration_sibling_app_min_storage_needed", longExtra);
            }
        }
    }
}
