package com.whatsapp.registration.directmigration;

import X.AbstractC15850o0;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass02A;
import X.AnonymousClass15E;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.C12960it;
import X.C12980iv;
import X.C15830ny;
import X.C15860o1;
import X.C16490p7;
import X.C17050qB;
import X.C18350sJ;
import X.C19490uC;
import X.C19890uq;
import X.C20710wC;
import X.C20740wF;
import X.C20850wQ;
import X.C25651Af;
import X.C26991Fp;
import X.C27001Fq;
import X.C27011Fr;
import X.C27021Fs;
import X.C44121yH;
import X.C53972ft;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.backup.google.GoogleDriveRestoreAnimationView;
import com.whatsapp.components.RoundCornerProgressBar;

/* loaded from: classes2.dex */
public class RestoreFromConsumerDatabaseActivity extends ActivityC13790kL {
    public WaButton A00;
    public WaTextView A01;
    public WaTextView A02;
    public WaTextView A03;
    public WaTextView A04;
    public GoogleDriveRestoreAnimationView A05;
    public RoundCornerProgressBar A06;
    public C17050qB A07;
    public C19490uC A08;
    public C20850wQ A09;
    public C16490p7 A0A;
    public C27021Fs A0B;
    public C20710wC A0C;
    public C19890uq A0D;
    public C20740wF A0E;
    public C25651Af A0F;
    public C18350sJ A0G;
    public C26991Fp A0H;
    public C44121yH A0I;
    public C27011Fr A0J;
    public C27001Fq A0K;
    public AnonymousClass15E A0L;
    public C15860o1 A0M;
    public AbstractC15850o0 A0N;
    public C15830ny A0O;
    public boolean A0P;

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
    }

    public RestoreFromConsumerDatabaseActivity() {
        this(0);
    }

    public RestoreFromConsumerDatabaseActivity(int i) {
        this.A0P = false;
        ActivityC13830kP.A1P(this, 106);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0P) {
            this.A0P = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0D = (C19890uq) A1M.ABw.get();
            this.A08 = (C19490uC) A1M.A1A.get();
            this.A0B = (C27021Fs) A1M.A3R.get();
            this.A0C = C12980iv.A0e(A1M);
            this.A0O = (C15830ny) A1M.AKH.get();
            this.A0N = (AbstractC15850o0) A1M.ANA.get();
            this.A0M = (C15860o1) A1M.A3H.get();
            this.A07 = (C17050qB) A1M.ABL.get();
            this.A0E = (C20740wF) A1M.AIA.get();
            this.A0A = (C16490p7) A1M.ACJ.get();
            this.A0G = (C18350sJ) A1M.AHX.get();
            this.A0H = (C26991Fp) A1M.A5r.get();
            this.A0L = (AnonymousClass15E) A1M.ACW.get();
            this.A0J = (C27011Fr) A1M.AA2.get();
            this.A09 = (C20850wQ) A1M.ACI.get();
            this.A0K = (C27001Fq) A1M.ABF.get();
            this.A0F = (C25651Af) A1M.AFq.get();
        }
    }

    public final void A2e() {
        GoogleDriveRestoreAnimationView googleDriveRestoreAnimationView = this.A05;
        if (googleDriveRestoreAnimationView.A01 != 1) {
            googleDriveRestoreAnimationView.A01();
        }
        this.A01.setVisibility(0);
        this.A00.setVisibility(8);
        this.A04.setText(R.string.migration_title);
        this.A03.setText(R.string.migration_restore_from_source_app_do_not_close);
        this.A01.setText(R.string.migration_transferring_chats_and_media);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.restore_from_consumer_layout);
        this.A04 = (WaTextView) findViewById(R.id.restore_from_consumer_title);
        this.A03 = (WaTextView) findViewById(R.id.restore_from_consumer_sub_title);
        this.A01 = (WaTextView) findViewById(R.id.restore_from_consumer_bottom_info);
        this.A00 = (WaButton) findViewById(R.id.restore_from_consumer_action_btn);
        this.A02 = (WaTextView) findViewById(R.id.restore_from_consumer_progress_description);
        this.A06 = (RoundCornerProgressBar) findViewById(R.id.restore_from_consumer_progress_bar);
        this.A05 = (GoogleDriveRestoreAnimationView) findViewById(R.id.restore_from_consumer_animation_view);
        findViewById(R.id.restore_from_consumer_background_image).setBackgroundDrawable(AnonymousClass2GF.A00(this, ((ActivityC13830kP) this).A01, R.drawable.graphic_migration));
        C12960it.A12(this.A00, this, 30);
        A2e();
        C44121yH r0 = (C44121yH) new AnonymousClass02A(new C53972ft(this), this).A00(C44121yH.class);
        this.A0I = r0;
        C12960it.A18(this, r0.A02, 88);
        C12960it.A18(this, this.A0I.A04, 89);
    }
}
