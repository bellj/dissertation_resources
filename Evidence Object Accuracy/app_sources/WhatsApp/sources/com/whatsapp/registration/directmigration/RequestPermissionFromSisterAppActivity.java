package com.whatsapp.registration.directmigration;

import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.C12970iu;
import X.C20730wE;
import X.C21200x2;
import X.C249417m;
import X.C25661Ag;
import android.os.Bundle;
import com.whatsapp.RequestPermissionActivity;

/* loaded from: classes2.dex */
public class RequestPermissionFromSisterAppActivity extends RequestPermissionActivity {
    public boolean A00;

    public RequestPermissionFromSisterAppActivity() {
        this(0);
    }

    public RequestPermissionFromSisterAppActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 105);
    }

    @Override // X.AbstractActivityC457823d, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            ((RequestPermissionActivity) this).A05 = (C25661Ag) A1M.A8G.get();
            ((RequestPermissionActivity) this).A04 = (C21200x2) A1M.A2q.get();
            ((RequestPermissionActivity) this).A01 = (C20730wE) A1M.A4J.get();
            ((RequestPermissionActivity) this).A02 = C12970iu.A0Y(A1M);
            ((RequestPermissionActivity) this).A03 = C12970iu.A0Z(A1M);
            ((RequestPermissionActivity) this).A00 = (C249417m) A1M.A0S.get();
        }
    }

    @Override // com.whatsapp.RequestPermissionActivity
    public void A23(String str, Bundle bundle) {
        super.A23(A22(bundle, true), bundle);
    }
}
