package com.whatsapp.registration.directmigration;

import X.AbstractC15420nE;
import X.AbstractC15850o0;
import X.AnonymousClass009;
import X.AnonymousClass01T;
import X.C14330lG;
import X.C14820m6;
import X.C15450nH;
import X.C15570nT;
import X.C15810nw;
import X.C15820nx;
import X.C15830ny;
import X.C15860o1;
import X.C15880o3;
import X.C15890o4;
import X.C15900o5;
import X.C16490p7;
import X.C16550pE;
import X.C16560pF;
import X.C16600pJ;
import X.EnumC16570pG;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.JSONArray;

/* loaded from: classes2.dex */
public class MigrationContentProvider extends AbstractC15420nE {
    public static UriMatcher A0C;
    public static final List A0D;
    public C14330lG A00;
    public C15570nT A01;
    public C15450nH A02;
    public C15900o5 A03;
    public C15820nx A04;
    public C15810nw A05;
    public C15890o4 A06;
    public C14820m6 A07;
    public C15880o3 A08;
    public C15860o1 A09;
    public AbstractC15850o0 A0A;
    public C15830ny A0B;

    static {
        ArrayList arrayList = new ArrayList(Arrays.asList(new AnonymousClass01T("push_name", 2), new AnonymousClass01T("interface_gdrive_backup_frequency", 2), new AnonymousClass01T("interface_gdrive_backup_network_setting", 2), new AnonymousClass01T("gdrive_include_videos_in_backup", 1)));
        arrayList.addAll(C14820m6.A00());
        A0D = Collections.unmodifiableList(arrayList);
    }

    public static synchronized UriMatcher A01() {
        UriMatcher uriMatcher;
        synchronized (MigrationContentProvider.class) {
            if (A0C == null) {
                UriMatcher uriMatcher2 = new UriMatcher(-1);
                A0C = uriMatcher2;
                uriMatcher2.addURI("com.whatsapp.provider.MigrationContentProvider", "msg_store", 1);
                A0C.addURI("com.whatsapp.provider.MigrationContentProvider", "wallpaper", 2);
                A0C.addURI("com.whatsapp.provider.MigrationContentProvider", "chat_setting_store", 3);
                A0C.addURI("com.whatsapp.provider.MigrationContentProvider", "sticker_store", 4);
                A0C.addURI("com.whatsapp.provider.MigrationContentProvider", "share_preferences", 5);
                A0C.addURI("com.whatsapp.provider.MigrationContentProvider", "media", 7);
            }
            uriMatcher = A0C;
        }
        return uriMatcher;
    }

    public final int A02(Uri uri, int i) {
        return A03(uri.getQueryParameter("query_param_country_code"), uri.getQueryParameter("query_param_phone_number"), i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0067, code lost:
        if (r0 != false) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0077, code lost:
        if (r3 < r1) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0080, code lost:
        if (r11 == 268435456) goto L_0x0082;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A03(java.lang.String r9, java.lang.String r10, int r11) {
        /*
            r8 = this;
            android.content.Context r3 = r8.getContext()
            if (r3 != 0) goto L_0x0008
            r0 = 7
        L_0x0007:
            return r0
        L_0x0008:
            android.content.pm.PackageManager r1 = r3.getPackageManager()
            int r0 = android.os.Binder.getCallingUid()
            java.lang.String r2 = r1.getNameForUid(r0)
            r7 = 0
            android.content.pm.PackageManager r4 = r3.getPackageManager()     // Catch: RuntimeException -> 0x002b
            android.content.pm.ApplicationInfo r0 = r3.getApplicationInfo()     // Catch: RuntimeException -> 0x002b
            int r1 = r0.uid     // Catch: RuntimeException -> 0x002b
            int r0 = android.os.Binder.getCallingUid()     // Catch: RuntimeException -> 0x002b
            int r0 = r4.checkSignatures(r1, r0)     // Catch: RuntimeException -> 0x002b
            if (r0 != 0) goto L_0x003d
            r7 = 1
            goto L_0x003d
        L_0x002b:
            r4 = move-exception
            java.lang.String r1 = "FileSharingHelper/checkSameSignatureCaller. Error is : "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x003d:
            X.0nT r0 = r8.A01
            r0.A08()
            com.whatsapp.Me r1 = r0.A00
            r6 = 0
            if (r1 == 0) goto L_0x0058
            java.lang.String r0 = r1.cc
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x0058
            java.lang.String r0 = r1.number
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0058
            r6 = 1
        L_0x0058:
            java.lang.String r0 = "com.whatsapp.w4b"
            boolean r1 = r0.equals(r2)
            java.lang.String r0 = "com.whatsapp"
            boolean r0 = r0.equals(r2)
            if (r1 != 0) goto L_0x0069
            r5 = 0
            if (r0 == 0) goto L_0x006a
        L_0x0069:
            r5 = 1
        L_0x006a:
            if (r2 == 0) goto L_0x0079
            long r3 = X.AnonymousClass01U.A00(r3, r2)
            if (r1 == 0) goto L_0x0087
            r1 = 597(0x255, double:2.95E-321)
        L_0x0074:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r2 = 1
            if (r0 >= 0) goto L_0x007a
        L_0x0079:
            r2 = 0
        L_0x007a:
            r0 = -1
            if (r11 == r0) goto L_0x0082
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = 0
            if (r11 != r1) goto L_0x0083
        L_0x0082:
            r0 = 1
        L_0x0083:
            if (r7 != 0) goto L_0x008d
            r0 = 2
            return r0
        L_0x0087:
            if (r0 == 0) goto L_0x0079
            r1 = 452962(0x6e962, double:2.23793E-318)
            goto L_0x0074
        L_0x008d:
            if (r5 != 0) goto L_0x0091
            r0 = 3
            return r0
        L_0x0091:
            if (r2 != 0) goto L_0x0095
            r0 = 4
            return r0
        L_0x0095:
            if (r0 != 0) goto L_0x0099
            r0 = 5
            return r0
        L_0x0099:
            r0 = 0
            if (r6 != 0) goto L_0x0007
            r0 = 6
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.directmigration.MigrationContentProvider.A03(java.lang.String, java.lang.String, int):int");
    }

    public final File A04(String str) {
        if (!TextUtils.isEmpty(str) && this.A06.A07()) {
            File file = new File(this.A05.A04(), str);
            if (!this.A00.A0S(file) || !file.exists()) {
                return null;
            }
            return file;
        }
        return null;
    }

    public final void A05(MatrixCursor matrixCursor, File file, int i) {
        int length;
        if (i != 0) {
            File[] listFiles = file.listFiles();
            if (listFiles == null || (length = listFiles.length) == 0) {
                StringBuilder sb = new StringBuilder("MigrationContentProvider/fillMediaCursor/skipping folder ");
                sb.append(file);
                Log.i(sb.toString());
                return;
            }
            int i2 = 0;
            do {
                File file2 = listFiles[i2];
                if (file2.isDirectory()) {
                    A05(matrixCursor, file2, i - 1);
                } else {
                    matrixCursor.addRow(new String[]{file2.getAbsolutePath().replace(this.A05.A04().getAbsolutePath(), "")});
                }
                i2++;
            } while (i2 < length);
        }
    }

    @Override // android.content.ContentProvider
    public Bundle call(String str, String str2, Bundle bundle) {
        String str3;
        String str4;
        A01();
        if (bundle == null) {
            Log.e("MigrationContentProvider/call no params passed");
            str4 = "No params passed";
        } else {
            int A03 = A03(bundle.getString("query_param_country_code"), bundle.getString("query_param_phone_number"), -1);
            if (A03 != 0) {
                StringBuilder sb = new StringBuilder("MigrationContentProvider/call denied ");
                sb.append(A03);
                Log.w(sb.toString());
                StringBuilder sb2 = new StringBuilder("call denied (");
                sb2.append(A03);
                sb2.append(")");
                throw new SecurityException(sb2.toString());
            } else if (!"retrieve_rk".equals(str)) {
                StringBuilder sb3 = new StringBuilder("MigrationContentProvider/call failed/unsupported method ");
                sb3.append(str);
                Log.e(sb3.toString());
                StringBuilder sb4 = new StringBuilder("Unsupported method (");
                sb4.append(str);
                sb4.append(")");
                str4 = sb4.toString();
            } else {
                if (!this.A04.A04()) {
                    str3 = "MigrationContentProvider/retrieveRK/encryption disabled";
                } else {
                    byte[] byteArray = bundle.getByteArray("pk");
                    if (byteArray == null) {
                        Log.e("MigrationContentProvider/retrieveRK/no public key");
                        str4 = "No key provided";
                    } else {
                        byte[] A032 = this.A03.A03();
                        if (A032 == null) {
                            str3 = "MigrationContentProvider/retrieveRK/no root key";
                        } else {
                            try {
                                Bundle bundle2 = new Bundle(1);
                                bundle2.putByteArray("erk", C16550pE.A04(A032, byteArray));
                                C16560pF A00 = this.A03.A00();
                                if (this.A07.A00.getBoolean("encrypted_backup_using_encryption_key", false) || A00 == null) {
                                    return bundle2;
                                }
                                bundle2.putByteArray("ph", C16550pE.A04(A00.A01, byteArray));
                                bundle2.putByteArray("ps", C16550pE.A04(A00.A02, byteArray));
                                bundle2.putInt("ic", A00.A00);
                                return bundle2;
                            } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
                                Log.w("MigrationContentProvider/call encryption failed", e);
                                Bundle bundle3 = new Bundle();
                                bundle3.putString("error", "MigrationContentProvider/call encryption failed");
                                StringWriter stringWriter = new StringWriter();
                                e.printStackTrace(new PrintWriter(stringWriter));
                                bundle3.putString("exception", stringWriter.toString());
                                return bundle3;
                            }
                        }
                    }
                }
                Log.e(str3);
                return null;
            }
        }
        Bundle bundle4 = new Bundle();
        bundle4.putString("error", str4);
        return bundle4;
    }

    @Override // android.content.ContentProvider
    public Bundle call(String str, String str2, String str3, Bundle bundle) {
        A01();
        if (!"com.whatsapp.provider.MigrationContentProvider".equals(str)) {
            return null;
        }
        return call(str2, str3, bundle);
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        File A04;
        A01();
        if (A02(uri, -1) != 0 || A01().match(uri) != 7 || (A04 = A04(uri.getQueryParameter("path"))) == null || !A04.delete()) {
            return 0;
        }
        return 1;
    }

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        A01();
        if (A02(uri, -1) == 0) {
            if (1 == A01().match(uri)) {
                try {
                    Log.i("MigrationContentProvider/getType");
                    File A09 = this.A08.A09();
                    if (A09 != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("MigrationContentProvider/getType/msgstore-file-name = ");
                        sb.append(A09.getName());
                        Log.i(sb.toString());
                        return A09.getName();
                    }
                } catch (Exception e) {
                    Log.i("MigrationContentProvider/getType/exception = ", e);
                    return null;
                }
            } else {
                StringBuilder sb2 = new StringBuilder("This operation is not supported ");
                sb2.append(uri);
                throw new UnsupportedOperationException(sb2.toString());
            }
        }
        return null;
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        A01();
        return null;
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) {
        int i;
        int i2;
        File A09;
        String obj;
        A01();
        if ("r".equals(str)) {
            i = 268435456;
        } else if ("w".equals(str) || "wt".equals(str)) {
            i = 738197504;
        } else if ("wa".equals(str)) {
            i = 704643072;
        } else if ("rw".equals(str)) {
            i = 939524096;
        } else if ("rwt".equals(str)) {
            i = 1006632960;
        } else {
            StringBuilder sb = new StringBuilder("Invalid mode: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        int A02 = A02(uri, i);
        if (A02 == 0) {
            StringBuilder sb2 = new StringBuilder("MigrationContentProvider/openFile/");
            sb2.append(uri.getPath());
            Log.i(sb2.toString());
            int match = A01().match(uri);
            if (match == 1) {
                C15880o3 r6 = this.A08;
                C16600pJ r3 = r6.A0W;
                boolean z = true;
                synchronized (r3) {
                    r3.A00 = true;
                }
                C16490p7 r0 = r6.A0N;
                r0.A04();
                if (r0.A07.exists()) {
                    Log.i("messageStoreBackup/getFileForMigration/backup-db");
                    int A05 = r6.A05(null);
                    StringBuilder sb3 = new StringBuilder("messageStoreBackup/finish-backup-db-successful? = ");
                    if (A05 != 0) {
                        z = false;
                    }
                    sb3.append(z);
                    Log.i(sb3.toString());
                    if (A05 == 0) {
                        try {
                            A09 = r6.A09();
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("messageStoreBackup/getFileForMigration/latest-backup-file");
                            sb4.append(A09);
                            Log.i(sb4.toString());
                        } catch (IOException e) {
                            Log.e("messageStoreBackup/getFileForMigration/exception = ", e);
                            StringBuilder sb5 = new StringBuilder("messageStoreBackup/failed-to-get-backup-file");
                            sb5.append(e);
                            throw new FileNotFoundException(sb5.toString());
                        }
                    } else {
                        StringBuilder sb6 = new StringBuilder("messageStoreBackup/getFileForMigration/backup-failed/backup-result = ");
                        sb6.append(A05);
                        sb6.append(" log = ");
                        synchronized (r3) {
                            obj = r3.A01.toString();
                        }
                        sb6.append(obj);
                        throw new FileNotFoundException(sb6.toString());
                    }
                } else {
                    throw new FileNotFoundException("14");
                }
            } else if (match == 2) {
                A09 = this.A0A.A07();
            } else if (match == 3) {
                C15860o1 r32 = this.A09;
                ReentrantReadWriteLock.WriteLock writeLock = r32.A03().A04.writeLock();
                writeLock.lock();
                try {
                    A09 = r32.A0G.A00.getDatabasePath("chatsettings.db");
                    if (A09.exists()) {
                        r32.A03().close();
                    } else {
                        A09 = null;
                    }
                } finally {
                    writeLock.unlock();
                }
            } else if (match == 4) {
                A09 = this.A0B.A02(EnumC16570pG.A08);
            } else if (match == 7) {
                A09 = A04(uri.getQueryParameter("path"));
            } else {
                StringBuilder sb7 = new StringBuilder("Unknown URI ");
                sb7.append(uri);
                throw new IllegalArgumentException(sb7.toString());
            }
            if (A09 != null) {
                return ParcelFileDescriptor.open(A09, i);
            }
            return null;
        }
        switch (A02) {
            case 1:
                i2 = 8;
                break;
            case 2:
                i2 = 9;
                break;
            case 3:
                i2 = 10;
                break;
            case 4:
                i2 = 11;
                break;
            case 5:
                i2 = 12;
                break;
            case 6:
                i2 = 13;
                break;
            case 7:
                i2 = 0;
                break;
            default:
                throw new IllegalArgumentException("cannot convert granted to InitializationState");
        }
        throw new FileNotFoundException(String.valueOf(i2));
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        A01();
        int A02 = A02(uri, -1);
        if (A02 != 0) {
            StringBuilder sb = new StringBuilder("MigrationContentProvider/query denied ");
            sb.append(A02);
            Log.w(sb.toString());
            return null;
        }
        int match = A01().match(uri);
        if (match == 5) {
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{"key", "value", "valueType"});
            for (AnonymousClass01T r0 : A0D) {
                MatrixCursor.RowBuilder newRow = matrixCursor.newRow();
                Object obj = r0.A00;
                newRow.add(obj);
                Object obj2 = r0.A01;
                AnonymousClass009.A05(obj2);
                int intValue = ((Number) obj2).intValue();
                AnonymousClass009.A05(obj);
                String str4 = (String) obj;
                if (intValue != 0) {
                    if (intValue != 1) {
                        if (intValue != 2) {
                            if (intValue == 3) {
                                Set<String> stringSet = this.A07.A00.getStringSet(str4, new HashSet());
                                AnonymousClass009.A05(stringSet);
                                newRow.add(new JSONArray((Collection) stringSet).toString());
                            } else if (intValue != 1) {
                                if (intValue != 2) {
                                    if (intValue != 3) {
                                        throw new IllegalArgumentException("unexpected type");
                                    }
                                }
                            }
                            str3 = "string_set";
                        } else {
                            newRow.add(this.A07.A00.getString(str4, ""));
                        }
                        str3 = "string";
                    } else {
                        newRow.add(Integer.valueOf(this.A07.A00.getBoolean(str4, false) ? 1 : 0));
                    }
                    str3 = "boolean";
                } else {
                    newRow.add(Integer.valueOf(this.A07.A00.getInt(str4, 0)));
                    str3 = "int";
                }
                newRow.add(str3);
            }
            return matrixCursor;
        } else if (match == 7) {
            File A04 = this.A05.A04();
            MatrixCursor matrixCursor2 = new MatrixCursor(new String[]{"path"});
            if (this.A06.A07() && A04.exists()) {
                A05(matrixCursor2, A04, 4);
            }
            return matrixCursor2;
        } else {
            StringBuilder sb2 = new StringBuilder("Unknown URI ");
            sb2.append(uri);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        A01();
        return 0;
    }
}
