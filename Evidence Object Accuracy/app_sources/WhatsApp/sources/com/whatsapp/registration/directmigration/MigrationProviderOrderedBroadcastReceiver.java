package com.whatsapp.registration.directmigration;

import X.C14820m6;
import X.C15450nH;
import X.C15570nT;
import X.C18260sA;
import X.C22660zR;
import X.C22670zS;
import X.C27011Fr;
import android.content.BroadcastReceiver;

/* loaded from: classes2.dex */
public class MigrationProviderOrderedBroadcastReceiver extends BroadcastReceiver {
    public C15570nT A00;
    public C15450nH A01;
    public C22670zS A02;
    public C14820m6 A03;
    public C18260sA A04;
    public C22660zR A05;
    public C27011Fr A06;
    public final Object A07;
    public volatile boolean A08;

    public MigrationProviderOrderedBroadcastReceiver() {
        this(0);
    }

    public MigrationProviderOrderedBroadcastReceiver(int i) {
        this.A08 = false;
        this.A07 = new Object();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01c1, code lost:
        if (r0 != false) goto L_0x00f0;
     */
    @Override // android.content.BroadcastReceiver
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
        // Method dump skipped, instructions count: 492
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.directmigration.MigrationProviderOrderedBroadcastReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
