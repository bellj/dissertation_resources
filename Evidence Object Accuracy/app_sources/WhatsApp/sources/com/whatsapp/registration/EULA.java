package com.whatsapp.registration;

import X.AbstractC14440lR;
import X.AbstractC17940re;
import X.AbstractC18900tF;
import X.AbstractC27281Gs;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass10O;
import X.AnonymousClass12O;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass19H;
import X.AnonymousClass1C8;
import X.AnonymousClass1UB;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass36o;
import X.AnonymousClass3HG;
import X.AnonymousClass3J4;
import X.AnonymousClass4L4;
import X.AnonymousClass561;
import X.C003501n;
import X.C004802e;
import X.C102294ou;
import X.C104164rv;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14960mK;
import X.C15890o4;
import X.C17960rg;
import X.C18330sH;
import X.C18350sJ;
import X.C18790t3;
import X.C19890uq;
import X.C20220vP;
import X.C20630w4;
import X.C20640w5;
import X.C20760wH;
import X.C21750xv;
import X.C22040yO;
import X.C252018m;
import X.C25771At;
import X.C25971Bn;
import X.C26991Fp;
import X.C27011Fr;
import X.C36021jC;
import X.C41691tw;
import X.C42971wC;
import X.C43511x9;
import X.C457022s;
import X.C457122t;
import X.C52742bb;
import X.C56272kc;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.BottomSheetListView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.registration.EULA;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

/* loaded from: classes2.dex */
public class EULA extends ActivityC13790kL {
    public int A00;
    public View A01;
    public ViewTreeObserver.OnGlobalLayoutListener A02;
    public C20640w5 A03;
    public C18790t3 A04;
    public C18330sH A05;
    public C15890o4 A06;
    public AnonymousClass018 A07;
    public C25771At A08;
    public C22040yO A09;
    public C20760wH A0A;
    public AnonymousClass1C8 A0B;
    public C19890uq A0C;
    public C20220vP A0D;
    public C25971Bn A0E;
    public AnonymousClass3HG A0F;
    public C18350sJ A0G;
    public AnonymousClass10O A0H;
    public C26991Fp A0I;
    public C27011Fr A0J;
    public AnonymousClass4L4 A0K;
    public AnonymousClass17R A0L;
    public C20630w4 A0M;
    public AnonymousClass19H A0N;
    public AnonymousClass12U A0O;
    public C252018m A0P;
    public AnonymousClass12O A0Q;
    public C457022s A0R;
    public C21750xv A0S;
    public boolean A0T;
    public boolean A0U;
    public final AbstractC18900tF A0V;

    public EULA() {
        this(0);
        this.A00 = 0;
        this.A01 = null;
        this.A02 = new IDxLListenerShape13S0100000_1_I1(this, 10);
        this.A0V = new AnonymousClass561(this);
    }

    public EULA(int i) {
        this.A0T = false;
        ActivityC13830kP.A1P(this, 102);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(r2, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0M = (C20630w4) A1M.AJF.get();
            this.A0E = (C25971Bn) A1M.A9I.get();
            this.A03 = (C20640w5) A1M.AHk.get();
            this.A0N = (AnonymousClass19H) A1M.AJN.get();
            this.A0O = ActivityC13830kP.A1N(A1M);
            this.A04 = C12990iw.A0U(A1M);
            this.A0S = (C21750xv) A1M.ALL.get();
            this.A0L = (AnonymousClass17R) A1M.AIz.get();
            this.A09 = (C22040yO) A1M.A01.get();
            this.A0P = C12980iv.A0g(A1M);
            this.A0C = (C19890uq) A1M.ABw.get();
            this.A07 = C12960it.A0R(A1M);
            C17960rg builderWithExpectedSize = AbstractC17940re.builderWithExpectedSize(2);
            builderWithExpectedSize.addAll((Iterable) C12970iu.A12());
            Object obj = A1M.A8e.get();
            if (obj != null) {
                builderWithExpectedSize.add(obj);
                this.A0K = new AnonymousClass4L4(builderWithExpectedSize.build());
                this.A05 = (C18330sH) A1M.AN4.get();
                this.A0Q = (AnonymousClass12O) A1M.AMG.get();
                this.A08 = (C25771At) A1M.A7p.get();
                this.A0D = (C20220vP) A1M.AC3.get();
                this.A0G = (C18350sJ) A1M.AHX.get();
                this.A06 = C12970iu.A0Y(A1M);
                this.A0A = (C20760wH) A1M.A6A.get();
                this.A0H = (AnonymousClass10O) A1M.AMM.get();
                this.A0I = (C26991Fp) A1M.A5r.get();
                this.A0J = (C27011Fr) A1M.AA2.get();
                this.A0B = (AnonymousClass1C8) A1M.AAO.get();
                return;
            }
            throw C12980iv.A0n("Cannot return null from a non-@Nullable @Provides method");
        }
    }

    public final void A2e() {
        String str;
        View findViewById = findViewById(R.id.eula_layout);
        ((ActivityC13810kN) this).A09.A0H();
        C56272kc r4 = null;
        if (((ActivityC13790kL) this).A06.A02() < 10000000) {
            startActivity(C14960mK.A09(this, 10000000).setFlags(268435456));
        }
        if (((ActivityC13790kL) this).A0B.A00() != 0) {
            Log.e("eula/create/wrong-state bounce to main");
            startActivity(C14960mK.A04(this));
            finish();
            return;
        }
        C12990iw.A1N(new AnonymousClass36o(this), ((ActivityC13830kP) this).A05);
        TelephonyManager A0N = ((ActivityC13810kN) this).A08.A0N();
        if (A0N != null) {
            str = A0N.getSimCountryIso();
        } else {
            str = null;
        }
        boolean contains = C25971Bn.A00.contains(str);
        String string = getString(R.string.eula_agree);
        int i = R.string.eula_terms_of_service;
        if (contains) {
            i = R.string.eula_terms_of_service_idpc;
        }
        String A0X = C12960it.A0X(this, string, new Object[1], 0, i);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.eula_view);
        HashMap A11 = C12970iu.A11();
        A11.put("privacy-policy", ((ActivityC13790kL) this).A02.A00("https://www.whatsapp.com/legal/privacy-policy"));
        A11.put("terms-and-privacy-policy", ((ActivityC13790kL) this).A02.A00("https://www.whatsapp.com/legal/terms-of-service"));
        if (contains) {
            A11.put("learn-more", ((ActivityC13790kL) this).A02.A00("https://www.whatsapp.com/legal/information-for-people-who-dont-use-whatsapp"));
        }
        C42971wC.A09(this, ((ActivityC13790kL) this).A00, ((ActivityC13810kN) this).A05, textEmojiLabel, ((ActivityC13810kN) this).A08, A0X, A11);
        textEmojiLabel.setHighlightColor(0);
        if (contains) {
            textEmojiLabel.setTextSize(0, getResources().getDimension(R.dimen.registration_idpc_eula_hint_text_size));
        }
        C12960it.A12(AnonymousClass00T.A05(this, R.id.eula_accept), this, 23);
        if (getIntent().getBooleanExtra("show_registration_first_dlg", false)) {
            C36021jC.A01(this, 1);
        }
        this.A0G.A0A(0);
        if (this.A03.A03()) {
            Log.w("eula/clock-wrong");
            C43511x9.A01(this, this.A0C, this.A0D);
        }
        C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "input_enter_send", false);
        this.A01 = findViewById(R.id.eula_logo);
        if (findViewById != null) {
            findViewById.getViewTreeObserver().addOnGlobalLayoutListener(this.A02);
        }
        this.A05.A01();
        if (!((ActivityC13810kN) this).A09.A00.getBoolean("is_eula_loaded_once", false)) {
            if (Build.VERSION.SDK_INT >= 26 && AnonymousClass1UB.A00(this) == 0) {
                r4 = new C56272kc((Activity) this);
            }
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape11S0200000_I1_1(this, 45, r4));
            C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "is_eula_loaded_once", true);
        }
    }

    public final void A2f() {
        TextView A0M = C12970iu.A0M(this, R.id.language_picker);
        A0M.setText(AnonymousClass3J4.A00(AbstractC27281Gs.A01(Locale.getDefault())));
        A0M.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(this, 48, A0M));
        AnonymousClass2GE.A08(A0M, getResources().getColor(R.color.icon_primary));
        int height = getWindowManager().getDefaultDisplay().getHeight();
        if (C12980iv.A0H(this).orientation == 1) {
            View findViewById = findViewById(R.id.eula_logo);
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(findViewById);
            A0H.setMargins(A0H.leftMargin, height / 10, A0H.rightMargin, A0H.bottomMargin);
            findViewById.setLayoutParams(A0H);
            ((NestedScrollView) findViewById(R.id.eula_scroll_view)).A0E = new C104164rv(this);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0U = false;
        boolean A03 = AnonymousClass3J4.A03(((ActivityC13810kN) this).A08, this.A09);
        if (!A03) {
            C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        }
        this.A0J.A01();
        AbstractC14440lR r8 = ((ActivityC13830kP) this).A05;
        C18790t3 r3 = this.A04;
        C252018m r7 = this.A0P;
        AnonymousClass018 r4 = this.A07;
        this.A0F = new AnonymousClass3HG(r3, r4, this.A08, ((ActivityC13810kN) this).A0D, r7, r8);
        if (A03) {
            r4.A0B.add(this.A0V);
            if (C12980iv.A1W(((ActivityC13810kN) this).A09.A00, "is_ls_shown_during_reg")) {
                setContentView(R.layout.eula_with_ls);
                findViewById(R.id.eula_layout).startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.grow_from_center));
                A2e();
                A2f();
                return;
            }
            setContentView(R.layout.pre_tos);
            AnonymousClass1C8 r2 = this.A0B;
            r2.A03 = true;
            r2.A00 = System.currentTimeMillis();
            ImageView imageView = (ImageView) findViewById(R.id.next_button);
            AnonymousClass2GF.A01(this, imageView, this.A07, R.drawable.ic_fab_next);
            AbstractView$OnClickListenerC34281fs.A01(imageView, this, 38);
            BottomSheetListView bottomSheetListView = (BottomSheetListView) findViewById(R.id.language_selector_listview);
            AnonymousClass018 r32 = this.A07;
            bottomSheetListView.setAdapter((ListAdapter) new C52742bb(this, r32, AnonymousClass3J4.A01(((ActivityC13790kL) this).A01, ((ActivityC13810kN) this).A08, r32), AnonymousClass3J4.A02()));
            bottomSheetListView.setOnItemClickListener(new AdapterView.OnItemClickListener(bottomSheetListView, this) { // from class: X.3OF
                public final /* synthetic */ BottomSheetListView A00;
                public final /* synthetic */ EULA A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.widget.AdapterView.OnItemClickListener
                public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                    EULA eula = this.A01;
                    eula.A07.A0R(((C90284Nh) this.A00.getAdapter().getItem(i)).A01);
                    C12960it.A08(((ActivityC13810kN) eula).A09).putBoolean("is_ls_shown_during_reg", true).commit();
                    AnonymousClass1C8 r33 = eula.A0B;
                    int A01 = C12970iu.A01(r33.A02.A00, "language_selector_clicked_count");
                    C12960it.A08(r33.A02).putInt("language_selector_clicked_count", A01 + 1).commit();
                    eula.A0B.A00();
                    if (C28391Mz.A05()) {
                        eula.recreate();
                        return;
                    }
                    eula.finish();
                    eula.startActivity(eula.getIntent());
                    eula.overridePendingTransition(0, 0);
                }
            });
            bottomSheetListView.setOnScrollListener(new C102294ou(bottomSheetListView, this));
            ImageView imageView2 = (ImageView) findViewById(R.id.eula_logo);
            LayerDrawable layerDrawable = (LayerDrawable) imageView2.getDrawable();
            layerDrawable.setDrawableByLayerId(R.id.logo, AnonymousClass2GE.A04(layerDrawable.findDrawableByLayerId(R.id.logo), getResources().getColor(R.color.eulaDoodleTint)));
            imageView2.setImageDrawable(layerDrawable);
            return;
        }
        setContentView(R.layout.eula);
        A2e();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        int i2;
        int i3;
        int i4;
        int i5;
        String str;
        Set set;
        if (i == 1) {
            r2 = C12980iv.A0S(this);
            r2.A06(R.string.register_first);
            i2 = R.string.ok;
            i3 = 54;
        } else if (i != 2) {
            switch (i) {
                case 5:
                    C457022s r0 = this.A0R;
                    if (r0 == null || (set = r0.A00) == null || set.isEmpty()) {
                        str = "";
                    } else {
                        StringBuilder A0h = C12960it.A0h();
                        for (C457122t r1 : this.A0R.A00) {
                            A0h.append('\t');
                            A0h.append(r1.A00);
                            A0h.append('\n');
                        }
                        A0h.setLength(A0h.length() - 1);
                        str = A0h.toString();
                    }
                    r2 = C12980iv.A0S(this);
                    r2.A0A(C12960it.A0X(this, str, new Object[1], 0, R.string.task_killer_info_modern));
                    r2.A08(new DialogInterface.OnCancelListener() { // from class: X.4f9
                        @Override // android.content.DialogInterface.OnCancelListener
                        public final void onCancel(DialogInterface dialogInterface) {
                            EULA eula = EULA.this;
                            C36021jC.A00(eula, 5);
                            C36021jC.A01(eula, 6);
                        }
                    });
                    break;
                case 6:
                    this.A00 = 1;
                    r2 = C12980iv.A0S(this);
                    r2.A07(R.string.alert);
                    r2.A06(R.string.task_killer_detected);
                    r2.A0B(false);
                    C12970iu.A1L(r2, this, 57, R.string.dialog_button_more_info);
                    i4 = R.string.ok;
                    i5 = 55;
                    C12970iu.A1K(r2, this, i5, i4);
                    break;
                case 7:
                    r2 = C12980iv.A0S(this);
                    r2.A0A(C12960it.A0X(this, getString(R.string.localized_app_name), new Object[1], 0, R.string.custom_rom_info_app_name));
                    r2.A08(new DialogInterface.OnCancelListener() { // from class: X.4f8
                        @Override // android.content.DialogInterface.OnCancelListener
                        public final void onCancel(DialogInterface dialogInterface) {
                            EULA eula = EULA.this;
                            C36021jC.A00(eula, 7);
                            C36021jC.A01(eula, 8);
                        }
                    });
                    break;
                case 8:
                    this.A00 = 2;
                    r2 = C12980iv.A0S(this);
                    r2.A07(R.string.alert);
                    r2.A06(R.string.custom_rom_detected);
                    r2.A0B(false);
                    C12970iu.A1L(r2, this, 53, R.string.dialog_button_more_info);
                    i4 = R.string.ok;
                    i5 = 52;
                    C12970iu.A1K(r2, this, i5, i4);
                    break;
                case 9:
                    r2 = C12980iv.A0S(this);
                    r2.A07(R.string.alert);
                    r2.A06(R.string.clock_wrong);
                    i2 = R.string.ok;
                    i3 = 58;
                    break;
                default:
                    return super.onCreateDialog(i);
            }
            return r2.create();
        } else {
            r2 = C12980iv.A0S(this);
            r2.A07(R.string.alert);
            r2.A06(R.string.registration_cellular_network_required);
            i2 = R.string.ok;
            i3 = 56;
        }
        C12970iu.A1L(r2, this, i3, i2);
        return r2.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0F.A01();
        AnonymousClass018 r0 = this.A07;
        r0.A0B.remove(this.A0V);
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 0) {
            this.A0H.A01("eula");
            this.A0F.A02(this, this.A0H, "eula");
            return true;
        } else if (itemId == 1) {
            C003501n.A07(this);
            return true;
        } else if (itemId != 2) {
            return false;
        } else {
            C12960it.A08(((ActivityC13810kN) this).A09).putBoolean("is_ls_shown_during_reg", false).commit();
            recreate();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        int i;
        super.onResume();
        int i2 = this.A00;
        if (i2 == 1) {
            i = 6;
        } else if (i2 == 2) {
            i = 8;
        } else {
            return;
        }
        C36021jC.A01(this, i);
    }
}
