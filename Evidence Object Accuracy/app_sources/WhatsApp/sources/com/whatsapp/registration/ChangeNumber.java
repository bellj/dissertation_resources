package com.whatsapp.registration;

import X.AbstractActivityC452520u;
import X.AbstractC005102i;
import X.AbstractC13630k2;
import X.AbstractC13640k3;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass027;
import X.AnonymousClass11G;
import X.AnonymousClass12O;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass1CE;
import X.AnonymousClass1UB;
import X.AnonymousClass23M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2SS;
import X.AnonymousClass4KK;
import X.AnonymousClass510;
import X.AnonymousClass595;
import X.AnonymousClass5B8;
import X.C004802e;
import X.C103334qa;
import X.C13600jz;
import X.C13650k5;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C18350sJ;
import X.C18360sK;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19890uq;
import X.C20640w5;
import X.C20660w7;
import X.C20800wL;
import X.C20910wW;
import X.C20920wX;
import X.C21330xF;
import X.C21340xG;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22040yO;
import X.C22650zQ;
import X.C22660zR;
import X.C22670zS;
import X.C22680zT;
import X.C22690zU;
import X.C22700zV;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25771At;
import X.C25961Bm;
import X.C35751ig;
import X.C36021jC;
import X.C41691tw;
import X.C42941w9;
import X.C56282kd;
import X.C628638w;
import X.C77803ns;
import X.C852041q;
import X.C852141r;
import X.C91794Td;
import X.HandlerC467827n;
import X.ViewTreeObserver$OnPreDrawListenerC101904oH;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.components.PhoneNumberEntry;
import com.whatsapp.registration.ChangeNumber;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;

/* loaded from: classes2.dex */
public class ChangeNumber extends AbstractActivityC452520u {
    public static String A0N;
    public static String A0O;
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public View A04;
    public ScrollView A05;
    public C20920wX A06;
    public C22680zT A07;
    public AnonymousClass4KK A08;
    public C22700zV A09;
    public C18360sK A0A;
    public C15650ng A0B;
    public C16030oK A0C;
    public C19890uq A0D;
    public C20660w7 A0E;
    public C91794Td A0F;
    public AnonymousClass12O A0G;
    public ArrayList A0H;
    public boolean A0I;
    public final Handler A0J;
    public final AnonymousClass2SS A0K;
    public final AbstractView$OnClickListenerC34281fs A0L;
    public final Runnable A0M;

    public ChangeNumber() {
        this(0);
        this.A02 = 0;
        this.A03 = 0;
        this.A0M = new RunnableBRunnable0Shape10S0100000_I0_10(this, 28);
        this.A0K = new AnonymousClass595(this);
        this.A0J = new HandlerC467827n(Looper.getMainLooper(), this);
        this.A0L = new ViewOnClickCListenerShape15S0100000_I0_2(this, 11);
    }

    public ChangeNumber(int i) {
        this.A0I = false;
        A0R(new C103334qa(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0I) {
            this.A0I = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC452520u) this).A04 = (C21740xu) r1.AM1.get();
            ((AbstractActivityC452520u) this).A02 = (C20640w5) r1.AHk.get();
            ((AbstractActivityC452520u) this).A0G = (AnonymousClass12U) r1.AJd.get();
            ((AbstractActivityC452520u) this).A0I = (C22650zQ) r1.A4n.get();
            ((AbstractActivityC452520u) this).A0E = (C25661Ag) r1.A8G.get();
            ((AbstractActivityC452520u) this).A0A = (C22660zR) r1.AAk.get();
            ((AbstractActivityC452520u) this).A03 = (AnonymousClass19Y) r1.AI6.get();
            ((AbstractActivityC452520u) this).A07 = (C22040yO) r1.A01.get();
            ((AbstractActivityC452520u) this).A08 = (AnonymousClass11G) r1.AKq.get();
            ((AbstractActivityC452520u) this).A01 = (C22690zU) r1.A35.get();
            ((AbstractActivityC452520u) this).A06 = (C25771At) r1.A7p.get();
            ((AbstractActivityC452520u) this).A0D = (C18350sJ) r1.AHX.get();
            ((AbstractActivityC452520u) this).A05 = (C15890o4) r1.AN1.get();
            ((AbstractActivityC452520u) this).A0C = (C20800wL) r1.AHW.get();
            this.A0E = (C20660w7) r1.AIB.get();
            this.A0D = (C19890uq) r1.ABw.get();
            this.A06 = C20910wW.A00();
            this.A0B = (C15650ng) r1.A4m.get();
            this.A07 = (C22680zT) r1.AGW.get();
            this.A0G = (AnonymousClass12O) r1.AMG.get();
            this.A09 = (C22700zV) r1.AMN.get();
            this.A0A = (C18360sK) r1.AN0.get();
            this.A08 = AnonymousClass5B8.A00((C25961Bm) r1.A0o.get());
            this.A0C = (C16030oK) r1.AAd.get();
        }
    }

    @Override // X.AbstractActivityC452520u
    public void A2e() {
        C36021jC.A00(this, 1);
        super.A2e();
    }

    @Override // X.AbstractActivityC452520u
    public void A2g(String str, String str2, String str3) {
        super.A2g(str, str2, str3);
        if (((AbstractActivityC452520u) this).A0B.A02) {
            AnonymousClass23M.A0H(this, this.A0A, ((AbstractActivityC452520u) this).A0D, false);
        }
        ((AbstractActivityC452520u) this).A0D.A0D();
        finish();
    }

    public final void A2h() {
        float f;
        boolean canScrollVertically = this.A05.canScrollVertically(1);
        View view = this.A04;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public final void A2i() {
        this.A05.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101904oH(this));
    }

    public final void A2j() {
        Log.i("changenumber/proceedWithoutSmsRetriever/requesting RECEIVE_SMS permission");
        C35751ig r3 = new C35751ig(this);
        r3.A01 = R.drawable.permission_sms;
        r3.A0C = new String[]{"android.permission.RECEIVE_SMS"};
        r3.A02 = R.string.permission_sms_request;
        r3.A06 = true;
        A2E(r3.A00(), 2);
    }

    public final void A2k() {
        if (isFinishing()) {
            Log.i("changenumber/verify/cancel");
            return;
        }
        AbstractActivityC452520u.A0Q = 0;
        ((ActivityC13810kN) this).A09.A0i(null);
        this.A0C.A0D();
        Log.i("BusinessDirectoryStorageManager/onNumberChanged");
        C21330xF r1 = ((C21340xG) ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, getApplicationContext())).A2L.get()).A00;
        Log.i("BusinessDirectorySharedPrefManager/deleteLocation");
        r1.A00().edit().remove("current_search_location").apply();
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(getApplicationContext(), 36, ((ActivityC13810kN) this).A09));
        AbstractC14440lR r12 = ((ActivityC13830kP) this).A05;
        C22040yO r4 = ((AbstractActivityC452520u) this).A07;
        r12.Aaz(new C628638w(((ActivityC13810kN) this).A09, r4, this.A08.A00, this, ((AbstractActivityC452520u) this).A0C, AbstractActivityC452520u.A0R, AbstractActivityC452520u.A0S, null, null, AbstractActivityC452520u.A0Q, false), new Void[0]);
    }

    public final void A2l(boolean z) {
        boolean z2;
        long j;
        long j2;
        int i;
        Intent A08;
        String str = AbstractActivityC452520u.A0T;
        if (str != null) {
            z2 = true;
            A08 = C14960mK.A0X(this, str, AbstractActivityC452520u.A0P, this.A02, this.A03, z, false, true, false);
        } else {
            if (((AbstractActivityC452520u) this).A0N) {
                ((AbstractActivityC452520u) this).A0D.A0A(13);
                z2 = true;
                j = this.A02;
                j2 = this.A03;
                i = 1;
            } else {
                z2 = true;
                j = this.A02;
                j2 = this.A03;
                i = 0;
            }
            A08 = C14960mK.A08(this, i, j, j2, true, z);
        }
        A2G(A08, z2);
    }

    public final boolean A2m(C91794Td r7, String str, String str2) {
        EditText editText;
        int i;
        switch (AbstractActivityC452520u.A03(this.A07, str, str2)) {
            case 1:
                int parseInt = Integer.parseInt(str);
                String replaceAll = str2.replaceAll("\\D", "");
                try {
                    replaceAll = this.A07.A02(parseInt, replaceAll);
                } catch (IOException e) {
                    Log.e("changenumber/cc failed trimLeadingZero from CountryPhoneInfo", e);
                }
                StringBuilder sb = new StringBuilder("changenumber/cc=");
                sb.append(str);
                sb.append("/number=");
                sb.append(replaceAll);
                Log.i(sb.toString());
                AbstractActivityC452520u.A0R = str;
                AbstractActivityC452520u.A0S = replaceAll;
                return true;
            case 2:
                Adp(getString(R.string.register_bad_cc_length_with_placeholders, 1, 3));
                editText = r7.A02;
                break;
            case 3:
                Ado(R.string.register_bad_cc_valid);
                r7.A02.setText("");
                editText = r7.A02;
                break;
            case 4:
                Ado(R.string.register_empty_phone);
                editText = r7.A03;
                break;
            case 5:
                i = R.string.register_bad_phone_too_short;
                Adp(getString(i, ((AbstractActivityC452520u) this).A0I.A02(((ActivityC13830kP) this).A01, r7.A06)));
                editText = r7.A03;
                break;
            case 6:
                i = R.string.register_bad_phone_too_long;
                Adp(getString(i, ((AbstractActivityC452520u) this).A0I.A02(((ActivityC13830kP) this).A01, r7.A06)));
                editText = r7.A03;
                break;
            case 7:
                i = R.string.register_bad_phone;
                Adp(getString(i, ((AbstractActivityC452520u) this).A0I.A02(((ActivityC13830kP) this).A01, r7.A06)));
                editText = r7.A03;
                break;
            default:
                return false;
        }
        editText.requestFocus();
        return false;
    }

    @Override // X.AbstractC452720w
    public void AQ2() {
        this.A0M.run();
    }

    @Override // X.AbstractC452720w
    public void AT0(String str, String str2, byte[] bArr) {
        AnonymousClass12O r0 = this.A0G;
        r0.A03();
        r0.A05();
        this.A0E.A05();
        this.A0D.A0G(false);
        ((ActivityC13790kL) this).A01.A06();
        new File(getFilesDir(), "me").delete();
        ((AbstractActivityC452520u) this).A0D.A0C(AbstractActivityC452520u.A0R, AbstractActivityC452520u.A0S, null);
        ((AbstractActivityC452520u) this).A0D.A0A(4);
        this.A02 = (AnonymousClass23M.A02(str, 0) * 1000) + System.currentTimeMillis();
        this.A03 = (AnonymousClass23M.A02(str2, 0) * 1000) + System.currentTimeMillis();
        if (AnonymousClass1CE.A00(((ActivityC13810kN) this).A08, AbstractActivityC452520u.A0P)) {
            A2G(C14960mK.A0A(this, this.A02, this.A03, true), true);
        } else if (((AbstractActivityC452520u) this).A05.A02("android.permission.RECEIVE_SMS") == 0) {
            A2l(false);
        } else if (AnonymousClass1UB.A00(this) == 0) {
            C13600jz A01 = new C56282kd((Activity) this).A01(new C77803ns(), 1);
            AnonymousClass510 r02 = new AbstractC13640k3() { // from class: X.510
                @Override // X.AbstractC13640k3
                public final void AX4(Object obj) {
                    ChangeNumber changeNumber = ChangeNumber.this;
                    Log.i("changenumber/smsretriever/onsuccess");
                    changeNumber.A2l(true);
                }
            };
            Executor executor = C13650k5.A00;
            A01.A06(r02, executor);
            A01.A05(new AbstractC13630k2() { // from class: X.50x
                @Override // X.AbstractC13630k2
                public final void AQA(Exception exc) {
                    ChangeNumber changeNumber = ChangeNumber.this;
                    Log.e("changenumber/smsretriever/onfailure/ ", exc);
                    changeNumber.A2j();
                }
            }, executor);
        } else {
            A2j();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r8, int r9, android.content.Intent r10) {
        /*
        // Method dump skipped, instructions count: 466
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.ChangeNumber.onActivityResult(int, int, android.content.Intent):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (Build.VERSION.SDK_INT >= 21) {
            A2i();
        }
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String simCountryIso;
        super.onCreate(bundle);
        C41691tw.A07(getWindow(), false);
        C41691tw.A02(this, R.color.primary);
        setTitle(R.string.change_number_title);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0N(true);
        setContentView(R.layout.change_number);
        PhoneNumberEntry phoneNumberEntry = (PhoneNumberEntry) findViewById(R.id.registration_fields);
        PhoneNumberEntry phoneNumberEntry2 = (PhoneNumberEntry) findViewById(R.id.registration_new_fields);
        C91794Td r0 = new C91794Td();
        this.A0F = r0;
        r0.A05 = phoneNumberEntry;
        C91794Td r02 = new C91794Td();
        ((AbstractActivityC452520u) this).A09 = r02;
        r02.A05 = phoneNumberEntry2;
        this.A05 = (ScrollView) findViewById(R.id.scroll_view);
        this.A04 = findViewById(R.id.bottom_button_container);
        C91794Td r03 = this.A0F;
        WaEditText waEditText = phoneNumberEntry.A02;
        r03.A02 = waEditText;
        waEditText.setContentDescription(getString(R.string.old_country_code_content_description));
        C91794Td r04 = ((AbstractActivityC452520u) this).A09;
        WaEditText waEditText2 = phoneNumberEntry2.A02;
        r04.A02 = waEditText2;
        waEditText2.setContentDescription(getString(R.string.new_country_code_content_description));
        this.A0F.A03 = phoneNumberEntry.A03;
        C91794Td r1 = ((AbstractActivityC452520u) this).A09;
        WaEditText waEditText3 = phoneNumberEntry2.A03;
        r1.A03 = waEditText3;
        C42941w9.A03(waEditText3);
        C42941w9.A03(this.A0F.A03);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
        TelephonyManager A0N2 = ((ActivityC13810kN) this).A08.A0N();
        if (!(A0N2 == null || (simCountryIso = A0N2.getSimCountryIso()) == null)) {
            try {
                A0N = this.A07.A04(simCountryIso);
            } catch (IOException e) {
                Log.e("changenumber/iso/code failed to get code from CountryPhoneInfo", e);
            }
        }
        phoneNumberEntry.A04 = new C852041q(this);
        phoneNumberEntry2.A04 = new C852141r(this);
        C91794Td r12 = this.A0F;
        r12.A01 = AnonymousClass23M.A00(r12.A03);
        C91794Td r13 = this.A0F;
        r13.A00 = AnonymousClass23M.A00(r13.A02);
        C91794Td r14 = ((AbstractActivityC452520u) this).A09;
        r14.A01 = AnonymousClass23M.A00(r14.A03);
        C91794Td r15 = ((AbstractActivityC452520u) this).A09;
        r15.A00 = AnonymousClass23M.A00(r15.A02);
        TextView textView = (TextView) findViewById(R.id.next_btn);
        textView.setText(R.string.next);
        textView.setOnClickListener(this.A0L);
        String str = A0N;
        if (str != null) {
            this.A0F.A02.setText(str);
            ((AbstractActivityC452520u) this).A09.A02.setText(A0N);
        }
        String str2 = this.A0F.A06;
        if (str2 != null && str2.length() > 0) {
            StringBuilder sb = new StringBuilder("changenumber/country: ");
            sb.append(str2);
            Log.i(sb.toString());
            this.A0F.A05.A02(str2);
            ((AbstractActivityC452520u) this).A09.A05.A02(str2);
        }
        ((AbstractActivityC452520u) this).A0J = ((ActivityC13810kN) this).A09.A00.getString("change_number_new_number_banned", null);
        ((AbstractActivityC452520u) this).A0D.A0p.add(this.A0K);
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("pref_flash_call_education_screen_displayed", false).apply();
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("pref_prefer_sms_over_flash", false).apply();
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
            this.A05.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oa
                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    ChangeNumber.this.A2h();
                }
            });
            A2i();
        }
    }

    @Override // X.AbstractActivityC452520u, android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 1) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.register_connecting));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            return progressDialog;
        } else if (i != 2) {
            return super.onCreateDialog(i);
        } else {
            C004802e r2 = new C004802e(this);
            r2.A06(R.string.change_number_new_country_code_suggestion);
            r2.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener() { // from class: X.4fz
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ChangeNumber.this.A2k();
                }
            });
            return r2.create();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C18350sJ r0 = ((AbstractActivityC452520u) this).A0D;
        r0.A0p.remove(this.A0K);
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        SharedPreferences.Editor remove;
        super.onPause();
        C91794Td r1 = this.A0F;
        r1.A01 = AnonymousClass23M.A00(r1.A03);
        C91794Td r12 = this.A0F;
        r12.A00 = AnonymousClass23M.A00(r12.A02);
        C91794Td r13 = ((AbstractActivityC452520u) this).A09;
        r13.A01 = AnonymousClass23M.A00(r13.A03);
        C91794Td r14 = ((AbstractActivityC452520u) this).A09;
        r14.A00 = AnonymousClass23M.A00(r14.A02);
        String str = ((AbstractActivityC452520u) this).A0J;
        C14820m6 r0 = ((ActivityC13810kN) this).A09;
        if (str != null) {
            String str2 = AbstractActivityC452520u.A0R;
            String str3 = AbstractActivityC452520u.A0S;
            SharedPreferences.Editor edit = r0.A00.edit();
            StringBuilder sb = new StringBuilder("+");
            sb.append(str2);
            sb.append(str3);
            remove = edit.putString("change_number_new_number_banned", sb.toString());
        } else if (r0.A00.getString("change_number_new_number_banned", null) != null) {
            remove = ((ActivityC13810kN) this).A09.A00.edit().remove("change_number_new_number_banned");
        } else {
            return;
        }
        remove.apply();
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        A0N = bundle.getString("country_code");
        A0O = bundle.getString("phone_number");
        AbstractActivityC452520u.A0R = bundle.getString("countryCode");
        AbstractActivityC452520u.A0S = bundle.getString("phoneNumber");
        this.A0H = bundle.getStringArrayList("notifyJids");
        this.A01 = bundle.getInt("mode");
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        String str = A0N;
        if (str != null) {
            this.A0F.A02.setText(str);
        }
        C91794Td r0 = this.A0F;
        AnonymousClass23M.A0I(r0.A02, r0.A00);
        C91794Td r02 = this.A0F;
        AnonymousClass23M.A0I(r02.A03, r02.A01);
        C91794Td r03 = ((AbstractActivityC452520u) this).A09;
        AnonymousClass23M.A0I(r03.A02, r03.A00);
        C91794Td r04 = ((AbstractActivityC452520u) this).A09;
        AnonymousClass23M.A0I(r04.A03, r04.A01);
        this.A0F.A03.clearFocus();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("country_code", A0N);
        bundle.putCharSequence("phone_number", A0O);
        bundle.putCharSequence("countryCode", AbstractActivityC452520u.A0R);
        bundle.putCharSequence("phoneNumber", AbstractActivityC452520u.A0S);
        bundle.putStringArrayList("notifyJids", this.A0H);
        bundle.putInt("mode", this.A01);
    }
}
