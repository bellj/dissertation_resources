package com.whatsapp.registration;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC20260vT;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass04S;
import X.AnonymousClass105;
import X.AnonymousClass10O;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass1R5;
import X.AnonymousClass23M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2eq;
import X.AnonymousClass3HG;
import X.AnonymousClass3U4;
import X.AnonymousClass52S;
import X.C004802e;
import X.C103394qg;
import X.C12970iu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14920mG;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16590pI;
import X.C18350sJ;
import X.C18360sK;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C20800wL;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25771At;
import X.C38131nZ;
import X.C44421yt;
import X.C49342Kj;
import X.C52162aM;
import X.C64323Fc;
import X.C72463ee;
import X.CountDownTimerC51952Zu;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape0S2101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.util.Log;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class VerifyTwoFactorAuth extends ActivityC13790kL {
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public CountDownTimer A04;
    public ProgressBar A05;
    public TextView A06;
    public AnonymousClass04S A07;
    public CodeInputField A08;
    public AnonymousClass19Y A09;
    public C18790t3 A0A;
    public C16590pI A0B;
    public C18360sK A0C;
    public C15890o4 A0D;
    public C25771At A0E;
    public AnonymousClass11G A0F;
    public C64323Fc A0G;
    public AnonymousClass3HG A0H;
    public C20800wL A0I;
    public C18350sJ A0J;
    public AnonymousClass10O A0K;
    public C44421yt A0L;
    public C49342Kj A0M;
    public C25661Ag A0N;
    public AnonymousClass105 A0O;
    public C252018m A0P;
    public C14920mG A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public final Handler A0Y;
    public final AbstractC20260vT A0Z;
    public final Runnable A0a;

    public VerifyTwoFactorAuth() {
        this(0);
        this.A0Y = new Handler(Looper.getMainLooper());
        this.A0a = new RunnableBRunnable0Shape11S0100000_I0_11(this, 2);
        this.A0Z = new AbstractC20260vT() { // from class: X.55j
            @Override // X.AbstractC20260vT
            public final void AOa(AnonymousClass1I1 r4) {
                CodeInputField codeInputField;
                VerifyTwoFactorAuth verifyTwoFactorAuth = VerifyTwoFactorAuth.this;
                if (r4.A01 && (codeInputField = verifyTwoFactorAuth.A08) != null && codeInputField.getCode().length() == 6) {
                    verifyTwoFactorAuth.A2f(0, verifyTwoFactorAuth.A08.getCode(), false);
                }
            }
        };
    }

    public VerifyTwoFactorAuth(int i) {
        this.A0W = false;
        A0R(new C103394qg(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0W) {
            this.A0W = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0B = (C16590pI) r1.AMg.get();
            this.A0A = (C18790t3) r1.AJw.get();
            this.A09 = (AnonymousClass19Y) r1.AI6.get();
            this.A0P = (C252018m) r1.A7g.get();
            this.A0F = (AnonymousClass11G) r1.AKq.get();
            this.A0E = (C25771At) r1.A7p.get();
            this.A0J = (C18350sJ) r1.AHX.get();
            this.A0N = (C25661Ag) r1.A8G.get();
            this.A0D = (C15890o4) r1.AN1.get();
            this.A0Q = (C14920mG) r1.ALr.get();
            this.A0K = (AnonymousClass10O) r1.AMM.get();
            this.A0C = (C18360sK) r1.AN0.get();
            this.A0O = (AnonymousClass105) r1.AJc.get();
            this.A0I = (C20800wL) r1.AHW.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.two_factor_auth_wrong_code_message) {
            if (this.A08.isEnabled()) {
                InputMethodManager A0Q = ((ActivityC13810kN) this).A08.A0Q();
                AnonymousClass009.A05(A0Q);
                A0Q.toggleSoftInput(1, 0);
            }
        } else if (i == R.string.register_stale || i == R.string.register_verify_again || i == R.string.two_factor_auth_too_many_tries) {
            this.A0J.A09();
            startActivity(C14960mK.A05(this));
            finish();
        }
    }

    public final int A2e() {
        if ((this.A01 + (this.A03 * 1000)) - ((ActivityC13790kL) this).A05.A00() <= 0) {
            String str = this.A0U;
            if ("offline".equals(str)) {
                return 2;
            }
            if ("full".equals(str)) {
                return 3;
            }
        }
        return 1;
    }

    public final void A2f(int i, String str, boolean z) {
        AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
        C49342Kj r2 = new C49342Kj(((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A09, ((ActivityC13830kP) this).A01, this.A0I, this, this.A0S, this.A0R, str, this.A0T, i, z);
        this.A0M = r2;
        r1.Aaz(r2, new String[0]);
    }

    public final void A2g(long j) {
        CountDownTimer start;
        if (j < 1000) {
            getPreferences(0).edit().remove("code_retry_time").apply();
            CountDownTimer countDownTimer = this.A04;
            if (countDownTimer != null) {
                countDownTimer.cancel();
                start = null;
            } else {
                return;
            }
        } else {
            getPreferences(0).edit().putLong("code_retry_time", ((ActivityC13790kL) this).A05.A00() + j).apply();
            ((ActivityC13790kL) this).A0D.A01(this.A08);
            this.A08.setEnabled(false);
            this.A05.setProgress(0);
            this.A06.setText(R.string.two_factor_auth_code_guessed_too_fast_message);
            this.A06.setVisibility(0);
            start = new CountDownTimerC51952Zu(this, j, j).start();
        }
        this.A04 = start;
    }

    public void A2h(AnonymousClass1R5 r12) {
        this.A0U = r12.A08;
        this.A0T = r12.A07;
        this.A03 = r12.A03;
        this.A00 = r12.A02;
        this.A02 = r12.A01;
        long A00 = ((ActivityC13790kL) this).A05.A00();
        this.A01 = A00;
        ((ActivityC13810kN) this).A09.A0w(this.A0U, this.A0T, this.A03, this.A00, this.A02, A00);
    }

    public void A2i(String str, String str2) {
        this.A0J.A0C(this.A0R, this.A0S, str2);
        C14920mG r2 = this.A0Q;
        r2.A0A.Ab6(new RunnableBRunnable0Shape0S2101000_I0(r2, str, null, 5, 1));
        this.A0N.A02("2fa", "successful");
        if (this.A0G.A02) {
            AnonymousClass23M.A0H(this, this.A0C, this.A0J, false);
        } else if (!this.A0V) {
            Log.i("VerifyTwoFactorAuth/removeProgressDialog/");
            this.A0J.A0A(2);
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.registration.RegisterName");
            A2G(intent, true);
            return;
        } else {
            this.A0J.A0D();
        }
        finish();
    }

    public final void A2j(boolean z) {
        C44421yt r1 = this.A0L;
        if (r1 != null) {
            r1.A03(true);
        }
        if (z) {
            this.A00 = -1;
            ((ActivityC13810kN) this).A09.A0w(this.A0U, this.A0T, this.A03, -1, this.A02, this.A01);
        }
        this.A0Y.removeCallbacks(this.A0a);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(DefaultCrypto.BUFFER_SIZE);
        this.A0H = new AnonymousClass3HG(this.A0A, ((ActivityC13830kP) this).A01, this.A0E, ((ActivityC13810kN) this).A0D, this.A0P, ((ActivityC13830kP) this).A05);
        setTitle(R.string.two_factor_auth_verify_title);
        this.A0G = new C64323Fc(this, ((ActivityC13810kN) this).A09);
        Intent intent = getIntent();
        if (intent.getExtras() != null && intent.getBooleanExtra("changenumber", false)) {
            this.A0V = true;
        }
        setContentView(R.layout.activity_two_factor_auth_verify);
        this.A0N.A00("2fa");
        Toolbar toolbar = (Toolbar) findViewById(R.id.title_toolbar);
        if (!ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey()) {
            A1e(toolbar);
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0M(false);
                A1U.A0P(false);
            }
        }
        this.A08 = (CodeInputField) findViewById(R.id.code);
        this.A08.A08(new AnonymousClass52S(this), new AnonymousClass3U4(this), null, getString(R.string.accessibility_two_factor_auth_code_entry, 6), '*', '*', 6);
        this.A08.setPasswordTransformationEnabled(true);
        this.A05 = (ProgressBar) findViewById(R.id.progress_bar_code_input_blocked);
        this.A08.setEnabled(true);
        this.A05.setProgress(100);
        this.A06 = (TextView) findViewById(R.id.description_bottom);
        this.A0R = ((ActivityC13810kN) this).A09.A0B();
        this.A0S = ((ActivityC13810kN) this).A09.A0C();
        this.A0U = ((ActivityC13810kN) this).A09.A00.getString("registration_wipe_type", null);
        this.A0T = ((ActivityC13810kN) this).A09.A00.getString("registration_wipe_token", null);
        this.A03 = ((ActivityC13810kN) this).A09.A00.getLong("registration_wipe_wait", -1);
        this.A00 = ((ActivityC13810kN) this).A09.A00.getLong("registration_wipe_expiry", -1);
        this.A02 = ((ActivityC13810kN) this).A09.A00.getLong("registration_wipe_server_time", -1);
        this.A01 = ((ActivityC13810kN) this).A09.A00.getLong("registration_wipe_info_timestamp", -1);
        if (this.A00 > 0) {
            A2j(false);
            this.A0Y.postDelayed(this.A0a, 0);
        }
        if (bundle != null && !bundle.getBoolean("shouldShowTheForgetPinDialog", false)) {
            A2M("forgotPinDialogTag");
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 109) {
            return AnonymousClass23M.A03(this, this.A09, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A08, this.A0D, this.A0F, this.A0I, ((ActivityC13830kP) this).A05);
        } else if (i == 124) {
            return AnonymousClass23M.A04(this, this.A09, ((ActivityC13830kP) this).A01, this.A0F, new RunnableBRunnable0Shape11S0100000_I0_11(this, 0), this.A0R, this.A0S);
        } else {
            if (i == 125) {
                return AnonymousClass23M.A05(this, this.A09, this.A0F, this.A0R, this.A0S);
            }
            switch (i) {
                case 31:
                    ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage(getString(R.string.register_voice_verifying));
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    return progressDialog;
                case 32:
                    C004802e r4 = new C004802e(this);
                    r4.A0A(getString(R.string.register_check_connectivity, getString(R.string.connectivity_self_help_instructions)));
                    r4.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4gB
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            C36021jC.A00(VerifyTwoFactorAuth.this, 32);
                        }
                    });
                    return r4.create();
                case 33:
                    ProgressDialog progressDialog2 = new ProgressDialog(this);
                    progressDialog2.setMessage(getString(R.string.two_factor_auth_sending_email));
                    progressDialog2.setIndeterminate(true);
                    progressDialog2.setCancelable(false);
                    return progressDialog2;
                case 34:
                    ProgressDialog progressDialog3 = new ProgressDialog(this);
                    progressDialog3.setMessage(getString(R.string.two_factor_auth_resetting_account));
                    progressDialog3.setIndeterminate(true);
                    progressDialog3.setCancelable(false);
                    return progressDialog3;
                default:
                    return super.onCreateDialog(i);
            }
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C49342Kj r1 = this.A0M;
        if (r1 != null) {
            r1.A03(true);
        }
        A2j(false);
        CountDownTimer countDownTimer = this.A04;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A04 = null;
        }
        this.A0X = false;
        ((ActivityC13810kN) this).A07.A04(this.A0Z);
        this.A0H.A01();
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        StringBuilder sb = new StringBuilder("register-2fa +");
        sb.append(this.A0R);
        sb.append(this.A0S);
        String obj = sb.toString();
        int itemId = menuItem.getItemId();
        if (itemId == 0) {
            this.A0K.A01("verify-2fa");
            this.A0H.A02(this, this.A0K, obj);
            return true;
        } else if (itemId != 1) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            this.A0J.A09();
            startActivity(C14960mK.A01(this));
            finishAffinity();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A04 == null) {
            long j = getPreferences(0).getLong("code_retry_time", -1);
            if (j != -1) {
                A2g(j - ((ActivityC13790kL) this).A05.A00());
            }
        }
        this.A08.requestFocus();
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.description);
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, ((ActivityC13810kN) this).A08));
        textEmojiLabel.setText(AnonymousClass23M.A08(new RunnableBRunnable0Shape11S0100000_I0_11(this, 1), getString(R.string.two_factor_auth_verify_code_info), "forgot-pin"));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        boolean z = false;
        if (A0V().A0A("forgotPinDialogTag") != null) {
            z = true;
        }
        bundle.putBoolean("shouldShowTheForgetPinDialog", z);
        super.onSaveInstanceState(bundle);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (this.A0X) {
            this.A0X = true;
            try {
                ((ActivityC13810kN) this).A07.A03(this.A0Z);
            } catch (IllegalStateException unused) {
            }
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        Log.i("VerifyTwoFactorAuth/removeProgressDialog/");
        AnonymousClass04S r0 = this.A07;
        if (r0 != null) {
            r0.dismiss();
            this.A07 = null;
        }
        this.A0X = true;
        ((ActivityC13810kN) this).A07.A04(this.A0Z);
    }

    /* loaded from: classes2.dex */
    public class ConfirmResetCode extends Hilt_VerifyTwoFactorAuth_ConfirmResetCode {
        public AnonymousClass018 A00;

        public static ConfirmResetCode A00(int i, long j) {
            ConfirmResetCode confirmResetCode = new ConfirmResetCode();
            Bundle bundle = new Bundle();
            bundle.putInt("wipeStatus", i);
            bundle.putLong("timeToWaitInMillis", j);
            confirmResetCode.A0U(bundle);
            return confirmResetCode;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int millis;
            AnonymousClass018 r1;
            int i;
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            int i2 = bundle2.getInt("wipeStatus");
            long j = bundle2.getLong("timeToWaitInMillis");
            C004802e r11 = new C004802e(A0p());
            View inflate = LayoutInflater.from(A0p()).inflate(R.layout.two_fa_help_dialog, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(R.id.two_fa_help_dialog_text);
            View findViewById = inflate.findViewById(16908313);
            View findViewById2 = inflate.findViewById(16908314);
            View findViewById3 = inflate.findViewById(16908315);
            View findViewById4 = inflate.findViewById(R.id.spacer);
            findViewById.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 38));
            findViewById2.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 37));
            if (i2 == 1) {
                long millis2 = TimeUnit.DAYS.toMillis(1);
                if (j > millis2) {
                    millis = (int) (j / millis2);
                    r1 = this.A00;
                    i = 3;
                } else {
                    long millis3 = TimeUnit.HOURS.toMillis(1);
                    if (j > millis3) {
                        millis = (int) (j / millis3);
                        r1 = this.A00;
                        i = 2;
                    } else {
                        long millis4 = TimeUnit.MINUTES.toMillis(1);
                        if (j > millis4) {
                            millis = (int) (j / millis4);
                            r1 = this.A00;
                            i = 1;
                        } else {
                            millis = (int) (j / TimeUnit.SECONDS.toMillis(1));
                            r1 = this.A00;
                            i = 0;
                        }
                    }
                }
                textView.setText(A0J(R.string.two_factor_auth_forgot_code_info_with_time, C38131nZ.A02(r1, millis, i)));
            } else if (i2 == 2 || i2 == 3) {
                textView.setText(R.string.two_factor_auth_reset_info);
                findViewById3.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 39));
                findViewById3.setVisibility(0);
                findViewById4.setVisibility(0);
            }
            r11.setView(inflate);
            return r11.create();
        }
    }

    /* loaded from: classes3.dex */
    public class ConfirmWipe extends Hilt_VerifyTwoFactorAuth_ConfirmWipe {
        public static ConfirmWipe A00(int i) {
            ConfirmWipe confirmWipe = new ConfirmWipe();
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("wipeStatus", i);
            confirmWipe.A0U(A0D);
            return confirmWipe;
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int i;
            int i2 = ((AnonymousClass01E) this).A05.getInt("wipeStatus");
            ActivityC000900k A0B = A0B();
            C004802e r2 = new C004802e(A0B);
            C12970iu.A1L(r2, A0B, 59, R.string.two_factor_auth_reset_account_label);
            C72463ee.A0S(r2);
            if (i2 == 1 || i2 == 2) {
                i = R.string.two_factor_auth_reset_wipe_offline_info;
            } else {
                if (i2 == 3) {
                    i = R.string.two_factor_auth_reset_wipe_full_info;
                }
                return r2.create();
            }
            r2.A06(i);
            return r2.create();
        }
    }
}
