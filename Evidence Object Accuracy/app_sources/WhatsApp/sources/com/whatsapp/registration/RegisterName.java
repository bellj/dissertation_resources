package com.whatsapp.registration;

import X.AbstractActivityC28171Kz;
import X.AbstractC116455Vm;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC17940re;
import X.AbstractC32741cf;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass105;
import X.AnonymousClass10O;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Z;
import X.AnonymousClass116;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass16N;
import X.AnonymousClass193;
import X.AnonymousClass19H;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass1AU;
import X.AnonymousClass1CD;
import X.AnonymousClass23M;
import X.AnonymousClass27T;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2FR;
import X.AnonymousClass38U;
import X.AnonymousClass3HG;
import X.AnonymousClass47L;
import X.AnonymousClass4HD;
import X.AnonymousClass4L1;
import X.AnonymousClass4L4;
import X.C004802e;
import X.C103364qd;
import X.C1096952r;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15270mq;
import X.C15330mx;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16490p7;
import X.C16590pI;
import X.C16630pM;
import X.C16970q3;
import X.C17050qB;
import X.C17220qS;
import X.C17960rg;
import X.C18000rk;
import X.C18260sA;
import X.C18350sJ;
import X.C18470sV;
import X.C18620sk;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C18960tL;
import X.C19890uq;
import X.C20220vP;
import X.C20630w4;
import X.C20640w5;
import X.C20650w6;
import X.C20660w7;
import X.C20670w8;
import X.C20680w9;
import X.C20690wA;
import X.C20700wB;
import X.C20710wC;
import X.C20720wD;
import X.C20730wE;
import X.C20740wF;
import X.C20750wG;
import X.C20770wI;
import X.C20780wJ;
import X.C20790wK;
import X.C20800wL;
import X.C20810wM;
import X.C20820wN;
import X.C20830wO;
import X.C20840wP;
import X.C20850wQ;
import X.C21820y2;
import X.C21840y4;
import X.C22040yO;
import X.C22050yP;
import X.C22660zR;
import X.C22670zS;
import X.C22730zY;
import X.C231510o;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25671Ah;
import X.C25771At;
import X.C26041Bu;
import X.C26981Fo;
import X.C26991Fp;
import X.C27011Fr;
import X.C27131Gd;
import X.C36021jC;
import X.C38131nZ;
import X.C38491oB;
import X.C469928m;
import X.DialogC48262Ff;
import X.HandlerC73343g5;
import X.HandlerC73353g6;
import X.RunnableC49072Ja;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.PushnameEmojiBlacklistDialogFragment;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;
import java.util.HashSet;

/* loaded from: classes2.dex */
public class RegisterName extends AbstractActivityC28171Kz {
    public static RunnableC49072Ja A1S;
    public long A00;
    public Bitmap A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public C20770wI A05;
    public C20640w5 A06;
    public AnonymousClass19Y A07;
    public AnonymousClass16N A08;
    public C18790t3 A09;
    public WaEditText A0A;
    public C22730zY A0B;
    public C14650lo A0C;
    public C20690wA A0D;
    public C18960tL A0E;
    public AnonymousClass116 A0F;
    public AnonymousClass130 A0G;
    public C27131Gd A0H;
    public AnonymousClass10S A0I;
    public C15610nY A0J;
    public AnonymousClass10T A0K;
    public AnonymousClass131 A0L;
    public C20730wE A0M;
    public C20840wP A0N;
    public C20720wD A0O;
    public C16970q3 A0P;
    public C16590pI A0Q;
    public C15890o4 A0R;
    public C20830wO A0S;
    public C18260sA A0T;
    public C16490p7 A0U;
    public C15370n3 A0V;
    public C20790wK A0W;
    public C15270mq A0X;
    public C231510o A0Y;
    public C15330mx A0Z;
    public AnonymousClass193 A0a;
    public C25771At A0b;
    public C22050yP A0c;
    public C16120oU A0d;
    public C22040yO A0e;
    public AnonymousClass11G A0f;
    public C17220qS A0g;
    public C20660w7 A0h;
    public C20220vP A0i;
    public C18620sk A0j;
    public C16630pM A0k;
    public C20750wG A0l;
    public AnonymousClass10Z A0m;
    public AnonymousClass38U A0n;
    public C22660zR A0o;
    public C20680w9 A0p;
    public DialogC48262Ff A0q;
    public AnonymousClass27T A0r;
    public AnonymousClass3HG A0s;
    public C20800wL A0t;
    public RegistrationScrollView A0u;
    public AnonymousClass10O A0v;
    public C26991Fp A0w;
    public C27011Fr A0x;
    public C26981Fo A0y;
    public C25661Ag A0z;
    public AnonymousClass4L4 A10;
    public C25671Ah A11;
    public C20810wM A12;
    public AnonymousClass1AU A13;
    public C20630w4 A14;
    public AnonymousClass19H A15;
    public C20780wJ A16;
    public AnonymousClass1CD A17;
    public AnonymousClass105 A18;
    public AnonymousClass12U A19;
    public C252018m A1A;
    public C20700wB A1B;
    public AnonymousClass01H A1C;
    public AnonymousClass01H A1D;
    public AnonymousClass01H A1E;
    public Integer A1F;
    public Integer A1G;
    public Integer A1H;
    public Integer A1I;
    public boolean A1J;
    public boolean A1K = false;
    public boolean A1L;
    public boolean A1M;
    public final Handler A1N;
    public final Handler A1O;
    public final AbstractC116455Vm A1P;
    public final AnonymousClass4L1 A1Q;
    public final C469928m A1R;

    public RegisterName() {
        super(true, true);
        A0R(new C103364qd(this));
        this.A1R = new AnonymousClass47L(this);
        this.A0H = null;
        this.A1N = new HandlerC73343g5(Looper.getMainLooper(), this);
        this.A1O = new HandlerC73353g6(Looper.getMainLooper(), this);
        this.A1P = new C1096952r(this);
        this.A1Q = new AnonymousClass4L1(this);
    }

    public static void A02(Context context, String str) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.Main");
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.addFlags(268435456);
        intent.addFlags(2097152);
        Intent intent2 = new Intent();
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("duplicate", false);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, R.mipmap.icon));
        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.sendBroadcast(intent2);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A1K) {
            this.A1K = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            ((AbstractActivityC28171Kz) this).A05 = (C20650w6) r2.A3A.get();
            ((AbstractActivityC28171Kz) this).A09 = (C18470sV) r2.AK8.get();
            ((AbstractActivityC28171Kz) this).A02 = (C20670w8) r2.AMw.get();
            ((AbstractActivityC28171Kz) this).A03 = (C15550nR) r2.A45.get();
            ((AbstractActivityC28171Kz) this).A0B = (C19890uq) r2.ABw.get();
            ((AbstractActivityC28171Kz) this).A0A = (C20710wC) r2.A8m.get();
            ((AbstractActivityC28171Kz) this).A0E = (AbstractC15850o0) r2.ANA.get();
            ((AbstractActivityC28171Kz) this).A04 = (C17050qB) r2.ABL.get();
            ((AbstractActivityC28171Kz) this).A0C = (C20740wF) r2.AIA.get();
            ((AbstractActivityC28171Kz) this).A0D = (C18350sJ) r2.AHX.get();
            ((AbstractActivityC28171Kz) this).A06 = (C20820wN) r2.ACG.get();
            ((AbstractActivityC28171Kz) this).A08 = (C26041Bu) r2.ACL.get();
            ((AbstractActivityC28171Kz) this).A00 = (AnonymousClass2FR) r1.A0P.get();
            ((AbstractActivityC28171Kz) this).A07 = (C20850wQ) r2.ACI.get();
            this.A14 = (C20630w4) r2.AJF.get();
            this.A13 = (AnonymousClass1AU) r2.AJD.get();
            this.A0Q = (C16590pI) r2.AMg.get();
            this.A06 = (C20640w5) r2.AHk.get();
            this.A15 = (AnonymousClass19H) r2.AJN.get();
            this.A19 = (AnonymousClass12U) r2.AJd.get();
            this.A09 = (C18790t3) r2.AJw.get();
            this.A0d = (C16120oU) r2.ANE.get();
            this.A0h = (C20660w7) r2.AIB.get();
            this.A0P = (C16970q3) r2.A0a.get();
            this.A0z = (C25661Ag) r2.A8G.get();
            this.A0Y = (C231510o) r2.AHO.get();
            this.A0o = (C22660zR) r2.AAk.get();
            this.A07 = (AnonymousClass19Y) r2.AI6.get();
            this.A0g = (C17220qS) r2.ABt.get();
            this.A0e = (C22040yO) r2.A01.get();
            this.A0G = (AnonymousClass130) r2.A41.get();
            this.A1A = (C252018m) r2.A7g.get();
            this.A0J = (C15610nY) r2.AMe.get();
            this.A0p = (C20680w9) r2.AGx.get();
            this.A0D = (C20690wA) r2.AJa.get();
            this.A0I = (AnonymousClass10S) r2.A46.get();
            this.A1B = (C20700wB) r2.A6m.get();
            this.A0O = (C20720wD) r2.A5o.get();
            this.A0c = (C22050yP) r2.A7v.get();
            this.A0K = (AnonymousClass10T) r2.A47.get();
            this.A0M = (C20730wE) r2.A4J.get();
            this.A0T = (C18260sA) r2.AAZ.get();
            this.A0y = (C26981Fo) r2.ABG.get();
            this.A0f = (AnonymousClass11G) r2.AKq.get();
            this.A0F = (AnonymousClass116) r2.A3z.get();
            this.A1E = C18000rk.A00(r1.A0F);
            this.A0a = (AnonymousClass193) r2.A6S.get();
            this.A0b = (C25771At) r2.A7p.get();
            this.A0i = (C20220vP) r2.AC3.get();
            this.A0U = (C16490p7) r2.ACJ.get();
            this.A0l = (C20750wG) r2.AGL.get();
            this.A0m = (AnonymousClass10Z) r2.AGM.get();
            this.A0R = (C15890o4) r2.AN1.get();
            this.A08 = (AnonymousClass16N) r2.AIH.get();
            this.A05 = (C20770wI) r2.AG4.get();
            this.A0v = (AnonymousClass10O) r2.AMM.get();
            this.A0C = (C14650lo) r2.A2V.get();
            this.A0w = (C26991Fp) r2.A5r.get();
            this.A16 = (C20780wJ) r2.AJT.get();
            this.A18 = (AnonymousClass105) r2.AJc.get();
            this.A0W = (C20790wK) r2.A61.get();
            C17960rg builderWithExpectedSize = AbstractC17940re.builderWithExpectedSize(2);
            builderWithExpectedSize.addAll((Iterable) new HashSet());
            Object obj = r2.A8e.get();
            if (obj != null) {
                builderWithExpectedSize.add(obj);
                this.A10 = new AnonymousClass4L4(builderWithExpectedSize.build());
                this.A0t = (C20800wL) r2.AHW.get();
                this.A0j = (C18620sk) r2.AFB.get();
                this.A0k = (C16630pM) r2.AIc.get();
                this.A12 = (C20810wM) r2.AJ5.get();
                this.A0L = (AnonymousClass131) r2.A49.get();
                this.A0S = (C20830wO) r2.A4W.get();
                this.A0E = (C18960tL) r2.ACj.get();
                this.A1C = C18000rk.A00(r2.A0T);
                this.A0N = (C20840wP) r2.A4P.get();
                this.A0x = (C27011Fr) r2.AA2.get();
                this.A0B = (C22730zY) r2.A8Y.get();
                this.A11 = (C25671Ah) r2.A8i.get();
                this.A17 = (AnonymousClass1CD) r2.AJb.get();
                this.A1D = C18000rk.A00(r1.A0E);
                return;
            }
            throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
        }
    }

    @Override // X.AbstractActivityC28171Kz
    public void A2g(boolean z) {
        super.A2g(z);
        DialogC48262Ff r1 = this.A0q;
        if (r1 != null && z) {
            r1.A00(1);
        }
    }

    public void A2h() {
        ((AbstractActivityC28171Kz) this).A05.A07(false);
        ((AbstractActivityC28171Kz) this).A07.A01();
        this.A0M.A04();
        A2e();
    }

    public void A2i() {
        Log.i("registername/start");
        Editable text = this.A0A.getText();
        AnonymousClass009.A05(text);
        String A04 = AbstractC32741cf.A04(text.toString().trim());
        if (C38491oB.A03(A04, AnonymousClass4HD.A01)) {
            Log.w("registername/checkmarks in pushname");
            Adm(PushnameEmojiBlacklistDialogFragment.A00(A04));
        } else if (A04.length() == 0) {
            Log.w("registername/no-pushname");
            ((ActivityC13810kN) this).A05.A07(R.string.register_failure_noname, 0);
        } else {
            RunnableC49072Ja r4 = A1S;
            if (r4 == null || r4.A03) {
                StringBuilder sb = new StringBuilder("registername/check-sinitializer, null?");
                boolean z = false;
                if (r4 == null) {
                    z = true;
                }
                sb.append(z);
                Log.i(sb.toString());
                C20630w4 r0 = this.A14;
                C15570nT r02 = ((ActivityC13790kL) this).A01;
                C16120oU r03 = this.A0d;
                C16970q3 r04 = this.A0P;
                C20660w7 r05 = this.A0h;
                C18470sV r06 = ((AbstractActivityC28171Kz) this).A09;
                C20670w8 r07 = ((AbstractActivityC28171Kz) this).A02;
                C15550nR r08 = ((AbstractActivityC28171Kz) this).A03;
                C15610nY r09 = this.A0J;
                AnonymousClass018 r010 = ((ActivityC13830kP) this).A01;
                C20690wA r011 = this.A0D;
                C20700wB r012 = this.A1B;
                C20710wC r013 = ((AbstractActivityC28171Kz) this).A0A;
                C20720wD r014 = this.A0O;
                C20730wE r015 = this.A0M;
                C20740wF r016 = ((AbstractActivityC28171Kz) this).A0C;
                C20750wG r017 = this.A0l;
                C18350sJ r15 = ((AbstractActivityC28171Kz) this).A0D;
                C14820m6 r14 = ((ActivityC13810kN) this).A09;
                C20770wI r13 = this.A05;
                C20780wJ r12 = this.A16;
                C20790wK r11 = this.A0W;
                C18620sk r10 = this.A0j;
                C20810wM r9 = this.A12;
                C20820wN r8 = ((AbstractActivityC28171Kz) this).A06;
                C20830wO r7 = this.A0S;
                RunnableC49072Ja r018 = new RunnableC49072Ja(this.A1N, r02, r13, r07, r011, this.A0E, r08, r09, r015, this.A0N, r014, r04, r14, r010, r7, r8, r06, r11, r03, r013, r016, r05, r10, r017, this.A1Q, r15, r9, r0, r12, r012);
                A1S = r018;
                r018.A01 = this.A00;
                ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1100000_I0(35, A04, this));
                C36021jC.A01(this, 0);
                if (((CompoundButton) findViewById(R.id.cbx_app_shortcut)).isChecked()) {
                    A02(this, getString(R.string.launcher_app_name));
                }
                View view = this.A03;
                if (view != null) {
                    view.setVisibility(4);
                }
                ((ActivityC13810kN) this).A09.A00.edit().putLong("com.whatsapp.registername.initializer_start_time", System.currentTimeMillis()).apply();
                this.A1O.sendEmptyMessageDelayed(0, 600000);
                this.A0z.A00("home");
            }
        }
    }

    public final void A2j() {
        Log.i("registername/check-for-local-and-remote-backups");
        Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.backup.google.RestoreFromBackupActivity");
        className.setAction("action_show_restore_one_time_setup");
        startActivityForResult(className, 14);
    }

    public final void A2k() {
        View view;
        long j = ((ActivityC13810kN) this).A09.A00.getLong("com.whatsapp.registername.initializer_start_time", -1);
        if (j > 0 && System.currentTimeMillis() - j > 600000 && (view = this.A03) != null) {
            view.setVisibility(0);
        }
    }

    public final void A2l() {
        C14900mE r3 = ((ActivityC13810kN) this).A05;
        AnonymousClass4L4 r8 = this.A10;
        AnonymousClass38U r2 = new AnonymousClass38U(r3, ((ActivityC13810kN) this).A09, ((AbstractActivityC28171Kz) this).A07, this, this.A0x, r8);
        this.A0n = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0055, code lost:
        if (r1 == null) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2m() {
        /*
            r5 = this;
            android.content.res.Resources r1 = r5.getResources()
            r0 = 2131166715(0x7f0705fb, float:1.7947683E38)
            int r2 = r1.getDimensionPixelSize(r0)
            android.content.res.Resources r1 = r5.getResources()
            r0 = 2131166714(0x7f0705fa, float:1.7947681E38)
            float r4 = r1.getDimension(r0)
            r3 = 0
            r5.A1L = r3
            X.0n3 r0 = r5.A0V
            if (r0 == 0) goto L_0x0064
            X.0nT r0 = r5.A01
            r0.A08()
            X.1Ih r0 = r0.A05
            boolean r0 = X.C27611If.A00(r0)
            if (r0 != 0) goto L_0x0064
            android.widget.ImageView r1 = r5.A04
            r0 = 1
            r1.setEnabled(r0)
            android.view.View r1 = r5.A02
            r0 = 8
            r1.setVisibility(r0)
            X.10T r1 = r5.A0K
            X.0n3 r0 = r5.A0V
            java.io.File r0 = r1.A01(r0)
            X.AnonymousClass009.A05(r0)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0057
            X.131 r1 = r5.A0L
            X.0n3 r0 = r5.A0V
            android.graphics.Bitmap r1 = r1.A00(r5, r0, r4, r2)
            if (r1 == 0) goto L_0x0053
            r3 = 1
        L_0x0053:
            r5.A1L = r3
            if (r1 != 0) goto L_0x005e
        L_0x0057:
            r0 = 2131232064(0x7f080540, float:1.8080227E38)
            android.graphics.Bitmap r1 = X.AnonymousClass130.A00(r5, r4, r0, r2)
        L_0x005e:
            android.widget.ImageView r0 = r5.A04
            r0.setImageBitmap(r1)
            return
        L_0x0064:
            android.widget.ImageView r0 = r5.A04
            r0.setEnabled(r3)
            android.view.View r0 = r5.A02
            r0.setVisibility(r3)
            android.graphics.Bitmap r1 = r5.A01
            if (r1 != 0) goto L_0x005e
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ALPHA_8
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r2, r2, r0)
            r5.A01 = r1
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.RegisterName.A2m():void");
    }

    public final void A2n(Integer num, Integer num2) {
        if (this.A0R.A07() && this.A1I.intValue() == 1) {
            this.A1I = num;
        }
        if (this.A0F.A00() && this.A1H.intValue() == 1) {
            this.A1H = num2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r13, int r14, android.content.Intent r15) {
        /*
        // Method dump skipped, instructions count: 522
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.RegisterName.onActivityResult(int, int, android.content.Intent):void");
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C15270mq r0 = this.A0X;
        if (r0 == null || !r0.isShowing()) {
            AnonymousClass12P.A03(this);
        } else {
            this.A0X.dismiss();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass27T r1 = this.A0r;
        if (r1 != null) {
            r1.onCreate(r1.onSaveInstanceState());
            AnonymousClass27T r2 = this.A0r;
            r2.A01.A03 = r2.findViewById(R.id.pay_ed_contact_support);
            A2k();
        }
        DialogC48262Ff r12 = this.A0q;
        if (r12 != null) {
            r12.onCreate(r12.onSaveInstanceState());
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v18, resolved type: com.whatsapp.WaEditText */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v35 */
    /* JADX WARN: Type inference failed for: r0v36, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r0v40 */
    /* JADX WARN: Type inference failed for: r0v59 */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x025d, code lost:
        if (r12 != null) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002c, code lost:
        if (r22.getBoolean("started_gdrive_new_user_activity", false) == false) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0281, code lost:
        if (r12 != null) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x028d, code lost:
        if (r12.contains("@") == false) goto L_0x032d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x028f, code lost:
        r1 = r12.indexOf("@");
        r0 = r0 == true ? 1 : 0;
        r0 = r0 == true ? 1 : 0;
        r0 = r0 == true ? 1 : 0;
        r0 = r0 == true ? 1 : 0;
        r12 = r12.substring(r0, r1).replace('.', ' ');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x032d, code lost:
        r12 = r12.replace('.', ' ');
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractActivityC28171Kz, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r22) {
        /*
        // Method dump skipped, instructions count: 823
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.registration.RegisterName.onCreate(android.os.Bundle):void");
    }

    @Override // X.AbstractActivityC28171Kz, android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 0) {
            Log.i("registername/dialog/initprogress");
            if (A1S == null) {
                Log.w("registername/dialog/initprogress/init-null/remove");
                new Handler(Looper.getMainLooper()).postDelayed(new RunnableBRunnable0Shape10S0100000_I0_10(this, 36), 3);
            }
            AnonymousClass27T r1 = new AnonymousClass27T(((ActivityC13810kN) this).A08, ((ActivityC13790kL) this).A05, ((ActivityC13830kP) this).A01, this);
            this.A0r = r1;
            r1.setCancelable(false);
            return this.A0r;
        } else if (i == 1) {
            Log.w("registername/dialog/failed-net");
            C004802e r4 = new C004802e(this);
            r4.A07(R.string.initialization_fail_title);
            r4.A0A(getString(R.string.initialization_fail_message, getString(R.string.connectivity_self_help_instructions)));
            r4.setPositiveButton(R.string.initialization_fail_retry, new DialogInterface.OnClickListener() { // from class: X.4g1
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    RegisterName registerName = RegisterName.this;
                    registerName.A2i();
                    C36021jC.A00(registerName, 1);
                }
            });
            return r4.create();
        } else if (i == 103) {
            Log.i("registername/dialog/restore");
            DialogC48262Ff r42 = new DialogC48262Ff(this, ((ActivityC13810kN) this).A08, ((ActivityC13790kL) this).A05, ((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A07, this.A0U, this, ((AbstractActivityC28171Kz) this).A0D);
            this.A0q = r42;
            r42.setCancelable(false);
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1100000_I0(34, C38131nZ.A01(((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A07.A06()).toString(), this));
            return this.A0q;
        } else if (i != 109) {
            return super.onCreateDialog(i);
        } else {
            Log.w("registername/dialog/cant-connect");
            return AnonymousClass23M.A03(this, this.A07, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A08, this.A0R, this.A0f, this.A0t, ((ActivityC13830kP) this).A05);
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        if (isFinishing()) {
            this.A0i.A07();
        }
        C27131Gd r1 = this.A0H;
        if (r1 != null) {
            this.A0I.A04(r1);
            this.A0H = null;
        }
        AnonymousClass38U r12 = this.A0n;
        if (r12 != null) {
            r12.A00 = null;
            r12.A03(true);
            this.A0n = null;
        }
        this.A0s.A01();
        RegistrationScrollView registrationScrollView = this.A0u;
        if (registrationScrollView != null) {
            registrationScrollView.getViewTreeObserver().removeOnScrollChangedListener(registrationScrollView.A09);
            if (registrationScrollView.A02 != null) {
                registrationScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(registrationScrollView.A02);
            }
            this.A0u = null;
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 0) {
            this.A0v.A01("register-name");
            this.A0s.A02(this, this.A0v, "register-name");
            return true;
        } else if (itemId != 1) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            ((AbstractActivityC28171Kz) this).A0D.A09();
            startActivity(C14960mK.A01(this));
            finishAffinity();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        Handler handler;
        super.onPause();
        RunnableC49072Ja r2 = A1S;
        if (r2 != null && (handler = r2.A02) != null) {
            handler.removeMessages(0);
            r2.A02 = null;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (A1S != null) {
            C36021jC.A01(this, 0);
            RunnableC49072Ja r2 = A1S;
            Handler handler = this.A1N;
            if (r2.A03) {
                handler.sendEmptyMessage(0);
            }
            r2.A02 = handler;
            A2k();
        }
        if (((ActivityC13790kL) this).A0B.A01() && this.A0r == null) {
            C36021jC.A01(this, 0);
            Log.i("registername/resume reg verified; explicitly display continue screen");
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("started_gdrive_new_user_activity", this.A1M);
    }
}
