package com.whatsapp.registration;

import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C15570nT;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RegistrationCompletedReceiver extends BroadcastReceiver {
    public C15570nT A00;
    public C14820m6 A01;
    public final Object A02;
    public volatile boolean A03;

    public RegistrationCompletedReceiver() {
        this(0);
    }

    public RegistrationCompletedReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = C12970iu.A0S(r1);
                    this.A01 = C12970iu.A0Z(r1);
                    this.A03 = true;
                }
            }
        }
        Log.i("received broadcast that smba was registered on this device");
        if (this.A00.A0F(UserJid.getNullable(intent.getStringExtra("jid")))) {
            Log.i("smba registered this clients phone number");
            C12960it.A0t(C12960it.A08(this.A01), "registration_biz_registered_on_device", true);
        }
    }
}
