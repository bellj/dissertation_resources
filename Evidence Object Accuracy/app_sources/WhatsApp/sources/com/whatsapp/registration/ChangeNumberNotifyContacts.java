package com.whatsapp.registration;

import X.AbstractC005102i;
import X.AbstractC14640lm;
import X.AbstractC28491Nn;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass116;
import X.AnonymousClass12U;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C19990v2;
import X.C238013b;
import X.C58232oM;
import X.ViewTreeObserver$OnPreDrawListenerC101914oI;
import X.ViewTreeObserver$OnPreDrawListenerC101984oP;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.registration.ChangeNumberNotifyContacts;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class ChangeNumberNotifyContacts extends ActivityC13790kL {
    public int A00;
    public int A01;
    public View A02;
    public View A03;
    public View A04;
    public RadioButton A05;
    public RadioButton A06;
    public RadioButton A07;
    public ScrollView A08;
    public Switch A09;
    public TextEmojiLabel A0A;
    public C238013b A0B;
    public AnonymousClass116 A0C;
    public C15550nR A0D;
    public C19990v2 A0E;
    public AnonymousClass12U A0F;
    public List A0G;
    public boolean A0H;

    public ChangeNumberNotifyContacts() {
        this(0);
    }

    public ChangeNumberNotifyContacts(int i) {
        this.A0H = false;
        ActivityC13830kP.A1P(this, 101);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0E = C12980iv.A0c(A1M);
            this.A0F = ActivityC13830kP.A1N(A1M);
            this.A0D = C12960it.A0O(A1M);
            this.A0B = (C238013b) A1M.A1Z.get();
            this.A0C = (AnonymousClass116) A1M.A3z.get();
        }
    }

    public final void A2e() {
        float f;
        boolean canScrollVertically = this.A08.canScrollVertically(1);
        View view = this.A02;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public final void A2f() {
        this.A08.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101914oI(this));
    }

    public final void A2g() {
        this.A01 = 2;
        this.A03.setVisibility(0);
        this.A0G.clear();
        List list = this.A0G;
        ArrayList A0l = C12960it.A0l();
        HashSet A12 = C12970iu.A12();
        A2i(A0l);
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) C12970iu.A0a(it).A0B(UserJid.class);
            if (r1 != null && this.A0E.A0D(r1)) {
                A12.add(r1);
            }
        }
        list.addAll(A12);
    }

    public final void A2h() {
        int i = this.A01;
        boolean z = false;
        Switch r0 = this.A09;
        if (i == 0) {
            r0.setChecked(false);
            this.A0A.setText(R.string.change_number_notify_none);
            this.A03.setVisibility(8);
            this.A06.setChecked(true);
            return;
        }
        r0.setChecked(true);
        int size = this.A0G.size();
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, size, 0);
        Spanned fromHtml = Html.fromHtml(((ActivityC13830kP) this).A01.A0I(objArr, R.plurals.change_number_n_contacts, (long) size));
        SpannableStringBuilder A0J = C12990iw.A0J(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if ("contacts-link".equals(uRLSpan.getURL())) {
                    int spanStart = A0J.getSpanStart(uRLSpan);
                    int spanEnd = A0J.getSpanEnd(uRLSpan);
                    int spanFlags = A0J.getSpanFlags(uRLSpan);
                    A0J.removeSpan(uRLSpan);
                    A0J.setSpan(new C58232oM(this, this), spanStart, spanEnd, spanFlags);
                }
            }
        }
        AbstractC28491Nn.A03(this.A0A);
        AbstractC28491Nn.A04(this.A0A, ((ActivityC13810kN) this).A08);
        this.A0A.setText(A0J);
        this.A03.setVisibility(0);
        this.A05.setChecked(C12960it.A1V(this.A01, 1));
        this.A06.setChecked(C12960it.A1V(this.A01, 2));
        RadioButton radioButton = this.A07;
        if (this.A01 == 3) {
            z = true;
        }
        radioButton.setChecked(z);
    }

    public final void A2i(ArrayList arrayList) {
        this.A0D.A06.A0S(arrayList, 1, false, true);
        Set A03 = this.A0B.A03();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            if (C15370n3.A07(C12970iu.A0a(it), A03)) {
                it.remove();
            }
        }
    }

    public void A2j(List list) {
        ArrayList A0l = C12960it.A0l();
        A2i(A0l);
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            Jid A0B = C12970iu.A0a(it).A0B(UserJid.class);
            if (A0B != null) {
                list.add(A0B);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 150) {
            if (i2 != -1) {
                Log.i("listmembersselector/permissions denied");
                this.A09.setChecked(false);
                return;
            }
            A2g();
        } else if (i != 1) {
            super.onActivityResult(i, i2, intent);
            return;
        } else if (i2 == -1) {
            this.A0G = C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("jids"));
            this.A01 = 3;
        }
        A2h();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (Build.VERSION.SDK_INT >= 21) {
            A2f();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Class<UserJid> cls;
        ArrayList<String> stringArrayListExtra;
        super.onCreate(bundle);
        setTitle(R.string.change_number_title);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0N(true);
        setContentView(R.layout.change_number_notify_contacts);
        C12960it.A12(findViewById(R.id.confirm_change_btn), this, 21);
        Intent intent = getIntent();
        TextView A0M = C12970iu.A0M(this, R.id.change_number_from_to);
        String A0G = ((ActivityC13830kP) this).A01.A0G(C12960it.A0d(intent.getStringExtra("oldJid"), C12960it.A0k("+")));
        String A0G2 = ((ActivityC13830kP) this).A01.A0G(C12960it.A0d(intent.getStringExtra("newJid"), C12960it.A0j("+")));
        Object[] objArr = new Object[2];
        objArr[0] = A0G;
        String A0X = C12960it.A0X(this, A0G2, objArr, 1, R.string.change_number_confirm_old_new);
        int indexOf = A0X.indexOf(A0G);
        int indexOf2 = A0X.indexOf(A0G2);
        SpannableString spannableString = new SpannableString(A0X);
        ForegroundColorSpan A0M2 = C12980iv.A0M(this, R.color.settings_item_title_text);
        int length = A0G.length() + indexOf;
        spannableString.setSpan(A0M2, indexOf, length, 17);
        spannableString.setSpan(new TypefaceSpan("sans-serif-medium"), indexOf, length, 17);
        ForegroundColorSpan A0M3 = C12980iv.A0M(this, R.color.settings_item_title_text);
        int length2 = A0G2.length() + indexOf2;
        spannableString.setSpan(A0M3, indexOf2, length2, 17);
        spannableString.setSpan(new TypefaceSpan("sans-serif-medium"), indexOf2, length2, 17);
        A0M.setText(spannableString);
        this.A08 = (ScrollView) findViewById(R.id.scroll_view);
        this.A04 = findViewById(R.id.notify_contacts_container);
        Switch r1 = (Switch) findViewById(R.id.notify_contacts_switch);
        this.A09 = r1;
        r1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.3ON
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ChangeNumberNotifyContacts changeNumberNotifyContacts = ChangeNumberNotifyContacts.this;
                if (!z) {
                    changeNumberNotifyContacts.A01 = 0;
                    changeNumberNotifyContacts.A03.setVisibility(8);
                    changeNumberNotifyContacts.A0G.clear();
                } else if (!changeNumberNotifyContacts.A0C.A00()) {
                    RequestPermissionActivity.A0D(changeNumberNotifyContacts, R.string.permission_contacts_access_on_notify_contacts_change_number_request, R.string.permission_contacts_access_on_notify_contacts_change_number);
                    return;
                } else {
                    changeNumberNotifyContacts.A2g();
                }
                changeNumberNotifyContacts.A2h();
            }
        });
        C12960it.A12(this.A04, this, 22);
        View findViewById = findViewById(R.id.change_number_radio_buttons_container);
        this.A03 = findViewById;
        this.A05 = (RadioButton) findViewById.findViewById(R.id.change_number_all_btn);
        C12960it.A12(findViewById(R.id.change_number_all), this, 20);
        this.A06 = (RadioButton) this.A03.findViewById(R.id.change_number_chats_btn);
        C12960it.A12(findViewById(R.id.change_number_chats), this, 20);
        this.A07 = (RadioButton) this.A03.findViewById(R.id.change_number_custom_btn);
        C12960it.A12(findViewById(R.id.change_number_custom), this, 20);
        this.A0A = (TextEmojiLabel) findViewById(R.id.change_number_notified_amount);
        this.A02 = findViewById(R.id.bottom_button_container);
        if (bundle != null) {
            int i = bundle.getInt("mode");
            this.A01 = i;
            if (i == 3) {
                cls = UserJid.class;
                stringArrayListExtra = bundle.getStringArrayList("selectedJids");
                this.A0G = C15380n4.A07(cls, stringArrayListExtra);
            }
        } else {
            int intExtra = intent.getIntExtra("mode", 2);
            this.A01 = intExtra;
            if (intExtra == 3) {
                cls = UserJid.class;
                stringArrayListExtra = intent.getStringArrayListExtra("preselectedJids");
                this.A0G = C15380n4.A07(cls, stringArrayListExtra);
            }
        }
        if (this.A0G == null) {
            this.A0G = C12960it.A0l();
        }
        if (!this.A0C.A00()) {
            this.A01 = 0;
            this.A03.setVisibility(8);
            this.A0G.clear();
        } else {
            int i2 = this.A01;
            if (i2 == 1) {
                this.A01 = 1;
                this.A0G.clear();
                A2j(this.A0G);
            } else if (i2 == 2) {
                A2g();
            } else if (i2 == 3) {
                ArrayList A0l = C12960it.A0l();
                A2j(A0l);
                HashSet hashSet = new HashSet(A0l);
                Iterator it = this.A0G.iterator();
                while (it.hasNext()) {
                    if (!hashSet.contains(it.next())) {
                        it.remove();
                    }
                }
            }
        }
        A2h();
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00 = getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation);
            this.A08.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4ob
                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    ChangeNumberNotifyContacts.this.A2e();
                }
            });
            A2f();
        }
    }

    public void onRadioButtonClicked(View view) {
        int id = view.getId();
        if (id == R.id.change_number_all) {
            if (this.A01 != 1) {
                this.A01 = 1;
                this.A0G.clear();
                A2j(this.A0G);
            } else {
                return;
            }
        } else if (id == R.id.change_number_chats) {
            if (this.A01 != 2) {
                A2g();
            } else {
                return;
            }
        } else if (id == R.id.change_number_custom) {
            startActivityForResult(C12990iw.A0D(this, NotifyContactsSelector.class), 1);
            return;
        } else {
            return;
        }
        A2h();
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        boolean isChecked = this.A09.isChecked();
        super.onRestoreInstanceState(bundle);
        this.A04.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101984oP(this, isChecked));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putStringArrayList("selectedJids", C15380n4.A06(this.A0G));
        bundle.putInt("mode", this.A01);
    }
}
