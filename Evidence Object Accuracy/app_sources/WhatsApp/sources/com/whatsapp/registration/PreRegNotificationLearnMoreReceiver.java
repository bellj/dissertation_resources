package com.whatsapp.registration;

import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C18360sK;
import X.C252018m;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* loaded from: classes2.dex */
public class PreRegNotificationLearnMoreReceiver extends BroadcastReceiver {
    public AnonymousClass12P A00;
    public C18360sK A01;
    public C14820m6 A02;
    public C252018m A03;
    public final Object A04;
    public volatile boolean A05;

    public PreRegNotificationLearnMoreReceiver() {
        this(0);
    }

    public PreRegNotificationLearnMoreReceiver(int i) {
        this.A05 = false;
        this.A04 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A05) {
            synchronized (this.A04) {
                if (!this.A05) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = C12980iv.A0W(r1);
                    this.A03 = C12980iv.A0g(r1);
                    this.A02 = C12970iu.A0Z(r1);
                    this.A01 = (C18360sK) r1.AN0.get();
                    this.A05 = true;
                }
            }
        }
        this.A00.A06(context, C12970iu.A0B(this.A03.A03("30035737")).setFlags(268435456));
        this.A02.A1B(false);
        this.A01.A04(20, null);
    }
}
