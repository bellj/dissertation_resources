package com.whatsapp.registration;

import X.AbstractActivityC452520u;
import X.AbstractC452820x;
import X.ActivityC13790kL;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.AnonymousClass0OC;
import X.C004802e;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C22680zT;
import X.C52722bZ;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.registration.SelectPhoneNumberDialog;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class SelectPhoneNumberDialog extends Hilt_SelectPhoneNumberDialog {
    public C22680zT A00;
    public AnonymousClass018 A01;
    public AbstractC452820x A02;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A02 = null;
    }

    @Override // com.whatsapp.registration.Hilt_SelectPhoneNumberDialog, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC452820x) {
            this.A02 = (AbstractC452820x) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ArrayList parcelableArrayList = A03().getParcelableArrayList("deviceSimInfoList");
        AnonymousClass009.A05(parcelableArrayList);
        StringBuilder A0k = C12960it.A0k("select-phone-number-dialog/number-of-suggestions: ");
        A0k.append(parcelableArrayList.size());
        C12960it.A1F(A0k);
        Context A01 = A01();
        C52722bZ r3 = new C52722bZ(A01, this.A00, this.A01, parcelableArrayList);
        C004802e A0S = C12980iv.A0S(A01);
        A0S.A07(R.string.select_phone_number_dialog_title);
        AnonymousClass0OC r0 = A0S.A01;
        r0.A0D = r3;
        r0.A05 = null;
        A0S.setPositiveButton(R.string.use, new DialogInterface.OnClickListener(r3, this, parcelableArrayList) { // from class: X.3Kk
            public final /* synthetic */ C52722bZ A00;
            public final /* synthetic */ SelectPhoneNumberDialog A01;
            public final /* synthetic */ ArrayList A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                SelectPhoneNumberDialog selectPhoneNumberDialog = this.A01;
                ArrayList arrayList = this.A02;
                C52722bZ r1 = this.A00;
                Log.i("select-phone-number-dialog/use-clicked");
                C65993Lw r4 = (C65993Lw) arrayList.get(r1.A00);
                AbstractC452820x r32 = selectPhoneNumberDialog.A02;
                if (r32 != null) {
                    RegisterPhone registerPhone = (RegisterPhone) r32;
                    registerPhone.A0a.A02 = C12960it.A0V();
                    registerPhone.A0Q = r4.A00;
                    String str = r4.A02;
                    registerPhone.A0R = str;
                    ((AbstractActivityC452520u) registerPhone).A09.A03.setText(str);
                    ((AbstractActivityC452520u) registerPhone).A09.A02.setText(registerPhone.A0Q);
                    EditText editText = ((AbstractActivityC452520u) registerPhone).A09.A03;
                    String A0Y = C12960it.A0Y(editText.getText());
                    AnonymousClass009.A05(A0Y);
                    editText.setSelection(A0Y.length());
                }
                selectPhoneNumberDialog.A1B();
            }
        });
        AnonymousClass04S A0O = C12990iw.A0O(A0S, this, 28, R.string.cancel);
        A0O.A00.A0J.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4p2
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                C52722bZ r1 = C52722bZ.this;
                Log.i("select-phone-number-dialog/phone-number-selected");
                if (r1.A00 != i) {
                    r1.A00 = i;
                    r1.notifyDataSetChanged();
                }
            }
        });
        return A0O;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        AbstractC452820x r0 = this.A02;
        if (r0 != null) {
            AbstractActivityC452520u r02 = (AbstractActivityC452520u) r0;
            ((ActivityC13790kL) r02).A0D.A02(r02.A09.A03);
        }
    }
}
