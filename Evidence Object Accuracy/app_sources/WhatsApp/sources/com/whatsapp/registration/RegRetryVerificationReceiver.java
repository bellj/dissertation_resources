package com.whatsapp.registration;

import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1UY;
import X.AnonymousClass22D;
import X.C005602s;
import X.C12960it;
import X.C12970iu;
import X.C14960mK;
import X.C15510nN;
import X.C18360sK;
import X.C22630zO;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RegRetryVerificationReceiver extends BroadcastReceiver {
    public C18360sK A00;
    public AnonymousClass018 A01;
    public C15510nN A02;
    public final Object A03;
    public volatile boolean A04;

    public RegRetryVerificationReceiver() {
        this(0);
    }

    public RegRetryVerificationReceiver(int i) {
        this.A04 = false;
        this.A03 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A04) {
            synchronized (this.A03) {
                if (!this.A04) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = C12960it.A0R(r1);
                    this.A00 = (C18360sK) r1.AN0.get();
                    this.A02 = (C15510nN) r1.AHZ.get();
                    this.A04 = true;
                }
            }
        }
        Log.i("reg-retry-verification-receiver/timeout");
        if (this.A02.A01() || this.A02.A00() == 10) {
            Log.i("app-init/async/registrationretry/verified");
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        String A09 = this.A01.A09(R.string.localized_app_name);
        String A0B = this.A01.A0B(R.string.verification_retry_headline_app_name, A09);
        String A092 = this.A01.A09(R.string.verification_retry_message);
        PendingIntent A00 = AnonymousClass1UY.A00(context, 1, C14960mK.A04(context), 0);
        C005602s A002 = C22630zO.A00(context);
        A002.A0J = "critical_app_alerts@1";
        A002.A0B(A0B);
        A002.A05(currentTimeMillis);
        A002.A02(3);
        A002.A0D(true);
        A002.A0A(A09);
        A002.A09(A092);
        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
        notificationCompat$BigTextStyle.A09(A092);
        A002.A08(notificationCompat$BigTextStyle);
        A002.A09 = A00;
        C18360sK.A01(A002, R.drawable.notifybar);
        this.A00.A03(1, A002.A01());
    }
}
