package com.whatsapp.registration;

import X.AbstractActivityC452520u;
import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC452820x;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass04S;
import X.AnonymousClass0R6;
import X.AnonymousClass0V9;
import X.AnonymousClass105;
import X.AnonymousClass10O;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass23M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass2eq;
import X.AnonymousClass2x3;
import X.AnonymousClass3HG;
import X.AnonymousClass3SB;
import X.AnonymousClass4KK;
import X.AnonymousClass5B8;
import X.AnonymousClass5WF;
import X.C003501n;
import X.C004802e;
import X.C06110Sf;
import X.C103374qe;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15680nj;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16590pI;
import X.C16630pM;
import X.C18350sJ;
import X.C18360sK;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C19890uq;
import X.C20220vP;
import X.C20640w5;
import X.C20800wL;
import X.C20920wX;
import X.C21740xu;
import X.C21820y2;
import X.C21840y4;
import X.C22040yO;
import X.C22650zQ;
import X.C22660zR;
import X.C22670zS;
import X.C22680zT;
import X.C22690zU;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C25661Ag;
import X.C25771At;
import X.C25961Bm;
import X.C26991Fp;
import X.C27011Fr;
import X.C32591cP;
import X.C35751ig;
import X.C36021jC;
import X.C42941w9;
import X.C43511x9;
import X.C467927q;
import X.C52162aM;
import X.C58422p2;
import X.C628638w;
import X.C64323Fc;
import X.C65993Lw;
import X.C83313x2;
import X.C91794Td;
import X.CountDownTimerC51922Zr;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaEditText;
import com.whatsapp.components.PhoneNumberEntry;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class RegisterPhone extends AbstractActivityC452520u implements AnonymousClass5WF, AbstractC452820x {
    public static boolean A0b;
    public static boolean A0c;
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public Dialog A04;
    public ScrollView A05;
    public TextView A06;
    public C22680zT A07;
    public C18790t3 A08;
    public TextEmojiLabel A09;
    public AnonymousClass4KK A0A;
    public C16590pI A0B;
    public C18360sK A0C;
    public C15680nj A0D;
    public C19890uq A0E;
    public C20220vP A0F;
    public C16630pM A0G;
    public C628638w A0H;
    public AnonymousClass3HG A0I;
    public AnonymousClass10O A0J;
    public C26991Fp A0K;
    public C27011Fr A0L;
    public AnonymousClass17R A0M;
    public AnonymousClass105 A0N;
    public C252018m A0O;
    public String A0P;
    public String A0Q;
    public String A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public final C20920wX A0Z;
    public final C467927q A0a;

    @Override // X.AbstractC452720w
    public void AQ2() {
    }

    public RegisterPhone() {
        this(0);
        this.A00 = 30;
        this.A02 = 0;
        this.A03 = 0;
        this.A0a = new C467927q();
        this.A0Z = C20920wX.A00();
    }

    public RegisterPhone(int i) {
        this.A0T = false;
        A0R(new C103374qe(this));
    }

    public static List A02(C22680zT r5, List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C65993Lw r2 = (C65993Lw) it.next();
            if (AbstractActivityC452520u.A03(r5, r2.A00, r2.A02) == 1) {
                arrayList.add(r2);
            }
        }
        return arrayList;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC452520u) this).A04 = (C21740xu) r1.AM1.get();
            ((AbstractActivityC452520u) this).A02 = (C20640w5) r1.AHk.get();
            ((AbstractActivityC452520u) this).A0G = (AnonymousClass12U) r1.AJd.get();
            ((AbstractActivityC452520u) this).A0I = (C22650zQ) r1.A4n.get();
            ((AbstractActivityC452520u) this).A0E = (C25661Ag) r1.A8G.get();
            ((AbstractActivityC452520u) this).A0A = (C22660zR) r1.AAk.get();
            ((AbstractActivityC452520u) this).A03 = (AnonymousClass19Y) r1.AI6.get();
            ((AbstractActivityC452520u) this).A07 = (C22040yO) r1.A01.get();
            ((AbstractActivityC452520u) this).A08 = (AnonymousClass11G) r1.AKq.get();
            ((AbstractActivityC452520u) this).A01 = (C22690zU) r1.A35.get();
            ((AbstractActivityC452520u) this).A06 = (C25771At) r1.A7p.get();
            ((AbstractActivityC452520u) this).A0D = (C18350sJ) r1.AHX.get();
            ((AbstractActivityC452520u) this).A05 = (C15890o4) r1.AN1.get();
            ((AbstractActivityC452520u) this).A0C = (C20800wL) r1.AHW.get();
            this.A0B = (C16590pI) r1.AMg.get();
            this.A08 = (C18790t3) r1.AJw.get();
            this.A0M = (AnonymousClass17R) r1.AIz.get();
            this.A0O = (C252018m) r1.A7g.get();
            this.A0E = (C19890uq) r1.ABw.get();
            this.A07 = (C22680zT) r1.AGW.get();
            this.A0F = (C20220vP) r1.AC3.get();
            this.A0D = (C15680nj) r1.A4e.get();
            this.A0J = (AnonymousClass10O) r1.AMM.get();
            this.A0C = (C18360sK) r1.AN0.get();
            this.A0A = AnonymousClass5B8.A00((C25961Bm) r1.A0o.get());
            this.A0K = (C26991Fp) r1.A5r.get();
            this.A0N = (AnonymousClass105) r1.AJc.get();
            this.A0G = (C16630pM) r1.AIc.get();
            this.A0L = (C27011Fr) r1.AA2.get();
        }
    }

    @Override // X.AbstractActivityC452520u
    public void A2g(String str, String str2, String str3) {
        super.A2g(str, str2, str3);
        A2f(7);
        ((AbstractActivityC452520u) this).A0E.A02("enter_number", "successful");
        boolean z = ((AbstractActivityC452520u) this).A0B.A02;
        C18350sJ r2 = ((AbstractActivityC452520u) this).A0D;
        if (z) {
            AnonymousClass23M.A0H(this, this.A0C, r2, false);
        } else {
            r2.A0A(2);
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.registration.RegisterName");
            startActivity(intent);
        }
        finish();
    }

    public void A2h() {
        this.A0U = false;
        this.A00 = 30;
        if (this.A06.getVisibility() == 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(150);
            this.A06.startAnimation(alphaAnimation);
            alphaAnimation.setAnimationListener(new C83313x2(this));
            return;
        }
        this.A06.setVisibility(8);
    }

    public void A2i() {
        String obj;
        String obj2;
        A0b = false;
        Editable text = ((AbstractActivityC452520u) this).A09.A02.getText();
        if (text == null) {
            obj = null;
        } else {
            obj = text.toString();
        }
        Editable text2 = ((AbstractActivityC452520u) this).A09.A03.getText();
        if (text2 == null) {
            obj2 = null;
        } else {
            obj2 = text2.toString();
        }
        if (obj == null || obj2 == null || obj.equals("") || AnonymousClass23M.A0C(this.A07, obj2, obj, this.A0P) == null) {
            A2h();
        } else {
            new CountDownTimerC51922Zr(this).start();
        }
    }

    public final void A2j() {
        Log.i("register/phone/reset-state");
        this.A0X = false;
        A2f(7);
        AnonymousClass23M.A0J(((ActivityC13810kN) this).A09, "");
        AbstractActivityC452520u.A0Q = 0;
        ((ActivityC13810kN) this).A09.A0i(null);
        ((AbstractActivityC452520u) this).A0D.A0C(null, null, null);
        ((AbstractActivityC452520u) this).A0D.A0A(0);
    }

    public final void A2k() {
        C14900mE r1;
        int i;
        Log.i("register/phone/whats-my-number/permission-granted");
        C467927q r5 = this.A0a;
        r5.A01 = 1;
        TelephonyManager A0N = ((ActivityC13810kN) this).A08.A0N();
        boolean z = false;
        if (A0N != null && A0N.getSimState() == 1) {
            z = true;
        }
        if (z) {
            Log.i("register/phone/whats-my-number/no-sim");
            r5.A04 = -1;
            r1 = ((ActivityC13810kN) this).A05;
            i = R.string.no_sim_error;
        } else {
            List A0F = AnonymousClass23M.A0F(this.A0Z, ((ActivityC13810kN) this).A08, ((AbstractActivityC452520u) this).A05);
            int size = A0F.size();
            List A02 = A02(this.A07, A0F);
            int size2 = A02.size();
            int i2 = 0;
            if (size != size2) {
                i2 = 1;
            }
            r5.A03 = Integer.valueOf(i2);
            r5.A04 = Integer.valueOf(size2);
            if (size2 == 0) {
                Log.i("register/phone/whats-my-number/unable-to-get-phone-number-from-sim");
                r1 = ((ActivityC13810kN) this).A05;
                i = R.string.no_phone_number_sim_error;
            } else {
                Log.i("register/phone/whats-my-number/show-select-phone-number-dialog");
                ((ActivityC13790kL) this).A0D.A01(((AbstractActivityC452520u) this).A09.A03);
                ArrayList<? extends Parcelable> arrayList = new ArrayList<>(A02);
                SelectPhoneNumberDialog selectPhoneNumberDialog = new SelectPhoneNumberDialog();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("deviceSimInfoList", arrayList);
                selectPhoneNumberDialog.A0U(bundle);
                Adl(selectPhoneNumberDialog, null);
                return;
            }
        }
        r1.A07(i, 1);
    }

    public final void A2l(boolean z) {
        long j;
        long j2;
        boolean z2;
        String str;
        int i;
        boolean z3;
        long j3;
        long j4;
        int i2;
        Intent A08;
        A2f(0);
        StringBuilder sb = new StringBuilder("registerPhone/startVerifySms useSmsRetriever ");
        sb.append(z);
        sb.append(", shouldStartBanAppealFlowForBlockedUser ");
        sb.append(this.A0X);
        sb.append(", flashType ");
        sb.append(AbstractActivityC452520u.A0P);
        Log.i(sb.toString());
        if (AbstractActivityC452520u.A0T != null) {
            ((AbstractActivityC452520u) this).A0D.A0A(12);
            j = this.A02;
            j2 = this.A03;
            z2 = false;
            str = AbstractActivityC452520u.A0T;
            i = AbstractActivityC452520u.A0P;
        } else {
            if (this.A0X) {
                ((AbstractActivityC452520u) this).A0D.A0A(9);
                z3 = false;
                j3 = this.A02;
                j4 = this.A03;
                i2 = 3;
            } else {
                boolean z4 = ((AbstractActivityC452520u) this).A0N;
                C18350sJ r1 = ((AbstractActivityC452520u) this).A0D;
                if (z4) {
                    r1.A0A(13);
                    z3 = false;
                    j3 = this.A02;
                    j4 = this.A03;
                    i2 = 1;
                } else {
                    r1.A0A(4);
                    j = this.A02;
                    j2 = this.A03;
                    z2 = false;
                    str = null;
                    i = -1;
                }
            }
            A08 = C14960mK.A08(this, i2, j3, j4, z3, z);
            startActivity(A08);
            finish();
        }
        A08 = C14960mK.A0X(this, str, i, j, j2, z, z2, false, false);
        startActivity(A08);
        finish();
    }

    @Override // X.AbstractC452720w
    public void AT0(String str, String str2, byte[] bArr) {
        this.A02 = (AnonymousClass23M.A02(str, 0) * 1000) + System.currentTimeMillis();
        this.A03 = (AnonymousClass23M.A02(str2, 0) * 1000) + System.currentTimeMillis();
        if (!((AbstractActivityC452520u) this).A0B.A02) {
            C36021jC.A01(this, 21);
        }
    }

    @Override // X.AnonymousClass5WF
    public void AZW() {
        boolean z = true;
        if (((AbstractActivityC452520u) this).A05.A02("android.permission.RECEIVE_SMS") == 0) {
            z = false;
        }
        if (z) {
            Log.i("registerphone/proceedWithoutSmsRetriever/requesting RECEIVE_SMS permission");
            C35751ig r3 = new C35751ig(this);
            r3.A01 = R.drawable.permission_sms;
            r3.A0C = new String[]{"android.permission.RECEIVE_SMS"};
            r3.A02 = R.string.permission_sms_request;
            r3.A0A = null;
            r3.A06 = true;
            startActivityForResult(r3.A00(), 1);
            return;
        }
        Log.i("registerphone/proceedWithoutSmsRetriever/NOT requesting RECEIVE_SMS permission for SMB");
        A2l(false);
    }

    @Override // X.AnonymousClass5WF
    public void AeF() {
        A2l(true);
    }

    @Override // android.app.Activity
    public SharedPreferences getPreferences(int i) {
        return this.A0G.A01(getLocalClassName());
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        if (i == 0) {
            if (i2 == -1) {
                AbstractActivityC452520u.A0R = intent.getStringExtra("cc");
                String stringExtra = intent.getStringExtra("iso");
                String stringExtra2 = intent.getStringExtra("country_name");
                ((AbstractActivityC452520u) this).A09.A02.setText(AbstractActivityC452520u.A0R);
                ((AbstractActivityC452520u) this).A09.A04.setText(stringExtra2);
                ((AbstractActivityC452520u) this).A09.A05.A02(stringExtra);
                SharedPreferences preferences = getPreferences(0);
                SharedPreferences.Editor edit = preferences.edit();
                edit.putString("com.whatsapp.registration.RegisterPhone.input_country_code", AbstractActivityC452520u.A0R);
                edit.putString("com.whatsapp.registration.RegisterPhone.country_code", AbstractActivityC452520u.A0R);
                if (preferences.getInt("com.whatsapp.registration.RegisterPhone.phone_number_position", -1) == -1) {
                    edit.putInt("com.whatsapp.registration.RegisterPhone.phone_number_position", Integer.MAX_VALUE);
                }
                edit.putInt("com.whatsapp.registration.RegisterPhone.country_code_position", -1);
                if (!edit.commit()) {
                    Log.w("registerphone/actresult/commit failed");
                }
            }
            this.A0W = false;
        } else if (i == 1) {
            StringBuilder sb = new StringBuilder("register/phone/sms permission ");
            if (i2 == -1) {
                str = "granted";
            } else {
                str = "denied";
            }
            sb.append(str);
            Log.i(sb.toString());
            A2l(false);
        } else if (i != 155) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            this.A0P = AnonymousClass23M.A0D(((ActivityC13810kN) this).A08, this.A0B, ((AbstractActivityC452520u) this).A05);
            A2k();
        }
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        ServiceInfo serviceInfo;
        String str2;
        String str3;
        String str4;
        super.onCreate(bundle);
        setContentView(R.layout.registerphone);
        ((ActivityC13810kN) this).A09.A0g(null);
        ((ActivityC13810kN) this).A09.A0j(null);
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("pref_flash_call_education_screen_displayed", false).apply();
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("pref_prefer_sms_over_flash", false).apply();
        Context applicationContext = getApplicationContext();
        C14820m6 r0 = ((ActivityC13810kN) this).A09;
        if (applicationContext != null) {
            C06110Sf r4 = new C06110Sf(applicationContext);
            AnonymousClass3SB r9 = new AnonymousClass3SB(r4, r0);
            if (r4.A03()) {
                AnonymousClass0R6.A00("Service connection is valid. No need to re-initialize.");
                r9.ARQ(0);
            } else {
                int i = r4.A00;
                if (i == 1) {
                    str3 = "Client is already in the process of connecting to the service.";
                } else if (i == 3) {
                    str3 = "Client was already closed and can't be reused. Please create another instance.";
                } else {
                    AnonymousClass0R6.A00("Starting install referrer service setup.");
                    Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
                    intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
                    Context context = r4.A03;
                    List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
                    if (queryIntentServices == null || queryIntentServices.isEmpty() || (serviceInfo = queryIntentServices.get(0).serviceInfo) == null) {
                        r4.A00 = 0;
                        str = "Install Referrer service unavailable on device.";
                    } else {
                        String str5 = serviceInfo.packageName;
                        String str6 = serviceInfo.name;
                        if ("com.android.vending".equals(str5) && str6 != null) {
                            if (context.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                                Intent intent2 = new Intent(intent);
                                AnonymousClass0V9 r02 = new AnonymousClass0V9(r4, r9);
                                r4.A01 = r02;
                                try {
                                } catch (SecurityException unused) {
                                    str2 = "No permission to connect to service.";
                                }
                                if (context.bindService(intent2, r02, 1)) {
                                    str = "Service was bonded successfully.";
                                } else {
                                    str2 = "Connection to service is blocked.";
                                    AnonymousClass0R6.A01(str2);
                                    r4.A00 = 0;
                                }
                            }
                        }
                        str2 = "Play Store missing or incompatible. Version 8.3.73 or later required.";
                        AnonymousClass0R6.A01(str2);
                        r4.A00 = 0;
                    }
                    AnonymousClass0R6.A00(str);
                }
                AnonymousClass0R6.A01(str3);
            }
            this.A0I = new AnonymousClass3HG(this.A08, ((ActivityC13830kP) this).A01, ((AbstractActivityC452520u) this).A06, ((ActivityC13810kN) this).A0D, this.A0O, ((ActivityC13830kP) this).A05);
            this.A0P = AnonymousClass23M.A0D(((ActivityC13810kN) this).A08, this.A0B, ((AbstractActivityC452520u) this).A05);
            if (bundle != null) {
                this.A0X = bundle.getBoolean("shouldStartBanAppealForBlockedUser");
            }
            if (((ActivityC13790kL) this).A0B.A00() != 1) {
                Log.e("register/phone/create/wrong-state bounce to main");
                startActivity(C14960mK.A04(this));
                finish();
                return;
            }
            Toolbar toolbar = (Toolbar) findViewById(R.id.title_toolbar);
            if (!ViewConfiguration.get(this).hasPermanentMenuKey()) {
                A1e(toolbar);
                AbstractC005102i A1U = A1U();
                if (A1U != null) {
                    A1U.A0M(false);
                    A1U.A0P(false);
                }
            }
            ((TextView) findViewById(R.id.register_phone_toolbar_title)).setText(R.string.register_phone_header_experiment);
            Intent intent3 = getIntent();
            if (intent3.getExtras() != null) {
                if (intent3.getBooleanExtra("com.whatsapp.registration.RegisterPhone.show_underage_account_ban_dialog", false)) {
                    C36021jC.A01(this, 125);
                }
                if (intent3.getBooleanExtra("com.whatsapp.registration.RegisterPhone.resetstate", false)) {
                    A2j();
                }
                this.A0S = intent3.getBooleanExtra("com.whatsapp.registration.RegisterPhone.clear_phone_number", false);
                if (!(intent3.getStringExtra("com.whatsapp.registration.RegisterPhone.country_code") == null || intent3.getStringExtra("com.whatsapp.registration.RegisterPhone.phone_number") == null)) {
                    getPreferences(0).edit().putString("com.whatsapp.registration.RegisterPhone.input_phone_number", intent3.getStringExtra("com.whatsapp.registration.RegisterPhone.phone_number")).putString("com.whatsapp.registration.RegisterPhone.input_country_code", intent3.getStringExtra("com.whatsapp.registration.RegisterPhone.country_code")).apply();
                }
                if (intent3.getBooleanExtra("com.whatsapp.registration.RegisterPhone.tapped_sms_link", false)) {
                    Log.i("register/phone/link/instructions/dialog");
                    Adp(getString(R.string.register_tapped_link_no_phone_number, getString(R.string.ok)));
                }
            } else {
                this.A0S = false;
            }
            C91794Td r1 = new C91794Td();
            ((AbstractActivityC452520u) this).A09 = r1;
            r1.A05 = (PhoneNumberEntry) findViewById(R.id.registration_fields);
            C91794Td r2 = ((AbstractActivityC452520u) this).A09;
            PhoneNumberEntry phoneNumberEntry = r2.A05;
            phoneNumberEntry.A04 = new AnonymousClass2x3(this);
            r2.A02 = phoneNumberEntry.A02;
            r2.A04 = (TextView) findViewById(R.id.registration_country);
            ((AbstractActivityC452520u) this).A09.A04.setBackground(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.abc_spinner_textfield_background_material), ((ActivityC13830kP) this).A01));
            C91794Td r12 = ((AbstractActivityC452520u) this).A09;
            WaEditText waEditText = r12.A05.A03;
            r12.A03 = waEditText;
            C42941w9.A03(waEditText);
            if (((ActivityC13830kP) this).A01.A04().A06) {
                ((AbstractActivityC452520u) this).A09.A05.setPadding(getResources().getDimensionPixelSize(R.dimen.card_h_padding), ((AbstractActivityC452520u) this).A09.A05.getPaddingTop(), ((AbstractActivityC452520u) this).A09.A05.getPaddingRight(), ((AbstractActivityC452520u) this).A09.A05.getPaddingBottom());
            }
            this.A05 = (ScrollView) findViewById(R.id.scroll_view);
            TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.registration_info);
            this.A09 = textEmojiLabel;
            textEmojiLabel.A07 = new C52162aM();
            TextEmojiLabel textEmojiLabel2 = this.A09;
            textEmojiLabel2.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel2, ((ActivityC13810kN) this).A08));
            TextEmojiLabel textEmojiLabel3 = this.A09;
            C252818u r11 = ((ActivityC13790kL) this).A02;
            String string = getString(R.string.tos_registration_info);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(string));
            URLSpan[] uRLSpanArr = (URLSpan[]) spannableStringBuilder.getSpans(0, string.length(), URLSpan.class);
            if (uRLSpanArr != null) {
                for (URLSpan uRLSpan : uRLSpanArr) {
                    spannableStringBuilder.setSpan(new C58422p2(this, this, r11.A00, r11.A01, r11, r11.A02, uRLSpan.getURL()), spannableStringBuilder.getSpanStart(uRLSpan), spannableStringBuilder.getSpanEnd(uRLSpan), spannableStringBuilder.getSpanFlags(uRLSpan));
                }
                for (URLSpan uRLSpan2 : uRLSpanArr) {
                    spannableStringBuilder.removeSpan(uRLSpan2);
                }
            }
            textEmojiLabel3.setText(spannableStringBuilder);
            this.A09.setVisibility(8);
            TextView textView = (TextView) findViewById(R.id.mistyped_undercard_text);
            this.A06 = textView;
            textView.setVisibility(8);
            if (getPreferences(0).getString("com.whatsapp.registration.RegisterPhone.input_country_code", null) == null) {
                TelephonyManager A0N = ((ActivityC13810kN) this).A08.A0N();
                if (A0N == null) {
                    str4 = "register/phone tm=null";
                } else {
                    String simCountryIso = A0N.getSimCountryIso();
                    if (simCountryIso != null) {
                        try {
                            String A04 = this.A07.A04(simCountryIso);
                            if (A04 != null) {
                                SharedPreferences.Editor edit = getPreferences(0).edit();
                                edit.putString("com.whatsapp.registration.RegisterPhone.input_country_code", A04);
                                if (!edit.commit()) {
                                    str4 = "register/phone/input_cc/commit failed";
                                }
                            }
                        } catch (IOException e) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("register/phone/iso: ");
                            sb.append(simCountryIso);
                            sb.append(" failed to lookupCallingCode from CountryPhoneInfo");
                            Log.e(sb.toString(), e);
                        }
                    }
                }
                Log.w(str4);
            }
            ((AbstractActivityC452520u) this).A09.A04.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 12));
            ((AbstractActivityC452520u) this).A09.A03.requestFocus();
            ((AbstractActivityC452520u) this).A09.A03.setCursorVisible(true);
            Button button = (Button) findViewById(R.id.registration_submit);
            button.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 13));
            String str7 = AbstractActivityC452520u.A0R;
            if (str7 != null) {
                ((AbstractActivityC452520u) this).A09.A02.setText(str7);
            }
            String charSequence = ((AbstractActivityC452520u) this).A09.A04.getText().toString();
            if (charSequence.length() > 0) {
                ((AbstractActivityC452520u) this).A09.A05.A02(charSequence);
            }
            Point point = new Point();
            getWindowManager().getDefaultDisplay().getSize(point);
            if (point.y <= 480) {
                getWindow().setSoftInputMode(3);
            }
            if (((AbstractActivityC452520u) this).A02.A03()) {
                Log.w("register/phone/clock-wrong");
                C43511x9.A01(this, this.A0E, this.A0F);
            } else if (((AbstractActivityC452520u) this).A02.A02()) {
                Log.w("register/phone/sw-expired");
                C43511x9.A02(this, this.A0E, this.A0F);
            }
            this.A05.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(button, this) { // from class: X.3NX
                public final /* synthetic */ Button A00;
                public final /* synthetic */ RegisterPhone A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
                public final void onGlobalLayout() {
                    RegisterPhone registerPhone = this.A01;
                    Button button2 = this.A00;
                    int height = registerPhone.A05.getRootView().getHeight() - registerPhone.A05.getHeight();
                    if (height > registerPhone.getResources().getDimensionPixelSize(R.dimen.registration_scroll_view_density)) {
                        registerPhone.A05.smoothScrollTo(0, button2.getTop());
                        StringBuilder A0k = C12960it.A0k("register/name/layout heightDiff:");
                        A0k.append(height);
                        Log.i(C12960it.A0d("scroll view", A0k));
                    }
                }
            });
            Log.i("register/phone/whats-my-number/enabled");
            TextEmojiLabel textEmojiLabel4 = (TextEmojiLabel) findViewById(R.id.description);
            textEmojiLabel4.A07 = new C52162aM();
            textEmojiLabel4.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel4, ((ActivityC13810kN) this).A08));
            textEmojiLabel4.setText(AnonymousClass23M.A08(new RunnableBRunnable0Shape10S0100000_I0_10(this, 40), getString(R.string.register_phone_number_code_confirm_new), "whats-my-number"));
            textEmojiLabel4.setLinkTextColor(AnonymousClass00T.A00(this, R.color.link_color));
            findViewById(R.id.carrier_charge_warning).setVisibility(0);
            return;
        }
        throw new IllegalArgumentException("Please provide a valid Context.");
    }

    @Override // X.AbstractActivityC452520u, android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 21) {
            return super.onCreateDialog(i);
        }
        Log.i("register/phone/dialog/num_confirm");
        boolean z = ((AbstractActivityC452520u) this).A0M;
        int i2 = R.string.register_phone_phone_number_confirmation_message_new;
        if (z) {
            i2 = R.string.smb_register_possible_migration;
        }
        String string = getString(i2, ((ActivityC13830kP) this).A01.A0G(AnonymousClass23M.A0E(AbstractActivityC452520u.A0R, AbstractActivityC452520u.A0S)));
        C004802e r2 = new C004802e(this);
        r2.A0A(Html.fromHtml(string));
        r2.A0B(false);
        boolean z2 = ((AbstractActivityC452520u) this).A0M;
        int i3 = R.string.ok;
        if (z2) {
            i3 = R.string.smb_register_possible_migration_switch;
        }
        r2.setPositiveButton(i3, new DialogInterface.OnClickListener() { // from class: X.3KG
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                RegisterPhone registerPhone = RegisterPhone.this;
                C36021jC.A00(registerPhone, 21);
                ((ActivityC13810kN) registerPhone).A09.A0v(AbstractActivityC452520u.A0R, AbstractActivityC452520u.A0S);
                if (AbstractActivityC452520u.A0T != null || registerPhone.A0X || !AnonymousClass1CE.A00(((ActivityC13810kN) registerPhone).A08, AbstractActivityC452520u.A0P)) {
                    C63183Ap.A00(registerPhone.A0B, registerPhone);
                    return;
                }
                registerPhone.A2f(0);
                registerPhone.A2G(C14960mK.A0A(registerPhone, registerPhone.A02, registerPhone.A03, false), true);
            }
        });
        r2.A00(R.string.register_edit_button, new DialogInterface.OnClickListener() { // from class: X.4g2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                RegisterPhone registerPhone = RegisterPhone.this;
                registerPhone.A2f(0);
                C36021jC.A00(registerPhone, 21);
            }
        });
        AnonymousClass04S create = r2.create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: X.4hI
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                RegisterPhone.this.A04 = null;
            }
        });
        this.A04 = create;
        return create;
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 5, 0, R.string.registration_help);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        if (this.A0H != null) {
            Log.i("register/phone/destroy canceling task");
            this.A0H.A03(true);
            this.A0H = null;
        }
        this.A0I.A01();
        super.onDestroy();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra("com.whatsapp.registration.RegisterPhone.tapped_sms_link", false)) {
            Log.i("register/phone/newintent/link/instructions/dialog");
            Adp(getString(R.string.register_tapped_link_no_phone_number, getString(R.string.ok)));
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String obj;
        String str;
        String str2;
        switch (menuItem.getItemId()) {
            case 0:
                ((AbstractActivityC452520u) this).A0D.A09();
                startActivity(C14960mK.A01(this));
                finishAffinity();
                return true;
            case 1:
                String replaceAll = ((AbstractActivityC452520u) this).A09.A02.getText().toString().replaceAll("\\D", "");
                String replaceAll2 = ((AbstractActivityC452520u) this).A09.A03.getText().toString().replaceAll("\\D", "");
                byte[] A0C = C003501n.A0C();
                StringBuilder sb = new StringBuilder();
                sb.append(replaceAll);
                sb.append(replaceAll2);
                C003501n.A0A(this, C32591cP.A00(sb.toString()), A0C);
                return true;
            case 2:
                C003501n.A07(this);
                return true;
            case 3:
                ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(getApplicationContext(), 36, ((ActivityC13810kN) this).A09));
                return true;
            case 4:
                String replaceAll3 = ((AbstractActivityC452520u) this).A09.A02.getText().toString().replaceAll("\\D", "");
                String replaceAll4 = ((AbstractActivityC452520u) this).A09.A03.getText().toString().replaceAll("\\D", "");
                StringBuilder sb2 = new StringBuilder();
                sb2.append(replaceAll3);
                sb2.append(replaceAll4);
                byte[] A0F = C003501n.A0F(this, C32591cP.A00(sb2.toString()));
                StringBuilder sb3 = new StringBuilder("register-phone rc=");
                if (A0F == null) {
                    obj = "(null)";
                } else {
                    StringBuilder sb4 = new StringBuilder();
                    int length = A0F.length;
                    for (int i = 0; i < length; i++) {
                        sb4.append(String.format("%02X", Byte.valueOf(A0F[i])));
                    }
                    obj = sb4.toString();
                }
                sb3.append(obj);
                Log.i(sb3.toString());
                return true;
            case 5:
                AnonymousClass10O r1 = this.A0J;
                if (((AbstractActivityC452520u) this).A0L) {
                    str = "validNumber";
                } else {
                    str = "notValidNumber";
                }
                r1.A02(str);
                AnonymousClass10O r12 = this.A0J;
                if (((AbstractActivityC452520u) this).A0K) {
                    str2 = "emptyNumber";
                } else {
                    str2 = "notEmptyNumber";
                }
                r12.A02(str2);
                this.A0J.A01("register-phone");
                this.A0I.A02(this, this.A0J, "register-phone");
                return true;
            case 6:
                startActivity(new Intent().setClassName(this, "com.whatsapp.DebugToolsActivity"));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        C64323Fc r1 = ((AbstractActivityC452520u) this).A0B;
        r1.A02 = true;
        AnonymousClass23M.A0J(r1.A04, AnonymousClass23M.A00);
        StringBuilder sb = new StringBuilder("register/phone/pause ");
        sb.append(AbstractActivityC452520u.A0O);
        Log.i(sb.toString());
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putString("com.whatsapp.registration.RegisterPhone.country_code", AbstractActivityC452520u.A0R);
        edit.putString("com.whatsapp.registration.RegisterPhone.phone_number", AbstractActivityC452520u.A0S);
        edit.putInt("com.whatsapp.registration.RegisterPhone.verification_state", AbstractActivityC452520u.A0O);
        edit.putString("com.whatsapp.registration.RegisterPhone.input_phone_number", ((AbstractActivityC452520u) this).A09.A03.getText().toString());
        edit.putString("com.whatsapp.registration.RegisterPhone.input_country_code", ((AbstractActivityC452520u) this).A09.A02.getText().toString());
        edit.putInt("com.whatsapp.registration.RegisterPhone.country_code_position", AnonymousClass23M.A00(((AbstractActivityC452520u) this).A09.A02));
        edit.putInt("com.whatsapp.registration.RegisterPhone.phone_number_position", AnonymousClass23M.A00(((AbstractActivityC452520u) this).A09.A03));
        if (!edit.commit()) {
            Log.w("register/phone/pause/commit failed");
        }
    }

    @Override // X.AbstractActivityC452520u, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        ((AbstractActivityC452520u) this).A0B.A00();
        SharedPreferences preferences = getPreferences(0);
        AbstractActivityC452520u.A0R = preferences.getString("com.whatsapp.registration.RegisterPhone.country_code", null);
        AbstractActivityC452520u.A0S = preferences.getString("com.whatsapp.registration.RegisterPhone.phone_number", null);
        AbstractActivityC452520u.A0O = preferences.getInt("com.whatsapp.registration.RegisterPhone.verification_state", 7);
        if (this.A0S) {
            this.A0S = false;
            ((AbstractActivityC452520u) this).A09.A03.setText("");
        } else {
            String string = preferences.getString("com.whatsapp.registration.RegisterPhone.input_phone_number", null);
            ((AbstractActivityC452520u) this).A09.A03.setText(string);
            if (!TextUtils.isEmpty(string)) {
                ((AbstractActivityC452520u) this).A0K = false;
                ((AbstractActivityC452520u) this).A0L = true;
            }
        }
        ((AbstractActivityC452520u) this).A09.A02.setText(preferences.getString("com.whatsapp.registration.RegisterPhone.input_country_code", null));
        if (TextUtils.isEmpty(((AbstractActivityC452520u) this).A09.A02.getText())) {
            ((AbstractActivityC452520u) this).A09.A02.requestFocus();
        }
        AnonymousClass23M.A0I(((AbstractActivityC452520u) this).A09.A03, preferences.getInt("com.whatsapp.registration.RegisterPhone.phone_number_position", -1));
        AnonymousClass23M.A0I(((AbstractActivityC452520u) this).A09.A02, preferences.getInt("com.whatsapp.registration.RegisterPhone.country_code_position", -1));
        StringBuilder sb = new StringBuilder("register/phone/resume ");
        sb.append(AbstractActivityC452520u.A0O);
        Log.i(sb.toString());
        if (AbstractActivityC452520u.A0O == 15) {
            if (AbstractActivityC452520u.A0R == null || AbstractActivityC452520u.A0S == null) {
                Log.i("register/phone/reset-state");
                A2f(7);
            } else {
                C36021jC.A01(this, 21);
            }
        }
        this.A0C.A04(1, null);
        ((AbstractActivityC452520u) this).A0D.A0A(1);
        C15680nj r1 = this.A0D;
        r1.A00.A0B();
        ArrayList arrayList = r1.A01;
        synchronized (arrayList) {
            arrayList.clear();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("shouldStartBanAppealForBlockedUser", this.A0X);
    }
}
