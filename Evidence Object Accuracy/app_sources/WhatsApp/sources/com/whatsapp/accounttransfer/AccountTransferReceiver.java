package com.whatsapp.accounttransfer;

import X.AbstractC14440lR;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass1UB;
import X.AnonymousClass1US;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class AccountTransferReceiver extends BroadcastReceiver {
    public AnonymousClass01d A00;
    public AbstractC14440lR A01;
    public final Object A02;
    public volatile boolean A03;

    public AccountTransferReceiver() {
        this(0);
    }

    public AccountTransferReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        KeyguardManager A07;
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = C12960it.A0T(r1);
                    this.A00 = C12960it.A0Q(r1);
                    this.A03 = true;
                }
            }
        }
        String action = intent.getAction();
        Log.i(C12960it.A0d(action, C12960it.A0k("AccountTransferReceiver/onReceive/action=")));
        if (AnonymousClass1US.A0C(action)) {
            str = "AccountTransferReceiver/onReceive/action is empty";
        } else {
            AnonymousClass01d r2 = this.A00;
            if (Build.VERSION.SDK_INT < 23 || (A07 = r2.A07()) == null || !A07.isDeviceSecure() || AnonymousClass1UB.A00(context) != 0) {
                str = "AccountTransferReceiver/onReceive/disabled";
            } else if (action.equals("com.google.android.gms.auth.START_ACCOUNT_EXPORT")) {
                C12990iw.A1O(this.A01, context, 14);
                return;
            } else {
                return;
            }
        }
        Log.i(str);
    }
}
