package com.whatsapp;

import X.AnonymousClass01J;
import X.AnonymousClass025;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C14820m6;
import X.C15510nN;
import X.C15570nT;
import X.C20220vP;
import X.C20230vQ;
import X.C20380vf;
import X.C20390vg;
import X.C20400vh;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.push.RegistrationIntentService;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class UpdatedOurAppReceiver extends BroadcastReceiver {
    public C15570nT A00;
    public C14820m6 A01;
    public C20220vP A02;
    public C20230vQ A03;
    public C20380vf A04;
    public C20400vh A05;
    public C20390vg A06;
    public C15510nN A07;
    public final Object A08;
    public volatile boolean A09;

    public UpdatedOurAppReceiver() {
        this(0);
    }

    public UpdatedOurAppReceiver(int i) {
        this.A09 = false;
        this.A08 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A09) {
            synchronized (this.A08) {
                if (!this.A09) {
                    AnonymousClass01J r2 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = C12970iu.A0S(r2);
                    this.A01 = C12970iu.A0Z(r2);
                    this.A02 = (C20220vP) r2.AC3.get();
                    this.A03 = (C20230vQ) r2.ACe.get();
                    this.A07 = (C15510nN) r2.AHZ.get();
                    this.A04 = (C20380vf) r2.ACR.get();
                    this.A06 = (C20390vg) r2.AEi.get();
                    this.A05 = (C20400vh) r2.AE8.get();
                    this.A09 = true;
                }
            }
        }
        if (intent != null && "android.intent.action.MY_PACKAGE_REPLACED".equals(intent.getAction())) {
            Log.i("received broadcast that com.whatsapp was updated");
            this.A01.A00.getInt("c2dm_app_vers", 0);
            this.A01.A00.getString("c2dm_reg_id", null);
            C15570nT r0 = this.A00;
            r0.A08();
            if (r0.A00 != null) {
                Log.i("updatedappreceiver/request-refresh");
                RegistrationIntentService.A01(context.getApplicationContext());
            } else {
                Log.i("updateappreceiver/skip-refresh");
            }
            if (this.A07.A01()) {
                Log.i("updatedappreceiver/update-notif");
                this.A02.A07();
                this.A03.A04(true);
                this.A04.A01();
                this.A06.A01();
                this.A05.A00();
            }
            AnonymousClass025.A00(this.A01.A04());
        }
    }
}
