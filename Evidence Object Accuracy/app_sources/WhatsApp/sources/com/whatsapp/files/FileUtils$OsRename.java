package com.whatsapp.files;

import X.C12960it;
import android.system.ErrnoException;
import android.system.Os;
import com.whatsapp.util.Log;
import java.io.File;

/* loaded from: classes3.dex */
public class FileUtils$OsRename {
    public static int attempt(File file, File file2) {
        try {
            Os.rename(file.getAbsolutePath(), file2.getAbsolutePath());
            return -1;
        } catch (ErrnoException e) {
            StringBuilder A0k = C12960it.A0k("MMS Os.rename also failed, from=");
            A0k.append(file.getAbsolutePath());
            A0k.append(" to=");
            A0k.append(file2.getAbsolutePath());
            A0k.append(" errno=");
            Log.e(C12960it.A0f(A0k, e.errno), e);
            return e.errno;
        }
    }
}
