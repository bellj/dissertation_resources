package com.whatsapp.viewsharedcontacts;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass190;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass4SA;
import X.C103664r7;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16370ot;
import X.C16590pI;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22680zT;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26501Ds;
import X.C30041Vv;
import X.C30721Yo;
import X.C42941w9;
import X.C626438a;
import X.C75623k9;
import X.C90484Ob;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public class ViewSharedContactArrayActivity extends ActivityC13790kL {
    public C22680zT A00;
    public C16170oZ A01;
    public C14650lo A02;
    public C15550nR A03;
    public AnonymousClass190 A04;
    public C15610nY A05;
    public AnonymousClass1J1 A06;
    public C21270x9 A07;
    public C16590pI A08;
    public C15890o4 A09;
    public AnonymousClass018 A0A;
    public C16370ot A0B;
    public C15650ng A0C;
    public C16120oU A0D;
    public AbstractC14640lm A0E;
    public AnonymousClass198 A0F;
    public C26501Ds A0G;
    public AnonymousClass19Z A0H;
    public List A0I;
    public Pattern A0J;
    public C30721Yo A0K;
    public boolean A0L;
    public boolean A0M;
    public final ArrayList A0N;
    public final ArrayList A0O;
    public final ArrayList A0P;
    public final List A0Q;

    public ViewSharedContactArrayActivity() {
        this(0);
        this.A0N = new ArrayList();
        this.A0O = new ArrayList();
        this.A0Q = new ArrayList();
        this.A0P = new ArrayList();
    }

    public ViewSharedContactArrayActivity(int i) {
        this.A0L = false;
        A0R(new C103664r7(this));
    }

    public static final C90484Ob A02(SparseArray sparseArray, int i) {
        C90484Ob r0 = (C90484Ob) sparseArray.get(i);
        if (r0 != null) {
            return r0;
        }
        C90484Ob r02 = new C90484Ob();
        sparseArray.put(i, r02);
        return r02;
    }

    public static /* synthetic */ String A03(ViewSharedContactArrayActivity viewSharedContactArrayActivity, Class cls, int i) {
        String str = null;
        try {
            str = viewSharedContactArrayActivity.A0A.A00.getResources().getString(((Integer) cls.getMethod("getTypeLabelResource", Integer.TYPE).invoke(null, Integer.valueOf(i))).intValue());
            return str;
        } catch (Exception e) {
            Log.e(e);
            return str;
        }
    }

    public static /* synthetic */ void A09(C75623k9 r3) {
        r3.A01.setClickable(false);
        ImageView imageView = r3.A04;
        imageView.setVisibility(8);
        imageView.setClickable(false);
        ImageView imageView2 = r3.A05;
        imageView2.setVisibility(8);
        imageView2.setClickable(false);
    }

    public static /* synthetic */ void A0A(C75623k9 r4, ViewSharedContactArrayActivity viewSharedContactArrayActivity, String str, String str2, int i, int i2, boolean z) {
        TextView textView = r4.A07;
        if (i2 > 1) {
            textView.setMaxLines(i2);
            textView.setSingleLine(false);
        } else {
            textView.setSingleLine(true);
        }
        C42941w9.A03(textView);
        if (!str.equalsIgnoreCase("null")) {
            textView.setText(str);
        }
        if (str2 == null || str2.equalsIgnoreCase("null")) {
            r4.A06.setText(R.string.no_phone_type);
        } else {
            r4.A06.setText(str2);
        }
        r4.A03.setImageResource(i);
        if (viewSharedContactArrayActivity.A0M) {
            CheckBox checkBox = r4.A02;
            checkBox.setChecked(z);
            checkBox.setClickable(false);
            checkBox.setVisibility(0);
            r4.A00.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(viewSharedContactArrayActivity, 21));
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A08 = (C16590pI) r1.AMg.get();
            this.A0D = (C16120oU) r1.ANE.get();
            this.A01 = (C16170oZ) r1.AM4.get();
            this.A0G = (C26501Ds) r1.AML.get();
            this.A0H = (AnonymousClass19Z) r1.A2o.get();
            this.A07 = (C21270x9) r1.A4A.get();
            this.A03 = (C15550nR) r1.A45.get();
            this.A05 = (C15610nY) r1.AMe.get();
            this.A0A = (AnonymousClass018) r1.ANb.get();
            this.A0C = (C15650ng) r1.A4m.get();
            this.A00 = (C22680zT) r1.AGW.get();
            this.A04 = (AnonymousClass190) r1.AIZ.get();
            this.A0F = (AnonymousClass198) r1.A0J.get();
            this.A0B = (C16370ot) r1.A2b.get();
            this.A09 = (C15890o4) r1.AN1.get();
            this.A02 = (C14650lo) r1.A2V.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.error_parse_vcard) {
            finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            if (i2 == -1 && this.A0K != null) {
                String str = null;
                if (!(intent == null || intent.getData() == null)) {
                    str = intent.getData().getLastPathSegment();
                }
                this.A04.A02(this.A0K.A08(), str, this.A0P, this.A0Q);
            }
            this.A0F.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.view_shared_contact_array);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("vcard");
        AnonymousClass1IS A09 = C30041Vv.A09(intent.getBundleExtra("vcard_message"));
        List stringArrayListExtra = intent.getStringArrayListExtra("vcard_array");
        Uri uri = (Uri) intent.getParcelableExtra("vcard_uri");
        ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("vcard_sender_infos");
        if (stringExtra != null) {
            stringArrayListExtra = Collections.singletonList(stringExtra);
        }
        AnonymousClass4SA r9 = new AnonymousClass4SA(uri, A09, stringArrayListExtra, parcelableArrayListExtra);
        this.A06 = this.A07.A04(this, "view-shared-contact-array");
        this.A0M = getIntent().getBooleanExtra("edit_mode", true);
        this.A0E = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        this.A0I = r9.A02;
        AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
        C16590pI r5 = this.A08;
        C26501Ds r8 = this.A0G;
        r1.Aaz(new C626438a(this.A02, this.A03, r5, this.A0A, this.A0B, r8, r9, this), new Void[0]);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.A00();
    }

    public final void toggleCheckBox(View view) {
        CompoundButton compoundButton = (CompoundButton) view.findViewById(R.id.cbx);
        boolean z = true;
        if (compoundButton.isChecked()) {
            z = false;
        }
        compoundButton.setChecked(z);
        ((C90484Ob) view.getTag()).A01 = compoundButton.isChecked();
    }
}
