package com.whatsapp;

import X.AbstractServiceC003801r;
import X.AnonymousClass22D;
import X.C12960it;
import X.C12970iu;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/* loaded from: classes2.dex */
public class AlarmBroadcastReceiver extends BroadcastReceiver {
    public final Object A00;
    public volatile boolean A01;

    public AlarmBroadcastReceiver() {
        this(0);
    }

    public AlarmBroadcastReceiver(int i) {
        this.A01 = false;
        this.A00 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A01) {
            synchronized (this.A00) {
                if (!this.A01) {
                    AnonymousClass22D.A00(context);
                    this.A01 = true;
                }
            }
        }
        Intent intent2 = new Intent(intent).setClass(context, AlarmService.class);
        StringBuilder A0k = C12960it.A0k("AlarmBroadcastReceiver dispatching to AlarmService; intent=");
        A0k.append(intent);
        A0k.append("; elapsedRealtime=");
        A0k.append(SystemClock.elapsedRealtime());
        C12960it.A1F(A0k);
        AbstractServiceC003801r.A00(context, intent2, AlarmService.class, 3);
    }
}
