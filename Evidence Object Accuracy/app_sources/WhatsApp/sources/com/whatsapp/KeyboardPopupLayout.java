package com.whatsapp;

import X.AbstractC15280mr;
import X.AbstractC49822Mw;
import X.AbstractC49832My;
import X.C252718t;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class KeyboardPopupLayout extends AbstractC49832My implements AbstractC49822Mw {
    public int A00;
    public int A01;
    public int A02;
    public Paint A03;
    public AbstractC15280mr A04;
    public C252718t A05;
    public boolean A06;
    public boolean A07;
    public final Rect A08;
    public final int[] A09;

    public KeyboardPopupLayout(Context context) {
        super(context);
        this.A06 = false;
        this.A02 = -1;
        this.A01 = -1;
        this.A08 = new Rect();
        this.A09 = new int[2];
    }

    public KeyboardPopupLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A06 = false;
        this.A02 = -1;
        this.A01 = -1;
        this.A08 = new Rect();
        this.A09 = new int[2];
    }

    public KeyboardPopupLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A06 = false;
        this.A02 = -1;
        this.A01 = -1;
        this.A08 = new Rect();
        this.A09 = new int[2];
    }

    public KeyboardPopupLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.A06 = false;
        this.A02 = -1;
        this.A01 = -1;
        this.A08 = new Rect();
        this.A09 = new int[2];
    }

    public AbstractC15280mr getKeyboardPopup() {
        return this.A04;
    }

    private int getSizeWithKeyboard() {
        int i = getResources().getConfiguration().orientation;
        if (i == 1) {
            return this.A02;
        }
        if (i != 2) {
            return -1;
        }
        return this.A01;
    }

    @Override // android.view.View
    public void invalidate() {
        if (!this.A06) {
            super.invalidate();
        }
    }

    @Override // android.view.View
    public void invalidate(int i, int i2, int i3, int i4) {
        if (!this.A06) {
            super.invalidate(i, i2, i3, i4);
        }
    }

    @Override // android.view.View
    public void invalidate(Rect rect) {
        if (!this.A06) {
            super.invalidate(rect);
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03 != null && this.A00 != getHeight()) {
            Rect rect = this.A08;
            rect.set(0, this.A00, getWidth(), getHeight());
            canvas.drawRect(rect, this.A03);
        }
    }

    @Override // android.widget.RelativeLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        if (this.A06) {
            return;
        }
        if (this.A04 != null) {
            super.onLayout(z, i, i2, i3, i2 + this.A00);
            int[] iArr = this.A09;
            getLocationInWindow(iArr);
            boolean z2 = this.A07;
            AbstractC15280mr r3 = this.A04;
            int paddingLeft = iArr[0] + getPaddingLeft();
            if (z2) {
                i5 = 1000000;
            } else {
                i5 = iArr[1] + this.A00;
            }
            r3.update(paddingLeft, i5, (i3 - i) - getPaddingRight(), r3.A01);
            return;
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x009b, code lost:
        if (X.C252718t.A00(r7) != false) goto L_0x009d;
     */
    @Override // android.widget.RelativeLayout, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
        // Method dump skipped, instructions count: 267
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.KeyboardPopupLayout.onMeasure(int, int):void");
    }

    @Override // android.widget.RelativeLayout, android.view.ViewParent, android.view.View
    public void requestLayout() {
        if (!this.A06) {
            super.requestLayout();
        }
    }

    public void setHeightShouldWrap(boolean z) {
        this.A07 = z;
    }

    @Override // X.AbstractC49822Mw
    public void setKeyboardPopup(AbstractC15280mr r2) {
        if (this.A04 != r2) {
            this.A04 = r2;
            requestLayout();
        }
    }

    public void setKeyboardPopupBackgroundColor(int i) {
        if (this.A03 == null) {
            this.A03 = new Paint(1);
        }
        setWillNotDraw(false);
        this.A03.setColor(i);
    }
}
