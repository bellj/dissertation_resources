package com.whatsapp.inappsupport.ui;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10G;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass18U;
import X.AnonymousClass19M;
import X.AnonymousClass19Y;
import X.AnonymousClass1BS;
import X.AnonymousClass1MI;
import X.AnonymousClass1MJ;
import X.AnonymousClass2AC;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3FC;
import X.AnonymousClass43T;
import X.C103134qG;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C17050qB;
import X.C17070qD;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C19990v2;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22650zQ;
import X.C22670zS;
import X.C22710zW;
import X.C249317l;
import X.C252018m;
import X.C252618s;
import X.C252718t;
import X.C252818u;
import X.C254819o;
import X.C35891iw;
import X.C627338j;
import X.C627638m;
import X.DialogInterface$OnClickListenerC96674fp;
import X.DialogInterface$OnClickListenerC97494hA;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.inappsupport.ui.ContactUsActivity;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class ContactUsActivity extends ActivityC13790kL implements AnonymousClass1MI {
    public EditText A00;
    public TextView A01;
    public AppCompatCheckBox A02;
    public AnonymousClass10G A03;
    public AnonymousClass18U A04;
    public AnonymousClass19Y A05;
    public C18790t3 A06;
    public C17050qB A07;
    public AnonymousClass018 A08;
    public C19990v2 A09;
    public C21320xE A0A;
    public C16120oU A0B;
    public AnonymousClass1MJ A0C;
    public AnonymousClass11G A0D;
    public AnonymousClass1BS A0E;
    public C627638m A0F;
    public C35891iw A0G;
    public C22710zW A0H;
    public C17070qD A0I;
    public C252618s A0J;
    public C254819o A0K;
    public AnonymousClass3FC A0L;
    public C252018m A0M;
    public C22650zQ A0N;
    public String A0O;
    public String A0P;
    public String A0Q;
    public boolean A0R;

    public ContactUsActivity() {
        this(0);
    }

    public ContactUsActivity(int i) {
        this.A0R = false;
        A0R(new C103134qG(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0R) {
            this.A0R = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A06 = (C18790t3) r1.AJw.get();
            this.A09 = (C19990v2) r1.A3M.get();
            this.A0B = (C16120oU) r1.ANE.get();
            this.A0N = (C22650zQ) r1.A4n.get();
            this.A04 = (AnonymousClass18U) r1.AAU.get();
            this.A05 = (AnonymousClass19Y) r1.AI6.get();
            this.A0M = (C252018m) r1.A7g.get();
            this.A08 = (AnonymousClass018) r1.ANb.get();
            this.A0I = (C17070qD) r1.AFC.get();
            this.A03 = (AnonymousClass10G) r1.A5K.get();
            this.A07 = (C17050qB) r1.ABL.get();
            this.A0D = (AnonymousClass11G) r1.AKq.get();
            this.A0K = (C254819o) r1.A4D.get();
            this.A0H = (C22710zW) r1.AF7.get();
            this.A0A = (C21320xE) r1.A4Y.get();
            C252618s r0 = (C252618s) r1.A2M.get();
            if (r0 != null) {
                this.A0J = r0;
                this.A0E = (AnonymousClass1BS) r1.A3K.get();
                return;
            }
            throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == 1) {
            finish();
        }
    }

    public final ArrayList A2e(ArrayList arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(null, arrayList);
        Parcel obtain = Parcel.obtain();
        obtain.writeValue(bundle);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        return marshall.length > 450000 ? A2e(new ArrayList(arrayList.subList(0, arrayList.size() >> 1))) : arrayList;
    }

    public void A2f(int i, String str) {
        AnonymousClass43T r1 = new AnonymousClass43T();
        r1.A00 = Integer.valueOf(i);
        r1.A01 = str;
        r1.A02 = this.A08.A06();
        this.A0B.A06(r1);
    }

    @Override // X.AnonymousClass1MI
    public void AV0(boolean z) {
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 11) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            String str = null;
            if (intent != null) {
                if (intent.getIntExtra("com.whatsapp.inappsupport.ui.ContactUsActvity.support_type", 1) == 2) {
                    this.A0G.A01();
                    return;
                }
                str = intent.getStringExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.debug_info");
            }
            this.A0G.A03(str);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!TextUtils.isEmpty(this.A00.getText().toString().trim())) {
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], R.string.support_form_changes_will_be_lost_confirmation);
            A01.A02(DialogInterface$OnClickListenerC97494hA.A00, R.string.back_to_request);
            DialogInterface$OnClickListenerC96674fp r1 = new DialogInterface.OnClickListener() { // from class: X.4fp
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ContactUsActivity contactUsActivity = ContactUsActivity.this;
                    dialogInterface.dismiss();
                    contactUsActivity.finish();
                }
            };
            A01.A04 = R.string.cancel;
            A01.A07 = r1;
            A01.A01().A1F(A0V(), null);
        } else {
            super.onBackPressed();
        }
        ContactUsActivity contactUsActivity = this.A0G.A02;
        AnonymousClass009.A05(contactUsActivity);
        contactUsActivity.A2f(1, null);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A0L.A00();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x02dd  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r22) {
        /*
        // Method dump skipped, instructions count: 748
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.inappsupport.ui.ContactUsActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        ContactUsActivity contactUsActivity = this.A0G.A02;
        AnonymousClass009.A05(contactUsActivity);
        if ("biz-directory-browsing".equals(contactUsActivity.getIntent().getStringExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.from"))) {
            return true;
        }
        menu.add(0, R.id.menuitem_contact_us_email, 0, getString(R.string.contact_us_email)).setShowAsAction(0);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0G.A02 = null;
        C627338j r1 = this.A0K.A00;
        if (r1 != null) {
            r1.A03(false);
        }
        C627638m r12 = this.A0F;
        if (r12 != null) {
            r12.A03(false);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            C35891iw r2 = this.A0G;
            ContactUsActivity contactUsActivity = r2.A02;
            AnonymousClass009.A05(contactUsActivity);
            contactUsActivity.A2f(1, null);
            r2.A02.finish();
            return true;
        } else if (itemId != R.id.menuitem_contact_us_email) {
            return false;
        } else {
            this.A0G.A02(2);
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A00.clearFocus();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        C35891iw r2 = this.A0G;
        r2.A03 = null;
        r2.A09.A04(r2.A08);
        super.onStop();
    }
}
