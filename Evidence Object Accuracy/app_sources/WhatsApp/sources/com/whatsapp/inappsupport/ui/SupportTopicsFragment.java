package com.whatsapp.inappsupport.ui;

import X.AnonymousClass009;
import X.AnonymousClass12P;
import X.AnonymousClass3M6;
import X.C12960it;
import X.C52762bd;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class SupportTopicsFragment extends Hilt_SupportTopicsFragment {
    public AnonymousClass3M6 A00;
    public SupportTopicsActivity A01;
    public ArrayList A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        int i = 0;
        View inflate = layoutInflater.inflate(R.layout.support_topics_fragment, viewGroup, false);
        View findViewById = inflate.findViewById(R.id.topic_list_header);
        if (this.A01.A01 != 2) {
            i = 8;
        }
        findViewById.setVisibility(i);
        ((AbsListView) inflate.findViewById(R.id.topic_list)).setAdapter((ListAdapter) new C52762bd(this.A01, this.A02));
        return inflate;
    }

    @Override // com.whatsapp.inappsupport.ui.Hilt_SupportTopicsFragment, X.AnonymousClass01E
    public void A15(Context context) {
        if (AnonymousClass12P.A00(context) instanceof SupportTopicsActivity) {
            this.A01 = (SupportTopicsActivity) AnonymousClass12P.A00(context);
            super.A15(context);
            return;
        }
        StringBuilder A0k = C12960it.A0k("SupportTopicsFragment");
        A0k.append(" can only be used with ");
        throw C12960it.A0U(C12960it.A0d("SupportTopicsActivity", A0k));
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A00 = (AnonymousClass3M6) A03().getParcelable("parent_topic");
        ArrayList parcelableArrayList = A03().getParcelableArrayList("topics");
        AnonymousClass009.A05(parcelableArrayList);
        this.A02 = parcelableArrayList;
    }
}
