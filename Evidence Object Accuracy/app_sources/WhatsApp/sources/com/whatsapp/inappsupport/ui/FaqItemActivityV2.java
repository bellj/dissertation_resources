package com.whatsapp.inappsupport.ui;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass2FL;
import X.AnonymousClass3FC;
import X.C12960it;
import X.C12970iu;
import X.C74073hG;
import X.ViewTreeObserver$OnPreDrawListenerC66443Np;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class FaqItemActivityV2 extends ActivityC13790kL {
    public AnonymousClass3FC A00;
    public String A01;
    public boolean A02;

    public FaqItemActivityV2() {
        this(0);
    }

    public FaqItemActivityV2(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 78);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass3FC r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String string = getString(R.string.settings_help);
        setTitle(string);
        setContentView(R.layout.faq_item_v2);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        ActivityC13790kL.A0c(this, toolbar, ((ActivityC13830kP) this).A01);
        toolbar.setTitle(string);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 26));
        A1e(toolbar);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("com.whatsapp.inappsupport.ui.FaqItemActivityV2.html_content");
        String stringExtra2 = intent.getStringExtra("com.whatsapp.inappsupport.ui.FaqItemActivityV2.url");
        WebView webView = (WebView) findViewById(R.id.faq_item_web_view);
        webView.loadDataWithBaseURL(stringExtra2, stringExtra, "text/html", AnonymousClass01V.A08, null);
        View findViewById = findViewById(R.id.not_helpful_button_container);
        findViewById.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66443Np(findViewById, this));
        this.A00 = new AnonymousClass3FC(webView, findViewById, getResources().getDimensionPixelSize(R.dimen.settings_bottom_button_elevation));
        webView.setWebViewClient(new C74073hG(this));
        C12960it.A0x(this.A00.A01, this, 25);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (TextUtils.isEmpty(getIntent().getStringExtra("com.whatsapp.inappsupport.ui.FaqItemActivityV2.url"))) {
            return super.onCreateOptionsMenu(menu);
        }
        this.A01 = getIntent().getStringExtra("com.whatsapp.inappsupport.ui.FaqItemActivityV2.url");
        menu.add(0, R.id.menuitem_open_in_browser, 0, getString(R.string.faq_open_in_browser)).setShowAsAction(0);
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
            return true;
        } else if (itemId != R.id.menuitem_open_in_browser) {
            return false;
        } else {
            startActivity(C12970iu.A0B(Uri.parse(this.A01)));
            return true;
        }
    }
}
