package com.whatsapp.inappsupport.ui;

import X.AbstractActivityC58492pZ;
import X.AbstractC005102i;
import X.AbstractC11780gr;
import X.ActivityC13790kL;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01F;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.AnonymousClass3M6;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C87934Dp;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class SupportTopicsActivity extends AbstractActivityC58492pZ implements AbstractC11780gr {
    public int A00;
    public int A01;
    public MenuItem A02;
    public AnonymousClass19Y A03;
    public AnonymousClass11G A04;
    public List A05;

    public static Intent A02(Context context, Bundle bundle, ArrayList arrayList) {
        Intent A0D = C12990iw.A0D(context, SupportTopicsActivity.class);
        A0D.putParcelableArrayListExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.support_topics", arrayList);
        A0D.putExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.ui_version", 1);
        A0D.putExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.contact_us_action", 3);
        if (bundle != null) {
            A0D.putExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.describe_problem_bundle", bundle);
        }
        return A0D;
    }

    public void A2e(AnonymousClass3M6 r14) {
        int i = this.A00;
        if (i == 1 || i == 2) {
            setResult(-1, C87934Dp.A00(getIntent()));
            finish();
            return;
        }
        ArrayList A0y = C12980iv.A0y(this.A05);
        ArrayList A0y2 = C12980iv.A0y(this.A05);
        for (int i2 = 0; i2 < this.A05.size(); i2++) {
            if (((SupportTopicsFragment) this.A05.get(i2)).A00 != null) {
                AnonymousClass3M6 r1 = ((SupportTopicsFragment) this.A05.get(i2)).A00;
                A0y.add(r1.A03);
                A0y2.add(r1.A02);
            }
        }
        if (r14 != null) {
            A0y.add(r14.A03);
            A0y2.add(r14.A02);
        }
        String string = getIntent().getBundleExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.describe_problem_bundle").getString("com.whatsapp.support.DescribeProblemActivity.from");
        AnonymousClass19Y r3 = this.A03;
        boolean A00 = this.A04.A00();
        if (string == null) {
            string = "support_topics";
        }
        startActivity(r3.A00(this, getIntent().getBundleExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.describe_problem_bundle"), null, null, string, null, A0y2, A0y, A00));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 15 && i2 == -1) {
            setResult(-1, C87934Dp.A00(getIntent()));
            finish();
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        boolean z;
        if (!this.A05.isEmpty()) {
            List list = this.A05;
            list.remove(C12980iv.A0C(list));
            if (!this.A05.isEmpty()) {
                List list2 = this.A05;
                SupportTopicsFragment supportTopicsFragment = (SupportTopicsFragment) list2.get(C12980iv.A0C(list2));
                MenuItem menuItem = this.A02;
                if (menuItem != null) {
                    AnonymousClass3M6 r0 = supportTopicsFragment.A00;
                    if (r0 != null) {
                        z = r0.A06;
                    } else {
                        z = false;
                    }
                    menuItem.setVisible(z);
                }
            }
        }
        super.onBackPressed();
    }

    @Override // X.AbstractC11780gr
    public void onBackStackChanged() {
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            int A03 = A0V().A03();
            int i = R.string.payment_support_topic_flow_secondary_title;
            if (A03 == 0) {
                i = R.string.payment_support_topic_flow_primary_title;
            }
            A1U.A0I(getString(i));
            A1U.A0M(true);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Intent intent = getIntent();
        this.A01 = intent.getIntExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.ui_version", 1);
        this.A00 = intent.getIntExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.contact_us_action", 3);
        if (this.A01 == 2) {
            String string = getString(R.string.settings_help);
            setTheme(2131952370);
            super.onCreate(bundle);
            setTitle(string);
            setContentView(R.layout.support_topics_activity);
            findViewById(R.id.toolbar_layout).setVisibility(0);
            Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
            ActivityC13790kL.A0c(this, toolbar, ((ActivityC13830kP) this).A01);
            toolbar.setTitle(string);
            toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 28));
            A1e(toolbar);
            View findViewById = findViewById(R.id.contact_us_button);
            findViewById.setVisibility(0);
            C12960it.A0x(findViewById, this, 27);
        } else {
            super.onCreate(bundle);
            setContentView(R.layout.support_topics_activity);
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0I(getString(R.string.payment_support_topic_flow_primary_title));
                A1U.A0M(true);
            }
        }
        this.A05 = C12960it.A0l();
        AnonymousClass01F A0V = A0V();
        ArrayList arrayList = A0V.A0F;
        if (arrayList == null) {
            arrayList = C12960it.A0l();
            A0V.A0F = arrayList;
        }
        arrayList.add(this);
        ArrayList<? extends Parcelable> parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.support_topics");
        SupportTopicsFragment supportTopicsFragment = new SupportTopicsFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("parent_topic", null);
        A0D.putParcelableArrayList("topics", parcelableArrayListExtra);
        supportTopicsFragment.A0U(A0D);
        C004902f A0P = C12970iu.A0P(this);
        A0P.A06(supportTopicsFragment, R.id.support_topics_container);
        A0P.A01();
        this.A05.add(supportTopicsFragment);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.A01 != 1) {
            return super.onCreateOptionsMenu(menu);
        }
        getMenuInflater().inflate(R.menu.support_topics_menu, menu);
        MenuItem findItem = menu.findItem(R.id.support_topic_skip);
        this.A02 = findItem;
        findItem.setVisible(false);
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332 && !this.A05.isEmpty()) {
            onBackPressed();
            return true;
        } else if (menuItem.getItemId() != R.id.support_topic_skip) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            A2e(null);
            return true;
        }
    }
}
