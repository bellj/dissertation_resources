package com.whatsapp.community;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass11F;
import X.AnonymousClass12F;
import X.AnonymousClass15K;
import X.AnonymousClass1OU;
import X.AnonymousClass2FL;
import X.AnonymousClass3ZW;
import X.AnonymousClass3ZY;
import X.AnonymousClass4KH;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15550nR;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C17220qS;
import X.C18640sm;
import X.C19990v2;
import X.C20710wC;
import X.C21270x9;
import X.C22050yP;
import X.C22640zP;
import X.C37271lv;
import X.C48992Is;
import X.C54272gS;
import X.C63783Cx;
import X.C63793Cy;
import android.content.Intent;
import android.content.res.Resources;
import android.os.SystemClock;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class ManageGroupsInCommunityActivity extends ActivityC13790kL {
    public Spinner A00;
    public AbstractC005102i A01;
    public RecyclerView A02;
    public C48992Is A03;
    public C22640zP A04;
    public C54272gS A05;
    public C37271lv A06;
    public C15550nR A07;
    public C15610nY A08;
    public C21270x9 A09;
    public C19990v2 A0A;
    public C15600nX A0B;
    public AnonymousClass15K A0C;
    public AnonymousClass11F A0D;
    public C22050yP A0E;
    public C20710wC A0F;
    public C15580nU A0G;
    public C17220qS A0H;
    public AnonymousClass12F A0I;
    public boolean A0J;
    public final AnonymousClass4KH A0K;

    public ManageGroupsInCommunityActivity() {
        this(0);
        this.A0K = new AnonymousClass4KH(this);
    }

    public ManageGroupsInCommunityActivity(int i) {
        this.A0J = false;
        ActivityC13830kP.A1P(this, 45);
    }

    public static /* synthetic */ boolean A03(ManageGroupsInCommunityActivity manageGroupsInCommunityActivity) {
        if (C12960it.A05(manageGroupsInCommunityActivity.A06.A0R.A01()) < manageGroupsInCommunityActivity.A04.A0G.A02(1238) + 1) {
            return false;
        }
        int A02 = manageGroupsInCommunityActivity.A04.A0G.A02(1238);
        Resources resources = manageGroupsInCommunityActivity.getResources();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1P(A1b, A02, 0);
        Toast.makeText(manageGroupsInCommunityActivity, resources.getQuantityString(R.plurals.reached_max_allowed_groups, A02, A1b), 0).show();
        return true;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0J) {
            this.A0J = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0A = C12980iv.A0c(A1M);
            this.A09 = C12970iu.A0W(A1M);
            this.A0H = C12990iw.A0c(A1M);
            this.A0C = (AnonymousClass15K) A1M.AKk.get();
            this.A07 = C12960it.A0O(A1M);
            this.A08 = C12960it.A0P(A1M);
            this.A0F = C12980iv.A0e(A1M);
            this.A0I = C12990iw.A0f(A1M);
            this.A0E = (C22050yP) A1M.A7v.get();
            this.A0D = (AnonymousClass11F) A1M.AE3.get();
            this.A04 = (C22640zP) A1M.A3Z.get();
            this.A0B = C12980iv.A0d(A1M);
            this.A03 = (C48992Is) A1L.A0a.get();
        }
    }

    public final void A2e(AnonymousClass1OU r7) {
        GroupJid groupJid = r7.A02;
        AnonymousClass009.A05(groupJid);
        if (!((ActivityC13810kN) this).A07.A0B()) {
            ((ActivityC13810kN) this).A05.A04(C18640sm.A01(getApplicationContext()));
            return;
        }
        A2C(R.string.community_remove_group_progress_dialog_title);
        new C63793Cy(((ActivityC13810kN) this).A03, this.A0G, this.A0H, new AnonymousClass3ZY(this, r7)).A00(Collections.singletonList(groupJid));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 10) {
            super.onActivityResult(i, i2, intent);
            return;
        }
        if (i2 == -1) {
            if (intent != null && intent.getExtras() != null) {
                ArrayList<String> stringArrayList = intent.getExtras().getStringArrayList("selected_jids");
                if (stringArrayList != null && !stringArrayList.isEmpty()) {
                    if (!((ActivityC13810kN) this).A07.A0B()) {
                        ((ActivityC13810kN) this).A05.A04(C18640sm.A01(getApplicationContext()));
                        return;
                    }
                    long uptimeMillis = SystemClock.uptimeMillis();
                    ArrayList A0l = C12960it.A0l();
                    Iterator<String> it = stringArrayList.iterator();
                    while (it.hasNext()) {
                        GroupJid nullable = GroupJid.getNullable(C12970iu.A0x(it));
                        if (nullable != null) {
                            A0l.add(nullable);
                        }
                    }
                    Ady(R.string.participant_adding, R.string.register_wait_message);
                    new C63783Cx(((ActivityC13810kN) this).A03, this.A0G, this.A0H, new AnonymousClass3ZW(this, uptimeMillis)).A00(A0l);
                    return;
                }
            } else {
                return;
            }
        } else if (i2 != -10) {
            return;
        }
        ((ActivityC13810kN) this).A05.A04(R.string.no_groups_to_link_error);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x00ad, code lost:
        if (r15.A0B.A0D(r15.A0G) == false) goto L_0x00af;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
            r15 = this;
            r4 = r15
            r0 = r16
            super.onCreate(r0)
            android.content.Intent r1 = r15.getIntent()
            java.lang.String r0 = "parent_group_jid"
            X.0nU r0 = X.ActivityC13790kL.A0U(r1, r0)
            r15.A0G = r0
            r0 = 2131558489(0x7f0d0059, float:1.8742295E38)
            r15.setContentView(r0)
            r0 = 2131362739(0x7f0a03b3, float:1.8345267E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r15, r0)
            r2 = 8
            r0.setVisibility(r2)
            X.02i r0 = X.C12970iu.A0N(r15)
            r15.A01 = r0
            r1 = 1
            r0.A0P(r1)
            X.02i r0 = r15.A01
            r0.A0M(r1)
            X.02i r1 = r15.A01
            r0 = 2131889219(0x7f120c43, float:1.9413095E38)
            r1.A0A(r0)
            r0 = 2131361950(0x7f0a009e, float:1.8343667E38)
            android.view.View r1 = r15.findViewById(r0)
            r0 = 43
            X.AbstractView$OnClickListenerC34281fs.A00(r1, r15, r0)
            r0 = 2131361949(0x7f0a009d, float:1.8343665E38)
            android.view.View r1 = r15.findViewById(r0)
            r0 = 44
            X.AbstractView$OnClickListenerC34281fs.A00(r1, r15, r0)
            X.0x9 r1 = r15.A09
            java.lang.String r0 = "add-groups-to-community"
            X.1J1 r9 = r1.A04(r15, r0)
            X.2Is r3 = r15.A03
            X.0nU r1 = r15.A0G
            X.3RZ r0 = new X.3RZ
            r0.<init>(r3, r1)
            X.02A r1 = new X.02A
            r1.<init>(r0, r15)
            java.lang.Class<X.1lv> r0 = X.C37271lv.class
            X.015 r0 = r1.A00(r0)
            X.1lv r0 = (X.C37271lv) r0
            r15.A06 = r0
            r0 = 2131361974(0x7f0a00b6, float:1.8343715E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r15, r0)
            androidx.recyclerview.widget.RecyclerView r0 = (androidx.recyclerview.widget.RecyclerView) r0
            r15.A02 = r0
            r0 = 2131361951(0x7f0a009f, float:1.8343669E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r15, r0)
            android.widget.Spinner r0 = (android.widget.Spinner) r0
            r15.A00 = r0
            androidx.recyclerview.widget.RecyclerView r0 = r15.A02
            X.C12990iw.A1K(r0)
            X.0nT r5 = r15.A01
            X.0v2 r10 = r15.A0A
            X.0nR r7 = r15.A07
            X.0nY r8 = r15.A08
            X.12F r13 = r15.A0I
            X.11F r12 = r15.A0D
            X.0nX r11 = r15.A0B
            X.0zP r0 = r15.A04
            boolean r0 = r0.A06()
            if (r0 == 0) goto L_0x00af
            X.0nX r1 = r15.A0B
            X.0nU r0 = r15.A0G
            boolean r0 = r1.A0D(r0)
            r14 = 1
            if (r0 != 0) goto L_0x00b0
        L_0x00af:
            r14 = 0
        L_0x00b0:
            X.4KH r6 = r15.A0K
            X.2gS r3 = new X.2gS
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r15.A05 = r3
            androidx.recyclerview.widget.RecyclerView r0 = r15.A02
            r0.setAdapter(r3)
            android.widget.Spinner r1 = r15.A00
            r0 = 0
            r1.setVisibility(r0)
            androidx.recyclerview.widget.RecyclerView r0 = r15.A02
            r0.setVisibility(r2)
            X.1lv r0 = r15.A06
            X.1jQ r1 = r0.A0S
            r0 = 58
            X.C12960it.A18(r15, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.ManageGroupsInCommunityActivity.onCreate(android.os.Bundle):void");
    }
}
