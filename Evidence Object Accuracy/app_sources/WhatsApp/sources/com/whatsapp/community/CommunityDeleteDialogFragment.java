package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AnonymousClass009;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15580nU;
import X.C15610nY;
import X.C16170oZ;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CommunityDeleteDialogFragment extends Hilt_CommunityDeleteDialogFragment {
    public C15450nH A00;
    public C16170oZ A01;
    public C15550nR A02;
    public C15610nY A03;
    public C14830m7 A04;
    public AbstractC14440lR A05;

    public static CommunityDeleteDialogFragment A00(C15580nU r3) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r3.getRawString());
        CommunityDeleteDialogFragment communityDeleteDialogFragment = new CommunityDeleteDialogFragment();
        communityDeleteDialogFragment.A0U(A0D);
        return communityDeleteDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String A0q;
        AbstractC14640lm A01 = AbstractC14640lm.A01(A03().getString("jid"));
        AnonymousClass009.A05(A01);
        C15370n3 A0B = this.A02.A0B(A01);
        String A04 = this.A03.A04(A0B);
        C004802e A0S = C12980iv.A0S(A0C());
        if (this.A00.A05(AbstractC15460nI.A0f)) {
            A0q = A0I(R.string.delete_community_dialog_message_md_enabled);
        } else {
            A0q = C12970iu.A0q(this, A04, C12970iu.A1b(), 0, R.string.delete_community_dialog_message_exit);
        }
        A0S.A07(R.string.delete_community_dialog_title);
        A0S.A0A(A0q);
        return C12960it.A0L(new IDxCListenerShape3S0200000_1_I1(A0B, 3, this), A0S, R.string.delete_community);
    }
}
