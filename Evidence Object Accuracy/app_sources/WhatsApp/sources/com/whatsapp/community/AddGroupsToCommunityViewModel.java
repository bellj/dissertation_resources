package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AnonymousClass015;
import X.AnonymousClass10V;
import X.AnonymousClass10Z;
import X.C12970iu;
import X.C15370n3;
import X.C20710wC;
import X.C20830wO;
import X.C36161jQ;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
public class AddGroupsToCommunityViewModel extends AnonymousClass015 {
    public C15370n3 A00;
    public Set A01 = C12970iu.A12();
    public final AbstractC15710nm A02;
    public final AnonymousClass10V A03;
    public final C20830wO A04;
    public final C20710wC A05;
    public final AnonymousClass10Z A06;
    public final C36161jQ A07 = new C36161jQ(C12970iu.A12());
    public final C36161jQ A08 = new C36161jQ(C12970iu.A12());
    public final C36161jQ A09 = new C36161jQ(C12970iu.A12());
    public final AbstractC14440lR A0A;
    public final Set A0B = C12970iu.A12();

    public AddGroupsToCommunityViewModel(AbstractC15710nm r3, AnonymousClass10V r4, C20830wO r5, C20710wC r6, AnonymousClass10Z r7, AbstractC14440lR r8) {
        this.A02 = r3;
        this.A0A = r8;
        this.A05 = r6;
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
    }

    public final void A04() {
        HashSet A12 = C12970iu.A12();
        C15370n3 r0 = this.A00;
        if (r0 != null) {
            A12.add(r0);
        }
        A12.addAll(this.A01);
        A12.addAll(this.A0B);
        this.A07.A0A(Collections.unmodifiableSet(A12));
    }
}
