package com.whatsapp.community;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass10Y;
import X.AnonymousClass11F;
import X.AnonymousClass11L;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass1BF;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15860o1;
import X.C16590pI;
import X.C17070qD;
import X.C19990v2;
import X.C20040v7;
import X.C20710wC;
import X.C21190x1;
import X.C21270x9;
import X.C21320xE;
import X.C21400xM;
import X.C22050yP;
import X.C22330yu;
import X.C22640zP;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C244215l;
import X.C253519b;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_CommunityFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A00() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A00();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A00()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.Hilt_CommunityFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A00();
        A18();
    }

    public void A18() {
        if (!this.A02) {
            this.A02 = true;
            CommunityFragment communityFragment = (CommunityFragment) this;
            AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            ((WaFragment) communityFragment).A00 = (AnonymousClass182) r2.A94.get();
            ((WaFragment) communityFragment).A01 = (AnonymousClass180) r2.ALt.get();
            communityFragment.A0J = (C14830m7) r2.ALb.get();
            communityFragment.A0W = (C14850m9) r2.A04.get();
            communityFragment.A02 = (C14900mE) r2.A8X.get();
            communityFragment.A0b = (AnonymousClass13H) r2.ABY.get();
            communityFragment.A03 = (C15570nT) r2.AAr.get();
            communityFragment.A0K = (C16590pI) r2.AMg.get();
            communityFragment.A0j = (AbstractC14440lR) r2.ANe.get();
            communityFragment.A0N = (C19990v2) r2.A3M.get();
            communityFragment.A04 = (C15450nH) r2.AII.get();
            communityFragment.A00 = (AnonymousClass12P) r2.A0H.get();
            communityFragment.A0H = (C21270x9) r2.A4A.get();
            communityFragment.A0S = (C20040v7) r2.AAK.get();
            communityFragment.A0e = (AnonymousClass14X) r2.AFM.get();
            communityFragment.A0D = (AnonymousClass130) r2.A41.get();
            communityFragment.A01 = (C253519b) r2.A4C.get();
            communityFragment.A0E = (C15550nR) r2.A45.get();
            communityFragment.A0P = (C241714m) r2.A4l.get();
            communityFragment.A0G = (C15610nY) r2.AMe.get();
            communityFragment.A0M = (AnonymousClass018) r2.ANb.get();
            communityFragment.A0f = (C21190x1) r2.A3G.get();
            communityFragment.A0d = (C17070qD) r2.AFC.get();
            communityFragment.A08 = (C238013b) r2.A1Z.get();
            communityFragment.A0F = (AnonymousClass10S) r2.A46.get();
            communityFragment.A0Y = (C20710wC) r2.A8m.get();
            communityFragment.A0T = (AnonymousClass10Y) r2.AAR.get();
            communityFragment.A0i = (AnonymousClass12F) r2.AJM.get();
            communityFragment.A0g = (C15860o1) r2.A3H.get();
            communityFragment.A0X = (C22050yP) r2.A7v.get();
            communityFragment.A0h = (C240514a) r2.AJL.get();
            communityFragment.A09 = (C22330yu) r2.A3I.get();
            communityFragment.A0V = (AnonymousClass11F) r2.AE3.get();
            communityFragment.A0U = (C21400xM) r2.ABd.get();
            communityFragment.A0L = (C14820m6) r2.AN3.get();
            communityFragment.A0A = (C22640zP) r2.A3Z.get();
            communityFragment.A0C = (AnonymousClass1BF) r2.A3b.get();
            communityFragment.A0R = (C236812p) r2.AAC.get();
            communityFragment.A0O = (C21320xE) r2.A4Y.get();
            communityFragment.A0c = (C22710zW) r2.AF7.get();
            communityFragment.A06 = (C14650lo) r2.A2V.get();
            communityFragment.A07 = (AnonymousClass10A) r2.A2W.get();
            communityFragment.A0Q = (C15600nX) r2.A8x.get();
            communityFragment.A05 = (AnonymousClass11L) r2.ALG.get();
            communityFragment.A0Z = (C244215l) r2.A8y.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
