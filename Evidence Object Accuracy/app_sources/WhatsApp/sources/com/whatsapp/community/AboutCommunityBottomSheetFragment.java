package com.whatsapp.community;

import X.AbstractC28491Nn;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass1MW;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C15580nU;
import X.C22640zP;
import X.C252018m;
import X.C252818u;
import X.C253118x;
import X.C27531Hw;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape1S0000000_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.GroupJid;

/* loaded from: classes2.dex */
public class AboutCommunityBottomSheetFragment extends Hilt_AboutCommunityBottomSheetFragment {
    public C252818u A00;
    public C22640zP A01;
    public AnonymousClass01d A02;
    public C14820m6 A03;
    public C15580nU A04;
    public C252018m A05;
    public C253118x A06;

    public static AboutCommunityBottomSheetFragment A00(GroupJid groupJid) {
        AboutCommunityBottomSheetFragment aboutCommunityBottomSheetFragment = new AboutCommunityBottomSheetFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("EXTRA_PARENT_GROUP_JID", groupJid.getRawString());
        aboutCommunityBottomSheetFragment.A0U(A0D);
        return aboutCommunityBottomSheetFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.about_community_bottom_sheet, viewGroup, true);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        try {
            this.A04 = C15580nU.A03(A03().getString("EXTRA_PARENT_GROUP_JID"));
        } catch (AnonymousClass1MW e) {
            throw new RuntimeException(e);
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C12960it.A0t(C12960it.A08(this.A03), "about_community_nux", true);
        C27531Hw.A06(C12960it.A0I(view, R.id.about_community_title));
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.about_community_description);
        String[] strArr = {C252018m.A00(this.A05, "570221114584995")};
        AbstractC28491Nn.A05(A0T, this.A02, this.A06.A01(A01(), C12970iu.A0q(this, "learn-more", new Object[1], 0, R.string.about_community_description), new Runnable[]{new RunnableBRunnable0Shape1S0000000_I1()}, new String[]{"learn-more"}, strArr));
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass028.A0D(view, R.id.about_community_join_button), this, 41);
    }
}
