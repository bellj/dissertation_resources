package com.whatsapp.community;

import X.AnonymousClass4KE;
import X.C12960it;
import X.C12970iu;
import X.C15570nT;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.base.WaDialogFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class CommunityAdminDialogFragment extends WaDialogFragment {
    public int A00;
    public AnonymousClass4KE A01;
    public UserJid A02;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        if (A03.containsKey("dialog_id")) {
            this.A00 = A03.getInt("dialog_id");
            UserJid nullable = UserJid.getNullable(A03.getString("user_jid"));
            this.A02 = nullable;
            if (nullable != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(A01());
                if (A03.containsKey("title")) {
                    builder.setTitle(A03.getString("title"));
                }
                if (A03.containsKey("message")) {
                    builder.setMessage(A03.getCharSequence("message"));
                }
                if (A03.containsKey("positive_button")) {
                    builder.setPositiveButton(A03.getString("positive_button"), new IDxCListenerShape8S0100000_1_I1(this, 13));
                }
                if (A03.containsKey("negative_button")) {
                    builder.setNegativeButton(A03.getString("negative_button"), new IDxCListenerShape8S0100000_1_I1(this, 12));
                }
                return builder.create();
            }
            throw C12970iu.A0f("CommunityAdminDialogFragment/user jid must be passed in");
        }
        throw C12960it.A0U("dialog_id should be provided.");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        C15570nT.A00(this);
    }
}
