package com.whatsapp.community;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass1BF;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C22050yP;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CommunityNUXActivity extends ActivityC13790kL {
    public AnonymousClass1BF A00;
    public C22050yP A01;
    public boolean A02;

    public CommunityNUXActivity() {
        this(0);
    }

    public CommunityNUXActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 43);
    }

    public static /* synthetic */ void A02(CommunityNUXActivity communityNUXActivity) {
        String A00 = communityNUXActivity.A00.A00();
        C22050yP r2 = communityNUXActivity.A01;
        Integer A0h = C12970iu.A0h();
        r2.A0B(A0h, A0h, null, A00);
        super.onBackPressed();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (C22050yP) A1M.A7v.get();
            this.A00 = (AnonymousClass1BF) A1M.A3b.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        this.A01.A0B(8, C12970iu.A0h(), null, this.A00.A00());
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_community_nux);
        C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "community_nux", true);
        C12960it.A0y(AnonymousClass00T.A05(this, R.id.community_nux_next_button), this, 12);
        C12960it.A0y(AnonymousClass00T.A05(this, R.id.community_nux_close), this, 13);
    }
}
