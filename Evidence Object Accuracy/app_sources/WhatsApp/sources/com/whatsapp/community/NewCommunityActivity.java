package com.whatsapp.community;

import X.AbstractActivityC37221lk;
import X.AbstractC15460nI;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass10T;
import X.AnonymousClass10Z;
import X.AnonymousClass11F;
import X.AnonymousClass131;
import X.AnonymousClass19M;
import X.AnonymousClass1BF;
import X.AnonymousClass1VR;
import X.AnonymousClass2FL;
import X.AnonymousClass2ZZ;
import X.AnonymousClass367;
import X.AnonymousClass51K;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15370n3;
import X.C20710wC;
import X.C22050yP;
import X.C51842Za;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.util.Log;
import java.io.File;

/* loaded from: classes2.dex */
public class NewCommunityActivity extends AbstractActivityC37221lk {
    public C15370n3 A00;
    public C20710wC A01;
    public boolean A02;

    public NewCommunityActivity() {
        this(0);
    }

    public NewCommunityActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 46);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AbstractActivityC37221lk) this).A07 = (C22050yP) A1M.A7v.get();
            ((AbstractActivityC37221lk) this).A04 = (AnonymousClass10T) A1M.A47.get();
            ((AbstractActivityC37221lk) this).A06 = (AnonymousClass11F) A1M.AE3.get();
            ((AbstractActivityC37221lk) this).A09 = (AnonymousClass10Z) A1M.AGM.get();
            ((AbstractActivityC37221lk) this).A03 = (AnonymousClass1BF) A1M.A3b.get();
            ((AbstractActivityC37221lk) this).A08 = C12990iw.A0e(A1M);
            ((AbstractActivityC37221lk) this).A05 = (AnonymousClass131) A1M.A49.get();
            this.A01 = C12980iv.A0e(A1M);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Drawable r0;
        if (i != 64206) {
            if (i == 16436755) {
                ((AbstractActivityC37221lk) this).A09.A02(this.A00).delete();
                if (i2 != -1) {
                    if (i2 == 0 && intent != null) {
                        ((AbstractActivityC37221lk) this).A09.A03(intent, this);
                        return;
                    }
                    return;
                }
            } else if (i == 16436756 && i2 == -1) {
                finish();
                return;
            } else {
                super.onActivityResult(i, i2, intent);
                return;
            }
        } else if (i2 == -1) {
            if (intent != null) {
                if (intent.getBooleanExtra("is_reset", false)) {
                    Log.i("newcommunity/resetphoto");
                    File A00 = ((AbstractActivityC37221lk) this).A04.A00(this.A00);
                    AnonymousClass009.A05(A00);
                    A00.delete();
                    File A01 = ((AbstractActivityC37221lk) this).A04.A01(this.A00);
                    AnonymousClass009.A05(A01);
                    A01.delete();
                    ((AbstractActivityC37221lk) this).A00.setImageDrawable(((AbstractActivityC37221lk) this).A06.A00(getTheme(), getResources(), AnonymousClass51K.A00, R.drawable.avatar_parent_large));
                    return;
                } else if (intent.getBooleanExtra("skip_cropping", false)) {
                    ((AbstractActivityC37221lk) this).A09.A02(this.A00).delete();
                }
            }
            Log.i("newcommunity/cropphoto");
            ((AbstractActivityC37221lk) this).A09.A05(intent, this, this, this.A00, 16436755);
            return;
        } else {
            return;
        }
        Log.i("newcommunity/photopicked");
        Bitmap A002 = ((AbstractActivityC37221lk) this).A05.A00(this, this.A00, 0.0f, getResources().getDimensionPixelSize(R.dimen.registration_profile_photo_size));
        if (A002 != null) {
            ImageView imageView = ((AbstractActivityC37221lk) this).A00;
            AnonymousClass11F r02 = ((AbstractActivityC37221lk) this).A06;
            Resources resources = getResources();
            AnonymousClass51K r2 = AnonymousClass51K.A00;
            if (r02.A00.A07(1257)) {
                r0 = new AnonymousClass2ZZ(resources, A002, r2);
            } else {
                r0 = new C51842Za(resources, A002, r2);
            }
            imageView.setImageDrawable(r0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003b, code lost:
        r4 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        r3.A0B(8, java.lang.Integer.valueOf(r4), null, r5);
        super.onBackPressed();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0048, code lost:
        if (r0 > 0) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0013, code lost:
        if (r0 > 0) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        if (r6 > 0) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        ((X.AbstractActivityC37221lk) r7).A07.A0B(X.C12990iw.A0j(), 5, X.C12980iv.A0l(r6), r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        r3 = ((X.AbstractActivityC37221lk) r7).A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0039, code lost:
        if (((X.AbstractActivityC37221lk) r7).A03.A03 != false) goto L_0x003c;
     */
    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onBackPressed() {
        /*
            r7 = this;
            X.1BF r0 = r7.A03
            java.lang.String r5 = r0.A00()
            X.1BF r0 = r7.A03
            int r6 = r0.A00
            int r0 = r0.A01
            r4 = 5
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            if (r6 > 0) goto L_0x0048
            if (r0 <= 0) goto L_0x002d
        L_0x0015:
            X.0yP r1 = r7.A07
            java.lang.Long r0 = X.C12980iv.A0l(r0)
            r1.A0B(r3, r3, r0, r5)
            if (r6 <= 0) goto L_0x002d
        L_0x0020:
            X.0yP r2 = r7.A07
            java.lang.Integer r1 = X.C12990iw.A0j()
            java.lang.Long r0 = X.C12980iv.A0l(r6)
            r2.A0B(r1, r3, r0, r5)
        L_0x002d:
            X.0yP r3 = r7.A07
            r0 = 8
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            X.1BF r0 = r7.A03
            boolean r0 = r0.A03
            if (r0 != 0) goto L_0x003c
            r4 = 4
        L_0x003c:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r4)
            r0 = 0
            r3.A0B(r2, r1, r0, r5)
            super.onBackPressed()
            return
        L_0x0048:
            if (r0 <= 0) goto L_0x0020
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.NewCommunityActivity.onBackPressed():void");
    }

    @Override // X.AbstractActivityC37221lk, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00 = new AnonymousClass1VR(this.A01.A07().getRawString());
        ((AbstractActivityC37221lk) this).A02 = (WaEditText) findViewById(R.id.group_name);
        int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A2A);
        C12990iw.A1I(((AbstractActivityC37221lk) this).A02, new InputFilter[1], A02);
        WaEditText waEditText = ((AbstractActivityC37221lk) this).A02;
        AnonymousClass19M r5 = ((ActivityC13810kN) this).A0B;
        waEditText.addTextChangedListener(new AnonymousClass367(waEditText, C12970iu.A0M(this, R.id.name_counter), ((ActivityC13810kN) this).A08, ((ActivityC13830kP) this).A01, r5, ((AbstractActivityC37221lk) this).A08, A02, A02, false));
    }
}
