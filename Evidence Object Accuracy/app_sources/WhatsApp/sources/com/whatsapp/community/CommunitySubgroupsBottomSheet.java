package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AnonymousClass018;
import X.AnonymousClass0QI;
import X.AnonymousClass10Y;
import X.AnonymousClass11L;
import X.AnonymousClass12F;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass1J1;
import X.AnonymousClass3WV;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15860o1;
import X.C16590pI;
import X.C17070qD;
import X.C19990v2;
import X.C20040v7;
import X.C20710wC;
import X.C21190x1;
import X.C21270x9;
import X.C21400xM;
import X.C22640zP;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C253519b;
import X.C48992Is;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CommunitySubgroupsBottomSheet extends Hilt_CommunitySubgroupsBottomSheet {
    public AnonymousClass0QI A00;
    public C253519b A01;
    public C48992Is A02;
    public C14900mE A03;
    public C15570nT A04;
    public C15450nH A05;
    public AnonymousClass11L A06;
    public C14650lo A07;
    public C238013b A08;
    public C22640zP A09;
    public AnonymousClass3WV A0A;
    public AnonymousClass130 A0B;
    public C15550nR A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public C21270x9 A0F;
    public C14830m7 A0G;
    public C16590pI A0H;
    public C14820m6 A0I;
    public AnonymousClass018 A0J;
    public C19990v2 A0K;
    public C241714m A0L;
    public C15600nX A0M;
    public C236812p A0N;
    public C20040v7 A0O;
    public AnonymousClass10Y A0P;
    public C21400xM A0Q;
    public C14850m9 A0R;
    public C20710wC A0S;
    public AnonymousClass13H A0T;
    public C22710zW A0U;
    public C17070qD A0V;
    public AnonymousClass14X A0W;
    public C21190x1 A0X;
    public C15860o1 A0Y;
    public C240514a A0Z;
    public AnonymousClass12F A0a;
    public AbstractC14440lR A0b;

    public static CommunitySubgroupsBottomSheet A00(C15580nU r4) {
        CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet = new CommunitySubgroupsBottomSheet();
        Bundle bundle = new Bundle();
        bundle.putString("extra_community_jid", r4.getRawString());
        communitySubgroupsBottomSheet.A0U(bundle);
        return communitySubgroupsBottomSheet;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.fragment_community_subgroups_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A14() {
        super.A14();
        AnonymousClass1J1 r0 = this.A0E;
        if (r0 != null) {
            r0.A00();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x00af, code lost:
        if (r1.A0B.A0D(r3) == false) goto L_0x00b1;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r21, android.view.View r22) {
        /*
        // Method dump skipped, instructions count: 485
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.CommunitySubgroupsBottomSheet.A17(android.os.Bundle, android.view.View):void");
    }
}
