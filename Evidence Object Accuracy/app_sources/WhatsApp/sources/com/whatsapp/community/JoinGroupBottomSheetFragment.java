package com.whatsapp.community;

import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass11F;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass1E8;
import X.AnonymousClass1J1;
import X.AnonymousClass4JD;
import X.AnonymousClass5RQ;
import X.C14820m6;
import X.C14830m7;
import X.C14900mE;
import X.C15580nU;
import X.C21270x9;
import X.C21740xu;
import X.C27531Hw;
import X.C468027s;
import X.C67393Ri;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.community.AboutCommunityBottomSheetFragment;
import com.whatsapp.community.JoinGroupBottomSheetFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.text.ReadMoreTextView;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class JoinGroupBottomSheetFragment extends Hilt_JoinGroupBottomSheetFragment {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public Button A05;
    public Button A06;
    public ImageButton A07;
    public ImageView A08;
    public ImageView A09;
    public ImageView A0A;
    public ImageView A0B;
    public ImageView A0C;
    public ImageView A0D;
    public ProgressBar A0E;
    public ScrollView A0F;
    public TextView A0G;
    public TextView A0H;
    public TextView A0I;
    public TextView A0J;
    public TextView A0K;
    public TextView A0L;
    public AnonymousClass12P A0M;
    public AnonymousClass4JD A0N;
    public C14900mE A0O;
    public TextEmojiLabel A0P;
    public C21740xu A0Q;
    public AnonymousClass5RQ A0R;
    public C468027s A0S;
    public AnonymousClass130 A0T;
    public AnonymousClass1J1 A0U;
    public C21270x9 A0V;
    public C14830m7 A0W;
    public C14820m6 A0X;
    public AnonymousClass018 A0Y;
    public AnonymousClass11F A0Z;
    public AnonymousClass1E8 A0a;
    public C15580nU A0b;
    public ReadMoreTextView A0c;
    public List A0d;
    public boolean A0e;

    public static JoinGroupBottomSheetFragment A00(GroupJid groupJid, GroupJid groupJid2, int i) {
        JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = new JoinGroupBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putString("arg_parent_group_jid", groupJid.getRawString());
        bundle.putString("arg_group_jid", groupJid2.getRawString());
        int i2 = 3;
        if (i != 3) {
            i2 = 4;
        }
        bundle.putInt("use_case", i2);
        joinGroupBottomSheetFragment.A0U(bundle);
        return joinGroupBottomSheetFragment;
    }

    public static JoinGroupBottomSheetFragment A01(String str, int i, boolean z) {
        JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = new JoinGroupBottomSheetFragment();
        Bundle bundle = new Bundle();
        int i2 = 2;
        if (i != 1) {
            if (i != 2) {
                i2 = 5;
                if (i != 3) {
                    i2 = 0;
                }
            } else {
                i2 = 1;
            }
        }
        bundle.putInt("use_case", i2);
        bundle.putString("invite_link_code", str);
        bundle.putBoolean("invite_from_referrer", z);
        joinGroupBottomSheetFragment.A0U(bundle);
        return joinGroupBottomSheetFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.community_join_subgroup_bottom_sheet, viewGroup, true);
        this.A0F = (ScrollView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_content_scrollview);
        this.A02 = AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_join_button_container);
        this.A04 = AnonymousClass028.A0D(inflate, R.id.subgroup_info_container_loading);
        this.A03 = AnonymousClass028.A0D(inflate, R.id.subgroup_info_container_loaded);
        this.A00 = AnonymousClass028.A0D(inflate, R.id.subgroup_info_container_error);
        this.A0H = (TextView) AnonymousClass028.A0D(inflate, R.id.subgroup_info_container_error_message);
        this.A0I = (TextView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_retry_button);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_group_title);
        this.A0K = textView;
        C27531Hw.A06(textView);
        this.A08 = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_group_icon);
        this.A0J = (TextView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_group_subtitle);
        this.A0G = (TextView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_group_participant_count);
        this.A0c = (ReadMoreTextView) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_description_text);
        this.A0P = (TextEmojiLabel) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_disclaimer);
        this.A05 = (Button) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_join_button);
        this.A0E = (ProgressBar) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_join_button_spinner);
        this.A06 = (Button) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_view_group);
        this.A07 = (ImageButton) AnonymousClass028.A0D(inflate, R.id.join_group_bottom_sheet_close_button);
        this.A01 = AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview);
        this.A09 = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview_icon_1);
        this.A0A = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview_icon_2);
        this.A0B = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview_icon_3);
        this.A0C = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview_icon_4);
        this.A0D = (ImageView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_preview_icon_5);
        ArrayList arrayList = new ArrayList();
        this.A0d = arrayList;
        arrayList.add(this.A09);
        arrayList.add(this.A0A);
        arrayList.add(this.A0B);
        arrayList.add(this.A0C);
        this.A0d.add(this.A0D);
        this.A0L = (TextView) AnonymousClass028.A0D(inflate, R.id.join_group_contact_count_view);
        return inflate;
    }

    @Override // com.whatsapp.community.Hilt_JoinGroupBottomSheetFragment, com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AnonymousClass5RQ) {
            this.A0R = (AnonymousClass5RQ) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A0b = C15580nU.A04(A03().getString("arg_parent_group_jid"));
        C468027s r1 = (C468027s) new AnonymousClass02A(new C67393Ri(this.A0N, this.A0b, C15580nU.A04(A03().getString("arg_group_jid")), A03().getString("invite_link_code"), A03().getInt("use_case"), A03().getBoolean("invite_from_referrer")), this).A00(C468027s.class);
        r1.A07(false);
        this.A0S = r1;
        r1.A0U.A05(this, new AnonymousClass02B() { // from class: X.3QD
            /* JADX WARNING: Code restructure failed: missing block: B:166:0x0329, code lost:
                if (r9 != 2) goto L_0x032b;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:27:0x003c, code lost:
                if (r6 != 5) goto L_0x003e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:33:0x0049, code lost:
                if (r6 != 5) goto L_0x004b;
             */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0044  */
            /* JADX WARNING: Removed duplicated region for block: B:37:0x006c  */
            /* JADX WARNING: Removed duplicated region for block: B:51:0x00c9  */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r17) {
                /*
                // Method dump skipped, instructions count: 1054
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3QD.ANq(java.lang.Object):void");
            }
        });
        this.A0S.A0Q.A05(this, new AnonymousClass02B() { // from class: X.3QA
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Drawable r0;
                JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = JoinGroupBottomSheetFragment.this;
                Bitmap bitmap = (Bitmap) obj;
                if (bitmap != null) {
                    int i = joinGroupBottomSheetFragment.A0S.A00;
                    ImageView imageView = joinGroupBottomSheetFragment.A08;
                    if (i == 2) {
                        AnonymousClass11F r02 = joinGroupBottomSheetFragment.A0Z;
                        Resources resources = imageView.getResources();
                        AnonymousClass51K r2 = AnonymousClass51K.A00;
                        if (r02.A00.A07(1257)) {
                            r0 = new AnonymousClass2ZZ(resources, bitmap, r2);
                        } else {
                            r0 = new C51842Za(resources, bitmap, r2);
                        }
                        imageView.setImageDrawable(r0);
                        return;
                    }
                    imageView.setImageBitmap(bitmap);
                }
            }
        });
        this.A0S.A0V.A05(this, new AnonymousClass02B() { // from class: X.3QE
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = JoinGroupBottomSheetFragment.this;
                int A05 = C12960it.A05(obj);
                if (A05 != 0) {
                    if (A05 != 1) {
                        C15580nU r12 = joinGroupBottomSheetFragment.A0S.A0J;
                        if (r12 != null) {
                            ActivityC13810kN r4 = (ActivityC13810kN) joinGroupBottomSheetFragment.A0C();
                            if (A05 == 2) {
                                joinGroupBottomSheetFragment.A0v(C14960mK.A0H(r4, r12));
                            } else if (A05 == 3) {
                                r4.Adm(AboutCommunityBottomSheetFragment.A00(r12));
                            } else if (A05 == 4) {
                                Intent putExtra = new C14960mK().A0j(r4, r12).putExtra("start_t", SystemClock.uptimeMillis());
                                C35741ib.A00(putExtra, "RequestToJoinGroupBottomSheetFragment");
                                r4.A2G(putExtra, false);
                            }
                        } else {
                            return;
                        }
                    }
                    AnonymousClass5RQ r13 = joinGroupBottomSheetFragment.A0R;
                    if (r13 != null) {
                        AnonymousClass34P r14 = (AnonymousClass34P) r13;
                        if (!C36021jC.A03(r14)) {
                            r14.A0S.A08 = null;
                        }
                    }
                    joinGroupBottomSheetFragment.A1B();
                }
            }
        });
        this.A0S.A0R.A05(this, new AnonymousClass02B() { // from class: X.3QC
            /* JADX WARNING: Code restructure failed: missing block: B:29:0x006e, code lost:
                if (r4 != 5) goto L_0x0070;
             */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0076  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x0082  */
            /* JADX WARNING: Removed duplicated region for block: B:66:0x0163  */
            /* JADX WARNING: Removed duplicated region for block: B:75:0x01a4  */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r16) {
                /*
                // Method dump skipped, instructions count: 491
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3QC.ANq(java.lang.Object):void");
            }
        });
        this.A0S.A0P.A05(this, new AnonymousClass02B() { // from class: X.3QB
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Object obj2;
                JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = JoinGroupBottomSheetFragment.this;
                Pair pair = (Pair) obj;
                joinGroupBottomSheetFragment.A0c.setVisibility(0);
                C93004Yo.A00(joinGroupBottomSheetFragment.A02, joinGroupBottomSheetFragment.A0F);
                int A05 = C12960it.A05(pair.first);
                if (A05 == 0) {
                    joinGroupBottomSheetFragment.A0c.A0G(null, joinGroupBottomSheetFragment.A0I(R.string.group_announcement_description));
                } else if (A05 == 1) {
                    joinGroupBottomSheetFragment.A0c.setVisibility(8);
                } else if (A05 == 2 && (obj2 = pair.second) != null) {
                    joinGroupBottomSheetFragment.A0c.A0G(null, (CharSequence) obj2);
                }
            }
        });
        this.A0U = this.A0V.A04(A01(), "join-group-bottom-sheet");
        this.A0e = A03().getBoolean("invite_from_referrer");
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A0c.A09.A05(this, new AnonymousClass02B() { // from class: X.4sz
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                JoinGroupBottomSheetFragment joinGroupBottomSheetFragment = JoinGroupBottomSheetFragment.this;
                if (Boolean.TRUE.equals(obj)) {
                    C93004Yo.A00(joinGroupBottomSheetFragment.A02, joinGroupBottomSheetFragment.A0F);
                }
            }
        });
        this.A07.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 41));
    }

    public final void A1M(int i) {
        if (i > 0) {
            TextView textView = this.A0L;
            textView.setText(textView.getContext().getString(R.string.additional_participant_count, Integer.valueOf(i)));
            this.A0L.setVisibility(0);
            return;
        }
        this.A0L.setVisibility(8);
    }
}
