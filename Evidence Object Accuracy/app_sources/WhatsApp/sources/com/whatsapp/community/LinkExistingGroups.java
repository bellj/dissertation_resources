package com.whatsapp.community;

import X.AbstractActivityC36611kC;
import X.AbstractC005102i;
import X.AbstractC15590nW;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass12U;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C20710wC;
import X.C253118x;
import X.C29951Vj;
import X.C52162aM;
import X.C64043Ea;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class LinkExistingGroups extends AbstractActivityC36611kC {
    public View A00;
    public C15600nX A01;
    public C20710wC A02;
    public AnonymousClass12U A03;
    public C253118x A04;
    public boolean A05;

    public LinkExistingGroups() {
        this(0);
    }

    public LinkExistingGroups(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 44);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A04 = (C253118x) A1M.AAW.get();
            this.A03 = ActivityC13830kP.A1N(A1M);
            this.A02 = C12980iv.A0e(A1M);
            this.A01 = C12980iv.A0d(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A2y(int i) {
        int i2;
        long j;
        Object[] A1a;
        if (A1U() == null) {
            Log.e("LinkExistingGroups/updateTitle/getSupportActionBar is null");
            return;
        }
        int A2j = A2j();
        AbstractC005102i A1U = A1U();
        AnonymousClass018 r4 = this.A0S;
        if (A2j == Integer.MAX_VALUE) {
            i2 = R.plurals.n_groups_added;
            j = (long) i;
            A1a = new Object[1];
            C12960it.A1P(A1a, i, 0);
        } else {
            i2 = R.plurals.n_of_m_groups_added;
            j = (long) i;
            A1a = C12980iv.A1a();
            C12960it.A1P(A1a, i, 0);
            C12960it.A1P(A1a, A2j, 1);
        }
        A1U.A0H(r4.A0I(A1a, i2, j));
    }

    @Override // X.AbstractActivityC36611kC
    public void A31(C64043Ea r6, C15370n3 r7) {
        TextEmojiLabel textEmojiLabel = r6.A02;
        textEmojiLabel.setSingleLine(false);
        textEmojiLabel.setMaxLines(2);
        C29951Vj r1 = r7.A0F;
        if (!r7.A0K() || r1 == null) {
            super.A31(r6, r7);
            return;
        }
        int i = r1.A00;
        if (i == 0) {
            textEmojiLabel.setVisibility(0);
            C15610nY r2 = ((AbstractActivityC36611kC) this).A0L;
            textEmojiLabel.A0G(null, (String) r2.A09.get(r7.A0B(AbstractC15590nW.class)));
            r6.A01(r7.A0d);
        } else if (i == 2) {
            String str = null;
            C15580nU r12 = r1.A01;
            if (r12 != null) {
                C15370n3 A0B = ((AbstractActivityC36611kC) this).A0J.A0B(r12);
                str = C12960it.A0X(this, C15610nY.A01(((AbstractActivityC36611kC) this).A0L, A0B), C12970iu.A1b(), 0, R.string.link_to_another_community);
            }
            r6.A00(str, false);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A36(List list) {
        if (list.isEmpty()) {
            setResult(-10);
            finish();
            return;
        }
        super.A36(list);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C29951Vj r0 = C12970iu.A0a(it).A0F;
            if (r0 != null && r0.A00 == 0) {
                return;
            }
        }
        TextView A0I = C12960it.A0I(A2o(), R.id.multiple_contact_picker_warning_text);
        A0I.setText(this.A04.A02(this, new RunnableBRunnable0Shape15S0100000_I1_1(this, 15), getString(R.string.create_group_instead), "create_new_group"));
        C52162aM.A00(A0I);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 150) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 != -1) {
            Log.i("LinkExistingGroups/permissions denied");
            finish();
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null && !((AbstractActivityC36611kC) this).A0I.A00()) {
            RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_link_existing_groups_request, R.string.permission_contacts_access_on_link_existing_groups);
        }
    }
}
