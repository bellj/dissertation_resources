package com.whatsapp.community;

import X.AbstractC005102i;
import X.AbstractC11790gs;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC33041dC;
import X.AbstractC469528i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass10Y;
import X.AnonymousClass11A;
import X.AnonymousClass11F;
import X.AnonymousClass11L;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass19M;
import X.AnonymousClass1BF;
import X.AnonymousClass1E8;
import X.AnonymousClass1J1;
import X.AnonymousClass2AA;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2IH;
import X.AnonymousClass2IT;
import X.AnonymousClass2TT;
import X.AnonymousClass3EZ;
import X.AnonymousClass3RZ;
import X.AnonymousClass3WV;
import X.AnonymousClass42V;
import X.AnonymousClass450;
import X.AnonymousClass519;
import X.AnonymousClass5UJ;
import X.C009604u;
import X.C102934pw;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15860o1;
import X.C15880o3;
import X.C16590pI;
import X.C17070qD;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19990v2;
import X.C20040v7;
import X.C20660w7;
import X.C20710wC;
import X.C21190x1;
import X.C21270x9;
import X.C21320xE;
import X.C21400xM;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22330yu;
import X.C22640zP;
import X.C22670zS;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C244215l;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253519b;
import X.C27151Gf;
import X.C33051dD;
import X.C33061dE;
import X.C36801kb;
import X.C37271lv;
import X.C42861w0;
import X.C48992Is;
import X.C54692h8;
import X.C63543Bz;
import X.C67313Ra;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.google.android.material.appbar.AppBarLayout;
import com.whatsapp.R;
import com.whatsapp.community.AboutCommunityBottomSheetFragment;
import com.whatsapp.community.CommunityHomeActivity;
import com.whatsapp.community.NewCommunityAdminBottomSheetFragment;
import com.whatsapp.communitysuspend.CommunitySuspendDialogFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import java.util.Collection;
import java.util.List;

/* loaded from: classes2.dex */
public class CommunityHomeActivity extends ActivityC13790kL {
    public int A00;
    public long A01;
    public Menu A02;
    public ImageView A03;
    public TextView A04;
    public TextView A05;
    public C253519b A06;
    public AnonymousClass2IT A07;
    public AnonymousClass2IH A08;
    public C48992Is A09;
    public AnonymousClass11L A0A;
    public C14650lo A0B;
    public AnonymousClass10A A0C;
    public C238013b A0D;
    public C22330yu A0E;
    public C22640zP A0F;
    public C37271lv A0G;
    public AnonymousClass1BF A0H;
    public AnonymousClass130 A0I;
    public C15550nR A0J;
    public AnonymousClass10S A0K;
    public C15610nY A0L;
    public AnonymousClass1J1 A0M;
    public C21270x9 A0N;
    public C33051dD A0O;
    public C16590pI A0P;
    public C19990v2 A0Q;
    public C21320xE A0R;
    public C241714m A0S;
    public C15600nX A0T;
    public C236812p A0U;
    public C20040v7 A0V;
    public AnonymousClass10Y A0W;
    public C15370n3 A0X;
    public C21400xM A0Y;
    public AnonymousClass11F A0Z;
    public C22050yP A0a;
    public C20710wC A0b;
    public AnonymousClass11A A0c;
    public C244215l A0d;
    public AnonymousClass1E8 A0e;
    public C33061dE A0f;
    public C15580nU A0g;
    public AnonymousClass13H A0h;
    public C17220qS A0i;
    public C20660w7 A0j;
    public C22710zW A0k;
    public C17070qD A0l;
    public AnonymousClass14X A0m;
    public C21190x1 A0n;
    public C15860o1 A0o;
    public C240514a A0p;
    public AnonymousClass12F A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public final C27151Gf A0u;
    public final AnonymousClass5UJ A0v;
    public final AbstractC469528i A0w;

    public CommunityHomeActivity() {
        this(0);
        this.A0v = new AnonymousClass5UJ() { // from class: X.578
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                CommunityHomeActivity communityHomeActivity = CommunityHomeActivity.this;
                if (communityHomeActivity.A0g.equals(r3)) {
                    communityHomeActivity.invalidateOptionsMenu();
                }
            }
        };
        this.A0w = new AnonymousClass2AA(this);
        this.A0u = new AnonymousClass42V(this);
    }

    public CommunityHomeActivity(int i) {
        this.A0s = false;
        A0R(new C102934pw(this));
    }

    public static /* synthetic */ void A02(Bundle bundle, CommunityHomeActivity communityHomeActivity) {
        if (bundle.getInt("dialogAction") == 1) {
            C15580nU r1 = (C15580nU) bundle.getParcelable("parentGroupJid");
            AnonymousClass009.A05(r1);
            AnonymousClass3EZ A00 = communityHomeActivity.A08.A00(communityHomeActivity, r1, 1);
            C15570nT r0 = ((ActivityC13790kL) communityHomeActivity).A01;
            r0.A08();
            A00.A00(r0.A05);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0s) {
            this.A0s = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A0h = (AnonymousClass13H) r2.ABY.get();
            this.A0P = (C16590pI) r2.AMg.get();
            this.A0Q = (C19990v2) r2.A3M.get();
            this.A0j = (C20660w7) r2.AIB.get();
            this.A0N = (C21270x9) r2.A4A.get();
            this.A0V = (C20040v7) r2.AAK.get();
            this.A0i = (C17220qS) r2.ABt.get();
            this.A0m = (AnonymousClass14X) r2.AFM.get();
            this.A0J = (C15550nR) r2.A45.get();
            this.A0I = (AnonymousClass130) r2.A41.get();
            this.A06 = (C253519b) r2.A4C.get();
            this.A0S = (C241714m) r2.A4l.get();
            this.A0L = (C15610nY) r2.AMe.get();
            this.A0n = (C21190x1) r2.A3G.get();
            this.A0l = (C17070qD) r2.AFC.get();
            this.A0D = (C238013b) r2.A1Z.get();
            this.A0K = (AnonymousClass10S) r2.A46.get();
            this.A0b = (C20710wC) r2.A8m.get();
            this.A0W = (AnonymousClass10Y) r2.AAR.get();
            this.A0q = (AnonymousClass12F) r2.AJM.get();
            this.A0a = (C22050yP) r2.A7v.get();
            this.A0p = (C240514a) r2.AJL.get();
            this.A0o = (C15860o1) r2.A3H.get();
            this.A0E = (C22330yu) r2.A3I.get();
            this.A0Z = (AnonymousClass11F) r2.AE3.get();
            this.A0Y = (C21400xM) r2.ABd.get();
            this.A0F = (C22640zP) r2.A3Z.get();
            this.A0H = (AnonymousClass1BF) r2.A3b.get();
            this.A0U = (C236812p) r2.AAC.get();
            this.A0e = (AnonymousClass1E8) r2.ADy.get();
            this.A0R = (C21320xE) r2.A4Y.get();
            this.A0k = (C22710zW) r2.AF7.get();
            this.A0B = (C14650lo) r2.A2V.get();
            this.A0C = (AnonymousClass10A) r2.A2W.get();
            this.A0c = (AnonymousClass11A) r2.A8o.get();
            this.A0T = (C15600nX) r2.A8x.get();
            this.A0A = (AnonymousClass11L) r2.ALG.get();
            this.A07 = (AnonymousClass2IT) r1.A0K.get();
            this.A0d = (C244215l) r2.A8y.get();
            this.A09 = (C48992Is) r1.A0a.get();
            this.A08 = (AnonymousClass2IH) r1.A0o.get();
        }
    }

    public final void A2e() {
        if (!(!((ActivityC13810kN) this).A0E)) {
            startActivity(C14960mK.A02(this).addFlags(603979776));
        }
    }

    public final void A2f(int i) {
        this.A00 = i;
        if (this.A0t) {
            this.A05.setText(R.string.community_home_subtitle);
        } else {
            this.A05.setText(getResources().getQuantityString(R.plurals.parent_home_header_group_info, i, Integer.valueOf(i)));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 123) {
            super.onActivityResult(i, i2, intent);
        } else if (intent != null && i2 == -1) {
            if (!((ActivityC13810kN) this).A07.A0B()) {
                ((ActivityC13810kN) this).A05.A07(R.string.edit_community_no_internet, 0);
                return;
            }
            String stringExtra = intent.getStringExtra("extra_community_description");
            if (stringExtra != null) {
                AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
                C14830m7 r7 = ((ActivityC13790kL) this).A05;
                r1.Aaz(new AnonymousClass450(((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A01, this, r7, this.A0R, this.A0X, this.A0j, stringExtra), new Void[0]);
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A0r) {
            Intent A02 = C14960mK.A02(this);
            A02.setFlags(67108864);
            startActivity(A02);
            return;
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.A01 = SystemClock.uptimeMillis();
        super.onCreate(bundle);
        this.A0M = this.A0N.A04(this, "community-home");
        setContentView(R.layout.activity_community_home);
        C15580nU A04 = C15580nU.A04(getIntent().getStringExtra("parent_group_jid"));
        AnonymousClass009.A05(A04);
        this.A0g = A04;
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(this, 42));
        C15370n3 A0A = this.A0J.A0A(this.A0g);
        this.A0X = A0A;
        if (A0A == null) {
            A2e();
            return;
        }
        this.A0e.A03(this.A0w);
        this.A03 = (ImageView) AnonymousClass00T.A05(this, R.id.communityPhoto);
        this.A04 = (TextView) AnonymousClass00T.A05(this, R.id.communityName);
        this.A05 = (TextView) AnonymousClass00T.A05(this, R.id.communityStatus);
        A1e((Toolbar) AnonymousClass00T.A05(this, R.id.toolbar));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0P(false);
        A1U.A0D(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.icon_secondary), ((ActivityC13830kP) this).A01));
        AppBarLayout appBarLayout = (AppBarLayout) AnonymousClass00T.A05(this, R.id.app_bar);
        AbstractC005102i A1U2 = A1U();
        AnonymousClass018 r11 = ((ActivityC13830kP) this).A01;
        ImageView imageView = this.A03;
        TextView textView = this.A04;
        TextView textView2 = this.A05;
        View view = new View(this);
        if (A1U2.A03() == null) {
            A1U2.A0G(view, new C009604u(-1, -1));
        }
        A1U2.A0N(true);
        View A03 = A1U2.A03();
        AnonymousClass009.A03(A03);
        AnonymousClass519 r6 = new AnonymousClass519(A03, imageView, textView, textView2, r11);
        appBarLayout.setExpanded(true);
        appBarLayout.A01(r6);
        C37271lv r1 = (C37271lv) new AnonymousClass02A(new AnonymousClass3RZ(this.A09, this.A0g), this).A00(C37271lv.class);
        this.A0G = r1;
        r1.A05.A05(this, new AnonymousClass02B() { // from class: X.4sv
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity communityHomeActivity = CommunityHomeActivity.this;
                communityHomeActivity.A0M.A06(communityHomeActivity.A03, (C15370n3) obj);
            }
        });
        this.A0G.A04.A05(this, new AnonymousClass02B() { // from class: X.4sy
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity.this.A04.setText((String) obj);
            }
        });
        this.A0G.A0R.A05(this, new AnonymousClass02B() { // from class: X.4sx
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity.this.A2f(C12960it.A05(obj));
            }
        });
        A0V().A0e(new AbstractC11790gs() { // from class: X.4rx
            @Override // X.AbstractC11790gs
            public final void AQn(String str, Bundle bundle2) {
                CommunityHomeActivity.A02(bundle2, CommunityHomeActivity.this);
            }
        }, this, NewCommunityAdminBottomSheetFragment.A03);
        ((C36801kb) new AnonymousClass02A(new C67313Ra(this.A07, this.A0X), this).A00(C36801kb.class)).A05.A05(this, new AnonymousClass02B() { // from class: X.3Q9
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity communityHomeActivity = CommunityHomeActivity.this;
                boolean A1Y = C12970iu.A1Y(obj);
                communityHomeActivity.A0t = A1Y;
                communityHomeActivity.A2f(communityHomeActivity.A00);
                communityHomeActivity.invalidateOptionsMenu();
                Menu menu = communityHomeActivity.A02;
                if (menu != null) {
                    menu.close();
                }
                if (A1Y) {
                    communityHomeActivity.Adm(new CommunitySuspendDialogFragment());
                }
            }
        });
        AnonymousClass11A r12 = this.A0c;
        r12.A00.add(this.A0v);
        C15580nU r4 = this.A0g;
        AbstractC15710nm r2 = ((ActivityC13810kN) this).A03;
        AbstractC14440lR r62 = ((ActivityC13830kP) this).A05;
        new C42861w0(r2, this.A0b, r4, this.A0i, r62).A00();
        C15580nU r13 = this.A0g;
        C14830m7 r14 = ((ActivityC13790kL) this).A05;
        C14900mE r15 = ((ActivityC13810kN) this).A05;
        AbstractC14440lR r16 = ((ActivityC13830kP) this).A05;
        C20040v7 r5 = this.A0V;
        C21190x1 r42 = this.A0n;
        AnonymousClass12F r3 = this.A0q;
        AnonymousClass3WV r29 = new AnonymousClass3WV(this, r15, this.A0F, r14, r5, r42, this.A0p, r3, r16);
        C14850m9 r17 = ((ActivityC13810kN) this).A0C;
        AnonymousClass13H r18 = this.A0h;
        C15570nT r19 = ((ActivityC13790kL) this).A01;
        C16590pI r110 = this.A0P;
        C19990v2 r111 = this.A0Q;
        C15450nH r112 = ((ActivityC13810kN) this).A06;
        C63543Bz r122 = new C63543Bz(this);
        AnonymousClass12P r113 = ((ActivityC13790kL) this).A00;
        AnonymousClass14X r114 = this.A0m;
        C15550nR r115 = this.A0J;
        AnonymousClass130 r116 = this.A0I;
        C253519b r117 = this.A06;
        C241714m r118 = this.A0S;
        C15610nY r119 = this.A0L;
        AnonymousClass018 r120 = ((ActivityC13830kP) this).A01;
        C17070qD r121 = this.A0l;
        AnonymousClass2TT r1110 = new AnonymousClass2TT(this);
        C238013b r123 = this.A0D;
        C20710wC r124 = this.A0b;
        AnonymousClass10Y r125 = this.A0W;
        AnonymousClass12F r126 = this.A0q;
        C15860o1 r127 = this.A0o;
        C22050yP r128 = this.A0a;
        AnonymousClass11F r152 = this.A0Z;
        C21400xM r142 = this.A0Y;
        C14820m6 r132 = ((ActivityC13810kN) this).A09;
        AnonymousClass1J1 r10 = this.A0M;
        C22640zP r9 = this.A0F;
        AnonymousClass1BF r8 = this.A0H;
        C236812p r7 = this.A0U;
        C22710zW r63 = this.A0k;
        C14650lo r52 = this.A0B;
        C37271lv r43 = this.A0G;
        this.A0f = new C33061dE(this, r113, r117, r15, r19, r112, this.A0A, r1110, r52, r123, r9, r8, r116, r115, r119, r10, r29, r122, r14, r110, r132, r120, r111, r118, this.A0T, r7, r125, r142, r152, r17, r128, r124, r43, r43, new AbstractC33041dC() { // from class: X.57L
            @Override // X.AbstractC33041dC
            public final void ALu(AbstractC15340mz r129) {
            }
        }, r43, r13, r18, r63, r121, r114, r127, r126, r16, 3);
        RecyclerView recyclerView = (RecyclerView) AnonymousClass00T.A05(this, R.id.subgroup_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(1);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(this.A0f);
        recyclerView.A0l(new C54692h8(this, recyclerView, this.A0f, null));
        this.A0G.A0T.A05(this, new AnonymousClass02B() { // from class: X.4su
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C33061dE r129 = CommunityHomeActivity.this.A0f;
                List list = r129.A00;
                list.clear();
                list.addAll((Collection) obj);
                r129.A02();
            }
        });
        C33061dE r72 = this.A0f;
        AnonymousClass10S r44 = this.A0K;
        C33051dD r129 = new C33051dD(this.A0C, this.A0E, r44, this.A0R, this.A0d, r72);
        this.A0O = r129;
        r129.A00();
        this.A0G.A00 = 50;
        this.A0R.A03(this.A0u);
        this.A0G.A07.A05(this, new AnonymousClass02B() { // from class: X.3Q8
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity communityHomeActivity = CommunityHomeActivity.this;
                NewCommunityAdminBottomSheetFragment newCommunityAdminBottomSheetFragment = new NewCommunityAdminBottomSheetFragment();
                Bundle A0D = C12970iu.A0D();
                A0D.putParcelable("parent_group_jid", (Jid) obj);
                newCommunityAdminBottomSheetFragment.A0U(A0D);
                communityHomeActivity.Adm(newCommunityAdminBottomSheetFragment);
            }
        });
        this.A0G.A06.A05(this, new AnonymousClass02B() { // from class: X.4sw
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityHomeActivity.this.Adm(AboutCommunityBottomSheetFragment.A00((GroupJid) obj));
            }
        });
        this.A03.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 18));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r3.A0T.A0D(r3.A0g) == false) goto L_0x0019;
     */
    @Override // X.ActivityC13790kL, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onCreateOptionsMenu(android.view.Menu r4) {
        /*
            r3 = this;
            r3.A02 = r4
            boolean r0 = r3.A0t
            if (r0 != 0) goto L_0x0029
            X.0zP r0 = r3.A0F
            boolean r0 = r0.A06()
            if (r0 == 0) goto L_0x0019
            X.0nX r1 = r3.A0T
            X.0nU r0 = r3.A0g
            boolean r0 = r1.A0D(r0)
            r2 = 1
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r2 = 0
        L_0x001a:
            android.view.MenuInflater r1 = r3.getMenuInflater()
            r0 = 2131623945(0x7f0e0009, float:1.8875056E38)
            if (r2 == 0) goto L_0x0026
            r0 = 2131623944(0x7f0e0008, float:1.8875054E38)
        L_0x0026:
            r1.inflate(r0, r4)
        L_0x0029:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.CommunityHomeActivity.onCreateOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C33051dD r0 = this.A0O;
        if (r0 != null) {
            r0.A01();
        }
        AnonymousClass1J1 r02 = this.A0M;
        if (r02 != null) {
            r02.A00();
        }
        AnonymousClass11A r03 = this.A0c;
        if (r03 != null) {
            r03.A00.remove(this.A0v);
        }
        AnonymousClass1E8 r1 = this.A0e;
        if (r1 != null) {
            r1.A04(this.A0w);
        }
        C21320xE r12 = this.A0R;
        if (r12 != null) {
            r12.A04(this.A0u);
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent intent;
        if (menuItem.getItemId() == R.id.menu_manage_groups) {
            intent = C14960mK.A0J(this, this.A0g);
        } else if (menuItem.getItemId() == R.id.menu_edit_community) {
            C15580nU r3 = this.A0g;
            Intent intent2 = new Intent();
            intent2.setClassName(getPackageName(), "com.whatsapp.community.EditCommunityActivity");
            intent2.putExtra("extra_community_jid", C15380n4.A03(r3));
            startActivityForResult(intent2, 123);
            return true;
        } else if (menuItem.getItemId() == 16908332) {
            onBackPressed();
            return true;
        } else if (menuItem.getItemId() == R.id.menu_view_members) {
            C15580nU r2 = this.A0g;
            intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.community.CommunityMembersActivity");
            intent.putExtra("extra_community_jid", C15380n4.A03(r2));
        } else if (menuItem.getItemId() == R.id.menu_report_community) {
            Adm(CommunitySpamReportDialogFragment.A00(this.A0g, "overflow_menu_community_report"));
            return true;
        } else {
            super.onOptionsItemSelected(menuItem);
            return false;
        }
        startActivity(intent);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0Q.A0F(this.A0g)) {
            A2e();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0a.A04(9, SystemClock.uptimeMillis() - this.A01);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        this.A0r = true;
        super.onStop();
    }
}
