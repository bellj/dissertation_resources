package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AbstractC37281lw;
import X.AbstractC37291lx;
import X.AbstractC37301ly;
import X.ActivityC000900k;
import X.AnonymousClass00X;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10A;
import X.AnonymousClass10S;
import X.AnonymousClass10Y;
import X.AnonymousClass11F;
import X.AnonymousClass11L;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass1BF;
import X.AnonymousClass1J1;
import X.AnonymousClass2LB;
import X.AnonymousClass2TT;
import X.AnonymousClass3WV;
import X.AnonymousClass47S;
import X.AnonymousClass47T;
import X.AnonymousClass57K;
import X.AnonymousClass57M;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15860o1;
import X.C16590pI;
import X.C17070qD;
import X.C19990v2;
import X.C20040v7;
import X.C20710wC;
import X.C21190x1;
import X.C21270x9;
import X.C21320xE;
import X.C21400xM;
import X.C22050yP;
import X.C22330yu;
import X.C22640zP;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C244215l;
import X.C253519b;
import X.C33051dD;
import X.C33061dE;
import X.C36161jQ;
import X.C63543Bz;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.community.CommunityFragment;
import com.whatsapp.jid.GroupJid;

/* loaded from: classes2.dex */
public class CommunityFragment extends Hilt_CommunityFragment implements AbstractC14940mI, AbstractC14770m1 {
    public AnonymousClass12P A00;
    public C253519b A01;
    public C14900mE A02;
    public C15570nT A03;
    public C15450nH A04;
    public AnonymousClass11L A05;
    public C14650lo A06;
    public AnonymousClass10A A07;
    public C238013b A08;
    public C22330yu A09;
    public C22640zP A0A;
    public CommunityTabViewModel A0B;
    public AnonymousClass1BF A0C;
    public AnonymousClass130 A0D;
    public C15550nR A0E;
    public AnonymousClass10S A0F;
    public C15610nY A0G;
    public C21270x9 A0H;
    public C33051dD A0I;
    public C14830m7 A0J;
    public C16590pI A0K;
    public C14820m6 A0L;
    public AnonymousClass018 A0M;
    public C19990v2 A0N;
    public C21320xE A0O;
    public C241714m A0P;
    public C15600nX A0Q;
    public C236812p A0R;
    public C20040v7 A0S;
    public AnonymousClass10Y A0T;
    public C21400xM A0U;
    public AnonymousClass11F A0V;
    public C14850m9 A0W;
    public C22050yP A0X;
    public C20710wC A0Y;
    public C244215l A0Z;
    public C33061dE A0a;
    public AnonymousClass13H A0b;
    public C22710zW A0c;
    public C17070qD A0d;
    public AnonymousClass14X A0e;
    public C21190x1 A0f;
    public C15860o1 A0g;
    public C240514a A0h;
    public AnonymousClass12F A0i;
    public AbstractC14440lR A0j;
    public boolean A0k = false;
    public final AnonymousClass02B A0l = new AnonymousClass02B() { // from class: X.4st
        @Override // X.AnonymousClass02B
        public final void ANq(Object obj) {
            CommunityFragment communityFragment = CommunityFragment.this;
            communityFragment.A02.A0H(new RunnableBRunnable0Shape2S0200000_I0_2(communityFragment, 5, obj));
        }
    };

    @Override // X.AbstractC14770m1
    public /* synthetic */ void A68(AnonymousClass2LB r1) {
    }

    @Override // X.AbstractC14940mI
    public String AE3() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AE4() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AE5() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AGV() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AGW() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AHA() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AHB() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public void ASK() {
    }

    @Override // X.AbstractC14940mI
    public void AVh() {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void Acn(boolean z) {
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        this.A0B.A0N.A05(this, this.A0l);
        super.A0r();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.fragment_community, viewGroup, false);
        RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(inflate, R.id.community_recycler_view);
        recyclerView.A0h = true;
        inflate.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager());
        CommunityTabViewModel communityTabViewModel = (CommunityTabViewModel) new AnonymousClass02A(this).A00(CommunityTabViewModel.class);
        this.A0B = communityTabViewModel;
        communityTabViewModel.A0M.A05(A0G(), this.A0l);
        this.A0B.A0Q.A05(A0G(), new AnonymousClass02B() { // from class: X.4sr
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityFragment communityFragment = CommunityFragment.this;
                communityFragment.A0v(C14960mK.A0J(communityFragment.A01(), (GroupJid) obj));
            }
        });
        this.A0B.A0P.A05(A0G(), new AnonymousClass02B() { // from class: X.4ss
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityFragment communityFragment = CommunityFragment.this;
                communityFragment.A0A.A03(communityFragment.A01(), (GroupJid) obj);
            }
        });
        AnonymousClass1J1 A04 = this.A0H.A04(A0B(), "community-tab");
        ActivityC000900k A0B = A0B();
        C14830m7 r2 = this.A0J;
        C14900mE r22 = this.A02;
        AbstractC14440lR r23 = this.A0j;
        C20040v7 r6 = this.A0S;
        C21190x1 r5 = this.A0f;
        AnonymousClass12F r4 = this.A0i;
        AnonymousClass3WV r32 = new AnonymousClass3WV(A0B, r22, this.A0A, r2, r6, r5, this.A0h, r4, r23);
        C14850m9 r24 = this.A0W;
        AnonymousClass13H r25 = this.A0b;
        C15570nT r26 = this.A03;
        C16590pI r27 = this.A0K;
        C19990v2 r28 = this.A0N;
        C15450nH r29 = this.A04;
        C63543Bz r8 = new C63543Bz(A0p());
        AnonymousClass12P r210 = this.A00;
        AnonymousClass14X r211 = this.A0e;
        C15550nR r212 = this.A0E;
        AnonymousClass130 r213 = this.A0D;
        C253519b r214 = this.A01;
        C241714m r215 = this.A0P;
        C15610nY r216 = this.A0G;
        AnonymousClass018 r217 = this.A0M;
        C17070qD r218 = this.A0d;
        AnonymousClass2TT r62 = new AnonymousClass2TT(A0p());
        C238013b r219 = this.A08;
        C20710wC r220 = this.A0Y;
        AnonymousClass10Y r221 = this.A0T;
        AnonymousClass12F r222 = this.A0i;
        C15860o1 r223 = this.A0g;
        C22050yP r224 = this.A0X;
        AnonymousClass11F r225 = this.A0V;
        C21400xM r226 = this.A0U;
        C14820m6 r15 = this.A0L;
        C22640zP r14 = this.A0A;
        AnonymousClass1BF r13 = this.A0C;
        C236812p r12 = this.A0R;
        C22710zW r11 = this.A0c;
        C14650lo r10 = this.A06;
        AnonymousClass57K r49 = new AbstractC37291lx() { // from class: X.57K
            @Override // X.AbstractC37291lx
            public final void AQ3(C90264Nf r1) {
            }
        };
        C15600nX r9 = this.A0Q;
        AnonymousClass11L r52 = this.A05;
        AnonymousClass57M r51 = new AbstractC37281lw() { // from class: X.57M
            @Override // X.AbstractC37281lw
            public final void ARV() {
            }
        };
        C33061dE r227 = new C33061dE(A0B(), r210, r214, r22, r26, r29, r52, r62, r10, r219, r14, r13, r213, r212, r216, A04, r32, r8, r2, r27, r15, r217, r28, r215, r9, r12, r221, r226, r225, r24, r224, r220, new AbstractC37301ly() { // from class: X.57J
        }, r49, this.A0B, r51, null, r25, r11, r218, r211, r223, r222, r23, 4);
        this.A0a = r227;
        recyclerView.setAdapter(r227);
        recyclerView.A0l(new AnonymousClass47S(AnonymousClass00X.A04(null, A02(), R.drawable.community_divider_shadow), this));
        recyclerView.A0l(new AnonymousClass47T(AnonymousClass00X.A04(null, A02(), R.drawable.subgroup_divider), this));
        C33061dE r82 = this.A0a;
        AnonymousClass10S r53 = this.A0F;
        C33051dD r228 = new C33051dD(this.A07, this.A09, r53, this.A0O, this.A0Z, r82);
        this.A0I = r228;
        r228.A00();
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A0I.A01();
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        boolean z = this.A0k;
        C36161jQ r1 = this.A0B.A0N;
        AnonymousClass02B r0 = this.A0l;
        if (z) {
            r1.A09(r0);
        } else {
            r1.A05(this, r0);
        }
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        CommunityTabViewModel communityTabViewModel = this.A0B;
        if (communityTabViewModel.A00 != null) {
            communityTabViewModel.A0O.execute(new RunnableBRunnable0Shape4S0100000_I0_4(communityTabViewModel, 0));
        }
        super.A14();
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void A5j(AbstractC14780m2 r1) {
        r1.AM3();
    }

    @Override // X.AbstractC14770m1
    public void Aco(boolean z) {
        this.A0k = z;
        C36161jQ r1 = this.A0B.A0N;
        AnonymousClass02B r0 = this.A0l;
        if (z) {
            r1.A09(r0);
        } else {
            r1.A05(this, r0);
        }
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ boolean Aed() {
        return false;
    }
}
