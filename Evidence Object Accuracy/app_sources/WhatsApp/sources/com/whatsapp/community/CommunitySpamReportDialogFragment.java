package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass028;
import X.AnonymousClass04S;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C15580nU;
import X.C254119h;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.R;
import com.whatsapp.community.CommunitySpamReportDialogFragment;

/* loaded from: classes2.dex */
public class CommunitySpamReportDialogFragment extends Hilt_CommunitySpamReportDialogFragment {
    public C14900mE A00;
    public C15550nR A01;
    public C254119h A02;
    public AbstractC14440lR A03;

    public static CommunitySpamReportDialogFragment A00(C15580nU r3, String str) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r3.getRawString());
        A0D.putString("spamFlow", str);
        CommunitySpamReportDialogFragment communitySpamReportDialogFragment = new CommunitySpamReportDialogFragment();
        communitySpamReportDialogFragment.A0U(A0D);
        return communitySpamReportDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC13810kN r7 = (ActivityC13810kN) A0B();
        AbstractC14640lm A01 = AbstractC14640lm.A01(A03().getString("jid"));
        AnonymousClass009.A05(A01);
        String string = A03().getString("spamFlow");
        C15370n3 A0B = this.A01.A0B(A01);
        View inflate = LayoutInflater.from(A0p()).inflate(R.layout.dialog_report_spam, (ViewGroup) null);
        TextView A0I = C12960it.A0I(inflate, R.id.report_spam_dialog_message);
        AnonymousClass009.A05(r7);
        C004802e A0S = C12980iv.A0S(r7);
        A0S.setView(inflate);
        A0S.A07(R.string.report_community_ask);
        A0I.setText(R.string.reporting_dialog_community_text);
        AnonymousClass028.A0D(inflate, R.id.block_container).setVisibility(8);
        C12990iw.A0z(new DialogInterface.OnClickListener(r7, this, A0B, string) { // from class: X.3Kq
            public final /* synthetic */ ActivityC13810kN A00;
            public final /* synthetic */ CommunitySpamReportDialogFragment A01;
            public final /* synthetic */ C15370n3 A02;
            public final /* synthetic */ String A03;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
                this.A03 = r4;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CommunitySpamReportDialogFragment communitySpamReportDialogFragment = this.A01;
                ActivityC13810kN r1 = this.A00;
                C15370n3 r4 = this.A02;
                String str = this.A03;
                if (communitySpamReportDialogFragment.A02.A02(r1)) {
                    C14960mK.A0d(communitySpamReportDialogFragment);
                    communitySpamReportDialogFragment.A00.A06(R.string.reporting_spam_title, R.string.register_wait_message);
                    communitySpamReportDialogFragment.A03.Ab2(new RunnableBRunnable0Shape1S1200000_I1(r4, communitySpamReportDialogFragment, str, 13));
                }
            }
        }, null, A0S, R.string.report_spam);
        AnonymousClass04S create = A0S.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
