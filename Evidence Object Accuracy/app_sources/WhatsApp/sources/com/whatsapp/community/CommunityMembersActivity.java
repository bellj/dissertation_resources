package com.whatsapp.community;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass024;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass12F;
import X.AnonymousClass198;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass2IH;
import X.AnonymousClass3DP;
import X.AnonymousClass3EZ;
import X.AnonymousClass3RY;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14860mA;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C20660w7;
import X.C20710wC;
import X.C20730wE;
import X.C21270x9;
import X.C21320xE;
import X.C237913a;
import X.C254219i;
import X.C36691kN;
import X.C49002It;
import X.C54442gj;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.community.CommunityMembersActivity;

/* loaded from: classes2.dex */
public class CommunityMembersActivity extends ActivityC13790kL {
    public AnonymousClass2IH A00;
    public C49002It A01;
    public C237913a A02;
    public AnonymousClass3EZ A03;
    public C15550nR A04;
    public C15610nY A05;
    public C21270x9 A06;
    public C20730wE A07;
    public C21320xE A08;
    public C15600nX A09;
    public C20710wC A0A;
    public C20660w7 A0B;
    public AnonymousClass12F A0C;
    public AnonymousClass198 A0D;
    public C254219i A0E;
    public C14860mA A0F;
    public Runnable A0G;
    public boolean A0H;

    public CommunityMembersActivity() {
        this(0);
    }

    public CommunityMembersActivity(int i) {
        this.A0H = false;
        ActivityC13830kP.A1P(this, 42);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (C49002It) A1L.A0b.get();
            this.A02 = (C237913a) A1M.ACt.get();
            this.A05 = C12960it.A0P(A1M);
            this.A06 = C12970iu.A0W(A1M);
            this.A0C = C12990iw.A0f(A1M);
            this.A0E = (C254219i) A1M.A0K.get();
            this.A0D = (AnonymousClass198) A1M.A0J.get();
            this.A07 = (C20730wE) A1M.A4J.get();
            this.A09 = C12980iv.A0d(A1M);
            this.A0A = C12980iv.A0e(A1M);
            this.A08 = (C21320xE) A1M.A4Y.get();
            this.A0B = C12990iw.A0d(A1M);
            this.A0F = (C14860mA) A1M.ANU.get();
            this.A04 = C12960it.A0O(A1M);
            this.A00 = (AnonymousClass2IH) A1L.A0o.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10) {
            this.A07.A06();
            this.A0D.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_community_view_members);
        A1e(ActivityC13790kL.A0Q(this));
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0P(true);
        A0N.A0M(true);
        A0N.A0A(R.string.members_title);
        AnonymousClass1J1 A04 = this.A06.A04(this, "community-view-members");
        RecyclerView recyclerView = (RecyclerView) AnonymousClass00T.A05(this, R.id.community_members_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(1);
        recyclerView.setLayoutManager(linearLayoutManager);
        C15580nU A0U = ActivityC13790kL.A0U(getIntent(), "extra_community_jid");
        AnonymousClass3EZ A00 = this.A00.A00(this, A0U, 2);
        this.A03 = A00;
        C237913a r13 = this.A02;
        C15570nT r6 = ((ActivityC13790kL) this).A01;
        C15610nY r10 = this.A05;
        C54442gj r11 = new C54442gj(r6, r13, new AnonymousClass3DP(((ActivityC13810kN) this).A05, r6, A00, this, this.A04, r10, this.A0D, this.A0E), r10, A04, A0U, this.A0C);
        r11.A07(true);
        r11.A00 = new AnonymousClass024() { // from class: X.4rZ
            @Override // X.AnonymousClass024
            public final void accept(Object obj) {
                CommunityMembersActivity communityMembersActivity = CommunityMembersActivity.this;
                communityMembersActivity.A2K(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: INVOKE  
                      (r0v0 'communityMembersActivity' com.whatsapp.community.CommunityMembersActivity)
                      (wrap: X.3U8 : 0x0012: CONSTRUCTOR  (r1v0 X.3U8 A[REMOVE]) = 
                      (r0v0 'communityMembersActivity' com.whatsapp.community.CommunityMembersActivity)
                      (wrap: X.3FF : 0x0002: CHECK_CAST (r7v1 X.3FF A[REMOVE]) = (X.3FF) (r7v0 'obj' java.lang.Object))
                     call: X.3U8.<init>(com.whatsapp.community.CommunityMembersActivity, X.3FF):void type: CONSTRUCTOR)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.cadmin_make_admin_dialog_title int)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.cadmin_make_admin_rank_mismatch_dialog_text int)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.group_community_management_try_again_label int)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.cancel int)
                     type: VIRTUAL call: X.0kN.A2K(X.2GV, int, int, int, int):void in method: X.4rZ.accept(java.lang.Object):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0012: CONSTRUCTOR  (r1v0 X.3U8 A[REMOVE]) = 
                      (r0v0 'communityMembersActivity' com.whatsapp.community.CommunityMembersActivity)
                      (wrap: X.3FF : 0x0002: CHECK_CAST (r7v1 X.3FF A[REMOVE]) = (X.3FF) (r7v0 'obj' java.lang.Object))
                     call: X.3U8.<init>(com.whatsapp.community.CommunityMembersActivity, X.3FF):void type: CONSTRUCTOR in method: X.4rZ.accept(java.lang.Object):void, file: classes3.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3U8, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    com.whatsapp.community.CommunityMembersActivity r0 = com.whatsapp.community.CommunityMembersActivity.this
                    X.3FF r7 = (X.AnonymousClass3FF) r7
                    r2 = 2131886857(0x7f120309, float:1.9408305E38)
                    r3 = 2131886858(0x7f12030a, float:1.9408307E38)
                    r4 = 2131888659(0x7f120a13, float:1.941196E38)
                    r5 = 2131886925(0x7f12034d, float:1.9408443E38)
                    X.3U8 r1 = new X.3U8
                    r1.<init>(r0, r7)
                    r0.A2K(r1, r2, r3, r4, r5)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C103944rZ.accept(java.lang.Object):void");
            }
        };
        recyclerView.setAdapter(r11);
        C36691kN r2 = (C36691kN) new AnonymousClass02A(new AnonymousClass3RY(this.A01, A0U), this).A00(C36691kN.class);
        r2.A0D.A05(this, new AnonymousClass02B(r11, this) { // from class: X.3RF
            public final /* synthetic */ C54442gj A00;
            public final /* synthetic */ CommunityMembersActivity A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                CommunityMembersActivity communityMembersActivity = this.A01;
                C54442gj r22 = this.A00;
                AnonymousClass1JO r62 = (AnonymousClass1JO) obj;
                if (r22.A02.A03 == 0) {
                    r22.A0E(r62);
                    return;
                }
                ((ActivityC13810kN) communityMembersActivity).A05.A0G(communityMembersActivity.A0G);
                RunnableBRunnable0Shape11S0200000_I1_1 runnableBRunnable0Shape11S0200000_I1_1 = new RunnableBRunnable0Shape11S0200000_I1_1(r22, 3, r62);
                communityMembersActivity.A0G = runnableBRunnable0Shape11S0200000_I1_1;
                ((ActivityC13810kN) communityMembersActivity).A05.A0J(runnableBRunnable0Shape11S0200000_I1_1, 500);
            }
        });
        r2.A00.A05(this, new AnonymousClass02B(r11) { // from class: X.3RE
            public final /* synthetic */ C54442gj A01;

            {
                this.A01 = r2;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
                if (r12.A00() == false) goto L_0x0024;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
                if (r1 == false) goto L_0x0011;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r12) {
                /*
                // Method dump skipped, instructions count: 281
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3RE.ANq(java.lang.Object):void");
            }
        });
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        Runnable runnable = this.A0G;
        if (runnable != null) {
            ((ActivityC13810kN) this).A05.A0G(runnable);
        }
    }
}
