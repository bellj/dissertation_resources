package com.whatsapp.community;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass3WF;
import X.C15370n3;
import X.C42941w9;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* loaded from: classes2.dex */
public class SubgroupPileView extends LinearLayout implements AnonymousClass004 {
    public ThumbnailButton A00;
    public ThumbnailButton A01;
    public ThumbnailButton A02;
    public AnonymousClass130 A03;
    public AnonymousClass018 A04;
    public AnonymousClass2P7 A05;
    public boolean A06;

    public SubgroupPileView(Context context) {
        this(context, null);
    }

    public SubgroupPileView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SubgroupPileView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A04 = (AnonymousClass018) r1.ANb.get();
            this.A03 = (AnonymousClass130) r1.A41.get();
        }
        LayoutInflater.from(context).inflate(R.layout.subgroup_pile_view, (ViewGroup) this, true);
        setGravity(1);
        this.A02 = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.subgroup_pile_top_profile_photo);
        this.A01 = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.subgroup_pile_middle_face);
        this.A00 = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.subgroup_pile_bottom_face);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0M);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, getResources().getDimensionPixelSize(R.dimen.space_xxLoose));
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(2, getResources().getDimensionPixelOffset(R.dimen.space_tight_halfStep));
            int color = obtainStyledAttributes.getColor(0, -1);
            obtainStyledAttributes.recycle();
            this.A01.setLayoutParams(new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
            this.A00.setLayoutParams(new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
            this.A02.setLayoutParams(new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
            int min = Math.min(0, dimensionPixelSize2 - dimensionPixelSize);
            C42941w9.A07(this.A00, this.A04, 0, min);
            C42941w9.A07(this.A01, this.A04, 0, min);
            if (color != -1) {
                this.A02.A03 = color;
                this.A01.A03 = color;
                this.A00.A03 = color;
            }
            Drawable A04 = AnonymousClass00T.A04(context, R.drawable.subgroup_facepile_circle_bottom);
            AnonymousClass009.A05(A04);
            this.A00.setImageDrawable(A04);
            Drawable A042 = AnonymousClass00T.A04(context, R.drawable.subgroup_facepile_circle_middle);
            AnonymousClass009.A05(A042);
            this.A01.setImageDrawable(A042);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r3 == 3) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.C19990v2 r3, X.C15600nX r4, X.C15580nU r5) {
        /*
            int r3 = r3.A02(r5)
            r2 = 0
            r0 = 2
            if (r3 == r0) goto L_0x000c
            r0 = 3
            r1 = 0
            if (r3 != r0) goto L_0x000d
        L_0x000c:
            r1 = 1
        L_0x000d:
            if (r5 == 0) goto L_0x0018
            boolean r0 = r4.A0C(r5)
            if (r0 == 0) goto L_0x0018
            if (r1 == 0) goto L_0x0018
            r2 = 1
        L_0x0018:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.SubgroupPileView.A00(X.0v2, X.0nX, X.0nU):boolean");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    public View getTransitionView() {
        return this.A02;
    }

    public void setSubgroupProfilePhoto(C15370n3 r4, AnonymousClass1J1 r5) {
        r5.A02(this.A02, new AnonymousClass3WF(this, r4), r4, false);
    }
}
