package com.whatsapp.community;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10T;
import X.AnonymousClass10V;
import X.AnonymousClass10Z;
import X.AnonymousClass11F;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1BF;
import X.AnonymousClass1E8;
import X.AnonymousClass1J1;
import X.AnonymousClass1JV;
import X.AnonymousClass2A9;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2VD;
import X.AnonymousClass3ZP;
import X.AnonymousClass4KD;
import X.AnonymousClass51M;
import X.C102924pv;
import X.C1095252a;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18770sz;
import X.C18810t5;
import X.C19990v2;
import X.C20710wC;
import X.C20830wO;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22640zP;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27151Gf;
import X.C29951Vj;
import X.C33151dW;
import X.C42801vt;
import X.C50912Rv;
import X.C54262gR;
import X.C623736x;
import X.C63323Bd;
import X.C68923Xg;
import X.C92204Uy;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.community.AddGroupsToCommunityActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes2.dex */
public class AddGroupsToCommunityActivity extends ActivityC13790kL {
    public AbstractC005102i A00;
    public AddGroupsToCommunityViewModel A01;
    public C22640zP A02;
    public C54262gR A03;
    public AnonymousClass1BF A04;
    public C15550nR A05;
    public C15610nY A06;
    public AnonymousClass10T A07;
    public AnonymousClass10V A08;
    public AnonymousClass1J1 A09;
    public C21270x9 A0A;
    public AnonymousClass018 A0B;
    public C19990v2 A0C;
    public C20830wO A0D;
    public C21320xE A0E;
    public C15600nX A0F;
    public C18770sz A0G;
    public AnonymousClass11F A0H;
    public C22050yP A0I;
    public C20710wC A0J;
    public AnonymousClass1E8 A0K;
    public C17220qS A0L;
    public AnonymousClass10Z A0M;
    public AnonymousClass12F A0N;
    public boolean A0O;
    public final C27151Gf A0P;
    public final AtomicReference A0Q;

    public AddGroupsToCommunityActivity() {
        this(0);
        this.A0Q = new AtomicReference();
        this.A0P = new C33151dW(this);
    }

    public AddGroupsToCommunityActivity(int i) {
        this.A0O = false;
        A0R(new C102924pv(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0O) {
            this.A0O = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0C = (C19990v2) r1.A3M.get();
            this.A0A = (C21270x9) r1.A4A.get();
            this.A0L = (C17220qS) r1.ABt.get();
            this.A05 = (C15550nR) r1.A45.get();
            this.A06 = (C15610nY) r1.AMe.get();
            this.A0B = (AnonymousClass018) r1.ANb.get();
            this.A0J = (C20710wC) r1.A8m.get();
            this.A0N = (AnonymousClass12F) r1.AJM.get();
            this.A0I = (C22050yP) r1.A7v.get();
            this.A0G = (C18770sz) r1.AM8.get();
            this.A07 = (AnonymousClass10T) r1.A47.get();
            this.A0H = (AnonymousClass11F) r1.AE3.get();
            this.A08 = (AnonymousClass10V) r1.A48.get();
            this.A0M = (AnonymousClass10Z) r1.AGM.get();
            this.A02 = (C22640zP) r1.A3Z.get();
            this.A04 = (AnonymousClass1BF) r1.A3b.get();
            this.A0K = (AnonymousClass1E8) r1.ADy.get();
            this.A0E = (C21320xE) r1.A4Y.get();
            this.A0F = (C15600nX) r1.A8x.get();
            this.A0D = (C20830wO) r1.A4W.get();
        }
    }

    public final void A2e() {
        if (C50912Rv.filter((Collection) this.A01.A07.A01(), new AnonymousClass51M(false)).size() == 0) {
            Toast.makeText(this, (int) R.string.create_community_requires_more_than_one_subgroup, 0).show();
        } else if (!((ActivityC13810kN) this).A07.A0B()) {
            A2K(new C1095252a(this), 0, R.string.create_community_failed_to_be_created, R.string.create_community_failed_try_again, R.string.create_community_failed_cancel);
        } else {
            A2C(R.string.creating_community);
            String stringExtra = getIntent().getStringExtra("extra_community_name");
            AnonymousClass009.A05(stringExtra);
            String stringExtra2 = getIntent().getStringExtra("extra_community_description");
            C14830m7 r0 = ((ActivityC13790kL) this).A05;
            C14850m9 r02 = ((ActivityC13810kN) this).A0C;
            C14900mE r15 = ((ActivityC13810kN) this).A05;
            AbstractC15710nm r14 = ((ActivityC13810kN) this).A03;
            C15570nT r13 = ((ActivityC13790kL) this).A01;
            C17220qS r12 = this.A0L;
            C15550nR r11 = this.A05;
            C20710wC r10 = this.A0J;
            C22050yP r8 = this.A0I;
            AnonymousClass10T r3 = this.A07;
            AnonymousClass10Z r2 = this.A0M;
            AnonymousClass2A9 r4 = new AnonymousClass2A9(this, r14, r15, r13, new C92204Uy(this), r11, r3, r0, r02, r8, r10, this.A0K, r12, r2);
            C14830m7 r102 = r4.A09;
            r4.A00 = r102.A00();
            C20710wC r9 = r4.A0C;
            AnonymousClass1JV A07 = r9.A07();
            C14850m9 r82 = r4.A0A;
            new AnonymousClass3ZP(r4.A04, r4.A06, r102, r82, r9, new C68923Xg((Uri) getIntent().getParcelableExtra("extra_community_photo_uri"), r4, (Set) this.A01.A08.A01(), (Set) this.A01.A09.A01()), new C63323Bd(null, A07, null, stringExtra, stringExtra2, new ArrayList(), 0, true), r4.A0E).A00();
        }
    }

    public final void A2f() {
        if (((Set) this.A01.A07.A01()).size() < this.A02.A0G.A02(1238) + 1) {
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.group.NewGroup");
            intent.putExtra("create_group_for_community", true);
            A2E(intent, 11);
            return;
        }
        int A02 = this.A02.A0G.A02(1238);
        A2Q("", getResources().getQuantityString(R.plurals.link_group_max_limit, A02, Integer.valueOf(A02)));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Bundle bundle;
        if (i != 10) {
            if (i != 11) {
                super.onActivityResult(i, i2, intent);
            } else if (i2 == -1 && intent != null && intent.getExtras() != null && (bundle = (Bundle) intent.getExtras().getParcelable("group_created")) != null) {
                AddGroupsToCommunityViewModel addGroupsToCommunityViewModel = this.A01;
                AnonymousClass2VD A00 = AnonymousClass2VD.A00(bundle);
                C15370n3 r4 = new C15370n3(A00.A02);
                r4.A0K = A00.A03;
                r4.A01 = A00.A00;
                Uri uri = A00.A01;
                if (uri != null) {
                    File file = new File(uri.getPath());
                    if (file.exists()) {
                        try {
                            C42801vt A01 = addGroupsToCommunityViewModel.A06.A01(file);
                            addGroupsToCommunityViewModel.A03.A01(r4, A01.A00, A01.A01);
                        } catch (IOException e) {
                            Log.e("newgroup/failed to update photo", e);
                        }
                    }
                }
                Set set = addGroupsToCommunityViewModel.A0B;
                if (set.add(r4)) {
                    addGroupsToCommunityViewModel.A09.A0A(set);
                    addGroupsToCommunityViewModel.A04();
                }
            }
        } else if (i2 == -1) {
            if (intent != null && intent.getExtras() != null) {
                if (intent.getExtras().getBoolean("should_open_new_group")) {
                    Intent intent2 = new Intent();
                    intent2.setClassName(getPackageName(), "com.whatsapp.group.NewGroup");
                    intent2.putExtra("create_group_for_community", true);
                    A2E(intent2, 11);
                    return;
                }
                ArrayList<String> stringArrayList = intent.getExtras().getStringArrayList("selected_jids");
                if (stringArrayList != null) {
                    AddGroupsToCommunityViewModel addGroupsToCommunityViewModel2 = this.A01;
                    addGroupsToCommunityViewModel2.A0A.Aaz(new C623736x(addGroupsToCommunityViewModel2, new HashSet(stringArrayList)), new String[0]);
                }
            }
        } else if (i2 == -10) {
            Ado(R.string.no_groups_to_link_error);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass1BF r2 = this.A04;
        int size = ((Set) this.A01.A08.A01()).size();
        int size2 = ((Set) this.A01.A09.A01()).size();
        r2.A00 = size;
        r2.A01 = size2;
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A04.A03 = true;
        setContentView(R.layout.add_groups_to_parent_group);
        this.A09 = this.A0A.A04(this, "add-groups-to-community");
        this.A01 = (AddGroupsToCommunityViewModel) new AnonymousClass02A(this).A00(AddGroupsToCommunityViewModel.class);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        this.A00 = A1U;
        this.A0E.A03(this.A0P);
        this.A00.A0P(true);
        this.A00.A0M(true);
        this.A00.A0A(R.string.add_groups);
        AnonymousClass00T.A05(this, R.id.add_groups_new_group).setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 37));
        AnonymousClass00T.A05(this, R.id.add_groups_link_existing_groups).setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 38));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.added_groups);
        recyclerView.setLayoutManager(new LinearLayoutManager());
        C15610nY r7 = this.A06;
        AnonymousClass12F r11 = this.A0N;
        AnonymousClass11F r10 = this.A0H;
        C54262gR r4 = new C54262gR(this, new AnonymousClass4KD(this), r7, this.A09, this.A0F, r10, r11);
        this.A03 = r4;
        recyclerView.setAdapter(r4);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.community_add_groups_done_button);
        imageView.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 39));
        imageView.setImageDrawable(AnonymousClass00T.A04(this, R.drawable.ic_fab_check));
        AddGroupsToCommunityViewModel addGroupsToCommunityViewModel = this.A01;
        String stringExtra = getIntent().getStringExtra("extra_community_name");
        AnonymousClass009.A05(stringExtra);
        C15370n3 r3 = new C15370n3(addGroupsToCommunityViewModel.A05.A07());
        r3.A0K = stringExtra;
        r3.A0F = new C29951Vj(null, 3);
        addGroupsToCommunityViewModel.A00 = r3;
        addGroupsToCommunityViewModel.A04();
        this.A01.A07.A05(this, new AnonymousClass02B() { // from class: X.3Q7
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AddGroupsToCommunityActivity addGroupsToCommunityActivity = AddGroupsToCommunityActivity.this;
                AbstractC005102i r6 = addGroupsToCommunityActivity.A00;
                Resources resources = addGroupsToCommunityActivity.getResources();
                int A02 = addGroupsToCommunityActivity.A02.A0G.A02(1238) + 1;
                Object[] A1a = C12980iv.A1a();
                C12960it.A1O(A1a, C50912Rv.filter((Collection) addGroupsToCommunityActivity.A01.A07.A01(), new AnonymousClass51M(true)).size());
                C12960it.A1P(A1a, addGroupsToCommunityActivity.A02.A0G.A02(1238) + 1, 1);
                r6.A0H(resources.getQuantityString(R.plurals.n_of_m_for_community, A02, A1a));
                addGroupsToCommunityActivity.A03.A01.A05((Collection) obj);
            }
        });
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        AnonymousClass1J1 r0 = this.A09;
        if (r0 != null) {
            r0.A00();
            this.A09 = null;
        }
        this.A0E.A04(this.A0P);
        super.onDestroy();
    }
}
