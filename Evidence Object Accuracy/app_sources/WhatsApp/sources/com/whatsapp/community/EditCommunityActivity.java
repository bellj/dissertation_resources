package com.whatsapp.community;

import X.AbstractActivityC37221lk;
import X.AbstractC001200n;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10V;
import X.AnonymousClass10Z;
import X.AnonymousClass11F;
import X.AnonymousClass12P;
import X.AnonymousClass131;
import X.AnonymousClass19M;
import X.AnonymousClass1BF;
import X.AnonymousClass1J1;
import X.AnonymousClass1PD;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C102944px;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C37131lX;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.jid.GroupJid;

/* loaded from: classes2.dex */
public class EditCommunityActivity extends AbstractActivityC37221lk {
    public C15550nR A00;
    public AnonymousClass10S A01;
    public C15610nY A02;
    public AnonymousClass10V A03;
    public AnonymousClass1J1 A04;
    public C21270x9 A05;
    public C15370n3 A06;
    public GroupJid A07;
    public boolean A08;
    public final C27131Gd A09;

    public EditCommunityActivity() {
        this(0);
        this.A09 = new C37131lX(this);
    }

    public EditCommunityActivity(int i) {
        this.A08 = false;
        A0R(new C102944px(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC37221lk) this).A07 = (C22050yP) r1.A7v.get();
            ((AbstractActivityC37221lk) this).A04 = (AnonymousClass10T) r1.A47.get();
            ((AbstractActivityC37221lk) this).A06 = (AnonymousClass11F) r1.AE3.get();
            ((AbstractActivityC37221lk) this).A09 = (AnonymousClass10Z) r1.AGM.get();
            ((AbstractActivityC37221lk) this).A03 = (AnonymousClass1BF) r1.A3b.get();
            ((AbstractActivityC37221lk) this).A08 = (C16630pM) r1.AIc.get();
            ((AbstractActivityC37221lk) this).A05 = (AnonymousClass131) r1.A49.get();
            this.A05 = (C21270x9) r1.A4A.get();
            this.A00 = (C15550nR) r1.A45.get();
            this.A02 = (C15610nY) r1.AMe.get();
            this.A01 = (AnonymousClass10S) r1.A46.get();
            this.A03 = (AnonymousClass10V) r1.A48.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 64206) {
            if (i != 16436755) {
                super.onActivityResult(i, i2, intent);
                return;
            }
            ((AbstractActivityC37221lk) this).A09.A01.A0M("tmpi").delete();
            if (i2 != -1) {
                if (i2 == 0 && intent != null) {
                    ((AbstractActivityC37221lk) this).A09.A03(intent, this);
                    return;
                }
                return;
            }
        } else if (i2 == -1) {
            if (intent != null) {
                if (intent.getBooleanExtra("is_reset", false)) {
                    this.A01.A06(this.A07);
                    ((AbstractActivityC37221lk) this).A09.A08(this.A06);
                    return;
                } else if (intent.getBooleanExtra("skip_cropping", false)) {
                    ((AbstractActivityC37221lk) this).A09.A01.A0M("tmpi").delete();
                }
            }
            ((AbstractActivityC37221lk) this).A09.A04(intent, this, 16436755);
            return;
        } else {
            return;
        }
        this.A01.A06(this.A07);
        ((AbstractActivityC37221lk) this).A09.A0A(this.A06);
    }

    @Override // X.AbstractActivityC37221lk, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AnonymousClass00T.A05(this, R.id.name_counter).setVisibility(8);
        AnonymousClass1J1 A04 = this.A05.A04(this, "community-home");
        ((ActivityC001000l) this).A06.A00(new AnonymousClass054(A04) { // from class: com.whatsapp.contact.photos.ContactPhotos$LoaderLifecycleEventObserver
            public final AnonymousClass1J1 A00;

            {
                this.A00 = r1;
            }

            @Override // X.AnonymousClass054
            public void AWQ(AnonymousClass074 r2, AbstractC001200n r3) {
                if (r2 == AnonymousClass074.ON_DESTROY) {
                    this.A00.A00();
                    r3.ADr().A01(this);
                }
            }
        });
        this.A04 = A04;
        this.A01.A03(this.A09);
        C15580nU A042 = C15580nU.A04(getIntent().getStringExtra("extra_community_jid"));
        AnonymousClass009.A05(A042);
        this.A07 = A042;
        this.A06 = this.A00.A0B(A042);
        ((AbstractActivityC37221lk) this).A02.setEnabled(false);
        ((AbstractActivityC37221lk) this).A02.setText(this.A02.A04(this.A06));
        WaEditText waEditText = ((AbstractActivityC37221lk) this).A01;
        AnonymousClass1PD r0 = this.A06.A0G;
        AnonymousClass009.A05(r0);
        waEditText.setText(r0.A02);
        this.A04.A06(((AbstractActivityC37221lk) this).A00, this.A06);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A01.A04(this.A09);
    }
}
