package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C12980iv;
import X.C15600nX;
import X.C22640zP;

/* loaded from: classes2.dex */
public class ConversationCommunityViewModel extends AnonymousClass015 {
    public Boolean A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final C22640zP A03;
    public final C15600nX A04;
    public final AbstractC14440lR A05;

    public ConversationCommunityViewModel(C22640zP r2, C15600nX r3, AbstractC14440lR r4) {
        this.A05 = r4;
        this.A03 = r2;
        this.A04 = r3;
    }
}
