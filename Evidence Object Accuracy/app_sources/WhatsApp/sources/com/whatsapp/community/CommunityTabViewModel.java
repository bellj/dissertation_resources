package com.whatsapp.community;

import X.AbstractC14440lR;
import X.AbstractC15340mz;
import X.AbstractC18860tB;
import X.AbstractC33041dC;
import X.AbstractC33331dp;
import X.AbstractC40571ro;
import X.AbstractC469528i;
import X.AnonymousClass009;
import X.AnonymousClass015;
import X.AnonymousClass03H;
import X.AnonymousClass10Y;
import X.AnonymousClass12H;
import X.AnonymousClass1E5;
import X.AnonymousClass1E8;
import X.AnonymousClass1EC;
import X.AnonymousClass1OU;
import X.AnonymousClass1PE;
import X.AnonymousClass428;
import X.AnonymousClass42W;
import X.AnonymousClass4Ww;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15680nj;
import X.C19990v2;
import X.C21320xE;
import X.C22640zP;
import X.C244215l;
import X.C27151Gf;
import X.C27691It;
import X.C32961d1;
import X.C36161jQ;
import X.C469428h;
import X.C859144y;
import X.ExecutorC27271Gr;
import com.facebook.redex.RunnableBRunnable0Shape0S0700000_I0;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.jid.GroupJid;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class CommunityTabViewModel extends AnonymousClass015 implements AbstractC33041dC, AnonymousClass03H {
    public static final Comparator A0S = new Comparator() { // from class: X.5CC
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            AnonymousClass1PE r7 = (AnonymousClass1PE) obj;
            AnonymousClass1PE r8 = (AnonymousClass1PE) obj2;
            int min = (int) (Math.min((long) r8.A06, 1L) - Math.min((long) r7.A06, 1L));
            if (min != 0) {
                return min;
            }
            int i = (r8.A02() > r7.A02() ? 1 : (r8.A02() == r7.A02() ? 0 : -1));
            if (i == 0) {
                return CommunityTabViewModel.A00(r7, r8);
            }
            return i;
        }
    };
    public AnonymousClass1PE A00;
    public AbstractC18860tB A01 = new AnonymousClass42W(this);
    public AbstractC33331dp A02 = new C469428h(this);
    public AbstractC469528i A03 = new C859144y(this);
    public Map A04 = new LinkedHashMap();
    public Map A05 = new HashMap();
    public Set A06 = new HashSet();
    public final C15570nT A07;
    public final C22640zP A08;
    public final AbstractC40571ro A09 = new AnonymousClass428(this);
    public final AnonymousClass1EC A0A;
    public final C19990v2 A0B;
    public final C27151Gf A0C = new C32961d1(this);
    public final C21320xE A0D;
    public final C15680nj A0E;
    public final C15600nX A0F;
    public final AnonymousClass10Y A0G;
    public final AnonymousClass12H A0H;
    public final C14850m9 A0I;
    public final C244215l A0J;
    public final AnonymousClass1E8 A0K;
    public final AnonymousClass1E5 A0L;
    public final C36161jQ A0M;
    public final C36161jQ A0N;
    public final ExecutorC27271Gr A0O;
    public final C27691It A0P = new C27691It();
    public final C27691It A0Q = new C27691It();
    public final Comparator A0R = new Comparator() { // from class: X.3cb
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            AnonymousClass1PE r6;
            AnonymousClass1PE r10 = (AnonymousClass1PE) obj;
            AnonymousClass1PE r11 = (AnonymousClass1PE) obj2;
            Map map = CommunityTabViewModel.this.A04;
            List list = (List) map.get(r10);
            List list2 = (List) map.get(r11);
            AnonymousClass1PE r7 = null;
            if (list == null || list.isEmpty()) {
                r6 = null;
            } else {
                r6 = (AnonymousClass1PE) list.get(0);
            }
            if (list2 != null && !list2.isEmpty()) {
                r7 = (AnonymousClass1PE) list2.get(0);
            }
            if (r6 == null) {
                if (r7 == null) {
                    return (int) (r11.A02() - r10.A02());
                }
                return -1;
            } else if (r7 == null) {
                return 1;
            } else {
                int min = (int) (Math.min((long) r7.A06, 1L) - Math.min((long) r6.A06, 1L));
                if (min != 0) {
                    return min;
                }
                int i = (r7.A02() > r6.A02() ? 1 : (r7.A02() == r6.A02() ? 0 : -1));
                if (i == 0) {
                    return CommunityTabViewModel.A00(r10, r11);
                }
                return i;
            }
        }
    };

    public CommunityTabViewModel(C14900mE r15, C15570nT r16, C22640zP r17, AnonymousClass1EC r18, C19990v2 r19, C21320xE r20, C15680nj r21, C15600nX r22, AnonymousClass10Y r23, AnonymousClass12H r24, C14850m9 r25, C244215l r26, AnonymousClass1E8 r27, AnonymousClass1E5 r28, AbstractC14440lR r29) {
        this.A0I = r25;
        this.A07 = r16;
        this.A0B = r19;
        ExecutorC27271Gr r3 = new ExecutorC27271Gr(r29, false);
        this.A0O = r3;
        this.A0G = r23;
        this.A0H = r24;
        this.A0L = r28;
        this.A0A = r18;
        this.A08 = r17;
        this.A0E = r21;
        this.A0K = r27;
        this.A0D = r20;
        this.A0F = r22;
        this.A0J = r26;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass4Ww(10, null));
        this.A0N = new C36161jQ(new ArrayList(arrayList));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new AnonymousClass4Ww(10, null));
        this.A0M = new C36161jQ(new ArrayList(arrayList2));
        r3.execute(new RunnableBRunnable0Shape0S0700000_I0(this, r15, r18, r20, r27, r26, r24, 0));
    }

    public static int A00(AnonymousClass1PE r2, AnonymousClass1PE r3) {
        String A06 = r2.A06();
        String A062 = r3.A06();
        if (A06 == null) {
            if (A062 != null) {
                return 1;
            }
        } else if (A062 == null) {
            return -1;
        } else {
            int compareTo = A06.compareTo(A062);
            if (compareTo != 0) {
                return compareTo;
            }
        }
        if (r2.A05() == null && r3.A05() == null) {
            return 0;
        }
        if (r2.A05() == null) {
            return -1;
        }
        return r2.A05().getRawString().compareTo(r3.A05().getRawString());
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0A.A04(this.A09);
        this.A0D.A04(this.A0C);
        this.A0K.A04(this.A03);
        this.A0H.A04(this.A01);
        this.A0J.A04(this.A02);
    }

    public final List A04(AnonymousClass1PE r5) {
        List<AnonymousClass1PE> list = (List) this.A04.remove(r5);
        if (list != null) {
            for (AnonymousClass1PE r0 : list) {
                this.A05.remove(GroupJid.of(r0.A05()));
            }
        }
        return list;
    }

    public final List A05(AnonymousClass1PE r6, Map map) {
        List<AnonymousClass1OU> A01 = this.A08.A01(C15580nU.A02(r6.A05()));
        if (!A01.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AnonymousClass1OU r1 : A01) {
                C19990v2 r0 = this.A0B;
                GroupJid groupJid = r1.A02;
                AnonymousClass1PE A06 = r0.A06(groupJid);
                if (A06 != null) {
                    map.put(groupJid, r6);
                    arrayList.add(A06);
                }
            }
            if (!arrayList.isEmpty()) {
                return arrayList;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00de, code lost:
        if (r4.isEmpty() != false) goto L_0x00e0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(boolean r14) {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.community.CommunityTabViewModel.A06(boolean):void");
    }

    @Override // X.AbstractC33041dC
    public void ALu(AbstractC15340mz r4) {
        GroupJid of;
        C27691It r0;
        if (r4 == null) {
            AnonymousClass009.A07("CommunityTabViewModel/onActivityRowTapped from a null message");
            return;
        }
        AnonymousClass1PE A06 = this.A0B.A06(r4.A0z.A00);
        if (A06 != null && (of = GroupJid.of(A06.A05())) != null) {
            this.A00 = A06;
            if (this.A0F.A0D(of)) {
                r0 = this.A0Q;
            } else {
                r0 = this.A0P;
            }
            r0.A0A(of);
        }
    }
}
