package com.whatsapp.community.deactivate;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass04S;
import X.AnonymousClass5RR;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15550nR;
import X.C15580nU;
import X.C15610nY;
import X.C16700pc;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class DeactivateCommunityConfirmationFragment extends Hilt_DeactivateCommunityConfirmationFragment {
    public AnonymousClass5RR A00;
    public C15550nR A01;
    public C15610nY A02;

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog instanceof AnonymousClass04S) {
            Button button = ((AnonymousClass04S) dialog).A00.A0G;
            C12960it.A0s(button.getContext(), button, R.color.red_error);
        }
    }

    @Override // com.whatsapp.community.deactivate.Hilt_DeactivateCommunityConfirmationFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        C16700pc.A0E(context, 0);
        super.A15(context);
        AnonymousClass009.A05(context);
        this.A00 = (AnonymousClass5RR) context;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String string = A03().getString("parent_group_jid");
        AnonymousClass009.A05(string);
        C16700pc.A0B(string);
        C15580nU A03 = C15580nU.A03(string);
        C16700pc.A0B(A03);
        C15550nR r0 = this.A01;
        if (r0 != null) {
            C15370n3 A0B = r0.A0B(A03);
            ActivityC000900k A0C = A0C();
            Object[] objArr = new Object[1];
            C15610nY r02 = this.A02;
            if (r02 != null) {
                String A0X = C12960it.A0X(A0C, r02.A04(A0B), objArr, 0, R.string.deactivate_community_confirmation_title);
                C16700pc.A0B(A0X);
                Object[] objArr2 = new Object[1];
                C15610nY r03 = this.A02;
                if (r03 != null) {
                    String A0X2 = C12960it.A0X(A0C, r03.A04(A0B), objArr2, 0, R.string.deactivate_community_confirmation_message);
                    C16700pc.A0B(A0X2);
                    C004802e A0S = C12980iv.A0S(A0C);
                    A0S.setTitle(A0X);
                    A0S.A0A(A0X2);
                    A0S.A0B(true);
                    C12970iu.A1K(A0S, this, 23, R.string.cancel);
                    C12970iu.A1M(A0S, this, 14, R.string.deactivate_community_confirmation_dialog_positive_button);
                    return A0S.create();
                }
                throw C16700pc.A06("waContactNames");
            }
            throw C16700pc.A06("waContactNames");
        }
        throw C16700pc.A06("contactManager");
    }
}
