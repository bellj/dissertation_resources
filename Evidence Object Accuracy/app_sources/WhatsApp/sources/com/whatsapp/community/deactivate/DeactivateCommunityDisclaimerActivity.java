package com.whatsapp.community.deactivate;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass2GV;
import X.AnonymousClass5RR;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C15370n3;
import X.C15550nR;
import X.C15580nU;
import X.C15610nY;
import X.C16700pc;
import X.C17220qS;
import X.C21270x9;
import X.C93004Yo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.community.deactivate.DeactivateCommunityDisclaimerActivity;

/* loaded from: classes2.dex */
public final class DeactivateCommunityDisclaimerActivity extends ActivityC13790kL implements AnonymousClass5RR {
    public View A00;
    public C15550nR A01;
    public C15610nY A02;
    public C21270x9 A03;
    public C15370n3 A04;
    public C15580nU A05;
    public C17220qS A06;
    public boolean A07;

    public DeactivateCommunityDisclaimerActivity() {
        this(0);
    }

    public DeactivateCommunityDisclaimerActivity(int i) {
        this.A07 = false;
        ActivityC13830kP.A1P(this, 47);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A03 = C12970iu.A0W(A1M);
            this.A06 = C12990iw.A0c(A1M);
            this.A01 = C12960it.A0O(A1M);
            this.A02 = C12960it.A0P(A1M);
        }
    }

    public final void A2e() {
        if (!((ActivityC13810kN) this).A07.A0B()) {
            A2K(new AnonymousClass2GV() { // from class: X.52c
                @Override // X.AnonymousClass2GV
                public final void AO1() {
                    DeactivateCommunityDisclaimerActivity deactivateCommunityDisclaimerActivity = DeactivateCommunityDisclaimerActivity.this;
                    C16700pc.A0E(deactivateCommunityDisclaimerActivity, 0);
                    deactivateCommunityDisclaimerActivity.A2e();
                }
            }, 0, R.string.deactivate_community_failed_to_be_deactivated, R.string.deactivate_community_failed_try_again, R.string.deactivate_community_failed_cancel);
            return;
        }
        C15580nU r0 = this.A05;
        if (r0 == null) {
            throw C16700pc.A06("parentGroupJid");
        }
        DeactivateCommunityConfirmationFragment deactivateCommunityConfirmationFragment = new DeactivateCommunityConfirmationFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("parent_group_jid", r0.getRawString());
        deactivateCommunityConfirmationFragment.A0U(A0D);
        Adl(deactivateCommunityConfirmationFragment, "DeactivateCommunityDisclaimerActivity");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_community_deactivate_disclaimer);
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A0Q.setTitle(R.string.deactivate_community);
        A1e(A0Q);
        C12970iu.A0N(this).A0M(true);
        C15580nU A03 = C15580nU.A03(getIntent().getStringExtra("parent_group_jid"));
        C16700pc.A0B(A03);
        this.A05 = A03;
        C15550nR r0 = this.A01;
        if (r0 != null) {
            this.A04 = r0.A0B(A03);
            this.A00 = C16700pc.A00(this, R.id.deactivate_community_main_view);
            ImageView imageView = (ImageView) C16700pc.A00(this, R.id.deactivate_community_disclaimer_photo_view);
            C21270x9 r1 = this.A03;
            if (r1 != null) {
                AnonymousClass1J1 A04 = r1.A04(this, "deactivate-community-disclaimer");
                C15370n3 r02 = this.A04;
                if (r02 == null) {
                    throw C16700pc.A06("parentGroupContact");
                }
                A04.A06(imageView, r02);
                C12960it.A0y(AnonymousClass00T.A05(this, R.id.community_deactivate_disclaimer_continue_button), this, 15);
                TextView A0N = C12990iw.A0N(this, R.id.deactivate_community_disclaimer_title);
                Object[] objArr = new Object[1];
                C15610nY r12 = this.A02;
                if (r12 != null) {
                    C15370n3 r03 = this.A04;
                    if (r03 == null) {
                        throw C16700pc.A06("parentGroupContact");
                    }
                    A0N.setText(C12960it.A0X(this, r12.A04(r03), objArr, 0, R.string.deactivate_community_disclaimer_title_prompt));
                    C93004Yo.A00(C16700pc.A00(this, R.id.community_deactivate_disclaimer_continue_button_container), (ScrollView) C16700pc.A00(this, R.id.deactivate_community_disclaimer_scrollview));
                    return;
                }
                throw C16700pc.A06("waContactNames");
            }
            throw C16700pc.A06("contactPhotos");
        }
        throw C16700pc.A06("contactManager");
    }
}
