package com.whatsapp.community.deactivate;

import X.AnonymousClass009;
import X.AnonymousClass4GA;
import X.C015007d;
import X.C12970iu;
import X.C16700pc;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class DeactivateCommunityInfoRow extends LinearLayout {
    public ImageView A00;
    public ImageView A01;
    public TextView A02;
    public TextView A03;
    public boolean A04;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public DeactivateCommunityInfoRow(Context context) {
        this(context, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DeactivateCommunityInfoRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C16700pc.A0E(context, 1);
        A01(context, attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DeactivateCommunityInfoRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C16700pc.A0E(context, 1);
        A01(context, attributeSet);
    }

    public final void A00() {
        TextView textView;
        int i;
        boolean z = this.A04;
        ImageView imageView = this.A00;
        if (z) {
            if (imageView == null) {
                throw C16700pc.A06("rowCollapseChevronView");
            }
            imageView.setImageDrawable(C12970iu.A0C(getContext(), R.drawable.ic_chevron_up));
            textView = this.A02;
            if (textView == null) {
                throw C16700pc.A06("rowDescriptionView");
            }
            i = 0;
        } else if (imageView == null) {
            throw C16700pc.A06("rowCollapseChevronView");
        } else {
            imageView.setImageDrawable(C12970iu.A0C(getContext(), R.drawable.ic_chevron_down));
            textView = this.A02;
            if (textView == null) {
                throw C16700pc.A06("rowDescriptionView");
            }
            i = 8;
        }
        textView.setVisibility(i);
    }

    /* JADX INFO: finally extract failed */
    public final void A01(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate(R.layout.community_deactivate_disclaimer_row, this);
        this.A01 = (ImageView) C16700pc.A02(this, R.id.cdir_icon);
        this.A00 = (ImageView) C16700pc.A02(this, R.id.cdir_collapse_arrow);
        this.A03 = (TextView) C16700pc.A02(this, R.id.cdir_row_title);
        this.A02 = (TextView) C16700pc.A02(this, R.id.cdir_description);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.space_xLoose);
        setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        setBackground(C12970iu.A0C(context, R.drawable.selector_orange_gradient));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4GA.A00);
        C16700pc.A0B(obtainStyledAttributes);
        try {
            TextView textView = this.A03;
            if (textView == null) {
                throw C16700pc.A06("rowTitleView");
            }
            String string = obtainStyledAttributes.getString(2);
            AnonymousClass009.A05(string);
            textView.setText(string);
            TextView textView2 = this.A02;
            if (textView2 == null) {
                throw C16700pc.A06("rowDescriptionView");
            }
            String string2 = obtainStyledAttributes.getString(0);
            AnonymousClass009.A05(string2);
            textView2.setText(string2);
            int resourceId = obtainStyledAttributes.getResourceId(1, -1);
            if (resourceId != -1) {
                ImageView imageView = this.A01;
                if (imageView == null) {
                    throw C16700pc.A06("rowIconView");
                }
                imageView.setImageDrawable(C015007d.A01(context, resourceId));
            }
            obtainStyledAttributes.recycle();
            A00();
            setOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(this, 16));
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }
}
