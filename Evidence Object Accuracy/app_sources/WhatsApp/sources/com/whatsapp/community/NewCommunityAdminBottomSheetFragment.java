package com.whatsapp.community;

import X.AbstractC28491Nn;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass028;
import X.C12960it;
import X.C12970iu;
import X.C15580nU;
import X.C252818u;
import X.C253118x;
import X.C27531Hw;
import X.C53902fd;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape1S0000000_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class NewCommunityAdminBottomSheetFragment extends Hilt_NewCommunityAdminBottomSheetFragment {
    public static final String A03 = NewCommunityAdminBottomSheetFragment.class.getName();
    public C252818u A00;
    public C53902fd A01;
    public C253118x A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C15580nU r1 = (C15580nU) A03().getParcelable("parent_group_jid");
        if (r1 != null) {
            this.A01.A00 = r1;
            return layoutInflater.inflate(R.layout.new_community_admin_bottom_sheet, viewGroup, true);
        }
        Log.e("NewCommunityAdminBottomSheetFragment/onCreateView parent jid was null");
        A1B();
        return null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        C12960it.A18(this, this.A01.A01, 59);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C27531Hw.A06(C12960it.A0I(view, R.id.newCommunityAdminNux_title));
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.newCommunityAdminNux_description);
        AbstractC28491Nn.A03(A0T);
        String[] strArr = {this.A00.A00("https://www.whatsapp.com/communities/learning").toString()};
        A0T.setText(this.A02.A01(A01(), C12970iu.A0q(this, "learn-more", new Object[1], 0, R.string.newCommunityAdminNux_description_text), new Runnable[]{new RunnableBRunnable0Shape1S0000000_I1()}, new String[]{"learn-more"}, strArr));
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass028.A0D(view, R.id.newCommunityAdminNux_continueButton), this, 46);
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass028.A0D(view, R.id.newCommunityAdminNux_removeAsAdminButton), this, 47);
    }
}
