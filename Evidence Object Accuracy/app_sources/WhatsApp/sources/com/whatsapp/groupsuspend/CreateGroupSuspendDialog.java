package com.whatsapp.groupsuspend;

import X.ActivityC000900k;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.C004802e;
import X.C12970iu;
import X.C12980iv;
import X.C15220ml;
import X.C253118x;
import X.C52162aM;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class CreateGroupSuspendDialog extends Hilt_CreateGroupSuspendDialog {
    public AnonymousClass19Y A00;
    public C15220ml A01;
    public AnonymousClass11G A02;
    public C253118x A03;

    public static CreateGroupSuspendDialog A00(boolean z, boolean z2) {
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("isSuspendedV1Enabled", z);
        A0D.putBoolean("hasMe", z2);
        CreateGroupSuspendDialog createGroupSuspendDialog = new CreateGroupSuspendDialog();
        createGroupSuspendDialog.A0U(A0D);
        return createGroupSuspendDialog;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        TextView textView = (TextView) A19().findViewById(16908299);
        if (textView != null) {
            C52162aM.A00(textView);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        boolean z = A03().getBoolean("isSuspendedV1Enabled");
        boolean z2 = A03().getBoolean("hasMe");
        C004802e A0S = C12980iv.A0S(A0C);
        IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I1 = new IDxCListenerShape3S0200000_1_I1(A0C, 11, this);
        IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I12 = new IDxCListenerShape3S0200000_1_I1(A0C, 10, this);
        if (!z) {
            A0S.A06(R.string.group_suspend_dialog_heading);
            A0S.setNegativeButton(R.string.register_user_support_button, iDxCListenerShape3S0200000_1_I1);
            A0S.A00(R.string.learn_more, iDxCListenerShape3S0200000_1_I12);
        } else if (z2) {
            A0S.A0A(this.A03.A02(A01(), new RunnableBRunnable0Shape11S0200000_I1_1(this, 30, A0C), C12970iu.A0q(this, "learn-more", C12970iu.A1b(), 0, R.string.group_suspend_dialog_heading_v1), "learn-more"));
            A0S.setNegativeButton(R.string.register_user_support_button, iDxCListenerShape3S0200000_1_I1);
        } else {
            A0S.A06(R.string.suspended_group_error_message);
            A0S.setNegativeButton(R.string.learn_more, iDxCListenerShape3S0200000_1_I12);
        }
        A0S.setPositiveButton(R.string.group_suspend_dialog_dismiss, null);
        return A0S.create();
    }
}
