package com.whatsapp.usernotice;

import X.AnonymousClass01J;
import X.AnonymousClass2P6;
import X.AnonymousClass36P;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes2.dex */
public class UserNoticeModalIconView extends AnonymousClass36P {
    public ImageView A00;
    public boolean A01;

    public UserNoticeModalIconView(Context context) {
        super(context);
        A00();
    }

    public UserNoticeModalIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public UserNoticeModalIconView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public UserNoticeModalIconView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    @Override // X.AbstractC37211li
    public void A00() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((WaImageView) this).A00 = C12960it.A0R(A00);
            ((AnonymousClass36P) this).A01 = C12960it.A0T(A00);
        }
    }

    @Override // X.AnonymousClass36P
    public int getTargetIconSize() {
        return getResources().getDimensionPixelSize(R.dimen.user_notice_modal_server_icon_size);
    }

    public void setDefaultIconView(ImageView imageView) {
        this.A00 = imageView;
    }
}
