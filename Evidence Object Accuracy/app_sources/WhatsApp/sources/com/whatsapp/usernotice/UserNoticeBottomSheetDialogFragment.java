package com.whatsapp.usernotice;

import X.AbstractC11770gq;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass064;
import X.AnonymousClass0KS;
import X.AnonymousClass12O;
import X.AnonymousClass12S;
import X.AnonymousClass1CT;
import X.AnonymousClass2eq;
import X.AnonymousClass3I2;
import X.AnonymousClass3NT;
import X.AnonymousClass4LJ;
import X.C14900mE;
import X.C27531Hw;
import X.C42631vX;
import X.C43841xg;
import X.C43851xh;
import X.C52162aM;
import X.C72793fB;
import X.C80683sh;
import X.C92274Vf;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101724nz;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BulletSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape16S0100000_I0_3;

/* loaded from: classes2.dex */
public class UserNoticeBottomSheetDialogFragment extends Hilt_UserNoticeBottomSheetDialogFragment {
    public int A00;
    public ValueAnimator A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public LinearLayout A05;
    public TextView A06;
    public TextView A07;
    public NestedScrollView A08;
    public C14900mE A09;
    public AnonymousClass01d A0A;
    public AnonymousClass1CT A0B;
    public AnonymousClass12S A0C;
    public AnonymousClass12O A0D;
    public C43841xg A0E;
    public UserNoticeModalIconView A0F;
    public Runnable A0G;
    public final View.OnClickListener A0H = new ViewOnClickCListenerShape16S0100000_I0_3(this, 3);
    public final AbstractC11770gq A0I = new AbstractC11770gq() { // from class: X.3PP
        @Override // X.AbstractC11770gq
        public final void AVa(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
            UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = UserNoticeBottomSheetDialogFragment.this;
            userNoticeBottomSheetDialogFragment.A1O(false, false);
            userNoticeBottomSheetDialogFragment.A1M();
            Runnable runnable = userNoticeBottomSheetDialogFragment.A0G;
            if (runnable != null) {
                userNoticeBottomSheetDialogFragment.A09.A0G(runnable);
            }
            if ((userNoticeBottomSheetDialogFragment.A02.getY() - C12990iw.A03(userNoticeBottomSheetDialogFragment.A08)) - ((float) userNoticeBottomSheetDialogFragment.A08.getScrollY()) >= 0.0f) {
                RunnableBRunnable0Shape13S0100000_I0_13 runnableBRunnable0Shape13S0100000_I0_13 = new RunnableBRunnable0Shape13S0100000_I0_13(userNoticeBottomSheetDialogFragment, 10);
                userNoticeBottomSheetDialogFragment.A0G = runnableBRunnable0Shape13S0100000_I0_13;
                userNoticeBottomSheetDialogFragment.A09.A0J(runnableBRunnable0Shape13S0100000_I0_13, 600);
            }
        }
    };
    public final AnonymousClass4LJ A0J = new AnonymousClass4LJ(this);

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Log.i("UserNoticeBottomSheetDialogFragment/onCreateView");
        this.A0E = C43841xg.A00(A03());
        View inflate = layoutInflater.inflate(R.layout.user_notice_modal, viewGroup, true);
        inflate.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101724nz(inflate, this));
        this.A08 = (NestedScrollView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_scrollview);
        this.A03 = AnonymousClass028.A0D(inflate, R.id.user_notice_modal_scroll_decoration_group);
        AnonymousClass028.A0D(inflate, R.id.user_notice_modal_scroll_button).setOnClickListener(this.A0H);
        this.A08.setNestedScrollingEnabled(false);
        NestedScrollView nestedScrollView = this.A08;
        nestedScrollView.A0E = this.A0I;
        nestedScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NT(this));
        this.A02 = AnonymousClass028.A0D(inflate, R.id.user_notice_modal_button_divider);
        ImageView imageView = (ImageView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_default_icon);
        this.A04 = imageView;
        imageView.setContentDescription(((C43851xh) this.A0E).A02);
        UserNoticeModalIconView userNoticeModalIconView = (UserNoticeModalIconView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_server_icon);
        this.A0F = userNoticeModalIconView;
        userNoticeModalIconView.A00 = this.A04;
        userNoticeModalIconView.A02(this.A0E);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_body);
        textEmojiLabel.setMovementMethod(LinkMovementMethod.getInstance());
        A1N(textEmojiLabel, this.A0E.A02);
        A1N((TextEmojiLabel) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_footer), this.A0E.A04);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_title);
        this.A07 = textView;
        textView.setText(this.A0E.A07);
        AnonymousClass028.A0l(this.A07, true);
        this.A06 = (TextView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_sticky_title);
        int dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.user_notice_modal_horizontal_padding);
        int dimensionPixelSize2 = A02().getDimensionPixelSize(R.dimen.user_notice_modal_sticky_title_padding);
        this.A00 = dimensionPixelSize2;
        this.A06.setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2);
        this.A06.setMaxLines(5);
        this.A06.setEllipsize(TextUtils.TruncateAt.END);
        TextView textView2 = this.A06;
        Drawable A04 = AnonymousClass00T.A04(A01(), R.drawable.bottom_sheet_background);
        AnonymousClass009.A05(A04);
        textView2.setBackground(A04);
        this.A06.setText(this.A0E.A07);
        AnonymousClass028.A0V(this.A06, A02().getDimension(R.dimen.user_notice_modal_sticky_title_elevation));
        AnonymousClass028.A0l(this.A06, true);
        LinearLayout linearLayout = (LinearLayout) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_bullets);
        this.A05 = linearLayout;
        LayoutInflater from = LayoutInflater.from(A01());
        int dimensionPixelSize3 = A02().getDimensionPixelSize(R.dimen.user_notice_modal_bullet_span_gap);
        for (int i = 0; i < this.A0E.A08.size(); i++) {
            TextEmojiLabel textEmojiLabel2 = (TextEmojiLabel) from.inflate(R.layout.user_notice_modal_text_bullet, (ViewGroup) linearLayout, false);
            textEmojiLabel2.setTag(Integer.valueOf(i));
            linearLayout.addView(textEmojiLabel2);
            textEmojiLabel2.setMovementMethod(new C52162aM());
            textEmojiLabel2.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel2, this.A0A));
            SpannableString A00 = AnonymousClass3I2.A00(A01(), this.A0J, ((C92274Vf) this.A0E.A08.get(i)).A02);
            SpannableString spannableString = new SpannableString(A00.toString());
            spannableString.setSpan(new BulletSpan(dimensionPixelSize3), 0, A00.length(), 17);
            Object[] spans = A00.getSpans(0, A00.length(), Object.class);
            for (Object obj : spans) {
                spannableString.setSpan(obj, A00.getSpanStart(obj), A00.getSpanEnd(obj), 17);
            }
            textEmojiLabel2.setText(spannableString);
        }
        TextView textView3 = (TextView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_agree_button);
        textView3.setText(this.A0E.A01);
        textView3.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 13));
        TextView textView4 = (TextView) AnonymousClass028.A0D(inflate, R.id.user_notice_modal_dismiss_button);
        if (!TextUtils.isEmpty(this.A0E.A03)) {
            textView4.setText(this.A0E.A03);
            textView4.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 14));
        } else {
            textView4.setVisibility(8);
            AnonymousClass064 r0 = (AnonymousClass064) textView3.getLayoutParams();
            r0.A0T = 0;
            textView3.setLayoutParams(r0);
        }
        A1G(!TextUtils.isEmpty(this.A0E.A03));
        AnonymousClass12S r2 = this.A0C;
        int i2 = 7;
        if (!TextUtils.isEmpty(this.A0E.A03)) {
            i2 = 3;
        }
        r2.A01(Integer.valueOf(i2));
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A08.A0E = null;
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        super.A1L(view);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = Resources.getSystem().getDisplayMetrics().heightPixels - C27531Hw.A02(view.getContext(), AnonymousClass01d.A02(A01()));
        view.setLayoutParams(layoutParams);
        A00.A0E = new C80683sh(A00, this);
        A00.A0M(3);
    }

    public final void A1M() {
        int i = 0;
        boolean z = false;
        if (((float) this.A08.getScrollY()) > this.A07.getY() - ((float) this.A00)) {
            z = true;
        }
        TextView textView = this.A07;
        int i2 = 0;
        if (z) {
            i2 = 4;
        }
        textView.setVisibility(i2);
        TextView textView2 = this.A06;
        if (!z) {
            i = 8;
        }
        textView2.setVisibility(i);
    }

    public final void A1N(TextEmojiLabel textEmojiLabel, String str) {
        if (TextUtils.isEmpty(str)) {
            textEmojiLabel.setVisibility(8);
            return;
        }
        textEmojiLabel.setMovementMethod(new C52162aM());
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, this.A0A));
        Context A01 = A01();
        AnonymousClass009.A05(str);
        textEmojiLabel.setText(AnonymousClass3I2.A00(A01, this.A0J, str));
    }

    public final void A1O(boolean z, boolean z2) {
        long j;
        ValueAnimator valueAnimator = this.A01;
        if (valueAnimator == null) {
            ValueAnimator valueAnimator2 = new ValueAnimator();
            this.A01 = valueAnimator2;
            valueAnimator2.setInterpolator(new AccelerateDecelerateInterpolator());
            this.A01.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eX
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                    UserNoticeBottomSheetDialogFragment.this.A03.setAlpha(C12960it.A00(valueAnimator3));
                }
            });
        } else {
            valueAnimator.cancel();
            this.A01.removeAllListeners();
        }
        this.A01.addListener(new C72793fB(this, z));
        float alpha = this.A03.getAlpha();
        float f = 0.0f;
        if (z) {
            f = 1.0f;
        }
        this.A01.setFloatValues(alpha, f);
        ValueAnimator valueAnimator3 = this.A01;
        if (z2) {
            j = 400;
        } else {
            j = 0;
        }
        valueAnimator3.setDuration(j);
        this.A01.start();
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A1L(AnonymousClass0KS.A00(A19(), R.id.design_bottom_sheet));
        int dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.user_notice_modal_default_icon_size);
        C42631vX.A01(this.A04, dimensionPixelSize, dimensionPixelSize);
        int dimensionPixelSize2 = A02().getDimensionPixelSize(R.dimen.user_notice_modal_server_icon_size);
        C42631vX.A01(this.A0F, dimensionPixelSize2, dimensionPixelSize2);
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A05.getLayoutParams();
        int dimensionPixelSize3 = A02().getDimensionPixelSize(R.dimen.user_notice_modal_bullets_horizontal_margin);
        marginLayoutParams.leftMargin = dimensionPixelSize3;
        marginLayoutParams.rightMargin = dimensionPixelSize3;
        this.A05.setLayoutParams(marginLayoutParams);
        int dimensionPixelSize4 = A02().getDimensionPixelSize(R.dimen.user_notice_modal_horizontal_padding);
        NestedScrollView nestedScrollView = this.A08;
        nestedScrollView.setPadding(dimensionPixelSize4, nestedScrollView.getPaddingTop(), dimensionPixelSize4, this.A08.getPaddingBottom());
        TextView textView = this.A06;
        textView.setPadding(dimensionPixelSize4, textView.getPaddingTop(), dimensionPixelSize4, this.A06.getPaddingBottom());
        this.A08.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NT(this));
    }
}
