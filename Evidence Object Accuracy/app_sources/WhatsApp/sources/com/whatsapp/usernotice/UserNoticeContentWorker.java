package com.whatsapp.usernotice;

import X.AbstractC37631mk;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass042;
import X.AnonymousClass043;
import X.AnonymousClass0GK;
import X.AnonymousClass0GL;
import X.AnonymousClass12M;
import X.AnonymousClass12S;
import X.AnonymousClass1P1;
import X.C006403a;
import X.C006503b;
import X.C18790t3;
import X.C18800t4;
import X.C18810t5;
import X.C43841xg;
import X.C43891xl;
import X.C43911xp;
import X.C43931xr;
import android.content.Context;
import android.net.TrafficStats;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/* loaded from: classes2.dex */
public class UserNoticeContentWorker extends Worker {
    public final C18790t3 A00;
    public final C18810t5 A01;
    public final C18800t4 A02;
    public final AnonymousClass12M A03;
    public final AnonymousClass12S A04;

    public UserNoticeContentWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = (C18790t3) r1.AJw.get();
        this.A04 = (AnonymousClass12S) r1.AMF.get();
        this.A02 = (C18800t4) r1.AHs.get();
        this.A01 = (C18810t5) r1.AMu.get();
        this.A03 = (AnonymousClass12M) r1.AME.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        AnonymousClass043 r1;
        WorkerParameters workerParameters = super.A01;
        C006503b r2 = workerParameters.A01;
        int A02 = r2.A02("notice_id", -1);
        String A03 = r2.A03("url");
        if (A02 == -1 || A03 == null || workerParameters.A00 > 4) {
            this.A04.A02(2);
            return new AnonymousClass0GK();
        }
        try {
            TrafficStats.setThreadStatsTag(16);
            AbstractC37631mk A00 = this.A01.A00().A00(this.A02, A03, null);
            try {
                if (A00.A7O() != 200) {
                    this.A04.A02(2);
                    r1 = new AnonymousClass042();
                } else {
                    byte[] A04 = AnonymousClass1P1.A04(A00.AAZ(this.A00, null, 27));
                    C43891xl A002 = C43931xr.A00(new ByteArrayInputStream(A04), A02);
                    if (A002 == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("UserNoticeContentManager/storeUserNoticeContent/cannot parse response for notice: ");
                        sb.append(A02);
                        Log.i(sb.toString());
                        this.A04.A02(3);
                        r1 = new AnonymousClass042();
                    } else {
                        if (!this.A03.A08(new ByteArrayInputStream(A04), "content.json", A02)) {
                            r1 = new AnonymousClass042();
                        } else {
                            ArrayList arrayList = new ArrayList();
                            ArrayList arrayList2 = new ArrayList();
                            C43911xp r12 = A002.A02;
                            if (r12 != null) {
                                arrayList.add("banner_icon_light.png");
                                arrayList2.add(r12.A03);
                                arrayList.add("banner_icon_dark.png");
                                arrayList2.add(r12.A02);
                            }
                            C43841xg r13 = A002.A04;
                            if (r13 != null) {
                                arrayList.add("modal_icon_light.png");
                                arrayList2.add(r13.A06);
                                arrayList.add("modal_icon_dark.png");
                                arrayList2.add(r13.A05);
                            }
                            C43841xg r14 = A002.A03;
                            if (r14 != null) {
                                arrayList.add("blocking_modal_icon_light.png");
                                arrayList2.add(r14.A06);
                                arrayList.add("blocking_modal_icon_dark.png");
                                arrayList2.add(r14.A05);
                            }
                            C006403a r4 = new C006403a();
                            Map map = r4.A00;
                            map.put("file_name_list", (String[]) arrayList.toArray(new String[0]));
                            map.put("url_list", (String[]) arrayList2.toArray(new String[0]));
                            r1 = new AnonymousClass0GL(r4.A00());
                        }
                    }
                }
                A00.close();
                return r1;
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException e) {
            Log.e("UserNoticeContentWorker/doWork/fetch failed ", e);
            this.A04.A02(2);
            return new AnonymousClass0GK();
        } finally {
            TrafficStats.clearThreadStatsTag();
        }
    }
}
