package com.whatsapp.usernotice;

import X.AbstractFutureC44231yX;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass0GK;
import X.AnonymousClass12O;
import X.AnonymousClass12S;
import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import X.AnonymousClass3ZK;
import X.AnonymousClass4LK;
import X.C006503b;
import X.C05380Pi;
import X.C08740bm;
import X.C17220qS;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class UserNoticeStageUpdateWorker extends ListenableWorker {
    public final C17220qS A00;
    public final AnonymousClass12S A01;
    public final AnonymousClass12O A02;

    public UserNoticeStageUpdateWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A00 = r1.A3P();
        this.A01 = (AnonymousClass12S) r1.AMF.get();
        this.A02 = (AnonymousClass12O) r1.AMG.get();
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A01() {
        Object obj;
        AnonymousClass4LK r1 = new Object() { // from class: X.4LK
        };
        C05380Pi r7 = new C05380Pi();
        C08740bm r6 = new C08740bm(r7);
        r7.A00 = r6;
        r7.A02 = r1.getClass();
        try {
            UserNoticeStageUpdateWorker userNoticeStageUpdateWorker = UserNoticeStageUpdateWorker.this;
            C006503b r2 = ((ListenableWorker) userNoticeStageUpdateWorker).A01.A01;
            int A02 = r2.A02("notice_id", -1);
            int A022 = r2.A02("stage", -1);
            int A023 = r2.A02("version", -1);
            if (A02 == -1 || A022 == -1 || A023 == -1) {
                obj = new AnonymousClass0GK();
            } else {
                StringBuilder sb = new StringBuilder("UserNoticeStageUpdateWorker/startWork/noticeId: ");
                sb.append(A02);
                sb.append(" stage: ");
                sb.append(A022);
                Log.i(sb.toString());
                C17220qS r4 = userNoticeStageUpdateWorker.A00;
                String A01 = r4.A01();
                r4.A0D(new AnonymousClass3ZK(r7, userNoticeStageUpdateWorker, A02, A023, A022), new AnonymousClass1V8(new AnonymousClass1V8("notice", new AnonymousClass1W9[]{new AnonymousClass1W9("id", Integer.toString(A02)), new AnonymousClass1W9("stage", Integer.toString(A022))}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("to", "s.whatsapp.net"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("xmlns", "tos"), new AnonymousClass1W9("id", A01)}), A01, 254, 32000);
                obj = "Send Stage Update";
            }
            r7.A02 = obj;
            return r6;
        } catch (Exception e) {
            r6.A00(e);
            return r6;
        }
    }
}
