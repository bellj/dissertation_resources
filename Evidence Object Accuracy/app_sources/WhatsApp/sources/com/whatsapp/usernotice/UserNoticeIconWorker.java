package com.whatsapp.usernotice;

import X.AbstractC37631mk;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass042;
import X.AnonymousClass043;
import X.AnonymousClass0GK;
import X.AnonymousClass0GL;
import X.AnonymousClass12M;
import X.AnonymousClass12S;
import X.C006503b;
import X.C18790t3;
import X.C18800t4;
import X.C18810t5;
import android.content.Context;
import android.net.TrafficStats;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes2.dex */
public class UserNoticeIconWorker extends Worker {
    public final C18790t3 A00;
    public final C18810t5 A01;
    public final C18800t4 A02;
    public final AnonymousClass12M A03;
    public final AnonymousClass12S A04;

    public UserNoticeIconWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = (C18790t3) r1.AJw.get();
        this.A04 = (AnonymousClass12S) r1.AMF.get();
        this.A02 = (C18800t4) r1.AHs.get();
        this.A01 = (C18810t5) r1.AMu.get();
        this.A03 = (AnonymousClass12M) r1.AME.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        String[] strArr;
        String[] strArr2;
        AnonymousClass043 r1;
        AbstractC37631mk A00;
        WorkerParameters workerParameters = super.A01;
        C006503b r12 = workerParameters.A01;
        int A02 = r12.A02("notice_id", -1);
        Map map = r12.A00;
        Object obj = map.get("file_name_list");
        if (obj instanceof String[]) {
            strArr = (String[]) obj;
        } else {
            strArr = null;
        }
        Object obj2 = map.get("url_list");
        if (obj2 instanceof String[]) {
            strArr2 = (String[]) obj2;
        } else {
            strArr2 = null;
        }
        if (A02 == -1 || strArr == null || strArr2 == null || workerParameters.A00 > 4) {
            this.A04.A02(4);
            return new AnonymousClass0GK();
        }
        TrafficStats.setThreadStatsTag(16);
        for (int i = 0; i < strArr2.length; i++) {
            try {
                try {
                    A00 = this.A01.A00().A00(this.A02, strArr2[i], null);
                } catch (IOException e) {
                    Log.e("UserNoticeContentWorker/doWork/fetch failed ", e);
                    this.A04.A02(4);
                    r1 = new AnonymousClass0GK();
                }
                try {
                    if (A00.A7O() != 200) {
                        this.A04.A02(4);
                        r1 = new AnonymousClass0GK();
                    } else {
                        if (!this.A03.A08(A00.AAZ(this.A00, null, 27), strArr[i], A02)) {
                            r1 = new AnonymousClass042();
                        } else {
                            TrafficStats.clearThreadStatsTag();
                        }
                    }
                    A00.close();
                    return r1;
                } finally {
                    try {
                        A00.close();
                    } catch (Throwable unused) {
                    }
                }
            } finally {
                TrafficStats.clearThreadStatsTag();
            }
        }
        return new AnonymousClass0GL(C006503b.A01);
    }
}
