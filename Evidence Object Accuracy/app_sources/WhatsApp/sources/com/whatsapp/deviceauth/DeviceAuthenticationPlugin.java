package com.whatsapp.deviceauth;

import X.AnonymousClass03H;
import X.AnonymousClass074;
import androidx.lifecycle.OnLifecycleEvent;

/* loaded from: classes.dex */
public abstract class DeviceAuthenticationPlugin implements AnonymousClass03H {
    public abstract void A00();

    public abstract boolean A01();

    @OnLifecycleEvent(AnonymousClass074.ON_CREATE)
    public void onCreate() {
        if (A01()) {
            A00();
        }
    }
}
