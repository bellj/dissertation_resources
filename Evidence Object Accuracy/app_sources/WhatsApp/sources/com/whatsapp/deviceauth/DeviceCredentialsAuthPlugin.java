package com.whatsapp.deviceauth;

import X.AbstractC15710nm;
import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass0PW;
import X.AnonymousClass0XW;
import X.AnonymousClass5U7;
import X.C05000Nw;
import X.C05130Oj;
import X.C05500Pu;
import X.C06150Sj;
import X.C53392eC;
import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class DeviceCredentialsAuthPlugin extends DeviceAuthenticationPlugin {
    public C06150Sj A00;
    public C05000Nw A01;
    public C05500Pu A02;
    public final int A03;
    public final AnonymousClass0PW A04;
    public final ActivityC000900k A05;
    public final AnonymousClass01d A06;

    public DeviceCredentialsAuthPlugin(ActivityC000900k r3, AbstractC15710nm r4, AnonymousClass01d r5, AnonymousClass5U7 r6, int i) {
        this.A06 = r5;
        this.A05 = r3;
        this.A03 = i;
        this.A04 = new C53392eC(r4, r6, "DeviceCredentialsAuthPlugin");
        r3.A06.A00(this);
    }

    @Override // com.whatsapp.deviceauth.DeviceAuthenticationPlugin
    public void A00() {
        if (Build.VERSION.SDK_INT >= 30) {
            ActivityC000900k r3 = this.A05;
            this.A02 = new C05500Pu(this.A04, r3, AnonymousClass00T.A07(r3));
            this.A01 = A02();
        }
    }

    @Override // com.whatsapp.deviceauth.DeviceAuthenticationPlugin
    public boolean A01() {
        int i = Build.VERSION.SDK_INT;
        if (i < 23 || !A06()) {
            return false;
        }
        if (i >= 30) {
            return A05();
        }
        if (i == 29) {
            return this.A06.A0S("android.software.secure_lock_screen");
        }
        return true;
    }

    public final C05000Nw A02() {
        C05130Oj r2 = new C05130Oj();
        r2.A03 = this.A05.getString(this.A03);
        r2.A00 = 32768;
        return r2.A00();
    }

    public void A03() {
        int i = Build.VERSION.SDK_INT;
        if (i < 23) {
            throw new IllegalStateException("DeviceCredentialsAuthPlugin/authenticate: SDK must be 23 or higher. Have you checked if you can authenticate?");
        } else if (i >= 30) {
            A04();
        } else {
            KeyguardManager A07 = this.A06.A07();
            if (A07 != null) {
                ActivityC000900k r2 = this.A05;
                Intent createConfirmDeviceCredentialIntent = A07.createConfirmDeviceCredentialIntent(r2.getString(this.A03), "");
                Log.i("DeviceCredentialsAuthPlugin/authentication-attempt-API29AndBelow");
                r2.startActivityForResult(createConfirmDeviceCredentialIntent, 12345);
                return;
            }
            throw new IllegalStateException("DeviceCredentialsManager/authenticate: Cannot get KeyguardManager. Have you checked if you can authenticate?");
        }
    }

    public final void A04() {
        if (this.A02 == null || this.A01 == null) {
            throw new IllegalStateException("DeviceCredentialsAuthPlugin/authenticate: No prompt created. Have you checked if you can authenticate?");
        }
        Log.i("DeviceCredentialsAuthPlugin/authentication-attempt-API30AndAbove");
        this.A02.A03(this.A01);
    }

    public final boolean A05() {
        C06150Sj r1 = this.A00;
        if (r1 == null) {
            r1 = new C06150Sj(new AnonymousClass0XW(this.A05));
            this.A00 = r1;
        }
        return r1.A03(32768) == 0;
    }

    public final boolean A06() {
        KeyguardManager A07 = this.A06.A07();
        return A07 != null && A07.isDeviceSecure();
    }
}
