package com.whatsapp.deviceauth;

import X.AbstractC15710nm;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass0XW;
import X.AnonymousClass5U7;
import X.C05000Nw;
import X.C05130Oj;
import X.C05500Pu;
import X.C06150Sj;
import X.C12960it;
import X.C14850m9;
import X.C14900mE;
import X.C53392eC;
import android.app.KeyguardManager;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.deviceauth.BiometricAuthPlugin;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class BiometricAuthPlugin extends DeviceAuthenticationPlugin {
    public C06150Sj A00;
    public C05000Nw A01;
    public C05500Pu A02;
    public final int A03;
    public final int A04;
    public final ActivityC000900k A05;
    public final AbstractC15710nm A06;
    public final C14900mE A07;
    public final AnonymousClass01d A08;
    public final AnonymousClass5U7 A09;
    public final DeviceCredentialsAuthPlugin A0A;
    public final C14850m9 A0B;

    public BiometricAuthPlugin(ActivityC000900k r7, AbstractC15710nm r8, C14900mE r9, AnonymousClass01d r10, AnonymousClass5U7 r11, C14850m9 r12, int i, int i2) {
        this.A0B = r12;
        this.A07 = r9;
        this.A06 = r8;
        this.A08 = r10;
        this.A05 = r7;
        this.A04 = i;
        this.A03 = i2;
        this.A09 = r11;
        this.A0A = new DeviceCredentialsAuthPlugin(r7, r8, r10, r11, i);
        r7.A06.A00(this);
    }

    @Override // com.whatsapp.deviceauth.DeviceAuthenticationPlugin
    public void A00() {
        String str;
        ActivityC000900k r5 = this.A05;
        this.A02 = new C05500Pu(new C53392eC(this.A06, new AnonymousClass5U7() { // from class: X.56P
            @Override // X.AnonymousClass5U7
            public final void AMd(int i) {
                BiometricAuthPlugin.this.A03(i);
            }
        }, "BiometricAuthPlugin"), r5, AnonymousClass00T.A07(r5));
        C05130Oj r1 = new C05130Oj();
        r1.A03 = r5.getString(this.A04);
        int i = this.A03;
        if (i != 0) {
            str = r5.getString(i);
        } else {
            str = null;
        }
        r1.A02 = str;
        r1.A00 = 33023;
        r1.A04 = false;
        this.A01 = r1.A00();
    }

    @Override // com.whatsapp.deviceauth.DeviceAuthenticationPlugin
    public boolean A01() {
        return Build.VERSION.SDK_INT >= 23 && this.A0B.A07(482) && A04() && A05();
    }

    public void A02() {
        if (this.A02 == null || this.A01 == null) {
            throw C12960it.A0U("BiometricAuthPlugin/authenticate: No prompt created. Have you checked if you can authenticate?");
        }
        Log.i("BiometricAuthPlugin/authentication-attempt");
        this.A02.A03(this.A01);
    }

    public final void A03(int i) {
        if (Build.VERSION.SDK_INT != 29 && (i == 2 || i == 3)) {
            C05500Pu r0 = this.A02;
            AnonymousClass009.A05(r0);
            r0.A00();
            this.A07.A0J(new RunnableBRunnable0Shape15S0100000_I1_1(this.A0A, 32), 200);
        } else if (i == 2) {
            this.A09.AMd(4);
        } else {
            this.A09.AMd(i);
        }
    }

    public final boolean A04() {
        C06150Sj r1 = this.A00;
        if (r1 == null) {
            r1 = new C06150Sj(new AnonymousClass0XW(this.A05));
            this.A00 = r1;
        }
        return C12960it.A1T(r1.A03(255));
    }

    public final boolean A05() {
        String str;
        KeyguardManager A07 = this.A08.A07();
        if (A07 == null || !A07.isDeviceSecure()) {
            str = "BiometricAuthPlugin/NoDeviceCredentials";
        } else if (this.A0A.A01()) {
            return true;
        } else {
            str = "BiometricAuthPlugin/CannotAuthenticateWithDeviceCredentials";
        }
        Log.i(str);
        return false;
    }
}
