package com.whatsapp;

import X.AbstractC17940re;
import X.AbstractC18220s6;
import X.AbstractServiceC003701q;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass020;
import X.AnonymousClass022;
import X.AnonymousClass023;
import X.AnonymousClass03v;
import X.AnonymousClass0GQ;
import X.AnonymousClass5B7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C17060qC;
import X.C18210s5;
import X.C18300sE;
import X.C19410u4;
import X.C19900ur;
import X.C19920ut;
import X.C20270vU;
import X.C20880wT;
import X.C21020wh;
import X.C21710xr;
import X.C58182oH;
import X.C71083cM;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.text.TextUtils;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;
import com.whatsapp.workers.ntp.NtpSyncWorker;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class AlarmService extends AbstractServiceC003701q implements AnonymousClass004 {
    public WhatsAppLibLoader A00;
    public Set A01;
    public boolean A02;
    public final Object A03;
    public volatile AbstractC18220s6 A04;
    public volatile C71083cM A05;

    public AlarmService() {
        this(0);
    }

    public AlarmService(int i) {
        this.A03 = C12970iu.A0l();
        this.A02 = false;
    }

    @Override // X.AbstractServiceC003801r
    public boolean A04() {
        boolean z;
        AbstractC18220s6 r3 = this.A04;
        if (r3 == null) {
            return false;
        }
        if (!(r3 instanceof C18300sE)) {
            z = true;
        } else {
            z = false;
        }
        StringBuilder A0k = C12960it.A0k("AlarmService/onStopCurrentWork; retry=");
        A0k.append(z);
        A0k.append(", handler= ");
        Log.i(C12960it.A0d(C12980iv.A0s(r3), A0k));
        return z;
    }

    @Override // X.AbstractServiceC003801r
    public void A05(Intent intent) {
        String str;
        long j;
        String action = intent.getAction();
        Log.i(C12960it.A0d(action, C12960it.A0k("AlarmService/onHandleWork received intent with action ")));
        if (action == null) {
            Log.w("AlarmService/onHandleWork received null action in intent");
        } else if (!this.A00.A03()) {
            Log.e("AlarmService/onHandleWork skip, native libraries missing");
        } else {
            try {
                if (TextUtils.equals(action, "com.whatsapp.action.SETUP")) {
                    Log.i(C12960it.A0b("AlarmService/setup; intent=", intent));
                    for (AbstractC18220s6 r1 : this.A01) {
                        Log.i(C12960it.A0d(C12980iv.A0s(r1), C12960it.A0k("AlarmService/setup: ")));
                        if (r1 instanceof C21020wh) {
                            C21020wh r12 = (C21020wh) r1;
                            SharedPreferences sharedPreferences = r12.A01.A00;
                            String string = sharedPreferences.getString("web_session_verification_browser_ids", null);
                            if (!(string == null || Arrays.asList(string.split(",")) == null || sharedPreferences.getLong("web_session_verification_when_millis", -1) == -1)) {
                                r12.A02.A0G(r12.A00, sharedPreferences.getLong("web_session_verification_when_millis", -1));
                            }
                        } else if (r1 instanceof C20880wT) {
                            ((C20880wT) r1).A02();
                        } else if (r1 instanceof C20270vU) {
                            C20270vU r13 = (C20270vU) r1;
                            if (r13.A04.A07(170)) {
                                Log.i("NtpAction; cancelling ntp sync using alarm manager.");
                                PendingIntent A00 = r13.A00("com.whatsapp.action.UPDATE_NTP", 536870912);
                                if (A00 != null) {
                                    AlarmManager A04 = r13.A00.A04();
                                    if (A04 != null) {
                                        A04.cancel(A00);
                                    }
                                    A00.cancel();
                                }
                                Log.i("NtpAction; setting ntp sync using work manager.");
                                C17060qC r5 = r13.A05;
                                TimeUnit timeUnit = TimeUnit.MILLISECONDS;
                                AnonymousClass0GQ r4 = new AnonymousClass0GQ(NtpSyncWorker.class, timeUnit, timeUnit);
                                r4.A01.add("tag.whatsapp.time.ntp");
                                AnonymousClass020 A002 = r4.A00();
                                C21710xr r6 = r5.A02;
                                new AnonymousClass03v(AnonymousClass023.KEEP, (AnonymousClass022) r6.get(), "name.whatsapp.time.ntp", Collections.singletonList(A002), null).A03();
                                SharedPreferences.Editor edit = r5.A01.A01("ntp-scheduler").edit();
                                synchronized (r6) {
                                    j = r6.A00;
                                }
                                edit.putLong("/ntp/work_manager_init", j).apply();
                            } else {
                                Log.i("NtpAction; cancelling ntp sync using work manager.");
                                C21710xr r42 = r13.A05.A02;
                                ((AnonymousClass022) r42.get()).A08("name.whatsapp.time.ntp");
                                ((AnonymousClass022) r42.get()).A07("tag.whatsapp.time.ntp");
                                Log.i("NtpAction; setting up ntp sync using alarm manager.");
                                PendingIntent A003 = r13.A00("com.whatsapp.action.UPDATE_NTP", 134217728);
                                AlarmManager A042 = r13.A00.A04();
                                if (A042 != null) {
                                    A042.setInexactRepeating(3, 43200000 + SystemClock.elapsedRealtime(), 43200000, A003);
                                } else {
                                    Log.w("NtpAction/setupUpdateNtpAlarm AlarmManager is null");
                                }
                            }
                            r13.A02(null);
                        } else if (r1 instanceof C19920ut) {
                            C19920ut r14 = (C19920ut) r1;
                            Log.i("HourlyCronAction; setting hourly cron using alarms");
                            if (r14.A00("com.whatsapp.action.HOURLY_CRON", 536870912) == null) {
                                AlarmManager A043 = r14.A00.A04();
                                if (A043 != null) {
                                    A043.setInexactRepeating(3, 3600000 + SystemClock.elapsedRealtime(), 3600000, r14.A00("com.whatsapp.action.HOURLY_CRON", 0));
                                } else {
                                    Log.w("HourlyCronAction; setup skipped, AlarmManager is null");
                                }
                            }
                        } else if (r1 instanceof C19900ur) {
                            ((C19900ur) r1).A02();
                        } else if (r1 instanceof C19410u4) {
                            C19410u4 r15 = (C19410u4) r1;
                            r15.A03();
                            r15.A02();
                        } else if (r1 instanceof C18300sE) {
                            ((C18300sE) r1).A02();
                        }
                    }
                } else {
                    for (AbstractC18220s6 r43 : this.A01) {
                        if (!(r43 instanceof C21020wh)) {
                            if (r43 instanceof C20880wT) {
                                str = "com.whatsapp.action.ROTATE_SIGNED_PREKEY";
                            } else if (r43 instanceof C20270vU) {
                                str = "com.whatsapp.action.UPDATE_NTP";
                            } else if (r43 instanceof C19920ut) {
                                str = "com.whatsapp.action.HOURLY_CRON";
                            } else if (r43 instanceof C19900ur) {
                                str = "com.whatsapp.action.HEARTBEAT_WAKEUP";
                            } else if (r43 instanceof C19410u4) {
                                String action2 = intent.getAction();
                                if (!"com.whatsapp.action.DAILY_CRON".equals(action2) && !"com.whatsapp.action.DAILY_CATCHUP_CRON".equals(action2)) {
                                }
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append("AlarmService/onHandleWork: handling ");
                                A0h.append(action);
                                A0h.append(" using ");
                                Log.i(C12960it.A0d(C12980iv.A0s(r43), A0h));
                                this.A04 = r43;
                                r43.A01(intent);
                                break;
                            } else if (r43 instanceof C18300sE) {
                                str = "com.whatsapp.action.BACKUP_MESSAGES";
                            } else {
                                continue;
                            }
                            if (str.equals(intent.getAction())) {
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append("AlarmService/onHandleWork: handling ");
                                A0h.append(action);
                                A0h.append(" using ");
                                Log.i(C12960it.A0d(C12980iv.A0s(r43), A0h));
                                this.A04 = r43;
                                r43.A01(intent);
                                break;
                            }
                        }
                    }
                    Log.w(C12960it.A0Z(intent, "AlarmService/onHandleWork: received unrecognized intent; intent=", C12960it.A0h()));
                }
            } finally {
                this.A04 = null;
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A05 == null) {
            synchronized (this.A03) {
                if (this.A05 == null) {
                    this.A05 = new C71083cM(this);
                }
            }
        }
        return this.A05.generatedComponent();
    }

    @Override // X.AbstractServiceC003801r, android.app.Service
    public void onCreate() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A00 = (WhatsAppLibLoader) r1.ANa.get();
            C18300sE A20 = r1.A20();
            C19410u4 A21 = r1.A21();
            C19920ut A24 = r1.A24();
            C20270vU A25 = r1.A25();
            C20880wT A26 = r1.A26();
            C19900ur A22 = r1.A22();
            C18210s5 A1z = r1.A1z();
            C21020wh A27 = r1.A27();
            HashSet A12 = C12970iu.A12();
            A12.add(A20);
            A12.add(A21);
            A12.add(A24);
            A12.add(A25);
            A12.add(A26);
            A12.add(A22);
            A12.add(A1z);
            A12.add(A27);
            this.A01 = AbstractC17940re.copyOf(A12);
        }
        super.onCreate();
    }
}
