package com.whatsapp.service;

import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AnonymousClass01H;
import X.AnonymousClass01N;
import X.AnonymousClass17R;
import X.AnonymousClass1JK;
import X.AnonymousClass1UY;
import X.C002601e;
import X.C005602s;
import X.C14840m8;
import X.C15450nH;
import X.C16590pI;
import X.C18360sK;
import X.C22100yW;
import X.C22630zO;
import X.C238713i;
import X.C238813j;
import X.C245716a;
import X.C65303Iz;
import X.C70653bd;
import X.ExecutorC41461tZ;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import com.facebook.redex.RunnableBRunnable0Shape0S1101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.R;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.service.MDSyncService;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public final class MDSyncService extends AnonymousClass1JK {
    public int A00;
    public RunnableBRunnable0Shape1S0300000_I0_1 A01;
    public C15450nH A02;
    public C16590pI A03;
    public C22100yW A04;
    public C245716a A05;
    public C238813j A06;
    public C14840m8 A07;
    public C238713i A08;
    public AnonymousClass17R A09;
    public AbstractC14440lR A0A;
    public AbstractC14590lg A0B;
    public String A0C = null;
    public boolean A0D;
    public boolean A0E = false;
    public final AnonymousClass01H A0F = new C002601e(null, new AnonymousClass01N() { // from class: X.5EG
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return C12970iu.A0E();
        }
    });

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public MDSyncService() {
        super("md-sync-service", true);
    }

    public final void A03(int i) {
        if (this.A0C == null) {
            this.A0C = getString(R.string.notification_text_sync_with_companion_client);
        }
        StringBuilder sb = new StringBuilder("md-sync-service/showForegroundNotification startId:");
        sb.append(i);
        Log.i(sb.toString());
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A09 = AnonymousClass1UY.A00(this, 0, C65303Iz.A02(this), 0);
        int i2 = -2;
        if (Build.VERSION.SDK_INT >= 26) {
            i2 = -1;
        }
        A00.A03 = i2;
        A00.A0B(this.A0C);
        A00.A09(this.A0C);
        C18360sK.A01(A00, R.drawable.notify_web_client_connected);
        A01(i, A00.A01(), 221770024);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        Log.i("md-sync-service/onCreate");
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("md-sync-service/onDestroy");
        AbstractC14590lg r1 = this.A0B;
        if (r1 != null) {
            this.A06.A00.A02(r1);
            this.A0B = null;
        }
        RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 = this.A01;
        if (runnableBRunnable0Shape1S0300000_I0_1 != null) {
            synchronized (runnableBRunnable0Shape1S0300000_I0_1) {
                ((AtomicBoolean) runnableBRunnable0Shape1S0300000_I0_1.A00).set(true);
            }
            this.A01 = null;
        }
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        int i3;
        StringBuilder sb = new StringBuilder("md-sync-service/onStartCommand:");
        sb.append(intent);
        sb.append("; startId: ");
        sb.append(i2);
        Log.i(sb.toString());
        if (intent != null) {
            if ("com.whatsapp.service.MDSyncService.STOP_HISTORY_SYNC".equals(intent.getAction())) {
                this.A0D = false;
            } else if (!"com.whatsapp.service.MDSyncService.START_HISTORY_SYNC".equals(intent.getAction())) {
                if ("com.whatsapp.service.MDSyncService.RMR_STARTED".equals(intent.getAction())) {
                    i3 = this.A00 + 1;
                } else if ("com.whatsapp.service.MDSyncService.RMR_COMPLETED".equals(intent.getAction())) {
                    i3 = this.A00 - 1;
                }
                this.A00 = i3;
            } else if (this.A01 == null) {
                this.A0D = true;
                RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 = new RunnableBRunnable0Shape1S0300000_I0_1(this.A06, this.A08);
                this.A01 = runnableBRunnable0Shape1S0300000_I0_1;
                this.A0A.Ab2(runnableBRunnable0Shape1S0300000_I0_1);
            }
        }
        A03(i2);
        if (!this.A0D && this.A00 <= 0) {
            ((AnonymousClass1JK) this).A01.A01(this.A03.A00, MDSyncService.class);
        } else if (this.A0B == null) {
            C70653bd r6 = new AbstractC14590lg(i2) { // from class: X.3bd
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    String string;
                    MDSyncService mDSyncService = MDSyncService.this;
                    int i4 = this.A00;
                    Map map = (Map) obj;
                    if (map.containsValue(Boolean.TRUE)) {
                        Iterator A0n = C12960it.A0n(map);
                        while (true) {
                            if (!A0n.hasNext()) {
                                string = mDSyncService.getString(R.string.notification_text_sync_with_companion_client);
                                break;
                            }
                            Map.Entry A15 = C12970iu.A15(A0n);
                            if (A15.getValue() == Boolean.TRUE) {
                                AnonymousClass1JU A06 = mDSyncService.A04.A06(((DeviceJid) A15.getKey()).device);
                                if (A06 != null) {
                                    string = C12960it.A0X(mDSyncService, AnonymousClass1JU.A00(mDSyncService, A06), C12970iu.A1b(), 0, R.string.notification_text_sync_with_one_companion_client);
                                    break;
                                }
                                Log.e(C12970iu.A0s(A15.getKey(), C12960it.A0k("md-sync-service/getNotificationText companionDeviceInfo missing for ")));
                            }
                        }
                        ((Handler) mDSyncService.A0F.get()).post(new RunnableBRunnable0Shape0S1101000_I0(mDSyncService, string, i4, 2));
                        return;
                    }
                    ((AnonymousClass1JK) mDSyncService).A01.A03(mDSyncService.A03.A00, C12990iw.A0E("com.whatsapp.service.MDSyncService.STOP_HISTORY_SYNC"), MDSyncService.class);
                }
            };
            this.A0B = r6;
            C238813j r5 = this.A06;
            r5.A02.execute(new RunnableBRunnable0Shape1S0300000_I0_1(r5, r6, new ExecutorC41461tZ(this.A0A), 2));
            return 1;
        }
        return 1;
    }
}
