package com.whatsapp.service;

import X.AnonymousClass046;
import X.AnonymousClass1JK;
import X.AnonymousClass1UY;
import X.C005602s;
import X.C14960mK;
import X.C18360sK;
import X.C22630zO;
import android.app.Notification;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.IBinder;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class GcmFGService extends AnonymousClass1JK {
    public boolean A00 = false;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public GcmFGService() {
        super("gcmfgservice", false);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        Log.i("gcmfgservice/onCreate");
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("gcmfgservice/onDestroy");
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        StringBuilder sb = new StringBuilder("gcmfgservice/onStartCommand:");
        sb.append(intent);
        sb.append(" startId:");
        sb.append(i2);
        Log.i(sb.toString());
        Resources resources = getResources();
        if (resources instanceof AnonymousClass046) {
            resources = ((AnonymousClass046) resources).A00;
        }
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A0B(resources.getString(R.string.localized_app_name));
        A00.A0A(resources.getString(R.string.localized_app_name));
        A00.A09(resources.getString(R.string.notification_text_gcm_fg));
        A00.A09 = AnonymousClass1UY.A00(this, 1, C14960mK.A02(this), 0);
        int i3 = Build.VERSION.SDK_INT;
        int i4 = -2;
        if (i3 >= 26) {
            i4 = -1;
        }
        A00.A03 = i4;
        if (i3 != 24) {
            C18360sK.A01(A00, R.drawable.notifybar);
        }
        Notification A01 = A00.A01();
        int i5 = 11;
        if (i3 == 24) {
            Notification.Builder recoverBuilder = Notification.Builder.recoverBuilder(this, A01);
            recoverBuilder.setSmallIcon(Icon.createWithBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.notifybar)));
            A01 = recoverBuilder.build();
            i5 = 221770011;
        }
        A01(i2, A01, i5);
        return 1;
    }
}
