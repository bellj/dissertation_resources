package com.whatsapp.service;

import X.AnonymousClass1JK;
import X.C26461Dl;
import android.content.Intent;
import android.os.IBinder;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class WebClientService extends AnonymousClass1JK {
    public C26461Dl A00;
    public boolean A01 = false;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public WebClientService() {
        super("webclientservice", true);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        Log.i("webclientservice/onCreate");
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        Log.i("webclientservice/onDestroy");
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        StringBuilder sb = new StringBuilder("webclientservice/onStartCommand:");
        sb.append(intent);
        Log.i(sb.toString());
        boolean z = false;
        if (intent != null && intent.getBooleanExtra("isPortal", false)) {
            z = true;
        }
        A01(i2, this.A00.A00(z), 9);
        return 1;
    }
}
