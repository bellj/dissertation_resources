package com.whatsapp.service;

import X.AbstractServiceC27491Hs;
import X.AnonymousClass004;
import X.AnonymousClass11P;
import X.AnonymousClass5B7;
import X.C35191hP;
import X.C58182oH;
import X.C71083cM;
import android.content.Intent;
import android.os.IBinder;

/* loaded from: classes2.dex */
public class BackgroundMediaControlService extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public AnonymousClass11P A00;
    public boolean A01;
    public final Object A02;
    public volatile C71083cM A03;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public BackgroundMediaControlService() {
        this(0);
    }

    public BackgroundMediaControlService(int i) {
        this.A02 = new Object();
        this.A01 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    this.A03 = new C71083cM(this);
                }
            }
        }
        return this.A03.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A01) {
            this.A01 = true;
            this.A00 = (AnonymousClass11P) ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01.ABp.get();
        }
        super.onCreate();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        String str;
        if (intent != null) {
            str = intent.getAction();
        } else {
            str = null;
        }
        C35191hP A00 = this.A00.A00();
        if (str != null) {
            if (str.equals("com.whatsapp.service.BackgroundMediaControlService.STOP")) {
                this.A00.A04();
            } else if (str.equals("com.whatsapp.service.BackgroundMediaControlService.START") && A00 != null) {
                A00.A0A(0, true, false);
            }
        }
        stopSelf();
        return 2;
    }
}
