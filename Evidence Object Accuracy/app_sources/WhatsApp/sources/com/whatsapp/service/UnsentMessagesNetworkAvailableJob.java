package com.whatsapp.service;

import X.AbstractC14440lR;
import X.AbstractC18860tB;
import X.AnonymousClass004;
import X.AnonymousClass12H;
import X.AnonymousClass132;
import X.AnonymousClass5B7;
import X.C12970iu;
import X.C16490p7;
import X.C19890uq;
import X.C26061Bw;
import X.C44221yW;
import X.C71083cM;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Handler;
import com.whatsapp.service.UnsentMessagesNetworkAvailableJob;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class UnsentMessagesNetworkAvailableJob extends JobService implements AnonymousClass004 {
    public JobParameters A00;
    public AnonymousClass12H A01;
    public C16490p7 A02;
    public AnonymousClass132 A03;
    public C19890uq A04;
    public AbstractC14440lR A05;
    public boolean A06;
    public final Handler A07;
    public final AbstractC18860tB A08;
    public final Object A09;
    public final Runnable A0A;
    public volatile C71083cM A0B;

    public UnsentMessagesNetworkAvailableJob() {
        this(0);
        this.A07 = new Handler();
        this.A08 = new C44221yW(this);
        this.A0A = new Runnable() { // from class: X.3l1
            @Override // java.lang.Runnable
            public final void run() {
                UnsentMessagesNetworkAvailableJob.A05(UnsentMessagesNetworkAvailableJob.this);
            }
        };
    }

    public UnsentMessagesNetworkAvailableJob(int i) {
        this.A09 = C12970iu.A0l();
        this.A06 = false;
    }

    public static /* synthetic */ void A04(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob) {
        C16490p7 r0 = unsentMessagesNetworkAvailableJob.A02;
        r0.A04();
        if (!r0.A01 || !unsentMessagesNetworkAvailableJob.A03.A03()) {
            JobParameters jobParameters = unsentMessagesNetworkAvailableJob.A00;
            if (jobParameters != null) {
                unsentMessagesNetworkAvailableJob.jobFinished(jobParameters, false);
                unsentMessagesNetworkAvailableJob.A00 = null;
                return;
            }
            return;
        }
        Handler handler = unsentMessagesNetworkAvailableJob.A07;
        handler.post(new Runnable() { // from class: X.3kz
            @Override // java.lang.Runnable
            public final void run() {
                r0.A01.A03(UnsentMessagesNetworkAvailableJob.this.A08);
            }
        });
        Log.i("Unsent messages found, scheduling timeout task");
        handler.postDelayed(unsentMessagesNetworkAvailableJob.A0A, C26061Bw.A0L);
        unsentMessagesNetworkAvailableJob.A04.A0C(0, false, true, false, false);
    }

    public static /* synthetic */ void A05(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob) {
        unsentMessagesNetworkAvailableJob.A01.A04(unsentMessagesNetworkAvailableJob.A08);
        JobParameters jobParameters = unsentMessagesNetworkAvailableJob.A00;
        if (jobParameters != null) {
            unsentMessagesNetworkAvailableJob.jobFinished(jobParameters, false);
            unsentMessagesNetworkAvailableJob.A00 = null;
        }
    }

    public static /* synthetic */ void A06(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob) {
        JobParameters jobParameters = unsentMessagesNetworkAvailableJob.A00;
        if (jobParameters != null) {
            unsentMessagesNetworkAvailableJob.jobFinished(jobParameters, false);
            unsentMessagesNetworkAvailableJob.A00 = null;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A0B == null) {
            synchronized (this.A09) {
                if (this.A0B == null) {
                    this.A0B = new C71083cM(this);
                }
            }
        }
        return this.A0B.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        Log.i("UnsentMessagesNetworkAvailableJob/onCreate");
        if (!this.A06) {
            this.A06 = true;
            ((AnonymousClass5B7) generatedComponent()).A00(this);
        }
        super.onCreate();
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.i("UnsentMessagesNetworkAvailableJob/onDestroy");
        super.onDestroy();
    }

    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        if (jobParameters.getJobId() != 6 || this.A00 != null) {
            return false;
        }
        this.A00 = jobParameters;
        this.A05.Ab2(new Runnable() { // from class: X.3l0
            @Override // java.lang.Runnable
            public final void run() {
                UnsentMessagesNetworkAvailableJob.A04(UnsentMessagesNetworkAvailableJob.this);
            }
        });
        return true;
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        if (this.A00 == null) {
            return true;
        }
        this.A01.A04(this.A08);
        this.A07.removeCallbacks(this.A0A);
        this.A00 = null;
        return true;
    }
}
