package com.whatsapp.service;

import X.AbstractFutureC44231yX;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass040;
import X.AnonymousClass0GL;
import X.AnonymousClass538;
import X.C006503b;
import X.C14900mE;
import X.C16240og;
import X.C18640sm;
import X.C19890uq;
import X.C26061Bw;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;
import java.util.concurrent.Executor;

/* loaded from: classes2.dex */
public class RestoreChatConnectionWorker extends ListenableWorker {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final AnonymousClass040 A01 = new AnonymousClass040();
    public final C14900mE A02;
    public final C16240og A03;
    public final C18640sm A04;
    public final C19890uq A05;

    public RestoreChatConnectionWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A02 = (C14900mE) r1.A8X.get();
        this.A05 = (C19890uq) r1.ABw.get();
        this.A03 = (C16240og) r1.ANq.get();
        this.A04 = r1.A7Z();
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A01() {
        C16240og r3 = this.A03;
        if (r3.A04 == 2) {
            Log.i("RestoreChatConnectionWorker/doWork nothing to do");
            AnonymousClass040 r32 = this.A01;
            r32.A09(new AnonymousClass0GL(C006503b.A01));
            return r32;
        }
        AnonymousClass538 r2 = new AnonymousClass538(this);
        r3.A03(r2);
        AnonymousClass040 r33 = this.A01;
        RunnableBRunnable0Shape7S0200000_I0_7 runnableBRunnable0Shape7S0200000_I0_7 = new RunnableBRunnable0Shape7S0200000_I0_7(this, 42, r2);
        Executor executor = this.A02.A06;
        r33.A5i(runnableBRunnable0Shape7S0200000_I0_7, executor);
        RunnableBRunnable0Shape11S0100000_I0_11 runnableBRunnable0Shape11S0100000_I0_11 = new RunnableBRunnable0Shape11S0100000_I0_11(this, 36);
        this.A00.postDelayed(runnableBRunnable0Shape11S0100000_I0_11, C26061Bw.A0L);
        r33.A5i(new RunnableBRunnable0Shape7S0200000_I0_7(this, 43, runnableBRunnable0Shape11S0100000_I0_11), executor);
        this.A05.A0F(null, null, 0, false, true, false, false, false, this.A04.A0C());
        return r33;
    }

    @Override // androidx.work.ListenableWorker
    public void A03() {
        this.A01.cancel(true);
    }
}
