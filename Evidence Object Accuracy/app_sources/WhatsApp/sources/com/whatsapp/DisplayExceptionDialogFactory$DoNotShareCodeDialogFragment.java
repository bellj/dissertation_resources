package com.whatsapp;

import X.ActivityC000900k;
import X.AnonymousClass12P;
import X.C004802e;
import X.C252018m;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment extends Hilt_DisplayExceptionDialogFactory_DoNotShareCodeDialogFragment {
    public AnonymousClass12P A00;
    public C252018m A01;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        C004802e r2 = new C004802e(A0C);
        r2.A07(R.string.pre_registration_do_not_share_code_dialog_title);
        r2.A06(R.string.pre_registration_do_not_share_code_dialog_message);
        r2.A0B(true);
        r2.setPositiveButton(R.string.ok, null);
        r2.setNegativeButton(R.string.learn_more, new DialogInterface.OnClickListener(A0C, this) { // from class: X.4gQ
            public final /* synthetic */ Context A00;
            public final /* synthetic */ DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DisplayExceptionDialogFactory$DoNotShareCodeDialogFragment displayExceptionDialogFactory$DoNotShareCodeDialogFragment = this.A01;
                displayExceptionDialogFactory$DoNotShareCodeDialogFragment.A00.A06(this.A00, new Intent("android.intent.action.VIEW", displayExceptionDialogFactory$DoNotShareCodeDialogFragment.A01.A03("30035737")));
                displayExceptionDialogFactory$DoNotShareCodeDialogFragment.A1B();
            }
        });
        return r2.create();
    }
}
