package com.whatsapp;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.C004802e;
import X.C238013b;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$ContactBlockedDialogFragment extends Hilt_DisplayExceptionDialogFactory_ContactBlockedDialogFragment {
    public C238013b A00;

    public static DialogFragment A00(String str, ArrayList arrayList) {
        DisplayExceptionDialogFactory$ContactBlockedDialogFragment displayExceptionDialogFactory$ContactBlockedDialogFragment = new DisplayExceptionDialogFactory$ContactBlockedDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message", str);
        bundle.putParcelableArrayList("jids", arrayList);
        displayExceptionDialogFactory$ContactBlockedDialogFragment.A0U(bundle);
        return displayExceptionDialogFactory$ContactBlockedDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Log.w("home/dialog contact-blocked");
        Bundle A03 = A03();
        String string = A03.getString("message");
        AnonymousClass009.A05(string);
        ArrayList parcelableArrayList = A03.getParcelableArrayList("jids");
        AnonymousClass009.A05(parcelableArrayList);
        ActivityC000900k A0C = A0C();
        C238013b r3 = this.A00;
        C004802e r2 = new C004802e(A0C);
        r2.A0A(string);
        r2.setPositiveButton(R.string.unblock, new DialogInterface.OnClickListener(A0C, r3, parcelableArrayList) { // from class: X.3Kb
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ C238013b A01;
            public final /* synthetic */ List A02;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                List list = this.A02;
                C238013b r22 = this.A01;
                Activity activity = this.A00;
                if (list.size() == 1) {
                    Object A0o = C12980iv.A0o(list);
                    AnonymousClass009.A05(A0o);
                    r22.A0C(activity, (UserJid) A0o);
                    return;
                }
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(activity.getPackageName(), "com.whatsapp.blocklist.BlockList");
                activity.startActivity(A0A);
            }
        });
        r2.setNegativeButton(R.string.cancel, null);
        return r2.create();
    }
}
