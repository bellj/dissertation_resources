package com.whatsapp;

import X.AbstractActivityC14700lu;
import X.AbstractC005102i;
import X.AbstractC009504t;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14710lv;
import X.AbstractC14720lw;
import X.AbstractC14730lx;
import X.AbstractC14740ly;
import X.AbstractC14750lz;
import X.AbstractC14760m0;
import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC18900tF;
import X.AbstractC32851cq;
import X.AbstractC33101dL;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass02Q;
import X.AnonymousClass078;
import X.AnonymousClass07E;
import X.AnonymousClass07F;
import X.AnonymousClass07G;
import X.AnonymousClass07M;
import X.AnonymousClass10T;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass19D;
import X.AnonymousClass19F;
import X.AnonymousClass19H;
import X.AnonymousClass19Z;
import X.AnonymousClass1A1;
import X.AnonymousClass1AM;
import X.AnonymousClass1AU;
import X.AnonymousClass1AV;
import X.AnonymousClass1AW;
import X.AnonymousClass1AY;
import X.AnonymousClass1AZ;
import X.AnonymousClass1FO;
import X.AnonymousClass1J1;
import X.AnonymousClass1SF;
import X.AnonymousClass1US;
import X.AnonymousClass1W2;
import X.AnonymousClass1s5;
import X.AnonymousClass1s8;
import X.AnonymousClass1s9;
import X.AnonymousClass2AC;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2GJ;
import X.AnonymousClass2GK;
import X.AnonymousClass2IW;
import X.AnonymousClass2KN;
import X.AnonymousClass2LB;
import X.AnonymousClass2LD;
import X.AnonymousClass2LE;
import X.AnonymousClass2LG;
import X.AnonymousClass2LH;
import X.AnonymousClass2LL;
import X.AnonymousClass2M5;
import X.AnonymousClass2MZ;
import X.AnonymousClass2Ma;
import X.AnonymousClass2Mb;
import X.AnonymousClass2Mc;
import X.AnonymousClass2Mk;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2P8;
import X.C003401m;
import X.C004902f;
import X.C14790m3;
import X.C14810m5;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14900mE;
import X.C14910mF;
import X.C14920mG;
import X.C14930mH;
import X.C14960mK;
import X.C15240mn;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15680nj;
import X.C15820nx;
import X.C15890o4;
import X.C16120oU;
import X.C16430p0;
import X.C16490p7;
import X.C16590pI;
import X.C17010q7;
import X.C17050qB;
import X.C17070qD;
import X.C18000rk;
import X.C18330sH;
import X.C18360sK;
import X.C18910tG;
import X.C18980tN;
import X.C19890uq;
import X.C19990v2;
import X.C20030v6;
import X.C20040v7;
import X.C20220vP;
import X.C20230vQ;
import X.C20640w5;
import X.C20650w6;
import X.C20670w8;
import X.C20710wC;
import X.C20780wJ;
import X.C20810wM;
import X.C20830wO;
import X.C20970wc;
import X.C21220x4;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C21300xC;
import X.C21320xE;
import X.C21740xu;
import X.C21880y8;
import X.C22050yP;
import X.C22630zO;
import X.C22640zP;
import X.C22660zR;
import X.C22710zW;
import X.C236712o;
import X.C237212t;
import X.C240514a;
import X.C243915i;
import X.C251118d;
import X.C25601Aa;
import X.C25611Ab;
import X.C27151Gf;
import X.C28391Mz;
import X.C32991d4;
import X.C35191hP;
import X.C35741ib;
import X.C36821kh;
import X.C40691s6;
import X.C41691tw;
import X.C42961wB;
import X.C48232Fc;
import X.C49512La;
import X.C49522Lb;
import X.C49542Le;
import X.C49572Lh;
import X.C49612Lm;
import X.C49632Lo;
import X.C49642Lp;
import X.C49652Lq;
import X.C49672Ls;
import X.C88064Ed;
import X.ViewTreeObserver$OnGlobalLayoutListenerC33691ev;
import X.ViewTreeObserver$OnPreDrawListenerC49622Ln;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.HomeActivity;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.collections.observablelistview.ObservableListView;
import com.whatsapp.community.CommunityFragment;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.profile.ProfilePhotoReminder;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class HomeActivity extends AbstractActivityC14700lu implements AbstractC14710lv, AbstractC14720lw, AbstractC14730lx, AbstractC14740ly, AbstractC14750lz, AbstractC14760m0 {
    public static final int A25;
    public static final int A26;
    public static final int A27;
    public static final int A28;
    public static final List A29 = new ArrayList();
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 200;
    public long A04;
    public long A05 = 0;
    public AnimatorSet A06;
    public BroadcastReceiver A07;
    public View A08;
    public View A09;
    public View A0A;
    public View A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public ViewGroup A0F;
    public AbstractC009504t A0G;
    public SearchView A0H;
    public Toolbar A0I;
    public AnonymousClass01E A0J;
    public AnonymousClass07M A0K;
    public CameraHomeFragment A0L;
    public TabsPager A0M;
    public C14790m3 A0N;
    public C14930mH A0O;
    public PagerSlidingTabStrip A0P;
    public C20640w5 A0Q;
    public AnonymousClass1FO A0R;
    public C21740xu A0S;
    public C20670w8 A0T;
    public C18330sH A0U;
    public C15820nx A0V;
    public C251118d A0W;
    public C16430p0 A0X;
    public C25601Aa A0Y;
    public AnonymousClass1AV A0Z;
    public C20970wc A0a;
    public AnonymousClass1s8 A0b;
    public AnonymousClass2GJ A0c;
    public C243915i A0d;
    public C22640zP A0e;
    public C21220x4 A0f;
    public C15550nR A0g;
    public C15610nY A0h;
    public AnonymousClass10T A0i;
    public AnonymousClass1J1 A0j;
    public C21270x9 A0k;
    public C17010q7 A0l;
    public AnonymousClass19D A0m;
    public AnonymousClass11P A0n;
    public C17050qB A0o;
    public C16590pI A0p;
    public C18360sK A0q;
    public C15890o4 A0r;
    public C20650w6 A0s;
    public C19990v2 A0t;
    public C20830wO A0u;
    public C27151Gf A0v = null;
    public C21320xE A0w;
    public C15680nj A0x;
    public C15650ng A0y;
    public C15240mn A0z;
    public C20040v7 A10;
    public AnonymousClass12H A11;
    public C16490p7 A12;
    public C20030v6 A13;
    public C22050yP A14;
    public C16120oU A15;
    public C20710wC A16;
    public C19890uq A17;
    public C14840m8 A18;
    public C20220vP A19;
    public C20230vQ A1A;
    public C22630zO A1B;
    public C22710zW A1C;
    public C17070qD A1D;
    public C14910mF A1E;
    public AnonymousClass2GK A1F;
    public C22660zR A1G;
    public C18910tG A1H;
    public SearchViewModel A1I;
    public C49512La A1J;
    public AnonymousClass1AY A1K;
    public AnonymousClass17R A1L;
    public C25611Ab A1M;
    public C20810wM A1N;
    public AnonymousClass1AZ A1O;
    public AnonymousClass1AU A1P;
    public AnonymousClass1AM A1Q;
    public C240514a A1R;
    public AnonymousClass12F A1S;
    public AnonymousClass19H A1T;
    public C88064Ed A1U;
    public C20780wJ A1V;
    public AnonymousClass12U A1W;
    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev A1X;
    public C14920mG A1Y;
    public C21880y8 A1Z;
    public C21300xC A1a;
    public AnonymousClass1AW A1b;
    public C236712o A1c = null;
    public C21260x8 A1d;
    public AnonymousClass19Z A1e;
    public C237212t A1f;
    public C21280xA A1g;
    public AnonymousClass01H A1h;
    public AnonymousClass01H A1i;
    public AnonymousClass01H A1j;
    public AnonymousClass01H A1k;
    public AnonymousClass01H A1l;
    public AnonymousClass01H A1m;
    public boolean A1n;
    public boolean A1o;
    public boolean A1p;
    public boolean A1q;
    public final Rect A1r = new Rect();
    public final View.OnLayoutChangeListener A1s = new AnonymousClass2LG(this);
    public final Interpolator A1t = new AnonymousClass078();
    public final AnonymousClass2LB A1u = new AnonymousClass2LB();
    public final AnonymousClass2LE A1v = new AnonymousClass2LE(this);
    public final AbstractC32851cq A1w = new AnonymousClass2LD(this);
    public final AbstractC18900tF A1x = new AbstractC18900tF() { // from class: X.2LC
        @Override // X.AbstractC18900tF
        public final void ASC() {
            HomeActivity.this.A1p = true;
        }
    };
    public final AbstractC14780m2 A1y = new AbstractC14780m2() { // from class: X.2LF
        @Override // X.AbstractC14780m2
        public final boolean AM3() {
            String str;
            HomeActivity homeActivity = HomeActivity.this;
            C15230mm r2 = ((ActivityC13830kP) homeActivity).A02;
            int i = homeActivity.A03;
            if (i != 100) {
                str = i != 300 ? i != 400 ? i != 500 ? i != 600 ? "chat" : "community" : "business_home" : "calls" : "status";
            } else {
                str = "camera";
            }
            if (r2.A03 != null && r2.A0G.A07(1807)) {
                r2.A03.A0A("tabs", str, true);
            }
            ((ActivityC13830kP) homeActivity).A02.A00();
            return false;
        }
    };
    public final Runnable A1z = new RunnableBRunnable0Shape0S0100000_I0(this, 45);
    public final Runnable A20 = new RunnableBRunnable0Shape1S0100000_I0_1(this, 1);
    public final Runnable A21 = new RunnableBRunnable0Shape1S0100000_I0_1(this, 4);
    public final Runnable A22 = new RunnableBRunnable0Shape0S0100000_I0(this, 41);
    public final Runnable A23 = new RunnableBRunnable0Shape0S0100000_I0(this, 42);
    public final Random A24 = new Random();

    @Override // X.ActivityC13830kP
    public boolean A21() {
        return true;
    }

    @Override // X.ActivityC13790kL
    public boolean A2c() {
        return true;
    }

    @Override // X.AbstractC14720lw
    public void ASg() {
    }

    @Override // X.AbstractC14720lw
    public boolean Add() {
        return false;
    }

    static {
        int i = Build.VERSION.SDK_INT;
        int i2 = 250;
        int i3 = 220;
        if (i >= 21) {
            i3 = 250;
        }
        A27 = i3;
        int i4 = 220;
        if (i >= 21) {
            i4 = 250;
        }
        A28 = i4;
        int i5 = 220;
        if (i >= 21) {
            i5 = 500;
        }
        A25 = i5;
        if (i < 21) {
            i2 = 220;
        }
        A26 = i2;
    }

    public static int A02(AnonymousClass018 r2, int i) {
        boolean z = !r2.A04().A06;
        List list = A29;
        if (z) {
            return list.indexOf(Integer.valueOf(i));
        }
        return (list.size() - list.indexOf(Integer.valueOf(i))) - 1;
    }

    public static final Pair A03(int i, int i2, int i3, int i4) {
        double d;
        double sqrt = Math.sqrt((double) ((i2 * i2) + (i3 * i3)));
        if (Build.VERSION.SDK_INT >= 21) {
            d = sqrt / ((double) i3);
        } else {
            d = 1.0d;
        }
        double d2 = ((double) i) / d;
        int i5 = (int) ((((double) i4) / ((double) i3)) * d2);
        return new Pair(Integer.valueOf(i5), Integer.valueOf((int) (d2 - ((double) i5))));
    }

    public static void A09(Context context, Intent intent, C15550nR r9, C20650w6 r10, C19990v2 r11, C15650ng r12) {
        String str;
        String str2;
        String str3;
        AbstractC14640lm A01;
        JSONException e;
        if ("android.nfc.action.NDEF_DISCOVERED".equals(intent.getAction())) {
            Log.i("newchatnfc/processnfcintent");
            NdefMessage ndefMessage = (NdefMessage) intent.getParcelableArrayExtra("android.nfc.extra.NDEF_MESSAGES")[0];
            if ("application/com.whatsapp.chat".equals(new String(ndefMessage.getRecords()[0].getType(), Charset.forName("US-ASCII")))) {
                String str4 = new String(ndefMessage.getRecords()[0].getPayload(), Charset.forName("US-ASCII"));
                String str5 = null;
                try {
                    JSONObject jSONObject = new JSONObject(str4);
                    str2 = jSONObject.getString("jid");
                    try {
                        str = jSONObject.getString("id");
                        try {
                            str3 = jSONObject.getString("name");
                        } catch (JSONException e2) {
                            e = e2;
                            Log.e("newchatnfc/processnfcintent", e);
                            str3 = null;
                            A01 = AbstractC14640lm.A01(str2);
                            if (A01 != null) {
                            }
                            StringBuilder sb = new StringBuilder("newchatnfc/processnfcintent jid is invalid:");
                            sb.append(C15380n4.A03(A01));
                            sb.append(" id:");
                            sb.append(str);
                            Log.i(sb.toString());
                            return;
                        }
                    } catch (JSONException e3) {
                        e = e3;
                        str = null;
                    }
                } catch (JSONException e4) {
                    e = e4;
                    str2 = null;
                    str = null;
                }
                A01 = AbstractC14640lm.A01(str2);
                if (A01 != null || str == null) {
                    StringBuilder sb = new StringBuilder("newchatnfc/processnfcintent jid is invalid:");
                    sb.append(C15380n4.A03(A01));
                    sb.append(" id:");
                    sb.append(str);
                    Log.i(sb.toString());
                    return;
                }
                if (!r11.A0D(A01)) {
                    C15370n3 A0A = r9.A0A(A01);
                    if (A0A == null || A0A.A0C == null) {
                        str5 = str3;
                    }
                    r10.A04(A01, new RunnableBRunnable0Shape10S0200000_I1(r12, 32, A01), str5);
                }
                Intent A0i = new C14960mK().A0i(context, A01);
                C35741ib.A00(A0i, "NewChatNfc:processNfcIntent");
                context.startActivity(A0i);
            }
        }
    }

    public static void A0A(View view, AnonymousClass01E r6) {
        ActivityC000900k A0B = r6.A0B();
        if (A0B instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) A0B;
            ViewGroup viewGroup = (ViewGroup) view.findViewById(16908290);
            viewGroup.setPadding(0, homeActivity.A2e(), 0, 0);
            viewGroup.setClipToPadding(false);
            viewGroup.setClipChildren(false);
            ObservableListView observableListView = (ObservableListView) view.findViewById(16908298);
            AnonymousClass2LL r1 = new AnonymousClass2LL(homeActivity);
            AnonymousClass028.A0a(r1, 2);
            observableListView.addHeaderView(r1, null, false);
            observableListView.A09 = homeActivity;
        }
    }

    public static void A0B(View view, AnonymousClass01E r3, int i) {
        ActivityC000900k A0B = r3.A0B();
        if (A0B instanceof HomeActivity) {
            ListView listView = (ListView) view.findViewById(16908298);
            AnonymousClass2LH r2 = new AnonymousClass2LH(A0B, i);
            AnonymousClass028.A0a(r2, 2);
            if (listView != null) {
                listView.addFooterView(r2, null, false);
            }
        }
    }

    public static /* synthetic */ void A0D(HomeActivity homeActivity) {
        if (Build.VERSION.SDK_INT >= 21) {
            homeActivity.A2m();
        }
    }

    @Override // X.ActivityC000900k
    public void A0g(Intent intent, Bundle bundle, AnonymousClass01E r8, int i) {
        SearchFragment searchFragment;
        if (intent.getLongExtra("row_id", -1) == -1) {
            if (A33()) {
                if (A35() && (searchFragment = (SearchFragment) A0V().A0A("search_fragment")) != null) {
                    searchFragment.A1G.A0O(1);
                }
            } else if (this.A0D.getVisibility() == 0) {
                ((ActivityC13810kN) this).A05.A0J(new RunnableBRunnable0Shape0S0100000_I0(this, 43), (long) getResources().getInteger(17694722));
            }
        }
        super.A0g(intent, bundle, r8, i);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j
    public AbstractC009504t A1W(AnonymousClass02Q r5) {
        this.A0G = super.A1W(r5);
        if (!A36()) {
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setObjectValues(Integer.valueOf(AnonymousClass00T.A00(this, R.color.primary)), Integer.valueOf(AnonymousClass00T.A00(this, R.color.action_mode)));
            valueAnimator.setEvaluator(new ArgbEvaluator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.2Md
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    HomeActivity homeActivity = HomeActivity.this;
                    homeActivity.A0P.setBackgroundColor(((Number) valueAnimator2.getAnimatedValue()).intValue());
                    int i = 0;
                    while (true) {
                        C14790m3 r1 = homeActivity.A0N;
                        if (i < r1.A00) {
                            r1.A0H(i).A05.setTextColor(((Number) valueAnimator2.getAnimatedValue()).intValue());
                            i++;
                        } else {
                            return;
                        }
                    }
                }
            });
            valueAnimator.start();
        }
        C41691tw.A02(this, R.color.action_mode_dark);
        AnonymousClass12P.A04(findViewById(R.id.action_mode_bar), getWindowManager());
        AnonymousClass2IW r2 = (AnonymousClass2IW) ((C18980tN) this.A1i.get()).A00(AnonymousClass2IW.class);
        boolean z = false;
        if (this.A0G != null) {
            z = true;
        }
        r2.A00.setShouldHideBanner(z);
        return this.A0G;
    }

    @Override // X.ActivityC13830kP
    public void A20() {
        boolean z;
        synchronized (ProfilePhotoReminder.class) {
            z = ProfilePhotoReminder.A0N;
        }
        if (!z && ((ActivityC13810kN) this).A07.A0B()) {
            C14820m6 r7 = ((ActivityC13810kN) this).A09;
            C20640w5 r0 = this.A0Q;
            synchronized (ProfilePhotoReminder.class) {
                boolean z2 = false;
                if (!r0.A03()) {
                    long j = ProfilePhotoReminder.A0M;
                    if (j < 0) {
                        j = r7.A00.getLong("wa_last_reminder_timestamp", -1);
                        ProfilePhotoReminder.A0M = j;
                    }
                    if (j >= 0) {
                        if ((new Date(System.currentTimeMillis()).getTime() - new Date(ProfilePhotoReminder.A0M).getTime()) / 86400000 >= 90) {
                            z2 = true;
                        }
                    }
                }
                if (!z2) {
                    return;
                }
            }
            C15570nT r02 = ((ActivityC13790kL) this).A01;
            r02.A08();
            if (r02.A01 != null) {
                AnonymousClass10T r1 = this.A0i;
                C15570nT r03 = ((ActivityC13790kL) this).A01;
                r03.A08();
                File A01 = r1.A01(r03.A01);
                AnonymousClass009.A05(A01);
                if (A01.exists()) {
                    return;
                }
            }
            ((ActivityC13790kL) this).A01.A08();
            ProfilePhotoReminder.A02(this.A0Q, ((ActivityC13810kN) this).A09);
            ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape0S0100000_I0(this, 47));
        }
    }

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (A2f(this.A0M.getCurrentItem()) != 100) {
            return;
        }
        if (i == R.string.permission_storage_need_access || i == R.string.record_need_sd_card_message || i == R.string.record_need_sd_card_message_shared_storage || i == R.string.error_no_disc_space || i == R.string.insufficient_storage_dialog_subtitle) {
            this.A0M.A0F(A02(((ActivityC13830kP) this).A01, 200), true);
        }
    }

    public final int A2e() {
        int i = 1;
        if (Build.VERSION.SDK_INT >= 22) {
            i = 0;
        }
        return A29.size() > 1 ? i + getResources().getDimensionPixelSize(R.dimen.actionbar_height) : i;
    }

    public final int A2f(int i) {
        Object obj;
        boolean z = !((ActivityC13830kP) this).A01.A04().A06;
        List list = A29;
        if (z) {
            obj = list.get(i);
        } else {
            obj = list.get((list.size() - i) - 1);
        }
        return ((Number) obj).intValue();
    }

    public final Animator A2g(int i) {
        View view;
        View findViewById;
        AnonymousClass01E A2h = A2h();
        if (A2h == null || (view = A2h.A0A) == null || (findViewById = view.findViewById(16908290)) == null) {
            return null;
        }
        findViewById.clearAnimation();
        ValueAnimator ofInt = ValueAnimator.ofInt((int) findViewById.getTranslationY(), i);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(findViewById) { // from class: X.2LV
            public final /* synthetic */ View A00;

            {
                this.A00 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                this.A00.setTranslationY((float) ((Number) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        return ofInt;
    }

    public final AnonymousClass01E A2h() {
        return (AnonymousClass01E) A2i(A02(((ActivityC13830kP) this).A01, this.A03));
    }

    public final AbstractC14770m1 A2i(int i) {
        boolean z;
        int A2f = A2f(i);
        for (AnonymousClass01E r1 : A23()) {
            if (A2f != 100) {
                if (A2f == 200) {
                    z = r1 instanceof ConversationsFragment;
                } else if (A2f == 300) {
                    z = r1 instanceof StatusesFragment;
                } else if (A2f == 400) {
                    z = r1 instanceof CallsHistoryFragment;
                } else if (A2f != 500 && A2f == 600 && (r1 instanceof CommunityFragment)) {
                    return (CommunityFragment) r1;
                }
                if (z) {
                    return (AbstractC14770m1) r1;
                }
            } else if (r1 instanceof CameraHomeFragment) {
                return (CameraHomeFragment) r1;
            }
        }
        return null;
    }

    public final ObservableListView A2j() {
        View view;
        AnonymousClass01E A2h = A2h();
        if (A2h == null || (view = A2h.A0A) == null) {
            return null;
        }
        return (ObservableListView) view.findViewById(16908298);
    }

    public final void A2k() {
        C14810m5 A0H = this.A0N.A0H(A02(((ActivityC13830kP) this).A01, 300));
        if (A0H.A00 != 0) {
            A0H.A00 = 0;
            A2s();
        }
        long j = this.A04;
        if (j != 0) {
            ((ActivityC13810kN) this).A09.A00.edit().putLong("last_notified_status_row_id", j).apply();
        }
    }

    public final void A2l() {
        DialogFragment dialogFragment;
        Dialog dialog;
        String str = "migration_remove_dialog";
        AnonymousClass01E A0A = A0V().A0A(str);
        if (!(A0A instanceof DialogFragment) || A0A == null) {
            str = "migration_update_dialog";
        }
        AnonymousClass01E A0A2 = A0V().A0A(str);
        if ((A0A2 instanceof DialogFragment) && (dialogFragment = (DialogFragment) A0A2) != null && (dialog = dialogFragment.A03) != null && dialog.isShowing()) {
            dialogFragment.A1B();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r2.A0B.getVisibility() != 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2m() {
        /*
            r2 = this;
            X.0m9 r0 = r2.A0C
            boolean r0 = X.AnonymousClass1SF.A0P(r0)
            if (r0 == 0) goto L_0x0011
            android.view.View r0 = r2.A0B
            int r1 = r0.getVisibility()
            r0 = 1
            if (r1 == 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            r1 = 0
            if (r0 == 0) goto L_0x0023
            android.view.Window r0 = r2.getWindow()
            X.C41691tw.A07(r0, r1)
            r1 = 2131099863(0x7f0600d7, float:1.7812091E38)
        L_0x001f:
            X.C41691tw.A02(r2, r1)
            return
        L_0x0023:
            boolean r0 = r2.A33()
            if (r0 == 0) goto L_0x002f
            boolean r0 = r2.A35()
            if (r0 != 0) goto L_0x0035
        L_0x002f:
            boolean r0 = r2.A36()
            if (r0 == 0) goto L_0x0042
        L_0x0035:
            boolean r0 = X.C28391Mz.A03()
            if (r0 == 0) goto L_0x0042
            r0 = 2131100793(0x7f060479, float:1.7813977E38)
            X.C41691tw.A03(r2, r0)
            return
        L_0x0042:
            android.view.Window r0 = r2.getWindow()
            X.C41691tw.A07(r0, r1)
            X.04t r0 = r2.A0G
            r1 = 2131100698(0x7f06041a, float:1.7813785E38)
            if (r0 == 0) goto L_0x001f
            r1 = 2131099686(0x7f060026, float:1.7811732E38)
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.A2m():void");
    }

    public final void A2n() {
        AbstractC14940mI r2;
        AbstractC14770m1 A2i = A2i(A02(((ActivityC13830kP) this).A01, this.A03));
        if ((A2i instanceof AbstractC14940mI) && (r2 = (AbstractC14940mI) A2i) != null) {
            C14930mH r1 = this.A0O;
            r1.A00 = false;
            r1.A02(r2);
        }
    }

    public final void A2o() {
        this.A05 = System.currentTimeMillis();
        C18910tG r1 = this.A1H;
        synchronized (r1) {
            r1.A00 = UUID.randomUUID().toString();
        }
        A2x(1);
        AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], R.string.seamless_migration_update_dialog_description);
        A01.A05 = R.string.seamless_migration_update_dialog_title;
        A01.A0B = new Object[0];
        A01.A02(new DialogInterface.OnClickListener() { // from class: X.2MY
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                HomeActivity homeActivity = HomeActivity.this;
                homeActivity.A2x(2);
                C18910tG r5 = homeActivity.A1H;
                synchronized (r5) {
                    C14820m6 r3 = r5.A05;
                    if (r3.A00.getBoolean("seamless_migration_in_progress", false)) {
                        Log.e("SeamlessManager_initializeSeamlessMigration/Already in process. Can't start another one.");
                    } else {
                        Log.i("SeamlessManager_initializeSeamlessMigration/Seamless Migration initiated.");
                        r3.A18(true);
                        if (r5.A00 == null) {
                            r5.A00 = UUID.randomUUID().toString();
                        }
                        r5.A06.A03(r5.A0A);
                        C22230yk r2 = r5.A09;
                        String str = r5.A00;
                        if (r2.A05.A06 && r2.A0G.A02()) {
                            r2.A0C.A08(Message.obtain(null, 0, 284, 0, new C49592Lk(str)), false);
                        }
                        r5.A0B.AbK(r5.A0C, "SeamlessManager/initSeamlessMigration", 100000);
                        r5.A01 = true;
                    }
                }
            }
        }, R.string.seamless_migration_update_dialog_confirm);
        AnonymousClass2MZ r12 = new DialogInterface.OnClickListener() { // from class: X.2MZ
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                HomeActivity homeActivity = HomeActivity.this;
                homeActivity.A2x(3);
                AnonymousClass2AC A012 = MessageDialogFragment.A01(new Object[0], R.string.seamless_migration_update_confirmation_dialog_description);
                A012.A05 = R.string.seamless_migration_update_confirmation_dialog_title;
                A012.A0B = new Object[0];
                A012.A02(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0025: INVOKE  
                      (r2v0 'A012' X.2AC)
                      (wrap: X.4fL : 0x0022: CONSTRUCTOR  (r0v4 X.4fL A[REMOVE]) = (r4v0 'homeActivity' com.whatsapp.HomeActivity) call: X.4fL.<init>(com.whatsapp.HomeActivity):void type: CONSTRUCTOR)
                      (wrap: int : ?: SGET   com.whatsapp.R.string.seamless_migration_update_confirmation_dialog_confirm int)
                     type: VIRTUAL call: X.2AC.A02(android.content.DialogInterface$OnClickListener, int):void in method: X.2MZ.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0022: CONSTRUCTOR  (r0v4 X.4fL A[REMOVE]) = (r4v0 'homeActivity' com.whatsapp.HomeActivity) call: X.4fL.<init>(com.whatsapp.HomeActivity):void type: CONSTRUCTOR in method: X.2MZ.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.4fL, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    com.whatsapp.HomeActivity r4 = com.whatsapp.HomeActivity.this
                    r0 = 3
                    java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                    r4.A2x(r0)
                    r1 = 2131891426(0x7f1214e2, float:1.9417572E38)
                    r3 = 0
                    java.lang.Object[] r0 = new java.lang.Object[r3]
                    X.2AC r2 = com.whatsapp.MessageDialogFragment.A01(r0, r1)
                    java.lang.Object[] r1 = new java.lang.Object[r3]
                    r0 = 2131891427(0x7f1214e3, float:1.9417574E38)
                    r2.A05 = r0
                    r2.A0B = r1
                    r1 = 2131891425(0x7f1214e1, float:1.941757E38)
                    X.4fL r0 = new X.4fL
                    r0.<init>(r4)
                    r2.A02(r0, r1)
                    X.4fK r1 = new X.4fK
                    r1.<init>(r4)
                    r0 = 2131891424(0x7f1214e0, float:1.9417568E38)
                    r2.A04 = r0
                    r2.A07 = r1
                    androidx.fragment.app.DialogFragment r2 = r2.A01()
                    r2.A1G(r3)
                    X.01F r1 = r4.A0V()
                    java.lang.String r0 = "migration_remove_dialog"
                    r2.A1F(r1, r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2MZ.onClick(android.content.DialogInterface, int):void");
            }
        };
        A01.A04 = R.string.seamless_migration_update_dialog_remove;
        A01.A07 = r12;
        DialogFragment A012 = A01.A01();
        A012.A1G(false);
        A012.A1F(A0V(), "migration_update_dialog");
    }

    public final void A2p() {
        if (this.A08.getTranslationY() != 0.0f) {
            this.A08.animate().cancel();
            this.A08.animate().translationY(0.0f).setDuration(250).start();
            this.A01 = 0;
        }
        A30(true);
    }

    public final void A2q() {
        this.A1o = false;
        AnonymousClass01F A0V = A0V();
        if (!A0V.A0m()) {
            SearchFragment searchFragment = (SearchFragment) A0V.A0A("search_fragment");
            if (searchFragment != null) {
                ActivityC000900k A0B = searchFragment.A0B();
                if (A0B != null) {
                    C41691tw.A07(A0B.getWindow(), false);
                }
                searchFragment.A1G.A0X(false);
            }
            A0V().A0g("search_fragment", 1);
            this.A0F.setVisibility(8);
            SearchViewModel searchViewModel = this.A1I;
            if (searchViewModel != null) {
                searchViewModel.A0P(0);
                this.A1I.A0O(4);
            }
        }
    }

    public final void A2r() {
        int i;
        int size;
        C15680nj r0 = this.A0x;
        C19990v2 r5 = r0.A00;
        r5.A0B();
        ArrayList arrayList = r0.A01;
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            i = 0;
            while (it.hasNext()) {
                AnonymousClass1W2 r1 = (AnonymousClass1W2) it.next();
                if (!r5.A0E(r1.A01) && r5.A00(r1.A01) != 0) {
                    i++;
                }
            }
        }
        this.A0N.A0H(A02(((ActivityC13830kP) this).A01, 200)).A00 = i;
        List list = A29;
        if (list.contains(400)) {
            C14810m5 A0H = this.A0N.A0H(A02(((ActivityC13830kP) this).A01, 400));
            C237212t r02 = this.A1f;
            r02.A00();
            if (r02.A00.size() > 0) {
                C237212t r03 = this.A1f;
                r03.A00();
                size = r03.A00.size();
            } else {
                C20230vQ r04 = this.A1A;
                r04.A03();
                size = r04.A00.size();
            }
            A0H.A00 = size;
        }
        if (list.contains(600) && list.contains(600)) {
            C14810m5 A0H2 = this.A0N.A0H(A02(((ActivityC13830kP) this).A01, 600));
            ((ActivityC13810kN) this).A05.A0G(this.A1z);
            int i2 = 0;
            if (((ActivityC13810kN) this).A09.A00.getLong("last_community_activity", 0) > ((ActivityC13810kN) this).A09.A00.getLong("last_seen_community_activity", 0)) {
                i2 = 1;
            }
            A0H2.A00 = i2;
        }
        A2s();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0073, code lost:
        if (r0.A00.size() > 0) goto L_0x0075;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x004c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2s() {
        /*
        // Method dump skipped, instructions count: 256
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.A2s():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00df, code lost:
        if (r4 != null) goto L_0x00af;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2t(android.content.Intent r10) {
        /*
        // Method dump skipped, instructions count: 264
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.A2t(android.content.Intent):void");
    }

    public final void A2u(Intent intent) {
        if (intent.getBooleanExtra("show_mute", false)) {
            this.A0d.A01 = 8;
            AbstractC14640lm A01 = AbstractC14640lm.A01(intent.getStringExtra("mute_jid"));
            AnonymousClass009.A05(A01);
            MuteDialogFragment.A01(Collections.singleton(A01)).A1F(A0V(), null);
        }
    }

    public final void A2v(Intent intent) {
        Uri data = intent.getData();
        if (C42961wB.A00(data)) {
            C15550nR r2 = this.A0g;
            AnonymousClass009.A0E(C42961wB.A00(data));
            C15370n3 A06 = r2.A06(ContentUris.parseId(data));
            AbstractC14770m1 A2i = A2i(A02(((ActivityC13830kP) this).A01, this.A03));
            if (A06 != null && (A2i instanceof ConversationsFragment)) {
                ((ConversationsFragment) A2i).A1Q(A06);
            }
        }
    }

    public final void A2w(DialogFragment dialogFragment) {
        this.A19.A03 = true;
        this.A17.A0G(true);
        Adm(dialogFragment);
    }

    public final void A2x(Integer num) {
        long currentTimeMillis = System.currentTimeMillis();
        Long valueOf = Long.valueOf((currentTimeMillis - this.A05) / 1000);
        C18910tG r2 = this.A1H;
        AnonymousClass2KN r1 = new AnonymousClass2KN();
        r1.A00 = num;
        r1.A03 = Long.valueOf(currentTimeMillis / 1000);
        r1.A01 = valueOf;
        r2.A02(r1.A00());
    }

    public final void A2y(String str) {
        int width;
        int i;
        View view;
        View findViewById;
        int i2;
        AbstractC14940mI r2;
        AbstractC14770m1 A2i = A2i(A02(((ActivityC13830kP) this).A01, this.A03));
        if (A2i != null && A2i.Aed() && !A36()) {
            View findViewById2 = findViewById(R.id.menuitem_search);
            if (findViewById2 != null) {
                int[] iArr = new int[2];
                findViewById2.getLocationInWindow(iArr);
                width = iArr[0] + (findViewById2.getWidth() / 2);
                i = findViewById2.getTop() + (findViewById2.getHeight() / 2);
            } else {
                width = this.A08.getWidth();
                i = 0;
            }
            if (this.A0z.A04() != 5 || (i2 = this.A03) != 200) {
                if (this.A0H == null) {
                    C48232Fc.A00(this.A0D);
                    getLayoutInflater().inflate(R.layout.home_search_view_layout, (ViewGroup) this.A0D, true);
                    SearchView searchView = (SearchView) this.A0D.findViewById(R.id.search_view);
                    this.A0H = searchView;
                    TextView textView = (TextView) AnonymousClass028.A0D(searchView, R.id.search_src_text);
                    textView.setTextColor(AnonymousClass00T.A00(this, R.color.search_text_color));
                    textView.setHintTextColor(AnonymousClass00T.A00(this, R.color.hint_text));
                    this.A0H.setIconifiedByDefault(false);
                    this.A0H.setQueryHint(getString(R.string.search_hint));
                    SearchView searchView2 = this.A0H;
                    searchView2.A0B = new AnonymousClass2Ma(this);
                    ((ImageView) searchView2.findViewById(R.id.search_mag_icon)).setImageDrawable(new AnonymousClass2Mb(AnonymousClass00T.A04(this, R.drawable.ic_back), this));
                    this.A0H.A07 = new ViewOnClickCListenerShape0S0100000_I0(this, 10);
                    if (!AnonymousClass1US.A0C(str)) {
                        this.A0H.A0F(str);
                    }
                    ImageView imageView = (ImageView) this.A0D.findViewById(R.id.search_back);
                    SearchView searchView3 = this.A0H;
                    if (!(searchView3 == null || searchView3.getContext() == null)) {
                        imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A01(this.A0H.getContext(), R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), ((ActivityC13830kP) this).A01));
                    }
                    imageView.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 9));
                }
                this.A0D.setVisibility(0);
                int i3 = Build.VERSION.SDK_INT;
                View view2 = this.A0D;
                if (i3 < 21) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-view2.getHeight()), 0.0f);
                    translateAnimation.setDuration((long) A27);
                    this.A0D.clearAnimation();
                    this.A0D.startAnimation(translateAnimation);
                } else if (view2.isAttachedToWindow()) {
                    View view3 = this.A0D;
                    Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view3, width, i, 0.0f, (float) view3.getWidth());
                    createCircularReveal.setDuration((long) A27);
                    createCircularReveal.start();
                }
                AnonymousClass01E A2h = A2h();
                if (A2h == null || (view = A2h.A0A) == null || (findViewById = view.findViewById(16908290)) == null) {
                    this.A0H.setIconified(false);
                } else {
                    findViewById.setPadding(0, A2e() - getResources().getDimensionPixelSize(R.dimen.tab_height), 0, 0);
                    findViewById.clearAnimation();
                    TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, (float) getResources().getDimensionPixelSize(R.dimen.tab_height), 0.0f);
                    translateAnimation2.setDuration((long) ((A27 << 2) / 3));
                    translateAnimation2.setAnimationListener(new AnonymousClass2Mc(this));
                    findViewById.startAnimation(translateAnimation2);
                }
                TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-getResources().getDimensionPixelSize(R.dimen.tab_height)));
                translateAnimation3.setDuration((long) A27);
                this.A08.startAnimation(translateAnimation3);
                this.A08.setVisibility(8);
                if (C28391Mz.A02()) {
                    A2m();
                }
            } else if (this.A0F != null) {
                AbstractC14770m1 A2i2 = A2i(A02(((ActivityC13830kP) this).A01, i2));
                if ((A2i2 instanceof AbstractC14940mI) && (r2 = (AbstractC14940mI) A2i2) != null) {
                    C14930mH r1 = this.A0O;
                    r1.A00 = true;
                    r1.A02(r2);
                }
                AnonymousClass01F A0V = A0V();
                AnonymousClass01E A0A = A0V.A0A("search_fragment");
                this.A0F.setVisibility(0);
                if (A0A == null) {
                    C49512La r10 = this.A1J;
                    Random random = r10.A06;
                    long nextLong = random.nextLong();
                    while (r10.A01 == nextLong && r10.A00 < 5) {
                        nextLong = random.nextLong();
                        r10.A00++;
                    }
                    r10.A00 = 0;
                    r10.A01 = nextLong;
                    r10.A00(1, 0);
                    SearchFragment A00 = SearchFragment.A00(Integer.valueOf(A25), Integer.valueOf(A26), width, i);
                    C004902f r12 = new C004902f(A0V);
                    r12.A0H = true;
                    r12.A0B(A00, "search_fragment", R.id.search_fragment_holder);
                    r12.A0F("search_fragment");
                    r12.A01();
                    A0V.A0k(true);
                    A0V.A0J();
                    this.A0F.addOnLayoutChangeListener(this.A1s);
                }
            } else {
                return;
            }
            AnonymousClass01E A2h2 = A2h();
            if (A2h2 instanceof AbstractC33101dL) {
                ((AbstractC33101dL) A2h2).A8v();
            }
        }
    }

    public void A2z(boolean z) {
        int width;
        int i;
        if (A33()) {
            if ((A35() || A34()) && !A0V().A0m()) {
                A31(false);
                A2n();
                SearchFragment searchFragment = (SearchFragment) A0V().A0A("search_fragment");
                this.A0F.setBackgroundResource(0);
                if (!this.A1o) {
                    this.A1o = true;
                    if (searchFragment != null) {
                        if (!z) {
                            A2q();
                        } else {
                            RunnableBRunnable0Shape1S0100000_I0_1 runnableBRunnable0Shape1S0100000_I0_1 = new RunnableBRunnable0Shape1S0100000_I0_1(this, 0);
                            if (searchFragment.A02 != null) {
                                ActivityC000900k A0B = searchFragment.A0B();
                                if (A0B != null) {
                                    C41691tw.A07(A0B.getWindow(), false);
                                }
                                searchFragment.A1D(runnableBRunnable0Shape1S0100000_I0_1, searchFragment.A02.getLeft(), searchFragment.A02.getTop(), searchFragment.A02.getRight(), searchFragment.A02.getBottom(), false);
                            }
                        }
                    }
                    this.A0F.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
                    Animator A2g = A2g(0);
                    int height = this.A0E.getHeight();
                    this.A08.clearAnimation();
                    ValueAnimator ofInt = ValueAnimator.ofInt(height, 0);
                    ofInt.addUpdateListener(new C49542Le(new FrameLayout.LayoutParams(-1, -2), new LinearLayout.LayoutParams(-1, -2), this, false));
                    int A2e = A2e() + getResources().getDimensionPixelSize(R.dimen.tab_height);
                    int width2 = this.A08.getWidth();
                    int height2 = this.A08.getHeight();
                    if (z) {
                        i = A26;
                    } else {
                        i = 0;
                    }
                    Pair A03 = A03(i, height2, width2, A2e);
                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.setStartDelay((long) ((Number) A03.first).intValue());
                    animatorSet.setDuration((long) ((Number) A03.second).intValue());
                    animatorSet.setInterpolator(this.A1t);
                    animatorSet.playTogether(ofInt, A2g);
                    animatorSet.addListener(new C49572Lh(this));
                    animatorSet.start();
                }
            } else {
                return;
            }
        } else if (this.A0D.getVisibility() == 0) {
            this.A1u.A01 = "";
            this.A0H.A0F("");
            if (!z) {
                this.A0H.setIconified(true);
                this.A0D.setVisibility(4);
            } else if (Build.VERSION.SDK_INT >= 21) {
                int width3 = (this.A0D.getWidth() - getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)) - (getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) >> 1);
                View view = this.A0D;
                if (!((ActivityC13830kP) this).A01.A04().A06) {
                    width = width3;
                } else {
                    width = this.A0D.getWidth() - width3;
                }
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, width, this.A0D.getHeight() >> 1, (float) width3, 0.0f);
                createCircularReveal.setDuration((long) A28);
                createCircularReveal.addListener(new AnonymousClass2Mk(this));
                createCircularReveal.start();
            } else {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-this.A0D.getHeight()));
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation);
                animationSet.addAnimation(translateAnimation);
                animationSet.setDuration((long) A28);
                animationSet.setAnimationListener(new C49612Lm(this));
                this.A0D.startAnimation(animationSet);
            }
            A31(z);
        } else {
            return;
        }
        AbstractC14770m1 A2i = A2i(A02(((ActivityC13830kP) this).A01, this.A03));
        if (A2i != null) {
            A2i.Acn(false);
        }
    }

    public final void A30(boolean z) {
        AnonymousClass01E r0;
        View view;
        ObservableListView observableListView;
        int height = this.A0I.getHeight();
        for (int i = 0; i < this.A0N.A00; i++) {
            if (!(i == this.A0M.getCurrentItem() || (r0 = (AnonymousClass01E) A2i(i)) == null || (view = r0.A0A) == null || (observableListView = (ObservableListView) view.findViewById(16908298)) == null)) {
                int i2 = observableListView.A04;
                if (z) {
                    if (i2 > 0) {
                        observableListView.setSelection(0);
                    }
                } else if (i2 < height) {
                    observableListView.setSelection(1);
                }
            }
        }
    }

    public final void A31(boolean z) {
        View view;
        View view2;
        this.A08.setVisibility(0);
        AnonymousClass01E A2h = A2h();
        if (A2h == null || (view2 = A2h.A0A) == null) {
            view = null;
        } else {
            view = view2.findViewById(16908290);
            if (view != null) {
                view.setPadding(0, A2e(), 0, 0);
            }
        }
        if (z) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-getResources().getDimensionPixelSize(R.dimen.tab_height)), 0.0f);
            translateAnimation.setDuration((long) A28);
            this.A08.startAnimation(translateAnimation);
            if (view != null) {
                view.startAnimation(translateAnimation);
            }
        }
        if (!AnonymousClass1SF.A0P(((ActivityC13810kN) this).A0C) || this.A0B.getVisibility() != 0) {
            C41691tw.A07(getWindow(), false);
            C41691tw.A02(this, R.color.primary);
        }
        AnonymousClass01E A2h2 = A2h();
        if (A2h2 instanceof AbstractC33101dL) {
            ((AbstractC33101dL) A2h2).A9K();
        }
    }

    public final boolean A32() {
        String A02;
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (r0.A00 != null) {
            C16490p7 r02 = this.A12;
            r02.A04();
            if (r02.A01 && ((ActivityC13790kL) this).A0B.A01()) {
                return true;
            }
        }
        Log.i("home/create/no-me-or-msgstore-db");
        ((ActivityC13810kN) this).A03.AaV("home/conversations bounced to main", null, false);
        Intent intent = getIntent();
        if ("android.intent.action.VIEW".equals(intent.getAction()) && (A02 = AcceptInviteLinkActivity.A02(intent.getData())) != null && ((ActivityC13810kN) this).A0C.A07(1700)) {
            ((ActivityC13810kN) this).A09.A00.edit().putString("invite_code_from_referrer", A02).apply();
            ((ActivityC13810kN) this).A09.A00.edit().putLong("referrer_clicked_time", TimeUnit.MILLISECONDS.toSeconds(((ActivityC13790kL) this).A05.A00())).apply();
        }
        startActivity(C14960mK.A04(this));
        finish();
        return false;
    }

    public final boolean A33() {
        return this.A0z.A04() == 5 && this.A03 == 200;
    }

    public final boolean A34() {
        AnonymousClass01E A0A;
        AnonymousClass009.A0A("Mismatch search mode", A33());
        AnonymousClass01F A0V = A0V();
        if (A0V.A0A("search_fragment") == null || (A0A = A0V.A0A("media_view_fragment")) == null || !A0A.A0e()) {
            return false;
        }
        return true;
    }

    public final boolean A35() {
        AnonymousClass01E A0A;
        AnonymousClass009.A0A("Mismatch search mode", A33());
        AnonymousClass01F A0V = A0V();
        if (A0V.A03() != 1 || (A0A = A0V.A0A("search_fragment")) == null || !A0A.A0e()) {
            return false;
        }
        return true;
    }

    public final boolean A36() {
        if (A33()) {
            return A35();
        }
        return this.A0D.getVisibility() == 0;
    }

    public final boolean A37(Intent intent) {
        int i;
        Uri data = intent.getData();
        if ("com.whatsapp.intent.action.CALLS".equals(intent.getAction())) {
            i = 400;
        } else if ("com.whatsapp.intent.action.CHATS".equals(intent.getAction())) {
            i = 200;
        } else if (!"com.whatsapp.intent.action.STATUSES".equals(intent.getAction()) && (!"android.intent.action.VIEW".equals(intent.getAction()) || data == null || !"whatsapp".equals(data.getScheme()) || !"status".equals(data.getHost()))) {
            return false;
        } else {
            i = 300;
        }
        this.A0M.A0F(A02(((ActivityC13830kP) this).A01, i), false);
        this.A03 = i;
        return true;
    }

    @Override // X.AbstractC14710lv
    public AnonymousClass1s8 ABD() {
        return this.A0b;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A01;
    }

    @Override // X.AbstractC14740ly
    public SearchViewModel AGU(AnonymousClass1J1 r42) {
        C16590pI r0 = this.A0p;
        C14850m9 r02 = ((ActivityC13810kN) this).A0C;
        C14900mE r03 = ((ActivityC13810kN) this).A05;
        AbstractC14440lR r04 = ((ActivityC13830kP) this).A05;
        AbstractC15710nm r05 = ((ActivityC13810kN) this).A03;
        C20040v7 r06 = this.A10;
        C15550nR r07 = this.A0g;
        AnonymousClass1AW r08 = this.A1b;
        C15610nY r09 = this.A0h;
        AnonymousClass018 r010 = ((ActivityC13830kP) this).A01;
        C15240mn r15 = this.A0z;
        AnonymousClass12H r14 = this.A11;
        AnonymousClass12F r13 = this.A1S;
        C22050yP r12 = this.A14;
        C240514a r11 = this.A1R;
        AnonymousClass1AY r10 = this.A1K;
        C251118d r9 = this.A0W;
        C15680nj r8 = this.A0x;
        AnonymousClass1AM r7 = this.A1Q;
        C49512La r6 = this.A1J;
        C20030v6 r5 = this.A13;
        return (SearchViewModel) new AnonymousClass02A(new C49522Lb(this, r05, r03, r9, this.A0Y, r07, r09, r42, r0, r010, this.A0u, r8, r15, r06, r14, r5, r02, r12, ((ActivityC13830kP) this).A03, r6, r10, r7, r11, r13, r08, r04), this).A00(SearchViewModel.class);
    }

    @Override // X.AbstractC14720lw
    public void APJ() {
        SearchFragment searchFragment = (SearchFragment) A0V().A0A("search_fragment");
        if (searchFragment != null) {
            searchFragment.A1G.A0P(4);
        }
    }

    @Override // X.AbstractC14720lw
    public void ASf() {
        SearchFragment searchFragment = (SearchFragment) A0V().A0A("search_fragment");
        if (searchFragment != null) {
            searchFragment.A1G.onResume();
            searchFragment.A1G.A0P(3);
        }
        A0V().A0H();
    }

    @Override // X.AbstractC14730lx
    public void AUT() {
        this.A0b.A0J.A0Z = false;
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r6) {
        super.AXC(r6);
        this.A0G = null;
        ((AnonymousClass2IW) ((C18980tN) this.A1i.get()).A00(AnonymousClass2IW.class)).A00.setShouldHideBanner(false);
        A2m();
        if (!A36()) {
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setObjectValues(Integer.valueOf(AnonymousClass00T.A00(this, R.color.action_mode)), Integer.valueOf(AnonymousClass00T.A00(this, R.color.primary)));
            valueAnimator.setEvaluator(new ArgbEvaluator());
            valueAnimator.setDuration(valueAnimator.getDuration() + 25);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.2MP
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    HomeActivity homeActivity = HomeActivity.this;
                    homeActivity.A0P.setBackgroundColor(((Number) valueAnimator2.getAnimatedValue()).intValue());
                    int i = 0;
                    while (true) {
                        C14790m3 r1 = homeActivity.A0N;
                        if (i < r1.A00) {
                            r1.A0H(i).A05.setTextColor(((Number) valueAnimator2.getAnimatedValue()).intValue());
                            i++;
                        } else {
                            return;
                        }
                    }
                }
            });
            valueAnimator.start();
        }
    }

    @Override // X.AbstractC14720lw
    public void AXE() {
        SearchFragment searchFragment = (SearchFragment) A0V().A0A("search_fragment");
        if (searchFragment != null) {
            searchFragment.A1G.A0P(5);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    public final void installOnDrawListener(View view) {
        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC49622Ln(view, this));
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 30) {
            if (i != 90) {
                if (i == 200) {
                    if (i2 == -1) {
                        throw new UnsupportedOperationException();
                    }
                    return;
                }
            } else if (this.A0b.A08 != null && ((ActivityC13810kN) this).A0C.A07(1857)) {
                this.A0b.A0D(101, i2, intent);
                return;
            }
            super.onActivityResult(i, i2, intent);
            return;
        }
        TabsPager tabsPager = this.A0M;
        if (i2 == -1) {
            tabsPager.A0F(A02(((ActivityC13830kP) this).A01, 100), true);
            this.A0b.A08();
            this.A0b.A07();
            C49632Lo r0 = this.A0b.A0C;
            if (r0 == null || r0.A0A.A0B != 3) {
                findViewById(R.id.root_view).setSystemUiVisibility(4);
            }
            this.A0U.A02();
            return;
        }
        tabsPager.A0F(A02(((ActivityC13830kP) this).A01, 200), true);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (A33()) {
            if (A35()) {
                SearchFragment searchFragment = (SearchFragment) A0V().A0A("search_fragment");
                if (searchFragment != null) {
                    SearchViewModel searchViewModel = searchFragment.A1G;
                    C49642Lp r2 = searchViewModel.A0M;
                    if (r2.size() >= 3) {
                        r2.pop();
                        C49652Lq peek = r2.peek();
                        if (peek != null) {
                            AnonymousClass07E r22 = searchViewModel.A0g;
                            r22.A04("query_text", peek.A04);
                            r22.A04("search_type", peek.A03);
                            r22.A04("search_jid", peek.A01);
                            r22.A04("smart_filter", peek.A02);
                            return;
                        }
                    }
                    searchViewModel.A0W(true);
                    return;
                }
                return;
            } else if (A34()) {
                SearchFragment searchFragment2 = (SearchFragment) A0V().A0A("search_fragment");
                if (searchFragment2 != null) {
                    searchFragment2.A1G.onResume();
                    searchFragment2.A1G.A0P(3);
                }
                super.onBackPressed();
            }
        } else if (this.A0D.getVisibility() == 0) {
            A2z(true);
            return;
        }
        AnonymousClass1s8 r0 = this.A0b;
        if (r0 != null && r0.A0P()) {
            return;
        }
        if (this.A03 != 200) {
            this.A0M.A0F(A02(((ActivityC13830kP) this).A01, 200), true);
            return;
        }
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            this.A1k.get();
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A2p();
        AnonymousClass12P.A04(findViewById(R.id.action_mode_bar), getWindowManager());
        AnonymousClass1s8 r1 = this.A0b;
        if (r1.A08 != null) {
            r1.A0C.A03(true);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C003401m.A01("HomeActivity/onCreate");
        try {
            this.A1p = false;
            ((ActivityC13830kP) this).A02.A09("HomeActivity");
            ((ActivityC13830kP) this).A02.A08("HomeActivity_onCreate");
            if (C28391Mz.A02()) {
                getWindow().addFlags(Integer.MIN_VALUE);
            }
            A1b(5);
            super.onCreate(bundle);
            List list = A29;
            list.clear();
            int i = 100;
            if (this.A16.A0V()) {
                i = 600;
            }
            list.add(Integer.valueOf(i));
            list.add(200);
            list.add(300);
            list.add(400);
            setTitle(R.string.localized_app_name);
            View inflate = getLayoutInflater().inflate(R.layout.home, (ViewGroup) null, false);
            this.A0C = inflate;
            setContentView(inflate);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            this.A0I = toolbar;
            A1e(toolbar);
            this.A08 = AnonymousClass028.A0D(this.A0C, R.id.header);
            this.A0A = AnonymousClass028.A0D(this.A0C, R.id.pager_holder);
            this.A0E = AnonymousClass028.A0D(this.A08, R.id.toolbar_padding);
            this.A0D = findViewById(R.id.search_holder);
            this.A0F = (ViewGroup) findViewById(R.id.search_fragment_holder);
            this.A0O = new C14930mH((ImageView) findViewById(R.id.fab), (ImageView) findViewById(R.id.fab_second), (ImageView) findViewById(R.id.fab_third), (TextView) findViewById(R.id.fabText), ((ActivityC13830kP) this).A01);
            C49672Ls r11 = new C49672Ls(this);
            if (bundle == null) {
                this.A0J = (AnonymousClass01E) this.A1j.get();
            } else {
                AnonymousClass01E A08 = A0V().A08(bundle, "cameraMediaPickerFragment");
                this.A0J = A08;
                if (A08 == null) {
                    this.A0J = (AnonymousClass01E) this.A1j.get();
                }
            }
            this.A0b = this.A0c.A00(this.A0J, r11, this.A1F.A00(this, ((ActivityC13810kN) this).A0C.A07(611), ((ActivityC13810kN) this).A06.A05(AbstractC15460nI.A12)));
            this.A0C.setSystemUiVisibility(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            if (C28391Mz.A02()) {
                getWindow().addFlags(Integer.MIN_VALUE);
            }
            AnonymousClass12P.A05(getWindow());
            AnonymousClass028.A0h(this.A0C, new AnonymousClass07F() { // from class: X.2Lv
                @Override // X.AnonymousClass07F
                public final C018408o AMH(View view, C018408o r8) {
                    HomeActivity homeActivity = HomeActivity.this;
                    Rect rect = homeActivity.A1r;
                    rect.set(r8.A04(), r8.A06(), r8.A05(), r8.A03());
                    homeActivity.findViewById(R.id.navigation_bar_protection).setPadding(0, 0, 0, rect.bottom);
                    return r8;
                }
            });
            AbstractC005102i A1U = A1U();
            AnonymousClass009.A05(A1U);
            A1U.A0M(false);
            A1g(false);
            if (A32()) {
                this.A1a.A00();
                if (C20640w5.A00()) {
                    Log.w("home/device-not-supported");
                    A2w(new DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment());
                } else if (this.A0Q.A03()) {
                    Log.w("home/clock-wrong");
                    A2w(new DisplayExceptionDialogFactory$ClockWrongDialogFragment());
                } else if (this.A0Q.A02()) {
                    Log.w("home/sw-expired");
                    A2w(new DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment());
                } else if (this.A0R.A02(this.A0Q) > 0) {
                    Adm(new DisplayExceptionDialogFactory$SoftwareAboutToExpireDialogFragment());
                }
                this.A0M = (TabsPager) findViewById(R.id.pager);
                C14790m3 r7 = new C14790m3(A0V(), this);
                this.A0N = r7;
                this.A0M.setAdapter(r7);
                this.A0M.setOffscreenPageLimit(this.A0N.A00);
                this.A0M.post(new RunnableBRunnable0Shape0S0100000_I0(this, 44));
                A1U().A07(0.0f);
                AnonymousClass028.A0V(this.A08, getResources().getDimension(R.dimen.actionbar_elevation));
                if (!A37(getIntent())) {
                    this.A0M.A0F(A02(((ActivityC13830kP) this).A01, 200), false);
                    this.A03 = 200;
                }
                PagerSlidingTabStrip pagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
                this.A0P = pagerSlidingTabStrip;
                AnonymousClass028.A0a(pagerSlidingTabStrip, 2);
                AnonymousClass028.A0c(this.A0P, 0);
                this.A0P.setViewPager(this.A0M);
                this.A0P.A0M = this.A1v;
                if (list.size() == 1) {
                    this.A0P.setVisibility(8);
                }
                A2r();
                boolean z = false;
                if (this.A01 == 0) {
                    z = true;
                }
                A30(z);
                C32991d4 r1 = new C32991d4(this);
                this.A0v = r1;
                this.A0w.A03(r1);
                AnonymousClass1s5 r12 = new AnonymousClass1s5(this);
                this.A1c = r12;
                this.A1d.A03(r12);
                C36821kh r0 = new C36821kh(this);
                r0.A0A = null;
                ((AnonymousClass2IW) ((C18980tN) this.A1i.get()).A00(AnonymousClass2IW.class)).A00 = r0;
                this.A0B = r0;
                ViewGroup viewGroup = (ViewGroup) findViewById(R.id.call_notification_holder);
                if (viewGroup != null) {
                    viewGroup.addView(this.A0B, -1, -2);
                }
                ((AnonymousClass2IW) ((C18980tN) this.A1i.get()).A00(AnonymousClass2IW.class)).A00.A00 = new AnonymousClass2M5() { // from class: X.2M4
                    @Override // X.AnonymousClass2M5
                    public final void AYQ(int i2) {
                        HomeActivity.A0D(HomeActivity.this);
                    }
                };
                SearchViewModel AGU = AGU(this.A0k.A04(getApplicationContext(), "home-activity-contact-photo"));
                this.A1I = AGU;
                AGU.A03.A05(this, new AnonymousClass02B() { // from class: X.2M6
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        HomeActivity homeActivity = HomeActivity.this;
                        Number number = (Number) obj;
                        if (number != null && !homeActivity.isFinishing()) {
                            int intValue = number.intValue();
                            if (intValue == 0 || intValue == 1) {
                                homeActivity.A1e(homeActivity.A0I);
                            }
                        }
                    }
                });
                SearchViewModel searchViewModel = this.A1I;
                searchViewModel.A0S.A05(this, new AnonymousClass02B() { // from class: X.2M7
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        HomeActivity homeActivity = HomeActivity.this;
                        Boolean bool = (Boolean) obj;
                        if (bool != null) {
                            homeActivity.A1I.A0P(1);
                            if (!homeActivity.isFinishing()) {
                                homeActivity.A2z(bool.booleanValue());
                            }
                        }
                    }
                });
                installOnDrawListener(this.A08);
                ((ActivityC13830kP) this).A02.A04(this.A08, null, "HomeActivity", 1);
                if (bundle == null) {
                    C14830m7 r10 = ((ActivityC13790kL) this).A05;
                    C15570nT r9 = ((ActivityC13790kL) this).A01;
                    C19990v2 r72 = this.A0t;
                    C20650w6 r6 = this.A0s;
                    C15550nR r5 = this.A0g;
                    C15650ng r4 = this.A0y;
                    if (this.A0r.A02("android.permission.NFC") == 0) {
                        NfcAdapter defaultAdapter = NfcAdapter.getDefaultAdapter(this);
                        if (defaultAdapter != null) {
                            try {
                                defaultAdapter.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback(this, r9, r10) { // from class: X.2M8
                                    public final /* synthetic */ Activity A00;
                                    public final /* synthetic */ C15570nT A01;
                                    public final /* synthetic */ C14830m7 A02;

                                    {
                                        this.A01 = r2;
                                        this.A00 = r1;
                                        this.A02 = r3;
                                    }

                                    @Override // android.nfc.NfcAdapter.CreateNdefMessageCallback
                                    public final NdefMessage createNdefMessage(NfcEvent nfcEvent) {
                                        C15570nT r73 = this.A01;
                                        C14830m7 r8 = this.A02;
                                        Log.i("newchatnfc/createndef");
                                        NdefRecord[] ndefRecordArr = new NdefRecord[2];
                                        byte[] bytes = "application/com.whatsapp.chat".getBytes(Charset.forName("US-ASCII"));
                                        r73.A08();
                                        C27631Ih r02 = r73.A05;
                                        AnonymousClass009.A05(r02);
                                        byte[] bytes2 = r02.getRawString().getBytes(Charset.forName("US-ASCII"));
                                        JSONObject jSONObject = new JSONObject();
                                        try {
                                            r73.A08();
                                            C27631Ih r03 = r73.A05;
                                            AnonymousClass009.A05(r03);
                                            jSONObject.put("jid", r03.getRawString());
                                            jSONObject.put("id", AnonymousClass15O.A00(r73, r8, false));
                                            jSONObject.put("name", r73.A05());
                                        } catch (JSONException e) {
                                            Log.e("newchatnfc/", e);
                                        }
                                        ndefRecordArr[0] = new NdefRecord(2, bytes, bytes2, jSONObject.toString().getBytes(Charset.forName("US-ASCII")));
                                        ndefRecordArr[1] = NdefRecord.createApplicationRecord("com.whatsapp");
                                        return new NdefMessage(ndefRecordArr);
                                    }
                                }, this, new Activity[0]);
                            } catch (IllegalStateException | SecurityException e) {
                                Log.i("newchatnfc/ ", e);
                            }
                        }
                        if ("android.nfc.action.NDEF_DISCOVERED".equals(getIntent().getAction())) {
                            A09(this, getIntent(), r5, r6, r72, r4);
                        }
                    }
                    A2t(getIntent());
                    Uri data = getIntent().getData();
                    if (data != null) {
                        C14850m9 r42 = ((ActivityC13810kN) this).A0C;
                        AnonymousClass19Z r3 = this.A1e;
                        AnonymousClass1SF.A0H(data, this, ((ActivityC13810kN) this).A05, this.A0S, r42, r3);
                    }
                }
                AnonymousClass018 r02 = ((ActivityC13830kP) this).A01;
                r02.A0B.add(this.A1x);
                A2u(getIntent());
                A2v(getIntent());
                ((ActivityC13830kP) this).A02.A0A("HomeActivity created");
            }
        } finally {
            ((ActivityC13830kP) this).A02.A07("HomeActivity_onCreate");
            C003401m.A00();
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu.size() == 0) {
            MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(AnonymousClass00T.A04(this, R.drawable.ic_action_search));
            icon.setShowAsAction(2);
            AnonymousClass07G.A00(ColorStateList.valueOf(AnonymousClass00T.A00(this, R.color.homeActivityMenuItem)), icon);
            menu.add(4, R.id.menuitem_settings, 196608, R.string.menuitem_settings).setAlphabeticShortcut('o');
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C27151Gf r1 = this.A0v;
        if (r1 != null) {
            this.A0w.A04(r1);
            this.A0v = null;
        }
        C236712o r12 = this.A1c;
        if (r12 != null) {
            this.A1d.A04(r12);
            this.A1c = null;
        }
        this.A0b.A04();
        AnonymousClass018 r0 = ((ActivityC13830kP) this).A01;
        r0.A0B.remove(this.A1x);
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A02(this.A09, this.A0n);
            C35191hP A00 = this.A0n.A00();
            if (A00 != null) {
                ((AnonymousClass19F) this.A1l.get()).Ac2();
                A00.A0H(true, false);
                A00.A0K = null;
            }
            AnonymousClass1J1 r02 = this.A0j;
            if (r02 != null) {
                r02.A00();
                this.A0j = null;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if ((this.A03 != 100 || !this.A0b.A0R(i)) && !super.onKeyDown(i, keyEvent)) {
            return false;
        }
        return true;
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return (this.A03 == 100 && this.A0b.A0S(i)) || super.onKeyUp(i, keyEvent);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onMenuOpened(int i, Menu menu) {
        C14850m9 r1 = this.A0W.A00;
        if (r1.A07(450) && r1.A07(1294) && (A2h() instanceof ConversationsFragment)) {
            this.A0X.A00(4, 7);
        }
        if (i == 108 && menu != null) {
            C21880y8 r3 = this.A1Z;
            C14850m9 r2 = r3.A04;
            if (r2.A02(1486) != 1 && r2.A07(1391) && r3.A08()) {
                C21880y8 r22 = this.A1Z;
                if (r22.A0C) {
                    r22.A07(new RunnableBRunnable0Shape13S0100000_I0_13(r22, 7));
                }
            }
        }
        return super.onMenuOpened(i, menu);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("home/newintent");
        if (A37(intent)) {
            A2z(false);
        }
        C19990v2 r8 = this.A0t;
        A09(this, intent, this.A0g, this.A0s, r8, this.A0y);
        A2t(intent);
        A2u(intent);
        A2v(intent);
        Uri data = intent.getData();
        if (data != null) {
            C14850m9 r10 = ((ActivityC13810kN) this).A0C;
            AnonymousClass19Z r11 = this.A1e;
            AnonymousClass1SF.A0H(data, this, ((ActivityC13810kN) this).A05, this.A0S, r10, r11);
        }
        String stringExtra = intent.getStringExtra("snackbar_message");
        if (stringExtra != null) {
            ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape0S1100000_I0(0, stringExtra, this));
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent intent;
        String str;
        Intent className;
        if (menuItem.getItemId() == R.id.menuitem_search) {
            C17010q7 r5 = this.A0l;
            int A02 = r5.A06.A02(1261);
            if (A02 != -1 && r5.A05.A00() - r5.A04.A01().getLong("last_out_contact_sync_time", -1) > ((long) A02) * 60000) {
                r5.A00();
            }
            A2y("");
            return true;
        }
        if (menuItem.getItemId() == R.id.menuitem_settings) {
            className = new Intent();
            className.setClassName(getPackageName(), "com.whatsapp.settings.Settings");
        } else if (menuItem.getItemId() == R.id.menuitem_payments) {
            Class AFa = this.A1D.A02().AFa();
            if (AFa != null) {
                StringBuilder sb = new StringBuilder("PAY: HomeActivity - Loading payment class: ");
                sb.append(AFa);
                Log.i(sb.toString());
                className = new Intent(this, AFa);
                className.putExtra("referral_screen", "chat");
            } else {
                Log.e("PAY: HomeActivity - can't find payment service");
                ((ActivityC13810kN) this).A05.A0E(getString(R.string.payments_unavailable_title), 1);
                return true;
            }
        } else if (menuItem.getItemId() == R.id.menuitem_orders) {
            throw new UnsupportedOperationException();
        } else {
            if (menuItem.getItemId() == R.id.menuitem_debug) {
                intent = new Intent();
                str = "com.whatsapp.Advanced";
            } else if (menuItem.getItemId() != R.id.menuitem_debug_new) {
                return super.onOptionsItemSelected(menuItem);
            } else {
                intent = new Intent();
                str = "com.whatsapp.DebugToolsActivity";
            }
            className = intent.setClassName(this, str);
        }
        startActivity(className);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A0b.A05();
        BroadcastReceiver broadcastReceiver = this.A07;
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            this.A07 = null;
        }
        A2l();
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A07(this.A0n);
            AnonymousClass1A1 r3 = (AnonymousClass1A1) this.A1k.get();
            if (!AnonymousClass1A1.A00(((ActivityC13810kN) this).A00)) {
                r3.A05 = false;
                r3.A04 = false;
                r3.A03 = false;
                return;
            }
            boolean z = r3.A02;
            r3.A05 = true;
            if (z) {
                r3.A04 = false;
            } else {
                r3.A04 = true;
            }
            r3.A03 = true;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:50:0x00b1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onPrepareOptionsMenu(android.view.Menu r9) {
        /*
            r8 = this;
            r0 = 2131364436(0x7f0a0a54, float:1.834871E38)
            android.view.MenuItem r2 = r9.findItem(r0)
            int r1 = r8.A03
            X.018 r0 = r8.A01
            int r0 = A02(r0, r1)
            X.0m1 r0 = r8.A2i(r0)
            if (r0 == 0) goto L_0x002f
            if (r2 == 0) goto L_0x002f
            boolean r0 = r0.Aed()
            if (r0 == 0) goto L_0x00a4
            r0 = 1
            r2.setEnabled(r0)
            r0 = 2131100308(0x7f060294, float:1.7812994E38)
        L_0x0024:
            int r0 = X.AnonymousClass00T.A00(r8, r0)
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            X.AnonymousClass07G.A00(r0, r2)
        L_0x002f:
            X.0y8 r4 = r8.A1Z
            X.0m9 r3 = r4.A04
            r0 = 1486(0x5ce, float:2.082E-42)
            int r2 = r3.A02(r0)
            r1 = 0
            r0 = 1
            if (r2 == r0) goto L_0x004c
            r0 = 1391(0x56f, float:1.949E-42)
            boolean r0 = r3.A07(r0)
            if (r0 == 0) goto L_0x004c
            boolean r0 = r4.A08()
            if (r0 == 0) goto L_0x004c
            r1 = 1
        L_0x004c:
            r7 = 0
            r2 = 1
            if (r1 == 0) goto L_0x0095
            java.lang.Integer[] r1 = new java.lang.Integer[r2]
            r0 = 2131364439(0x7f0a0a57, float:1.8348715E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1[r7] = r0
        L_0x005b:
            int r0 = r1.length
            r6 = 0
            if (r0 == 0) goto L_0x0060
            r6 = 1
        L_0x0060:
            java.util.List r5 = java.util.Arrays.asList(r1)
            boolean r4 = r9 instanceof X.AnonymousClass07H
            if (r4 == 0) goto L_0x00ad
            r0 = r9
            X.07H r0 = (X.AnonymousClass07H) r0
            r0.A05()
            java.util.ArrayList r1 = r0.A06
            int r0 = r1.size()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r0)
            java.util.Iterator r1 = r1.iterator()
        L_0x007d:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r1.next()
            android.view.MenuItem r0 = (android.view.MenuItem) r0
            int r0 = r0.getItemId()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3.add(r0)
            goto L_0x007d
        L_0x0095:
            X.0y8 r0 = r8.A1Z
            X.0m9 r1 = r0.A04
            r0 = 1486(0x5ce, float:2.082E-42)
            int r0 = r1.A02(r0)
            if (r0 == r2) goto L_0x00f7
            java.lang.Integer[] r1 = new java.lang.Integer[r7]
            goto L_0x005b
        L_0x00a4:
            r0 = 0
            r2.setEnabled(r0)
            r0 = 2131100698(0x7f06041a, float:1.7813785E38)
            goto L_0x0024
        L_0x00ad:
            java.util.List r3 = java.util.Collections.emptyList()
        L_0x00b1:
            r2 = 0
        L_0x00b2:
            int r0 = r9.size()
            if (r2 >= r0) goto L_0x00f0
            android.view.MenuItem r1 = r9.getItem(r2)
            int r0 = r1.getItemId()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x00cf
            if (r6 != 0) goto L_0x00d2
            r1.setIcon(r7)
        L_0x00cf:
            int r2 = r2 + 1
            goto L_0x00b2
        L_0x00d2:
            r0 = 2131232127(0x7f08057f, float:1.8080354E38)
            r1.setIcon(r0)
            int r0 = r1.getItemId()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x00eb
            r0 = 0
        L_0x00e7:
            X.AnonymousClass07G.A00(r0, r1)
            goto L_0x00cf
        L_0x00eb:
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r7)
            goto L_0x00e7
        L_0x00f0:
            if (r4 == 0) goto L_0x00f7
            r0 = r9
            X.07H r0 = (X.AnonymousClass07H) r0
            r0.A0H = r6
        L_0x00f7:
            boolean r0 = super.onPrepareOptionsMenu(r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.onPrepareOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public void onRestart() {
        ((ActivityC13830kP) this).A02.A05(this.A08, null, "HomeActivity", 1);
        super.onRestart();
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        int i = bundle.getInt("selected_tab", this.A03);
        this.A03 = i;
        this.A0M.A0F(A02(((ActivityC13830kP) this).A01, i), false);
        if (this.A03 == 100) {
            AnonymousClass2LE r1 = this.A1v;
            if (!r1.A00) {
                r1.A00 = true;
                r1.A00();
            }
            CameraHomeFragment cameraHomeFragment = (CameraHomeFragment) this.A0N.A0G(A02(((ActivityC13830kP) this).A01, 100));
            this.A0L = cameraHomeFragment;
            cameraHomeFragment.A0n(true);
        }
        if (bundle.getBoolean("search")) {
            A2y(bundle.getString("query", ""));
        }
        this.A0b.A0G(bundle);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c4 A[Catch: all -> 0x027f, TryCatch #0 {all -> 0x027f, blocks: (B:3:0x0013, B:5:0x0021, B:7:0x0027, B:9:0x002e, B:11:0x003e, B:12:0x0043, B:14:0x0049, B:16:0x004f, B:17:0x005c, B:19:0x0062, B:21:0x0074, B:23:0x0081, B:25:0x0087, B:26:0x0094, B:28:0x00a2, B:29:0x00ab, B:30:0x00b2, B:32:0x00c4, B:33:0x00c8, B:35:0x00d2, B:37:0x00e0, B:39:0x00e8, B:40:0x00f3, B:42:0x00f9, B:43:0x0102, B:45:0x0108, B:46:0x0111, B:48:0x011b, B:49:0x0125, B:51:0x0132, B:53:0x0145, B:54:0x0151, B:55:0x015f, B:56:0x0182, B:59:0x018e, B:62:0x0195, B:65:0x019c, B:67:0x01a0, B:70:0x01b6, B:73:0x01cb, B:74:0x01d8, B:75:0x01e9, B:77:0x01f3, B:79:0x0203, B:81:0x0209, B:82:0x0212, B:83:0x021e), top: B:89:0x0013 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01a0 A[Catch: all -> 0x027f, TryCatch #0 {all -> 0x027f, blocks: (B:3:0x0013, B:5:0x0021, B:7:0x0027, B:9:0x002e, B:11:0x003e, B:12:0x0043, B:14:0x0049, B:16:0x004f, B:17:0x005c, B:19:0x0062, B:21:0x0074, B:23:0x0081, B:25:0x0087, B:26:0x0094, B:28:0x00a2, B:29:0x00ab, B:30:0x00b2, B:32:0x00c4, B:33:0x00c8, B:35:0x00d2, B:37:0x00e0, B:39:0x00e8, B:40:0x00f3, B:42:0x00f9, B:43:0x0102, B:45:0x0108, B:46:0x0111, B:48:0x011b, B:49:0x0125, B:51:0x0132, B:53:0x0145, B:54:0x0151, B:55:0x015f, B:56:0x0182, B:59:0x018e, B:62:0x0195, B:65:0x019c, B:67:0x01a0, B:70:0x01b6, B:73:0x01cb, B:74:0x01d8, B:75:0x01e9, B:77:0x01f3, B:79:0x0203, B:81:0x0209, B:82:0x0212, B:83:0x021e), top: B:89:0x0013 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01f3 A[Catch: all -> 0x027f, TryCatch #0 {all -> 0x027f, blocks: (B:3:0x0013, B:5:0x0021, B:7:0x0027, B:9:0x002e, B:11:0x003e, B:12:0x0043, B:14:0x0049, B:16:0x004f, B:17:0x005c, B:19:0x0062, B:21:0x0074, B:23:0x0081, B:25:0x0087, B:26:0x0094, B:28:0x00a2, B:29:0x00ab, B:30:0x00b2, B:32:0x00c4, B:33:0x00c8, B:35:0x00d2, B:37:0x00e0, B:39:0x00e8, B:40:0x00f3, B:42:0x00f9, B:43:0x0102, B:45:0x0108, B:46:0x0111, B:48:0x011b, B:49:0x0125, B:51:0x0132, B:53:0x0145, B:54:0x0151, B:55:0x015f, B:56:0x0182, B:59:0x018e, B:62:0x0195, B:65:0x019c, B:67:0x01a0, B:70:0x01b6, B:73:0x01cb, B:74:0x01d8, B:75:0x01e9, B:77:0x01f3, B:79:0x0203, B:81:0x0209, B:82:0x0212, B:83:0x021e), top: B:89:0x0013 }] */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
        // Method dump skipped, instructions count: 651
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.onResume():void");
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        SearchView searchView;
        bundle.putInt("selected_tab", this.A03);
        boolean A36 = A36();
        bundle.putBoolean("search", A36);
        if (A36 && (searchView = this.A0H) != null && searchView.A0k.getText().length() > 0) {
            bundle.putString("query", this.A0H.A0k.getText().toString());
        }
        AnonymousClass01E A0A = A0V().A0A("cameraMediaPickerFragment");
        if (A0A != null) {
            A0V().A0P(bundle, A0A, "cameraMediaPickerFragment");
        }
        this.A0b.A0H(bundle);
        A2l();
        super.onSaveInstanceState(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        A2y("");
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
    }

    /* loaded from: classes2.dex */
    public class TabsPager extends ViewPager implements AnonymousClass004 {
        public AnonymousClass11P A00;
        public AnonymousClass018 A01;
        public C14850m9 A02;
        public AnonymousClass01H A03;
        public AnonymousClass2P7 A04;
        public boolean A05;
        public boolean A06;

        public TabsPager(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, 0);
        }

        public TabsPager(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet);
            if (!this.A06) {
                this.A06 = true;
                AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
                this.A02 = (C14850m9) r1.A04.get();
                this.A01 = (AnonymousClass018) r1.ANb.get();
                this.A00 = (AnonymousClass11P) r1.ABp.get();
                this.A03 = C18000rk.A00(r1.ADw);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
            if (r5 != 0.0f) goto L_0x0013;
         */
        @Override // androidx.viewpager.widget.ViewPager
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void A0B(int r4, float r5, int r6) {
            /*
                r3 = this;
                super.A0B(r4, r5, r6)
                X.018 r1 = r3.A01
                r0 = 100
                int r0 = com.whatsapp.HomeActivity.A02(r1, r0)
                if (r4 != r0) goto L_0x0013
                r0 = 0
                int r1 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
                r0 = 1
                if (r1 == 0) goto L_0x0014
            L_0x0013:
                r0 = 0
            L_0x0014:
                r3.A05 = r0
                X.0m9 r1 = r3.A02
                r0 = 931(0x3a3, float:1.305E-42)
                boolean r0 = r1.A07(r0)
                if (r0 == 0) goto L_0x002f
                boolean r0 = r3.A05
                if (r0 == 0) goto L_0x002f
                X.11P r2 = r3.A00
                X.01H r1 = r3.A03
                android.view.View r0 = r3.getRootView()
                X.C40691s6.A04(r0, r2, r1)
            L_0x002f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.HomeActivity.TabsPager.A0B(int, float, int):void");
        }

        public final boolean A0O(MotionEvent motionEvent) {
            HomeActivity homeActivity = (HomeActivity) AnonymousClass12P.A01(getContext(), HomeActivity.class);
            AnonymousClass1s9 r0 = homeActivity.A0b.A0A;
            if (r0 != null && r0.AJy()) {
                return false;
            }
            AnonymousClass2P8 r02 = homeActivity.A0b.A0E;
            if (r02 != null && r02.A04.getVisibility() == 0) {
                return false;
            }
            if (!this.A05 || motionEvent.getPointerCount() < 2) {
                return true;
            }
            return false;
        }

        @Override // X.AnonymousClass005
        public final Object generatedComponent() {
            AnonymousClass2P7 r0 = this.A04;
            if (r0 == null) {
                r0 = new AnonymousClass2P7(this);
                this.A04 = r0;
            }
            return r0.generatedComponent();
        }

        @Override // androidx.viewpager.widget.ViewPager, android.view.ViewGroup
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            return A0O(motionEvent) && super.onInterceptTouchEvent(motionEvent);
        }

        @Override // androidx.viewpager.widget.ViewPager, android.view.View
        public boolean onTouchEvent(MotionEvent motionEvent) {
            return A0O(motionEvent) && super.onTouchEvent(motionEvent);
        }

        @Override // androidx.viewpager.widget.ViewPager
        public void setCurrentItem(int i) {
            HomeActivity homeActivity;
            AbstractC14770m1 A2i;
            View view;
            AbsListView absListView;
            if (!(i != getCurrentItem() || (A2i = (homeActivity = (HomeActivity) AnonymousClass12P.A01(getContext(), HomeActivity.class)).A2i(i)) == null || (view = ((AnonymousClass01E) A2i).A0A) == null || (absListView = (AbsListView) view.findViewById(16908298)) == null)) {
                if (absListView.getFirstVisiblePosition() < 8) {
                    absListView.smoothScrollToPosition(0);
                } else {
                    absListView.setSelection(0);
                }
                homeActivity.A2p();
            }
            super.setCurrentItem(i);
        }
    }
}
