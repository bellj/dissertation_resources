package com.whatsapp;

import X.AnonymousClass009;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C15550nR;
import X.C15580nU;
import X.C15610nY;
import X.C20710wC;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;

/* loaded from: classes2.dex */
public class RevokeLinkConfirmationDialogFragment extends Hilt_RevokeLinkConfirmationDialogFragment {
    public C15550nR A00;
    public C15610nY A01;
    public C20710wC A02;

    public static RevokeLinkConfirmationDialogFragment A00(C15580nU r4, boolean z) {
        RevokeLinkConfirmationDialogFragment revokeLinkConfirmationDialogFragment = new RevokeLinkConfirmationDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r4.getRawString());
        A0D.putBoolean("from_qr", z);
        revokeLinkConfirmationDialogFragment.A0U(A0D);
        return revokeLinkConfirmationDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String A0q;
        Bundle A03 = A03();
        boolean z = A03.getBoolean("from_qr");
        C004802e A0K = C12960it.A0K(this);
        int i = R.string.revoke_invite_link;
        if (z) {
            i = R.string.contact_qr_revoke_ok_button;
        }
        A0K.A03(new IDxCListenerShape9S0100000_2_I1(this, 2), A0I(i));
        A0K.A01(null, A0I(R.string.cancel));
        if (z) {
            A0K.setTitle(A0I(R.string.contact_qr_revoke_title));
            A0q = A0I(R.string.reset_group_invite_link_and_qr_code_confirmation);
        } else {
            String string = A03.getString("jid");
            AnonymousClass009.A05(string);
            C15580nU A04 = C15580nU.A04(string);
            boolean A0b = this.A02.A0b(A04);
            int i2 = R.string.reset_link_confirmation;
            if (A0b) {
                i2 = R.string.reset_link_confirmation_parent_group;
            }
            A0q = C12970iu.A0q(this, this.A01.A04(C15550nR.A00(this.A00, A04)), C12970iu.A1b(), 0, i2);
        }
        A0K.A0A(A0q);
        return A0K.create();
    }
}
