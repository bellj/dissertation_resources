package com.whatsapp.bottomsheet;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

/* loaded from: classes3.dex */
public class LockableBottomSheetBehavior extends BottomSheetBehavior {
    public boolean A00 = true;

    public LockableBottomSheetBehavior() {
    }

    public LockableBottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
    public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        if (!this.A00) {
            return false;
        }
        return super.A0C(motionEvent, view, coordinatorLayout);
    }
}
