package com.whatsapp.stickers;

import X.AnonymousClass03J;
import X.C39631qF;
import X.C75793kT;
import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.whatsapp.WaImageView;

/* loaded from: classes2.dex */
public class StickerView extends WaImageView {
    public int A00;
    public AnonymousClass03J A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass03J A05;

    public StickerView(Context context) {
        super(context);
        A00();
        this.A05 = new C75793kT(this);
    }

    public StickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A05 = new C75793kT(this);
    }

    public StickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A05 = new C75793kT(this);
    }

    public StickerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A02() {
        Drawable drawable = getDrawable();
        if (drawable instanceof C39631qF) {
            C39631qF r2 = (C39631qF) drawable;
            r2.A03 = this.A03;
            int i = this.A00;
            if (!r2.A04) {
                r2.A01 = i;
            } else if (r2.A01 < i) {
                r2.A01 = i;
                r2.A00 = 0;
            }
        }
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
    }

    public void A03() {
        Drawable drawable = getDrawable();
        if (drawable instanceof Animatable) {
            Animatable animatable = (Animatable) drawable;
            if (animatable.isRunning()) {
                animatable.stop();
            }
        }
    }

    public AnonymousClass03J getAnimationCallback() {
        return this.A01;
    }

    public boolean getLoopIndefinitely() {
        return this.A03;
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.A04 && this.A03) {
            A02();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A03();
    }

    @Override // android.view.View
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (i != 0) {
            A03();
        } else if (this.A04 && this.A03) {
            A02();
        }
    }

    public void setAnimationCallback(AnonymousClass03J r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        setScaleType(ImageView.ScaleType.FIT_CENTER);
        Drawable drawable2 = getDrawable();
        if (drawable2 != drawable && (drawable2 instanceof C39631qF)) {
            C39631qF r2 = (C39631qF) drawable2;
            r2.A08.remove(this.A05);
            r2.stop();
        }
        super.setImageDrawable(drawable);
        if (drawable instanceof C39631qF) {
            ((C39631qF) drawable).A08.add(this.A05);
        }
    }

    public void setLoopIndefinitely(boolean z) {
        this.A03 = z;
    }

    public void setMaxLoops(int i) {
        this.A00 = i;
    }

    public void setUserVisibleForIndefiniteLoop(boolean z) {
        this.A04 = z;
    }
}
