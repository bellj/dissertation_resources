package com.whatsapp.stickers;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass04S;
import X.AnonymousClass1KZ;
import X.AnonymousClass3KZ;
import X.C004802e;
import X.C235512c;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.stickers.ConfirmPackDeleteDialogFragment;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import java.lang.ref.WeakReference;

/* loaded from: classes2.dex */
public class ConfirmPackDeleteDialogFragment extends Hilt_ConfirmPackDeleteDialogFragment {
    public C235512c A00;

    public static ConfirmPackDeleteDialogFragment A00(AnonymousClass1KZ r4) {
        ConfirmPackDeleteDialogFragment confirmPackDeleteDialogFragment = new ConfirmPackDeleteDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pack_id", r4.A0D);
        bundle.putString("pack_name", r4.A0F);
        confirmPackDeleteDialogFragment.A0U(bundle);
        return confirmPackDeleteDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0B = A0B();
        String string = A03().getString("pack_id");
        AnonymousClass009.A05(string);
        String string2 = A03().getString("pack_name");
        AnonymousClass009.A05(string2);
        AnonymousClass3KZ r5 = new DialogInterface.OnClickListener(string) { // from class: X.3KZ
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AnonymousClass1KQ r2;
                ConfirmPackDeleteDialogFragment confirmPackDeleteDialogFragment = ConfirmPackDeleteDialogFragment.this;
                String str = this.A01;
                if (i == -1) {
                    try {
                        r2 = (AnonymousClass1KQ) confirmPackDeleteDialogFragment.A0p();
                        if (r2 != null) {
                            StickerStorePackPreviewActivity stickerStorePackPreviewActivity = (StickerStorePackPreviewActivity) r2;
                            stickerStorePackPreviewActivity.A0W = true;
                            stickerStorePackPreviewActivity.A2e();
                        }
                    } catch (ClassCastException unused) {
                        r2 = null;
                    }
                    WeakReference A10 = C12970iu.A10(r2);
                    C235512c r3 = confirmPackDeleteDialogFragment.A00;
                    AnonymousClass29R r22 = new AnonymousClass29R(r3.A0P, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002a: CONSTRUCTOR  (r2v1 'r22' X.29R) = 
                          (wrap: X.146 : 0x0026: IGET  (r0v2 X.146 A[REMOVE]) = (r3v0 'r3' X.12c) X.12c.A0P X.146)
                          (wrap: X.3aJ : 0x0023: CONSTRUCTOR  (r1v0 X.3aJ A[REMOVE]) = (r0v1 'A10' java.lang.ref.WeakReference) call: X.3aJ.<init>(java.lang.ref.WeakReference):void type: CONSTRUCTOR)
                          (r3v0 'r3' X.12c)
                         call: X.29R.<init>(X.146, X.29Q, X.12c):void type: CONSTRUCTOR in method: X.3KZ.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0023: CONSTRUCTOR  (r1v0 X.3aJ A[REMOVE]) = (r0v1 'A10' java.lang.ref.WeakReference) call: X.3aJ.<init>(java.lang.ref.WeakReference):void type: CONSTRUCTOR in method: X.3KZ.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3aJ, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        com.whatsapp.stickers.ConfirmPackDeleteDialogFragment r5 = com.whatsapp.stickers.ConfirmPackDeleteDialogFragment.this
                        java.lang.String r4 = r6.A01
                        r0 = -1
                        if (r8 != r0) goto L_0x003c
                        android.content.Context r2 = r5.A0p()     // Catch: ClassCastException -> 0x0010
                        X.1KQ r2 = (X.AnonymousClass1KQ) r2     // Catch: ClassCastException -> 0x0010
                        if (r2 == 0) goto L_0x001b
                        goto L_0x0012
                    L_0x0010:
                        r2 = 0
                        goto L_0x001b
                    L_0x0012:
                        r1 = r2
                        com.whatsapp.stickers.StickerStorePackPreviewActivity r1 = (com.whatsapp.stickers.StickerStorePackPreviewActivity) r1
                        r0 = 1
                        r1.A0W = r0
                        r1.A2e()
                    L_0x001b:
                        java.lang.ref.WeakReference r0 = X.C12970iu.A10(r2)
                        X.12c r3 = r5.A00
                        X.3aJ r1 = new X.3aJ
                        r1.<init>(r0)
                        X.146 r0 = r3.A0P
                        X.29R r2 = new X.29R
                        r2.<init>(r0, r1, r3)
                        java.lang.String[] r1 = X.C13000ix.A08()
                        r0 = 0
                        r1[r0] = r4
                        X.0lR r0 = r3.A0Y
                        r0.Aaz(r2, r1)
                        r5.A1B()
                    L_0x003c:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3KZ.onClick(android.content.DialogInterface, int):void");
                }
            };
            C004802e r4 = new C004802e(A0B);
            r4.A0A(A0J(R.string.sticker_pack_removal_confirmation, string2));
            r4.setPositiveButton(R.string.delete, r5);
            r4.setNegativeButton(R.string.cancel, null);
            AnonymousClass04S create = r4.create();
            create.setCanceledOnTouchOutside(true);
            return create;
        }
    }
