package com.whatsapp.stickers;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC16350or;
import X.ActivityC000900k;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass0KS;
import X.AnonymousClass1KM;
import X.AnonymousClass2FH;
import X.AnonymousClass2I1;
import X.AnonymousClass35f;
import X.AnonymousClass38X;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14900mE;
import X.C16120oU;
import X.C48572Gu;
import X.C66563Ob;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ProviderInfo;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.ViewOnClickCListenerShape11S0100000_I1_5;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class AddThirdPartyStickerPackActivity extends ActivityC000900k implements AnonymousClass004 {
    public C16120oU A00;
    public AnonymousClass38X A01;
    public AnonymousClass2I1 A02;
    public AbstractC14440lR A03;
    public boolean A04;
    public final Object A05;
    public volatile AnonymousClass2FH A06;

    public AddThirdPartyStickerPackActivity() {
        this(0);
    }

    public AddThirdPartyStickerPackActivity(int i) {
        this.A05 = C12970iu.A0l();
        this.A04 = false;
        A0R(new C66563Ob(this));
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A06 == null) {
            synchronized (this.A05) {
                if (this.A06 == null) {
                    this.A06 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A06.generatedComponent();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String packageName;
        String A0d;
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra("sticker_pack_id");
        String stringExtra2 = getIntent().getStringExtra("sticker_pack_authority");
        String stringExtra3 = getIntent().getStringExtra("sticker_pack_name");
        if (!(getCallingActivity() == null || (packageName = getCallingActivity().getPackageName()) == null)) {
            ProviderInfo resolveContentProvider = this.A02.A00.resolveContentProvider(stringExtra2, 128);
            if (resolveContentProvider == null) {
                A0d = C12960it.A0d(stringExtra2, C12960it.A0k("cannot find the provider for authority: "));
            } else if (!packageName.equals(resolveContentProvider.packageName)) {
                StringBuilder A0k = C12960it.A0k("the calling activity: ");
                A0k.append(packageName);
                A0k.append(" does not own authority: ");
                A0d = C12960it.A0d(stringExtra2, A0k);
            }
            Intent A0A = C12970iu.A0A();
            A0A.putExtra("validation_error", A0d);
            setResult(0, A0A);
            Log.e(A0d);
            finish();
            overridePendingTransition(0, 0);
            return;
        }
        AnonymousClass38X r2 = new AnonymousClass38X(this, this.A00, this.A02, stringExtra, stringExtra2, stringExtra3);
        this.A01 = r2;
        C12960it.A1E(r2, this.A03);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass38X r0 = this.A01;
        if (r0 != null && !((AbstractC16350or) r0).A02.isCancelled()) {
            this.A01.A03(true);
        }
    }

    /* loaded from: classes2.dex */
    public class AddStickerPackDialogFragment extends Hilt_AddThirdPartyStickerPackActivity_AddStickerPackDialogFragment {
        public C14900mE A00;
        public AnonymousClass018 A01;
        public AnonymousClass2I1 A02;
        public String A03;
        public String A04;
        public String A05;
        public String A06;
        public final View.OnClickListener A07 = new ViewOnClickCListenerShape11S0100000_I1_5(this, 26);
        public final View.OnClickListener A08 = new ViewOnClickCListenerShape11S0100000_I1_5(this, 25);
        public final View.OnClickListener A09 = new ViewOnClickCListenerShape11S0100000_I1_5(this, 24);
        public final AnonymousClass1KM A0A = new AnonymousClass35f(this);

        public static AddStickerPackDialogFragment A00(String str, String str2, String str3) {
            Bundle A0D = C12970iu.A0D();
            A0D.putString("sticker_pack_id", str);
            A0D.putString("sticker_pack_authority", str2);
            A0D.putString("sticker_pack_name", str3);
            AddStickerPackDialogFragment addStickerPackDialogFragment = new AddStickerPackDialogFragment();
            addStickerPackDialogFragment.A0U(A0D);
            return addStickerPackDialogFragment;
        }

        @Override // X.AnonymousClass01E
        public void A11() {
            super.A11();
            AnonymousClass2I1 r0 = this.A02;
            r0.A01.A04(this.A0A);
        }

        @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
        public void A16(Bundle bundle) {
            super.A16(bundle);
            AnonymousClass2I1 r0 = this.A02;
            r0.A01.A03(this.A0A);
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            super.A1A(bundle);
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            if (bundle2 != null) {
                this.A04 = bundle2.getString("sticker_pack_id");
                this.A03 = bundle2.getString("sticker_pack_authority");
                String string = bundle2.getString("sticker_pack_name");
                this.A05 = string;
                if (string != null) {
                    this.A06 = Html.escapeHtml(string);
                }
            }
            View A0O = C12980iv.A0O(LayoutInflater.from(A0p()), R.layout.add_third_party_sticker_dialog);
            TextView A0J = C12960it.A0J(A0O, R.id.message_text_view);
            AnonymousClass018 r5 = this.A01;
            Object[] A1b = C12970iu.A1b();
            A1b[0] = r5.A09(R.string.localized_app_name);
            A0J.setText(r5.A0B(R.string.validate_sticker_progress_message_with_app, A1b));
            View findViewById = A0O.findViewById(R.id.ok_button);
            findViewById.setVisibility(8);
            findViewById.setOnClickListener(this.A09);
            View findViewById2 = A0O.findViewById(R.id.cancel_button);
            findViewById2.setVisibility(8);
            findViewById2.setOnClickListener(this.A08);
            View findViewById3 = A0O.findViewById(R.id.add_button);
            findViewById3.setOnClickListener(this.A07);
            findViewById3.setVisibility(8);
            C004802e A0K = C12960it.A0K(this);
            A0K.setView(A0O);
            return A0K.create();
        }

        public final void A1K(String str, int i, int i2, int i3) {
            Dialog dialog = ((DialogFragment) this).A03;
            if (dialog != null) {
                View findViewById = dialog.findViewById(R.id.message_text_view);
                AnonymousClass009.A03(findViewById);
                ((TextView) findViewById).setText(Html.fromHtml(str));
                AnonymousClass0KS.A00(dialog, R.id.progress_bar).setVisibility(i);
                AnonymousClass0KS.A00(dialog, R.id.ok_button).setVisibility(i2);
                AnonymousClass0KS.A00(dialog, R.id.cancel_button).setVisibility(i3);
                AnonymousClass0KS.A00(dialog, R.id.add_button).setVisibility(i3);
            }
        }

        @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
        public void onDismiss(DialogInterface dialogInterface) {
            super.onDismiss(dialogInterface);
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                A0B.finish();
                A0B.overridePendingTransition(0, 0);
            }
        }
    }
}
