package com.whatsapp.stickers;

import X.AnonymousClass009;
import X.AnonymousClass01V;
import X.C14350lI;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/* loaded from: classes2.dex */
public class WebpUtils {
    public static final Random A00 = new Random();

    public static native boolean createFirstThumbnail(byte[] bArr, int i, String str);

    public static native byte[] fetchWebpMetadata(String str);

    public static native int getFirstWebpThumbnailMinimumFileLength(String str);

    public static native boolean insertWebpMetadata(String str, String str2, byte[] bArr);

    public static native WebpInfo verifyWebpFileIntegrity(String str);

    public static String A00(File file) {
        Throwable e;
        StringBuilder sb;
        String str;
        int i;
        AnonymousClass009.A00();
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-256");
                byte[] bArr = new byte[4];
                byte[] bArr2 = new byte[4];
                bufferedInputStream.skip(8);
                int i2 = 0;
                boolean z = true;
                int i3 = 0;
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byte b = bArr[0];
                    if (b == 69 && bArr[1] == 88 && bArr[2] == 73 && bArr[3] == 70) {
                        i3 = 4;
                    } else {
                        byte b2 = bArr2[3];
                        if (b2 == 69 && b == 88 && bArr[1] == 73 && bArr[2] == 70) {
                            i3 = 3;
                        } else {
                            byte b3 = bArr2[2];
                            if (b3 == 69 && b2 == 88 && b == 73 && bArr[1] == 70) {
                                i3 = 2;
                            } else if (bArr2[1] == 69 && b3 == 88 && b2 == 73 && b == 70) {
                                i3 = 1;
                            }
                        }
                    }
                    if (z) {
                        i = 4;
                        if (i3 == 4) {
                            break;
                        }
                    } else if (i3 > 0) {
                        instance.update(bArr2, 0, i3);
                        break;
                    } else {
                        instance.update(bArr2, 0, i2);
                        i = 4;
                    }
                    System.arraycopy(bArr, 0, bArr2, 0, i);
                    i2 = read;
                    z = false;
                }
                instance.update(bArr2, 0, i2);
                String encodeToString = Base64.encodeToString(instance.digest(), 2);
                file.getAbsolutePath();
                bufferedInputStream.close();
                return encodeToString;
            } catch (Throwable th) {
                try {
                    bufferedInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (FileNotFoundException e2) {
            e = e2;
            sb = new StringBuilder();
            str = "WebpUtils/getFileHashExcludingMetadata/file not found:";
            sb.append(str);
            sb.append(file.getAbsolutePath());
            Log.e(sb.toString(), e);
            return null;
        } catch (IOException e3) {
            e = e3;
            sb = new StringBuilder();
            str = "WebpUtils/getFileHashExcludingMetadata/io exception, file path:";
            sb.append(str);
            sb.append(file.getAbsolutePath());
            Log.e(sb.toString(), e);
            return null;
        } catch (NoSuchAlgorithmException e4) {
            Log.e("WebpUtils/getFileHashExcludingMetadata/no such algorithms exception", e4);
            return null;
        }
    }

    public static boolean A01(File file, byte[] bArr) {
        File file2;
        if (!file.exists()) {
            return false;
        }
        if (bArr == null || bArr.length == 0) {
            return true;
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(file.getAbsolutePath());
            sb.append(".");
            sb.append(A00.nextLong());
            sb.append(".tmp");
            file2 = new File(sb.toString());
            try {
                new String(bArr, AnonymousClass01V.A08);
                boolean insertWebpMetadata = insertWebpMetadata(file.getAbsolutePath(), file2.getAbsolutePath(), bArr);
                file.getAbsolutePath();
                if (insertWebpMetadata) {
                    boolean renameTo = file2.renameTo(file);
                    file2.getAbsolutePath();
                    file.getAbsolutePath();
                    return renameTo;
                }
            } catch (UnsupportedEncodingException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("WebpUtils/insertWebpMetadata/error when converting bytes to string, input file:");
                sb2.append(file);
                Log.e(sb2.toString(), e);
            }
            return false;
        } finally {
            C14350lI.A0M(file2);
        }
    }
}
