package com.whatsapp.stickers;

import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02H;
import X.AnonymousClass0F6;
import X.AnonymousClass12V;
import X.AnonymousClass146;
import X.AnonymousClass1AB;
import X.AnonymousClass1KM;
import X.AnonymousClass1KZ;
import X.AnonymousClass376;
import X.AnonymousClass3NR;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C18170s1;
import X.C235512c;
import X.C27531Hw;
import X.C33501eB;
import X.C38671oW;
import X.C38721ob;
import X.C54082g9;
import X.C621735g;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.productinfra.avatar.ui.stickers.upsell.AvatarStickerUpsellView;
import java.util.List;

/* loaded from: classes2.dex */
public abstract class StickerStoreTabFragment extends Hilt_StickerStoreTabFragment {
    public int A00;
    public LayoutInflater A01;
    public View A02;
    public LinearLayoutManager A03;
    public RecyclerView A04;
    public C14900mE A05;
    public AnonymousClass018 A06;
    public C14850m9 A07;
    public AnonymousClass12V A08;
    public C18170s1 A09;
    public AnonymousClass1AB A0A;
    public AnonymousClass146 A0B;
    public C235512c A0C;
    public C38671oW A0D;
    public List A0E;
    public final ViewTreeObserver.OnGlobalLayoutListener A0F = new AnonymousClass3NR(this);
    public final AnonymousClass1KM A0G = new C33501eB(this);

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        int i;
        this.A01 = layoutInflater;
        boolean z = this instanceof StickerStoreMyTabFragment;
        if (!z) {
            boolean A07 = this.A07.A07(1396);
            i = R.layout.sticker_store_featured_layout;
            if (A07) {
                i = R.layout.sticker_store_featured_layout_with_avatar;
            }
        } else {
            i = R.layout.sticker_store_my_layout;
        }
        View inflate = layoutInflater.inflate(i, viewGroup, false);
        this.A04 = (RecyclerView) AnonymousClass028.A0D(inflate, R.id.store_recycler_view);
        this.A02 = AnonymousClass028.A0D(inflate, R.id.store_progress);
        A0p();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        this.A03 = linearLayoutManager;
        linearLayoutManager.A1Q(1);
        LinearLayoutManager linearLayoutManager2 = this.A03;
        ((AnonymousClass02H) linearLayoutManager2).A0A = true;
        this.A04.setLayoutManager(linearLayoutManager2);
        this.A04.getViewTreeObserver().addOnGlobalLayoutListener(this.A0F);
        this.A04.setNestedScrollingEnabled(true);
        this.A0B.A03(this.A0G);
        if (z) {
            StickerStoreMyTabFragment stickerStoreMyTabFragment = (StickerStoreMyTabFragment) this;
            stickerStoreMyTabFragment.A00 = inflate.findViewById(R.id.empty);
            TextView textView = (TextView) inflate.findViewById(R.id.get_stickers_button);
            C27531Hw.A06(textView);
            textView.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(stickerStoreMyTabFragment, 45));
        } else if (this instanceof StickerStoreFeaturedTabFragment) {
            StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = (StickerStoreFeaturedTabFragment) this;
            stickerStoreFeaturedTabFragment.A01 = inflate.findViewById(R.id.empty);
            View A0D = AnonymousClass028.A0D(inflate, R.id.floating_discover_third_party_app_button);
            stickerStoreFeaturedTabFragment.A02 = A0D;
            A0D.setVisibility(0);
            stickerStoreFeaturedTabFragment.A02.setContentDescription(stickerStoreFeaturedTabFragment.A0I(R.string.sticker_store_discover_sticker_apps_footer));
            stickerStoreFeaturedTabFragment.A02.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(stickerStoreFeaturedTabFragment, 44));
            if (((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A07.A07(1396)) {
                stickerStoreFeaturedTabFragment.A05.A01(3);
                NestedScrollView nestedScrollView = (NestedScrollView) AnonymousClass028.A0D(inflate, R.id.nested_scroll_view);
                stickerStoreFeaturedTabFragment.A06 = (AvatarStickerUpsellView) AnonymousClass028.A0D(inflate, R.id.sticker_avatar_upsell);
                Rect rect = new Rect();
                stickerStoreFeaturedTabFragment.A00 = rect;
                nestedScrollView.getHitRect(rect);
                nestedScrollView.A0E = stickerStoreFeaturedTabFragment.A09;
            } else {
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A04.A0n(stickerStoreFeaturedTabFragment.A0A);
            }
        }
        A1B();
        if (!z) {
            StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment2 = (StickerStoreFeaturedTabFragment) this;
            ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment2).A02.setVisibility(0);
            if (!stickerStoreFeaturedTabFragment2.A07) {
                stickerStoreFeaturedTabFragment2.A08 = true;
                C235512c r3 = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment2).A0C;
                r3.A0Y.Aaz(new AnonymousClass376(new C621735g(stickerStoreFeaturedTabFragment2), r3), new Object[0]);
            }
            return inflate;
        }
        StickerStoreMyTabFragment stickerStoreMyTabFragment2 = (StickerStoreMyTabFragment) this;
        stickerStoreMyTabFragment2.A05 = false;
        AnonymousClass0F6 r1 = new AnonymousClass0F6(new C54082g9(stickerStoreMyTabFragment2));
        stickerStoreMyTabFragment2.A01 = r1;
        r1.A0C(((StickerStoreTabFragment) stickerStoreMyTabFragment2).A04);
        ((StickerStoreTabFragment) stickerStoreMyTabFragment2).A02.postDelayed(new RunnableBRunnable0Shape12S0100000_I0_12(stickerStoreMyTabFragment2, 24), 300);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A04.getViewTreeObserver().removeGlobalOnLayoutListener(this.A0F);
        C235512c r2 = this.A0C;
        C38721ob r0 = r2.A00;
        if (r0 != null) {
            r0.A02.A02(false);
            r2.A00 = null;
        }
        AnonymousClass1AB r02 = this.A0A;
        if (r02 != null) {
            r02.A03();
        }
        this.A0B.A04(this.A0G);
        super.A12();
    }

    public void A1A() {
        if (this instanceof StickerStoreFeaturedTabFragment) {
            StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = (StickerStoreFeaturedTabFragment) this;
            if (stickerStoreFeaturedTabFragment.A07) {
                stickerStoreFeaturedTabFragment.A08 = true;
                C235512c r3 = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0C;
                r3.A0Y.Aaz(new AnonymousClass376(new C621735g(stickerStoreFeaturedTabFragment), r3), new Object[0]);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r0.A0D() != 0) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1B() {
        /*
            r3 = this;
            X.1oW r0 = r3.A0D
            if (r0 == 0) goto L_0x000b
            int r0 = r0.A0D()
            r2 = 1
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            r2 = 0
        L_0x000c:
            r1 = r3
            boolean r0 = r3 instanceof com.whatsapp.stickers.StickerStoreMyTabFragment
            if (r0 != 0) goto L_0x0020
            com.whatsapp.stickers.StickerStoreFeaturedTabFragment r1 = (com.whatsapp.stickers.StickerStoreFeaturedTabFragment) r1
            android.view.View r1 = r1.A01
        L_0x0015:
            if (r1 == 0) goto L_0x001f
            r0 = 8
            if (r2 == 0) goto L_0x001c
            r0 = 0
        L_0x001c:
            r1.setVisibility(r0)
        L_0x001f:
            return
        L_0x0020:
            com.whatsapp.stickers.StickerStoreMyTabFragment r1 = (com.whatsapp.stickers.StickerStoreMyTabFragment) r1
            android.view.View r1 = r1.A00
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.stickers.StickerStoreTabFragment.A1B():void");
    }

    public void A1C(AnonymousClass1KZ r4, int i) {
        String str;
        Context A0p = A0p();
        String str2 = r4.A0D;
        if (!(this instanceof StickerStoreMyTabFragment)) {
            str = "sticker_store_featured_tab";
        } else {
            str = "sticker_store_my_tab";
        }
        A0C().startActivityForResult(C14960mK.A0Y(A0p, str2, str), 1);
    }
}
