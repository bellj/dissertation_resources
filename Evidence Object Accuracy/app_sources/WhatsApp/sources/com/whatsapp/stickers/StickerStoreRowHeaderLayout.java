package com.whatsapp.stickers;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/* loaded from: classes2.dex */
public class StickerStoreRowHeaderLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public StickerStoreRowHeaderLayout(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public StickerStoreRowHeaderLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }

    public StickerStoreRowHeaderLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public StickerStoreRowHeaderLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        String str;
        super.onMeasure(i, i2);
        if (getChildCount() != 4) {
            str = "StickerStoreRowHeaderLayout should have 4 children!";
        } else {
            int measuredWidth = getMeasuredWidth();
            View childAt = getChildAt(0);
            View childAt2 = getChildAt(1);
            View childAt3 = getChildAt(2);
            View childAt4 = getChildAt(3);
            if (childAt == null || childAt3 == null || childAt4 == null || childAt2 == null) {
                str = "StickerStoreRowHeaderLayout should have 4 children! Title View, Animated View, Author View and a Remaining View";
            } else {
                childAt4.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), i2);
                int max = Math.max(measuredWidth - childAt4.getMeasuredWidth(), 0);
                childAt2.measure(View.MeasureSpec.makeMeasureSpec(max, Integer.MIN_VALUE), i2);
                int measuredWidth2 = max - childAt2.getMeasuredWidth();
                childAt.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth2, Integer.MIN_VALUE), i2);
                childAt3.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth2, Integer.MIN_VALUE), i2);
                int measuredWidth3 = childAt3.getMeasuredWidth();
                int measuredWidth4 = childAt.getMeasuredWidth();
                if (measuredWidth3 + measuredWidth4 > measuredWidth2) {
                    int min = Math.min(measuredWidth2 / 3, measuredWidth3);
                    measuredWidth4 = Math.min((measuredWidth2 << 1) / 3, measuredWidth4);
                    int i3 = measuredWidth2 - (min + measuredWidth4);
                    if (min == measuredWidth3) {
                        measuredWidth4 += i3;
                    } else {
                        min += i3;
                    }
                    measuredWidth3 = min;
                }
                childAt3.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth3, Integer.MIN_VALUE), i2);
                childAt.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth4, Integer.MIN_VALUE), i2);
                return;
            }
        }
        AnonymousClass009.A07(str);
    }
}
