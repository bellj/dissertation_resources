package com.whatsapp.stickers;

import X.AbstractC14440lR;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass04S;
import X.AnonymousClass1KR;
import X.AnonymousClass1KS;
import X.C002701f;
import X.C004802e;
import X.C12980iv;
import X.C235512c;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class StarStickerFromPickerDialogFragment extends Hilt_StarStickerFromPickerDialogFragment {
    public C002701f A00;
    public AnonymousClass1KR A01;
    public AnonymousClass1KS A02;
    public C235512c A03;
    public AbstractC14440lR A04;

    @Override // com.whatsapp.stickers.Hilt_StarStickerFromPickerDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A01 = (AnonymousClass1KR) context;
        } catch (ClassCastException unused) {
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        Parcelable parcelable = A03().getParcelable("sticker");
        AnonymousClass009.A05(parcelable);
        this.A02 = (AnonymousClass1KS) parcelable;
        C004802e A0S = C12980iv.A0S(A0C);
        A0S.A06(R.string.sticker_save_to_picker_title);
        String A0I = A0I(R.string.sticker_save_to_picker);
        A0S.A03(new IDxCListenerShape8S0100000_1_I1(this, 34), A0I);
        A0S.setNegativeButton(R.string.cancel, null);
        AnonymousClass04S create = A0S.create();
        create.setOnShowListener(new DialogInterface.OnShowListener(A0I) { // from class: X.4hb
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                AnonymousClass04S r0 = AnonymousClass04S.this;
                r0.A00.A0G.setContentDescription(this.A01);
            }
        });
        return create;
    }
}
