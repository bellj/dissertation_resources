package com.whatsapp.stickers;

import X.AbstractC13900kW;
import X.AbstractC14440lR;
import X.AbstractC39661qJ;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass028;
import X.AnonymousClass04S;
import X.AnonymousClass0U5;
import X.AnonymousClass12P;
import X.AnonymousClass12V;
import X.AnonymousClass1AB;
import X.AnonymousClass1KS;
import X.AnonymousClass4U1;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C18120rw;
import X.C18170s1;
import X.C235512c;
import X.C26671Ej;
import X.C27531Hw;
import X.C625637s;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerInfoDialogFragment;
import java.util.Collections;

/* loaded from: classes2.dex */
public class StickerInfoDialogFragment extends Hilt_StickerInfoDialogFragment {
    public int A00;
    public View A01;
    public View A02;
    public Button A03;
    public Button A04;
    public Button A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public AnonymousClass12P A09;
    public C14850m9 A0A;
    public AnonymousClass12V A0B;
    public C18170s1 A0C;
    public C18120rw A0D;
    public AnonymousClass1KS A0E;
    public AnonymousClass1AB A0F;
    public AnonymousClass4U1 A0G;
    public C235512c A0H;
    public StickerView A0I;
    public C26671Ej A0J;
    public AbstractC14440lR A0K;
    public boolean A0L;
    public final DialogInterface.OnClickListener A0M = new IDxCListenerShape8S0100000_1_I1(this, 35);
    public final DialogInterface.OnClickListener A0N = new IDxCListenerShape9S0100000_2_I1(this, 70);

    public static StickerInfoDialogFragment A00(AnonymousClass1KS r3, boolean z) {
        StickerInfoDialogFragment stickerInfoDialogFragment = new StickerInfoDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("sticker", r3);
        A0D.putBoolean("from_me", z);
        stickerInfoDialogFragment.A0U(A0D);
        return stickerInfoDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A0F = null;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        AnonymousClass0U5 r1 = ((AnonymousClass04S) ((DialogFragment) this).A03).A00;
        Button button = r1.A0G;
        this.A03 = button;
        this.A04 = r1.A0E;
        this.A05 = r1.A0F;
        if (this.A0F != null && this.A0E != null && this.A0I != null && this.A0G == null) {
            button.setVisibility(8);
            this.A04.setVisibility(8);
            this.A05.setVisibility(8);
            AnonymousClass1AB r0 = this.A0F;
            AnonymousClass1KS r2 = this.A0E;
            StickerView stickerView = this.A0I;
            int i = this.A00;
            r0.A04(stickerView, r2, new AbstractC39661qJ() { // from class: X.59R
                @Override // X.AbstractC39661qJ
                public final void AWe(boolean z) {
                    StickerInfoDialogFragment.this.A0I.A02();
                }
            }, 1, i, i, true, false);
            C235512c r10 = this.A0H;
            AnonymousClass1KS r8 = this.A0E;
            C12990iw.A1N(new C625637s(this.A0B, this.A0C, r8, this, r10), this.A0K);
        }
    }

    @Override // com.whatsapp.stickers.Hilt_StickerInfoDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        AnonymousClass009.A0A("StickerInfoDialogFragment requires host context implement StickerImageFileLoaderProvider", context instanceof AbstractC13900kW);
        this.A0F = ((AbstractC13900kW) context).AGx();
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        Bundle A03 = A03();
        this.A0E = (AnonymousClass1KS) A03.getParcelable("sticker");
        this.A0L = A03.getBoolean("from_me");
        C004802e A0S = C12980iv.A0S(A0C);
        LayoutInflater layoutInflater = A0C.getLayoutInflater();
        this.A00 = A02().getDimensionPixelSize(R.dimen.conversation_row_sticker_size);
        View inflate = layoutInflater.inflate(R.layout.sticker_detail_dialog, (ViewGroup) null);
        StickerView stickerView = (StickerView) AnonymousClass028.A0D(inflate, R.id.sticker_view);
        this.A0I = stickerView;
        stickerView.A03 = true;
        this.A01 = AnonymousClass028.A0D(inflate, R.id.progress_view);
        this.A02 = AnonymousClass028.A0D(inflate, R.id.sticker_info_container);
        this.A07 = C12960it.A0I(inflate, R.id.sticker_pack_name);
        this.A08 = C12960it.A0I(inflate, R.id.sticker_pack_publisher);
        this.A06 = C12960it.A0I(inflate, R.id.bullet_sticker_info);
        C27531Hw.A06(this.A07);
        C12990iw.A0z(this.A0M, null, A0S, R.string.sticker_remove_from_favorites);
        A0S.A00(R.string.sticker_remove_from_favorites, this.A0N);
        A0S.setView(inflate);
        return A0S.create();
    }

    public final void A1K(AnonymousClass1KS r6, AnonymousClass4U1 r7) {
        if (r7.A06) {
            C235512c r4 = this.A0H;
            r4.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r4, 6, Collections.singleton(r6)));
            return;
        }
        this.A0H.A0K(Collections.singleton(r6));
        boolean z = r7.A05;
        C26671Ej r0 = this.A0J;
        if (z) {
            r0.A05("starred");
        } else {
            r0.A06("starred");
        }
    }
}
