package com.whatsapp.stickers;

import X.AbstractC15420nE;
import X.AnonymousClass0o6;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class WhitelistPackQueryContentProvider extends AbstractC15420nE {
    public UriMatcher A00;
    public AnonymousClass0o6 A01;

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        A01();
        StringBuilder sb = new StringBuilder("vnd.android.cursor.item/vnd.");
        sb.append("com.whatsapp.provider.sticker_whitelist_check");
        sb.append(".");
        sb.append("is_whitelisted");
        return sb.toString();
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        UriMatcher uriMatcher;
        ProviderInfo resolveContentProvider;
        String str3;
        A01();
        try {
            synchronized (this) {
                if (this.A00 == null) {
                    UriMatcher uriMatcher2 = new UriMatcher(-1);
                    this.A00 = uriMatcher2;
                    uriMatcher2.addURI("com.whatsapp.provider.sticker_whitelist_check", "is_whitelisted", 1);
                }
                uriMatcher = this.A00;
            }
            int i = 1;
            if (uriMatcher.match(uri) == 1 && getContext() != null) {
                PackageManager packageManager = getContext().getPackageManager();
                String queryParameter = uri.getQueryParameter("authority");
                String queryParameter2 = uri.getQueryParameter("identifier");
                if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2) && (resolveContentProvider = packageManager.resolveContentProvider(queryParameter, 128)) != null) {
                    if (Build.VERSION.SDK_INT >= 19) {
                        str3 = getCallingPackage();
                    } else {
                        str3 = packageManager.getNameForUid(Binder.getCallingUid());
                    }
                    if (str3 == null || !str3.equals(resolveContentProvider.packageName)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("the calling package ");
                        sb.append(str3);
                        sb.append(" does not own the queried authority: ");
                        sb.append(queryParameter);
                        Log.w(sb.toString());
                    } else {
                        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"result"});
                        MatrixCursor.RowBuilder newRow = matrixCursor.newRow();
                        if (!this.A01.A02(queryParameter, queryParameter2)) {
                            i = 0;
                        }
                        newRow.add(Integer.valueOf(i));
                        return matrixCursor;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            Log.e("Exception when querying whitelist packs", e);
            return null;
        }
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }
}
