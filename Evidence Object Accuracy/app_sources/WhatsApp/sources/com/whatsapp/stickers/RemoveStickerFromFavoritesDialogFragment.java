package com.whatsapp.stickers;

import X.ActivityC000900k;
import X.AnonymousClass1KS;
import X.C004802e;
import X.C12960it;
import X.C12980iv;
import X.C235512c;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class RemoveStickerFromFavoritesDialogFragment extends Hilt_RemoveStickerFromFavoritesDialogFragment {
    public AnonymousClass1KS A00;
    public C235512c A01;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        this.A00 = (AnonymousClass1KS) A03().getParcelable("sticker");
        C004802e A0S = C12980iv.A0S(A0C);
        A0S.A06(R.string.sticker_remove_from_tray_title);
        return C12960it.A0L(new IDxCListenerShape8S0100000_1_I1(this, 33), A0S, R.string.sticker_remove_from_tray);
    }
}
