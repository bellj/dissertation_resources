package com.whatsapp.stickers;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass12V;
import X.AnonymousClass146;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass1AB;
import X.C14850m9;
import X.C14900mE;
import X.C16120oU;
import X.C235512c;
import X.C235812f;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_StickerStoreTabFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A19()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.stickers.Hilt_StickerStoreTabFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    public void A18() {
        if (this instanceof Hilt_StickerStoreMyTabFragment) {
            Hilt_StickerStoreMyTabFragment hilt_StickerStoreMyTabFragment = (Hilt_StickerStoreMyTabFragment) this;
            if (!hilt_StickerStoreMyTabFragment.A02) {
                hilt_StickerStoreMyTabFragment.A02 = true;
                StickerStoreMyTabFragment stickerStoreMyTabFragment = (StickerStoreMyTabFragment) hilt_StickerStoreMyTabFragment;
                AnonymousClass01J r2 = ((C51112Sw) ((AbstractC51092Su) hilt_StickerStoreMyTabFragment.generatedComponent())).A0Y;
                ((WaFragment) stickerStoreMyTabFragment).A00 = (AnonymousClass182) r2.A94.get();
                ((WaFragment) stickerStoreMyTabFragment).A01 = (AnonymousClass180) r2.ALt.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A07 = (C14850m9) r2.A04.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A05 = (C14900mE) r2.A8X.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A06 = (AnonymousClass018) r2.ANb.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0B = (AnonymousClass146) r2.AKM.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A08 = (AnonymousClass12V) r2.A0p.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A09 = r2.A41();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0C = (C235512c) r2.AKS.get();
                ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0A = (AnonymousClass1AB) r2.AKI.get();
                stickerStoreMyTabFragment.A04 = (AbstractC14440lR) r2.ANe.get();
                stickerStoreMyTabFragment.A02 = r2.A42();
            }
        } else if (this instanceof Hilt_StickerStoreFeaturedTabFragment) {
            Hilt_StickerStoreFeaturedTabFragment hilt_StickerStoreFeaturedTabFragment = (Hilt_StickerStoreFeaturedTabFragment) this;
            if (!hilt_StickerStoreFeaturedTabFragment.A02) {
                hilt_StickerStoreFeaturedTabFragment.A02 = true;
                StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = (StickerStoreFeaturedTabFragment) hilt_StickerStoreFeaturedTabFragment;
                AnonymousClass01J r22 = ((C51112Sw) ((AbstractC51092Su) hilt_StickerStoreFeaturedTabFragment.generatedComponent())).A0Y;
                ((WaFragment) stickerStoreFeaturedTabFragment).A00 = (AnonymousClass182) r22.A94.get();
                ((WaFragment) stickerStoreFeaturedTabFragment).A01 = (AnonymousClass180) r22.ALt.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A07 = (C14850m9) r22.A04.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A05 = (C14900mE) r22.A8X.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A06 = (AnonymousClass018) r22.ANb.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0B = (AnonymousClass146) r22.AKM.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A08 = (AnonymousClass12V) r22.A0p.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A09 = r22.A41();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0C = (C235512c) r22.AKS.get();
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0A = (AnonymousClass1AB) r22.AKI.get();
                stickerStoreFeaturedTabFragment.A04 = (C16120oU) r22.ANE.get();
                stickerStoreFeaturedTabFragment.A03 = (AnonymousClass12P) r22.A0H.get();
                stickerStoreFeaturedTabFragment.A05 = (C235812f) r22.A13.get();
            }
        } else if (!this.A02) {
            this.A02 = true;
            StickerStoreTabFragment stickerStoreTabFragment = (StickerStoreTabFragment) this;
            AnonymousClass01J r23 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            ((WaFragment) stickerStoreTabFragment).A00 = (AnonymousClass182) r23.A94.get();
            ((WaFragment) stickerStoreTabFragment).A01 = (AnonymousClass180) r23.ALt.get();
            stickerStoreTabFragment.A07 = (C14850m9) r23.A04.get();
            stickerStoreTabFragment.A05 = (C14900mE) r23.A8X.get();
            stickerStoreTabFragment.A06 = (AnonymousClass018) r23.ANb.get();
            stickerStoreTabFragment.A0B = (AnonymousClass146) r23.AKM.get();
            stickerStoreTabFragment.A08 = (AnonymousClass12V) r23.A0p.get();
            stickerStoreTabFragment.A09 = r23.A41();
            stickerStoreTabFragment.A0C = (C235512c) r23.AKS.get();
            stickerStoreTabFragment.A0A = (AnonymousClass1AB) r23.AKI.get();
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
