package com.whatsapp.stickers;

import X.AbstractActivityC48272Fh;
import X.ActivityC13810kN;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass0B5;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass3FN;
import X.AnonymousClass3S6;
import X.AnonymousClass3S8;
import X.AnonymousClass51G;
import X.C48282Fi;
import X.C80653se;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class StickerStoreActivity extends AbstractActivityC48272Fh {
    public View A00;
    public ViewPager A01;
    public BottomSheetBehavior A02;
    public TabLayout A03;
    public AnonymousClass018 A04;
    public C48282Fi A05;
    public StickerStoreFeaturedTabFragment A06;
    public StickerStoreMyTabFragment A07;

    public final void A2e(AnonymousClass01E r3, int i) {
        this.A05.A00.add(r3);
        TabLayout tabLayout = this.A03;
        AnonymousClass3FN A03 = tabLayout.A03();
        A03.A01(i);
        tabLayout.A0E(A03);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1 && i2 == 1 && this.A05 != null) {
            this.A01.postDelayed(new RunnableBRunnable0Shape12S0100000_I0_12(this, 21), 300);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.sticker_store);
        View view = ((ActivityC13810kN) this).A00;
        this.A00 = view;
        View findViewById = view.findViewById(R.id.store_container);
        this.A03 = (TabLayout) this.A00.findViewById(R.id.sticker_store_tabs);
        this.A01 = (ViewPager) this.A00.findViewById(R.id.sticker_store_pager);
        this.A05 = new C48282Fi(A0V());
        this.A06 = new StickerStoreFeaturedTabFragment();
        this.A07 = new StickerStoreMyTabFragment();
        StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = this.A06;
        boolean z = false;
        if (bundle == null) {
            z = true;
        }
        stickerStoreFeaturedTabFragment.A07 = z;
        AnonymousClass028.A0c(this.A03, 0);
        if (!this.A04.A04().A06) {
            A2e(this.A06, R.string.sticker_store_featured_tab_title);
            A2e(this.A07, R.string.sticker_store_my_tab_title);
        } else {
            A2e(this.A07, R.string.sticker_store_my_tab_title);
            A2e(this.A06, R.string.sticker_store_featured_tab_title);
        }
        this.A01.setAdapter(this.A05);
        this.A01.A0G(new AnonymousClass3S8(this.A03));
        this.A01.A0G(new AnonymousClass3S6(this));
        this.A01.A0F((!this.A04.A04().A06 ? 1 : 0) ^ 1, false);
        this.A03.A0D(new AnonymousClass51G(this));
        Toolbar toolbar = (Toolbar) findViewById.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), this.A04));
        toolbar.setNavigationContentDescription(R.string.sticker_store_back_button_content_description);
        toolbar.setTitle(R.string.sticker_store_title);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 43));
        AnonymousClass3 r1 = new BottomSheetBehavior() { // from class: com.whatsapp.stickers.StickerStoreActivity.3
        };
        this.A02 = r1;
        r1.A0J = true;
        r1.A0M(4);
        this.A02.A0N = true;
        BottomSheetBehavior bottomSheetBehavior = this.A02;
        ((AnonymousClass0B5) findViewById.getLayoutParams()).A00(bottomSheetBehavior);
        bottomSheetBehavior.A0E = new C80653se(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        BottomSheetBehavior bottomSheetBehavior = this.A02;
        if (bottomSheetBehavior != null && bottomSheetBehavior.A0B == 4) {
            this.A00.post(new RunnableBRunnable0Shape12S0100000_I0_12(this, 22));
        }
    }
}
