package com.whatsapp.stickers;

import X.AbstractC05270Ox;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC17410ql;
import X.AbstractC20260vT;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass146;
import X.AnonymousClass15J;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1I1;
import X.AnonymousClass1KM;
import X.AnonymousClass1KO;
import X.AnonymousClass1KQ;
import X.AnonymousClass1KR;
import X.AnonymousClass1KS;
import X.AnonymousClass1KZ;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass377;
import X.AnonymousClass3NQ;
import X.AnonymousClass4LF;
import X.AnonymousClass4OS;
import X.C103584qz;
import X.C1111858k;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C17050qB;
import X.C18120rw;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20980wd;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C235512c;
import X.C235812f;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C54472gm;
import X.C627938p;
import X.C69823aI;
import X.C75123jL;
import X.C91014Qc;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import com.whatsapp.util.ViewOnClickCListenerShape16S0100000_I0_3;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class StickerStorePackPreviewActivity extends ActivityC13790kL implements AbstractC20260vT, AnonymousClass1KQ, AnonymousClass1KR {
    public int A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public ImageView A05;
    public ImageView A06;
    public TextView A07;
    public TextView A08;
    public TextView A09;
    public GridLayoutManager A0A;
    public RecyclerView A0B;
    public Button A0C;
    public Button A0D;
    public Button A0E;
    public C17050qB A0F;
    public C18120rw A0G;
    public C20980wd A0H;
    public C235812f A0I;
    public AnonymousClass1AB A0J;
    public AnonymousClass146 A0K;
    public C91014Qc A0L;
    public C54472gm A0M;
    public C235512c A0N;
    public C627938p A0O;
    public StickerView A0P;
    public AnonymousClass15J A0Q;
    public String A0R;
    public Map A0S;
    public Map A0T;
    public Set A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public final ViewTreeObserver.OnGlobalLayoutListener A0b;
    public final AbstractC05270Ox A0c;
    public final AbstractC17410ql A0d;
    public final AnonymousClass1KM A0e;
    public final AnonymousClass4LF A0f;

    public StickerStorePackPreviewActivity() {
        this(0);
        this.A0e = new AnonymousClass1KO(this);
        this.A0d = new C1111858k(this);
        this.A0V = false;
        this.A0c = new C75123jL(this);
        this.A0f = new AnonymousClass4LF(this);
        this.A0b = new AnonymousClass3NQ(this);
    }

    public StickerStorePackPreviewActivity(int i) {
        this.A0X = false;
        A0R(new C103584qz(this));
    }

    public static /* synthetic */ void A02(AnonymousClass1KZ r8, StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        C91014Qc r1 = stickerStorePackPreviewActivity.A0L;
        r1.A02 = r8;
        r1.A01 = new SparseBooleanArray();
        r1.A00 = new SparseBooleanArray();
        stickerStorePackPreviewActivity.A0T = new HashMap();
        stickerStorePackPreviewActivity.A0U = null;
        ((ActivityC13830kP) stickerStorePackPreviewActivity).A05.Aaz(new AnonymousClass377(stickerStorePackPreviewActivity.A0N, new AnonymousClass4OS(r8, stickerStorePackPreviewActivity)), r8);
        for (int i = 0; i < r8.A04.size(); i++) {
            stickerStorePackPreviewActivity.A0T.put(((AnonymousClass1KS) r8.A04.get(i)).A0C, Integer.valueOf(i));
        }
        if (stickerStorePackPreviewActivity.A0M == null) {
            C54472gm r12 = new C54472gm(stickerStorePackPreviewActivity.A0J, stickerStorePackPreviewActivity.A0P, stickerStorePackPreviewActivity.A0N.A04(), stickerStorePackPreviewActivity.getResources().getDimensionPixelSize(R.dimen.sticker_store_preview_item), stickerStorePackPreviewActivity.getResources().getDimensionPixelSize(R.dimen.sticker_store_preview_padding), true, stickerStorePackPreviewActivity.A0a);
            stickerStorePackPreviewActivity.A0M = r12;
            r12.A05 = stickerStorePackPreviewActivity.A0f;
            stickerStorePackPreviewActivity.A0B.setAdapter(r12);
        }
        C54472gm r13 = stickerStorePackPreviewActivity.A0M;
        r13.A04 = stickerStorePackPreviewActivity.A0L;
        r13.A02();
        stickerStorePackPreviewActivity.A2e();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0X) {
            this.A0X = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0I = (C235812f) r1.A13.get();
            this.A0K = (AnonymousClass146) r1.AKM.get();
            this.A0F = (C17050qB) r1.ABL.get();
            this.A0N = (C235512c) r1.AKS.get();
            this.A0J = (AnonymousClass1AB) r1.AKI.get();
            this.A0G = r1.A42();
            this.A0H = (C20980wd) r1.A0t.get();
            this.A0Q = (AnonymousClass15J) r1.AKD.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0100, code lost:
        if (r7 != false) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x017d, code lost:
        if (r6.A01() == false) goto L_0x017f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2e() {
        /*
        // Method dump skipped, instructions count: 512
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.stickers.StickerStorePackPreviewActivity.A2e():void");
    }

    public final boolean A2f() {
        String str;
        return ((ActivityC13810kN) this).A0C.A07(1396) && (str = this.A0R) != null && str.equals("meta-avatar");
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r2) {
        if (r2.A01) {
            A2e();
            C54472gm r0 = this.A0M;
            if (r0 != null) {
                r0.A02();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 28) {
            super.onActivityResult(i, i2, intent);
            finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.sticker_store_pack_preview);
        this.A0R = getIntent().getStringExtra("sticker_pack_id");
        this.A0L = new C91014Qc();
        String stringExtra = getIntent().getStringExtra("sticker_pack_preview_source");
        this.A0a = "sticker_store_my_tab".equals(stringExtra);
        this.A0Y = "deeplink".equals(stringExtra);
        this.A0Z = "info_dialog".equals(stringExtra);
        this.A0K.A03(this.A0e);
        if (A2f()) {
            this.A0H.A03(this.A0d);
        }
        this.A0N.A0J(new C69823aI(this), this.A0R, true);
        if (this.A0R == null) {
            Log.e("StickerStorePackPreviewActivity/onCreate no pack id passed");
            finish();
        }
        View view = ((ActivityC13810kN) this).A00;
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), ((ActivityC13830kP) this).A01));
        toolbar.setTitle(R.string.sticker_store_pack_preview_title);
        toolbar.setNavigationContentDescription(R.string.sticker_pack_preview_back_button_content_description);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 46));
        A1e(toolbar);
        this.A01 = view.findViewById(R.id.details_container);
        this.A04 = view.findViewById(R.id.loading_progress);
        this.A08 = (TextView) view.findViewById(R.id.pack_preview_title);
        this.A09 = (TextView) view.findViewById(R.id.pack_preview_publisher);
        this.A07 = (TextView) view.findViewById(R.id.pack_preview_description);
        this.A03 = view.findViewById(R.id.pack_download_progress);
        this.A06 = (ImageView) view.findViewById(R.id.pack_tray_icon);
        this.A02 = view.findViewById(R.id.divider);
        this.A0D = (Button) view.findViewById(R.id.download_btn);
        this.A0C = (Button) view.findViewById(R.id.delete_btn);
        this.A0E = (Button) view.findViewById(R.id.edit_avatar_btn);
        this.A05 = (ImageView) view.findViewById(R.id.sticker_pack_animation_icon);
        this.A0D.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 48));
        this.A0C.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 49));
        this.A0E.setOnClickListener(new ViewOnClickCListenerShape16S0100000_I0_3(this, 0));
        this.A0A = new GridLayoutManager(1);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.sticker_preview_recycler);
        this.A0B = recyclerView;
        recyclerView.setLayoutManager(this.A0A);
        this.A0B.A0n(this.A0c);
        this.A0B.getViewTreeObserver().addOnGlobalLayoutListener(this.A0b);
        StickerView stickerView = (StickerView) view.findViewById(R.id.sticker_preview_expanded_sticker);
        this.A0P = stickerView;
        stickerView.A03 = true;
        ((ActivityC13810kN) this).A07.A03((Object) this);
        if (A2f()) {
            this.A0I.A02(16);
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!this.A0R.contains(" ")) {
            getMenuInflater().inflate(R.menu.sticker_pack_preview, menu);
            MenuItem findItem = menu.findItem(R.id.menu_sticker_pack_share);
            Drawable icon = findItem.getIcon();
            icon.mutate();
            icon.setColorFilter(getResources().getColor(R.color.sticker_pack_forward_button_color), PorterDuff.Mode.SRC_IN);
            findItem.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0K.A04(this.A0e);
        AnonymousClass1AB r0 = this.A0J;
        if (r0 != null) {
            r0.A03();
        }
        ((ActivityC13810kN) this).A07.A04(this);
        C627938p r1 = this.A0O;
        if (r1 != null) {
            r1.A03(true);
            this.A0O = null;
        }
        Map map = this.A0S;
        if (map != null) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(new ArrayList(map.values()), 25));
            this.A0S.clear();
            this.A0S = null;
        }
        if (A2f()) {
            this.A0H.A04(this.A0d);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menu_sticker_pack_share) {
            return super.onOptionsItemSelected(menuItem);
        }
        String format = String.format("https://wa.me/stickerpack/%s", this.A0R);
        Intent intent = new Intent();
        intent.setClassName(getPackageName(), "com.whatsapp.contact.picker.ContactPicker");
        intent.putExtra("android.intent.extra.TEXT", format);
        intent.setType("text/plain");
        intent.addFlags(524288);
        startActivity(intent);
        return true;
    }
}
