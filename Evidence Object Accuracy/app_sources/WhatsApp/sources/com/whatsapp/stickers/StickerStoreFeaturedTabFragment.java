package com.whatsapp.stickers;

import X.AbstractC05270Ox;
import X.AbstractC11770gq;
import X.AnonymousClass12P;
import X.AnonymousClass1KZ;
import X.AnonymousClass3PO;
import X.C16120oU;
import X.C235512c;
import X.C235812f;
import X.C54762hF;
import android.graphics.Rect;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.productinfra.avatar.ui.stickers.upsell.AvatarStickerUpsellView;

/* loaded from: classes2.dex */
public class StickerStoreFeaturedTabFragment extends Hilt_StickerStoreFeaturedTabFragment {
    public Rect A00;
    public View A01;
    public View A02;
    public AnonymousClass12P A03;
    public C16120oU A04;
    public C235812f A05;
    public AvatarStickerUpsellView A06;
    public boolean A07;
    public boolean A08;
    public final AbstractC11770gq A09 = new AnonymousClass3PO(this);
    public final AbstractC05270Ox A0A = new C54762hF(this);

    @Override // X.AnonymousClass01E
    public void A11() {
        this.A05.A00(3);
        super.A11();
    }

    @Override // com.whatsapp.stickers.StickerStoreTabFragment
    public void A1B() {
        super.A1B();
        View view = ((StickerStoreTabFragment) this).A02;
        if (view != null) {
            int i = 8;
            if (this.A08) {
                i = 0;
            }
            view.setVisibility(i);
        }
    }

    @Override // com.whatsapp.stickers.StickerStoreTabFragment
    public void A1C(AnonymousClass1KZ r5, int i) {
        super.A1C(r5, i);
        r5.A06 = false;
        ((StickerStoreTabFragment) this).A0D.A03(i);
        C235512c r3 = ((StickerStoreTabFragment) this).A0C;
        r3.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r3, 4, r5));
    }
}
