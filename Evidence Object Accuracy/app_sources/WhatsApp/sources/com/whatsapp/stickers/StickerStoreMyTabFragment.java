package com.whatsapp.stickers;

import X.AbstractC14440lR;
import X.AbstractC33511eG;
import X.AnonymousClass0F6;
import X.AnonymousClass1KZ;
import X.AnonymousClass2VT;
import X.C18120rw;
import X.C235512c;
import X.C38671oW;
import X.C38711oa;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import java.util.List;

/* loaded from: classes2.dex */
public class StickerStoreMyTabFragment extends Hilt_StickerStoreMyTabFragment implements AbstractC33511eG {
    public View A00;
    public AnonymousClass0F6 A01;
    public C18120rw A02;
    public C38711oa A03;
    public AbstractC14440lR A04;
    public boolean A05;

    @Override // X.AnonymousClass01E
    public void A14() {
        super.A14();
        List list = ((StickerStoreTabFragment) this).A0E;
        if (list != null && this.A05) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass1KZ) ((StickerStoreTabFragment) this).A0E.get(i)).A00 = size - i;
            }
            C235512c r4 = ((StickerStoreTabFragment) this).A0C;
            r4.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r4, 9, ((StickerStoreTabFragment) this).A0E));
        }
    }

    public final void A1D() {
        C38711oa r2;
        C38711oa r0 = this.A03;
        if (r0 != null) {
            r0.A03(true);
        }
        boolean A07 = ((StickerStoreTabFragment) this).A07.A07(1396);
        C235512c r02 = ((StickerStoreTabFragment) this).A0C;
        if (A07) {
            r2 = new C38711oa(r02, this, 0);
        } else {
            r2 = new C38711oa(r02, this, 1);
        }
        this.A03 = r2;
        this.A04.Aaz(r2, new Void[0]);
    }

    @Override // X.AbstractC33511eG
    public void ATJ(AnonymousClass1KZ r5) {
        C38671oW r3 = ((StickerStoreTabFragment) this).A0D;
        if ((r3 instanceof AnonymousClass2VT) && r3.A00 != null) {
            String str = r5.A0D;
            for (int i = 0; i < r3.A00.size(); i++) {
                if (str.equals(((AnonymousClass1KZ) r3.A00.get(i)).A0D)) {
                    r3.A00.set(i, r5);
                    r3.A03(i);
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC33511eG
    public void ATK(List list) {
        ((StickerStoreTabFragment) this).A0E = list;
        C38671oW r0 = ((StickerStoreTabFragment) this).A0D;
        if (r0 == null) {
            AnonymousClass2VT r3 = new AnonymousClass2VT(this, list);
            ((StickerStoreTabFragment) this).A0D = r3;
            RecyclerView recyclerView = ((StickerStoreTabFragment) this).A04;
            if (recyclerView != null) {
                recyclerView.setLayoutFrozen(false);
                recyclerView.A0j(r3, true, true);
                recyclerView.A0r(true);
                recyclerView.requestLayout();
            }
            A1B();
            return;
        }
        r0.A00 = list;
        r0.A02();
    }

    @Override // X.AbstractC33511eG
    public void ATL() {
        this.A03 = null;
    }

    @Override // X.AbstractC33511eG
    public void ATM(String str) {
        if (((StickerStoreTabFragment) this).A0E != null) {
            for (int i = 0; i < ((StickerStoreTabFragment) this).A0E.size(); i++) {
                if (((AnonymousClass1KZ) ((StickerStoreTabFragment) this).A0E.get(i)).A0D.equals(str)) {
                    ((StickerStoreTabFragment) this).A0E.remove(i);
                    C38671oW r1 = ((StickerStoreTabFragment) this).A0D;
                    if (r1 instanceof AnonymousClass2VT) {
                        r1.A00 = ((StickerStoreTabFragment) this).A0E;
                        r1.A02();
                        return;
                    }
                    return;
                }
            }
        }
    }
}
