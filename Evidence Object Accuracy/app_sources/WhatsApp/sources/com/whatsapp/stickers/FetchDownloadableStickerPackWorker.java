package com.whatsapp.stickers;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass043;
import X.AnonymousClass0GL;
import X.C006503b;
import X.C232911c;
import android.content.Context;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/* loaded from: classes2.dex */
public class FetchDownloadableStickerPackWorker extends Worker {
    public final C232911c A00;

    public FetchDownloadableStickerPackWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.A00 = (C232911c) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AKV.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        this.A00.A00();
        return new AnonymousClass0GL(C006503b.A01);
    }
}
