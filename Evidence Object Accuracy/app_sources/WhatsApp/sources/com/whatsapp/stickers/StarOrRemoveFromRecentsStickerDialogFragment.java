package com.whatsapp.stickers;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass1KS;
import X.C004802e;
import X.C12970iu;
import X.C12980iv;
import X.C22210yi;
import X.C235512c;
import X.DialogInterface$OnClickListenerC65533Ka;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.R;
import com.whatsapp.stickers.StarOrRemoveFromRecentsStickerDialogFragment;
import java.util.Collections;

/* loaded from: classes2.dex */
public class StarOrRemoveFromRecentsStickerDialogFragment extends Hilt_StarOrRemoveFromRecentsStickerDialogFragment {
    public C22210yi A00;
    public C22210yi A01;
    public AnonymousClass1KS A02;
    public C235512c A03;

    public static StarOrRemoveFromRecentsStickerDialogFragment A00(AnonymousClass1KS r3, boolean z) {
        StarOrRemoveFromRecentsStickerDialogFragment starOrRemoveFromRecentsStickerDialogFragment = new StarOrRemoveFromRecentsStickerDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("sticker", r3);
        A0D.putBoolean("avocado_sticker", z);
        starOrRemoveFromRecentsStickerDialogFragment.A0U(A0D);
        return starOrRemoveFromRecentsStickerDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        Bundle A03 = A03();
        Parcelable parcelable = A03.getParcelable("sticker");
        AnonymousClass009.A05(parcelable);
        this.A02 = (AnonymousClass1KS) parcelable;
        DialogInterface$OnClickListenerC65533Ka r2 = new DialogInterface.OnClickListener(A03.getBoolean("avocado_sticker", false)) { // from class: X.3Ka
            public final /* synthetic */ boolean A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C22210yi r4;
                StarOrRemoveFromRecentsStickerDialogFragment starOrRemoveFromRecentsStickerDialogFragment = StarOrRemoveFromRecentsStickerDialogFragment.this;
                boolean z = this.A01;
                if (i == -3) {
                    if (z) {
                        r4 = starOrRemoveFromRecentsStickerDialogFragment.A00;
                    } else {
                        r4 = starOrRemoveFromRecentsStickerDialogFragment.A01;
                    }
                    r4.A09.execute(new RunnableBRunnable0Shape7S0200000_I0_7(r4, 49, starOrRemoveFromRecentsStickerDialogFragment.A02));
                } else if (i == -1) {
                    starOrRemoveFromRecentsStickerDialogFragment.A03.A0K(Collections.singleton(starOrRemoveFromRecentsStickerDialogFragment.A02));
                }
            }
        };
        C004802e A0S = C12980iv.A0S(A0C);
        A0S.A06(R.string.sticker_save_to_picker_title);
        A0S.setPositiveButton(R.string.sticker_save_to_picker, r2);
        A0S.A00(R.string.sticker_remove_from_recents_option, r2);
        A0S.setNegativeButton(R.string.cancel, r2);
        return A0S.create();
    }
}
