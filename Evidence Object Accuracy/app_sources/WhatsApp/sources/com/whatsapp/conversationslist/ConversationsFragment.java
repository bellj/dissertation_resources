package com.whatsapp.conversationslist;

import X.AbstractC009504t;
import X.AbstractC115995Ts;
import X.AbstractC13990kf;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC33021d9;
import X.AbstractC33091dK;
import X.AbstractC33101dL;
import X.AbstractC33331dp;
import X.AbstractC51662Vw;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass027;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass04v;
import X.AnonymousClass10A;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Y;
import X.AnonymousClass116;
import X.AnonymousClass11H;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12O;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass132;
import X.AnonymousClass139;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass161;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass185;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1A1;
import X.AnonymousClass1AM;
import X.AnonymousClass1AQ;
import X.AnonymousClass1AR;
import X.AnonymousClass1C5;
import X.AnonymousClass1CT;
import X.AnonymousClass1J1;
import X.AnonymousClass1JV;
import X.AnonymousClass1PE;
import X.AnonymousClass1US;
import X.AnonymousClass1XB;
import X.AnonymousClass23N;
import X.AnonymousClass2Cu;
import X.AnonymousClass2Dn;
import X.AnonymousClass2I2;
import X.AnonymousClass2LB;
import X.AnonymousClass2VX;
import X.AnonymousClass2VY;
import X.AnonymousClass2WJ;
import X.AnonymousClass2WN;
import X.AnonymousClass2eq;
import X.AnonymousClass36v;
import X.AnonymousClass37U;
import X.AnonymousClass37Z;
import X.AnonymousClass3DG;
import X.AnonymousClass4KV;
import X.AnonymousClass53I;
import X.AnonymousClass5TL;
import X.AnonymousClass5U3;
import X.AnonymousClass5U4;
import X.C102234oo;
import X.C1103455e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C14960mK;
import X.C15240mn;
import X.C15250mo;
import X.C15370n3;
import X.C15380n4;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16490p7;
import X.C16590pI;
import X.C16970q3;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C18330sH;
import X.C18640sm;
import X.C18850tA;
import X.C19990v2;
import X.C20040v7;
import X.C20220vP;
import X.C20650w6;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21190x1;
import X.C21240x6;
import X.C21270x9;
import X.C21300xC;
import X.C21320xE;
import X.C21400xM;
import X.C22050yP;
import X.C22100yW;
import X.C22230yk;
import X.C22330yu;
import X.C22630zO;
import X.C22640zP;
import X.C22710zW;
import X.C22730zY;
import X.C236812p;
import X.C237412v;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C245716a;
import X.C246716k;
import X.C250417w;
import X.C251118d;
import X.C252718t;
import X.C253018w;
import X.C253519b;
import X.C254219i;
import X.C255719x;
import X.C27131Gd;
import X.C27151Gf;
import X.C27531Hw;
import X.C28181Ma;
import X.C28391Mz;
import X.C30461Xm;
import X.C32221bo;
import X.C32941cz;
import X.C33701ew;
import X.C34271fr;
import X.C35741ib;
import X.C36451ju;
import X.C36891ko;
import X.C36901kp;
import X.C42401v9;
import X.C42571vR;
import X.C42581vS;
import X.C42591vT;
import X.C42611vV;
import X.C42631vX;
import X.C42641vY;
import X.C43831xf;
import X.C43901xo;
import X.C44771zW;
import X.C48962Ip;
import X.C52162aM;
import X.C52302aa;
import X.C53002cC;
import X.C53042cM;
import X.C63523Bx;
import X.C63543Bz;
import X.C63563Cb;
import X.C65303Iz;
import X.C67323Rb;
import X.C68163Ui;
import X.C70103ak;
import X.C70113al;
import X.C73973h6;
import X.C84393zE;
import X.C851441i;
import X.C854742y;
import X.C854842z;
import X.C858244h;
import X.C88134Ek;
import X.C90814Pi;
import X.C92434Vw;
import X.ExecutorC27271Gr;
import X.ServiceConnectionC42621vW;
import X.ViewTreeObserver$OnGlobalLayoutListenerC33691ev;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Space;
import android.widget.TextView;
import androidx.fragment.app.ListFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0501000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.Conversation;
import com.whatsapp.EmptyTellAFriendView;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.community.SubgroupPileView;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class ConversationsFragment extends Hilt_ConversationsFragment implements AbstractC33091dK, AbstractC14940mI, AbstractC14770m1, AbstractC33101dL, AbstractC13990kf, AbstractC33021d9 {
    public int A00;
    public long A01;
    public long A02;
    public View A03;
    public View A04;
    public ViewGroup A05;
    public ImageView A06;
    public ListView A07;
    public ProgressBar A08;
    public TextView A09;
    public TextView A0A;
    public TextView A0B;
    public TextView A0C;
    public AbstractC009504t A0D;
    public AnonymousClass17S A0E;
    public AnonymousClass12P A0F;
    public C253519b A0G;
    public AbstractC15710nm A0H;
    public C48962Ip A0I;
    public C53042cM A0J;
    public C53042cM A0K;
    public C14900mE A0L;
    public C15570nT A0M;
    public C15450nH A0N;
    public AnonymousClass11L A0O;
    public AnonymousClass1AR A0P;
    public C16170oZ A0Q;
    public C18330sH A0R;
    public C22730zY A0S;
    public AnonymousClass10J A0T;
    public AnonymousClass10K A0U;
    public C63523Bx A0V;
    public C14650lo A0W;
    public AnonymousClass10A A0X;
    public C246716k A0Y;
    public C238013b A0Z;
    public C251118d A0a;
    public C16430p0 A0b;
    public C22330yu A0c;
    public C243915i A0d;
    public C22640zP A0e;
    public C237412v A0f;
    public C18850tA A0g;
    public AnonymousClass116 A0h;
    public AnonymousClass130 A0i;
    public C15550nR A0j;
    public AnonymousClass10S A0k;
    public C15610nY A0l;
    public AnonymousClass10T A0m;
    public AnonymousClass1J1 A0n;
    public C21270x9 A0o;
    public C21240x6 A0p;
    public C17010q7 A0q;
    public C20730wE A0r;
    public C250417w A0s;
    public AnonymousClass11P A0t;
    public C53002cC A0u;
    public C53002cC A0v;
    public ArchiveHeaderViewModel A0w;
    public C63563Cb A0x;
    public C36901kp A0y;
    public C42591vT A0z;
    public C42581vS A10;
    public C42571vR A11;
    public AnonymousClass4KV A12;
    public C36891ko A13;
    public AnonymousClass2I2 A14;
    public C63543Bz A15;
    public C42401v9 A16;
    public C18640sm A17;
    public AnonymousClass01d A18;
    public C15410nB A19;
    public C14830m7 A1A;
    public C16590pI A1B;
    public C15890o4 A1C;
    public C14820m6 A1D;
    public AnonymousClass018 A1E;
    public C20650w6 A1F;
    public C19990v2 A1G;
    public C20830wO A1H;
    public C21320xE A1I;
    public C15680nj A1J;
    public C241714m A1K;
    public C15240mn A1L;
    public C15600nX A1M;
    public AnonymousClass1C5 A1N;
    public C236812p A1O;
    public C20040v7 A1P;
    public AnonymousClass10Y A1Q;
    public C16490p7 A1R;
    public C242114q A1S;
    public AnonymousClass132 A1T;
    public C21400xM A1U;
    public C22100yW A1V;
    public C245716a A1W;
    public AnonymousClass19M A1X;
    public C14850m9 A1Y;
    public C22050yP A1Z;
    public C16120oU A1a;
    public C20710wC A1b;
    public C244215l A1c;
    public AnonymousClass139 A1d;
    public AbstractC14640lm A1e;
    public AbstractC14640lm A1f;
    public AnonymousClass13H A1g;
    public C17220qS A1h;
    public C14840m8 A1i;
    public C22230yk A1j;
    public C20220vP A1k;
    public C22630zO A1l;
    public AnonymousClass11H A1m;
    public C22710zW A1n;
    public C17070qD A1o;
    public AnonymousClass14X A1p;
    public C21190x1 A1q;
    public AnonymousClass185 A1r;
    public AbstractC14780m2 A1s;
    public C15860o1 A1t;
    public AnonymousClass17R A1u;
    public AnonymousClass161 A1v;
    public AnonymousClass1AQ A1w;
    public AnonymousClass1AM A1x;
    public C240514a A1y;
    public AnonymousClass12F A1z;
    public C253018w A20;
    public AnonymousClass12U A21;
    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev A22;
    public StatusesViewModel A23;
    public AnonymousClass1CT A24;
    public AnonymousClass12O A25;
    public AnonymousClass198 A26;
    public C254219i A27;
    public C21300xC A28;
    public C255719x A29;
    public C252718t A2A;
    public AbstractC14440lR A2B;
    public C14860mA A2C;
    public AnonymousClass01H A2D;
    public AnonymousClass01H A2E;
    public ArrayList A2F;
    public LinkedHashSet A2G = new LinkedHashSet();
    public Set A2H = new HashSet();
    public boolean A2I = true;
    public boolean A2J = false;
    public boolean A2K;
    public boolean A2L;
    public final ServiceConnection A2M = new ServiceConnectionC42621vW(this);
    public final AnonymousClass2Cu A2N = new C84393zE(this);
    public final AnonymousClass2Dn A2O = new C851441i(this);
    public final AbstractC115995Ts A2P = new AbstractC115995Ts() { // from class: X.3WD
        @Override // X.AbstractC115995Ts
        public final void ATR(List list, List list2, List list3) {
            ConversationsFragment conversationsFragment = ConversationsFragment.this;
            ActivityC000900k A0B = conversationsFragment.A0B();
            if (A0B != null && !A0B.isFinishing()) {
                C15450nH r2 = conversationsFragment.A0N;
                C14840m8 r1 = conversationsFragment.A1i;
                if (r2.A05(AbstractC15460nI.A0d) || r1.A03() || r1.A04() || conversationsFragment.A17.A0B()) {
                    ActivityC000900k A0B2 = conversationsFragment.A0B();
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(A0B2.getPackageName(), "com.whatsapp.companiondevice.LinkedDevicesActivity");
                    conversationsFragment.A0v(A0A);
                    return;
                }
                conversationsFragment.A0L.A07(C18640sm.A01(conversationsFragment.A0B()), 0);
            }
        }
    };
    public final C27131Gd A2Q = new C36451ju(this);
    public final C42611vV A2R = new C42611vV(this);
    public final C27151Gf A2S = new C32941cz(this);
    public final C15250mo A2T = new C15250mo(this.A1E);
    public final AbstractC33331dp A2U = new C858244h(this);

    @Override // X.AbstractC14940mI
    public String AGV() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AGW() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AHA() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AHB() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public void AVh() {
    }

    @Override // X.AbstractC14770m1
    public boolean Aed() {
        return true;
    }

    public static /* synthetic */ AbstractC14640lm A01(ConversationsFragment conversationsFragment) {
        if (conversationsFragment.A2G.size() == 1) {
            return (AbstractC14640lm) conversationsFragment.A2G.iterator().next();
        }
        Log.i("conversations/getSoloSelectionJid/not a solo selection");
        return null;
    }

    public static void A02(View view) {
        AnonymousClass028.A0g(view, new AnonymousClass2VY(new AnonymousClass2VX[]{new AnonymousClass2VX(16, R.string.contacts_row_action_click), new AnonymousClass2VX(32, R.string.accessibility_contact_long_press)}));
    }

    public static /* synthetic */ void A03(ConversationsFragment conversationsFragment) {
        conversationsFragment.A1F();
        conversationsFragment.A13.A03 = true;
        if (conversationsFragment.A0y != null) {
            conversationsFragment.A1K();
            conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(conversationsFragment.A1H, 26));
        }
    }

    public static /* synthetic */ void A04(ConversationsFragment conversationsFragment, AbstractC14640lm r5) {
        if (conversationsFragment.A0y != null) {
            View A1A = conversationsFragment.A1A(r5);
            if (A1A != null) {
                conversationsFragment.A1H.A01(r5);
                ((ViewHolder) A1A.getTag()).A0H(null, -1, false);
            } else if (conversationsFragment.A1G.A0E(r5)) {
                conversationsFragment.A1I();
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        ArrayList<String> stringArrayList;
        C28181Ma r2 = new C28181Ma("conversations/create");
        super.A0m(bundle);
        A0M();
        if (this.A15 == null) {
            this.A15 = new C63543Bz(A0p());
        }
        this.A2B.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A19, 37));
        if (this.A1n.A03()) {
            C16490p7 r0 = this.A1R;
            r0.A04();
            if (r0.A01) {
                this.A2B.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this.A1m, 27));
            }
        }
        this.A0k.A03(this.A2Q);
        this.A0c.A03(this.A2O);
        this.A1I.A03(this.A2S);
        this.A0X.A03(this.A2N);
        this.A1c.A03(this.A2U);
        this.A2F = A1B();
        ListFragment.A00(this);
        ListView listView = ((ListFragment) this).A04;
        this.A07 = listView;
        listView.setFastScrollEnabled(false);
        this.A07.setScrollbarFadingEnabled(true);
        this.A1r.A02(A0p());
        this.A07.setOnScrollListener(new C102234oo(this));
        if (this.A0S.A09() && C44771zW.A0H(this.A1D)) {
            View inflate = A0C().getLayoutInflater().inflate(R.layout.conversations_google_drive_header, (ViewGroup) this.A07, false);
            this.A03 = inflate;
            this.A07.addHeaderView(inflate);
            AnonymousClass028.A0D(this.A07, R.id.google_drive_progress_view).setVisibility(8);
        }
        C36901kp r3 = new C36901kp(this.A0j, this, new AnonymousClass5U4() { // from class: X.55g
            @Override // X.AnonymousClass5U4
            public final void AVW(int i) {
                C42401v9 r1 = C42401v9.this;
                if (!r1.A04) {
                    C12980iv.A1R(r1.A05, i);
                    if (r1.A02 != null && r1.isValid()) {
                        r1.A02.run();
                    }
                }
            }
        }, this.A1E, this.A1t);
        this.A0y = r3;
        this.A07.setAdapter((ListAdapter) r3);
        this.A07.post(new RunnableBRunnable0Shape5S0100000_I0_5(this, 20));
        this.A13.A03 = true;
        A1K();
        this.A07.setOnCreateContextMenuListener(this);
        if (bundle != null) {
            this.A1f = AbstractC14640lm.A01(bundle.getString("LongPressedRowJid"));
            if (this.A2I && (stringArrayList = bundle.getStringArrayList("SelectedRowJids")) != null) {
                LinkedHashSet linkedHashSet = this.A2G;
                linkedHashSet.clear();
                linkedHashSet.addAll(C15380n4.A07(AbstractC14640lm.class, stringArrayList));
                if (!this.A2G.isEmpty()) {
                    A1G();
                }
            }
        }
        r2.A01();
        this.A2B.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this, 24));
    }

    @Override // com.whatsapp.base.WaListFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        super.A0n(z);
        if (((AnonymousClass01E) this).A03 >= 7 && z) {
            this.A14.A01(this);
        }
    }

    @Override // X.AnonymousClass01E
    public boolean A0o(MenuItem menuItem) {
        return this.A11.A03(menuItem, this, A0C());
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        Log.i("conversationsFragment/onPause");
        super.A0r();
        C36891ko r2 = this.A13;
        AnonymousClass37Z r1 = r2.A01;
        if (r1 != null) {
            r1.A03(true);
        }
        r2.A03 = false;
        ObjectAnimator objectAnimator = this.A13.A00;
        if (objectAnimator != null) {
            objectAnimator.end();
        }
        this.A01 = 0;
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (this.A11.A02(i)) {
            return;
        }
        if (i != 12) {
            if (i == 150 && i2 == -1) {
                A1H();
            }
        } else if (i2 == -1) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(intent.getStringExtra("contact"));
            AnonymousClass009.A05(A01);
            if (this.A1Y.A07(931)) {
                ((AnonymousClass1A1) this.A2D.get()).A03(A01);
            }
            Intent A0g = new C14960mK().A0g(A0p(), this.A0j.A0B(A01));
            A0g.putExtra("show_keyboard", true);
            A0g.putExtra("entry_point_conversion_source", intent.getStringExtra("entry_point_conversion_source"));
            A0g.putExtra("entry_point_conversion_app", intent.getStringExtra("entry_point_conversion_app"));
            A0g.putExtra("contact_out_address_book", intent.getBooleanExtra("contact_out_address_book", false));
            A0g.putExtra("start_t", SystemClock.uptimeMillis());
            C35741ib.A00(A0g, "ConversationsFragment:onActivityResult:codeStartChat");
            this.A1q.A00();
            A0v(A0g);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        AbstractC14640lm r0 = this.A1f;
        if (r0 != null) {
            bundle.putString("LongPressedRowJid", r0.getRawString());
        }
        bundle.putStringArrayList("SelectedRowJids", C15380n4.A06(this.A2G));
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(1, R.id.menuitem_new_group, 0, R.string.menuitem_groupchat).setAlphabeticShortcut('g');
        this.A0M.A08();
        menu.add(1, R.id.menuitem_new_broadcast, 0, R.string.menuitem_list).setAlphabeticShortcut('b');
        this.A0M.A08();
        boolean A04 = C65303Iz.A04(this.A0N);
        int i = R.string.menuitem_whatsapp_web;
        if (A04) {
            i = R.string.menuitem_linked_devices;
        }
        menu.add(1, R.id.menuitem_scan_qr, 0, i).setAlphabeticShortcut('q');
        if (this.A0f.A00()) {
            this.A0M.A08();
            menu.add(1, R.id.menuitem_companion, 0, R.string.menuitem_companion);
        }
        menu.add(1, R.id.menuitem_starred, 0, R.string.menuitem_starred).setAlphabeticShortcut('s');
        C14850m9 r1 = this.A0a.A00;
        if (r1.A07(450) && r1.A07(1294)) {
            ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, this.A1B.A00)).A2F.get();
            menu.add(1, R.id.menuitem_businesses_nearby, 0, R.string.biz_dir_find_businesses_v2);
        }
        if (this.A1Y.A07(1188)) {
            menu.add(1, R.id.menuitem_orders, 0, R.string.orders_activity_title);
        }
        if (this.A1n.A03()) {
            this.A0M.A08();
            menu.add(1, R.id.menuitem_payments, 0, R.string.payments_activity_title);
        }
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        Intent intent;
        String packageName;
        String str;
        if (menuItem.getItemId() == R.id.menuitem_new_conversation) {
            ASK();
            return true;
        }
        if (menuItem.getItemId() == R.id.menuitem_new_broadcast) {
            AnonymousClass4KV r2 = this.A12;
            C854742y r1 = new C854742y();
            r1.A00 = 3;
            r2.A00.A07(r1);
            this.A0q.A00();
            ActivityC000900k A0B = A0B();
            Set emptySet = Collections.emptySet();
            intent = new Intent();
            intent.setClassName(A0B.getPackageName(), "com.whatsapp.contact.picker.ListMembersSelector");
            if (!emptySet.isEmpty()) {
                intent.putExtra("selected", C15380n4.A06(emptySet));
            }
        } else if (menuItem.getItemId() == R.id.menuitem_new_group) {
            AnonymousClass4KV r22 = this.A12;
            C854742y r12 = new C854742y();
            r12.A00 = 2;
            r22.A00.A07(r12);
            this.A0q.A00();
            this.A1Z.A03(2);
            ActivityC000900k A0C = A0C();
            A0C.startActivity(C14960mK.A0b(A0C, null, 2));
            return true;
        } else if (menuItem.getItemId() == R.id.menuitem_scan_qr) {
            C14860mA r5 = this.A2C;
            this.A2B.Aaz(new AnonymousClass37U(this.A2P, this.A1V, this.A1d, r5), new Void[0]);
            return true;
        } else {
            if (menuItem.getItemId() == R.id.menuitem_archived_chats) {
                ActivityC000900k A0B2 = A0B();
                intent = new Intent();
                packageName = A0B2.getPackageName();
                str = "com.whatsapp.conversationslist.ArchivedConversationsActivity";
            } else if (menuItem.getItemId() == R.id.menuitem_starred) {
                AnonymousClass4KV r23 = this.A12;
                C854742y r13 = new C854742y();
                r13.A00 = 6;
                r23.A00.A07(r13);
                intent = C14960mK.A0L(A0B(), null);
            } else if (menuItem.getItemId() == R.id.menuitem_businesses_nearby) {
                this.A0F.A06(A01(), new Intent("android.intent.action.VIEW").setClassName(A01().getPackageName(), "com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity"));
                this.A0b.A00(4, 8);
                return true;
            } else if (menuItem.getItemId() != R.id.menuitem_companion) {
                return false;
            } else {
                Context A0p = A0p();
                intent = new Intent();
                packageName = A0p.getPackageName();
                str = "com.whatsapp.companionmode.registration.ConvertPrimaryToCompanionActivity";
            }
            intent.setClassName(packageName, str);
        }
        A0v(intent);
        return true;
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (this.A1Y.A07(1533)) {
            StatusesViewModel statusesViewModel = (StatusesViewModel) new AnonymousClass02A(new C67323Rb(this.A0I, true), this).A00(StatusesViewModel.class);
            this.A23 = statusesViewModel;
            statusesViewModel.A00 = this;
            statusesViewModel.A05.A05(A0G(), new AnonymousClass02B() { // from class: X.3QK
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    ArrayList A0x;
                    Object tag;
                    ConversationsFragment conversationsFragment = ConversationsFragment.this;
                    Map map = (Map) obj;
                    Set set = conversationsFragment.A23.A0G;
                    synchronized (set) {
                        A0x = C12980iv.A0x(set);
                        set.clear();
                    }
                    Iterator it = A0x.iterator();
                    while (it.hasNext()) {
                        AbstractC14640lm A0b = C12990iw.A0b(it);
                        if (conversationsFragment.A0y != null) {
                            View A1A = conversationsFragment.A1A(A0b);
                            if (A1A == null) {
                                tag = null;
                            } else {
                                tag = A1A.getTag();
                            }
                            if (tag instanceof ViewHolder) {
                                C92434Vw r3 = (C92434Vw) map.get(A0b);
                                AbstractC65103Id r2 = ((ViewHolder) tag).A01;
                                if (r2 instanceof AnonymousClass5RT) {
                                    C61202za r22 = (C61202za) r2;
                                    r22.A02 = r3;
                                    ImageView imageView = ((AbstractC65103Id) r22).A0B.A08;
                                    if (imageView instanceof WDSProfilePhoto) {
                                        r22.A0A(r3, (WDSProfilePhoto) imageView);
                                    }
                                }
                            }
                        }
                    }
                }
            });
            super.A0K.A00(this.A23);
        }
        View inflate = layoutInflater.inflate(R.layout.conversations, viewGroup, false);
        ListView listView = (ListView) AnonymousClass028.A0D(inflate, 16908298);
        View inflate2 = A0C().getLayoutInflater().inflate(R.layout.conversations_tip_row, (ViewGroup) listView, false);
        listView.addFooterView(inflate2, null, false);
        this.A0C = (TextView) inflate2.findViewById(R.id.conversations_row_tip_tv);
        this.A2K = AnonymousClass23N.A05(this.A18.A0P());
        boolean z = this instanceof ArchivedConversationsFragment;
        if (!z) {
            C53002cC r5 = new C53002cC(A0B(), 1);
            this.A0u = r5;
            r5.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 22));
            listView.addFooterView(this.A0u, null, false);
            this.A0u.setVisibility(false);
        }
        View inflate3 = layoutInflater.inflate(R.layout.conversations_tip_row, (ViewGroup) listView, false);
        listView.addFooterView(inflate3, null, true);
        TextView textView = (TextView) inflate3.findViewById(R.id.conversations_row_tip_tv);
        this.A09 = textView;
        textView.setBackgroundResource(R.drawable.selector_orange_gradient);
        this.A09.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 43));
        AnonymousClass028.A0g(inflate3, new AnonymousClass04v());
        if (!C28391Mz.A01()) {
            this.A14.A00(listView, this);
        }
        HomeActivity.A0A(inflate, this);
        if (!this.A1Y.A07(1071)) {
            HomeActivity.A0B(inflate, this, A02().getDimensionPixelSize(R.dimen.conversations_row_height));
        }
        this.A05 = (ViewGroup) inflate.findViewById(R.id.banner_holder);
        if (!z) {
            C53042cM r2 = new C53042cM(A0B(), A0F());
            this.A0J = r2;
            listView.addHeaderView(r2, null, true);
        }
        if (this.A04 == null) {
            this.A04 = new Space(A0p());
            this.A04.setLayoutParams(new AbsListView.LayoutParams(-1, A02().getDimensionPixelSize(R.dimen.conversation_list_padding_top)));
        }
        listView.removeHeaderView(this.A04);
        listView.addHeaderView(this.A04);
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Log.i("conversationsFragment/onDestroy");
        super.A11();
        this.A0k.A04(this.A2Q);
        this.A0c.A04(this.A2O);
        this.A1I.A04(this.A2S);
        this.A0X.A04(this.A2N);
        this.A1c.A04(this.A2U);
        C42591vT r1 = this.A0z;
        if (r1 != null) {
            this.A0T.A01.A04(r1);
        }
        C36891ko r2 = this.A13;
        AnonymousClass37Z r12 = r2.A01;
        if (r12 != null) {
            r12.A03(true);
        }
        r2.A03 = false;
        this.A0n.A00();
        this.A2L = false;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        View findViewById;
        Log.i("conversationsFragment/onResume");
        super.A13();
        this.A1l.A0K.clear();
        this.A28.A00();
        if (this.A0S.A09() && (findViewById = this.A07.findViewById(R.id.google_drive_progress_view)) != null) {
            int visibility = findViewById.getVisibility();
            boolean A0H = C44771zW.A0H(this.A1D);
            if (visibility == 8) {
                if (A0H) {
                    findViewById.setVisibility(0);
                    Log.i("conversations/gdrive-header/gdrive-media-restore-pending/show-view");
                    A0C().bindService(C14960mK.A0U(A0B(), null), this.A2M, 1);
                    View view = this.A03;
                    AnonymousClass009.A03(view);
                    view.setVisibility(0);
                    this.A07.setHeaderDividersEnabled(true);
                    this.A06 = (ImageView) this.A07.findViewById(R.id.google_drive_image_view);
                    ProgressBar progressBar = (ProgressBar) this.A07.findViewById(R.id.google_drive_progress);
                    this.A08 = progressBar;
                    C88134Ek.A00(progressBar, AnonymousClass00T.A00(A01(), R.color.media_message_progress_determinate));
                    TextView textView = (TextView) this.A07.findViewById(R.id.google_drive_backup_info_title);
                    this.A0B = textView;
                    C27531Hw.A06(textView);
                    this.A0A = (TextView) this.A07.findViewById(R.id.google_drive_backup_info_message);
                    this.A06.setImageResource(R.drawable.ic_in_progress);
                    ImageView imageView = this.A06;
                    C73973h6 r2 = new C73973h6();
                    r2.setDuration(2000);
                    r2.setRepeatCount(-1);
                    r2.setInterpolator(new LinearInterpolator());
                    r2.A00 = 0;
                    r2.A01 = true;
                    imageView.setAnimation(r2);
                    this.A03.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 44));
                }
            } else if (!A0H) {
                Log.i("conversations/resume/gdrive-header/gdrive-media-restore-done/hide-view");
                this.A07.findViewById(R.id.google_drive_progress_view).setVisibility(8);
                A0C().unbindService(this.A2M);
            }
        }
        AnonymousClass12O r3 = this.A25;
        Log.i("UserNoticeManager/transitionUserNoticeStageIfNecessary");
        C43831xf A01 = r3.A08.A01();
        if (A01 == null) {
            Log.i("UserNoticeManager/transitionUserNoticeStageIfNecessary/no metadata");
        } else {
            int i = A01.A00;
            if (C43901xo.A00(r3.A03, i)) {
                StringBuilder sb = new StringBuilder("UserNoticeManager/transitionUserNoticeStageIfNecessary/green alert disabled, notice: ");
                sb.append(i);
                Log.i(sb.toString());
                r3.A06();
            } else {
                r3.A09(r3.A07.A03(A01), A01);
            }
        }
        this.A24.A01(A01(), false);
        A1I();
        A1K();
        if (this.A0D != null) {
            A1J();
            this.A0D.A06();
        }
        if (this.A2L) {
            this.A2L = false;
            ActivityC000900k A0B = A0B();
            Intent intent = new Intent();
            intent.setClassName(A0B.getPackageName(), "com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity");
            A0v(intent);
        }
        this.A14.A01(this);
    }

    @Override // com.whatsapp.conversationslist.Hilt_ConversationsFragment, X.AnonymousClass01E
    public void A15(Context context) {
        Log.i("conversations/attach");
        super.A15(context);
        AbstractC009504t r0 = this.A0D;
        if (r0 != null) {
            r0.A06();
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        Log.i("conversationsFragment/onCreate");
        this.A16 = new C42401v9();
        this.A0n = this.A0o.A04(A0p(), "conversations-fragment");
        this.A0x = new C63563Cb(new ExecutorC27271Gr(this.A2B, true));
        C14850m9 r3 = this.A1Y;
        AbstractC14440lR r2 = this.A2B;
        C16120oU r1 = this.A1a;
        this.A13 = new C36891ko(this.A0j, this.A0l, this.A0m, this.A0n, this, this.A1E, r3, r1, r2);
        Context A0p = A0p();
        C14830m7 r0 = this.A1A;
        C14900mE r15 = this.A0L;
        AbstractC14440lR r14 = this.A2B;
        C20650w6 r13 = this.A1F;
        C15450nH r12 = this.A0N;
        C18850tA r11 = this.A0g;
        C15550nR r10 = this.A0j;
        C22230yk r9 = this.A1j;
        C20710wC r8 = this.A1b;
        C15860o1 r6 = this.A1t;
        AnonymousClass132 r5 = this.A1T;
        C242114q r4 = this.A1S;
        C255719x r32 = this.A29;
        this.A10 = new C42581vS(A0p, r15, r12, r11, r10, new AnonymousClass5U3() { // from class: X.55d
            @Override // X.AnonymousClass5U3
            public final void A5X(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
                ConversationsFragment.this.A1S(charSequence, charSequence2, onClickListener);
            }
        }, r0, r13, this.A1M, r4, r5, r8, r9, r6, r32, r14);
        Context A0p2 = A0p();
        AnonymousClass01F r02 = super.A0H;
        C14830m7 r03 = this.A1A;
        C253018w r04 = this.A20;
        C14900mE r05 = this.A0L;
        C15570nT r06 = this.A0M;
        AbstractC14440lR r07 = this.A2B;
        C19990v2 r08 = this.A1G;
        AnonymousClass12U r09 = this.A21;
        C20650w6 r010 = this.A1F;
        C16170oZ r011 = this.A0Q;
        C15550nR r012 = this.A0j;
        AnonymousClass018 r013 = this.A1E;
        C20710wC r014 = this.A1b;
        AnonymousClass12F r152 = this.A1z;
        AnonymousClass198 r142 = this.A26;
        C15860o1 r132 = this.A1t;
        C18330sH r122 = this.A0R;
        C254219i r112 = this.A27;
        C20730wE r102 = this.A0r;
        C20220vP r92 = this.A1k;
        C14820m6 r82 = this.A1D;
        C15680nj r62 = this.A1J;
        C14650lo r52 = this.A0W;
        C250417w r42 = this.A0s;
        C255719x r33 = this.A29;
        this.A11 = new C42571vR(A0p2, r02, r05, r06, r011, r122, r52, r012, r102, r42, this.A10, r03, r82, r013, r010, r08, r62, this.A1M, r014, r92, r132, r152, r04, r09, r142, r112, r33, r07, 10);
        this.A12 = new AnonymousClass4KV(this.A1a);
        this.A16.A02 = new RunnableBRunnable0Shape5S0100000_I0_5(this, 22);
        super.A16(bundle);
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        if (C28391Mz.A01()) {
            AnonymousClass2I2 r1 = this.A14;
            ListFragment.A00(this);
            r1.A00(((ListFragment) this).A04, this);
        }
        if (this.A1Y.A07(1071)) {
            HomeActivity.A0B(view, this, A02().getDimensionPixelSize(R.dimen.conversations_row_height));
        }
    }

    public final View A1A(AbstractC14640lm r6) {
        if (r6 != null) {
            for (int i = 0; i < this.A07.getChildCount(); i++) {
                View childAt = this.A07.getChildAt(i);
                Object tag = childAt.getTag();
                if (tag instanceof ViewHolder) {
                    AbstractC51662Vw r1 = ((ViewHolder) tag).A02;
                    if ((r1 instanceof AnonymousClass2WJ) && r6.equals(((AnonymousClass2WJ) r1).A00)) {
                        return childAt;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList A1B() {
        ArrayList arrayList;
        if (!(this instanceof ArchivedConversationsFragment)) {
            List<AbstractC14640lm> A09 = this.A1J.A09(this.A1t);
            arrayList = new ArrayList(A09.size());
            for (AbstractC14640lm r1 : A09) {
                arrayList.add(new AnonymousClass2WJ(r1));
            }
        } else {
            List<AbstractC14640lm> A05 = this.A1J.A05();
            arrayList = new ArrayList(A05.size());
            for (AbstractC14640lm r12 : A05) {
                arrayList.add(new AnonymousClass2WJ(r12));
            }
        }
        return arrayList;
    }

    public final ArrayList A1C() {
        ArrayList arrayList = this.A2F;
        if (arrayList == null) {
            return A1B();
        }
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (!(next instanceof C1103455e) && !(next instanceof AnonymousClass2WN)) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    public void A1D() {
        C53002cC r1 = this.A0v;
        if (r1 != null) {
            r1.setVisibility(false);
        }
        ArchiveHeaderViewModel archiveHeaderViewModel = this.A0w;
        if (archiveHeaderViewModel != null) {
            archiveHeaderViewModel.A01.A04(this);
            this.A0w = null;
        }
    }

    public void A1E() {
        C36901kp r0 = this.A0y;
        if (r0 != null) {
            AnonymousClass2LB r1 = r0.A00;
            if (r1.A01() || !TextUtils.isEmpty(r1.A01)) {
                C36901kp r02 = this.A0y;
                r02.getFilter().filter(r02.A00.A01);
            } else {
                this.A2F = A1B();
            }
            A1I();
            A1J();
            AbstractC009504t r03 = this.A0D;
            if (r03 != null) {
                r03.A06();
            }
            this.A0y.notifyDataSetChanged();
            A1K();
        }
    }

    public void A1F() {
        ActivityC000900k A0B = A0B();
        if (A0B != null && A0c() && !A0B.isFinishing()) {
            A1I();
            A1J();
            AbstractC009504t r0 = this.A0D;
            if (r0 != null) {
                r0.A06();
            }
            C36901kp r02 = this.A0y;
            if (r02 != null) {
                r02.notifyDataSetChanged();
            }
        }
    }

    public void A1G() {
        C53002cC r0 = this.A0v;
        if (r0 != null) {
            r0.setEnableState(false);
        }
        C53002cC r02 = this.A0u;
        if (r02 != null) {
            r02.setEnableState(false);
        }
        ActivityC000900k A0B = A0B();
        AnonymousClass009.A05(A0B);
        this.A0D = ((ActivityC000800j) A0B).A1W(this.A2R);
    }

    public void A1H() {
        startActivityForResult(new C42641vY(A0B()).A00(), 12);
    }

    public void A1I() {
        TextView textView;
        String format;
        long j;
        String format2;
        if (!(this instanceof ArchivedConversationsFragment)) {
            if (this.A0J != null) {
                C63523Bx r4 = this.A0V;
                AnonymousClass53I r3 = new AnonymousClass5TL() { // from class: X.53I
                    @Override // X.AnonymousClass5TL
                    public final void AOq(C90814Pi r32) {
                        ConversationsFragment conversationsFragment = ConversationsFragment.this;
                        ActivityC000900k A0B = conversationsFragment.A0B();
                        if (A0B != null && !A0B.isFinishing()) {
                            conversationsFragment.A0J.A01(r32);
                        }
                    }
                };
                C90814Pi r0 = r4.A00;
                if (r0 != null) {
                    r3.AOq(r0);
                } else {
                    r4.A0E.Aaz(new AnonymousClass36v(r3, r4), new Void[0]);
                }
            }
            this.A00 = this.A1J.A00();
            if (!this.A1D.A00.getBoolean("archive_v2_enabled", false) || !(A0B() instanceof HomeActivity)) {
                A1D();
                C53002cC r02 = this.A0u;
                if (r02 != null) {
                    r02.setVisibility(false);
                }
                int i = this.A00;
                TextView textView2 = this.A09;
                if (i > 0) {
                    textView2.setVisibility(0);
                    this.A09.setText(A0J(R.string.archived_chats_count, Integer.valueOf(this.A00)));
                    textView = this.A0C;
                } else {
                    textView2.setVisibility(8);
                    A1L();
                    return;
                }
            } else {
                this.A09.setVisibility(8);
                A1L();
                if (this.A00 <= 0) {
                    A1D();
                    C53002cC r03 = this.A0u;
                    if (r03 != null) {
                        r03.setVisibility(false);
                        return;
                    }
                    return;
                } else if (this.A1D.A00.getBoolean("notify_new_message_for_archived_chats", false)) {
                    this.A0C.setVisibility(8);
                    this.A09.setVisibility(8);
                    A1D();
                    C53002cC r32 = this.A0u;
                    if (this.A00 == 0) {
                        format2 = null;
                    } else {
                        format2 = this.A1E.A0J().format((long) this.A00);
                    }
                    r32.setContentIndicatorText(format2);
                    this.A0u.setVisibility(true);
                    return;
                } else {
                    if (this.A0v == null) {
                        C53002cC r04 = new C53002cC(A0B(), 2);
                        this.A0v = r04;
                        AnonymousClass23N.A04(r04, false);
                        this.A0v.setImportantMessageTag(R.id.archive_v2_imp_chat_indicator);
                        ListFragment.A00(this);
                        ((ListFragment) this).A04.addHeaderView(this.A0v, null, false);
                    }
                    ArchiveHeaderViewModel archiveHeaderViewModel = this.A0w;
                    if (archiveHeaderViewModel == null) {
                        archiveHeaderViewModel = (ArchiveHeaderViewModel) new AnonymousClass02A(this).A00(ArchiveHeaderViewModel.class);
                        this.A0w = archiveHeaderViewModel;
                    }
                    archiveHeaderViewModel.A01.A05(A0B(), new AnonymousClass02B() { // from class: X.4t6
                        @Override // X.AnonymousClass02B
                        public final void ANq(Object obj) {
                            ConversationsFragment.this.A0v.setContentIndicatorText((String) obj);
                        }
                    });
                    ArchiveHeaderViewModel archiveHeaderViewModel2 = this.A0w;
                    archiveHeaderViewModel2.A00 = 0;
                    if (archiveHeaderViewModel2.A06.A07(363)) {
                        long j2 = archiveHeaderViewModel2.A02.A00.getLong("last_message_row_id_since_archive_open", 0);
                        for (AbstractC14640lm r1 : archiveHeaderViewModel2.A05.A05()) {
                            if (GroupJid.of(r1) != null) {
                                C19990v2 r05 = archiveHeaderViewModel2.A04;
                                long A04 = r05.A04(r1);
                                AnonymousClass1PE r06 = (AnonymousClass1PE) r05.A0B().get(r1);
                                if (r06 == null) {
                                    j = 1;
                                } else {
                                    j = r06.A0K;
                                }
                                if (j > Math.max(j2, A04)) {
                                    Log.i("archive/hasUnseenImportantMsgChat");
                                    format = "@";
                                    break;
                                }
                            }
                        }
                    }
                    for (AbstractC14640lm r12 : archiveHeaderViewModel2.A05.A05()) {
                        if (archiveHeaderViewModel2.A04.A00(r12) != 0) {
                            archiveHeaderViewModel2.A00++;
                        }
                    }
                    if (archiveHeaderViewModel2.A00 == 0) {
                        format = null;
                    } else {
                        format = archiveHeaderViewModel2.A03.A0J().format((long) archiveHeaderViewModel2.A00);
                    }
                    AnonymousClass016 r13 = archiveHeaderViewModel2.A01;
                    if (!AnonymousClass1US.A0D(format, (CharSequence) r13.A01())) {
                        r13.A0A(format);
                    }
                    this.A0v.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 20));
                    this.A0v.setVisibility(true);
                    C53002cC r07 = this.A0u;
                    if (r07 != null) {
                        r07.setVisibility(false);
                    }
                    textView = this.A09;
                }
            }
            textView.setVisibility(8);
            return;
        }
        ArchivedConversationsFragment archivedConversationsFragment = (ArchivedConversationsFragment) this;
        ((ConversationsFragment) archivedConversationsFragment).A09.setVisibility(8);
        ((ConversationsFragment) archivedConversationsFragment).A0C.setVisibility(8);
        archivedConversationsFragment.A1D();
        C53002cC r14 = archivedConversationsFragment.A0u;
        if (r14 != null) {
            r14.setVisibility(false);
        }
        View view = archivedConversationsFragment.A01;
        if (view != null) {
            view.setVisibility(8);
        }
        View view2 = archivedConversationsFragment.A02;
        if (view2 != null) {
            view2.setVisibility(8);
        }
        if (!C16970q3.A02(archivedConversationsFragment.A1D)) {
            View view3 = archivedConversationsFragment.A01;
            if (view3 != null) {
                view3.setVisibility(8);
            }
        } else if (archivedConversationsFragment.A1D.A00.getInt("new_archive_nux_shown_count", 0) <= 3) {
            if (archivedConversationsFragment.A01 == null) {
                View A1U = archivedConversationsFragment.A1U(R.layout.conversations_archive_nux);
                archivedConversationsFragment.A01 = A1U;
                C27531Hw.A06((TextView) A1U.findViewById(R.id.archive_nux_title));
                TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(archivedConversationsFragment.A01, R.id.archive_nux_sub_title);
                String A0I = archivedConversationsFragment.A0I(R.string.archive_nux_sub_title);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(A0I));
                URLSpan[] uRLSpanArr = (URLSpan[]) spannableStringBuilder.getSpans(0, A0I.length(), URLSpan.class);
                if (uRLSpanArr != null) {
                    for (URLSpan uRLSpan : uRLSpanArr) {
                        if ("whatsapp://archive_settings".equals(uRLSpan.getURL())) {
                            spannableStringBuilder.setSpan(new C52302aa(archivedConversationsFragment.A01(), uRLSpan.getURL()), spannableStringBuilder.getSpanStart(uRLSpan), spannableStringBuilder.getSpanEnd(uRLSpan), spannableStringBuilder.getSpanFlags(uRLSpan));
                            spannableStringBuilder.removeSpan(uRLSpan);
                        }
                    }
                }
                textEmojiLabel.setMovementMethod(new C52162aM());
                textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, archivedConversationsFragment.A18));
                textEmojiLabel.setText(spannableStringBuilder);
                archivedConversationsFragment.A01.findViewById(R.id.archive_nux_cancel).setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(archivedConversationsFragment, 42));
            }
            archivedConversationsFragment.A01.setVisibility(0);
        }
        if (archivedConversationsFragment.A1D.A00.getBoolean("archive_v2_enabled", false) && archivedConversationsFragment.A1D.A00.getInt("new_archive_nux_shown_count", 0) > 3) {
            archivedConversationsFragment.A1V();
        }
    }

    public final void A1J() {
        ArrayList arrayList = this.A2F;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (!this.A2G.isEmpty() || !this.A2H.isEmpty()) {
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                HashSet hashSet = new HashSet();
                Iterator it = this.A2F.iterator();
                while (it.hasNext()) {
                    AbstractC14640lm ADg = ((AbstractC51662Vw) it.next()).ADg();
                    if (ADg != null) {
                        if (this.A2G.contains(ADg)) {
                            linkedHashSet.add(ADg);
                        }
                        if (this.A2H.contains(ADg)) {
                            hashSet.add(ADg);
                        }
                    }
                }
                this.A2G = linkedHashSet;
                this.A2H = hashSet;
            }
        }
    }

    public final void A1K() {
        if (this.A0y.getCount() != 0) {
            ViewGroup viewGroup = this.A05;
            if (viewGroup != null) {
                viewGroup.setVisibility(8);
            }
        } else if (!(this instanceof ArchivedConversationsFragment)) {
            C36901kp r0 = this.A0y;
            AnonymousClass009.A05(r0);
            int count = r0.getCount();
            boolean z = false;
            if (count == 0) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            if (this.A1D.A00.getBoolean("archive_v2_enabled", false)) {
                this.A0C.setVisibility(8);
            }
            View view = super.A0A;
            if (view != null) {
                ViewGroup viewGroup2 = (ViewGroup) AnonymousClass028.A0D(view, R.id.conversations_empty_no_contacts);
                ViewGroup viewGroup3 = (ViewGroup) view.findViewById(R.id.conversations_empty_permission_denied);
                ViewGroup viewGroup4 = (ViewGroup) view.findViewById(R.id.conversations_empty_nux);
                View findViewById = view.findViewById(R.id.search_no_matches);
                this.A05.setVisibility(8);
                C36901kp r02 = this.A0y;
                if (r02 != null) {
                    AnonymousClass2LB r1 = r02.A00;
                    if (r1.A01() || !TextUtils.isEmpty(r1.A01)) {
                        findViewById.setVisibility(0);
                        this.A07.setEmptyView(findViewById);
                        viewGroup2.setVisibility(8);
                        viewGroup3.setVisibility(8);
                        viewGroup4.setVisibility(8);
                        return;
                    }
                }
                findViewById.setVisibility(8);
                if (this.A0j.A04() > 0) {
                    this.A0M.A08();
                    viewGroup2.setVisibility(8);
                    viewGroup3.setVisibility(8);
                    viewGroup4.setVisibility(8);
                    this.A07.setEmptyView(viewGroup4);
                    if (this.A1J.A00() == 0) {
                        if (viewGroup4.getChildCount() == 0) {
                            A0C().getLayoutInflater().inflate(R.layout.empty_nux, viewGroup4, true);
                        }
                        viewGroup4.setVisibility(0);
                        A1M();
                        this.A13.A01();
                        C36891ko r3 = this.A13;
                        ObjectAnimator objectAnimator = r3.A00;
                        if (objectAnimator == null) {
                            View findViewById2 = r3.A09.A05().findViewById(R.id.conversations_empty_nux).findViewById(R.id.instruction_arrow);
                            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(findViewById2, "translationX", TypedValue.applyDimension(1, -8.0f, findViewById2.getResources().getDisplayMetrics()));
                            r3.A00 = ofFloat;
                            ofFloat.setDuration(1100L);
                            r3.A00.setRepeatCount(-1);
                            r3.A00.setRepeatMode(2);
                            r3.A00.setInterpolator(new AccelerateDecelerateInterpolator());
                        } else if (objectAnimator.isRunning()) {
                            return;
                        }
                        r3.A00.start();
                    }
                } else if (this.A0h.A00()) {
                    if (viewGroup2.getChildCount() == 0) {
                        EmptyTellAFriendView emptyTellAFriendView = new EmptyTellAFriendView(A0p());
                        viewGroup2.addView(emptyTellAFriendView);
                        emptyTellAFriendView.setInviteButtonClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 45));
                    }
                    viewGroup2.setVisibility(0);
                    A1M();
                    viewGroup3.setVisibility(8);
                    viewGroup4.setVisibility(8);
                    this.A07.setEmptyView(viewGroup2);
                } else {
                    if (viewGroup3.getChildCount() == 0) {
                        A0C().getLayoutInflater().inflate(R.layout.empty_contacts_permissions_needed, viewGroup3, true);
                        viewGroup3.findViewById(R.id.button_open_permission_settings).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 21));
                    }
                    viewGroup3.setVisibility(0);
                    viewGroup2.setVisibility(8);
                    viewGroup4.setVisibility(8);
                    this.A07.setEmptyView(viewGroup3);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r4.A2K != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1L() {
        /*
            r4 = this;
            android.widget.TextView r3 = r4.A0C
            X.0m6 r0 = r4.A1D
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "delete_chat_count"
            r0 = 0
            int r1 = r2.getInt(r1, r0)
            r0 = 3
            if (r1 >= r0) goto L_0x0015
            boolean r1 = r4.A2K
            r0 = 0
            if (r1 == 0) goto L_0x0017
        L_0x0015:
            r0 = 8
        L_0x0017:
            r3.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversationslist.ConversationsFragment.A1L():void");
    }

    public final void A1M() {
        if (this.A0K == null) {
            C53042cM r1 = new C53042cM(A0B(), A0F());
            this.A0K = r1;
            this.A05.addView(r1);
        }
        C63523Bx r4 = this.A0V;
        C68163Ui r3 = new AnonymousClass5TL() { // from class: X.3Ui
            @Override // X.AnonymousClass5TL
            public final void AOq(C90814Pi r32) {
                ViewGroup viewGroup;
                int i;
                ConversationsFragment conversationsFragment = ConversationsFragment.this;
                ActivityC000900k A0B = conversationsFragment.A0B();
                if (A0B != null && !A0B.isFinishing()) {
                    if (conversationsFragment.A0K.A02(r32)) {
                        conversationsFragment.A0K.A01(r32);
                        viewGroup = conversationsFragment.A05;
                        i = 0;
                    } else {
                        viewGroup = conversationsFragment.A05;
                        i = 8;
                    }
                    viewGroup.setVisibility(i);
                }
            }
        };
        C90814Pi r0 = r4.A00;
        if (r0 != null) {
            r3.AOq(r0);
        } else {
            r4.A0E.Aaz(new AnonymousClass36v(r3, r4), new Void[0]);
        }
    }

    public final void A1N(int i) {
        Object tag;
        if (!this.A2G.isEmpty()) {
            if (i > 2) {
                Log.w("conversations/undefined animation behaviour. defaulting to IMMEDIATELY_ANIMATE");
                i = 2;
            }
            this.A2H.clear();
            for (int i2 = 0; i2 < this.A07.getChildCount(); i2++) {
                View childAt = this.A07.getChildAt(i2);
                if (!(childAt == null || (tag = childAt.getTag()) == null || !(tag instanceof ViewHolder))) {
                    ViewHolder viewHolder = (ViewHolder) tag;
                    if (this.A2G.contains(viewHolder.A02.ADg())) {
                        if (i == 0) {
                            View view = viewHolder.A05;
                            view.setBackgroundResource(0);
                            C42631vX.A00(view);
                            viewHolder.A0K(false, false);
                        } else if (i == 1) {
                            this.A2H.add(viewHolder.A02.ADg());
                        } else if (i == 2) {
                            View view2 = viewHolder.A05;
                            view2.setBackgroundResource(0);
                            C42631vX.A00(view2);
                            viewHolder.A0K(false, true);
                        }
                    }
                }
            }
            this.A2G.clear();
        }
    }

    public final void A1O(int i) {
        A1N(i);
        AbstractC009504t r0 = this.A0D;
        if (r0 != null) {
            r0.A05();
        }
        if (i == 1) {
            A1F();
        }
        C53002cC r02 = this.A0v;
        if (r02 != null) {
            r02.setEnableState(true);
        }
        C53002cC r03 = this.A0u;
        if (r03 != null) {
            r03.setEnableState(true);
        }
    }

    public final void A1P(View view, ViewHolder viewHolder, AbstractC14640lm r13) {
        if (this.A2I) {
            boolean contains = this.A2G.contains(r13);
            LinkedHashSet linkedHashSet = this.A2G;
            if (contains) {
                linkedHashSet.remove(r13);
                if (this.A2G.isEmpty() && this.A0D != null) {
                    A1O(2);
                }
                view.setBackgroundResource(0);
                view.postDelayed(new RunnableBRunnable0Shape5S0100000_I0_5(view, 19), 1);
                viewHolder.A0K(false, true);
                AnonymousClass23N.A00(A0B(), this.A18, A0I(R.string.accessibility_announcement_chat_deselected));
                A02(view);
            } else {
                linkedHashSet.add(r13);
                if (this.A0D == null && (A0B() instanceof ActivityC000800j)) {
                    A1G();
                }
                view.setBackgroundResource(R.color.home_row_selection);
                viewHolder.A0K(true, true);
                AnonymousClass028.A0g(view, new AnonymousClass2VY(new AnonymousClass2VX[]{new AnonymousClass2VX(32, R.string.accessibility_action_long_click_selected_chat_to_deselect)}));
            }
            AbstractC009504t r0 = this.A0D;
            if (r0 != null) {
                r0.A06();
            }
            if (!this.A2G.isEmpty()) {
                AnonymousClass23N.A00(A0B(), this.A18, this.A1E.A0I(new Object[]{Integer.valueOf(this.A2G.size())}, R.plurals.n_items_selected, (long) this.A2G.size()));
            }
        }
    }

    public void A1Q(C15370n3 r6) {
        AbstractC14640lm r4 = (AbstractC14640lm) r6.A0B(AbstractC14640lm.class);
        this.A1e = r4;
        if (r4 != null) {
            int i = 0;
            while (true) {
                C36901kp r1 = this.A0y;
                if (i < r1.getCount()) {
                    if (r4.equals(((AbstractC51662Vw) r1.A07.A2F.get(i)).ADg())) {
                        break;
                    }
                    i++;
                } else {
                    i = 0;
                    break;
                }
            }
            if (this.A07.getFirstVisiblePosition() >= i || this.A07.getLastVisiblePosition() <= i) {
                this.A07.setTranscriptMode(0);
                this.A07.setSelectionFromTop(i, this.A07.getHeight() / 3);
            }
        }
    }

    public final void A1R(C15370n3 r4, AbstractC14640lm r5, boolean z) {
        try {
            Intent A00 = this.A27.A00(r4, r5, z);
            A1O(2);
            startActivityForResult(A00, 10);
            this.A26.A02(z, 7);
        } catch (ActivityNotFoundException unused) {
            this.A0L.A07(R.string.activity_not_found, 0);
        }
    }

    public void A1S(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
        ViewTreeObserver$OnGlobalLayoutListenerC33691ev r2;
        int i;
        if (!(this instanceof ArchivedConversationsFragment)) {
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                C34271fr A00 = C34271fr.A00(A0B.findViewById(R.id.pager_holder), charSequence, 0);
                A00.A07(charSequence2, onClickListener);
                A00.A06(AnonymousClass00T.A00(A0B, R.color.snackbarButton));
                ArrayList arrayList = new ArrayList();
                arrayList.add(A0B.findViewById(R.id.fab));
                arrayList.add(A0B.findViewById(R.id.fab_second));
                r2 = new ViewTreeObserver$OnGlobalLayoutListenerC33691ev(this, A00, this.A18, arrayList, false);
                this.A22 = r2;
                i = 21;
            } else {
                return;
            }
        } else {
            ActivityC000900k A0C = A0C();
            View findViewById = A0C.findViewById(R.id.container);
            if (findViewById != null) {
                C34271fr A002 = C34271fr.A00(findViewById, charSequence, 0);
                A002.A07(charSequence2, onClickListener);
                A002.A06(AnonymousClass00T.A00(A0C, R.color.snackbarButton));
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(A0C.findViewById(R.id.fab));
                arrayList2.add(A0C.findViewById(R.id.fab_second));
                r2 = new ViewTreeObserver$OnGlobalLayoutListenerC33691ev(this, A002, this.A18, arrayList2, false);
                this.A22 = r2;
                i = 18;
            } else {
                return;
            }
        }
        r2.A03(new RunnableBRunnable0Shape5S0100000_I0_5(this, i));
        this.A22.A01();
    }

    @Override // X.AbstractC14770m1
    public void A5j(AbstractC14780m2 r2) {
        if (this.A2J) {
            r2.AM3();
        } else {
            this.A1s = r2;
        }
    }

    @Override // X.AbstractC14770m1
    public void A68(AnonymousClass2LB r6) {
        if (TextUtils.isEmpty(this.A0y.A00.A01)) {
            if (this.A02 == 0) {
                this.A02 = SystemClock.uptimeMillis();
            }
        } else if (TextUtils.isEmpty(r6.A01)) {
            this.A02 = 0;
        }
        this.A0y.A00 = r6.clone();
        this.A0y.getFilter().filter(r6.A01);
    }

    @Override // X.AbstractC33091dK
    public void A7D() {
        this.A1e = null;
    }

    @Override // X.AbstractC33101dL
    public void A8v() {
        this.A2I = false;
    }

    @Override // X.AbstractC33101dL
    public void A9K() {
        this.A2I = true;
    }

    @Override // X.AbstractC33091dK
    public AbstractC14640lm ADL() {
        return this.A1e;
    }

    @Override // X.AbstractC14940mI
    public String AE3() {
        return A0I(R.string.menuitem_new);
    }

    @Override // X.AbstractC14940mI
    public Drawable AE4() {
        return AnonymousClass00T.A04(A01(), R.drawable.ic_action_compose);
    }

    @Override // X.AbstractC14940mI
    public String AE5() {
        if (!this.A1Y.A07(1266) || this.A1J.A01() != 0) {
            return null;
        }
        return A0I(R.string.start_chat_button_text);
    }

    @Override // X.AbstractC33091dK
    public List AFi() {
        return this.A0y.A01;
    }

    @Override // X.AbstractC33091dK
    public Set AGa() {
        return this.A2G;
    }

    @Override // X.AbstractC13990kf
    public void AM0(long j) {
        C15370n3 A06 = this.A0j.A06(j);
        AnonymousClass009.A05(A06);
        Jid A0B = A06.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        A1R(A06, (AbstractC14640lm) A0B, false);
    }

    @Override // X.AbstractC33091dK
    public void AO5(ViewHolder viewHolder, AbstractC14640lm r7, int i) {
        if (this.A0D != null) {
            A1P(viewHolder.A05, viewHolder, r7);
            return;
        }
        StringBuilder sb = new StringBuilder("conversations/click/jid ");
        sb.append(r7);
        Log.i(sb.toString());
        if (viewHolder.A06.getVisibility() != 0) {
            boolean z = false;
            if (SystemClock.elapsedRealtime() - this.A01 < 1000) {
                z = true;
            }
            this.A01 = SystemClock.elapsedRealtime();
            if (!z) {
                if (this.A1Y.A07(931)) {
                    ((AnonymousClass1A1) this.A2D.get()).A03(r7);
                }
                Intent putExtra = new C14960mK().A0i(A01(), r7).putExtra("start_t", SystemClock.uptimeMillis());
                Conversation.A09(putExtra, this.A0y.A00);
                C35741ib.A00(putExtra, "ConversationsFragment:onClickConversation");
                this.A1q.A00();
                A0v(putExtra);
            }
        }
    }

    @Override // X.AbstractC33091dK
    public void AO6(View view, ViewHolder viewHolder, AbstractC14640lm r8, int i, int i2) {
        C92434Vw A04;
        if (this.A0D != null) {
            A1P(view, viewHolder, r8);
            return;
        }
        boolean z = false;
        if (SystemClock.elapsedRealtime() - this.A01 < 1000) {
            z = true;
        }
        this.A01 = SystemClock.elapsedRealtime();
        if (!z) {
            StatusesViewModel statusesViewModel = this.A23;
            if (statusesViewModel == null || (A04 = statusesViewModel.A04(UserJid.of(r8))) == null || !A04.A00() || A04.A01 <= 0) {
                C15580nU A02 = C15580nU.A02(r8);
                if (A02 == null || !SubgroupPileView.A00(this.A1G, this.A1M, A02)) {
                    boolean A07 = this.A1Y.A07(1533);
                    int i3 = R.id.contact_photo;
                    if (A07) {
                        i3 = R.id.wds_profile_picture;
                    }
                    View findViewById = view.findViewById(i3);
                    boolean A0b = this.A1b.A0b(A02);
                    int i4 = 2;
                    if (A0b) {
                        i4 = 3;
                    }
                    AnonymousClass3DG r1 = new AnonymousClass3DG(this.A1Y, r8, null);
                    r1.A02 = AnonymousClass028.A0J(findViewById);
                    r1.A01 = i4;
                    r1.A00(A0B(), findViewById);
                    return;
                }
                this.A2B.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 17, A02));
                return;
            }
            this.A23.A08(r8, 5, Integer.valueOf(i));
            A0v(C14960mK.A0F(A01(), r8, Boolean.TRUE));
        }
    }

    @Override // X.AbstractC33091dK
    public void AO7(ViewHolder viewHolder, AbstractC15340mz r7) {
        boolean z = false;
        if (SystemClock.elapsedRealtime() - this.A01 < 1000) {
            z = true;
        }
        this.A01 = SystemClock.elapsedRealtime();
        if (!z) {
            if (this.A1Y.A07(931)) {
                ((AnonymousClass1A1) this.A2D.get()).A03(((AnonymousClass2WN) viewHolder.A02).A00.A0z.A00);
            }
            Intent A0m = new C14960mK().A0m(A01(), ((AnonymousClass2WN) viewHolder.A02).A00, this.A0y.A00.A01);
            Conversation.A09(A0m, this.A0y.A00);
            View currentFocus = A0B().getCurrentFocus();
            if (currentFocus != null) {
                this.A2A.A01(currentFocus);
            }
            C35741ib.A00(A0m, "ConversationsFragment:onClickMessage");
            this.A1q.A00();
            A0v(A0m);
        }
    }

    @Override // X.AbstractC33091dK
    public void AO8(AnonymousClass1JV r18) {
        int i;
        C20710wC r6 = this.A1b;
        if (r6.A0H.A0B()) {
            AbstractC15340mz A01 = r6.A0Z.A01(r18);
            if (A01 instanceof AnonymousClass1XB) {
                Set set = r6.A0y;
                if (!set.contains(r18) && ((AnonymousClass1XB) A01).A00 == 3) {
                    set.add(r18);
                    if (A01 instanceof C32221bo) {
                        i = ((C32221bo) A01).A00;
                    } else {
                        i = 0;
                    }
                    r6.A0S.A0S(r6.A0s.A03(r18, A01.A0I(), ((C30461Xm) A01).A01, 2, i, r6.A0I.A00()));
                    C15370n3 A0B = r6.A0B.A0B(r18);
                    AnonymousClass10T r0 = r6.A0E;
                    File A00 = r0.A00(A0B);
                    AnonymousClass009.A05(A00);
                    File A012 = r0.A01(A0B);
                    AnonymousClass009.A05(A012);
                    r6.A0v.Ab2(new RunnableBRunnable0Shape0S0501000_I0(r18, A012, A01, r6, A00, i, 0));
                    return;
                }
                return;
            }
            return;
        }
        boolean A03 = C18640sm.A03((Context) A0B());
        int i2 = R.string.network_required;
        if (A03) {
            i2 = R.string.network_required_airplane_on;
        }
        this.A0L.A07(i2, 0);
    }

    @Override // X.AbstractC13990kf
    public void AOj(long j) {
        C15370n3 A06 = this.A0j.A06(j);
        AnonymousClass009.A05(A06);
        Jid A0B = A06.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        A1R(A06, (AbstractC14640lm) A0B, true);
    }

    @Override // X.AbstractC33091dK
    public void ASJ(View view, ViewHolder viewHolder, AbstractC14640lm r6, int i) {
        if (this.A2I) {
            A1P(view, viewHolder, r6);
            return;
        }
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            this.A0d.A01 = 4;
            A0B.openContextMenu(view);
        }
    }

    @Override // X.AbstractC14940mI
    public void ASK() {
        View findViewById;
        if (this.A0h.A00()) {
            C42641vY r1 = new C42641vY(A0B());
            r1.A0B = true;
            startActivityForResult(r1.A00(), 12);
        } else {
            RequestPermissionActivity.A0O(this, R.string.permission_contacts_access_request, R.string.permission_contacts_needed);
        }
        View view = super.A0A;
        if (view != null && (findViewById = view.findViewById(R.id.conversations_empty_nux)) != null && findViewById.getVisibility() == 0) {
            C854842z r12 = new C854842z();
            r12.A00 = 3;
            this.A1a.A07(r12);
        }
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r2) {
        this.A23.A09(r2);
    }

    @Override // X.AbstractC33091dK
    public boolean AaG(Jid jid) {
        return this.A2H.remove(jid);
    }

    @Override // X.AbstractC14770m1
    public void Acn(boolean z) {
        if (z) {
            this.A2B.Ab6(new RunnableBRunnable0Shape5S0100000_I0_5(this, 23));
        }
    }

    @Override // X.AbstractC14770m1
    public void Aco(boolean z) {
        ViewTreeObserver$OnGlobalLayoutListenerC33691ev r0;
        if (!z && (r0 = this.A22) != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AbstractC009504t r0 = this.A0D;
        if (r0 != null) {
            r0.A06();
        }
    }

    @Override // X.AnonymousClass01E, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        boolean z;
        boolean z2;
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        ViewHolder viewHolder = (ViewHolder) ((AdapterView.AdapterContextMenuInfo) contextMenuInfo).targetView.getTag();
        if (viewHolder == null) {
            Log.i("conversations/context/null");
            return;
        }
        AbstractC51662Vw r1 = viewHolder.A02;
        if (r1 instanceof AnonymousClass2WJ) {
            AbstractC14640lm r3 = ((AnonymousClass2WJ) r1).A00;
            AnonymousClass009.A05(r3);
            this.A1f = r3;
            C42571vR r2 = this.A11;
            AnonymousClass009.A05(r3);
            boolean z3 = this instanceof ArchivedConversationsFragment;
            if (!z3) {
                z = true;
            } else {
                z = false;
            }
            if (!z3) {
                z2 = true;
            } else {
                z2 = !C16970q3.A01(this.A1D);
            }
            r2.A00(contextMenu, r3, z, z2);
        }
    }

    /* loaded from: classes2.dex */
    public class BulkDeleteConversationDialogFragment extends Hilt_ConversationsFragment_BulkDeleteConversationDialogFragment {
        public C16170oZ A00;
        public C15550nR A01;
        public C14820m6 A02;
        public AnonymousClass018 A03;
        public C21320xE A04;
        public AnonymousClass19M A05;
        public C255719x A06;
        public AbstractC14440lR A07;

        /* JADX WARNING: Removed duplicated region for block: B:11:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0070  */
        @Override // androidx.fragment.app.DialogFragment
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.app.Dialog A1A(android.os.Bundle r15) {
            /*
                r14 = this;
                java.lang.Class<X.0lm> r2 = X.AbstractC14640lm.class
                android.os.Bundle r1 = r14.A03()
                java.lang.String r0 = "selection_jids"
                java.util.ArrayList r0 = r1.getStringArrayList(r0)
                java.util.List r2 = X.C15380n4.A07(r2, r0)
                android.os.Bundle r1 = r14.A05
                java.lang.String r0 = "unsent_count"
                r7 = 0
                int r5 = r1.getInt(r0, r7)
                int r6 = r2.size()
                X.3aj r9 = new X.3aj
                r9.<init>(r14, r2)
                android.os.Bundle r1 = r14.A05
                java.lang.String r0 = "chatContainsStarredMessages"
                boolean r3 = r1.getBoolean(r0, r7)
                android.content.res.Resources r2 = r14.A02()
                r1 = 2131755018(0x7f10000a, float:1.9140903E38)
                r4 = 1
                java.lang.Object[] r0 = new java.lang.Object[r4]
                X.C12960it.A1P(r0, r6, r7)
                java.lang.String r10 = r2.getQuantityString(r1, r6, r0)
                java.lang.String r0 = "\n"
                if (r3 == 0) goto L_0x0083
                java.lang.StringBuilder r3 = X.C12960it.A0j(r10)
                if (r5 <= 0) goto L_0x007c
                r3.append(r0)
                r0 = 2131892469(0x7f1218f5, float:1.9419687E38)
            L_0x004c:
                java.lang.String r0 = r14.A0I(r0)
            L_0x0050:
                java.lang.String r10 = X.C12960it.A0d(r0, r3)
            L_0x0054:
                X.19x r3 = r14.A06
                X.00k r4 = r14.A0C()
                boolean r0 = r3.A08()
                if (r0 == 0) goto L_0x0070
                X.59p r5 = new X.59p
                r5.<init>(r9)
                r8 = 2
                r9 = 0
                X.02e r0 = r3.A04(r4, r5, r6, r7, r8, r9)
            L_0x006b:
                X.04S r0 = r0.create()
                return r0
            L_0x0070:
                r13 = 1
                r11 = 2131887632(0x7f120610, float:1.9409877E38)
                r7 = r3
                r8 = r4
                r12 = r6
                X.02e r0 = r7.A03(r8, r9, r10, r11, r12, r13)
                goto L_0x006b
            L_0x007c:
                r3.append(r0)
                r0 = 2131891935(0x7f1216df, float:1.9418604E38)
                goto L_0x004c
            L_0x0083:
                if (r5 <= 0) goto L_0x0054
                java.lang.StringBuilder r3 = X.C12960it.A0j(r10)
                r3.append(r0)
                android.content.res.Resources r2 = r14.A02()
                r1 = 2131755333(0x7f100145, float:1.9141542E38)
                java.lang.Object[] r0 = new java.lang.Object[r4]
                X.C12960it.A1P(r0, r5, r7)
                java.lang.String r0 = r2.getQuantityString(r1, r5, r0)
                goto L_0x0050
            */
            throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversationslist.ConversationsFragment.BulkDeleteConversationDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
        }
    }

    /* loaded from: classes2.dex */
    public class DeleteBroadcastListDialogFragment extends Hilt_ConversationsFragment_DeleteBroadcastListDialogFragment {
        public C16170oZ A00;
        public C15550nR A01;
        public C15610nY A02;
        public C14820m6 A03;
        public C21320xE A04;
        public AnonymousClass19M A05;
        public C255719x A06;
        public AbstractC14440lR A07;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int i;
            String A0q;
            int i2;
            String string = A03().getString("jid");
            AbstractC14640lm A01 = AbstractC14640lm.A01(string);
            AnonymousClass009.A06(A01, C12960it.A0d(string, C12960it.A0k("Invalid rawJid=")));
            C15370n3 A0B = this.A01.A0B(A01);
            C70103ak r5 = new C70103ak(this, A0B);
            boolean z = ((AnonymousClass01E) this).A05.getBoolean("chatContainsStarredMessages", false);
            boolean isEmpty = TextUtils.isEmpty(A0B.A0K);
            if (z) {
                if (isEmpty) {
                    i2 = R.string.delete_list_unnamed_starred_dialog_title;
                    A0q = A0I(i2);
                } else {
                    i = R.string.delete_list_starred_dialog_title;
                    A0q = C12970iu.A0q(this, this.A02.A04(A0B), new Object[1], 0, i);
                }
            } else if (isEmpty) {
                i2 = R.string.delete_list_unnamed_dialog_title;
                A0q = A0I(i2);
            } else {
                i = R.string.delete_list_dialog_title;
                A0q = C12970iu.A0q(this, this.A02.A04(A0B), new Object[1], 0, i);
            }
            return this.A06.A02(A0C(), r5, A0q, 1).create();
        }
    }

    /* loaded from: classes2.dex */
    public class DeleteContactDialogFragment extends Hilt_ConversationsFragment_DeleteContactDialogFragment {
        public C16170oZ A00;
        public C15550nR A01;
        public C15610nY A02;
        public C14820m6 A03;
        public C21320xE A04;
        public AnonymousClass19M A05;
        public C255719x A06;
        public AbstractC14440lR A07;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            String quantityString;
            int i;
            String string = A03().getString("jid");
            C15550nR r2 = this.A01;
            AbstractC14640lm A01 = AbstractC14640lm.A01(string);
            AnonymousClass009.A06(A01, C12960it.A0d(string, C12960it.A0k("Invalid rawJid=")));
            C15370n3 A0B = r2.A0B(A01);
            int i2 = ((AnonymousClass01E) this).A05.getInt("unsent_count");
            C70113al r6 = new C70113al(this, A0B);
            if (((AnonymousClass01E) this).A05.getBoolean("chatContainsStarredMessages", false)) {
                i = R.string.delete_contact_with_unsent_and_starred_dialog_title;
                if (i2 == 0) {
                    i = R.string.delete_contact_with_starred_dialog_title;
                }
            } else if (i2 == 0) {
                i = R.string.delete_contact_dialog_title;
            } else {
                Resources A02 = A02();
                Object[] A1a = C12980iv.A1a();
                A1a[0] = this.A02.A04(A0B);
                C12960it.A1P(A1a, i2, 1);
                quantityString = A02.getQuantityString(R.plurals.delete_contact_with_unsent_dialog_title, i2, A1a);
                return this.A06.A02(A0C(), r6, quantityString, 0).create();
            }
            quantityString = C12970iu.A0q(this, this.A02.A04(A0B), new Object[1], 0, i);
            return this.A06.A02(A0C(), r6, quantityString, 0).create();
        }
    }

    /* loaded from: classes2.dex */
    public class DeleteGroupDialogFragment extends Hilt_ConversationsFragment_DeleteGroupDialogFragment {
        public C16170oZ A00;
        public C15550nR A01;
        public C15610nY A02;
        public C14820m6 A03;
        public C21320xE A04;
        public AnonymousClass10Y A05;
        public AnonymousClass19M A06;
        public C255719x A07;
        public AbstractC14440lR A08;

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x003e, code lost:
            if (r5 != null) goto L_0x0046;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
            if (r1 == 3) goto L_0x003a;
         */
        @Override // androidx.fragment.app.DialogFragment
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.app.Dialog A1A(android.os.Bundle r8) {
            /*
                r7 = this;
                android.os.Bundle r1 = r7.A03()
                java.lang.String r0 = "jid"
                java.lang.String r2 = r1.getString(r0)
                X.0lm r1 = X.AbstractC14640lm.A01(r2)
                java.lang.String r0 = "Invalid rawJid="
                java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
                java.lang.String r0 = X.C12960it.A0d(r2, r0)
                X.AnonymousClass009.A06(r1, r0)
                X.0nR r0 = r7.A01
                X.0n3 r3 = r0.A0B(r1)
                java.lang.String r0 = r3.A0K
                r6 = 2
                if (r0 != 0) goto L_0x0040
                X.10Y r0 = r7.A05
                X.0mz r2 = r0.A01(r1)
                boolean r0 = r2 instanceof X.AnonymousClass1XB
                if (r0 == 0) goto L_0x0040
                r0 = r2
                X.1XB r0 = (X.AnonymousClass1XB) r0
                int r1 = r0.A00
                if (r1 == r6) goto L_0x003a
                r0 = 3
                if (r1 != r0) goto L_0x0040
            L_0x003a:
                java.lang.String r5 = r2.A0I()
                if (r5 != 0) goto L_0x0046
            L_0x0040:
                X.0nY r0 = r7.A02
                java.lang.String r5 = r0.A04(r3)
            L_0x0046:
                X.3am r4 = new X.3am
                r4.<init>(r7, r3)
                android.os.Bundle r1 = r7.A05
                java.lang.String r0 = "chatContainsStarredMessages"
                r3 = 0
                boolean r2 = r1.getBoolean(r0, r3)
                r0 = 1
                r1 = 2131887699(0x7f120653, float:1.9410012E38)
                if (r2 == 0) goto L_0x005d
                r1 = 2131887700(0x7f120654, float:1.9410014E38)
            L_0x005d:
                java.lang.Object[] r0 = new java.lang.Object[r0]
                java.lang.String r2 = X.C12970iu.A0q(r7, r5, r0, r3, r1)
                X.19x r1 = r7.A07
                X.00k r0 = r7.A0C()
                X.02e r0 = r1.A02(r0, r4, r2, r6)
                X.04S r0 = r0.create()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversationslist.ConversationsFragment.DeleteGroupDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
        }
    }
}
