package com.whatsapp.conversationslist;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC36671kL;
import X.AnonymousClass028;
import X.AnonymousClass11B;
import X.AnonymousClass19M;
import X.C004802e;
import X.C14820m6;
import X.C14860mA;
import X.C14900mE;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15860o1;
import X.C16970q3;
import X.C18640sm;
import X.C19990v2;
import X.C20650w6;
import X.C20660w7;
import X.C20710wC;
import X.C21320xE;
import X.C254119h;
import X.C32021bU;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S1411000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class LeaveGroupsDialogFragment extends Hilt_LeaveGroupsDialogFragment {
    public C14900mE A00;
    public C15450nH A01;
    public C15550nR A02;
    public C15610nY A03;
    public C254119h A04;
    public C18640sm A05;
    public C14820m6 A06;
    public C20650w6 A07;
    public C19990v2 A08;
    public C21320xE A09;
    public C15600nX A0A;
    public AnonymousClass11B A0B;
    public AnonymousClass19M A0C;
    public C20710wC A0D;
    public C20660w7 A0E;
    public C15860o1 A0F;
    public AbstractC14440lR A0G;
    public C14860mA A0H;

    public static LeaveGroupsDialogFragment A00(C15580nU r4, String str, int i, int i2, boolean z, boolean z2) {
        LeaveGroupsDialogFragment leaveGroupsDialogFragment = new LeaveGroupsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", r4.getRawString());
        bundle.putInt("unsent_count", i);
        bundle.putBoolean("report_upsell", z);
        bundle.putString("block_spam_flow", str);
        bundle.putInt("leave_group_action", i2);
        bundle.putBoolean("show_neutral_button", z2);
        leaveGroupsDialogFragment.A0U(bundle);
        return leaveGroupsDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Iterable stringArrayList;
        String quantityString;
        int i;
        int i2;
        DialogInterface.OnClickListener onClickListener;
        Bundle A03 = A03();
        int i3 = A03.getInt("unsent_count", 0);
        String string = A03.getString("jid");
        boolean z = A03.getBoolean("report_upsell", false);
        String string2 = A03.getString("block_spam_flow");
        boolean z2 = A03.getBoolean("show_neutral_button", true);
        int i4 = A03.getInt("leave_group_action", 1);
        if (string != null) {
            stringArrayList = Collections.singletonList(string);
        } else {
            stringArrayList = A03.getStringArrayList("selection_jids");
        }
        List<AbstractC14640lm> A07 = C15380n4.A07(C15580nU.class, stringArrayList);
        int size = A07.size();
        AbstractC14640lm A01 = AbstractC14640lm.A01(string);
        if (A01 != null) {
            C15370n3 A0B = this.A02.A0B(A01);
            if (i3 == 0) {
                quantityString = A0J(R.string.exit_group_dialog_title, this.A03.A04(A0B));
            } else {
                quantityString = A02().getQuantityString(R.plurals.exit_group_with_unsent_dialog_title, i3, this.A03.A04(A0B), Integer.valueOf(i3));
            }
        } else {
            quantityString = A02().getQuantityString(R.plurals.bulk_leave_conversations, size, Integer.valueOf(size));
            if (i3 > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(quantityString);
                sb.append("\n");
                sb.append(A02().getQuantityString(R.plurals.unsent_messages_in_selection, i3, Integer.valueOf(i3)));
                quantityString = sb.toString();
            }
        }
        C004802e r3 = new C004802e(A0C());
        CharSequence A05 = AbstractC36671kL.A05(A0B(), this.A0C, quantityString);
        C15580nU A04 = C15580nU.A04(string);
        CheckBox checkBox = null;
        if (z) {
            View inflate = LayoutInflater.from(A0p()).inflate(R.layout.dialog_with_checkbox, (ViewGroup) null);
            r3.setTitle(A05);
            r3.setView(inflate);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.dialog_message)).setText(R.string.exit_group_dialog_message);
            checkBox = (CheckBox) AnonymousClass028.A0D(inflate, R.id.checkbox);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.checkbox_header)).setText(R.string.report_group);
            ((TextView) AnonymousClass028.A0D(inflate, R.id.checkbox_message)).setText(R.string.reporting_flow_general_group);
            AnonymousClass028.A0D(inflate, R.id.checkbox_container).setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(checkBox, 46));
        } else if (this.A0B.A02.A07(1597)) {
            if (!A1K(A04)) {
                r3.setTitle(A05);
            }
            if (A07.size() != 1) {
                i = R.string.exit_group_dialog_message_silent_exit_multi;
            } else if (A1K(A04)) {
                int i5 = R.string.community_admin_cant_leave_cag_message;
                if (this.A0A.A0G(A04)) {
                    i5 = R.string.community_creator_cant_leave_cag_message;
                }
                r3.A06(i5);
            } else {
                i = R.string.exit_group_dialog_message_silent_exit;
            }
            r3.A06(i);
        } else {
            r3.A0A(A05);
        }
        r3.A0B(true);
        if (z2) {
            ArrayList arrayList = new ArrayList();
            boolean z3 = true;
            for (AbstractC14640lm r1 : A07) {
                boolean A0S = this.A0F.A0S(r1);
                z3 &= A0S;
                if (!A0S) {
                    arrayList.add(r1);
                }
            }
            C32021bU r2 = new C32021bU(Boolean.valueOf(z3), arrayList);
            boolean booleanValue = ((Boolean) r2.A00).booleanValue();
            List list = (List) r2.A01;
            if (C16970q3.A01(this.A06)) {
                ArrayList arrayList2 = new ArrayList();
                boolean z4 = true;
                for (AbstractC14640lm r12 : A07) {
                    boolean A0E = this.A08.A0E(r12);
                    z4 &= A0E;
                    if (!A0E) {
                        arrayList2.add(r12);
                    }
                }
                C32021bU r13 = new C32021bU(Boolean.valueOf(z4), arrayList2);
                boolean booleanValue2 = ((Boolean) r13.A00).booleanValue();
                List list2 = (List) r13.A01;
                if (!booleanValue2) {
                    boolean A1K = A1K(A04);
                    i2 = R.string.archive_instead;
                    onClickListener = new DialogInterface.OnClickListener(list2) { // from class: X.4ga
                        public final /* synthetic */ List A01;

                        {
                            this.A01 = r2;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i6) {
                            LeaveGroupsDialogFragment leaveGroupsDialogFragment = LeaveGroupsDialogFragment.this;
                            leaveGroupsDialogFragment.A0G.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(leaveGroupsDialogFragment, 30, this.A01));
                            leaveGroupsDialogFragment.A1B();
                        }
                    };
                    if (A1K) {
                        r3.setNegativeButton(R.string.archive_instead, onClickListener);
                    }
                    r3.A00(i2, onClickListener);
                }
            } else if (!booleanValue) {
                i2 = R.string.mute_instead;
                onClickListener = new DialogInterface.OnClickListener(list) { // from class: X.4gZ
                    public final /* synthetic */ List A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i6) {
                        LeaveGroupsDialogFragment leaveGroupsDialogFragment = LeaveGroupsDialogFragment.this;
                        MuteDialogFragment.A01(this.A01).A1F(leaveGroupsDialogFragment.A0F(), null);
                        leaveGroupsDialogFragment.A1B();
                    }
                };
                r3.A00(i2, onClickListener);
            }
        }
        if (A1K(A04)) {
            r3.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fg
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i6) {
                    LeaveGroupsDialogFragment.this.A1B();
                }
            });
        } else {
            r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fh
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i6) {
                    LeaveGroupsDialogFragment.this.A1B();
                }
            });
            r3.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener(checkBox, this, string2, A07, i4, z) { // from class: X.3Ky
                public final /* synthetic */ int A00;
                public final /* synthetic */ CheckBox A01;
                public final /* synthetic */ LeaveGroupsDialogFragment A02;
                public final /* synthetic */ String A03;
                public final /* synthetic */ List A04;
                public final /* synthetic */ boolean A05;

                {
                    this.A02 = r2;
                    this.A00 = r5;
                    this.A04 = r4;
                    this.A05 = r6;
                    this.A01 = r1;
                    this.A03 = r3;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i6) {
                    LeaveGroupsDialogFragment leaveGroupsDialogFragment = this.A02;
                    int i7 = this.A00;
                    List list3 = this.A04;
                    boolean z5 = this.A05;
                    CheckBox checkBox2 = this.A01;
                    String str = this.A03;
                    ActivityC000900k A0B2 = leaveGroupsDialogFragment.A0B();
                    Log.i("LeaveGroupsDialogFragment/user-try-leaveGroup");
                    boolean A0B3 = leaveGroupsDialogFragment.A05.A0B();
                    C14900mE r22 = leaveGroupsDialogFragment.A00;
                    if (A0B3) {
                        if (i7 != 1) {
                            r22.A06(R.string.participant_removing, R.string.register_wait_message);
                            if (i7 == 2) {
                                C14960mK.A0d(leaveGroupsDialogFragment);
                            }
                        }
                        Iterator it = list3.iterator();
                        while (it.hasNext()) {
                            AbstractC14640lm A0b = C12990iw.A0b(it);
                            Log.i(C12960it.A0b("LeaveGroupsDialogFragment/exit/group:", A0b));
                            leaveGroupsDialogFragment.A09.A09(A0b, true);
                            leaveGroupsDialogFragment.A0G.Ab2(new RunnableBRunnable0Shape0S1411000_I0(leaveGroupsDialogFragment, checkBox2, A0b, A0B2, str, i7, 2, z5));
                        }
                    } else {
                        r22.A07(R.string.failed_to_leave_group, 0);
                    }
                    C14820m6 r4 = leaveGroupsDialogFragment.A06;
                    C12960it.A0u(r4.A00, "delete_chat_count", C12970iu.A01(r4.A00, "delete_chat_count") + list3.size());
                    if (i7 == 1) {
                        leaveGroupsDialogFragment.A09.A06(2);
                    }
                    leaveGroupsDialogFragment.A1B();
                }
            });
        }
        return r3.create();
    }

    public final boolean A1K(C15580nU r3) {
        return r3 != null && this.A08.A02(r3) == 3 && this.A0A.A0D(r3);
    }
}
