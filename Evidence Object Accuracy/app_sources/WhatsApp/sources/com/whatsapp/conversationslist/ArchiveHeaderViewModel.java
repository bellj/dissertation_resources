package com.whatsapp.conversationslist;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.C12980iv;
import X.C14820m6;
import X.C14850m9;
import X.C15680nj;
import X.C19990v2;

/* loaded from: classes3.dex */
public class ArchiveHeaderViewModel extends AnonymousClass015 {
    public int A00 = 0;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C14820m6 A02;
    public final AnonymousClass018 A03;
    public final C19990v2 A04;
    public final C15680nj A05;
    public final C14850m9 A06;

    public ArchiveHeaderViewModel(C14820m6 r2, AnonymousClass018 r3, C19990v2 r4, C15680nj r5, C14850m9 r6) {
        this.A06 = r6;
        this.A04 = r4;
        this.A03 = r3;
        this.A02 = r2;
        this.A05 = r5;
    }
}
