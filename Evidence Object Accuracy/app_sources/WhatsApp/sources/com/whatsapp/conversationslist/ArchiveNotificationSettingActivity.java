package com.whatsapp.conversationslist;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C16970q3;
import android.os.Bundle;
import android.widget.CompoundButton;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.R;
import com.whatsapp.components.WaSwitchView;
import com.whatsapp.conversationslist.ArchiveNotificationSettingActivity;

/* loaded from: classes2.dex */
public class ArchiveNotificationSettingActivity extends ActivityC13790kL {
    public C16970q3 A00;
    public boolean A01;

    public ArchiveNotificationSettingActivity() {
        this(0);
    }

    public ArchiveNotificationSettingActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 61);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C16970q3) A1M.A0a.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        setContentView(R.layout.archive_notification_activity);
        setTitle(R.string.archive_settings);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        ActivityC13790kL.A0c(this, toolbar, ((ActivityC13830kP) this).A01);
        toolbar.setTitle(getString(R.string.archive_settings));
        toolbar.setBackgroundResource(R.color.primary);
        toolbar.A0C(this, 2131952341);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(this, 48));
        A1e(toolbar);
        WaSwitchView waSwitchView = (WaSwitchView) AnonymousClass00T.A05(this, R.id.notify_new_message_switch_view);
        waSwitchView.setChecked(true ^ ((ActivityC13810kN) this).A09.A00.getBoolean("notify_new_message_for_archived_chats", false));
        waSwitchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.4p8
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ArchiveNotificationSettingActivity.this.A00.A05(!z);
            }
        });
        waSwitchView.setOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(waSwitchView, 46));
        WaSwitchView waSwitchView2 = (WaSwitchView) AnonymousClass00T.A05(this, R.id.auto_hide_switch_view);
        waSwitchView2.setChecked(((ActivityC13810kN) this).A09.A00.getBoolean("auto_archive_inactive_chats", false));
        waSwitchView2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.3OL
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                C12960it.A0t(C12960it.A08(((ActivityC13810kN) ArchiveNotificationSettingActivity.this).A09), "auto_archive_inactive_chats", z);
            }
        });
        waSwitchView2.setOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(waSwitchView2, 47));
        waSwitchView2.setVisibility(8);
    }
}
