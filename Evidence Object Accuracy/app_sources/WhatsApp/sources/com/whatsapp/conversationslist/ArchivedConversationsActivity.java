package com.whatsapp.conversationslist;

import X.AbstractC009504t;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00E;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass2FL;
import X.C004902f;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C16970q3;
import X.C20140vH;
import X.C41691tw;
import android.os.Bundle;
import android.view.MenuItem;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ArchivedConversationsActivity extends ActivityC13790kL {
    public C20140vH A00;
    public boolean A01;

    public ArchivedConversationsActivity() {
        this(0);
    }

    public ArchivedConversationsActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 62);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C20140vH) A1M.AHo.get();
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A02(this, R.color.primary);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        super.AXD(r2);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        boolean A1W = C12980iv.A1W(((ActivityC13810kN) this).A09.A00, "archive_v2_enabled");
        int i = R.string.archived_chats;
        if (A1W) {
            i = R.string.archived_chats_v2;
        }
        setTitle(i);
        A1U().A0M(true);
        setContentView(R.layout.archived_conversations);
        if (bundle == null) {
            C004902f A0P = C12970iu.A0P(this);
            A0P.A06(new ArchivedConversationsFragment(), R.id.container);
            A0P.A01();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        AbstractC14440lR r4 = ((ActivityC13830kP) this).A05;
        C20140vH r3 = this.A00;
        C14820m6 r2 = ((ActivityC13810kN) this).A09;
        if (C16970q3.A02(r2)) {
            r4.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r2, 16, r3));
        }
    }
}
