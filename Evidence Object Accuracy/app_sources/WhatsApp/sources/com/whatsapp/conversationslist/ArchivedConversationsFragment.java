package com.whatsapp.conversationslist;

import X.ActivityC000900k;
import X.AnonymousClass23N;
import X.C14820m6;
import X.C16970q3;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.fragment.app.ListFragment;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ArchivedConversationsFragment extends Hilt_ArchivedConversationsFragment {
    public AnimatorSet A00;
    public View A01;
    public View A02;

    @Override // com.whatsapp.conversationslist.ConversationsFragment, X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        if (this.A1D.A00.getBoolean("archive_v2_enabled", false)) {
            ((ConversationsFragment) this).A0M.A08();
            menu.add(1, R.id.menuitem_archive_chat_notifications, 0, R.string.archive_settings);
            return;
        }
        super.A0y(menu, menuInflater);
    }

    @Override // com.whatsapp.conversationslist.ConversationsFragment, X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_archive_chat_notifications) {
            return super.A0z(menuItem);
        }
        ActivityC000900k A0B = A0B();
        if (A0B == null) {
            return true;
        }
        A0v(new Intent().setClassName(A0B.getPackageName(), "com.whatsapp.conversationslist.ArchiveNotificationSettingActivity"));
        return true;
    }

    @Override // com.whatsapp.conversationslist.ConversationsFragment, androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (C16970q3.A02(this.A1D)) {
            C14820m6 r3 = this.A1D;
            r3.A00.edit().putInt("new_archive_nux_shown_count", r3.A00.getInt("new_archive_nux_shown_count", 0) + 1).apply();
        }
        return super.A10(bundle, layoutInflater, viewGroup);
    }

    @Override // com.whatsapp.conversationslist.ConversationsFragment
    public void A1E() {
        super.A1E();
        if (this.A1J.A00() == 0) {
            A0B().finish();
        }
    }

    public final View A1U(int i) {
        LayoutInflater layoutInflater = A0C().getLayoutInflater();
        ListFragment.A00(this);
        View inflate = layoutInflater.inflate(i, (ViewGroup) ((ListFragment) this).A04, false);
        FrameLayout frameLayout = new FrameLayout(A01());
        AnonymousClass23N.A04(frameLayout, false);
        frameLayout.addView(inflate);
        ListFragment.A00(this);
        ((ListFragment) this).A04.addHeaderView(frameLayout, null, false);
        return inflate;
    }

    public final void A1V() {
        if (this.A1Y.A07(923)) {
            if (this.A02 == null) {
                View A1U = A1U(R.layout.archived_chats_header);
                this.A02 = A1U;
                A1U.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 41));
            }
            TextView textView = (TextView) this.A02.findViewById(R.id.title);
            boolean z = this.A1D.A00.getBoolean("notify_new_message_for_archived_chats", false);
            int i = R.string.archived_chats_setting_header_title_keep_archived;
            if (z) {
                i = R.string.archived_chats_setting_header_title_auto_unarchive;
            }
            textView.setText(i);
            this.A02.setVisibility(0);
        }
    }
}
