package com.whatsapp.conversationslist;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10A;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Y;
import X.AnonymousClass116;
import X.AnonymousClass11H;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12O;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass132;
import X.AnonymousClass139;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass161;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass185;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1AM;
import X.AnonymousClass1AQ;
import X.AnonymousClass1AR;
import X.AnonymousClass1C5;
import X.AnonymousClass1CT;
import X.AnonymousClass2FL;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C15240mn;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16490p7;
import X.C16590pI;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C18000rk;
import X.C18330sH;
import X.C18640sm;
import X.C18850tA;
import X.C19990v2;
import X.C20040v7;
import X.C20220vP;
import X.C20650w6;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21190x1;
import X.C21240x6;
import X.C21270x9;
import X.C21300xC;
import X.C21320xE;
import X.C21400xM;
import X.C22050yP;
import X.C22100yW;
import X.C22230yk;
import X.C22330yu;
import X.C22630zO;
import X.C22640zP;
import X.C22710zW;
import X.C22730zY;
import X.C236812p;
import X.C237412v;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C245716a;
import X.C246716k;
import X.C250417w;
import X.C251118d;
import X.C252718t;
import X.C253018w;
import X.C253519b;
import X.C254219i;
import X.C255719x;
import X.C48572Gu;
import X.C48962Ip;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaListFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_ConversationsFragment extends WaListFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A05() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A05();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A05()
            r3.A19()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversationslist.Hilt_ConversationsFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A05();
        A19();
    }

    public void A19() {
        if (!this.A02) {
            this.A02 = true;
            ConversationsFragment conversationsFragment = (ConversationsFragment) this;
            C51112Sw r4 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r3 = r4.A0Y;
            ((WaListFragment) conversationsFragment).A00 = (AnonymousClass182) r3.A94.get();
            ((WaListFragment) conversationsFragment).A01 = (AnonymousClass180) r3.ALt.get();
            conversationsFragment.A1A = (C14830m7) r3.ALb.get();
            conversationsFragment.A1Y = (C14850m9) r3.A04.get();
            conversationsFragment.A0L = (C14900mE) r3.A8X.get();
            conversationsFragment.A2A = (C252718t) r3.A9K.get();
            conversationsFragment.A1g = (AnonymousClass13H) r3.ABY.get();
            conversationsFragment.A20 = (C253018w) r3.AJS.get();
            conversationsFragment.A0M = (C15570nT) r3.AAr.get();
            conversationsFragment.A1B = (C16590pI) r3.AMg.get();
            conversationsFragment.A2B = (AbstractC14440lR) r3.ANe.get();
            conversationsFragment.A0H = (AbstractC15710nm) r3.A4o.get();
            conversationsFragment.A1G = (C19990v2) r3.A3M.get();
            conversationsFragment.A21 = (AnonymousClass12U) r3.AJd.get();
            conversationsFragment.A1a = (C16120oU) r3.ANE.get();
            conversationsFragment.A2C = (C14860mA) r3.ANU.get();
            conversationsFragment.A0E = (AnonymousClass17S) r3.A0F.get();
            conversationsFragment.A1F = (C20650w6) r3.A3A.get();
            conversationsFragment.A1X = (AnonymousClass19M) r3.A6R.get();
            conversationsFragment.A0N = (C15450nH) r3.AII.get();
            conversationsFragment.A0g = (C18850tA) r3.AKx.get();
            conversationsFragment.A0P = (AnonymousClass1AR) r3.ALM.get();
            conversationsFragment.A0Q = (C16170oZ) r3.AM4.get();
            conversationsFragment.A0F = (AnonymousClass12P) r3.A0H.get();
            conversationsFragment.A0o = (C21270x9) r3.A4A.get();
            conversationsFragment.A1P = (C20040v7) r3.AAK.get();
            conversationsFragment.A1h = (C17220qS) r3.ABt.get();
            conversationsFragment.A1p = (AnonymousClass14X) r3.AFM.get();
            conversationsFragment.A1u = (AnonymousClass17R) r3.AIz.get();
            conversationsFragment.A0i = (AnonymousClass130) r3.A41.get();
            conversationsFragment.A0j = (C15550nR) r3.A45.get();
            conversationsFragment.A0G = (C253519b) r3.A4C.get();
            conversationsFragment.A1K = (C241714m) r3.A4l.get();
            conversationsFragment.A18 = (AnonymousClass01d) r3.ALI.get();
            conversationsFragment.A0l = (C15610nY) r3.AMe.get();
            conversationsFragment.A1j = (C22230yk) r3.ANT.get();
            conversationsFragment.A1E = (AnonymousClass018) r3.ANb.get();
            conversationsFragment.A1o = (C17070qD) r3.AFC.get();
            conversationsFragment.A1q = (C21190x1) r3.A3G.get();
            conversationsFragment.A1L = (C15240mn) r3.A8F.get();
            conversationsFragment.A28 = (C21300xC) r3.A0c.get();
            conversationsFragment.A0Z = (C238013b) r3.A1Z.get();
            conversationsFragment.A0d = (C243915i) r3.A37.get();
            conversationsFragment.A0k = (AnonymousClass10S) r3.A46.get();
            conversationsFragment.A1b = (C20710wC) r3.A8m.get();
            conversationsFragment.A0p = (C21240x6) r3.AA7.get();
            conversationsFragment.A1Q = (AnonymousClass10Y) r3.AAR.get();
            conversationsFragment.A1z = (AnonymousClass12F) r3.AJM.get();
            conversationsFragment.A26 = (AnonymousClass198) r3.A0J.get();
            AnonymousClass2FL r2 = r4.A0V;
            conversationsFragment.A14 = r2.A05();
            conversationsFragment.A1t = (C15860o1) r3.A3H.get();
            conversationsFragment.A1Z = (C22050yP) r3.A7v.get();
            conversationsFragment.A1y = (C240514a) r3.AJL.get();
            conversationsFragment.A1i = (C14840m8) r3.ACi.get();
            conversationsFragment.A1l = (C22630zO) r3.AD7.get();
            conversationsFragment.A19 = (C15410nB) r3.ALJ.get();
            conversationsFragment.A25 = (AnonymousClass12O) r3.AMG.get();
            conversationsFragment.A0R = (C18330sH) r3.AN4.get();
            conversationsFragment.A27 = (C254219i) r3.A0K.get();
            conversationsFragment.A0c = (C22330yu) r3.A3I.get();
            conversationsFragment.A0m = (AnonymousClass10T) r3.A47.get();
            conversationsFragment.A0r = (C20730wE) r3.A4J.get();
            conversationsFragment.A1T = (AnonymousClass132) r3.ALx.get();
            conversationsFragment.A0h = (AnonymousClass116) r3.A3z.get();
            conversationsFragment.A0a = (C251118d) r3.A2D.get();
            conversationsFragment.A1k = (C20220vP) r3.AC3.get();
            conversationsFragment.A1R = (C16490p7) r3.ACJ.get();
            conversationsFragment.A1W = (C245716a) r3.AJR.get();
            conversationsFragment.A1S = (C242114q) r3.AJq.get();
            conversationsFragment.A1C = (C15890o4) r3.AN1.get();
            conversationsFragment.A1U = (C21400xM) r3.ABd.get();
            conversationsFragment.A1D = (C14820m6) r3.AN3.get();
            conversationsFragment.A0Y = (C246716k) r3.A2X.get();
            conversationsFragment.A0e = (C22640zP) r3.A3Z.get();
            conversationsFragment.A1J = (C15680nj) r3.A4e.get();
            conversationsFragment.A1O = (C236812p) r3.AAC.get();
            conversationsFragment.A1x = (AnonymousClass1AM) r3.AJG.get();
            conversationsFragment.A1I = (C21320xE) r3.A4Y.get();
            conversationsFragment.A1N = (AnonymousClass1C5) r3.A9N.get();
            conversationsFragment.A1m = (AnonymousClass11H) r3.AEp.get();
            conversationsFragment.A1n = (C22710zW) r3.AF7.get();
            conversationsFragment.A0V = r4.A00();
            conversationsFragment.A0W = (C14650lo) r3.A2V.get();
            conversationsFragment.A1V = (C22100yW) r3.A3g.get();
            conversationsFragment.A0s = (C250417w) r3.A4b.get();
            conversationsFragment.A1r = (AnonymousClass185) r3.AHy.get();
            conversationsFragment.A24 = (AnonymousClass1CT) r3.AMB.get();
            conversationsFragment.A0X = (AnonymousClass10A) r3.A2W.get();
            conversationsFragment.A29 = (C255719x) r3.A5X.get();
            conversationsFragment.A1M = (C15600nX) r3.A8x.get();
            conversationsFragment.A17 = (C18640sm) r3.A3u.get();
            conversationsFragment.A0O = (AnonymousClass11L) r3.ALG.get();
            conversationsFragment.A0f = (C237412v) r3.A3k.get();
            conversationsFragment.A0q = (C17010q7) r3.A43.get();
            conversationsFragment.A1H = (C20830wO) r3.A4W.get();
            conversationsFragment.A1c = (C244215l) r3.A8y.get();
            conversationsFragment.A1v = (AnonymousClass161) r3.AJ4.get();
            conversationsFragment.A0S = (C22730zY) r3.A8Y.get();
            conversationsFragment.A0U = (AnonymousClass10K) r3.A8c.get();
            conversationsFragment.A1w = (AnonymousClass1AQ) r3.AJ7.get();
            conversationsFragment.A0b = (C16430p0) r3.A5y.get();
            conversationsFragment.A0T = (AnonymousClass10J) r3.A8b.get();
            conversationsFragment.A1d = (AnonymousClass139) r3.A9o.get();
            conversationsFragment.A0t = (AnonymousClass11P) r3.ABp.get();
            conversationsFragment.A2D = C18000rk.A00(r3.ADw);
            conversationsFragment.A2E = C18000rk.A00(r3.AID);
            conversationsFragment.A0I = (C48962Ip) r2.A00.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
