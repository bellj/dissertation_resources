package com.whatsapp.conversationslist;

import X.AbstractC14440lR;
import X.AbstractC33091dK;
import X.AbstractC51662Vw;
import X.AbstractC51682Vy;
import X.AbstractC65103Id;
import X.AbstractC93424a9;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass10Y;
import X.AnonymousClass11L;
import X.AnonymousClass12F;
import X.AnonymousClass130;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass1J1;
import X.AnonymousClass2GE;
import X.AnonymousClass2WJ;
import X.AnonymousClass2WK;
import X.AnonymousClass2WN;
import X.AnonymousClass3J9;
import X.AnonymousClass48M;
import X.AnonymousClass4A5;
import X.AnonymousClass5U4;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15860o1;
import X.C16590pI;
import X.C17070qD;
import X.C19990v2;
import X.C20710wC;
import X.C21400xM;
import X.C22710zW;
import X.C236812p;
import X.C238013b;
import X.C241714m;
import X.C253519b;
import X.C29941Vi;
import X.C32341c0;
import X.C42941w9;
import X.C61182zY;
import X.C61192zZ;
import X.C61202za;
import X.C63543Bz;
import X.C63563Cb;
import X.C64303Fa;
import X.C92434Vw;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.lifecycle.OnLifecycleEvent;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.community.SubgroupPileView;
import com.whatsapp.components.ConversationListRowHeaderView;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* loaded from: classes.dex */
public class ViewHolder extends AbstractC51682Vy implements AnonymousClass03H {
    public C64303Fa A00;
    public AbstractC65103Id A01;
    public AbstractC51662Vw A02;
    public final View A03;
    public final View A04;
    public final View A05;
    public final View A06;
    public final ImageView A07;
    public final ImageView A08;
    public final ImageView A09;
    public final ImageView A0A;
    public final ImageView A0B;
    public final ImageView A0C;
    public final ImageView A0D;
    public final ImageView A0E;
    public final ImageView A0F;
    public final TextView A0G;
    public final C253519b A0H;
    public final C15570nT A0I;
    public final C15450nH A0J;
    public final AnonymousClass11L A0K;
    public final TextEmojiLabel A0L;
    public final TextEmojiLabel A0M;
    public final WaImageView A0N;
    public final WaImageView A0O;
    public final WaTextView A0P;
    public final C14650lo A0Q;
    public final C238013b A0R;
    public final SubgroupPileView A0S;
    public final ConversationListRowHeaderView A0T;
    public final SelectionCheckView A0U;
    public final AnonymousClass130 A0V;
    public final C15550nR A0W;
    public final C15610nY A0X;
    public final AnonymousClass1J1 A0Y;
    public final C63563Cb A0Z;
    public final AbstractC33091dK A0a;
    public final C14830m7 A0b;
    public final C16590pI A0c;
    public final C14820m6 A0d;
    public final AnonymousClass018 A0e;
    public final C19990v2 A0f;
    public final C241714m A0g;
    public final C15600nX A0h;
    public final C236812p A0i;
    public final AnonymousClass10Y A0j;
    public final C21400xM A0k;
    public final C14850m9 A0l;
    public final C20710wC A0m;
    public final AnonymousClass13H A0n;
    public final C22710zW A0o;
    public final C17070qD A0p;
    public final AnonymousClass14X A0q;
    public final C15860o1 A0r;
    public final AnonymousClass12F A0s;
    public final AnonymousClass3J9 A0t;
    public final AbstractC14440lR A0u;
    public final AbstractC93424a9 A0v = new AnonymousClass48M();

    public ViewHolder(Context context, View view, C253519b r11, C15570nT r12, C15450nH r13, AnonymousClass11L r14, C14650lo r15, C238013b r16, AnonymousClass130 r17, C15550nR r18, C15610nY r19, AnonymousClass1J1 r20, C63563Cb r21, AbstractC33091dK r22, C14830m7 r23, C16590pI r24, C14820m6 r25, AnonymousClass018 r26, C19990v2 r27, C241714m r28, C15600nX r29, C236812p r30, AnonymousClass10Y r31, C21400xM r32, C14850m9 r33, C20710wC r34, AnonymousClass13H r35, C22710zW r36, C17070qD r37, AnonymousClass14X r38, C15860o1 r39, AnonymousClass12F r40, AnonymousClass3J9 r41, AbstractC14440lR r42) {
        super(view);
        this.A0b = r23;
        this.A0l = r33;
        this.A0n = r35;
        this.A0I = r12;
        this.A0c = r24;
        this.A0u = r42;
        this.A0f = r27;
        this.A0J = r13;
        this.A0q = r38;
        this.A0V = r17;
        this.A0W = r18;
        this.A0H = r11;
        this.A0g = r28;
        this.A0X = r19;
        this.A0e = r26;
        this.A0p = r37;
        this.A0t = r41;
        this.A0R = r16;
        this.A0m = r34;
        this.A0j = r31;
        this.A0s = r40;
        this.A0r = r39;
        this.A0Y = r20;
        this.A0k = r32;
        this.A0d = r25;
        this.A0i = r30;
        this.A0o = r36;
        this.A0Z = r21;
        this.A0Q = r15;
        this.A0h = r29;
        this.A0K = r14;
        this.A0a = r22;
        ConversationListRowHeaderView conversationListRowHeaderView = (ConversationListRowHeaderView) AnonymousClass028.A0D(view, R.id.conversations_row_header);
        this.A0T = conversationListRowHeaderView;
        this.A00 = new C64303Fa(r24.A01(), conversationListRowHeaderView, r19, r40);
        this.A05 = AnonymousClass028.A0D(view, R.id.contact_row_container);
        this.A00.A00();
        this.A06 = AnonymousClass028.A0D(view, R.id.progressbar_small);
        this.A08 = A0E(view);
        this.A0S = (SubgroupPileView) AnonymousClass028.A0D(view, R.id.subgroup_contact_photo);
        this.A04 = AnonymousClass028.A0D(view, R.id.contact_selector);
        this.A0L = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.single_msg_tv);
        this.A0M = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.msg_from_tv);
        this.A0O = (WaImageView) AnonymousClass028.A0D(view, R.id.conversations_row_unseen_important_message_indicator);
        TextView textView = (TextView) AnonymousClass028.A0D(view, R.id.conversations_row_message_count);
        this.A0G = textView;
        this.A0N = (WaImageView) AnonymousClass028.A0D(view, R.id.community_unread_indicator);
        this.A0E = (ImageView) AnonymousClass028.A0D(view, R.id.status_indicator);
        this.A0F = (ImageView) AnonymousClass028.A0D(view, R.id.status_reply_indicator);
        this.A0B = (ImageView) AnonymousClass028.A0D(view, R.id.media_indicator);
        this.A0P = (WaTextView) AnonymousClass028.A0D(view, R.id.payments_indicator);
        ImageView imageView = (ImageView) AnonymousClass028.A0D(view, R.id.mute_indicator);
        this.A0C = imageView;
        ImageView imageView2 = (ImageView) AnonymousClass028.A0D(view, R.id.pin_indicator);
        this.A0D = imageView2;
        if (r33.A07(363)) {
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.conversation_row_indicator_margin_left);
            C42941w9.A07(imageView, r26, dimensionPixelSize, 0);
            C42941w9.A07(imageView2, r26, dimensionPixelSize, 0);
            C42941w9.A07(textView, r26, dimensionPixelSize, 0);
        }
        boolean A07 = r33.A07(363);
        int i = R.color.conversationBadgeTint;
        if (A07) {
            imageView2.setImageDrawable(AnonymousClass00T.A04(context, R.drawable.ic_inline_pin_new));
            i = R.color.msgStatusTint;
        }
        AnonymousClass2GE.A07(imageView2, AnonymousClass00T.A00(context, i));
        this.A0A = (ImageView) AnonymousClass028.A0D(view, R.id.live_location_indicator);
        this.A03 = AnonymousClass028.A0D(view, R.id.archived_indicator);
        this.A0U = (SelectionCheckView) AnonymousClass028.A0D(view, R.id.selection_check);
        this.A09 = (ImageView) AnonymousClass028.A0D(view, R.id.conversations_row_ephemeral_status);
        this.A07 = (ImageView) AnonymousClass028.A0D(view, R.id.conversations_row_call_type_indicator);
    }

    public final ImageView A0E(View view) {
        ImageView imageView = (ImageView) AnonymousClass028.A0D(view, R.id.wds_profile_picture);
        ImageView imageView2 = (ImageView) AnonymousClass028.A0D(view, R.id.contact_photo);
        if (this.A0l.A07(1533)) {
            imageView2.setVisibility(8);
            imageView.setVisibility(0);
            return imageView;
        }
        imageView2.setVisibility(0);
        imageView.setVisibility(8);
        return imageView2;
    }

    public void A0F() {
        AbstractC65103Id r0 = this.A01;
        if (r0 != null) {
            r0.A04();
        }
    }

    public void A0G(Activity activity, Context context, AbstractC51662Vw r58, AnonymousClass5U4 r59, C63543Bz r60, C92434Vw r61, int i, int i2, boolean z) {
        if (!C29941Vi.A00(this.A02, r58)) {
            A0F();
            this.A02 = r58;
        }
        this.A08.setTag(null);
        if (r58 instanceof AnonymousClass2WJ) {
            C14830m7 r0 = this.A0b;
            C14850m9 r02 = this.A0l;
            AnonymousClass13H r03 = this.A0n;
            C15570nT r04 = this.A0I;
            C16590pI r05 = this.A0c;
            AbstractC14440lR r06 = this.A0u;
            C19990v2 r07 = this.A0f;
            C15450nH r08 = this.A0J;
            AnonymousClass14X r09 = this.A0q;
            AnonymousClass130 r010 = this.A0V;
            C15550nR r011 = this.A0W;
            C253519b r012 = this.A0H;
            C241714m r013 = this.A0g;
            C15610nY r014 = this.A0X;
            AnonymousClass018 r015 = this.A0e;
            C17070qD r016 = this.A0p;
            AnonymousClass3J9 r017 = this.A0t;
            C238013b r15 = this.A0R;
            C20710wC r14 = this.A0m;
            AnonymousClass10Y r13 = this.A0j;
            C15860o1 r12 = this.A0r;
            C21400xM r11 = this.A0k;
            C14820m6 r10 = this.A0d;
            C236812p r9 = this.A0i;
            C63563Cb r8 = this.A0Z;
            C22710zW r7 = this.A0o;
            C14650lo r6 = this.A0Q;
            C15600nX r5 = this.A0h;
            this.A01 = new C61202za(activity, context, r012, r04, r08, this.A0K, r6, r15, r010, r011, r014, this.A0Y, r8, this.A0a, r60, this, r0, r05, r10, r015, r07, r013, r5, r9, r13, r11, r02, r14, r03, r7, r016, r09, r12, r61, r017, r06, i);
        } else if (r58 instanceof AnonymousClass2WK) {
            C16590pI r018 = this.A0c;
            C14830m7 r019 = this.A0b;
            C14850m9 r020 = this.A0l;
            AnonymousClass13H r021 = this.A0n;
            C15570nT r022 = this.A0I;
            C19990v2 r023 = this.A0f;
            C15450nH r024 = this.A0J;
            AnonymousClass14X r025 = this.A0q;
            C15550nR r152 = this.A0W;
            C241714m r142 = this.A0g;
            C15610nY r132 = this.A0X;
            AnonymousClass018 r122 = this.A0e;
            C17070qD r112 = this.A0p;
            C238013b r102 = this.A0R;
            C20710wC r92 = this.A0m;
            C15860o1 r82 = this.A0r;
            C22710zW r72 = this.A0o;
            C14650lo r62 = this.A0Q;
            this.A01 = new C61192zZ(activity, context, r022, r024, this.A0K, r62, r102, r152, r132, this.A0Y, this.A0a, r60, this, r019, r018, r122, r023, r142, r020, r92, r021, r72, r112, r025, r82, r61, this.A0t);
        } else if (r58 instanceof AnonymousClass2WN) {
            C16590pI r026 = this.A0c;
            C14830m7 r027 = this.A0b;
            C14850m9 r028 = this.A0l;
            AnonymousClass13H r029 = this.A0n;
            C15570nT r030 = this.A0I;
            C19990v2 r031 = this.A0f;
            C15450nH r032 = this.A0J;
            AnonymousClass14X r153 = this.A0q;
            C15550nR r143 = this.A0W;
            C241714m r133 = this.A0g;
            C15610nY r123 = this.A0X;
            AnonymousClass018 r113 = this.A0e;
            C17070qD r103 = this.A0p;
            C238013b r93 = this.A0R;
            C20710wC r83 = this.A0m;
            C22710zW r73 = this.A0o;
            C14650lo r63 = this.A0Q;
            this.A01 = new C61182zY(activity, context, r030, r032, this.A0K, r63, r93, r143, r123, this.A0Z, this.A0a, r60, this, r027, r026, r113, r031, r133, r028, r83, r029, r73, r103, r153, this.A0t);
        }
        A0H(r59, i2, z);
    }

    public void A0H(AnonymousClass5U4 r3, int i, boolean z) {
        this.A01.A05(this.A02, r3, i, z);
    }

    public final void A0I(WDSProfilePhoto wDSProfilePhoto, boolean z) {
        AbstractC93424a9 r0;
        AbstractC93424a9 profileBadge = wDSProfilePhoto.getProfileBadge();
        if ((profileBadge instanceof AnonymousClass48M) && !z) {
            r0 = null;
        } else if (profileBadge == null && z) {
            r0 = this.A0v;
        } else {
            return;
        }
        wDSProfilePhoto.setProfileBadge(r0);
    }

    public void A0J(boolean z, int i) {
        int i2 = 8;
        if (this.A0S.getVisibility() != 0) {
            ImageView imageView = this.A08;
            if (imageView instanceof WDSProfilePhoto) {
                A0I((WDSProfilePhoto) imageView, z);
                this.A09.setVisibility(8);
                return;
            }
        }
        ImageView imageView2 = this.A09;
        if (z) {
            i2 = 0;
        }
        imageView2.setVisibility(i2);
        imageView2.setContentDescription(C32341c0.A04(this.A0e, i));
        imageView2.setImageResource(R.drawable.ic_chatlist_ephemeral_v2);
    }

    public void A0K(boolean z, boolean z2) {
        SelectionCheckView selectionCheckView;
        AnonymousClass4A5 r0;
        int i = 8;
        if (this.A0S.getVisibility() != 0) {
            ImageView imageView = this.A08;
            if (imageView instanceof WDSProfilePhoto) {
                if (z) {
                    r0 = AnonymousClass4A5.A01;
                } else {
                    r0 = AnonymousClass4A5.A02;
                }
                ((WDSProfilePhoto) imageView).A02(r0, z2);
                selectionCheckView = this.A0U;
                selectionCheckView.setVisibility(i);
            }
        }
        selectionCheckView = this.A0U;
        selectionCheckView.A04(z, z2);
        if (z) {
            i = 0;
        }
        selectionCheckView.setVisibility(i);
    }

    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    public void onDestroy() {
        AbstractC65103Id r0 = this.A01;
        if (r0 != null) {
            r0.A04();
        }
    }
}
