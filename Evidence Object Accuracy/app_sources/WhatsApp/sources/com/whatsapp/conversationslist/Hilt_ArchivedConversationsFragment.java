package com.whatsapp.conversationslist;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10A;
import X.AnonymousClass10J;
import X.AnonymousClass10K;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Y;
import X.AnonymousClass116;
import X.AnonymousClass11H;
import X.AnonymousClass11L;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12O;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass132;
import X.AnonymousClass139;
import X.AnonymousClass13H;
import X.AnonymousClass14X;
import X.AnonymousClass161;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass185;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass1AM;
import X.AnonymousClass1AQ;
import X.AnonymousClass1AR;
import X.AnonymousClass1C5;
import X.AnonymousClass1CT;
import X.AnonymousClass2FL;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C15240mn;
import X.C15410nB;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15680nj;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16430p0;
import X.C16490p7;
import X.C16590pI;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C18000rk;
import X.C18330sH;
import X.C18640sm;
import X.C18850tA;
import X.C19990v2;
import X.C20040v7;
import X.C20220vP;
import X.C20650w6;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21190x1;
import X.C21240x6;
import X.C21270x9;
import X.C21300xC;
import X.C21320xE;
import X.C21400xM;
import X.C22050yP;
import X.C22100yW;
import X.C22230yk;
import X.C22330yu;
import X.C22630zO;
import X.C22640zP;
import X.C22710zW;
import X.C22730zY;
import X.C236812p;
import X.C237412v;
import X.C238013b;
import X.C240514a;
import X.C241714m;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C245716a;
import X.C246716k;
import X.C250417w;
import X.C251118d;
import X.C252718t;
import X.C253018w;
import X.C253519b;
import X.C254219i;
import X.C255719x;
import X.C48962Ip;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaListFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_ArchivedConversationsFragment extends ConversationsFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    @Override // com.whatsapp.conversationslist.Hilt_ConversationsFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1T();
        return this.A00;
    }

    @Override // com.whatsapp.conversationslist.Hilt_ConversationsFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // com.whatsapp.conversationslist.Hilt_ConversationsFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A1T()
            r3.A19()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversationslist.Hilt_ArchivedConversationsFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.conversationslist.ConversationsFragment, com.whatsapp.conversationslist.Hilt_ConversationsFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1T();
        A19();
    }

    @Override // com.whatsapp.conversationslist.Hilt_ConversationsFragment
    public void A19() {
        if (!this.A02) {
            this.A02 = true;
            C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r2 = r3.A0Y;
            ((WaListFragment) this).A00 = (AnonymousClass182) r2.A94.get();
            ((WaListFragment) this).A01 = (AnonymousClass180) r2.ALt.get();
            this.A1A = (C14830m7) r2.ALb.get();
            this.A1Y = (C14850m9) r2.A04.get();
            ((ConversationsFragment) this).A0L = (C14900mE) r2.A8X.get();
            this.A2A = (C252718t) r2.A9K.get();
            this.A1g = (AnonymousClass13H) r2.ABY.get();
            this.A20 = (C253018w) r2.AJS.get();
            ((ConversationsFragment) this).A0M = (C15570nT) r2.AAr.get();
            this.A1B = (C16590pI) r2.AMg.get();
            this.A2B = (AbstractC14440lR) r2.ANe.get();
            ((ConversationsFragment) this).A0H = (AbstractC15710nm) r2.A4o.get();
            this.A1G = (C19990v2) r2.A3M.get();
            this.A21 = (AnonymousClass12U) r2.AJd.get();
            this.A1a = (C16120oU) r2.ANE.get();
            this.A2C = (C14860mA) r2.ANU.get();
            ((ConversationsFragment) this).A0E = (AnonymousClass17S) r2.A0F.get();
            this.A1F = (C20650w6) r2.A3A.get();
            this.A1X = (AnonymousClass19M) r2.A6R.get();
            ((ConversationsFragment) this).A0N = (C15450nH) r2.AII.get();
            ((ConversationsFragment) this).A0g = (C18850tA) r2.AKx.get();
            ((ConversationsFragment) this).A0P = (AnonymousClass1AR) r2.ALM.get();
            ((ConversationsFragment) this).A0Q = (C16170oZ) r2.AM4.get();
            ((ConversationsFragment) this).A0F = (AnonymousClass12P) r2.A0H.get();
            this.A0o = (C21270x9) r2.A4A.get();
            this.A1P = (C20040v7) r2.AAK.get();
            this.A1h = (C17220qS) r2.ABt.get();
            this.A1p = (AnonymousClass14X) r2.AFM.get();
            this.A1u = (AnonymousClass17R) r2.AIz.get();
            ((ConversationsFragment) this).A0i = (AnonymousClass130) r2.A41.get();
            ((ConversationsFragment) this).A0j = (C15550nR) r2.A45.get();
            ((ConversationsFragment) this).A0G = (C253519b) r2.A4C.get();
            this.A1K = (C241714m) r2.A4l.get();
            this.A18 = (AnonymousClass01d) r2.ALI.get();
            ((ConversationsFragment) this).A0l = (C15610nY) r2.AMe.get();
            this.A1j = (C22230yk) r2.ANT.get();
            this.A1E = (AnonymousClass018) r2.ANb.get();
            this.A1o = (C17070qD) r2.AFC.get();
            this.A1q = (C21190x1) r2.A3G.get();
            this.A1L = (C15240mn) r2.A8F.get();
            this.A28 = (C21300xC) r2.A0c.get();
            ((ConversationsFragment) this).A0Z = (C238013b) r2.A1Z.get();
            ((ConversationsFragment) this).A0d = (C243915i) r2.A37.get();
            ((ConversationsFragment) this).A0k = (AnonymousClass10S) r2.A46.get();
            this.A1b = (C20710wC) r2.A8m.get();
            this.A0p = (C21240x6) r2.AA7.get();
            this.A1Q = (AnonymousClass10Y) r2.AAR.get();
            this.A1z = (AnonymousClass12F) r2.AJM.get();
            this.A26 = (AnonymousClass198) r2.A0J.get();
            AnonymousClass2FL r1 = r3.A0V;
            this.A14 = r1.A05();
            this.A1t = (C15860o1) r2.A3H.get();
            this.A1Z = (C22050yP) r2.A7v.get();
            this.A1y = (C240514a) r2.AJL.get();
            this.A1i = (C14840m8) r2.ACi.get();
            this.A1l = (C22630zO) r2.AD7.get();
            this.A19 = (C15410nB) r2.ALJ.get();
            this.A25 = (AnonymousClass12O) r2.AMG.get();
            ((ConversationsFragment) this).A0R = (C18330sH) r2.AN4.get();
            this.A27 = (C254219i) r2.A0K.get();
            ((ConversationsFragment) this).A0c = (C22330yu) r2.A3I.get();
            this.A0m = (AnonymousClass10T) r2.A47.get();
            this.A0r = (C20730wE) r2.A4J.get();
            this.A1T = (AnonymousClass132) r2.ALx.get();
            ((ConversationsFragment) this).A0h = (AnonymousClass116) r2.A3z.get();
            ((ConversationsFragment) this).A0a = (C251118d) r2.A2D.get();
            this.A1k = (C20220vP) r2.AC3.get();
            this.A1R = (C16490p7) r2.ACJ.get();
            this.A1W = (C245716a) r2.AJR.get();
            this.A1S = (C242114q) r2.AJq.get();
            this.A1C = (C15890o4) r2.AN1.get();
            this.A1U = (C21400xM) r2.ABd.get();
            this.A1D = (C14820m6) r2.AN3.get();
            ((ConversationsFragment) this).A0Y = (C246716k) r2.A2X.get();
            ((ConversationsFragment) this).A0e = (C22640zP) r2.A3Z.get();
            this.A1J = (C15680nj) r2.A4e.get();
            this.A1O = (C236812p) r2.AAC.get();
            this.A1x = (AnonymousClass1AM) r2.AJG.get();
            this.A1I = (C21320xE) r2.A4Y.get();
            this.A1N = (AnonymousClass1C5) r2.A9N.get();
            this.A1m = (AnonymousClass11H) r2.AEp.get();
            this.A1n = (C22710zW) r2.AF7.get();
            ((ConversationsFragment) this).A0V = r3.A00();
            ((ConversationsFragment) this).A0W = (C14650lo) r2.A2V.get();
            this.A1V = (C22100yW) r2.A3g.get();
            this.A0s = (C250417w) r2.A4b.get();
            this.A1r = (AnonymousClass185) r2.AHy.get();
            this.A24 = (AnonymousClass1CT) r2.AMB.get();
            ((ConversationsFragment) this).A0X = (AnonymousClass10A) r2.A2W.get();
            this.A29 = (C255719x) r2.A5X.get();
            this.A1M = (C15600nX) r2.A8x.get();
            this.A17 = (C18640sm) r2.A3u.get();
            ((ConversationsFragment) this).A0O = (AnonymousClass11L) r2.ALG.get();
            ((ConversationsFragment) this).A0f = (C237412v) r2.A3k.get();
            this.A0q = (C17010q7) r2.A43.get();
            this.A1H = (C20830wO) r2.A4W.get();
            this.A1c = (C244215l) r2.A8y.get();
            this.A1v = (AnonymousClass161) r2.AJ4.get();
            ((ConversationsFragment) this).A0S = (C22730zY) r2.A8Y.get();
            ((ConversationsFragment) this).A0U = (AnonymousClass10K) r2.A8c.get();
            this.A1w = (AnonymousClass1AQ) r2.AJ7.get();
            ((ConversationsFragment) this).A0b = (C16430p0) r2.A5y.get();
            ((ConversationsFragment) this).A0T = (AnonymousClass10J) r2.A8b.get();
            this.A1d = (AnonymousClass139) r2.A9o.get();
            this.A0t = (AnonymousClass11P) r2.ABp.get();
            this.A2D = C18000rk.A00(r2.ADw);
            this.A2E = C18000rk.A00(r2.AID);
            ((ConversationsFragment) this).A0I = (C48962Ip) r1.A00.get();
        }
    }

    public final void A1T() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }
}
