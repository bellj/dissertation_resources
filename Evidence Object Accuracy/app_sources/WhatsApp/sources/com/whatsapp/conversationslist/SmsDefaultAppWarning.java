package com.whatsapp.conversationslist;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1AR;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C36021jC;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.conversationslist.SmsDefaultAppWarning;

/* loaded from: classes2.dex */
public class SmsDefaultAppWarning extends ActivityC13790kL {
    public AnonymousClass1AR A00;
    public boolean A01;

    public SmsDefaultAppWarning() {
        this(0);
    }

    public SmsDefaultAppWarning(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 63);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (AnonymousClass1AR) A1M.ALM.get();
        }
    }

    public final void A2e() {
        this.A00.A00(this, getIntent().getData(), 17, C12960it.A0X(this, "https://whatsapp.com/dl/", C12970iu.A1b(), 0, R.string.tell_a_friend_sms));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        ActivityInfo activityInfo;
        super.onCreate(bundle);
        Intent A0E = C12990iw.A0E("android.intent.action.SENDTO");
        A0E.setData(getIntent().getData());
        ResolveInfo resolveActivity = getPackageManager().resolveActivity(A0E, 0);
        if (resolveActivity == null || (activityInfo = resolveActivity.activityInfo) == null || !"com.whatsapp".equals(activityInfo.packageName)) {
            C36021jC.A01(this, 1);
        } else {
            C36021jC.A01(this, 0);
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 0) {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.warning_sms_default_app);
            A0S.A00(R.string.sms_invite, new IDxCListenerShape9S0100000_2_I1(this, 34));
            C12970iu.A1K(A0S, this, 33, R.string.sms_reset);
            C12970iu.A1M(A0S, this, 21, R.string.sms_sms);
            A0S.A08(new DialogInterface.OnCancelListener() { // from class: X.4f4
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    SmsDefaultAppWarning.this.finish();
                }
            });
            return A0S.create();
        } else if (i != 1) {
            return super.onCreateDialog(i);
        } else {
            C004802e A0S2 = C12980iv.A0S(this);
            A0S2.A06(R.string.warning_sms);
            A0S2.A00(R.string.sms_invite, new IDxCListenerShape9S0100000_2_I1(this, 32));
            C12970iu.A1M(A0S2, this, 20, R.string.sms_sms);
            A0S2.A08(new DialogInterface.OnCancelListener() { // from class: X.4f3
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    SmsDefaultAppWarning.this.finish();
                }
            });
            return A0S2.create();
        }
    }
}
