package com.whatsapp.phonematching;

import X.AbstractC115375Rh;
import X.ActivityC13790kL;
import X.AnonymousClass009;
import X.AnonymousClass12P;
import X.AnonymousClass2SS;
import X.C15570nT;
import X.C18350sJ;
import X.C20660w7;
import X.C69513Zn;
import X.HandlerC52082aD;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/* loaded from: classes2.dex */
public class MatchPhoneNumberFragment extends Hilt_MatchPhoneNumberFragment {
    public C15570nT A00;
    public ActivityC13790kL A01;
    public C20660w7 A02;
    public HandlerC52082aD A03;
    public C18350sJ A04;
    public final AnonymousClass2SS A05 = new C69513Zn(this);

    public static void A00(ActivityC13790kL r1) {
        DialogFragment dialogFragment = (DialogFragment) r1.A0V().A0A("PROGRESS");
        if (dialogFragment != null) {
            dialogFragment.A1C();
        }
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        C18350sJ r0 = this.A04;
        r0.A0p.remove(this.A05);
        this.A03.removeMessages(4);
        ((CountryAndPhoneNumberFragment) this).A0C = null;
        super.A11();
    }

    @Override // com.whatsapp.phonematching.Hilt_MatchPhoneNumberFragment, com.whatsapp.phonematching.CountryAndPhoneNumberFragment, com.whatsapp.phonematching.Hilt_CountryAndPhoneNumberFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        ActivityC13790kL r0 = (ActivityC13790kL) AnonymousClass12P.A01(context, ActivityC13790kL.class);
        this.A01 = r0;
        AnonymousClass009.A0A("activity needs to implement PhoneNumberMatchingCallback", r0 instanceof AbstractC115375Rh);
        ActivityC13790kL r2 = this.A01;
        AbstractC115375Rh r1 = (AbstractC115375Rh) r2;
        if (this.A03 == null) {
            this.A03 = new HandlerC52082aD(r2, r1);
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        C18350sJ r0 = this.A04;
        r0.A0p.add(this.A05);
        ((CountryAndPhoneNumberFragment) this).A0C = this;
    }
}
