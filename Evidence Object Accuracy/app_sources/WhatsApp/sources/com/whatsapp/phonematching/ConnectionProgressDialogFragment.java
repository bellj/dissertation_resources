package com.whatsapp.phonematching;

import X.AnonymousClass01F;
import X.C004902f;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConnectionProgressDialogFragment extends Hilt_ConnectionProgressDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ProgressDialog progressDialog = new ProgressDialog(A0B());
        progressDialog.setMessage(A0I(R.string.register_connecting));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }
}
