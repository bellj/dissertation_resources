package com.whatsapp.phonematching;

import X.ActivityC13790kL;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass23M;
import X.AnonymousClass2GF;
import X.AnonymousClass2x2;
import X.C12960it;
import X.C22650zQ;
import X.C22680zT;
import X.C42941w9;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.PhoneNumberEntry;
import com.whatsapp.util.Log;
import java.io.IOException;

/* loaded from: classes2.dex */
public class CountryAndPhoneNumberFragment extends Hilt_CountryAndPhoneNumberFragment {
    public int A00;
    public int A01;
    public EditText A02;
    public EditText A03;
    public TextView A04;
    public TextView A05;
    public TextView A06;
    public C22680zT A07;
    public ActivityC13790kL A08;
    public PhoneNumberEntry A09;
    public AnonymousClass01d A0A;
    public AnonymousClass018 A0B;
    public MatchPhoneNumberFragment A0C;
    public C22650zQ A0D;
    public String A0E = null;
    public String A0F;

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        this.A01 = AnonymousClass23M.A00(this.A03);
        this.A00 = AnonymousClass23M.A00(this.A02);
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 0 && i2 == -1 && intent != null) {
            this.A0E = intent.getStringExtra("cc");
            this.A0F = intent.getStringExtra("iso");
            String stringExtra = intent.getStringExtra("country_name");
            this.A02.setText(this.A0E);
            this.A06.setText(stringExtra);
            this.A09.A02(this.A0F);
            if (this.A01 == -1) {
                this.A01 = Integer.MAX_VALUE;
            }
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.country_and_phone_number);
        this.A09 = (PhoneNumberEntry) A0F.findViewById(R.id.phone_number_entry);
        this.A06 = C12960it.A0J(A0F, R.id.registration_country);
        this.A04 = C12960it.A0J(A0F, R.id.registration_country_error_view);
        this.A05 = C12960it.A0J(A0F, R.id.registration_country_label);
        PhoneNumberEntry phoneNumberEntry = this.A09;
        this.A02 = phoneNumberEntry.A02;
        this.A03 = phoneNumberEntry.A03;
        phoneNumberEntry.A04 = new AnonymousClass2x2(this);
        TelephonyManager A0N = this.A0A.A0N();
        if (A0N == null) {
            Log.w("CountryAndPhoneNumberFragment tm=null");
        } else {
            String simCountryIso = A0N.getSimCountryIso();
            if (simCountryIso != null) {
                try {
                    this.A0E = this.A07.A04(simCountryIso);
                } catch (IOException e) {
                    Log.e("CountryAndPhoneNumberFragment/iso/code failed to get code from CountryPhoneInfo", e);
                }
            }
        }
        Drawable A04 = AnonymousClass00T.A04(this.A08, R.drawable.abc_spinner_textfield_background_material);
        boolean z = C42941w9.A01;
        TextView textView = this.A06;
        if (z) {
            textView.setBackground(A04);
        } else {
            textView.setBackground(new AnonymousClass2GF(A04, this.A0B));
        }
        C42941w9.A03(this.A03);
        if (Build.VERSION.SDK_INT < 21) {
            this.A06.getBackground().setColorFilter(AnonymousClass00T.A00(this.A08, R.color.settings_delete_account_spinner_tint), PorterDuff.Mode.SRC_IN);
        }
        C12960it.A12(this.A06, this, 4);
        this.A03.requestFocus();
        this.A01 = AnonymousClass23M.A00(this.A03);
        this.A00 = AnonymousClass23M.A00(this.A02);
        String str = this.A0E;
        if (str != null) {
            this.A02.setText(str);
        }
        if (!TextUtils.isEmpty(this.A0F)) {
            Log.i(C12960it.A0d(this.A0F, C12960it.A0k("CountryAndPhoneNumberFragment/country: ")));
            this.A09.A02(this.A0F);
        }
        return A0F;
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        String str = this.A0E;
        if (str != null) {
            this.A02.setText(str);
        }
        String str2 = this.A0F;
        if (str2 != null) {
            this.A06.setText(this.A0D.A02(this.A0B, str2));
        }
        AnonymousClass23M.A0I(this.A02, this.A00);
        AnonymousClass23M.A0I(this.A03, this.A01);
        this.A03.clearFocus();
    }

    @Override // com.whatsapp.phonematching.Hilt_CountryAndPhoneNumberFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A08 = (ActivityC13790kL) AnonymousClass12P.A01(context, ActivityC13790kL.class);
    }
}
