package com.whatsapp.phonematching;

import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.C103804rL;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C22650zQ;
import X.C22680zT;
import X.C28141Kv;
import X.C48232Fc;
import X.C52712bY;
import X.C58032o1;
import X.C73143fk;
import X.C83303x1;
import android.animation.Animator;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CountryPicker extends ActivityC13770kJ {
    public static final int A07;
    public static final int A08;
    public View A00;
    public SearchView A01;
    public Toolbar A02;
    public C22680zT A03;
    public C52712bY A04;
    public C22650zQ A05;
    public boolean A06;

    static {
        int i = Build.VERSION.SDK_INT;
        int i2 = 250;
        int i3 = 220;
        if (i >= 21) {
            i3 = 250;
        }
        A07 = i3;
        if (i < 21) {
            i2 = 220;
        }
        A08 = i2;
    }

    public CountryPicker() {
        this(0);
    }

    public CountryPicker(int i) {
        this.A06 = false;
        ActivityC13830kP.A1P(this, 89);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A05 = (C22650zQ) A1M.A4n.get();
            this.A03 = (C22680zT) A1M.AGW.get();
        }
    }

    public final void A2g() {
        int width;
        if (A2h()) {
            this.A01.A0F("");
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            long j = (long) A08;
            alphaAnimation.setDuration(j);
            this.A02.startAnimation(alphaAnimation);
            if (Build.VERSION.SDK_INT >= 21) {
                int width2 = (this.A00.getWidth() - getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)) - ((getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * 3) >> 1);
                View view = this.A00;
                if (C28141Kv.A01(((ActivityC13830kP) this).A01)) {
                    width = width2;
                } else {
                    width = this.A00.getWidth() - width2;
                }
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, width, C13000ix.A00(this.A00), (float) width2, 0.0f);
                createCircularReveal.setDuration(j);
                createCircularReveal.addListener(new IDxLAdapterShape0S0100000_1_I1(this, 3));
                createCircularReveal.start();
                return;
            }
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-this.A00.getHeight()));
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(alphaAnimation2);
            animationSet.addAnimation(translateAnimation);
            animationSet.setDuration(j);
            animationSet.setAnimationListener(new C58032o1(this));
            this.A00.startAnimation(animationSet);
        }
    }

    public final boolean A2h() {
        StringBuilder A0k = C12960it.A0k("Visible");
        A0k.append(C12960it.A1T(this.A00.getVisibility()));
        C12960it.A1F(A0k);
        if (this.A00.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (A2h()) {
            A2g();
        } else {
            super.onBackPressed();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d6, code lost:
        if (r2.A01(r1) == false) goto L_0x00d8;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
        // Method dump skipped, instructions count: 383
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.phonematching.CountryPicker.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(AnonymousClass2GE.A03(this, C12970iu.A0C(this, R.drawable.ic_action_search_teal), R.color.lightActionBarItemDrawableTint)).setShowAsAction(2);
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int width;
        StringBuilder A0k = C12960it.A0k("item.getItemId()");
        A0k.append(menuItem.getItemId());
        A0k.append(C12960it.A1V(menuItem.getItemId(), R.id.menuitem_search));
        C12960it.A1F(A0k);
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            super.onBackPressed();
        } else if (itemId == R.id.menuitem_search) {
            if (!A2h()) {
                if (this.A01 == null) {
                    C48232Fc.A00(this.A00);
                    getLayoutInflater().inflate(R.layout.home_search_view_layout, (ViewGroup) this.A00, true);
                    SearchView searchView = (SearchView) this.A00.findViewById(R.id.search_view);
                    this.A01 = searchView;
                    TextView A0J = C12960it.A0J(searchView, R.id.search_src_text);
                    C12960it.A0s(this, A0J, R.color.body_gray);
                    A0J.setHintTextColor(AnonymousClass00T.A00(this, R.color.body_light_gray));
                    this.A01.setIconifiedByDefault(false);
                    this.A01.setQueryHint(getString(R.string.search_country_hint));
                    SearchView searchView2 = this.A01;
                    searchView2.A0B = new C103804rL(this);
                    C12970iu.A0L(searchView2, R.id.search_mag_icon).setImageDrawable(new C73143fk(AnonymousClass00T.A04(this, R.drawable.ic_back), this));
                    ImageView A0L = C12970iu.A0L(this.A01, R.id.search_close_btn);
                    if (A0L != null) {
                        A0L.setImageResource(R.drawable.ic_backup_cancel);
                    }
                    ImageView A0L2 = C12970iu.A0L(this.A00, R.id.search_back);
                    A0L2.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), ((ActivityC13830kP) this).A01));
                    C12960it.A12(A0L2, this, 5);
                    this.A01.setMaxWidth(Integer.MAX_VALUE);
                }
                this.A02.setVisibility(8);
                this.A00.setVisibility(0);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration((long) A08);
                alphaAnimation.setAnimationListener(new C83303x1(this));
                this.A02.startAnimation(alphaAnimation);
                if (Build.VERSION.SDK_INT < 21) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-this.A02.getHeight()), 0.0f);
                    translateAnimation.setDuration((long) A07);
                    this.A00.clearAnimation();
                    this.A00.startAnimation(translateAnimation);
                    return true;
                } else if (this.A00.isAttachedToWindow()) {
                    int width2 = (this.A02.getWidth() - getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)) - ((getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * 3) >> 1);
                    View view = this.A00;
                    if (C28141Kv.A01(((ActivityC13830kP) this).A01)) {
                        width = width2;
                    } else {
                        width = this.A02.getWidth() - width2;
                    }
                    Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, width, C13000ix.A00(this.A02), 0.0f, (float) width2);
                    createCircularReveal.setDuration((long) A07);
                    createCircularReveal.start();
                    Log.i("Detach");
                }
            }
            return true;
        }
        return false;
    }
}
