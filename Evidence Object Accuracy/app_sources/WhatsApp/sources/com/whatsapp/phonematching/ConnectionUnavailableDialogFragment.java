package com.whatsapp.phonematching;

import X.AbstractC14440lR;
import X.ActivityC000900k;
import X.AnonymousClass01F;
import X.AnonymousClass01d;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.C004802e;
import X.C004902f;
import X.C12970iu;
import X.C13010iy;
import X.C15890o4;
import X.C18640sm;
import X.C20800wL;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class ConnectionUnavailableDialogFragment extends Hilt_ConnectionUnavailableDialogFragment {
    public AnonymousClass19Y A00;
    public C18640sm A01;
    public AnonymousClass01d A02;
    public C15890o4 A03;
    public AnonymousClass11G A04;
    public C20800wL A05;
    public AbstractC14440lR A06;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A01 = C13010iy.A01(this);
        C004802e r3 = new C004802e(A01);
        r3.A06(R.string.register_try_again_later);
        r3.setPositiveButton(R.string.check_system_status, new IDxCListenerShape4S0200000_2_I1(A01, 11, this));
        C12970iu.A1K(r3, this, 48, R.string.cancel);
        return r3.create();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }
}
