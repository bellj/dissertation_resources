package com.whatsapp.contextualhelp;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.C12970iu;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.WaInAppBrowsingActivity;

/* loaded from: classes2.dex */
public class ContextualHelpActivity extends WaInAppBrowsingActivity {
    public boolean A00;

    public ContextualHelpActivity() {
        this(0);
    }

    public ContextualHelpActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 58);
    }

    @Override // X.AbstractActivityC58452pV, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13790kL.A0f(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contextual_help_menu, menu);
        MenuItem findItem = menu.findItem(R.id.menu_more);
        findItem.setIcon(AnonymousClass2GE.A04(findItem.getIcon(), getResources().getColor(R.color.dark_gray)));
        return true;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_open_in_browser) {
            return false;
        }
        startActivity(C12970iu.A0B(Uri.parse(getIntent().getStringExtra("webview_url"))));
        return true;
    }
}
