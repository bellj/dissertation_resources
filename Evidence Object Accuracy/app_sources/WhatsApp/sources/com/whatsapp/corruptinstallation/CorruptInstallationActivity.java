package com.whatsapp.corruptinstallation;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass11G;
import X.AnonymousClass19Y;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C21740xu;
import X.C73503gL;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CorruptInstallationActivity extends ActivityC13790kL {
    public AnonymousClass19Y A00;
    public C21740xu A01;
    public AnonymousClass11G A02;
    public boolean A03;

    public CorruptInstallationActivity() {
        this(0);
    }

    public CorruptInstallationActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 64);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = (C21740xu) A1M.AM1.get();
            this.A00 = (AnonymousClass19Y) A1M.AI6.get();
            this.A02 = (AnonymousClass11G) A1M.AKq.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_corrupt_installation);
        TextView A0M = C12970iu.A0M(this, R.id.corrupt_installation_contact_support_textview);
        Spanned fromHtml = Html.fromHtml(getString(R.string.corrupt_installation_contact_support_prompt));
        SpannableStringBuilder A0J = C12990iw.A0J(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if ("contact-support".equals(uRLSpan.getURL())) {
                    Log.i("contact-support link found");
                    int spanStart = A0J.getSpanStart(uRLSpan);
                    int spanEnd = A0J.getSpanEnd(uRLSpan);
                    int spanFlags = A0J.getSpanFlags(uRLSpan);
                    A0J.removeSpan(uRLSpan);
                    A0J.setSpan(new C73503gL(this.A00.A00(this, null, null, null, "corrupt-install", null, null, null, false)), spanStart, spanEnd, spanFlags);
                }
            }
        }
        A0M.setText(A0J);
        C12990iw.A1F(A0M);
        TextView A0M2 = C12970iu.A0M(this, R.id.corrupt_installation_description_website_distribution_textview);
        C12990iw.A1F(A0M2);
        A0M2.setText(Html.fromHtml(C12960it.A0X(this, "https://www.whatsapp.com/android/", C12970iu.A1b(), 0, R.string.corrupt_installation_description_website_distribution)));
        C12970iu.A1N(this, R.id.play_store_div, 8);
    }
}
