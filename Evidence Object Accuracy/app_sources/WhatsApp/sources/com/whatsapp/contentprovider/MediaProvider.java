package com.whatsapp.contentprovider;

import X.AbstractC15420nE;
import X.AbstractC16130oV;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.C14330lG;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15670ni;
import X.C15680nj;
import X.C15690nk;
import X.C16150oX;
import X.C16170oZ;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

/* loaded from: classes2.dex */
public class MediaProvider extends AbstractC15420nE {
    public static UriMatcher A0A;
    public static final String A0B;
    public static final String A0C;
    public static final String A0D;
    public static final String[] A0E = {"_display_name", "_size"};
    public C14330lG A00;
    public C15550nR A01;
    public C15610nY A02;
    public C14830m7 A03;
    public AnonymousClass018 A04;
    public C15680nj A05;
    public C15650ng A06;
    public C15660nh A07;
    public C15670ni A08;
    public C15690nk A09;

    static {
        StringBuilder sb = new StringBuilder("com.whatsapp");
        sb.append(".provider.media");
        A0B = sb.toString();
        StringBuilder sb2 = new StringBuilder("vnd.android.cursor.dir/vnd.");
        sb2.append("com.whatsapp");
        sb2.append(".provider.media.buckets");
        A0C = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("vnd.android.cursor.dir/vnd.");
        sb3.append("com.whatsapp");
        sb3.append(".provider.media.items");
        A0D = sb3.toString();
    }

    public static int A01(String str) {
        if ("r".equals(str)) {
            return 268435456;
        }
        if ("w".equals(str) || "wt".equals(str)) {
            return 738197504;
        }
        if ("wa".equals(str)) {
            return 704643072;
        }
        if ("rw".equals(str)) {
            return 939524096;
        }
        if ("rwt".equals(str)) {
            return 1006632960;
        }
        StringBuilder sb = new StringBuilder("Invalid mode: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }

    public static synchronized UriMatcher A02() {
        UriMatcher uriMatcher;
        synchronized (MediaProvider.class) {
            if (A0A == null) {
                UriMatcher uriMatcher2 = new UriMatcher(-1);
                A0A = uriMatcher2;
                String str = A0B;
                uriMatcher2.addURI(str, "buckets", 1);
                A0A.addURI(str, "items", 2);
                A0A.addURI(str, "item/*", 3);
                A0A.addURI(str, "gdpr_report", 4);
                A0A.addURI(str, "personal_dyi_report", 6);
                A0A.addURI(str, "business_dyi_report", 11);
                A0A.addURI(str, "business_activity_report", 7);
                A0A.addURI(str, "export_chat/*/*", 5);
                A0A.addURI(str, "thumbnail/*", 8);
                A0A.addURI(str, "export/*", 9);
                A0A.addURI(str, "support", 10);
            }
            uriMatcher = A0A;
        }
        return uriMatcher;
    }

    public static Uri A03(C15670ni r4, long j) {
        String obj = UUID.randomUUID().toString();
        r4.A01(obj, Long.toString(j), "image/jpeg", "");
        return new Uri.Builder().scheme("content").authority(A0B).appendPath("thumbnail").appendEncodedPath(obj).build();
    }

    public static Uri A04(C15670ni r4, AbstractC16130oV r5) {
        int i = Build.VERSION.SDK_INT;
        C16150oX r0 = r5.A02;
        AnonymousClass009.A05(r0);
        if (i < 21) {
            return Uri.fromFile(r0.A0F);
        }
        File file = r0.A0F;
        AnonymousClass009.A05(file);
        String obj = UUID.randomUUID().toString();
        r4.A01(obj, file.getAbsolutePath(), C16170oZ.A00(r5), file.getName());
        return new Uri.Builder().scheme("content").authority(A0B).appendPath("item").appendEncodedPath(obj).build();
    }

    public static Uri A05(String str, String str2) {
        return new Uri.Builder().scheme("content").authority(A0B).appendPath(str).appendQueryParameter("id", str2).build();
    }

    public static final String A06(Uri uri) {
        String queryParameter = uri.getQueryParameter("id");
        if (queryParameter != null) {
            return queryParameter;
        }
        StringBuilder sb = new StringBuilder("Unknown URI ");
        sb.append(uri);
        throw new IllegalArgumentException(sb.toString());
    }

    public final Cursor A07(Uri uri, File file, String str, String[] strArr) {
        int i;
        try {
            A08(uri, file);
            if (strArr == null) {
                strArr = A0E;
            }
            int length = strArr.length;
            String[] strArr2 = new String[length];
            Object[] objArr = new Object[length];
            int i2 = 0;
            for (String str2 : strArr) {
                if ("_display_name".equals(str2)) {
                    strArr2[i2] = "_display_name";
                    i = i2 + 1;
                    objArr[i2] = str;
                } else if ("_size".equals(str2)) {
                    strArr2[i2] = "_size";
                    i = i2 + 1;
                    objArr[i2] = Long.valueOf(file.length());
                }
                i2 = i;
            }
            String[] strArr3 = new String[i2];
            System.arraycopy(strArr2, 0, strArr3, 0, i2);
            Object[] objArr2 = new Object[i2];
            System.arraycopy(objArr, 0, objArr2, 0, i2);
            MatrixCursor matrixCursor = new MatrixCursor(strArr3, 1);
            matrixCursor.addRow(objArr2);
            return matrixCursor;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public final void A08(Uri uri, File file) {
        if (!file.exists()) {
            StringBuilder sb = new StringBuilder("File not found for uri: ");
            sb.append(uri);
            throw new FileNotFoundException(sb.toString());
        } else if (file.lastModified() < this.A03.A00() - 3600000) {
            file.delete();
            StringBuilder sb2 = new StringBuilder("File expired for uri: ");
            sb2.append(uri);
            throw new FileNotFoundException(sb2.toString());
        }
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        A01();
        throw new UnsupportedOperationException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0067, code lost:
        if (r1 != null) goto L_0x0069;
     */
    @Override // android.content.ContentProvider
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getType(android.net.Uri r8) {
        /*
            r7 = this;
            r7.A01()
            android.content.UriMatcher r0 = A02()
            int r0 = r0.match(r8)
            switch(r0) {
                case 1: goto L_0x007f;
                case 2: goto L_0x007c;
                case 3: goto L_0x0029;
                case 4: goto L_0x0026;
                case 5: goto L_0x0022;
                case 6: goto L_0x0026;
                case 7: goto L_0x0026;
                case 8: goto L_0x0074;
                case 9: goto L_0x0026;
                case 10: goto L_0x0026;
                case 11: goto L_0x0026;
                default: goto L_0x000e;
            }
        L_0x000e:
            java.lang.String r1 = "Unknown URI "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r8)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0022:
            java.lang.String r0 = "text/plain"
            return r0
        L_0x0026:
            java.lang.String r0 = "application/zip"
            return r0
        L_0x0029:
            X.0ni r0 = r7.A08
            java.lang.String r1 = r8.getLastPathSegment()
            X.0oj r0 = r0.A00
            X.0on r5 = r0.get()
            X.0op r6 = r5.A03     // Catch: all -> 0x0077
            java.lang.String r4 = "SELECT mime_type FROM shared_media_ids WHERE item_uuid =?AND expiration_timestamp >?"
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch: all -> 0x0077
            r0 = 0
            r3[r0] = r1     // Catch: all -> 0x0077
            r2 = 1
            long r0 = java.lang.System.currentTimeMillis()     // Catch: all -> 0x0077
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x0077
            r3[r2] = r0     // Catch: all -> 0x0077
            android.database.Cursor r1 = r6.A09(r4, r3)     // Catch: all -> 0x0077
            if (r1 == 0) goto L_0x0066
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0061
            if (r0 == 0) goto L_0x0066
            java.lang.String r0 = "mime_type"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0061
            java.lang.String r0 = r1.getString(r0)     // Catch: all -> 0x0061
            goto L_0x0069
        L_0x0061:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0065
        L_0x0065:
            throw r0     // Catch: all -> 0x0077
        L_0x0066:
            r0 = 0
            if (r1 == 0) goto L_0x006c
        L_0x0069:
            r1.close()     // Catch: all -> 0x0077
        L_0x006c:
            r5.close()
            if (r0 != 0) goto L_0x0076
            java.lang.String r0 = "application/octet-stream"
            return r0
        L_0x0074:
            java.lang.String r0 = "image/jpeg"
        L_0x0076:
            return r0
        L_0x0077:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x007b
        L_0x007b:
            throw r0
        L_0x007c:
            java.lang.String r0 = com.whatsapp.contentprovider.MediaProvider.A0D
            return r0
        L_0x007f:
            java.lang.String r0 = com.whatsapp.contentprovider.MediaProvider.A0C
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contentprovider.MediaProvider.getType(android.net.Uri):java.lang.String");
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        A01();
        throw new UnsupportedOperationException();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    @Override // android.content.ContentProvider
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.ParcelFileDescriptor openFile(android.net.Uri r8, java.lang.String r9) {
        /*
        // Method dump skipped, instructions count: 672
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contentprovider.MediaProvider.openFile(android.net.Uri, java.lang.String):android.os.ParcelFileDescriptor");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // android.content.ContentProvider
    public android.database.Cursor query(android.net.Uri r19, java.lang.String[] r20, java.lang.String r21, java.lang.String[] r22, java.lang.String r23) {
        /*
        // Method dump skipped, instructions count: 882
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.contentprovider.MediaProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        A01();
        throw new UnsupportedOperationException();
    }
}
