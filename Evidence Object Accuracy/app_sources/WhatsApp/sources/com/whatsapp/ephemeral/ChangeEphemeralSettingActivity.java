package com.whatsapp.ephemeral;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass11J;
import X.AnonymousClass12P;
import X.AnonymousClass15P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass5TY;
import X.C103074qA;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16170oZ;
import X.C17170qN;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20660w7;
import X.C20710wC;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22700zV;
import X.C238013b;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C36701kO;
import X.C49372Km;
import X.C614330i;
import X.RunnableC32531cJ;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ChangeEphemeralSettingActivity extends ActivityC13790kL {
    public int A00;
    public int A01;
    public int A02;
    public C16170oZ A03;
    public C238013b A04;
    public AnonymousClass10S A05;
    public C22700zV A06;
    public AnonymousClass11J A07;
    public C17170qN A08;
    public C21320xE A09;
    public C15600nX A0A;
    public AnonymousClass15P A0B;
    public C16120oU A0C;
    public C20710wC A0D;
    public AbstractC14640lm A0E;
    public C20660w7 A0F;
    public C252018m A0G;
    public C14860mA A0H;
    public boolean A0I;
    public final C27131Gd A0J;

    public ChangeEphemeralSettingActivity() {
        this(0);
        this.A0J = new C36701kO(this);
    }

    public ChangeEphemeralSettingActivity(int i) {
        this.A0I = false;
        A0R(new C103074qA(this));
    }

    public static void A02(ActivityC13810kN r5, C238013b r6, C22700zV r7, UserJid userJid, int i, int i2) {
        if (!r7.A02(userJid)) {
            Intent A0E = C14960mK.A0E(r5, userJid, i, i2);
            if (r6.A0I(userJid)) {
                int i3 = R.string.ephemeral_unblock_to_turn_setting_on;
                if (i > 0) {
                    i3 = R.string.ephemeral_unblock_to_turn_setting_off;
                }
                r5.Adm(UnblockDialogFragment.A00(new AnonymousClass5TY(r5, A0E, r6, userJid) { // from class: X.544
                    public final /* synthetic */ Activity A00;
                    public final /* synthetic */ Intent A01;
                    public final /* synthetic */ C238013b A02;
                    public final /* synthetic */ UserJid A03;

                    {
                        this.A00 = r1;
                        this.A02 = r3;
                        this.A01 = r2;
                        this.A03 = r4;
                    }

                    @Override // X.AnonymousClass5TY
                    public final void AfB() {
                        Activity activity = this.A00;
                        C238013b r4 = this.A02;
                        Intent intent = this.A01;
                        r4.A09(activity, 
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: INVOKE  
                              (r4v0 'r4' X.13b)
                              (r5v0 'activity' android.app.Activity)
                              (wrap: X.53z : 0x000e: CONSTRUCTOR  (r0v0 X.53z A[REMOVE]) = 
                              (r3v0 'intent' android.content.Intent)
                              (wrap: java.lang.ref.WeakReference : 0x0008: INVOKE  (r1v0 java.lang.ref.WeakReference A[REMOVE]) = (r5v0 'activity' android.app.Activity) type: STATIC call: X.0iu.A10(java.lang.Object):java.lang.ref.WeakReference)
                             call: X.53z.<init>(android.content.Intent, java.lang.ref.WeakReference):void type: CONSTRUCTOR)
                              (wrap: com.whatsapp.jid.UserJid : 0x0006: IGET  (r2v0 com.whatsapp.jid.UserJid A[REMOVE]) = (r6v0 'this' X.544 A[IMMUTABLE_TYPE, THIS]) X.544.A03 com.whatsapp.jid.UserJid)
                             type: VIRTUAL call: X.13b.A09(android.app.Activity, X.1P3, com.whatsapp.jid.UserJid):void in method: X.544.AfB():void, file: classes3.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: CONSTRUCTOR  (r0v0 X.53z A[REMOVE]) = 
                              (r3v0 'intent' android.content.Intent)
                              (wrap: java.lang.ref.WeakReference : 0x0008: INVOKE  (r1v0 java.lang.ref.WeakReference A[REMOVE]) = (r5v0 'activity' android.app.Activity) type: STATIC call: X.0iu.A10(java.lang.Object):java.lang.ref.WeakReference)
                             call: X.53z.<init>(android.content.Intent, java.lang.ref.WeakReference):void type: CONSTRUCTOR in method: X.544.AfB():void, file: classes3.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 15 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.53z, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 21 more
                            */
                        /*
                            this = this;
                            android.app.Activity r5 = r6.A00
                            X.13b r4 = r6.A02
                            android.content.Intent r3 = r6.A01
                            com.whatsapp.jid.UserJid r2 = r6.A03
                            java.lang.ref.WeakReference r1 = X.C12970iu.A10(r5)
                            X.53z r0 = new X.53z
                            r0.<init>(r3, r1)
                            r4.A09(r5, r0, r2)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass544.AfB():void");
                    }
                }, r5.getString(i3), R.string.blocked_title, false));
                return;
            }
            r5.startActivity(A0E);
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0I) {
            this.A0I = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0C = (C16120oU) r1.ANE.get();
            this.A0H = (C14860mA) r1.ANU.get();
            this.A03 = (C16170oZ) r1.AM4.get();
            this.A0F = (C20660w7) r1.AIB.get();
            this.A0G = (C252018m) r1.A7g.get();
            this.A04 = (C238013b) r1.A1Z.get();
            this.A05 = (AnonymousClass10S) r1.A46.get();
            this.A0D = (C20710wC) r1.A8m.get();
            this.A06 = (C22700zV) r1.AMN.get();
            this.A09 = (C21320xE) r1.A4Y.get();
            this.A0A = (C15600nX) r1.A8x.get();
            this.A08 = (C17170qN) r1.AMt.get();
            this.A07 = (AnonymousClass11J) r1.A3m.get();
            this.A0B = (AnonymousClass15P) r1.A64.get();
        }
    }

    public final void A2e() {
        C14900mE r2;
        int i;
        int i2;
        long j;
        AbstractC14640lm r1 = this.A0E;
        AnonymousClass009.A05(r1);
        boolean z = r1 instanceof UserJid;
        if (!z || !this.A04.A0I((UserJid) r1)) {
            int i3 = this.A02;
            if (i3 != -1 && this.A01 != i3) {
                if (((ActivityC13810kN) this).A07.A0B()) {
                    AbstractC14640lm r5 = this.A0E;
                    if (C15380n4.A0K(r5)) {
                        C15580nU r52 = (C15580nU) r5;
                        i2 = this.A02;
                        C20660w7 r0 = this.A0F;
                        C14860mA r7 = this.A0H;
                        r0.A07(new RunnableC32531cJ(this.A09, this.A0D, r52, null, r7, null, null, 224), r52, i2);
                    } else if (z) {
                        i2 = this.A02;
                        this.A03.A0K((UserJid) r5, i2);
                    } else {
                        StringBuilder sb = new StringBuilder("Ephemeral not supported for this type of jid, type=");
                        sb.append(r5.getType());
                        Log.e(sb.toString());
                        return;
                    }
                    C614330i r3 = new C614330i();
                    r3.A02 = Long.valueOf((long) i2);
                    int i4 = this.A01;
                    if (i4 == -1) {
                        j = 0;
                    } else {
                        j = (long) i4;
                    }
                    r3.A03 = Long.valueOf(j);
                    int i5 = this.A00;
                    int i6 = 2;
                    if (i5 != 2) {
                        i6 = 3;
                        if (i5 != 3) {
                            i6 = 1;
                        }
                    }
                    r3.A00 = Integer.valueOf(i6);
                    AbstractC14640lm r22 = this.A0E;
                    if (C15380n4.A0K(r22)) {
                        C15600nX r12 = this.A0A;
                        C15580nU A02 = C15580nU.A02(r22);
                        AnonymousClass009.A05(A02);
                        r3.A01 = Integer.valueOf(C49372Km.A01(r12.A02(A02).A07()));
                    }
                    this.A0C.A07(r3);
                    return;
                }
                r2 = ((ActivityC13810kN) this).A05;
                i = R.string.ephemeral_setting_internet_needed;
            } else {
                return;
            }
        } else {
            r2 = ((ActivityC13810kN) this).A05;
            int i7 = this.A02;
            i = R.string.ephemeral_unblock_to_turn_setting_on;
            if (i7 == 0) {
                i = R.string.ephemeral_unblock_to_turn_setting_off;
            }
        }
        r2.A07(i, 1);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e();
        super.onBackPressed();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x009e, code lost:
        if ((r8 instanceof com.whatsapp.jid.UserJid) != false) goto L_0x00a0;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r20) {
        /*
        // Method dump skipped, instructions count: 531
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.ephemeral.ChangeEphemeralSettingActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A05.A04(this.A0J);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            A2e();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("selected_setting", this.A02);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        EphemeralNUXDialog.A00(A0V(), ((ActivityC13810kN) this).A09, true);
    }
}
