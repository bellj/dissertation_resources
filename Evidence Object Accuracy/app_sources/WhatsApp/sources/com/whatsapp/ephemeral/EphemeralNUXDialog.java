package com.whatsapp.ephemeral;

import X.AbstractC33131dQ;
import X.ActivityC000900k;
import X.ActivityC001000l;
import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass05I;
import X.AnonymousClass12P;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14820m6;
import X.C14850m9;
import X.C252018m;
import X.C93004Yo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.airbnb.lottie.LottieAnimationView;
import com.whatsapp.R;
import com.whatsapp.ephemeral.EphemeralNUXDialog;
import com.whatsapp.group.NewGroup;

/* loaded from: classes2.dex */
public class EphemeralNUXDialog extends Hilt_EphemeralNUXDialog {
    public View A00;
    public ScrollView A01;
    public AnonymousClass12P A02;
    public C14820m6 A03;
    public C14850m9 A04;
    public C252018m A05;

    public static void A00(AnonymousClass01F r4, C14820m6 r5, boolean z) {
        if (!r4.A0m() && (!C12980iv.A1W(r5.A00, "ephemeral_nux")) && r4.A0A("ephemeral_nux") == null) {
            EphemeralNUXDialog ephemeralNUXDialog = new EphemeralNUXDialog();
            Bundle A0D = C12970iu.A0D();
            A0D.putBoolean("from_settings", z);
            ephemeralNUXDialog.A0U(A0D);
            ephemeralNUXDialog.A1F(r4, "ephemeral_nux");
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (this.A03.A00.getBoolean("ephemeral_nux", false)) {
            A1C();
        }
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: X.4hQ
                @Override // android.content.DialogInterface.OnKeyListener
                public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    ActivityC000900k A0B;
                    EphemeralNUXDialog ephemeralNUXDialog = EphemeralNUXDialog.this;
                    if (i != 4 || keyEvent.getAction() != 0 || (A0B = ephemeralNUXDialog.A0B()) == null) {
                        return false;
                    }
                    A0B.onBackPressed();
                    return true;
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            A1K(dialog);
            C93004Yo.A00(this.A00, this.A01);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        boolean z = A03().getBoolean("from_settings");
        View A0F = C12960it.A0F(A0C().getLayoutInflater(), null, R.layout.ephemeral_nux);
        TextView A0I = C12960it.A0I(A0F, R.id.ephemeral_nux_title);
        TextView A0I2 = C12960it.A0I(A0F, R.id.ephemeral_nux_content);
        TextView A0I3 = C12960it.A0I(A0F, R.id.ephemeral_nux_finished);
        View A0D = AnonymousClass028.A0D(A0F, R.id.ephemeral_nux_go_to_faq);
        this.A00 = AnonymousClass028.A0D(A0F, R.id.ephemeral_nux_buttons_container);
        this.A01 = (ScrollView) AnonymousClass028.A0D(A0F, R.id.ephemeral_nux_scroller);
        int i = R.string.ephemeral_nux_someone_turned_on_title;
        int i2 = R.string.ephemeral_nux_someone_turned_on_content_generic;
        int i3 = R.string.ok;
        if (z) {
            i = R.string.ephemeral_nux_from_settings_title;
            i2 = R.string.ephemeral_nux_from_settings_content_generic;
            i3 = R.string.ephemeral_nux_finished;
        }
        C12960it.A0x(A0I3, this, 14);
        C12960it.A0x(A0D, this, 13);
        A0I.setText(i);
        A0I2.setText(i2);
        A0I3.setText(i3);
        View A0D2 = AnonymousClass028.A0D(A0F, R.id.nux_icon);
        LottieAnimationView lottieAnimationView = (LottieAnimationView) AnonymousClass028.A0D(A0F, R.id.ephemeral_lottie_animation);
        lottieAnimationView.setAnimation("ephemeral_settings_lottie_animation.lottie");
        lottieAnimationView.setVisibility(0);
        A0D2.setVisibility(8);
        return new AlertDialog.Builder(A01()).setView(A0F).create();
    }

    public final void A1K(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.width = Math.min(A02().getDimensionPixelSize(R.dimen.ephemeral_nux_width), A02().getDisplayMetrics().widthPixels);
            attributes.height = -2;
            window.setAttributes(attributes);
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            A1K(dialog);
            C93004Yo.A00(this.A00, this.A01);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B instanceof AbstractC33131dQ) {
            NewGroup newGroup = (NewGroup) ((AbstractC33131dQ) A0B);
            if (((ActivityC001000l) newGroup).A06.A02.compareTo(AnonymousClass05I.CREATED) >= 0) {
                ChangeEphemeralSettingsDialog.A00(newGroup.A0V(), newGroup.A00);
            }
        }
    }
}
