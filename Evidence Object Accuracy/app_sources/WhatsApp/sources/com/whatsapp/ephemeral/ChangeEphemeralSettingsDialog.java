package com.whatsapp.ephemeral;

import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.C12970iu;
import X.C14850m9;
import X.C17170qN;
import X.C32341c0;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.ephemeral.ChangeEphemeralSettingsDialog;
import com.whatsapp.group.NewGroup;

/* loaded from: classes2.dex */
public class ChangeEphemeralSettingsDialog extends Hilt_ChangeEphemeralSettingsDialog {
    public C17170qN A00;
    public C14850m9 A01;

    public static void A00(AnonymousClass01F r3, int i) {
        ChangeEphemeralSettingsDialog changeEphemeralSettingsDialog = new ChangeEphemeralSettingsDialog();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("from_settings", i);
        changeEphemeralSettingsDialog.A0U(A0D);
        changeEphemeralSettingsDialog.A1F(r3, "group_ephemeral_settings_dialog");
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        View inflate = A0C().getLayoutInflater().inflate(R.layout.disappearing_messages_group_settings_dialog, (ViewGroup) null, false);
        RadioGroup radioGroup = (RadioGroup) AnonymousClass028.A0D(inflate, R.id.disappearing_messages_settings_dialog_radio_group);
        C32341c0.A05(radioGroup, this.A01, A03().getInt("from_settings", 0), false);
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            View childAt = radioGroup.getChildAt(i);
            if (childAt instanceof RadioButton) {
                ((TextView) childAt).setTextSize(0, A02().getDimension(R.dimen.disappearing_messages_dialog_radio_button_textsize));
            }
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() { // from class: X.3OP
            @Override // android.widget.RadioGroup.OnCheckedChangeListener
            public final void onCheckedChanged(RadioGroup radioGroup2, int i2) {
                ChangeEphemeralSettingsDialog changeEphemeralSettingsDialog = ChangeEphemeralSettingsDialog.this;
                ActivityC000900k A0B = changeEphemeralSettingsDialog.A0B();
                if (A0B instanceof AbstractC33121dP) {
                    ((NewGroup) ((AbstractC33121dP) A0B)).A2e(C12960it.A05(AnonymousClass028.A0D(radioGroup2, i2).getTag()));
                }
                changeEphemeralSettingsDialog.A1B();
            }
        });
        return new AlertDialog.Builder(A01()).setView(inflate).create();
    }
}
