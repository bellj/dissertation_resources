package com.whatsapp.ephemeral;

import X.AbstractC15340mz;
import X.ActivityC000900k;
import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14820m6;
import X.C252018m;
import X.C93004Yo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ViewOnceNUXDialog extends Hilt_ViewOnceNUXDialog {
    public View A00;
    public ScrollView A01;
    public AnonymousClass12P A02;
    public C14820m6 A03;
    public C252018m A04;

    public static void A00(AnonymousClass01F r3, AbstractC15340mz r4, boolean z) {
        Bundle A0D = C12970iu.A0D();
        if (r4 != null) {
            A0D.putInt("MESSAGE_TYPE", r4.A0y);
        }
        A0D.putBoolean("FORCE_SHOW", z);
        ViewOnceNUXDialog viewOnceNUXDialog = new ViewOnceNUXDialog();
        viewOnceNUXDialog.A0U(A0D);
        viewOnceNUXDialog.A1F(r3, "view_once_nux");
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        String str;
        super.A13();
        if (!A03().getBoolean("FORCE_SHOW", false)) {
            C14820m6 r3 = this.A03;
            int i = A03().getInt("MESSAGE_TYPE", -1);
            SharedPreferences sharedPreferences = r3.A00;
            if (i == -1) {
                str = "view_once_nux";
            } else {
                str = "view_once_receiver_nux";
            }
            if (C12980iv.A1W(sharedPreferences, str)) {
                A1C();
            }
        }
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            A1L(dialog);
            C93004Yo.A00(this.A00, this.A01);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        int i;
        ActivityC000900k A0C = A0C();
        View A0F = C12960it.A0F(A0C.getLayoutInflater(), null, R.layout.view_once_nux);
        View A0D = AnonymousClass028.A0D(A0F, R.id.view_once_nux_finished);
        View A0D2 = AnonymousClass028.A0D(A0F, R.id.view_once_nux_go_to_faq);
        TextView A0I = C12960it.A0I(A0F, R.id.view_once_nux_title);
        TextView A0I2 = C12960it.A0I(A0F, R.id.view_once_nux_content);
        if (A03().getInt("MESSAGE_TYPE", -1) == -1) {
            C12990iw.A1H(A0I, this, R.string.view_once_nux_sender_title);
            i = R.string.view_once_nux_sender_content;
        } else if (A03().getInt("MESSAGE_TYPE", -1) == 42) {
            C12990iw.A1H(A0I, this, R.string.view_once_nux_receiver_photo_title);
            i = R.string.view_once_nux_receiver_photo_content;
        } else {
            C12990iw.A1H(A0I, this, R.string.view_once_nux_receiver_video_title);
            i = R.string.view_once_nux_receiver_video_content;
        }
        C12990iw.A1H(A0I2, this, i);
        this.A00 = AnonymousClass028.A0D(A0F, R.id.view_once_nux_buttons_container);
        this.A01 = (ScrollView) AnonymousClass028.A0D(A0F, R.id.view_once_nux_scroller);
        C12960it.A0x(A0D, this, 15);
        C12960it.A0x(A0D2, this, 16);
        return new AlertDialog.Builder(A0C).setView(A0F).create();
    }

    public final void A1K() {
        String str;
        C14820m6 r4 = this.A03;
        int i = A03().getInt("MESSAGE_TYPE", -1);
        SharedPreferences.Editor A08 = C12960it.A08(r4);
        if (i == -1) {
            str = "view_once_nux";
        } else {
            str = "view_once_receiver_nux";
        }
        C12960it.A0t(A08, str, true);
        A1C();
    }

    public final void A1L(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.width = Math.min(A02().getDimensionPixelSize(R.dimen.view_once_nux_width), A02().getDisplayMetrics().widthPixels);
            attributes.height = -2;
            window.setAttributes(attributes);
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Dialog dialog = ((DialogFragment) this).A03;
        if (dialog != null) {
            A1L(dialog);
            C93004Yo.A00(this.A00, this.A01);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        String str;
        C14820m6 r4 = this.A03;
        int i = A03().getInt("MESSAGE_TYPE", -1);
        SharedPreferences.Editor A08 = C12960it.A08(r4);
        if (i == -1) {
            str = "view_once_nux";
        } else {
            str = "view_once_receiver_nux";
        }
        C12960it.A0t(A08, str, true);
        super.onDismiss(dialogInterface);
    }
}
