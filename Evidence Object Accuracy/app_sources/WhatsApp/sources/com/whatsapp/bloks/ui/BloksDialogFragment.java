package com.whatsapp.bloks.ui;

import X.AbstractC116545Vw;
import X.ActivityC000800j;
import X.AnonymousClass009;
import X.AnonymousClass5RK;
import X.AnonymousClass67R;
import X.AnonymousClass67S;
import X.C119485eW;
import X.C12960it;
import X.C12970iu;
import X.C14900mE;
import X.C15570nT;
import X.C252718t;
import X.C48912Ik;
import X.C64893Hi;
import X.C73633gY;
import X.C89264Jh;
import X.C89294Jk;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.facebook.rendercore.RootHostView;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public class BloksDialogFragment extends Hilt_BloksDialogFragment implements AbstractC116545Vw {
    public View A00;
    public FrameLayout A01;
    public C48912Ik A02;
    public C14900mE A03;
    public C15570nT A04;
    public AnonymousClass67R A05;
    public C89294Jk A06;
    public AnonymousClass5RK A07;
    public C73633gY A08;
    public C252718t A09;
    public Boolean A0A;
    public Map A0B;

    public static BloksDialogFragment A00(String str, HashMap hashMap) {
        BloksDialogFragment bloksDialogFragment = new BloksDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("screen_name", str);
        A0D.putSerializable("screen_params", hashMap);
        A0D.putBoolean("hot_reload", false);
        bloksDialogFragment.A0U(A0D);
        return bloksDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        View currentFocus = A0C().getCurrentFocus();
        if (currentFocus != null) {
            this.A09.A01(currentFocus);
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.fragment_bloks);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        C73633gY r1 = this.A08;
        C64893Hi r0 = r1.A04;
        if (r0 != null) {
            r0.A04();
            r1.A04 = null;
        }
        this.A01 = null;
        this.A07 = null;
        this.A00 = null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        C119485eW A00 = this.A02.A00((ActivityC000800j) A0C(), A0F(), new C89264Jh(this.A0B));
        A0p();
        Bundle A03 = A03();
        String string = A03().getString("screen_name");
        AnonymousClass009.A05(string);
        this.A08.A01(A03, (ActivityC000800j) A0B(), this, A00, this.A05, this, string, (HashMap) A03().getSerializable("screen_params"));
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass67S r0 = new AnonymousClass5RK(view) { // from class: X.67S
            public final /* synthetic */ View A00;

            {
                this.A00 = r1;
            }
        };
        this.A07 = r0;
        this.A08.A03 = (RootHostView) r0.A00.findViewById(R.id.bloks_container);
        this.A00 = view.findViewById(R.id.bloks_dialogfragment_progressbar);
        this.A01 = (FrameLayout) view.findViewById(R.id.bloks_dialogfragment);
        this.A08.A00();
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog A1A = super.A1A(bundle);
        A1A.setCanceledOnTouchOutside(false);
        Window window = A1A.getWindow();
        if (window != null) {
            window.requestFeature(1);
        }
        return A1A;
    }

    @Override // X.AbstractC116545Vw
    public void AJJ(Boolean bool) {
        this.A0A = bool;
    }

    @Override // X.AbstractC116545Vw
    public void Abo(C89294Jk r1) {
        this.A06 = r1;
    }
}
