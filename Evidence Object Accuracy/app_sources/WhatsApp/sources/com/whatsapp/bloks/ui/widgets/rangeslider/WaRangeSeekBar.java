package com.whatsapp.bloks.ui.widgets.rangeslider;

import X.AnonymousClass676;
import X.AnonymousClass67T;
import X.AnonymousClass6LS;
import X.AnonymousClass6LT;
import X.AnonymousClass6Ll;
import X.AnonymousClass6Lm;
import X.C117565aA;
import X.C12960it;
import X.C129985yc;
import X.EnumC124525pg;
import X.EnumC124585pm;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class WaRangeSeekBar extends FrameLayout implements AnonymousClass6Ll, AnonymousClass6Lm, AnonymousClass6LS {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public int A06;
    public Paint A07;
    public Paint A08;
    public Paint A09;
    public Paint A0A;
    public Paint A0B;
    public C129985yc A0C;
    public AnonymousClass6LT A0D;
    public EnumC124525pg A0E;

    public WaRangeSeekBar(Context context) {
        super(context);
        A00();
    }

    public WaRangeSeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public WaRangeSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public final void A00() {
        C129985yc r6 = new C129985yc(getContext());
        this.A0C = r6;
        EnumC124585pm[] r4 = {EnumC124585pm.A02, EnumC124585pm.A03};
        int i = 0;
        r6.A05 = 0;
        int i2 = 0;
        do {
            if (r4[i2] != null) {
                int i3 = r4[i2].flag | i;
                r6.A05 = i3;
                i = i3;
            }
            i2++;
        } while (i2 < 2);
        r6.A0D = this;
        r6.A0E = this;
        r6.A0G = this;
        Resources A09 = C12960it.A09(this);
        Paint paint = new Paint();
        this.A07 = paint;
        paint.setColor(A09.getColor(R.color.primary_light));
        this.A07.setAntiAlias(true);
        this.A07.setStrokeWidth((float) A09.getDimensionPixelSize(R.dimen.wa_range_seek_bar_width));
        Paint paint2 = new Paint();
        this.A08 = paint2;
        paint2.setColor(A09.getColor(R.color.secondary_text));
        this.A08.setStrokeWidth((float) A09.getDimensionPixelSize(R.dimen.wa_range_seek_bar_width));
        Paint paint3 = new Paint();
        this.A09 = paint3;
        paint3.setColor(A09.getColor(R.color.primary_light));
        this.A09.setAlpha(127);
        this.A09.setAntiAlias(true);
        Paint paint4 = new Paint();
        this.A0A = paint4;
        paint4.setColor(A09.getColor(R.color.primary_light));
        this.A0A.setAntiAlias(true);
        Paint paint5 = new Paint();
        this.A0B = paint5;
        paint5.setColor(A09.getColor(R.color.primary_light));
        this.A0B.setAntiAlias(true);
        this.A04 = A09.getDimensionPixelSize(R.dimen.wa_range_seek_bar_thumb_bg_radius);
        this.A05 = A09.getDimensionPixelSize(R.dimen.wa_range_seek_bar_thumb_border_radius);
        this.A06 = A09.getDimensionPixelSize(R.dimen.wa_range_seek_bar_thumb_radius);
        this.A03 = Float.NaN;
        this.A02 = Float.NaN;
    }

    public final void A01() {
        AnonymousClass6LT r0 = this.A0D;
        if (r0 != null) {
            AnonymousClass67T r02 = (AnonymousClass67T) r0;
            AnonymousClass676.A01(r02.A01, this.A03, this.A02, r02.A00);
        }
    }

    public final void A02() {
        AnonymousClass6LT r0 = this.A0D;
        if (r0 != null) {
            AnonymousClass67T r02 = (AnonymousClass67T) r0;
            AnonymousClass676.A01(r02.A01, this.A03, this.A02, r02.A00);
        }
    }

    @Override // X.AnonymousClass6Ll
    public boolean APU(EnumC124585pm r3, float f, float f2) {
        setCurrentThumb(f);
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return true;
    }

    @Override // X.AnonymousClass6Lm
    public boolean AXR(float f, float f2) {
        if (Math.abs(f2 - ((float) (getMeasuredHeight() >> 1))) > ((float) (this.A04 << 1))) {
            return false;
        }
        setCurrentThumb(f);
        setCurrentPosition(f);
        this.A0E = null;
        A01();
        return true;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        float measuredHeight = (float) (getMeasuredHeight() >> 1);
        float startThumbX = getStartThumbX();
        float endThumbX = getEndThumbX();
        canvas.save();
        canvas.drawLine((float) this.A04, measuredHeight, (float) (getWidth() - this.A04), measuredHeight, this.A08);
        canvas.drawCircle(startThumbX, measuredHeight, (float) this.A04, this.A09);
        canvas.drawCircle(endThumbX, measuredHeight, (float) this.A04, this.A09);
        canvas.drawLine(startThumbX, measuredHeight, endThumbX, measuredHeight, this.A07);
        canvas.drawCircle(startThumbX, measuredHeight, (float) this.A05, this.A0A);
        canvas.drawCircle(endThumbX, measuredHeight, (float) this.A05, this.A0A);
        canvas.drawCircle(startThumbX, measuredHeight, (float) this.A06, this.A0B);
        canvas.drawCircle(endThumbX, measuredHeight, (float) this.A06, this.A0B);
        canvas.restore();
    }

    private int getCenterY() {
        return getMeasuredHeight() >> 1;
    }

    private float getCurrentPosition() {
        return this.A0E == EnumC124525pg.START ? getStartThumbX() : getEndThumbX();
    }

    private float getEndThumbX() {
        float f = this.A02;
        float f2 = this.A01;
        float f3 = this.A00;
        float f4 = (float) this.A04;
        return (((f - f2) / (f3 - f2)) * (((float) (getWidth() - this.A04)) - f4)) + f4;
    }

    private int getLeftBound() {
        return this.A04;
    }

    public float getRangeEndValue() {
        return this.A02;
    }

    public float getRangeStartValue() {
        return this.A03;
    }

    private int getRightBound() {
        return getWidth() - this.A04;
    }

    private float getStartThumbX() {
        float f = this.A03;
        float f2 = this.A01;
        float f3 = this.A00;
        float f4 = (float) this.A04;
        return (((f - f2) / (f3 - f2)) * (((float) (getWidth() - this.A04)) - f4)) + f4;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.A0C.A02(motionEvent);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C117565aA r4 = (C117565aA) parcelable;
        super.onRestoreInstanceState(r4.getSuperState());
        float f = r4.A01;
        float f2 = r4.A00;
        if (f >= this.A01 && f2 <= this.A00 && f <= f2) {
            this.A03 = f;
            this.A02 = f2;
            invalidate();
            A02();
        }
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        C117565aA r1 = new C117565aA(super.onSaveInstanceState());
        r1.A01 = this.A03;
        r1.A00 = this.A02;
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0095, code lost:
        if (r2 <= 0) goto L_0x00a1;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
        // Method dump skipped, instructions count: 245
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.bloks.ui.widgets.rangeslider.WaRangeSeekBar.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.View
    public boolean performClick() {
        return super.performClick();
    }

    private void setCurrentPosition(float f) {
        if (this.A0E != null) {
            float f2 = (float) this.A04;
            float f3 = this.A01;
            float f4 = this.A00;
            float width = (((f - f2) / (((float) (getWidth() - this.A04)) - f2)) * (f4 - f3)) + f3;
            if (this.A0E == EnumC124525pg.START) {
                float f5 = this.A02;
                this.A03 = Math.min(Math.max(f3, f5), Math.max(Math.min(f3, f5), width));
            } else {
                float f6 = this.A03;
                this.A02 = Math.min(Math.max(f6, f4), Math.max(Math.min(f6, f4), width));
            }
            invalidate();
            A02();
        }
    }

    private void setCurrentThumb(float f) {
        float abs = Math.abs(getEndThumbX() - f);
        float abs2 = Math.abs(getStartThumbX() - f);
        this.A0E = ((abs > abs2 ? 1 : (abs == abs2 ? 0 : -1)) == 0 ? (getStartThumbX() > f ? 1 : (getStartThumbX() == f ? 0 : -1)) : (abs > abs2 ? 1 : (abs == abs2 ? 0 : -1))) < 0 ? EnumC124525pg.END : EnumC124525pg.START;
    }

    public void setRangeSeekBarChangeListener(AnonymousClass6LT r1) {
        this.A0D = r1;
    }
}
