package com.whatsapp.bloks.ui;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.AnonymousClass1BZ;
import X.C12970iu;
import X.C15570nT;
import X.C252718t;
import X.C48912Ik;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import X.C72453ed;
import X.C73633gY;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaDialogFragment;

/* loaded from: classes4.dex */
public abstract class Hilt_BloksDialogFragment extends WaDialogFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1J();
        return this.A00;
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1J()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.bloks.ui.Hilt_BloksDialogFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1J();
        A1I();
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment
    public void A1I() {
        if (!this.A02) {
            this.A02 = true;
            BloksDialogFragment bloksDialogFragment = (BloksDialogFragment) this;
            C51112Sw r1 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r3 = r1.A0Y;
            C72453ed.A18(r3, bloksDialogFragment);
            bloksDialogFragment.A03 = C12970iu.A0R(r3);
            bloksDialogFragment.A05 = r1.A02();
            bloksDialogFragment.A04 = (C15570nT) r3.AAr.get();
            bloksDialogFragment.A09 = (C252718t) r3.A9K.get();
            bloksDialogFragment.A02 = (C48912Ik) r1.A0V.A19.get();
            C73633gY r12 = new C73633gY();
            r12.A05 = (AnonymousClass1BZ) r3.A1l.get();
            r12.A0A = r3.A4d();
            bloksDialogFragment.A08 = r12;
            bloksDialogFragment.A0B = r3.A4d();
        }
    }

    public final void A1J() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }
}
