package com.whatsapp.bloks.components;

import X.AbstractC12080hL;
import X.AbstractC12880ii;
import X.AbstractC14200l1;
import X.AnonymousClass09V;
import X.AnonymousClass0BC;
import X.AnonymousClass0BX;
import X.AnonymousClass0BY;
import X.AnonymousClass0LQ;
import X.AnonymousClass0LS;
import X.AnonymousClass0NR;
import X.AnonymousClass0OS;
import X.AnonymousClass0PL;
import X.AnonymousClass0QB;
import X.AnonymousClass0R8;
import X.AnonymousClass0TG;
import X.AnonymousClass0TU;
import X.AnonymousClass0UU;
import X.AnonymousClass28D;
import X.C04540Mc;
import X.C04550Md;
import X.C05150Ol;
import X.C08360b0;
import X.C08380b3;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14210l2;
import X.C14220l3;
import X.C14230l4;
import X.C14250l6;
import X.C14260l7;
import X.C28701Oq;
import X.C55982k5;
import X.C64173En;
import X.EnumC03700Iu;
import X.EnumC03850Jj;
import X.EnumC03890Jn;
import X.EnumC03900Jo;
import X.EnumC03910Jp;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.DialogFragment;
import com.facebook.common.util.redex.OriginalClassName;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class BkCdsBottomSheetFragment extends DialogFragment implements AbstractC12880ii {
    public AnonymousClass0QB A00;
    public AnonymousClass0UU A01;
    public C64173En A02;

    public static BkCdsBottomSheetFragment A00(AnonymousClass0UU r3, String str) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("request_data", str);
        A0D.putBundle("open_screen_config", r3.A05());
        BkCdsBottomSheetFragment bkCdsBottomSheetFragment = new BkCdsBottomSheetFragment();
        bkCdsBottomSheetFragment.A0U(A0D);
        return bkCdsBottomSheetFragment;
    }

    public static void A01(Activity activity, int i) {
        if (Build.VERSION.SDK_INT != 26 || activity.getApplicationInfo().targetSdkVersion <= 26 || (!A02(activity, 16842840) && !A02(activity, 16842839) && !A02(activity, 16843763))) {
            try {
                activity.setRequestedOrientation(i);
            } catch (IllegalStateException e) {
                if ("Only fullscreen activities can request orientation".equals(e.getMessage())) {
                    Object[] A1b = C12970iu.A1b();
                    A1b[0] = OriginalClassName.getClassSimpleName(activity);
                    AnonymousClass0TU.A01(e, A1b);
                    return;
                }
                throw e;
            }
        }
    }

    public static boolean A02(Activity activity, int i) {
        TypedValue typedValue = new TypedValue();
        activity.getTheme().resolveAttribute(i, typedValue, true);
        if (typedValue.type != 18 || typedValue.data == 0) {
            return false;
        }
        return true;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        if (this.A00 != null) {
            AnonymousClass0UU r0 = this.A01;
            C14230l4 r5 = r0.A05;
            AbstractC14200l1 r4 = r0.A07;
            C14260l7 r3 = r0.A04;
            AnonymousClass28D r2 = r0.A06;
            if (r4 == null) {
                return;
            }
            if (r2 != null && r3 != null) {
                C14210l2 r02 = new C14210l2();
                r02.A04(r3, 0);
                C28701Oq.A01(r3, r2, new C14220l3(r02.A00), r4);
            } else if (r5 != null) {
                C14210l2 r03 = new C14210l2();
                r03.A04(r3, 0);
                C14250l6.A00(r5, new C14220l3(r03.A00), r4);
            }
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        AnonymousClass0UU r0 = this.A01;
        if (r0 != null) {
            bundle.putBundle("open_screen_config", r0.A05());
        }
        super.A0w(bundle);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        AnonymousClass0QB A1I = A1I();
        Context A01 = A01();
        AnonymousClass0UU r4 = this.A01;
        C04540Mc r6 = new C04540Mc(A1I);
        C04550Md r1 = new C04550Md(A1I);
        C14260l7 r3 = r4.A04;
        A1I.A03 = new AnonymousClass0PL(A01, r6, r3);
        A1I.A02 = new C05150Ol(A01, r6, r1, r3);
        A1I.A04 = r4.A03;
        Activity A00 = AnonymousClass0R8.A00(A01);
        if (A00 != null) {
            A1I.A06 = Integer.valueOf(A00.getRequestedOrientation());
            A01(A00, 1);
        }
        AnonymousClass0BX r12 = new AnonymousClass0BX(A01, A1I.A04);
        A1I.A00 = r12;
        A1I.A01 = new AnonymousClass0BY(A01, r12, r4, r3);
        AnonymousClass0OS r42 = (AnonymousClass0OS) A1I.A0B.peek();
        if (r42 != null) {
            A1I.A00.A01.A03((View) r42.A00.A02(A01).A00, EnumC03850Jj.DEFAULT, false);
            C55982k5 r13 = r42.A01;
            AnonymousClass0BX r0 = A1I.A00;
            if (r0 != null) {
                ViewGroup viewGroup2 = r0.A00;
                viewGroup2.removeAllViews();
                viewGroup2.addView(r13);
            }
        }
        return A1I.A01;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Activity A00;
        super.A11();
        AnonymousClass0QB r4 = this.A00;
        if (r4 != null) {
            Context A01 = A01();
            Deque<AnonymousClass0OS> deque = r4.A0B;
            for (AnonymousClass0OS r0 : deque) {
                r0.A00.A05();
            }
            deque.clear();
            r4.A09.clear();
            r4.A0A.clear();
            r4.A08.clear();
            if (!(r4.A06 == null || (A00 = AnonymousClass0R8.A00(A01)) == null)) {
                A01(A00, r4.A06.intValue());
                r4.A06 = null;
            }
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        AnonymousClass0QB r4 = this.A00;
        if (r4 != null) {
            for (AnonymousClass0OS r2 : r4.A0B) {
                r2.A00.A06();
                AnonymousClass0BX r0 = r4.A00;
                if (r0 != null) {
                    r0.A00.removeView(r2.A01);
                }
            }
            AnonymousClass0PL r02 = r4.A03;
            if (r02 != null) {
                r02.A00 = null;
                r4.A03 = null;
            }
            C05150Ol r03 = r4.A02;
            if (r03 != null) {
                r03.A00 = null;
                r4.A02 = null;
            }
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        Bundle bundle2;
        super.A16(bundle);
        if (bundle != null) {
            A1B();
        }
        Bundle A03 = A03();
        if (bundle == null) {
            bundle2 = A03.getBundle("open_screen_config");
        } else {
            bundle2 = bundle.getBundle("open_screen_config");
        }
        this.A01 = AnonymousClass0UU.A03(bundle2);
        this.A00 = new AnonymousClass0QB();
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        AbstractC12080hL r14;
        AbstractC12080hL[] r13;
        AnonymousClass0BC r7;
        Window window;
        AbstractC12080hL[] r11;
        float f;
        AbstractC12080hL[] r10;
        AnonymousClass0QB A1I = A1I();
        Context A01 = A01();
        AnonymousClass0UU r3 = this.A01;
        EnumC03910Jp r0 = r3.A03;
        A1I.A04 = r0;
        EnumC03910Jp r9 = EnumC03910Jp.FULL_SCREEN;
        if (r0 != r9) {
            A1I.A04 = r0;
            if (r0 != r9) {
                AnonymousClass09V r2 = new AnonymousClass09V(A01);
                EnumC03890Jn r72 = r3.A01;
                if (!r72.equals(EnumC03890Jn.AUTO)) {
                    if (r72.equals(EnumC03890Jn.ENABLED)) {
                        r2.setCanceledOnTouchOutside(true);
                    } else if (r72.equals(EnumC03890Jn.DISABLED)) {
                        r2.setCanceledOnTouchOutside(false);
                    }
                }
                int A00 = (int) AnonymousClass0LQ.A00(A01, 4.0f);
                r2.A05.setPadding(A00, A00, A00, A00);
                EnumC03910Jp r8 = r3.A03;
                if (!r8.equals(EnumC03910Jp.FLEXIBLE_SHEET)) {
                    switch (r8.ordinal()) {
                        case 0:
                            f = 1.0f;
                            break;
                        case 1:
                            f = 0.75f;
                            break;
                        default:
                            throw C12980iv.A0u("Encountered unsupported CDS bottom sheet style.");
                    }
                    C08380b3 r12 = new AbstractC12080hL(f) { // from class: X.0b3
                        public final /* synthetic */ float A00;

                        {
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC12080hL
                        public final int AFn(View view, int i) {
                            return (int) (this.A00 * ((float) i));
                        }
                    };
                    r2.A08 = r12;
                    r7 = r2.A09;
                    AbstractC12080hL r132 = r2.A07;
                    if (r132 == null) {
                        r14 = AnonymousClass09V.A0H;
                        r10 = new AbstractC12080hL[]{r14, r12};
                    } else {
                        r14 = AnonymousClass09V.A0H;
                        r10 = new AbstractC12080hL[]{r14, r12, r132};
                    }
                    r7.A02(r10, r2.isShowing());
                    r2.A07 = r12;
                    AbstractC12080hL r102 = r2.A08;
                    r13 = r102 == null ? new AbstractC12080hL[]{r14, r12} : new AbstractC12080hL[]{r14, r102, r12};
                } else {
                    C08360b0 r133 = new AbstractC12080hL() { // from class: X.0b0
                        @Override // X.AbstractC12080hL
                        public final int AFn(View view, int i) {
                            int measuredHeight;
                            if (view == null) {
                                measuredHeight = 0;
                            } else {
                                measuredHeight = view.getMeasuredHeight();
                            }
                            return Math.min(measuredHeight, (int) (((float) i) * 0.75f));
                        }
                    };
                    r2.A08 = r133;
                    r7 = r2.A09;
                    AbstractC12080hL r122 = r2.A07;
                    if (r122 == null) {
                        r14 = AnonymousClass09V.A0H;
                        r11 = new AbstractC12080hL[]{r14, r133};
                    } else {
                        r14 = AnonymousClass09V.A0H;
                        r11 = new AbstractC12080hL[]{r14, r133, r122};
                    }
                    r7.A02(r11, r2.isShowing());
                    r2.A07 = null;
                    AbstractC12080hL r02 = r2.A08;
                    r13 = r02 == null ? new AbstractC12080hL[]{r14} : new AbstractC12080hL[]{r14, r02};
                }
                r7.A02(r13, r2.isShowing());
                if (r2.A0E) {
                    r2.A0E = false;
                }
                if (!r2.A0A) {
                    r2.A0A = true;
                    r2.A02(r2.A00);
                }
                r7.A0B = true;
                EnumC03900Jo r1 = r3.A02;
                if (r1 != EnumC03900Jo.AUTO ? r1 == EnumC03900Jo.DISABLED : r8 == EnumC03910Jp.FULL_SHEET || r8 == r9) {
                    AnonymousClass0LS r15 = new AnonymousClass0LS();
                    r7.A08 = Collections.singletonList(r14);
                    r7.A03 = r15;
                }
                int A002 = AnonymousClass0TG.A00(A01, EnumC03700Iu.A02, r3.A04);
                if (r2.A02 != A002) {
                    r2.A02 = A002;
                    r2.A02(r2.A00);
                }
                float alpha = ((float) Color.alpha(A002)) / 255.0f;
                if (r2.A01 != alpha) {
                    r2.A01 = alpha;
                    r2.A02(r2.A00);
                }
                if (Build.VERSION.SDK_INT >= 21 && (window = r2.getWindow()) != null) {
                    window.setStatusBarColor(0);
                }
                A1I.A05 = r2;
                r2.A06 = new AnonymousClass0NR(A01, A1I, r3);
                Activity A003 = AnonymousClass0R8.A00(A01);
                if (A003 != null) {
                    List A012 = AnonymousClass0R8.A01(A003);
                    if (A012 != null && !A012.isEmpty()) {
                        Iterator it = A012.iterator();
                        while (it.hasNext() && it.next() != this) {
                        }
                    }
                    return r2;
                }
                throw C12960it.A0U("Cannot show a fragment in a null activity");
            }
            throw C12980iv.A0u("onCreateDialog() is not supported for CDS full screen.");
        }
        throw C12980iv.A0u("onFragmentCreateDialog() is not supported for CDS full screen.");
    }

    public final AnonymousClass0QB A1I() {
        AnonymousClass0QB r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        throw C12960it.A0U("Must initialize bottom sheet delegate!");
    }

    @Override // X.AbstractC12880ii
    public C64173En A8A() {
        return this.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r8 == 7) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        if (r8 == 7) goto L_0x0045;
     */
    @Override // X.AbstractC12090hM
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AUv(int r8) {
        /*
            r7 = this;
            X.0QB r5 = r7.A1I()
            X.0BY r0 = r5.A01
            if (r0 == 0) goto L_0x0022
            X.0A2 r6 = r0.A03
            if (r6 == 0) goto L_0x0022
            X.0Jq r4 = r0.A08
            X.0Jq r0 = X.EnumC03920Jq.ANIMATED_WHILE_LOADING
            boolean r0 = r4.equals(r0)
            r3 = 7
            r2 = 0
            r1 = 1
            if (r0 == 0) goto L_0x0037
            if (r8 == r1) goto L_0x0045
            if (r8 == 0) goto L_0x0045
            if (r8 != r3) goto L_0x0049
        L_0x001f:
            r6.A01(r2)
        L_0x0022:
            if (r8 != 0) goto L_0x0049
            X.0PL r3 = r5.A03
            if (r3 == 0) goto L_0x0036
            X.0BY r2 = r5.A01
            if (r2 == 0) goto L_0x0036
            android.os.Handler r1 = r3.A02
            X.0dQ r0 = new X.0dQ
            r0.<init>(r2, r3)
            r1.post(r0)
        L_0x0036:
            return
        L_0x0037:
            X.0Jq r0 = X.EnumC03920Jq.ANIMATED_WHILE_LOADED
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0022
            if (r8 == r1) goto L_0x001f
            if (r8 == 0) goto L_0x001f
            if (r8 != r3) goto L_0x0049
        L_0x0045:
            r6.A01(r1)
            goto L_0x0022
        L_0x0049:
            r0 = 5
            if (r8 == r0) goto L_0x005e
            r0 = 6
            if (r8 != r0) goto L_0x0036
            X.0PL r2 = r5.A03
            if (r2 == 0) goto L_0x0036
            android.os.Handler r1 = r2.A02
            X.0cp r0 = new X.0cp
            r0.<init>(r2)
            r1.post(r0)
            return
        L_0x005e:
            X.0Ol r0 = r5.A02
            if (r0 == 0) goto L_0x0036
            X.0BY r0 = r5.A01
            if (r0 == 0) goto L_0x0036
            X.0PL r2 = r5.A03
            if (r2 == 0) goto L_0x0074
            android.os.Handler r1 = r2.A02
            X.0cp r0 = new X.0cp
            r0.<init>(r2)
            r1.post(r0)
        L_0x0074:
            X.0Ol r3 = r5.A02
            X.0BY r2 = r5.A01
            android.os.Handler r1 = r3.A02
            X.0dP r0 = new X.0dP
            r0.<init>(r2, r3)
            r1.post(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.bloks.components.BkCdsBottomSheetFragment.AUv(int):void");
    }

    @Override // X.AbstractC12880ii
    public void AYa(AnonymousClass0OS r7, C14260l7 r8, AbstractC14200l1 r9, int i) {
        A1I().A04(A01(), r7, EnumC03850Jj.DEFAULT, r8, i);
    }
}
