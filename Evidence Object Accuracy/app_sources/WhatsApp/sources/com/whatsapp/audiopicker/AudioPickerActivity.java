package com.whatsapp.audiopicker;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass03M;
import X.AnonymousClass0QL;
import X.AnonymousClass11P;
import X.AnonymousClass19D;
import X.AnonymousClass1A1;
import X.AnonymousClass1CY;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14850m9;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C18000rk;
import X.C18720su;
import X.C21270x9;
import X.C35191hP;
import X.C40691s6;
import X.C457522x;
import X.C48232Fc;
import X.C52842bm;
import X.C54012fx;
import X.C63223At;
import X.C63353Bg;
import X.C66733Os;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

/* loaded from: classes2.dex */
public class AudioPickerActivity extends ActivityC13770kJ implements AnonymousClass03M {
    public AudioManager A00;
    public Menu A01;
    public View A02;
    public ImageButton A03;
    public ListView A04;
    public RelativeLayout A05;
    public RelativeLayout A06;
    public TextView A07;
    public C48232Fc A08;
    public C52842bm A09;
    public C18720su A0A;
    public C15550nR A0B;
    public C15610nY A0C;
    public AnonymousClass1J1 A0D;
    public C21270x9 A0E;
    public AnonymousClass19D A0F;
    public AnonymousClass11P A0G;
    public C15370n3 A0H;
    public C457522x A0I;
    public AnonymousClass1CY A0J;
    public AnonymousClass01H A0K;
    public AnonymousClass01H A0L;
    public String A0M;
    public ArrayList A0N;
    public LinkedHashMap A0O;
    public boolean A0P;

    public AudioPickerActivity() {
        this(0);
    }

    public AudioPickerActivity(int i) {
        this.A0P = false;
        ActivityC13830kP.A1P(this, 11);
    }

    public static /* synthetic */ void A02(AudioPickerActivity audioPickerActivity) {
        ArrayList<? extends Parcelable> A0l = C12960it.A0l();
        Iterator A0t = C12990iw.A0t(audioPickerActivity.A0O);
        while (A0t.hasNext()) {
            A0l.add(ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, (long) ((C63353Bg) A0t.next()).A00));
        }
        Intent A0A = C12970iu.A0A();
        A0A.putParcelableArrayListExtra("result_uris", A0l);
        C12960it.A0q(audioPickerActivity, A0A);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0P) {
            this.A0P = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0A = (C18720su) A1M.A2c.get();
            this.A0J = (AnonymousClass1CY) A1M.ABO.get();
            this.A0E = C12970iu.A0W(A1M);
            this.A0B = C12960it.A0O(A1M);
            this.A0C = C12960it.A0P(A1M);
            this.A0F = (AnonymousClass19D) A1M.ABo.get();
            this.A0G = (AnonymousClass11P) A1M.ABp.get();
            this.A0K = C18000rk.A00(A1M.ADw);
            this.A0L = C18000rk.A00(A1M.AID);
        }
    }

    public final void A2g() {
        Menu menu;
        MenuItem findItem;
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A06(A1U, "supportActionBar is null");
        Iterator A0t = C12990iw.A0t(this.A0O);
        while (A0t.hasNext()) {
            String str = ((C63353Bg) A0t.next()).A03;
            if (str == null || !new File(str).exists()) {
                A0t.remove();
            }
        }
        boolean z = true;
        if (this.A09.getCursor() == null) {
            this.A04.setVisibility(8);
            this.A06.setVisibility(8);
            this.A05.setVisibility(0);
            this.A07.setVisibility(8);
            if (!ActivityC13810kN.A1I(this) || !this.A0G.A0C()) {
                this.A0G.A06();
            }
        } else {
            this.A05.setVisibility(8);
            int count = this.A09.getCursor().getCount();
            ListView listView = this.A04;
            if (count == 0) {
                listView.setVisibility(8);
                C63223At.A00(this.A03, false, false);
                boolean A05 = this.A08.A05();
                RelativeLayout relativeLayout = this.A06;
                if (A05) {
                    relativeLayout.setVisibility(8);
                    this.A07.setVisibility(0);
                    this.A07.setText(C12960it.A0X(this, this.A0M, new Object[1], 0, R.string.audio_nothing_found));
                } else {
                    relativeLayout.setVisibility(0);
                    this.A07.setVisibility(8);
                    this.A0O.clear();
                }
            } else {
                listView.setVisibility(0);
                this.A06.setVisibility(8);
                this.A07.setVisibility(8);
                LinkedHashMap linkedHashMap = this.A0O;
                if (linkedHashMap.isEmpty()) {
                    A1U.A09(R.string.tap_to_select);
                } else {
                    AnonymousClass018 r5 = ((ActivityC13830kP) this).A01;
                    long size = (long) linkedHashMap.size();
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, linkedHashMap.size(), 0);
                    A1U.A0H(r5.A0I(objArr, R.plurals.n_selected, size));
                }
                C63223At.A00(this.A03, !this.A0O.isEmpty(), false);
                menu = this.A01;
                if (menu != null && (findItem = menu.findItem(R.id.menuitem_search)) != null) {
                    if (this.A09.getCursor() == null || this.A09.getCursor().getCount() <= 0) {
                        z = false;
                    }
                    findItem.setVisible(z);
                    return;
                }
            }
        }
        A1U.A0H("");
        menu = this.A01;
        if (menu != null) {
        }
    }

    @Override // X.AnonymousClass03M
    public AnonymousClass0QL AOi(Bundle bundle, int i) {
        return new C54012fx(getContentResolver(), this, this.A0N);
    }

    @Override // X.AnonymousClass03M
    public /* bridge */ /* synthetic */ void AS2(AnonymousClass0QL r2, Object obj) {
        this.A09.swapCursor((Cursor) obj);
        A2g();
    }

    @Override // X.AnonymousClass03M
    public void AS8(AnonymousClass0QL r3) {
        this.A09.swapCursor(null);
        A2g();
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (ActivityC13810kN.A1I(this)) {
            this.A0K.get();
        }
        if (this.A08.A05()) {
            if (!this.A0O.isEmpty()) {
                C63223At.A00(this.A03, true, true);
            }
            this.A08.A04(true);
            return;
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_audio_file_list);
        this.A0O = new LinkedHashMap();
        this.A0I = new C457522x(getContentResolver(), new Handler(), this.A0A, "audio-picker");
        Toolbar A0Q = ActivityC13790kL.A0Q(this);
        A1e(A0Q);
        this.A08 = new C48232Fc(this, findViewById(R.id.search_holder), new C66733Os(this), A0Q, ((ActivityC13830kP) this).A01);
        this.A0H = C15550nR.A00(this.A0B, C12970iu.A0c(this));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A06(A1U, "supportActionBar is null");
        A1U.A0M(true);
        A1U.A0I(C12960it.A0X(this, this.A0C.A04(this.A0H), new Object[1], 0, R.string.send_to_contact));
        this.A06 = (RelativeLayout) findViewById(R.id.no_audio_layout);
        this.A05 = (RelativeLayout) findViewById(R.id.loading_audio_layout);
        this.A07 = C12970iu.A0M(this, R.id.empty);
        ListView A2e = A2e();
        this.A04 = A2e;
        A2e.setBackground(null);
        ImageButton imageButton = (ImageButton) findViewById(R.id.fab);
        this.A03 = imageButton;
        C63223At.A00(imageButton, false, false);
        C12960it.A10(this.A03, this, 13);
        C12960it.A0r(this, this.A03, R.string.send);
        C52842bm r0 = new C52842bm(this, this);
        this.A09 = r0;
        A2f(r0);
        this.A00 = ((ActivityC13810kN) this).A08.A0G();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean z = false;
        menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search).setShowAsAction(10);
        this.A01 = menu;
        MenuItem findItem = menu.findItem(R.id.menuitem_search);
        if (findItem != null) {
            if (this.A04.getCount() > 0) {
                z = true;
            }
            findItem.setVisible(z);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0I.A00();
        this.A0I = null;
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A02(this.A02, this.A0G);
            AnonymousClass1J1 r0 = this.A0D;
            if (r0 != null) {
                r0.A00();
                this.A0D = null;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        AudioManager audioManager = this.A00;
        if (audioManager != null) {
            if (i == 24) {
                audioManager.adjustStreamVolume(3, 1, 1);
                return true;
            } else if (i == 25) {
                audioManager.adjustStreamVolume(3, -1, 1);
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_search) {
            onSearchRequested();
            return true;
        } else if (itemId != 16908332) {
            return true;
        } else {
            finish();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (ActivityC13810kN.A1I(this)) {
            C40691s6.A07(this.A0G);
            ((AnonymousClass1A1) this.A0K.get()).A02(((ActivityC13810kN) this).A00);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (ActivityC13810kN.A1I(this)) {
            boolean z = ((AnonymousClass1A1) this.A0K.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r15 = ((ActivityC13810kN) this).A0C;
                C14900mE r13 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
                C21270x9 r10 = this.A0E;
                C15550nR r9 = this.A0B;
                C15610nY r8 = this.A0C;
                AnonymousClass018 r7 = ((ActivityC13830kP) this).A01;
                Pair A00 = C40691s6.A00(this, view, this.A02, r13, r12, r9, r8, this.A0D, r10, this.A0F, this.A0G, ((ActivityC13810kN) this).A09, r7, r15, r11, this.A0K, this.A0L, "audio-picker-activity");
                this.A02 = (View) A00.first;
                this.A0D = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A0G, this.A0K);
            }
            ((AnonymousClass1A1) this.A0K.get()).A01();
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        C63223At.A00(this.A03, false, true);
        this.A08.A01();
        C12960it.A10(findViewById(R.id.search_back), this, 12);
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        A2g();
        A0W().A00(null, this);
        super.onStart();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        C35191hP A00;
        super.onStop();
        if ((!ActivityC13810kN.A1I(this) || !this.A0G.A0C()) && (A00 = this.A0G.A00()) != null) {
            A00.A0H(true, false);
            this.A0G.A08(null);
        }
    }
}
