package com.whatsapp;

import X.AnonymousClass01E;
import X.AnonymousClass1SM;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;

/* loaded from: classes2.dex */
public class SingleSelectionDialogFragment extends Hilt_SingleSelectionDialogFragment {
    public int A00;
    public int A01;
    public int A02;
    public String A03;
    public boolean A04;
    public String[] A05;

    public static Bundle A00(String[] strArr, int i, int i2, int i3) {
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogId", i);
        A0D.putInt("currentIndex", i2);
        A0D.putInt("dialogTitleResId", i3);
        A0D.putStringArray("items", strArr);
        A0D.putBoolean("showConfirmation", true);
        return A0D;
    }

    public static SingleSelectionDialogFragment A01(int i, int i2, int i3, int i4) {
        SingleSelectionDialogFragment singleSelectionDialogFragment = new SingleSelectionDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogId", i);
        A0D.putInt("currentIndex", i3);
        A0D.putInt("dialogTitleResId", i2);
        A0D.putInt("itemsArrayResId", i4);
        singleSelectionDialogFragment.A0U(A0D);
        return singleSelectionDialogFragment;
    }

    public static SingleSelectionDialogFragment A02(String[] strArr, int i, int i2, int i3) {
        SingleSelectionDialogFragment singleSelectionDialogFragment = new SingleSelectionDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogId", i);
        A0D.putInt("currentIndex", i3);
        A0D.putInt("dialogTitleResId", i2);
        A0D.putStringArray("items", strArr);
        singleSelectionDialogFragment.A0U(A0D);
        return singleSelectionDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        String string;
        String[] stringArray;
        super.A16(bundle);
        if (A0B() instanceof AnonymousClass1SM) {
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            this.A01 = bundle2.getInt("dialogId");
            this.A00 = bundle2.getInt("currentIndex");
            if (bundle2.containsKey("dialogTitleResId")) {
                string = A0I(bundle2.getInt("dialogTitleResId"));
            } else {
                string = bundle2.getString("dialogTitle");
            }
            this.A03 = string;
            if (bundle2.containsKey("itemsArrayResId")) {
                stringArray = A02().getStringArray(bundle2.getInt("itemsArrayResId"));
            } else {
                stringArray = bundle2.getStringArray("items");
            }
            this.A05 = stringArray;
            this.A04 = bundle2.getBoolean("showConfirmation", false);
            return;
        }
        throw C12960it.A0U(C12960it.A0d("SingleSelectionDialogListener", C12960it.A0k("Activity must implement ")));
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        return A1K().create();
    }

    public C004802e A1K() {
        C004802e A0O = C12970iu.A0O(this);
        A0O.setTitle(this.A03);
        int i = this.A00;
        this.A02 = i;
        A0O.A09(new IDxCListenerShape8S0100000_1_I1(this, 3), this.A05, i);
        if (this.A04) {
            C12970iu.A1M(A0O, this, 2, R.string.ok);
            A0O.setNegativeButton(R.string.cancel, null);
        }
        return A0O;
    }
}
