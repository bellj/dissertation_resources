package com.whatsapp;

import X.AnonymousClass18U;
import X.AnonymousClass2QN;
import X.AnonymousClass2eq;
import X.C14900mE;
import X.C252018m;
import X.C42971wC;
import X.C52162aM;
import X.C58272oQ;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class FAQTextView extends TextEmojiLabel {
    public C14900mE A00;
    public AnonymousClass18U A01;
    public C252018m A02;
    public boolean A03;

    public FAQTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FAQTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2QN.A09, 0, 0);
            try {
                String A0E = ((WaTextView) this).A01.A0E(obtainStyledAttributes, 1);
                String string = obtainStyledAttributes.getString(0);
                if (!(A0E == null || string == null)) {
                    setEducationTextFromArticleID(new SpannableStringBuilder(A0E), string);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        setAccessibilityHelper(new AnonymousClass2eq(this, this.A08));
        setClickable(true);
    }

    public void setEducationText(Spannable spannable, String str, String str2) {
        setLinksClickable(true);
        setFocusable(false);
        this.A07 = new C52162aM();
        if (str2 == null) {
            str2 = getContext().getString(R.string.learn_more);
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str2);
        spannableStringBuilder.setSpan(new C58272oQ(getContext(), this.A01, this.A00, this.A08, str), 0, str2.length(), 33);
        setText(C42971wC.A02(getContext().getString(R.string.faq_text_placeholders), spannable, spannableStringBuilder));
    }

    public void setEducationTextFromArticleID(Spannable spannable, String str) {
        setEducationText(spannable, this.A02.A03(str).toString(), null);
    }

    public void setEducationTextFromNamedArticle(Spannable spannable, String str, String str2) {
        setEducationText(spannable, this.A02.A04(str, str2).toString(), null);
    }
}
