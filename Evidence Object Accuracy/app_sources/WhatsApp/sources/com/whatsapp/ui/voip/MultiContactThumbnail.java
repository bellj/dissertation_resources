package com.whatsapp.ui.voip;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass1J1;
import X.AnonymousClass28F;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12990iw;
import X.C15370n3;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.List;

/* loaded from: classes2.dex */
public class MultiContactThumbnail extends ViewGroup implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public Canvas A03;
    public Paint A04;
    public RectF A05;
    public AnonymousClass018 A06;
    public AnonymousClass2P7 A07;
    public boolean A08;

    public MultiContactThumbnail(Context context) {
        this(context, null);
    }

    public MultiContactThumbnail(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MultiContactThumbnail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A08) {
            this.A08 = true;
            this.A06 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A00 = 0.0f;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0D);
            this.A00 = obtainStyledAttributes.getDimension(1, this.A00);
            this.A02 = obtainStyledAttributes.getDimensionPixelSize(0, this.A02);
            obtainStyledAttributes.recycle();
        }
        int i2 = ((int) this.A00) << 1;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, Bitmap.Config.ARGB_8888);
        this.A03 = new Canvas(createBitmap);
        float f = (float) i2;
        this.A05 = new RectF(0.0f, 0.0f, f, f);
        this.A04 = C12990iw.A0G(3);
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        this.A04.setShader(new BitmapShader(createBitmap, tileMode, tileMode));
        setWillNotDraw(false);
    }

    public void A00(AnonymousClass28F r6, AnonymousClass1J1 r7, List list) {
        boolean z = true;
        if (list.size() < 1) {
            z = false;
        }
        AnonymousClass009.A0A(C12960it.A0f(C12960it.A0k("Value %d out of bounds for numImages"), this.A01), z);
        int i = 4;
        if (list.size() <= 4) {
            i = list.size();
        }
        setNumImages(i);
        for (int i2 = 0; i2 < i; i2++) {
            r7.A02((ImageView) getChildAt(i2), r6, (C15370n3) list.get(i2), false);
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Canvas canvas2 = this.A03;
        canvas2.drawColor(0, PorterDuff.Mode.CLEAR);
        super.draw(canvas2);
        RectF rectF = this.A05;
        float f = this.A00;
        canvas.drawRoundRect(rectF, f, f, this.A04);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0042, code lost:
        if (r0 <= 1) goto L_0x0044;
     */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r15, int r16, int r17, int r18, int r19) {
        /*
            r14 = this;
            int r0 = r14.A01
            r11 = 1
            if (r0 < r11) goto L_0x0081
            int r7 = r14.getPaddingLeft()
            int r10 = r14.getPaddingTop()
            int r6 = r14.getMeasuredWidth()
            int r0 = r14.getPaddingRight()
            int r6 = r6 - r0
            int r9 = r14.getMeasuredHeight()
            int r0 = r14.getPaddingBottom()
            int r9 = r9 - r0
            X.018 r0 = r14.A06
            boolean r13 = X.C28141Kv.A00(r0)
            int r6 = r6 - r7
            int r9 = r9 - r10
            int r12 = r6 >> 1
            int r0 = r14.A02
            int r1 = r12 - r0
            int r5 = r9 >> 1
            int r4 = r5 - r0
            int r12 = r12 + r7
            int r12 = r12 + r0
            int r5 = r5 + r10
            int r5 = r5 + r0
            int r0 = r14.A01
            if (r0 == r11) goto L_0x003a
            r6 = r1
        L_0x003a:
            r8 = 3
            r3 = r4
            if (r0 > r8) goto L_0x003f
            r3 = r9
        L_0x003f:
            if (r13 == 0) goto L_0x0044
            r2 = r12
            if (r0 > r11) goto L_0x0045
        L_0x0044:
            r2 = r7
        L_0x0045:
            r0 = 0
            android.view.View r1 = r14.getChildAt(r0)
            int r0 = r2 + r6
            int r3 = r3 + r10
            r1.layout(r2, r10, r0, r3)
            int r0 = r14.A01
            if (r0 == r11) goto L_0x0081
            r3 = r12
            if (r13 == 0) goto L_0x0058
            r3 = r7
        L_0x0058:
            r2 = 2
            if (r0 <= r2) goto L_0x005c
            r9 = r4
        L_0x005c:
            android.view.View r0 = r14.getChildAt(r11)
            int r1 = r3 + r6
            int r9 = r9 + r10
            r0.layout(r3, r10, r1, r9)
            int r0 = r14.A01
            if (r0 == r2) goto L_0x0081
            android.view.View r0 = r14.getChildAt(r2)
            int r4 = r4 + r5
            r0.layout(r3, r5, r1, r4)
            int r0 = r14.A01
            if (r0 == r8) goto L_0x0081
            if (r13 == 0) goto L_0x0079
            r7 = r12
        L_0x0079:
            android.view.View r0 = r14.getChildAt(r8)
            int r6 = r6 + r7
            r0.layout(r7, r5, r6, r4)
        L_0x0081:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.ui.voip.MultiContactThumbnail.onLayout(boolean, int, int, int, int):void");
    }

    private void setNumImages(int i) {
        if (i == this.A01) {
            invalidate();
            return;
        }
        int childCount = getChildCount();
        int childCount2 = getChildCount();
        if (i <= childCount) {
            int i2 = childCount2 - i;
            int childCount3 = getChildCount();
            for (int i3 = 0; i3 < i2; i3++) {
                getChildAt((childCount3 - 1) - i3).setVisibility(8);
            }
        } else {
            int i4 = i - childCount2;
            for (int i5 = 0; i5 < i4; i5++) {
                ImageView imageView = new ImageView(getContext());
                C12990iw.A1E(imageView);
                addView(imageView);
            }
        }
        for (int i6 = 0; i6 < i; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 0) {
                childAt.setVisibility(0);
            }
        }
        this.A01 = i;
    }
}
