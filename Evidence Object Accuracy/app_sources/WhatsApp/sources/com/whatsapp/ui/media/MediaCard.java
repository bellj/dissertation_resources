package com.whatsapp.ui.media;

import X.AnonymousClass028;
import X.AnonymousClass36E;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class MediaCard extends AnonymousClass36E {
    public LinearLayout A00;

    public MediaCard(Context context) {
        this(context, null);
    }

    public MediaCard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MediaCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // X.AnonymousClass36H
    public void A07(int i, int i2) {
        super.A07(i, i2);
        LinearLayout linearLayout = this.A00;
        if (i < 0) {
            i = linearLayout.getPaddingLeft();
        }
        int paddingTop = this.A00.getPaddingTop();
        if (i2 < 0) {
            i2 = this.A00.getPaddingRight();
        }
        linearLayout.setPadding(i, paddingTop, i2, this.A00.getPaddingBottom());
    }

    @Override // X.AnonymousClass36H
    public void A08(AttributeSet attributeSet) {
        super.A08(attributeSet);
        this.A00 = (LinearLayout) AnonymousClass028.A0D(this, R.id.media_card_thumbs);
    }

    @Override // X.AnonymousClass36H
    public int getThumbnailPixelSize() {
        return getResources().getDimensionPixelSize(R.dimen.medium_thumbnail_size);
    }

    @Override // X.AnonymousClass36H
    public void setError(String str) {
        super.setError(str);
        this.A00.setVisibility(8);
    }
}
