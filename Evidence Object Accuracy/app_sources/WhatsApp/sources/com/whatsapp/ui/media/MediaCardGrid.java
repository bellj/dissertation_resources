package com.whatsapp.ui.media;

import X.AbstractC53212dH;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass36H;
import X.C12960it;
import X.C74083hH;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewStub;
import android.widget.GridView;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class MediaCardGrid extends AnonymousClass36H {
    public GridView A00;
    public AnonymousClass01d A01;
    public C74083hH A02;
    public ArrayList A03;
    public boolean A04;

    public MediaCardGrid(Context context) {
        this(context, null);
    }

    public MediaCardGrid(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MediaCardGrid(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    @Override // X.AbstractC53212dH
    public void A01() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A0B = C12960it.A0R(A00);
            this.A01 = C12960it.A0Q(A00);
        }
    }

    @Override // X.AnonymousClass36H
    public void A04() {
        super.A04();
        this.A00.setVisibility(8);
    }

    @Override // X.AnonymousClass36H
    public void A05() {
        super.A05();
        this.A00.setVisibility(0);
    }

    @Override // X.AnonymousClass36H
    public void A08(AttributeSet attributeSet) {
        super.A08(attributeSet);
        ViewStub viewStub = (ViewStub) AnonymousClass028.A0D(this, R.id.media_card_grid_stub);
        viewStub.setLayoutResource(R.layout.media_card_grid);
        this.A00 = (GridView) viewStub.inflate();
    }

    @Override // X.AnonymousClass36H
    public int getThumbnailPixelSize() {
        return AbstractC53212dH.A00(new DisplayMetrics(), this, AnonymousClass01d.A02(getContext())) / 3;
    }

    @Override // X.AnonymousClass36H
    public void setError(String str) {
        super.setError(str);
        this.A00.setVisibility(8);
    }
}
