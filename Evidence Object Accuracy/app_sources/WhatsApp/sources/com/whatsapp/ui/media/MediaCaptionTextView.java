package com.whatsapp.ui.media;

import X.AbstractC116255Us;
import X.AbstractC32741cf;
import X.AbstractC36671kL;
import X.C42971wC;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.text.ReadMoreTextView;

/* loaded from: classes2.dex */
public class MediaCaptionTextView extends ReadMoreTextView {
    public boolean A00;

    public MediaCaptionTextView(Context context) {
        super(context);
        A08();
        A02();
    }

    public MediaCaptionTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
        A02();
    }

    public MediaCaptionTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        A02();
    }

    public MediaCaptionTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A08();
    }

    private void A02() {
        setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 12));
        ((ReadMoreTextView) this).A02 = new AbstractC116255Us() { // from class: X.59e
            @Override // X.AbstractC116255Us
            public final boolean AO3() {
                return true;
            }
        };
    }

    public void setCaptionText(CharSequence charSequence) {
        float dimensionPixelSize;
        if (TextUtils.isEmpty(charSequence)) {
            setVisibility(8);
            return;
        }
        int A00 = AbstractC32741cf.A00(charSequence);
        if (A00 <= 0 || A00 > 3) {
            Resources resources = getContext().getResources();
            int length = charSequence.length();
            int i = R.dimen.caption_text_size_small;
            if (length < 96) {
                i = R.dimen.caption_text_size_large;
            }
            dimensionPixelSize = (float) resources.getDimensionPixelSize(i);
        } else {
            float dimensionPixelSize2 = (float) getContext().getResources().getDimensionPixelSize(R.dimen.caption_text_size_large);
            dimensionPixelSize = dimensionPixelSize2 + (((Math.max(dimensionPixelSize2, Math.min(dimensionPixelSize2, (getContext().getResources().getDisplayMetrics().density * dimensionPixelSize2) / getContext().getResources().getDisplayMetrics().scaledDensity) * 1.5f) - dimensionPixelSize2) * ((float) (4 - A00))) / 3.0f);
        }
        int i2 = 8388611;
        if (charSequence.length() < 96) {
            i2 = 17;
        }
        setGravity(i2);
        setTextSize(0, dimensionPixelSize);
        setText(AbstractC36671kL.A03(getContext(), getPaint(), ((TextEmojiLabel) this).A09, C42971wC.A03(((TextEmojiLabel) this).A08, ((TextEmojiLabel) this).A0A, charSequence)));
        setVisibility(0);
    }
}
