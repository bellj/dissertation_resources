package com.whatsapp.camera.bottomsheet;

import X.AbstractC017308c;
import X.AbstractC14710lv;
import X.AbstractC35611iN;
import X.AbstractC454421p;
import X.AbstractC48852Ic;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass01T;
import X.AnonymousClass028;
import X.AnonymousClass1s8;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2T1;
import X.AnonymousClass2T3;
import X.AnonymousClass2T8;
import X.C39341ph;
import X.C41691tw;
import X.C453421e;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.StickyHeadersRecyclerView;
import com.whatsapp.camera.bottomsheet.CameraMediaPickerFragment;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class CameraMediaPickerFragment extends Hilt_CameraMediaPickerFragment implements AbstractC48852Ic {
    public BroadcastReceiver A00;
    public MenuItem A01;
    public Toolbar A02;
    public Toolbar A03;
    public final C453421e A04 = new C453421e();
    public final HashSet A05 = new LinkedHashSet();
    public final List A06 = new ArrayList();

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        BroadcastReceiver broadcastReceiver = this.A00;
        if (broadcastReceiver != null) {
            A0B().unregisterReceiver(broadcastReceiver);
            this.A00 = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        AnonymousClass1s8 ABD;
        ActivityC000900k A0B = A0B();
        if (!(!(A0B instanceof AbstractC14710lv) || (ABD = ((AbstractC14710lv) A0B).ABD()) == null || ABD.A08 == null)) {
            ABD.A0D(i, i2, intent);
        }
        if (i != 101) {
            return;
        }
        if (i2 == -1) {
            this.A05.clear();
        } else if (i2 == 1) {
            ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
            HashSet hashSet = this.A05;
            hashSet.clear();
            if (parcelableArrayListExtra != null) {
                hashSet.addAll(parcelableArrayListExtra);
            }
            if (!A1J()) {
                A1M();
            }
            A1O();
            this.A04.A01(intent.getExtras());
            ((MediaGalleryFragmentBase) this).A06.A02();
        }
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putParcelableArrayList("android.intent.extra.STREAM", new ArrayList<>(this.A05));
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.camera_gallery, viewGroup, false);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A11() {
        super.A11();
        StickyHeadersRecyclerView stickyHeadersRecyclerView = ((MediaGalleryFragmentBase) this).A08;
        if (stickyHeadersRecyclerView != null) {
            int childCount = stickyHeadersRecyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ((MediaGalleryFragmentBase) this).A08.getChildAt(i);
                if (childAt instanceof AnonymousClass2T1) {
                    ((ImageView) childAt).setImageDrawable(null);
                }
            }
        }
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A13() {
        super.A13();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
        intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addDataScheme("file");
        AnonymousClass2T8 r1 = new AnonymousClass2T8(this);
        this.A00 = r1;
        A0B().registerReceiver(r1, intentFilter);
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.A03 = toolbar;
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(A01(), R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), ((MediaGalleryFragmentBase) this).A0E));
        this.A03.setNavigationContentDescription(R.string.back);
        this.A03.getMenu().add(0, R.id.menuitem_select_multiple, 0, R.string.select_multiple).setIcon(AnonymousClass2GE.A01(A0p(), R.drawable.ic_action_select_multiple_teal, R.color.lightActionBarItemDrawableTint)).setShowAsAction(2);
        Toolbar toolbar2 = this.A03;
        toolbar2.A0R = new AbstractC017308c() { // from class: X.2T9
            @Override // X.AbstractC017308c
            public final boolean onMenuItemClick(MenuItem menuItem) {
                CameraMediaPickerFragment cameraMediaPickerFragment = CameraMediaPickerFragment.this;
                if (menuItem.getItemId() != R.id.menuitem_select_multiple) {
                    return false;
                }
                cameraMediaPickerFragment.A1M();
                return true;
            }
        };
        toolbar2.setNavigationOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 49));
        Toolbar toolbar3 = (Toolbar) view.findViewById(R.id.gallery_action_mode_bar);
        this.A02 = toolbar3;
        MenuItem add = toolbar3.getMenu().add(0, R.id.menuitem_select_multiple, 0, R.string.ok);
        this.A01 = add;
        add.setShowAsAction(2);
        this.A02.setNavigationIcon(new AnonymousClass2GF(AnonymousClass00T.A04(A0p(), R.drawable.ic_back), ((MediaGalleryFragmentBase) this).A0E));
        this.A02.setNavigationContentDescription(R.string.back);
        Toolbar toolbar4 = this.A02;
        toolbar4.A0R = new AbstractC017308c() { // from class: X.2TA
            @Override // X.AbstractC017308c
            public final boolean onMenuItemClick(MenuItem menuItem) {
                CameraMediaPickerFragment cameraMediaPickerFragment = CameraMediaPickerFragment.this;
                if (menuItem.getItemId() != R.id.menuitem_select_multiple) {
                    return false;
                }
                cameraMediaPickerFragment.A1Q(cameraMediaPickerFragment.A05);
                return true;
            }
        };
        toolbar4.setNavigationOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 0));
    }

    @Override // com.whatsapp.gallery.MediaGalleryFragmentBase
    public boolean A1L(AbstractC35611iN r5, AnonymousClass2T3 r6) {
        if (A1J()) {
            A1P(r5);
            return true;
        }
        HashSet hashSet = this.A05;
        Uri AAE = r5.AAE();
        hashSet.add(AAE);
        this.A04.A03(new C39341ph(AAE));
        A1M();
        ((MediaGalleryFragmentBase) this).A06.A02();
        A1F(hashSet.size());
        return true;
    }

    public final void A1M() {
        if (this.A02.getVisibility() != 0) {
            this.A02.setVisibility(0);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(120);
            this.A02.startAnimation(alphaAnimation);
        }
        this.A03.setVisibility(4);
        A1O();
        Window window = A0C().getWindow();
        AnonymousClass009.A05(window);
        C41691tw.A07(window, false);
        C41691tw.A02(A0C(), R.color.action_mode);
    }

    public final void A1N() {
        if (this.A02.getVisibility() != 4) {
            this.A02.setVisibility(4);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(120);
            this.A02.startAnimation(alphaAnimation);
        }
        this.A03.setVisibility(0);
        this.A05.clear();
        this.A04.A00.clear();
        ((MediaGalleryFragmentBase) this).A06.A02();
        AnonymousClass009.A05(A0C().getWindow());
        int i = Build.VERSION.SDK_INT;
        ActivityC000900k A0C = A0C();
        if (i >= 23) {
            C41691tw.A03(A0C, R.color.lightStatusBarBackgroundColor);
        } else {
            C41691tw.A02(A0C, R.color.primary);
        }
    }

    public final void A1O() {
        HashSet hashSet = this.A05;
        boolean isEmpty = hashSet.isEmpty();
        Toolbar toolbar = this.A02;
        if (isEmpty) {
            toolbar.setTitle(R.string.select_multiple_title);
        } else {
            toolbar.setTitle(((MediaGalleryFragmentBase) this).A0E.A0I(new Object[]{Integer.valueOf(hashSet.size())}, R.plurals.n_photos_selected, (long) hashSet.size()));
        }
        this.A01.setVisible(true ^ hashSet.isEmpty());
    }

    public final void A1P(AbstractC35611iN r8) {
        if (r8 == null) {
            return;
        }
        if (A1J()) {
            HashSet hashSet = this.A05;
            Uri AAE = r8.AAE();
            if (hashSet.contains(AAE)) {
                hashSet.remove(AAE);
            } else if (hashSet.size() < 30) {
                hashSet.add(AAE);
                this.A04.A03(new C39341ph(AAE));
            } else {
                ((MediaGalleryFragmentBase) this).A07.A0E(A01().getString(R.string.share_too_many_items_with_placeholder, 30), 0);
            }
            if (hashSet.isEmpty()) {
                A1N();
            } else {
                A1O();
                A1F(hashSet.size());
            }
            ((MediaGalleryFragmentBase) this).A06.A02();
            return;
        }
        HashSet hashSet2 = new HashSet();
        Uri AAE2 = r8.AAE();
        hashSet2.add(AAE2);
        this.A04.A03(new C39341ph(AAE2));
        A1Q(hashSet2);
    }

    public final void A1Q(HashSet hashSet) {
        AnonymousClass1s8 ABD;
        Bitmap bitmap;
        AbstractC35611iN r7;
        AnonymousClass2T3 A1A;
        if (!hashSet.isEmpty()) {
            ArrayList arrayList = new ArrayList(hashSet);
            ActivityC000900k A0B = A0B();
            if ((A0B instanceof AbstractC14710lv) && (ABD = ((AbstractC14710lv) A0B).ABD()) != null) {
                ArrayList arrayList2 = null;
                if (!AbstractC454421p.A00 || arrayList.size() != 1 || ((AnonymousClass01E) this).A0A == null || (A1A = A1A((Uri) arrayList.get(0))) == null) {
                    bitmap = null;
                    r7 = null;
                } else {
                    arrayList2 = new ArrayList();
                    arrayList2.add(new AnonymousClass01T(A1A, arrayList.get(0).toString()));
                    View findViewById = ((AnonymousClass01E) this).A0A.findViewById(R.id.gallery_header_transition);
                    arrayList2.add(new AnonymousClass01T(findViewById, AnonymousClass028.A0J(findViewById)));
                    View findViewById2 = ((AnonymousClass01E) this).A0A.findViewById(R.id.gallery_footer_transition);
                    arrayList2.add(new AnonymousClass01T(findViewById2, AnonymousClass028.A0J(findViewById2)));
                    View findViewById3 = ((AnonymousClass01E) this).A0A.findViewById(R.id.gallery_send_button_transition);
                    arrayList2.add(new AnonymousClass01T(findViewById3, AnonymousClass028.A0J(findViewById3)));
                    bitmap = A1A.A00;
                    r7 = A1A.A05;
                }
                ABD.A0F(bitmap, this, r7, arrayList, arrayList2);
            }
        }
    }

    @Override // X.AbstractC48852Ic
    public void AGY(C453421e r3, Collection collection) {
        collection.clear();
        collection.addAll(this.A05);
        C453421e r0 = this.A04;
        Map map = r3.A00;
        map.clear();
        map.putAll(r0.A00);
    }

    @Override // X.AbstractC48852Ic
    public void AZy() {
        A1H(false);
    }

    @Override // X.AbstractC48852Ic
    public void Acp(C453421e r4, Collection collection, Collection collection2) {
        HashSet hashSet = this.A05;
        if (!hashSet.isEmpty() || !collection2.isEmpty()) {
            hashSet.clear();
            hashSet.addAll(collection2);
            List list = this.A06;
            list.clear();
            list.addAll(collection);
            Map map = this.A04.A00;
            map.clear();
            map.putAll(r4.A00);
            if (hashSet.isEmpty()) {
                A1N();
            } else {
                A1M();
            }
            ((MediaGalleryFragmentBase) this).A06.A02();
        }
    }
}
