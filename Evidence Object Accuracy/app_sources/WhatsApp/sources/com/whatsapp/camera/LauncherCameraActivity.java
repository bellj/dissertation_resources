package com.whatsapp.camera;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2GJ;
import X.C12970iu;
import X.C16490p7;
import X.C18000rk;
import X.C18720su;
import X.C21200x2;
import X.C245115u;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* loaded from: classes2.dex */
public class LauncherCameraActivity extends CameraActivity {
    public boolean A00;

    public LauncherCameraActivity() {
        this(0);
    }

    public LauncherCameraActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 39);
    }

    @Override // X.AnonymousClass2GI, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((CameraActivity) this).A01 = (C18720su) A1M.A2c.get();
            ((CameraActivity) this).A06 = (C245115u) A1M.A7s.get();
            ((CameraActivity) this).A03 = (AnonymousClass2GJ) A1L.A0C.get();
            ((CameraActivity) this).A09 = A1L.A0J();
            ((CameraActivity) this).A08 = (C21200x2) A1M.A2q.get();
            ((CameraActivity) this).A07 = (WhatsAppLibLoader) A1M.ANa.get();
            ((CameraActivity) this).A05 = (C16490p7) A1M.ACJ.get();
            ((CameraActivity) this).A04 = C12970iu.A0Y(A1M);
            ((CameraActivity) this).A0A = C18000rk.A00(A1L.A15);
        }
    }
}
