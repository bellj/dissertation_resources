package com.whatsapp.camera;

import X.AbstractC14710lv;
import X.AbstractC14730lx;
import X.AnonymousClass00E;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01V;
import X.AnonymousClass1s8;
import X.AnonymousClass2GI;
import X.AnonymousClass2GJ;
import X.AnonymousClass2GK;
import X.C15890o4;
import X.C16490p7;
import X.C18720su;
import X.C21200x2;
import X.C245115u;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* loaded from: classes2.dex */
public class CameraActivity extends AnonymousClass2GI implements AbstractC14710lv, AbstractC14730lx {
    public AnonymousClass01E A00;
    public C18720su A01;
    public AnonymousClass1s8 A02;
    public AnonymousClass2GJ A03;
    public C15890o4 A04;
    public C16490p7 A05;
    public C245115u A06;
    public WhatsAppLibLoader A07;
    public C21200x2 A08;
    public AnonymousClass2GK A09;
    public AnonymousClass01H A0A;
    public boolean A0B;
    public final Rect A0C = new Rect();

    @Override // X.AbstractC14710lv
    public AnonymousClass1s8 ABD() {
        return this.A02;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC14730lx
    public void AUT() {
        this.A02.A0J.A0Z = false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 30) {
            if (i != 90) {
                super.onActivityResult(i, i2, intent);
            } else {
                this.A02.A0D(i, i2, intent);
            }
        } else if (i2 == -1) {
            this.A02.A08();
        } else {
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!this.A02.A0P()) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass1s8 r1 = this.A02;
        if (r1.A08 != null) {
            r1.A0C.A03(true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0024, code lost:
        if (r0 == null) goto L_0x0026;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r22) {
        /*
        // Method dump skipped, instructions count: 635
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.camera.CameraActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A02.A04();
        this.A01.A02().A00.A06(-1);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return this.A02.A0R(i) || super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return this.A02.A0S(i) || super.onKeyUp(i, keyEvent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A08.A00();
        this.A02.A05();
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A02.A0G(bundle);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A02.A06();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        AnonymousClass01E A0A = A0V().A0A("cameraMediaPickerFragment");
        if (A0A != null) {
            A0V().A0P(bundle, A0A, "cameraMediaPickerFragment");
        }
        this.A02.A0H(bundle);
    }
}
