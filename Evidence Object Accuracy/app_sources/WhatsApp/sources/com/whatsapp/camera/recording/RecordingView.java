package com.whatsapp.camera.recording;

import X.AnonymousClass028;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class RecordingView extends RelativeLayout {
    public TextView A00;
    public CircularProgressBar A01;

    public RecordingView(Context context) {
        super(context, null);
        A00();
    }

    public RecordingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        A00();
    }

    public RecordingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        RelativeLayout.inflate(getContext(), R.layout.recording_view, this);
        this.A01 = (CircularProgressBar) AnonymousClass028.A0D(this, R.id.recording_progress);
        this.A00 = (TextView) AnonymousClass028.A0D(this, R.id.recording_time);
        this.A01.setMax(100);
    }
}
