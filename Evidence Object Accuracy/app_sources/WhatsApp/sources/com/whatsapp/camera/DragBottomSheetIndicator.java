package com.whatsapp.camera;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

/* loaded from: classes2.dex */
public class DragBottomSheetIndicator extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public long A02;
    public long A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public boolean A06;
    public final Paint A07;
    public final Paint A08;
    public final Path A09;

    public DragBottomSheetIndicator(Context context) {
        super(context);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A07 = C12990iw.A0G(1);
        this.A08 = C12990iw.A0G(1);
        this.A09 = new Path();
        A00(context);
    }

    public DragBottomSheetIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A07 = C12990iw.A0G(1);
        this.A08 = C12990iw.A0G(1);
        this.A09 = new Path();
        A00(context);
    }

    public DragBottomSheetIndicator(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A07 = C12990iw.A0G(1);
        this.A08 = C12990iw.A0G(1);
        this.A09 = new Path();
        A00(context);
    }

    public DragBottomSheetIndicator(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A07 = C12990iw.A0G(1);
        this.A08 = C12990iw.A0G(1);
        this.A09 = new Path();
        A00(context);
    }

    public DragBottomSheetIndicator(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        Paint paint = this.A07;
        C12990iw.A13(paint);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        paint.setStrokeWidth(C12960it.A01(context) * 2.0f);
        paint.setColor(-1);
        Paint paint2 = this.A08;
        C12990iw.A13(paint2);
        paint2.setStrokeCap(Paint.Cap.ROUND);
        paint2.setStrokeJoin(Paint.Join.ROUND);
        paint2.setStrokeWidth(C12960it.A01(context) * 4.0f);
        paint2.setColor(855638016);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float elapsedRealtime = ((this.A00 - this.A01) * 1000.0f) / ((float) (SystemClock.elapsedRealtime() - this.A03));
        if (elapsedRealtime > 1.0f) {
            elapsedRealtime = 1.0f;
        } else if (elapsedRealtime < -1.0f) {
            elapsedRealtime = -1.0f;
        }
        if (elapsedRealtime > 0.0f) {
            elapsedRealtime /= 2.0f;
        }
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int A08 = C12980iv.A08(this);
        int A07 = C12980iv.A07(this);
        Path path = this.A09;
        path.reset();
        float f = ((float) ((A07 + paddingTop) >> 1)) + (((float) (A07 - paddingTop)) * elapsedRealtime);
        path.moveTo((float) paddingLeft, f);
        path.lineTo((float) ((paddingLeft + A08) >> 1), (float) paddingTop);
        path.lineTo((float) A08, f);
        canvas.drawPath(path, this.A08);
        canvas.drawPath(path, this.A07);
        canvas.translate(0.0f, C12990iw.A03(this));
        if (this.A06) {
            invalidate();
        }
    }

    public void setOffset(float f) {
        this.A03 = this.A02;
        this.A01 = this.A00;
        this.A02 = SystemClock.elapsedRealtime();
        this.A00 = f;
        invalidate();
    }

    public void setUpdating(boolean z) {
        this.A06 = z;
        if (z) {
            invalidate();
        }
    }
}
