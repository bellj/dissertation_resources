package com.whatsapp.camera;

import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.AnonymousClass3G9;
import X.AnonymousClass5IJ;
import X.C12960it;
import X.C12970iu;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import com.whatsapp.R;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class CameraLayout extends RelativeLayout implements AnonymousClass004 {
    public static final Set A0F = new AnonymousClass5IJ();
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public AnonymousClass2P7 A07;
    public boolean A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final Display A0D;
    public final Map A0E;

    public CameraLayout(Context context) {
        this(context, null);
    }

    public CameraLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CameraLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A08) {
            this.A08 = true;
            generatedComponent();
        }
        this.A0E = C12970iu.A11();
        RelativeLayout.inflate(context, R.layout.camera_layout, this);
        this.A0D = AnonymousClass01d.A02(context).getDefaultDisplay();
        int A01 = AnonymousClass3G9.A01(context, 2.0f);
        int i2 = A01 << 2;
        this.A0C = i2;
        this.A09 = A01 * 6;
        int i3 = i2 + i2;
        this.A0B = i3;
        this.A0A = i3 * 10;
    }

    public static final void A00(View view, Map map, int i, int i2, int i3, int i4) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(view.getLayoutParams());
        for (Object obj : A0F) {
            layoutParams.addRule(C12960it.A05(obj), 0);
        }
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            layoutParams.addRule(C12960it.A05(A15.getKey()), C12960it.A05(A15.getValue()));
        }
        layoutParams.setMargins(i, i2, i3, i4);
        view.setLayoutParams(layoutParams);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.RelativeLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        Map map;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        View view;
        int i12;
        int i13;
        super.onLayout(z, i, i2, i3, i4);
        if (this.A05 == null) {
            this.A05 = AnonymousClass028.A0D(this, R.id.shutter);
            this.A01 = AnonymousClass028.A0D(this, R.id.flash_btn);
            this.A06 = AnonymousClass028.A0D(this, R.id.switch_camera_btn);
            this.A04 = AnonymousClass028.A0D(this, R.id.recording_hint);
            this.A02 = AnonymousClass028.A0D(this, R.id.gallery_btn);
            this.A00 = AnonymousClass028.A0D(this, R.id.close_camera_btn);
            this.A03 = AnonymousClass028.A0D(this, R.id.select_multiple);
        }
        if (z) {
            int rotation = this.A0D.getRotation();
            if (rotation != 1) {
                View view2 = this.A04;
                if (rotation != 3) {
                    view2.setVisibility(0);
                    map = this.A0E;
                    map.clear();
                    Integer A0g = C12970iu.A0g();
                    Integer valueOf = Integer.valueOf((int) R.id.recording_hint);
                    map.put(A0g, valueOf);
                    map.put(14, -1);
                    A00(this.A05, map, 0, 0, 0, 0);
                    map.clear();
                    map.put(A0g, valueOf);
                    map.put(11, -1);
                    View view3 = this.A06;
                    i6 = this.A0C;
                    int i14 = this.A09;
                    i11 = i6;
                    A00(view3, map, i6, 0, i11, i14);
                    map.clear();
                    map.put(A0g, valueOf);
                    map.put(9, -1);
                    A00(this.A02, map, i6, 0, i11, i14);
                    map.clear();
                    map.put(10, -1);
                    map.put(11, -1);
                    A00(this.A01, map, i6, i6, i11, i6);
                    map.clear();
                    A00(this.A00, map, i6, i6, i11, i6);
                    map.clear();
                    map.put(11, -1);
                    map.put(12, -1);
                    view = this.A03;
                    i13 = this.A0A;
                    i12 = 0;
                    A00(view, map, i6, i12, i11, i13);
                }
                view2.setVisibility(8);
                map = this.A0E;
                map.clear();
                i10 = -1;
                map.put(9, -1);
                i9 = 15;
                map.put(15, -1);
                View view4 = this.A05;
                i6 = this.A0C;
                A00(view4, map, i6, 0, i6, 0);
                map.clear();
                map.put(12, -1);
                map.put(9, -1);
                View view5 = this.A06;
                i5 = this.A0B;
                A00(view5, map, i5, i5, i5, i5);
                map.clear();
                i7 = 10;
                map.put(10, -1);
                map.put(9, -1);
                A00(this.A02, map, i5, i5, i5, i5);
                map.clear();
                map.put(12, -1);
                i8 = 11;
            } else {
                this.A04.setVisibility(8);
                map = this.A0E;
                map.clear();
                i10 = -1;
                map.put(11, -1);
                i9 = 15;
                map.put(15, -1);
                View view6 = this.A05;
                i6 = this.A0C;
                A00(view6, map, i6, 0, i6, 0);
                map.clear();
                map.put(10, -1);
                map.put(11, -1);
                View view7 = this.A06;
                i5 = this.A0B;
                A00(view7, map, i5, i5, i5, i5);
                map.clear();
                i7 = 12;
                map.put(12, -1);
                map.put(11, -1);
                A00(this.A02, map, i5, i5, i5, i5);
                map.clear();
                map.put(10, -1);
                i8 = 9;
            }
            Integer valueOf2 = Integer.valueOf(i8);
            map.put(valueOf2, i10);
            i11 = i6;
            A00(this.A01, map, i6, i5, i11, i5);
            map.clear();
            map.put(i7, i10);
            map.put(valueOf2, i10);
            A00(this.A00, map, i6, i5, i11, i5);
            map.clear();
            map.put(valueOf2, i10);
            map.put(i9, i10);
            view = this.A03;
            i12 = 0;
            i13 = 0;
            A00(view, map, i6, i12, i11, i13);
        }
    }
}
