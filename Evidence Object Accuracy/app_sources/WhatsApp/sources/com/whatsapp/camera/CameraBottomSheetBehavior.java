package com.whatsapp.camera;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

/* loaded from: classes3.dex */
public class CameraBottomSheetBehavior extends BottomSheetBehavior {
    @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
    public void A0A(View view, View view2, CoordinatorLayout coordinatorLayout, int i) {
        if (this.A0B != 4) {
            super.A0A(view, view2, coordinatorLayout, i);
        }
    }
}
