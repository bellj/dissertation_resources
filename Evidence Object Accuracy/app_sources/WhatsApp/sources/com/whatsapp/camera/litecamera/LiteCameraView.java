package com.whatsapp.camera.litecamera;

import X.AbstractC14440lR;
import X.AbstractC467527b;
import X.AnonymousClass004;
import X.AnonymousClass1s9;
import X.AnonymousClass2AQ;
import X.AnonymousClass2P7;
import X.AnonymousClass3HX;
import X.AnonymousClass4K1;
import X.AnonymousClass4K2;
import X.AnonymousClass4K3;
import X.AnonymousClass4K4;
import X.AnonymousClass4NI;
import X.AnonymousClass61N;
import X.AnonymousClass643;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C129845yO;
import X.C130565zc;
import X.C21200x2;
import X.C64113Eh;
import X.C92614Wq;
import android.content.SharedPreferences;
import android.widget.FrameLayout;
import java.io.File;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class LiteCameraView extends FrameLayout implements AnonymousClass1s9, AnonymousClass004 {
    public AbstractC467527b A00;
    public C21200x2 A01;
    public AbstractC14440lR A02;
    public AnonymousClass2P7 A03;
    public String A04;
    public List A05;
    public List A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final SharedPreferences A0B;
    public final AnonymousClass61N A0C;
    public final AnonymousClass643 A0D;
    public final AnonymousClass3HX A0E;
    public final AnonymousClass4K1 A0F;
    public final AnonymousClass4K2 A0G;
    public final AnonymousClass4K3 A0H;
    public final C92614Wq A0I;
    public volatile boolean A0J;

    @Override // X.AnonymousClass1s9
    public int getCameraType() {
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ce, code lost:
        if (r0 <= 0) goto L_0x00d0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LiteCameraView(android.content.Context r10, int r11) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.camera.litecamera.LiteCameraView.<init>(android.content.Context, int):void");
    }

    public static final int A00(String str) {
        switch (str.hashCode()) {
            case 3551:
                if (str.equals("on")) {
                    return 1;
                }
                break;
            case 109935:
                if (str.equals("off")) {
                    return 0;
                }
                break;
            case 3005871:
                if (str.equals("auto")) {
                    return 2;
                }
                break;
        }
        throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("Not able to map app flash mode: ")));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01() {
        /*
            r4 = this;
            boolean r0 = r4.AJS()
            if (r0 == 0) goto L_0x006b
            java.util.List r0 = r4.A06
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0048
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "off"
            r2[r1] = r0
            r1 = 1
            java.lang.String r0 = "on"
            r2[r1] = r0
            java.util.List r0 = java.util.Arrays.asList(r2)
            java.util.List r0 = java.util.Collections.unmodifiableList(r0)
            r4.A06 = r0
        L_0x0025:
            java.util.List r3 = r4.getFlashModes()
            android.content.SharedPreferences r2 = r4.A0B
            java.lang.String r0 = r4.getFlashModesCountPrefKey()
            int r1 = X.C12970iu.A01(r2, r0)
            int r0 = r3.size()
            if (r1 == r0) goto L_0x0048
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r1 = r4.getFlashModesCountPrefKey()
            int r0 = r3.size()
            X.C12970iu.A1B(r2, r1, r0)
        L_0x0048:
            java.util.List r1 = r4.getFlashModes()
            java.lang.String r0 = r4.A04
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0058
            java.lang.String r0 = "off"
            r4.A04 = r0
        L_0x0058:
            X.643 r1 = r4.A0D
            java.lang.String r0 = r4.A04
            int r0 = A00(r0)
            r1.A0B(r0)
            r0 = 3
            boolean r0 = r1.A0S(r0)
            r4.A0A = r0
            return
        L_0x006b:
            java.util.List r0 = r4.A05
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0048
            boolean r0 = r4.AJS()
            if (r0 != 0) goto L_0x00ab
            r0 = 3
            java.util.ArrayList r3 = X.C12980iv.A0w(r0)
            java.lang.String r0 = "off"
            r3.add(r0)
            X.643 r2 = r4.A0D
            java.lang.String r1 = "on"
            int r0 = A00(r1)
            boolean r0 = r2.A0S(r0)
            if (r0 == 0) goto L_0x0094
            r3.add(r1)
        L_0x0094:
            java.lang.String r1 = "auto"
            int r0 = A00(r1)
            boolean r0 = r2.A0S(r0)
            if (r0 == 0) goto L_0x00a3
            r3.add(r1)
        L_0x00a3:
            java.util.List r0 = java.util.Collections.unmodifiableList(r3)
            r4.A05 = r0
            goto L_0x0025
        L_0x00ab:
            java.lang.String r0 = "Cannot create back camera flash list while in front camera"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.camera.litecamera.LiteCameraView.A01():void");
    }

    @Override // X.AnonymousClass1s9
    public void A7E() {
        C64113Eh r1 = this.A0E.A03;
        synchronized (r1) {
            r1.A00 = null;
        }
    }

    @Override // X.AnonymousClass1s9
    public void AA4(float f, float f2) {
        AnonymousClass643 r2 = this.A0D;
        r2.A0N(new AnonymousClass4K4(this));
        r2.A0E((int) f, (int) f2);
    }

    @Override // X.AnonymousClass1s9
    public boolean AJS() {
        return C12970iu.A1W(this.A0D.A01());
    }

    @Override // X.AnonymousClass1s9
    public boolean AJV() {
        return this.A0J;
    }

    @Override // X.AnonymousClass1s9
    public boolean AJy() {
        return this.A0D.A0P();
    }

    @Override // X.AnonymousClass1s9
    public boolean AK9() {
        return "torch".equals(this.A04);
    }

    @Override // X.AnonymousClass1s9
    public boolean ALa() {
        return AJS() && !this.A04.equals("off");
    }

    @Override // X.AnonymousClass1s9
    public void ALf() {
        AnonymousClass643 r1 = this.A0D;
        if (r1.A0Q()) {
            this.A0E.A01();
            r1.A0A();
        }
    }

    @Override // X.AnonymousClass1s9
    public String ALg() {
        List flashModes = getFlashModes();
        if (flashModes.isEmpty()) {
            return "off";
        }
        int indexOf = flashModes.indexOf(this.A04);
        if (indexOf < 0) {
            indexOf = flashModes.indexOf("off");
        }
        String A0g = C12960it.A0g(flashModes, (indexOf + 1) % flashModes.size());
        this.A04 = A0g;
        this.A0D.A0B(A00(A0g));
        return this.A04;
    }

    @Override // X.AnonymousClass1s9
    public void Aap() {
        if (this.A0J) {
            AbstractC467527b r0 = this.A00;
            if (r0 != null) {
                r0.AUF();
                return;
            }
            return;
        }
        Aar();
    }

    @Override // X.AnonymousClass1s9
    public void Aar() {
        AnonymousClass643 r1 = this.A0D;
        r1.A0O(this.A09);
        r1.A0J(this.A0F);
        r1.A0L(this.A0G);
        r1.A08();
        this.A0I.A01(10000);
    }

    @Override // X.AnonymousClass1s9
    public int AdD(int i) {
        AnonymousClass643 r0 = this.A0D;
        r0.A0D(i);
        return r0.A03();
    }

    @Override // X.AnonymousClass1s9
    public void AeK(File file, int i) {
        this.A0D.A0M(this.A0H, file);
    }

    @Override // X.AnonymousClass1s9
    public void AeT() {
        this.A0D.A09();
    }

    @Override // X.AnonymousClass1s9
    public boolean Aee() {
        return this.A0A;
    }

    @Override // X.AnonymousClass1s9
    public void Aei(AnonymousClass2AQ r4, boolean z) {
        C130565zc A00 = C130565zc.A00();
        A00.A01 = z;
        A00.A00 = true;
        this.A0D.A0F(A00, new AnonymousClass4NI(r4, this));
    }

    @Override // X.AnonymousClass1s9
    public void Af2() {
        String str;
        if (this.A0A) {
            boolean AK9 = AK9();
            AnonymousClass643 r1 = this.A0D;
            if (AK9) {
                r1.A0B(0);
                str = "off";
            } else {
                r1.A0B(3);
                str = "torch";
            }
            this.A04 = str;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // X.AnonymousClass1s9
    public int getCameraApi() {
        return this.A0D.A0R() ? 1 : 0;
    }

    @Override // X.AnonymousClass1s9
    public String getFlashMode() {
        return this.A04;
    }

    @Override // X.AnonymousClass1s9
    public List getFlashModes() {
        return AJS() ? this.A06 : this.A05;
    }

    private String getFlashModesCountPrefKey() {
        return C12960it.A0f(C12960it.A0k("flash_modes_count"), this.A0D.A01());
    }

    @Override // X.AnonymousClass1s9
    public int getMaxZoom() {
        return this.A0D.A02();
    }

    @Override // X.AnonymousClass1s9
    public int getNumberOfCameras() {
        return C12980iv.A03(this.A0D.A0Q() ? 1 : 0);
    }

    @Override // X.AnonymousClass1s9
    public long getPictureResolution() {
        C129845yO r0 = this.A0C.A00;
        if (r0 != null) {
            return (long) (r0.A02 * r0.A01);
        }
        return 0;
    }

    @Override // X.AnonymousClass1s9
    public int getStoredFlashModeCount() {
        return C12970iu.A01(this.A0B, getFlashModesCountPrefKey());
    }

    @Override // X.AnonymousClass1s9
    public long getVideoResolution() {
        C129845yO r0 = this.A0C.A02;
        if (r0 != null) {
            return (long) (r0.A02 * r0.A01);
        }
        return 0;
    }

    @Override // X.AnonymousClass1s9
    public void pause() {
        AnonymousClass643 r1 = this.A0D;
        r1.A07();
        r1.A0K(this.A0F);
        r1.A0L(null);
        r1.A0I(null);
        this.A0E.A01();
        this.A0J = false;
        this.A0I.A00();
    }

    @Override // X.AnonymousClass1s9
    public void setCameraCallback(AbstractC467527b r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1s9
    public void setQrDecodeHints(Map map) {
        this.A0E.A03.A02 = map;
    }

    @Override // X.AnonymousClass1s9
    public void setQrScanningEnabled(boolean z) {
        if (z != this.A09) {
            this.A09 = z;
            if (z) {
                AnonymousClass643 r2 = this.A0D;
                AnonymousClass3HX r1 = this.A0E;
                r2.A0I(r1.A01);
                if (!r1.A08) {
                    r1.A03.A01();
                    r1.A08 = true;
                    return;
                }
                return;
            }
            this.A0E.A01();
            this.A0D.A0I(null);
        }
    }
}
