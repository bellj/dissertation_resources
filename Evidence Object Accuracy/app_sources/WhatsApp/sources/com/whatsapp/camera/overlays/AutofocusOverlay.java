package com.whatsapp.camera.overlays;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class AutofocusOverlay extends View implements AnonymousClass004 {
    public float A00;
    public RectF A01;
    public AnonymousClass2P7 A02;
    public Boolean A03;
    public boolean A04;
    public final Paint A05;
    public final Runnable A06;

    public AutofocusOverlay(Context context) {
        super(context);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        this.A05 = new Paint(1);
        this.A06 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 34);
        A00(context);
    }

    public AutofocusOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A05 = new Paint(1);
        this.A06 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 34);
        A00(context);
    }

    public AutofocusOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        this.A05 = new Paint(1);
        this.A06 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 34);
        A00(context);
    }

    public AutofocusOverlay(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        this.A05 = new Paint(1);
        this.A06 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 34);
        A00(context);
    }

    public AutofocusOverlay(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        Paint paint = this.A05;
        paint.setStrokeWidth(getContext().getResources().getDimension(R.dimen.autofocus_stroke_size));
        paint.setStyle(Paint.Style.STROKE);
        this.A00 = context.getResources().getDimension(R.dimen.autofocus_box_size);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        Paint paint;
        int i;
        Boolean bool = this.A03;
        if (bool == null) {
            paint = this.A05;
            i = -1;
        } else {
            Boolean bool2 = Boolean.TRUE;
            paint = this.A05;
            i = -65536;
            if (bool == bool2) {
                i = -16711936;
            }
        }
        paint.setColor(i);
        canvas.drawRect(this.A01, paint);
    }
}
