package com.whatsapp.camera.overlays;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ZoomOverlay extends View implements AnonymousClass004 {
    public float A00;
    public AnonymousClass2P7 A01;
    public String A02;
    public boolean A03;
    public final Paint A04;
    public final RectF A05;
    public final TextPaint A06;
    public final Runnable A07;

    public ZoomOverlay(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new RectF();
        this.A04 = new Paint(1);
        this.A06 = new TextPaint(1);
        this.A07 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 35);
        A00(context);
    }

    public ZoomOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A05 = new RectF();
        this.A04 = new Paint(1);
        this.A06 = new TextPaint(1);
        this.A07 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 35);
        A00(context);
    }

    public ZoomOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new RectF();
        this.A04 = new Paint(1);
        this.A06 = new TextPaint(1);
        this.A07 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 35);
        A00(context);
    }

    public ZoomOverlay(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new RectF();
        this.A04 = new Paint(1);
        this.A06 = new TextPaint(1);
        this.A07 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 35);
        A00(context);
    }

    public ZoomOverlay(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        Paint paint = this.A04;
        paint.setStrokeWidth(context.getResources().getDimension(R.dimen.zoom_stroke_size));
        paint.setStyle(Paint.Style.STROKE);
        TextPaint textPaint = this.A06;
        textPaint.setTextSize(context.getResources().getDimension(R.dimen.zoom_text_size));
        textPaint.setColor(-1711276033);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setFakeBoldText(true);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public float getMaxScale() {
        return (((float) Math.min(getWidth() >> 1, getHeight() >> 1)) * 0.9f) / this.A06.measureText("x00.0");
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int width = getWidth() >> 1;
        int height = getHeight() >> 1;
        float min = ((float) Math.min(width, height)) * 0.9f;
        Paint paint = this.A04;
        paint.setColor(-1711276033);
        RectF rectF = this.A05;
        float f = (float) width;
        float f2 = (float) height;
        rectF.set(f - min, f2 - min, f + min, f2 + min);
        canvas.drawOval(rectF, paint);
        String str = this.A02;
        if (str != null) {
            TextPaint textPaint = this.A06;
            canvas.drawText(str, f, f2 - ((textPaint.descent() + textPaint.ascent()) / 2.0f), textPaint);
        }
        float measureText = this.A06.measureText("x00.0");
        rectF.set(f - measureText, f2 - measureText, f + measureText, f2 + measureText);
        canvas.drawOval(rectF, paint);
        float min2 = Math.min(min, measureText * this.A00);
        paint.setColor(-13388315);
        rectF.set(f - min2, f2 - min2, f + min2, f2 + min2);
        canvas.drawOval(rectF, paint);
    }
}
