package com.whatsapp.camera.overlays;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12990iw;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ShutterOverlay extends View implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public boolean A02;
    public final Paint A03;

    public ShutterOverlay(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = C12960it.A0A();
        A00(context);
    }

    public ShutterOverlay(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A03 = C12960it.A0A();
        A00(context);
    }

    public ShutterOverlay(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = C12960it.A0A();
        A00(context);
    }

    public ShutterOverlay(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = C12960it.A0A();
        A00(context);
    }

    public ShutterOverlay(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        Paint paint = this.A03;
        paint.setStrokeWidth(context.getResources().getDimension(R.dimen.shutter_stroke_size));
        C12990iw.A13(paint);
        paint.setColor(-1);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A02) {
            canvas.drawRect(0.0f, 0.0f, C12990iw.A02(this), C12990iw.A03(this), this.A03);
        }
    }
}
