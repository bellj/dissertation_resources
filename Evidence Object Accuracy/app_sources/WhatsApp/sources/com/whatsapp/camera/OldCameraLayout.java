package com.whatsapp.camera;

import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass2P7;
import X.C52362ag;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class OldCameraLayout extends FrameLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public AnonymousClass2P7 A05;
    public boolean A06;
    public final Display A07;
    public final C52362ag A08;

    public OldCameraLayout(Context context) {
        this(context, null);
    }

    public OldCameraLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public OldCameraLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
        FrameLayout.inflate(context, R.layout.camera_layout_old, this);
        this.A07 = AnonymousClass01d.A02(context).getDefaultDisplay();
        this.A08 = new C52362ag(context, this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0131, code lost:
        if (r20.A02.getVisibility() != 8) goto L_0x0133;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0133, code lost:
        r0 = r20.A02.getMeasuredWidth();
        r2 = r20.A02;
        r0 = r0 >> 1;
        r2.layout(r10 - r0, r8, r10 + r0, r2.getMeasuredHeight() + r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0148, code lost:
        r6 = r6 + r9;
        r20.A01.layout((r6 - r13) >> 1, r8, (r6 + r13) >> 1, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0155, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0198, code lost:
        if (r20.A02.getVisibility() == 0) goto L_0x0133;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(int r21, int r22, int r23, int r24, int r25) {
        /*
        // Method dump skipped, instructions count: 411
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.camera.OldCameraLayout.A00(int, int, int, int, int):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A08.enable();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A08.disable();
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        A00(this.A07.getRotation(), i, i2, i3, i4);
    }
}
