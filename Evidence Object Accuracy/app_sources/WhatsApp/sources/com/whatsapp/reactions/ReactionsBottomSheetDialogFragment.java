package com.whatsapp.reactions;

import X.AbstractC11920h5;
import X.AbstractC14640lm;
import X.AbstractC467627l;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass19O;
import X.AnonymousClass2VG;
import X.AnonymousClass3D7;
import X.AnonymousClass3FN;
import X.AnonymousClass3S8;
import X.AnonymousClass4KQ;
import X.AnonymousClass51F;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C16170oZ;
import X.C16370ot;
import X.C36161jQ;
import X.C40481rf;
import X.C53092ck;
import X.C58352og;
import X.C67413Rk;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;
import com.whatsapp.WaTabLayout;
import com.whatsapp.WaViewPager;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.reactions.ReactionsBottomSheetDialogFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class ReactionsBottomSheetDialogFragment extends Hilt_ReactionsBottomSheetDialogFragment {
    public AbstractC467627l A00 = new AnonymousClass51F(this);
    public AnonymousClass12P A01;
    public C15570nT A02;
    public C16170oZ A03;
    public WaTabLayout A04;
    public WaViewPager A05;
    public AnonymousClass130 A06;
    public C15550nR A07;
    public C15610nY A08;
    public AnonymousClass131 A09;
    public AnonymousClass4KQ A0A;
    public AnonymousClass018 A0B;
    public C16370ot A0C;
    public AbstractC14640lm A0D;
    public C40481rf A0E;
    public C58352og A0F;
    public AnonymousClass19O A0G;
    public boolean A0H;

    public static /* synthetic */ void A00(AnonymousClass3FN r2, ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment) {
        int A0O;
        if (r2.A00 >= reactionsBottomSheetDialogFragment.A0F.A01()) {
            A0O = 0;
        } else {
            A0O = reactionsBottomSheetDialogFragment.A05.A0O(r2.A00);
        }
        reactionsBottomSheetDialogFragment.A05.A0P(A0O);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.reactions_bottom_sheet, viewGroup, true);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        Window window = A19().getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.dimAmount = 0.3f;
            window.setAttributes(attributes);
        }
        AnonymousClass2VG r10 = (AnonymousClass2VG) new AnonymousClass02A(new C67413Rk(this.A03, this.A0C, this.A0D, this.A0E, this.A0G, this.A0H), this).A00(AnonymousClass2VG.class);
        this.A04 = (WaTabLayout) AnonymousClass028.A0D(view, R.id.reactions_bottom_sheet_tab_layout);
        this.A05 = (WaViewPager) AnonymousClass028.A0D(view, R.id.reactions_bottom_sheet_view_pager);
        C15570nT r4 = this.A02;
        AnonymousClass130 r5 = this.A06;
        C15550nR r6 = this.A07;
        C15610nY r7 = this.A08;
        AnonymousClass018 r9 = this.A0B;
        C58352og r1 = new C58352og(A01(), A0G(), r4, r5, r6, r7, this.A09, r9, r10);
        this.A0F = r1;
        this.A05.setAdapter(r1);
        this.A05.A0H(new AbstractC11920h5() { // from class: X.4uZ
            @Override // X.AbstractC11920h5
            public final void Af5(View view2, float f) {
                boolean z = true;
                if (f != 0.0f) {
                    if (f == 1.0f || f == -1.0f) {
                        z = false;
                    } else {
                        return;
                    }
                }
                AnonymousClass028.A0m(view2, z);
                view2.requestLayout();
            }
        }, false);
        this.A05.A0G(new AnonymousClass3S8(this.A04));
        this.A04.post(new RunnableBRunnable0Shape10S0100000_I0_10(this, 27));
        C36161jQ r52 = r10.A05;
        r52.A05(A0G(), new AnonymousClass02B(r10) { // from class: X.3RO
            public final /* synthetic */ AnonymousClass2VG A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                int i;
                ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = ReactionsBottomSheetDialogFragment.this;
                int i2 = this.A01.A00;
                WaTabLayout waTabLayout = reactionsBottomSheetDialogFragment.A04;
                if (waTabLayout.A0c.size() > 0 && i2 >= 0) {
                    ArrayList arrayList = waTabLayout.A0c;
                    if (i2 < arrayList.size()) {
                        int A0I = waTabLayout.A0I(i2, false);
                        AnonymousClass3FN r0 = waTabLayout.A0O;
                        if (r0 != null) {
                            i = r0.A00;
                        } else {
                            i = 0;
                        }
                        AnonymousClass2cj r02 = waTabLayout.A0a;
                        C53092ck r12 = (C53092ck) r02.getChildAt(A0I);
                        r02.removeViewAt(A0I);
                        if (r12 != null) {
                            r12.setTab(null);
                            r12.setSelected(false);
                            waTabLayout.A0Z.Aa6(r12);
                        }
                        waTabLayout.requestLayout();
                        AnonymousClass3FN r2 = (AnonymousClass3FN) arrayList.remove(A0I);
                        if (r2 != null) {
                            r2.A03 = null;
                            r2.A02 = null;
                            r2.A06 = null;
                            r2.A05 = null;
                            r2.A04 = null;
                            r2.A00 = -1;
                            r2.A01 = null;
                            TabLayout.A0d.Aa6(r2);
                        }
                        int size = arrayList.size();
                        for (int i3 = A0I; i3 < size; i3++) {
                            ((AnonymousClass3FN) arrayList.get(i3)).A00 = i3;
                        }
                        if (i == A0I) {
                            AnonymousClass3FN r13 = null;
                            if (!arrayList.isEmpty()) {
                                r13 = (AnonymousClass3FN) arrayList.get(Math.max(0, A0I - 1));
                            }
                            waTabLayout.A0G(r13, true);
                        }
                    }
                }
            }
        });
        LayoutInflater from = LayoutInflater.from(A0p());
        r10.A03.A02.A05(A0G(), new AnonymousClass02B(from, this) { // from class: X.3RN
            public final /* synthetic */ LayoutInflater A00;
            public final /* synthetic */ ReactionsBottomSheetDialogFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = this.A01;
                LayoutInflater layoutInflater = this.A00;
                List list = (List) obj;
                if (list.isEmpty()) {
                    reactionsBottomSheetDialogFragment.A1C();
                    return;
                }
                AnonymousClass018 r102 = reactionsBottomSheetDialogFragment.A0B;
                int size = list.size();
                Context context = layoutInflater.getContext();
                View A0O = C12980iv.A0O(layoutInflater, R.layout.reactions_bottom_sheet_tab);
                TextView A0I = C12960it.A0I(A0O, R.id.reactions_bottom_sheet_tab_counter_text);
                Resources resources = context.getResources();
                Object[] A1b = C12970iu.A1b();
                A1b[0] = AnonymousClass3J7.A02(context, r102, size);
                C12980iv.A15(resources, A0I, A1b, R.plurals.reactions_bottom_sheet_all_tab_title, size);
                String A02 = AnonymousClass3J7.A02(context, r102, size);
                Resources resources2 = context.getResources();
                Object[] A1b2 = C12970iu.A1b();
                A1b2[0] = A02;
                A0O.setContentDescription(resources2.getQuantityString(R.plurals.reactions_a11y_all_tab_content_description, size, A1b2));
                reactionsBottomSheetDialogFragment.A1M(A0O, 0);
            }
        });
        for (AnonymousClass3D7 r3 : (List) r52.A01()) {
            r3.A02.A05(A0G(), new AnonymousClass02B(from, this, r3) { // from class: X.3RV
                public final /* synthetic */ LayoutInflater A00;
                public final /* synthetic */ ReactionsBottomSheetDialogFragment A01;
                public final /* synthetic */ AnonymousClass3D7 A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = this.A01;
                    AnonymousClass3D7 r2 = this.A02;
                    LayoutInflater layoutInflater = this.A00;
                    List list = (List) obj;
                    if (!list.isEmpty()) {
                        int i = r2.A00;
                        AnonymousClass018 r32 = reactionsBottomSheetDialogFragment.A0B;
                        String str = r2.A03;
                        int size = list.size();
                        Context context = layoutInflater.getContext();
                        View A0O = C12980iv.A0O(layoutInflater, R.layout.reactions_bottom_sheet_tab);
                        C12970iu.A0T(A0O, R.id.reactions_bottom_sheet_tab_emoji_text).A0G(null, str);
                        C12960it.A0I(A0O, R.id.reactions_bottom_sheet_tab_counter_text).setText(AnonymousClass3J7.A02(context, r32, size));
                        String A02 = AnonymousClass3J7.A02(context, r32, size);
                        Resources resources = context.getResources();
                        Object[] A1a = C12980iv.A1a();
                        C12970iu.A1U(A02, str, A1a);
                        A0O.setContentDescription(resources.getQuantityString(R.plurals.reactions_a11y_emoji_tab_content_description, size, A1a));
                        reactionsBottomSheetDialogFragment.A1M(A0O, i);
                    }
                }
            });
        }
        r52.A05(A0G(), new AnonymousClass02B() { // from class: X.4tP
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = ReactionsBottomSheetDialogFragment.this;
                reactionsBottomSheetDialogFragment.A04.setupTabsForAccessibility(reactionsBottomSheetDialogFragment.A05);
            }
        });
        r10.A06.A05(A0G(), new AnonymousClass02B() { // from class: X.3Qb
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Number number = (Number) obj;
                AnonymousClass4KQ r0 = ReactionsBottomSheetDialogFragment.this.A0A;
                if (r0 != null) {
                    long longValue = number.longValue();
                    AnonymousClass2y2 r53 = r0.A00;
                    List list = r53.A05;
                    if (list != null) {
                        int i = 0;
                        Iterator it = list.iterator();
                        while (it.hasNext() && longValue != C12980iv.A0f(it).A11) {
                            i++;
                        }
                        Intent A1N = r53.A1N();
                        if (AbstractC454421p.A00) {
                            A1N.putExtra("start_index", i);
                        }
                        C12990iw.A10(A1N, r53);
                    }
                }
            }
        });
        r10.A07.A05(A0G(), new AnonymousClass02B() { // from class: X.3Qa
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = ReactionsBottomSheetDialogFragment.this;
                UserJid of = UserJid.of((Jid) obj);
                if (of != null) {
                    reactionsBottomSheetDialogFragment.A01.A06(reactionsBottomSheetDialogFragment.A0p(), C14960mK.A0R(reactionsBottomSheetDialogFragment.A0p(), of, null, true));
                }
            }
        });
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment
    public void A1L(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = A02().getDimensionPixelSize(R.dimen.reactions_bottom_sheet_height);
        view.setLayoutParams(layoutParams);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        A00.A0L(layoutParams.height);
        A00.A0M(3);
    }

    public final void A1M(View view, int i) {
        AnonymousClass3FN A0J = this.A04.A0J(i);
        if (A0J == null) {
            AnonymousClass3FN A03 = this.A04.A03();
            A03.A01 = view;
            C53092ck r0 = A03.A02;
            if (r0 != null) {
                r0.A00();
            }
            WaTabLayout waTabLayout = this.A04;
            waTabLayout.A0F(A03, waTabLayout.A0I(i, true), waTabLayout.A0c.isEmpty());
            return;
        }
        A0J.A01 = null;
        C53092ck r02 = A0J.A02;
        if (r02 != null) {
            r02.A00();
        }
        A0J.A01 = view;
        C53092ck r03 = A0J.A02;
        if (r03 != null) {
            r03.A00();
        }
    }
}
