package com.whatsapp.reactions;

import X.AbstractC116775Wt;
import X.C12980iv;
import X.C12990iw;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes2.dex */
public class ReactionEmojiTextView extends TextEmojiLabel implements AbstractC116775Wt {
    public float A00 = 1.0f;
    public float A01;
    public float A02 = 1.0f;
    public int A03;
    public Paint A04;

    public ReactionEmojiTextView(Context context) {
        super(context);
        A02();
    }

    public ReactionEmojiTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public ReactionEmojiTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }

    private void A02() {
        Paint A0F = C12990iw.A0F();
        this.A04 = A0F;
        C12980iv.A12(getContext(), A0F, R.color.reactions_tray_item_bg);
        this.A03 = this.A04.getAlpha();
        this.A01 = ((float) getResources().getDimensionPixelSize(R.dimen.reaction_tray_item_bg_size)) / 2.0f;
    }

    @Override // com.whatsapp.TextEmojiLabel, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        if (isSelected()) {
            canvas.save();
            float f = this.A02;
            canvas.scale(f, f, getPivotX(), getPivotY());
            canvas.drawCircle(getPivotX(), getPivotY(), this.A01, this.A04);
            canvas.restore();
        }
        canvas.save();
        float f2 = this.A00;
        canvas.scale(f2, f2, getPivotX(), getPivotY());
        super.onDraw(canvas);
        canvas.restore();
    }

    @Override // X.AbstractC116775Wt
    public void setBackgroundAlpha(float f) {
        this.A04.setAlpha((int) (f * ((float) this.A03)));
        invalidate();
    }

    @Override // X.AbstractC116775Wt
    public void setBackgroundScale(float f) {
        this.A02 = f;
        invalidate();
    }

    public void setForegroundAlpha(float f) {
        getPaint().setAlpha((int) (f * 255.0f));
        invalidate();
    }

    @Override // X.AbstractC116775Wt
    public void setForegroundScale(float f) {
        this.A00 = f;
        invalidate();
    }
}
