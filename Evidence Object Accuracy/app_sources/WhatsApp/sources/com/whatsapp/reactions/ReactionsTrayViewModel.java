package com.whatsapp.reactions;

import X.AbstractC15340mz;
import X.AnonymousClass015;
import X.AnonymousClass4X9;
import X.C14850m9;
import X.C15570nT;
import X.C16630pM;
import X.C26661Ei;
import X.C36161jQ;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class ReactionsTrayViewModel extends AnonymousClass015 {
    public static final int A0B;
    public static final List A0C;
    public int A00;
    public int A01 = 0;
    public AbstractC15340mz A02;
    public List A03 = A0C;
    public boolean A04;
    public final C15570nT A05;
    public final C14850m9 A06;
    public final C16630pM A07;
    public final C26661Ei A08;
    public final C36161jQ A09 = new C36161jQ(0);
    public final C36161jQ A0A = new C36161jQ(new AnonymousClass4X9(null, null, false));

    static {
        List asList = Arrays.asList("👍", "❤️", "😂", "😮", "😢", "🙏");
        A0C = asList;
        A0B = asList.size();
    }

    public ReactionsTrayViewModel(C15570nT r4, C14850m9 r5, C16630pM r6, C26661Ei r7) {
        this.A06 = r5;
        this.A05 = r4;
        this.A08 = r7;
        this.A07 = r6;
    }

    public void A04(int i) {
        if (i == 0) {
            boolean z = false;
            if (((Number) this.A09.A01()).intValue() == 2) {
                z = true;
            }
            this.A04 = z;
        }
        C36161jQ r1 = this.A09;
        if (((Number) r1.A01()).intValue() == i) {
            return;
        }
        if (i != 1) {
            r1.A0B(Integer.valueOf(i));
            return;
        }
        throw new IllegalArgumentException("Use setDisplayStateToTrayOpen instead of setDisplayState(DisplayState.TRAY_OPEN)");
    }

    public void A05(String str) {
        A04(0);
        C36161jQ r3 = this.A0A;
        if (!str.equals(((AnonymousClass4X9) r3.A01()).A00)) {
            r3.A0B(new AnonymousClass4X9(((AnonymousClass4X9) r3.A01()).A00, str, true));
        }
    }
}
