package com.whatsapp.reactions;

import X.AbstractC116775Wt;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass00X;
import X.AnonymousClass23N;
import X.C015607k;
import X.C12960it;
import X.C12980iv;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ReactionPlusView extends View implements AbstractC116775Wt {
    public float A00;
    public float A01 = 0.625f;
    public float A02 = 1.0f;
    public int A03;
    public int A04;
    public Drawable A05;
    public final Paint A06 = C12960it.A0A();

    public ReactionPlusView(Context context) {
        super(context);
        A00();
    }

    public ReactionPlusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public ReactionPlusView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public final void A00() {
        this.A04 = getResources().getDimensionPixelSize(R.dimen.reaction_tray_keyboard_plus_size);
        this.A00 = ((float) getResources().getDimensionPixelSize(R.dimen.reaction_tray_item_bg_size)) / 2.0f;
        Drawable A04 = AnonymousClass00X.A04(C12980iv.A0I(this), getResources(), R.drawable.ic_reactions_plus);
        AnonymousClass009.A05(A04);
        this.A05 = A04;
        Drawable A03 = C015607k.A03(A04);
        this.A05 = A03;
        C015607k.A0A(A03, AnonymousClass00T.A00(getContext(), R.color.reactions_plus_tint));
        Paint paint = this.A06;
        C12980iv.A12(getContext(), paint, R.color.reactions_plus_background_color);
        this.A03 = paint.getAlpha();
        AnonymousClass23N.A01(this);
        C12960it.A0r(getContext(), this, R.string.reactions_a11y_plus_view_content_description);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        float f = this.A01;
        canvas.scale(f, f, getPivotX(), getPivotY());
        canvas.drawCircle(getPivotX(), getPivotY(), this.A00, this.A06);
        canvas.restore();
        canvas.save();
        float f2 = this.A02;
        canvas.scale(f2, f2, getPivotX(), getPivotY());
        this.A05.draw(canvas);
        canvas.restore();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        int i5 = i >> 1;
        int i6 = i2 >> 1;
        int i7 = this.A04 >> 1;
        this.A05.setBounds(i5 - i7, i6 - i7, i5 + i7, i6 + i7);
    }

    @Override // X.AbstractC116775Wt
    public void setBackgroundAlpha(float f) {
        this.A06.setAlpha((int) (((float) this.A03) * f));
        invalidate();
    }

    @Override // X.AbstractC116775Wt
    public void setBackgroundScale(float f) {
        this.A01 = f * 0.625f;
        invalidate();
    }

    public void setForegroundAlpha(float f) {
        this.A05.setAlpha((int) (f * 255.0f));
        invalidate();
    }

    @Override // X.AbstractC116775Wt
    public void setForegroundScale(float f) {
        this.A02 = f;
        invalidate();
    }
}
