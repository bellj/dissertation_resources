package com.whatsapp;

import X.AnonymousClass180;
import X.AnonymousClass182;
import X.C51122Sx;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.RoundedBottomSheetDialogFragment;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.languageselector.LanguageSelectorBottomSheet;

/* loaded from: classes2.dex */
public abstract class RoundedBottomSheetDialogFragment extends Hilt_RoundedBottomSheetDialogFragment {
    public AnonymousClass182 A00;
    public AnonymousClass180 A01;

    @Override // X.AnonymousClass01E
    public void A0n(boolean z) {
        C51122Sx.A02(this, this.A00, this.A01, this.A0j, z);
        super.A0n(z);
    }

    @Override // androidx.fragment.app.DialogFragment
    public int A18() {
        if (this instanceof LanguageSelectorBottomSheet) {
            return R.style.LanguageSelectorBottomSheetDialogTheme;
        }
        if (this instanceof CartFragment) {
            return R.style.CartFragmentTheme;
        }
        if (!(this instanceof WAChatIntroBottomSheet)) {
            return R.style.RoundedBottomSheetDialogTheme;
        }
        return R.style.WACIntroBottomSheetTheme;
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog A1A = super.A1A(bundle);
        A1A.setOnShowListener(new DialogInterface.OnShowListener(A1A, this) { // from class: X.4hZ
            public final /* synthetic */ Dialog A00;
            public final /* synthetic */ RoundedBottomSheetDialogFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                this.A01.A1L(AnonymousClass0KS.A00(this.A00, R.id.design_bottom_sheet));
            }
        });
        return A1A;
    }

    public int A1K() {
        Point point = new Point();
        A0C().getWindowManager().getDefaultDisplay().getSize(point);
        Rect rect = new Rect();
        A0C().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return point.y - rect.top;
    }

    public void A1L(View view) {
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(view);
        A00.A0M(3);
        A00.A0N = true;
        A00.A0L(view.getHeight());
    }
}
