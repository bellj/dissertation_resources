package com.whatsapp.calling;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass1BA;
import X.AnonymousClass5V7;
import X.C21740xu;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.calling.VoipAppUpdateActivity;

/* loaded from: classes2.dex */
public class VoipAppUpdateActivity extends ActivityC13830kP {
    public C21740xu A00;
    public AnonymousClass1BA A01;
    public boolean A02;
    public final AnonymousClass5V7 A03;

    public VoipAppUpdateActivity() {
        this(0);
        this.A03 = new AnonymousClass5V7() { // from class: X.5Ar
            @Override // X.AnonymousClass5V7
            public final void A9y() {
                VoipAppUpdateActivity.this.finish();
            }
        };
    }

    public VoipAppUpdateActivity(int i) {
        this.A02 = false;
        ActivityC13830kP.A1P(this, 31);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            this.A00 = (C21740xu) A1M.AM1.get();
            this.A01 = (AnonymousClass1BA) A1M.A2e.get();
        }
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActivityC13830kP.A1O(this);
        setContentView(R.layout.voip_app_update_dialog);
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass00T.A05(this, R.id.cancel), this, 31);
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass00T.A05(this, R.id.upgrade), this, 32);
        AnonymousClass1BA r0 = this.A01;
        r0.A00.add(this.A03);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1BA r0 = this.A01;
        r0.A00.remove(this.A03);
    }
}
