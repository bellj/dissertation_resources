package com.whatsapp.calling.views;

import X.AbstractC115955To;
import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C06260Su;
import X.C12960it;
import X.C53692es;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class CallResponseLayout extends FrameLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public AbstractC115955To A02;
    public AnonymousClass01d A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final ViewConfiguration A08;
    public final C06260Su A09;

    public CallResponseLayout(Context context) {
        this(context, null);
    }

    public CallResponseLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A09 = new C06260Su(getContext(), this, new C53692es(this));
        this.A08 = ViewConfiguration.get(getContext());
    }

    public CallResponseLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A05) {
            this.A05 = true;
            this.A03 = C12960it.A0Q(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // android.view.View
    public void computeScroll() {
        super.computeScroll();
        if (this.A09.A0A()) {
            postInvalidateOnAnimation();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 3 && actionMasked != 1) {
            return this.A09.A0E(motionEvent);
        }
        this.A09.A02();
        return false;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.A09.A07(motionEvent);
        return true;
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        int height;
        super.onVisibilityChanged(view, i);
        if (this.A01 != null && i == 0 && (height = (getHeight() - this.A01.getHeight()) - ((int) this.A01.getY())) != 0) {
            AnonymousClass028.A0Y(this.A01, height);
        }
    }

    public void setCallResponseSwipeUpHintView(View view) {
        this.A00 = view;
    }

    public void setCallResponseView(View view) {
        this.A01 = view;
    }

    public void setResponseListener(AbstractC115955To r1) {
        this.A02 = r1;
    }

    public void setShowSwipeUpHintByDefault(boolean z) {
        this.A06 = z;
    }

    public void setTouchDownAfterDrag(boolean z) {
        this.A07 = z;
    }
}
