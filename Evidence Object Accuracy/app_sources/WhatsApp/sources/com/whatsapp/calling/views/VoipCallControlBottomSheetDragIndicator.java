package com.whatsapp.calling.views;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass23N;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4G9;
import X.AnonymousClass5RP;
import X.C1101554l;
import X.C64523Fw;
import X.View$OnTouchListenerC101334nM;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class VoipCallControlBottomSheetDragIndicator extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public int A02;
    public AnonymousClass5RP A03;
    public AnonymousClass018 A04;
    public AnonymousClass2P7 A05;
    public boolean A06;
    public boolean A07;
    public final Paint A08;
    public final Path A09;

    public VoipCallControlBottomSheetDragIndicator(Context context) {
        super(context);
        A00();
        this.A08 = new Paint(1);
        this.A09 = new Path();
        this.A01 = -1.0f;
    }

    public VoipCallControlBottomSheetDragIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A08 = new Paint(1);
        this.A09 = new Path();
        this.A01 = -1.0f;
        A01(context, attributeSet);
    }

    public VoipCallControlBottomSheetDragIndicator(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A08 = new Paint(1);
        this.A09 = new Path();
        this.A01 = -1.0f;
        A01(context, attributeSet);
    }

    public VoipCallControlBottomSheetDragIndicator(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A08 = new Paint(1);
        this.A09 = new Path();
        this.A01 = -1.0f;
        A01(context, attributeSet);
    }

    public VoipCallControlBottomSheetDragIndicator(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A07) {
            this.A07 = true;
            this.A04 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    /* JADX INFO: finally extract failed */
    public final void A01(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4G9.A00);
            try {
                int color = obtainStyledAttributes.getColor(0, -1);
                obtainStyledAttributes.recycle();
                Paint paint = this.A08;
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeCap(Paint.Cap.ROUND);
                paint.setStrokeWidth(context.getResources().getDisplayMetrics().density * 4.0f);
                paint.setColor(color);
                this.A00 = TypedValue.applyDimension(1, 2.5f, getResources().getDisplayMetrics());
                AnonymousClass23N.A01(this);
                Context context2 = getContext();
                int i = (this.A01 > 0.0f ? 1 : (this.A01 == 0.0f ? 0 : -1));
                int i2 = R.string.voip_joinable_expanding_participant_list_description;
                if (i > 0) {
                    i2 = R.string.voip_joinable_minimize_participant_list_description;
                }
                setContentDescription(context2.getString(i2));
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        }
    }

    public final boolean A02(int i) {
        AnonymousClass5RP r0;
        if (i == 0) {
            this.A06 = true;
            return false;
        } else if (i != 1) {
            if (i != 2) {
                this.A06 = false;
            }
            return false;
        } else {
            if (this.A06 && (r0 = this.A03) != null) {
                float f = this.A01;
                C64523Fw r2 = ((C1101554l) r0).A00.A0F;
                if (r2 != null) {
                    int i2 = 3;
                    if (f > 0.0f) {
                        i2 = 4;
                    }
                    r2.A02(i2);
                }
            }
            this.A06 = false;
            return true;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float paddingLeft = (float) getPaddingLeft();
        float paddingTop = (float) getPaddingTop();
        float width = (float) (getWidth() - getPaddingRight());
        float height = (float) (getHeight() - getPaddingBottom());
        float f = (height + paddingTop) / 2.0f;
        float f2 = this.A01;
        float f3 = (((height - paddingTop) / 2.0f) * f2) + f;
        float f4 = f + (this.A00 * (-f2));
        Path path = this.A09;
        path.reset();
        path.moveTo(paddingLeft, f4);
        path.lineTo((paddingLeft + width) / 2.0f, f3);
        path.lineTo(width, f4);
        canvas.drawPath(path, this.A08);
    }

    public void setOnClickListener(AnonymousClass5RP r2) {
        this.A03 = r2;
        super.setOnTouchListener(new View$OnTouchListenerC101334nM(this));
    }

    public void setSlideOffset(float f) {
        float f2;
        if (f > 1.0f) {
            f2 = 1.0f;
        } else {
            f2 = f <= 0.0f ? -1.0f : (f - 0.5f) * 2.0f;
        }
        this.A01 = f2;
        if (f2 == 1.0f || f2 == -1.0f) {
            Context context = getContext();
            int i = (this.A01 > 0.0f ? 1 : (this.A01 == 0.0f ? 0 : -1));
            int i2 = R.string.voip_joinable_expanding_participant_list_description;
            if (i > 0) {
                i2 = R.string.voip_joinable_minimize_participant_list_description;
            }
            setContentDescription(context.getString(i2));
        }
        invalidate();
    }
}
