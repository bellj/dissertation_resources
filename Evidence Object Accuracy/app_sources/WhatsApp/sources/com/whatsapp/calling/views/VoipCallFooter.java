package com.whatsapp.calling.views;

import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C14850m9;
import X.C15450nH;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class VoipCallFooter extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public View A02;
    public ImageButton A03;
    public ImageButton A04;
    public ImageButton A05;
    public ImageButton A06;
    public ImageButton A07;
    public C15450nH A08;
    public C14820m6 A09;
    public C14850m9 A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;

    public VoipCallFooter(Context context) {
        this(context, null);
    }

    public VoipCallFooter(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VoipCallFooter(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A08 = (C15450nH) A00.AII.get();
            this.A0A = C12960it.A0S(A00);
            this.A09 = C12970iu.A0Z(A00);
        }
        LayoutInflater.from(context).inflate(R.layout.voip_call_control_sheet_footer, (ViewGroup) this, true);
        this.A06 = (ImageButton) findViewById(R.id.speaker_btn);
        this.A03 = (ImageButton) findViewById(R.id.bluetooth_btn);
        this.A00 = findViewById(R.id.bluetooth_btn_layout);
        this.A07 = (ImageButton) findViewById(R.id.toggle_video_btn);
        this.A02 = findViewById(R.id.toggle_video_btn_layout);
        this.A05 = (ImageButton) findViewById(R.id.mute_btn);
        this.A04 = (ImageButton) AnonymousClass028.A0D(this, R.id.footer_end_call_btn);
        this.A01 = AnonymousClass028.A0D(this, R.id.end_call_btn_layout);
    }

    public void A00() {
        this.A06.setImageResource(R.drawable.ic_voip_speaker_control);
        ImageButton imageButton = this.A07;
        imageButton.setImageResource(R.drawable.ic_voip_video_control);
        C12960it.A0r(getContext(), imageButton, R.string.voip_call_turn_on_video_btn_description);
        A03(false);
    }

    public void A01() {
        this.A06.setImageResource(R.drawable.ic_voip_switch_camera_control);
        ImageButton imageButton = this.A07;
        imageButton.setImageResource(R.drawable.ic_voip_switch_to_voice_control);
        C12960it.A0r(getContext(), imageButton, R.string.voip_call_turn_off_video_btn_description);
        A03(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r11.videoEnabled != false) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        if (r13 != false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0065, code lost:
        if (com.whatsapp.voipcalling.Voip.getCameraCount() <= 1) goto L_0x0067;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(com.whatsapp.voipcalling.CallInfo r11, int r12, boolean r13) {
        /*
            r10 = this;
            com.whatsapp.voipcalling.Voip$CallState r4 = r11.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.NONE
            if (r4 == r0) goto L_0x0050
            boolean r0 = com.whatsapp.voipcalling.Voip.A0A(r4)
            if (r0 != 0) goto L_0x0050
            X.0m6 r1 = r10.A09
            X.0m9 r0 = r10.A0A
            boolean r0 = X.AnonymousClass1SF.A0M(r1, r0)
            if (r0 == 0) goto L_0x0025
            boolean r0 = r11.isGroupCall()
            if (r0 != 0) goto L_0x0020
            java.lang.String r0 = r11.callLinkToken
            if (r0 == 0) goto L_0x0025
        L_0x0020:
            boolean r0 = r11.videoEnabled
            r2 = 1
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            r2 = 0
        L_0x0026:
            android.view.View r1 = r10.A02
            r5 = 8
            r3 = 0
            r0 = 0
            if (r2 == 0) goto L_0x0030
            r0 = 8
        L_0x0030:
            r1.setVisibility(r0)
            if (r2 == 0) goto L_0x0039
            r0 = 1082130432(0x40800000, float:4.0)
            if (r13 == 0) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            r10.setWeightSum(r0)
            boolean r0 = r11.isCallLinkLobbyState
            if (r0 == 0) goto L_0x0051
            android.widget.ImageButton r0 = r10.A06
            X.C65293Iy.A04(r0, r3)
            android.widget.ImageButton r0 = r10.A07
            X.C65293Iy.A04(r0, r3)
            android.widget.ImageButton r0 = r10.A05
            X.C65293Iy.A04(r0, r3)
        L_0x0050:
            return
        L_0x0051:
            X.1S6 r2 = r11.self
            boolean r0 = r11.videoEnabled
            r8 = 3
            r6 = 1
            android.widget.ImageButton r9 = r10.A06
            if (r0 == 0) goto L_0x00b5
            int r0 = r2.A04
            r7 = 6
            if (r0 == r7) goto L_0x0067
            int r0 = com.whatsapp.voipcalling.Voip.getCameraCount()
            r1 = 1
            if (r0 > r6) goto L_0x0068
        L_0x0067:
            r1 = 0
        L_0x0068:
            X.C65293Iy.A04(r9, r1)
            r9.setSelected(r3)
            android.widget.ImageButton r1 = r10.A03
            boolean r0 = X.C12960it.A1V(r12, r8)
            r1.setSelected(r0)
            boolean r0 = r2.A09
            r0 = r0 ^ r6
            X.C65293Iy.A04(r1, r0)
            android.widget.ImageButton r6 = r10.A07
            int r1 = r2.A04
            r0 = 0
            if (r1 != r7) goto L_0x0085
            r0 = 1
        L_0x0085:
            r6.setSelected(r0)
        L_0x0088:
            android.view.View r0 = r10.A00
            if (r13 == 0) goto L_0x008d
            r5 = 0
        L_0x008d:
            r0.setVisibility(r5)
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACTIVE
            if (r4 != r0) goto L_0x00a5
            boolean r0 = r11.isGroupCall()
            if (r0 == 0) goto L_0x009e
            boolean r0 = r11.videoEnabled
            if (r0 == 0) goto L_0x00a5
        L_0x009e:
            boolean r0 = r11.isCallOnHold()
            if (r0 != 0) goto L_0x00a5
            r3 = 1
        L_0x00a5:
            X.C65293Iy.A04(r6, r3)
            android.widget.ImageButton r1 = r10.A05
            boolean r0 = r2.A0C
            r1.setSelected(r0)
            boolean r0 = r11.videoEnabled
            r10.A03(r0)
            return
        L_0x00b5:
            boolean r0 = r2.A09
            r0 = r0 ^ r6
            X.C65293Iy.A04(r9, r0)
            boolean r0 = X.C12960it.A1V(r12, r6)
            r9.setSelected(r0)
            android.widget.ImageButton r1 = r10.A03
            boolean r0 = X.C12960it.A1V(r12, r8)
            r1.setSelected(r0)
            boolean r0 = r2.A09
            r0 = r0 ^ r6
            X.C65293Iy.A04(r1, r0)
            android.widget.ImageButton r6 = r10.A07
            r6.setSelected(r3)
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.views.VoipCallFooter.A02(com.whatsapp.voipcalling.CallInfo, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r5.A07.isSelected() != false) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(boolean r6) {
        /*
            r5 = this;
            android.content.Context r4 = r5.getContext()
            android.widget.ImageButton r2 = r5.A06
            if (r6 != 0) goto L_0x0090
            boolean r1 = r2.isSelected()
            r0 = 2131892986(0x7f121afa, float:1.9420736E38)
            if (r1 == 0) goto L_0x0014
            r0 = 2131892985(0x7f121af9, float:1.9420734E38)
        L_0x0014:
            java.lang.String r1 = r4.getString(r0)
            r0 = 0
            if (r6 == 0) goto L_0x0022
            r0 = 2131892980(0x7f121af4, float:1.9420724E38)
            java.lang.String r0 = r4.getString(r0)
        L_0x0022:
            X.C65293Iy.A03(r2, r1, r0)
            android.widget.ImageButton r2 = r5.A03
            boolean r1 = r2.isSelected()
            r0 = 2131892929(0x7f121ac1, float:1.942062E38)
            if (r1 == 0) goto L_0x0033
            r0 = 2131892928(0x7f121ac0, float:1.9420618E38)
        L_0x0033:
            java.lang.String r1 = r4.getString(r0)
            r0 = 2131892946(0x7f121ad2, float:1.9420655E38)
            java.lang.String r0 = r4.getString(r0)
            X.C65293Iy.A03(r2, r1, r0)
            if (r6 == 0) goto L_0x004c
            android.widget.ImageButton r0 = r5.A07
            boolean r0 = r0.isSelected()
            r3 = 1
            if (r0 == 0) goto L_0x004d
        L_0x004c:
            r3 = 0
        L_0x004d:
            android.widget.ImageButton r2 = r5.A07
            r0 = 2131892984(0x7f121af8, float:1.9420732E38)
            if (r3 == 0) goto L_0x0057
            r0 = 2131892982(0x7f121af6, float:1.9420728E38)
        L_0x0057:
            java.lang.String r1 = r4.getString(r0)
            r0 = 2131892983(0x7f121af7, float:1.942073E38)
            if (r3 == 0) goto L_0x0063
            r0 = 2131892981(0x7f121af5, float:1.9420726E38)
        L_0x0063:
            java.lang.String r0 = r4.getString(r0)
            X.C65293Iy.A03(r2, r1, r0)
            android.widget.ImageButton r3 = r5.A05
            boolean r1 = r3.isSelected()
            r0 = 2131892970(0x7f121aea, float:1.9420703E38)
            if (r1 == 0) goto L_0x0078
            r0 = 2131892989(0x7f121afd, float:1.9420742E38)
        L_0x0078:
            java.lang.String r2 = r4.getString(r0)
            boolean r1 = r3.isSelected()
            r0 = 2131892969(0x7f121ae9, float:1.9420701E38)
            if (r1 == 0) goto L_0x0088
            r0 = 2131892988(0x7f121afc, float:1.942074E38)
        L_0x0088:
            java.lang.String r0 = r4.getString(r0)
            X.C65293Iy.A03(r3, r2, r0)
            return
        L_0x0090:
            r0 = 2131892980(0x7f121af4, float:1.9420724E38)
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.views.VoipCallFooter.A03(boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    public void setBluetoothButtonClickListener(View.OnClickListener onClickListener) {
        this.A03.setOnClickListener(onClickListener);
    }

    public void setEndCallButtonClickListener(View.OnClickListener onClickListener) {
        this.A04.setOnClickListener(onClickListener);
    }

    public void setMuteButtonClickListener(View.OnClickListener onClickListener) {
        this.A05.setOnClickListener(onClickListener);
    }

    public void setSpeakerButtonClickListener(View.OnClickListener onClickListener) {
        this.A06.setOnClickListener(onClickListener);
    }

    public void setToggleVideoButtonClickListener(View.OnClickListener onClickListener) {
        this.A07.setOnClickListener(onClickListener);
    }

    public void setToggleVideoButtonSelected(boolean z) {
        this.A07.setSelected(z);
    }
}
