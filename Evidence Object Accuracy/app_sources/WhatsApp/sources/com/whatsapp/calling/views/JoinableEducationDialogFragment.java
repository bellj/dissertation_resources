package com.whatsapp.calling.views;

import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.C004802e;
import X.C013606j;
import X.C12970iu;
import X.C12980iv;
import X.C13010iy;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class JoinableEducationDialogFragment extends Hilt_JoinableEducationDialogFragment {
    public AnonymousClass018 A00;
    public boolean A01;

    public static JoinableEducationDialogFragment A00() {
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("bundle_param_voice_call", false);
        JoinableEducationDialogFragment joinableEducationDialogFragment = new JoinableEducationDialogFragment();
        joinableEducationDialogFragment.A0U(A0D);
        return joinableEducationDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        if (!(bundle == null && (bundle = ((AnonymousClass01E) this).A05) == null)) {
            this.A01 = bundle.getBoolean("bundle_param_voice_call", false);
        }
        ActivityC000900k A01 = C13010iy.A01(this);
        C004802e A0S = C12980iv.A0S(A01);
        View inflate = LayoutInflater.from(A01).inflate(R.layout.voip_call_joinable_education_dialog, (ViewGroup) null, false);
        ImageView A0L = C12970iu.A0L(inflate, R.id.voip_call_joinable_education_dialog_icon);
        if (this.A01) {
            C013606j A012 = C013606j.A01(null, A02(), R.drawable.ic_voip_joinable_calls_education_stars_voice);
            AnonymousClass009.A05(A012);
            A0L.setImageDrawable(A012);
            A0L.setContentDescription(A0I(R.string.voip_joinable_education_icon_content_description_voice));
        }
        A0S.setView(inflate);
        A0S.setPositiveButton(R.string.ok, null);
        return A0S.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
    }
}
