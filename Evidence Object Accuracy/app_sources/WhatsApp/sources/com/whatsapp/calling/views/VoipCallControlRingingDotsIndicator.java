package com.whatsapp.calling.views;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class VoipCallControlRingingDotsIndicator extends View implements AnonymousClass004 {
    public int A00;
    public int A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public final Paint A04;
    public final float[] A05;

    public VoipCallControlRingingDotsIndicator(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new float[3];
        this.A04 = C12960it.A0A();
        A00(context);
    }

    public VoipCallControlRingingDotsIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        this.A05 = new float[3];
        this.A04 = C12960it.A0A();
        A00(context);
    }

    public VoipCallControlRingingDotsIndicator(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new float[3];
        this.A04 = C12960it.A0A();
        A00(context);
    }

    public VoipCallControlRingingDotsIndicator(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        this.A05 = new float[3];
        this.A04 = C12960it.A0A();
        A00(context);
    }

    public VoipCallControlRingingDotsIndicator(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.call_control_bottom_ringing_dots_radius);
        this.A01 = dimensionPixelSize;
        this.A00 = dimensionPixelSize << 1;
        C12980iv.A12(getContext(), this.A04, 17170443);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i = 0;
        do {
            Paint paint = this.A04;
            paint.setAlpha((int) (this.A05[i] * 255.0f));
            int i2 = this.A01;
            float f = (float) i2;
            canvas.drawCircle((float) (((this.A00 << 1) * i) + i2), f, f, paint);
            i++;
        } while (i < 3);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int i3 = this.A00;
        setMeasuredDimension(i3 * 5, i3);
    }
}
