package com.whatsapp.calling.views;

import X.AnonymousClass01d;
import X.AnonymousClass1SF;

/* loaded from: classes3.dex */
public class AppSettingsWarningDialogFragment extends Hilt_AppSettingsWarningDialogFragment {
    public AnonymousClass01d A00;

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (!AnonymousClass1SF.A0K(this.A00)) {
            A1B();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r2 > 30) goto L_0x002d;
     */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r6) {
        /*
            r5 = this;
            X.00k r0 = r5.A0C()
            X.02e r3 = new X.02e
            r3.<init>(r0)
            r0 = 2131892891(0x7f121a9b, float:1.9420543E38)
            r3.A07(r0)
            java.lang.String r0 = android.os.Build.MANUFACTURER
            java.lang.String r4 = "samsung"
            boolean r0 = r4.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x002d
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 31
            r1 = 2131892888(0x7f121a98, float:1.9420537E38)
            if (r2 == r0) goto L_0x0030
            r0 = 28
            if (r2 < r0) goto L_0x002d
            r0 = 30
            r1 = 2131892890(0x7f121a9a, float:1.9420541E38)
            if (r2 <= r0) goto L_0x0030
        L_0x002d:
            r1 = 2131892889(0x7f121a99, float:1.942054E38)
        L_0x0030:
            r3.A06(r1)
            java.lang.String r0 = android.os.Build.MANUFACTURER
            boolean r0 = r4.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x004d
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 28
            if (r1 < r0) goto L_0x004d
            r0 = 31
            if (r1 > r0) goto L_0x004d
            r1 = 2131890872(0x7f1212b8, float:1.9416448E38)
            r0 = 21
            X.C12970iu.A1L(r3, r5, r0, r1)
        L_0x004d:
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            r0 = 20
            X.C12970iu.A1K(r3, r5, r0, r1)
            X.04S r0 = r3.create()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.views.AppSettingsWarningDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }
}
