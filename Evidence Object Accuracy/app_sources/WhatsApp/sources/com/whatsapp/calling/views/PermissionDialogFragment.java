package com.whatsapp.calling.views;

import X.AnonymousClass009;
import X.AnonymousClass1L1;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import X.C15550nR;
import X.C15610nY;
import X.C15890o4;
import X.C21820y2;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class PermissionDialogFragment extends Hilt_PermissionDialogFragment {
    public int A00;
    public Dialog A01;
    public Button A02;
    public TextView A03;
    public AnonymousClass1L1 A04;
    public C15550nR A05;
    public C15610nY A06;
    public C15890o4 A07;
    public C14820m6 A08;
    public C21820y2 A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public String[] A0D;

    public static PermissionDialogFragment A00(UserJid userJid, int i, boolean z, boolean z2, boolean z3) {
        PermissionDialogFragment permissionDialogFragment = new PermissionDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", userJid.getRawString());
        A0D.putBoolean("microphone", z);
        A0D.putBoolean("camera", z2);
        A0D.putBoolean("phone", z3);
        A0D.putInt("request_code", i);
        permissionDialogFragment.A0U(A0D);
        return permissionDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0j(int i, String[] strArr, int[] iArr) {
        boolean z = false;
        if (i != 100) {
            AnonymousClass009.A0A("Unknown request code", false);
            return;
        }
        StringBuilder A0k = C12960it.A0k("PermissionDialogFragment/onRequestPermissionsResult permissions: ");
        A0k.append(Arrays.toString(strArr));
        A0k.append(", grantResults: ");
        Log.i(C12960it.A0d(Arrays.toString(iArr), A0k));
        int length = iArr.length;
        boolean z2 = false;
        if (length <= 0) {
            z = z2;
            break;
        }
        z2 = true;
        int i2 = 0;
        while (iArr[i2] == 0) {
            i2++;
            if (i2 >= length) {
                z = z2;
                break;
            }
        }
        AnonymousClass1L1 r1 = this.A04;
        if (r1 != null) {
            int i3 = this.A00;
            if (z) {
                r1.ATi(strArr, i3);
            } else {
                r1.ATh(i3);
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A04 = null;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        Window window = this.A01.getWindow();
        AnonymousClass009.A05(window);
        window.setLayout(A02().getDisplayMetrics().widthPixels, A02().getDisplayMetrics().heightPixels);
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        super.A11();
        Dialog dialog = this.A01;
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        if (this.A0A) {
            String[] strArr = this.A0D;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (this.A07.A02(strArr[i]) != 0) {
                        break;
                    }
                    i++;
                } else {
                    this.A01.dismiss();
                    if (this.A04 != null) {
                        new Handler().post(new RunnableBRunnable0Shape14S0100000_I1(this, 49));
                    }
                }
            }
            this.A0A = false;
        }
    }

    @Override // com.whatsapp.calling.views.Hilt_PermissionDialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A04 = (AnonymousClass1L1) context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01f0, code lost:
        if (r0 == false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01f3, code lost:
        if (r8 != false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01f5, code lost:
        r2 = com.whatsapp.R.string.permission_cam_access_on_incoming_call_locked_screen_request;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0201, code lost:
        if (r10.A0C != false) goto L_0x01c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0204, code lost:
        r2 = com.whatsapp.R.string.permission_cam_access_on_incoming_call_request;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0034, code lost:
        if (r5 != false) goto L_0x0036;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01b0  */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A16(android.os.Bundle r11) {
        /*
        // Method dump skipped, instructions count: 624
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.views.PermissionDialogFragment.A16(android.os.Bundle):void");
    }
}
