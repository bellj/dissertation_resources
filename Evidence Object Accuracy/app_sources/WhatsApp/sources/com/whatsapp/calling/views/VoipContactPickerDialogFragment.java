package com.whatsapp.calling.views;

import X.AnonymousClass1S6;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C15380n4;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes2.dex */
public class VoipContactPickerDialogFragment extends Hilt_VoipContactPickerDialogFragment {
    public final ContactPickerFragment A00 = new ContactPickerFragment();

    public static VoipContactPickerDialogFragment A00(Map map, boolean z) {
        VoipContactPickerDialogFragment voipContactPickerDialogFragment = new VoipContactPickerDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("is_video_call", z);
        voipContactPickerDialogFragment.A0U(A0D);
        ArrayList A0l = C12960it.A0l();
        Iterator A0o = C12960it.A0o(map);
        while (A0o.hasNext()) {
            AnonymousClass1S6 r2 = (AnonymousClass1S6) A0o.next();
            if (!r2.A0F && r2.A01 != 11) {
                A0l.add(r2.A06);
            }
        }
        Bundle A0D2 = C12970iu.A0D();
        A0D2.putBoolean("for_group_call", true);
        A0D2.putStringArrayList("contacts_to_exclude", C15380n4.A06(A0l));
        ContactPickerFragment contactPickerFragment = voipContactPickerDialogFragment.A00;
        Bundle A0D3 = C12970iu.A0D();
        A0D3.putBundle("extras", A0D2);
        contactPickerFragment.A0U(A0D3);
        return voipContactPickerDialogFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.voip_contact_picker_dialog_fragment);
        C004902f r2 = new C004902f(A0E());
        r2.A06(this.A00, R.id.fragment_container);
        r2.A03();
        return A0F;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004d, code lost:
        if (r1 == false) goto L_0x004f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A13() {
        /*
            r5 = this;
            super.A13()
            android.app.Dialog r1 = r5.A03
            if (r1 == 0) goto L_0x0059
            X.4hO r0 = new X.4hO
            r0.<init>()
            r1.setOnKeyListener(r0)
            android.app.Dialog r0 = r5.A03
            android.view.Window r0 = r0.getWindow()
            if (r0 == 0) goto L_0x0059
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0059
            android.app.Dialog r0 = r5.A03
            android.view.Window r2 = r0.getWindow()
            android.app.Dialog r0 = r5.A03
            android.content.Context r1 = r0.getContext()
            r0 = 2131100725(0x7f060435, float:1.781384E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r2.setStatusBarColor(r0)
            android.app.Dialog r0 = r5.A03
            android.view.Window r4 = r0.getWindow()
            android.app.Dialog r0 = r5.A03
            android.content.Context r3 = r0.getContext()
            android.os.Bundle r2 = r5.A05
            if (r2 == 0) goto L_0x004f
            r1 = 0
            java.lang.String r0 = "is_video_call"
            boolean r1 = r2.getBoolean(r0, r1)
            r0 = 2131100596(0x7f0603b4, float:1.7813578E38)
            if (r1 != 0) goto L_0x0052
        L_0x004f:
            r0 = 2131101093(0x7f0605a5, float:1.7814586E38)
        L_0x0052:
            int r0 = X.AnonymousClass00T.A00(r3, r0)
            r4.setNavigationBarColor(r0)
        L_0x0059:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.views.VoipContactPickerDialogFragment.A13():void");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, R.style.VoipContactPickerDialogFragment);
    }
}
