package com.whatsapp.calling.videoparticipant;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1CP;
import X.AnonymousClass2A6;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3NG;
import X.AnonymousClass4GV;
import X.AnonymousClass4RW;
import X.AnonymousClass5RO;
import X.C12960it;
import X.C12970iu;
import X.C15450nH;
import X.C28141Kv;
import X.C42941w9;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public final class VideoCallParticipantViewLayout extends FrameLayout implements AnonymousClass2A6, AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public ValueAnimator A07;
    public Bitmap A08;
    public View.OnClickListener A09;
    public View.OnClickListener A0A;
    public View.OnLongClickListener A0B;
    public View.OnTouchListener A0C;
    public View.OnTouchListener A0D;
    public C15450nH A0E;
    public AnonymousClass5RO A0F;
    public AnonymousClass1CP A0G;
    public AnonymousClass018 A0H;
    public AnonymousClass2P7 A0I;
    public Boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public final VideoCallParticipantView A0P;
    public final VideoCallParticipantView A0Q;
    public final Map A0R;

    public VideoCallParticipantViewLayout(Context context) {
        this(context, null);
    }

    public VideoCallParticipantViewLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VideoCallParticipantViewLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0L) {
            this.A0L = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A0E = (C15450nH) A00.AII.get();
            this.A0G = (AnonymousClass1CP) A00.AMO.get();
            this.A0H = C12960it.A0R(A00);
        }
        this.A0R = C12970iu.A11();
        this.A0O = false;
        this.A03 = getResources().getDimensionPixelSize(R.dimen.call_pip_min_margin);
        this.A00 = 0.225f;
        this.A0M = true;
        this.A0N = true;
        VideoCallParticipantView videoCallParticipantView = new VideoCallParticipantView(context, null);
        this.A0P = videoCallParticipantView;
        videoCallParticipantView.setVisibility(8);
        addView(videoCallParticipantView, new ViewGroup.MarginLayoutParams(-1, -1));
        VideoCallParticipantView videoCallParticipantView2 = new VideoCallParticipantView(context, null);
        this.A0Q = videoCallParticipantView2;
        videoCallParticipantView2.setVisibility(8);
        View view = videoCallParticipantView2.A0P;
        if (view instanceof SurfaceView) {
            ((SurfaceView) view).setZOrderMediaOverlay(true);
        }
        addView(videoCallParticipantView2, new ViewGroup.MarginLayoutParams(-1, -1));
        this.A01 = 0;
        this.A0K = !AnonymousClass4GV.A00;
    }

    public ViewGroup.MarginLayoutParams A00(Point point) {
        Point point2;
        int i;
        int i2;
        int i3;
        if (getWidth() == 0 || getHeight() == 0 || point == null || point.x == 0 || point.y == 0) {
            Log.i("voip/VideoCallParticipantViewLayout/calculatePipLayoutParamsForVideo cancelled");
            return null;
        }
        int width = getWidth();
        int height = getHeight();
        int i4 = point.x;
        int i5 = point.y;
        int min = Math.min(i4, i5);
        int max = Math.max(i4, i5);
        int min2 = Math.min(width, height);
        if (i4 < i5) {
            width = height;
        }
        float f = (float) min;
        int min3 = (int) (((float) min2) * Math.min(this.A00, ((((float) width) * 0.5f) * f) / ((float) (max * min2))));
        int i6 = (int) (((float) min3) / (f / ((float) max)));
        if (i4 < i5) {
            point2 = new Point(min3, i6);
        } else {
            point2 = new Point(i6, min3);
        }
        AnonymousClass4RW A02 = A02(point2.x, point2.y);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(point2.x, point2.y);
        boolean z = this.A0N;
        if (z) {
            i = A02.A00;
        } else {
            i = A02.A02;
        }
        marginLayoutParams.leftMargin = i;
        if (z) {
            i2 = A02.A02;
        } else {
            i2 = A02.A00;
        }
        marginLayoutParams.rightMargin = i2;
        if (this.A0M) {
            i3 = A02.A01;
        } else {
            i3 = A02.A03;
        }
        marginLayoutParams.topMargin = i3;
        marginLayoutParams.bottomMargin = 0;
        return marginLayoutParams;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r3 >= r5) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.whatsapp.calling.videoparticipant.VideoCallParticipantView A01(int r7) {
        /*
            r6 = this;
            int r5 = r6.getChildCount()
            int r4 = r6.A01
            int r3 = r5 - r4
            int r3 = r3 + r7
            if (r3 < 0) goto L_0x000e
            r2 = 1
            if (r3 < r5) goto L_0x000f
        L_0x000e:
            r2 = 0
        L_0x000f:
            java.lang.String r0 = "VideoCallParticipantView, wrong index = "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r7)
            java.lang.String r0 = ", total count = "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = ", active count = "
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r4)
            X.AnonymousClass009.A0A(r0, r2)
            android.view.View r0 = r6.getChildAt(r3)
            com.whatsapp.calling.videoparticipant.VideoCallParticipantView r0 = (com.whatsapp.calling.videoparticipant.VideoCallParticipantView) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout.A01(int):com.whatsapp.calling.videoparticipant.VideoCallParticipantView");
    }

    public AnonymousClass4RW A02(int i, int i2) {
        int i3 = this.A03;
        return new AnonymousClass4RW(i3, (getWidth() - i) - i3, this.A04 + i3, ((getHeight() - i2) - i3) - this.A02);
    }

    public void A03() {
        Boolean A00 = Voip.A00("options.android_pip_lock_surfaceview");
        this.A0J = A00;
        if (A00 != null && A00.booleanValue()) {
            Iterator A0n = C12960it.A0n(this.A0R);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                View view = (View) A15.getKey();
                SurfaceHolder surfaceHolder = ((VideoPort) A15.getValue()).getSurfaceHolder();
                if (surfaceHolder != null) {
                    surfaceHolder.setFixedSize(view.getWidth(), view.getHeight());
                }
            }
        }
    }

    public void A04() {
        Boolean bool = this.A0J;
        if (bool != null && bool.booleanValue()) {
            Iterator A0o = C12960it.A0o(this.A0R);
            while (A0o.hasNext()) {
                SurfaceHolder surfaceHolder = ((VideoPort) A0o.next()).getSurfaceHolder();
                if (surfaceHolder != null) {
                    surfaceHolder.setSizeFromLayout();
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public final void A05(int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        VideoCallParticipantView A01 = A01(i);
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(A01);
        A0H.width = i2;
        A0H.height = i3;
        A01.setLayoutParams(A0H);
        AnonymousClass018 r7 = this.A0H;
        if (r7 != null) {
            C42941w9.A09(A01, r7, i4, i5, (getWidth() - i4) - i2, 0);
            int i8 = this.A01;
            int i9 = 2;
            if (i8 == 3 && i == 2) {
                i7 = 6;
            } else {
                int i10 = 1;
                if (i == i8 - 1 || i % 2 != 0) {
                    i10 = 0;
                }
                if (C28141Kv.A00(r7)) {
                    i6 = C12960it.A1T(i10);
                } else {
                    i6 = i10;
                }
                int i11 = this.A01;
                if (i == i11 - 1 || (i10 != 0 && i >= i11 - 3 && i11 > 3)) {
                    i7 = 4;
                    if (i6 != 0) {
                        i7 = 5;
                    }
                } else {
                    if (i6 == 0) {
                        i9 = 3;
                    }
                    A01.A03 = i9;
                }
            }
            A01.A03 = i7;
        }
        A01.A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0085, code lost:
        if ((r8 % 2) != 0) goto L_0x0087;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(int r23, boolean r24) {
        /*
        // Method dump skipped, instructions count: 217
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout.A06(int, boolean):void");
    }

    public final void A07(VideoCallParticipantView videoCallParticipantView) {
        videoCallParticipantView.A03 = 0;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(videoCallParticipantView);
        if (!(A0H.width == -1 && A0H.height == -1 && A0H.topMargin == 0 && A0H.leftMargin == 0 && A0H.bottomMargin == 0 && A0H.rightMargin == 0)) {
            AnonymousClass018 r4 = this.A0H;
            if (r4 != null) {
                C42941w9.A09(videoCallParticipantView, r4, 0, 0, 0, 0);
            }
            A0H.height = -1;
            A0H.width = -1;
            videoCallParticipantView.setLayoutParams(A0H);
        }
        videoCallParticipantView.A02();
    }

    public final void A08(VideoCallParticipantView videoCallParticipantView) {
        if (videoCallParticipantView.A03 == 1) {
            videoCallParticipantView.setOnTouchListener(this.A0D);
            videoCallParticipantView.setOnClickListener(this.A0A);
        } else {
            videoCallParticipantView.setOnTouchListener(this.A0C);
            videoCallParticipantView.setOnLongClickListener(this.A0B);
        }
        videoCallParticipantView.A0L.setOnClickListener(this.A09);
    }

    @Override // X.AnonymousClass2A6
    public VideoPort AHY(VideoCallParticipantView videoCallParticipantView) {
        Map map = this.A0R;
        VideoPort videoPort = (VideoPort) map.get(videoCallParticipantView);
        if (videoPort != null) {
            return videoPort;
        }
        VideoPort A00 = this.A0G.A00(videoCallParticipantView.A0P);
        map.put(videoCallParticipantView, A00);
        return A00;
    }

    @Override // X.AnonymousClass2A6
    public void Afb(Point point, VideoCallParticipantView videoCallParticipantView) {
        int i;
        int i2;
        int i3;
        boolean z = true;
        if (videoCallParticipantView.A03 == 1) {
            VideoCallParticipantView videoCallParticipantView2 = this.A0Q;
            if (videoCallParticipantView != videoCallParticipantView2) {
                z = false;
            }
            AnonymousClass009.A0A("only pipView can be in Pip mode", z);
            if (videoCallParticipantView == videoCallParticipantView2) {
                AnonymousClass009.A0A("pipView is not in Pip mode", C12970iu.A1W(videoCallParticipantView2.A03));
                ViewGroup.MarginLayoutParams A00 = A00(point);
                if (A00 != null) {
                    ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(videoCallParticipantView2);
                    A0H.height = A00.height;
                    A0H.width = A00.width;
                    AnonymousClass018 r0 = this.A0H;
                    if (r0 == null || !C28141Kv.A01(r0)) {
                        i = A00.rightMargin;
                        i2 = A00.topMargin;
                        i3 = A00.leftMargin;
                    } else {
                        i = A00.leftMargin;
                        i2 = A00.topMargin;
                        i3 = A00.rightMargin;
                    }
                    A0H.setMargins(i, i2, i3, A00.bottomMargin);
                    StringBuilder A0k = C12960it.A0k("voip/VideoCallParticipantViewLayout/updatePipLayoutParams leftMargin: ");
                    A0k.append(A0H.leftMargin);
                    A0k.append(", topMargin: ");
                    A0k.append(A0H.topMargin);
                    A0k.append(", Pip size: ");
                    A0k.append(A0H.width);
                    A0k.append("x");
                    A0k.append(A0H.height);
                    A0k.append(", container size: ");
                    A0k.append(getWidth());
                    A0k.append("x");
                    A0k.append(getHeight());
                    C12960it.A1F(A0k);
                    videoCallParticipantView2.setLayoutParams(A0H);
                    videoCallParticipantView2.A02();
                }
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0I;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0I = r0;
        }
        return r0.generatedComponent();
    }

    public int getActiveChildCount() {
        return this.A01;
    }

    public List getActiveChildUserJids() {
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < this.A01; i++) {
            VideoCallParticipantView A01 = A01(i);
            if (!(A01 == null || A01.A0O == null)) {
                A0l.add(A01.A0O);
            }
        }
        return A0l;
    }

    public Bitmap getCachedViewBitmap() {
        if (!(this.A08 != null && getWidth() == this.A08.getWidth() && getHeight() == this.A08.getHeight())) {
            this.A08 = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        }
        draw(new Canvas(this.A08));
        return this.A08;
    }

    public VideoCallParticipantView getPiPView() {
        return this.A0Q;
    }

    public void setCommonViewListeners(View.OnTouchListener onTouchListener, View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener) {
        this.A0C = onTouchListener;
        this.A0B = onLongClickListener;
        this.A09 = onClickListener;
        for (int i = 0; i < this.A01; i++) {
            A08(A01(i));
        }
    }

    public void setPipBottomOffset(int i) {
        this.A02 = i;
    }

    public void setPipMaxRatio(float f) {
        this.A00 = f;
    }

    public void setPipTopOffset(int i) {
        this.A04 = i;
    }

    public void setPipViewListeners(AnonymousClass5RO r2, View.OnClickListener onClickListener) {
        this.A0F = r2;
        this.A0D = new AnonymousClass3NG(this);
        this.A0A = onClickListener;
        A08(this.A0Q);
    }
}
