package com.whatsapp.calling.videoparticipant;

import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2A8;
import X.C14850m9;
import X.C73813gq;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class VideoCallParticipantView extends AnonymousClass2A8 {
    public static final int A0Q = ViewConfiguration.getLongPressTimeout();
    public static final int[] A0R = {Integer.MIN_VALUE, 0, 0};
    public static final int[] A0S = {Integer.MIN_VALUE, 0};
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public GradientDrawable A07;
    public GradientDrawable A08;
    public GradientDrawable A09;
    public GradientDrawable A0A;
    public GradientDrawable A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public View A0F;
    public View A0G;
    public FrameLayout A0H;
    public ImageView A0I;
    public ImageView A0J;
    public TextView A0K;
    public WaImageButton A0L;
    public AnonymousClass018 A0M;
    public C14850m9 A0N;
    public UserJid A0O;
    public final View A0P;

    public VideoCallParticipantView(Context context) {
        this(context, null);
    }

    public VideoCallParticipantView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VideoCallParticipantView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setFocusable(false);
        if (Build.VERSION.SDK_INT > 19) {
            setImportantForAccessibility(4);
        }
        if (this.A0M != null) {
            LayoutInflater.from(context).inflate(R.layout.video_call_participant_view, this);
        }
        SurfaceView surfaceView = new SurfaceView(context, attributeSet, i);
        this.A0P = surfaceView;
        addView(surfaceView, 0, new FrameLayout.LayoutParams(-1, -1));
        this.A0F = findViewById(R.id.status_layout);
        this.A0K = (TextView) findViewById(R.id.status);
        this.A0L = (WaImageButton) findViewById(R.id.video_call_status_button);
        this.A0E = findViewById(R.id.mute_image);
        this.A0I = (ImageView) findViewById(R.id.frame_overlay);
        this.A0J = (ImageView) findViewById(R.id.video_call_participant_photo);
        this.A0H = (FrameLayout) findViewById(R.id.mute_layout);
        this.A0C = findViewById(R.id.camera_off_image);
        this.A0G = findViewById(R.id.video_status_container);
        this.A0D = AnonymousClass028.A0D(this, R.id.dark_overlay);
        this.A06 = AnonymousClass00T.A00(getContext(), R.color.call_video_overlay);
        this.A05 = AnonymousClass00T.A00(getContext(), 17170445);
        this.A01 = getResources().getDimensionPixelSize(R.dimen.call_grid_mode_mute_icon_margin);
        this.A02 = getResources().getDimensionPixelSize(R.dimen.call_mute_icon_layout_size);
        this.A04 = getResources().getDimensionPixelSize(R.dimen.call_pip_mode_icon_margin);
        this.A03 = 0;
    }

    public final GradientDrawable A00(GradientDrawable.Orientation orientation) {
        int[] iArr;
        if (this.A03 == 1) {
            iArr = A0S;
        } else {
            iArr = A0R;
        }
        GradientDrawable gradientDrawable = new GradientDrawable(orientation, iArr);
        gradientDrawable.setGradientType(0);
        gradientDrawable.setShape(0);
        return gradientDrawable;
    }

    public void A01() {
        this.A0G.setVisibility(8);
    }

    public void A02() {
        int i;
        switch (this.A03) {
            case 1:
                A05(81, 0, 0, 0, this.A04);
                A04(81, -1, -2);
                return;
            case 2:
                i = 83;
                int i2 = this.A01;
                A05(83, i2, 0, 0, i2);
                break;
            case 3:
                i = 85;
                int i3 = this.A01;
                A05(85, 0, 0, i3, i3);
                break;
            case 4:
                i = 53;
                int i4 = this.A01;
                A05(53, 0, i4, i4, 0);
                break;
            case 5:
                i = 51;
                int i5 = this.A01;
                A05(51, i5, i5, 0, 0);
                break;
            case 6:
                i = 49;
                A05(49, 0, this.A01, 0, 0);
                break;
            case 7:
                A05(81, 0, 0, 0, this.A01);
                A04(81, -1, -1);
                return;
            default:
                return;
        }
        int i6 = this.A02;
        A04(i, i6, i6);
    }

    public void A03(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            setClipToOutline(true);
            setOutlineProvider(new C73813gq(this, i));
        }
    }

    public final void A04(int i, int i2, int i3) {
        FrameLayout frameLayout = this.A0H;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameLayout.getLayoutParams();
        layoutParams.gravity = i;
        layoutParams.width = i2;
        layoutParams.height = i3;
        frameLayout.setLayoutParams(layoutParams);
        frameLayout.setBackground(getMuteIconGradient());
    }

    public final void A05(int i, int i2, int i3, int i4, int i5) {
        View view = this.A0E;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.gravity = i;
        layoutParams.leftMargin = i2;
        layoutParams.topMargin = i3;
        layoutParams.rightMargin = i4;
        layoutParams.bottomMargin = i5;
        view.setLayoutParams(layoutParams);
    }

    public void A06(CharSequence charSequence, boolean z) {
        int paddingLeft;
        int i = 0;
        this.A0G.setVisibility(0);
        TextView textView = this.A0K;
        textView.setText(charSequence);
        textView.setVisibility(0);
        WaImageButton waImageButton = this.A0L;
        int i2 = 8;
        if (z) {
            i2 = 0;
        }
        waImageButton.setVisibility(i2);
        if (!z) {
            i = getResources().getDimensionPixelSize(R.dimen.call_cancel_button_touch_padding);
        }
        boolean z2 = this.A0M.A04().A06;
        if (z2) {
            paddingLeft = i;
        } else {
            paddingLeft = textView.getPaddingLeft();
        }
        int paddingTop = textView.getPaddingTop();
        if (z2) {
            i = textView.getPaddingRight();
        }
        textView.setPadding(paddingLeft, paddingTop, i, textView.getPaddingBottom());
    }

    public void A07(boolean z, boolean z2) {
        View view = this.A0C;
        int i = 0;
        int i2 = 8;
        if (z2) {
            i2 = 0;
        }
        view.setVisibility(i2);
        FrameLayout frameLayout = this.A0H;
        if (!z) {
            i = 8;
        }
        frameLayout.setVisibility(i);
    }

    public boolean A08() {
        int i = this.A03;
        return i == 5 || i == 4 || i == 2 || i == 3 || i == 6;
    }

    public WaImageButton getCancelButton() {
        return this.A0L;
    }

    public ImageView getFrameOverlay() {
        return this.A0I;
    }

    public UserJid getJid() {
        return this.A0O;
    }

    @Override // android.view.ViewGroup
    public int getLayoutMode() {
        return this.A03;
    }

    private Drawable getMuteIconGradient() {
        int i = this.A03;
        if (i == 1) {
            GradientDrawable gradientDrawable = this.A09;
            if (gradientDrawable != null) {
                return gradientDrawable;
            }
            GradientDrawable A00 = A00(GradientDrawable.Orientation.BOTTOM_TOP);
            this.A09 = A00;
            return A00;
        } else if (i == 2) {
            GradientDrawable gradientDrawable2 = this.A07;
            if (gradientDrawable2 != null) {
                return gradientDrawable2;
            }
            GradientDrawable A002 = A00(GradientDrawable.Orientation.BL_TR);
            this.A07 = A002;
            return A002;
        } else if (i == 3) {
            if (this.A07 == null) {
                this.A08 = A00(GradientDrawable.Orientation.BR_TL);
            }
            return this.A08;
        } else if (i == 4) {
            GradientDrawable gradientDrawable3 = this.A0B;
            if (gradientDrawable3 != null) {
                return gradientDrawable3;
            }
            GradientDrawable A003 = A00(GradientDrawable.Orientation.TR_BL);
            this.A0B = A003;
            return A003;
        } else if (i != 5) {
            return null;
        } else {
            GradientDrawable gradientDrawable4 = this.A0A;
            if (gradientDrawable4 != null) {
                return gradientDrawable4;
            }
            GradientDrawable A004 = A00(GradientDrawable.Orientation.TL_BR);
            this.A0A = A004;
            return A004;
        }
    }

    public ImageView getPhotoImageView() {
        return this.A0J;
    }

    public View getVideoView() {
        return this.A0P;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        float f = this.A00;
        if (f > 0.0f && (i3 = (int) (((float) size) * f)) <= size2) {
            i2 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
                layoutParams2.addRule(13, -1);
                setLayoutParams(layoutParams2);
            }
        }
        super.onMeasure(i, i2);
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        return true;
    }

    public void setAspectRatio(float f) {
        this.A00 = f;
    }

    public void setJid(UserJid userJid) {
        this.A0O = userJid;
    }

    @Override // android.view.ViewGroup
    public void setLayoutMode(int i) {
        this.A03 = i;
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        this.A0P.setVisibility(i);
    }
}
