package com.whatsapp.calling.videoparticipant;

import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass1CP;
import X.AnonymousClass1S6;
import X.AnonymousClass2A6;
import X.AnonymousClass2SU;
import X.C004902f;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C95724eF;
import X.ViewTreeObserver$OnPreDrawListenerC66383Nj;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;

/* loaded from: classes2.dex */
public class MaximizedParticipantVideoDialogFragment extends Hilt_MaximizedParticipantVideoDialogFragment implements AnonymousClass2A6 {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public View A04;
    public WaTextView A05;
    public WaTextView A06;
    public VideoCallParticipantView A07;
    public AnonymousClass2SU A08;
    public AnonymousClass1CP A09;
    public C15550nR A0A;
    public C15610nY A0B;
    public AnonymousClass018 A0C;
    public VideoPort A0D;
    public boolean A0E = false;
    public final int A0F;
    public final int A0G;
    public final int A0H;
    public final int A0I;
    public final DialogInterface.OnDismissListener A0J;
    public final Drawable A0K;
    public final View.OnClickListener A0L;
    public final Runnable A0M;

    @Override // X.AnonymousClass2A6
    public void Afb(Point point, VideoCallParticipantView videoCallParticipantView) {
    }

    public MaximizedParticipantVideoDialogFragment(DialogInterface.OnDismissListener onDismissListener, Drawable drawable, View.OnClickListener onClickListener, Runnable runnable, int i, int i2, int i3, int i4) {
        this.A0J = onDismissListener;
        this.A0L = onClickListener;
        this.A0H = i;
        this.A0I = i2;
        this.A0G = i3;
        this.A0F = i4;
        this.A0K = drawable;
        this.A0M = runnable;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        Dialog A19 = A19();
        if (A19.getWindow() != null) {
            A19.getWindow().setLayout(-1, -1);
            A19.getWindow().setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            A19.getWindow().clearFlags(2);
            A19.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        View decorView;
        Dialog dialog = new Dialog(A0B(), R.style.MaximizedVideoCallDialog);
        dialog.setCancelable(true);
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(1);
        }
        dialog.setContentView(R.layout.group_call_video_maximize_dialog);
        dialog.setOnDismissListener(this.A0J);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: X.4hN
            @Override // android.content.DialogInterface.OnKeyListener
            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = MaximizedParticipantVideoDialogFragment.this;
                if (i != 4 || keyEvent.getAction() != 1) {
                    return false;
                }
                maximizedParticipantVideoDialogFragment.A1L(true);
                return true;
            }
        });
        if (dialog.getWindow() == null || (decorView = dialog.getWindow().getDecorView()) == null || this.A08 == null) {
            AnonymousClass009.A07("failed to initialize MaximizedParticipantVideoDialogFragment");
        } else {
            this.A07 = (VideoCallParticipantView) AnonymousClass028.A0D(decorView, R.id.video_view);
            this.A06 = (WaTextView) AnonymousClass028.A0D(decorView, R.id.name);
            this.A05 = (WaTextView) AnonymousClass028.A0D(decorView, R.id.name_byline);
            this.A04 = AnonymousClass028.A0D(decorView, R.id.background_overlay);
            View A0D = AnonymousClass028.A0D(decorView, R.id.container);
            VideoCallParticipantView videoCallParticipantView = this.A07;
            videoCallParticipantView.A03 = 7;
            videoCallParticipantView.A02();
            this.A07.A0L.setOnClickListener(this.A0L);
            VideoCallParticipantView videoCallParticipantView2 = this.A07;
            videoCallParticipantView2.A00 = 1.5f;
            videoCallParticipantView2.A03(A02().getDimensionPixelSize(R.dimen.maximized_video_call_rounded_corner));
            this.A07.setBackgroundColor(-16777216);
            this.A08.A06(this.A07);
            CallInfo callInfo = Voip.getCallInfo();
            if (callInfo == null) {
                Log.w("MaximizedParticipantVideoDialogFragment can not get callInfo");
            } else {
                A1K(callInfo, (AnonymousClass1S6) callInfo.participants.get(this.A08.A04));
                if (callInfo.self.A06.equals(this.A08.A04)) {
                    this.A06.setText(R.string.you);
                } else {
                    C15370n3 A0B = this.A0A.A0B(this.A08.A04);
                    this.A06.setText(this.A0B.A04(A0B));
                    if (TextUtils.isEmpty(A0B.A0K)) {
                        this.A05.setText(this.A0B.A09(A0B));
                        this.A05.setVisibility(0);
                    }
                }
            }
            A0D.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 48));
            this.A07.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66383Nj(this));
            A0D.setBackground(this.A0K);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            dialog.getWindow().setNavigationBarColor(AnonymousClass00T.A00(dialog.getContext(), R.color.paletteSurface_1dp));
        }
        return dialog;
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        if (this.A0E) {
            Log.w("MaximizedParticipantVideoDialogFragment already attached");
            return;
        }
        this.A0E = true;
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
        this.A0M.run();
    }

    public void A1K(CallInfo callInfo, AnonymousClass1S6 r5) {
        AnonymousClass2SU r2;
        if (A0c() && r5 != null && (r2 = this.A08) != null && this.A07 != null && r5.A06.equals(r2.A04)) {
            if (!callInfo.callId.equals(Voip.getCurrentCallId())) {
                if (this.A08.A04.equals(callInfo.self.A06)) {
                    this.A08.A03();
                }
            } else if (callInfo.participants.size() > 2) {
                this.A08.A08(callInfo, r5);
                return;
            }
            A1L(false);
        }
    }

    public void A1L(boolean z) {
        float f;
        float f2;
        if (A0c()) {
            Log.i("voip/MaximizedParticipantVideoDialogFragment/dismissDialog");
            VideoPort videoPort = this.A0D;
            if (videoPort != null) {
                videoPort.release();
            }
            AnonymousClass2SU r0 = this.A08;
            if (r0 != null) {
                r0.A04();
            }
            this.A0J.onDismiss(((DialogFragment) this).A03);
            RunnableBRunnable0Shape3S0100000_I0_3 runnableBRunnable0Shape3S0100000_I0_3 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 27);
            AnonymousClass009.A03(this.A07);
            AnonymousClass009.A03(this.A06);
            AnonymousClass009.A03(this.A05);
            DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(1.5f);
            ViewPropertyAnimator duration = this.A07.animate().setDuration(250);
            if (z) {
                f = this.A01;
            } else {
                f = 0.0f;
            }
            ViewPropertyAnimator scaleX = duration.scaleX(f);
            if (z) {
                f2 = this.A00;
            } else {
                f2 = 0.0f;
            }
            scaleX.scaleY(f2).translationX((float) this.A02).translationY((float) this.A03).setInterpolator(decelerateInterpolator).setListener(new C95724eF(this, runnableBRunnable0Shape3S0100000_I0_3)).start();
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(250);
            this.A06.startAnimation(alphaAnimation);
            if (this.A05.getVisibility() == 0) {
                this.A05.startAnimation(alphaAnimation);
            }
            View view = this.A04;
            AnonymousClass009.A03(view);
            view.setAlpha(0.4f);
            this.A04.animate().setDuration(250).alpha(0.0f);
        }
    }

    @Override // X.AnonymousClass2A6
    public VideoPort AHY(VideoCallParticipantView videoCallParticipantView) {
        VideoPort videoPort = this.A0D;
        if (videoPort != null) {
            return videoPort;
        }
        VideoPort A00 = this.A09.A00(videoCallParticipantView.A0P);
        this.A0D = A00;
        return A00;
    }
}
