package com.whatsapp.calling.spam;

import X.AbstractC14440lR;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass1BA;
import X.AnonymousClass2FL;
import X.AnonymousClass3K7;
import X.AnonymousClass5V7;
import X.C004802e;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C15650ng;
import X.C16170oZ;
import X.C18640sm;
import X.C19990v2;
import X.C20220vP;
import X.C21250x7;
import X.C22230yk;
import X.C238013b;
import X.C250417w;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.whatsapp.R;
import com.whatsapp.calling.spam.CallSpamActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CallSpamActivity extends ActivityC13790kL {
    public C15550nR A00;
    public C19990v2 A01;
    public C21250x7 A02;
    public C22230yk A03;
    public AnonymousClass1BA A04;
    public boolean A05;
    public final AnonymousClass5V7 A06;

    /* loaded from: classes2.dex */
    public class ReportSpamOrBlockDialogFragment extends Hilt_CallSpamActivity_ReportSpamOrBlockDialogFragment {
        public long A00;
        public CheckBox A01;
        public C14900mE A02;
        public C16170oZ A03;
        public C238013b A04;
        public C15550nR A05;
        public C15610nY A06;
        public C250417w A07;
        public C18640sm A08;
        public C15650ng A09;
        public C15370n3 A0A;
        public UserJid A0B;
        public UserJid A0C;
        public C22230yk A0D;
        public C20220vP A0E;
        public AbstractC14440lR A0F;
        public String A0G;
        public String A0H;
        public boolean A0I;
        public boolean A0J;
        public boolean A0K;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            String str;
            String A0J;
            Log.i("callspamactivity/createdialog");
            Bundle A03 = A03();
            UserJid nullable = UserJid.getNullable(A03.getString("caller_jid"));
            AnonymousClass009.A05(nullable);
            this.A0C = nullable;
            this.A0B = UserJid.getNullable(A03.getString("call_creator_jid"));
            C15370n3 A0A = this.A05.A0A(this.A0C);
            AnonymousClass009.A05(A0A);
            this.A0A = A0A;
            String string = A03.getString("call_id");
            AnonymousClass009.A05(string);
            this.A0G = string;
            this.A00 = A03.getLong("call_duration", -1);
            this.A0I = A03.getBoolean("call_terminator", false);
            this.A0H = A03.getString("call_termination_reason");
            this.A0K = A03.getBoolean("call_video", false);
            AnonymousClass3K7 r7 = new AnonymousClass3K7(this);
            ActivityC000900k A0C = A0C();
            C004802e r5 = new C004802e(A0C);
            if (this.A0J) {
                A0J = A0I(R.string.report_contact_ask);
            } else {
                Object[] objArr = new Object[1];
                C15370n3 r1 = this.A0A;
                if (r1 != null) {
                    str = this.A06.A04(r1);
                } else {
                    str = "";
                }
                objArr[0] = str;
                A0J = A0J(R.string.block_ask, objArr);
            }
            r5.A0A(A0J);
            r5.setPositiveButton(R.string.ok, r7);
            r5.setNegativeButton(R.string.cancel, null);
            if (this.A0J) {
                View inflate = LayoutInflater.from(A0C).inflate(R.layout.report_spam_dialog, (ViewGroup) null);
                CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.block_contact);
                this.A01 = checkBox;
                checkBox.setChecked(true);
                r5.setView(inflate);
            }
            return r5.create();
        }
    }

    public CallSpamActivity() {
        this(0);
        this.A06 = new AnonymousClass5V7() { // from class: X.5At
            @Override // X.AnonymousClass5V7
            public final void A9y() {
                CallSpamActivity.this.finish();
            }
        };
    }

    public CallSpamActivity(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 38);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A01 = C12980iv.A0c(A1M);
            this.A02 = (C21250x7) A1M.AJh.get();
            this.A00 = C12960it.A0O(A1M);
            this.A03 = (C22230yk) A1M.ANT.get();
            this.A04 = (AnonymousClass1BA) A1M.A2e.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        UserJid nullable;
        super.onCreate(bundle);
        Bundle A0H = C12990iw.A0H(this);
        String str2 = null;
        if (A0H == null || (nullable = UserJid.getNullable(A0H.getString("caller_jid"))) == null) {
            StringBuilder A0k = C12960it.A0k("callspamactivity/create/not-creating/bad-jid: ");
            if (A0H != null) {
                str2 = A0H.getString("caller_jid");
            }
            str = C12960it.A0d(str2, A0k);
        } else {
            C15370n3 A0A = this.A00.A0A(nullable);
            String string = A0H.getString("call_id");
            if (A0A == null || string == null) {
                str = "callspamactivity/create/not-creating/null-args";
            } else {
                ActivityC13830kP.A1O(this);
                setContentView(R.layout.call_spam);
                AbstractView$OnClickListenerC34281fs.A02(findViewById(R.id.call_spam_report), this, A0H, 25);
                AbstractView$OnClickListenerC34281fs.A02(findViewById(R.id.call_spam_not_spam), this, nullable, 26);
                AbstractView$OnClickListenerC34281fs.A02(findViewById(R.id.call_spam_block), this, A0H, 27);
                this.A04.A00.add(this.A06);
                return;
            }
        }
        Log.e(str);
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1BA r0 = this.A04;
        r0.A00.remove(this.A06);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        finish();
    }
}
