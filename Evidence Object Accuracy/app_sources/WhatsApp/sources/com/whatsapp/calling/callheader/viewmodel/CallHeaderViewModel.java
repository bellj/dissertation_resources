package com.whatsapp.calling.callheader.viewmodel;

import X.AnonymousClass016;
import X.AnonymousClass2OR;
import X.C12980iv;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C48782Ht;

/* loaded from: classes2.dex */
public class CallHeaderViewModel extends AnonymousClass2OR {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final C15570nT A01;
    public final C48782Ht A02;
    public final C15550nR A03;
    public final C15610nY A04;

    public CallHeaderViewModel(C15570nT r2, C48782Ht r3, C15550nR r4, C15610nY r5) {
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        r3.A03(this);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A02.A04(this);
    }
}
