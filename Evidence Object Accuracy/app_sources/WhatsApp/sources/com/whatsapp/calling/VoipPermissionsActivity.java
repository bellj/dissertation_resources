package com.whatsapp.calling;

import X.AbstractC009404s;
import X.AbstractC15710nm;
import X.ActivityC000800j;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass19Z;
import X.AnonymousClass1MW;
import X.AnonymousClass1SF;
import X.AnonymousClass1YT;
import X.AnonymousClass1YU;
import X.AnonymousClass2FH;
import X.AnonymousClass30D;
import X.AnonymousClass3OY;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14850m9;
import X.C14900mE;
import X.C15370n3;
import X.C15380n4;
import X.C15550nR;
import X.C15890o4;
import X.C16120oU;
import X.C18750sx;
import X.C35751ig;
import X.C48572Gu;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class VoipPermissionsActivity extends ActivityC000800j implements AnonymousClass004 {
    public int A00;
    public AbstractC15710nm A01;
    public C14900mE A02;
    public C15550nR A03;
    public C15890o4 A04;
    public C18750sx A05;
    public C14850m9 A06;
    public C16120oU A07;
    public GroupJid A08;
    public AnonymousClass1YT A09;
    public AnonymousClass19Z A0A;
    public String A0B;
    public List A0C;
    public boolean A0D;
    public boolean A0E;
    public final Object A0F;
    public volatile AnonymousClass2FH A0G;

    public VoipPermissionsActivity() {
        this(0);
        this.A0C = C12960it.A0l();
        this.A0B = null;
    }

    public VoipPermissionsActivity(int i) {
        this.A0F = C12970iu.A0l();
        this.A0D = false;
        A0R(new AnonymousClass3OY(this));
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A0G == null) {
            synchronized (this.A0F) {
                if (this.A0G == null) {
                    this.A0G = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A0G.generatedComponent();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        StringBuilder A0k = C12960it.A0k("VoipPermissionsActivity onActivityResult got result: ");
        A0k.append(i2);
        A0k.append(" for request: ");
        A0k.append(i);
        A0k.append(" data: ");
        A0k.append(intent);
        C12960it.A1F(A0k);
        if (i != 152 && i != 156) {
            StringBuilder A0k2 = C12960it.A0k("VoipPermissionsActivity onActivityResult unhandled request: ");
            A0k2.append(i);
            A0k2.append(" result: ");
            A0k2.append(i2);
            C12960it.A1F(A0k2);
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            if (this.A09 == null) {
                ArrayList A0l = C12960it.A0l();
                Iterator it = this.A0C.iterator();
                while (it.hasNext()) {
                    C15370n3 A0A = this.A03.A0A(C12990iw.A0b(it));
                    if (A0A != null) {
                        A0l.add(A0A);
                    }
                }
                if (!AnonymousClass1SF.A0O(this.A06) || this.A0B == null) {
                    Log.i("VoipPermissionsActivity onActivityResult starting call");
                    this.A0A.A03(this, this.A08, A0l, this.A00, this.A0E);
                } else {
                    Log.i("VoipPermissionsActivity onActivityResult starting call link lobby");
                    this.A0A.A07(this, this.A0B, this.A0E);
                }
            } else {
                int i3 = 0;
                if (intent != null) {
                    i3 = intent.getIntExtra("lobby_entry_point", 0);
                }
                this.A0A.A06(this, this.A09, i3);
            }
        } else if (i == 156 && i2 == 0) {
            AnonymousClass30D r1 = new AnonymousClass30D();
            r1.A00 = "voip_call_fail_phone_perm_denied";
            this.A07.A07(r1);
        }
        finish();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Log.i("voip/VoipPermissionsActivity/onCreate");
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent.getBooleanExtra("join_call_log", false)) {
            try {
                this.A09 = this.A05.A04(new AnonymousClass1YU(intent.getIntExtra("call_log_transaction_id", -1), UserJid.get(intent.getStringExtra("call_log_user_jid")), intent.getStringExtra("call_log_call_id"), intent.getBooleanExtra("call_log_from_me", false)));
            } catch (AnonymousClass1MW unused) {
                Log.e("voip/VoipPermissionsActivity/onCreate invalid jid");
                return;
            }
        } else {
            this.A0B = intent.getStringExtra("call_link_lobby_token");
            this.A0C = C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("jids"));
            if (!AnonymousClass1SF.A0O(this.A06) || this.A0B == null) {
                AnonymousClass009.A0A("There must be at least one jid", !this.A0C.isEmpty());
            }
            this.A00 = intent.getIntExtra("call_from", -1);
            if (intent.hasExtra("group_jid")) {
                this.A08 = GroupJid.getNullable(intent.getStringExtra("group_jid"));
            }
        }
        this.A0E = intent.getBooleanExtra("video_call", false);
        int intExtra = intent.getIntExtra("permission_type", -1);
        if (intExtra == 0) {
            RequestPermissionActivity.A0M(this, this.A02, this.A04, this.A0E);
        } else if (intExtra != 1) {
            Log.i(C12960it.A0W(intExtra, "voip/VoipPermissionsActivity/onCreate unhandled permissionType: "));
        } else {
            Log.i("request/permission/checkPhonePermissionForVoipCall");
            C35751ig r2 = new C35751ig(this);
            r2.A01 = R.drawable.permission_call;
            r2.A02 = R.string.permission_phone_access_request;
            r2.A03 = R.string.permission_phone_access;
            r2.A0C = new String[]{"android.permission.READ_PHONE_STATE"};
            r2.A06 = true;
            startActivityForResult(r2.A00(), 156);
        }
    }
}
