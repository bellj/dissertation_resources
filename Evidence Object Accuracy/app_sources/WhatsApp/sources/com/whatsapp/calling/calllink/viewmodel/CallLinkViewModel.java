package com.whatsapp.calling.calllink.viewmodel;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass07E;
import X.C12960it;
import X.C12980iv;
import X.C18640sm;
import X.C26391De;
import X.C65973Lu;
import X.C90974Py;
import android.os.Message;
import com.whatsapp.R;
import java.util.Set;

/* loaded from: classes2.dex */
public class CallLinkViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass07E A02;
    public final C90974Py A03;
    public final C18640sm A04;

    public CallLinkViewModel(AnonymousClass07E r4, C90974Py r5, C18640sm r6) {
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A01 = A0T;
        AnonymousClass016 A0T2 = C12980iv.A0T();
        this.A00 = A0T2;
        this.A03 = r5;
        r5.A02.add(this);
        this.A02 = r4;
        this.A04 = r6;
        C12960it.A1A(A0T2, R.string.call_link_description);
        C12960it.A1A(A0T, R.string.call_link_share_email_subject);
        AnonymousClass016 A02 = this.A02.A02("saved_state_link");
        if (A02.A01() == null || ((C65973Lu) A02.A01()).A03 != 1) {
            A04(A05());
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C90974Py r1 = this.A03;
        Set set = r1.A02;
        set.remove(this);
        if (set.size() == 0) {
            r1.A00.A04(r1);
        }
    }

    public final void A04(boolean z) {
        if (!this.A04.A0B()) {
            this.A02.A04("saved_state_link", new C65973Lu("", 3, 0, R.color.list_item_title, 0));
            return;
        }
        this.A02.A04("saved_state_link", new C65973Lu("", 0, R.string.creating_new_link, R.color.list_item_sub_title, 0));
        this.A03.A01.A00(new C26391De(Message.obtain(null, 0, z ? 1 : 0, 0), "create_call_link"));
    }

    public final boolean A05() {
        Boolean bool = (Boolean) this.A02.A02.get("saved_state_is_video");
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }
}
