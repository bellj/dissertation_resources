package com.whatsapp.calling.calllink.view;

import X.AbstractActivityC42891w4;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass07E;
import X.AnonymousClass1SM;
import X.AnonymousClass2FL;
import X.AnonymousClass2V9;
import X.C100544m5;
import X.C12960it;
import X.C13000ix;
import X.C83663xe;
import X.C83673xf;
import X.C83683xg;
import X.C83693xh;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.calling.calllink.viewmodel.CallLinkViewModel;

/* loaded from: classes2.dex */
public class CallLinkActivity extends AbstractActivityC42891w4 implements AnonymousClass1SM {
    public ViewGroup A00;
    public C83663xe A01;
    public C83693xh A02;
    public C83683xg A03;
    public C83673xf A04;
    public CallLinkViewModel A05;
    public boolean A06;

    public CallLinkActivity() {
        this(0);
    }

    public CallLinkActivity(int i) {
        this.A06 = false;
        ActivityC13830kP.A1P(this, 36);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        CallLinkViewModel callLinkViewModel = this.A05;
        if (callLinkViewModel != null && i == 1 && i2 != (!callLinkViewModel.A05())) {
            callLinkViewModel.A04(C12960it.A1T(i2));
        }
    }

    @Override // X.AbstractActivityC42891w4, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.new_call_link);
        this.A00 = (ViewGroup) findViewById(R.id.link_btn);
        View findViewById = findViewById(R.id.link_btn);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.call_link_activity_link_view_padding);
        findViewById.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        this.A05 = (CallLinkViewModel) C13000ix.A02(this).A00(CallLinkViewModel.class);
        C83693xh r1 = new C83693xh();
        this.A02 = r1;
        ((AnonymousClass2V9) r1).A00 = A2e();
        this.A02 = this.A02;
        A2i();
        this.A04 = A2h();
        this.A01 = A2f();
        this.A03 = A2g();
        CallLinkViewModel callLinkViewModel = this.A05;
        if (callLinkViewModel != null) {
            C12960it.A18(this, callLinkViewModel.A02.A02("saved_state_link"), 46);
            C12960it.A18(this, this.A05.A00, 47);
            CallLinkViewModel callLinkViewModel2 = this.A05;
            AnonymousClass07E r4 = callLinkViewModel2.A02;
            boolean A05 = callLinkViewModel2.A05();
            int i = R.drawable.ic_btn_call_audio;
            int i2 = R.string.voice_call_link;
            if (A05) {
                i = R.drawable.ic_btn_call_video;
                i2 = R.string.video_call_link;
            }
            C12960it.A17(this, r4.A01(new C100544m5(i, i2, !callLinkViewModel2.A05()), "saved_state_link_type"), 21);
            C12960it.A17(this, this.A05.A01, 22);
        }
    }
}
