package com.whatsapp.calling.controls.viewmodel;

import X.AbstractC15710nm;
import X.AnonymousClass016;
import X.AnonymousClass2CT;
import X.AnonymousClass2OR;
import X.C12970iu;
import X.C12980iv;
import X.C15550nR;
import X.C15610nY;
import X.C17140qK;
import X.C20710wC;
import X.C21250x7;
import X.C48782Ht;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.voipcalling.CallInfo;
import java.util.Set;

/* loaded from: classes2.dex */
public class ParticipantsListViewModel extends AnonymousClass2OR {
    public int A00;
    public AnonymousClass2CT A01;
    public boolean A02 = false;
    public boolean A03 = false;
    public boolean A04 = false;
    public final AnonymousClass016 A05 = C12980iv.A0T();
    public final AbstractC15710nm A06;
    public final C48782Ht A07;
    public final C15550nR A08;
    public final C15610nY A09;
    public final C21250x7 A0A;
    public final C20710wC A0B;
    public final C17140qK A0C;
    public final Set A0D = C12970iu.A12();

    public ParticipantsListViewModel(AbstractC15710nm r4, C48782Ht r5, C15550nR r6, C15610nY r7, C21250x7 r8, C20710wC r9, C17140qK r10) {
        this.A06 = r4;
        this.A07 = r5;
        this.A0A = r8;
        this.A08 = r6;
        this.A09 = r7;
        this.A0B = r9;
        this.A0C = r10;
        this.A00 = r10.A01().getInt("inline_education", 0);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        if (this.A03) {
            this.A07.A04(this);
        }
    }

    public GroupJid A05() {
        CallInfo A2i;
        if (this.A03) {
            return this.A07.A05().A01;
        }
        AnonymousClass2CT r0 = this.A01;
        if (r0 == null || (A2i = r0.A00.A2i()) == null) {
            return null;
        }
        return A2i.groupJid;
    }
}
