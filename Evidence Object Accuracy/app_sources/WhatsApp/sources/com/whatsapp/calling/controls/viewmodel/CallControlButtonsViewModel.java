package com.whatsapp.calling.controls.viewmodel;

import X.AnonymousClass016;
import X.AnonymousClass2OR;
import X.C12980iv;
import X.C48782Ht;

/* loaded from: classes2.dex */
public class CallControlButtonsViewModel extends AnonymousClass2OR {
    public int A00 = 0;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C48782Ht A02;

    public CallControlButtonsViewModel(C48782Ht r2) {
        this.A02 = r2;
        r2.A03(this);
        A04(r2.A05());
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A02.A04(this);
    }
}
