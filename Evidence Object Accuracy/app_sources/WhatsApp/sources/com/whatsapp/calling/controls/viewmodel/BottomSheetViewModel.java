package com.whatsapp.calling.controls.viewmodel;

import X.AnonymousClass016;
import X.AnonymousClass01d;
import X.AnonymousClass2OR;
import X.AnonymousClass3HT;
import X.C12980iv;
import X.C36161jQ;
import X.C48782Ht;

/* loaded from: classes2.dex */
public class BottomSheetViewModel extends AnonymousClass2OR {
    public AnonymousClass3HT A00;
    public boolean A01;
    public boolean A02;
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final AnonymousClass016 A04 = C12980iv.A0T();
    public final AnonymousClass016 A05 = C12980iv.A0T();
    public final AnonymousClass016 A06 = C12980iv.A0T();
    public final C48782Ht A07;
    public final AnonymousClass01d A08;
    public final C36161jQ A09;
    public final C36161jQ A0A;

    public BottomSheetViewModel(C48782Ht r3, AnonymousClass01d r4) {
        Boolean bool = Boolean.FALSE;
        this.A09 = new C36161jQ(bool);
        this.A0A = new C36161jQ(bool);
        this.A07 = r3;
        this.A08 = r4;
        r3.A03(this);
        A04(r3.A05());
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A07.A04(this);
    }
}
