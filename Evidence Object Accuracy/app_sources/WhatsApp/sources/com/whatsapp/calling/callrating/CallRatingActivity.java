package com.whatsapp.calling.callrating;

import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass19M;
import X.AnonymousClass1BA;
import X.AnonymousClass5V7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14850m9;
import X.C16630pM;
import X.C17140qK;
import X.C17150qL;
import X.C22050yP;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.StarRatingBar;
import com.whatsapp.calling.callrating.CallRatingActivity;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CallRatingActivity extends ActivityC13830kP {
    public static final int[] A0K = {R.string.rating_1, R.string.rating_2, R.string.rating_3, R.string.rating_4, R.string.rating_5};
    public View A00;
    public EditText A01;
    public LinearLayout A02;
    public LinearLayout A03;
    public TextView A04;
    public StarRatingBar A05;
    public AnonymousClass01d A06;
    public AnonymousClass19M A07;
    public C14850m9 A08;
    public C22050yP A09;
    public WamCall A0A;
    public C16630pM A0B;
    public AnonymousClass1BA A0C;
    public C17140qK A0D;
    public C17150qL A0E;
    public Integer A0F;
    public String A0G;
    public boolean A0H;
    public boolean A0I;
    public final AnonymousClass5V7 A0J;

    public CallRatingActivity() {
        this(0);
        this.A0J = new AnonymousClass5V7() { // from class: X.5As
            @Override // X.AnonymousClass5V7
            public final void A9y() {
                CallRatingActivity.this.finish();
            }
        };
    }

    public CallRatingActivity(int i) {
        this.A0H = false;
        ActivityC13830kP.A1P(this, 37);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            this.A08 = C12960it.A0S(A1M);
            this.A07 = (AnonymousClass19M) A1M.A6R.get();
            this.A06 = C12960it.A0Q(A1M);
            this.A09 = (C22050yP) A1M.A7v.get();
            this.A0E = (C17150qL) A1M.AMb.get();
            this.A0C = (AnonymousClass1BA) A1M.A2e.get();
            this.A0D = (C17140qK) A1M.AMZ.get();
            this.A0B = C12990iw.A0e(A1M);
        }
    }

    public final void A22() {
        int i = this.A05.A00;
        String trim = C12970iu.A0p(this.A01).trim();
        View view = this.A00;
        boolean z = false;
        if (i > 0 || trim.codePointCount(0, trim.length()) >= 3) {
            z = true;
        }
        view.setEnabled(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ed, code lost:
        if (r0.longValue() < 1) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f0, code lost:
        if (r1 != null) goto L_0x00f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00f2, code lost:
        r0 = r1.videoEnabled;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00f4, code lost:
        if (r0 == null) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00fa, code lost:
        if (r0.booleanValue() == false) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00fc, code lost:
        r1 = X.C12980iv.A0j();
        r0 = com.whatsapp.R.string.call_video_not_clear;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0103, code lost:
        if (r5 == false) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0105, code lost:
        r0 = com.whatsapp.R.string.group_call_video_not_clear;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0108, code lost:
        X.ActivityC13830kP.A1Q(r1, r3, r0);
        X.ActivityC13830kP.A1Q(X.C12980iv.A0k(), r3, com.whatsapp.R.string.call_video_pause);
        r1 = X.C12970iu.A0h();
        r0 = com.whatsapp.R.string.call_audio_not_clear;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x011c, code lost:
        if (r5 == false) goto L_0x0121;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011e, code lost:
        r0 = com.whatsapp.R.string.group_call_audio_not_clear;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0121, code lost:
        X.ActivityC13830kP.A1Q(r1, r3, r0);
        java.util.Collections.shuffle(r3);
        r7 = new int[]{com.whatsapp.R.id.call_problem_0_checkbox, com.whatsapp.R.id.call_problem_1_checkbox, com.whatsapp.R.id.call_problem_2_checkbox};
        r6 = new int[]{com.whatsapp.R.id.call_problem_0_textview, com.whatsapp.R.id.call_problem_1_textview, com.whatsapp.R.id.call_problem_2_textview};
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x014c, code lost:
        r1 = X.C12970iu.A0M(r14, r6[r5]);
        r1.setText(X.C12960it.A05(((android.util.Pair) r3.get(r5)).second));
        r1.setTag(java.lang.Integer.valueOf(r7[r5]));
        X.C12960it.A0z(r1, r14, 45);
        r1 = findViewById(r7[r5]);
        r1.setTag(((android.util.Pair) r3.get(r5)).first);
        X.C12960it.A0z(r1, r14, 46);
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0187, code lost:
        if (r5 < 3) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0189, code lost:
        X.AbstractView$OnClickListenerC34281fs.A00(r14.A00, r14, 37);
        r14.A01.setFilters(new android.text.InputFilter[]{new X.C100654mG(androidx.core.view.inputmethod.EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)});
        r8 = r14.A01;
        r8.addTextChangedListener(new X.C622635v(r8, r14, r14.A06, ((X.ActivityC13830kP) r14).A01, r14.A07, r14.A0B));
        r14.A0C.A00.add(r14.A0J);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01bb, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01bc, code lost:
        r0 = com.whatsapp.R.string.call_problem_distortion;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01c3, code lost:
        if (r5 == false) goto L_0x01c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01c5, code lost:
        r0 = com.whatsapp.R.string.group_call_problem_distortion;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01c8, code lost:
        X.ActivityC13830kP.A1Q(0, r3, r0);
        X.ActivityC13830kP.A1Q(1, r3, com.whatsapp.R.string.call_problem_echo);
        r1 = X.C12970iu.A0g();
        r0 = com.whatsapp.R.string.call_problem_no_sound;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x01dc, code lost:
        if (r5 == false) goto L_0x0121;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01de, code lost:
        r0 = com.whatsapp.R.string.group_call_problem_no_sound;
     */
    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r15) {
        /*
        // Method dump skipped, instructions count: 490
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callrating.CallRatingActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1BA r0 = this.A0C;
        r0.A00.remove(this.A0J);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        String str;
        super.onStop();
        WamCall wamCall = this.A0A;
        if (wamCall != null) {
            StringBuilder A0k = C12960it.A0k("callratingactivity/postCallEvent with rating ");
            A0k.append(wamCall.userRating);
            C12960it.A1F(A0k);
            C17140qK r1 = this.A0D;
            WamCall wamCall2 = this.A0A;
            if (wamCall2 != null) {
                str = wamCall2.callRandomId;
            } else {
                str = null;
            }
            C12970iu.A1D(r1.A01().edit(), "call_rating_last_call", str);
            this.A09.A07(this.A0A, this.A0I);
            if (this.A0G != null) {
                StringBuilder A0k2 = C12960it.A0k("callratingactivity/uploadTimeSeries with rating ");
                A0k2.append(this.A0A.userRating);
                A0k2.append("time series dir ");
                Log.i(C12960it.A0d(this.A0G, A0k2));
                this.A0E.A02(this.A0A, this.A0G);
            }
            this.A0A = null;
        }
        finish();
    }
}
