package com.whatsapp.calling.callrating;

import X.AbstractC16710pd;
import X.ActivityC13830kP;
import X.AnonymousClass4Yq;
import X.C12980iv;
import X.C71883df;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class CallRatingActivityV2 extends ActivityC13830kP {
    public final AbstractC16710pd A00 = AnonymousClass4Yq.A00(new C71883df(this));

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawable(C12980iv.A0L(this, R.color.wds_cool_gray_alpha_60));
        ((DialogFragment) this.A00.getValue()).A1F(A0V(), "CallRatingBottomSheet");
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        CallRatingBottomSheet callRatingBottomSheet = (CallRatingBottomSheet) this.A00.getValue();
        BottomSheetBehavior bottomSheetBehavior = callRatingBottomSheet.A00;
        if (bottomSheetBehavior != null && 5 == bottomSheetBehavior.A0B) {
            bottomSheetBehavior.A0M(5);
        }
        callRatingBottomSheet.A01.AJ3();
    }
}
