package com.whatsapp.calling.callrating;

import X.AnonymousClass028;
import X.AnonymousClass1WK;
import X.C12960it;
import X.C16700pc;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class CallRatingBottomSheet extends BottomSheetDialogFragment {
    public BottomSheetBehavior A00;
    public final AnonymousClass1WK A01;

    public /* synthetic */ CallRatingBottomSheet(AnonymousClass1WK r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        C16700pc.A0E(layoutInflater, 0);
        View inflate = layoutInflater.inflate(R.layout.call_rating_bottom_sheet, viewGroup);
        if (inflate == null) {
            return null;
        }
        C12960it.A0z(AnonymousClass028.A0D(inflate, R.id.close_button), this, 48);
        C12960it.A0z(AnonymousClass028.A0D(inflate, R.id.submit_button), this, 47);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(AnonymousClass028.A0D(inflate, R.id.bottom_sheet));
        if (A00 == null) {
            A00 = null;
        } else {
            A00.A0J = true;
        }
        this.A00 = A00;
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        C16700pc.A0E(context, 0);
        super.A15(context);
        Log.i("calling/CallRatingBottomSheet onAttach");
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, R.style.CallRatingBottomSheet);
    }
}
