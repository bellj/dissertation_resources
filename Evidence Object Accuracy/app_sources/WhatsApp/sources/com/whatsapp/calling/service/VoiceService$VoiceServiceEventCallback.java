package com.whatsapp.calling.service;

import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass07E;
import X.AnonymousClass16A;
import X.AnonymousClass1L4;
import X.AnonymousClass1S6;
import X.AnonymousClass1S7;
import X.AnonymousClass1SF;
import X.AnonymousClass1YK;
import X.AnonymousClass1YT;
import X.AnonymousClass1YU;
import X.AnonymousClass1YW;
import X.AnonymousClass2KU;
import X.AnonymousClass2MM;
import X.AnonymousClass2O6;
import X.AnonymousClass2O7;
import X.AnonymousClass4NG;
import X.AnonymousClass4NH;
import X.C100544m5;
import X.C15570nT;
import X.C16120oU;
import X.C17150qL;
import X.C18750sx;
import X.C18770sz;
import X.C18790t3;
import X.C19950uw;
import X.C21280xA;
import X.C26391De;
import X.C28471Ni;
import X.C29631Ua;
import X.C32721cd;
import X.C64263Ew;
import X.C65973Lu;
import X.C90504Od;
import X.C90974Py;
import X.C90984Pz;
import android.content.Context;
import android.net.Uri;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0401000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.calling.calllink.viewmodel.CallLinkViewModel;
import com.whatsapp.fieldstats.events.WamJoinableCall;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallGroupInfo;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.CallLinkInfo;
import com.whatsapp.voipcalling.CallOfferAckError;
import com.whatsapp.voipcalling.SyncDevicesUserInfo;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipEventCallback;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class VoiceService$VoiceServiceEventCallback implements VoipEventCallback {
    public final C90504Od bufferQueue = new C90504Od();
    public final C19950uw httpsFormPostFactory;
    public final /* synthetic */ C29631Ua this$0;

    private boolean isFatalErrorCode(int i) {
        return i == 432;
    }

    public VoiceService$VoiceServiceEventCallback(C29631Ua r2, C19950uw r3) {
        this.this$0 = r2;
        this.httpsFormPostFactory = r3;
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void NoSamplingRatesForAudioRecord() {
        Log.i("VoiceService EVENT:NoSamplingRatesForAudioRecord");
        C29631Ua r2 = this.this$0;
        r2.A0Y(23, r2.A1P.getString(R.string.voip_call_failed_audio_record_issue));
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void audioDriverRestart() {
        Log.i("VoiceService EVENT:audioDriverRestart");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void audioInitError() {
        Log.i("VoiceService EVENT:audioInitError");
        this.this$0.A2R.A01().edit().remove("audio_sampling_hash").remove("audio_sampling_rates").apply();
        C29631Ua r2 = this.this$0;
        r2.A0Y(23, r2.A1P.getString(R.string.voip_call_failed_audio_record_issue));
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void audioRouteChangeRequest(int i) {
        Log.i("VoiceService EVENT:audioRouteChangeRequest");
        this.this$0.A0L.removeMessages(27);
        this.this$0.A0L.obtainMessage(27, i, 0).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void audioStreamStarted() {
        Log.i("VoiceService EVENT:audioStreamStarted");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void audioTestReplayFinished() {
        throw new AssertionError("audioTestReplayFinished is a debug only method");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void batteryLevelLow() {
        Log.i("VoiceService EVENT:batteryLevelLow");
        this.this$0.A0L.sendEmptyMessage(18);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callAcceptFailed() {
        Log.i("VoiceService EVENT:callAcceptFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callAcceptReceived() {
        Log.i("VoiceService EVENT:callAcceptReceived");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callAutoConnected(String str, String str2) {
        Log.i("VoiceService EVENT:callAutoConnected");
        Message.obtain(this.this$0.A0L, 31, new AnonymousClass4NG(str, str2)).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callCaptureBufferFilled(Voip.DebugTapType debugTapType, byte[] bArr, int i, Voip.RecordingInfo[] recordingInfoArr) {
        AnonymousClass009.A0E(false);
        if (bArr != null && bArr.length > 0 && i > 0 && recordingInfoArr != null) {
            C29631Ua.A2l.execute(new RunnableBRunnable0Shape0S0401000_I0(this, recordingInfoArr, debugTapType, bArr, i, 1));
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callCaptureEnded(Voip.DebugTapType debugTapType, Voip.RecordingInfo[] recordingInfoArr) {
        AnonymousClass009.A0E(false);
        C29631Ua.A2l.execute(new RunnableBRunnable0Shape1S0200000_I0_1(recordingInfoArr, 43, debugTapType));
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callEnding(Voip.CallLogInfo callLogInfo, int i, String str) {
        Object valueOf;
        C29631Ua r2;
        AnonymousClass1L4 r4;
        Boolean A00;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:callEnding result=");
        if (callLogInfo == null) {
            valueOf = "null";
        } else {
            valueOf = Integer.valueOf(callLogInfo.callLogResultType);
        }
        sb.append(valueOf);
        sb.append(" rating interval=");
        sb.append(i);
        sb.append(" time series dir= ");
        sb.append(str);
        Log.i(sb.toString());
        if (this.this$0.A2h == null) {
            this.this$0.A2h = Integer.valueOf(i);
        }
        this.this$0.A2i = str;
        if (this.this$0.A2g == null) {
            this.this$0.A2g = Voip.A00("options.show_voip_app_update_prompt_dialog");
        }
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo == null) {
            Log.e("we are not in a active call");
            return;
        }
        int i2 = callInfo.callResult;
        if (i2 == 20) {
            Log.i("VoiceService:callEnding send pending relay offer if call was ended from web client");
            C29631Ua r22 = this.this$0;
            UserJid peerJid = callInfo.getPeerJid();
            AnonymousClass009.A05(peerJid);
            r22.A0f(peerJid, callInfo.callId, true);
            return;
        }
        if (i2 == 1 && (A00 = Voip.A00("options.wa_log_dummy_time_series")) != null && A00.booleanValue()) {
            C17150qL r5 = this.this$0.A2S;
            r5.A05.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(r5, 44));
        }
        Boolean A002 = Voip.A00("options.wa_log_time_series");
        if (A002 != null && A002.booleanValue()) {
            this.this$0.A1U.A04();
            File file = new File(this.this$0.A1P.getFilesDir(), "wa_call_time_series.mtar.gz");
            if (file.exists()) {
                try {
                    String A003 = this.this$0.A1T.A00();
                    try {
                        C28471Ni A004 = this.httpsFormPostFactory.A00(null, "https://crashlogs.whatsapp.net/wa_clb_data", 16);
                        A004.A06("access_token", "1063127757113399|745146ffa34413f9dbb5469f5370b7af");
                        A004.A05(new FileInputStream(file), "attachment", file.getName(), 0, file.length());
                        A004.A07("from_jid", A003);
                        A004.A07("tags", "voip_time_series");
                        A004.A07("android_hprof_extras", ((AnonymousClass16A) this.this$0.A1T).A04(null).A00());
                        A004.A02(null);
                    } catch (IOException e) {
                        Log.w("app/VoiceService: could not open time series log data", e);
                    }
                    if (!file.delete()) {
                        Log.i("app/VoiceService: time series log could not be deleted");
                    }
                } finally {
                    if (!file.delete()) {
                        Log.i("app/VoiceService: time series log could not be deleted");
                    }
                }
            }
        }
        this.this$0.A0U();
        int i3 = callInfo.callSetupErrorType;
        if (i3 == 17 && (callInfo.isCaller || callInfo.callState == Voip.CallState.ACCEPT_SENT)) {
            C29631Ua r52 = this.this$0;
            UserJid peerJid2 = callInfo.getPeerJid();
            AnonymousClass009.A05(peerJid2);
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(peerJid2);
            r52.A1V.A0H(new RunnableBRunnable0Shape0S1201000_I0(r52, arrayList, null, 6, 0));
        } else if (callInfo.callResult == 6 && ((i3 == 18 || i3 == 19) && (r4 = (r2 = this.this$0).A0c) != null)) {
            ((VoipActivityV2) r4).A1S = r2.A1P.getString(R.string.voip_call_camera_error);
        }
        AnonymousClass1YT A0E = this.this$0.A0E(callInfo.callId);
        if (A0E == null) {
            AnonymousClass009.A05(callInfo.getInitialPeerJid());
            StringBuilder sb2 = new StringBuilder("VoiceService:callEnding getCallLog with key[jid=");
            sb2.append(callInfo.getInitialPeerJid());
            sb2.append("; fromMe=");
            sb2.append(callInfo.isCaller);
            sb2.append("; callId=");
            sb2.append(callInfo.callId);
            sb2.append("; transactionId=");
            sb2.append(callInfo.initialGroupTransactionId);
            sb2.append("]");
            Log.i(sb2.toString());
            A0E = this.this$0.A1w.A04(new AnonymousClass1YU(callInfo.initialGroupTransactionId, callInfo.getInitialPeerJid(), AnonymousClass1SF.A0B(callInfo.callId), callInfo.isCaller));
            if (A0E == null) {
                StringBuilder sb3 = new StringBuilder("can not find message for call ");
                sb3.append(callInfo.callId);
                AnonymousClass009.A07(sb3.toString());
                return;
            }
        }
        int i4 = 0;
        if (callLogInfo != null) {
            if (!(callInfo.isJoinableGroupCall && callInfo.callState == Voip.CallState.REJOINING && A0E.A00 == 5)) {
                A0E.A06(callLogInfo.callLogResultType);
            }
            Map map = callLogInfo.groupCallLogs;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    UserJid nullable = UserJid.getNullable((String) entry.getKey());
                    if (nullable != null) {
                        A0E.A08(nullable, ((Number) entry.getValue()).intValue());
                    } else {
                        StringBuilder sb4 = new StringBuilder("VoiceService:callEnding got a bad group participant jid: ");
                        sb4.append((String) entry.getKey());
                        Log.e(sb4.toString());
                    }
                }
            }
            long j = callLogInfo.txTotalBytes;
            if (j < 0 || j > 1073741824) {
                StringBuilder sb5 = new StringBuilder("Not recording too big value for txTotalBytes ");
                sb5.append(j);
                Log.e(sb5.toString());
            } else {
                i4 = (int) (((long) 0) + j);
                C18790t3 r3 = this.this$0.A1Y;
                r3.A04(j, 2);
                r3.A06.A01(j);
            }
            long j2 = callLogInfo.rxTotalBytes;
            if (j2 < 0 || j2 > 1073741824) {
                StringBuilder sb6 = new StringBuilder("Not recording too big value for rxTotalBytes ");
                sb6.append(j2);
                Log.e(sb6.toString());
            } else {
                i4 = (int) (((long) i4) + j2);
                C18790t3 r23 = this.this$0.A1Y;
                r23.A03(j2, 2);
                r23.A06.A02(j2, 2);
            }
        }
        int max = A0E.A01 + ((int) ((Math.max(0L, callInfo.callDuration) + 999) / 1000));
        synchronized (A0E) {
            if (A0E.A01 != max) {
                A0E.A0I = true;
            }
            A0E.A01 = max;
        }
        long j3 = A0E.A02 + ((long) i4);
        synchronized (A0E) {
            if (A0E.A02 != j3) {
                A0E.A0I = true;
            }
            A0E.A02 = j3;
        }
        A0E.A0A(callInfo.videoEnabled);
        this.this$0.A0e(callInfo.groupJid, A0E);
        StringBuilder sb7 = new StringBuilder("VoiceService:callEnding update call log db, call result = ");
        sb7.append(A0E.A00);
        sb7.append(", video=");
        sb7.append(A0E.A0H);
        sb7.append(", duration=");
        sb7.append(A0E.A01);
        sb7.append(", total data usage: ");
        sb7.append(i4);
        sb7.append("B");
        Log.i(sb7.toString());
        this.this$0.A1w.A08(A0E);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callGridRankingChanged() {
        this.this$0.A0L.sendEmptyMessage(45);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callLinkStateChanged(int i, CallLinkInfo callLinkInfo) {
        if (callLinkInfo != null) {
            int i2 = callLinkInfo.linkState;
            StringBuilder sb = new StringBuilder("VoiceService EVENT:callLinkStateChanged(");
            sb.append(Voip.A06(i));
            sb.append(", ");
            sb.append(Voip.A06(i2));
            sb.append(')');
            Log.i(sb.toString());
            if (i == 3 && i2 == 4) {
                C29631Ua.A06(this.this$0, callLinkInfo.videoEnabled);
                CallInfo callInfo = Voip.getCallInfo();
                AnonymousClass009.A05(callInfo);
                if (this.this$0.A0E(callInfo.callId) == null) {
                    AnonymousClass1YW A0C = this.this$0.A0C(callLinkInfo.creatorJid, callLinkInfo.token);
                    UserJid peerJid = callInfo.getPeerJid();
                    AnonymousClass009.A05(peerJid);
                    int i3 = callInfo.initialGroupTransactionId;
                    UserJid creatorJid = callInfo.getCreatorJid();
                    AnonymousClass009.A05(creatorJid);
                    DeviceJid primaryDevice = creatorJid.getPrimaryDevice();
                    C29631Ua r4 = this.this$0;
                    C18750sx r7 = r4.A1w;
                    String A0B = AnonymousClass1SF.A0B(callInfo.callId);
                    long A00 = r4.A1o.A00();
                    boolean z = callLinkInfo.videoEnabled;
                    AnonymousClass009.A05(primaryDevice);
                    AnonymousClass1YT A03 = r7.A03(primaryDevice, peerJid, A0B, i3, A00, true, z, false);
                    this.this$0.A0p(A03, true);
                    synchronized (A03) {
                        A03.A0F = A0C;
                        A03.A0I = true;
                    }
                    this.this$0.A1w.A09(A03);
                }
            }
            Message obtain = Message.obtain(this.this$0.A0L, 42, callLinkInfo);
            obtain.arg1 = i;
            obtain.sendToTarget();
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callMissed(String str, String str2, String str3, String str4, int i, long j, boolean z, CallGroupInfo callGroupInfo, boolean z2, boolean z3, boolean z4) {
        int i2;
        boolean z5;
        GroupJid groupJid;
        Log.i("VoiceService EVENT:callMissed");
        if (callGroupInfo == null || callGroupInfo.participants.length <= 0) {
            i2 = -1;
        } else {
            i2 = callGroupInfo.transactionId;
        }
        C29631Ua r0 = this.this$0;
        UserJid nullable = UserJid.getNullable(str2);
        AnonymousClass009.A05(nullable);
        AnonymousClass1YT A04 = r0.A1w.A04(new AnonymousClass1YU(i2, nullable, AnonymousClass1SF.A0B(str), false));
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo != null) {
            AnonymousClass1S7 r1 = callInfo.callWaitingInfo;
            z5 = str.equals(r1.A04);
            if (A04 != null) {
                if (str.equals(callInfo.callId)) {
                    groupJid = callInfo.groupJid;
                } else {
                    groupJid = z5 ? r1.A02 : null;
                }
                this.this$0.A0e(groupJid, A04);
            } else {
                return;
            }
        } else {
            z5 = false;
            if (A04 == null) {
                return;
            }
        }
        this.this$0.A0i(callGroupInfo, A04, Integer.valueOf(i), str, str3, str4, 4, j, z, z5, z2, z3, z4);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callOfferAcked() {
        Log.i("VoiceService EVENT:callOfferAcked");
        this.this$0.A0K.removeMessages(2);
        this.this$0.A10 = true;
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo != null) {
            if (callInfo.isJoinableGroupCall) {
                C29631Ua r1 = this.this$0;
                UserJid initialPeerJid = callInfo.getInitialPeerJid();
                AnonymousClass009.A05(initialPeerJid);
                boolean z = callInfo.isCaller;
                String str = callInfo.callId;
                AnonymousClass1YT A04 = r1.A1w.A04(new AnonymousClass1YU(callInfo.initialGroupTransactionId, initialPeerJid, AnonymousClass1SF.A0B(str), z));
                if (A04 != null) {
                    for (UserJid userJid : callInfo.participants.keySet()) {
                        AnonymousClass1S6 r0 = (AnonymousClass1S6) callInfo.participants.get(userJid);
                        if (r0 != null && !r0.A0F) {
                            A04.A08(userJid, 2);
                        }
                    }
                    A04.A0A(callInfo.videoEnabled);
                    this.this$0.A0e(callInfo.groupJid, A04);
                    C29631Ua r12 = this.this$0;
                    if (A04.A06 == null) {
                        r12.A0p(A04, true);
                    }
                    this.this$0.A1w.A08(A04);
                }
                this.this$0.A2R.A05(callInfo.callId);
            }
            C29631Ua.A06(this.this$0, callInfo.videoEnabled);
            if (Voip.A00("options.caller_end_call_threshold") != null) {
                this.this$0.A0i = Voip.A01("options.caller_end_call_threshold");
            }
            Voip.CallState callState = callInfo.callState;
            if (callState == Voip.CallState.CALLING || callState == Voip.CallState.PRE_ACCEPT_RECEIVED) {
                Integer A01 = Voip.A01("options.caller_timeout");
                if (A01 != null) {
                    long intValue = (long) (A01.intValue() * 1000);
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    C29631Ua r9 = this.this$0;
                    long j = intValue - (elapsedRealtime - r9.A09);
                    if (j > 0 && j < 120000) {
                        r9.A0K.removeCallbacksAndMessages(null);
                        this.this$0.A0K.sendEmptyMessageDelayed(0, j);
                        StringBuilder sb = new StringBuilder("voip/receive_message/call-offer-ack change the caller timeout to ");
                        sb.append(intValue);
                        sb.append(", remaining ");
                        sb.append(j);
                        Log.i(sb.toString());
                    }
                }
                this.this$0.A0s(callInfo.callState, callInfo.callId);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        if (isSelfNacked(r13) != false) goto L_0x0055;
     */
    @Override // com.whatsapp.voipcalling.VoipEventCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOfferNacked(com.whatsapp.voipcalling.CallOfferAckError[] r13) {
        /*
            r12 = this;
            com.whatsapp.voipcalling.CallInfo r8 = com.whatsapp.voipcalling.Voip.getCallInfo()
            if (r8 != 0) goto L_0x000d
            java.lang.String r0 = "we are not in a active call"
        L_0x0009:
            com.whatsapp.util.Log.e(r0)
            return
        L_0x000d:
            if (r13 == 0) goto L_0x0028
            int r10 = r13.length
            if (r10 == 0) goto L_0x0028
            r11 = 0
            r7 = 1
            if (r10 != r7) goto L_0x002b
            r0 = r13[r11]
            int r1 = r0.errorCode
            r0 = 304(0x130, float:4.26E-43)
            if (r1 == r0) goto L_0x0022
            r0 = 400(0x190, float:5.6E-43)
            if (r1 != r0) goto L_0x002b
        L_0x0022:
            java.lang.String r0 = "Server received duplicate offers. Just return"
            com.whatsapp.util.Log.w(r0)
            return
        L_0x0028:
            java.lang.String r0 = "Received offer nack without any errors"
            goto L_0x0009
        L_0x002b:
            java.lang.String r1 = "VoiceService EVENT:callOfferNacked error: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>(r10)
            java.util.Map r0 = r8.participants
            int r1 = r0.size()
            int r1 = r1 - r7
            r0 = r13[r11]
            int r5 = r0.errorCode
            if (r1 == r10) goto L_0x0055
            boolean r0 = r12.isSelfNacked(r13)
            r9 = 0
            if (r0 == 0) goto L_0x0056
        L_0x0055:
            r9 = 1
        L_0x0056:
            r4 = 0
        L_0x0057:
            r3 = r13[r4]
            int r2 = r3.errorCode
            r1 = 432(0x1b0, float:6.05E-43)
            r0 = 0
            if (r2 != r1) goto L_0x0061
            r0 = 1
        L_0x0061:
            if (r9 != 0) goto L_0x0088
            if (r0 == 0) goto L_0x0067
            int r5 = r3.errorCode
        L_0x0067:
            int r0 = r3.errorCode
            if (r0 == r1) goto L_0x0088
            r9 = 0
        L_0x006c:
            com.whatsapp.jid.UserJid r0 = r3.errorJid
            r6.add(r0)
            int r4 = r4 + 1
            if (r4 < r10) goto L_0x0057
            com.whatsapp.voipcalling.Voip$CallState r1 = r8.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.CALLING
            if (r1 != r0) goto L_0x007e
            if (r9 == 0) goto L_0x007e
            r11 = 1
        L_0x007e:
            X.1Ua r0 = r12.this$0
            r0.A10 = r7
            if (r11 == 0) goto L_0x008a
            r12.handleFatalOfferNack(r6, r5, r8)
            return
        L_0x0088:
            r9 = 1
            goto L_0x006c
        L_0x008a:
            r12.handleNonFatalOfferNack(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback.callOfferNacked(com.whatsapp.voipcalling.CallOfferAckError[]):void");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callOfferReceiptReceived() {
        Log.i("VoiceService EVENT:callOfferReceiptReceived");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callOfferReceived() {
        Log.i("VoiceService EVENT:callOfferReceived");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callOfferSent() {
        Log.i("VoiceService EVENT:callOfferSent");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callPreAcceptReceived() {
        Log.i("VoiceService EVENT:callPreAcceptReceived");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0090  */
    @Override // com.whatsapp.voipcalling.VoipEventCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callRejectReceived(java.lang.String r12, java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 270
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback.callRejectReceived(java.lang.String, java.lang.String):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0193  */
    @Override // com.whatsapp.voipcalling.VoipEventCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callStateChanged(com.whatsapp.voipcalling.Voip.CallState r15, com.whatsapp.voipcalling.CallInfo r16) {
        /*
        // Method dump skipped, instructions count: 466
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback.callStateChanged(com.whatsapp.voipcalling.Voip$CallState, com.whatsapp.voipcalling.CallInfo):void");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callTerminateReceived(String str) {
        AnonymousClass2KU r1;
        Pair A00;
        AnonymousClass2O6 r5;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:callTerminateReceived, callId:");
        sb.append(str);
        Log.i(sb.toString());
        if (str != null && !TextUtils.equals(Voip.getCurrentCallId(), str) && C21280xA.A02() && (A00 = (r1 = Voip.A01).A00(str)) != null && (r5 = (AnonymousClass2O6) A00.second) != null) {
            String A0B = AnonymousClass1SF.A0B(str);
            C18750sx r7 = this.this$0.A1w;
            AnonymousClass2O7 r6 = r5.A01;
            Jid jid = ((AnonymousClass2MM) r6).A00;
            AnonymousClass009.A05(jid);
            AnonymousClass1YT A04 = r7.A04(new AnonymousClass1YU(r5.A00, ((DeviceJid) jid).getUserJid(), A0B, false));
            if (A04 != null) {
                C29631Ua r72 = this.this$0;
                long j = r6.A00;
                String str2 = r6.A05;
                String str3 = r6.A04;
                CallGroupInfo callGroupInfo = r5.A02;
                boolean z = r5.A03;
                boolean z2 = false;
                if (A04.A06 != null) {
                    z2 = true;
                }
                r72.A0i(callGroupInfo, A04, 5, str, str2, str3, 4, j, false, false, z, z2, false);
            }
            r1.A01(str);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void callWaitingStateChanged(int i) {
        String str;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:callWaitingStateChanged ");
        sb.append(i);
        Log.i(sb.toString());
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo != null) {
            str = callInfo.callWaitingInfo.A04;
        } else {
            str = null;
        }
        this.this$0.A0L.removeMessages(34);
        this.this$0.A0L.obtainMessage(34, i, 0, str).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void errorGatheringHostCandidates() {
        Log.i("VoiceService EVENT:errorGatheringHostCandidates");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0161, code lost:
        if (((android.media.audiofx.AcousticEchoCanceler) r0).getEnabled() != false) goto L_0x0163;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x03b2  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x03d1  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0409  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x020c  */
    @Override // com.whatsapp.voipcalling.VoipEventCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void fieldstatsReady(com.whatsapp.fieldstats.events.WamCall r25, java.lang.String r26, boolean r27, boolean r28) {
        /*
        // Method dump skipped, instructions count: 1140
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback.fieldstatsReady(com.whatsapp.fieldstats.events.WamCall, java.lang.String, boolean, boolean):void");
    }

    public byte[] getByteBuffer(int i) {
        byte[] bArr;
        C90504Od r3 = this.bufferQueue;
        synchronized (r3) {
            Iterator it = r3.A01.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r3.A00 += i;
                    bArr = new byte[i];
                    break;
                }
                bArr = (byte[]) it.next();
                if (bArr.length >= i) {
                    it.remove();
                    break;
                }
            }
        }
        return bArr;
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void groupInfoChanged() {
        AnonymousClass1YT A0E;
        Log.i("VoiceService EVENT:groupInfoChanged");
        CallInfo callInfo = Voip.getCallInfo();
        boolean z = true;
        if (callInfo == null) {
            z = false;
        } else if (callInfo.callState != Voip.CallState.NONE) {
            if (AnonymousClass1SF.A0O(this.this$0.A20) && (A0E = this.this$0.A0E(callInfo.callId)) != null) {
                AnonymousClass1YU r5 = A0E.A0B;
                UserJid userJid = r5.A01;
                C15570nT r0 = this.this$0.A1W;
                r0.A08();
                if (userJid.equals(r0.A05)) {
                    C18750sx r4 = this.this$0.A1w;
                    UserJid peerJid = callInfo.getPeerJid();
                    AnonymousClass009.A05(peerJid);
                    AnonymousClass1YT A05 = r4.A05(new AnonymousClass1YU(r5.A00, peerJid, r5.A02, true), A0E);
                    A05.A08(callInfo.getPeerJid(), 2);
                    this.this$0.A1w.A09(A05);
                }
            }
            this.this$0.A0L.removeMessages(25);
            this.this$0.A0L.sendEmptyMessage(25);
            return;
        }
        AnonymousClass009.A0A(" CallInfo should not be null in groupInfoChanged callback", z);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void groupParticipantLeft(String str, String str2, int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:groupParticipantLeft (");
        sb.append(i);
        sb.append(")");
        Log.i(sb.toString());
        if (i == 4) {
            if (str2 == null) {
                str2 = "";
            }
            callRejectReceived(str, str2);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleAcceptAckFailed(String str, int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:handleAcceptAckFailed, error_code = ");
        sb.append(i);
        sb.append("error_raw_device_jid: ");
        sb.append(str);
        Log.i(sb.toString());
        C29631Ua r2 = this.this$0;
        int i2 = 26;
        if (i != 434) {
            i2 = 25;
        }
        r2.A0Y(i2, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleAcceptFailed() {
        Log.i("VoiceService EVENT:handleAcceptFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleCallFatal(int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:handleCallFatal Reason: ");
        sb.append(i);
        Log.i(sb.toString());
        AbstractC15710nm r3 = this.this$0.A1T;
        StringBuilder sb2 = new StringBuilder("voip/callFatal Reason:");
        sb2.append(i);
        r3.AaV("VoiceServiceEventCallback/handleCallFatal", sb2.toString(), true);
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleFDLeakDetected() {
        Log.i("VoiceService EVENT:handleFDLeakDetected");
    }

    private void handleFatalOfferNack(ArrayList arrayList, int i, CallInfo callInfo) {
        int i2 = 3;
        if (i != 401) {
            i2 = 10;
            int i3 = 2;
            if (i != 460) {
                if (i == 411) {
                    r0.A1V.A0I(new RunnableBRunnable0Shape0S0201000_I0(this.this$0, arrayList, 11, 8));
                } else if (i == 412) {
                    r0.A1V.A0I(new RunnableBRunnable0Shape0S0201000_I0(this.this$0, arrayList, 10, 8));
                } else if (i == 431) {
                    i2 = 15;
                } else if (i != 432) {
                    i2 = 0;
                    switch (i) {
                        case 403:
                            if (callInfo.videoEnabled) {
                                i2 = 7;
                                break;
                            }
                            break;
                        case 404:
                            i2 = 16;
                            break;
                        case 405:
                            i2 = 4;
                            break;
                        case 406:
                            if (callInfo.videoEnabled) {
                                i3 = 11;
                            }
                            i2 = i3;
                            break;
                        default:
                            switch (i) {
                                case 426:
                                    i2 = 1;
                                    if (callInfo.videoEnabled) {
                                        i2 = 9;
                                        break;
                                    }
                                    break;
                                case 427:
                                    i2 = 12;
                                    break;
                                case 428:
                                    i2 = 14;
                                    break;
                                default:
                                    if (callInfo.videoEnabled) {
                                        i2 = 8;
                                        break;
                                    }
                                    break;
                            }
                    }
                } else {
                    r0.A1V.A0I(new RunnableBRunnable0Shape0S0201000_I0(this.this$0, arrayList, 2, 8));
                }
                i2 = -1;
            } else if (!callInfo.videoEnabled) {
                i2 = 2;
            }
        }
        if (i2 != -1) {
            C29631Ua r3 = this.this$0;
            r3.A1V.A0H(new RunnableBRunnable0Shape0S1201000_I0(r3, arrayList, null, i2, 0));
        }
        this.this$0.A0Y(25, null);
    }

    private void handleNonFatalOfferNack(List list, int i) {
        Context context;
        int i2;
        String string;
        AnonymousClass018 r6;
        int i3;
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(this.this$0.A1j.A0A(this.this$0.A1i.A0B((AbstractC14640lm) it.next()), -1));
        }
        String A00 = C32721cd.A00(this.this$0.A1j.A04, arrayList, true);
        int i4 = 1;
        if (i == 427) {
            CallInfo callInfo = Voip.getCallInfo();
            AnonymousClass1S6 r2 = null;
            if (callInfo != null) {
                if (list.size() == 1) {
                    r2 = callInfo.getInfoByJid((UserJid) list.get(0));
                }
                if (callInfo.isGroupCall() && callInfo.participants.size() > 4) {
                    C29631Ua r1 = this.this$0;
                    if (r2 != null && r2.A0E) {
                        i4 = 6;
                    }
                    r1.A1V.A0I(new RunnableBRunnable0Shape0S0201000_I0(r1, list, i4, 8));
                    return;
                } else if (r2 != null && r2.A01 == 1) {
                    context = this.this$0.A1P;
                    i2 = R.string.voip_peer_group_call_not_supported;
                    string = context.getString(i2, arrayList.get(0));
                }
            }
            r6 = this.this$0.A1s;
            i3 = R.plurals.voip_group_call_not_supported_plural;
            string = r6.A0I(new Object[]{A00}, i3, (long) list.size());
        } else if (i == 428) {
            context = this.this$0.A1P;
            i2 = R.string.voip_group_call_reach_maximum_during_call;
            string = context.getString(i2, arrayList.get(0));
        } else if (i == 431) {
            string = this.this$0.A1P.getString(R.string.unable_to_add_participant_to_group_call, A00);
        } else if (i != 435) {
            AnonymousClass009.A0A("Unknown error code", false);
            r6 = this.this$0.A1s;
            i3 = R.plurals.voip_not_allowed_at_this_time_plural;
            string = r6.A0I(new Object[]{A00}, i3, (long) list.size());
        } else {
            string = this.this$0.A1P.getString(R.string.unable_to_add_participant_to_linked_group_call);
        }
        Message.obtain(this.this$0.A0L, 26, string).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleOfferAckFailed() {
        Log.i("VoiceService EVENT:handleOfferAckFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleOfferFailed() {
        Log.i("VoiceService EVENT:handleOfferFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handlePreAcceptFailed() {
        Log.i("VoiceService EVENT:handlePreAcceptFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void handleVoipAssert(String str, int i) {
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":");
        sb.append(i);
        String obj = sb.toString();
        if (this.this$0.A2d.add(obj)) {
            AbstractC15710nm r2 = this.this$0.A1T;
            StringBuilder sb2 = new StringBuilder("voip-assert:");
            sb2.append(str);
            r2.AaV(sb2.toString(), obj, false);
            str2 = " (first occurence)";
        } else {
            str2 = "";
        }
        StringBuilder sb3 = new StringBuilder("VoipAssert at ");
        sb3.append(obj);
        sb3.append(str2);
        Log.e(sb3.toString());
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void heartbeatNacked(int i, String str) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:heartbeatNacked callId: ");
        sb.append(str);
        sb.append(" errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        if (str.equals(Voip.getCurrentCallId())) {
            this.this$0.A0Y(25, null);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void interruptionStateChanged() {
        Log.i("VoiceService EVENT:interruptionStateChanged");
        this.this$0.A0L.removeMessages(30);
        this.this$0.A0L.obtainMessage(30).sendToTarget();
        this.this$0.A1g.A00(new C26391De("refresh_notification"));
    }

    private boolean isSelfNacked(CallOfferAckError[] callOfferAckErrorArr) {
        for (CallOfferAckError callOfferAckError : callOfferAckErrorArr) {
            if (this.this$0.A1W.A0F(callOfferAckError.errorJid)) {
                return true;
            }
        }
        return false;
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void joinableFieldstatsReady(WamJoinableCall wamJoinableCall, boolean z) {
        long j;
        long j2;
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo == null) {
            Log.e("VoiceService:joinableFieldstatsReady not in an active call");
            return;
        }
        AnonymousClass4NH A0D = this.this$0.A0D(callInfo.callId);
        String A0H = this.this$0.A0H(callInfo.callId, wamJoinableCall.callRandomId);
        wamJoinableCall.callRandomId = A0H;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:joinableFieldstatsReady callId:");
        sb.append(callInfo.callId);
        sb.append(" callRandomId:");
        sb.append(A0H);
        sb.append(" realtime:");
        sb.append(z);
        Log.i(sb.toString());
        C29631Ua r7 = this.this$0;
        int i = r7.A0k;
        if (i == null) {
            if (r7.A0B == 0) {
                i = 5;
            } else {
                AnonymousClass009.A0A("Bug with tracking call lobby", false);
                r7 = this.this$0;
                i = 0;
            }
            r7.A0k = i;
        }
        wamJoinableCall.lobbyEntryPoint = i;
        long j3 = r7.A0B;
        if (j3 > 0) {
            wamJoinableCall.lobbyVisibleT = Long.valueOf(System.currentTimeMillis() - j3);
        }
        if (r7.A15) {
            wamJoinableCall.hasSpamDialog = Boolean.TRUE;
        }
        if (A0D.A01) {
            wamJoinableCall.isRering = Boolean.TRUE;
        }
        if (r7.A1F) {
            wamJoinableCall.previousJoinNotEnded = Boolean.TRUE;
        }
        Boolean bool = wamJoinableCall.isRejoin;
        if (bool != null && bool.booleanValue()) {
            C90984Pz r3 = r7.A0V;
            if (r3 != null) {
                long j4 = r3.A00;
                if (j4 == 0) {
                    j2 = -1;
                } else {
                    j2 = j4 - r3.A01;
                }
                wamJoinableCall.lobbyAckLatencyMs = Long.valueOf(j2);
            }
            C90984Pz r32 = r7.A0U;
            if (r32 != null) {
                long j5 = r32.A00;
                if (j5 == 0) {
                    j = -1;
                } else {
                    j = j5 - r32.A01;
                }
                wamJoinableCall.acceptAckLatencyMs = Long.valueOf(j);
            }
            long j6 = r7.A2R.A01().getLong("zombie_cleanup", 0);
            if (!(wamJoinableCall.lobbyExitNackCode == null || j6 == 0)) {
                wamJoinableCall.timeSinceLastClientPollMinutes = Long.valueOf(((System.currentTimeMillis() - j6) / 1000) / 60);
            }
        }
        C16120oU r0 = this.this$0.A21.A0G;
        r0.A06(wamJoinableCall);
        if (z) {
            r0.A01();
        }
    }

    public /* synthetic */ void lambda$callCaptureBufferFilled$3(Voip.RecordingInfo[] recordingInfoArr, Voip.DebugTapType debugTapType, byte[] bArr, int i) {
        int ordinal = debugTapType.ordinal();
        Voip.RecordingInfo recordingInfo = recordingInfoArr[ordinal];
        if (recordingInfo == null) {
            recordingInfoArr[ordinal] = new Voip.RecordingInfo(this.this$0.A1U, debugTapType);
            recordingInfo = recordingInfoArr[ordinal];
        }
        OutputStream outputStream = recordingInfo.outputStream;
        if (outputStream == null) {
            Log.e("voip/callCaptureBufferFilled/OutputStream/null");
        } else {
            try {
                outputStream.write(bArr, 0, i);
            } catch (IOException e) {
                Log.e(e);
            }
            C90504Od r1 = this.bufferQueue;
            synchronized (r1) {
                if (bArr != null) {
                    r1.A01.addFirst(bArr);
                }
            }
            if (recordingInfo.outputFile.length() >= 52428800) {
                Log.i("callCaptureBufferFilled stop recording due to exceeds file size limit");
            } else {
                return;
            }
        }
        Voip.stopCallRecording();
    }

    public static /* synthetic */ void lambda$callCaptureEnded$4(Voip.RecordingInfo[] recordingInfoArr, Voip.DebugTapType debugTapType) {
        OutputStream outputStream;
        Voip.RecordingInfo recordingInfo = recordingInfoArr[debugTapType.ordinal()];
        if (recordingInfo != null && (outputStream = recordingInfo.outputStream) != null) {
            try {
                outputStream.close();
                StringBuilder sb = new StringBuilder();
                sb.append("VoiceService EVENT:callCaptureEnded ");
                sb.append(recordingInfo.outputFile);
                sb.append(" size ");
                sb.append(recordingInfo.outputFile.length());
                Log.i(sb.toString());
            } catch (IOException e) {
                Log.e(e);
            }
        }
    }

    public /* synthetic */ void lambda$callStateChanged$0(CallInfo callInfo) {
        C64263Ew r3 = this.this$0.A2K;
        UserJid initialPeerJid = callInfo.getInitialPeerJid();
        AnonymousClass009.A05(initialPeerJid);
        if (r3.A0H.A07(520)) {
            r3.A0J.A01(initialPeerJid);
            r3.A0D.A01().A00(initialPeerJid);
        }
    }

    public /* synthetic */ void lambda$linkCreateAcked$1(String str, boolean z) {
        String str2;
        for (C90974Py r2 : this.this$0.A1d.A01()) {
            Uri.Builder authority = new Uri.Builder().scheme("https").authority("call.whatsapp.com");
            if (z) {
                str2 = "video";
            } else {
                str2 = "voice";
            }
            String obj = authority.appendPath(str2).appendPath(str).build().toString();
            for (CallLinkViewModel callLinkViewModel : r2.A02) {
                AnonymousClass07E r4 = callLinkViewModel.A02;
                r4.A04("saved_state_is_video", Boolean.valueOf(z));
                int i = R.string.call_link_share_text_voice;
                if (z) {
                    i = R.string.call_link_share_text_video;
                }
                r4.A04("saved_state_link", new C65973Lu(obj, 1, 0, R.color.list_item_title, i));
                boolean A05 = callLinkViewModel.A05();
                int i2 = R.drawable.ic_btn_call_audio;
                int i3 = R.string.voice_call_link;
                if (A05) {
                    i2 = R.drawable.ic_btn_call_video;
                    i3 = R.string.video_call_link;
                }
                r4.A04("saved_state_link_type", new C100544m5(i2, i3, !callLinkViewModel.A05()));
            }
        }
    }

    public /* synthetic */ void lambda$linkCreateNacked$2() {
        for (C90974Py r0 : this.this$0.A1d.A01()) {
            for (CallLinkViewModel callLinkViewModel : r0.A02) {
                callLinkViewModel.A02.A04("saved_state_link", new C65973Lu("", 2, 0, R.color.list_item_title, 0));
            }
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void linkCreateAcked(String str, boolean z) {
        String str2;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:linkCreateAcked token: ");
        sb.append(str);
        sb.append(" media: ");
        if (z) {
            str2 = "video";
        } else {
            str2 = "audio";
        }
        sb.append(str2);
        Log.i(sb.toString());
        this.this$0.A1V.A0H(new RunnableBRunnable0Shape0S1110000_I0(this, str, 3, z));
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void linkCreateNacked(int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:linkCreateNacked errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        this.this$0.A1V.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 26));
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void linkJoinNacked(int i) {
        int i2;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:linkJoinNacked errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        C29631Ua r1 = this.this$0;
        if (i == 400 || i == 404) {
            i2 = 22;
        } else {
            i2 = 24;
            if (i != 428) {
                i2 = 23;
            }
        }
        Message obtainMessage = r1.A0L.obtainMessage(44);
        obtainMessage.arg1 = i2;
        obtainMessage.sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void linkQueryNacked(int i) {
        int i2;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:linkQueryNacked errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        C29631Ua r1 = this.this$0;
        if (i == 400 || i == 404) {
            i2 = 22;
        } else {
            i2 = 24;
            if (i != 428) {
                i2 = 23;
            }
        }
        Message obtainMessage = r1.A0L.obtainMessage(44);
        obtainMessage.arg1 = i2;
        obtainMessage.sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void lobbyNacked(int i, String str) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:lobbyNacked callId: ");
        sb.append(str);
        sb.append(" errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        if (str.equals(Voip.getCurrentCallId())) {
            this.this$0.A0Y(25, null);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void lobbyTimeout() {
        Log.i("VoiceService EVENT:lobbyTimeout");
        Voip.CallState currentCallState = Voip.getCurrentCallState();
        Voip.CallState callState = Voip.CallState.REJOINING;
        C29631Ua r2 = this.this$0;
        if (currentCallState == callState) {
            r2.A0Y(28, null);
            return;
        }
        try {
            if (Voip.getCurrentCallState() == Voip.CallState.LINK) {
                CallLinkInfo callLinkInfo = Voip.getCallLinkInfo();
                AnonymousClass009.A05(callLinkInfo);
                int i = callLinkInfo.linkState;
                if (i == 1 || i == 2) {
                    Message obtainMessage = this.this$0.A0L.obtainMessage(44);
                    obtainMessage.arg1 = 21;
                    obtainMessage.sendToTarget();
                }
            }
        } catch (UnsatisfiedLinkError e) {
            Log.e("unable to query for current call state", e);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void lonelyStateTimeout() {
        Log.i("VoiceService EVENT:lonelyStateTimeout");
        this.this$0.A0Y(27, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void mediaStreamError() {
        Log.i("VoiceService EVENT:mediaStreamError");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void mediaStreamStartError() {
        Log.i("VoiceService EVENT:mediaStreamStartError");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void missingRelayInfo() {
        Log.i("VoiceService EVENT:missingRelayInfo");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void muteRequestFailed(UserJid userJid) {
        Log.i("VoiceService EVENT:muteRequestFailed");
        this.this$0.A0L.removeMessages(43);
        Message message = new Message();
        message.what = 43;
        message.obj = userJid;
        this.this$0.A0L.sendMessage(message);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void muteStateChanged() {
        Log.i("VoiceService EVENT:muteStateChanged");
        this.this$0.A0L.removeMessages(32);
        this.this$0.A0L.sendEmptyMessage(32);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void mutedByOthers(UserJid userJid) {
        Log.i("VoiceService EVENT:mutedByOthers");
        this.this$0.A0L.removeMessages(41);
        Message message = new Message();
        message.what = 41;
        message.obj = userJid;
        this.this$0.A0L.sendMessage(message);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pNegotaitionFailed() {
        Log.i("VoiceService EVENT:p2pNegotaitionFailed");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pNegotiationSuccess() {
        Log.i("VoiceService EVENT:p2pNegotiationSuccess");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pTransportCreateFailed() {
        Log.i("VoiceService EVENT:p2pTransportCreateFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pTransportMediaCreateFailed() {
        Log.i("VoiceService EVENT:p2pTransportMediaCreateFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pTransportRestartSuccess() {
        Log.i("VoiceService EVENT:p2pTransportRestartSuccess");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void p2pTransportStartFailed() {
        Log.i("VoiceService EVENT:p2pTransportStartFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void peerBatteryLevelLow(String str) {
        UserJid nullable = UserJid.getNullable(str);
        AnonymousClass009.A05(nullable);
        StringBuilder sb = new StringBuilder("VoiceService EVENT:peerBatteryLevelLow, Jid:");
        sb.append(nullable);
        Log.i(sb.toString());
        C29631Ua r1 = this.this$0;
        Message message = new Message();
        message.what = 19;
        message.obj = nullable;
        r1.A0L.sendMessageDelayed(message, 3000);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void peerVideoStateChanged(int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:peerVideoStateChanged ");
        sb.append(i);
        Log.i(sb.toString());
        this.this$0.A0L.removeMessages(12);
        this.this$0.A0L.obtainMessage(12, i, 0).sendToTarget();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0045, code lost:
        if (r7.this$0.A2P.A00 == 2) goto L_0x0047;
     */
    @Override // com.whatsapp.voipcalling.VoipEventCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void playCallTone(int r8) {
        /*
            r7 = this;
            java.lang.String r1 = "VoiceService EVENT:playCallTone type:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            X.1Ua r0 = r7.this$0
            java.util.Map r1 = r0.A2b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            java.lang.Object r3 = r1.get(r0)
            java.lang.Number r3 = (java.lang.Number) r3
            X.1Ua r4 = r7.this$0
            android.media.SoundPool r0 = r4.A0I
            if (r0 != 0) goto L_0x002e
            r2 = 1
            r1 = 0
            android.media.SoundPool r0 = new android.media.SoundPool
            r0.<init>(r2, r1, r1)
            r4.A0I = r0
        L_0x002e:
            if (r3 == 0) goto L_0x005a
            X.1Ua r0 = r7.this$0
            X.0m6 r1 = r0.A1r
            X.0m9 r0 = r0.A20
            boolean r0 = X.AnonymousClass1SF.A0M(r1, r0)
            if (r0 != 0) goto L_0x0047
            X.1Ua r0 = r7.this$0
            X.2Nu r0 = r0.A2P
            int r1 = r0.A00
            r0 = 2
            r2 = 1056964608(0x3f000000, float:0.5)
            if (r1 != r0) goto L_0x0049
        L_0x0047:
            r2 = 1065353216(0x3f800000, float:1.0)
        L_0x0049:
            X.1Ua r0 = r7.this$0
            android.media.SoundPool r0 = r0.A0I
            int r1 = r3.intValue()
            r4 = 0
            r6 = 1065353216(0x3f800000, float:1.0)
            r5 = 0
            r3 = r2
            r0.play(r1, r2, r3, r4, r5, r6)
            return
        L_0x005a:
            java.lang.String r0 = "VoiceService:playCallTone sound pool has not been loaded successfully"
            com.whatsapp.util.Log.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback.playCallTone(int):void");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rejectedDecryptionFailure(String str, String str2, byte[] bArr, int i) {
        DeviceJid nullable = DeviceJid.getNullable(str);
        AnonymousClass009.A05(nullable);
        StringBuilder sb = new StringBuilder("VoiceService EVENT:rejectedDecryptionFailure, Jid:");
        sb.append(nullable);
        sb.append(", callId:");
        sb.append(str2);
        sb.append(", retryCount:");
        sb.append(i);
        Log.i(sb.toString());
        this.this$0.A2Z.put(nullable, str2);
        if (!this.this$0.A2K.A01(nullable, AnonymousClass1SF.A0B(str2), bArr, i)) {
            this.this$0.A0Y(29, null);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void relayBindsFailed(boolean z) {
        String str;
        int i;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:relayBindsFailed self bad asn=");
        sb.append(z);
        Log.i(sb.toString());
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo == null) {
            Log.e("we are not in a active call");
            return;
        }
        C29631Ua r6 = this.this$0;
        r6.A18 = z;
        int A05 = r6.A1m.A05(true);
        if (A05 == 0) {
            str = r6.A1P.getString(R.string.voip_call_failed_no_network);
        } else {
            if (callInfo.isCaller || callInfo.callState == Voip.CallState.ACCEPT_SENT) {
                boolean z2 = r6.A18;
                Context context = r6.A1P;
                if (z2) {
                    i = R.string.voip_not_connected_cellular;
                    if (A05 == 1) {
                        i = R.string.voip_not_connected_wifi;
                    }
                } else {
                    i = R.string.voip_call_failed_incompatible_cellular;
                    if (A05 == 1) {
                        i = R.string.voip_call_failed_incompatible_wifi;
                    }
                }
                String string = context.getString(i);
                UserJid peerJid = callInfo.getPeerJid();
                AnonymousClass009.A05(peerJid);
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(peerJid);
                r6.A1V.A0H(new RunnableBRunnable0Shape0S1201000_I0(r6, arrayList, string, 5, 0));
            }
            str = null;
        }
        r6.A0Y(3, str);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void relayCreateSuccess() {
        Log.i("VoiceService EVENT:relayCreateSuccess");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void relayElectionSendFailed() {
        Log.i("VoiceService EVENT:relayElectionSendFailed");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void relayLatencySendFailed() {
        Log.i("VoiceService EVENT:relayLatencySendFailed");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void restartCamera() {
        AnonymousClass1L4 r3;
        Log.i("VoiceService EVENT:restartCamera");
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo != null && (r3 = this.this$0.A0c) != null) {
            AnonymousClass1S6 r2 = callInfo.self;
            VoipActivityV2 voipActivityV2 = (VoipActivityV2) r3;
            Log.i("VoipActivityV2/restartCameraPreview ");
            voipActivityV2.A0E.removeMessages(12);
            voipActivityV2.A33();
            voipActivityV2.A3H(voipActivityV2.A10, r2);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rtcpByeReceived() {
        Log.i("VoiceService EVENT:rtcpByeReceived");
        this.this$0.A0Y(18, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rxTimeout() {
        Log.i("VoiceService EVENT:rxTimeout");
        this.this$0.A0Y(5, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rxTrafficStarted() {
        Log.i("VoiceService EVENT:rxTrafficStarted");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rxTrafficStateForPeerChanged() {
        Log.i("VoiceService EVENT:rxTrafficStateForPeerChanged");
        this.this$0.A0L.removeMessages(33);
        this.this$0.A0L.sendEmptyMessage(33);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void rxTrafficStopped() {
        boolean z = this.this$0.A1R.A00;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:rxTrafficStopped.  powerSavingMode: ");
        sb.append(C29631Ua.A09(this.this$0));
        sb.append(", isAppInForeground: ");
        sb.append(z);
        sb.append(", screenLocked: ");
        sb.append(this.this$0.A1G);
        Log.i(sb.toString());
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void selfVideoStateChanged(int i) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:selfVideoStateChanged ");
        sb.append(i);
        Log.i(sb.toString());
        this.this$0.A0L.removeMessages(11);
        this.this$0.A0L.obtainMessage(11, i, 0).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void sendAcceptFailed() {
        Log.i("VoiceService EVENT:sendAcceptFailed");
        this.this$0.A0N();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void sendJoinableClientPollCriticalEvent(int i) {
        String str;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:sendJoinableClientPollCriticalEvent errorCode:");
        sb.append(i);
        Log.i(sb.toString());
        if (AnonymousClass1SF.A0P(this.this$0.A20)) {
            str = "linked-group-call";
        } else {
            str = "joinable-group-call";
        }
        AbstractC15710nm r3 = this.this$0.A1T;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("/client-poll-nack");
        r3.AaV(sb2.toString(), String.valueOf(i), false);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void sendLinkedGroupCallDowngradedCriticalEvent(boolean z) {
        String str;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:sendLinkedGroupCallDowngradedCriticalEvent isPendingCall:");
        sb.append(z);
        Log.i(sb.toString());
        StringBuilder sb2 = new StringBuilder("linked-group-call/downgrade-");
        if (z) {
            str = "pending-call";
        } else {
            str = "ongoing-call";
        }
        sb2.append(str);
        this.this$0.A1T.AaV(sb2.toString(), null, false);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void sendOfferFailed() {
        Log.i("VoiceService EVENT:sendOfferFailed");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void soundPortCreateFailed() {
        Log.i("VoiceService EVENT:soundPortCreateFailed");
        this.this$0.A0Y(29, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void soundPortCreated(int i) {
        Object A04;
        Object A03;
        StringBuilder sb = new StringBuilder("VoiceService EVENT:soundPortCreated with engine type ");
        sb.append(i);
        Log.i(sb.toString());
        Integer A01 = Voip.A01("aec.builtin");
        C29631Ua r1 = this.this$0;
        if (r1.A0r == null && A01 != null) {
            int intValue = A01.intValue();
            if (intValue == 2) {
                r1.A0r = Voip.A02(r1.A2L.previousAudioSessionId, true);
            } else if (intValue == 3) {
                r1.A0r = Voip.A02(r1.A2L.previousAudioSessionId, false);
            }
        }
        Integer A012 = Voip.A01("agc.builtin");
        C29631Ua r12 = this.this$0;
        if (r12.A0s == null && A012 != null) {
            int intValue2 = A012.intValue();
            if (intValue2 == 2) {
                A03 = Voip.A03(r12.A2L.previousAudioSessionId, true);
            } else if (intValue2 == 3) {
                A03 = Voip.A03(r12.A2L.previousAudioSessionId, false);
            }
            r12.A0s = A03;
        }
        Integer A013 = Voip.A01("ns.builtin");
        C29631Ua r13 = this.this$0;
        if (r13.A0t == null && A013 != null) {
            int intValue3 = A013.intValue();
            if (intValue3 == 2) {
                A04 = Voip.A04(r13.A2L.previousAudioSessionId, true);
            } else if (intValue3 == 3) {
                A04 = Voip.A04(r13.A2L.previousAudioSessionId, false);
            } else {
                return;
            }
            r13.A0t = A04;
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void speakerStatusChanged(UserJid[] userJidArr, int[] iArr) {
        boolean z = false;
        if (userJidArr.length == iArr.length) {
            z = true;
        }
        AnonymousClass009.A0A("Participant jid list and audio level list should be one-to-one mapped", z);
        Message obtainMessage = this.this$0.A0L.obtainMessage(39);
        obtainMessage.getData().putParcelableArray("participant_jids", userJidArr);
        obtainMessage.getData().putIntArray("audio_levels", iArr);
        obtainMessage.sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void syncDevices(SyncDevicesUserInfo[] syncDevicesUserInfoArr) {
        Log.i("VoiceService EVENT:syncDevices");
        ArrayList arrayList = new ArrayList();
        for (SyncDevicesUserInfo syncDevicesUserInfo : syncDevicesUserInfoArr) {
            C18770sz r9 = this.this$0.A1z;
            UserJid[] userJidArr = {syncDevicesUserInfo.jid};
            String str = syncDevicesUserInfo.phash;
            if (!TextUtils.isEmpty(str)) {
                HashSet hashSet = new HashSet();
                hashSet.addAll(r9.A0E(userJidArr[0]));
                if (AnonymousClass1YK.A00(hashSet).equals(str)) {
                }
            }
            arrayList.add(syncDevicesUserInfo.jid);
        }
        if (!arrayList.isEmpty()) {
            this.this$0.A1k.A01((UserJid[]) arrayList.toArray(new UserJid[0]), 4);
        }
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void transportCandSendFailed() {
        Log.i("VoiceService EVENT:transportCandSendFailed");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void txTimeout() {
        Log.i("VoiceService EVENT:txTimeout");
        this.this$0.A0Y(6, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void updateJoinableCallLog(int i, String str, UserJid[] userJidArr) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:updateJoinableCallLog callId:");
        sb.append(str);
        Log.i(sb.toString());
        Message obtainMessage = this.this$0.A0L.obtainMessage(36, i, 0, str);
        if (i == 2) {
            obtainMessage.getData().putParcelableArray("participant_jids", userJidArr);
        }
        obtainMessage.sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void updateVoipSettings(boolean z) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:updateVoipSettings isVideoCall: ");
        sb.append(z);
        Log.i(sb.toString());
        C29631Ua.A06(this.this$0, z);
    }

    private void validateCallState(Voip.CallState callState) {
        boolean z = false;
        if (callState != Voip.CallState.RECEIVED_CALL_WITHOUT_OFFER) {
            z = true;
        }
        AnonymousClass009.A0B("This call state is not supported in Android", z);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoCaptureStarted() {
        Log.i("VoiceService EVENT:videoCaptureStarted");
        this.this$0.A0L.sendEmptyMessage(13);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoCodecMismatch() {
        Log.i("VoiceService EVENT:videoCodecMismatch");
        this.this$0.A1V.A04(R.string.video_call_fallback_to_voice_call);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoCodecStateChanged() {
        Log.i("VoiceService EVENT:videoCodecStateChanged");
        this.this$0.A0L.removeMessages(15);
        this.this$0.A0L.sendEmptyMessage(15);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoDecodeFatalError() {
        Log.i("VoiceService EVENT:videoDecodeFatalError");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoDecodePaused() {
        Log.i("VoiceService EVENT:videoDecodePaused");
        this.this$0.A0L.removeMessages(15);
        this.this$0.A0L.sendEmptyMessage(15);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoDecodeResumed() {
        Log.i("VoiceService EVENT:videoDecodeResumed");
        this.this$0.A0L.removeMessages(15);
        this.this$0.A0L.sendEmptyMessage(15);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoDecodeStarted() {
        Log.i("VoiceService EVENT:videoDecodeStarted");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoEncodeFatalError() {
        Log.i("VoiceService EVENT:videoEncodeFatalError");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoPortCreateFailed() {
        Log.i("VoiceService EVENT:videoPortCreateFailed");
        this.this$0.A0Y(17, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoPortCreated(String str) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:videoPortCreated ");
        sb.append(str);
        Log.i(sb.toString());
        this.this$0.A0L.obtainMessage(7).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoPreviewError() {
        Log.i("VoiceService EVENT:videoPreviewError");
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoPreviewReady() {
        Log.i("VoiceService EVENT:videoPreviewReady");
        this.this$0.A0L.sendEmptyMessage(9);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoRenderFormatChanged(String str) {
        this.this$0.A0L.obtainMessage(10, str).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoRenderStarted(String str) {
        StringBuilder sb = new StringBuilder("VoiceService EVENT:videoRenderStarted ");
        sb.append(str);
        Log.i(sb.toString());
        this.this$0.A0L.obtainMessage(8, str).sendToTarget();
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void videoStreamCreateError() {
        Log.i("VoiceService EVENT:videoStreamCreateError");
        this.this$0.A0Y(16, null);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void weakWifiSwitchedToCellular() {
        Log.i("VoiceService EVENT:weakWifiSwitchedToCellular");
        this.this$0.A0L.sendEmptyMessage(35);
    }

    @Override // com.whatsapp.voipcalling.VoipEventCallback
    public void willCreateSoundPort() {
        Log.i("VoiceService EVENT:willCreateSoundPort");
    }
}
