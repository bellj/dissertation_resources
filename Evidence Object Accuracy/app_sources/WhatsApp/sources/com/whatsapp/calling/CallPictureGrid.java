package com.whatsapp.calling;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1J1;
import X.AnonymousClass1L6;
import X.AnonymousClass1SF;
import X.AnonymousClass28F;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass5RL;
import X.C14850m9;
import X.C15450nH;
import X.C15550nR;
import X.C37251lt;
import X.C72693f1;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.whatsapp.voipcalling.CallInfo;
import java.util.List;

/* loaded from: classes2.dex */
public class CallPictureGrid extends RecyclerView implements AnonymousClass004 {
    public C15450nH A00;
    public C37251lt A01;
    public AnonymousClass5RL A02;
    public C15550nR A03;
    public AnonymousClass28F A04;
    public AnonymousClass018 A05;
    public C14850m9 A06;
    public AnonymousClass2P7 A07;
    public boolean A08;

    public CallPictureGrid(Context context) {
        this(context, null);
    }

    public CallPictureGrid(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CallPictureGrid(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A06 = (C14850m9) r1.A04.get();
            this.A00 = (C15450nH) r1.AII.get();
            this.A03 = (C15550nR) r1.A45.get();
            this.A05 = (AnonymousClass018) r1.ANb.get();
        }
        this.A01 = new C37251lt(this, this.A03, this.A05, getHeight());
        setLayoutManager(new NonScrollingGridLayoutManager());
        setAdapter(this.A01);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i4 != i2) {
            C37251lt r0 = this.A01;
            r0.A00 = i2;
            r0.A02();
        }
    }

    public void setCallInfo(CallInfo callInfo) {
        this.A01.A02 = callInfo;
    }

    public void setCancelListener(AnonymousClass5RL r1) {
        this.A02 = r1;
    }

    public void setContacts(List list) {
        if (AnonymousClass1SF.A0P(this.A06) && this.A01.A08.isEmpty() && !list.isEmpty()) {
            setAlpha(0.0f);
            animate().alpha(1.0f).setDuration(400).setInterpolator(new AccelerateDecelerateInterpolator()).setStartDelay(200).setListener(new C72693f1(this)).start();
        }
        C37251lt r3 = this.A01;
        List list2 = r3.A08;
        list2.clear();
        if (list.size() > 8) {
            list = list.subList(0, 8);
        }
        list2.addAll(list);
        r3.A02();
    }

    public void setParticipantStatusStringProvider(AnonymousClass1L6 r2) {
        this.A01.A03 = r2;
    }

    public void setPhotoDisplayer(AnonymousClass28F r1) {
        this.A04 = r1;
    }

    public void setPhotoLoader(AnonymousClass1J1 r2) {
        this.A01.A01 = r2;
    }

    /* loaded from: classes3.dex */
    public class NonScrollingGridLayoutManager extends StaggeredGridLayoutManager {
        @Override // androidx.recyclerview.widget.StaggeredGridLayoutManager, X.AnonymousClass02H
        public boolean A14() {
            return false;
        }

        @Override // androidx.recyclerview.widget.StaggeredGridLayoutManager, X.AnonymousClass02H
        public boolean A15() {
            return false;
        }

        public NonScrollingGridLayoutManager() {
            super(2);
        }
    }
}
