package com.whatsapp.calling.callhistory;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass116;
import X.AnonymousClass12F;
import X.AnonymousClass12G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass19Z;
import X.AnonymousClass1AR;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C17140qK;
import X.C18750sx;
import X.C20710wC;
import X.C21250x7;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C22330yu;
import X.C236812p;
import X.C237512w;
import X.C244215l;
import X.C253018w;
import X.C48572Gu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaListFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_CallsHistoryFragment extends WaListFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    private void A02() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A02();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A02()
            r3.A19()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callhistory.Hilt_CallsHistoryFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A02();
        A19();
    }

    public void A19() {
        if (!this.A02) {
            this.A02 = true;
            CallsHistoryFragment callsHistoryFragment = (CallsHistoryFragment) this;
            C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) generatedComponent());
            AnonymousClass01J r2 = r3.A0Y;
            ((WaListFragment) callsHistoryFragment).A00 = (AnonymousClass182) r2.A94.get();
            ((WaListFragment) callsHistoryFragment).A01 = (AnonymousClass180) r2.ALt.get();
            callsHistoryFragment.A0J = (C14830m7) r2.ALb.get();
            callsHistoryFragment.A0P = (C14850m9) r2.A04.get();
            callsHistoryFragment.A03 = (C14900mE) r2.A8X.get();
            callsHistoryFragment.A0U = (C253018w) r2.AJS.get();
            callsHistoryFragment.A04 = (C15570nT) r2.AAr.get();
            callsHistoryFragment.A0W = (AbstractC14440lR) r2.ANe.get();
            callsHistoryFragment.A0V = (AnonymousClass12U) r2.AJd.get();
            callsHistoryFragment.A05 = (C15450nH) r2.AII.get();
            callsHistoryFragment.A0O = (C21250x7) r2.AJh.get();
            callsHistoryFragment.A06 = (AnonymousClass1AR) r2.ALM.get();
            callsHistoryFragment.A0Z = (AnonymousClass19Z) r2.A2o.get();
            callsHistoryFragment.A02 = (AnonymousClass12P) r2.A0H.get();
            callsHistoryFragment.A0G = (C21270x9) r2.A4A.get();
            callsHistoryFragment.A0S = (AnonymousClass17R) r2.AIz.get();
            callsHistoryFragment.A0b = (C21280xA) r2.AMU.get();
            callsHistoryFragment.A0B = (C15550nR) r2.A45.get();
            callsHistoryFragment.A0I = (AnonymousClass01d) r2.ALI.get();
            callsHistoryFragment.A0D = (C15610nY) r2.AMe.get();
            callsHistoryFragment.A0K = (AnonymousClass018) r2.ANb.get();
            callsHistoryFragment.A0X = (AnonymousClass12G) r2.A2h.get();
            callsHistoryFragment.A0C = (AnonymousClass10S) r2.A46.get();
            callsHistoryFragment.A0Q = (C20710wC) r2.A8m.get();
            callsHistoryFragment.A0T = (AnonymousClass12F) r2.AJM.get();
            callsHistoryFragment.A0H = r3.A0V.A05();
            callsHistoryFragment.A0L = (C18750sx) r2.A2p.get();
            callsHistoryFragment.A0A = (AnonymousClass116) r2.A3z.get();
            callsHistoryFragment.A0N = (C236812p) r2.AAC.get();
            callsHistoryFragment.A0c = (C17140qK) r2.AMZ.get();
            callsHistoryFragment.A0M = (C15600nX) r2.A8x.get();
            callsHistoryFragment.A0R = (C244215l) r2.A8y.get();
            callsHistoryFragment.A0a = (C237512w) r2.AAD.get();
            callsHistoryFragment.A09 = (C22330yu) r2.A3I.get();
            callsHistoryFragment.A0Y = (C21260x8) r2.A2k.get();
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
