package com.whatsapp.calling.callhistory.group;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass0B5;
import X.AnonymousClass19Z;
import X.AnonymousClass23N;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.C103734rE;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C48232Fc;
import X.C56712lX;
import X.C73103fg;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101484nb;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import androidx.appcompat.widget.SearchView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class GroupCallParticipantPickerSheet extends GroupCallParticipantPicker {
    public float A00;
    public float A01;
    public ColorDrawable A02;
    public View A03;
    public View A04;
    public View A05;
    public ViewTreeObserver.OnGlobalLayoutListener A06;
    public SearchView A07;
    public BottomSheetBehavior A08;
    public boolean A09;
    public boolean A0A;

    public GroupCallParticipantPickerSheet() {
        this(0);
        this.A06 = new ViewTreeObserver$OnGlobalLayoutListenerC101484nb(this);
    }

    public GroupCallParticipantPickerSheet(int i) {
        this.A09 = false;
        ActivityC13830kP.A1P(this, 35);
    }

    @Override // X.AnonymousClass2xE, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            ((GroupCallParticipantPicker) this).A00 = (AnonymousClass19Z) A1M.A2o.get();
        }
    }

    public final void A3A() {
        this.A07.A0F("");
        AnonymousClass0B5 r1 = (AnonymousClass0B5) this.A03.getLayoutParams();
        r1.A00(this.A08);
        ((ViewGroup.MarginLayoutParams) r1).height = (int) this.A00;
        this.A03.setLayoutParams(r1);
        this.A05.setVisibility(0);
        this.A04.setVisibility(8);
    }

    public final void A3B() {
        int size;
        Point point = new Point();
        C12970iu.A17(this, point);
        Rect A0J = C12980iv.A0J();
        C12970iu.A0G(this).getWindowVisibleDisplayFrame(A0J);
        float f = (float) (point.y - A0J.top);
        this.A01 = f;
        this.A00 = (float) ((int) (f * 0.75f));
        if (!AnonymousClass23N.A05(((ActivityC13810kN) this).A08.A0P())) {
            int i = (int) (this.A01 * 0.55f);
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.group_call_participant_picker_sheet_search_holder_height) + getResources().getDimensionPixelSize(R.dimen.info_screen_card_spacing);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.contact_picker_row_height);
            int i2 = i + ((dimensionPixelSize2 >> 1) - ((i - dimensionPixelSize) % dimensionPixelSize2));
            ArrayList<String> stringArrayListExtra = getIntent().getStringArrayListExtra("jids");
            if (stringArrayListExtra != null && (size = stringArrayListExtra.size()) > 0) {
                i2 = Math.min(i2, dimensionPixelSize + getResources().getDimensionPixelSize(R.dimen.selected_contacts_layout_height_big) + (dimensionPixelSize2 * size));
            }
            this.A08.A0L(i2);
        }
    }

    public final void A3C() {
        AnonymousClass0B5 r1 = (AnonymousClass0B5) this.A03.getLayoutParams();
        r1.A00(null);
        ((ViewGroup.MarginLayoutParams) r1).height = -1;
        this.A03.setLayoutParams(r1);
        this.A07.setIconified(false);
        this.A05.setVisibility(8);
        this.A04.setVisibility(0);
    }

    @Override // com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker, X.AbstractActivityC36611kC, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A04.getVisibility() == 0) {
            A3A();
        } else {
            this.A08.A0M(5);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A3B();
        if (this.A04.getVisibility() != 0) {
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this.A03);
            A0H.height = (int) this.A00;
            this.A03.setLayoutParams(A0H);
        }
        this.A0A = true;
        this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A06);
        this.A03.requestLayout();
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        findViewById(R.id.action_bar).setVisibility(8);
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
        }
        View findViewById = findViewById(R.id.bottom_sheet);
        this.A03 = findViewById;
        this.A08 = BottomSheetBehavior.A00(findViewById);
        this.A03.getViewTreeObserver().addOnGlobalLayoutListener(this.A06);
        BottomSheetBehavior bottomSheetBehavior = this.A08;
        bottomSheetBehavior.A0J = true;
        bottomSheetBehavior.A0M(5);
        A3B();
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this.A03);
        A0H.height = (int) this.A00;
        this.A03.setLayoutParams(A0H);
        ListView A2e = A2e();
        if (i >= 21) {
            A2e.setNestedScrollingEnabled(true);
        }
        View findViewById2 = findViewById(R.id.background);
        AnonymousClass028.A0a(findViewById2, 2);
        PointF pointF = new PointF();
        C12960it.A13(findViewById2, this, pointF, 24);
        findViewById2.setOnTouchListener(new View.OnTouchListener(pointF) { // from class: X.4nL
            public final /* synthetic */ PointF A00;

            {
                this.A00 = r1;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                this.A00.set(motionEvent.getX(), motionEvent.getY());
                return false;
            }
        });
        ColorDrawable colorDrawable = new ColorDrawable();
        this.A02 = colorDrawable;
        findViewById2.setBackground(colorDrawable);
        AlphaAnimation A0J = C12970iu.A0J();
        A0J.setDuration((long) getResources().getInteger(17694720));
        findViewById2.startAnimation(A0J);
        this.A08.A0E = new C56712lX(this);
        this.A05 = findViewById(R.id.title_holder);
        View findViewById3 = findViewById(R.id.search_holder_sheet);
        this.A04 = findViewById3;
        C48232Fc.A00(findViewById3);
        this.A04.setVisibility(8);
        SearchView searchView = (SearchView) this.A04.findViewById(R.id.search_view);
        this.A07 = searchView;
        searchView.setIconifiedByDefault(false);
        this.A07.setQueryHint(getString(R.string.group_call_participant_search_hint));
        C12970iu.A0L(this.A07, R.id.search_mag_icon).setImageDrawable(new C73103fg(AnonymousClass00T.A04(this, R.drawable.ic_back), this));
        this.A07.A0B = new C103734rE(this);
        ImageView A0L = C12970iu.A0L(this.A04, R.id.search_back);
        A0L.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A04(getResources().getDrawable(R.drawable.ic_back), getResources().getColor(R.color.lightActionBarItemDrawableTint)), this.A0S));
        AbstractView$OnClickListenerC34281fs.A00(A0L, this, 35);
        C12960it.A0z(findViewById(R.id.search_btn), this, 44);
        C12970iu.A0M(this, R.id.sheet_title).setText(this.A0S.A0D((long) ActivityC13790kL.A0X(this).size(), R.plurals.group_call_participant_picker_sheet_title));
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13770kJ, android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.getBoolean("search")) {
            A3C();
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("search", C12960it.A1T(this.A04.getVisibility()));
    }
}
