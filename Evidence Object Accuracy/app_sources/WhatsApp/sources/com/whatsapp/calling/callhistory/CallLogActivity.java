package com.whatsapp.calling.callhistory;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC33331dp;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass1MY;
import X.AnonymousClass1YT;
import X.AnonymousClass1YU;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass2TT;
import X.AnonymousClass41Q;
import X.AnonymousClass425;
import X.AnonymousClass44U;
import X.C004802e;
import X.C102194ok;
import X.C102874pq;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C18470sV;
import X.C18640sm;
import X.C18750sx;
import X.C18810t5;
import X.C20710wC;
import X.C20730wE;
import X.C20830wO;
import X.C21820y2;
import X.C21840y4;
import X.C22330yu;
import X.C22670zS;
import X.C238013b;
import X.C244215l;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C254219i;
import X.C27131Gd;
import X.C27531Hw;
import X.C28801Pb;
import X.C36021jC;
import X.C48032Ea;
import X.C52802bi;
import X.C65293Iy;
import X.C863747a;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.calling.callhistory.CallLogActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape2S0110000_I0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

/* loaded from: classes2.dex */
public class CallLogActivity extends ActivityC13790kL {
    public View A00;
    public ImageButton A01;
    public ImageButton A02;
    public ImageView A03;
    public ListView A04;
    public TextView A05;
    public C28801Pb A06;
    public C238013b A07;
    public C48032Ea A08;
    public C22330yu A09;
    public AnonymousClass130 A0A;
    public C15550nR A0B;
    public AnonymousClass10S A0C;
    public C15610nY A0D;
    public AnonymousClass131 A0E;
    public C20730wE A0F;
    public C15890o4 A0G;
    public C18750sx A0H;
    public C20830wO A0I;
    public C15600nX A0J;
    public C15370n3 A0K;
    public C16120oU A0L;
    public C20710wC A0M;
    public C244215l A0N;
    public AbstractC14640lm A0O;
    public AnonymousClass12F A0P;
    public AnonymousClass198 A0Q;
    public C254219i A0R;
    public AnonymousClass19Z A0S;
    public ArrayList A0T;
    public boolean A0U;
    public final AnonymousClass2Dn A0V;
    public final C27131Gd A0W;
    public final AbstractC33331dp A0X;

    public CallLogActivity() {
        this(0);
        this.A0W = new AnonymousClass425(this);
        this.A0V = new AnonymousClass41Q(this);
        this.A0X = new AnonymousClass44U(this);
    }

    public CallLogActivity(int i) {
        this.A0U = false;
        A0R(new C102874pq(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0U) {
            this.A0U = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0L = (C16120oU) r1.ANE.get();
            this.A0S = (AnonymousClass19Z) r1.A2o.get();
            this.A0A = (AnonymousClass130) r1.A41.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A07 = (C238013b) r1.A1Z.get();
            this.A0C = (AnonymousClass10S) r1.A46.get();
            this.A0M = (C20710wC) r1.A8m.get();
            this.A0P = (AnonymousClass12F) r1.AJM.get();
            this.A0Q = (AnonymousClass198) r1.A0J.get();
            this.A0H = (C18750sx) r1.A2p.get();
            this.A0R = (C254219i) r1.A0K.get();
            this.A09 = (C22330yu) r1.A3I.get();
            this.A0F = (C20730wE) r1.A4J.get();
            this.A0G = (C15890o4) r1.AN1.get();
            this.A0J = (C15600nX) r1.A8x.get();
            this.A0E = (AnonymousClass131) r1.A49.get();
            this.A0I = (C20830wO) r1.A4W.get();
            this.A0N = (C244215l) r1.A8y.get();
        }
    }

    public final void A2e() {
        Log.i("calllog/update");
        C15370n3 A01 = this.A0I.A01(this.A0O);
        this.A0K = A01;
        this.A0A.A06(this.A03, A01);
        this.A06.A06(this.A0K);
        String str = this.A0K.A0R;
        if (str == null || str.isEmpty()) {
            this.A05.setVisibility(8);
        } else {
            this.A05.setVisibility(0);
            this.A05.setText(this.A0K.A0R);
        }
        C48032Ea r0 = this.A08;
        if (r0 != null) {
            r0.A03(true);
        }
        C48032Ea r2 = new C48032Ea(this, this);
        this.A08 = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
        boolean z = !this.A0M.A0Y(this.A0K);
        C65293Iy.A04(this.A01, z);
        C65293Iy.A04(this.A02, z);
    }

    public final void A2f() {
        int i;
        View childAt = this.A04.getChildAt(0);
        if (childAt == null) {
            return;
        }
        if (this.A04.getWidth() > this.A04.getHeight()) {
            if (this.A04.getFirstVisiblePosition() == 0) {
                i = childAt.getTop();
            } else {
                i = (-this.A00.getHeight()) + 1;
            }
            View view = this.A00;
            view.offsetTopAndBottom(i - view.getTop());
        } else if (this.A00.getTop() != 0) {
            View view2 = this.A00;
            view2.offsetTopAndBottom(-view2.getTop());
        }
    }

    public final void A2g(boolean z) {
        Jid A0B = this.A0K.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        Intent A00 = this.A0R.A00(this.A0K, (AbstractC14640lm) A0B, z);
        int i = 11;
        if (z) {
            i = 10;
        }
        try {
            startActivityForResult(A00, i);
            this.A0Q.A02(z, 1);
        } catch (ActivityNotFoundException | SecurityException e) {
            Log.w("calllog/opt system contact list could not found", e);
            C36021jC.A01(this, 2);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 11 || i == 10) {
            if (i2 == -1) {
                this.A0F.A06();
            }
            this.A0Q.A00();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String formatDateTime;
        AnonymousClass018 r2;
        Locale A00;
        int i;
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        setTitle(R.string.call_details);
        setContentView(R.layout.contact_call_log_v2);
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        this.A0O = A01;
        this.A04 = (ListView) findViewById(16908298);
        View inflate = getLayoutInflater().inflate(R.layout.contact_call_log_header_v2, (ViewGroup) this.A04, false);
        AnonymousClass028.A0a(inflate, 2);
        this.A04.addHeaderView(inflate, null, false);
        View findViewById = findViewById(R.id.header);
        this.A00 = findViewById;
        findViewById.setClickable(true);
        findViewById(R.id.contact_info_container).setFocusable(true);
        C28801Pb r0 = new C28801Pb(this, (TextEmojiLabel) findViewById(R.id.conversation_contact_name), this.A0D, this.A0P);
        this.A06 = r0;
        C27531Hw.A06(r0.A01);
        this.A05 = (TextView) findViewById(R.id.conversation_contact_status);
        View findViewById2 = findViewById(R.id.divider);
        AnonymousClass018 r3 = ((ActivityC13830kP) this).A01;
        AnonymousClass009.A05(this);
        findViewById2.setBackground(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.list_header_divider), r3));
        this.A04.setOnScrollListener(new C102194ok(this));
        this.A04.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4na
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                CallLogActivity.this.A2f();
            }
        });
        this.A03 = (ImageView) findViewById(R.id.photo_btn);
        StringBuilder sb = new StringBuilder();
        sb.append(new AnonymousClass2TT(this).A00(R.string.transition_photo));
        sb.append("-avatar");
        String obj = sb.toString();
        AnonymousClass028.A0k(this.A03, obj);
        this.A03.setOnClickListener(new C863747a(this, ((ActivityC13810kN) this).A0C, this.A0O, 6, obj));
        this.A01 = (ImageButton) AnonymousClass00T.A05(this, R.id.call_btn);
        this.A02 = (ImageButton) AnonymousClass00T.A05(this, R.id.video_call_btn);
        this.A01.setOnClickListener(new ViewOnClickCListenerShape2S0110000_I0(this, 0, false));
        this.A02.setOnClickListener(new ViewOnClickCListenerShape2S0110000_I0(this, 0, true));
        C52802bi r8 = new C52802bi(this);
        this.A04.setAdapter((ListAdapter) r8);
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("calls");
        if (parcelableArrayListExtra != null) {
            this.A0T = new ArrayList();
            Iterator it = parcelableArrayListExtra.iterator();
            while (it.hasNext()) {
                AnonymousClass1YU r02 = (AnonymousClass1YU) ((Parcelable) it.next());
                C18750sx r6 = this.A0H;
                UserJid userJid = r02.A01;
                boolean z = r02.A03;
                AnonymousClass1YT A04 = r6.A04(new AnonymousClass1YU(r02.A00, userJid, r02.A02, z));
                if (A04 != null) {
                    this.A0T.add(A04);
                }
            }
            r8.A00 = this.A0T;
            r8.notifyDataSetChanged();
            ArrayList arrayList = this.A0T;
            if (!arrayList.isEmpty()) {
                long A02 = ((ActivityC13790kL) this).A05.A02(((AnonymousClass1YT) arrayList.get(0)).A09);
                TextView textView = (TextView) findViewById(R.id.calls_title);
                if (DateUtils.isToday(A02)) {
                    r2 = ((ActivityC13830kP) this).A01;
                    A00 = AnonymousClass018.A00(r2.A00);
                    i = 270;
                } else if (DateUtils.isToday(86400000 + A02)) {
                    r2 = ((ActivityC13830kP) this).A01;
                    A00 = AnonymousClass018.A00(r2.A00);
                    i = 294;
                } else {
                    formatDateTime = DateUtils.formatDateTime(this, A02, 16);
                    textView.setText(formatDateTime);
                }
                formatDateTime = AnonymousClass1MY.A05(A00, r2.A08(i));
                textView.setText(formatDateTime);
            } else {
                finish();
            }
        }
        A2e();
        this.A0C.A03(this.A0W);
        this.A09.A03(this.A0V);
        this.A0N.A03(this.A0X);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        if (i == 1) {
            Log.i("calllog/dialog-add-contact");
            r2 = new C004802e(this);
            r2.A06(R.string.add_contact_as_new_or_existing);
            r2.setPositiveButton(R.string.new_contact, new DialogInterface.OnClickListener() { // from class: X.4fR
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    CallLogActivity callLogActivity = CallLogActivity.this;
                    C36021jC.A00(callLogActivity, 1);
                    callLogActivity.A2g(true);
                }
            });
            r2.A00(R.string.existing_contact, new DialogInterface.OnClickListener() { // from class: X.4fS
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    CallLogActivity callLogActivity = CallLogActivity.this;
                    C36021jC.A00(callLogActivity, 1);
                    callLogActivity.A2g(false);
                }
            });
        } else if (i != 2) {
            return super.onCreateDialog(i);
        } else {
            Log.w("calllog/add to contacts: activity not found, probably tablet");
            r2 = new C004802e(this);
            r2.A06(R.string.activity_not_found);
            r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fQ
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(CallLogActivity.this, 2);
                }
            });
        }
        return r2.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_new_conversation, 0, R.string.menuitem_new).setIcon(R.drawable.ic_action_message).setAlphabeticShortcut('n').setShowAsAction(2);
        menu.add(0, R.id.menuitem_clear_call_log, 0, R.string.clear_single_log).setIcon(R.drawable.ic_action_delete);
        if (this.A0O instanceof GroupJid) {
            return true;
        }
        if (!this.A0K.A0I()) {
            ((ActivityC13790kL) this).A01.A08();
            menu.add(0, R.id.menuitem_add_to_contacts, 0, R.string.add_contact);
        }
        menu.add(0, R.id.menuitem_unblock_contact, 0, R.string.unblock);
        menu.add(0, R.id.menuitem_block_contact, 0, R.string.block);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0C.A04(this.A0W);
        this.A09.A04(this.A0V);
        this.A0N.A04(this.A0X);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            if (menuItem.getItemId() == R.id.menuitem_clear_call_log) {
                Log.i("calllog/delete");
                ArrayList arrayList = this.A0T;
                if (arrayList != null) {
                    this.A0H.A0C(arrayList);
                }
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_new_conversation) {
                Log.i("calllog/new_conversation");
                ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, this.A0K));
            } else if (menuItem.getItemId() == R.id.menuitem_add_to_contacts) {
                C36021jC.A01(this, 1);
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_unblock_contact) {
                this.A07.A0B(this, this.A0K, true);
                return true;
            } else {
                boolean z = false;
                if (menuItem.getItemId() != R.id.menuitem_block_contact) {
                    return false;
                }
                C15370n3 r0 = this.A0K;
                if (r0 != null && r0.A0J()) {
                    z = true;
                }
                UserJid of = UserJid.of(this.A0O);
                AnonymousClass009.A05(of);
                if (z) {
                    startActivity(C14960mK.A0S(this, of, "call_log", true, false, false));
                    return true;
                }
                Adm(BlockConfirmationDialogFragment.A00(of, "call_log", false, true, false));
                return true;
            }
        }
        finish();
        return true;
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean A0I = this.A07.A0I((UserJid) this.A0K.A0B(UserJid.class));
        MenuItem findItem = menu.findItem(R.id.menuitem_unblock_contact);
        if (findItem != null) {
            findItem.setVisible(A0I);
        }
        MenuItem findItem2 = menu.findItem(R.id.menuitem_block_contact);
        if (findItem2 != null) {
            findItem2.setVisible(!A0I);
        }
        return true;
    }
}
