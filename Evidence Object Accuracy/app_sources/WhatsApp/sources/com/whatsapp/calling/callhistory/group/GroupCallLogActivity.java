package com.whatsapp.calling.callhistory.group;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass10S;
import X.AnonymousClass12F;
import X.AnonymousClass19Z;
import X.AnonymousClass1J1;
import X.AnonymousClass1YT;
import X.AnonymousClass1YU;
import X.AnonymousClass1YV;
import X.AnonymousClass1YW;
import X.AnonymousClass28F;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2OC;
import X.C1102754x;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15550nR;
import X.C15610nY;
import X.C18750sx;
import X.C21270x9;
import X.C27131Gd;
import X.C38131nZ;
import X.C44891zj;
import X.C54482gn;
import X.C65293Iy;
import X.C71313cj;
import X.C852841y;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.ui.voip.MultiContactThumbnail;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public class GroupCallLogActivity extends ActivityC13790kL {
    public C54482gn A00;
    public C15550nR A01;
    public AnonymousClass10S A02;
    public C15610nY A03;
    public AnonymousClass1J1 A04;
    public AnonymousClass1J1 A05;
    public C21270x9 A06;
    public C18750sx A07;
    public AnonymousClass12F A08;
    public AnonymousClass1YT A09;
    public AnonymousClass19Z A0A;
    public boolean A0B;
    public final C27131Gd A0C;
    public final AnonymousClass28F A0D;

    public GroupCallLogActivity() {
        this(0);
        this.A0C = new C852841y(this);
        this.A0D = new C1102754x(this);
    }

    public GroupCallLogActivity(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 33);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0A = (AnonymousClass19Z) A1M.A2o.get();
            this.A06 = C12970iu.A0W(A1M);
            this.A03 = C12960it.A0P(A1M);
            this.A01 = C12960it.A0O(A1M);
            this.A02 = C12990iw.A0Z(A1M);
            this.A08 = C12990iw.A0f(A1M);
            this.A07 = (C18750sx) A1M.A2p.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass1YT r0;
        int i;
        int i2;
        String string;
        String str;
        super.onCreate(bundle);
        C12970iu.A0N(this).A0M(true);
        setTitle(R.string.call_details);
        setContentView(R.layout.group_call_info_activity);
        AnonymousClass1YU r02 = (AnonymousClass1YU) getIntent().getParcelableExtra("call_log_key");
        if (r02 != null) {
            C18750sx r6 = this.A07;
            UserJid userJid = r02.A01;
            boolean z = r02.A03;
            r0 = r6.A04(new AnonymousClass1YU(r02.A00, userJid, r02.A02, z));
        } else {
            r0 = null;
        }
        this.A09 = r0;
        if (r0 == null) {
            Log.i("call log missing");
            finish();
            return;
        }
        this.A05 = this.A06.A04(this, "group-call-log-activity");
        this.A04 = this.A06.A05("group-call-log-multi-contact", 0.0f, getResources().getDimensionPixelSize(R.dimen.group_call_log_avatar_size));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.participants_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(1));
        C54482gn r03 = new C54482gn(this);
        this.A00 = r03;
        recyclerView.setAdapter(r03);
        List<AnonymousClass1YV> A04 = this.A09.A04();
        UserJid userJid2 = this.A09.A0B.A01;
        int i3 = 0;
        while (i3 < A04.size() && !((AnonymousClass1YV) A04.get(i3)).A02.equals(userJid2)) {
            i3++;
        }
        if (i3 != 0 && i3 < A04.size()) {
            Object obj = A04.get(i3);
            A04.remove(i3);
            A04.add(0, obj);
        }
        Collections.sort(A04.subList(1 ^ (this.A09.A0B.A03 ? 1 : 0), A04.size()), new C71313cj(((ActivityC13810kN) this).A06, this.A01, this.A03));
        C54482gn r1 = this.A00;
        r1.A00 = C12980iv.A0x(A04);
        r1.A02();
        AnonymousClass1YT r62 = this.A09;
        TextView A0M = C12970iu.A0M(this, R.id.call_type_text);
        ImageView imageView = (ImageView) findViewById(R.id.call_type_icon);
        if (r62.A0F != null) {
            AnonymousClass2OC A02 = C65293Iy.A02(this.A01, this.A03, CallsHistoryFragment.A01(((ActivityC13810kN) this).A06, this.A01, this.A03, r62, C12960it.A0l()), false);
            if (A02 == null) {
                string = null;
            } else {
                string = A02.A00(this);
            }
            i = R.drawable.ic_call_link;
        } else {
            if (r62.A0B.A03) {
                i = R.drawable.ic_call_outgoing;
                i2 = R.string.outgoing_call;
            } else {
                int i4 = r62.A00;
                i = R.drawable.ic_call_missed;
                i2 = R.string.missed_call;
                if (i4 == 5) {
                    i = R.drawable.ic_call_incoming;
                    i2 = R.string.incoming_call;
                }
            }
            string = getString(i2);
        }
        A0M.setText(string);
        imageView.setImageResource(i);
        AnonymousClass2GE.A05(this, imageView, C65293Iy.A00(r62));
        C38131nZ.A0C(C12970iu.A0M(this, R.id.call_duration), ((ActivityC13830kP) this).A01, (long) r62.A01);
        C12970iu.A0M(this, R.id.call_data).setText(C44891zj.A04(((ActivityC13830kP) this).A01, r62.A02));
        C12970iu.A0M(this, R.id.call_date).setText(C38131nZ.A01(((ActivityC13830kP) this).A01, ((ActivityC13790kL) this).A05.A02(r62.A09)));
        ArrayList A0l = C12960it.A0l();
        for (AnonymousClass1YV r04 : A04) {
            A0l.add(this.A01.A0B(r04.A02));
        }
        ((MultiContactThumbnail) findViewById(R.id.multi_contact_photo)).A00(this.A0D, this.A04, A0l);
        if (this.A09.A0F != null) {
            AnonymousClass1YW r2 = this.A09.A0F;
            boolean z2 = this.A09.A0H;
            findViewById(R.id.divider).setVisibility(8);
            findViewById(R.id.call_link_container).setVisibility(0);
            TextView A0M2 = C12970iu.A0M(this, R.id.call_link_text);
            View findViewById = findViewById(R.id.join_btn);
            String str2 = r2.A02;
            Uri.Builder authority = new Uri.Builder().scheme("https").authority("call.whatsapp.com");
            if (z2) {
                str = "video";
            } else {
                str = "voice";
            }
            A0M2.setText(authority.appendPath(str).appendPath(str2).build().toString());
            findViewById.setOnClickListener(new View.OnClickListener(str2, z2) { // from class: X.3lc
                public final /* synthetic */ String A01;
                public final /* synthetic */ boolean A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GroupCallLogActivity groupCallLogActivity = GroupCallLogActivity.this;
                    groupCallLogActivity.A0A.A07(groupCallLogActivity, this.A01, this.A02);
                }
            });
        }
        this.A02.A03(this.A0C);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_clear_call_log, 0, R.string.clear_single_log).setIcon(R.drawable.ic_action_delete);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A02.A04(this.A0C);
        AnonymousClass1J1 r0 = this.A05;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass1J1 r02 = this.A04;
        if (r02 != null) {
            r02.A00();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_clear_call_log) {
            Log.i("calllog/delete");
            this.A07.A0C(Collections.singletonList(this.A09));
        } else if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }
}
