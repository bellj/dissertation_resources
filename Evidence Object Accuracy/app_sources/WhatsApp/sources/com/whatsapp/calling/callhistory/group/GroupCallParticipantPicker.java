package com.whatsapp.calling.callhistory.group;

import X.AbstractActivityC36611kC;
import X.ActivityC13790kL;
import X.AnonymousClass028;
import X.AnonymousClass04v;
import X.AnonymousClass19Z;
import X.AnonymousClass2xE;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class GroupCallParticipantPicker extends AnonymousClass2xE {
    public AnonymousClass19Z A00;

    @Override // X.ActivityC13770kJ
    public void A2f(ListAdapter listAdapter) {
        int intExtra = getIntent().getIntExtra("hidden_jids", 0);
        if (intExtra > 0) {
            ListView A2e = A2e();
            View inflate = getLayoutInflater().inflate(R.layout.group_call_participant_picker_sheet_footer, (ViewGroup) A2e, false);
            A2e.addFooterView(inflate, null, false);
            TextView A0J = C12960it.A0J(inflate, R.id.group_members_not_shown);
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, intExtra, 0);
            A0J.setText(this.A0S.A0I(A1b, R.plurals.group_members_not_shown_message, (long) intExtra));
            AnonymousClass028.A0g(inflate, new AnonymousClass04v());
        }
        super.A2f(listAdapter);
    }

    @Override // X.AbstractActivityC36611kC
    public void A2y(int i) {
        if (i > 0 || A1U() == null) {
            super.A2y(i);
        } else {
            A1U().A09(R.string.add_paticipants);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A35(ArrayList arrayList) {
        List A0X = ActivityC13790kL.A0X(this);
        if (A0X.isEmpty()) {
            super.A35(arrayList);
        } else {
            A39(arrayList, A0X);
        }
    }

    public final void A39(ArrayList arrayList, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((AbstractActivityC36611kC) this).A0J.A0B(C12990iw.A0b(it)));
        }
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        setResult(0);
    }
}
