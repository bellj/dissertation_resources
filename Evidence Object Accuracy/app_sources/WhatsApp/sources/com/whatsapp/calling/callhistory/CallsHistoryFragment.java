package com.whatsapp.calling.callhistory;

import X.AbstractC009504t;
import X.AbstractC14440lR;
import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AbstractC33101dL;
import X.AbstractC33331dp;
import X.AbstractC47972Dm;
import X.AbstractC92184Uw;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02Q;
import X.AnonymousClass10S;
import X.AnonymousClass110;
import X.AnonymousClass116;
import X.AnonymousClass12F;
import X.AnonymousClass12G;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass17R;
import X.AnonymousClass19Z;
import X.AnonymousClass1AR;
import X.AnonymousClass1J1;
import X.AnonymousClass1SF;
import X.AnonymousClass1YT;
import X.AnonymousClass1YU;
import X.AnonymousClass1YV;
import X.AnonymousClass23N;
import X.AnonymousClass2Dn;
import X.AnonymousClass2GE;
import X.AnonymousClass2I2;
import X.AnonymousClass2LB;
import X.AnonymousClass32O;
import X.AnonymousClass36S;
import X.AnonymousClass38F;
import X.AnonymousClass3K6;
import X.AnonymousClass5W1;
import X.C004802e;
import X.C1100854e;
import X.C1100954f;
import X.C111805Au;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C17140qK;
import X.C18750sx;
import X.C20230vQ;
import X.C20710wC;
import X.C21250x7;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C22330yu;
import X.C236712o;
import X.C236812p;
import X.C237512w;
import X.C244215l;
import X.C253018w;
import X.C27131Gd;
import X.C28391Mz;
import X.C36761kV;
import X.C36771kY;
import X.C38121nY;
import X.C42641vY;
import X.C51122Sx;
import X.C52252aV;
import X.C60172w8;
import X.C60222wF;
import X.C60312wd;
import X.C64503Fu;
import X.C66653Ok;
import X.C865847z;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.ListFragment;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.EmptyTellAFriendView;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class CallsHistoryFragment extends Hilt_CallsHistoryFragment implements AbstractC14940mI, AbstractC14770m1, AbstractC33101dL, AnonymousClass110 {
    public MenuItem A00;
    public AbstractC009504t A01;
    public AnonymousClass12P A02;
    public C14900mE A03;
    public C15570nT A04;
    public C15450nH A05;
    public AnonymousClass1AR A06;
    public C36771kY A07;
    public AnonymousClass38F A08;
    public C22330yu A09;
    public AnonymousClass116 A0A;
    public C15550nR A0B;
    public AnonymousClass10S A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public AnonymousClass1J1 A0F;
    public C21270x9 A0G;
    public AnonymousClass2I2 A0H;
    public AnonymousClass01d A0I;
    public C14830m7 A0J;
    public AnonymousClass018 A0K;
    public C18750sx A0L;
    public C15600nX A0M;
    public C236812p A0N;
    public C21250x7 A0O;
    public C14850m9 A0P;
    public C20710wC A0Q;
    public C244215l A0R;
    public AnonymousClass17R A0S;
    public AnonymousClass12F A0T;
    public C253018w A0U;
    public AnonymousClass12U A0V;
    public AbstractC14440lR A0W;
    public AnonymousClass12G A0X;
    public C21260x8 A0Y;
    public AnonymousClass19Z A0Z;
    public C237512w A0a;
    public C21280xA A0b;
    public C17140qK A0c;
    public CharSequence A0d;
    public ArrayList A0e;
    public ArrayList A0f = new ArrayList();
    public LinkedHashMap A0g = new LinkedHashMap();
    public boolean A0h = true;
    public boolean A0i;
    public final AnonymousClass02Q A0j = new C66653Ok(this);
    public final AnonymousClass2Dn A0k = new C60312wd(this);
    public final C27131Gd A0l = new C36761kV(this);
    public final AbstractC33331dp A0m = new AnonymousClass32O(this);
    public final AbstractC47972Dm A0n = new C111805Au(this);
    public final C236712o A0o = new C865847z(this);
    public final Runnable A0p = new RunnableBRunnable0Shape3S0100000_I0_3(this, 24);
    public final HashSet A0q = new HashSet();
    public final Set A0r = new HashSet();

    @Override // X.AbstractC14940mI
    public String AE5() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AGV() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AGW() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AHA() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AHB() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public void AVh() {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void Acn(boolean z) {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void Aco(boolean z) {
    }

    @Override // X.AbstractC14770m1
    public boolean Aed() {
        return true;
    }

    public static List A01(C15450nH r6, C15550nR r7, C15610nY r8, AnonymousClass1YT r9, ArrayList arrayList) {
        List A04 = r9.A04();
        AnonymousClass1YU r5 = r9.A0B;
        UserJid userJid = r5.A01;
        int i = 0;
        while (i < A04.size() && !((AnonymousClass1YV) A04.get(i)).A02.equals(userJid)) {
            i++;
        }
        if (i != 0 && i < A04.size()) {
            Object obj = A04.get(i);
            A04.remove(i);
            A04.add(0, obj);
        }
        int i2 = !r5.A03 ? 1 : 0;
        if (A04.size() > 0) {
            Collections.sort(A04.subList(i2, A04.size()), new C60222wF(r6, r7, r8, arrayList));
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < A04.size(); i3++) {
            arrayList2.add(((AnonymousClass1YV) A04.get(i3)).A02);
        }
        return arrayList2;
    }

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        HashSet hashSet;
        Log.i("voip/CallsFragment/onActivityCreated");
        super.A0m(bundle);
        A0M();
        ListFragment.A00(this);
        ListView listView = ((ListFragment) this).A04;
        listView.setFastScrollEnabled(false);
        listView.setScrollbarFadingEnabled(true);
        listView.setOnItemClickListener(new AnonymousClass36S(this));
        ListFragment.A00(this);
        ((ListFragment) this).A04.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: X.3OG
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                String obj;
                CallsHistoryFragment callsHistoryFragment = CallsHistoryFragment.this;
                if (!(view.getTag() instanceof AbstractC92184Uw)) {
                    return false;
                }
                AbstractC92184Uw r4 = (AbstractC92184Uw) view.getTag();
                if (r4 != null) {
                    AnonymousClass5W1 r3 = r4.A00;
                    if (r3.ADd() == 2 && callsHistoryFragment.A0h) {
                        if (TextUtils.isEmpty(((C1100854e) r3).A00.A03())) {
                            obj = C12960it.A0W(i, "calls/longclick/empty callgroup id/pos ");
                            Log.i(obj);
                            return false;
                        }
                        callsHistoryFragment.A1G(((C1100854e) r4.A00).A00, (C60172w8) r4);
                        return true;
                    }
                }
                StringBuilder A0k = C12960it.A0k("calls/longclick position = ");
                A0k.append(i);
                A0k.append(" holder == null ? ");
                A0k.append(C12980iv.A1X(r4));
                A0k.append(" searching = ");
                A0k.append(!callsHistoryFragment.A0e.isEmpty());
                obj = A0k.toString();
                Log.i(obj);
                return false;
            }
        });
        if (!(bundle == null || (hashSet = (HashSet) bundle.getSerializable("SelectedCallGroupIds")) == null)) {
            HashSet hashSet2 = this.A0q;
            hashSet2.clear();
            hashSet2.addAll(hashSet);
            if (!hashSet2.isEmpty()) {
                this.A01 = ((ActivityC000800j) A0B()).A1W(this.A0j);
            }
        }
        A05().findViewById(R.id.init_calls_progress).setVisibility(0);
        C36771kY r0 = new C36771kY(this);
        this.A07 = r0;
        A18(r0);
        this.A0C.A03(this.A0l);
        this.A0X.A03(this.A0n);
        this.A09.A03(this.A0k);
        this.A0R.A03(this.A0m);
        this.A0Y.A03(this.A0o);
        A1C();
    }

    @Override // com.whatsapp.base.WaListFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        super.A0n(z);
        if (((AnonymousClass01E) this).A03 >= 7 && z) {
            this.A0H.A01(this);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        Log.i("voip/CallsFragment/onPause");
        super.A0r();
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        A1B();
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        boolean z = true;
        if (i != 10) {
            if (i == 150 && i2 == -1) {
                this.A0i = true;
                A1E();
            }
        } else if (i2 == -1) {
            UserJid nullable = UserJid.getNullable(intent.getStringExtra("contact"));
            AnonymousClass009.A05(nullable);
            int intExtra = intent.getIntExtra("call_type", 1);
            if (intExtra == 1 || intExtra == 2) {
                AnonymousClass19Z r3 = this.A0Z;
                C15370n3 A0B = this.A0B.A0B(nullable);
                ActivityC000900k A0C = A0C();
                if (intExtra != 2) {
                    z = false;
                }
                r3.A01(A0C, A0B, 3, z);
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putSerializable("SelectedCallGroupIds", this.A0q);
        bundle.putBoolean("request_sync", this.A0i);
    }

    @Override // X.AnonymousClass01E
    public void A0x(Menu menu) {
        MenuItem findItem = menu.findItem(R.id.menuitem_clear_call_log);
        this.A00 = findItem;
        if (findItem != null) {
            findItem.setVisible(!this.A0g.isEmpty());
        }
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(3, R.id.menuitem_clear_call_log, 0, R.string.clear_call_log);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuitem_new_call) {
            ASK();
        } else if (menuItem.getItemId() != R.id.menuitem_clear_call_log) {
            return false;
        } else {
            if (((AnonymousClass01E) this).A03 >= 7) {
                new ClearCallLogDialogFragment().A1F(super.A0H, null);
                return true;
            }
        }
        return true;
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View inflate = layoutInflater.inflate(R.layout.calls, viewGroup, false);
        ListView listView = (ListView) AnonymousClass028.A0D(inflate, 16908298);
        if (!C28391Mz.A01()) {
            this.A0H.A00(listView, this);
        }
        HomeActivity.A0A(inflate, this);
        if (!this.A0P.A07(1071)) {
            HomeActivity.A0B(inflate, this, A02().getDimensionPixelSize(R.dimen.conversations_row_height));
        }
        return inflate;
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Log.i("voip/CallsFragment/onDestroy");
        super.A11();
        this.A0C.A04(this.A0l);
        this.A0X.A04(this.A0n);
        this.A09.A04(this.A0k);
        this.A0R.A04(this.A0m);
        this.A0Y.A04(this.A0o);
        this.A0F.A00();
        this.A0E.A00();
        this.A03.A0G(this.A0p);
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        Log.i("voip/CallsFragment/onResume");
        super.A13();
        if (this.A0g.isEmpty()) {
            A1D();
        }
        this.A0H.A01(this);
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        Log.i("voip/CallsFragment/onCreate");
        this.A0F = this.A0G.A04(A0p(), "calls-fragment-single");
        this.A0E = this.A0G.A05("calls-fragment-multi", 0.0f, A02().getDimensionPixelSize(R.dimen.small_avatar_size));
        boolean z = false;
        if (bundle != null) {
            z = bundle.getBoolean("request_sync", false);
        }
        this.A0i = z;
        super.A16(bundle);
    }

    @Override // androidx.fragment.app.ListFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        if (C28391Mz.A01()) {
            AnonymousClass2I2 r1 = this.A0H;
            ListFragment.A00(this);
            r1.A00(((ListFragment) this).A04, this);
        }
        if (this.A0P.A07(1071)) {
            HomeActivity.A0B(view, this, A02().getDimensionPixelSize(R.dimen.conversations_row_height));
        }
    }

    public final void A1A() {
        AbstractC92184Uw r2;
        HashSet hashSet = this.A0q;
        if (!hashSet.isEmpty()) {
            this.A0r.clear();
            int i = 0;
            while (true) {
                ListFragment.A00(this);
                if (i < ((ListFragment) this).A04.getChildCount()) {
                    ListFragment.A00(this);
                    View childAt = ((ListFragment) this).A04.getChildAt(i);
                    if (!(childAt == null || (r2 = (AbstractC92184Uw) childAt.getTag()) == null || r2.A00.ADd() != 2)) {
                        C60172w8 r22 = (C60172w8) r2;
                        if (hashSet.contains(((C1100854e) ((AbstractC92184Uw) r22).A00).A00.A03())) {
                            r22.A01.setBackgroundResource(0);
                            r22.A0B.A04(false, true);
                        }
                    }
                    i++;
                } else {
                    hashSet.clear();
                    return;
                }
            }
        }
    }

    public final void A1B() {
        A1F();
        if (A0c() && super.A0A != null) {
            int dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.conversation_list_padding_top);
            LinkedHashMap linkedHashMap = this.A0g;
            if (!linkedHashMap.isEmpty()) {
                ArrayList arrayList = ((C64503Fu) linkedHashMap.values().iterator().next()).A03;
                if (!arrayList.isEmpty() && ((AnonymousClass1YT) arrayList.get(0)).A06 != null) {
                    dimensionPixelSize = A02().getDimensionPixelSize(R.dimen.joinable_calls_list_margin_top);
                }
            }
            ListFragment.A00(this);
            ListView listView = ((ListFragment) this).A04;
            listView.setClipToPadding(false);
            listView.setPadding(listView.getPaddingLeft(), dimensionPixelSize, listView.getPaddingRight(), listView.getPaddingBottom());
        }
    }

    public final void A1C() {
        AnonymousClass38F r1 = this.A08;
        if (r1 != null) {
            r1.A03(true);
        }
        AbstractC009504t r0 = this.A01;
        if (r0 != null) {
            r0.A06();
        }
        AnonymousClass38F r2 = new AnonymousClass38F(this);
        this.A08 = r2;
        this.A0W.Aaz(r2, new Void[0]);
    }

    public final void A1D() {
        int i;
        int i2;
        View view = super.A0A;
        if (view != null) {
            if (this.A0g.isEmpty()) {
                if (this.A08 != null) {
                    view.findViewById(R.id.init_calls_progress).setVisibility(0);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                } else if (this.A0B.A04() > 0) {
                    view.findViewById(R.id.init_calls_progress).setVisibility(8);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                    view.findViewById(R.id.welcome_calls_message).setVisibility(0);
                    view.findViewById(R.id.calls_empty_no_contacts).setVisibility(8);
                    view.findViewById(R.id.contacts_empty_permission_denied).setVisibility(8);
                    TextView textView = (TextView) view.findViewById(R.id.welcome_calls_message);
                    textView.setContentDescription(A0B().getString(R.string.accessible_welcome_calls_message));
                    textView.setText(C52252aV.A00(textView.getPaint(), AnonymousClass2GE.A01(A01(), R.drawable.ic_new_call_tip, R.color.icon_secondary), A0B().getString(R.string.welcome_calls_message)));
                    return;
                } else {
                    if (this.A0A.A00()) {
                        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(view, R.id.calls_empty_no_contacts);
                        if (viewGroup.getChildCount() == 0) {
                            EmptyTellAFriendView emptyTellAFriendView = new EmptyTellAFriendView(A0p());
                            viewGroup.addView(emptyTellAFriendView);
                            emptyTellAFriendView.setInviteButtonClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 47));
                        }
                        viewGroup.setVisibility(0);
                        i2 = R.id.contacts_empty_permission_denied;
                    } else {
                        ViewGroup viewGroup2 = (ViewGroup) view.findViewById(R.id.contacts_empty_permission_denied);
                        if (viewGroup2.getChildCount() == 0) {
                            A04().inflate(R.layout.empty_contacts_permissions_needed, viewGroup2, true);
                            viewGroup2.findViewById(R.id.button_open_permission_settings).setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 26));
                        }
                        viewGroup2.setVisibility(0);
                        i2 = R.id.calls_empty_no_contacts;
                    }
                    view.findViewById(i2).setVisibility(8);
                    view.findViewById(R.id.init_calls_progress).setVisibility(8);
                    view.findViewById(R.id.search_no_matches).setVisibility(8);
                    i = R.id.welcome_calls_message;
                    view.findViewById(i).setVisibility(8);
                }
            } else if (!TextUtils.isEmpty(this.A0d)) {
                view.findViewById(R.id.init_calls_progress).setVisibility(8);
                view.findViewById(R.id.search_no_matches).setVisibility(0);
                ((TextView) view.findViewById(R.id.search_no_matches)).setText(A0B().getString(R.string.search_no_results, this.A0d));
            } else {
                return;
            }
            view.findViewById(R.id.welcome_calls_message).setVisibility(8);
            view.findViewById(R.id.calls_empty_no_contacts).setVisibility(8);
            i = R.id.contacts_empty_permission_denied;
            view.findViewById(i).setVisibility(8);
        }
    }

    public final void A1E() {
        C42641vY r3 = new C42641vY(A0B());
        boolean z = true;
        r3.A04 = true;
        if (this.A0i) {
            this.A04.A08();
        } else {
            z = false;
        }
        r3.A0C = Boolean.valueOf(z);
        startActivityForResult(r3.A00(), 10);
        this.A0i = false;
    }

    public final void A1F() {
        C14900mE r0 = this.A03;
        Runnable runnable = this.A0p;
        r0.A0G(runnable);
        LinkedHashMap linkedHashMap = this.A0g;
        if (!linkedHashMap.isEmpty() && A0B() != null) {
            this.A03.A0J(runnable, (C38121nY.A01(((C64503Fu) this.A0g.get(linkedHashMap.keySet().iterator().next())).A01()) - System.currentTimeMillis()) + 1000);
        }
    }

    public void A1G(C64503Fu r12, C60172w8 r13) {
        String A03 = r12.A03();
        HashSet hashSet = this.A0q;
        if (hashSet.contains(A03)) {
            hashSet.remove(A03);
            if (hashSet.isEmpty() && this.A01 != null) {
                A1A();
                AbstractC009504t r0 = this.A01;
                if (r0 != null) {
                    r0.A05();
                }
            }
            r13.A01.setBackgroundResource(0);
            SelectionCheckView selectionCheckView = r13.A0B;
            selectionCheckView.setVisibility(8);
            selectionCheckView.A04(false, true);
        } else {
            hashSet.add(A03);
            if (this.A01 == null) {
                ActivityC000900k A0B = A0B();
                if (A0B instanceof ActivityC000800j) {
                    this.A01 = ((ActivityC000800j) A0B).A1W(this.A0j);
                }
            }
            r13.A01.setBackgroundResource(R.color.home_row_selection);
            SelectionCheckView selectionCheckView2 = r13.A0B;
            selectionCheckView2.setVisibility(0);
            selectionCheckView2.A04(true, true);
        }
        AbstractC009504t r02 = this.A01;
        if (r02 != null) {
            r02.A06();
        }
        if (!hashSet.isEmpty()) {
            AnonymousClass23N.A00(A0B(), this.A0I, this.A0K.A0I(new Object[]{Integer.valueOf(hashSet.size())}, R.plurals.n_items_selected, (long) hashSet.size()));
        }
    }

    public void A1H(AnonymousClass5W1 r9, AbstractC92184Uw r10) {
        Jid jid;
        int ADd = r9.ADd();
        if (ADd == 2) {
            C64503Fu r5 = ((C1100854e) r9).A00;
            ArrayList arrayList = r5.A03;
            if (arrayList.isEmpty()) {
                AnonymousClass009.A0A("voip/CallsFragment/onListItemClicked empty call group", false);
                return;
            }
            C60172w8 r102 = (C60172w8) r10;
            if (this.A01 != null) {
                A1G(r5, r102);
                return;
            }
            C15370n3 A01 = AnonymousClass1SF.A01(this.A0B, this.A0O, this.A0P, this.A0Q, (AnonymousClass1YT) arrayList.get(0));
            if (!r5.A04() || A01 != null) {
                ArrayList arrayList2 = new ArrayList();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    arrayList2.add(((AnonymousClass1YT) it.next()).A03());
                }
                if (A01 != null) {
                    jid = A01.A0D;
                } else {
                    C15370n3 A02 = r5.A02();
                    AnonymousClass009.A05(A02);
                    jid = A02.A0D;
                }
                Context A0p = A0p();
                Intent intent = new Intent();
                intent.setClassName(A0p.getPackageName(), "com.whatsapp.calling.callhistory.CallLogActivity");
                intent.putExtra("jid", C15380n4.A03(jid));
                intent.putExtra("calls", arrayList2);
                A0v(intent);
                return;
            }
            Context A012 = A01();
            Parcelable A03 = ((AnonymousClass1YT) arrayList.get(0)).A03();
            Intent intent2 = new Intent();
            intent2.setClassName(A012.getPackageName(), "com.whatsapp.calling.callhistory.group.GroupCallLogActivity");
            intent2.putExtra("call_log_key", A03);
            A012.startActivity(intent2);
        } else if (ADd == 1) {
            C51122Sx.A00(new C14960mK().A0i(A0p(), ((C1100954f) r9).A00), this);
        }
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void A5j(AbstractC14780m2 r1) {
        r1.AM3();
    }

    @Override // X.AbstractC14770m1
    public void A68(AnonymousClass2LB r3) {
        this.A0d = r3.A01;
        this.A07.getFilter().filter(this.A0d);
    }

    @Override // X.AbstractC33101dL
    public void A8v() {
        this.A0h = false;
    }

    @Override // X.AbstractC33101dL
    public void A9K() {
        this.A0h = true;
    }

    @Override // X.AbstractC14940mI
    public String AE3() {
        return A0B().getString(R.string.menuitem_new_call);
    }

    @Override // X.AbstractC14940mI
    public Drawable AE4() {
        return AnonymousClass00T.A04(A01(), R.drawable.ic_action_new_call);
    }

    @Override // X.AbstractC14940mI
    public void ASK() {
        if (C21280xA.A00()) {
            Log.w("voip/CallsFragment try to start outgoing call from active voip call");
            this.A03.A07(R.string.error_call_disabled_during_call, 0);
        } else if (this.A0A.A00()) {
            A1E();
        } else {
            RequestPermissionActivity.A0O(this, R.string.permission_contacts_access_on_new_call_request, R.string.permission_contacts_access_on_new_call);
        }
    }

    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AbstractC009504t r0 = this.A01;
        if (r0 != null) {
            r0.A06();
        }
    }

    /* loaded from: classes2.dex */
    public class ClearCallLogDialogFragment extends Hilt_CallsHistoryFragment_ClearCallLogDialogFragment {
        public C14900mE A00;
        public AnonymousClass018 A01;
        public C18750sx A02;
        public C20230vQ A03;
        public AbstractC14440lR A04;
        public AnonymousClass12G A05;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            AnonymousClass3K6 r1 = new AnonymousClass3K6(this);
            C004802e r2 = new C004802e(A0B());
            r2.A06(R.string.clear_call_log_ask);
            r2.setPositiveButton(R.string.ok, r1);
            r2.setNegativeButton(R.string.cancel, null);
            return r2.create();
        }
    }
}
