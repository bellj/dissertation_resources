package com.whatsapp.calling.callgrid.viewmodel;

import X.AnonymousClass1SF;
import X.AnonymousClass2OB;
import X.AnonymousClass2OC;
import X.AnonymousClass2OR;
import X.C12960it;
import X.C12980iv;
import X.C13000ix;
import X.C14820m6;
import X.C14850m9;
import X.C15550nR;
import X.C15610nY;
import X.C27691It;
import X.C48782Ht;
import X.C63493Bu;
import X.C65293Iy;
import android.widget.ImageView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class InCallBannerViewModel extends AnonymousClass2OR {
    public final C48782Ht A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14820m6 A03;
    public final C14850m9 A04;
    public final C27691It A05;
    public final C27691It A06;
    public final C27691It A07;
    public final List A08 = C12960it.A0l();

    public InCallBannerViewModel(C48782Ht r5, C15550nR r6, C15610nY r7, C14820m6 r8, C14850m9 r9) {
        C27691It A03 = C13000ix.A03();
        this.A06 = A03;
        C27691It A032 = C13000ix.A03();
        this.A05 = A032;
        C27691It A033 = C13000ix.A03();
        this.A07 = A033;
        this.A04 = r9;
        this.A01 = r6;
        this.A02 = r7;
        this.A03 = r8;
        A033.A0B(Boolean.FALSE);
        A032.A0B(C12960it.A0l());
        A03.A0B(null);
        this.A00 = r5;
        r5.A03(this);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A00.A04(this);
    }

    public final int A05(boolean z) {
        if (z) {
            return R.color.paletteSurface_dark;
        }
        if (AnonymousClass1SF.A0M(this.A03, this.A04)) {
            return R.color.voip_new_audio_grid_in_call_banner_color;
        }
        return R.color.primary_voip;
    }

    public final C63493Bu A06(C63493Bu r7, C63493Bu r8) {
        int i = r7.A01;
        if (i != r8.A01) {
            return null;
        }
        ArrayList A0x = C12980iv.A0x(r7.A07);
        for (Object obj : r8.A07) {
            if (!A0x.contains(obj)) {
                A0x.add(obj);
            }
        }
        if (i == 3) {
            return A07(A0x, r8.A00);
        }
        if (i == 2) {
            return A08(A0x, r8.A00);
        }
        return null;
    }

    public final C63493Bu A07(List list, int i) {
        AnonymousClass2OC A02 = C65293Iy.A02(this.A01, this.A02, list, true);
        AnonymousClass2OB r9 = new AnonymousClass2OB(new Object[]{A02}, R.plurals.voip_joinable_invited_to_the_call_announcement, list.size());
        AnonymousClass2OB r8 = new AnonymousClass2OB(new Object[0], R.plurals.voip_joinable_invited_to_the_call, list.size());
        boolean A0M = AnonymousClass1SF.A0M(this.A03, this.A04);
        ImageView.ScaleType scaleType = ImageView.ScaleType.FIT_CENTER;
        ArrayList A0l = C12960it.A0l();
        A0l.addAll(list);
        return new C63493Bu(scaleType, null, A02, r8, r9, A0l, 3, i, true, true, A0M, true);
    }

    public final C63493Bu A08(List list, int i) {
        AnonymousClass2OC A02 = C65293Iy.A02(this.A01, this.A02, list, true);
        AnonymousClass2OB r5 = new AnonymousClass2OB(new Object[0], R.plurals.voip_joinable_banner_notif_joined, list.size());
        boolean A0M = AnonymousClass1SF.A0M(this.A03, this.A04);
        ImageView.ScaleType scaleType = ImageView.ScaleType.FIT_CENTER;
        ArrayList A0l = C12960it.A0l();
        A0l.addAll(list);
        return new C63493Bu(scaleType, null, A02, r5, null, A0l, 2, i, true, false, A0M, true);
    }

    public final void A09(C63493Bu r6) {
        List list = this.A08;
        if (list.isEmpty()) {
            list.add(r6);
        } else {
            C63493Bu r1 = (C63493Bu) list.get(0);
            C63493Bu A06 = A06(r1, r6);
            if (A06 != null) {
                list.set(0, A06);
            } else {
                int i = r1.A01;
                int i2 = r6.A01;
                if (i >= i2) {
                    list.set(0, r6);
                } else {
                    for (int i3 = 1; i3 < list.size(); i3++) {
                        if (i2 < ((C63493Bu) list.get(i3)).A01) {
                            list.add(i3, r6);
                            return;
                        }
                        C63493Bu A062 = A06((C63493Bu) list.get(i3), r6);
                        if (A062 != null) {
                            list.set(i3, A062);
                            return;
                        }
                    }
                    list.add(r6);
                    return;
                }
            }
        }
        this.A06.A0B(list.get(0));
    }
}
