package com.whatsapp.calling.callgrid.view;

import X.AbstractC55202hx;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3NI;
import X.AnonymousClass4GV;
import X.AnonymousClass4RV;
import X.AnonymousClass5RM;
import X.AnonymousClass5RN;
import X.C1100554b;
import X.C1100654c;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C28141Kv;
import X.C37261lu;
import X.C60112vx;
import X.C64363Fg;
import X.C91984Tz;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.voipcalling.VoipActivityV2;

/* loaded from: classes2.dex */
public class PipViewContainer extends FrameLayout implements AnonymousClass004 {
    public ValueAnimator A00;
    public Point A01;
    public Pair A02;
    public C37261lu A03;
    public AbstractC55202hx A04;
    public AnonymousClass5RN A05;
    public C91984Tz A06;
    public AnonymousClass018 A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public boolean A0A;
    public final int A0B;
    public final int A0C;
    public final boolean A0D;

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public PipViewContainer(Context context) {
        this(context, null);
    }

    public PipViewContainer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PipViewContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2P6 r1 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            this.A07 = C12960it.A0R(r1.A06);
            this.A03 = (C37261lu) r1.A03.A0A.get();
        }
        this.A0B = context.getResources().getDimensionPixelSize(R.dimen.call_pip_min_margin);
        this.A0C = context.getResources().getDimensionPixelSize(R.dimen.call_pip_picture_in_picture_min_margin);
        this.A0D = C12960it.A1T(AnonymousClass4GV.A00 ? 1 : 0);
        setOnTouchListener(new AnonymousClass3NI(this));
    }

    public static /* synthetic */ void A00(PipViewContainer pipViewContainer, boolean z) {
        AnonymousClass5RM r0;
        AnonymousClass5RN r02 = pipViewContainer.A05;
        if (r02 != null && (r0 = ((C1100654c) r02).A00.A02) != null) {
            VoipActivityV2 voipActivityV2 = ((C1100554b) r0).A00;
            voipActivityV2.A1m = z;
            if (z && voipActivityV2.A1N != null) {
                voipActivityV2.A2z();
            }
        }
    }

    public final AnonymousClass4RV A01(Point point, Point point2, C91984Tz r10) {
        int dimensionPixelSize;
        int i = this.A0B;
        int i2 = r10.A05 + i;
        int i3 = 0;
        if (r10.A03 == 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = getResources().getDimensionPixelSize(r10.A03);
        }
        if (r10.A01 != 0) {
            i3 = getResources().getDimensionPixelSize(r10.A01);
        }
        return new AnonymousClass4RV(i, ((point.x - point2.x) - i) - dimensionPixelSize, i2, (((point.y - point2.y) - i) - r10.A02) - i3);
    }

    public final void A02() {
        float f;
        float f2;
        Point point;
        int i;
        int i2;
        int i3;
        C91984Tz r10 = this.A06;
        if (r10 != null) {
            Point point2 = this.A01;
            if (point2 == null) {
                point = new Point(0, 0);
            } else {
                int i4 = r10.A06;
                int i5 = r10.A04;
                int min = Math.min(i4, i5);
                int max = Math.max(i4, i5);
                int i6 = point2.x;
                int i7 = point2.y;
                int min2 = Math.min(i6, i7);
                int max2 = Math.max(i6, i7);
                int i8 = i6;
                if (i4 < i5) {
                    i8 = i7;
                }
                if (i4 >= i5) {
                    i6 = i7;
                }
                float f3 = (float) max2;
                float f4 = (float) min2;
                int i9 = (f3 > (2.5f * f4) ? 1 : (f3 == (2.5f * f4) ? 0 : -1));
                float f5 = r10.A00;
                if (i9 > 0) {
                    f = f5 * f3;
                    f2 = (float) max;
                } else {
                    f = f5 * f4;
                    f2 = (float) min;
                }
                float f6 = (float) min;
                float f7 = (float) max;
                float min3 = Math.min(Math.min(f / f2, (((float) i6) * 0.5f) / f6), (((float) i8) * 0.5f) / f7);
                int i10 = (int) (f6 * min3);
                int i11 = (int) (f7 * min3);
                if (i4 < i5) {
                    point = new Point(i10, i11);
                } else {
                    point = new Point(i11, i10);
                }
            }
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this);
            int i12 = point.x;
            A0H.width = i12;
            int i13 = point.y;
            A0H.height = i13;
            Point point3 = this.A01;
            boolean z = false;
            if (point3 != null) {
                C91984Tz r1 = this.A06;
                if (r1.A07) {
                    int i14 = this.A0C;
                    i = (point3.x - i12) - i14;
                    i3 = (point3.y - i13) - i14;
                    i2 = 0;
                } else {
                    AnonymousClass4RV A01 = A01(point3, point, r1);
                    C91984Tz r12 = this.A06;
                    if (r12.A09) {
                        i = A01.A00;
                        i2 = A01.A02;
                    } else {
                        i = A01.A02;
                        i2 = A01.A00;
                    }
                    if (r12.A08) {
                        i3 = A01.A01;
                    } else {
                        i3 = A01.A03;
                    }
                }
                if (C28141Kv.A01(this.A07)) {
                    A0H.setMargins(i, i3, i2, 0);
                } else {
                    A0H.setMargins(i2, i3, i, 0);
                }
            }
            setLayoutParams(A0H);
            if (A0H.height < A0H.width) {
                z = true;
            }
            if (z != this.A0A) {
                this.A0A = z;
                AbstractC55202hx r0 = this.A04;
                if (r0 != null) {
                    A04(r0.A05);
                }
            }
        }
    }

    public final void A03() {
        AnonymousClass5RN r1;
        Pair pair = this.A02;
        if (pair != null && (r1 = this.A05) != null) {
            boolean A1Y = C12970iu.A1Y(pair.first);
            boolean A1Y2 = C12970iu.A1Y(pair.second);
            CallGridViewModel callGridViewModel = ((C1100654c) r1).A00.A05;
            AnonymousClass009.A05(callGridViewModel);
            AnonymousClass016 r2 = callGridViewModel.A0B;
            Object A01 = r2.A01();
            AnonymousClass009.A05(A01);
            C91984Tz r12 = (C91984Tz) A01;
            if (!(r12.A09 == A1Y && r12.A08 == A1Y2)) {
                r12.A08 = A1Y2;
                r12.A09 = A1Y;
                r2.A0B(r12);
            }
            this.A02 = null;
        }
    }

    public final void A04(C64363Fg r4) {
        int i;
        removeAllViews();
        C37261lu r2 = this.A03;
        boolean z = this.A0A;
        if (!r4.A0H || r4.A09) {
            i = 7;
            if (z) {
                i = 8;
            }
        } else {
            i = 1;
        }
        AbstractC55202hx r1 = (AbstractC55202hx) r2.A01(this, i);
        this.A04 = r1;
        if (r1 instanceof C60112vx) {
            ((C60112vx) r1).A0H();
        }
        addView(this.A04.A0H, new ViewGroup.LayoutParams(-1, -1));
        this.A04.A0G(r4);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public Rect getGlobalVisibleRect() {
        AbstractC55202hx r2 = this.A04;
        Rect A0J = C12980iv.A0J();
        if (r2 != null && r2.A07()) {
            r2.A0H.getGlobalVisibleRect(A0J);
        }
        return A0J;
    }

    public boolean getIsLandscapeVideo() {
        return this.A0A;
    }

    public AbstractC55202hx getPipViewHolder() {
        return this.A04;
    }

    public void setPipListener(AnonymousClass5RN r1) {
        this.A05 = r1;
    }
}
