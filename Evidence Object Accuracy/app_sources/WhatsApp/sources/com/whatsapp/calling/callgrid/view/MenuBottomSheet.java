package com.whatsapp.calling.callgrid.view;

import X.AnonymousClass028;
import X.C12960it;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.MenuBottomSheetViewModel;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class MenuBottomSheet extends Hilt_MenuBottomSheet {
    public int A00 = 0;
    public LinearLayout A01;
    public MenuBottomSheetViewModel A02;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.call_grid_menu_bottom_sheet);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        LinearLayout linearLayout = (LinearLayout) view;
        this.A01 = linearLayout;
        C12960it.A0z(AnonymousClass028.A0D(linearLayout, R.id.close), this, 42);
        MenuBottomSheetViewModel menuBottomSheetViewModel = this.A02;
        if (menuBottomSheetViewModel != null) {
            C12960it.A19(A0G(), menuBottomSheetViewModel.A02, this, 43);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        MenuBottomSheetViewModel menuBottomSheetViewModel = this.A02;
        if (menuBottomSheetViewModel != null) {
            int i = this.A00;
            UserJid userJid = menuBottomSheetViewModel.A00;
            if (userJid != null) {
                menuBottomSheetViewModel.A06.A0B(C12960it.A0D(userJid, i));
            }
        }
    }
}
