package com.whatsapp.calling.callgrid.view;

import X.AnonymousClass004;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C015007d;
import X.C015607k;
import X.C12960it;
import X.C12980iv;
import X.C13000ix;
import X.C14850m9;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.whatsapp.R;
import java.util.Random;

/* loaded from: classes2.dex */
public class VoiceParticipantAudioWave extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ValueAnimator A06;
    public ValueAnimator A07;
    public Paint A08;
    public Drawable A09;
    public C14850m9 A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;
    public boolean A0D;
    public double[] A0E;
    public double[] A0F;
    public double[] A0G;
    public final Interpolator A0H;
    public final Random A0I;

    public VoiceParticipantAudioWave(Context context) {
        super(context);
        this.A0I = C12960it.A0p(this);
        this.A0H = new LinearInterpolator();
        A03(context);
    }

    public VoiceParticipantAudioWave(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A0I = C12960it.A0p(this);
        this.A0H = new LinearInterpolator();
        A03(context);
    }

    public VoiceParticipantAudioWave(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0I = C12960it.A0p(this);
        this.A0H = new LinearInterpolator();
        A03(context);
    }

    public VoiceParticipantAudioWave(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.A0I = C12960it.A0p(this);
        this.A0H = new LinearInterpolator();
        A03(context);
    }

    public VoiceParticipantAudioWave(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0C) {
            this.A0C = true;
            this.A0A = C12960it.A0S(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A01() {
        int lineCount = getLineCount();
        if (lineCount > 0) {
            this.A0E = new double[lineCount];
            int i = lineCount >> 1;
            double pow = Math.pow(3.0d / ((double) getHeight()), 1.0d / ((double) i));
            this.A0E[i] = (double) getHeight();
            int i2 = 1;
            while (true) {
                int i3 = i - i2;
                if (i3 >= 0) {
                    double[] dArr = this.A0E;
                    dArr[i3] = dArr[i3 + 1] * pow;
                    int i4 = i + i2;
                    if (i4 < lineCount) {
                        dArr[i4] = dArr[i3];
                    }
                    i2++;
                } else {
                    this.A0F = new double[lineCount];
                    this.A0G = new double[lineCount];
                    return;
                }
            }
        }
    }

    public final void A02(float f, boolean z) {
        long j;
        if (getVisibility() == 0) {
            ValueAnimator valueAnimator = this.A06;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            this.A0F = this.A0G;
            this.A0G = new double[this.A0E.length];
            int i = this.A04;
            float f2 = (float) i;
            float max = Math.max(f, f2);
            float f3 = (max - f2) / ((float) (127 - i));
            int i2 = 1;
            while (true) {
                double[] dArr = this.A0E;
                if (i2 >= dArr.length - 1) {
                    break;
                }
                double[] dArr2 = this.A0G;
                double d = dArr[i2];
                Random random = this.A0I;
                if (random.nextFloat() < 0.3f) {
                    d = ((double) ((random.nextFloat() * 0.7f) + 0.3f)) * d;
                }
                dArr2[i2] = d * ((double) f3);
                i2++;
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.A06 = ofFloat;
            if (z) {
                j = (long) this.A02;
            } else {
                j = 0;
            }
            ofFloat.setDuration(j);
            this.A06.setInterpolator(this.A0H);
            C12980iv.A11(this.A06, this, 4);
            this.A06.start();
            this.A01 = max;
        }
    }

    public final void A03(Context context) {
        this.A02 = this.A0A.A02(1106);
        int min = Math.min(this.A0A.A02(1213), 127);
        this.A04 = min;
        if (min >= 127) {
            this.A04 = 0;
        }
        this.A03 = context.getResources().getDimensionPixelSize(R.dimen.voice_call_audio_wave_line_width);
        Paint paint = this.A08;
        paint.setStrokeCap(Paint.Cap.ROUND);
        C12980iv.A12(getContext(), paint, 17170443);
        paint.setStrokeWidth((float) this.A03);
        A01();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    private int getLineCount() {
        if (getWidth() == 0) {
            return 0;
        }
        int width = getWidth();
        int i = this.A03;
        int i2 = (width - i) / (i << 1);
        if (i2 % 2 == 0) {
            i2--;
        }
        this.A05 = (getWidth() - ((i2 << 1) * this.A03)) >> 1;
        return i2;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        Drawable drawable;
        super.onDraw(canvas);
        if (!this.A0D || (drawable = this.A09) == null) {
            i = Integer.MAX_VALUE;
        } else {
            i = (this.A0E.length - 5) >> 1;
            int i2 = this.A03;
            int i3 = i2 * 9;
            int i4 = i3 >> 1;
            int i5 = this.A03;
            drawable.setBounds(this.A05 + i2 + ((i2 << 1) * i), C13000ix.A00(this) - i4, this.A05 + i5 + ((i5 << 1) * i) + i3, C13000ix.A00(this) + i4);
            this.A09.draw(canvas);
        }
        for (int i6 = 0; i6 < this.A0E.length; i6++) {
            if (i6 < i || i6 >= i + 5) {
                int i7 = this.A03;
                int i8 = this.A05 + i7 + ((i7 << 1) * i6);
                double d = this.A0G[i6];
                double d2 = this.A0F[i6];
                float f = (float) i8;
                float f2 = ((float) (((d - d2) * ((double) this.A00)) + d2)) / 2.0f;
                canvas.drawLine(f, ((float) C13000ix.A00(this)) - f2, f, ((float) C13000ix.A00(this)) + f2, this.A08);
            }
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        A01();
        A02(this.A01, true);
    }

    public void setAudioLevel(float f) {
        A02(f, true);
    }

    public void setColor(int i) {
        Paint paint = this.A08;
        paint.setColor(i);
        Drawable drawable = this.A09;
        if (drawable != null) {
            C015607k.A0A(drawable, paint.getColor());
        }
        invalidate();
    }

    public void setMuteIconVisibility(boolean z) {
        if (this.A0D != z) {
            this.A0D = z;
            Drawable drawable = this.A09;
            if (drawable == null) {
                drawable = C015007d.A01(getContext(), R.drawable.ic_voip_voice_mute);
                this.A09 = drawable;
            }
            C015607k.A0A(drawable, this.A08.getColor());
            invalidate();
        }
    }
}
