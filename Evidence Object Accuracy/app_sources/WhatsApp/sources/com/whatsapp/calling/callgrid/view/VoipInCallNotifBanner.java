package com.whatsapp.calling.callgrid.view;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass28F;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C15550nR;
import X.C21270x9;
import X.C27531Hw;
import X.C63493Bu;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner;
import com.whatsapp.calling.callgrid.viewmodel.InCallBannerViewModel;
import com.whatsapp.calling.views.VoipCallControlRingingDotsIndicator;
import com.whatsapp.ui.voip.MultiContactThumbnail;

/* loaded from: classes2.dex */
public class VoipInCallNotifBanner extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public Animator A03;
    public InCallBannerViewModel A04;
    public C63493Bu A05;
    public AnonymousClass130 A06;
    public C15550nR A07;
    public AnonymousClass28F A08;
    public C21270x9 A09;
    public AnonymousClass01d A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;
    public final Handler A0D;
    public final ImageView A0E;
    public final TextEmojiLabel A0F;
    public final TextEmojiLabel A0G;
    public final VoipCallControlRingingDotsIndicator A0H;
    public final AnonymousClass1J1 A0I;
    public final MultiContactThumbnail A0J;

    public VoipInCallNotifBanner(Context context) {
        this(context, null);
    }

    public VoipInCallNotifBanner(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A0D = new Handler(new Handler.Callback() { // from class: X.4iM
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                VoipInCallNotifBanner voipInCallNotifBanner = VoipInCallNotifBanner.this;
                if (message.what != 0) {
                    return true;
                }
                voipInCallNotifBanner.A01();
                return true;
            }
        });
        LayoutInflater.from(context).inflate(R.layout.voip_in_call_banner_notification, (ViewGroup) this, true);
        setVisibility(8);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.title);
        this.A0G = A0T;
        this.A0F = C12970iu.A0T(this, R.id.subtitle);
        this.A0E = C12970iu.A0K(this, R.id.leftAddOn);
        this.A0J = (MultiContactThumbnail) AnonymousClass028.A0D(this, R.id.avatar);
        this.A0H = (VoipCallControlRingingDotsIndicator) AnonymousClass028.A0D(this, R.id.ringing_dots);
        A0T.setTypeface(C27531Hw.A03(context), 0);
        C12960it.A0s(context, A0T, R.color.paletteOnSurface);
        this.A0I = this.A09.A05("voip-in-call-notif-banner-multi", 0.0f, getResources().getDimensionPixelSize(R.dimen.small_list_avatar_size));
        AnonymousClass028.A0a(this, 4);
    }

    public VoipInCallNotifBanner(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A09 = C12970iu.A0W(A00);
            this.A06 = C12990iw.A0Y(A00);
            this.A07 = C12960it.A0O(A00);
            this.A0A = C12960it.A0Q(A00);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner r11, X.C63493Bu r12) {
        /*
        // Method dump skipped, instructions count: 464
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner.A00(com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner, X.3Bu):void");
    }

    public void A01() {
        this.A0D.removeMessages(0);
        if (getVisibility() != 8) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "translationY", 0.0f, getResources().getDimension(R.dimen.call_new_participant_banner_bottom_margin));
            this.A03 = ofFloat;
            ofFloat.setDuration(600L);
            this.A03.setInterpolator(new DecelerateInterpolator(2.0f));
            this.A03.addListener(new IDxLAdapterShape0S0100000_1_I1(this, 1));
            this.A03.start();
        }
        this.A0H.clearAnimation();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    public int getBannerHeight() {
        int i = this.A02;
        if (i != 0) {
            return i;
        }
        int dimension = (((int) getResources().getDimension(R.dimen.contact_picker_row_height)) + (((int) getResources().getDimension(R.dimen.horizontal_padding)) << 1)) - ((int) getResources().getDimension(R.dimen.call_pip_min_margin));
        this.A02 = dimension;
        return dimension;
    }

    private void setupBannerBackground(int i) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(AnonymousClass00T.A00(getContext(), i));
        gradientDrawable.setCornerRadius(getResources().getDimension(R.dimen.call_new_participant_banner_corner_radius));
        setBackground(gradientDrawable);
    }
}
