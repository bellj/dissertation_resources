package com.whatsapp.calling.callgrid.view;

import X.AnonymousClass02H;
import X.C05480Ps;
import X.C48042Ec;
import X.C89384Jt;
import X.C94554c5;
import android.os.Handler;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/* loaded from: classes2.dex */
public class CallGridLayoutManager extends StaggeredGridLayoutManager {
    public int A00 = 0;
    public int A01 = 0;
    public C89384Jt A02;
    public C48042Ec A03;
    public boolean A04 = false;
    public boolean A05;
    public boolean A06;
    public final Handler A07;

    public CallGridLayoutManager(C48042Ec r2) {
        super(1);
        this.A03 = r2;
        this.A07 = new Handler();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0092, code lost:
        if ((r7 % 2) == 0) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c0, code lost:
        if ((r7 % 2) == 0) goto L_0x00c2;
     */
    @Override // androidx.recyclerview.widget.StaggeredGridLayoutManager, X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(X.AnonymousClass0QS r11, X.C05480Ps r12) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.view.CallGridLayoutManager.A0u(X.0QS, X.0Ps):void");
    }

    @Override // androidx.recyclerview.widget.StaggeredGridLayoutManager, X.AnonymousClass02H
    public void A0w(C05480Ps r4) {
        super.A0w(r4);
        this.A01 = ((AnonymousClass02H) this).A00;
        this.A00 = C94554c5.A00(A07(), ((AnonymousClass02H) this).A00, this.A05);
        C89384Jt r0 = this.A02;
        if (r0 != null) {
            CallGrid.A02(r0.A00);
        }
    }
}
