package com.whatsapp.calling.callgrid.viewmodel;

import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass018;
import X.AnonymousClass1S6;
import X.AnonymousClass2OR;
import X.AnonymousClass3CZ;
import X.AnonymousClass4ND;
import X.C14850m9;
import X.C15550nR;
import X.C15600nX;
import X.C27691It;
import X.C36161jQ;
import X.C48782Ht;
import X.C64363Fg;
import X.C89404Jv;
import X.C91984Tz;
import android.graphics.Point;
import android.graphics.Rect;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class CallGridViewModel extends AnonymousClass2OR {
    public int A00;
    public Rect A01;
    public UserJid A02;
    public Map A03;
    public boolean A04;
    public final AnonymousClass016 A05;
    public final AnonymousClass016 A06 = new AnonymousClass016(new AnonymousClass4ND());
    public final AnonymousClass016 A07 = new AnonymousClass016(null);
    public final AnonymousClass016 A08;
    public final AnonymousClass016 A09;
    public final AnonymousClass016 A0A;
    public final AnonymousClass016 A0B = new AnonymousClass016(new C91984Tz());
    public final C48782Ht A0C;
    public final C89404Jv A0D;
    public final AnonymousClass3CZ A0E;
    public final C15550nR A0F;
    public final C15600nX A0G;
    public final C14850m9 A0H;
    public final C36161jQ A0I;
    public final C36161jQ A0J;
    public final C36161jQ A0K;
    public final C27691It A0L;
    public final C27691It A0M;
    public final VoipCameraManager A0N;
    public final HashMap A0O = new HashMap();
    public final LinkedHashMap A0P;

    public CallGridViewModel(C48782Ht r6, C15550nR r7, AnonymousClass018 r8, C15600nX r9, C14850m9 r10, VoipCameraManager voipCameraManager) {
        Boolean bool = Boolean.FALSE;
        this.A0J = new C36161jQ(bool);
        this.A0E = new AnonymousClass3CZ();
        this.A05 = new AnonymousClass016(0L);
        this.A09 = new AnonymousClass016(null);
        C27691It r2 = new C27691It();
        this.A0M = r2;
        this.A0I = new C36161jQ(bool);
        this.A0K = new C36161jQ(new Rect());
        this.A02 = null;
        this.A03 = new HashMap();
        this.A0L = new C27691It();
        C89404Jv r1 = new C89404Jv(this);
        this.A0D = r1;
        this.A0H = r10;
        this.A0F = r7;
        this.A0N = voipCameraManager;
        this.A0G = r9;
        this.A0P = new LinkedHashMap();
        this.A0A = new AnonymousClass016();
        this.A08 = new AnonymousClass016();
        r2.A0B(new ArrayList());
        this.A0C = r6;
        r6.A03(this);
        r6.A0C.add(r1);
        boolean z = !r8.A04().A06;
        AnonymousClass016 r22 = this.A0B;
        Object A01 = r22.A01();
        AnonymousClass009.A05(A01);
        C91984Tz r12 = (C91984Tz) A01;
        r12.A03 = R.dimen.voip_call_grid_margin;
        r12.A01 = R.dimen.voip_call_grid_margin;
        if (r12.A09 != z || !r12.A08) {
            r12.A09 = z;
            r12.A08 = true;
            r22.A0B(r12);
        }
    }

    public static int A00(AnonymousClass1S6 r2) {
        if (r2.A09) {
            return 2;
        }
        if (r2.A0D) {
            return 3;
        }
        int i = r2.A04;
        if (i == 2) {
            return 9;
        }
        if (r2.A0C) {
            return 5;
        }
        if (i == 6) {
            return 7;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:105:0x02ca */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r11v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r11v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x02a0, code lost:
        if (r3 <= ((java.lang.Number) r7.first).intValue()) goto L_0x02a2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02cc  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0173  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A01(java.util.List r40, boolean r41) {
        /*
        // Method dump skipped, instructions count: 740
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel.A01(java.util.List, boolean):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r6.A09 != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.Map A02(X.AnonymousClass3HK r6) {
        /*
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            com.whatsapp.voipcalling.Voip$CallState r1 = r6.A05
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACTIVE
            if (r1 != r0) goto L_0x0010
            boolean r0 = r6.A09
            r4 = 1
            if (r0 == 0) goto L_0x0011
        L_0x0010:
            r4 = 0
        L_0x0011:
            X.0qP r0 = r6.A00
            X.0re r0 = r0.entrySet()
            X.1I5 r3 = r0.iterator()
        L_0x001b:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x004a
            java.lang.Object r2 = r3.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r0 = r2.getValue()
            X.1S6 r0 = (X.AnonymousClass1S6) r0
            boolean r0 = r0.A0F
            if (r0 != 0) goto L_0x003e
            java.lang.Object r0 = r2.getValue()
            X.1S6 r0 = (X.AnonymousClass1S6) r0
            int r1 = r0.A01
            r0 = 1
            if (r1 != r0) goto L_0x001b
            if (r4 == 0) goto L_0x001b
        L_0x003e:
            java.lang.Object r1 = r2.getKey()
            java.lang.Object r0 = r2.getValue()
            r5.put(r1, r0)
            goto L_0x001b
        L_0x004a:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel.A02(X.3HK):java.util.Map");
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C48782Ht r0 = this.A0C;
        r0.A04(this);
        r0.A0C.remove(this.A0D);
    }

    public final Point A05(AnonymousClass1S6 r5) {
        int i;
        int i2;
        int i3;
        if (r5.A0F) {
            VoipCameraManager voipCameraManager = this.A0N;
            Point adjustedCameraPreviewSize = voipCameraManager.getAdjustedCameraPreviewSize();
            if (adjustedCameraPreviewSize == null && r5.A04 == 6) {
                return voipCameraManager.lastAdjustedCameraPreviewSize;
            }
            return adjustedCameraPreviewSize;
        }
        int i4 = 0;
        if (r5.A0G && (i3 = this.A00) >= 0) {
            i4 = i3 * 90;
        }
        if (((((r5.A03 * 90) - i4) + 360) % 360) % 180 != 0) {
            i = r5.A02;
            i2 = r5.A05;
        } else {
            i = r5.A05;
            i2 = r5.A02;
        }
        return new Point(i, i2);
    }

    public final void A06() {
        AnonymousClass016 r1;
        Object arrayList;
        LinkedHashMap linkedHashMap = this.A0P;
        ArrayList arrayList2 = new ArrayList(linkedHashMap.values());
        if (!((Boolean) this.A0J.A01()).booleanValue() || linkedHashMap.size() <= 8) {
            this.A0A.A0B(arrayList2);
            r1 = this.A08;
            arrayList = new ArrayList();
        } else {
            this.A0A.A0B(arrayList2.subList(0, 6));
            r1 = this.A08;
            arrayList = arrayList2.subList(6, arrayList2.size());
        }
        r1.A0B(arrayList);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:304:0x064d */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r10v0 */
    /* JADX WARN: Type inference failed for: r10v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v2, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01d7, code lost:
        if (r10 >= -1) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01e5, code lost:
        if (r2 == 3) goto L_0x01e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x01f1, code lost:
        if (r2 != 2) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01f9, code lost:
        if (r13 != -1) goto L_0x01fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x022e, code lost:
        if (r14 == false) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0327, code lost:
        if (r2 != 5) goto L_0x0329;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0416, code lost:
        if (r0 != null) goto L_0x0422;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0420, code lost:
        if (r0 == null) goto L_0x0425;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0425, code lost:
        if (r1 == null) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x0428, code lost:
        if (r1 == r0) goto L_0x042a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x042a, code lost:
        r0 = r1.getWidth() / 40;
        r8 = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x0432, code lost:
        if (r0 < 8) goto L_0x0439;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x0434, code lost:
        r8 = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x0436, code lost:
        if (r0 > 16) goto L_0x0439;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x0438, code lost:
        r8 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0439, code lost:
        com.whatsapp.filter.FilterUtils.blurNative(r1, r8, 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x046c, code lost:
        if (r2 != 2) goto L_0x046e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x046f, code lost:
        if (r0 != false) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00dd, code lost:
        if (r0.equals(r41.A02) == false) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fc, code lost:
        if (r0.equals(r1.A01()) != false) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0121, code lost:
        if (r2 != 9) goto L_0x046e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01f7  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02bd  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02d0  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x02f3  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x02b6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:300:0x008e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01ab  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07(X.AnonymousClass3HK r42, boolean r43) {
        /*
        // Method dump skipped, instructions count: 1676
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel.A07(X.3HK, boolean):void");
    }

    public final void A08(UserJid userJid) {
        LinkedHashMap linkedHashMap = this.A0P;
        if (linkedHashMap.size() > 1) {
            AnonymousClass016 r1 = this.A07;
            if (r1.A01() == null || userJid.equals(r1.A01())) {
                Object obj = linkedHashMap.get(userJid);
                AnonymousClass009.A05(obj);
                if (((C64363Fg) obj).A0A) {
                    userJid = null;
                }
                r1.A0B(userJid);
                A07(this.A0C.A05(), false);
                return;
            }
            Log.e("voip/CallGridViewModel//toggleFocusedView previous focused participant must be cleared before switching");
        }
    }

    public final void A09(AnonymousClass1S6 r8) {
        int i;
        int i2;
        Point A05;
        AnonymousClass4ND r5 = new AnonymousClass4ND();
        if (!r8.A0F || r8.A04 == 6) {
            i = 5;
            i2 = 7;
        } else {
            i = 9;
            i2 = 16;
        }
        Point point = new Point(i, i2);
        if (!(r8.A04 == 6 || (A05 = A05(r8)) == null)) {
            int i3 = A05.x;
            int i4 = A05.y;
            if (((float) i3) / ((float) i4) > ((float) point.x) / ((float) point.y)) {
                point.x = i3;
                point.y = i4;
            }
        }
        r5.A01 = point.x;
        r5.A00 = point.y;
        this.A06.A0B(r5);
    }

    public final void A0A(AnonymousClass1S6 r5) {
        Point A05 = A05(r5);
        if (A05 != null) {
            AnonymousClass016 r2 = this.A0B;
            Object A01 = r2.A01();
            AnonymousClass009.A05(A01);
            C91984Tz r1 = (C91984Tz) A01;
            r1.A06 = A05.x;
            r1.A04 = A05.y;
            r2.A0B(r1);
        }
    }

    public void A0B(List list) {
        Set set = this.A0C.A0E;
        if (!set.containsAll(list) || set.size() != list.size()) {
            set.clear();
            set.addAll(list);
            Voip.updateParticipantsRxSubscription((UserJid[]) set.toArray(new UserJid[set.size()]));
        }
    }
}
