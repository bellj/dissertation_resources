package com.whatsapp.calling.callgrid.viewmodel;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass01N;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16590pI;
import X.C17140qK;
import X.C29941Vi;
import X.C52372ah;
import X.C97944ht;
import android.app.Activity;
import android.hardware.display.DisplayManager;
import android.os.Build;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class OrientationViewModel extends AnonymousClass015 {
    public DisplayManager.DisplayListener A00;
    public C52372ah A01;
    public boolean A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final AnonymousClass016 A06 = C12980iv.A0T();
    public final C16590pI A07;
    public final AnonymousClass01N A08;

    public OrientationViewModel(C16590pI r5, C17140qK r6, AnonymousClass01N r7) {
        this.A07 = r5;
        this.A08 = r7;
        this.A02 = C12970iu.A1Y(r7.get());
        int i = r6.A01().getInt("portrait_mode_threshold", 30);
        this.A05 = i;
        int i2 = r6.A01().getInt("landscape_mode_threshold", 30);
        this.A04 = i2;
        StringBuilder A0k = C12960it.A0k("OrientationViewModel/ctor portraitModeThreshold = ");
        A0k.append(i);
        A0k.append(" landscapeModeThreshold = ");
        A0k.append(i2);
        C12960it.A1F(A0k);
        if (this.A02 && Build.VERSION.SDK_INT >= 17) {
            A06(A04());
        }
    }

    public static /* synthetic */ void A00(OrientationViewModel orientationViewModel) {
        if (orientationViewModel.A02 && Build.VERSION.SDK_INT >= 17) {
            orientationViewModel.A06(orientationViewModel.A04());
        }
    }

    public static /* synthetic */ void A01(OrientationViewModel orientationViewModel, int i) {
        if (!orientationViewModel.A02 || Build.VERSION.SDK_INT < 17) {
            orientationViewModel.A06(i);
        }
    }

    public final int A04() {
        return (4 - A05().getDisplay(0).getRotation()) % 4;
    }

    public final DisplayManager A05() {
        return (DisplayManager) this.A07.A00.getSystemService("display");
    }

    public final void A06(int i) {
        AnonymousClass016 r2 = this.A06;
        Object A01 = r2.A01();
        Integer valueOf = Integer.valueOf(i);
        if (!C29941Vi.A00(A01, valueOf)) {
            Log.i(C12960it.A0W(i, "voip/OrientationViewModel/setOrientation "));
            r2.A0B(valueOf);
        }
    }

    public void A07(Activity activity) {
        C52372ah r1 = this.A01;
        if (r1 == null) {
            r1 = new C52372ah(activity, this);
            this.A01 = r1;
        }
        if (!this.A03 && r1.canDetectOrientation()) {
            Log.i("voip/OrientationViewModel/enableOrientationListener");
            this.A01.enable();
            this.A03 = true;
        }
        if (Build.VERSION.SDK_INT >= 17 && this.A00 == null) {
            this.A00 = new C97944ht(this);
            A05().registerDisplayListener(this.A00, C12970iu.A0E());
        }
    }

    public boolean A08() {
        boolean z;
        if (!this.A03 || this.A01 == null) {
            z = false;
        } else {
            z = true;
            Log.i("voip/OrientationViewModel/disableOrientationListener");
            this.A01.disable();
            this.A03 = false;
        }
        if (Build.VERSION.SDK_INT >= 17 && this.A00 != null) {
            A05().unregisterDisplayListener(this.A00);
            this.A00 = null;
        }
        return z;
    }
}
