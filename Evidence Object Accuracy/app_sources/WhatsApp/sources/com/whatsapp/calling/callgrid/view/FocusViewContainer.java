package com.whatsapp.calling.callgrid.view;

import X.AbstractC001200n;
import X.AbstractC55202hx;
import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C15610nY;
import X.C37261lu;
import X.C64363Fg;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.calling.callgrid.viewmodel.MenuBottomSheetViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* loaded from: classes2.dex */
public class FocusViewContainer extends ConstraintLayout implements AnonymousClass004 {
    public Rect A00;
    public FrameLayout A01;
    public LinearLayout A02;
    public WaTextView A03;
    public C37261lu A04;
    public AbstractC55202hx A05;
    public MenuBottomSheetViewModel A06;
    public C15610nY A07;
    public AnonymousClass2P7 A08;
    public boolean A09;

    public FocusViewContainer(Context context) {
        this(context, null);
    }

    public FocusViewContainer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FocusViewContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2P6 r1 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            this.A04 = (C37261lu) r1.A03.A0A.get();
            this.A07 = C12960it.A0P(r1.A06);
        }
        LayoutInflater.from(context).inflate(R.layout.call_grid_focus_container, (ViewGroup) this, true);
        this.A03 = C12960it.A0N(this, R.id.participant_name);
        this.A01 = (FrameLayout) AnonymousClass028.A0D(this, R.id.participant_view_container);
        this.A02 = (LinearLayout) AnonymousClass028.A0D(this, R.id.menu_list_layout);
        setOnClickListener(new ViewOnClickCListenerShape17S0100000_I1(this, 33));
        this.A00 = C12980iv.A0J();
    }

    public static /* synthetic */ void A00(FocusViewContainer focusViewContainer) {
        FrameLayout frameLayout = focusViewContainer.A01;
        frameLayout.setScaleX(1.0f);
        frameLayout.setScaleY(1.0f);
        frameLayout.setTranslationX(0.0f);
        frameLayout.setTranslationY(0.0f);
        focusViewContainer.A03.animate().alpha(1.0f);
        focusViewContainer.A02.animate().alpha(1.0f);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public AbstractC55202hx getFocusViewHolder() {
        return this.A05;
    }

    public UserJid getVisiblePeerJid() {
        AbstractC55202hx r1;
        if (getVisibility() != 0 || (r1 = this.A05) == null || !r1.A07()) {
            return null;
        }
        C64363Fg r12 = r1.A05;
        if (!r12.A0G) {
            return r12.A0S;
        }
        return null;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.A00.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
    }

    public void setMenuViewModel(AbstractC001200n r3, MenuBottomSheetViewModel menuBottomSheetViewModel) {
        this.A06 = menuBottomSheetViewModel;
        C12960it.A19(r3, menuBottomSheetViewModel.A01, this, 42);
    }
}
