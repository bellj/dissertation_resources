package com.whatsapp.calling.callgrid.view;

import X.AbstractC001200n;
import X.AbstractC008704k;
import X.AbstractC05270Ox;
import X.AbstractC116665Wi;
import X.AbstractC55202hx;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass10S;
import X.AnonymousClass1J1;
import X.AnonymousClass1O4;
import X.AnonymousClass2FL;
import X.AnonymousClass2JB;
import X.AnonymousClass2JC;
import X.AnonymousClass2JD;
import X.AnonymousClass2JE;
import X.AnonymousClass2JF;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3CK;
import X.AnonymousClass3W9;
import X.AnonymousClass5RM;
import X.C1100654c;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C18720su;
import X.C21270x9;
import X.C37261lu;
import X.C48042Ec;
import X.C60102vv;
import X.C60142w0;
import X.C74993j7;
import X.C75003j8;
import X.C89374Js;
import X.C89384Jt;
import X.C89394Ju;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class CallGrid extends FrameLayout implements AnonymousClass004 {
    public Parcelable A00;
    public C18720su A01;
    public AnonymousClass5RM A02;
    public C37261lu A03;
    public C60102vv A04;
    public CallGridViewModel A05;
    public AnonymousClass10S A06;
    public C21270x9 A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final View A0C;
    public final View A0D;
    public final AnonymousClass054 A0E;
    public final LinearLayoutManager A0F;
    public final AbstractC05270Ox A0G;
    public final AbstractC05270Ox A0H;
    public final RecyclerView A0I;
    public final RecyclerView A0J;
    public final C89374Js A0K;
    public final AnonymousClass3CK A0L;
    public final C48042Ec A0M;
    public final CallGridLayoutManager A0N;
    public final FocusViewContainer A0O;
    public final PipViewContainer A0P;
    public final AbstractC116665Wi A0Q;

    public CallGrid(Context context) {
        this(context, null);
    }

    public CallGrid(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CallGrid(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2P6 r1 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass2FL r2 = r1.A03;
            this.A03 = (C37261lu) r2.A0A.get();
            AnonymousClass01J r12 = r1.A06;
            this.A04 = new C60102vv((AnonymousClass2JB) r2.A0p.get(), (AnonymousClass2JC) r2.A0q.get(), (AnonymousClass2JD) r2.A0r.get(), (AnonymousClass2JE) r2.A0s.get(), (AnonymousClass2JF) r2.A0t.get(), C12960it.A0S(r12));
            this.A07 = C12970iu.A0W(r12);
            this.A06 = C12990iw.A0Z(r12);
            this.A01 = (C18720su) r12.A2c.get();
        }
        this.A0H = new C74993j7(this);
        this.A0G = new C75003j8(this);
        this.A0E = new AnonymousClass054() { // from class: com.whatsapp.calling.callgrid.view.CallGrid$$ExternalSyntheticLambda0
            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r7, AbstractC001200n r8) {
                CallGrid callGrid = CallGrid.this;
                if (r7 == AnonymousClass074.ON_START) {
                    int i2 = C12990iw.A0K(callGrid).widthPixels;
                    C89374Js r5 = callGrid.A0K;
                    C21270x9 r4 = callGrid.A07;
                    AnonymousClass1J1 A05 = r4.A05("call-grid", 0.0f, i2);
                    Map map = r5.A00;
                    map.put(0, A05);
                    map.put(C12960it.A0V(), r4.A04(callGrid.getContext(), "voip-call-control-bottom-sheet"));
                    C37261lu r0 = callGrid.A03;
                    r0.A01 = r5;
                    C60102vv r22 = callGrid.A04;
                    ((C37261lu) r22).A01 = r5;
                    AnonymousClass10S r13 = callGrid.A06;
                    r13.A03(r0.A0B);
                    r13.A03(r22.A0B);
                    callGrid.A0J.A0n(callGrid.A0H);
                    callGrid.A0I.A0n(callGrid.A0G);
                } else if (r7 == AnonymousClass074.ON_STOP) {
                    C89374Js r02 = callGrid.A0K;
                    if (r02 != null) {
                        Map map2 = r02.A00;
                        Iterator A0o = C12960it.A0o(map2);
                        while (A0o.hasNext()) {
                            ((AnonymousClass1J1) A0o.next()).A00();
                        }
                        map2.clear();
                    }
                    C18720su r03 = callGrid.A01;
                    synchronized (r03.A0C) {
                        AnonymousClass1O4 r14 = r03.A02;
                        if (r14 != null) {
                            r14.A02(0);
                        }
                    }
                    AnonymousClass10S r15 = callGrid.A06;
                    r15.A04(callGrid.A03.A0B);
                    r15.A04(callGrid.A04.A0B);
                    callGrid.A0J.A0o(callGrid.A0H);
                    callGrid.A0I.A0o(callGrid.A0G);
                }
            }
        };
        AnonymousClass3W9 r4 = new AnonymousClass3W9(this);
        this.A0Q = r4;
        AnonymousClass3CK r3 = new AnonymousClass3CK(this);
        this.A0L = r3;
        LayoutInflater.from(context).inflate(R.layout.call_grid, (ViewGroup) this, true);
        C37261lu r0 = this.A03;
        r0.A03 = r4;
        r0.A02 = r3;
        C60102vv r02 = this.A04;
        r02.A03 = r4;
        r02.A02 = r3;
        RecyclerView A0R = C12990iw.A0R(this, R.id.call_grid_recycler_view);
        this.A0J = A0R;
        A0R.setAdapter(this.A03);
        RecyclerView A0R2 = C12990iw.A0R(this, R.id.call_grid_h_scroll_recycler_view);
        this.A0I = A0R2;
        A0R2.setAdapter(this.A04);
        this.A0D = AnonymousClass028.A0D(this, R.id.call_grid_top_scrolling_peek_overlay);
        this.A0C = AnonymousClass028.A0D(this, R.id.call_grid_bottom_scrolling_peek_overlay);
        C89384Jt r42 = new C89384Jt(this);
        C48042Ec r32 = new C48042Ec();
        this.A0M = r32;
        r32.A00 = new C89394Ju(this);
        ((AbstractC008704k) r32).A00 = false;
        CallGridLayoutManager callGridLayoutManager = new CallGridLayoutManager(r32);
        this.A0N = callGridLayoutManager;
        callGridLayoutManager.A02 = r42;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(0);
        this.A0F = linearLayoutManager;
        A0R2.setLayoutManager(linearLayoutManager);
        A0R.setLayoutManager(callGridLayoutManager);
        A0R.setItemAnimator(r32);
        this.A0B = false;
        PipViewContainer pipViewContainer = (PipViewContainer) AnonymousClass028.A0D(this, R.id.pip_view_container);
        this.A0P = pipViewContainer;
        pipViewContainer.A05 = new C1100654c(this);
        this.A0O = (FocusViewContainer) AnonymousClass028.A0D(this, R.id.focus_view_container);
        this.A0K = new C89374Js();
    }

    public static /* synthetic */ void A02(CallGrid callGrid) {
        int size = callGrid.A03.A0D.size();
        Log.i(C12960it.A0W(size, "CallGrid/updateGridLayoutMode, nTiles: "));
        for (int i = 0; i < size; i++) {
            AbstractC55202hx r2 = (AbstractC55202hx) callGrid.A0J.A0C(i);
            if (r2 instanceof C60142w0) {
                int i2 = 0;
                if (size > 2) {
                    i2 = 2;
                    if (size <= 8) {
                        i2 = 1;
                    }
                }
                r2.A0A(i2);
            }
        }
        callGrid.A04();
        if (callGrid.A05 != null && callGrid.A0B && callGrid.A04.A0D.size() > 0) {
            callGrid.A05.A0B(callGrid.getVisibleParticipantJids());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        return r0.A0C(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass03U A03(X.C64363Fg r5) {
        /*
            r4 = this;
            X.1lu r2 = r4.A03
            r3 = 0
        L_0x0003:
            java.util.List r1 = r2.A0D
            int r0 = r1.size()
            if (r3 >= r0) goto L_0x0027
            java.lang.Object r0 = r1.get(r3)
            X.3Fg r0 = (X.C64363Fg) r0
            com.whatsapp.jid.UserJid r1 = r5.A0S
            com.whatsapp.jid.UserJid r0 = r0.A0S
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0024
            if (r3 < 0) goto L_0x0027
            androidx.recyclerview.widget.RecyclerView r0 = r4.A0J
        L_0x001f:
            X.03U r0 = r0.A0C(r3)
            return r0
        L_0x0024:
            int r3 = r3 + 1
            goto L_0x0003
        L_0x0027:
            X.2vv r2 = r4.A04
            r3 = 0
        L_0x002a:
            java.util.List r1 = r2.A0D
            int r0 = r1.size()
            if (r3 >= r0) goto L_0x004a
            java.lang.Object r0 = r1.get(r3)
            X.3Fg r0 = (X.C64363Fg) r0
            com.whatsapp.jid.UserJid r1 = r5.A0S
            com.whatsapp.jid.UserJid r0 = r0.A0S
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0047
            if (r3 < 0) goto L_0x004a
            androidx.recyclerview.widget.RecyclerView r0 = r4.A0I
            goto L_0x001f
        L_0x0047:
            int r3 = r3 + 1
            goto L_0x002a
        L_0x004a:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.view.CallGrid.A03(X.3Fg):X.03U");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r4.A0J.canScrollVertically(-1) == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04() {
        /*
            r4 = this;
            android.view.View r2 = r4.A0D
            boolean r0 = r4.A0B
            r3 = 0
            if (r0 != 0) goto L_0x0011
            androidx.recyclerview.widget.RecyclerView r1 = r4.A0J
            r0 = -1
            boolean r1 = r1.canScrollVertically(r0)
            r0 = 0
            if (r1 != 0) goto L_0x0013
        L_0x0011:
            r0 = 8
        L_0x0013:
            r2.setVisibility(r0)
            android.view.View r2 = r4.A0C
            boolean r0 = r4.A0B
            if (r0 != 0) goto L_0x0029
            androidx.recyclerview.widget.RecyclerView r1 = r4.A0J
            r0 = 1
            boolean r0 = r1.canScrollVertically(r0)
            if (r0 == 0) goto L_0x0029
        L_0x0025:
            r2.setVisibility(r3)
            return
        L_0x0029:
            r3 = 8
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.view.CallGrid.A04():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:146:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x017d  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0195  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(java.util.List r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 597
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.callgrid.view.CallGrid.A05(java.util.List, boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public FocusViewContainer getFocusViewContainer() {
        return this.A0O;
    }

    public PipViewContainer getPipViewContainer() {
        return this.A0P;
    }

    /* access modifiers changed from: private */
    public List getVisibleParticipantJids() {
        AnonymousClass009.A0E(this.A0B);
        RecyclerView recyclerView = this.A0J;
        AnonymousClass009.A0E(C12960it.A1W(recyclerView.getLayoutManager()));
        ArrayList A0l = C12960it.A0l();
        FocusViewContainer focusViewContainer = this.A0O;
        if (focusViewContainer.getVisiblePeerJid() != null) {
            A0l.add(focusViewContainer.getVisiblePeerJid());
        }
        for (int i = 0; i <= recyclerView.getLayoutManager().A07(); i++) {
            AbstractC55202hx r1 = (AbstractC55202hx) recyclerView.A0C(i);
            if (r1 != null && r1.A07() && !r1.A05.A0G) {
                A0l.add(r1.A05.A0S);
            }
        }
        LinearLayoutManager linearLayoutManager = this.A0F;
        int A1A = linearLayoutManager.A1A();
        int A1C = linearLayoutManager.A1C();
        for (int i2 = A1A; i2 <= A1C; i2++) {
            AbstractC55202hx r2 = (AbstractC55202hx) this.A0I.A0C(i2);
            if (r2 != null && r2.A07()) {
                if (i2 == A1A || i2 == A1C) {
                    Rect A0J = C12980iv.A0J();
                    View view = r2.A0H;
                    view.getGlobalVisibleRect(A0J);
                    if (A0J.width() < view.getWidth() / 3) {
                    }
                }
                A0l.add(r2.A05.A0S);
            }
        }
        return A0l;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        PipViewContainer pipViewContainer = this.A0P;
        pipViewContainer.A01 = new Point(i, i2);
        if (pipViewContainer.isLayoutRequested()) {
            pipViewContainer.post(new RunnableBRunnable0Shape14S0100000_I1(pipViewContainer, 43));
        } else {
            pipViewContainer.A02();
        }
        int measuredHeight = (int) (0.04d * ((double) getMeasuredHeight()));
        Log.i(C12960it.A0W(measuredHeight, "CallGrid/onSizeChanged, scrolling peek height: "));
        View view = this.A0D;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        View view2 = this.A0C;
        ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
        layoutParams.height = measuredHeight;
        layoutParams2.height = measuredHeight;
        view.setLayoutParams(layoutParams);
        view2.setLayoutParams(layoutParams2);
    }

    public void setCallGridListener(AnonymousClass5RM r1) {
        this.A02 = r1;
    }

    /* access modifiers changed from: private */
    public void setMargins(Rect rect) {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            marginLayoutParams.leftMargin = rect.left;
            marginLayoutParams.topMargin = rect.top;
            marginLayoutParams.bottomMargin = rect.bottom;
            marginLayoutParams.rightMargin = rect.right;
            setLayoutParams(marginLayoutParams);
        }
    }
}
