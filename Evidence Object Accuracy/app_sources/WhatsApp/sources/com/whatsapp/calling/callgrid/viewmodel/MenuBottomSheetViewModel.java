package com.whatsapp.calling.callgrid.viewmodel;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C13000ix;
import X.C14850m9;
import X.C15550nR;
import X.C15610nY;
import X.C27691It;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class MenuBottomSheetViewModel extends AnonymousClass015 {
    public UserJid A00;
    public final AnonymousClass016 A01 = new AnonymousClass016(null);
    public final AnonymousClass016 A02 = new AnonymousClass016(null);
    public final C15550nR A03;
    public final C15610nY A04;
    public final C14850m9 A05;
    public final C27691It A06 = C13000ix.A03();

    public MenuBottomSheetViewModel(C15550nR r3, C15610nY r4, C14850m9 r5) {
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
    }
}
