package com.whatsapp.calling;

import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C15550nR;
import X.C15610nY;
import X.C21260x8;
import X.C236712o;
import X.C252018m;
import X.C27531Hw;
import X.C32721cd;
import X.C865747y;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S1100000_I1;
import com.whatsapp.R;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class VoipNotAllowedActivity extends ActivityC13790kL {
    public C15550nR A00;
    public C15610nY A01;
    public C252018m A02;
    public C21260x8 A03;
    public boolean A04;
    public final C236712o A05;

    public VoipNotAllowedActivity() {
        this(0);
        this.A05 = new C865747y(this);
    }

    public VoipNotAllowedActivity(int i) {
        this.A04 = false;
        ActivityC13830kP.A1P(this, 32);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A03 = (C21260x8) A1M.A2k.get();
            this.A00 = C12960it.A0O(A1M);
            this.A01 = C12960it.A0P(A1M);
            this.A02 = C12980iv.A0g(A1M);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        LinearLayout linearLayout = (LinearLayout) AnonymousClass00T.A05(this, R.id.content);
        int i = 1;
        if (configuration.orientation != 1) {
            i = 0;
        }
        linearLayout.setOrientation(i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String A00;
        int i;
        String A0X;
        AnonymousClass018 r10;
        int i2;
        int i3;
        super.onCreate(bundle);
        setContentView(R.layout.voip_not_allowed);
        getWindow().addFlags(524288);
        TextView A0N = C12990iw.A0N(this, R.id.title);
        C27531Hw.A06(A0N);
        List A0X2 = ActivityC13790kL.A0X(this);
        AnonymousClass009.A0A("Missing jids", !A0X2.isEmpty());
        int intExtra = getIntent().getIntExtra("reason", 0);
        ArrayList A0y = C12980iv.A0y(A0X2);
        if (intExtra == 0 || intExtra == 12 || intExtra == 14) {
            Iterator it = A0X2.iterator();
            while (it.hasNext()) {
                A0y.add(C15610nY.A01(this.A01, this.A00.A0B(C12990iw.A0b(it))));
            }
            A00 = C32721cd.A00(this.A01.A04, A0y, true);
        } else {
            AnonymousClass009.A0A("Incorrect number of arguments", C12960it.A1V(A0X2.size(), 1));
            A00 = C15610nY.A01(this.A01, this.A00.A0B((AbstractC14640lm) A0X2.get(0)));
        }
        TextView A0N2 = C12990iw.A0N(this, R.id.message);
        String str = null;
        switch (intExtra) {
            case 1:
                i = R.string.voip_not_allowed_needs_update;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 2:
                i = R.string.voip_not_allowed_never;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 3:
                A0N2.setText(R.string.voip_not_allowed_caller_country);
                str = C252018m.A00(this.A02, "28030008");
                break;
            case 4:
                A0N2.setText(C12960it.A0X(this, A00, new Object[1], 0, R.string.voip_not_allowed_callee_country));
                str = C252018m.A00(this.A02, "28030008");
                break;
            case 5:
                A0N.setText(R.string.voip_not_connected_title);
                A0X = getIntent().getStringExtra("message");
                A0N2.setText(A0X);
                break;
            case 6:
                A0N.setText(R.string.voip_not_connected_title);
                i = R.string.voip_not_connected_peer_fail;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 7:
                A0N2.setText(R.string.voip_video_not_enabled_for_caller);
                break;
            case 8:
                i = R.string.voip_video_not_allowed_at_this_time;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 9:
                i = R.string.voip_video_call_app_needs_update;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 10:
            case 11:
                i = R.string.voip_video_call_old_os_ver;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case 12:
                r10 = ((ActivityC13830kP) this).A01;
                i2 = R.plurals.voip_group_call_not_supported_plural;
                A0X = r10.A0I(new Object[]{A00}, i2, (long) A0X2.size());
                A0N2.setText(A0X);
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i = R.string.voip_group_call_old_os_ver;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1O);
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, A02, 0);
                A0X = ((ActivityC13830kP) this).A01.A0I(objArr, R.plurals.voip_group_call_reach_maximum, (long) A02);
                A0N2.setText(A0X);
                break;
            case 15:
                i = R.string.unable_to_add_participant_to_group_call;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                i = R.string.voip_peer_not_registered;
                A0X = C12960it.A0X(this, A00, new Object[1], 0, i);
                A0N2.setText(A0X);
                break;
            default:
                r10 = ((ActivityC13830kP) this).A01;
                i2 = R.plurals.voip_not_allowed_at_this_time_plural;
                A0X = r10.A0I(new Object[]{A00}, i2, (long) A0X2.size());
                A0N2.setText(A0X);
                break;
        }
        TextView A0N3 = C12990iw.A0N(this, R.id.ok);
        View A05 = AnonymousClass00T.A05(this, R.id.more);
        if (str == null) {
            A05.setVisibility(8);
            i3 = R.string.ok;
        } else {
            A05.setVisibility(0);
            A05.setOnClickListener(new ViewOnClickCListenerShape1S1100000_I1(1, str, this));
            i3 = R.string.ok_got_it;
        }
        A0N3.setText(i3);
        C12960it.A0z(A0N3, this, 41);
        LinearLayout linearLayout = (LinearLayout) AnonymousClass00T.A05(this, R.id.content);
        if (C12980iv.A0H(this).orientation == 1) {
            linearLayout.setOrientation(1);
        } else {
            linearLayout.setOrientation(0);
        }
        this.A03.A03(this.A05);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A03.A04(this.A05);
    }
}
