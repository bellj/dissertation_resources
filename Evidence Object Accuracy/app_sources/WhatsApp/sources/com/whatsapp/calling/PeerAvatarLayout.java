package com.whatsapp.calling;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass1SF;
import X.AnonymousClass28F;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C1102954z;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14820m6;
import X.C14850m9;
import X.C21270x9;
import X.C54212gM;
import android.content.Context;
import android.util.AttributeSet;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class PeerAvatarLayout extends RecyclerView implements AnonymousClass004 {
    public int A00;
    public int A01;
    public C54212gM A02;
    public AnonymousClass130 A03;
    public AnonymousClass28F A04;
    public AnonymousClass1J1 A05;
    public C21270x9 A06;
    public C14820m6 A07;
    public AnonymousClass018 A08;
    public C14850m9 A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;

    /* loaded from: classes3.dex */
    public class NonScrollingLinearLayoutManager extends LinearLayoutManager {
        @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
        public boolean A14() {
            return false;
        }

        @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
        public boolean A15() {
            return false;
        }
    }

    public PeerAvatarLayout(Context context) {
        this(context, null);
    }

    public PeerAvatarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PeerAvatarLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A09 = C12960it.A0S(A00);
            this.A07 = C12970iu.A0Z(A00);
            this.A03 = C12990iw.A0Y(A00);
            this.A06 = C12970iu.A0W(A00);
            this.A08 = C12960it.A0R(A00);
        }
        this.A02 = new C54212gM(this);
        NonScrollingLinearLayoutManager nonScrollingLinearLayoutManager = new NonScrollingLinearLayoutManager();
        nonScrollingLinearLayoutManager.A1Q(0);
        setLayoutManager(nonScrollingLinearLayoutManager);
        setAdapter(this.A02);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.call_avatar_view_elevation);
        this.A01 = getResources().getDimensionPixelSize(R.dimen.call_avatar_view_elevation_difference);
        this.A04 = new C1102954z(this.A03, AnonymousClass1SF.A0M(this.A07, this.A09));
        this.A05 = this.A06.A05("peer-avatar-photo", 0.0f, getResources().getDimensionPixelSize(R.dimen.call_avatar_view_photo_quality));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }
}
