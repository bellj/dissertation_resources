package com.whatsapp.calling;

import X.AbstractC14640lm;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12F;
import X.AnonymousClass1S6;
import X.AnonymousClass1SF;
import X.AnonymousClass2OC;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2YM;
import X.C14820m6;
import X.C14850m9;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15600nX;
import X.C15610nY;
import X.C20710wC;
import X.C21250x7;
import X.C252718t;
import X.C28801Pb;
import X.C53192d4;
import X.C54212gM;
import X.C65293Iy;
import X.C72773f9;
import X.C74343hp;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class CallDetailsLayout extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public Typeface A03;
    public FrameLayout A04;
    public TextView A05;
    public TextView A06;
    public C15450nH A07;
    public C28801Pb A08;
    public PeerAvatarLayout A09;
    public C15550nR A0A;
    public C15610nY A0B;
    public AnonymousClass01d A0C;
    public C14820m6 A0D;
    public AnonymousClass018 A0E;
    public C15600nX A0F;
    public C21250x7 A0G;
    public C14850m9 A0H;
    public C20710wC A0I;
    public AnonymousClass12F A0J;
    public C252718t A0K;
    public AnonymousClass2P7 A0L;
    public boolean A0M;

    public CallDetailsLayout(Context context) {
        this(context, null);
    }

    public CallDetailsLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CallDetailsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A0H = (C14850m9) r1.A04.get();
            this.A0K = (C252718t) r1.A9K.get();
            this.A07 = (C15450nH) r1.AII.get();
            this.A0G = (C21250x7) r1.AJh.get();
            this.A0A = (C15550nR) r1.A45.get();
            this.A0C = (AnonymousClass01d) r1.ALI.get();
            this.A0B = (C15610nY) r1.AMe.get();
            this.A0E = (AnonymousClass018) r1.ANb.get();
            this.A0I = (C20710wC) r1.A8m.get();
            this.A0J = (AnonymousClass12F) r1.AJM.get();
            this.A0F = (C15600nX) r1.A8x.get();
            this.A0D = (C14820m6) r1.AN3.get();
        }
        LayoutInflater.from(context).inflate(R.layout.call_details_layout, (ViewGroup) this, true);
        this.A06 = (TextView) findViewById(R.id.name);
        this.A08 = new C28801Pb(this, this.A0B, this.A0J, (int) R.id.name);
        this.A05 = (TextView) findViewById(R.id.call_status);
        this.A09 = (PeerAvatarLayout) findViewById(R.id.peer_avatar_layout);
        this.A04 = (FrameLayout) findViewById(R.id.peer_avatar_container);
        this.A01 = getResources().getColor(R.color.primary_voip);
        this.A02 = getResources().getColor(R.color.transparent);
        this.A03 = Typeface.create("sans-serif", 0);
        AnonymousClass028.A0g(this.A05, new C74343hp(this));
    }

    public static final ObjectAnimator A00(View view, String str, float f) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, str, f);
        ofFloat.setInterpolator(new DecelerateInterpolator());
        ofFloat.setDuration(125L);
        return ofFloat;
    }

    public static final void A01(View view, Integer num, Integer num2) {
        int intValue;
        int intValue2;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        if (num == null) {
            intValue = marginLayoutParams.topMargin;
        } else {
            intValue = num.intValue();
        }
        Integer valueOf = Integer.valueOf(intValue);
        if (num2 == null) {
            intValue2 = marginLayoutParams.bottomMargin;
        } else {
            intValue2 = num2.intValue();
        }
        Integer valueOf2 = Integer.valueOf(intValue2);
        int i = marginLayoutParams.topMargin;
        int intValue3 = valueOf.intValue();
        if (i != intValue3 || marginLayoutParams.bottomMargin != valueOf2.intValue()) {
            marginLayoutParams.topMargin = intValue3;
            marginLayoutParams.bottomMargin = valueOf2.intValue();
            view.setLayoutParams(marginLayoutParams);
        }
    }

    public void A02(GroupJid groupJid) {
        C15370n3 A02;
        if (groupJid != null) {
            PeerAvatarLayout peerAvatarLayout = this.A09;
            if (peerAvatarLayout.getVisibility() != 8 && (A02 = AnonymousClass1SF.A02(this.A0A, this.A0G, this.A0I, groupJid)) != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(A02);
                C54212gM r1 = peerAvatarLayout.A02;
                List list = r1.A00;
                list.clear();
                list.addAll(arrayList);
                r1.A02();
            }
        }
    }

    public void A03(CallInfo callInfo) {
        boolean z;
        AbstractC14640lm r0;
        Voip.CallState callState;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (AnonymousClass1S6 r3 : callInfo.participants.values()) {
            if (!r3.A0F) {
                int i = r3.A01;
                UserJid userJid = r3.A06;
                if (i == 1) {
                    arrayList.add(userJid);
                } else {
                    arrayList2.add(userJid);
                }
            }
        }
        ArrayList arrayList3 = new ArrayList(arrayList);
        arrayList3.addAll(arrayList2);
        setCallDetailsDescription(arrayList3, callInfo);
        GroupJid groupJid = callInfo.groupJid;
        C15550nR r5 = this.A0A;
        C15610nY r8 = this.A0B;
        String A09 = AnonymousClass1SF.A09(r5, r8, this.A0G, this.A0I, groupJid);
        int size = arrayList3.size();
        if (callInfo.isInLonelyState() || size != 0) {
            AnonymousClass009.A05(callInfo.getInitialPeerJid());
            if (callInfo.isInLonelyState() || !((callState = callInfo.callState) == Voip.CallState.ACTIVE || callState == Voip.CallState.ACCEPT_SENT || callState == Voip.CallState.ACCEPT_RECEIVED)) {
                z = false;
            } else {
                z = true;
            }
            C28801Pb r2 = this.A08;
            r2.A01.setTypeface(this.A03, 0);
            if (size == 1) {
                if (arrayList3.contains(callInfo.getInitialPeerJid())) {
                    r0 = callInfo.getInitialPeerJid();
                } else {
                    r0 = (AbstractC14640lm) arrayList3.get(0);
                }
                r2.A06(r5.A0B(r0));
                return;
            }
            if (A09 == null) {
                if (callInfo.videoEnabled || !z) {
                    Context context = getContext();
                    AnonymousClass2OC A02 = C65293Iy.A02(r5, r8, arrayList3, false);
                    if (A02 == null) {
                        A09 = null;
                    } else {
                        A09 = A02.A00(context);
                    }
                } else {
                    A09 = getContext().getString(R.string.group_voip_call_title);
                }
            }
            r2.A09(null, A09);
        }
    }

    public void A04(Voip.CallState callState, boolean z, boolean z2) {
        String str;
        Log.i("voip/CallDetailsLayout/animateAvatarLayout");
        if (callState == Voip.CallState.NONE) {
            str = "voip/CallDetailsLayout/animateAvatarLayout return directly, no call is going on";
        } else {
            int i = this.A00;
            if (i == 1) {
                StringBuilder sb = new StringBuilder("voip/CallDetailsLayout/animateAvatarLayout return directly, avatarAnimationState: ");
                sb.append(i);
                str = sb.toString();
            } else if (this.A04.getVisibility() == 8) {
                str = "voip/CallDetailsLayout/animateAvatarLayout return directly, peerAvatarLayout.getVisibility() == View.GONE ";
            } else {
                PeerAvatarLayout peerAvatarLayout = this.A09;
                int height = peerAvatarLayout.getHeight();
                if (height == 0) {
                    peerAvatarLayout.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                }
                this.A00 = 1;
                int i2 = 3;
                if (z) {
                    i2 = 1;
                }
                ObjectAnimator[] objectAnimatorArr = new ObjectAnimator[i2];
                if (z) {
                    objectAnimatorArr[0] = A00(this, "alpha", 0.0f);
                } else if (!AnonymousClass1SF.A0P(this.A0H) || z2) {
                    for (int i3 = 0; i3 < peerAvatarLayout.getChildCount(); i3++) {
                        LinearLayout linearLayout = ((C53192d4) peerAvatarLayout.getChildAt(i3)).A01;
                        height = linearLayout.getMeasuredHeight();
                        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
                        scaleAnimation.setInterpolator(new DecelerateInterpolator());
                        scaleAnimation.setDuration(125);
                        linearLayout.startAnimation(scaleAnimation);
                    }
                    float f = (float) (-height);
                    objectAnimatorArr[0] = A00(peerAvatarLayout, "translationY", f);
                    objectAnimatorArr[1] = A00(this.A06, "translationY", f);
                    objectAnimatorArr[2] = A00(this.A05, "translationY", f);
                } else {
                    peerAvatarLayout.animate().alpha(0.0f).setDuration(200).setListener(new AnonymousClass2YM(this)).start();
                    return;
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimatorArr);
                animatorSet.start();
                animatorSet.addListener(new C72773f9(this, z));
                return;
            }
        }
        Log.i(str);
    }

    public void A05(Voip.CallState callState, boolean z, boolean z2, boolean z3) {
        if (!z3 && ((callState != Voip.CallState.ACTIVE && callState != Voip.CallState.ACCEPT_SENT) || z)) {
            A06(callState, z2, true, z);
            setAlpha(1.0f);
            setVisibility(0);
            setBackgroundColor(0);
        } else if (this.A00 != 1) {
            setVisibility(8);
            this.A04.setVisibility(8);
        }
    }

    public final void A06(Voip.CallState callState, boolean z, boolean z2, boolean z3) {
        boolean z4;
        C14820m6 r0 = this.A0D;
        C14850m9 r5 = this.A0H;
        int i = 0;
        if (!AnonymousClass1SF.A0N(r0, r5) || !z) {
            z4 = true;
            if (((!Voip.A0A(callState) && callState != Voip.CallState.CALLING && callState != Voip.CallState.PRE_ACCEPT_RECEIVED && !z3) || z) && this.A00 != 1) {
                z4 = false;
            }
            A01(this.A06, Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.joinable_call_name_top_margin)), Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.joinable_call_name_bottom_margin)));
        } else {
            A01(this.A06, Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.m5_joinable_call_name_top_margin)), Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.m5_joinable_call_name_bottom_margin)));
            z4 = false;
        }
        if (!AnonymousClass1SF.A0P(r5) || this.A04.getVisibility() != 0 || z4) {
            FrameLayout frameLayout = this.A04;
            if (!z4) {
                i = 8;
            }
            frameLayout.setVisibility(i);
        } else {
            A04(callState, z2, z);
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.joinable_call_top_bar_height);
        if (z4) {
            dimensionPixelSize = (dimensionPixelSize + getResources().getDimensionPixelSize(R.dimen.call_avatar_top_bar_margin)) - getResources().getDimensionPixelSize(R.dimen.call_avatar_view_elevation);
        }
        A01(this, Integer.valueOf(dimensionPixelSize), null);
    }

    public void A07(String str, String str2) {
        TextView textView = this.A05;
        int i = 0;
        if (str == null) {
            i = 8;
        }
        textView.setVisibility(i);
        textView.setText(str);
        textView.setContentDescription(str2);
    }

    public void A08(List list) {
        PeerAvatarLayout peerAvatarLayout = this.A09;
        if (peerAvatarLayout.getVisibility() != 8) {
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < list.size() && i < 7) {
                arrayList.add(this.A0A.A0B((AbstractC14640lm) list.get(i)));
                i++;
            }
            C54212gM r1 = peerAvatarLayout.A02;
            List list2 = r1.A00;
            list2.clear();
            list2.addAll(arrayList);
            r1.A02();
        }
    }

    public boolean A09(CallInfo callInfo) {
        if (callInfo.isGroupCall()) {
            if (callInfo.videoEnabled) {
                return true;
            }
            if (AnonymousClass1SF.A02(this.A0A, this.A0G, this.A0I, callInfo.groupJid) == null) {
                return true;
            }
        }
        return AnonymousClass1SF.A0N(this.A0D, this.A0H) && !Voip.A0A(callInfo.callState) && callInfo.videoEnabled;
    }

    @Override // android.view.View
    public void clearAnimation() {
        PeerAvatarLayout peerAvatarLayout = this.A09;
        peerAvatarLayout.clearAnimation();
        peerAvatarLayout.setTranslationY(0.0f);
        peerAvatarLayout.setAlpha(1.0f);
        TextView textView = this.A05;
        textView.clearAnimation();
        textView.setTranslationY(0.0f);
        TextView textView2 = this.A06;
        textView2.clearAnimation();
        textView2.setTranslationY(0.0f);
        for (int i = 0; i < peerAvatarLayout.getChildCount(); i++) {
            ThumbnailButton thumbnailButton = ((C53192d4) peerAvatarLayout.getChildAt(i)).A02;
            thumbnailButton.clearAnimation();
            thumbnailButton.setScaleX(1.0f);
            thumbnailButton.setScaleY(1.0f);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0L;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0L = r0;
        }
        return r0.generatedComponent();
    }

    public String getNameViewContentDescription() {
        TextView textView = this.A06;
        if (textView.getContentDescription() != null) {
            return textView.getContentDescription().toString();
        }
        return null;
    }

    public void setCallDetailsDescription(List list, CallInfo callInfo) {
        String A00;
        String str;
        setFocusable(true);
        AnonymousClass028.A0a(this.A04, 1);
        GroupJid groupJid = callInfo.groupJid;
        C15550nR r9 = this.A0A;
        C15610nY r7 = this.A0B;
        String A09 = AnonymousClass1SF.A09(r9, r7, this.A0G, this.A0I, groupJid);
        if (A09 != null) {
            A00 = A09;
        } else {
            Context context = getContext();
            AnonymousClass2OC A02 = C65293Iy.A02(r9, r7, list, false);
            if (A02 == null) {
                A00 = null;
            } else {
                A00 = A02.A00(context);
            }
        }
        if (Voip.A0A(callInfo.callState)) {
            Context context2 = getContext();
            boolean z = callInfo.videoEnabled;
            int i = R.string.voip_joinable_incoming_voice_call_label;
            if (z) {
                i = R.string.voip_joinable_incoming_video_call_label;
            }
            str = context2.getString(i);
            AnonymousClass028.A0a(this.A05, 2);
            if (A09 != null) {
                TextView textView = this.A06;
                Context context3 = getContext();
                UserJid peerJid = callInfo.getPeerJid();
                AnonymousClass009.A05(peerJid);
                textView.setContentDescription(context3.getString(R.string.linked_group_call_incoming_call_label_with_placeholders, str, r7.A0A(r9.A0B(peerJid), -1), A00));
                return;
            }
        } else if (!callInfo.isGroupCall() || (!callInfo.isInLonelyState() && callInfo.callState != Voip.CallState.CALLING)) {
            Context context4 = getContext();
            boolean z2 = callInfo.videoEnabled;
            int i2 = R.string.audio_call;
            if (z2) {
                i2 = R.string.video_call;
            }
            str = context4.getString(i2);
            TextView textView2 = this.A05;
            AnonymousClass028.A0a(textView2, 1);
            textView2.setFocusable(true);
        } else {
            TextView textView3 = this.A06;
            Context context5 = getContext();
            boolean z3 = callInfo.videoEnabled;
            int i3 = R.string.voip_joinable_outgoing_voice_call_label;
            if (z3) {
                i3 = R.string.voip_joinable_outgoing_video_call_label;
            }
            textView3.setContentDescription(context5.getString(i3, A00));
            AnonymousClass028.A0a(this.A05, 2);
            return;
        }
        this.A06.setContentDescription(getContext().getString(R.string.voip_joinable_accessibility_call_label_with_placeholders, str, A00));
    }
}
