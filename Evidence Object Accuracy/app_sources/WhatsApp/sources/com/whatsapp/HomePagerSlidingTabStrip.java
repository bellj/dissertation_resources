package com.whatsapp;

import X.AnonymousClass2QL;
import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes2.dex */
public class HomePagerSlidingTabStrip extends PagerSlidingTabStrip {
    public boolean A00;

    public HomePagerSlidingTabStrip(Context context) {
        super(context, null);
        A00();
    }

    public HomePagerSlidingTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public HomePagerSlidingTabStrip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public HomePagerSlidingTabStrip(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    @Override // com.whatsapp.PagerSlidingTabStrip
    public void A02(View view, int i) {
        LayoutTransition layoutTransition;
        super.A02(view, i);
        if (view instanceof ViewGroup) {
            View childAt = ((ViewGroup) view).getChildAt(0);
            if ((childAt instanceof ViewGroup) && (layoutTransition = ((ViewGroup) childAt).getLayoutTransition()) != null) {
                layoutTransition.addTransitionListener(new AnonymousClass2QL(this));
            }
        }
    }
}
