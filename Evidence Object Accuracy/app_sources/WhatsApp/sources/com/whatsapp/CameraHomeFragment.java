package com.whatsapp;

import X.AbstractC14770m1;
import X.AbstractC14780m2;
import X.AbstractC14940mI;
import X.AnonymousClass2LB;
import android.graphics.drawable.Drawable;

/* loaded from: classes2.dex */
public class CameraHomeFragment extends Hilt_CameraHomeFragment implements AbstractC14940mI, AbstractC14770m1 {
    @Override // X.AbstractC14770m1
    public /* synthetic */ void A68(AnonymousClass2LB r1) {
    }

    @Override // X.AbstractC14940mI
    public String AE3() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AE4() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AE5() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AGV() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AGW() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public String AHA() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public Drawable AHB() {
        return null;
    }

    @Override // X.AbstractC14940mI
    public void ASK() {
    }

    @Override // X.AbstractC14940mI
    public void AVh() {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void Acn(boolean z) {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void Aco(boolean z) {
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ void A5j(AbstractC14780m2 r1) {
        r1.AM3();
    }

    @Override // X.AbstractC14770m1
    public /* synthetic */ boolean Aed() {
        return false;
    }
}
