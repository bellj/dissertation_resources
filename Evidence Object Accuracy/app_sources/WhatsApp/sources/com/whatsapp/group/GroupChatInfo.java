package com.whatsapp.group;

import X.AbstractActivityC13840kQ;
import X.AbstractActivityC33001d7;
import X.AbstractC116255Us;
import X.AbstractC116275Uu;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.AbstractC33021d9;
import X.AbstractC33111dN;
import X.AbstractC33331dp;
import X.AbstractC36111jL;
import X.AbstractC36121jM;
import X.AbstractC36671kL;
import X.AbstractC454421p;
import X.AbstractC469528i;
import X.AbstractC58392on;
import X.ActivityC001000l;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass04v;
import X.AnonymousClass10S;
import X.AnonymousClass10V;
import X.AnonymousClass10Z;
import X.AnonymousClass11A;
import X.AnonymousClass11F;
import X.AnonymousClass11G;
import X.AnonymousClass12F;
import X.AnonymousClass12G;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass132;
import X.AnonymousClass13M;
import X.AnonymousClass13P;
import X.AnonymousClass13Q;
import X.AnonymousClass14X;
import X.AnonymousClass150;
import X.AnonymousClass193;
import X.AnonymousClass198;
import X.AnonymousClass19M;
import X.AnonymousClass19O;
import X.AnonymousClass19Q;
import X.AnonymousClass19Y;
import X.AnonymousClass1AT;
import X.AnonymousClass1BB;
import X.AnonymousClass1E5;
import X.AnonymousClass1E8;
import X.AnonymousClass1J1;
import X.AnonymousClass1Q6;
import X.AnonymousClass23N;
import X.AnonymousClass2BM;
import X.AnonymousClass2BP;
import X.AnonymousClass2Dn;
import X.AnonymousClass2Ew;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass2IJ;
import X.AnonymousClass2IT;
import X.AnonymousClass2J0;
import X.AnonymousClass2TT;
import X.AnonymousClass2eq;
import X.AnonymousClass31K;
import X.AnonymousClass37H;
import X.AnonymousClass3DS;
import X.AnonymousClass3FT;
import X.AnonymousClass3GI;
import X.AnonymousClass4V1;
import X.AnonymousClass5U9;
import X.AnonymousClass5UJ;
import X.C004802e;
import X.C004902f;
import X.C103104qD;
import X.C1114659m;
import X.C12970iu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15220ml;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15660nh;
import X.C15810nw;
import X.C15860o1;
import X.C15880o3;
import X.C15890o4;
import X.C16030oK;
import X.C16120oU;
import X.C16170oZ;
import X.C16630pM;
import X.C17010q7;
import X.C17070qD;
import X.C17220qS;
import X.C17900ra;
import X.C18470sV;
import X.C18640sm;
import X.C18750sx;
import X.C18810t5;
import X.C19850um;
import X.C19990v2;
import X.C20000v3;
import X.C20050v8;
import X.C20650w6;
import X.C20660w7;
import X.C20710wC;
import X.C20730wE;
import X.C21260x8;
import X.C21270x9;
import X.C21280xA;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22050yP;
import X.C22100yW;
import X.C22280yp;
import X.C22330yu;
import X.C22640zP;
import X.C22670zS;
import X.C22700zV;
import X.C22710zW;
import X.C231510o;
import X.C236812p;
import X.C237913a;
import X.C242114q;
import X.C243915i;
import X.C244215l;
import X.C244415n;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C253118x;
import X.C254219i;
import X.C255719x;
import X.C27131Gd;
import X.C27151Gf;
import X.C28391Mz;
import X.C28421Nd;
import X.C32931cy;
import X.C33181da;
import X.C33701ew;
import X.C35301hb;
import X.C35491i4;
import X.C36021jC;
import X.C36031jD;
import X.C36041jE;
import X.C36141jO;
import X.C36151jP;
import X.C36191jT;
import X.C36431js;
import X.C36661kH;
import X.C36801kb;
import X.C38131nZ;
import X.C42971wC;
import X.C47552Bj;
import X.C48962Ip;
import X.C49012Iu;
import X.C617832m;
import X.C626638c;
import X.C67313Ra;
import X.C67323Rb;
import X.C67353Re;
import X.C69003Xo;
import X.C69033Xr;
import X.C70083ai;
import X.C74483i6;
import X.C850641a;
import X.C858444j;
import X.C859244z;
import X.C89474Kc;
import X.DialogC58332oe;
import X.ExecutorC27271Gr;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.ListItemWithLeftIcon;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.WaTextView;
import com.whatsapp.communitysuspend.CommunitySuspendDialogFragment;
import com.whatsapp.components.InviteViaLinkView;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.view.custom.GroupDetailsCard;
import com.whatsapp.groupsuspend.CreateGroupSuspendDialog;
import com.whatsapp.invites.InviteGroupParticipantsActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.text.ReadMoreTextView;
import com.whatsapp.ui.media.MediaCard;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class GroupChatInfo extends AbstractActivityC33001d7 implements AbstractC33111dN, AbstractC33021d9 {
    public long A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public View A0A;
    public View A0B;
    public ImageView A0C;
    public LinearLayout A0D;
    public ListView A0E;
    public TextView A0F;
    public TextView A0G;
    public TextView A0H;
    public TextView A0I;
    public TextView A0J;
    public AnonymousClass2IT A0K;
    public AnonymousClass2J0 A0L;
    public C48962Ip A0M;
    public C49012Iu A0N;
    public AnonymousClass2IJ A0O;
    public C237913a A0P;
    public AnonymousClass19Y A0Q;
    public WaTextView A0R;
    public C22330yu A0S;
    public AnonymousClass2Ew A0T;
    public C22640zP A0U;
    public AnonymousClass130 A0V;
    public AnonymousClass10S A0W;
    public C22700zV A0X;
    public C15610nY A0Y;
    public AnonymousClass10V A0Z;
    public AnonymousClass1J1 A0a;
    public C21270x9 A0b;
    public AnonymousClass131 A0c;
    public C17010q7 A0d;
    public C20730wE A0e;
    public C15220ml A0f;
    public C18750sx A0g;
    public C20650w6 A0h;
    public C21320xE A0i;
    public AnonymousClass13M A0j;
    public C236812p A0k;
    public AnonymousClass12H A0l;
    public C242114q A0m;
    public AnonymousClass132 A0n;
    public C15370n3 A0o;
    public C15370n3 A0p;
    public C22100yW A0q;
    public AnonymousClass11F A0r;
    public C231510o A0s;
    public AnonymousClass193 A0t;
    public C22050yP A0u;
    public C16120oU A0v;
    public AnonymousClass31K A0w;
    public GroupCallButtonController A0x;
    public C36661kH A0y;
    public AnonymousClass2BM A0z;
    public AnonymousClass3FT A10;
    public C74483i6 A11;
    public C35301hb A12;
    public C36041jE A13;
    public C36191jT A14;
    public AnonymousClass11A A15;
    public C244215l A16;
    public AnonymousClass1E8 A17;
    public GroupDetailsCard A18;
    public AnonymousClass1E5 A19;
    public AnonymousClass11G A1A;
    public AnonymousClass2BP A1B;
    public C15580nU A1C;
    public C15580nU A1D;
    public C16030oK A1E;
    public C244415n A1F;
    public C17220qS A1G;
    public C20660w7 A1H;
    public C16630pM A1I;
    public C22280yp A1J;
    public AnonymousClass1AT A1K;
    public AnonymousClass10Z A1L;
    public AnonymousClass12F A1M;
    public AnonymousClass12U A1N;
    public StatusesViewModel A1O;
    public ReadMoreTextView A1P;
    public MediaCard A1Q;
    public AnonymousClass198 A1R;
    public C254219i A1S;
    public C253118x A1T;
    public ExecutorC27271Gr A1U;
    public AbstractC16350or A1V;
    public AnonymousClass12G A1W;
    public C21260x8 A1X;
    public C21280xA A1Y;
    public C14860mA A1Z;
    public String A1a;
    public boolean A1b;
    public boolean A1c;
    public final CompoundButton.OnCheckedChangeListener A1d;
    public final AnonymousClass2Dn A1e;
    public final C27131Gd A1f;
    public final C27151Gf A1g;
    public final AnonymousClass5UJ A1h;
    public final AbstractC33331dp A1i;
    public final AbstractC469528i A1j;
    public final AnonymousClass13Q A1k;
    public final AnonymousClass13P A1l;

    @Override // X.AbstractActivityC13840kQ
    public int A1m() {
        return 703930303;
    }

    public GroupChatInfo() {
        this(0);
        this.A1j = new C859244z(this);
        this.A1f = new C36431js(this);
        this.A1e = new C850641a(this);
        this.A1i = new C858444j(this);
        this.A1g = new C32931cy(this);
        this.A1h = new AnonymousClass5UJ() { // from class: X.57E
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                if (groupChatInfo.A1C.equals(r3)) {
                    groupChatInfo.A2y();
                }
            }
        };
        this.A1d = new C36031jD(this);
        this.A1k = new C69003Xo(this);
        this.A1l = new C69033Xr(this);
    }

    public GroupChatInfo(int i) {
        this.A1b = false;
        A0R(new C103104qD(this));
    }

    public static /* synthetic */ void A02(AnonymousClass2TT r13, GroupChatInfo groupChatInfo) {
        int i;
        int i2;
        if (((AbstractActivityC33001d7) groupChatInfo).A0H.A0Y(groupChatInfo.A0o)) {
            i2 = R.string.failed_update_photo_no_longer_available;
        } else if (!((AbstractActivityC33001d7) groupChatInfo).A0C.A0C(groupChatInfo.A1C)) {
            i2 = R.string.failed_update_photo_not_authorized;
        } else {
            C15370n3 r3 = groupChatInfo.A0o;
            int i3 = 0;
            if (!r3.A0X) {
                C15580nU r2 = groupChatInfo.A1C;
                C15600nX r0 = ((AbstractActivityC33001d7) groupChatInfo).A0C;
                boolean A0C = r0.A0C(r2);
                boolean A0D = r0.A0D(r2);
                if (!A0C || (!A0D && r3.A0i)) {
                    Toast.makeText(groupChatInfo, (int) R.string.failed_update_group_info_not_admin, 0).show();
                    return;
                } else {
                    groupChatInfo.A33();
                    return;
                }
            } else if (!((AbstractActivityC33001d7) groupChatInfo).A0Q) {
                if (C28391Mz.A02()) {
                    i = groupChatInfo.getWindow().getStatusBarColor();
                } else {
                    i = 0;
                }
                if (C28391Mz.A04()) {
                    i3 = groupChatInfo.getWindow().getNavigationBarColor();
                }
                Intent A0N = C14960mK.A0N(groupChatInfo, groupChatInfo.A0o.A0D, null, 0.0f, i, 0, i3, 0, false);
                boolean A07 = ((ActivityC13810kN) groupChatInfo).A0C.A07(1533);
                AnonymousClass2Ew r1 = groupChatInfo.A0T;
                int i4 = R.id.profile_picture_image;
                if (A07) {
                    i4 = R.id.wds_profile_picture;
                }
                groupChatInfo.startActivityForResult(A0N, 15, AbstractC454421p.A05(groupChatInfo, AnonymousClass028.A0D(r1, i4), r13.A00(R.string.transition_photo)));
                return;
            } else {
                return;
            }
        }
        groupChatInfo.Ado(i2);
    }

    public static /* synthetic */ void A03(GroupChatInfo groupChatInfo) {
        C15370n3 A09 = ((AbstractActivityC33001d7) groupChatInfo).A06.A09(groupChatInfo.A1C);
        groupChatInfo.A0o = A09;
        if (A09 != null) {
            C36041jE r3 = groupChatInfo.A13;
            r3.A0H.Ab6(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 42));
            groupChatInfo.A30();
            groupChatInfo.A31();
            groupChatInfo.A36();
        }
    }

    @Override // X.AbstractActivityC33011d8, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A1b) {
            this.A1b = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            ((AbstractActivityC33001d7) this).A0L = (AnonymousClass14X) r2.AFM.get();
            ((AbstractActivityC33001d7) this).A09 = (C19990v2) r2.A3M.get();
            ((AbstractActivityC33001d7) this).A01 = (C16170oZ) r2.AM4.get();
            ((AbstractActivityC33001d7) this).A0A = (C15650ng) r2.A4m.get();
            super.A0P = (AnonymousClass19O) r2.ACO.get();
            ((AbstractActivityC33001d7) this).A06 = (C15550nR) r2.A45.get();
            ((AbstractActivityC33001d7) this).A02 = (C19850um) r2.A2v.get();
            ((AbstractActivityC33001d7) this).A08 = (AnonymousClass018) r2.ANb.get();
            ((AbstractActivityC33001d7) this).A0K = (C17070qD) r2.AFC.get();
            ((AbstractActivityC33001d7) this).A04 = (C243915i) r2.A37.get();
            ((AbstractActivityC33001d7) this).A0H = (C20710wC) r2.A8m.get();
            ((AbstractActivityC33001d7) this).A0D = (C20000v3) r2.AAG.get();
            ((AbstractActivityC33001d7) this).A0E = (C20050v8) r2.AAV.get();
            ((AbstractActivityC33001d7) this).A0F = (C15660nh) r2.ABE.get();
            ((AbstractActivityC33001d7) this).A0N = (C15860o1) r2.A3H.get();
            ((AbstractActivityC33001d7) this).A0I = (C17900ra) r2.AF5.get();
            ((AbstractActivityC33001d7) this).A03 = (AnonymousClass19Q) r2.A2u.get();
            ((AbstractActivityC33001d7) this).A07 = (C15890o4) r2.AN1.get();
            ((AbstractActivityC33001d7) this).A0B = (AnonymousClass1BB) r2.A68.get();
            ((AbstractActivityC33001d7) this).A0J = (C22710zW) r2.AF7.get();
            ((AbstractActivityC33001d7) this).A0O = (C255719x) r2.A5X.get();
            ((AbstractActivityC33001d7) this).A0C = (C15600nX) r2.A8x.get();
            ((AbstractActivityC33001d7) this).A0G = (AnonymousClass150) r2.A63.get();
            this.A0P = (C237913a) r2.ACt.get();
            this.A1T = (C253118x) r2.AAW.get();
            this.A1N = (AnonymousClass12U) r2.AJd.get();
            this.A0v = (C16120oU) r2.ANE.get();
            this.A1Z = (C14860mA) r2.ANU.get();
            this.A0h = (C20650w6) r2.A3A.get();
            this.A1H = (C20660w7) r2.AIB.get();
            this.A0s = (C231510o) r2.AHO.get();
            this.A0Q = (AnonymousClass19Y) r2.AI6.get();
            this.A1X = (C21260x8) r2.A2k.get();
            this.A0b = (C21270x9) r2.A4A.get();
            this.A1F = (C244415n) r2.AAg.get();
            this.A1Y = (C21280xA) r2.AMU.get();
            this.A0V = (AnonymousClass130) r2.A41.get();
            this.A0Y = (C15610nY) r2.AMe.get();
            this.A1J = (C22280yp) r2.AFw.get();
            this.A1W = (AnonymousClass12G) r2.A2h.get();
            this.A0W = (AnonymousClass10S) r2.A46.get();
            this.A0l = (AnonymousClass12H) r2.AC5.get();
            this.A1M = (AnonymousClass12F) r2.AJM.get();
            this.A1R = (AnonymousClass198) r2.A0J.get();
            this.A0g = (C18750sx) r2.A2p.get();
            this.A0u = (C22050yP) r2.A7v.get();
            this.A1A = (AnonymousClass11G) r2.AKq.get();
            this.A1S = (C254219i) r2.A0K.get();
            this.A19 = (AnonymousClass1E5) r2.AKs.get();
            this.A0S = (C22330yu) r2.A3I.get();
            this.A0e = (C20730wE) r2.A4J.get();
            this.A1G = (C17220qS) r2.ABt.get();
            this.A0r = (AnonymousClass11F) r2.AE3.get();
            this.A0n = (AnonymousClass132) r2.ALx.get();
            this.A0Z = (AnonymousClass10V) r2.A48.get();
            this.A0t = (AnonymousClass193) r2.A6S.get();
            this.A1L = (AnonymousClass10Z) r2.AGM.get();
            this.A0X = (C22700zV) r2.AMN.get();
            this.A0m = (C242114q) r2.AJq.get();
            this.A0U = (C22640zP) r2.A3Z.get();
            this.A0k = (C236812p) r2.AAC.get();
            this.A0f = (C15220ml) r2.A4V.get();
            this.A0i = (C21320xE) r2.A4Y.get();
            this.A17 = (AnonymousClass1E8) r2.ADy.get();
            this.A0q = (C22100yW) r2.A3g.get();
            this.A1E = (C16030oK) r2.AAd.get();
            this.A15 = (AnonymousClass11A) r2.A8o.get();
            this.A1I = (C16630pM) r2.AIc.get();
            this.A0j = (AnonymousClass13M) r2.A8p.get();
            this.A0K = (AnonymousClass2IT) r1.A0K.get();
            this.A1K = (AnonymousClass1AT) r2.AG2.get();
            this.A0d = (C17010q7) r2.A43.get();
            this.A0c = (AnonymousClass131) r2.A49.get();
            this.A16 = (C244215l) r2.A8y.get();
            this.A0M = (C48962Ip) r1.A00.get();
            this.A0O = (AnonymousClass2IJ) r1.A0d.get();
            this.A0L = (AnonymousClass2J0) r1.A0H.get();
            this.A0N = (C49012Iu) r1.A0c.get();
        }
    }

    @Override // X.AbstractActivityC13840kQ
    public AnonymousClass1Q6 A1n() {
        AnonymousClass1Q6 A1n = super.A1n();
        A1n.A03 = true;
        A1n.A00 = 7;
        A1n.A04 = true;
        return A1n;
    }

    @Override // X.AbstractActivityC33001d7
    public void A2t(ArrayList arrayList) {
        super.A2t(arrayList);
        View findViewById = findViewById(R.id.header_bottom_shadow);
        if (findViewById == null) {
            return;
        }
        if (this.A03.getVisibility() != 8 || (arrayList != null && !arrayList.isEmpty() && ((AbstractActivityC33001d7) this).A0C.A0C(this.A1C))) {
            findViewById.setVisibility(8);
        } else {
            findViewById.setVisibility(0);
        }
    }

    public C15580nU A2u() {
        C15370n3 r1 = this.A0o;
        if (r1 == null) {
            return null;
        }
        return (C15580nU) r1.A0B(C15580nU.class);
    }

    public void A2v() {
        AnonymousClass009.A05(this.A1C);
        if (!((ActivityC13810kN) this).A07.A0B()) {
            boolean A03 = C18640sm.A03(getApplicationContext());
            int i = R.string.network_required;
            if (A03) {
                i = R.string.network_required_airplane_on;
            }
            ((ActivityC13810kN) this).A05.A04(i);
            return;
        }
        AbstractC14440lR r12 = ((ActivityC13830kP) this).A05;
        C14900mE r5 = ((ActivityC13810kN) this).A05;
        r12.Aaz(new C626638c(((ActivityC13810kN) this).A03, r5, this.A0S, this.A0U, this.A0Y, this.A0o, this, this.A1G, r12), this.A1D);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        if (r0 != false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2w() {
        /*
            r7 = this;
            r0 = 2131361965(0x7f0a00ad, float:1.8343697E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r7, r0)
            r7.A01 = r0
            X.0m9 r6 = r7.A0C
            X.0nX r2 = r7.A0C
            X.0wC r1 = r7.A0H
            X.1E5 r5 = r7.A19
            X.0n3 r4 = r7.A0o
            X.0nU r0 = r7.A1C
            boolean r3 = r2.A0D(r0)
            r0 = 1863(0x747, float:2.61E-42)
            boolean r0 = r6.A07(r0)
            r2 = 1
            if (r0 == 0) goto L_0x0043
            int r0 = r4.A03
            if (r0 != r2) goto L_0x0043
        L_0x0026:
            boolean r1 = r1.A0Y(r4)
            boolean r0 = r5.A00(r4)
            if (r3 != 0) goto L_0x0032
            if (r2 == 0) goto L_0x0037
        L_0x0032:
            if (r1 != 0) goto L_0x0037
            r2 = 1
            if (r0 == 0) goto L_0x0038
        L_0x0037:
            r2 = 0
        L_0x0038:
            android.view.View r1 = r7.A01
            r0 = 8
            if (r2 == 0) goto L_0x003f
            r0 = 0
        L_0x003f:
            r1.setVisibility(r0)
            return
        L_0x0043:
            r2 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.A2w():void");
    }

    public final void A2x() {
        AnonymousClass31K r1 = this.A0w;
        if (r1 != null) {
            r1.A01 = Boolean.TRUE;
        }
        int A06 = ((AbstractActivityC33001d7) this).A0H.A06(this.A1C);
        if (((Number) this.A13.A0G.A01()).intValue() < A06) {
            this.A0d.A00();
            C15580nU r3 = this.A1C;
            Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.contact.picker.AddGroupParticipantsSelector");
            className.putExtra("gid", r3.getRawString());
            startActivityForResult(className, 12);
            return;
        }
        C004802e r5 = new C004802e(this);
        r5.A07(R.string.alert);
        r5.A0A(getResources().getQuantityString(R.plurals.groupchat_reach_limit, A06, Integer.valueOf(A06)));
        r5.setPositiveButton(R.string.ok, null);
        r5.create().show();
    }

    public final void A2y() {
        Log.i("groupchatinfo/refresh");
        this.A0o = ((AbstractActivityC33001d7) this).A06.A0B(this.A1C);
        A3B(((Number) this.A13.A0G.A01()).intValue());
        A32();
        A3A();
        A30();
        A31();
        A36();
        C36041jE r3 = this.A13;
        r3.A0H.Ab6(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 42));
        this.A14.A00.setVisibility(8);
        boolean A0D = ((AbstractActivityC33001d7) this).A0C.A0D(this.A1C);
        A38();
        A37();
        A3F(A0D);
        A2w();
        invalidateOptionsMenu();
        A2z();
    }

    public final void A2z() {
        Log.i("GroupChatInfo/refreshCommunityHeaderAndLinkToCommunityHomeRow()");
        if (((AbstractActivityC33001d7) this).A0H.A0V()) {
            int A02 = ((AbstractActivityC33001d7) this).A09.A02(this.A1C);
            if (A02 != 2 && A02 != 3) {
                Log.i("GroupChatInfo/group type is incorrect()");
            } else if (((AbstractActivityC33001d7) this).A0C.A0C(this.A1C)) {
                this.A1U.execute(new RunnableBRunnable0Shape6S0100000_I0_6(this, 39));
            }
        }
    }

    public final void A30() {
        AbstractC58392on r4 = (AbstractC58392on) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.encryption_info_view);
        boolean z = this.A0o.A0Y;
        int i = R.string.contact_encryption;
        int i2 = R.string.group_info_encrypted;
        int i3 = 46;
        if (z) {
            i = R.string.settings_security;
            i2 = R.string.security_card_description_for_in_app_support;
            i3 = 45;
        }
        r4.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, i3));
        r4.setTitle(getString(i));
        r4.setDescription(getString(i2));
        r4.setVisibility(0);
    }

    public final void A31() {
        int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1P);
        View view = this.A03;
        if (A02 > 0) {
            view.setVisibility(0);
            String str = this.A0o.A0G.A02;
            if (((AbstractActivityC33001d7) this).A0H.A0a(this.A1C) && TextUtils.isEmpty(str)) {
                str = getString(R.string.group_announcement_description);
            }
            if (!TextUtils.isEmpty(str)) {
                this.A0R.setVisibility(8);
                this.A05.setVisibility(0);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(C42971wC.A03(((ActivityC13810kN) this).A08, this.A1I, AbstractC36671kL.A03(this, this.A1P.getPaint(), ((ActivityC13810kN) this).A0B, str)));
                this.A1T.A04(this, spannableStringBuilder);
                this.A1P.A0G(null, spannableStringBuilder);
                return;
            } else if (!((AbstractActivityC33001d7) this).A0C.A0C(this.A1C) || ((!((AbstractActivityC33001d7) this).A0C.A0D(this.A1C) && this.A0o.A0i) || ((AbstractActivityC33001d7) this).A0H.A0Y(this.A0o))) {
                this.A0R.setVisibility(8);
                view = this.A03;
            } else {
                this.A05.setVisibility(8);
                this.A0R.setVisibility(0);
                return;
            }
        }
        view.setVisibility(8);
    }

    public final void A32() {
        int i;
        int i2;
        int i3;
        String A0B;
        C15370n3 A0A;
        this.A0T.setTitleText(this.A0Y.A04(this.A0o));
        this.A0T.setTitleVerified(this.A0o.A0M());
        long A01 = C28421Nd.A01(this.A0o.A0P, Long.MIN_VALUE);
        C15550nR r7 = ((AbstractActivityC33001d7) this).A06;
        AbstractC14640lm r0 = (AbstractC14640lm) this.A0o.A0B(GroupJid.class);
        UserJid userJid = null;
        if (!(r0 == null || (A0A = r7.A0A(r0)) == null)) {
            userJid = A0A.A0E;
        }
        boolean A0F = ((ActivityC13790kL) this).A01.A0F(userJid);
        boolean z = false;
        if (userJid != null) {
            z = true;
        }
        if (this.A0o.A0Y) {
            A0B = getString(R.string.whatsapp_support);
        } else if (A01 != Long.MIN_VALUE) {
            long A02 = ((ActivityC13790kL) this).A05.A02(A01);
            AnonymousClass018 r5 = ((AbstractActivityC33001d7) this).A08;
            if (A0F) {
                i = R.string.group_creator_you_with_time_today;
                i2 = R.string.group_creator_you_with_time_yesterday;
                i3 = R.string.group_creator_you_with_time;
            } else if (z) {
                A0B = C38131nZ.A0B(r5, new Object[]{r5.A0F(this.A0Y.A0B(((AbstractActivityC33001d7) this).A06.A0B(userJid), 1, false))}, R.string.group_creator_name_with_time_today, R.string.group_creator_name_with_time_yesterday, R.string.group_creator_name_with_time, A02, true);
            } else {
                i = R.string.group_create_time_today;
                i2 = R.string.group_create_time_yesterday;
                i3 = R.string.group_create_time;
            }
            A0B = C38131nZ.A0B(r5, new Object[0], i, i2, i3, A02, true);
        } else if (A0F) {
            A0B = getString(R.string.group_creator_you);
        } else {
            A0B = z ? getString(R.string.group_creator_name, ((AbstractActivityC33001d7) this).A08.A0F(this.A0Y.A04(((AbstractActivityC33001d7) this).A06.A0B(userJid)))) : null;
        }
        TextView textView = this.A0F;
        if (textView != null) {
            textView.setText(A0B);
        }
    }

    public final void A33() {
        if (((AbstractActivityC33001d7) this).A07.A07()) {
            this.A1L.A06(this, this.A0o, 13);
            return;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_group_photo_update_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_group_photo_update;
        }
        RequestPermissionActivity.A0K(this, R.string.permission_storage_need_write_access_on_group_photo_update_request, i2);
    }

    public final void A34() {
        AnonymousClass2BM r0 = this.A0z;
        if (r0 != null) {
            r0.A03(true);
        }
        A2l();
        C74483i6 r02 = this.A11;
        r02.A01 = true;
        r02.A02.A0B(true);
        C14830m7 r5 = ((ActivityC13790kL) this).A05;
        C14900mE r3 = ((ActivityC13810kN) this).A05;
        C15550nR r4 = ((AbstractActivityC33001d7) this).A06;
        C17070qD r15 = ((AbstractActivityC33001d7) this).A0K;
        C20000v3 r8 = ((AbstractActivityC33001d7) this).A0D;
        C20050v8 r9 = ((AbstractActivityC33001d7) this).A0E;
        C15660nh r10 = ((AbstractActivityC33001d7) this).A0F;
        C242114q r11 = this.A0m;
        AnonymousClass2BM r2 = new AnonymousClass2BM(r3, r4, r5, ((AbstractActivityC33001d7) this).A0B, this.A0j, r8, r9, r10, r11, this.A0o, this, ((AbstractActivityC33001d7) this).A0J, r15);
        this.A0z = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    public final void A35() {
        A2q(AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.mute_layout), this.A1d, ((AbstractActivityC33001d7) this).A0N.A08(this.A1C.getRawString()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
        if (r3 != false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A36() {
        /*
            r5 = this;
            X.0nU r3 = r5.A1C
            X.0nX r2 = r5.A0C
            X.0n3 r1 = r5.A0o
            X.1E5 r0 = r5.A19
            boolean r4 = X.AnonymousClass3GI.A00(r2, r1, r0, r3)
            X.0wC r1 = r5.A0H
            X.0n3 r0 = r5.A0o
            boolean r3 = r1.A0Y(r0)
            r0 = 2131363828(0x7f0a07f4, float:1.8347476E38)
            android.view.View r2 = r5.findViewById(r0)
            r1 = 0
            if (r2 == 0) goto L_0x0028
            if (r4 == 0) goto L_0x0023
            r0 = 0
            if (r3 == 0) goto L_0x0025
        L_0x0023:
            r0 = 8
        L_0x0025:
            r2.setVisibility(r0)
        L_0x0028:
            r0 = 2131363827(0x7f0a07f3, float:1.8347474E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r5, r0)
            if (r4 == 0) goto L_0x0037
            if (r3 != 0) goto L_0x0037
        L_0x0033:
            r0.setVisibility(r1)
            return
        L_0x0037:
            r1 = 8
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.A36():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0076, code lost:
        if (r0 == 0) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d3, code lost:
        if (r0 != null) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005a, code lost:
        if (r6.A0X.A0G(r5) == false) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A37() {
        /*
            r7 = this;
            android.view.View r1 = r7.A00
            r0 = 2131364611(0x7f0a0b03, float:1.8349064E38)
            android.view.View r3 = X.AnonymousClass028.A0D(r1, r0)
            android.view.View r1 = r7.A00
            r0 = 2131361930(0x7f0a008a, float:1.8343626E38)
            android.view.View r6 = X.AnonymousClass028.A0D(r1, r0)
            android.view.View r1 = r7.A00
            r0 = 2131362800(0x7f0a03f0, float:1.834539E38)
            android.view.View r5 = X.AnonymousClass028.A0D(r1, r0)
            X.0nX r1 = r7.A0C
            X.0nU r0 = r7.A1C
            boolean r0 = r1.A0C(r0)
            r2 = 0
            r4 = 8
            if (r0 == 0) goto L_0x00a2
            r0 = 2131888129(0x7f120801, float:1.9410885E38)
            java.lang.String r1 = r7.getString(r0)
            r0 = 2131231873(0x7f080481, float:1.807984E38)
            r7.A2s(r1, r0)
            r3.setVisibility(r4)
            r6.setVisibility(r2)
            r5.setVisibility(r2)
            X.0wC r6 = r7.A0H
            X.0nU r5 = r7.A2u()
            boolean r0 = r6.A0V()
            if (r0 == 0) goto L_0x005c
            X.0v2 r0 = r6.A0O
            int r1 = r0.A02(r5)
            r0 = 2
            if (r1 != r0) goto L_0x005c
            X.0nX r0 = r6.A0X
            boolean r0 = r0.A0G(r5)
            r1 = 1
            if (r0 != 0) goto L_0x005d
        L_0x005c:
            r1 = 0
        L_0x005d:
            r0 = 2131365506(0x7f0a0e82, float:1.835088E38)
            android.view.View r0 = X.AnonymousClass00T.A05(r7, r0)
            if (r1 == 0) goto L_0x00d5
            r0.setVisibility(r2)
        L_0x0069:
            android.view.View r0 = r7.A03
            int r0 = r0.getVisibility()
            if (r0 == 0) goto L_0x0078
            int r0 = r3.getVisibility()
            r3 = 0
            if (r0 != 0) goto L_0x0079
        L_0x0078:
            r3 = 1
        L_0x0079:
            com.whatsapp.ui.media.MediaCard r1 = r7.A1Q
            X.AnonymousClass009.A03(r1)
            r0 = 8
            if (r3 == 0) goto L_0x0083
            r0 = 0
        L_0x0083:
            r1.setTopShadowVisibility(r0)
            if (r3 == 0) goto L_0x009f
            android.view.View r0 = r7.A03
            int r0 = r0.getVisibility()
            if (r0 == 0) goto L_0x009f
        L_0x0090:
            r1 = 2131363847(0x7f0a0807, float:1.8347514E38)
            android.view.View r0 = r7.A00
            android.view.View r0 = r0.findViewById(r1)
            if (r0 == 0) goto L_0x009e
            r0.setVisibility(r2)
        L_0x009e:
            return
        L_0x009f:
            r2 = 8
            goto L_0x0090
        L_0x00a2:
            r0 = 2131887697(0x7f120651, float:1.9410008E38)
            java.lang.String r1 = r7.getString(r0)
            r0 = 2131231620(0x7f080384, float:1.8079326E38)
            r7.A2s(r1, r0)
            X.0wC r1 = r7.A0H
            X.0n3 r0 = r7.A0o
            boolean r0 = r1.A0Y(r0)
            if (r0 != 0) goto L_0x00c6
            X.1E5 r1 = r7.A19
            X.0n3 r0 = r7.A0o
            boolean r0 = r1.A00(r0)
            if (r0 != 0) goto L_0x00c6
            r3.setVisibility(r2)
        L_0x00c6:
            r6.setVisibility(r4)
            r5.setVisibility(r4)
            r0 = 2131364130(0x7f0a0922, float:1.8348088E38)
            android.view.View r0 = r7.findViewById(r0)
            if (r0 == 0) goto L_0x0069
        L_0x00d5:
            r0.setVisibility(r4)
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.A37():void");
    }

    public final void A38() {
        TextView textView;
        Resources resources;
        int i;
        boolean A0C = ((AbstractActivityC33001d7) this).A0C.A0C(this.A1C);
        boolean A0f = this.A1E.A0f(A2u());
        List A09 = this.A1E.A09(this.A1C);
        if (!A0C || (A09.isEmpty() && !A0f)) {
            this.A09.setVisibility(8);
            return;
        }
        this.A09.setVisibility(0);
        if (!A0f) {
            textView = this.A0H;
            resources = getResources();
            i = R.plurals.contact_info_live_location_description;
        } else if (A09.isEmpty()) {
            this.A0H.setText(R.string.contact_info_live_location_description_you_are_sharing);
            return;
        } else if (A09.size() == 1) {
            this.A0H.setText(getString(R.string.contact_info_live_location_description_you_and_friend_are_sharing, ((AbstractActivityC33001d7) this).A08.A0F(this.A0Y.A04(((AbstractActivityC33001d7) this).A06.A0B((AbstractC14640lm) A09.get(0))))));
            return;
        } else {
            textView = this.A0H;
            resources = getResources();
            i = R.plurals.contact_info_live_location_description_you_and_other_people_are_sharing;
        }
        textView.setText(resources.getQuantityString(i, A09.size(), Integer.valueOf(A09.size())));
    }

    public final void A39() {
        C33181da A08 = ((AbstractActivityC33001d7) this).A0N.A08(this.A1C.getRawString());
        View findViewById = findViewById(R.id.notifications_info);
        int i = 0;
        if (findViewById != null) {
            if (!A08.A0I) {
                i = 8;
            }
            findViewById.setVisibility(i);
            return;
        }
        ListItemWithLeftIcon listItemWithLeftIcon = (ListItemWithLeftIcon) findViewById(R.id.notifications_layout);
        if (!A08.A0I) {
            i = 8;
        }
        listItemWithLeftIcon.setDescriptionVisibility(i);
    }

    public final void A3A() {
        AbstractC16350or r1 = this.A1V;
        if (r1 != null) {
            r1.A03(true);
        }
        AnonymousClass37H r2 = new AnonymousClass37H(this.A0c, this.A0o, this);
        this.A1V = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (r11.A0J.A05(r9) != 1) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0040, code lost:
        if (r2.A0O.A02(r1) != 2) goto L_0x0042;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3B(int r13) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.A3B(int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (r12.A0o.A0i == false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A3C(java.lang.String r13) {
        /*
            r12 = this;
            r9 = r12
            X.0nX r1 = r12.A0C
            X.0nU r0 = r12.A1C
            boolean r4 = r1.A0C(r0)
            X.0nX r1 = r12.A0C
            X.0nU r0 = r12.A1C
            boolean r0 = r1.A0D(r0)
            r7 = 1
            r2 = 0
            if (r0 != 0) goto L_0x001c
            X.0n3 r0 = r12.A0o
            boolean r0 = r0.A0i
            r3 = 1
            if (r0 != 0) goto L_0x001d
        L_0x001c:
            r3 = 0
        L_0x001d:
            java.lang.String r1 = "\n\\s*\n\\s*[\n\\s]+"
            java.lang.String r0 = "\n\n"
            java.lang.String r11 = r13.replaceAll(r1, r0)
            if (r4 != 0) goto L_0x002e
            r0 = 2131888252(0x7f12087c, float:1.9411134E38)
        L_0x002a:
            r12.Ado(r0)
        L_0x002d:
            return
        L_0x002e:
            if (r3 == 0) goto L_0x0034
            r0 = 2131888251(0x7f12087b, float:1.9411132E38)
            goto L_0x002a
        L_0x0034:
            X.0n3 r0 = r12.A0o
            X.1PD r0 = r0.A0G
            java.lang.String r0 = r0.A02
            boolean r0 = android.text.TextUtils.equals(r0, r11)
            if (r0 != 0) goto L_0x002d
            X.0sm r0 = r12.A07
            boolean r0 = r0.A0B()
            if (r0 == 0) goto L_0x0088
            int r3 = X.AnonymousClass2VC.A00(r11)
            X.0nH r1 = r12.A06
            X.0oW r0 = X.AbstractC15460nI.A1P
            int r6 = r1.A02(r0)
            if (r3 > r6) goto L_0x006f
            X.0lR r1 = r12.A05
            X.0m7 r6 = r12.A05
            X.0mE r4 = r12.A05
            X.0nT r5 = r12.A01
            X.0w7 r10 = r12.A1H
            X.0xE r7 = r12.A0i
            X.0n3 r8 = r12.A0o
            X.32Z r3 = new X.32Z
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            java.lang.Void[] r0 = new java.lang.Void[r2]
            r1.Aaz(r3, r0)
            return
        L_0x006f:
            X.0mE r5 = r12.A05
            android.content.res.Resources r4 = r12.getResources()
            r3 = 2131755052(0x7f10002c, float:1.9140972E38)
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r1[r2] = r0
            java.lang.String r0 = r4.getQuantityString(r3, r6, r1)
            r5.A0E(r0, r2)
            return
        L_0x0088:
            X.0mE r1 = r12.A05
            r0 = 2131889600(0x7f120dc0, float:1.9413868E38)
            r1.A07(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.A3C(java.lang.String):void");
    }

    public final void A3D(List list) {
        if (((ActivityC13810kN) this).A07.A0B()) {
            Ady(R.string.participant_adding, R.string.register_wait_message);
            C14860mA r9 = this.A1Z;
            C47552Bj r4 = new C47552Bj(this.A0i, this, ((AbstractActivityC33001d7) this).A0H, this.A1C, r9, list);
            C20660w7 r1 = this.A1H;
            if (r1.A01.A06) {
                Log.i("sendmethods/sendAddParticipants");
                r1.A06.A08(Message.obtain(null, 0, 15, 0, r4), false);
                return;
            }
            return;
        }
        boolean A03 = C18640sm.A03(getApplicationContext());
        int i = R.string.network_required;
        if (A03) {
            i = R.string.network_required_airplane_on;
        }
        ((ActivityC13810kN) this).A05.A04(i);
        A2y();
    }

    public final void A3E(boolean z) {
        AnonymousClass028.A0a(this.A0T, 4);
        C004902f r2 = new C004902f(A0V());
        GroupParticipantsSearchFragment groupParticipantsSearchFragment = new GroupParticipantsSearchFragment();
        r2.A07(groupParticipantsSearchFragment, R.id.search_container);
        groupParticipantsSearchFragment.A01 = this.A0T;
        groupParticipantsSearchFragment.A00 = 0;
        groupParticipantsSearchFragment.A09 = z;
        groupParticipantsSearchFragment.A08 = true;
        groupParticipantsSearchFragment.A0A = true;
        r2.A0F(null);
        r2.A01();
    }

    public final void A3F(boolean z) {
        AnonymousClass2Ew r3 = this.A0T;
        Integer valueOf = Integer.valueOf(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material));
        AnonymousClass009.A05(valueOf);
        r3.A09(valueOf.intValue(), (z ? 1 : 0) * getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material));
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC33111dN
    public void AVU(UserJid userJid) {
        A2C(R.string.revoking_invite);
        ((ActivityC13830kP) this).A05.Aaz(new C617832m(((ActivityC13810kN) this).A05, this, this.A1C, userJid, this.A1H), new Void[0]);
    }

    @Override // X.AbstractC33021d9
    public void AWa(C33701ew r2) {
        StatusesViewModel statusesViewModel = this.A1O;
        if (statusesViewModel != null) {
            statusesViewModel.A09(r2);
        }
    }

    @Override // X.AbstractActivityC33001d7, android.app.Activity
    public void finishAfterTransition() {
        if (AbstractC454421p.A00) {
            this.A06.setTransitionName(null);
            TransitionSet transitionSet = new TransitionSet();
            Slide slide = new Slide(48);
            slide.addTarget(this.A06);
            transitionSet.addTransition(slide);
            Slide slide2 = new Slide(80);
            slide2.addTarget(this.A0E);
            transitionSet.addTransition(slide2);
            getWindow().setReturnTransition(transitionSet);
        }
        super.finishAfterTransition();
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10) {
            this.A0e.A06();
            this.A1R.A00();
        } else if (i != 151) {
            if (i == 16) {
                A39();
            } else if (i != 17) {
                switch (i) {
                    case 12:
                        if (i2 == -1) {
                            A3D(C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("contacts")));
                            return;
                        }
                        return;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        if (i2 == -1) {
                            if (intent != null) {
                                if (!intent.getBooleanExtra("is_reset", false)) {
                                    if (intent.getBooleanExtra("skip_cropping", false)) {
                                        this.A1L.A01.A0M("tmpi").delete();
                                        break;
                                    }
                                } else {
                                    this.A0W.A06(A2u());
                                    this.A0B.setVisibility(0);
                                    this.A1L.A08(this.A0o);
                                    return;
                                }
                            }
                            this.A1L.A04(intent, this, 14);
                            return;
                        }
                        return;
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        this.A1L.A01.A0M("tmpi").delete();
                        if (i2 != -1) {
                            if (i2 == 0 && intent != null) {
                                this.A1L.A03(intent, this);
                                return;
                            }
                            return;
                        }
                        break;
                    default:
                        return;
                }
                this.A0W.A06(A2u());
                if (this.A1L.A0A(this.A0o)) {
                    this.A0B.setVisibility(0);
                }
            } else if (i2 == 0) {
                InviteGroupParticipantsActivity.A02(this, intent, ((ActivityC13810kN) this).A00, 17).A03();
            } else if (i2 == -1) {
                A34();
            }
        } else if (i2 == -1) {
            AnonymousClass10V r0 = this.A0Z;
            r0.A05.A04(this.A0o);
            A3A();
            A33();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AnonymousClass01E A07 = A0V().A07(R.id.search_container);
        if (A07 instanceof GroupParticipantsSearchFragment) {
            ((GroupParticipantsSearchFragment) A07).A1B();
        } else {
            super.onBackPressed();
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        Intent intent;
        C14960mK r1;
        boolean z;
        AbstractC36121jM r12 = ((AnonymousClass4V1) ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).targetView.getTag()).A00;
        if (r12 instanceof AbstractC36111jL) {
            this.A0p = ((AbstractC36111jL) r12).A00;
        }
        int itemId = menuItem.getItemId();
        C15370n3 r4 = this.A0p;
        if (r4 == null) {
            return true;
        }
        switch (itemId) {
            case 0:
                if (r4.A0C == null) {
                    return true;
                }
                r1 = new C14960mK();
                intent = r1.A0h(this, r4, 4);
                startActivity(intent);
                return true;
            case 1:
                ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, r4).putExtra("args_conversation_screen_entry_point", 3));
                return true;
            case 2:
                z = true;
                C254219i r13 = this.A1S;
                Jid A0B = r4.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(A0B);
                Intent A00 = r13.A00(r4, (AbstractC14640lm) A0B, z);
                A00.setFlags(524288);
                try {
                    startActivityForResult(A00, 10);
                    this.A1R.A02(z, 8);
                    return true;
                } catch (ActivityNotFoundException unused) {
                    C36021jC.A01(this, 5);
                    return true;
                }
            case 3:
                z = false;
                C254219i r13 = this.A1S;
                Jid A0B = r4.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(A0B);
                Intent A00 = r13.A00(r4, (AbstractC14640lm) A0B, z);
                A00.setFlags(524288);
                startActivityForResult(A00, 10);
                this.A1R.A02(z, 8);
                return true;
            case 4:
                if (r4.A0B(UserJid.class) != null) {
                    r1 = new C14960mK();
                    r4 = this.A0p;
                    intent = r1.A0h(this, r4, 4);
                    startActivity(intent);
                    return true;
                }
                Log.e("group-chat-info/view-business-profile/error no-resource");
                return true;
            case 5:
                C36021jC.A01(this, 6);
                return true;
            case 6:
                AnonymousClass3FT r14 = this.A10;
                Jid A0B2 = r4.A0B(UserJid.class);
                AnonymousClass009.A05(A0B2);
                r14.A00((UserJid) A0B2, null);
                return true;
            case 7:
                AnonymousClass3FT r15 = this.A10;
                Jid A0B3 = r4.A0B(UserJid.class);
                AnonymousClass009.A05(A0B3);
                r15.A01((UserJid) A0B3, null);
                return true;
            case 8:
                intent = C14960mK.A0V(this, C15380n4.A03(r4.A0B(UserJid.class)));
                startActivity(intent);
                return true;
            default:
                return true;
        }
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        View A05;
        UserJid nullable;
        this.A00 = SystemClock.uptimeMillis();
        super.onCreate(bundle);
        A1w("on_create");
        this.A1c = getIntent().getBooleanExtra("show_chat_action", true);
        AnonymousClass3DS A00 = this.A0L.A00(((AbstractActivityC13840kQ) this).A00);
        A0c();
        setTitle(R.string.group_info);
        ((AbstractActivityC33001d7) this).A04.A01 = 3;
        this.A0a = this.A0b.A04(this, "group-chat-info");
        setContentView(R.layout.groupchat_info);
        this.A0T = (AnonymousClass2Ew) findViewById(R.id.content);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        toolbar.setTitle("");
        toolbar.A07();
        A1e(toolbar);
        A1U().A0M(true);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_back_shadow), ((AbstractActivityC33001d7) this).A08));
        this.A0E = A2e();
        View inflate = getLayoutInflater().inflate(R.layout.groupchat_info_header, (ViewGroup) this.A0E, false);
        this.A07 = inflate;
        AnonymousClass028.A0a(inflate, 2);
        this.A0E.addHeaderView(this.A07, null, false);
        this.A06 = findViewById(R.id.header);
        MediaCard mediaCard = (MediaCard) findViewById(R.id.media_card_view);
        this.A1Q = mediaCard;
        mediaCard.setTitleTextColor(AnonymousClass00T.A00(this, R.color.ui_refresh_contact_info_subtitle));
        this.A1Q.setSeeMoreColor(AnonymousClass00T.A00(this, R.color.ui_refresh_contact_info_subtitle));
        this.A0A = findViewById(R.id.participants_card);
        TextView textView = (TextView) findViewById(R.id.participants_title);
        this.A0J = textView;
        AnonymousClass028.A0l(textView, true);
        this.A0I = (TextView) findViewById(R.id.participants_info);
        this.A1U = new ExecutorC27271Gr(((ActivityC13830kP) this).A05, false);
        this.A08 = findViewById(R.id.invites_card);
        this.A0G = (TextView) findViewById(R.id.invites_info);
        GroupDetailsCard groupDetailsCard = (GroupDetailsCard) findViewById(R.id.group_details_card);
        this.A18 = groupDetailsCard;
        if (groupDetailsCard != null) {
            C14850m9 r0 = ((ActivityC13810kN) this).A0C;
            C15570nT r02 = ((ActivityC13790kL) this).A01;
            AbstractC14440lR r03 = ((ActivityC13830kP) this).A05;
            C19990v2 r04 = ((AbstractActivityC33001d7) this).A09;
            C15450nH r15 = ((ActivityC13810kN) this).A06;
            C21260x8 r14 = this.A1X;
            C21280xA r10 = this.A1Y;
            C15550nR r9 = ((AbstractActivityC33001d7) this).A06;
            AnonymousClass12G r8 = this.A1W;
            this.A0x = new GroupCallButtonController(r02, r15, r9, this.A0g, r04, ((AbstractActivityC33001d7) this).A0C, this.A0k, r0, ((AbstractActivityC33001d7) this).A0H, r03, r8, r14, r10);
            ((ActivityC001000l) this).A06.A00(groupDetailsCard);
            this.A18.setAddPersonOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 11));
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.invites_recycler_view);
        this.A0T.A05();
        View inflate2 = getLayoutInflater().inflate(R.layout.groupchat_info_footer, (ViewGroup) this.A0E, false);
        this.A04 = inflate2;
        AnonymousClass028.A0g(inflate2, new AnonymousClass04v());
        this.A0E.addFooterView(this.A04, null, false);
        this.A0D = new LinearLayout(this);
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        this.A0D.setPadding(0, 0, 0, point.y);
        AnonymousClass028.A0a(this.A0D, 2);
        this.A0E.addFooterView(this.A0D, null, false);
        A1w("fetch_group_contact");
        C15580nU A04 = C15580nU.A04(getIntent().getStringExtra("gid"));
        this.A1C = A04;
        if (A04 == null) {
            Log.e("group_info/on_create: exiting due to null gid");
            A1v("fetch_group_contact");
            A1z(7952);
            finish();
            return;
        }
        this.A0T.setRadius(-1.0f);
        this.A0o = ((AbstractActivityC33001d7) this).A06.A0B(this.A1C);
        A1v("fetch_group_contact");
        A00.A00(this.A0o);
        AnonymousClass2TT r102 = new AnonymousClass2TT(this);
        this.A08.setVisibility(8);
        this.A1B = new AnonymousClass2BP(this, r102, this.A0Y, this.A0a, ((AbstractActivityC33001d7) this).A08, this.A1M);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(1);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(this.A1B);
        this.A0y = new C36661kH(new C89474Kc(this), this);
        C74483i6 r05 = (C74483i6) new AnonymousClass02A(this).A00(C74483i6.class);
        this.A11 = r05;
        r05.A02.A05(this, new AnonymousClass02B() { // from class: X.4t9
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                boolean A1Y = C12970iu.A1Y(obj);
                groupChatInfo.A1g(A1Y);
                if (!A1Y) {
                    groupChatInfo.A1u();
                }
            }
        });
        A1w("update_group_participants");
        if (((ActivityC13810kN) this).A0C.A07(1533)) {
            StatusesViewModel statusesViewModel = (StatusesViewModel) new AnonymousClass02A(new C67323Rb(this.A0M, false), this).A00(StatusesViewModel.class);
            this.A1O = statusesViewModel;
            statusesViewModel.A00 = this;
            ((ActivityC001000l) this).A06.A00(statusesViewModel);
            this.A1O.A05.A05(this, new AnonymousClass02B() { // from class: X.4tC
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    C36661kH r1 = GroupChatInfo.this.A0y;
                    Map map = r1.A06;
                    map.clear();
                    map.putAll((Map) obj);
                    r1.notifyDataSetChanged();
                }
            });
        }
        this.A0A.setVisibility(8);
        this.A13 = (C36041jE) new AnonymousClass02A(new C67353Re(this.A0N, this.A1C, ((AbstractActivityC13840kQ) this).A00), this).A00(C36041jE.class);
        C74483i6 r1 = this.A11;
        r1.A00 = true;
        r1.A02.A0B(true);
        this.A13.A0B.A05(this, new AnonymousClass02B() { // from class: X.3QN
            /* JADX WARNING: Code restructure failed: missing block: B:35:0x0104, code lost:
                if (r7.A0X.A0G(r6) == false) goto L_0x0106;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:9:0x0046, code lost:
                if (X.C42931w8.A00(r7, r9, r11, r6, r12) != false) goto L_0x0048;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 332
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3QN.ANq(java.lang.Object):void");
            }
        });
        this.A13.A0G.A05(this, new AnonymousClass02B() { // from class: X.4tA
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                int A052 = C12960it.A05(obj);
                groupChatInfo.A3B(A052);
                TextView textView2 = groupChatInfo.A0J;
                Resources resources = groupChatInfo.getResources();
                Object[] objArr = new Object[1];
                C12960it.A1O(objArr, A052);
                textView2.setText(resources.getQuantityString(R.plurals.participants_title, A052, objArr));
            }
        });
        this.A13.A0E.A05(this, new AnonymousClass02B() { // from class: X.4tB
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                GroupChatInfo.this.A0y.A00((List) obj);
            }
        });
        C36041jE r3 = this.A13;
        r3.A0H.Ab6(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 42));
        ((C36801kb) new AnonymousClass02A(new C67313Ra(this.A0K, this.A0o), this).A00(C36801kb.class)).A05.A05(this, new AnonymousClass02B() { // from class: X.4t8
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                if (C12970iu.A1Y(obj)) {
                    groupChatInfo.Adm(new CommunitySuspendDialogFragment());
                }
            }
        });
        this.A0E.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4p0
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                GroupChatInfo.this.onListItemClicked(view);
            }
        });
        this.A0o.toString();
        A3F(((AbstractActivityC33001d7) this).A0C.A0D(this.A1C));
        A2w();
        this.A01.findViewById(R.id.add_participant_button).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 47));
        AnonymousClass31K r12 = new AnonymousClass31K();
        this.A0w = r12;
        ((InviteViaLinkView) this.A01.findViewById(R.id.invite_via_link_button)).setupOnClick(this.A1C, this, r12);
        AnonymousClass00T.A05(this, R.id.participants_search).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 35));
        this.A09 = AnonymousClass00T.A05(this, R.id.live_location_card);
        this.A0H = (TextView) AnonymousClass00T.A05(this, R.id.live_location_info);
        C20660w7 r32 = this.A1H;
        C15580nU r2 = this.A1C;
        if (this.A0Y.A0L(this.A0o, -1) || TextUtils.isEmpty(this.A0o.A0P)) {
            str = null;
        } else {
            str = "interactive";
        }
        r32.A0D(r2, str, 0);
        A3A();
        A1w("update_group_info");
        A34();
        A1v("update_group_info");
        A30();
        C15370n3 r13 = this.A0o;
        if (r13 != null && !r13.A0Y) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 4, r13.A0D));
        }
        AnonymousClass00T.A05(this, R.id.starred_messages_layout).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 36));
        AnonymousClass00T.A05(this, R.id.kept_messages_layout).setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 10));
        AnonymousClass00T.A05(this, R.id.payment_transactions_layout).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 37));
        AnonymousClass00T.A05(this, R.id.remove_group_from_community).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 38));
        AnonymousClass00T.A05(this, R.id.exit_group_btn).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 39));
        AnonymousClass00T.A05(this, R.id.report_group_btn).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 40));
        C1114659m r16 = new AbstractC116275Uu() { // from class: X.59m
            @Override // X.AbstractC116275Uu
            public final void AO9() {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                groupChatInfo.startActivity(C14960mK.A0C(groupChatInfo, groupChatInfo.A1C));
            }
        };
        MediaCard mediaCard2 = this.A1Q;
        AnonymousClass009.A03(mediaCard2);
        mediaCard2.setSeeMoreClickListener(r16);
        ImageView imageView = (ImageView) findViewById(R.id.picture);
        this.A0C = imageView;
        AnonymousClass23N.A02(imageView, R.string.action_open_image);
        if (!((AbstractActivityC33001d7) this).A0H.A0a(this.A1C)) {
            this.A0T.A0A = new ViewOnClickCListenerShape0S0200000_I0(this, 32, r102);
        }
        this.A0B = findViewById(R.id.photo_progress);
        this.A0F = (TextView) findViewById(R.id.creation_info_view);
        this.A0E.setAdapter((ListAdapter) this.A0y);
        registerForContextMenu(this.A0E);
        this.A0o.toString();
        A1w("update_group_header");
        A32();
        A3B(((Number) this.A13.A0G.A01()).intValue());
        A1v("update_group_header");
        View findViewById = findViewById(R.id.change_subject_progress);
        C15580nU r6 = this.A1C;
        C14900mE r06 = ((ActivityC13810kN) this).A05;
        C14860mA r07 = this.A1Z;
        C20660w7 r08 = this.A1H;
        C15450nH r09 = ((ActivityC13810kN) this).A06;
        AnonymousClass018 r010 = ((AbstractActivityC33001d7) this).A08;
        this.A14 = new C36191jT(findViewById, this, r06, r09, ((AbstractActivityC33001d7) this).A06, this.A0Y, ((ActivityC13810kN) this).A07, r010, this.A0i, ((AbstractActivityC33001d7) this).A0C, ((AbstractActivityC33001d7) this).A0H, r6, r08, r07);
        this.A10 = this.A0O.A00(this, null, r6);
        this.A1P = (ReadMoreTextView) findViewById(R.id.group_description);
        if (getIntent().getBooleanExtra("show_description", false)) {
            this.A1P.setLinesLimit(0);
            this.A0h.A02(this.A1C);
        }
        ReadMoreTextView readMoreTextView = this.A1P;
        readMoreTextView.setAccessibilityHelper(new AnonymousClass2eq(readMoreTextView, ((ActivityC13810kN) this).A08));
        this.A1P.A02 = new AbstractC116255Us() { // from class: X.59f
            @Override // X.AbstractC116255Us
            public final boolean AO3() {
                GroupChatInfo groupChatInfo = GroupChatInfo.this;
                groupChatInfo.A0h.A02(groupChatInfo.A1C);
                return false;
            }
        };
        this.A05 = findViewById(R.id.has_description_view);
        WaTextView waTextView = (WaTextView) findViewById(R.id.no_description_view);
        this.A0R = waTextView;
        waTextView.setText(R.string.add_group_description);
        this.A02 = findViewById(R.id.change_description_progress);
        this.A03 = findViewById(R.id.description_card);
        A31();
        if (((AbstractActivityC33001d7) this).A0C.A0G(this.A1C) || !((AbstractActivityC33001d7) this).A0H.A0a(this.A1C)) {
            this.A03.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 8));
        }
        A2z();
        A37();
        AnonymousClass00T.A05(this, R.id.live_location_card).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 41));
        this.A1E.A0W(this.A1k);
        this.A1E.A0X(this.A1l);
        A36();
        AnonymousClass00T.A05(this, R.id.group_settings_layout).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 42));
        A39();
        AnonymousClass00T.A05(this, R.id.notifications_layout).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 43));
        AnonymousClass00T.A05(this, R.id.media_visibility_layout).setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 12));
        A35();
        A2u();
        C35301hb r17 = new C35301hb(new C35491i4(this), this.A1C);
        this.A12 = r17;
        this.A0l.A03(r17);
        this.A0W.A03(this.A1f);
        this.A17.A03(this.A1j);
        this.A0S.A03(this.A1e);
        this.A0i.A03(this.A1g);
        this.A16.A03(this.A1i);
        if (!(bundle == null || (nullable = UserJid.getNullable(bundle.getString("selected_jid"))) == null)) {
            this.A0p = ((AbstractActivityC33001d7) this).A06.A0B(nullable);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            if (getIntent().getBooleanExtra("circular_transition", false)) {
                A05 = this.A06;
            } else {
                A05 = AnonymousClass00T.A05(this, R.id.picture);
            }
            A05.setTransitionName(r102.A00(R.string.transition_photo));
        }
        this.A15.A00.add(this.A1h);
        AnonymousClass31K r18 = this.A0w;
        r18.A06 = Boolean.TRUE;
        GroupDetailsCard groupDetailsCard2 = this.A18;
        if (groupDetailsCard2 != null) {
            groupDetailsCard2.A0G = r18;
        }
        if (this.A19.A00(this.A0o)) {
            Adm(new CommunitySuspendDialogFragment());
        } else if (((AbstractActivityC33001d7) this).A0H.A0X(this.A0o)) {
            Adm(CreateGroupSuspendDialog.A00(((AbstractActivityC33001d7) this).A0H.A0Y(this.A0o), ((AbstractActivityC33001d7) this).A0C.A0C(this.A1C)));
        }
        A1v("on_create");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0077, code lost:
        if (r0.A01 == 0) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a1, code lost:
        if (r2 != false) goto L_0x0098;
     */
    @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreateContextMenu(android.view.ContextMenu r9, android.view.View r10, android.view.ContextMenu.ContextMenuInfo r11) {
        /*
            r8 = this;
            super.onCreateContextMenu(r9, r10, r11)
            android.widget.AdapterView$AdapterContextMenuInfo r11 = (android.widget.AdapterView.AdapterContextMenuInfo) r11
            android.view.View r0 = r11.targetView
            java.lang.Object r0 = r0.getTag()
            X.4V1 r0 = (X.AnonymousClass4V1) r0
            X.1jM r1 = r0.A00
            boolean r0 = r1 instanceof X.AbstractC36111jL
            if (r0 == 0) goto L_0x00a0
            X.1jL r1 = (X.AbstractC36111jL) r1
            X.0n3 r6 = r1.A00
            java.lang.Class<com.whatsapp.jid.UserJid> r0 = com.whatsapp.jid.UserJid.class
            com.whatsapp.jid.Jid r2 = r6.A0B(r0)
            com.whatsapp.jid.UserJid r2 = (com.whatsapp.jid.UserJid) r2
            X.AnonymousClass009.A05(r2)
            X.1jE r0 = r8.A13
            boolean r0 = r0.A05(r2)
            if (r0 != 0) goto L_0x00a0
            X.0nY r1 = r8.A0Y
            r0 = -1
            java.lang.String r7 = r1.A0A(r6, r0)
            r1 = 2131889365(0x7f120cd5, float:1.9413392E38)
            r4 = 1
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r3 = 0
            r0[r3] = r7
            java.lang.String r0 = r8.getString(r1, r0)
            r9.add(r3, r4, r3, r0)
            X.1Pc r0 = r6.A0C
            r5 = 3
            if (r0 != 0) goto L_0x00a4
            X.0nT r0 = r8.A01
            r0.A08()
            r1 = 2
            r0 = 2131886219(0x7f12008b, float:1.940701E38)
            r9.add(r3, r1, r3, r0)
            r0 = 2131886228(0x7f120094, float:1.9407029E38)
            r9.add(r3, r5, r3, r0)
        L_0x0058:
            X.0nX r1 = r8.A0C
            X.0nU r0 = r8.A1C
            boolean r0 = r1.A0D(r0)
            if (r0 == 0) goto L_0x0098
            X.0v2 r1 = r8.A09
            X.0nU r0 = r8.A1C
            int r6 = r1.A02(r0)
            X.0nX r1 = r8.A0C
            X.0nU r0 = r8.A1C
            X.1YO r0 = r1.A01(r0, r2)
            if (r0 == 0) goto L_0x0079
            int r0 = r0.A01
            r2 = 1
            if (r0 != 0) goto L_0x007a
        L_0x0079:
            r2 = 0
        L_0x007a:
            if (r6 == r5) goto L_0x00a1
            r1 = 7
            r0 = 2131887788(0x7f1206ac, float:1.9410193E38)
            if (r2 != 0) goto L_0x0086
            r1 = 6
            r0 = 2131889215(0x7f120c3f, float:1.9413087E38)
        L_0x0086:
            r9.add(r3, r1, r3, r0)
        L_0x0089:
            r2 = 5
            r1 = 2131891281(0x7f121451, float:1.9417278E38)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r3] = r7
            java.lang.String r0 = r8.getString(r1, r0)
            r9.add(r3, r2, r3, r0)
        L_0x0098:
            r1 = 8
            r0 = 2131892684(0x7f1219cc, float:1.9420123E38)
            r9.add(r3, r1, r3, r0)
        L_0x00a0:
            return
        L_0x00a1:
            if (r2 == 0) goto L_0x0089
            goto L_0x0098
        L_0x00a4:
            boolean r0 = r6.A0J()
            if (r0 == 0) goto L_0x00b2
            r1 = 4
            r0 = 2131892749(0x7f121a0d, float:1.9420255E38)
            r9.add(r3, r1, r3, r0)
            goto L_0x0058
        L_0x00b2:
            r1 = 2131892754(0x7f121a12, float:1.9420265E38)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r3] = r7
            java.lang.String r0 = r8.getString(r1, r0)
            r9.add(r3, r3, r3, r0)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu$ContextMenuInfo):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        if (i == 1) {
            r3 = ((AbstractActivityC33001d7) this).A0O.A01(this, new C70083ai(this), getString(R.string.delete_group_dialog_title, this.A0Y.A04(this.A0o)), 2);
        } else if (i == 4) {
            C14830m7 r1 = ((ActivityC13790kL) this).A05;
            C14850m9 r12 = ((ActivityC13810kN) this).A0C;
            C14900mE r11 = ((ActivityC13810kN) this).A05;
            C252718t r10 = ((ActivityC13790kL) this).A0D;
            AbstractC15710nm r9 = ((ActivityC13810kN) this).A03;
            AnonymousClass19M r8 = ((ActivityC13810kN) this).A0B;
            C231510o r7 = this.A0s;
            AnonymousClass01d r6 = ((ActivityC13810kN) this).A08;
            AnonymousClass018 r5 = ((AbstractActivityC33001d7) this).A08;
            AnonymousClass193 r4 = this.A0t;
            C14820m6 r32 = ((ActivityC13810kN) this).A09;
            C16630pM r2 = this.A1I;
            C15610nY r13 = this.A0Y;
            C15550nR r14 = ((AbstractActivityC33001d7) this).A06;
            Jid A0B = this.A0o.A0B(C15580nU.class);
            AnonymousClass009.A05(A0B);
            C15370n3 A09 = r14.A09((AbstractC14640lm) A0B);
            AnonymousClass009.A05(A09);
            return new DialogC58332oe(this, r9, r11, r6, r1, r32, r5, new AnonymousClass5U9() { // from class: X.3Wq
                /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
                    if (r3.A0i == false) goto L_0x001d;
                 */
                @Override // X.AnonymousClass5U9
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void AZw(java.lang.String r15) {
                    /*
                        r14 = this;
                        X.1jT r9 = X.C36191jT.this
                        X.0nR r0 = r9.A04
                        X.0nU r11 = r9.A0B
                        X.0n3 r3 = r0.A0B(r11)
                        X.0nX r0 = r9.A09
                        boolean r2 = r0.A0C(r11)
                        boolean r0 = r0.A0D(r11)
                        r8 = 1
                        r7 = 0
                        if (r0 != 0) goto L_0x001d
                        boolean r1 = r3.A0i
                        r0 = 1
                        if (r1 != 0) goto L_0x001e
                    L_0x001d:
                        r0 = 0
                    L_0x001e:
                        if (r2 != 0) goto L_0x0029
                        X.0kN r1 = r9.A01
                        r0 = 2131888252(0x7f12087c, float:1.9411134E38)
                    L_0x0025:
                        r1.Ado(r0)
                    L_0x0028:
                        return
                    L_0x0029:
                        if (r0 == 0) goto L_0x0031
                        X.0kN r1 = r9.A01
                        r0 = 2131888251(0x7f12087b, float:1.9411132E38)
                        goto L_0x0025
                    L_0x0031:
                        X.0nY r0 = r9.A05
                        java.lang.String r0 = r0.A04(r3)
                        r13 = r15
                        boolean r0 = android.text.TextUtils.equals(r0, r15)
                        if (r0 != 0) goto L_0x0028
                        X.0sm r0 = r9.A06
                        boolean r0 = r0.A0B()
                        if (r0 == 0) goto L_0x00a2
                        int r2 = X.AnonymousClass2VC.A00(r15)
                        X.0nH r1 = r9.A03
                        X.0oW r0 = X.AbstractC15460nI.A2A
                        int r6 = r1.A02(r0)
                        if (r2 > r6) goto L_0x008d
                        android.view.View r0 = r9.A00
                        r0.setVisibility(r7)
                        java.lang.String r0 = "group_info/change subject:"
                        java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
                        java.lang.String r0 = X.C12960it.A0d(r15, r0)
                        com.whatsapp.util.Log.i(r0)
                        X.0w7 r1 = r9.A0C
                        X.0mA r12 = r9.A0D
                        X.0wC r10 = r9.A0A
                        X.0xE r8 = r9.A08
                        r3 = 0
                        X.1jS r7 = new X.1jS
                        r7.<init>(r8, r9, r10, r11, r12, r13)
                        X.0og r0 = r1.A01
                        boolean r0 = r0.A06
                        if (r0 == 0) goto L_0x0028
                        java.lang.String r0 = "sendmethods/sendSetGroupSubject"
                        com.whatsapp.util.Log.i(r0)
                        X.0qS r2 = r1.A06
                        r1 = 0
                        r0 = 17
                        android.os.Message r0 = android.os.Message.obtain(r3, r1, r0, r1, r7)
                        r2.A08(r0, r1)
                        return
                    L_0x008d:
                        X.0mE r5 = r9.A02
                        X.018 r4 = r9.A07
                        r3 = 2131755305(0x7f100129, float:1.9141486E38)
                        long r1 = (long) r6
                        java.lang.Object[] r0 = new java.lang.Object[r8]
                        X.C12960it.A1P(r0, r6, r7)
                        java.lang.String r0 = r4.A0I(r0, r3, r1)
                        r5.A0E(r0, r7)
                        return
                    L_0x00a2:
                        X.0mE r1 = r9.A02
                        r0 = 2131889601(0x7f120dc1, float:1.941387E38)
                        r1.A07(r0, r7)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C68763Wq.AZw(java.lang.String):void");
                }
            }, r8, r7, r4, r12, r2, r10, r13.A04(A09), 4, R.string.edit_group_subject_dialog_title, ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A2A), R.string.small_case_subject, R.string.no_emtpy_subject, 16385);
        } else if (i != 5) {
            if (i == 6) {
                C15370n3 r22 = this.A0p;
                if (r22 != null) {
                    String string = getString(R.string.remove_participant_dialog_title, this.A0Y.A04(r22), this.A0Y.A04(this.A0o));
                    r3 = new C004802e(this);
                    r3.A0A(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, string));
                    r3.A0B(true);
                    r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4fn
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            C36021jC.A00(GroupChatInfo.this, 6);
                        }
                    });
                    r3.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.3KC
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            GroupChatInfo groupChatInfo = GroupChatInfo.this;
                            C36021jC.A00(groupChatInfo, 6);
                            UserJid A05 = C15370n3.A05(groupChatInfo.A0p);
                            if (groupChatInfo.A13.A05(A05)) {
                                C20710wC r52 = ((AbstractActivityC33001d7) groupChatInfo).A0H;
                                C15580nU r33 = groupChatInfo.A1C;
                                r52.A0M(r33, Collections.singletonList(A05));
                                r52.A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(r52, 15, r33));
                            } else {
                                AnonymousClass1YO A01 = ((AbstractActivityC33001d7) groupChatInfo).A0C.A01(groupChatInfo.A1C, A05);
                                if (A01 != null && A01.A01 == 2) {
                                    Object[] A1b = C12970iu.A1b();
                                    A1b[0] = groupChatInfo.A0Y.A04(((AbstractActivityC33001d7) groupChatInfo).A06.A0B(A05));
                                    groupChatInfo.Adr(A1b, 0, R.string.error_removing_participant_406);
                                    return;
                                } else if (!((ActivityC13810kN) groupChatInfo).A07.A0B()) {
                                    ((ActivityC13810kN) groupChatInfo).A05.A04(C18640sm.A01(groupChatInfo.getApplicationContext()));
                                } else {
                                    groupChatInfo.Ady(R.string.participant_removing, R.string.register_wait_message);
                                    C14860mA r92 = groupChatInfo.A1Z;
                                    AnonymousClass32R r42 = new AnonymousClass32R(groupChatInfo.A0i, groupChatInfo, ((AbstractActivityC33001d7) groupChatInfo).A0H, groupChatInfo.A1C, r92, Collections.singletonList(A05));
                                    C20660w7 r15 = groupChatInfo.A1H;
                                    if (r15.A01.A06) {
                                        Log.i("sendmethods/sendRemoveParticipants");
                                        r15.A06.A08(Message.obtain(null, 0, 30, 0, r42), false);
                                        return;
                                    }
                                    return;
                                }
                            }
                            groupChatInfo.A2y();
                        }
                    });
                }
            } else if (i == 7) {
                int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1P);
                C14830m7 r142 = ((ActivityC13790kL) this).A05;
                C14850m9 r132 = ((ActivityC13810kN) this).A0C;
                C14900mE r122 = ((ActivityC13810kN) this).A05;
                C252718t r112 = ((ActivityC13790kL) this).A0D;
                AbstractC15710nm r102 = ((ActivityC13810kN) this).A03;
                AnonymousClass19M r92 = ((ActivityC13810kN) this).A0B;
                C231510o r82 = this.A0s;
                DialogC58332oe r15 = new DialogC58332oe(this, r102, r122, ((ActivityC13810kN) this).A08, r142, ((ActivityC13810kN) this).A09, ((AbstractActivityC33001d7) this).A08, new AnonymousClass5U9() { // from class: X.56U
                    @Override // X.AnonymousClass5U9
                    public final void AZw(String str) {
                        GroupChatInfo.this.A3C(str);
                    }
                }, r92, r82, this.A0t, r132, this.A1I, r112, this.A0o.A0G.A02, 7, R.string.edit_group_description_dialog_title, A02, R.string.description_hint, 0, 147457);
                r15.A05 = true;
                r15.A00 = A02 / 10;
                r15.A03 = getString(R.string.group_description_helper);
                return r15;
            }
            return super.onCreateDialog(i);
        } else {
            Log.w("groupchatinfo/add existing contact: activity not found, probably tablet");
            r3 = new C004802e(this);
            r3.A06(R.string.activity_not_found);
            r3.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fm
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(GroupChatInfo.this, 5);
                }
            });
        }
        return r3.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (((AbstractActivityC33001d7) this).A0C.A0D(this.A1C) && !((AbstractActivityC33001d7) this).A0H.A0Y(this.A0o) && !this.A19.A00(this.A0o)) {
            menu.add(0, 1, 0, R.string.add_paticipants).setShowAsAction(0);
        }
        if (!((AbstractActivityC33001d7) this).A0H.A0a(this.A1C) && !((AbstractActivityC33001d7) this).A0H.A0Y(this.A0o)) {
            menu.add(0, 4, 0, R.string.change_subject).setShowAsAction(0);
        }
        if (AnonymousClass3GI.A00(((AbstractActivityC33001d7) this).A0C, this.A0o, this.A19, this.A1C) && !((AbstractActivityC33001d7) this).A0H.A0Y(this.A0o)) {
            menu.add(0, 3, 0, R.string.group_settings_title).setShowAsAction(0);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass31K r2 = this.A0w;
        if (r2 != null) {
            StringBuilder sb = new StringBuilder("GroupChatInfo/WAM logging ");
            sb.append(r2.toString());
            Log.i(sb.toString());
            this.A0v.A07(this.A0w);
        }
        C16030oK r0 = this.A1E;
        r0.A0b.remove(this.A1k);
        C16030oK r02 = this.A1E;
        r02.A0c.remove(this.A1l);
        C35301hb r1 = this.A12;
        if (r1 != null) {
            this.A0l.A04(r1);
        }
        this.A0W.A04(this.A1f);
        this.A0S.A04(this.A1e);
        this.A17.A04(this.A1j);
        this.A0i.A04(this.A1g);
        this.A0a.A00();
        AnonymousClass11A r03 = this.A15;
        r03.A00.remove(this.A1h);
        this.A16.A04(this.A1i);
        GroupDetailsCard groupDetailsCard = this.A18;
        if (groupDetailsCard != null) {
            ((ActivityC001000l) this).A06.A01(groupDetailsCard);
        }
    }

    public void onListItemClicked(View view) {
        AbstractC36121jM r1 = ((AnonymousClass4V1) view.getTag()).A00;
        if (r1 instanceof AbstractC36111jL) {
            C15370n3 r3 = ((AbstractC36111jL) r1).A00;
            if (this.A13.A05((UserJid) r3.A0B(UserJid.class))) {
                A3D(Collections.singletonList(r3.A0B(UserJid.class)));
                return;
            }
            this.A0p = r3;
            view.showContextMenu();
        } else if (r1 instanceof C36151jP) {
            A3E(false);
        } else if (r1 instanceof C36141jO) {
            AnonymousClass028.A0a(this.A0T, 4);
            C004902f r2 = new C004902f(A0V());
            GroupParticipantsSearchFragment groupParticipantsSearchFragment = new GroupParticipantsSearchFragment();
            r2.A07(groupParticipantsSearchFragment, R.id.search_container);
            groupParticipantsSearchFragment.A01 = this.A0T;
            groupParticipantsSearchFragment.A00 = 1;
            groupParticipantsSearchFragment.A08 = false;
            groupParticipantsSearchFragment.A0A = false;
            r2.A0F(null);
            r2.A01();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0039, code lost:
        if (r1.A0i == false) goto L_0x003b;
     */
    @Override // X.ActivityC13810kN, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onOptionsItemSelected(android.view.MenuItem r8) {
        /*
            r7 = this;
            int r1 = r8.getItemId()
            r5 = 1
            if (r1 == r5) goto L_0x0063
            r0 = 2
            if (r1 == r0) goto L_0x005f
            r0 = 3
            if (r1 == r0) goto L_0x0051
            r6 = 4
            if (r1 == r6) goto L_0x001e
            r0 = 16908332(0x102002c, float:2.3877352E-38)
            if (r1 == r0) goto L_0x001a
            boolean r0 = super.onOptionsItemSelected(r8)
            return r0
        L_0x001a:
            X.AnonymousClass00T.A08(r7)
            return r5
        L_0x001e:
            X.1jT r4 = r7.A14
            X.0nX r3 = r4.A09
            X.0nU r2 = r4.A0B
            boolean r0 = r3.A0C(r2)
            if (r0 == 0) goto L_0x0047
            X.0nR r0 = r4.A04
            X.0n3 r1 = r0.A0B(r2)
            boolean r0 = r3.A0D(r2)
            if (r0 != 0) goto L_0x003b
            boolean r1 = r1.A0i
            r0 = 1
            if (r1 != 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            X.0kN r1 = r4.A01
            if (r0 == 0) goto L_0x004d
            r0 = 2131888251(0x7f12087b, float:1.9411132E38)
        L_0x0043:
            r1.Ado(r0)
            return r5
        L_0x0047:
            X.0kN r1 = r4.A01
            r0 = 2131892095(0x7f12177f, float:1.9418929E38)
            goto L_0x0043
        L_0x004d:
            X.C36021jC.A01(r1, r6)
            return r5
        L_0x0051:
            android.content.Context r1 = r7.getApplicationContext()
            X.0nU r0 = r7.A1C
            android.content.Intent r0 = X.C14960mK.A0I(r1, r0)
            r7.startActivity(r0)
            return r5
        L_0x005f:
            r7.A2u()
            return r5
        L_0x0063:
            r7.A2x()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.group.GroupChatInfo.onOptionsItemSelected(android.view.MenuItem):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        A38();
        if (((AbstractActivityC33001d7) this).A09.A0F(this.A1C) && !(!((ActivityC13810kN) this).A0E)) {
            startActivity(C14960mK.A02(getApplicationContext()).addFlags(603979776));
        }
        C15370n3 r1 = this.A0o;
        if (r1 != null && !r1.A0Y) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 4, r1.A0D));
        }
    }

    @Override // X.AbstractActivityC33001d7, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        C15370n3 r0 = this.A0p;
        if (r0 != null) {
            bundle.putString("selected_jid", C15380n4.A03(r0.A0D));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (((AbstractActivityC33001d7) this).A09.A03(A2u()) == 3) {
            this.A0u.A04(10, SystemClock.uptimeMillis() - this.A00);
        }
    }

    /* loaded from: classes3.dex */
    public class DescriptionConflictDialogFragment extends Hilt_GroupChatInfo_DescriptionConflictDialogFragment {
        public static DescriptionConflictDialogFragment A00(String str) {
            DescriptionConflictDialogFragment descriptionConflictDialogFragment = new DescriptionConflictDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("description", str);
            descriptionConflictDialogFragment.A0U(A0D);
            return descriptionConflictDialogFragment;
        }

        public static /* synthetic */ void A01(DescriptionConflictDialogFragment descriptionConflictDialogFragment) {
            Log.i("group_info/onclick_setDescription");
            String string = descriptionConflictDialogFragment.A03().getString("description");
            AnonymousClass009.A05(string);
            ((GroupChatInfo) descriptionConflictDialogFragment.A0B()).A3C(string);
            descriptionConflictDialogFragment.A1B();
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e r2 = new C004802e(A0C());
            r2.A06(R.string.group_error_description_conflict);
            r2.A0B(true);
            C12970iu.A1K(r2, this, 36, R.string.cancel);
            C12970iu.A1L(r2, this, 37, R.string.retry);
            return r2.create();
        }
    }
}
