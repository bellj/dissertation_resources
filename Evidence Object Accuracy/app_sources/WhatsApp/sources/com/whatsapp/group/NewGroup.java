package com.whatsapp.group;

import X.AbstractC005102i;
import X.AbstractC116455Vm;
import X.AbstractC14020ki;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AbstractC15710nm;
import X.AbstractC33121dP;
import X.AbstractC33131dQ;
import X.AbstractC58392on;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01T;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass10T;
import X.AnonymousClass10V;
import X.AnonymousClass10Z;
import X.AnonymousClass12P;
import X.AnonymousClass131;
import X.AnonymousClass150;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass1VR;
import X.AnonymousClass23N;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass367;
import X.AnonymousClass3UF;
import X.C100654mG;
import X.C103114qE;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C14950mJ;
import X.C15270mq;
import X.C15330mx;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C16630pM;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19990v2;
import X.C20710wC;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22140ya;
import X.C22670zS;
import X.C231510o;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27151Gf;
import X.C32341c0;
import X.C32921cx;
import X.C42941w9;
import X.C52752bc;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.group.NewGroup;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes2.dex */
public class NewGroup extends ActivityC13790kL implements AbstractC33121dP, AbstractC33131dQ {
    public int A00;
    public int A01;
    public Bundle A02;
    public ImageButton A03;
    public ImageView A04;
    public AnonymousClass01T A05;
    public KeyboardPopupLayout A06;
    public WaEditText A07;
    public C15550nR A08;
    public C15610nY A09;
    public AnonymousClass10T A0A;
    public AnonymousClass10V A0B;
    public AnonymousClass1J1 A0C;
    public C21270x9 A0D;
    public AnonymousClass131 A0E;
    public C19990v2 A0F;
    public C21320xE A0G;
    public C15650ng A0H;
    public C15600nX A0I;
    public AnonymousClass150 A0J;
    public C15270mq A0K;
    public C231510o A0L;
    public AnonymousClass193 A0M;
    public C16120oU A0N;
    public C20710wC A0O;
    public C15580nU A0P;
    public C17220qS A0Q;
    public C16630pM A0R;
    public AnonymousClass10Z A0S;
    public C22140ya A0T;
    public C14860mA A0U;
    public Integer A0V;
    public List A0W;
    public boolean A0X;
    public boolean A0Y;
    public final AbstractC116455Vm A0Z;
    public final C27151Gf A0a;
    public final C15370n3 A0b;
    public final AtomicReference A0c;

    public NewGroup() {
        this(0);
        this.A0c = new AtomicReference();
        this.A0b = new AnonymousClass1VR("");
        this.A0Z = new AnonymousClass3UF(this);
        this.A0a = new C32921cx(this);
    }

    public NewGroup(int i) {
        this.A0Y = false;
        A0R(new C103114qE(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0Y) {
            this.A0Y = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0F = (C19990v2) r1.A3M.get();
            this.A0N = (C16120oU) r1.ANE.get();
            this.A0U = (C14860mA) r1.ANU.get();
            this.A0L = (C231510o) r1.AHO.get();
            this.A0D = (C21270x9) r1.A4A.get();
            this.A0Q = (C17220qS) r1.ABt.get();
            this.A08 = (C15550nR) r1.A45.get();
            this.A09 = (C15610nY) r1.AMe.get();
            this.A0H = (C15650ng) r1.A4m.get();
            this.A0O = (C20710wC) r1.A8m.get();
            this.A0A = (AnonymousClass10T) r1.A47.get();
            this.A0B = (AnonymousClass10V) r1.A48.get();
            this.A0M = (AnonymousClass193) r1.A6S.get();
            this.A0S = (AnonymousClass10Z) r1.AGM.get();
            this.A0T = (C22140ya) r1.ALE.get();
            this.A0G = (C21320xE) r1.A4Y.get();
            this.A0I = (C15600nX) r1.A8x.get();
            this.A0R = (C16630pM) r1.AIc.get();
            this.A0E = (AnonymousClass131) r1.A49.get();
            this.A0J = (AnonymousClass150) r1.A63.get();
        }
    }

    public final void A2e(int i) {
        this.A00 = i;
        AbstractC58392on r1 = (AbstractC58392on) AnonymousClass00T.A05(this, R.id.group_ephemeral_duration_row_view);
        int i2 = R.color.icon_primary;
        if (i <= 0) {
            i2 = R.color.icon_secondary;
        }
        r1.setIconColor(AnonymousClass00T.A00(this, i2));
        r1.setDescription(C32341c0.A03(this, i, false, false));
    }

    public final void A2f(C15580nU r4) {
        Intent intent = new Intent();
        intent.putExtra("group_jid", r4.getRawString());
        if (this.A02 != null) {
            this.A07.A03();
            intent.putExtra("invite_bundle", this.A02);
        }
        setResult(-1, intent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        C15370n3 r4;
        if (i != 12) {
            if (i != 13) {
                super.onActivityResult(i, i2, intent);
                return;
            }
            AnonymousClass10Z r0 = this.A0S;
            r4 = this.A0b;
            r0.A02(r4).delete();
            if (i2 != -1) {
                if (i2 == 0 && intent != null) {
                    this.A0S.A03(intent, this);
                    return;
                }
                return;
            }
        } else if (i2 == -1) {
            if (intent != null) {
                if (intent.getBooleanExtra("is_reset", false)) {
                    Log.i("newgroup/resetphoto");
                    AnonymousClass10T r02 = this.A0A;
                    C15370n3 r1 = this.A0b;
                    File A00 = r02.A00(r1);
                    AnonymousClass009.A05(A00);
                    A00.delete();
                    File A01 = this.A0A.A01(r1);
                    AnonymousClass009.A05(A01);
                    A01.delete();
                    this.A04.setImageResource(R.drawable.ic_addphoto);
                    return;
                } else if (intent.getBooleanExtra("skip_cropping", false)) {
                    AnonymousClass10Z r03 = this.A0S;
                    r4 = this.A0b;
                    r03.A02(r4).delete();
                }
            }
            Log.i("newgroup/cropphoto");
            this.A0S.A05(intent, this, this, this.A0b, 13);
            return;
        } else {
            return;
        }
        Log.i("newgroup/photopicked");
        this.A04.setImageBitmap(this.A0E.A00(this, r4, 0.0f, getResources().getDimensionPixelSize(R.dimen.registration_profile_photo_size)));
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C15270mq r0 = this.A0K;
        if (r0 == null || !r0.isShowing()) {
            super.onBackPressed();
        } else {
            this.A0K.dismiss();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        Object[] objArr;
        int A06;
        super.onCreate(bundle);
        setTitle(R.string.new_group);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0N(true);
        A1U.A09(R.string.add_subject);
        setContentView(R.layout.new_group);
        this.A0C = this.A0D.A04(this, "new-group-activity");
        ImageView imageView = (ImageView) findViewById(R.id.change_photo_btn);
        this.A04 = imageView;
        imageView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 13));
        if (bundle == null) {
            this.A01 = 0;
            AnonymousClass10T r2 = this.A0A;
            C15370n3 r3 = this.A0b;
            File A00 = r2.A00(r3);
            AnonymousClass009.A05(A00);
            A00.delete();
            File A01 = this.A0A.A01(r3);
            AnonymousClass009.A05(A01);
            A01.delete();
        } else {
            this.A01 = bundle.getInt("input_method");
        }
        this.A06 = (KeyboardPopupLayout) findViewById(R.id.main);
        WaEditText waEditText = (WaEditText) findViewById(R.id.group_name);
        this.A07 = waEditText;
        waEditText.requestFocus();
        C252718t r6 = ((ActivityC13790kL) this).A0D;
        C15270mq r7 = new C15270mq(this, (ImageButton) findViewById(R.id.emoji_btn), ((ActivityC13810kN) this).A03, this.A06, this.A07, ((ActivityC13810kN) this).A08, ((ActivityC13810kN) this).A09, ((ActivityC13830kP) this).A01, ((ActivityC13810kN) this).A0B, this.A0L, this.A0M, this.A0R, r6);
        this.A0K = r7;
        r7.A0C(this.A0Z);
        C15330mx r72 = new C15330mx(this, ((ActivityC13830kP) this).A01, ((ActivityC13810kN) this).A0B, this.A0K, this.A0L, (EmojiSearchContainer) findViewById(R.id.emoji_search_container), this.A0R);
        r72.A00 = new AbstractC14020ki() { // from class: X.56i
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r32) {
                NewGroup.this.A0Z.APc(r32.A00);
            }
        };
        this.A0K.A0E = new RunnableBRunnable0Shape6S0100000_I0_6(r72, 46);
        this.A04.setImageResource(R.drawable.ic_addphoto);
        C42941w9.A0C(this.A07, ((ActivityC13830kP) this).A01);
        int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A2A);
        this.A07.setFilters(new InputFilter[]{new C100654mG(A02)});
        WaEditText waEditText2 = this.A07;
        waEditText2.addTextChangedListener(new AnonymousClass367(waEditText2, (TextView) findViewById(R.id.subject_counter_tv), ((ActivityC13810kN) this).A08, ((ActivityC13830kP) this).A01, ((ActivityC13810kN) this).A0B, this.A0R, A02, A02, false));
        this.A0X = getIntent().getBooleanExtra("create_group_for_community", false);
        if (getIntent() != null) {
            this.A0P = C15580nU.A04(getIntent().getStringExtra("parent_group_jid_to_link"));
        }
        List<AbstractC14640lm> A07 = C15380n4.A07(UserJid.class, getIntent().getStringArrayListExtra("selected"));
        this.A0W = new ArrayList(A07.size());
        if (!A07.isEmpty()) {
            for (AbstractC14640lm r4 : A07) {
                this.A0W.add(this.A08.A0B(r4));
            }
        }
        ImageButton imageButton = (ImageButton) AnonymousClass00T.A05(this, R.id.ok_btn);
        this.A03 = imageButton;
        imageButton.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(A07, 22, this));
        ((AbsListView) findViewById(R.id.selected_items)).setAdapter((ListAdapter) new C52752bc(this, this, this.A0W));
        int size = this.A0W.size();
        AtomicReference atomicReference = this.A0c;
        if (atomicReference.get() == null || (A06 = this.A0O.A06((C15580nU) atomicReference.get())) <= 0) {
            i = R.string.new_group_n_contacts_selected;
            objArr = new Object[]{Integer.valueOf(size)};
        } else {
            i = R.string.new_group_n_of_m_contacts_selected;
            objArr = new Object[]{Integer.valueOf(size), Integer.valueOf(A06)};
        }
        String string = getString(i, objArr);
        TextView textView = (TextView) findViewById(R.id.selected_header);
        textView.setText(string);
        AnonymousClass028.A0l(textView, true);
        this.A0G.A03(this.A0a);
        Integer valueOf = Integer.valueOf(getIntent().getIntExtra("entry_point", -1));
        if (valueOf.intValue() == -1) {
            valueOf = null;
        }
        this.A0V = valueOf;
        View A05 = AnonymousClass00T.A05(this, R.id.group_ephemeral_duration_row);
        A05.setVisibility(0);
        int intValue = this.A0J.A04().intValue();
        this.A00 = intValue;
        A2e(intValue);
        A05.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 48));
        AnonymousClass23N.A01(A05);
        if (this.A0P != null) {
            TextView textView2 = (TextView) AnonymousClass00T.A05(this, R.id.new_group_community_disclaimer);
            textView2.setVisibility(0);
            textView2.setText(getString(R.string.new_group_community_can_find_and_join_disclaimer, this.A09.A04(this.A08.A0B(this.A0P))));
        }
        if (this.A0X) {
            AnonymousClass00T.A05(this, R.id.new_group_community_hint).setVisibility(0);
            this.A03.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_fab_next), ((ActivityC13830kP) this).A01));
            AnonymousClass00T.A05(this, R.id.selected_header).setVisibility(8);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0G.A04(this.A0a);
        AnonymousClass1J1 r0 = this.A0C;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        int i;
        super.onSaveInstanceState(bundle);
        if (this.A0K.isShowing()) {
            i = 1;
        } else {
            i = 2;
            if (C252718t.A00(this.A06)) {
                i = 0;
            }
        }
        this.A01 = i;
        bundle.putInt("input_method", i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        int i = this.A01;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    return;
                }
            } else if (!this.A0K.isShowing()) {
                this.A06.post(new RunnableBRunnable0Shape6S0100000_I0_6(this, 48));
            }
            getWindow().setSoftInputMode(2);
            return;
        }
        getWindow().setSoftInputMode(4);
    }
}
