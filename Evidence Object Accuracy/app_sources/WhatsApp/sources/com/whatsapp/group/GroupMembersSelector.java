package com.whatsapp.group;

import X.AbstractActivityC36611kC;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass12U;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12980iv;
import X.C14960mK;
import X.C15580nU;
import X.C15600nX;
import X.C19990v2;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class GroupMembersSelector extends AbstractActivityC36611kC {
    public C19990v2 A00;
    public C15600nX A01;
    public C15580nU A02;
    public AnonymousClass12U A03;
    public boolean A04;

    public GroupMembersSelector() {
        this(0);
    }

    public GroupMembersSelector(int i) {
        this.A04 = false;
        ActivityC13830kP.A1P(this, 75);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A00 = C12980iv.A0c(A1M);
            this.A03 = ActivityC13830kP.A1N(A1M);
            this.A01 = C12980iv.A0d(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A2y(int i) {
        if (i <= 0) {
            A1U().A09(R.string.add_paticipants);
        } else {
            super.A2y(i);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        Intent A0i;
        if (i != 1) {
            if (i != 150) {
                super.onActivityResult(i, i2, intent);
                return;
            } else if (i2 != -1) {
                Log.i("groupmembersselector/permissions denied");
            } else {
                return;
            }
        } else if (i2 == -1) {
            if (intent != null) {
                C15580nU A0U = ActivityC13790kL.A0U(intent, "group_jid");
                Bundle bundleExtra = intent.getBundleExtra("invite_bundle");
                Log.i(C12960it.A0b("groupmembersselector/group created ", A0U));
                if (this.A00.A0D(A0U) && !AJN()) {
                    Log.i(C12960it.A0b("groupmembersselector/opening conversation", A0U));
                    if (this.A02 != null) {
                        A0i = new C14960mK().A0j(this, A0U);
                    } else {
                        A0i = new C14960mK().A0i(this, A0U);
                    }
                    if (bundleExtra != null) {
                        A0i.putExtra("invite_bundle", bundleExtra);
                    }
                    ((ActivityC13790kL) this).A00.A07(this, A0i);
                }
            }
            startActivity(C14960mK.A02(this));
        } else {
            return;
        }
        finish();
    }

    @Override // X.AbstractActivityC36611kC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent() != null) {
            this.A02 = C15580nU.A04(getIntent().getStringExtra("parent_group_jid_to_link"));
        }
        if (bundle == null && !((AbstractActivityC36611kC) this).A0I.A00()) {
            RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_new_group_request, R.string.permission_contacts_access_on_new_group);
        }
    }
}
