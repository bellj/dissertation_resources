package com.whatsapp.group;

import X.AbstractActivityC36611kC;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass1YO;
import X.AnonymousClass2FL;
import X.C12980iv;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class EditGroupAdminsSelector extends AbstractActivityC36611kC {
    public C15600nX A00;
    public boolean A01;

    public EditGroupAdminsSelector() {
        this(0);
    }

    public EditGroupAdminsSelector(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 72);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0O(A1M, this, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            ActivityC13770kJ.A0N(A1M, this);
            this.A00 = C12980iv.A0d(A1M);
        }
    }

    @Override // X.AbstractActivityC36611kC
    public void A35(ArrayList arrayList) {
        String stringExtra = getIntent().getStringExtra("gid");
        AnonymousClass009.A05(stringExtra);
        C15580nU A04 = C15580nU.A04(stringExtra);
        if (A04 != null) {
            Iterator it = this.A00.A02(A04).A07().iterator();
            while (it.hasNext()) {
                AnonymousClass1YO r1 = (AnonymousClass1YO) it.next();
                C15570nT r0 = ((ActivityC13790kL) this).A01;
                UserJid userJid = r1.A03;
                if (!r0.A0F(userJid) && r1.A01 != 2) {
                    arrayList.add(((AbstractActivityC36611kC) this).A0J.A0B(userJid));
                }
            }
        }
    }
}
