package com.whatsapp.group;

import X.AbstractC14440lR;
import X.AbstractC47972Dm;
import X.AnonymousClass03H;
import X.AnonymousClass12G;
import X.AnonymousClass1SF;
import X.AnonymousClass1YS;
import X.AnonymousClass1YT;
import X.AnonymousClass1s3;
import X.AnonymousClass37R;
import X.AnonymousClass4AU;
import X.AnonymousClass5V9;
import X.AnonymousClass5VA;
import X.C14850m9;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C18750sx;
import X.C19990v2;
import X.C20710wC;
import X.C21260x8;
import X.C21280xA;
import X.C236712o;
import X.C236812p;
import X.C40681rz;
import X.C70903c2;
import X.C864547j;
import com.whatsapp.R;
import com.whatsapp.group.GroupCallButtonController;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* loaded from: classes2.dex */
public class GroupCallButtonController implements AnonymousClass03H {
    public C15370n3 A00;
    public AnonymousClass4AU A01 = AnonymousClass4AU.NONE;
    public AnonymousClass1s3 A02;
    public C15580nU A03;
    public AnonymousClass1YT A04;
    public C864547j A05;
    public AnonymousClass37R A06;
    public AnonymousClass1YS A07;
    public final C15570nT A08;
    public final C15450nH A09;
    public final C15550nR A0A;
    public final C18750sx A0B;
    public final C19990v2 A0C;
    public final C15600nX A0D;
    public final C236812p A0E;
    public final C14850m9 A0F;
    public final C20710wC A0G;
    public final AbstractC14440lR A0H;
    public final AbstractC47972Dm A0I = new C70903c2(this);
    public final AnonymousClass12G A0J;
    public final C236712o A0K = new C40681rz(this);
    public final C21260x8 A0L;
    public final AnonymousClass5V9 A0M = new AnonymousClass5V9() { // from class: X.5Ay
        @Override // X.AnonymousClass5V9
        public final void ANW(AnonymousClass1YT r2) {
            GroupCallButtonController.this.A04 = r2;
        }
    };
    public final AnonymousClass5VA A0N = new AnonymousClass5VA() { // from class: X.3c6
        @Override // X.AnonymousClass5VA
        public final void ARe(AnonymousClass1YS r4) {
            GroupCallButtonController groupCallButtonController = GroupCallButtonController.this;
            StringBuilder A0k = C12960it.A0k("GroupCallButtonController/fetchJoinableCallLogCallback groupJid: ");
            A0k.append(groupCallButtonController.A03);
            C12960it.A1F(A0k);
            if (!C29941Vi.A00(r4, groupCallButtonController.A07)) {
                groupCallButtonController.A07 = r4;
                if (r4 != null) {
                    groupCallButtonController.A05(r4.A00);
                }
            }
            AnonymousClass1s3 r0 = groupCallButtonController.A02;
            if (r0 != null) {
                r0.A00.A02();
            }
        }
    };
    public final C21280xA A0O;

    public GroupCallButtonController(C15570nT r2, C15450nH r3, C15550nR r4, C18750sx r5, C19990v2 r6, C15600nX r7, C236812p r8, C14850m9 r9, C20710wC r10, AbstractC14440lR r11, AnonymousClass12G r12, C21260x8 r13, C21280xA r14) {
        this.A0F = r9;
        this.A08 = r2;
        this.A0H = r11;
        this.A0C = r6;
        this.A09 = r3;
        this.A0L = r13;
        this.A0O = r14;
        this.A0A = r4;
        this.A0J = r12;
        this.A0G = r10;
        this.A0B = r5;
        this.A0E = r8;
        this.A0D = r7;
    }

    public int A00() {
        CallInfo callInfo = Voip.getCallInfo();
        C15580nU r1 = this.A03;
        if (r1 == null || callInfo == null || !r1.equals(callInfo.groupJid)) {
            return R.string.voip_joinable_join;
        }
        return R.string.voip_return;
    }

    public AnonymousClass4AU A01() {
        return this.A01;
    }

    public void A02() {
        AnonymousClass4AU r0;
        C15370n3 r02 = this.A00;
        if (r02 == null) {
            r0 = AnonymousClass4AU.NONE;
        } else {
            C15580nU r1 = this.A03;
            C19990v2 r4 = this.A0C;
            if (r1 != null && !r02.A0Y && r4.A02(r1) != 3) {
                if (AnonymousClass1SF.A0P(this.A0F)) {
                    C236812p r3 = this.A0E;
                    if (r3.A07(this.A03)) {
                        AnonymousClass1YS A02 = r3.A02(this.A03);
                        this.A07 = A02;
                        if (A02 != null) {
                            A05(A02.A00);
                        }
                    } else {
                        AnonymousClass37R r2 = new AnonymousClass37R(r3, this.A03, this.A0N);
                        this.A06 = r2;
                        this.A0H.Ab5(r2, new Void[0]);
                    }
                }
                if (this.A07 != null) {
                    r0 = AnonymousClass4AU.JOIN_CALL;
                } else {
                    C15580nU r32 = this.A03;
                    C15450nH r22 = this.A09;
                    C15600nX r12 = this.A0D;
                    if (AnonymousClass1SF.A0J(r22, r4, r12, this.A00, r32)) {
                        r0 = AnonymousClass4AU.ONE_TAP;
                    } else if (r12.A0C(this.A03)) {
                        r0 = AnonymousClass4AU.CALL_PICKER;
                    } else {
                        return;
                    }
                }
            } else {
                return;
            }
        }
        this.A01 = r0;
    }

    public void A03() {
        this.A0J.A03(this.A0I);
        this.A0L.A03(this.A0K);
    }

    public void A04() {
        this.A0J.A04(this.A0I);
        this.A0L.A04(this.A0K);
        AnonymousClass37R r0 = this.A06;
        if (r0 != null) {
            r0.A03(true);
            this.A06 = null;
        }
        C864547j r02 = this.A05;
        if (r02 != null) {
            r02.A03(true);
            this.A05 = null;
        }
        this.A00 = null;
        this.A03 = null;
        this.A01 = AnonymousClass4AU.NONE;
        this.A04 = null;
        this.A02 = null;
    }

    public final void A05(long j) {
        C18750sx r1 = this.A0B;
        AnonymousClass1YT A01 = r1.A01(j);
        if (A01 != null) {
            this.A04 = A01;
        } else if (this.A05 == null) {
            C864547j r2 = new C864547j(r1, this.A0M, j);
            this.A05 = r2;
            this.A0H.Ab5(r2, new Void[0]);
        }
    }

    public boolean A06() {
        if (this.A00 == null) {
            return false;
        }
        return AnonymousClass1SF.A0Q(this.A03, Voip.getCallInfo());
    }

    public boolean A07() {
        C15370n3 r4 = this.A00;
        if (r4 == null) {
            return false;
        }
        C15580nU r6 = this.A03;
        C20710wC r5 = this.A0G;
        return AnonymousClass1SF.A0I(this.A08, this.A09, this.A0A, this.A0D, r4, r5, r6);
    }
}
