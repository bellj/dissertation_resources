package com.whatsapp.group;

import X.AbstractC28491Nn;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13770kJ;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass04D;
import X.AnonymousClass2Ew;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.C102274os;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C14850m9;
import X.C252718t;
import X.C27531Hw;
import X.C36041jE;
import X.C36081jI;
import X.C36661kH;
import X.C48232Fc;
import X.C66803Oz;
import X.C73133fj;
import X.C83243wv;
import X.C83383x9;
import X.C89474Kc;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.status.viewmodels.StatusesViewModel;

/* loaded from: classes2.dex */
public class GroupParticipantsSearchFragment extends Hilt_GroupParticipantsSearchFragment {
    public int A00 = 0;
    public AnonymousClass2Ew A01;
    public AnonymousClass01d A02;
    public AnonymousClass018 A03;
    public C14850m9 A04;
    public C36661kH A05;
    public C36041jE A06;
    public C252718t A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;

    @Override // X.AnonymousClass01E
    public void A0m(Bundle bundle) {
        C36081jI r7;
        View view;
        String string;
        super.A0m(bundle);
        if (bundle != null) {
            this.A00 = bundle.getInt("search_view_startup_mode");
            this.A08 = bundle.getBoolean("enter_animated");
            this.A0A = bundle.getBoolean("exit_animated");
            this.A09 = bundle.getBoolean("enter_ime");
        }
        GroupChatInfo groupChatInfo = (GroupChatInfo) A0C();
        View A05 = A05();
        ListView listView = (ListView) AnonymousClass028.A0D(A05, 16908298);
        if (this.A05 == null) {
            this.A05 = new C36661kH(new C89474Kc(groupChatInfo), groupChatInfo);
        }
        C36041jE r4 = (C36041jE) C13000ix.A02(groupChatInfo).A00(C36041jE.class);
        this.A06 = r4;
        int i = this.A00;
        if (i == 0) {
            r7 = r4.A0C;
        } else if (i == 1) {
            r7 = r4.A0D;
        } else {
            throw new AssertionError("Unreachable");
        }
        C12970iu.A1P(A0G(), r7, this.A05, 44);
        if (this.A04.A07(1533)) {
            StatusesViewModel statusesViewModel = (StatusesViewModel) C13000ix.A02(A0C()).A00(StatusesViewModel.class);
            this.A0K.A00(statusesViewModel);
            C12970iu.A1P(A0G(), statusesViewModel.A05, this, 45);
        }
        groupChatInfo.registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4p1
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view2, int i2, long j) {
                GroupChatInfo.this.onListItemClicked(view2);
            }
        });
        listView.setOnScrollListener(new C102274os(this));
        View findViewById = A05.findViewById(R.id.search_holder);
        C48232Fc.A00(findViewById);
        SearchView searchView = (SearchView) findViewById.findViewById(R.id.search_view);
        C12960it.A0s(A0p(), C12960it.A0J(searchView, R.id.search_src_text), R.color.search_text_color);
        searchView.setIconifiedByDefault(false);
        if (this.A08) {
            view = A1A();
        } else {
            view = null;
        }
        if (view != null) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (view.getTop() - listView.getPaddingTop()), 0.0f);
            translateAnimation.setDuration(240);
            translateAnimation.setAnimationListener(new C83383x9(searchView, this));
            listView.startAnimation(translateAnimation);
        } else if (this.A09) {
            searchView.setIconified(false);
            this.A09 = false;
        } else {
            this.A07.A01(searchView);
        }
        searchView.setQueryHint(A0I(R.string.search_hint));
        searchView.A0B = new C66803Oz(this);
        C12970iu.A0L(searchView, R.id.search_mag_icon).setImageDrawable(new C73133fj(AnonymousClass00T.A04(A0p(), R.drawable.ic_back), this));
        if (this.A08) {
            TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation2.setDuration(240);
            findViewById.startAnimation(translateAnimation2);
        }
        this.A08 = false;
        ImageView A0L = C12970iu.A0L(findViewById, R.id.search_back);
        A0L.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A04(A02().getDrawable(R.drawable.ic_back), A02().getColor(R.color.lightActionBarItemDrawableTint)), this.A03));
        AbstractView$OnClickListenerC34281fs.A01(A0L, this, 24);
        Context A01 = A01();
        if (this.A00 == 1 && (string = A01.getString(R.string.past_participants_title)) != null) {
            View inflate = View.inflate(A0p(), R.layout.groupchat_info_search_list_header, null);
            TextView A0I = C12960it.A0I(inflate, R.id.text);
            C27531Hw.A06(A0I);
            A0I.setText(string);
            listView.addHeaderView(inflate, null, false);
        }
        C36041jE r9 = this.A06;
        Context A012 = A01();
        if (this.A00 == 1) {
            Resources resources = A012.getResources();
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 60, 0);
            SpannableString A013 = r9.A0F.A01(A012, resources.getQuantityString(R.plurals.past_participants_footer, 60, objArr), new Runnable[]{new RunnableBRunnable0Shape0S0000000_I0(8)}, new String[]{"learn-more"}, new String[]{"https://faq.whatsapp.com/android/chats/how-to-exit-and-delete-groups"});
            View inflate2 = View.inflate(A0p(), R.layout.groupchat_info_search_list_footer, null);
            AbstractC28491Nn.A05(C12970iu.A0T(inflate2, R.id.text), this.A02, A013);
            listView.addFooterView(inflate2, null, false);
        }
        if (this.A00 == 0) {
            C36041jE r42 = this.A06;
            if (r42.A06.A02(r42.A09) == 3) {
                C36041jE r43 = this.A06;
                if (!r43.A07.A0D(r43.A09)) {
                    View inflate3 = View.inflate(A0p(), R.layout.groupchat_info_search_list_footer, null);
                    TextEmojiLabel A0T = C12970iu.A0T(inflate3, R.id.text);
                    AbstractC28491Nn.A04(A0T, this.A02);
                    AbstractC28491Nn.A03(A0T);
                    A0T.setText(R.string.announcement_members_disclaimer);
                    AnonymousClass04D.A08(A0T, R.style.announcementMembersDisclaimer);
                    listView.addFooterView(inflate3, null, false);
                }
            }
        }
        listView.setAdapter((ListAdapter) this.A05);
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putInt("search_view_startup_mode", this.A00);
        bundle.putBoolean("enter_animated", this.A08);
        bundle.putBoolean("exit_animated", this.A0A);
        bundle.putBoolean("enter_ime", this.A09);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.group_participants_search_fragment);
    }

    public final View A1A() {
        ActivityC13770kJ r5 = (ActivityC13770kJ) A0B();
        View view = null;
        if (r5 != null) {
            int childCount = r5.A2e().getChildCount();
            for (int i = 0; i < childCount && view == null; i++) {
                View childAt = r5.A2e().getChildAt(i);
                if (childAt.getTag() != null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    public void A1B() {
        View view;
        View view2 = super.A0A;
        if (view2 != null) {
            boolean A1V = C12960it.A1V(A0F().A03(), 1);
            if (this.A0A) {
                view = A1A();
            } else {
                view = null;
            }
            View findViewById = view2.findViewById(R.id.search_holder);
            findViewById.setVisibility(8);
            this.A07.A01(AnonymousClass028.A0D(findViewById, R.id.search_view));
            if (view != null) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration(240);
                findViewById.startAnimation(alphaAnimation);
                View findViewById2 = view2.findViewById(16908298);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (view.getTop() - findViewById2.getPaddingTop()));
                translateAnimation.setDuration(240);
                translateAnimation.setAnimationListener(new C83243wv(this));
                findViewById2.startAnimation(translateAnimation);
            } else {
                A0F().A0n();
            }
            AnonymousClass2Ew r0 = this.A01;
            if (r0 != null && A1V) {
                AnonymousClass028.A0a(r0, 1);
            }
        }
    }
}
