package com.whatsapp.group;

import X.AbstractC116245Ur;
import X.AbstractC13950kb;
import X.AbstractC14440lR;
import X.AbstractC253919f;
import X.AbstractC469028d;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass11F;
import X.AnonymousClass146;
import X.AnonymousClass19M;
import X.AnonymousClass1AD;
import X.AnonymousClass1BP;
import X.AnonymousClass1BR;
import X.AnonymousClass1KT;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass38P;
import X.AnonymousClass3UJ;
import X.AnonymousClass51L;
import X.C105624uM;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C14820m6;
import X.C14850m9;
import X.C15260mp;
import X.C15320mw;
import X.C15330mx;
import X.C16120oU;
import X.C22210yi;
import X.C231510o;
import X.C235512c;
import X.C252718t;
import X.C253719d;
import X.C54252gQ;
import X.C69883aO;
import X.C69893aP;
import X.C71753dS;
import X.C74473i5;
import X.C74493i9;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.gifsearch.GifSearchContainer;
import com.whatsapp.group.GroupProfileEmojiEditor;
import com.whatsapp.picker.search.PickerSearchDialogFragment;
import java.util.Map;

/* loaded from: classes2.dex */
public class GroupProfileEmojiEditor extends ActivityC13790kL implements AbstractC13950kb {
    public static final Map A0D = new C71753dS();
    public Bitmap A00;
    public View A01;
    public ImageView A02;
    public KeyboardPopupLayout A03;
    public AnonymousClass1AD A04;
    public AnonymousClass11F A05;
    public C74473i5 A06;
    public AnonymousClass1BR A07;
    public C69893aP A08;
    public C22210yi A09;
    public AnonymousClass146 A0A;
    public C235512c A0B;
    public boolean A0C;

    public GroupProfileEmojiEditor() {
        this(0);
    }

    public GroupProfileEmojiEditor(int i) {
        this.A0C = false;
        ActivityC13830kP.A1P(this, 76);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A09 = (C22210yi) A1M.AGm.get();
            this.A0A = (AnonymousClass146) A1M.AKM.get();
            this.A0B = (C235512c) A1M.AKS.get();
            this.A04 = (AnonymousClass1AD) A1M.A54.get();
            this.A05 = (AnonymousClass11F) A1M.AE3.get();
            this.A07 = (AnonymousClass1BR) A1M.A8z.get();
        }
    }

    @Override // X.AbstractC13950kb
    public void ATj(PickerSearchDialogFragment pickerSearchDialogFragment) {
        this.A08.A01(pickerSearchDialogFragment);
    }

    @Override // X.AbstractC13950kb
    public void Adk(DialogFragment dialogFragment) {
        Adm(dialogFragment);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!this.A07.A03.A02()) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.group_profile_emoji_editor);
        int[] intArray = getResources().getIntArray(R.array.group_editor_background_colors);
        int[] intArray2 = getResources().getIntArray(R.array.group_editor_background_color_rings);
        AbstractC469028d r5 = (AbstractC469028d) C12990iw.A0l(A0D, getIntent().getIntExtra("emojiEditorProfileTarget", 1));
        if (r5 == null) {
            r5 = AnonymousClass51L.A00;
        }
        this.A06 = (C74473i5) new AnonymousClass02A(new C105624uM(this, intArray), this).A00(C74473i5.class);
        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) findViewById(R.id.popup_keyboard_root);
        this.A03 = keyboardPopupLayout;
        keyboardPopupLayout.setKeyboardPopupBackgroundColor(AnonymousClass00T.A00(this, R.color.emoji_popup_body));
        C74493i9 r4 = (C74493i9) C13000ix.A02(this).A00(C74493i9.class);
        C235512c r10 = this.A0B;
        AbstractC14440lR r3 = ((ActivityC13830kP) this).A05;
        AnonymousClass1KT r11 = new AnonymousClass1KT(((ActivityC13810kN) this).A09, this.A09, this.A0A, r10, r3);
        C69893aP r32 = new C69893aP(r11);
        this.A08 = r32;
        AnonymousClass1BR r2 = this.A07;
        KeyboardPopupLayout keyboardPopupLayout2 = this.A03;
        AnonymousClass1AD r0 = this.A04;
        r2.A04 = r4;
        r2.A06 = r11;
        r2.A05 = r32;
        r2.A01 = r0;
        AnonymousClass1BP r112 = r2.A0E;
        r112.A00 = this;
        AnonymousClass1AD r13 = r2.A01;
        r112.A07 = r13.A01(r2.A0J, r2.A06);
        r112.A05 = r13.A00();
        r112.A02 = keyboardPopupLayout2;
        r112.A01 = null;
        r112.A03 = (WaEditText) AnonymousClass00T.A05(this, R.id.keyboardInput);
        r2.A02 = r112.A00();
        Resources resources = getResources();
        AnonymousClass3UJ r02 = new AnonymousClass3UJ(resources, r2);
        r2.A00 = r02;
        C15260mp r102 = r2.A02;
        r102.A0C(r02);
        C69883aO r03 = new AbstractC116245Ur(resources, this, r2, r32) { // from class: X.3aO
            public final /* synthetic */ Resources A00;
            public final /* synthetic */ GroupProfileEmojiEditor A01;
            public final /* synthetic */ AnonymousClass1BR A02;
            public final /* synthetic */ C69893aP A03;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
                this.A03 = r4;
            }

            @Override // X.AbstractC116245Ur
            public final void AWl(AnonymousClass1KS r113, Integer num, int i) {
                AnonymousClass1BR r33 = this.A02;
                r33.A0I.A05(null, new AnonymousClass1qI(this.A01, r113, 
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001e: INVOKE  
                      (wrap: X.1AB : 0x0008: IGET  (r1v0 X.1AB A[REMOVE]) = (r3v0 'r33' X.1BR) X.1BR.A0I X.1AB)
                      (null X.0mE)
                      (wrap: X.1qI : 0x001a: CONSTRUCTOR  (r3v1 X.1qI A[REMOVE]) = 
                      (wrap: com.whatsapp.group.GroupProfileEmojiEditor : 0x0002: IGET  (r4v0 com.whatsapp.group.GroupProfileEmojiEditor A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A01 com.whatsapp.group.GroupProfileEmojiEditor)
                      (r11v0 'r113' X.1KS)
                      (wrap: X.3a8 : 0x000c: CONSTRUCTOR  (r6v0 X.3a8 A[REMOVE]) = 
                      (wrap: android.content.res.Resources : 0x0004: IGET  (r2v0 android.content.res.Resources A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A00 android.content.res.Resources)
                      (r3v0 'r33' X.1BR)
                      (wrap: X.3aP : 0x0006: IGET  (r0v0 X.3aP A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A03 X.3aP)
                     call: X.3a8.<init>(android.content.res.Resources, X.1BR, X.3aP):void type: CONSTRUCTOR)
                      (wrap: java.lang.String : 0x0012: INVOKE  (r7v0 java.lang.String A[REMOVE]) = (r11v0 'r113' X.1KS), (640 int), (640 int) type: STATIC call: X.1AB.A00(X.1KS, int, int):java.lang.String)
                      (640 int)
                      (640 int)
                     call: X.1qI.<init>(android.content.Context, X.1KS, X.5Up, java.lang.String, int, int):void type: CONSTRUCTOR)
                      (null java.util.Map)
                     type: VIRTUAL call: X.1AB.A05(X.0mE, X.1qE, java.util.Map):void in method: X.3aO.AWl(X.1KS, java.lang.Integer, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: CONSTRUCTOR  (r3v1 X.1qI A[REMOVE]) = 
                      (wrap: com.whatsapp.group.GroupProfileEmojiEditor : 0x0002: IGET  (r4v0 com.whatsapp.group.GroupProfileEmojiEditor A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A01 com.whatsapp.group.GroupProfileEmojiEditor)
                      (r11v0 'r113' X.1KS)
                      (wrap: X.3a8 : 0x000c: CONSTRUCTOR  (r6v0 X.3a8 A[REMOVE]) = 
                      (wrap: android.content.res.Resources : 0x0004: IGET  (r2v0 android.content.res.Resources A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A00 android.content.res.Resources)
                      (r3v0 'r33' X.1BR)
                      (wrap: X.3aP : 0x0006: IGET  (r0v0 X.3aP A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A03 X.3aP)
                     call: X.3a8.<init>(android.content.res.Resources, X.1BR, X.3aP):void type: CONSTRUCTOR)
                      (wrap: java.lang.String : 0x0012: INVOKE  (r7v0 java.lang.String A[REMOVE]) = (r11v0 'r113' X.1KS), (640 int), (640 int) type: STATIC call: X.1AB.A00(X.1KS, int, int):java.lang.String)
                      (640 int)
                      (640 int)
                     call: X.1qI.<init>(android.content.Context, X.1KS, X.5Up, java.lang.String, int, int):void type: CONSTRUCTOR in method: X.3aO.AWl(X.1KS, java.lang.Integer, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000c: CONSTRUCTOR  (r6v0 X.3a8 A[REMOVE]) = 
                      (wrap: android.content.res.Resources : 0x0004: IGET  (r2v0 android.content.res.Resources A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A00 android.content.res.Resources)
                      (r3v0 'r33' X.1BR)
                      (wrap: X.3aP : 0x0006: IGET  (r0v0 X.3aP A[REMOVE]) = (r10v0 'this' X.3aO A[IMMUTABLE_TYPE, THIS]) X.3aO.A03 X.3aP)
                     call: X.3a8.<init>(android.content.res.Resources, X.1BR, X.3aP):void type: CONSTRUCTOR in method: X.3aO.AWl(X.1KS, java.lang.Integer, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3a8, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 27 more
                    */
                /*
                    this = this;
                    X.1BR r3 = r10.A02
                    com.whatsapp.group.GroupProfileEmojiEditor r4 = r10.A01
                    android.content.res.Resources r2 = r10.A00
                    X.3aP r0 = r10.A03
                    X.1AB r1 = r3.A0I
                    X.3a8 r6 = new X.3a8
                    r6.<init>(r2, r3, r0)
                    r8 = 640(0x280, float:8.97E-43)
                    r5 = r11
                    java.lang.String r7 = X.AnonymousClass1AB.A00(r11, r8, r8)
                    r9 = 640(0x280, float:8.97E-43)
                    X.1qI r3 = new X.1qI
                    r3.<init>(r4, r5, r6, r7, r8, r9)
                    r0 = 0
                    r1.A05(r0, r3, r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C69883aO.AWl(X.1KS, java.lang.Integer, int):void");
            }
        };
        r102.A0K(r03);
        r32.A04 = r03;
        C14850m9 r04 = r2.A0C;
        C253719d r05 = r2.A0F;
        C252718t r06 = r2.A0K;
        C16120oU r07 = r2.A0D;
        AnonymousClass01d r08 = r2.A07;
        AbstractC253919f r09 = r2.A0G;
        C14820m6 r010 = r2.A08;
        EmojiSearchContainer emojiSearchContainer = (EmojiSearchContainer) keyboardPopupLayout2.findViewById(R.id.emoji_search_container);
        C15260mp r14 = r2.A02;
        AnonymousClass19M r12 = r2.A0A;
        C231510o r113 = r2.A0B;
        C15320mw r011 = new C15320mw(this, r08, r010, r2.A09, r12, r113, emojiSearchContainer, r04, r07, r14, r05, (GifSearchContainer) keyboardPopupLayout2.findViewById(R.id.gif_search_container), r09, r2.A0H, r06);
        r2.A03 = r011;
        ((C15330mx) r011).A00 = r2;
        C15260mp r012 = r2.A02;
        r32.A02 = this;
        r32.A00 = r012;
        r012.A03 = r32;
        AnonymousClass1KT r013 = r2.A06;
        r013.A0B.A03(r013.A0A);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.icon_secondary), ((ActivityC13830kP) this).A01));
        A1e(toolbar);
        C12970iu.A0N(this).A0A(R.string.group_photo_editor_emoji_title);
        A1U().A0P(true);
        A1U().A0M(true);
        RecyclerView recyclerView = (RecyclerView) AnonymousClass00T.A05(this, R.id.colors_recycler);
        recyclerView.setAdapter(new C54252gQ(this, this.A06, intArray, intArray2));
        recyclerView.setLayoutManager(new LinearLayoutManager(0));
        this.A02 = (ImageView) AnonymousClass00T.A05(this, R.id.picturePreview);
        this.A06.A00.A05(this, new AnonymousClass02B(r5, this) { // from class: X.3RJ
            public final /* synthetic */ AbstractC469028d A00;
            public final /* synthetic */ GroupProfileEmojiEditor A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Drawable r1;
                GroupProfileEmojiEditor groupProfileEmojiEditor = this.A01;
                AbstractC469028d r33 = this.A00;
                AnonymousClass11F r15 = groupProfileEmojiEditor.A05;
                ColorDrawable colorDrawable = new ColorDrawable(((Number) obj).intValue());
                if (r15.A00.A07(1257)) {
                    r1 = new AnonymousClass2ZZ(colorDrawable, r33);
                } else {
                    r1 = new C51842Za(colorDrawable, r33);
                }
                groupProfileEmojiEditor.A02.setBackground(r1);
            }
        });
        C12960it.A18(this, r4.A00, 78);
        this.A01 = LayoutInflater.from(this).inflate(R.layout.group_profile_emoji_editor_picture_preview, (ViewGroup) ((ActivityC13810kN) this).A00, false);
        C12980iv.A1H(this.A03.getViewTreeObserver(), this, 6);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.done, 0, R.string.done).setIcon(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.action_profile_photo_editor_done, R.color.icon_secondary), ((ActivityC13830kP) this).A01)).setShowAsAction(2);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1BR r2 = this.A07;
        C15260mp r0 = r2.A02;
        r0.A0C(null);
        r0.A0K(null);
        r2.A05.A04 = null;
        ((C15330mx) r2.A03).A00 = null;
        r2.A06.A03();
        r2.A05.A00();
        r2.A02.dismiss();
        r2.A02.A0G();
        r2.A06 = null;
        r2.A05 = null;
        r2.A03 = null;
        r2.A00 = null;
        r2.A01 = null;
        r2.A02 = null;
        r2.A04 = null;
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.done) {
            C12960it.A1E(new AnonymousClass38P(this), ((ActivityC13830kP) this).A05);
            return true;
        } else if (menuItem.getItemId() != 16908332) {
            return true;
        } else {
            finish();
            return true;
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.done).setVisible(C12960it.A1W(this.A00));
        return true;
    }
}
