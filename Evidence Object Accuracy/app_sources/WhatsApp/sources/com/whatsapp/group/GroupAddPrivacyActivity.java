package com.whatsapp.group;

import X.AbstractActivityC58522pc;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass5UK;
import X.C12960it;
import X.C12970iu;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;

/* loaded from: classes2.dex */
public class GroupAddPrivacyActivity extends AbstractActivityC58522pc implements AnonymousClass5UK {
    public int A00;
    public boolean A01;
    public boolean A02;

    public GroupAddPrivacyActivity() {
        this(0);
        this.A02 = false;
    }

    public GroupAddPrivacyActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 74);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.AnonymousClass5UK
    public void A7f() {
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("groupadd", this.A00);
        C12960it.A0q(this, A0A);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            this.A00 = 3;
            Intent A0A = C12970iu.A0A();
            A0A.putExtra("groupadd", this.A00);
            C12960it.A0q(this, A0A);
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.AbstractActivityC58522pc, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        int i = ((ActivityC13810kN) this).A09.A00.getInt("privacy_groupadd", 0);
        this.A00 = i;
        int i2 = 0;
        this.A02 = C12960it.A1V(i, 2);
        ((AbstractActivityC58522pc) this).A03.setEnabled(false);
        boolean z = this.A02;
        RadioButton radioButton = ((AbstractActivityC58522pc) this).A03;
        if (!z) {
            i2 = 8;
        }
        radioButton.setVisibility(i2);
    }
}
