package com.whatsapp.group;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass02A;
import X.AnonymousClass11A;
import X.AnonymousClass1JO;
import X.AnonymousClass1YO;
import X.AnonymousClass2FL;
import X.AnonymousClass3ZC;
import X.AnonymousClass5UJ;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14830m7;
import X.C14850m9;
import X.C14860mA;
import X.C14900mE;
import X.C15370n3;
import X.C15380n4;
import X.C15450nH;
import X.C15550nR;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C18640sm;
import X.C19990v2;
import X.C20660w7;
import X.C20710wC;
import X.C21320xE;
import X.C22140ya;
import X.C36371jm;
import X.C49022Iv;
import X.C53962fs;
import X.C89484Kd;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.whatsapp.R;
import com.whatsapp.group.GroupSettingsActivity;
import com.whatsapp.group.GroupSettingsViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class GroupSettingsActivity extends ActivityC13790kL {
    public C49022Iv A00;
    public C15550nR A01;
    public C15610nY A02;
    public AnonymousClass018 A03;
    public C19990v2 A04;
    public C21320xE A05;
    public C15600nX A06;
    public C15370n3 A07;
    public C20710wC A08;
    public AnonymousClass11A A09;
    public GroupSettingsViewModel A0A;
    public AnonymousClass3ZC A0B;
    public C15580nU A0C;
    public C20660w7 A0D;
    public C14860mA A0E;
    public boolean A0F;
    public final CompoundButton.OnCheckedChangeListener A0G;
    public final AnonymousClass5UJ A0H;
    public final C89484Kd A0I;

    public GroupSettingsActivity() {
        this(0);
        this.A0H = new AnonymousClass5UJ() { // from class: X.3Xe
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r6) {
                GroupSettingsActivity groupSettingsActivity = GroupSettingsActivity.this;
                if (groupSettingsActivity.A0C.equals(r6)) {
                    GroupSettingsViewModel groupSettingsViewModel = groupSettingsActivity.A0A;
                    groupSettingsViewModel.A02.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(groupSettingsViewModel, 17, groupSettingsActivity.A0C));
                }
            }
        };
        this.A0I = new C89484Kd(this);
        this.A0G = new CompoundButton.OnCheckedChangeListener() { // from class: X.3OM
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                String str;
                GroupSettingsActivity groupSettingsActivity = GroupSettingsActivity.this;
                StringBuilder A0k = C12960it.A0k("GroupSettingsActivity require membership approval toggled ");
                if (z) {
                    str = "On";
                } else {
                    str = "Off";
                }
                Log.i(C12960it.A0d(str, A0k));
                ((ActivityC13830kP) groupSettingsActivity).A05.Ab2(new RunnableBRunnable0Shape1S0110000_I1(groupSettingsActivity, 7, z));
            }
        };
    }

    public GroupSettingsActivity(int i) {
        this.A0F = false;
        ActivityC13830kP.A1P(this, 77);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0F) {
            this.A0F = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A04 = C12980iv.A0c(A1M);
            this.A0D = C12990iw.A0d(A1M);
            this.A0E = (C14860mA) A1M.ANU.get();
            this.A01 = C12960it.A0O(A1M);
            this.A02 = C12960it.A0P(A1M);
            this.A03 = C12960it.A0R(A1M);
            this.A08 = C12980iv.A0e(A1M);
            this.A05 = (C21320xE) A1M.A4Y.get();
            this.A09 = (AnonymousClass11A) A1M.A8o.get();
            this.A06 = C12980iv.A0d(A1M);
            this.A00 = (C49022Iv) A1L.A0e.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        int A02;
        int i3;
        super.onActivityResult(i, i2, intent);
        if (i == 17 && i2 == -1) {
            List A07 = C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("jids"));
            AnonymousClass1JO A072 = this.A06.A02(this.A0C).A07();
            HashSet A12 = C12970iu.A12();
            Iterator it = A072.iterator();
            while (it.hasNext()) {
                AnonymousClass1YO r1 = (AnonymousClass1YO) it.next();
                UserJid userJid = r1.A03;
                if (!(((ActivityC13790kL) this).A01.A0F(userJid) || (i3 = r1.A01) == 0 || i3 == 2)) {
                    A12.add(userJid);
                }
            }
            ArrayList A0x = C12980iv.A0x(A07);
            A0x.removeAll(A12);
            ArrayList A0x2 = C12980iv.A0x(A12);
            A0x2.removeAll(A07);
            if (A0x.size() != 0 || A0x2.size() != 0) {
                if (!((ActivityC13810kN) this).A07.A0B()) {
                    ((ActivityC13810kN) this).A05.A07(C18640sm.A01(this), 0);
                    return;
                }
                C15600nX r3 = this.A06;
                int A022 = r3.A04.A02(this.A0C);
                C14850m9 r12 = r3.A0B;
                if (A022 == 1) {
                    A02 = r12.A02(1655);
                } else {
                    A02 = r12.A02(1304) - 1;
                }
                if (A02 >= (this.A06.A02(this.A0C).A0A().size() + A0x.size()) - A0x2.size()) {
                    C12990iw.A1N(new C36371jm(this, ((ActivityC13810kN) this).A05, this.A01, this.A02, this.A08, this.A0C, this.A0D, A0x, A0x2), ((ActivityC13830kP) this).A05);
                } else if (this.A08.A0b(this.A0C)) {
                    C20710wC.A02(3019, Integer.valueOf(A02));
                } else {
                    HashMap A11 = C12970iu.A11();
                    Iterator it2 = A0x.iterator();
                    while (it2.hasNext()) {
                        C12960it.A1K(it2.next(), A11, 419);
                    }
                    C20710wC.A02(3003, A11);
                }
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.group_settings_title);
        C12970iu.A0N(this).A0M(true);
        C15580nU A0U = ActivityC13790kL.A0U(getIntent(), "gid");
        this.A0C = A0U;
        this.A0B = new AnonymousClass3ZC(this.A0I, A0U, C12990iw.A0c(this.A00.A00.A03));
        GroupSettingsViewModel groupSettingsViewModel = (GroupSettingsViewModel) new AnonymousClass02A(new C53962fs(this), this).A00(GroupSettingsViewModel.class);
        this.A0A = groupSettingsViewModel;
        groupSettingsViewModel.A02.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(groupSettingsViewModel, 17, this.A0C));
        C12960it.A18(this, this.A0A.A00, 79);
        setContentView(R.layout.group_settings);
        GroupSettingsRowView groupSettingsRowView = (GroupSettingsRowView) AnonymousClass00T.A05(this, R.id.restricted_mode_layout);
        AbstractView$OnClickListenerC34281fs.A01(groupSettingsRowView, this, 25);
        if (this.A08.A0a(this.A0C)) {
            groupSettingsRowView.setVisibility(8);
        }
        View A05 = AnonymousClass00T.A05(this, R.id.restricted_mode_separator);
        View A052 = AnonymousClass00T.A05(this, R.id.announcement_group_layout_top_shadow);
        View A053 = AnonymousClass00T.A05(this, R.id.announcement_group_layout);
        View A054 = AnonymousClass00T.A05(this, R.id.announcement_group_layout_bottom_shadow);
        AbstractView$OnClickListenerC34281fs.A01(A053, this, 26);
        A05.setVisibility(8);
        A052.setVisibility(0);
        groupSettingsRowView.setDescriptionText(R.string.group_settings_restricted_mode_info_with_disappearing_messages);
        if (!this.A08.A0a(this.A0C)) {
            A053.setVisibility(0);
            A054.setVisibility(0);
        } else {
            C12980iv.A1C(A053, A052, A05, 8);
            A054.setVisibility(8);
        }
        GroupSettingsRowView groupSettingsRowView2 = (GroupSettingsRowView) AnonymousClass00T.A05(this, R.id.frequently_forwarded_layout);
        AbstractView$OnClickListenerC34281fs.A01(groupSettingsRowView2, this, 27);
        groupSettingsRowView2.setTitleText(R.string.group_settings_forward_many_times);
        AbstractView$OnClickListenerC34281fs.A01(AnonymousClass00T.A05(this, R.id.manage_admins), this, 28);
        View findViewById = findViewById(R.id.require_membership_approval);
        View A055 = AnonymousClass00T.A05(this, R.id.membership_approval_divider_top);
        View A056 = AnonymousClass00T.A05(this, R.id.membership_approval_divider_bottom);
        ((ActivityC13810kN) this).A0C.A07(1728);
        C12980iv.A1C(findViewById, A055, A056, 8);
        AnonymousClass11A r0 = this.A09;
        r0.A00.add(this.A0H);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass11A r0 = this.A09;
        r0.A00.remove(this.A0H);
    }

    /* loaded from: classes3.dex */
    public class EditGroupInfoDialogFragment extends Hilt_GroupSettingsActivity_EditGroupInfoDialogFragment {
        public static EditGroupInfoDialogFragment A00(C15580nU r4, boolean z) {
            EditGroupInfoDialogFragment editGroupInfoDialogFragment = new EditGroupInfoDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("gjid", r4.getRawString());
            A0D.putBoolean(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, z);
            editGroupInfoDialogFragment.A0U(A0D);
            return editGroupInfoDialogFragment;
        }
    }

    /* loaded from: classes3.dex */
    public class RestrictFrequentlyForwardedDialogFragment extends Hilt_GroupSettingsActivity_RestrictFrequentlyForwardedDialogFragment {
        public static RestrictFrequentlyForwardedDialogFragment A00(C15580nU r4, boolean z) {
            RestrictFrequentlyForwardedDialogFragment restrictFrequentlyForwardedDialogFragment = new RestrictFrequentlyForwardedDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("gjid", r4.getRawString());
            A0D.putBoolean(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, z);
            restrictFrequentlyForwardedDialogFragment.A0U(A0D);
            return restrictFrequentlyForwardedDialogFragment;
        }
    }

    /* loaded from: classes3.dex */
    public class SendMessagesDialogFragment extends Hilt_GroupSettingsActivity_SendMessagesDialogFragment {
        public static SendMessagesDialogFragment A00(C15580nU r4, boolean z) {
            SendMessagesDialogFragment sendMessagesDialogFragment = new SendMessagesDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putString("gjid", r4.getRawString());
            A0D.putBoolean(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, z);
            sendMessagesDialogFragment.A0U(A0D);
            return sendMessagesDialogFragment;
        }
    }

    /* loaded from: classes3.dex */
    public abstract class AdminSettingsDialogFragment extends Hilt_GroupSettingsActivity_AdminSettingsDialogFragment {
        public C14900mE A00;
        public C15450nH A01;
        public C15550nR A02;
        public C18640sm A03;
        public C14830m7 A04;
        public AnonymousClass018 A05;
        public C21320xE A06;
        public C15650ng A07;
        public C15600nX A08;
        public C15370n3 A09;
        public C20710wC A0A;
        public C15580nU A0B;
        public C20660w7 A0C;
        public C22140ya A0D;
        public C14860mA A0E;
        public boolean[] A0F = new boolean[1];

        @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
        public void A0w(Bundle bundle) {
            bundle.putBoolean(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, this.A0F[0]);
            super.A0w(bundle);
        }

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            int i;
            int i2;
            int i3;
            int i4;
            C15580nU A04 = C15580nU.A04(A03().getString("gjid"));
            AnonymousClass009.A05(A04);
            this.A0B = A04;
            this.A09 = this.A02.A0B(A04);
            if (bundle == null) {
                bundle = ((AnonymousClass01E) this).A05;
            }
            boolean z = bundle.getBoolean(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID);
            this.A0F[0] = z;
            View inflate = A0B().getLayoutInflater().inflate(R.layout.admin_settings_dialog, (ViewGroup) null, false);
            CompoundButton compoundButton = (CompoundButton) AnonymousClass028.A0D(inflate, R.id.first_radio_button);
            CompoundButton compoundButton2 = (CompoundButton) AnonymousClass028.A0D(inflate, R.id.second_radio_button);
            boolean z2 = this instanceof RestrictFrequentlyForwardedDialogFragment;
            if (!z2) {
                i = R.string.group_settings_all_participants;
            } else {
                i = R.string.group_settings_allow;
            }
            compoundButton.setText(A0I(i));
            if (!z2) {
                i2 = R.string.group_settings_only_admins;
            } else {
                i2 = R.string.group_settings_dont_allow;
            }
            compoundButton2.setText(A0I(i2));
            compoundButton.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 24));
            compoundButton2.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 23));
            if (z) {
                compoundButton2.setChecked(true);
            } else {
                compoundButton.setChecked(true);
            }
            C004802e A0O = C12970iu.A0O(this);
            boolean z3 = this instanceof SendMessagesDialogFragment;
            if (z3) {
                i3 = R.string.group_settings_announcement_title;
            } else if (!z2) {
                i3 = R.string.group_settings_restricted_mode_title;
            } else {
                i3 = R.string.group_settings_forwarded_many_times_title;
            }
            A0O.setTitle(A0I(i3));
            if (z3) {
                i4 = R.string.group_settings_announcement_info;
            } else if (!z2) {
                i4 = R.string.group_settings_restricted_mode_info_with_disappearing_messages;
            } else {
                i4 = R.string.group_settings_forwarded_many_times_info;
            }
            A0O.A0A(A0I(i4));
            A0O.A0B(true);
            A0O.setView(inflate);
            A0O.setNegativeButton(R.string.cancel, new IDxCListenerShape4S0000000_2_I1(13));
            C12970iu.A1L(A0O, this, 38, R.string.ok);
            return A0O.create();
        }
    }
}
