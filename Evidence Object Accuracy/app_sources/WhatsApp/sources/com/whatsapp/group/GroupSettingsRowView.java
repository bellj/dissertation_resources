package com.whatsapp.group;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12990iw;
import X.C13010iy;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class GroupSettingsRowView extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public TextView A01;
    public TextView A02;
    public TextView A03;
    public AnonymousClass2P7 A04;
    public boolean A05;

    public GroupSettingsRowView(Context context) {
        this(context, null);
    }

    public GroupSettingsRowView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        A00(context, attributeSet);
    }

    public GroupSettingsRowView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        A00(context, attributeSet);
    }

    public GroupSettingsRowView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context, AttributeSet attributeSet) {
        String str;
        String str2;
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.group_setting_row, this);
        this.A03 = C12960it.A0I(this, R.id.group_setting_row_title);
        this.A02 = C12960it.A0I(this, R.id.group_setting_row_info);
        this.A00 = AnonymousClass028.A0D(this, R.id.group_setting_row_description_container);
        this.A01 = C12960it.A0I(this, R.id.group_setting_row_description);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0B);
        try {
            int resourceId = obtainStyledAttributes.getResourceId(1, 0);
            if (resourceId != 0) {
                str = context.getString(resourceId);
            } else {
                str = null;
            }
            setTextOrHideView(this.A03, str);
            int resourceId2 = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId2 != 0) {
                str2 = context.getString(resourceId2);
            } else {
                str2 = null;
            }
            setTextOrHideView(this.A02, str2);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    public CharSequence getInfoText() {
        return this.A02.getText();
    }

    public CharSequence getTitleText() {
        return this.A03.getText();
    }

    public void setDescriptionText(int i) {
        this.A00.setVisibility(0);
        setTextOrHideView(this.A01, C12990iw.A0q(this, i));
    }

    public void setInfoText(int i) {
        setTextOrHideView(this.A02, C12990iw.A0q(this, i));
    }

    public final void setTextOrHideView(TextView textView, CharSequence charSequence) {
        textView.setVisibility(C13010iy.A00(TextUtils.isEmpty(charSequence) ? 1 : 0));
        textView.setText(charSequence);
    }

    public void setTitleText(int i) {
        setTextOrHideView(this.A03, C12990iw.A0q(this, i));
    }
}
