package com.whatsapp.group;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C15550nR;

/* loaded from: classes2.dex */
public class GroupSettingsViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final C15550nR A01;
    public final AbstractC14440lR A02;

    public GroupSettingsViewModel(C15550nR r2, AbstractC14440lR r3) {
        this.A02 = r3;
        this.A01 = r2;
    }
}
