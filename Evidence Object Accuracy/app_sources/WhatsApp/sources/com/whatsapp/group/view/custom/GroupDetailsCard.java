package com.whatsapp.group.view.custom;

import X.AbstractC36671kL;
import X.AnonymousClass028;
import X.AnonymousClass03H;
import X.AnonymousClass074;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass19Z;
import X.AnonymousClass1E5;
import X.AnonymousClass1s4;
import X.AnonymousClass31K;
import X.AnonymousClass4AU;
import X.AnonymousClass4GI;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C20710wC;
import X.C28801Pb;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.OnLifecycleEvent;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.contact.view.custom.ContactDetailsActionIcon;
import com.whatsapp.group.GroupCallButtonController;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;

/* loaded from: classes.dex */
public class GroupDetailsCard extends AnonymousClass1s4 implements AnonymousClass03H {
    public View A00;
    public TextView A01;
    public TextView A02;
    public AnonymousClass12P A03;
    public C15570nT A04;
    public C28801Pb A05;
    public WaTextView A06;
    public C15550nR A07;
    public C15610nY A08;
    public ContactDetailsActionIcon A09;
    public ContactDetailsActionIcon A0A;
    public ContactDetailsActionIcon A0B;
    public ContactDetailsActionIcon A0C;
    public C15600nX A0D;
    public C15370n3 A0E;
    public AnonymousClass19M A0F;
    public AnonymousClass31K A0G;
    public AnonymousClass4AU A0H;
    public GroupCallButtonController A0I;
    public C20710wC A0J;
    public AnonymousClass1E5 A0K;
    public C15580nU A0L;
    public AnonymousClass12F A0M;
    public AnonymousClass19Z A0N;

    public GroupDetailsCard(Context context) {
        super(context);
        A01();
    }

    public GroupDetailsCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public GroupDetailsCard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public void A01() {
        LayoutInflater.from(getContext()).inflate(R.layout.group_details_card, (ViewGroup) this, true);
        this.A0B = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_message);
        this.A00 = AnonymousClass028.A0D(this, R.id.action_add_person);
        this.A0A = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_search_chat);
        this.A09 = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_call);
        this.A0C = (ContactDetailsActionIcon) AnonymousClass028.A0D(this, R.id.action_videocall);
        this.A02 = (TextView) AnonymousClass028.A0D(this, R.id.group_subtitle);
        this.A01 = (TextView) AnonymousClass028.A0D(this, R.id.announcements_subtitle_number_of_participants);
        this.A06 = (WaTextView) AnonymousClass028.A0D(this, R.id.group_second_subtitle);
        this.A05 = new C28801Pb(this, this.A08, this.A0M, (int) R.id.group_title);
        A03();
    }

    public final void A02() {
        ContactDetailsActionIcon contactDetailsActionIcon;
        int i;
        Resources resources;
        int i2;
        GroupCallButtonController groupCallButtonController = this.A0I;
        if (groupCallButtonController != null) {
            groupCallButtonController.A02();
            GroupCallButtonController groupCallButtonController2 = this.A0I;
            this.A0H = groupCallButtonController2.A01();
            boolean A07 = groupCallButtonController2.A07();
            this.A09.setEnabled(A07);
            this.A0C.setEnabled(A07);
            int[] iArr = AnonymousClass4GI.A00;
            int ordinal = this.A0H.ordinal();
            int i3 = iArr[ordinal];
            switch (ordinal) {
                case 2:
                    this.A09.setVisibility(0);
                    this.A0C.setVisibility(8);
                    this.A09.A00(R.drawable.ic_action_call, getResources().getString(this.A0I.A00()));
                    this.A09.setEnabled(this.A0I.A06());
                    return;
                case 3:
                    this.A09.setVisibility(0);
                    this.A0C.setVisibility(8);
                    contactDetailsActionIcon = this.A09;
                    i = R.drawable.ic_action_new_call;
                    resources = getResources();
                    i2 = R.string.group_call;
                    break;
                default:
                    ContactDetailsActionIcon contactDetailsActionIcon2 = this.A09;
                    if (i3 == 3) {
                        contactDetailsActionIcon2.setVisibility(0);
                        this.A0C.setVisibility(0);
                        contactDetailsActionIcon = this.A09;
                        i = R.drawable.ic_action_call;
                        resources = getResources();
                        i2 = R.string.contact_info_action_icon_call;
                        break;
                    } else {
                        contactDetailsActionIcon2.setVisibility(8);
                        this.A0C.setVisibility(8);
                        return;
                    }
            }
            contactDetailsActionIcon.A00(i, resources.getString(i2));
        }
    }

    public final void A03() {
        this.A0B.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 49));
        this.A0A.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 15));
        this.A09.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 16));
        this.A0C.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 14));
    }

    @OnLifecycleEvent(AnonymousClass074.ON_CREATE)
    public void onActivityCreated() {
        GroupCallButtonController groupCallButtonController = this.A0I;
        if (groupCallButtonController != null) {
            groupCallButtonController.A03();
        }
    }

    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    public void onActivityDestroyed() {
        GroupCallButtonController groupCallButtonController = this.A0I;
        if (groupCallButtonController != null) {
            groupCallButtonController.A04();
        }
    }

    public void setAddPersonOnClickListener(View.OnClickListener onClickListener) {
        this.A00.setOnClickListener(onClickListener);
    }

    public void setGroupInfoLoggingEvent(AnonymousClass31K r1) {
        this.A0G = r1;
    }

    public void setSecondSubtitleText(String str) {
        if (TextUtils.isEmpty(str)) {
            this.A06.setVisibility(8);
            return;
        }
        this.A06.setVisibility(0);
        this.A06.setText(str);
    }

    public void setSubtitleNumberOfParticipantsText(String str) {
        if (TextUtils.isEmpty(str) || this.A0J.A0Y(this.A0E) || this.A0K.A00(this.A0E)) {
            this.A01.setVisibility(8);
            return;
        }
        this.A01.setVisibility(0);
        this.A01.setText(str);
    }

    public void setSubtitleText(String str) {
        this.A02.setText(str);
    }

    public void setTitleColor(int i) {
        this.A05.A04(i);
    }

    public void setTitleText(String str) {
        this.A05.A08(AbstractC36671kL.A04(getContext(), this.A05.A01(), this.A0F, str, 0.9f));
    }
}
