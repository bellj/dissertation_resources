package com.whatsapp.group;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC33331dp;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass0B5;
import X.AnonymousClass10S;
import X.AnonymousClass11A;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass1YM;
import X.AnonymousClass1YO;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass41Z;
import X.AnonymousClass5UJ;
import X.C103094qC;
import X.C103784rJ;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22330yu;
import X.C22670zS;
import X.C244215l;
import X.C245215v;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C25691Aj;
import X.C27131Gd;
import X.C32751cg;
import X.C36441jt;
import X.C36651kG;
import X.C48232Fc;
import X.C56762ld;
import X.C624837k;
import X.C73123fi;
import X.C858344i;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101584nl;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.group.GroupAdminPickerActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class GroupAdminPickerActivity extends ActivityC13790kL {
    public ColorDrawable A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public SearchView A05;
    public BottomSheetBehavior A06;
    public C22330yu A07;
    public C15550nR A08;
    public AnonymousClass10S A09;
    public C15610nY A0A;
    public AnonymousClass1J1 A0B;
    public C21270x9 A0C;
    public AnonymousClass018 A0D;
    public C245215v A0E;
    public C15600nX A0F;
    public C25691Aj A0G;
    public C624837k A0H;
    public C36651kG A0I;
    public AnonymousClass11A A0J;
    public C244215l A0K;
    public C15580nU A0L;
    public AnonymousClass12F A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public List A0Q;
    public boolean A0R;
    public final View.OnClickListener A0S;
    public final AnonymousClass2Dn A0T;
    public final C27131Gd A0U;
    public final AnonymousClass5UJ A0V;
    public final AbstractC33331dp A0W;

    public GroupAdminPickerActivity() {
        this(0);
        this.A0U = new C36441jt(this);
        this.A0T = new AnonymousClass41Z(this);
        this.A0W = new C858344i(this);
        this.A0V = new AnonymousClass5UJ() { // from class: X.57D
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                GroupAdminPickerActivity groupAdminPickerActivity = GroupAdminPickerActivity.this;
                C15580nU r0 = groupAdminPickerActivity.A0L;
                AnonymousClass009.A05(r0);
                if (r0.equals(r3)) {
                    groupAdminPickerActivity.A2g();
                    groupAdminPickerActivity.A2h(groupAdminPickerActivity.A0N);
                }
            }
        };
        this.A0S = new ViewOnClickCListenerShape2S0100000_I0_2(this, 6);
    }

    public GroupAdminPickerActivity(int i) {
        this.A0R = false;
        A0R(new C103094qC(this));
    }

    public static /* synthetic */ boolean A02(GroupAdminPickerActivity groupAdminPickerActivity, UserJid userJid) {
        if (userJid == null) {
            return false;
        }
        for (C15370n3 r1 : groupAdminPickerActivity.A0Q) {
            if (userJid.equals(r1.A0B(UserJid.class))) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0R) {
            this.A0R = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0C = (C21270x9) r1.A4A.get();
            this.A08 = (C15550nR) r1.A45.get();
            this.A0A = (C15610nY) r1.AMe.get();
            this.A0D = (AnonymousClass018) r1.ANb.get();
            this.A09 = (AnonymousClass10S) r1.A46.get();
            this.A0M = (AnonymousClass12F) r1.AJM.get();
            this.A07 = (C22330yu) r1.A3I.get();
            this.A0G = (C25691Aj) r1.AKj.get();
            this.A0J = (AnonymousClass11A) r1.A8o.get();
            this.A0F = (C15600nX) r1.A8x.get();
            this.A0E = (C245215v) r1.A8w.get();
            this.A0K = (C244215l) r1.A8y.get();
        }
    }

    public final void A2e() {
        this.A02.setPadding(0, getResources().getDimensionPixelSize(R.dimen.admin_picker_top_padding), 0, 0);
        ((AnonymousClass0B5) this.A02.getLayoutParams()).A00(this.A06);
        this.A00.setColor(2130706432);
        this.A04.setVisibility(0);
        this.A03.setVisibility(8);
        A2h(null);
    }

    public final void A2f() {
        this.A02.setPadding(0, 0, 0, 0);
        ((AnonymousClass0B5) this.A02.getLayoutParams()).A00(null);
        this.A00.setColor(AnonymousClass00T.A00(this, R.color.groupAdminPickerBackground));
        this.A05.setIconified(false);
        this.A04.setVisibility(8);
        this.A03.setVisibility(0);
    }

    public final void A2g() {
        AnonymousClass1YM A02;
        if (this.A0P == null || this.A0O == null) {
            C15600nX r1 = this.A0F;
            C15580nU r0 = this.A0L;
            AnonymousClass009.A05(r0);
            A02 = r1.A02(r0);
        } else {
            C25691Aj r02 = this.A0G;
            A02 = (AnonymousClass1YM) r02.A01.get(this.A0L);
        }
        this.A0Q = new ArrayList(A02.A02.size());
        Iterator it = A02.A0A().iterator();
        while (it.hasNext()) {
            C15570nT r03 = ((ActivityC13790kL) this).A01;
            UserJid userJid = ((AnonymousClass1YO) it.next()).A03;
            if (!r03.A0F(userJid)) {
                this.A0Q.add(this.A08.A0B(userJid));
            }
        }
    }

    public final void A2h(String str) {
        this.A0N = str;
        C624837k r1 = this.A0H;
        if (r1 != null) {
            r1.A03(true);
        }
        C624837k r2 = new C624837k(this.A0A, this.A0D, this, str, this.A0Q);
        this.A0H = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A03.getVisibility() == 0) {
            A2e();
        } else {
            this.A06.A0M(4);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.group_admin_picker);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
        }
        View findViewById = findViewById(R.id.bottom_sheet);
        this.A02 = findViewById;
        this.A06 = BottomSheetBehavior.A00(findViewById);
        this.A02.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101584nl(this));
        this.A01 = findViewById(R.id.background);
        PointF pointF = new PointF();
        this.A01.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 31, pointF));
        this.A01.setOnTouchListener(new View.OnTouchListener(pointF) { // from class: X.4nN
            public final /* synthetic */ PointF A00;

            {
                this.A00 = r1;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                this.A00.set(motionEvent.getX(), motionEvent.getY());
                return false;
            }
        });
        ColorDrawable colorDrawable = new ColorDrawable(2130706432);
        this.A00 = colorDrawable;
        this.A01.setBackground(colorDrawable);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) getResources().getInteger(17694720));
        this.A01.startAnimation(alphaAnimation);
        int A00 = AnonymousClass00T.A00(this, R.color.primary);
        this.A06.A0E = new C56762ld(this, A00);
        this.A04 = findViewById(R.id.title_holder);
        View findViewById2 = findViewById(R.id.search_holder);
        this.A03 = findViewById2;
        C48232Fc.A00(findViewById2);
        SearchView searchView = (SearchView) this.A03.findViewById(R.id.search_view);
        this.A05 = searchView;
        ((TextView) searchView.findViewById(R.id.search_src_text)).setTextColor(AnonymousClass00T.A00(this, R.color.search_text_color));
        this.A05.setIconifiedByDefault(false);
        this.A05.setQueryHint(getString(R.string.select_group_admin_search_hint));
        ((ImageView) this.A05.findViewById(R.id.search_mag_icon)).setImageDrawable(new C73123fi(AnonymousClass00T.A04(this, R.drawable.ic_back), this));
        this.A05.A0B = new C103784rJ(this);
        ImageView imageView = (ImageView) this.A03.findViewById(R.id.search_back);
        imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A01(this, R.drawable.ic_back, R.color.lightActionBarItemDrawableTint), this.A0D));
        imageView.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 34));
        findViewById(R.id.search_btn).setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 7));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager());
        this.A0B = this.A0C.A04(this, "group-admin-picker-activity");
        C15580nU A04 = C15580nU.A04(getIntent().getStringExtra("gid"));
        AnonymousClass009.A05(A04);
        this.A0L = A04;
        this.A0P = getIntent().getStringExtra("subgroup_subject");
        this.A0O = getIntent().getStringExtra("parent_group_jid");
        A2g();
        C36651kG r1 = new C36651kG(this);
        this.A0I = r1;
        r1.A01 = this.A0Q;
        r1.A00 = C32751cg.A02(r1.A02.A0D, null);
        r1.A02();
        recyclerView.setAdapter(this.A0I);
        this.A09.A03(this.A0U);
        this.A07.A03(this.A0T);
        AnonymousClass11A r0 = this.A0J;
        r0.A00.add(this.A0V);
        this.A0K.A03(this.A0W);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A09.A04(this.A0U);
        this.A07.A04(this.A0T);
        AnonymousClass11A r0 = this.A0J;
        r0.A00.remove(this.A0V);
        this.A0K.A04(this.A0W);
        this.A0B.A00();
        C25691Aj r02 = this.A0G;
        r02.A01.remove(this.A0L);
        C624837k r1 = this.A0H;
        if (r1 != null) {
            r1.A03(true);
        }
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.getBoolean("search")) {
            A2f();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        boolean z = false;
        if (this.A03.getVisibility() == 0) {
            z = true;
        }
        bundle.putBoolean("search", z);
    }
}
