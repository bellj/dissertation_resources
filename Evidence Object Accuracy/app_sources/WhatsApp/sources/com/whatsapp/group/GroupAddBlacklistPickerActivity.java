package com.whatsapp.group;

import X.AbstractActivityC36551k4;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1BN;
import X.AnonymousClass2FL;
import X.AnonymousClass5UK;
import X.C12960it;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class GroupAddBlacklistPickerActivity extends AbstractActivityC36551k4 implements AnonymousClass5UK {
    public AnonymousClass1BN A00;
    public boolean A01;
    public boolean A02;

    public GroupAddBlacklistPickerActivity() {
        this(0);
    }

    public GroupAddBlacklistPickerActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 73);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ActivityC13770kJ.A0M(this, A1M, ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this)));
            this.A00 = (AnonymousClass1BN) A1M.A8l.get();
        }
    }

    @Override // X.AnonymousClass5UK
    public void A7f() {
        ((ActivityC13810kN) this).A05.A06(0, R.string.info_update_dialog_title);
        C12960it.A18(this, this.A00.A01(this.A0U), 77);
    }

    @Override // X.AbstractActivityC36551k4, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A02 = getIntent().getBooleanExtra("was_nobody", false);
    }
}
