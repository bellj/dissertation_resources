package com.whatsapp.polls;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass1J2;
import X.AnonymousClass1J3;
import X.AnonymousClass1J4;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103244qR;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26701Em;
import X.C27671Iq;
import X.C38211ni;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class PollResultsActivity extends ActivityC13790kL {
    public RecyclerView A00;
    public AnonymousClass1J2 A01;
    public AnonymousClass1J3 A02;
    public WaTextView A03;
    public AnonymousClass1J1 A04;
    public C21270x9 A05;
    public C15650ng A06;
    public AnonymousClass1J4 A07;
    public PollResultsViewModel A08;
    public C26701Em A09;
    public C27671Iq A0A;
    public boolean A0B;

    public PollResultsActivity() {
        this(0);
    }

    public PollResultsActivity(int i) {
        this.A0B = false;
        A0R(new C103244qR(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A01 = (AnonymousClass1J2) r2.A0g.get();
            this.A02 = (AnonymousClass1J3) r2.A0h.get();
            this.A05 = (C21270x9) r1.A4A.get();
            this.A06 = (C15650ng) r1.A4m.get();
            this.A09 = (C26701Em) r1.ABc.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        PollResultsViewModel pollResultsViewModel = this.A08;
        if (pollResultsViewModel.A03) {
            pollResultsViewModel.A03 = false;
            pollResultsViewModel.A04();
            return;
        }
        super.onBackPressed();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.results_poll);
        setContentView(R.layout.poll_results);
        A1e((Toolbar) findViewById(R.id.toolbar));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0A(R.string.results_poll);
        AbstractC15340mz A03 = this.A06.A0K.A03(C38211ni.A02(getIntent()));
        AnonymousClass009.A05(A03);
        this.A0A = (C27671Iq) A03;
        WaTextView waTextView = (WaTextView) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.poll_results_question_text_view);
        this.A03 = waTextView;
        waTextView.setText(this.A0A.A02);
        this.A04 = this.A05.A04(getBaseContext(), "poll-results-activity");
        RunnableBRunnable0Shape9S0100000_I0_9 runnableBRunnable0Shape9S0100000_I0_9 = new RunnableBRunnable0Shape9S0100000_I0_9(this, 28);
        C26701Em r3 = this.A09;
        C27671Iq r2 = this.A0A;
        if (!C26701Em.A00(r2, (byte) 67)) {
            runnableBRunnable0Shape9S0100000_I0_9.run();
        } else {
            r3.A02(r2, runnableBRunnable0Shape9S0100000_I0_9, (byte) 67);
        }
    }
}
