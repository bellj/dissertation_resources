package com.whatsapp.polls;

import X.AnonymousClass015;
import X.AnonymousClass018;
import X.C12960it;
import X.C13000ix;
import X.C14830m7;
import X.C15550nR;
import X.C15610nY;
import X.C16170oZ;
import X.C16590pI;
import X.C27671Iq;
import X.C27691It;
import java.util.List;

/* loaded from: classes2.dex */
public class PollVoterViewModel extends AnonymousClass015 {
    public C27671Iq A00;
    public boolean A01;
    public final C16170oZ A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C27691It A08 = C13000ix.A03();
    public final C27691It A09 = C13000ix.A03();
    public final C27691It A0A = C13000ix.A03();
    public final List A0B = C12960it.A0l();

    public PollVoterViewModel(C16170oZ r2, C15550nR r3, C15610nY r4, C14830m7 r5, C16590pI r6, AnonymousClass018 r7) {
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r6;
        this.A07 = r7;
        this.A05 = r5;
        this.A02 = r2;
    }
}
