package com.whatsapp.polls;

import X.AbstractC005102i;
import X.AbstractC14640lm;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass0F6;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C13000ix;
import X.C54092gA;
import X.C54132gE;
import X.C74643iR;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.inputmethod.InputMethodManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* loaded from: classes2.dex */
public class PollCreatorActivity extends ActivityC13790kL {
    public Vibrator A00;
    public InputMethodManager A01;
    public RecyclerView A02;
    public WaButton A03;
    public AbstractC14640lm A04;
    public C54132gE A05;
    public PollCreatorViewModel A06;
    public boolean A07;

    public PollCreatorActivity() {
        this(0);
    }

    public PollCreatorActivity(int i) {
        this.A07 = false;
        ActivityC13830kP.A1P(this, 90);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.create_poll);
        setContentView(R.layout.poll_creator);
        A1e(ActivityC13790kL.A0Q(this));
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0A(R.string.create_poll);
        this.A04 = C12970iu.A0c(this);
        PollCreatorViewModel pollCreatorViewModel = (PollCreatorViewModel) C13000ix.A02(this).A00(PollCreatorViewModel.class);
        this.A06 = pollCreatorViewModel;
        C12960it.A17(this, pollCreatorViewModel.A0C, 56);
        C12960it.A17(this, this.A06.A0A, 55);
        C12960it.A18(this, this.A06.A0B, 84);
        C12960it.A17(this, this.A06.A0D, 57);
        C12960it.A18(this, this.A06.A09, 85);
        this.A02 = C12990iw.A0R(((ActivityC13810kN) this).A00, R.id.poll_creator_options_recycler_view);
        this.A00 = (Vibrator) getSystemService("vibrator");
        this.A01 = (InputMethodManager) getSystemService("input_method");
        new AnonymousClass0F6(new C54092gA(this)).A0C(this.A02);
        this.A02.setLayoutManager(new LinearLayoutManager());
        C54132gE r1 = new C54132gE(new C74643iR(), ((ActivityC13810kN) this).A0C, this.A06);
        this.A05 = r1;
        this.A02.setAdapter(r1);
        WaButton waButton = (WaButton) AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.poll_create_button);
        this.A03 = waButton;
        AbstractView$OnClickListenerC34281fs.A01(waButton, this, 37);
    }
}
