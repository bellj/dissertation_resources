package com.whatsapp.polls;

import X.AnonymousClass015;
import X.C1111458g;
import X.C1111558h;
import X.C112165Ch;
import X.C15550nR;
import X.C15610nY;
import X.C16590pI;
import X.C27671Iq;
import X.C27691It;
import X.C27701Iu;
import X.C27721Iy;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class PollResultsViewModel extends AnonymousClass015 {
    public int A00 = 0;
    public C27671Iq A01;
    public List A02 = new ArrayList();
    public boolean A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final C16590pI A06;
    public final C27691It A07 = new C27691It();
    public final Map A08 = new HashMap();

    public PollResultsViewModel(C15550nR r2, C15610nY r3, C16590pI r4) {
        this.A04 = r2;
        this.A05 = r3;
        this.A06 = r4;
    }

    public void A04() {
        C27671Iq r1 = this.A01;
        if (r1 != null) {
            this.A02 = new ArrayList();
            Collections.sort(r1.A04, new C112165Ch(this));
            for (C27701Iu r4 : this.A01.A04) {
                this.A02.add(new C1111558h(r4.A03, r4.A00, this.A00, r4.A01));
                List list = (List) this.A08.get(Long.valueOf(r4.A01));
                if (list == null) {
                    this.A02.add(new C27721Iy(null, this.A06.A02(R.string.sticker_search_no_results)));
                } else {
                    int i = 0;
                    Iterator it = list.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            Object next = it.next();
                            if (i >= 5) {
                                this.A02.add(new C1111458g(r4.A01));
                                break;
                            } else {
                                this.A02.add(next);
                                i++;
                            }
                        }
                    }
                }
            }
            this.A07.A0B(this.A02);
        }
    }
}
