package com.whatsapp.polls;

import X.AbstractC14440lR;
import X.AbstractC89694Ky;
import X.AnonymousClass015;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C13000ix;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C15570nT;
import X.C15650ng;
import X.C16170oZ;
import X.C20320vZ;
import X.C27691It;
import X.C861846a;
import X.C861946b;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/* loaded from: classes2.dex */
public class PollCreatorViewModel extends AnonymousClass015 {
    public int A00 = -1;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C16170oZ A03;
    public final C14830m7 A04;
    public final C15650ng A05;
    public final C14850m9 A06;
    public final C861946b A07 = new C861946b();
    public final C20320vZ A08;
    public final C27691It A09 = C13000ix.A03();
    public final C27691It A0A = C13000ix.A03();
    public final C27691It A0B = C13000ix.A03();
    public final C27691It A0C = C13000ix.A03();
    public final C27691It A0D = C13000ix.A03();
    public final AbstractC14440lR A0E;
    public final List A0F = C12960it.A0l();

    public PollCreatorViewModel(C14900mE r4, C15570nT r5, C16170oZ r6, C14830m7 r7, C15650ng r8, C14850m9 r9, C20320vZ r10, AbstractC14440lR r11) {
        this.A04 = r7;
        this.A06 = r9;
        this.A01 = r4;
        this.A02 = r5;
        this.A0E = r11;
        this.A03 = r6;
        this.A08 = r10;
        this.A05 = r8;
        List list = this.A0F;
        list.add(new C861846a(0));
        list.add(new C861846a(1));
        A04();
    }

    public final void A04() {
        ArrayList A0l = C12960it.A0l();
        A0l.add(this.A07);
        A0l.addAll(this.A0F);
        this.A0C.A0B(A0l);
    }

    public boolean A05(String str, int i) {
        List list = this.A0F;
        C861846a r1 = (C861846a) list.get(i);
        if (TextUtils.equals(r1.A00, str)) {
            return false;
        }
        r1.A00 = str;
        if (list.size() < this.A06.A02(1408)) {
            Iterator it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((C861846a) it.next()).A00.isEmpty()) {
                        break;
                    }
                } else {
                    list.add(new C861846a(((AbstractC89694Ky) list.get(C12980iv.A0C(list))).A00 + 1));
                    break;
                }
            }
        }
        A04();
        return true;
    }

    public boolean A06(boolean z) {
        List list;
        HashSet A12 = C12970iu.A12();
        Stack stack = new Stack();
        int i = 0;
        int i2 = 0;
        while (true) {
            list = this.A0F;
            if (i >= list.size()) {
                break;
            }
            String trim = ((C861846a) list.get(i)).A00.trim();
            if (!trim.isEmpty()) {
                i2++;
                if (!A12.contains(trim)) {
                    A12.add(trim);
                } else {
                    stack.push(Integer.valueOf(i));
                }
            }
            i++;
        }
        if (stack.isEmpty()) {
            C12970iu.A1Q(this.A09, -1);
            this.A00 = -1;
            if (i2 > 1) {
                return true;
            }
            return false;
        } else if (!z) {
            return true;
        } else {
            int A05 = C12960it.A05(stack.pop());
            C12970iu.A1Q(this.A09, A05);
            this.A00 = ((AbstractC89694Ky) list.get(A05)).A00;
            this.A0B.A0A(Boolean.FALSE);
            return false;
        }
    }
}
