package com.whatsapp.picker.search;

import X.AbstractC116245Ur;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01A;
import X.AnonymousClass02A;
import X.AnonymousClass15H;
import X.AnonymousClass165;
import X.AnonymousClass193;
import X.AnonymousClass1AB;
import X.AnonymousClass1KB;
import X.AnonymousClass1KS;
import X.AnonymousClass2B7;
import X.AnonymousClass364;
import X.AnonymousClass3FN;
import X.AnonymousClass3S8;
import X.AnonymousClass51E;
import X.C105564uG;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C15260mp;
import X.C16120oU;
import X.C22210yi;
import X.C37471mS;
import X.C53092ck;
import X.C53922fo;
import X.C54392ge;
import X.C54812hK;
import X.C63443Bp;
import X.C64783Gw;
import X.C69893aP;
import X.C74433hy;
import X.C75033jC;
import X.C854042r;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101784o5;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class StickerSearchDialogFragment extends Hilt_StickerSearchDialogFragment implements AbstractC116245Ur {
    public View A00;
    public ViewTreeObserver.OnGlobalLayoutListener A01;
    public RecyclerView A02;
    public ViewPager A03;
    public TabLayout A04;
    public WaEditText A05;
    public AnonymousClass018 A06;
    public AnonymousClass193 A07;
    public C14850m9 A08;
    public C16120oU A09;
    public ViewTreeObserver$OnGlobalLayoutListenerC101784o5 A0A;
    public C53922fo A0B;
    public C22210yi A0C;
    public C54392ge A0D;
    public AnonymousClass15H A0E;
    public Runnable A0F;
    public String A0G = "";
    public final C64783Gw A0H = new C64783Gw();

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        this.A05.A04(false);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        if (!TextUtils.isEmpty(this.A0G)) {
            bundle.putString("search_term", this.A0G);
        }
    }

    @Override // com.whatsapp.picker.search.PickerSearchDialogFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        AnonymousClass1AB r8;
        AnonymousClass2B7 r0;
        super.A10(bundle, layoutInflater, viewGroup);
        Context A01 = A01();
        if (bundle != null) {
            this.A0G = bundle.getString("search_term");
        }
        if (this.A0G == null) {
            this.A0G = "";
        }
        View inflate = layoutInflater.inflate(R.layout.sticker_search_dialog, viewGroup, false);
        this.A00 = inflate.findViewById(R.id.no_results);
        View findViewById = inflate.findViewById(R.id.get_more_stickers_button);
        if (((PickerSearchDialogFragment) this).A00 != null) {
            C12960it.A12(findViewById, this, 6);
        }
        this.A02 = (RecyclerView) inflate.findViewById(R.id.search_result);
        View findViewById2 = inflate.findViewById(R.id.search_bar_container);
        this.A05 = (WaEditText) inflate.findViewById(R.id.search_bar);
        C63443Bp r6 = new C63443Bp(A01, viewGroup, this.A02, this.A0D);
        this.A01 = r6.A07;
        this.A02.getViewTreeObserver().addOnGlobalLayoutListener(this.A01);
        this.A02.A0n(new C75033jC(this));
        C54812hK r62 = new C54812hK(A02(), r6.A08, this.A08);
        this.A02.A0n(r62);
        RecyclerView recyclerView = this.A02;
        this.A0A = new ViewTreeObserver$OnGlobalLayoutListenerC101784o5(recyclerView, r62);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this.A0A);
        C53922fo r02 = (C53922fo) new AnonymousClass02A(new C105564uG(this.A07), this).A00(C53922fo.class);
        this.A0B = r02;
        C12970iu.A1P(A0G(), r02.A00, this, 54);
        C12960it.A19(A0G(), this.A0B.A01, this, 83);
        if (this.A0D == null) {
            C69893aP r03 = ((PickerSearchDialogFragment) this).A00;
            AnonymousClass009.A05(r03);
            List list = r03.A05;
            if (list == null) {
                r03.A08.A01();
            } else {
                this.A0B.A00.A0B(list);
            }
            List A0z = C12980iv.A0z(this.A0B.A01);
            Context A0p = A0p();
            C15260mp r04 = ((PickerSearchDialogFragment) this).A00.A00;
            if (r04 == null || (r0 = r04.A0A) == null) {
                r8 = null;
            } else {
                r8 = r0.A09;
            }
            C54392ge r63 = new C54392ge(A0p, r8, this, 1, A0z);
            this.A0D = r63;
            this.A02.setAdapter(r63);
        }
        View findViewById3 = inflate.findViewById(R.id.clear_search_btn);
        AbstractView$OnClickListenerC34281fs.A01(findViewById3, this, 35);
        this.A05.addTextChangedListener(new AnonymousClass364(findViewById3, this));
        AbstractView$OnClickListenerC34281fs.A01(inflate.findViewById(R.id.back), this, 36);
        TabLayout tabLayout = (TabLayout) inflate.findViewById(R.id.sticker_category_tabs);
        this.A04 = tabLayout;
        tabLayout.A0A(AnonymousClass00T.A00(A0p(), R.color.mediaGalleryTabInactive), AnonymousClass00T.A00(A0p(), R.color.mediaGalleryTabActive));
        C12970iu.A18(A0p(), this.A04, R.color.elevated_background);
        C12970iu.A18(A0p(), findViewById2, R.color.elevated_background);
        A1L(R.string.sticker_search_tab_all, 0);
        A1L(R.string.sticker_search_tab_love, 1);
        A1L(R.string.sticker_search_tab_greetings, 2);
        A1L(R.string.sticker_search_tab_happy, 3);
        A1L(R.string.sticker_search_tab_sad, 4);
        A1L(R.string.sticker_search_tab_angry, 5);
        A1L(R.string.sticker_search_tab_celebrate, 6);
        this.A04.setTabMode(0);
        this.A03 = (ViewPager) inflate.findViewById(R.id.sticker_category_viewpager);
        this.A03.setAdapter(new C74433hy(A0E()));
        this.A03.setOffscreenPageLimit(7);
        this.A03.A0G(new AnonymousClass3S8(this.A04));
        this.A04.A0D(new AnonymousClass51E(this));
        this.A05.setText("");
        this.A05.requestFocus();
        this.A05.A04(false);
        this.A09.A07(new C854042r());
        AnonymousClass165 r64 = this.A0E.A01;
        synchronized (r64.A04) {
            C12970iu.A1B(r64.A00().edit(), "sticker_search_opened_count", r64.A00().getInt("sticker_search_opened_count", 0) + 1);
        }
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        this.A02.getViewTreeObserver().removeGlobalOnLayoutListener(this.A01);
        this.A02.getViewTreeObserver().removeGlobalOnLayoutListener(this.A0A);
        List list = this.A02.A0b;
        if (list != null) {
            list.clear();
        }
        Runnable runnable = this.A0F;
        if (runnable != null) {
            this.A05.removeCallbacks(runnable);
            this.A0F = null;
        }
        super.A12();
    }

    public List A1J(int i) {
        List<AnonymousClass1KS> A0z = C12980iv.A0z(this.A0B.A00);
        if (A0z == null) {
            return C12980iv.A0w(0);
        }
        C64783Gw r0 = this.A0H;
        if (i == 0) {
            return A0z;
        }
        ArrayList A0l = C12960it.A0l();
        Set set = (Set) C12990iw.A0l(r0.A00, i);
        if (set != null) {
            for (AnonymousClass1KS r4 : A0z) {
                AnonymousClass1KB r3 = r4.A04;
                if (r3 != null && r3.A08 != null) {
                    int i2 = 0;
                    while (true) {
                        C37471mS[] r1 = r3.A08;
                        if (i2 >= r1.length) {
                            break;
                        } else if (set.contains(r1[i2])) {
                            A0l.add(r4);
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return A0l;
    }

    public final void A1K() {
        View view;
        List A0z = C12980iv.A0z(this.A0B.A01);
        List A0z2 = C12980iv.A0z(this.A0B.A00);
        boolean isEmpty = TextUtils.isEmpty(this.A0G);
        int i = 0;
        TabLayout tabLayout = this.A04;
        if (isEmpty) {
            tabLayout.setVisibility(0);
            if (this.A03.getVisibility() != 0) {
                this.A03.setVisibility(0);
                A1M(true);
            }
            view = this.A00;
            if (A0z2 != null && !A0z2.isEmpty()) {
                i = 8;
            }
        } else {
            tabLayout.setVisibility(8);
            if (this.A03.getVisibility() != 8) {
                A1M(false);
                this.A03.setVisibility(8);
            }
            if (A0z == null || A0z.isEmpty()) {
                view = this.A00;
            } else {
                this.A00.setVisibility(8);
                return;
            }
        }
        view.setVisibility(i);
    }

    public final void A1L(int i, int i2) {
        AnonymousClass3FN A03 = this.A04.A03();
        A03.A01(i);
        A03.A06 = Integer.valueOf(i2);
        A03.A04 = C12970iu.A0q(this, A0I(i), C12970iu.A1b(), 0, R.string.sticker_search_tab_content_description);
        C53092ck r0 = A03.A02;
        if (r0 != null) {
            r0.A00();
        }
        this.A04.A0E(A03);
    }

    public final void A1M(boolean z) {
        StickerSearchTabFragment stickerSearchTabFragment;
        C54392ge r0;
        AnonymousClass01A r1 = this.A03.A0V;
        if ((r1 instanceof C74433hy) && (stickerSearchTabFragment = ((C74433hy) r1).A00) != null && (r0 = stickerSearchTabFragment.A03) != null) {
            r0.A04 = z;
            stickerSearchTabFragment.A01.setAdapter(null);
            stickerSearchTabFragment.A01.setAdapter(stickerSearchTabFragment.A03);
        }
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r2, Integer num, int i) {
        C69893aP r0 = ((PickerSearchDialogFragment) this).A00;
        if (r0 != null) {
            r0.AWl(r2, num, i);
        }
    }
}
