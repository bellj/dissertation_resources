package com.whatsapp.picker.search;

import X.AbstractC13950kb;
import X.ActivityC000900k;
import X.AnonymousClass1KT;
import X.C12980iv;
import X.C15260mp;
import X.C41691tw;
import X.C69893aP;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.picker.search.PickerSearchDialogFragment;

/* loaded from: classes2.dex */
public abstract class PickerSearchDialogFragment extends Hilt_PickerSearchDialogFragment {
    public C69893aP A00;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        ActivityC000900k A0B = A0B();
        if (!(A0B instanceof AbstractC13950kb)) {
            return null;
        }
        ((AbstractC13950kb) A0B).ATj(this);
        return null;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A1D(0, R.style.PickerSearchDialog);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog A1A = super.A1A(bundle);
        C41691tw.A05(A1A.getContext(), A1A.getWindow(), R.color.searchStatusBar);
        A1A.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: X.4hR
            @Override // android.content.DialogInterface.OnKeyListener
            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                PickerSearchDialogFragment pickerSearchDialogFragment = PickerSearchDialogFragment.this;
                if (i != 4 || keyEvent.getAction() != 1 || keyEvent.isCanceled()) {
                    return false;
                }
                pickerSearchDialogFragment.A1B();
                return true;
            }
        });
        return A1A;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        C15260mp r0;
        super.onDismiss(dialogInterface);
        C69893aP r2 = this.A00;
        if (r2 != null) {
            r2.A07 = false;
            if (r2.A06 && (r0 = r2.A00) != null) {
                r0.A06();
            }
            r2.A03 = null;
            AnonymousClass1KT r02 = r2.A08;
            r02.A01 = null;
            C12980iv.A1M(r02.A03);
            this.A00 = null;
        }
    }
}
