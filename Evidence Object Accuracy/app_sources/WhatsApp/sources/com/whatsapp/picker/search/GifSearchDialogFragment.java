package com.whatsapp.picker.search;

import X.AbstractC253919f;
import X.AnonymousClass01E;
import X.AnonymousClass01d;
import X.AnonymousClass5RW;
import X.AnonymousClass5UF;
import X.C12960it;
import X.C14820m6;
import X.C14850m9;
import X.C16120oU;
import X.C16630pM;
import X.C252718t;
import X.C253719d;
import X.C66013Ly;
import X.C69893aP;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.gifsearch.GifSearchContainer;

/* loaded from: classes2.dex */
public class GifSearchDialogFragment extends Hilt_GifSearchDialogFragment implements AnonymousClass5UF, AnonymousClass5RW {
    public AnonymousClass01d A00;
    public C14820m6 A01;
    public C14850m9 A02;
    public C16120oU A03;
    public C253719d A04;
    public AbstractC253919f A05;
    public C16630pM A06;
    public C252718t A07;

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        WaEditText waEditText;
        super.A0s();
        GifSearchContainer gifSearchContainer = (GifSearchContainer) ((AnonymousClass01E) this).A0A;
        if (gifSearchContainer != null && (waEditText = gifSearchContainer.A07) != null) {
            waEditText.A04(false);
        }
    }

    @Override // com.whatsapp.picker.search.PickerSearchDialogFragment, X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        super.A10(bundle, layoutInflater, viewGroup);
        GifSearchContainer gifSearchContainer = (GifSearchContainer) C12960it.A0F(layoutInflater, viewGroup, R.layout.gif_search_dialog);
        gifSearchContainer.A00 = 48;
        C14850m9 r5 = this.A02;
        C253719d r8 = this.A04;
        C252718t r12 = this.A07;
        C16120oU r6 = this.A03;
        gifSearchContainer.A01(A0C(), this.A00, this.A01, r5, r6, null, r8, this.A05, this, this.A06, r12);
        gifSearchContainer.A0F = this;
        return gifSearchContainer;
    }

    @Override // X.AnonymousClass5UF
    public void AR4(C66013Ly r2) {
        C69893aP r0 = ((PickerSearchDialogFragment) this).A00;
        if (r0 != null) {
            r0.AR4(r2);
        }
    }
}
