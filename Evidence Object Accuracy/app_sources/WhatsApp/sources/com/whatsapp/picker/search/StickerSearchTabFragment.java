package com.whatsapp.picker.search;

import X.AbstractC116245Ur;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass02B;
import X.AnonymousClass1AB;
import X.AnonymousClass1KS;
import X.AnonymousClass2B7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14850m9;
import X.C15260mp;
import X.C53922fo;
import X.C54392ge;
import X.C54812hK;
import X.C63443Bp;
import X.C69893aP;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.picker.search.StickerSearchDialogFragment;
import com.whatsapp.picker.search.StickerSearchTabFragment;
import java.util.List;

/* loaded from: classes2.dex */
public class StickerSearchTabFragment extends Hilt_StickerSearchTabFragment implements AbstractC116245Ur {
    public ViewTreeObserver.OnGlobalLayoutListener A00;
    public RecyclerView A01;
    public C14850m9 A02;
    public C54392ge A03;

    public static StickerSearchTabFragment A00(int i) {
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("sticker_category_tab", i);
        StickerSearchTabFragment stickerSearchTabFragment = new StickerSearchTabFragment();
        stickerSearchTabFragment.A0U(A0D);
        return stickerSearchTabFragment;
    }

    @Override // X.AnonymousClass01E
    public void A0r() {
        C54392ge r0 = this.A03;
        if (r0 != null) {
            r0.A04 = false;
            r0.A02();
        }
        super.A0r();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        AnonymousClass1AB r8;
        AnonymousClass2B7 r0;
        Context A01 = A01();
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.sticker_search_tab_results);
        this.A01 = (RecyclerView) A0F.findViewById(R.id.tab_result);
        AnonymousClass01E r6 = this.A0D;
        if (r6 instanceof StickerSearchDialogFragment) {
            StickerSearchDialogFragment stickerSearchDialogFragment = (StickerSearchDialogFragment) r6;
            C69893aP r4 = ((PickerSearchDialogFragment) stickerSearchDialogFragment).A00;
            AnonymousClass009.A05(r4);
            List A0l = C12960it.A0l();
            Bundle bundle2 = this.A05;
            if (bundle2 != null) {
                int i = bundle2.getInt("sticker_category_tab");
                C53922fo r02 = stickerSearchDialogFragment.A0B;
                if (r02 != null) {
                    r02.A00.A05(A0G(), new AnonymousClass02B(stickerSearchDialogFragment, this, i) { // from class: X.4uA
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ StickerSearchDialogFragment A01;
                        public final /* synthetic */ StickerSearchTabFragment A02;

                        {
                            this.A02 = r2;
                            this.A01 = r1;
                            this.A00 = r3;
                        }

                        @Override // X.AnonymousClass02B
                        public final void ANq(Object obj) {
                            StickerSearchTabFragment stickerSearchTabFragment = this.A02;
                            StickerSearchDialogFragment stickerSearchDialogFragment2 = this.A01;
                            int i2 = this.A00;
                            C54392ge r1 = stickerSearchTabFragment.A03;
                            if (r1 != null) {
                                r1.A0E(stickerSearchDialogFragment2.A1J(i2));
                                stickerSearchTabFragment.A03.A02();
                            }
                        }
                    });
                }
                A0l = stickerSearchDialogFragment.A1J(i);
            }
            C15260mp r03 = r4.A00;
            if (r03 == null || (r0 = r03.A0A) == null) {
                r8 = null;
            } else {
                r8 = r0.A09;
            }
            C54392ge r62 = new C54392ge(A01, r8, this, C12960it.A0V(), A0l);
            this.A03 = r62;
            this.A01.setAdapter(r62);
            C63443Bp r63 = new C63443Bp(A01, viewGroup, this.A01, this.A03);
            this.A00 = r63.A07;
            A0F.getViewTreeObserver().addOnGlobalLayoutListener(this.A00);
            this.A01.A0n(new C54812hK(A02(), r63.A08, this.A02));
            return A0F;
        }
        throw C12990iw.A0m("Parent fragment of StickerSearchTabFragment is not of type StickerSearchDialogFragment");
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A01.getViewTreeObserver().removeGlobalOnLayoutListener(this.A00);
        List list = this.A01.A0b;
        if (list != null) {
            list.clear();
        }
        super.A12();
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        C54392ge r0 = this.A03;
        if (r0 != null) {
            r0.A04 = true;
            r0.A02();
        }
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r3, Integer num, int i) {
        AnonymousClass01E r1 = this.A0D;
        if (r1 instanceof StickerSearchDialogFragment) {
            ((StickerSearchDialogFragment) r1).AWl(r3, num, i);
            return;
        }
        throw C12990iw.A0m("Parent fragment of StickerSearchTabFragment is not of type StickerSearchDialogFragment");
    }
}
