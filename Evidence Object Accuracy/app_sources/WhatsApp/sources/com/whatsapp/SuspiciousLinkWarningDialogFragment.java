package com.whatsapp;

import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass02S;
import X.AnonymousClass18U;
import X.AnonymousClass1BD;
import X.AnonymousClass1US;
import X.AnonymousClass35V;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14900mE;
import X.C252018m;
import X.C52162aM;
import X.C58272oQ;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import java.util.AbstractCollection;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
public class SuspiciousLinkWarningDialogFragment extends Hilt_SuspiciousLinkWarningDialogFragment {
    public C14900mE A00;
    public AnonymousClass18U A01;
    public AnonymousClass01d A02;
    public AnonymousClass018 A03;
    public AnonymousClass1BD A04;
    public AnonymousClass35V A05;
    public C252018m A06;
    public String A07;

    public SuspiciousLinkWarningDialogFragment() {
    }

    public SuspiciousLinkWarningDialogFragment(AnonymousClass35V r4, String str, String str2, Set set) {
        this.A05 = r4;
        this.A07 = str2;
        Bundle A0D = C12970iu.A0D();
        A0D.putString("url", str);
        A0D.putSerializable("phishingChars", new HashSet(set));
        A0U(A0D);
    }

    public static SuspiciousLinkWarningDialogFragment A00(String str, Set set) {
        SuspiciousLinkWarningDialogFragment suspiciousLinkWarningDialogFragment = new SuspiciousLinkWarningDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("url", str);
        A0D.putSerializable("phishingChars", new HashSet(set));
        suspiciousLinkWarningDialogFragment.A0U(A0D);
        return suspiciousLinkWarningDialogFragment;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        TextView textView = (TextView) ((DialogFragment) this).A03.findViewById(16908299);
        if (textView != null) {
            C52162aM.A00(textView);
            C12960it.A0s(A0B(), textView, R.color.suspicious_link_dialog_message_color);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        SpannableString spannableString;
        int length;
        String string = A03().getString("url");
        AbstractCollection abstractCollection = (AbstractCollection) A03().getSerializable("phishingChars");
        boolean z = true;
        z = true;
        z = true;
        SpannableStringBuilder A0J = C12990iw.A0J(Html.fromHtml(C12970iu.A0q(this, C252018m.A00(this.A06, "26000162"), new Object[1], 0, R.string.suspicious_link_dialog_description)));
        Object[] objArr = (URLSpan[]) A0J.getSpans(0, A0J.length(), URLSpan.class);
        if (objArr != null) {
            for (URLSpan uRLSpan : objArr) {
                A0J.setSpan(new C58272oQ(A0p(), this.A01, this.A00, this.A02, uRLSpan.getURL()), A0J.getSpanStart(uRLSpan), A0J.getSpanEnd(uRLSpan), A0J.getSpanFlags(uRLSpan));
            }
            for (Object obj : objArr) {
                A0J.removeSpan(obj);
            }
        }
        A0J.append("\n\n");
        if (!TextUtils.isEmpty(string) && abstractCollection != null) {
            ForegroundColorSpan A0M = C12980iv.A0M(A0p(), R.color.suspicious_link_text_background_color);
            if (string.codePointCount(0, string.length()) > 96) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(AnonymousClass1US.A03(96, string));
                spannableString = new SpannableString(C12960it.A0d("…", A0h));
            } else {
                spannableString = new SpannableString(string);
            }
            String[] split = Uri.parse(string).getHost().split("\\.");
            int i = -1;
            for (String str : split) {
                int i2 = 0;
                boolean z2 = false;
                int i3 = -1;
                while (true) {
                    length = str.length();
                    if (i2 >= length) {
                        break;
                    }
                    int codePointAt = str.codePointAt(i2);
                    int charCount = Character.charCount(codePointAt);
                    if (abstractCollection.contains(Integer.valueOf(codePointAt))) {
                        i3 = string.indexOf(codePointAt, i3 + 1);
                        int i4 = z ? 1 : 0;
                        int i5 = z ? 1 : 0;
                        int i6 = z ? 1 : 0;
                        int i7 = z ? 1 : 0;
                        int i8 = z ? 1 : 0;
                        spannableString.setSpan(new StyleSpan(i4), i3, i3 + charCount, 33);
                        z2 = true;
                    }
                    i2 += charCount;
                    z = true;
                }
                if (z2) {
                    i = string.indexOf(str, i + 1);
                    spannableString.setSpan(A0M, i, length + i, 33);
                }
            }
            AnonymousClass02S A03 = this.A03.A03();
            A0J.append(A03.A02(A03.A01, spannableString));
        }
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.suspicious_link_dialog_title);
        A0O.A0A(A0J);
        A0O.A0B(z);
        A0O.setNegativeButton(R.string.suspicious_link_warning_negative_button_text, new DialogInterface.OnClickListener(string) { // from class: X.3KK
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i9) {
                SuspiciousLinkWarningDialogFragment suspiciousLinkWarningDialogFragment = SuspiciousLinkWarningDialogFragment.this;
                suspiciousLinkWarningDialogFragment.A01.Ab9(suspiciousLinkWarningDialogFragment.A0p(), Uri.parse(this.A01));
                AnonymousClass1BD.A0K.put(suspiciousLinkWarningDialogFragment.A07, C12970iu.A0g());
            }
        });
        C12970iu.A1L(A0O, this, 4, R.string.suspicious_link_warning_positive_button_text);
        return A0O.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        AnonymousClass35V r0 = this.A05;
        if (r0 != null) {
            r0.A05();
        }
    }
}
