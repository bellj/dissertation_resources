package com.whatsapp;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15460nI;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass247;
import X.C004802e;
import X.C14820m6;
import X.C14830m7;
import X.C14900mE;
import X.C15380n4;
import X.C15450nH;
import X.C15470nJ;
import X.C15550nR;
import X.C15860o1;
import X.C16170oZ;
import X.C21320xE;
import X.C22330yu;
import X.C38131nZ;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.whatsapp.MuteDialogFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* loaded from: classes2.dex */
public class MuteDialogFragment extends Hilt_MuteDialogFragment {
    public C14900mE A00;
    public C15450nH A01;
    public C16170oZ A02;
    public C22330yu A03;
    public C15550nR A04;
    public C14830m7 A05;
    public C14820m6 A06;
    public AnonymousClass018 A07;
    public C21320xE A08;
    public C15860o1 A09;
    public AbstractC14440lR A0A;

    public static MuteDialogFragment A00(AbstractC14640lm r4) {
        MuteDialogFragment muteDialogFragment = new MuteDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", r4.getRawString());
        muteDialogFragment.A0U(bundle);
        return muteDialogFragment;
    }

    public static MuteDialogFragment A01(Collection collection) {
        MuteDialogFragment muteDialogFragment = new MuteDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("jids", C15380n4.A06(collection));
        bundle.putBoolean("mute_in_conversation_fragment", true);
        muteDialogFragment.A0U(bundle);
        return muteDialogFragment;
    }

    public static /* synthetic */ void A02(MuteDialogFragment muteDialogFragment, List list) {
        Bundle bundle;
        AbstractC14640lm A01;
        if (list == null && (bundle = ((AnonymousClass01E) muteDialogFragment).A05) != null && (A01 = AbstractC14640lm.A01(bundle.getString("jid"))) != null) {
            muteDialogFragment.A08.A07(A01);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        List A07;
        int[] iArr;
        int[] iArr2;
        boolean z;
        AbstractC14640lm A01 = AbstractC14640lm.A01(A03().getString("jid"));
        ArrayList<String> stringArrayList = A03().getStringArrayList("jids");
        if (stringArrayList == null) {
            A07 = null;
        } else {
            A07 = C15380n4.A07(AbstractC14640lm.class, stringArrayList);
        }
        boolean z2 = A03().getBoolean("mute_in_conversation_fragment");
        int[] iArr3 = new int[1];
        iArr3[0] = this.A06.A00.getInt("last_mute_selection", 0);
        C15450nH r0 = this.A01;
        C15470nJ r1 = AbstractC15460nI.A0m;
        if (r0.A05(r1)) {
            iArr = AnonymousClass247.A00;
        } else {
            iArr = AnonymousClass247.A02;
        }
        if (this.A01.A05(r1)) {
            iArr2 = AnonymousClass247.A01;
        } else {
            iArr2 = AnonymousClass247.A03;
        }
        int length = iArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = C38131nZ.A02(this.A07, iArr[i], iArr2[i]);
        }
        if (iArr3[0] >= length) {
            iArr3[0] = 0;
        }
        View inflate = A0C().getLayoutInflater().inflate(R.layout.mute_notifications, (ViewGroup) null, false);
        CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.mute_show_notifications);
        if (A01 == null || !this.A09.A0S(A01)) {
            z = this.A06.A00.getBoolean("last_mute_show_notifications", false);
        } else {
            z = this.A09.A08(A01.getRawString()).A0G;
        }
        checkBox.setChecked(z);
        C004802e r3 = new C004802e(A0B());
        r3.A07(R.string.mute_dialog_title);
        r3.A09(new DialogInterface.OnClickListener(iArr3) { // from class: X.4fN
            public final /* synthetic */ int[] A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                this.A00[0] = i2;
            }
        }, strArr, iArr3[0]);
        r3.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(checkBox, this, A01, A07, iArr3, z2) { // from class: X.3Kx
            public final /* synthetic */ CheckBox A00;
            public final /* synthetic */ MuteDialogFragment A01;
            public final /* synthetic */ AbstractC14640lm A02;
            public final /* synthetic */ List A03;
            public final /* synthetic */ boolean A04;
            public final /* synthetic */ int[] A05;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A05 = r5;
                this.A04 = r6;
                this.A03 = r4;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                int[] iArr4;
                long currentTimeMillis;
                MuteDialogFragment muteDialogFragment = this.A01;
                CheckBox checkBox2 = this.A00;
                int[] iArr5 = this.A05;
                boolean z3 = this.A04;
                List list = this.A03;
                AbstractC14640lm r7 = this.A02;
                boolean isChecked = checkBox2.isChecked();
                int i3 = iArr5[0];
                if (muteDialogFragment.A01.A05(AbstractC15460nI.A0m)) {
                    iArr4 = AnonymousClass247.A05;
                } else {
                    iArr4 = AnonymousClass247.A04;
                }
                int i4 = iArr4[i3];
                if (i4 == -1) {
                    currentTimeMillis = -1;
                } else {
                    currentTimeMillis = System.currentTimeMillis() + (((long) i4) * 60000);
                }
                C14820m6 r02 = muteDialogFragment.A06;
                C12960it.A0t(C12960it.A08(r02).putInt("last_mute_selection", iArr5[0]), "last_mute_show_notifications", isChecked);
                muteDialogFragment.A0A.Ab2(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0040: INVOKE  
                      (wrap: X.0lR : 0x0039: IGET  (r0v11 X.0lR A[REMOVE]) = (r6v0 'muteDialogFragment' com.whatsapp.MuteDialogFragment) com.whatsapp.MuteDialogFragment.A0A X.0lR)
                      (wrap: X.2j2 : 0x003d: CONSTRUCTOR  (r5v1 X.2j2 A[REMOVE]) = 
                      (r6v0 'muteDialogFragment' com.whatsapp.MuteDialogFragment)
                      (r7v0 'r7' X.0lm)
                      (r8v0 'list' java.util.List)
                      (r9v2 'currentTimeMillis' long)
                      (r11v0 'z3' boolean)
                      (r12v0 'isChecked' boolean)
                     call: X.2j2.<init>(com.whatsapp.MuteDialogFragment, X.0lm, java.util.List, long, boolean, boolean):void type: CONSTRUCTOR)
                     type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.3Kx.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003d: CONSTRUCTOR  (r5v1 X.2j2 A[REMOVE]) = 
                      (r6v0 'muteDialogFragment' com.whatsapp.MuteDialogFragment)
                      (r7v0 'r7' X.0lm)
                      (r8v0 'list' java.util.List)
                      (r9v2 'currentTimeMillis' long)
                      (r11v0 'z3' boolean)
                      (r12v0 'isChecked' boolean)
                     call: X.2j2.<init>(com.whatsapp.MuteDialogFragment, X.0lm, java.util.List, long, boolean, boolean):void type: CONSTRUCTOR in method: X.3Kx.onClick(android.content.DialogInterface, int):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.2j2, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    com.whatsapp.MuteDialogFragment r6 = r13.A01
                    android.widget.CheckBox r0 = r13.A00
                    int[] r4 = r13.A05
                    boolean r11 = r13.A04
                    java.util.List r8 = r13.A03
                    X.0lm r7 = r13.A02
                    boolean r12 = r0.isChecked()
                    r5 = 0
                    r2 = r4[r5]
                    X.0nH r1 = r6.A01
                    X.0nJ r0 = X.AbstractC15460nI.A0m
                    boolean r0 = r1.A05(r0)
                    if (r0 == 0) goto L_0x004f
                    int[] r0 = X.AnonymousClass247.A05
                L_0x001f:
                    r1 = r0[r2]
                    r0 = -1
                    if (r1 != r0) goto L_0x0044
                    r9 = -1
                L_0x0026:
                    X.0m6 r0 = r6.A06
                    r2 = r4[r5]
                    android.content.SharedPreferences$Editor r1 = X.C12960it.A08(r0)
                    java.lang.String r0 = "last_mute_selection"
                    android.content.SharedPreferences$Editor r1 = r1.putInt(r0, r2)
                    java.lang.String r0 = "last_mute_show_notifications"
                    X.C12960it.A0t(r1, r0, r12)
                    X.0lR r0 = r6.A0A
                    X.2j2 r5 = new X.2j2
                    r5.<init>(r6, r7, r8, r9, r11, r12)
                    r0.Ab2(r5)
                    return
                L_0x0044:
                    long r9 = java.lang.System.currentTimeMillis()
                    long r0 = (long) r1
                    r2 = 60000(0xea60, double:2.9644E-319)
                    long r0 = r0 * r2
                    long r9 = r9 + r0
                    goto L_0x0026
                L_0x004f:
                    int[] r0 = X.AnonymousClass247.A04
                    goto L_0x001f
                */
                throw new UnsupportedOperationException("Method not decompiled: X.DialogInterface$OnClickListenerC65763Kx.onClick(android.content.DialogInterface, int):void");
            }
        });
        r3.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(A07) { // from class: X.4gR
            public final /* synthetic */ List A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                MuteDialogFragment.A02(MuteDialogFragment.this, this.A01);
            }
        });
        r3.setView(inflate);
        return r3.create();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        Bundle bundle;
        AbstractC14640lm A01;
        if (A03().getString("jids") == null && (bundle = ((AnonymousClass01E) this).A05) != null && (A01 = AbstractC14640lm.A01(bundle.getString("jid"))) != null) {
            this.A08.A07(A01);
        }
    }
}
