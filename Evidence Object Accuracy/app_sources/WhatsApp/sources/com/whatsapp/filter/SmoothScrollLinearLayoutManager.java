package com.whatsapp.filter;

import X.AbstractC05520Pw;
import X.C05480Ps;
import X.C74723ig;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes3.dex */
public class SmoothScrollLinearLayoutManager extends LinearLayoutManager {
    public SmoothScrollLinearLayoutManager() {
        super(0);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public void A0x(C05480Ps r3, RecyclerView recyclerView, int i) {
        C74723ig r0 = new C74723ig(recyclerView.getContext(), this);
        ((AbstractC05520Pw) r0).A00 = i;
        A0R(r0);
    }
}
