package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass02M;
import X.AnonymousClass03U;
import X.AnonymousClass0MU;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2TX;
import X.AnonymousClass3S0;
import X.C54412gg;
import X.C54612h0;
import X.C73573gS;
import X.C74693iW;
import X.C74823iq;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes2.dex */
public class StickyHeadersRecyclerView extends RecyclerView implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public AnonymousClass0MU A07;
    public AnonymousClass03U A08;
    public AnonymousClass018 A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;
    public final Rect A0C;

    public StickyHeadersRecyclerView(Context context) {
        super(context);
        A0y();
        this.A02 = -1;
        this.A00 = -1;
        this.A0C = new Rect();
        A0z(context, null);
    }

    public StickyHeadersRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0y();
        this.A02 = -1;
        this.A00 = -1;
        this.A0C = new Rect();
        A0z(context, attributeSet);
    }

    public StickyHeadersRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0y();
        this.A02 = -1;
        this.A00 = -1;
        this.A0C = new Rect();
        A0z(context, attributeSet);
    }

    public StickyHeadersRecyclerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A0y();
    }

    public void A0y() {
        if (!this.A0B) {
            this.A0B = true;
            this.A09 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    public final void A0z(Context context, AttributeSet attributeSet) {
        int i = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0M);
            this.A03 = obtainStyledAttributes.getDimensionPixelSize(0, this.A03);
            i = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            obtainStyledAttributes.recycle();
        }
        if (this.A03 > 0) {
            A0l(new C54612h0(this.A09, i));
        }
        this.A0h = true;
        this.A07 = new AnonymousClass0MU(context, new C73573gS(this));
        this.A14.add(new AnonymousClass3S0(this));
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) getLayoutManager();
        C54412gg r8 = (C54412gg) this.A0N;
        int A1A = linearLayoutManager.A1A();
        if (A1A != -1) {
            if (A1A != this.A00) {
                this.A00 = A1A;
                long A0E = r8.A0E(A1A);
                this.A05 = A0E;
                int i = (int) (A0E >> 32);
                if (this.A02 != i) {
                    this.A02 = i;
                    ((AnonymousClass2TX) r8.A00).ANF(this.A08, i);
                    View view = this.A08.A0H;
                    view.measure(View.MeasureSpec.makeMeasureSpec(getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
                    view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
                }
            }
            this.A04 = 0;
            if ((this.A05 & 4294967295L) != 4294967295L) {
                int A19 = linearLayoutManager.A19();
                if (this.A01 != A19) {
                    this.A01 = A19;
                    this.A06 = r8.A0E(A19);
                }
                if ((this.A06 & 4294967295L) == 4294967295L) {
                    View A0C = linearLayoutManager.A0C(A19);
                    this.A04 = A0C.getTop() - this.A08.A0H.getMeasuredHeight();
                    Rect rect = this.A0C;
                    linearLayoutManager.A0J(A0C, rect);
                    int i2 = this.A04 - rect.top;
                    this.A04 = i2;
                    if (i2 > 0) {
                        this.A04 = 0;
                    }
                }
            }
            canvas.save();
            canvas.clipRect(0, this.A04, this.A08.A0H.getMeasuredWidth(), this.A04 + this.A08.A0H.getMeasuredHeight());
            canvas.translate(0.0f, (float) this.A04);
            this.A08.A0H.draw(canvas);
            canvas.restore();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    private C54412gg getStickyHeadersAdapter() {
        return (C54412gg) this.A0N;
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onMeasure(int i, int i2) {
        View view;
        super.onMeasure(i, i2);
        if (this.A03 > 0) {
            int measuredWidth = getMeasuredWidth();
            int i3 = this.A03;
            ((GridLayoutManager) getLayoutManager()).A1h(Math.max(1, (measuredWidth + (i3 >> 1)) / i3));
        }
        AnonymousClass03U r0 = this.A08;
        if (r0 != null && (view = r0.A0H) != null) {
            view.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(AnonymousClass02M r5) {
        super.setAdapter(new C54412gg(r5));
        AnonymousClass02M r0 = this.A0N;
        r0.A01.registerObserver(new C74823iq(this));
        if (this.A03 > 0) {
            int measuredWidth = getMeasuredWidth();
            int i = this.A03;
            int max = Math.max(1, (measuredWidth + (i >> 1)) / i);
            getContext();
            GridLayoutManager gridLayoutManager = new GridLayoutManager(max);
            gridLayoutManager.A01 = new C74693iW(gridLayoutManager, this);
            setLayoutManager(gridLayoutManager);
        } else {
            getContext();
            setLayoutManager(new LinearLayoutManager(1));
        }
        this.A08 = this.A0N.AOl(this, -1000);
    }
}
