package com.whatsapp.phoneid;

import X.AbstractC51752Yp;
import X.AnonymousClass01J;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C19370u0;
import android.content.Context;
import android.content.Intent;

/* loaded from: classes3.dex */
public class PhoneIdRequestReceiver extends AbstractC51752Yp {
    public C19370u0 A00;
    public final Object A01;
    public volatile boolean A02;

    public PhoneIdRequestReceiver() {
        this(0);
    }

    public PhoneIdRequestReceiver(int i) {
        this.A02 = false;
        this.A01 = C12970iu.A0l();
    }

    @Override // X.AbstractC51752Yp, android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = (C19370u0) ((AnonymousClass01J) AnonymousClass22D.A00(context)).AFi.get();
                    this.A02 = true;
                }
            }
        }
        super.onReceive(context, intent);
    }
}
