package com.whatsapp.workers.ntp;

import X.AnonymousClass01J;
import X.AnonymousClass043;
import X.C12990iw;
import X.C14830m7;
import X.C17060qC;
import X.C20250vS;
import android.content.Context;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/* loaded from: classes2.dex */
public class NtpSyncWorker extends Worker {
    public static volatile long A04;
    public C17060qC A00;
    public final Context A01;
    public final C14830m7 A02;
    public final C20250vS A03;

    public NtpSyncWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.A01 = context;
        AnonymousClass01J A0S = C12990iw.A0S(context.getApplicationContext());
        this.A00 = (C17060qC) A0S.ADe.get();
        this.A03 = (C20250vS) A0S.A66.get();
        this.A02 = A0S.Aen();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02a9  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02b5  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0154 A[Catch: all -> 0x02b8, TryCatch #5 {all -> 0x02b8, blocks: (B:17:0x0058, B:18:0x005b, B:19:0x0067, B:20:0x0075, B:22:0x007b, B:37:0x0147, B:40:0x014c, B:42:0x0150, B:44:0x0154, B:45:0x0157, B:46:0x015e, B:48:0x0163, B:50:0x0167, B:52:0x017a, B:53:0x0191, B:55:0x0195, B:57:0x019c, B:58:0x01a2, B:64:0x01d8, B:68:0x01e4, B:71:0x01ee, B:72:0x01f2, B:74:0x01f8, B:83:0x020e, B:85:0x0219, B:86:0x021e, B:88:0x0224, B:89:0x0227, B:90:0x022c, B:91:0x0236, B:92:0x023b, B:94:0x0241, B:95:0x0247, B:96:0x024a, B:97:0x0256, B:98:0x0259, B:100:0x025d, B:102:0x0282, B:23:0x0081, B:26:0x00d4, B:28:0x00e5, B:29:0x00eb, B:30:0x00fa, B:32:0x010a, B:34:0x011f, B:36:0x0134), top: B:120:0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x020d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass043 A00(android.content.Context r21, X.C14830m7 r22, X.C20250vS r23, X.C17060qC r24) {
        /*
        // Method dump skipped, instructions count: 701
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.workers.ntp.NtpSyncWorker.A00(android.content.Context, X.0m7, X.0vS, X.0qC):X.043");
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        C17060qC r3 = this.A00;
        return A00(this.A01, this.A02, this.A03, r3);
    }
}
