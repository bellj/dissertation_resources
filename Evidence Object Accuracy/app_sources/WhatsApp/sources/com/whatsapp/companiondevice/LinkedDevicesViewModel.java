package com.whatsapp.companiondevice;

import X.AbstractC14440lR;
import X.AnonymousClass014;
import X.AnonymousClass01I;
import X.AnonymousClass139;
import X.AnonymousClass1UU;
import X.AnonymousClass37U;
import X.AnonymousClass3WC;
import X.C12960it;
import X.C13000ix;
import X.C14840m8;
import X.C14860mA;
import X.C14900mE;
import X.C22100yW;
import X.C27691It;
import X.C71073cL;
import android.app.Application;
import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.Comparator;
import java.util.List;

/* loaded from: classes2.dex */
public class LinkedDevicesViewModel extends AnonymousClass014 {
    public List A00 = C12960it.A0l();
    public final C14900mE A01;
    public final C22100yW A02;
    public final AnonymousClass139 A03;
    public final C14840m8 A04;
    public final C27691It A05 = C13000ix.A03();
    public final C27691It A06 = C13000ix.A03();
    public final C27691It A07 = C13000ix.A03();
    public final C27691It A08 = C13000ix.A03();
    public final AbstractC14440lR A09;
    public final AnonymousClass1UU A0A = new C71073cL(this);
    public final C14860mA A0B;
    public final Comparator A0C = new IDxComparatorShape3S0000000_2_I1(13);

    public LinkedDevicesViewModel(Application application, C14900mE r4, C22100yW r5, AnonymousClass139 r6, C14840m8 r7, AbstractC14440lR r8, C14860mA r9) {
        super(application);
        this.A01 = r4;
        this.A09 = r8;
        this.A0B = r9;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C14860mA r0 = this.A0B;
        r0.A0R.remove(this.A0A);
    }

    public void A04() {
        if (AnonymousClass01I.A01()) {
            AbstractC14440lR r5 = this.A09;
            C14860mA r4 = this.A0B;
            C12960it.A1E(new AnonymousClass37U(new AnonymousClass3WC(this), this.A02, this.A03, r4), r5);
            return;
        }
        C14900mE.A01(this.A01, this, 18);
    }
}
