package com.whatsapp.companiondevice;

import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02M;
import X.AnonymousClass0QE;
import X.AnonymousClass10B;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass139;
import X.AnonymousClass16S;
import X.AnonymousClass1AT;
import X.AnonymousClass1I1;
import X.AnonymousClass1JU;
import X.AnonymousClass1UU;
import X.AnonymousClass2AC;
import X.AnonymousClass2FL;
import X.AnonymousClass2I2;
import X.AnonymousClass3CN;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C14830m7;
import X.C14840m8;
import X.C14850m9;
import X.C14860mA;
import X.C14880mC;
import X.C14890mD;
import X.C14900mE;
import X.C16120oU;
import X.C18640sm;
import X.C18850tA;
import X.C22100yW;
import X.C22130yZ;
import X.C22230yk;
import X.C232310w;
import X.C233411h;
import X.C233711k;
import X.C238813j;
import X.C245716a;
import X.C245916c;
import X.C252018m;
import X.C27691It;
import X.C54402gf;
import X.C61332zr;
import X.C63453Bq;
import X.C65303Iz;
import X.C74843is;
import X.DialogInterface$OnClickListenerC97494hA;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class LinkedDevicesActivity extends ActivityC13790kL implements DialogInterface.OnDismissListener {
    public RecyclerView A00;
    public AnonymousClass10B A01;
    public C54402gf A02;
    public LinkedDevicesDetailDialogFragment A03;
    public LinkedDevicesSharedViewModel A04;
    public C63453Bq A05;
    public LinkedDevicesViewModel A06;
    public C232310w A07;
    public AnonymousClass16S A08;
    public C18850tA A09;
    public C233411h A0A;
    public C233711k A0B;
    public AnonymousClass2I2 A0C;
    public C22100yW A0D;
    public C22130yZ A0E;
    public C245916c A0F;
    public C245716a A0G;
    public C238813j A0H;
    public C16120oU A0I;
    public AnonymousClass139 A0J;
    public C14840m8 A0K;
    public C22230yk A0L;
    public AnonymousClass1AT A0M;
    public AnonymousClass12U A0N;
    public C252018m A0O;
    public C14890mD A0P;
    public C14860mA A0Q;
    public boolean A0R;
    public final AnonymousClass0QE A0S;

    public LinkedDevicesActivity() {
        this(0);
        this.A0S = new C74843is(this);
    }

    public LinkedDevicesActivity(int i) {
        this.A0R = false;
        ActivityC13830kP.A1P(this, 48);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0R) {
            this.A0R = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0P = (C14890mD) A1M.ANL.get();
            this.A0Q = (C14860mA) A1M.ANU.get();
            this.A0N = ActivityC13830kP.A1N(A1M);
            this.A0I = C12970iu.A0b(A1M);
            this.A09 = (C18850tA) A1M.AKx.get();
            this.A0F = (C245916c) A1M.A5j.get();
            this.A0O = C12980iv.A0g(A1M);
            this.A0L = (C22230yk) A1M.ANT.get();
            this.A0A = (C233411h) A1M.AKz.get();
            this.A0C = A1L.A05();
            this.A0K = (C14840m8) A1M.ACi.get();
            this.A0G = (C245716a) A1M.AJR.get();
            this.A0E = (C22130yZ) A1M.A5d.get();
            this.A0D = (C22100yW) A1M.A3g.get();
            this.A0B = (C233711k) A1M.ALC.get();
            this.A07 = (C232310w) A1M.AL2.get();
            this.A0H = (C238813j) A1M.ABy.get();
            this.A08 = (AnonymousClass16S) A1M.AL4.get();
            this.A01 = (AnonymousClass10B) A1M.A5i.get();
            this.A0M = (AnonymousClass1AT) A1M.AG2.get();
            this.A0J = (AnonymousClass139) A1M.A9o.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1.A03.isEmpty() == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2e() {
        /*
            r5 = this;
            X.2gf r1 = r5.A02
            java.util.List r0 = r1.A00
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0013
            java.util.List r0 = r1.A03
            boolean r0 = r0.isEmpty()
            r4 = 0
            if (r0 != 0) goto L_0x0014
        L_0x0013:
            r4 = 1
        L_0x0014:
            X.2gf r0 = r5.A02
            boolean r3 = r0.A05
            android.content.Intent r2 = X.C12970iu.A0A()
            java.lang.String r1 = r5.getPackageName()
            java.lang.String r0 = "com.whatsapp.companiondevice.optin.ui.OptInActivity"
            android.content.Intent r1 = r2.setClassName(r1, r0)
            java.lang.String r0 = "arg_has_devices_linked"
            r1.putExtra(r0, r4)
            java.lang.String r0 = "arg_has_portal_device_linked"
            r1.putExtra(r0, r3)
            r0 = 100
            r5.startActivityForResult(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.companiondevice.LinkedDevicesActivity.A2e():void");
    }

    public final void A2f(List list) {
        boolean z;
        if (!isFinishing() && list != null) {
            C54402gf r5 = this.A02;
            List list2 = r5.A00;
            list2.clear();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1JU r0 = (AnonymousClass1JU) it.next();
                C61332zr r2 = new C61332zr(r0);
                Boolean bool = (Boolean) r5.A04.get(r0.A05);
                if (bool != null) {
                    boolean booleanValue = bool.booleanValue();
                    z = true;
                    if (booleanValue) {
                        r2.A00 = z;
                        list2.add(r2);
                    }
                }
                z = false;
                r2.A00 = z;
                list2.add(r2);
            }
            r5.A0E();
            r5.A02();
            LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment = this.A03;
            if (linkedDevicesDetailDialogFragment != null && linkedDevicesDetailDialogFragment.A08 != null) {
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    AnonymousClass1JU r22 = (AnonymousClass1JU) it2.next();
                    if (r22.A05.equals(this.A03.A08.A05)) {
                        LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment2 = this.A03;
                        linkedDevicesDetailDialogFragment2.A08 = r22;
                        linkedDevicesDetailDialogFragment2.A0E = null;
                        linkedDevicesDetailDialogFragment2.A0B = null;
                        if (linkedDevicesDetailDialogFragment2.A01 != null) {
                            linkedDevicesDetailDialogFragment2.A1J();
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    public final void A2g(List list) {
        if (!isFinishing() && list != null) {
            C54402gf r0 = this.A02;
            r0.A03 = list;
            r0.A0E();
            r0.A02();
            LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment = this.A03;
            if (linkedDevicesDetailDialogFragment != null && linkedDevicesDetailDialogFragment.A0E != null) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    C14880mC r3 = (C14880mC) it.next();
                    String str = r3.A0F;
                    LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment2 = this.A03;
                    if (str.equals(linkedDevicesDetailDialogFragment2.A0E.A0F)) {
                        linkedDevicesDetailDialogFragment2.A0E = r3;
                        linkedDevicesDetailDialogFragment2.A08 = null;
                        linkedDevicesDetailDialogFragment2.A0B = null;
                        if (linkedDevicesDetailDialogFragment2.A01 != null) {
                            linkedDevicesDetailDialogFragment2.A1J();
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100) {
            this.A06.A04();
        } else if (i != 101) {
            if (i == 12345) {
                LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = this.A05.A05;
                if (i2 == -1 || i2 == 4) {
                    linkedDevicesSharedViewModel.A0Q.A0B(null);
                }
            }
        } else if (i2 == -1 && intent != null && intent.getBooleanExtra("has_removed_all_devices", false)) {
            Log.i("LinkedDevicesActivity/onActivityResult removedAllDevices");
            C14900mE r2 = ((ActivityC13810kN) this).A05;
            r2.A02.post(new RunnableBRunnable0Shape15S0100000_I1_1(this, 17));
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        C14900mE.A01(((ActivityC13810kN) this).A05, this, 16);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass1I1 r0;
        Boolean valueOf;
        AnonymousClass2AC r2;
        int i;
        super.onCreate(bundle);
        boolean A04 = C65303Iz.A04(((ActivityC13810kN) this).A06);
        int i2 = R.string.whatsapp_web;
        if (A04) {
            i2 = R.string.linked_devices_screen_title;
        }
        setTitle(i2);
        C12970iu.A0N(this).A0M(true);
        setContentView(R.layout.linked_devices_activity);
        this.A04 = (LinkedDevicesSharedViewModel) C13000ix.A02(this).A00(LinkedDevicesSharedViewModel.class);
        this.A06 = (LinkedDevicesViewModel) C13000ix.A02(this).A00(LinkedDevicesViewModel.class);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.linked_device_recycler_view);
        this.A00 = recyclerView;
        C12990iw.A1K(recyclerView);
        AnonymousClass3CN r4 = new AnonymousClass3CN(this);
        C14830m7 r7 = ((ActivityC13790kL) this).A05;
        C14850m9 r11 = ((ActivityC13810kN) this).A0C;
        C14900mE r3 = ((ActivityC13810kN) this).A05;
        C14860mA r14 = this.A0Q;
        AnonymousClass12P r22 = ((ActivityC13790kL) this).A00;
        C252018m r13 = this.A0O;
        AnonymousClass01d r6 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r8 = ((ActivityC13830kP) this).A01;
        C14840m8 r12 = this.A0K;
        C22130yZ r10 = this.A0E;
        C54402gf r1 = new C54402gf(r22, r3, r4, this.A0B, r6, r7, r8, this.A0D, r10, r11, r12, r13, r14);
        this.A02 = r1;
        this.A00.setAdapter(r1);
        C54402gf r02 = this.A02;
        ((AnonymousClass02M) r02).A01.registerObserver(this.A0S);
        C14850m9 r42 = ((ActivityC13810kN) this).A0C;
        C14900mE r142 = ((ActivityC13810kN) this).A05;
        AbstractC15710nm r132 = ((ActivityC13810kN) this).A03;
        AnonymousClass12U r32 = this.A0N;
        C63453Bq r122 = new C63453Bq(r132, r142, this, this.A02, ((ActivityC13810kN) this).A08, this.A0G, r42, r32);
        this.A05 = r122;
        LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = r122.A05;
        C27691It r15 = linkedDevicesSharedViewModel.A0N;
        ActivityC13790kL r23 = r122.A03;
        C12960it.A19(r23, r15, r122, 64);
        C12960it.A19(r23, linkedDevicesSharedViewModel.A0M, r122, 62);
        C12970iu.A1P(r23, linkedDevicesSharedViewModel.A0O, r122, 34);
        C12970iu.A1P(r23, linkedDevicesSharedViewModel.A0P, r122, 35);
        C12970iu.A1P(r23, linkedDevicesSharedViewModel.A0K, r122, 33);
        C12960it.A19(r23, linkedDevicesSharedViewModel.A0J, r122, 63);
        C12960it.A19(r23, linkedDevicesSharedViewModel.A0S, r122, 65);
        C12960it.A19(r23, linkedDevicesSharedViewModel.A04, r122, 61);
        C12970iu.A1P(r23, linkedDevicesSharedViewModel.A0L, r122, 32);
        C12960it.A17(this, this.A04.A0R, 27);
        C12960it.A17(this, this.A04.A0Q, 28);
        C12960it.A17(this, this.A06.A08, 30);
        C12960it.A17(this, this.A06.A07, 29);
        C12960it.A17(this, this.A06.A06, 31);
        C12960it.A18(this, this.A06.A05, 60);
        LinkedDevicesSharedViewModel linkedDevicesSharedViewModel2 = this.A04;
        C238813j r62 = linkedDevicesSharedViewModel2.A0F;
        r62.A02.execute(new RunnableBRunnable0Shape1S0300000_I0_1(r62, linkedDevicesSharedViewModel2.A0U, linkedDevicesSharedViewModel2.A05.A06, 2));
        C18640sm r24 = linkedDevicesSharedViewModel2.A0A;
        r24.A03(linkedDevicesSharedViewModel2.A09);
        linkedDevicesSharedViewModel2.A0D.A03(linkedDevicesSharedViewModel2.A0C);
        synchronized (r24.A05) {
            r0 = r24.A00;
        }
        if (r0 == null) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(r0.A04);
        }
        linkedDevicesSharedViewModel2.A00 = valueOf;
        LinkedDevicesViewModel linkedDevicesViewModel = this.A06;
        C14860mA r03 = linkedDevicesViewModel.A0B;
        AnonymousClass1UU r25 = linkedDevicesViewModel.A0A;
        List list = r03.A0R;
        if (!list.contains(r25)) {
            list.add(r25);
        }
        linkedDevicesViewModel.A04();
        C14840m8 r16 = this.A0K;
        if ((!r16.A04() || C12980iv.A1W(r16.A04.A00, "md_opt_in_first_time_experience_shown")) && C12980iv.A1W(((ActivityC13810kN) this).A09.A00, "md_opt_in_show_forced_dialog")) {
            C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "md_opt_in_show_forced_dialog", false);
            AnonymousClass2AC r26 = new AnonymousClass2AC();
            r26.A02 = R.layout.md_forced_opt_in_dialog;
            IDxCListenerShape8S0100000_1_I1 iDxCListenerShape8S0100000_1_I1 = new IDxCListenerShape8S0100000_1_I1(this, 15);
            r26.A04 = R.string.learn_more;
            r26.A07 = iDxCListenerShape8S0100000_1_I1;
            r26.A02(DialogInterface$OnClickListenerC97494hA.A00, R.string.ok_short);
            r26.A01().A1F(A0V(), "first_time_experience_dialog");
        }
        C14840m8 r17 = this.A0K;
        if (r17.A04() && !C12980iv.A1W(r17.A04.A00, "md_opt_in_first_time_experience_shown")) {
            C12960it.A0t(C12960it.A08(((ActivityC13810kN) this).A09), "md_opt_in_first_time_experience_shown", true);
            if (this.A0K.A02()) {
                r2 = new AnonymousClass2AC();
                r2.A02 = R.layout.md_forced_opt_in_first_time_dialog;
                IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(this, 25);
                r2.A04 = R.string.upgrade;
                r2.A07 = iDxCListenerShape9S0100000_2_I1;
                i = R.string.later;
            } else {
                r2 = new AnonymousClass2AC();
                r2.A02 = R.layout.md_opt_in_first_time_dialog;
                IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I12 = new IDxCListenerShape9S0100000_2_I1(this, 24);
                r2.A04 = R.string.learn_more;
                r2.A07 = iDxCListenerShape9S0100000_2_I12;
                i = R.string.ok_short;
            }
            r2.A02(DialogInterface$OnClickListenerC97494hA.A00, i);
            r2.A01().A1F(A0V(), "first_time_experience_dialog");
        }
        AnonymousClass10B r43 = this.A01;
        if (r43.A03()) {
            SharedPreferences sharedPreferences = r43.A04.A00;
            boolean z = sharedPreferences.getBoolean("adv_key_index_list_require_update", false);
            int i3 = sharedPreferences.getInt("adv_key_index_list_update_retry_count", 0);
            if (z || i3 > 0) {
                Log.i("DeviceKeyIndexListUpdateHandler/onDevicesLoadedOnScreen/updating");
                r43.A00();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C54402gf r0 = this.A02;
        ((AnonymousClass02M) r0).A01.unregisterObserver(this.A0S);
        LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = this.A04;
        linkedDevicesSharedViewModel.A0A.A04(linkedDevicesSharedViewModel.A09);
        C238813j r02 = linkedDevicesSharedViewModel.A0F;
        r02.A00.A02(linkedDevicesSharedViewModel.A0U);
        linkedDevicesSharedViewModel.A0D.A04(linkedDevicesSharedViewModel.A0C);
        super.onDestroy();
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        this.A03 = null;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        DialogFragment dialogFragment;
        LinkedDevicesDetailDialogFragment linkedDevicesDetailDialogFragment = this.A03;
        if (linkedDevicesDetailDialogFragment != null) {
            linkedDevicesDetailDialogFragment.A1C();
        }
        DialogFragment dialogFragment2 = (DialogFragment) A0V().A0A("first_time_experience_dialog");
        if (dialogFragment2 != null) {
            dialogFragment2.A1C();
        }
        AnonymousClass01E A0A = this.A05.A03.A0V().A0A("wifi_speed_bump_dialog");
        if ((A0A instanceof WifiSpeedBumpDialogFragment) && (dialogFragment = (DialogFragment) A0A) != null) {
            dialogFragment.A1C();
        }
        super.onSaveInstanceState(bundle);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = this.A04;
        linkedDevicesSharedViewModel.A0T.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(linkedDevicesSharedViewModel, 9));
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = this.A04;
        Runnable runnable = linkedDevicesSharedViewModel.A01;
        if (runnable != null) {
            linkedDevicesSharedViewModel.A0T.AaP(runnable);
        }
    }
}
