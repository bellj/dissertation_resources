package com.whatsapp.companiondevice;

import X.AnonymousClass4KI;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class WifiSpeedBumpDialogFragment extends Hilt_WifiSpeedBumpDialogFragment {
    public AnonymousClass4KI A00;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        return new AlertDialog.Builder(A0p()).setTitle(R.string.wifi_speed_bump_dialog_title).setMessage(R.string.wifi_speed_bump_dialog_body).setPositiveButton(R.string.wifi_speed_bump_dialog_use_data_button_text, new IDxCListenerShape9S0100000_2_I1(this, 26)).setNegativeButton(R.string.wifi_speed_bump_dialog_cancel_button_text, (DialogInterface.OnClickListener) null).create();
    }
}
