package com.whatsapp.companiondevice;

import X.AbstractC14440lR;
import X.AbstractC14590lg;
import X.AbstractC15460nI;
import X.AbstractC20260vT;
import X.AnonymousClass014;
import X.AnonymousClass016;
import X.AnonymousClass139;
import X.AnonymousClass1GD;
import X.AnonymousClass5A5;
import X.C14820m6;
import X.C14840m8;
import X.C14860mA;
import X.C14890mD;
import X.C14900mE;
import X.C15450nH;
import X.C18640sm;
import X.C18850tA;
import X.C22100yW;
import X.C22230yk;
import X.C233411h;
import X.C238813j;
import X.C245916c;
import X.C27691It;
import X.C40971sf;
import android.app.Application;
import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;

/* loaded from: classes2.dex */
public class LinkedDevicesSharedViewModel extends AnonymousClass014 {
    public Boolean A00;
    public Runnable A01;
    public boolean A02;
    public final Application A03;
    public final AnonymousClass016 A04 = new AnonymousClass016();
    public final C14900mE A05;
    public final C15450nH A06;
    public final C18850tA A07;
    public final C233411h A08;
    public final AbstractC20260vT A09 = new AbstractC20260vT() { // from class: X.55h
        @Override // X.AbstractC20260vT
        public final void AOa(AnonymousClass1I1 r5) {
            boolean z;
            LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = LinkedDevicesSharedViewModel.this;
            Boolean bool = linkedDevicesSharedViewModel.A00;
            if ((bool == null || bool.booleanValue() != (z = r5.A04)) && (z = r5.A04)) {
                linkedDevicesSharedViewModel.A0J.A0B(null);
            }
            linkedDevicesSharedViewModel.A00 = Boolean.valueOf(z);
        }
    };
    public final C18640sm A0A;
    public final C14820m6 A0B;
    public final AnonymousClass1GD A0C = new C40971sf(this);
    public final C22100yW A0D;
    public final C245916c A0E;
    public final C238813j A0F;
    public final AnonymousClass139 A0G;
    public final C14840m8 A0H;
    public final C22230yk A0I;
    public final C27691It A0J = new C27691It();
    public final C27691It A0K = new C27691It();
    public final C27691It A0L = new C27691It();
    public final C27691It A0M = new C27691It();
    public final C27691It A0N = new C27691It();
    public final C27691It A0O = new C27691It();
    public final C27691It A0P = new C27691It();
    public final C27691It A0Q = new C27691It();
    public final C27691It A0R = new C27691It();
    public final C27691It A0S = new C27691It();
    public final AbstractC14440lR A0T;
    public final AbstractC14590lg A0U = new AnonymousClass5A5(this);
    public final C14890mD A0V;
    public final C14860mA A0W;

    public LinkedDevicesSharedViewModel(Application application, C14900mE r3, C15450nH r4, C18850tA r5, C233411h r6, C18640sm r7, C14820m6 r8, C22100yW r9, C245916c r10, C238813j r11, AnonymousClass139 r12, C14840m8 r13, C22230yk r14, AbstractC14440lR r15, C14890mD r16, C14860mA r17) {
        super(application);
        this.A05 = r3;
        this.A0T = r15;
        this.A03 = application;
        this.A06 = r4;
        this.A08 = r6;
        this.A0B = r8;
        this.A0H = r13;
        this.A0A = r7;
        this.A0W = r17;
        this.A0D = r9;
        this.A0G = r12;
        this.A0F = r11;
        this.A07 = r5;
        this.A0V = r16;
        this.A0I = r14;
        this.A0E = r10;
    }

    public void A04(boolean z) {
        C27691It r1;
        Integer num;
        if (!this.A0A.A0B()) {
            boolean A03 = C18640sm.A03((Context) this.A03);
            r1 = this.A0K;
            int i = R.string.network_required;
            if (A03) {
                i = R.string.network_required_airplane_on;
            }
            num = Integer.valueOf(i);
        } else {
            if (!this.A06.A05(AbstractC15460nI.A0b) || !z) {
                r1 = this.A0Q;
            } else {
                r1 = this.A0P;
            }
            num = null;
        }
        r1.A0B(num);
    }
}
