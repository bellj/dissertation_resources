package com.whatsapp.companiondevice;

import X.AbstractC16230of;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1GC;
import X.AnonymousClass1JU;
import X.AnonymousClass1UY;
import X.AnonymousClass22D;
import X.AnonymousClass3JK;
import X.C005602s;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14820m6;
import X.C18360sK;
import X.C21360xI;
import X.C22100yW;
import X.C22630zO;
import X.C249017i;
import X.C65303Iz;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class CompanionDeviceVerificationReceiver extends BroadcastReceiver {
    public C21360xI A00;
    public C14820m6 A01;
    public C22100yW A02;
    public final Object A03;
    public volatile boolean A04;

    public CompanionDeviceVerificationReceiver() {
        this(0);
    }

    public CompanionDeviceVerificationReceiver(int i) {
        this.A04 = false;
        this.A03 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        List asList;
        AnonymousClass1JU r4;
        if (!this.A04) {
            synchronized (this.A03) {
                if (!this.A04) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = C12970iu.A0Z(r1);
                    this.A02 = (C22100yW) r1.A3g.get();
                    this.A00 = (C21360xI) r1.A3j.get();
                    this.A04 = true;
                }
            }
        }
        String A0p = C12980iv.A0p(this.A01.A00, "companion_device_verification_ids");
        if (A0p != null && (asList = Arrays.asList(A0p.split(","))) != null) {
            Iterator it = asList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                String A0x = C12970iu.A0x(it);
                C22100yW r3 = this.A02;
                DeviceJid nullable = DeviceJid.getNullable(A0x);
                AnonymousClass009.A05(nullable);
                if (r3.A0L.A03() && (r4 = (AnonymousClass1JU) r3.A0I.A04.A00().A00.get(nullable)) != null) {
                    Iterator A00 = AbstractC16230of.A00(this.A00);
                    while (A00.hasNext()) {
                        C249017i r12 = ((AnonymousClass1GC) A00.next()).A00;
                        Context context2 = r12.A02.A00;
                        AnonymousClass018 r5 = r12.A04;
                        C18360sK r32 = r12.A03;
                        String string = context2.getString(R.string.notification_companion_device_verification_title);
                        String A002 = AnonymousClass3JK.A00(r5, r4.A04);
                        Object[] A1a = C12980iv.A1a();
                        A1a[0] = r4.A07;
                        String A0X = C12960it.A0X(context2, A002, A1a, 1, R.string.notification_companion_device_verification_description);
                        C005602s A003 = C22630zO.A00(context2);
                        A003.A0J = "other_notifications@1";
                        A003.A0B(string);
                        A003.A0A(string);
                        A003.A09(A0X);
                        A003.A09 = AnonymousClass1UY.A00(context2, 0, C65303Iz.A02(context2), 0);
                        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
                        notificationCompat$BigTextStyle.A09(A0X);
                        A003.A08(notificationCompat$BigTextStyle);
                        A003.A0D(true);
                        C18360sK.A01(A003, R.drawable.notify_web_client_connected);
                        r32.A03(21, A003.A01());
                    }
                }
            }
        } else {
            Log.e("CompanionDeviceVerificationReceiver/onReceive/ deviceIds are missing from prefs");
        }
        C12990iw.A11(C12960it.A08(this.A01), "companion_device_verification_ids");
        PendingIntent A01 = AnonymousClass1UY.A01(context, 0, intent, 536870912);
        if (A01 != null) {
            A01.cancel();
        }
    }
}
