package com.whatsapp.companiondevice.optin.ui;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass12P;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14840m8;
import X.C14900mE;
import X.C21220x4;
import X.C252018m;
import X.C42971wC;
import X.C53892fb;
import X.C67423Rl;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.companiondevice.optin.ui.ForcedOptInActivity;
import com.whatsapp.components.Button;

/* loaded from: classes2.dex */
public class ForcedOptInActivity extends ActivityC13790kL {
    public ProgressDialog A00;
    public View A01;
    public ScrollView A02;
    public TextEmojiLabel A03;
    public C21220x4 A04;
    public C53892fb A05;
    public Button A06;
    public C14840m8 A07;
    public C252018m A08;
    public boolean A09;

    public ForcedOptInActivity() {
        this(0);
    }

    public ForcedOptInActivity(int i) {
        this.A09 = false;
        ActivityC13830kP.A1P(this, 49);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A08 = C12980iv.A0g(A1M);
            this.A07 = (C14840m8) A1M.ACi.get();
            this.A04 = (C21220x4) A1M.ACk.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.forced_opt_in_activity);
        A1e((Toolbar) findViewById(R.id.title_toolbar));
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0A(R.string.md_forced_opt_in_screen_title);
        A0N.A0M(true);
        this.A02 = (ScrollView) AnonymousClass00T.A05(this, R.id.scroll_view);
        this.A01 = AnonymousClass00T.A05(this, R.id.update_sheet_shadow);
        this.A03 = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.improvement_description);
        this.A06 = (Button) AnonymousClass00T.A05(this, R.id.update_button);
        C14900mE r1 = ((ActivityC13810kN) this).A05;
        AbstractC14440lR r6 = ((ActivityC13830kP) this).A05;
        C14840m8 r5 = this.A07;
        this.A05 = (C53892fb) new AnonymousClass02A(new C67423Rl(r1, this.A04, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A09, r5, r6, true, false), this).A00(C53892fb.class);
        C14900mE r12 = ((ActivityC13810kN) this).A05;
        AnonymousClass12P r11 = ((ActivityC13790kL) this).A00;
        AnonymousClass01d r14 = ((ActivityC13810kN) this).A08;
        C42971wC.A08(this, this.A08.A04("download-and-installation", "about-linked-devices"), r11, r12, this.A03, r14, C12960it.A0X(this, "learn-more", new Object[1], 0, R.string.md_forced_opt_in_improvement_description), "learn-more");
        this.A02.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nf
            /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
                if ((!r3.A02.canScrollVertically(1)) == false) goto L_0x0015;
             */
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void onGlobalLayout() {
                /*
                    r4 = this;
                    com.whatsapp.companiondevice.optin.ui.ForcedOptInActivity r3 = com.whatsapp.companiondevice.optin.ui.ForcedOptInActivity.this
                    android.widget.ScrollView r0 = r3.A02
                    boolean r0 = X.C93004Yo.A01(r0)
                    r2 = 0
                    if (r0 == 0) goto L_0x001e
                    android.widget.ScrollView r0 = r3.A02
                    r1 = 1
                    boolean r0 = r0.canScrollVertically(r1)
                    r0 = r0 ^ r1
                    if (r0 != 0) goto L_0x001e
                L_0x0015:
                    android.view.View r0 = r3.A01
                    if (r1 != 0) goto L_0x001a
                    r2 = 4
                L_0x001a:
                    r0.setVisibility(r2)
                    return
                L_0x001e:
                    r1 = 0
                    goto L_0x0015
                */
                throw new UnsupportedOperationException("Method not decompiled: X.ViewTreeObserver$OnGlobalLayoutListenerC101524nf.onGlobalLayout():void");
            }
        });
        this.A02.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oX
            @Override // android.view.ViewTreeObserver.OnScrollChangedListener
            public final void onScrollChanged() {
                ForcedOptInActivity forcedOptInActivity = ForcedOptInActivity.this;
                boolean z = !(!forcedOptInActivity.A02.canScrollVertically(1));
                View view = forcedOptInActivity.A01;
                int i = 4;
                if (z) {
                    i = 0;
                }
                view.setVisibility(i);
            }
        });
        C12960it.A0y(this.A06, this, 27);
        C12960it.A17(this, this.A05.A03, 36);
        C12960it.A18(this, this.A05.A08, 66);
        C12960it.A18(this, this.A05.A09, 67);
        C12960it.A18(this, this.A05.A02, 68);
    }
}
