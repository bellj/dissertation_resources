package com.whatsapp.companiondevice.optin.ui;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass02A;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14840m8;
import X.C14900mE;
import X.C21220x4;
import X.C252018m;
import X.C42971wC;
import X.C53892fb;
import X.C67423Rl;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.companiondevice.optin.ui.OptInActivity;
import com.whatsapp.components.Button;

/* loaded from: classes2.dex */
public class OptInActivity extends ActivityC13790kL {
    public ProgressDialog A00;
    public View A01;
    public View A02;
    public ScrollView A03;
    public TextView A04;
    public TextView A05;
    public TextEmojiLabel A06;
    public TextEmojiLabel A07;
    public C21220x4 A08;
    public C53892fb A09;
    public Button A0A;
    public Button A0B;
    public C14840m8 A0C;
    public C252018m A0D;
    public boolean A0E;

    public OptInActivity() {
        this(0);
    }

    public OptInActivity(int i) {
        this.A0E = false;
        ActivityC13830kP.A1P(this, 50);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0E) {
            this.A0E = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0D = C12980iv.A0g(A1M);
            this.A0C = (C14840m8) A1M.ACi.get();
            this.A08 = (C21220x4) A1M.ACk.get();
        }
    }

    public final void A2e(TextEmojiLabel textEmojiLabel, String str, int i) {
        C14900mE r6 = ((ActivityC13810kN) this).A05;
        C42971wC.A08(this, this.A0D.A04("download-and-installation", "about-multi-device-beta"), ((ActivityC13790kL) this).A00, r6, textEmojiLabel, ((ActivityC13810kN) this).A08, C12960it.A0X(this, str, C12970iu.A1b(), 0, i), str);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        super.onCreate(bundle);
        setContentView(R.layout.opt_in_activity);
        A1e((Toolbar) findViewById(R.id.title_toolbar));
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0A(R.string.md_opt_in_screen_title);
        A0N.A0M(true);
        this.A03 = (ScrollView) AnonymousClass00T.A05(this, R.id.scroll_view);
        this.A02 = AnonymousClass00T.A05(this, R.id.opt_in_sheet_shadow);
        this.A04 = C12990iw.A0N(this, R.id.header_title);
        this.A06 = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.header_description);
        this.A07 = (TextEmojiLabel) AnonymousClass00T.A05(this, R.id.limitation_3_name);
        this.A05 = C12990iw.A0N(this, R.id.opt_in_clarification);
        this.A01 = AnonymousClass00T.A05(this, R.id.enrolled_header_group);
        this.A0A = (Button) AnonymousClass00T.A05(this, R.id.opt_in_button);
        this.A0B = (Button) AnonymousClass00T.A05(this, R.id.opt_out_button);
        Bundle A0H = C12990iw.A0H(this);
        if (A0H != null) {
            z = A0H.getBoolean("arg_has_devices_linked", false);
            z2 = A0H.getBoolean("arg_has_portal_device_linked", false);
        } else {
            z = false;
            z2 = false;
        }
        C14900mE r1 = ((ActivityC13810kN) this).A05;
        AbstractC14440lR r6 = ((ActivityC13830kP) this).A05;
        C14840m8 r5 = this.A0C;
        this.A09 = (C53892fb) new AnonymousClass02A(new C67423Rl(r1, this.A08, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A09, r5, r6, z, z2), this).A00(C53892fb.class);
        this.A03.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4ng
            /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
                if ((!r3.A03.canScrollVertically(1)) == false) goto L_0x0015;
             */
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void onGlobalLayout() {
                /*
                    r4 = this;
                    com.whatsapp.companiondevice.optin.ui.OptInActivity r3 = com.whatsapp.companiondevice.optin.ui.OptInActivity.this
                    android.widget.ScrollView r0 = r3.A03
                    boolean r0 = X.C93004Yo.A01(r0)
                    r2 = 0
                    if (r0 == 0) goto L_0x001e
                    android.widget.ScrollView r0 = r3.A03
                    r1 = 1
                    boolean r0 = r0.canScrollVertically(r1)
                    r0 = r0 ^ r1
                    if (r0 != 0) goto L_0x001e
                L_0x0015:
                    android.view.View r0 = r3.A02
                    if (r1 != 0) goto L_0x001a
                    r2 = 4
                L_0x001a:
                    r0.setVisibility(r2)
                    return
                L_0x001e:
                    r1 = 0
                    goto L_0x0015
                */
                throw new UnsupportedOperationException("Method not decompiled: X.ViewTreeObserver$OnGlobalLayoutListenerC101534ng.onGlobalLayout():void");
            }
        });
        this.A03.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oY
            @Override // android.view.ViewTreeObserver.OnScrollChangedListener
            public final void onScrollChanged() {
                OptInActivity optInActivity = OptInActivity.this;
                boolean z3 = !(!optInActivity.A03.canScrollVertically(1));
                View view = optInActivity.A02;
                int i = 4;
                if (z3) {
                    i = 0;
                }
                view.setVisibility(i);
            }
        });
        C12960it.A0y(this.A0A, this, 29);
        C12960it.A0y(this.A0B, this, 28);
        C12960it.A18(this, this.A09.A03, 71);
        C12960it.A18(this, this.A09.A08, 69);
        C12960it.A18(this, this.A09.A09, 70);
        C12960it.A18(this, this.A09.A02, 72);
    }
}
