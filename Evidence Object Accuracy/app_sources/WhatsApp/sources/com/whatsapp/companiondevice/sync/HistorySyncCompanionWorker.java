package com.whatsapp.companiondevice.sync;

import X.AbstractC14440lR;
import X.AbstractFutureC44231yX;
import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.C005602s;
import X.C05360Pg;
import X.C18360sK;
import X.C18950tK;
import X.C21420xO;
import X.C21430xP;
import X.C22630zO;
import X.C81353ts;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class HistorySyncCompanionWorker extends ListenableWorker {
    public final C81353ts A00 = new C81353ts();
    public final C18950tK A01;
    public final C21430xP A02;
    public final C21420xO A03;
    public final AbstractC14440lR A04;

    public HistorySyncCompanionWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A04 = r1.Ag3();
        this.A01 = (C18950tK) r1.AHL.get();
        this.A02 = (C21430xP) r1.A95.get();
        this.A03 = (C21420xO) r1.A96.get();
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A00() {
        Context context = super.A00;
        String string = context.getString(R.string.notification_text_history_sync_on_companion);
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "other_notifications@1";
        A00.A09(string);
        A00.A0B(string);
        A00.A03 = -1;
        C18360sK.A01(A00, R.drawable.notifybar);
        C81353ts r4 = new C81353ts();
        r4.A04(new C05360Pg(221770040, A00.A01(), 0));
        return r4;
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A01() {
        Log.i("HistorySyncCompanionWorker/ startWork");
        this.A04.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 17));
        return this.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00b7, code lost:
        if (r0 != null) goto L_0x00b9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04() {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.companiondevice.sync.HistorySyncCompanionWorker.A04():void");
    }
}
