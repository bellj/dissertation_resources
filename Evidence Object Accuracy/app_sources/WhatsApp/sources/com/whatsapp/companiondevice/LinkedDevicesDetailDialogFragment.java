package com.whatsapp.companiondevice;

import X.AbstractC14590lg;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass1JU;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C14830m7;
import X.C14860mA;
import X.C14880mC;
import X.C14900mE;
import X.C22100yW;
import X.C238813j;
import X.C252018m;
import X.C91384Rn;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.R;
import java.util.Map;

/* loaded from: classes2.dex */
public class LinkedDevicesDetailDialogFragment extends Hilt_LinkedDevicesDetailDialogFragment implements AbstractC14590lg {
    public DialogInterface.OnDismissListener A00;
    public View A01;
    public AnonymousClass12P A02;
    public C14900mE A03;
    public LinkedDevicesSharedViewModel A04;
    public AnonymousClass01d A05;
    public C14830m7 A06;
    public AnonymousClass018 A07;
    public AnonymousClass1JU A08;
    public C22100yW A09;
    public C238813j A0A;
    public C91384Rn A0B;
    public C252018m A0C;
    public C14860mA A0D;
    public C14880mC A0E;
    public Boolean A0F;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        this.A01 = C12960it.A0F(LayoutInflater.from(A0p()), null, R.layout.linked_devices_detail_dialog);
        this.A0F = null;
        C238813j r4 = this.A0A;
        r4.A02.execute(new RunnableBRunnable0Shape1S0300000_I0_1(r4, this, this.A03.A06, 2));
        A1J();
        C004802e A0O = C12970iu.A0O(this);
        A0O.setView(this.A01);
        return A0O.create();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x010d, code lost:
        if (r1 == false) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01e8, code lost:
        if (r11 <= 3600000) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r2.A0N() == false) goto L_0x0022;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0183  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1J() {
        /*
        // Method dump skipped, instructions count: 584
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.companiondevice.LinkedDevicesDetailDialogFragment.A1J():void");
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        boolean booleanValue;
        Map map = (Map) obj;
        AnonymousClass1JU r5 = this.A08;
        if (r5 != null && r5.A01 <= 0) {
            Boolean bool = (Boolean) map.get(r5.A05);
            if (bool == null) {
                booleanValue = false;
            } else {
                booleanValue = bool.booleanValue();
            }
            this.A0F = Boolean.valueOf(booleanValue);
            A1J();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        DialogInterface.OnDismissListener onDismissListener = this.A00;
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialogInterface);
        }
        this.A0A.A00.A02(this);
    }
}
