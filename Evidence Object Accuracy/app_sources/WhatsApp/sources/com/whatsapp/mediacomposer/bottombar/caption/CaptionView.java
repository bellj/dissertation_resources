package com.whatsapp.mediacomposer.bottombar.caption;

import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2Aj;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaImageView;
import com.whatsapp.mentions.MentionableEntry;
import java.util.List;

/* loaded from: classes2.dex */
public class CaptionView extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final Context A03;
    public final View A04;
    public final View A05;
    public final View A06;
    public final ImageButton A07;
    public final LinearLayout A08;
    public final WaImageButton A09;
    public final WaImageView A0A;
    public final MentionableEntry A0B;

    public CaptionView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A04);
        LinearLayout.inflate(getContext(), obtainStyledAttributes.getBoolean(0, false) ? R.layout.new_media_composer_caption_view : R.layout.media_composer_caption_view, this);
        this.A03 = context;
        this.A0B = (MentionableEntry) AnonymousClass028.A0D(this, R.id.caption);
        this.A08 = (LinearLayout) AnonymousClass028.A0D(this, R.id.left_button_holder);
        this.A07 = (ImageButton) AnonymousClass028.A0D(this, R.id.emoji_picker_btn);
        this.A05 = AnonymousClass028.A0D(this, R.id.left_button_spacer);
        this.A09 = (WaImageButton) AnonymousClass028.A0D(this, R.id.add_button);
        this.A04 = AnonymousClass028.A0D(this, R.id.left_button_spacer);
        this.A0A = C12980iv.A0X(this, R.id.view_once_toggle);
        this.A06 = AnonymousClass028.A0D(this, R.id.view_once_toggle_spacer);
        obtainStyledAttributes.recycle();
    }

    public CaptionView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public Paint getCaptionPaint() {
        return this.A0B.getPaint();
    }

    public String getCaptionStringText() {
        return this.A0B.getStringText();
    }

    public CharSequence getCaptionText() {
        MentionableEntry mentionableEntry = this.A0B;
        if (TextUtils.isEmpty(mentionableEntry.getText())) {
            return "";
        }
        return mentionableEntry.getText();
    }

    @Deprecated
    public WaEditText getCaptionTextView() {
        return this.A0B;
    }

    public int getCaptionTop() {
        int[] A07 = C13000ix.A07();
        this.A0B.getLocationInWindow(A07);
        return A07[1];
    }

    public int getCurrentTextColor() {
        return this.A0B.getCurrentTextColor();
    }

    @Deprecated
    public ImageButton getEmojiPickerButton() {
        return this.A07;
    }

    public List getMentions() {
        return this.A0B.getMentions();
    }

    public void setAddButtonClickable(boolean z) {
        this.A09.setClickable(z);
    }

    public void setAddButtonEnabled(boolean z) {
        this.A09.setEnabled(z);
    }

    public void setCaptionButtonsListener(AnonymousClass2Aj r3) {
        AbstractView$OnClickListenerC34281fs.A02(this.A09, this, r3, 44);
        C12960it.A0x(this.A0A, r3, 42);
    }

    public void setCaptionEditTextView(CharSequence charSequence) {
        MentionableEntry mentionableEntry = this.A0B;
        mentionableEntry.setText(charSequence);
        mentionableEntry.setSelection(charSequence.length(), charSequence.length());
        mentionableEntry.setInputEnterAction(6);
        C12990iw.A1I(mentionableEntry, new InputFilter[1], EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    }

    public void setCaptionText(CharSequence charSequence) {
        this.A0B.setText(charSequence);
    }

    public void setViewOnceButtonClickable(boolean z) {
        this.A0A.setClickable(z);
    }
}
