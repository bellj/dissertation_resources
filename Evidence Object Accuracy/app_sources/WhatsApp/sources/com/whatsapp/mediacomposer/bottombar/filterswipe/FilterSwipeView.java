package com.whatsapp.mediacomposer.bottombar.filterswipe;

import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class FilterSwipeView extends LinearLayout {
    public final TextView A00 = C12960it.A0I(this, R.id.filter_swipe_text);

    public FilterSwipeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LinearLayout.inflate(getContext(), R.layout.filter_swipe_view, this);
        C12960it.A0r(context, this, R.string.filter_swipe_up);
    }

    public int getFilterSwipeTextViewVisibility() {
        return this.A00.getVisibility();
    }

    public void setFilterSwipeTextVisibility(int i) {
        this.A00.setVisibility(i);
    }

    public void setText(int i) {
        this.A00.setText(i);
    }
}
