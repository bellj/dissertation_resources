package com.whatsapp.mediacomposer.bottombar.recipients;

import X.AbstractC33441e5;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass23N;
import X.AnonymousClass2GF;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C42941w9;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class RecipientsView extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass018 A01;
    public AbstractC33441e5 A02;
    public AnonymousClass2P7 A03;
    public boolean A04;
    public boolean A05;
    public final HorizontalScrollView A06;
    public final ImageView A07;
    public final ChipGroup A08;
    public final TextEmojiLabel A09;
    public final AbstractView$OnClickListenerC34281fs A0A;

    public RecipientsView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A0A = new ViewOnClickCListenerShape18S0100000_I1_1(this, 32);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0G);
        boolean z = obtainStyledAttributes.getBoolean(0, true);
        LinearLayout.inflate(getContext(), z ? R.layout.new_media_recipients_layout : R.layout.media_recipients_layout, this);
        this.A09 = C12970iu.A0T(this, R.id.recipients_text);
        ImageView A0L = C12970iu.A0L(this, R.id.recipients_prompt_icon);
        this.A07 = A0L;
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) AnonymousClass028.A0D(this, R.id.recipients_scroller);
        this.A06 = horizontalScrollView;
        this.A08 = z ? (ChipGroup) AnonymousClass028.A0D(this, R.id.recipient_chips) : null;
        if (A0L != null) {
            AnonymousClass2GF.A01(context, A0L, this.A01, R.drawable.chevron);
        }
        if (z) {
            AnonymousClass23N.A02(horizontalScrollView, R.string.edit);
        }
        obtainStyledAttributes.recycle();
        this.A04 = true;
        this.A00 = R.color.audience_selector_enabled_chip;
    }

    public RecipientsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A05) {
            this.A05 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final Chip A00(CharSequence charSequence) {
        Chip chip = new Chip(getContext(), null);
        chip.setChipCornerRadiusResource(R.dimen.space_xLoose);
        chip.setText(charSequence);
        C12960it.A0s(getContext(), chip, R.color.audience_selector_text_color_chip);
        chip.setChipBackgroundColorResource(this.A00);
        chip.setMinHeight(getResources().getDimensionPixelSize(R.dimen.media_recipient_chip_height));
        chip.setEnabled(this.A04);
        return chip;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public void setRecipientsChips(List list, CharSequence charSequence) {
        ChipGroup chipGroup = this.A08;
        if (chipGroup != null) {
            chipGroup.removeAllViews();
            if (charSequence != null) {
                Chip A00 = A00(charSequence);
                A00.setChipIcon(AnonymousClass00T.A04(getContext(), R.drawable.ic_status_recipient));
                A00.setChipIconSizeResource(R.dimen.media_recipient_status_icon_size);
                A00.setIconStartPaddingResource(R.dimen.media_recipient_status_icon_start_padding);
                A00.setTextStartPaddingResource(R.dimen.media_recipient_text_start_padding);
                A00.setTag("status_chip");
                A00.setOnClickListener(this.A0A);
                chipGroup.addView(A00);
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                Chip A002 = A00(charSequence);
                A002.setText(A0x);
                A002.setOnClickListener(this.A0A);
                chipGroup.addView(A002);
            }
            C42941w9.A0D(this.A06, this.A01);
        }
    }

    public void setRecipientsContentDescription(int i) {
        Resources resources = getResources();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1O(A1b, i);
        this.A06.setContentDescription(resources.getQuantityString(R.plurals.selected_recipients, i, A1b));
    }

    public void setRecipientsListener(AbstractC33441e5 r5) {
        this.A02 = r5;
        ChipGroup chipGroup = this.A08;
        if (chipGroup != null) {
            for (int i = 0; i < chipGroup.getChildCount(); i++) {
                chipGroup.getChildAt(i).setOnClickListener(this.A0A);
            }
        }
    }

    public void setRecipientsText(String str) {
        this.A09.A0G(null, str);
    }
}
