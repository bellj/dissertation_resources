package com.whatsapp.mediacomposer.bottombar;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C14850m9;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.bottombar.caption.CaptionView;
import com.whatsapp.mediacomposer.bottombar.recipients.RecipientsView;

/* loaded from: classes2.dex */
public class BottomBarView extends RelativeLayout implements AnonymousClass004 {
    public C14850m9 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final View A03;
    public final ImageView A04;
    public final RecyclerView A05;
    public final CaptionView A06;
    public final RecipientsView A07;
    public final boolean A08;

    public BottomBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        boolean A07 = this.A00.A07(815);
        this.A08 = A07;
        RelativeLayout.inflate(context, A07 ? R.layout.new_media_composer_bottom_bar : R.layout.media_composer_bottom_bar, this);
        this.A05 = C12990iw.A0R(this, R.id.thumbnails);
        this.A04 = C12970iu.A0K(this, R.id.send);
        this.A06 = (CaptionView) AnonymousClass028.A0D(this, R.id.caption_layout);
        this.A07 = (RecipientsView) AnonymousClass028.A0D(this, R.id.media_recipients);
        this.A03 = AnonymousClass028.A0D(this, R.id.filter_swipe);
    }

    public BottomBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12960it.A0S(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public boolean getIsAudienceSelectorEnabled() {
        return this.A08;
    }
}
