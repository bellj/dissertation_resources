package com.whatsapp.mediacomposer;

import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01d;
import X.AnonymousClass21T;
import X.AnonymousClass21Y;
import X.AnonymousClass2Ab;
import X.AnonymousClass39G;
import X.AnonymousClass3JD;
import X.AnonymousClass47W;
import X.C12960it;
import X.C12980iv;
import X.C14900mE;
import X.C38241nl;
import X.C38911ou;
import X.C38991p4;
import X.C39341ph;
import X.C453421e;
import android.content.Context;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* loaded from: classes2.dex */
public class GifComposerFragment extends Hilt_GifComposerFragment {
    public AnonymousClass21T A00;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.gif_composer_fragment);
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        AnonymousClass21T r0 = this.A00;
        if (r0 != null) {
            r0.A08();
            this.A00 = null;
        }
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        AnonymousClass21T r5;
        C38991p4 r7;
        int i;
        int i2;
        super.A17(bundle, view);
        AnonymousClass009.A0F(C12980iv.A1X(this.A00));
        AnonymousClass21Y r3 = (AnonymousClass21Y) A0B();
        Uri uri = ((MediaComposerFragment) this).A00;
        C453421e r52 = ((MediaComposerActivity) r3).A1B;
        File A05 = r52.A00(uri).A05();
        AnonymousClass009.A05(A05);
        if (bundle == null) {
            String A08 = r52.A00(((MediaComposerFragment) this).A00).A08();
            String ACe = r3.ACe(((MediaComposerFragment) this).A00);
            if (A08 == null) {
                C39341ph A00 = r52.A00(((MediaComposerFragment) this).A00);
                synchronized (A00) {
                    r7 = A00.A05;
                }
                if (r7 == null) {
                    try {
                        r7 = new C38991p4(A05);
                    } catch (AnonymousClass47W e) {
                        Log.e("GifComposerFragment/bad video", e);
                    }
                }
                if (r7.A02()) {
                    i = r7.A01;
                } else {
                    i = r7.A03;
                }
                float f = (float) i;
                if (r7.A02()) {
                    i2 = r7.A03;
                } else {
                    i2 = r7.A01;
                }
                RectF rectF = new RectF(0.0f, 0.0f, f, (float) i2);
                AnonymousClass2Ab r6 = ((MediaComposerFragment) this).A0D;
                r6.A0I.A06 = rectF;
                r6.A0H.A00 = 0.0f;
                r6.A05(rectF);
            } else {
                AnonymousClass3JD A03 = AnonymousClass3JD.A03(A01(), ((MediaComposerFragment) this).A07, ((MediaComposerFragment) this).A08, ((MediaComposerFragment) this).A0H, A08);
                if (A03 != null) {
                    AnonymousClass2Ab r62 = ((MediaComposerFragment) this).A0D;
                    r62.A0H.setDoodle(A03);
                    r62.A0O.A05(ACe);
                }
            }
        }
        try {
            try {
                C38911ou.A03(A05);
                r5 = new AnonymousClass39G(A0C(), A05);
            } catch (IOException unused) {
                C14900mE r72 = ((MediaComposerFragment) this).A03;
                AbstractC14440lR r10 = ((MediaComposerFragment) this).A0M;
                AnonymousClass01d r8 = ((MediaComposerFragment) this).A05;
                AnonymousClass018 r9 = ((MediaComposerFragment) this).A07;
                Context A01 = A01();
                C39341ph A002 = r52.A00(((MediaComposerFragment) this).A00);
                synchronized (A002) {
                    r5 = AnonymousClass21T.A00(A01, r72, r8, r9, r10, A05, true, A002.A0D, C38241nl.A01());
                }
            }
            this.A00 = r5;
            r5.A0A(true);
            C12980iv.A1D(this.A00.A04(), C12980iv.A0P(view, R.id.video_player));
            if (((MediaComposerFragment) this).A00.equals(r3.AAj())) {
                this.A00.A04().setAlpha(0.0f);
                A0C().A0d();
            }
        } catch (IOException e2) {
            Log.e("GifComposerFragment/onViewCreated videoPlayer initialization", e2);
            ((MediaComposerFragment) this).A03.A07(R.string.error_load_gif, 0);
            A0C().finish();
        }
    }
}
