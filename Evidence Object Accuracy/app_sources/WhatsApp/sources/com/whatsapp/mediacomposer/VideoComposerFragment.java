package com.whatsapp.mediacomposer;

import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass11P;
import X.AnonymousClass1CY;
import X.AnonymousClass21T;
import X.AnonymousClass2GE;
import X.C239713s;
import X.C38981p3;
import X.C38991p4;
import X.C47342Ag;
import X.View$OnAttachStateChangeListenerC100944mj;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.VideoTimelineView;
import java.io.File;

/* loaded from: classes2.dex */
public class VideoComposerFragment extends Hilt_VideoComposerFragment {
    public long A00;
    public long A01 = -1;
    public long A02;
    public long A03;
    public View.OnClickListener A04 = new ViewOnClickCListenerShape2S0100000_I0_2(this, 39);
    public View.OnClickListener A05 = new ViewOnClickCListenerShape2S0100000_I0_2(this, 38);
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public ImageView A0A;
    public ImageView A0B;
    public TextView A0C;
    public TextView A0D;
    public TextView A0E;
    public VideoTimelineView A0F;
    public AnonymousClass11P A0G;
    public C239713s A0H;
    public C38981p3 A0I;
    public C47342Ag A0J;
    public AnonymousClass1CY A0K;
    public C38991p4 A0L;
    public AnonymousClass21T A0M;
    public File A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public final View.OnAttachStateChangeListener A0T = new View$OnAttachStateChangeListenerC100944mj(this);
    public final Runnable A0U = new RunnableBRunnable0Shape8S0100000_I0_8(this, 5);

    @Override // X.AnonymousClass01E
    public void A0r() {
        super.A0r();
        A1H();
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.video_composer_fragment, viewGroup, false);
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A0F.A0H = null;
        AnonymousClass21T r0 = this.A0M;
        if (r0 != null) {
            r0.A08();
            this.A0M = null;
        }
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        super.A13();
        int A01 = this.A0M.A01();
        AnonymousClass21T r1 = this.A0M;
        int i = A01 + 1;
        if (A01 > 0) {
            i = A01 - 1;
        }
        r1.A09(i);
        this.A0M.A09(A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x021a, code lost:
        if (r18 != false) goto L_0x01ac;
     */
    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r29, android.view.View r30) {
        /*
        // Method dump skipped, instructions count: 1001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.VideoComposerFragment.A17(android.os.Bundle, android.view.View):void");
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment
    public void A1E(Rect rect) {
        super.A1E(rect);
        if (((AnonymousClass01E) this).A0A != null) {
            this.A09.setPadding(rect.left, rect.top + A02().getDimensionPixelSize(R.dimen.actionbar_height), rect.right, rect.bottom + A02().getDimensionPixelSize(R.dimen.actionbar_height));
            this.A08.setPadding(rect.left, rect.top, rect.right, 0);
        }
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment
    public void A1F(boolean z) {
        super.A1F(z);
        this.A0P = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007c, code lost:
        if (X.C239713s.A02(r9, r11, r0) == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d2, code lost:
        if (r4 != false) goto L_0x00af;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long A1I() {
        /*
            r19 = this;
            r8 = r19
            X.13s r2 = r8.A0H
            boolean r1 = r8.A0R
            boolean r0 = r8.A0Q
            X.1p3 r9 = r2.A03(r1, r0)
            r8.A0I = r9
            long r6 = r8.A03
            long r0 = r8.A02
            long r2 = r6 - r0
            r17 = 1000(0x3e8, double:4.94E-321)
            int r4 = (r2 > r17 ? 1 : (r2 == r17 ? 0 : -1))
            if (r4 >= 0) goto L_0x001c
            r2 = 1000(0x3e8, double:4.94E-321)
        L_0x001c:
            X.13s r13 = r8.A0H
            X.1p4 r11 = r8.A0L
            java.io.File r12 = r8.A0N
            boolean r5 = r8.A0Q
            boolean r4 = r8.A0O
            boolean r10 = r8.A0S
            X.0m9 r14 = r13.A04
            r13 = 422(0x1a6, float:5.91E-43)
            boolean r16 = r14.A07(r13)
            if (r10 != 0) goto L_0x006a
            r14 = 0
            int r13 = (r0 > r14 ? 1 : (r0 == r14 ? 0 : -1))
            if (r13 != 0) goto L_0x006a
            long r0 = r11.A04
            int r13 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r13 != 0) goto L_0x006a
            long r13 = r12.length()
            int r0 = r9.A00
            long r0 = (long) r0
            r6 = 1048576(0x100000, double:5.180654E-318)
            long r0 = r0 * r6
            int r6 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
            if (r6 > 0) goto L_0x006a
            long r4 = r12.length()
        L_0x0051:
            android.widget.TextView r1 = r8.A0C
            X.018 r0 = r8.A07
            long r2 = r2 / r17
            java.lang.String r0 = X.C38131nZ.A04(r0, r2)
            r1.setText(r0)
            android.widget.TextView r1 = r8.A0D
            X.018 r0 = r8.A07
            java.lang.String r0 = X.C44891zj.A03(r0, r4)
            r1.setText(r0)
            return r4
        L_0x006a:
            int r1 = X.C39011p7.A02(r16)
            r0 = 1
            if (r1 != r0) goto L_0x007e
            if (r10 != 0) goto L_0x0087
            r0 = 3
            if (r5 == 0) goto L_0x0078
            r0 = 13
        L_0x0078:
            boolean r0 = X.C239713s.A02(r9, r11, r0)
            if (r0 != 0) goto L_0x0087
        L_0x007e:
            long r4 = r12.length()
            long r4 = r4 * r2
            long r0 = r11.A04
            long r4 = r4 / r0
            goto L_0x0051
        L_0x0087:
            int r7 = r11.A03
            int r6 = r11.A01
            int r1 = r9.A02
            int r0 = java.lang.Math.max(r7, r6)
            int r0 = java.lang.Math.min(r1, r0)
            android.util.Pair r1 = X.C239713s.A01(r7, r6, r0)
            java.lang.Object r0 = r1.first
            java.lang.Number r0 = (java.lang.Number) r0
            int r10 = r0.intValue()
            java.lang.Object r0 = r1.second
            java.lang.Number r0 = (java.lang.Number) r0
            int r11 = r0.intValue()
            if (r5 == 0) goto L_0x00ba
            int r10 = r10 * r11
            int r0 = r10 << 1
            float r1 = (float) r0
        L_0x00af:
            r0 = 0
        L_0x00b0:
            float r1 = r1 + r0
            long r4 = r2 / r17
            float r0 = (float) r4
            float r1 = r1 * r0
            r0 = 1090519040(0x41000000, float:8.0)
            float r1 = r1 / r0
            long r4 = (long) r1
            goto L_0x0051
        L_0x00ba:
            int r0 = r9.A01
            float r5 = (float) r0
            int r0 = r10 * r11
            float r1 = (float) r0
            int r0 = r9.A00
            r12 = 9
            r9 = r0
            r13 = r2
            float r0 = X.C239713s.A00(r9, r10, r11, r12, r13)
            float r1 = r1 * r0
            float r1 = java.lang.Math.min(r5, r1)
            r0 = 1203470336(0x47bb8000, float:96000.0)
            if (r4 == 0) goto L_0x00b0
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.VideoComposerFragment.A1I():long");
    }

    public final void A1J() {
        if (this.A0M.A0B()) {
            A1H();
            return;
        }
        this.A0M.A04().setBackground(null);
        if (((long) this.A0M.A01()) > this.A03 - 2000) {
            this.A0M.A09((int) this.A02);
        }
        A1C();
    }

    public final void A1K() {
        ImageView imageView;
        int i;
        View view;
        View.OnClickListener onClickListener;
        Context A01 = A01();
        if (this.A0Q) {
            this.A0A.setImageResource(R.drawable.ic_unmute);
            AnonymousClass2GE.A07(this.A0A, AnonymousClass00T.A00(A01, R.color.white_alpha_40));
            view = this.A06;
            onClickListener = null;
        } else {
            boolean z = this.A0O;
            ImageView imageView2 = this.A0A;
            if (z) {
                imageView2.setImageResource(R.drawable.ic_unmute);
                imageView = this.A0A;
                i = R.string.unmute_video;
            } else {
                imageView2.setImageResource(R.drawable.ic_mute);
                imageView = this.A0A;
                i = R.string.mute_video;
            }
            imageView.setContentDescription(A0I(i));
            AnonymousClass2GE.A07(this.A0A, AnonymousClass00T.A00(A01, R.color.white_alpha_100));
            view = this.A06;
            onClickListener = this.A04;
        }
        view.setOnClickListener(onClickListener);
    }
}
