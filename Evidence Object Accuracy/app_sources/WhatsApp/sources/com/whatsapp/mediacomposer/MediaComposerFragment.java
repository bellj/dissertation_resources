package com.whatsapp.mediacomposer;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC454821u;
import X.AbstractC47302Ac;
import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01F;
import X.AnonymousClass01d;
import X.AnonymousClass146;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1BT;
import X.AnonymousClass1BU;
import X.AnonymousClass1CV;
import X.AnonymousClass1KW;
import X.AnonymousClass21Y;
import X.AnonymousClass2Ab;
import X.AnonymousClass33N;
import X.C002601e;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C22190yg;
import X.C235512c;
import X.C244415n;
import X.C252718t;
import X.C454921v;
import X.C470928x;
import X.C47342Ag;
import X.C48792Hu;
import X.C64453Fp;
import X.DialogC47362Ai;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.ClearableEditText;
import com.whatsapp.R;
import com.whatsapp.ephemeral.ViewOnceNUXDialog;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.mediacomposer.doodle.DoodleView;

/* loaded from: classes2.dex */
public abstract class MediaComposerFragment extends Hilt_MediaComposerFragment implements AbstractC47302Ac {
    public Uri A00;
    public Toast A01;
    public AbstractC15710nm A02;
    public C14900mE A03;
    public C15450nH A04;
    public AnonymousClass01d A05;
    public C14820m6 A06;
    public AnonymousClass018 A07;
    public AnonymousClass19M A08;
    public C14850m9 A09;
    public C244415n A0A;
    public C64453Fp A0B;
    public C48792Hu A0C;
    public AnonymousClass2Ab A0D;
    public AnonymousClass1BT A0E;
    public AnonymousClass1BU A0F;
    public AnonymousClass1CV A0G;
    public AnonymousClass1AB A0H;
    public AnonymousClass146 A0I;
    public C235512c A0J;
    public C252718t A0K;
    public C22190yg A0L;
    public AbstractC14440lR A0M;
    public final int[] A0N = new int[2];

    @Override // X.AnonymousClass01E
    public void A0k(Bundle bundle) {
        super.A0k(bundle);
        AnonymousClass2Ab r3 = this.A0D;
        r3.A0H.Acv(r3.A0E.A05.A00, r3.A0F.A00);
        r3.A08 = true;
    }

    @Override // X.AnonymousClass01E
    public void A0l() {
        AnonymousClass2Ab r2 = this.A0D;
        r2.A0E.A05(false);
        r2.A03.A00();
        super.A0l();
    }

    @Override // com.whatsapp.base.WaFragment, X.AnonymousClass01E
    public void A0n(boolean z) {
        try {
            super.A0n(z);
        } catch (NullPointerException unused) {
            this.A02.AaV("mediacomperserfragment-visibility-npe", null, true);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        Bundle extras;
        if (i != 2) {
            super.A0t(i, i2, intent);
        } else if (i2 == -1 && (extras = intent.getExtras()) != null) {
            String string = extras.getString("locations_string");
            if (TextUtils.isEmpty(string)) {
                string = A0I(R.string.attach_location);
            }
            double d = extras.getDouble("longitude");
            double d2 = extras.getDouble("latitude");
            AnonymousClass33N r1 = new AnonymousClass33N(A01(), this.A07, string, false);
            r1.A01 = d;
            r1.A00 = d2;
            this.A0D.A06(r1);
        }
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        C47342Ag r2 = ((MediaComposerActivity) ((AnonymousClass21Y) A0B())).A0i;
        if (r2.A03 == this.A0B) {
            r2.A03 = null;
        }
        AnonymousClass2Ab r4 = this.A0D;
        DoodleView doodleView = r4.A0H;
        C454921v r22 = doodleView.A0E;
        Bitmap bitmap = r22.A07;
        if (bitmap != null) {
            bitmap.recycle();
            r22.A07 = null;
        }
        Bitmap bitmap2 = r22.A08;
        if (bitmap2 != null) {
            bitmap2.recycle();
            r22.A08 = null;
        }
        Bitmap bitmap3 = r22.A06;
        if (bitmap3 != null) {
            bitmap3.recycle();
            r22.A06 = null;
        }
        Bitmap bitmap4 = r22.A05;
        if (bitmap4 != null) {
            bitmap4.recycle();
            r22.A05 = null;
        }
        doodleView.setEnabled(false);
        r4.A0A.removeCallbacksAndMessages(null);
        C002601e r1 = r4.A0T;
        if (r1.A00()) {
            AnonymousClass1KW r3 = (AnonymousClass1KW) r1.get();
            r3.A02.A03(true);
            r3.A06.quit();
            r3.A0J.removeMessages(0);
            r3.A0d.clear();
            r3.A0R.A00 = null;
            r3.A0W.A04(r3.A0V);
            r3.A0Q.A02();
        }
        C47342Ag r0 = r4.A0Q;
        if (r0 != null) {
            r0.A0H.setToolbarExtraVisibility(8);
        }
        Toast toast = this.A01;
        if (toast != null) {
            toast.cancel();
            this.A01 = null;
        }
        super.A12();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0063, code lost:
        if (r39.A09.A07(1493) == false) goto L_0x0065;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r40, android.view.View r41) {
        /*
            r39 = this;
            r13 = r39
            android.os.Bundle r1 = r13.A05
            java.lang.String r0 = "uri"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            android.net.Uri r0 = (android.net.Uri) r0
            r13.A00 = r0
            boolean r1 = r13 instanceof com.whatsapp.mediacomposer.VideoComposerFragment
            if (r1 != 0) goto L_0x00b5
            boolean r0 = r13 instanceof com.whatsapp.mediacomposer.ImageComposerFragment
            if (r0 != 0) goto L_0x00ab
            X.3Fp r2 = new X.3Fp
            r2.<init>(r13)
        L_0x001c:
            r13.A0B = r2
            X.0m9 r0 = r13.A09
            r21 = r0
            X.18t r0 = r13.A0K
            r20 = r0
            X.0lR r0 = r13.A0M
            r17 = r0
            X.19M r15 = r13.A08
            X.1BT r12 = r13.A0E
            X.01d r11 = r13.A05
            X.018 r10 = r13.A07
            X.146 r9 = r13.A0I
            X.12c r8 = r13.A0J
            X.1CV r7 = r13.A0G
            X.1BU r6 = r13.A0F
            X.1AB r5 = r13.A0H
            X.00k r16 = r13.A0C()
            X.3Fp r4 = r13.A0B
            X.00k r0 = r13.A0B()
            X.21Y r0 = (X.AnonymousClass21Y) r0
            com.whatsapp.mediacomposer.MediaComposerActivity r0 = (com.whatsapp.mediacomposer.MediaComposerActivity) r0
            X.2Ag r3 = r0.A0i
            if (r1 != 0) goto L_0x00a2
            boolean r0 = r13 instanceof com.whatsapp.mediacomposer.ImageComposerFragment
            if (r0 != 0) goto L_0x009a
            r2 = 0
        L_0x0053:
            X.2Hu r1 = r13.A0C
            boolean r0 = r13 instanceof com.whatsapp.mediacomposer.ImageComposerFragment
            if (r0 == 0) goto L_0x0065
            X.0m9 r14 = r13.A09
            r0 = 1493(0x5d5, float:2.092E-42)
            boolean r0 = r14.A07(r0)
            r38 = 1
            if (r0 != 0) goto L_0x0067
        L_0x0065:
            r38 = 0
        L_0x0067:
            X.2Ab r0 = new X.2Ab
            r26 = r13
            r30 = r13
            r19 = r13
            r18 = r41
            r31 = r7
            r32 = r3
            r33 = r5
            r34 = r9
            r35 = r8
            r36 = r20
            r37 = r17
            r22 = r10
            r23 = r15
            r24 = r21
            r25 = r4
            r27 = r1
            r28 = r12
            r29 = r6
            r15 = r0
            r17 = r2
            r20 = r13
            r21 = r11
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38)
            r13.A0D = r0
            return
        L_0x009a:
            r0 = r13
            com.whatsapp.mediacomposer.ImageComposerFragment r0 = (com.whatsapp.mediacomposer.ImageComposerFragment) r0
            com.whatsapp.mediacomposer.doodle.ImagePreviewContentLayout r0 = r0.A06
            X.3MP r2 = r0.A05
            goto L_0x0053
        L_0x00a2:
            r0 = r13
            com.whatsapp.mediacomposer.VideoComposerFragment r0 = (com.whatsapp.mediacomposer.VideoComposerFragment) r0
            X.4mO r2 = new X.4mO
            r2.<init>(r0)
            goto L_0x0053
        L_0x00ab:
            r0 = r13
            com.whatsapp.mediacomposer.ImageComposerFragment r0 = (com.whatsapp.mediacomposer.ImageComposerFragment) r0
            X.334 r2 = new X.334
            r2.<init>(r0)
            goto L_0x001c
        L_0x00b5:
            r0 = r13
            com.whatsapp.mediacomposer.VideoComposerFragment r0 = (com.whatsapp.mediacomposer.VideoComposerFragment) r0
            X.333 r2 = new X.333
            r2.<init>(r0)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerFragment.A17(android.os.Bundle, android.view.View):void");
    }

    public void A1A() {
        if (this instanceof VideoComposerFragment) {
            VideoComposerFragment videoComposerFragment = (VideoComposerFragment) this;
            View findViewById = videoComposerFragment.A05().findViewById(R.id.content);
            findViewById.setVisibility(0);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(300);
            findViewById.startAnimation(alphaAnimation);
            videoComposerFragment.A0M.A04().setAlpha(1.0f);
        } else if (this instanceof ImageComposerFragment) {
            ImageComposerFragment imageComposerFragment = (ImageComposerFragment) this;
            imageComposerFragment.A08.setVisibility(0);
            ActivityC000900k A0B = imageComposerFragment.A0B();
            if (A0B != null && A0B.getIntent().getIntExtra("origin", 1) == 29) {
                ((MediaComposerFragment) imageComposerFragment).A03.A0H(new RunnableBRunnable0Shape15S0100000_I1_1(((MediaComposerFragment) imageComposerFragment).A0D, 44));
            }
        } else if (this instanceof GifComposerFragment) {
            GifComposerFragment gifComposerFragment = (GifComposerFragment) this;
            gifComposerFragment.A00.A04().setAlpha(1.0f);
            gifComposerFragment.A00.A04().setVisibility(0);
        }
    }

    public void A1B() {
        if (this instanceof ImageComposerFragment) {
            ImageComposerFragment imageComposerFragment = (ImageComposerFragment) this;
            imageComposerFragment.A08.setVisibility(4);
            ActivityC000900k A0B = imageComposerFragment.A0B();
            if (A0B != null && A0B.getIntent().getIntExtra("origin", 1) == 29) {
                imageComposerFragment.A1K(false, true);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        if (r4.A0O != false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1C() {
        /*
            r5 = this;
            boolean r0 = r5 instanceof com.whatsapp.mediacomposer.VideoComposerFragment
            if (r0 != 0) goto L_0x002a
            boolean r0 = r5 instanceof com.whatsapp.mediacomposer.ImageComposerFragment
            if (r0 != 0) goto L_0x0029
            r3 = r5
            com.whatsapp.mediacomposer.GifComposerFragment r3 = (com.whatsapp.mediacomposer.GifComposerFragment) r3
            X.21T r0 = r3.A00
            r0.A07()
            X.2Ab r0 = r3.A0D
            com.whatsapp.mediacomposer.doodle.DoodleView r2 = r0.A0H
            X.21v r1 = r2.A0E
            r0 = 1
            r1.A0A = r0
            android.os.SystemClock.elapsedRealtime()
            r2.invalidate()
            X.21T r0 = r3.A00
            android.view.View r1 = r0.A04()
            r0 = 1
            r1.setKeepScreenOn(r0)
        L_0x0029:
            return
        L_0x002a:
            r4 = r5
            com.whatsapp.mediacomposer.VideoComposerFragment r4 = (com.whatsapp.mediacomposer.VideoComposerFragment) r4
            X.11P r0 = r4.A0G
            r0.A06()
            X.21T r3 = r4.A0M
            boolean r0 = r4.A0Q
            r2 = 1
            if (r0 != 0) goto L_0x003e
            boolean r1 = r4.A0O
            r0 = 0
            if (r1 == 0) goto L_0x003f
        L_0x003e:
            r0 = 1
        L_0x003f:
            r3.A0A(r0)
            X.21T r0 = r4.A0M
            r0.A07()
            X.2Ab r0 = r4.A0D
            com.whatsapp.mediacomposer.doodle.DoodleView r1 = r0.A0H
            X.21v r0 = r1.A0E
            r0.A0A = r2
            android.os.SystemClock.elapsedRealtime()
            r1.invalidate()
            X.21T r0 = r4.A0M
            android.view.View r0 = r0.A04()
            r0.setKeepScreenOn(r2)
            X.21T r0 = r4.A0M
            android.view.View r0 = r0.A04()
            java.lang.Runnable r3 = r4.A0U
            r0.removeCallbacks(r3)
            X.21T r0 = r4.A0M
            android.view.View r2 = r0.A04()
            r0 = 50
            r2.postDelayed(r3, r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r0 = 0
            android.view.animation.AlphaAnimation r2 = new android.view.animation.AlphaAnimation
            r2.<init>(r1, r0)
            r0 = 200(0xc8, double:9.9E-322)
            r2.setDuration(r0)
            android.view.View r0 = r4.A07
            r0.startAnimation(r2)
            android.view.View r1 = r4.A07
            r0 = 4
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerFragment.A1C():void");
    }

    public void A1D() {
        if (this instanceof VideoComposerFragment) {
            VideoComposerFragment videoComposerFragment = (VideoComposerFragment) this;
            ImageView imageView = videoComposerFragment.A0B;
            boolean z = videoComposerFragment.A0Q;
            int i = R.drawable.ic_gif_off;
            if (z) {
                i = R.drawable.ic_gif_on;
            }
            imageView.setImageResource(i);
            if (videoComposerFragment.A03 - videoComposerFragment.A02 > 7000 || videoComposerFragment.A0P) {
                if (videoComposerFragment.A0B.getVisibility() == 0) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) videoComposerFragment.A0B.getMeasuredWidth(), 0.0f, 0.0f);
                    translateAnimation.setDuration(100);
                    videoComposerFragment.A0B.startAnimation(translateAnimation);
                }
                videoComposerFragment.A0B.setOnClickListener(null);
                videoComposerFragment.A0B.setVisibility(8);
                return;
            }
            if (videoComposerFragment.A0B.getVisibility() == 8) {
                videoComposerFragment.A0B.measure(0, 0);
                TranslateAnimation translateAnimation2 = new TranslateAnimation((float) videoComposerFragment.A0B.getMeasuredWidth(), 0.0f, 0.0f, 0.0f);
                translateAnimation2.setDuration(100);
                videoComposerFragment.A0B.startAnimation(translateAnimation2);
            }
            videoComposerFragment.A0B.setOnClickListener(videoComposerFragment.A05);
            videoComposerFragment.A0B.setVisibility(0);
        }
    }

    public void A1E(Rect rect) {
        if (super.A0A != null) {
            AnonymousClass2Ab r5 = this.A0D;
            View view = r5.A0R.A03;
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            marginLayoutParams.leftMargin = rect.left;
            marginLayoutParams.topMargin = rect.top;
            marginLayoutParams.rightMargin = rect.right;
            marginLayoutParams.bottomMargin = rect.bottom;
            view.setLayoutParams(marginLayoutParams);
            r5.A0E.setInsets(rect);
            C002601e r1 = r5.A0T;
            if (r1.A00()) {
                ((AnonymousClass1KW) r1.get()).A0Q.setPadding(rect.left, rect.top, rect.right, rect.bottom);
            }
            r5.A09.set(rect);
        }
    }

    public void A1F(boolean z) {
        int captionTop;
        int i;
        Toast toast = this.A01;
        if (toast != null) {
            toast.cancel();
        }
        if (z && A0p() != null) {
            Context A01 = A01();
            MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) ((AnonymousClass21Y) A0B());
            DialogC47362Ai r0 = mediaComposerActivity.A0f;
            if (r0 == null || r0.A03.A04.getCaptionTop() == 0) {
                captionTop = mediaComposerActivity.A0e.A05.A04.getCaptionTop();
            } else {
                captionTop = Math.min(mediaComposerActivity.A0e.A05.A04.getCaptionTop(), mediaComposerActivity.A0f.A03.A04.getCaptionTop());
            }
            C14820m6 r1 = this.A06;
            AnonymousClass01F A0E = A0E();
            Toast toast2 = null;
            if (A0E.A0m() || r1.A00.getBoolean("view_once_nux", false) || A0E.A0A("view_once_nux") != null) {
                C14900mE r3 = this.A03;
                if (this instanceof VideoComposerFragment) {
                    i = R.string.view_once_video_sender_info;
                } else if (!(this instanceof ImageComposerFragment)) {
                    i = 0;
                } else {
                    i = R.string.view_once_photo_sender_info;
                }
                toast2 = r3.A02(A01.getString(i));
                toast2.setGravity(49, 0, captionTop >> 1);
                toast2.show();
            } else {
                ViewOnceNUXDialog.A00(A0E, null, false);
            }
            this.A01 = toast2;
        }
    }

    public boolean A1G() {
        AnonymousClass2Ab r5 = this.A0D;
        if (!r5.A08()) {
            C47342Ag r2 = r5.A0Q;
            if (r2.A00() != 2) {
                return false;
            }
            r2.A09(0);
            r5.A01();
        }
        C470928x r3 = ((AnonymousClass1KW) r5.A0T.get()).A0M;
        ClearableEditText clearableEditText = r3.A0A;
        if (clearableEditText.getVisibility() == 0) {
            clearableEditText.setText("");
        } else {
            ValueAnimator valueAnimator = r3.A01;
            if (valueAnimator == null || !valueAnimator.isRunning()) {
                C47342Ag r22 = r5.A0Q;
                r22.A0H.setBackButtonDrawable(R.drawable.ic_cam_close);
                r22.A05(r22.A00);
                r5.A02();
                return true;
            }
            long currentPlayTime = r3.A01.getCurrentPlayTime();
            r3.A01.cancel();
            r3.A00(currentPlayTime, false);
        }
        r3.A0C.A00.A0B(false);
        return true;
    }

    public boolean A1H() {
        if (this instanceof VideoComposerFragment) {
            VideoComposerFragment videoComposerFragment = (VideoComposerFragment) this;
            boolean A0B = videoComposerFragment.A0M.A0B();
            videoComposerFragment.A0M.A05();
            videoComposerFragment.A01 = (long) videoComposerFragment.A0M.A01();
            DoodleView doodleView = ((MediaComposerFragment) videoComposerFragment).A0D.A0H;
            doodleView.A0E.A0A = false;
            doodleView.invalidate();
            videoComposerFragment.A0M.A04().setKeepScreenOn(false);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(200);
            videoComposerFragment.A07.startAnimation(alphaAnimation);
            videoComposerFragment.A07.setVisibility(0);
            return A0B;
        } else if (this instanceof ImageComposerFragment) {
            return false;
        } else {
            GifComposerFragment gifComposerFragment = (GifComposerFragment) this;
            boolean A0B2 = gifComposerFragment.A00.A0B();
            gifComposerFragment.A00.A05();
            DoodleView doodleView2 = ((MediaComposerFragment) gifComposerFragment).A0D.A0H;
            doodleView2.A0E.A0A = false;
            doodleView2.invalidate();
            gifComposerFragment.A00.A04().setKeepScreenOn(false);
            return A0B2;
        }
    }

    @Override // X.AbstractC47302Ac
    public void AVx(AbstractC454821u r4) {
        Class cls;
        if (this.A0A.A06(A0p())) {
            cls = LocationPicker2.class;
        } else {
            cls = LocationPicker.class;
        }
        Intent intent = new Intent(A0p(), cls);
        intent.putExtra("sticker_mode", true);
        startActivityForResult(intent, 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001d, code lost:
        if (r3 == 180) goto L_0x001f;
     */
    @Override // X.AnonymousClass01E, android.content.ComponentCallbacks
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onConfigurationChanged(android.content.res.Configuration r5) {
        /*
            r4 = this;
            super.onConfigurationChanged(r5)
            X.01d r0 = r4.A05
            android.view.WindowManager r0 = r0.A0O()
            android.view.Display r0 = r0.getDefaultDisplay()
            int r3 = r0.getRotation()
            X.2Ab r2 = r4.A0D
            int r1 = r5.orientation
            r0 = 2
            if (r1 != r0) goto L_0x001f
            if (r3 == 0) goto L_0x001f
            r0 = 180(0xb4, float:2.52E-43)
            r1 = 1
            if (r3 != r0) goto L_0x0020
        L_0x001f:
            r1 = 0
        L_0x0020:
            boolean r0 = r2.A07
            if (r0 == r1) goto L_0x0029
            r2.A07 = r1
            r2.A03()
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerFragment.onConfigurationChanged(android.content.res.Configuration):void");
    }
}
