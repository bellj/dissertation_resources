package com.whatsapp.mediacomposer.doodle;

import X.AbstractC115315Ra;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2Ab;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3MP;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C64243Eu;
import X.RunnableC55822jO;
import X.RunnableC76183lD;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import com.facebook.redex.RunnableBRunnable0Shape0S0220102_I1;

/* loaded from: classes2.dex */
public class ImagePreviewContentLayout extends FrameLayout implements AnonymousClass004 {
    public Rect A00;
    public RectF A01;
    public AnonymousClass018 A02;
    public AnonymousClass2Ab A03;
    public AbstractC115315Ra A04;
    public AnonymousClass3MP A05;
    public AnonymousClass2P7 A06;
    public boolean A07;

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public ImagePreviewContentLayout(Context context) {
        super(context);
        C12970iu.A1A(context, this);
    }

    public ImagePreviewContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C12970iu.A1A(context, this);
    }

    public ImagePreviewContentLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C12970iu.A1A(context, this);
    }

    public ImagePreviewContentLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        C12970iu.A1A(context, this);
    }

    public ImagePreviewContentLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A01();
    }

    public void A00() {
        AnonymousClass3MP r3 = this.A05;
        RunnableC55822jO r1 = r3.A0J;
        if (r1 != null) {
            r1.A06 = false;
            r1.A07 = true;
        }
        r3.A0J = null;
        RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I1 = r3.A0H;
        if (runnableBRunnable0Shape0S0220102_I1 != null) {
            C12990iw.A1L(runnableBRunnable0Shape0S0220102_I1);
        }
        r3.A0H = null;
        RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I12 = r3.A0G;
        if (runnableBRunnable0Shape0S0220102_I12 != null) {
            C12990iw.A1L(runnableBRunnable0Shape0S0220102_I12);
        }
        r3.A0G = null;
        RunnableC76183lD r12 = r3.A0K;
        if (r12 != null) {
            r12.A01 = true;
        }
        r3.A0K = null;
        r3.A0E = null;
        r3.A0E = null;
    }

    public void A01() {
        if (!this.A07) {
            this.A07 = true;
            this.A02 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public GestureDetector.OnGestureListener getActionHandler() {
        return this.A05;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        canvas.setMatrix(this.A05.A09);
        Rect rect = this.A00;
        canvas.getClipBounds(rect);
        AnonymousClass2Ab r2 = this.A03;
        float f = this.A05.A00;
        C64243Eu r0 = r2.A0I;
        r0.A05 = rect;
        r0.A01 = f;
        super.onDraw(canvas);
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            RectF rectF = this.A01;
            rectF.set(0.0f, 0.0f, (float) C12960it.A04(this, getWidth()), (float) C12960it.A03(this));
            AnonymousClass3MP r1 = this.A05;
            r1.A0B.set(rectF);
            r1.A00();
            AnonymousClass3MP r2 = this.A05;
            r2.A0N = true;
            Matrix matrix = r2.A08;
            if (matrix == null || matrix.equals(r2.A09)) {
                r2.A00();
            }
        }
    }

    public void setDoodleController(AnonymousClass2Ab r1) {
        this.A03 = r1;
    }

    public void setImagePreviewContentLayoutListener(AbstractC115315Ra r1) {
        this.A04 = r1;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.A05.A0E = onClickListener;
    }

    public void setZoomableViewController(AnonymousClass3MP r1) {
        this.A05 = r1;
    }
}
