package com.whatsapp.mediacomposer.doodle.penmode;

import X.AnonymousClass028;
import X.AnonymousClass5Rb;
import X.AnonymousClass5UO;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public class PenModeView extends FrameLayout {
    public AnonymousClass5Rb A00;
    public final List A01 = C12960it.A0l();

    public PenModeView(Context context) {
        super(context);
        A00();
    }

    public PenModeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public PenModeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public final void A00() {
        FrameLayout.inflate(getContext(), R.layout.pen_mode_view, this);
        A01(new AnonymousClass5UO() { // from class: X.57s
            @Override // X.AnonymousClass5UO
            public final void AJ6(AnonymousClass5Rb r4) {
                DialogC51702Ye r0 = ((C1110157t) r4).A00;
                r0.A0E.A01(1, r0.A0B);
            }
        }, R.id.pen_mode_thin);
        A01(new AnonymousClass5UO() { // from class: X.57q
            @Override // X.AnonymousClass5UO
            public final void AJ6(AnonymousClass5Rb r4) {
                DialogC51702Ye r0 = ((C1110157t) r4).A00;
                r0.A0E.A01(2, r0.A09);
            }
        }, R.id.pen_mode_medium);
        A01(new AnonymousClass5UO() { // from class: X.57r
            @Override // X.AnonymousClass5UO
            public final void AJ6(AnonymousClass5Rb r4) {
                DialogC51702Ye r0 = ((C1110157t) r4).A00;
                r0.A0E.A01(3, r0.A0A);
            }
        }, R.id.pen_mode_thick);
        A01(new AnonymousClass5UO() { // from class: X.3YJ
            @Override // X.AnonymousClass5UO
            public final void AJ6(AnonymousClass5Rb r5) {
                AnonymousClass3FS r3 = ((C1110157t) r5).A00.A0E;
                if (!r3.A03) {
                    C63573Cc r2 = r3.A0A;
                    r2.A00(4);
                    r3.A04 = true;
                    r2.A01.A07.A01(r3.A07);
                    r3.A02 = r3.A06;
                }
            }
        }, R.id.pen_mode_blur);
    }

    public final void A01(AnonymousClass5UO r3, int i) {
        View A0D = AnonymousClass028.A0D(this, i);
        this.A01.add(A0D);
        C12960it.A14(A0D, this, r3, 17);
    }

    public void setOnSelectedListener(AnonymousClass5Rb r1) {
        this.A00 = r1;
    }
}
