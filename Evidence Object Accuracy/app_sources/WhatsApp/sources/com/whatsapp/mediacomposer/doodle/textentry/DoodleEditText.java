package com.whatsapp.mediacomposer.doodle.textentry;

import X.AbstractC115335Rd;
import X.AbstractC471028y;
import X.AnonymousClass290;
import X.AnonymousClass33L;
import X.AnonymousClass3YM;
import X.C1110257u;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.KeyEvent;
import com.whatsapp.WaEditText;

/* loaded from: classes2.dex */
public class DoodleEditText extends WaEditText {
    public int A00;
    public AbstractC115335Rd A01;
    public boolean A02;

    public DoodleEditText(Context context) {
        super(context);
        A02();
        this.A00 = 0;
        setLayerType(1, null);
    }

    public DoodleEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A00 = 0;
        setLayerType(1, null);
    }

    public DoodleEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A00 = 0;
        setLayerType(1, null);
    }

    public DoodleEditText(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A02();
    }

    @Override // com.whatsapp.WaEditText, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A00 == 3) {
            setTextColor(-16777216);
            getPaint().setStrokeWidth(getTextSize() / 12.0f);
            getPaint().setStyle(Paint.Style.STROKE);
            super.onDraw(canvas);
            setTextColor(-1);
            getPaint().setStrokeWidth(0.0f);
            getPaint().setStyle(Paint.Style.FILL);
        }
        super.onDraw(canvas);
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        AbstractC115335Rd r0 = this.A01;
        if (r0 != null) {
            C1110257u r02 = (C1110257u) r0;
            AbstractC471028y r3 = r02.A00;
            AnonymousClass290 r2 = r02.A01;
            if (i == 4 && keyEvent.getAction() == 1) {
                r3.A06.A05(r3.A09);
                AnonymousClass3YM r22 = (AnonymousClass3YM) r2;
                r22.A04.A03 = r3.A08.getText().toString();
                r22.dismiss();
            }
        }
        return super.onKeyPreIme(i, keyEvent);
    }

    public void setFontStyle(int i) {
        Typeface typeface;
        if (this.A00 != i) {
            this.A00 = i;
            if (i == 3) {
                typeface = AnonymousClass33L.A02(getContext());
            } else if (i == 2) {
                typeface = AnonymousClass33L.A03(getContext());
            } else if (i == 1) {
                typeface = Typeface.DEFAULT_BOLD;
            } else {
                typeface = Typeface.DEFAULT;
            }
            setTypeface(typeface);
            if (i == 3) {
                setAllCaps(true);
            } else {
                setAllCaps(false);
            }
        }
    }

    public void setOnKeyPreImeListener(AbstractC115335Rd r1) {
        this.A01 = r1;
    }
}
