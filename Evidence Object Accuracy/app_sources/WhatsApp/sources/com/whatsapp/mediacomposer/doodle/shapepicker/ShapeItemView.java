package com.whatsapp.mediacomposer.doodle.shapepicker;

import X.AnonymousClass004;
import X.AnonymousClass03X;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class ShapeItemView extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public String A01;
    public boolean A02;

    public ShapeItemView(Context context) {
        super(context, null);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ShapeItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }

    public ShapeItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ShapeItemView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
        setMeasuredDimension(defaultSize, defaultSize);
    }
}
