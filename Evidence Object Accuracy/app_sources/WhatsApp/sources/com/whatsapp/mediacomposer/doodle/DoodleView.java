package com.whatsapp.mediacomposer.doodle;

import X.AbstractC454621s;
import X.AbstractC454821u;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass33L;
import X.AnonymousClass3EC;
import X.AnonymousClass3JD;
import X.AnonymousClass3MO;
import X.AnonymousClass5Yv;
import X.C28391Mz;
import X.C454721t;
import X.C454921v;
import X.C47322Ae;
import X.C52462aq;
import X.C64243Eu;
import X.C89514Kg;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;
import org.json.JSONException;

/* loaded from: classes2.dex */
public class DoodleView extends View implements AbstractC454621s, AnonymousClass004 {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public AnonymousClass018 A04;
    public AnonymousClass19M A05;
    public AnonymousClass5Yv A06;
    public C47322Ae A07;
    public AnonymousClass3MO A08;
    public AnonymousClass1AB A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;
    public final RectF A0C;
    public final Handler A0D;
    public final C454921v A0E;
    public final C64243Eu A0F;
    public final AnonymousClass3EC A0G;
    public final C454721t A0H;
    public final Runnable A0I;

    public DoodleView(Context context) {
        super(context);
        A02();
        this.A03 = -65536;
        this.A01 = 8.0f;
        this.A02 = 8.0f;
        this.A0D = new Handler();
        this.A0I = new RunnableBRunnable0Shape8S0100000_I0_8(this, 9);
        C454721t r3 = new C454721t();
        this.A0H = r3;
        C64243Eu r2 = new C64243Eu();
        this.A0F = r2;
        this.A0G = new AnonymousClass3EC(r2);
        this.A0E = new C454921v(new C89514Kg(this), r2, r3);
        this.A0C = new RectF();
        if (C28391Mz.A03()) {
            setLayerType(2, null);
        }
    }

    public DoodleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
        this.A03 = -65536;
        this.A01 = 8.0f;
        this.A02 = 8.0f;
        this.A0D = new Handler();
        this.A0I = new RunnableBRunnable0Shape8S0100000_I0_8(this, 9);
        C454721t r3 = new C454721t();
        this.A0H = r3;
        C64243Eu r2 = new C64243Eu();
        this.A0F = r2;
        this.A0G = new AnonymousClass3EC(r2);
        this.A0E = new C454921v(new C89514Kg(this), r2, r3);
        this.A0C = new RectF();
        if (C28391Mz.A03()) {
            setLayerType(2, null);
        }
    }

    public DoodleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
        this.A03 = -65536;
        this.A01 = 8.0f;
        this.A02 = 8.0f;
        this.A0D = new Handler();
        this.A0I = new RunnableBRunnable0Shape8S0100000_I0_8(this, 9);
        C454721t r3 = new C454721t();
        this.A0H = r3;
        C64243Eu r2 = new C64243Eu();
        this.A0F = r2;
        this.A0G = new AnonymousClass3EC(r2);
        this.A0E = new C454921v(new C89514Kg(this), r2, r3);
        this.A0C = new RectF();
        if (C28391Mz.A03()) {
            setLayerType(2, null);
        }
    }

    public DoodleView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A02();
        this.A03 = -65536;
        this.A01 = 8.0f;
        this.A02 = 8.0f;
        this.A0D = new Handler();
        this.A0I = new RunnableBRunnable0Shape8S0100000_I0_8(this, 9);
        C454721t r3 = new C454721t();
        this.A0H = r3;
        C64243Eu r2 = new C64243Eu();
        this.A0F = r2;
        this.A0G = new AnonymousClass3EC(r2);
        this.A0E = new C454921v(new C89514Kg(this), r2, r3);
        this.A0C = new RectF();
        if (C28391Mz.A03()) {
            setLayerType(2, null);
        }
    }

    public DoodleView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A02();
    }

    public AbstractC454821u A00(MotionEvent motionEvent) {
        if (!A04() || motionEvent.getPointerCount() != 1) {
            return null;
        }
        return this.A0H.A00(this.A0G.A00(motionEvent.getX(), motionEvent.getY()));
    }

    public AbstractC454821u A01(MotionEvent motionEvent) {
        if (!A04() || motionEvent.getPointerCount() != 2) {
            return null;
        }
        AnonymousClass3EC r2 = this.A0G;
        PointF A00 = r2.A00(motionEvent.getX(0), motionEvent.getY(0));
        PointF A002 = r2.A00(motionEvent.getX(1), motionEvent.getY(1));
        C454721t r4 = this.A0H;
        AbstractC454821u A003 = r4.A00(A00);
        if (A003 != null) {
            return A003;
        }
        AbstractC454821u A004 = r4.A00(A002);
        if (A004 == null) {
            return r4.A00(new PointF((A00.x + A002.x) / 2.0f, (A00.y + A002.y) / 2.0f));
        }
        return A004;
    }

    public void A02() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A05 = (AnonymousClass19M) r1.A6R.get();
            this.A04 = (AnonymousClass018) r1.ANb.get();
            this.A09 = (AnonymousClass1AB) r1.AKI.get();
        }
    }

    public void A03(AbstractC454821u r13) {
        float f;
        float f2;
        C64243Eu r2 = this.A0F;
        RectF rectF = r2.A07;
        float width = rectF.width();
        float height = rectF.height();
        boolean z = r13 instanceof AnonymousClass33L;
        if (z) {
            f = (width * 7.0f) / 8.0f;
            f2 = height / 10.0f;
        } else {
            f = width / 2.0f;
            f2 = height / 2.0f;
        }
        PointF centerPoint = getCenterPoint();
        float f3 = centerPoint.x;
        float f4 = f / 2.0f;
        float f5 = centerPoint.y;
        float f6 = f2 / 2.0f;
        r13.A0Q(rectF, f3 - f4, f5 - f6, f3 + f4, f5 + f6);
        if (r13.A0J() && !z) {
            r13.A09(this.A03);
        }
        if (r13.A0K()) {
            r13.A0O(AbstractC454821u.A04 / this.A00);
        }
        r13.A08(1.0f / r2.A01, 2);
        r13.A00 += (float) (-r2.A02);
        C454721t r1 = this.A0H;
        r1.A03(r13);
        if (r13.A0B() && !r1.A06()) {
            this.A0D.postDelayed(this.A0I, 1000);
        }
        this.A07.A02 = false;
        AnonymousClass5Yv r0 = this.A06;
        if (r0 != null) {
            r0.AVy(r13);
        }
        invalidate();
    }

    public boolean A04() {
        C64243Eu r1 = this.A0F;
        return (r1.A06 == null || r1.A07 == null) ? false : true;
    }

    @Override // X.AbstractC454621s
    public void Acv(float f, int i) {
        C454721t r2 = this.A0H;
        AbstractC454821u r3 = r2.A01;
        if (!(r3 == null || r3 == r2.A02 || (!r3.A0K() && !r3.A0J()))) {
            r2.A00 = r3.A03();
            r3 = r2.A01;
            r2.A02 = r3;
        }
        this.A02 = f;
        float f2 = this.A00;
        if (f2 == 0.0f) {
            this.A01 = f;
        } else {
            this.A01 = f / f2;
        }
        this.A03 = i;
        C47322Ae r0 = this.A07;
        if (r0 != null && !r0.A02 && r3 != null) {
            if (r3.A0K() || r3.A0J()) {
                if (r3.A0J()) {
                    r3.A09(i);
                }
                AbstractC454821u r1 = r2.A01;
                if (r1.A0K()) {
                    r1.A0O(this.A01);
                }
                AbstractC454821u r4 = r2.A01;
                if (r4 instanceof AnonymousClass33L) {
                    AnonymousClass33L r42 = (AnonymousClass33L) r4;
                    float f3 = AbstractC454821u.A07;
                    float f4 = AbstractC454821u.A04;
                    float f5 = (f3 - f4) / 4.0f;
                    int i2 = 0;
                    if (f >= f4 + f5) {
                        i2 = 1;
                        if (f >= (2.0f * f5) + f4) {
                            i2 = 3;
                            if (f < f4 + (f5 * 3.0f)) {
                                i2 = 2;
                            }
                        }
                    }
                    r42.A0S(i2);
                }
                invalidate();
            }
        }
    }

    @Override // android.view.View
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return onTouchEvent(motionEvent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:0x013f, code lost:
        if (r2.A03 == ((int) r5.height())) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0051  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r13) {
        /*
        // Method dump skipped, instructions count: 645
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.doodle.DoodleView.draw(android.graphics.Canvas):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    private PointF getCenterPoint() {
        C64243Eu r2 = this.A0F;
        if (r2.A05 != null) {
            return this.A0G.A00(getX() + ((float) (getMeasuredWidth() >> 1)), getY() + ((float) (getMeasuredHeight() >> 1)));
        }
        return new PointF(r2.A07.centerX(), r2.A07.centerY());
    }

    public C454921v getDoodleRender() {
        return this.A0E;
    }

    public AnonymousClass3EC getPointsUtil() {
        return this.A0G;
    }

    public C454721t getShapeRepository() {
        return this.A0H;
    }

    public C64243Eu getState() {
        return this.A0F;
    }

    public float getStrokeScale() {
        return this.A00;
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.A0H.A06()) {
            this.A0D.postDelayed(this.A0I, 1000);
        }
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        this.A0D.removeCallbacks(this.A0I);
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        C64243Eu r6 = this.A0F;
        RectF rectF = r6.A07;
        if (rectF != null) {
            RectF rectF2 = this.A0C;
            rectF2.set(rectF);
            r6.A09.mapRect(rectF2);
            float measuredWidth = (float) getMeasuredWidth();
            float measuredHeight = (float) getMeasuredHeight();
            float width = rectF2.width() / rectF2.height();
            if (measuredWidth / measuredHeight < width) {
                measuredHeight = measuredWidth / width;
            } else {
                measuredWidth = measuredHeight * width;
            }
            r6.A00 = measuredWidth / rectF2.width();
            if (this.A00 == 0.0f || !(!this.A0H.A04.isEmpty())) {
                float f = r6.A00;
                this.A00 = f;
                this.A01 = this.A02 / f;
            }
            r6.A0B.set((((float) getMeasuredWidth()) - measuredWidth) / 2.0f, (((float) getMeasuredHeight()) - measuredHeight) / 2.0f, (((float) getMeasuredWidth()) + measuredWidth) / 2.0f, (((float) getMeasuredHeight()) + measuredHeight) / 2.0f);
            r6.A08 = getResources().getDisplayMetrics();
            r6.A03 = getMeasuredHeight();
            r6.A04 = getMeasuredWidth();
            C454921v r2 = this.A0E;
            if (r2.A04(false) || r2.A03(false)) {
                r2.A02();
            }
        }
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C52462aq r6 = (C52462aq) parcelable;
        String str = r6.A01;
        if (!TextUtils.isEmpty(str)) {
            AnonymousClass3JD A03 = AnonymousClass3JD.A03(getContext(), this.A04, this.A05, this.A09, str);
            if (A03 != null) {
                C64243Eu r2 = this.A0F;
                r2.A00(A03);
                C454721t r0 = this.A0H;
                r0.A02();
                r0.A04.addAll(A03.A06);
                r2.A08 = getResources().getDisplayMetrics();
                this.A0E.A02();
            }
            this.A0H.A05(r6.A02);
        }
        this.A07.A02 = r6.A03;
        this.A02 = r6.A00;
        requestLayout();
        this.A0E.A01();
        super.onRestoreInstanceState(r6.getSuperState());
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        String str;
        String str2;
        RectF rectF;
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        C64243Eu r0 = this.A0F;
        RectF rectF2 = r0.A06;
        if (rectF2 == null || (rectF = r0.A07) == null) {
            str = null;
        } else {
            str = new AnonymousClass3JD(rectF2, rectF, this.A0H.A05, r0.A02).A04();
        }
        C454721t r02 = this.A0H;
        try {
            str2 = r02.A03.A01(r02.A04);
        } catch (JSONException e) {
            Log.e("ShapeRepository/getUndoJson", e);
            str2 = null;
        }
        return new C52462aq(onSaveInstanceState, str, str2, this.A02, this.A07.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002e, code lost:
        if (r2 != 6) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a7, code lost:
        if (r1 != 6) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01b6, code lost:
        if (r2.getStrokeWidth() == r7.A01) goto L_0x01c8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ae A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0121  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r15) {
        /*
        // Method dump skipped, instructions count: 677
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.doodle.DoodleView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setControllers(AnonymousClass3MO r1, C47322Ae r2) {
        this.A08 = r1;
        this.A07 = r2;
    }

    public void setDoodle(AnonymousClass3JD r4) {
        C64243Eu r2 = this.A0F;
        r2.A00(r4);
        C454721t r0 = this.A0H;
        r0.A02();
        r0.A04.addAll(r4.A06);
        r2.A08 = getResources().getDisplayMetrics();
        C454921v r02 = this.A0E;
        r02.A02();
        requestLayout();
        r02.A01();
        invalidate();
    }

    public void setDoodleViewListener(AnonymousClass5Yv r2) {
        this.A06 = r2;
        this.A07.A00 = r2;
    }

    public void setStrokeColor(int i) {
        this.A03 = i;
    }

    public void setStrokeScale(float f) {
        this.A00 = f;
    }

    public void setStrokeWidth(float f) {
        this.A01 = f;
    }
}
