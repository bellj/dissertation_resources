package com.whatsapp.mediacomposer.doodle.textentry;

import X.AbstractC471028y;
import X.AnonymousClass028;
import X.AnonymousClass290;
import X.C12960it;
import X.C91494Ry;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Window;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* loaded from: classes2.dex */
public class TextEntryWave2View extends AbstractC471028y {
    public WaButton A00;

    public TextEntryWave2View(Context context) {
        this(context, null);
    }

    public TextEntryWave2View(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TextEntryWave2View(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // X.AbstractC471028y
    public void A00(Window window, AnonymousClass290 r3, C91494Ry r4, int[] iArr, boolean z) {
        super.A00(window, r3, r4, iArr, true);
        this.A00 = (WaButton) AnonymousClass028.A0D(this, R.id.done);
        setDoneListener(r3);
    }

    private void setDoneListener(AnonymousClass290 r3) {
        C12960it.A14(this.A00, this, r3, 19);
    }
}
