package com.whatsapp.mediacomposer.doodle.textentry;

import X.AbstractC471028y;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes3.dex */
public class TextEntryLegacyView extends AbstractC471028y {
    public TextEntryLegacyView(Context context) {
        this(context, null);
    }

    public TextEntryLegacyView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TextEntryLegacyView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
