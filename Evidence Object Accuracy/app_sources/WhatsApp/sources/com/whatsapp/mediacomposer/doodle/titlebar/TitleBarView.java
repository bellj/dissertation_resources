package com.whatsapp.mediacomposer.doodle.titlebar;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2GF;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2ZW;
import X.AnonymousClass33L;
import X.C12960it;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class TitleBarView extends RelativeLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public ImageView A02;
    public ImageView A03;
    public ImageView A04;
    public ImageView A05;
    public ImageView A06;
    public RelativeLayout A07;
    public WaButton A08;
    public WaTextView A09;
    public AnonymousClass018 A0A;
    public AnonymousClass2ZW A0B;
    public AnonymousClass2ZW A0C;
    public AnonymousClass2ZW A0D;
    public AnonymousClass2P7 A0E;
    public boolean A0F;

    public TitleBarView(Context context) {
        this(context, null);
    }

    public TitleBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TitleBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0F) {
            this.A0F = true;
            this.A0A = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0E;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0E = r0;
        }
        return r0.generatedComponent();
    }

    public final int getCropToolId() {
        return this.A03.getId();
    }

    public float getCropToolOffsetX() {
        return (this.A05.getX() - this.A03.getX()) - this.A03.getTranslationX();
    }

    public float getDeleteToolOffsetX() {
        return (this.A05.getX() - this.A04.getX()) - this.A04.getTranslationX();
    }

    public final int getPenToolId() {
        return this.A05.getId();
    }

    public final int getShapeToolId() {
        return this.A06.getId();
    }

    public float getShapeToolOffsetX() {
        return (this.A05.getX() - this.A06.getX()) - this.A06.getTranslationX();
    }

    public View getStartingViewFromToolbarExtra() {
        return this.A02;
    }

    public final int getTextToolId() {
        return this.A09.getId();
    }

    public float getTextToolOffsetX() {
        return (this.A05.getX() - this.A09.getX()) - this.A09.getTranslationX();
    }

    public final RelativeLayout getToolbarExtra() {
        return this.A07;
    }

    public void setBackButtonDrawable(int i) {
        AnonymousClass2GF.A01(getContext(), this.A02, this.A0A, i);
        this.A02.setVisibility(0);
        this.A08.setVisibility(8);
    }

    public void setCloseButtonAlpha(float f) {
        this.A02.setAlpha(f);
    }

    public final void setCropToolVisibility(int i) {
        this.A03.setVisibility(i);
    }

    public void setCropToolX(float f) {
        this.A03.setTranslationX(f);
    }

    public final void setDeleteButtonVisibility(int i) {
        this.A04.setVisibility(i);
    }

    public void setDeleteToolX(float f) {
        this.A04.setTranslationX(f);
    }

    public void setFont(int i) {
        Typeface typeface;
        WaTextView waTextView = this.A09;
        if (i == 3) {
            typeface = AnonymousClass33L.A02(getContext());
        } else if (i == 2) {
            typeface = AnonymousClass33L.A03(getContext());
        } else if (i == 1) {
            typeface = Typeface.DEFAULT_BOLD;
        } else {
            typeface = Typeface.DEFAULT;
        }
        waTextView.setTypeface(typeface);
    }

    public void setPenToolDrawableStrokePreview(boolean z) {
        this.A0B.A04 = z;
    }

    public void setShapeToolDrawableStrokePreview(boolean z) {
        this.A0C.A04 = z;
    }

    public void setShapeToolX(float f) {
        this.A06.setTranslationX(f);
    }

    public void setTextToolX(float f) {
        this.A09.setTranslationX(f);
    }

    public void setToolBarExtra(RelativeLayout relativeLayout) {
        this.A07 = relativeLayout;
    }

    public final void setToolbarExtraVisibility(int i) {
        this.A07.setVisibility(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r5 == 4) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setUndoButtonVisibility(int r5) {
        /*
            r4 = this;
            android.view.View r0 = r4.A01
            int r0 = r0.getVisibility()
            if (r0 == r5) goto L_0x0031
            android.view.View r0 = r4.A01
            r0.setVisibility(r5)
            r3 = 1065353216(0x3f800000, float:1.0)
            r1 = 4
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r5 == r1) goto L_0x0017
            r0 = 0
            if (r5 != r1) goto L_0x0018
        L_0x0017:
            r3 = 0
        L_0x0018:
            android.view.animation.AlphaAnimation r2 = new android.view.animation.AlphaAnimation
            r2.<init>(r0, r3)
            if (r5 != r1) goto L_0x0032
            X.07o r0 = new X.07o
            r0.<init>()
        L_0x0024:
            r2.setInterpolator(r0)
            r0 = 100
            r2.setDuration(r0)
            android.view.View r0 = r4.A01
            r0.startAnimation(r2)
        L_0x0031:
            return
        L_0x0032:
            X.078 r0 = new X.078
            r0.<init>()
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView.setUndoButtonVisibility(int):void");
    }

    public void setUndoToolX(float f) {
        this.A01.setTranslationX(f);
    }
}
