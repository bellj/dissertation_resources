package com.whatsapp.mediacomposer.doodle;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass5RZ;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import X.C52432an;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class ColorPickerView extends View implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public Bitmap A07;
    public AnonymousClass018 A08;
    public AnonymousClass5RZ A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;
    public boolean A0C;
    public int[] A0D;
    public final Paint A0E;
    public final Paint A0F;

    public ColorPickerView(Context context) {
        super(context);
        A00();
        this.A0E = C12990iw.A0F();
        this.A0F = C12960it.A0A();
    }

    public ColorPickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0E = C12990iw.A0F();
        this.A0F = C12960it.A0A();
        A02(context, attributeSet);
    }

    public ColorPickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0E = C12990iw.A0F();
        this.A0F = C12960it.A0A();
        A02(context, attributeSet);
    }

    public ColorPickerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A0E = C12990iw.A0F();
        this.A0F = C12960it.A0A();
        A02(context, attributeSet);
    }

    public ColorPickerView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0C) {
            this.A0C = true;
            this.A08 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A01() {
        int i;
        int i2 = this.A01;
        int i3 = i2 / 10;
        int i4 = i2 / 30;
        int i5 = ((i2 - i3) - i4) - i3;
        int i6 = 0;
        while (true) {
            if (i6 < i3) {
                int i7 = (i6 * 255) / i3;
                this.A0D[i6] = i7 | -16777216 | (i7 << 16) | (i7 << 8);
                i6++;
            }
        }
        for (i = 0; i < i4; i++) {
            this.A0D[i3 + i] = -1;
        }
        float[] fArr = new float[3];
        fArr[0] = 0.0f;
        fArr[2] = 1.0f;
        for (int i8 = 0; i8 < i3; i8++) {
            fArr[1] = ((float) i8) / ((float) i3);
            this.A0D[i3 + i4 + i8] = Color.HSVToColor(fArr);
        }
        fArr[1] = 0.8f;
        fArr[2] = 1.0f;
        for (int i9 = 0; i9 < i5; i9++) {
            fArr[0] = (((float) i9) * 360.0f) / ((float) i5);
            this.A0D[i3 + i4 + i3 + i9] = Color.HSVToColor(fArr);
        }
        Path path = new Path();
        path.setFillType(Path.FillType.WINDING);
        float[] fArr2 = new float[8];
        Arrays.fill(fArr2, ((float) this.A04) / 0.8f);
        path.addRoundRect(new RectF(1.0f, 1.0f, (float) (this.A04 - 1), (float) (this.A01 - 1)), fArr2, Path.Direction.CW);
        Bitmap bitmap = this.A07;
        if (!(bitmap != null && bitmap.getWidth() == this.A04 && this.A07.getHeight() == this.A01)) {
            this.A07 = Bitmap.createBitmap(this.A04, this.A01, Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(this.A07);
        Paint paint = this.A0E;
        C12990iw.A13(paint);
        canvas.save();
        canvas.clipPath(path);
        paint.setStrokeWidth(1.0f);
        for (int i10 = 0; i10 < this.A01; i10++) {
            paint.setColor(this.A0D[i10]);
            float f = (float) i10;
            canvas.drawLine(0.0f, f, (float) this.A04, f, paint);
        }
        canvas.restore();
        Paint paint2 = this.A0F;
        C12980iv.A12(getContext(), paint2, R.color.color_picker_border_color);
        C12990iw.A13(paint2);
        paint2.setStrokeWidth((float) getResources().getDimensionPixelSize(R.dimen.status_color_picker_border_width));
        canvas.drawPath(path, paint2);
    }

    public final void A02(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A05);
            this.A04 = (int) obtainStyledAttributes.getDimension(2, 1.0f);
            this.A06 = (int) obtainStyledAttributes.getDimension(1, 1.0f);
            this.A05 = (int) obtainStyledAttributes.getDimension(0, 1.0f);
            this.A00 = (float) this.A06;
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        int paddingRight;
        super.draw(canvas);
        if (C28141Kv.A01(this.A08)) {
            paddingRight = C12980iv.A08(this);
        } else {
            paddingRight = this.A04 + getPaddingRight();
        }
        canvas.drawBitmap(this.A07, (float) paddingRight, (float) getPaddingTop(), this.A0E);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public int getColor() {
        return this.A02;
    }

    public float getMinSize() {
        return (float) this.A06;
    }

    public float getSize() {
        return this.A00;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A03 != 0 && getMeasuredHeight() > this.A03) {
            setMeasuredDimension(getMeasuredWidth(), this.A03);
        }
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C52432an r2 = (C52432an) parcelable;
        this.A02 = r2.A01;
        this.A00 = r2.A00;
        super.onRestoreInstanceState(r2.getSuperState());
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        return new C52432an(super.onSaveInstanceState(), this.A00, this.A02);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int max = Math.max(1, (i2 - getPaddingTop()) - getPaddingBottom());
        this.A01 = max;
        int[] iArr = this.A0D;
        if (iArr == null || iArr.length < max) {
            this.A0D = new int[max];
        }
        A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0053, code lost:
        if (r7 != 6) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c9, code lost:
        if (r7 != 1) goto L_0x0055;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r10) {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.doodle.ColorPickerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    private void setColor(int i) {
        this.A02 = i;
    }

    public void setColorAndInvalidate(int i) {
        this.A02 = i;
        invalidate();
    }

    public void setListener(AnonymousClass5RZ r1) {
        this.A09 = r1;
    }

    public void setMaxHeight(int i) {
        this.A03 = i;
    }

    private void setSize(float f) {
        this.A00 = f;
    }

    public void setSizeAndInvalidate(float f) {
        this.A00 = f;
        invalidate();
    }
}
