package com.whatsapp.mediacomposer.doodle.shapepicker;

import X.AbstractC115325Rc;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass16A;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C28411Nc;
import X.C623937b;
import X.C64243Eu;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/* loaded from: classes2.dex */
public class ShapePickerView extends RelativeLayout implements AbstractC115325Rc, AnonymousClass004 {
    public ValueAnimator A00;
    public Bitmap A01;
    public Bitmap A02;
    public RectF A03;
    public AbstractC15710nm A04;
    public C64243Eu A05;
    public C623937b A06;
    public AbstractC14440lR A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public final Paint A0A;

    public ShapePickerView(Context context) {
        super(context);
        A00();
        this.A0A = C12960it.A0A();
    }

    public ShapePickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0A = C12960it.A0A();
    }

    public ShapePickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0A = C12960it.A0A();
    }

    public ShapePickerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A04 = C12970iu.A0Q(A00);
            this.A07 = C12960it.A0T(A00);
        }
    }

    public void A01(Bitmap bitmap, C64243Eu r11) {
        if (A02()) {
            invalidate();
        }
        if (r11.A04 <= 0 || r11.A03 <= 0) {
            ((AnonymousClass16A) this.A04).A08(new C28411Nc("shape-picker-doodle-view-state-dimen"), "shape-picker-doodle-view-state-dimen", r11.toString(), C12970iu.A11(), true);
            return;
        }
        this.A05 = r11;
        this.A02 = bitmap;
        RectF rectF = r11.A0B;
        this.A03 = new RectF(rectF);
        C623937b r2 = new C623937b(r11.A0A, new Rect(0, 0, r11.A04, r11.A03), new Rect((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom), this);
        this.A06 = r2;
        this.A07.Aaz(r2, bitmap);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r4.A02 != null) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02() {
        /*
            r4 = this;
            X.37b r0 = r4.A06
            X.C12980iv.A1M(r0)
            android.animation.ValueAnimator r0 = r4.A00
            if (r0 == 0) goto L_0x000c
            r0.cancel()
        L_0x000c:
            r3 = 0
            r4.A03 = r3
            android.graphics.Bitmap r2 = r4.A01
            if (r2 != 0) goto L_0x0018
            android.graphics.Bitmap r0 = r4.A02
            r1 = 0
            if (r0 == 0) goto L_0x0020
        L_0x0018:
            r1 = 1
            if (r2 == 0) goto L_0x0020
            r2.recycle()
            r4.A01 = r3
        L_0x0020:
            android.graphics.Bitmap r0 = r4.A02
            if (r0 == 0) goto L_0x0029
            r0.recycle()
            r4.A02 = r3
        L_0x0029:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerView.A02():boolean");
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Bitmap bitmap = this.A01;
        if (bitmap != null) {
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, this.A0A);
        }
        super.draw(canvas);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        C64243Eu r1 = this.A05;
        if (r1 != null && this.A02 != null && !r1.A0B.equals(this.A03)) {
            A01(this.A02.copy(Bitmap.Config.ARGB_8888, true), this.A05);
        }
    }
}
