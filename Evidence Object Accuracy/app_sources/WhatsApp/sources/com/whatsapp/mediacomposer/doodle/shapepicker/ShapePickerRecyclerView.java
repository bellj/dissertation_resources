package com.whatsapp.mediacomposer.doodle.shapepicker;

import X.AbstractC05520Pw;
import X.AbstractC55252i3;
import X.AnonymousClass02M;
import X.AnonymousClass1KX;
import X.C12960it;
import X.C12970iu;
import X.C74713iY;
import X.C74733ih;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.AttributeSet;
import androidx.recyclerview.widget.GridLayoutManager;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ShapePickerRecyclerView extends AbstractC55252i3 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public GridLayoutManager A04;
    public AbstractC05520Pw A05;

    public ShapePickerRecyclerView(Context context) {
        super(context);
        A0z(context);
    }

    public ShapePickerRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0z(context);
    }

    public ShapePickerRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0z(context);
    }

    public final void A0y() {
        if (this.A01 == -1) {
            int measuredWidth = getMeasuredWidth();
            int i = this.A02;
            int i2 = i + this.A03;
            int i3 = measuredWidth / i2;
            if ((i2 * i3) + i <= getMeasuredWidth()) {
                i3++;
            }
            this.A01 = Math.max(1, i3);
        }
    }

    public final void A0z(Context context) {
        this.A02 = context.getResources().getDimensionPixelOffset(R.dimen.shape_picker_new_shape_size);
        boolean A1V = C12960it.A1V(C12960it.A09(this).getConfiguration().orientation, 1);
        Resources A09 = C12960it.A09(this);
        int i = R.dimen.shape_picker_shape_landscape_spacing;
        if (A1V) {
            i = R.dimen.shape_picker_shape_portrait_spacing;
        }
        this.A03 = A09.getDimensionPixelSize(i);
        this.A00 = -1;
        this.A01 = -1;
        this.A0h = true;
        this.A05 = new C74733ih(context, this);
    }

    public final void A10(GridLayoutManager gridLayoutManager, AnonymousClass02M r4) {
        if (r4 instanceof AnonymousClass1KX) {
            AnonymousClass1KX r42 = (AnonymousClass1KX) r4;
            gridLayoutManager.A01 = new C74713iY(gridLayoutManager, r42, this);
            int i = this.A01;
            if (r42.A00 != i) {
                r42.A00 = i;
                if (r42.A02 == null) {
                    r42.A0G();
                }
            }
        }
    }

    public void A11(boolean z, boolean z2) {
        int i;
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.shape_picker_recycler_view_horizontal_padding);
        Resources resources = getResources();
        int i2 = R.dimen.shape_picker_recycler_view_portrait_top_padding;
        if (z) {
            i2 = R.dimen.shape_picker_recycler_view_landscape_top_padding;
        }
        int dimensionPixelSize2 = resources.getDimensionPixelSize(i2);
        if (z2) {
            Resources resources2 = getResources();
            int i3 = R.dimen.shape_picker_recycler_view_portrait_bottom_padding;
            if (z) {
                i3 = R.dimen.shape_picker_recycler_view_landscape_bottom_padding;
            }
            i = resources2.getDimensionPixelSize(i3);
        } else {
            i = 0;
        }
        setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, i);
    }

    public int getActualShapeSpacing() {
        int i = this.A00;
        if (i == -1) {
            A0y();
            if (this.A01 > 1) {
                int measuredWidth = getMeasuredWidth();
                int i2 = this.A01;
                i = (measuredWidth - (this.A02 * i2)) / (i2 - 1);
            } else {
                i = 0;
            }
            this.A00 = i;
        }
        return i;
    }

    public int getAdapterItemCount() {
        AnonymousClass02M r0 = this.A0N;
        if (r0 != null) {
            return r0.A0D();
        }
        throw C12960it.A0U("Must set adapter first");
    }

    public int getColumnCount() {
        A0y();
        return this.A01;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        boolean A1W = C12970iu.A1W(configuration.orientation);
        Resources A09 = C12960it.A09(this);
        int i = R.dimen.shape_picker_shape_landscape_spacing;
        if (A1W) {
            i = R.dimen.shape_picker_shape_portrait_spacing;
        }
        this.A03 = A09.getDimensionPixelSize(i);
        this.A00 = -1;
        this.A01 = -1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.A00 = -1;
        this.A01 = -1;
        A0y();
        GridLayoutManager gridLayoutManager = (GridLayoutManager) getLayoutManager();
        int i3 = this.A01;
        gridLayoutManager.A1h(i3 * i3);
        A10(gridLayoutManager, this.A0N);
        A0M();
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(AnonymousClass02M r3) {
        super.setAdapter(r3);
        this.A00 = -1;
        this.A01 = -1;
        A0y();
        getContext();
        int i = this.A01;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(i * i);
        this.A04 = gridLayoutManager;
        A10(gridLayoutManager, r3);
        setLayoutManager(this.A04);
    }
}
