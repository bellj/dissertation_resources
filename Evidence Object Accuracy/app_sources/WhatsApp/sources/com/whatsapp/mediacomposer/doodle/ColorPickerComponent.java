package com.whatsapp.mediacomposer.doodle;

import X.AbstractC454621s;
import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass0L1;
import X.AnonymousClass2P7;
import X.AnonymousClass334;
import X.AnonymousClass3GV;
import X.AnonymousClass5W4;
import X.C1109557n;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C64453Fp;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ColorPickerComponent extends LinearLayout implements AnonymousClass004 {
    public C64453Fp A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final View A03;
    public final ViewGroup A04;
    public final ColorPickerView A05;

    public ColorPickerComponent(Context context) {
        this(context, null);
    }

    public ColorPickerComponent(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ColorPickerComponent(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        ViewGroup viewGroup = (ViewGroup) C12960it.A0E(this).inflate(R.layout.color_picker_wave_one, (ViewGroup) this, true);
        this.A04 = viewGroup;
        ColorPickerView colorPickerView = (ColorPickerView) AnonymousClass028.A0D(viewGroup, R.id.color_picker);
        this.A05 = colorPickerView;
        this.A03 = AnonymousClass028.A0D(viewGroup, R.id.color_picker_container);
        AnonymousClass028.A0a(colorPickerView, 1);
        A02(colorPickerView.A02);
        A03(getResources().getConfiguration().orientation);
    }

    public void A00() {
        ColorPickerView colorPickerView = this.A05;
        if (colorPickerView.getVisibility() != 0) {
            colorPickerView.setVisibility(0);
            A01(R.anim.color_picker_animate_in);
        }
        C64453Fp r1 = this.A00;
        if (r1 != null && (r1 instanceof AnonymousClass334)) {
            ((AnonymousClass334) r1).A00.A1K(false, true);
        }
    }

    public final void A01(int i) {
        ColorPickerView colorPickerView = this.A05;
        colorPickerView.clearAnimation();
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), i);
        loadAnimation.setInterpolator(AnonymousClass0L1.A00(0.5f, 1.35f, 0.4f, 1.0f));
        colorPickerView.startAnimation(loadAnimation);
    }

    public final void A02(int i) {
        Integer[] numArr = AnonymousClass3GV.A01;
        int i2 = -1;
        double d = Double.MAX_VALUE;
        for (int i3 = 0; i3 < numArr.length; i3++) {
            double[] A00 = AnonymousClass3GV.A00(numArr[i3].intValue());
            double[] A002 = AnonymousClass3GV.A00(i);
            double d2 = A00[0] - A002[0];
            double d3 = A00[1];
            double d4 = A002[1];
            double d5 = A00[2];
            double d6 = A002[2];
            double d7 = d5 - d6;
            double sqrt = Math.sqrt(Math.pow(d3, 2.0d) + Math.pow(d5, 2.0d));
            double sqrt2 = sqrt - Math.sqrt(Math.pow(d4, 2.0d) + Math.pow(d6, 2.0d));
            double pow = (Math.pow(d3 - d4, 2.0d) + Math.pow(d7, 2.0d)) - Math.pow(sqrt2, 2.0d);
            double d8 = 0.0d;
            if (pow > 0.0d) {
                d8 = Math.sqrt(pow);
            }
            double sqrt3 = Math.sqrt((double) ((float) (Math.pow(d2 / 1.0d, 2.0d) + Math.pow(sqrt2 / ((0.045d * sqrt) + 1.0d), 2.0d) + Math.pow(d8 / ((sqrt * 0.015d) + 1.0d), 2.0d))));
            if (sqrt3 < d) {
                i2 = numArr[i3].intValue();
                d = sqrt3;
            }
        }
        this.A05.setContentDescription(C12990iw.A0o(getResources(), getResources().getString(((Number) C12990iw.A0l(AnonymousClass3GV.A00, i2)).intValue()), C12970iu.A1b(), 0, R.string.doodle_color_picker_with_color));
    }

    public final void A03(int i) {
        int i2;
        View view = this.A03;
        int paddingLeft = getPaddingLeft();
        int dimension = (int) getResources().getDimension(R.dimen.color_picker_top_padding);
        int paddingRight = getPaddingRight();
        if (i == 2) {
            i2 = (int) getResources().getDimension(R.dimen.color_picker_bottom_padding);
        } else {
            i2 = 0;
        }
        view.setPadding(paddingLeft, dimension, paddingRight, i2);
    }

    public void A04(C64453Fp r4, AnonymousClass5W4 r5, AbstractC454621s r6) {
        this.A00 = r4;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = (int) getResources().getDimension(R.dimen.color_picker_container_height);
        setLayoutParams(layoutParams);
        if (r6 != null) {
            ColorPickerView colorPickerView = this.A05;
            r6.Acv(colorPickerView.A00, colorPickerView.A02);
        }
        this.A05.A09 = new C1109557n(r5, this, r6);
    }

    public void A05(boolean z) {
        ColorPickerView colorPickerView = this.A05;
        if (colorPickerView.getVisibility() == 0) {
            if (z) {
                A01(R.anim.color_picker_animate_out);
            }
            colorPickerView.setVisibility(4);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public int getColorPickerHeight() {
        return this.A05.getHeight();
    }

    public float getMinSize() {
        return (float) this.A05.A06;
    }

    public int getSelectedColor() {
        return this.A05.A02;
    }

    public float getSelectedStrokeSize() {
        return this.A05.A00;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A03(configuration.orientation);
    }

    public void setColorAndInvalidate(int i) {
        this.A05.setColorAndInvalidate(i);
    }

    public void setInsets(Rect rect) {
        ViewGroup viewGroup = this.A04;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(viewGroup);
        A0H.leftMargin = rect.left;
        A0H.topMargin = rect.top;
        A0H.rightMargin = rect.right;
        A0H.bottomMargin = rect.bottom;
        viewGroup.setLayoutParams(A0H);
    }

    public void setMaxHeight(int i) {
        this.A05.A03 = i;
    }

    public void setSizeAndInvalidate(float f) {
        this.A05.setSizeAndInvalidate(f);
    }
}
