package com.whatsapp.mediacomposer;

import X.AbstractC14440lR;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass0B5;
import X.AnonymousClass1BI;
import X.AnonymousClass21U;
import X.AnonymousClass21V;
import X.AnonymousClass21Y;
import X.AnonymousClass21Z;
import X.AnonymousClass23D;
import X.AnonymousClass3X4;
import X.AnonymousClass3XD;
import X.AnonymousClass3YE;
import X.C003501n;
import X.C1107856w;
import X.C1109757p;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14330lG;
import X.C14820m6;
import X.C18720su;
import X.C453421e;
import X.C457522x;
import X.C68853Wz;
import X.C90404Nt;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.bottombar.filterswipe.FilterSwipeView;
import com.whatsapp.mediacomposer.doodle.ImagePreviewContentLayout;
import com.whatsapp.mediaview.PhotoView;
import java.io.File;

/* loaded from: classes2.dex */
public class ImageComposerFragment extends Hilt_ImageComposerFragment {
    public Bitmap A00;
    public C14330lG A01;
    public C18720su A02;
    public AnonymousClass1BI A03;
    public AnonymousClass23D A04;
    public AnonymousClass23D A05;
    public ImagePreviewContentLayout A06;
    public AnonymousClass21U A07;
    public PhotoView A08;
    public boolean A09;

    public static File A00(Uri uri, C14330lG r3) {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C003501n.A01(uri.toString()));
        return r3.A0M(C12960it.A0d("-crop", A0h));
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A0t(int r16, int r17, android.content.Intent r18) {
        /*
        // Method dump skipped, instructions count: 485
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.ImageComposerFragment.A0t(int, int, android.content.Intent):void");
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        bundle.putBoolean("handle-crop-image-result", this.A09);
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.image_composer_fragment);
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A12() {
        this.A06.A00();
        AnonymousClass21U r2 = this.A07;
        r2.A04 = null;
        r2.A03 = null;
        r2.A02 = null;
        View view = r2.A0L;
        if (view != null) {
            ((AnonymousClass0B5) view.getLayoutParams()).A00(null);
        }
        BottomSheetBehavior bottomSheetBehavior = r2.A08;
        if (bottomSheetBehavior != null) {
            bottomSheetBehavior.A0E = null;
        }
        r2.A03();
        C457522x r1 = ((MediaComposerActivity) ((AnonymousClass21Y) A0B())).A0V;
        if (r1 != null) {
            AnonymousClass23D r0 = this.A04;
            if (r0 != null) {
                r1.A01(r0);
            }
            AnonymousClass23D r02 = this.A05;
            if (r02 != null) {
                r1.A01(r02);
            }
        }
        super.A12();
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        this.A06 = (ImagePreviewContentLayout) view.findViewById(R.id.media_content);
        super.A17(bundle, view);
        int A00 = ActivityC13790kL.A0T(this).A00();
        C18720su r4 = this.A02;
        AbstractC14440lR r10 = ((MediaComposerFragment) this).A0M;
        AnonymousClass1BI r7 = this.A03;
        AnonymousClass018 r6 = ((MediaComposerFragment) this).A07;
        C14820m6 r5 = ((MediaComposerFragment) this).A06;
        this.A07 = new AnonymousClass21U(((MediaComposerFragment) this).A00, view, A0B(), r4, r5, r6, r7, new AnonymousClass21V(this), ((MediaComposerFragment) this).A0D, r10, A00);
        this.A08 = (PhotoView) view.findViewById(R.id.photo);
        ImagePreviewContentLayout imagePreviewContentLayout = this.A06;
        imagePreviewContentLayout.A03 = ((MediaComposerFragment) this).A0D;
        imagePreviewContentLayout.A04 = new C1109757p(this);
        C12960it.A0x(imagePreviewContentLayout, this, 41);
        if (bundle == null || !bundle.getBoolean("handle-crop-image-result", false)) {
            A1J(bundle);
        }
        if (this.A00 == null) {
            C68853Wz r2 = new C68853Wz(this);
            this.A05 = r2;
            C1107856w r1 = new C1107856w(this);
            C457522x r0 = ((MediaComposerActivity) ((AnonymousClass21Y) A0B())).A0V;
            if (r0 != null) {
                r0.A02(r2, r1);
            }
        }
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment
    public void A1E(Rect rect) {
        super.A1E(rect);
        if (((AnonymousClass01E) this).A0A != null) {
            AnonymousClass21U r4 = this.A07;
            if (!rect.equals(r4.A05)) {
                r4.A05 = new Rect(0, rect.top, 0, rect.bottom);
            }
        }
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment
    public boolean A1G() {
        return this.A07.A09() || super.A1G();
    }

    public final int A1I() {
        int i;
        String queryParameter = ((MediaComposerFragment) this).A00.getQueryParameter("rotation");
        if (queryParameter != null) {
            i = Integer.parseInt(queryParameter);
        } else {
            i = 0;
        }
        return (ActivityC13790kL.A0T(this).A01() + i) % 360;
    }

    public final void A1J(Bundle bundle) {
        this.A08.setTag(((MediaComposerFragment) this).A00);
        AnonymousClass21Y r4 = (AnonymousClass21Y) A0B();
        Uri uri = ((MediaComposerFragment) this).A00;
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) r4;
        C453421e r1 = mediaComposerActivity.A1B;
        File A03 = r1.A00(uri).A03();
        if (A03 == null) {
            A03 = r1.A00(((MediaComposerFragment) this).A00).A05();
        }
        Uri.Builder buildUpon = Uri.fromFile(A03).buildUpon();
        int A1I = A1I();
        if (A1I != 0) {
            buildUpon.appendQueryParameter("rotation", Integer.toString(A1I));
        }
        if (((MediaComposerFragment) this).A00.getQueryParameter("flip-h") != null) {
            buildUpon.appendQueryParameter("flip-h", ((MediaComposerFragment) this).A00.getQueryParameter("flip-h"));
        }
        AnonymousClass3X4 r2 = new AnonymousClass3X4(buildUpon.build(), this);
        this.A04 = r2;
        AnonymousClass3XD r12 = new AnonymousClass3XD(bundle, this, r4);
        C457522x r0 = mediaComposerActivity.A0V;
        if (r0 != null) {
            r0.A02(r2, r12);
        }
    }

    public final void A1K(boolean z, boolean z2) {
        AnonymousClass21U r0 = this.A07;
        if (z) {
            r0.A01();
        } else {
            r0.A06(z2);
        }
        ActivityC000900k A0B = A0B();
        if (A0B instanceof AnonymousClass21Z) {
            boolean z3 = !z;
            MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) ((AnonymousClass21Z) A0B);
            AnonymousClass3YE r2 = mediaComposerActivity.A0e;
            boolean A07 = mediaComposerActivity.A0b.A07();
            C90404Nt r02 = r2.A06;
            if (z3) {
                if (A07) {
                    FilterSwipeView filterSwipeView = r02.A01;
                    TextView textView = filterSwipeView.A00;
                    if (textView.getVisibility() == 0) {
                        C12990iw.A1B(textView, C12980iv.A0R());
                        filterSwipeView.setFilterSwipeTextVisibility(4);
                    }
                }
            } else if (A07) {
                FilterSwipeView filterSwipeView2 = r02.A01;
                TextView textView2 = filterSwipeView2.A00;
                if (textView2.getVisibility() == 4) {
                    filterSwipeView2.setFilterSwipeTextVisibility(0);
                    C12990iw.A1B(textView2, C12970iu.A0J());
                }
            }
        }
    }

    @Override // com.whatsapp.mediacomposer.MediaComposerFragment, X.AnonymousClass01E, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass21U r2 = this.A07;
        if (r2.A08 != null) {
            C12980iv.A1H(r2.A0N.getViewTreeObserver(), r2, 7);
        }
    }
}
