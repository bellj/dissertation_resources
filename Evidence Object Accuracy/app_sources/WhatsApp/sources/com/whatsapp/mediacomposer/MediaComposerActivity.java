package com.whatsapp.mediacomposer;

import X.AbstractC116105Ud;
import X.AbstractC14440lR;
import X.AbstractC14470lU;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC33451e6;
import X.AbstractC453021a;
import X.AbstractC453121b;
import X.AbstractC453221c;
import X.AbstractC454421p;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass017;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass13H;
import X.AnonymousClass17S;
import X.AnonymousClass18U;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1BI;
import X.AnonymousClass1BU;
import X.AnonymousClass1BX;
import X.AnonymousClass1KC;
import X.AnonymousClass1O4;
import X.AnonymousClass1VX;
import X.AnonymousClass21Y;
import X.AnonymousClass21Z;
import X.AnonymousClass2Ab;
import X.AnonymousClass2BB;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2JY;
import X.AnonymousClass31R;
import X.AnonymousClass391;
import X.AnonymousClass3JD;
import X.AnonymousClass3X7;
import X.AnonymousClass3YE;
import X.C006202y;
import X.C103204qN;
import X.C14300lD;
import X.C14330lG;
import X.C14410lO;
import X.C14450lS;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15380n4;
import X.C15410nB;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15610nY;
import X.C15650ng;
import X.C15680nj;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16590pI;
import X.C16630pM;
import X.C17070qD;
import X.C17170qN;
import X.C17220qS;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18720su;
import X.C18810t5;
import X.C20320vZ;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22190yg;
import X.C22260yn;
import X.C22410z2;
import X.C22610zM;
import X.C22670zS;
import X.C22700zV;
import X.C22710zW;
import X.C231510o;
import X.C239613r;
import X.C245115u;
import X.C249317l;
import X.C250918b;
import X.C251118d;
import X.C252718t;
import X.C252818u;
import X.C253318z;
import X.C253719d;
import X.C26311Cv;
import X.C26471Dp;
import X.C26771Et;
import X.C32731ce;
import X.C38991p4;
import X.C39341ph;
import X.C42641vY;
import X.C453321d;
import X.C453421e;
import X.C454721t;
import X.C457522x;
import X.C47342Ag;
import X.C51432Us;
import X.C60362wp;
import X.C63123Aj;
import X.C63763Cv;
import X.C64243Eu;
import X.C74253hf;
import X.C92974Yj;
import X.DialogC47362Ai;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.PhotoViewPager;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.bottombar.caption.CaptionView;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;

/* loaded from: classes2.dex */
public class MediaComposerActivity extends ActivityC13790kL implements AnonymousClass21Y, AbstractC33451e6, AnonymousClass21Z, AbstractC453021a, AbstractC453121b, AbstractC453221c {
    public int A00;
    public Bitmap A01;
    public View A02;
    public AnonymousClass017 A03;
    public AnonymousClass17S A04;
    public C22260yn A05;
    public AnonymousClass18U A06;
    public C239613r A07;
    public C16170oZ A08;
    public C250918b A09;
    public C251118d A0A;
    public C18720su A0B;
    public C15550nR A0C;
    public C26311Cv A0D;
    public C22700zV A0E;
    public C15610nY A0F;
    public C21270x9 A0G;
    public C253318z A0H;
    public C15410nB A0I;
    public C16590pI A0J;
    public C17170qN A0K;
    public C15890o4 A0L;
    public AnonymousClass018 A0M;
    public C22610zM A0N;
    public C15680nj A0O;
    public C15650ng A0P;
    public C18470sV A0Q;
    public AnonymousClass1BI A0R;
    public C231510o A0S;
    public AnonymousClass193 A0T;
    public C16120oU A0U;
    public C457522x A0V;
    public PhotoViewPager A0W;
    public C253719d A0X;
    public C14410lO A0Y;
    public C14300lD A0Z;
    public C26471Dp A0a;
    public C453321d A0b;
    public AnonymousClass2BB A0c;
    public C60362wp A0d;
    public AnonymousClass3YE A0e;
    public DialogC47362Ai A0f;
    public C63763Cv A0g;
    public AnonymousClass1BU A0h;
    public C47342Ag A0i;
    public AnonymousClass1BX A0j;
    public AnonymousClass13H A0k;
    public C245115u A0l;
    public C17220qS A0m;
    public C22410z2 A0n;
    public C22710zW A0o;
    public C17070qD A0p;
    public C16630pM A0q;
    public AnonymousClass391 A0r;
    public AnonymousClass2JY A0s;
    public AnonymousClass1AB A0t;
    public C22190yg A0u;
    public AnonymousClass01H A0v;
    public boolean A0w;
    public boolean A0x;
    public boolean A0y;
    public boolean A0z;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    public boolean A16;
    public final PointF A17;
    public final Rect A18;
    public final Handler A19;
    public final C006202y A1A;
    public final C453421e A1B;
    public final AbstractC116105Ud A1C;
    public final Runnable A1D;
    public final Collection A1E;
    public final HashMap A1F;
    public final HashSet A1G;
    public final Map A1H;

    public MediaComposerActivity() {
        this(0);
        this.A00 = 0;
        this.A1D = new RunnableBRunnable0Shape8S0100000_I0_8(this, 4);
        this.A1C = new AbstractC116105Ud() { // from class: X.591
            @Override // X.AbstractC116105Ud
            public final void AVK(String str, int i) {
                AnonymousClass2JY r2;
                MediaComposerActivity mediaComposerActivity = MediaComposerActivity.this;
                if (!mediaComposerActivity.AJN() && i == 2 && (r2 = mediaComposerActivity.A0s) != null) {
                    r2.A02(str, 3, false, false);
                }
            }
        };
        this.A1B = new C453421e();
        this.A1F = new HashMap();
        this.A1G = new HashSet();
        this.A1H = new HashMap();
        this.A1E = new ArrayList();
        this.A19 = new Handler(Looper.getMainLooper());
        this.A18 = new Rect();
        this.A17 = new PointF();
        this.A1A = new C74253hf(this, (int) ((AnonymousClass01V.A00 / 1024) / 6));
    }

    public MediaComposerActivity(int i) {
        this.A0z = false;
        A0R(new C103204qN(this));
    }

    public static final long A02(C39341ph r4) {
        C38991p4 r0;
        if (r4.A06() == null || r4.A06().byteValue() != 3) {
            return -1;
        }
        Point A02 = r4.A02();
        if (A02 != null) {
            return (long) (A02.y - A02.x);
        }
        synchronized (r4) {
            r0 = r4.A05;
        }
        if (r0 != null) {
            return r0.A04;
        }
        return -1;
    }

    public static /* synthetic */ void A03(Window window, MediaComposerActivity mediaComposerActivity) {
        if (mediaComposerActivity.A03.A01() == null) {
            mediaComposerActivity.A0x = true;
            window.setSharedElementEnterTransition(null);
            mediaComposerActivity.A02.setVisibility(8);
            mediaComposerActivity.A0d();
        }
    }

    public static void A09(C22190yg r3, Collection collection, Collection collection2) {
        Iterator it = collection2.iterator();
        while (it.hasNext()) {
            C39341ph r1 = (C39341ph) it.next();
            if (collection.contains(r1.A05())) {
                r3.A0E(r1.A05());
                r3.A0E(r1.A03());
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0z) {
            this.A0z = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0B = (C18720su) r1.A2c.get();
            this.A0X = (C253719d) r1.A8V.get();
            this.A0k = (AnonymousClass13H) r1.ABY.get();
            this.A07 = (C239613r) r1.AI9.get();
            this.A0J = (C16590pI) r1.AMg.get();
            this.A0U = (C16120oU) r1.ANE.get();
            this.A04 = (AnonymousClass17S) r1.A0F.get();
            this.A0l = (C245115u) r1.A7s.get();
            this.A0Q = (C18470sV) r1.AK8.get();
            this.A08 = (C16170oZ) r1.AM4.get();
            this.A0a = (C26471Dp) r1.A9J.get();
            this.A06 = (AnonymousClass18U) r1.AAU.get();
            this.A0Y = (C14410lO) r1.AB3.get();
            this.A0S = (C231510o) r1.AHO.get();
            this.A0G = (C21270x9) r1.A4A.get();
            this.A0R = (AnonymousClass1BI) r1.A83.get();
            this.A0m = (C17220qS) r1.ABt.get();
            this.A0C = (C15550nR) r1.A45.get();
            this.A0u = (C22190yg) r1.AB6.get();
            this.A0F = (C15610nY) r1.AMe.get();
            this.A0M = (AnonymousClass018) r1.ANb.get();
            this.A05 = (C22260yn) r1.A5I.get();
            this.A0Z = (C14300lD) r1.ABA.get();
            this.A0p = (C17070qD) r1.AFC.get();
            this.A0H = (C253318z) r1.A4B.get();
            this.A0P = (C15650ng) r1.A4m.get();
            this.A0I = (C15410nB) r1.ALJ.get();
            this.A0T = (AnonymousClass193) r1.A6S.get();
            this.A0E = (C22700zV) r1.AMN.get();
            this.A0L = (C15890o4) r1.AN1.get();
            this.A0O = (C15680nj) r1.A4e.get();
            this.A0h = (AnonymousClass1BU) r1.AIX.get();
            this.A0o = (C22710zW) r1.AF7.get();
            this.A0n = (C22410z2) r1.ANH.get();
            this.A0j = (AnonymousClass1BX) r1.AAu.get();
            this.A0t = (AnonymousClass1AB) r1.AKI.get();
            this.A0A = (C251118d) r1.A2D.get();
            this.A0q = (C16630pM) r1.AIc.get();
            this.A0D = (C26311Cv) r1.AAQ.get();
            this.A0N = (C22610zM) r1.A6b.get();
            this.A0K = (C17170qN) r1.AMt.get();
            this.A09 = (C250918b) r1.A2B.get();
            this.A0v = C18000rk.A00(r1.A4v);
        }
    }

    public final int A2e() {
        return ((Number) this.A0b.A01.A01()).intValue();
    }

    public final AnonymousClass31R A2f(byte b) {
        boolean contains = ((List) this.A0b.A00.A01()).contains(AnonymousClass1VX.A00);
        int size = ((List) this.A0b.A00.A01()).size();
        int intExtra = getIntent().getIntExtra("origin", 1);
        long longExtra = getIntent().getLongExtra("picker_open_time", 0);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean z = this.A11;
        boolean z2 = this.A10;
        C453321d r0 = this.A0b;
        return C63123Aj.A00(b, size, intExtra, longExtra, elapsedRealtime, getIntent().getLongExtra("gallery_duration_ms", -1), contains, false, z, z2, !r0.A09.equals(r0.A00.A01()));
    }

    public final MediaComposerFragment A2g() {
        Uri A02 = this.A0b.A02();
        if (A02 != null) {
            for (AnonymousClass01E r1 : A23()) {
                if (r1 instanceof MediaComposerFragment) {
                    MediaComposerFragment mediaComposerFragment = (MediaComposerFragment) r1;
                    if (A02.equals(mediaComposerFragment.A00)) {
                        return mediaComposerFragment;
                    }
                }
            }
        }
        return null;
    }

    public final AbstractC15340mz A2h() {
        long longExtra = getIntent().getLongExtra("quoted_message_row_id", 0);
        C15580nU A04 = C15580nU.A04(getIntent().getStringExtra("quoted_group_jid"));
        if (longExtra > 0) {
            return this.A0P.A0K.A00(longExtra);
        }
        if (A04 != null) {
            return C20320vZ.A00(A04, null, null, ((ActivityC13790kL) this).A05.A00());
        }
        return null;
    }

    public final List A2i() {
        return (List) this.A0b.A02.A01();
    }

    public final void A2j() {
        Map map = this.A1H;
        for (AbstractC14470lU r2 : map.values()) {
            C14300lD r5 = this.A0Z;
            AnonymousClass1KC r4 = (AnonymousClass1KC) r2;
            if (r4.A0U == null) {
                r4.A02 = true;
                C14450lS r1 = r4.A0K;
                AnonymousClass009.A05(r1);
                synchronized (r1) {
                    if (!r1.A0D) {
                        r1.A08 = 0;
                    } else {
                        r1.A08 = 1;
                    }
                }
                C26771Et r22 = r5.A0B;
                r22.A04.A00(r4.A01().A05).A05(r4);
                r22.A02.A05(r4);
                r22.A03.A05(r4);
                r5.A0E.A05(r4);
                r5.A0J.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(r5, 39, r4));
            } else {
                StringBuilder sb = new StringBuilder("app/mediajobmanager/attempting to cancel non-optimistic job, skipped, job=");
                sb.append(r2);
                Log.w(sb.toString());
            }
        }
        map.clear();
    }

    public final void A2k() {
        String str;
        String str2;
        for (AnonymousClass01E r5 : A23()) {
            if (r5 instanceof MediaComposerFragment) {
                MediaComposerFragment mediaComposerFragment = (MediaComposerFragment) r5;
                Uri uri = mediaComposerFragment.A00;
                C453421e r7 = this.A1B;
                C39341ph A00 = r7.A00(uri);
                if (!mediaComposerFragment.A0D.A0O.A04.isEmpty()) {
                    AnonymousClass2Ab r1 = mediaComposerFragment.A0D;
                    C64243Eu r0 = r1.A0I;
                    str = new AnonymousClass3JD(r0.A06, r0.A07, r1.A0O.A05, r0.A02).A04();
                } else {
                    str = null;
                }
                synchronized (A00) {
                    A00.A0A = str;
                }
                C39341ph A002 = r7.A00(uri);
                if (!mediaComposerFragment.A0D.A0O.A04.isEmpty()) {
                    C454721t r02 = mediaComposerFragment.A0D.A0O;
                    try {
                        str2 = r02.A03.A01(r02.A04);
                    } catch (JSONException e) {
                        Log.e("ShapeRepository/saveEditState", e);
                        str2 = null;
                    }
                } else {
                    str2 = null;
                }
                synchronized (A002) {
                    A002.A0B = str2;
                }
            }
        }
    }

    public final void A2l() {
        for (AnonymousClass01E r2 : A23()) {
            if (r2 instanceof ImageComposerFragment) {
                ImageComposerFragment imageComposerFragment = (ImageComposerFragment) r2;
                this.A1B.A00(((MediaComposerFragment) imageComposerFragment).A00).A0A(imageComposerFragment.A07.A01);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b3, code lost:
        if (r1 != false) goto L_0x00b5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x00bb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0021 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2m() {
        /*
        // Method dump skipped, instructions count: 523
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.A2m():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00f1, code lost:
        if ((!r0.A09.equals(r0.A00.A01())) == false) goto L_0x00f3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2n() {
        /*
        // Method dump skipped, instructions count: 416
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.A2n():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x004c, code lost:
        if (((java.util.List) r1.A00.A01()).isEmpty() == false) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2o() {
        /*
            r34 = this;
            r12 = r34
            X.0m9 r0 = r12.A0C
            r17 = r0
            X.18t r0 = r12.A0D
            r16 = r0
            X.0nm r15 = r12.A03
            X.19M r14 = r12.A0B
            X.0sV r13 = r12.A0Q
            X.10o r11 = r12.A0S
            X.01d r10 = r12.A08
            X.0nY r9 = r12.A0F
            X.018 r8 = r12.A0M
            X.193 r7 = r12.A0T
            X.0m6 r6 = r12.A09
            X.21d r5 = r12.A0b
            X.0pM r4 = r12.A0q
            X.01H r3 = r12.A0v
            X.016 r0 = r5.A00
            java.lang.Object r2 = r0.A01()
            java.util.List r2 = (java.util.List) r2
            X.3YE r0 = r12.A0e
            X.3DD r0 = r0.A05
            com.whatsapp.mediacomposer.bottombar.caption.CaptionView r0 = r0.A04
            java.lang.CharSequence r29 = r0.getCaptionText()
            if (r29 != 0) goto L_0x0038
            java.lang.String r29 = ""
        L_0x0038:
            X.21d r1 = r12.A0b
            boolean r0 = r1.A0B
            if (r0 == 0) goto L_0x004e
            X.016 r0 = r1.A00
            java.lang.Object r0 = r0.A01()
            java.util.List r0 = (java.util.List) r0
            boolean r0 = r0.isEmpty()
            r31 = 1
            if (r0 != 0) goto L_0x0050
        L_0x004e:
            r31 = 0
        L_0x0050:
            X.0mz r0 = r12.A2h()
            r32 = 0
            if (r0 != 0) goto L_0x005a
            r32 = 1
        L_0x005a:
            boolean r1 = r12.A11
            X.2Ai r0 = new X.2Ai
            r28 = r3
            r30 = r2
            r33 = r1
            r25 = r5
            r26 = r4
            r27 = r16
            r22 = r11
            r23 = r7
            r24 = r17
            r19 = r8
            r20 = r13
            r21 = r14
            r16 = r9
            r17 = r10
            r18 = r6
            r13 = r0
            r14 = r15
            r15 = r12
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33)
            r12.A0f = r0
            r0.show()
            X.3YE r1 = r12.A0e
            X.21d r0 = r12.A0b
            boolean r0 = r0.A08()
            r1.A02(r0)
            X.3YE r0 = r12.A0e
            X.4Nt r0 = r0.A06
            com.whatsapp.mediacomposer.bottombar.filterswipe.FilterSwipeView r1 = r0.A01
            android.widget.TextView r0 = r1.A00
            r0.clearAnimation()
            r0 = 8
            r1.setFilterSwipeTextVisibility(r0)
            X.2Ai r1 = r12.A0f
            X.3L6 r0 = new X.3L6
            r0.<init>()
            r1.setOnDismissListener(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.A2o():void");
    }

    public final void A2p() {
        byte b;
        this.A0e.A05.A00(Integer.valueOf(((Number) this.A0b.A05.A01()).intValue()));
        MediaComposerFragment A2g = A2g();
        if (A2g != null) {
            C453321d r0 = this.A0b;
            Uri A02 = r0.A02();
            if (A02 != null) {
                b = r0.A08.A05(r0.A07.A00(A02));
            } else {
                b = 0;
            }
            byte byteValue = Byte.valueOf(b).byteValue();
            if (byteValue != 13 && byteValue != 29) {
                boolean z = false;
                if (((Number) this.A0b.A05.A01()).intValue() == 3) {
                    z = true;
                }
                A2g.A1F(z);
                A2g.A1D();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01b2, code lost:
        if (r0 == false) goto L_0x01b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0207, code lost:
        if (r0 != false) goto L_0x0209;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0209, code lost:
        r4.A1C();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2q(int r12) {
        /*
        // Method dump skipped, instructions count: 591
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.A2q(int):void");
    }

    public final void A2r(Uri uri) {
        C453321d r1 = this.A0b;
        r1.A0A.remove(uri);
        r1.A03();
        Object remove = this.A1B.A00.remove(uri);
        if (remove != null) {
            this.A1G.add(remove);
        }
        this.A0d.A06();
        this.A0e.A09.A02.A02();
        if (A2i().isEmpty()) {
            finish();
            return;
        }
        if (A2e() >= 0) {
            APM();
            this.A0e.A04.setAlpha(1.0f - 0.0f);
            this.A0W.setCurrentItem(A2e());
            A2q(A2e());
        }
        this.A0e.A04(this.A0b.A08());
    }

    public final void A2s(Uri uri, AnonymousClass1O4 r6) {
        if (uri != null) {
            String obj = uri.toString();
            C006202y r2 = r6.A00;
            r2.A07(obj);
            StringBuilder sb = new StringBuilder();
            sb.append(uri.toString());
            sb.append("-thumb");
            r2.A07(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(uri.toString());
            sb2.append("-filter");
            r2.A07(sb2.toString());
            C006202y r22 = this.A1A;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(uri.toString());
            sb3.append("-thumb");
            r22.A07(sb3.toString());
        }
    }

    public final void A2t(C32731ce r7, List list) {
        this.A0g.A00(this.A0F, r7, list, C15380n4.A0P(list), true);
        AnonymousClass3YE r3 = this.A0e;
        boolean z = !((List) this.A0b.A00.A01()).isEmpty();
        CaptionView captionView = r3.A05.A04;
        captionView.getContext();
        AnonymousClass018 r0 = captionView.A00;
        if (z) {
            C92974Yj.A00(captionView, r0);
        } else {
            C92974Yj.A01(captionView, r0);
        }
        r3.A08.A01(z);
    }

    public final void A2u(boolean z) {
        long j;
        StringBuilder sb = new StringBuilder("MediaComposerActivity/openContactPicker uris size = ");
        sb.append(A2i().size());
        Log.i(sb.toString());
        HashSet hashSet = new HashSet();
        boolean z2 = false;
        if (((Number) this.A0b.A05.A01()).intValue() == 3) {
            z2 = true;
        }
        C453421e r2 = this.A1B;
        if (z2) {
            j = A02(r2.A00((Uri) A2i().get(0)));
            int i = 42;
            if (this.A0b.A00() == 3) {
                i = 43;
            }
            hashSet.add(Integer.valueOf(i));
        } else {
            Iterator it = new ArrayList(r2.A00.values()).iterator();
            j = -1;
            while (it.hasNext()) {
                C39341ph r1 = (C39341ph) it.next();
                if (r1.A06() != null) {
                    hashSet.add(Integer.valueOf(r1.A06().byteValue()));
                    j = Math.max(j, A02(r1));
                }
            }
        }
        C42641vY r12 = new C42641vY(this);
        r12.A0D = true;
        r12.A0S = (List) this.A0b.A00.A01();
        r12.A0F = true;
        r12.A0L = Long.valueOf(j);
        r12.A0R = new ArrayList(hashSet);
        r12.A0G = Boolean.valueOf(z);
        r12.A01 = (C32731ce) this.A0b.A03.A01();
        startActivityForResult(r12.A00(), 1);
    }

    @Override // X.AnonymousClass21Y
    public Uri AAj() {
        if (AbstractC454421p.A00) {
            return (Uri) getIntent().getParcelableExtra("animate_uri");
        }
        return null;
    }

    @Override // X.AnonymousClass21Y
    public String ACe(Uri uri) {
        String str;
        C39341ph A00 = this.A1B.A00(uri);
        synchronized (A00) {
            str = A00.A0B;
        }
        return str;
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AnonymousClass21Y
    public void APM() {
        A2s(this.A0b.A02(), this.A0B.A02());
        A2k();
        AnonymousClass3YE r2 = this.A0e;
        boolean A08 = this.A0b.A08();
        r2.A09.A02.A02();
        r2.A01(A08);
    }

    @Override // X.AbstractC33451e6
    public void AWY() {
        A2n();
    }

    @Override // X.AnonymousClass21Y
    public void Ad5(Uri uri, long j, long j2) {
        C39341ph A00 = this.A1B.A00(uri);
        Point point = new Point((int) j, (int) j2);
        synchronized (A00) {
            A00.A03 = point;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00aa, code lost:
        if (r9 != 100) goto L_0x00ac;
     */
    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchTouchEvent(android.view.MotionEvent r17) {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1 && intent != null) {
            if (i2 == -1) {
                this.A0b.A00.A0B(Collections.unmodifiableList(C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"))));
                Parcelable parcelableExtra = intent.getParcelableExtra("status_distribution");
                if (parcelableExtra != null) {
                    this.A0b.A03.A0B(parcelableExtra);
                }
                A2n();
            } else if (i2 == 0 && this.A11) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                Parcelable parcelableExtra2 = intent.getParcelableExtra("status_distribution");
                this.A0b.A00.A0B(Collections.unmodifiableList(A07));
                this.A0b.A03.A0B(parcelableExtra2);
                C453321d r1 = this.A0b;
                r1.A06(r1.A01());
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2m();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0522, code lost:
        if (r0 != null) goto L_0x0524;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r38) {
        /*
        // Method dump skipped, instructions count: 1502
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.MediaComposerActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        int intExtra;
        if (this.A0W != null) {
            for (int i = 0; i < this.A0W.getChildCount(); i++) {
                View childAt = this.A0W.getChildAt(i);
                if (childAt instanceof FrameLayout) {
                    int i2 = 0;
                    while (true) {
                        ViewGroup viewGroup = (ViewGroup) childAt;
                        if (i2 < viewGroup.getChildCount()) {
                            View childAt2 = viewGroup.getChildAt(i2);
                            if (childAt2 instanceof PhotoView) {
                                ((PhotoView) childAt2).A01();
                            }
                            i2++;
                        }
                    }
                }
            }
        }
        if (this.A00 == -1 || (intExtra = getIntent().getIntExtra("origin", 1)) == 5 || intExtra == 8 || intExtra == 9 || intExtra == 23 || intExtra == 22 || intExtra == 24 || intExtra == 25 || intExtra == 29) {
            this.A0B.A02().A00.A06(-1);
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0100000_I0(this.A0I, 37));
        super.onDestroy();
        AnonymousClass391 r1 = this.A0r;
        if (r1 != null) {
            r1.A03(true);
            this.A0r = null;
        }
        AnonymousClass3YE r0 = this.A0e;
        if (r0 != null) {
            C51432Us r4 = r0.A09.A02;
            Set<AnonymousClass3X7> set = r4.A0D;
            for (AnonymousClass3X7 r12 : set) {
                r4.A06.A01(r12);
                r12.A09.set(true);
            }
            set.clear();
            this.A0e = null;
        }
        C457522x r02 = this.A0V;
        if (r02 != null) {
            r02.A00();
            this.A0V = null;
        }
        AnonymousClass1BU r13 = this.A0h;
        synchronized (r13) {
            r13.A04.clear();
        }
        ((ActivityC13810kN) this).A05.A0G(this.A1D);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2m();
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelableArrayList("uris", new ArrayList<>((Collection) this.A0b.A02.A01()));
        ArrayList arrayList = new ArrayList();
        for (Object obj : A2i()) {
            arrayList.add(this.A1F.get(obj));
        }
        bundle.putSerializable("ids", arrayList);
        bundle.putInt("position", A2e());
        bundle.putInt("view_once", ((Number) this.A0b.A05.A01()).intValue());
        C453421e r0 = this.A1B;
        Bundle bundle2 = new Bundle();
        r0.A02(bundle2);
        bundle.putBundle("media_preview_params", bundle2);
        bundle.putBoolean("optimistic_started", this.A14);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0w = true;
        A2q(A2e());
        if (this.A03.A01() == null && !this.A16 && this.A0d != null && this.A0L.A07()) {
            ArrayList arrayList = new ArrayList();
            for (Uri uri : A2i()) {
                File A04 = this.A1B.A00(uri).A04();
                if (A04 == null || !A04.exists()) {
                    arrayList.add(uri);
                }
            }
            int size = arrayList.size();
            if (size != 0) {
                ((ActivityC13810kN) this).A05.A0E(this.A0M.A0I(new Object[]{Integer.valueOf(size)}, R.plurals.file_was_removed, (long) size), 1);
                if (size == A2i().size()) {
                    finish();
                    return;
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    A2r((Uri) it.next());
                }
            }
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0w = false;
        if (!this.A16) {
            A2j();
        }
    }
}
