package com.whatsapp.mediacomposer;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC51092Su;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass11P;
import X.AnonymousClass146;
import X.AnonymousClass180;
import X.AnonymousClass182;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1BI;
import X.AnonymousClass1BT;
import X.AnonymousClass1BU;
import X.AnonymousClass1CV;
import X.AnonymousClass1CY;
import X.C14330lG;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C18720su;
import X.C22190yg;
import X.C235512c;
import X.C239713s;
import X.C244415n;
import X.C252718t;
import X.C48572Gu;
import X.C48792Hu;
import X.C51052Sq;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaFragment;

/* loaded from: classes2.dex */
public abstract class Hilt_MediaComposerFragment extends WaFragment implements AnonymousClass004 {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;
    public final Object A03 = new Object();
    public volatile C51052Sq A04;

    @Override // X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A19();
        return this.A00;
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return LayoutInflater.from(new C51062Sr(super.A0q(bundle), this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C51052Sq.A00(r0) == r4) goto L_0x000f;
     */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r4) {
        /*
            r3 = this;
            super.A0u(r4)
            android.content.ContextWrapper r0 = r3.A00
            r1 = 0
            if (r0 == 0) goto L_0x000f
            android.content.Context r0 = X.C51052Sq.A00(r0)
            r2 = 0
            if (r0 != r4) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = "onAttach called multiple times with different Context! Hilt Fragments should not be retained."
            X.AnonymousClass2PX.A00(r0, r1, r2)
            r3.A19()
            r3.A18()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mediacomposer.Hilt_MediaComposerFragment.A0u(android.app.Activity):void");
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A19();
        A18();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v10, types: [com.whatsapp.mediacomposer.MediaComposerFragment] */
    public void A18() {
        AbstractC51092Su r2;
        Hilt_GifComposerFragment hilt_GifComposerFragment;
        if (this instanceof Hilt_VideoComposerFragment) {
            Hilt_VideoComposerFragment hilt_VideoComposerFragment = (Hilt_VideoComposerFragment) this;
            if (!hilt_VideoComposerFragment.A02) {
                hilt_VideoComposerFragment.A02 = true;
                VideoComposerFragment videoComposerFragment = (VideoComposerFragment) hilt_VideoComposerFragment;
                C51112Sw r3 = (C51112Sw) ((AbstractC51092Su) hilt_VideoComposerFragment.generatedComponent());
                AnonymousClass01J r22 = r3.A0Y;
                ((WaFragment) videoComposerFragment).A00 = (AnonymousClass182) r22.A94.get();
                ((WaFragment) videoComposerFragment).A01 = (AnonymousClass180) r22.ALt.get();
                ((MediaComposerFragment) videoComposerFragment).A09 = (C14850m9) r22.A04.get();
                ((MediaComposerFragment) videoComposerFragment).A03 = (C14900mE) r22.A8X.get();
                ((MediaComposerFragment) videoComposerFragment).A0K = (C252718t) r22.A9K.get();
                ((MediaComposerFragment) videoComposerFragment).A02 = (AbstractC15710nm) r22.A4o.get();
                ((MediaComposerFragment) videoComposerFragment).A0M = (AbstractC14440lR) r22.ANe.get();
                ((MediaComposerFragment) videoComposerFragment).A08 = (AnonymousClass19M) r22.A6R.get();
                ((MediaComposerFragment) videoComposerFragment).A04 = (C15450nH) r22.AII.get();
                ((MediaComposerFragment) videoComposerFragment).A0E = (AnonymousClass1BT) r22.AHQ.get();
                ((MediaComposerFragment) videoComposerFragment).A0A = (C244415n) r22.AAg.get();
                ((MediaComposerFragment) videoComposerFragment).A05 = (AnonymousClass01d) r22.ALI.get();
                ((MediaComposerFragment) videoComposerFragment).A0L = (C22190yg) r22.AB6.get();
                ((MediaComposerFragment) videoComposerFragment).A07 = (AnonymousClass018) r22.ANb.get();
                ((MediaComposerFragment) videoComposerFragment).A0I = (AnonymousClass146) r22.AKM.get();
                ((MediaComposerFragment) videoComposerFragment).A0J = (C235512c) r22.AKS.get();
                ((MediaComposerFragment) videoComposerFragment).A0G = (AnonymousClass1CV) r22.AIY.get();
                ((MediaComposerFragment) videoComposerFragment).A06 = (C14820m6) r22.AN3.get();
                ((MediaComposerFragment) videoComposerFragment).A0F = (AnonymousClass1BU) r22.AIX.get();
                ((MediaComposerFragment) videoComposerFragment).A0H = (AnonymousClass1AB) r22.AKI.get();
                ((MediaComposerFragment) videoComposerFragment).A0C = (C48792Hu) r3.A0W.A01.get();
                videoComposerFragment.A0K = (AnonymousClass1CY) r22.ABO.get();
                videoComposerFragment.A0H = (C239713s) r22.ALn.get();
                videoComposerFragment.A0G = (AnonymousClass11P) r22.ABp.get();
            }
        } else if (!(this instanceof Hilt_ImageComposerFragment)) {
            if (this instanceof Hilt_GifComposerFragment) {
                Hilt_GifComposerFragment hilt_GifComposerFragment2 = (Hilt_GifComposerFragment) this;
                if (!hilt_GifComposerFragment2.A02) {
                    hilt_GifComposerFragment2.A02 = true;
                    r2 = (AbstractC51092Su) hilt_GifComposerFragment2.generatedComponent();
                    hilt_GifComposerFragment = hilt_GifComposerFragment2;
                } else {
                    return;
                }
            } else if (!this.A02) {
                this.A02 = true;
                r2 = (AbstractC51092Su) generatedComponent();
                hilt_GifComposerFragment = (MediaComposerFragment) this;
            } else {
                return;
            }
            C51112Sw r23 = (C51112Sw) r2;
            AnonymousClass01J r32 = r23.A0Y;
            ((WaFragment) hilt_GifComposerFragment).A00 = (AnonymousClass182) r32.A94.get();
            ((WaFragment) hilt_GifComposerFragment).A01 = (AnonymousClass180) r32.ALt.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A09 = (C14850m9) r32.A04.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A03 = (C14900mE) r32.A8X.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0K = (C252718t) r32.A9K.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A02 = (AbstractC15710nm) r32.A4o.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0M = (AbstractC14440lR) r32.ANe.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A08 = (AnonymousClass19M) r32.A6R.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A04 = (C15450nH) r32.AII.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0E = (AnonymousClass1BT) r32.AHQ.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0A = (C244415n) r32.AAg.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A05 = (AnonymousClass01d) r32.ALI.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0L = (C22190yg) r32.AB6.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A07 = (AnonymousClass018) r32.ANb.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0I = (AnonymousClass146) r32.AKM.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0J = (C235512c) r32.AKS.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0G = (AnonymousClass1CV) r32.AIY.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A06 = (C14820m6) r32.AN3.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0F = (AnonymousClass1BU) r32.AIX.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0H = (AnonymousClass1AB) r32.AKI.get();
            ((MediaComposerFragment) hilt_GifComposerFragment).A0C = (C48792Hu) r23.A0W.A01.get();
        } else {
            Hilt_ImageComposerFragment hilt_ImageComposerFragment = (Hilt_ImageComposerFragment) this;
            if (!hilt_ImageComposerFragment.A02) {
                hilt_ImageComposerFragment.A02 = true;
                ImageComposerFragment imageComposerFragment = (ImageComposerFragment) hilt_ImageComposerFragment;
                C51112Sw r33 = (C51112Sw) ((AbstractC51092Su) hilt_ImageComposerFragment.generatedComponent());
                AnonymousClass01J r24 = r33.A0Y;
                ((WaFragment) imageComposerFragment).A00 = (AnonymousClass182) r24.A94.get();
                ((WaFragment) imageComposerFragment).A01 = (AnonymousClass180) r24.ALt.get();
                ((MediaComposerFragment) imageComposerFragment).A09 = (C14850m9) r24.A04.get();
                ((MediaComposerFragment) imageComposerFragment).A03 = (C14900mE) r24.A8X.get();
                ((MediaComposerFragment) imageComposerFragment).A0K = (C252718t) r24.A9K.get();
                ((MediaComposerFragment) imageComposerFragment).A02 = (AbstractC15710nm) r24.A4o.get();
                ((MediaComposerFragment) imageComposerFragment).A0M = (AbstractC14440lR) r24.ANe.get();
                ((MediaComposerFragment) imageComposerFragment).A08 = (AnonymousClass19M) r24.A6R.get();
                ((MediaComposerFragment) imageComposerFragment).A04 = (C15450nH) r24.AII.get();
                ((MediaComposerFragment) imageComposerFragment).A0E = (AnonymousClass1BT) r24.AHQ.get();
                ((MediaComposerFragment) imageComposerFragment).A0A = (C244415n) r24.AAg.get();
                ((MediaComposerFragment) imageComposerFragment).A05 = (AnonymousClass01d) r24.ALI.get();
                ((MediaComposerFragment) imageComposerFragment).A0L = (C22190yg) r24.AB6.get();
                ((MediaComposerFragment) imageComposerFragment).A07 = (AnonymousClass018) r24.ANb.get();
                ((MediaComposerFragment) imageComposerFragment).A0I = (AnonymousClass146) r24.AKM.get();
                ((MediaComposerFragment) imageComposerFragment).A0J = (C235512c) r24.AKS.get();
                ((MediaComposerFragment) imageComposerFragment).A0G = (AnonymousClass1CV) r24.AIY.get();
                ((MediaComposerFragment) imageComposerFragment).A06 = (C14820m6) r24.AN3.get();
                ((MediaComposerFragment) imageComposerFragment).A0F = (AnonymousClass1BU) r24.AIX.get();
                ((MediaComposerFragment) imageComposerFragment).A0H = (AnonymousClass1AB) r24.AKI.get();
                ((MediaComposerFragment) imageComposerFragment).A0C = (C48792Hu) r33.A0W.A01.get();
                imageComposerFragment.A02 = (C18720su) r24.A2c.get();
                imageComposerFragment.A01 = (C14330lG) r24.A7B.get();
                imageComposerFragment.A03 = (AnonymousClass1BI) r24.A83.get();
            }
        }
    }

    public final void A19() {
        if (this.A00 == null) {
            this.A00 = new C51062Sr(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }

    @Override // X.AnonymousClass01E, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A01(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A04 == null) {
            synchronized (this.A03) {
                if (this.A04 == null) {
                    this.A04 = new C51052Sq(this);
                }
            }
        }
        return this.A04.generatedComponent();
    }
}
