package com.whatsapp.wds.components.button;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass1WQ;
import X.AnonymousClass1WR;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4AK;
import X.AnonymousClass4HS;
import X.C10700f3;
import X.C114035Jy;
import X.C12960it;
import X.C16700pc;
import X.C28141Kv;
import X.C51012Sk;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes2.dex */
public final class WDSButtonGroup extends ViewGroup implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass4AK A01;
    public AnonymousClass4AK A02;
    public AnonymousClass2P7 A03;
    public boolean A04;
    public final List A05;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public WDSButtonGroup(Context context) {
        this(context, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public WDSButtonGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        C16700pc.A0E(context, 1);
        AnonymousClass4AK r4 = AnonymousClass4AK.A01;
        this.A02 = r4;
        this.A01 = AnonymousClass4AK.A02;
        this.A05 = new LinkedList();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4HS.A01, 0, 0);
            C16700pc.A0B(obtainStyledAttributes);
            int i = obtainStyledAttributes.getInt(0, -1);
            AnonymousClass4AK[] values = AnonymousClass4AK.values();
            if (i >= 0) {
                C16700pc.A0E(values, 0);
                if (i <= values.length - 1) {
                    r4 = values[i];
                }
            }
            setOrientationMode(r4);
            obtainStyledAttributes.recycle();
        }
    }

    public WDSButtonGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public /* synthetic */ WDSButtonGroup(Context context, AttributeSet attributeSet, int i, C51012Sk r5) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public final AnonymousClass4AK getOrientationMode() {
        return this.A02;
    }

    public final AnonymousClass018 getWhatsAppLocale() {
        return this.A00;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        List list = this.A05;
        list.clear();
        AnonymousClass1WR r1 = new AnonymousClass1WR(new AnonymousClass1WQ(new C114035Jy(), new C10700f3(this), true));
        while (r1.hasNext()) {
            list.add(r1.next());
        }
        int size = list.size();
        if (size <= 2) {
            int i5 = i3 - i;
            int i6 = i4 - i2;
            if (size == 1) {
                ((View) list.remove(0)).layout(0, 0, i5, i6);
            } else if (size == 2) {
                View view = (View) list.remove(0);
                View view2 = (View) list.remove(0);
                if (this.A01 == AnonymousClass4AK.A03) {
                    int i7 = i6 >> 1;
                    view.layout(0, 0, i5, i7);
                    view2.layout(0, i7, i5, i6);
                    return;
                }
                int i8 = i5 >> 1;
                AnonymousClass018 r0 = this.A00;
                if (r0 == null || C28141Kv.A01(r0)) {
                    view.layout(0, 0, i8, i6);
                    view2.layout(i8, 0, i5, i6);
                    return;
                }
                view.layout(i8, 0, i5, i6);
                view2.layout(0, 0, i8, i6);
            }
        } else {
            throw C12960it.A0U("WDSButtonGroup should not have more than 2 visible children!");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0064, code lost:
        if ((r8 << 1) > r6) goto L_0x0066;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r12, int r13) {
        /*
            r11 = this;
            int r10 = android.view.View.MeasureSpec.getMode(r12)
            int r6 = android.view.View.MeasureSpec.getSize(r12)
            int r3 = android.view.View.MeasureSpec.getMode(r13)
            int r2 = android.view.View.MeasureSpec.getSize(r13)
            X.0f3 r4 = new X.0f3
            r4.<init>(r11)
            X.5Jz r1 = new X.5Jz
            r1.<init>()
            r0 = 1
            X.1WQ r7 = new X.1WQ
            r7.<init>(r1, r4, r0)
            int r1 = X.C11100fk.A00(r7)
            r0 = 2
            r4 = 0
            if (r1 > r0) goto L_0x00f5
            X.4AK r0 = r11.A02
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0036;
                case 1: goto L_0x0074;
                case 2: goto L_0x0066;
                default: goto L_0x0031;
            }
        L_0x0031:
            X.5Gx r0 = X.C12990iw.A0v()
            throw r0
        L_0x0036:
            X.1WR r9 = new X.1WR
            r9.<init>(r7)
            r8 = 0
            r5 = 0
        L_0x003d:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0060
            java.lang.Object r1 = r9.next()
            android.view.View r1 = (android.view.View) r1
            r11.measureChild(r1, r12, r13)
            int r0 = r1.getMeasuredWidth()
            int r8 = java.lang.Math.max(r8, r0)
            r1.getMeasuredHeight()
            int r0 = r1.getMeasuredState()
            int r5 = android.view.ViewGroup.combineMeasuredStates(r5, r0)
            goto L_0x003d
        L_0x0060:
            if (r10 == 0) goto L_0x0074
            int r0 = r8 << 1
            if (r0 <= r6) goto L_0x0074
        L_0x0066:
            X.4AK r0 = X.AnonymousClass4AK.A03
        L_0x0068:
            r11.A01 = r0
            int r0 = X.C11100fk.A00(r7)
            if (r0 != 0) goto L_0x0077
            super.onMeasure(r12, r13)
            return
        L_0x0074:
            X.4AK r0 = X.AnonymousClass4AK.A02
            goto L_0x0068
        L_0x0077:
            X.4AK r1 = r11.A01
            X.4AK r0 = X.AnonymousClass4AK.A02
            r8 = 1073741824(0x40000000, float:2.0)
            if (r1 != r0) goto L_0x00b0
            int r0 = X.C11100fk.A00(r7)
            int r10 = r6 / r0
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            r3 = r13
        L_0x008a:
            X.1WR r2 = new X.1WR
            r2.<init>(r7)
            r5 = 0
        L_0x0090:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00bf
            java.lang.Object r1 = r2.next()
            android.view.View r1 = (android.view.View) r1
            r11.measureChild(r1, r9, r3)
            int r0 = r1.getMeasuredHeight()
            int r4 = java.lang.Math.max(r4, r0)
            int r0 = r1.getMeasuredState()
            int r5 = android.view.ViewGroup.combineMeasuredStates(r5, r0)
            goto L_0x0090
        L_0x00b0:
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r8)
            int r0 = X.C11100fk.A00(r7)
            int r2 = r2 / r0
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r3)
            r10 = r6
            goto L_0x008a
        L_0x00bf:
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r8)
            X.1WR r1 = new X.1WR
            r1.<init>(r7)
        L_0x00cc:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00dc
            java.lang.Object r0 = r1.next()
            android.view.View r0 = (android.view.View) r0
            r0.measure(r3, r2)
            goto L_0x00cc
        L_0x00dc:
            X.4AK r1 = r11.A01
            X.4AK r0 = X.AnonymousClass4AK.A03
            if (r1 != r0) goto L_0x00e7
            int r0 = X.C11100fk.A00(r7)
            int r4 = r4 * r0
        L_0x00e7:
            int r1 = android.view.ViewGroup.resolveSizeAndState(r6, r12, r5)
            int r0 = r5 << 16
            int r0 = android.view.ViewGroup.resolveSizeAndState(r4, r13, r0)
            r11.setMeasuredDimension(r1, r0)
            return
        L_0x00f5:
            java.lang.String r0 = "WDSButtonGroup should not have more than 2 visible children!"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wds.components.button.WDSButtonGroup.onMeasure(int, int):void");
    }

    public final void setOrientationMode(AnonymousClass4AK r2) {
        C16700pc.A0E(r2, 0);
        boolean A1X = C12960it.A1X(this.A02, r2);
        this.A02 = r2;
        if (A1X) {
            requestLayout();
        }
    }

    public final void setWhatsAppLocale(AnonymousClass018 r1) {
        this.A00 = r1;
    }
}
