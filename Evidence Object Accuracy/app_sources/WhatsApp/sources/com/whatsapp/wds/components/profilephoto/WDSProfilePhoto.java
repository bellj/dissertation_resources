package com.whatsapp.wds.components.profilephoto;

import X.AbstractC16710pd;
import X.AbstractC53342dy;
import X.AbstractC93424a9;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01Y;
import X.AnonymousClass1WL;
import X.AnonymousClass4A5;
import X.AnonymousClass4BH;
import X.AnonymousClass4BK;
import X.AnonymousClass4HS;
import X.AnonymousClass4V8;
import X.AnonymousClass4WD;
import X.AnonymousClass4YC;
import X.AnonymousClass5JA;
import X.AnonymousClass5JB;
import X.AnonymousClass5JC;
import X.AnonymousClass5VD;
import X.C106174vH;
import X.C113285Gx;
import X.C113975Js;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16700pc;
import X.C28141Kv;
import X.C51012Sk;
import X.C90534Og;
import X.C92714Xd;
import X.C93014Yp;
import X.C94364bg;
import X.EnumC87284Ax;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/* loaded from: classes3.dex */
public final class WDSProfilePhoto extends AbstractC53342dy implements AnonymousClass5VD {
    public AnonymousClass018 A00;
    public AnonymousClass4A5 A01;
    public AnonymousClass4BK A02;
    public AnonymousClass4BH A03;
    public AbstractC93424a9 A04;
    public EnumC87284Ax A05;
    public boolean A06;
    public final AbstractC16710pd A07;
    public final AbstractC16710pd A08;
    public final AbstractC16710pd A09;
    public final AbstractC16710pd A0A;
    public final AbstractC16710pd A0B;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public WDSProfilePhoto(Context context) {
        this(context, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WDSProfilePhoto(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        AnonymousClass4BK r1;
        C16700pc.A0E(context, 1);
        this.A07 = new AnonymousClass1WL(new AnonymousClass5JA());
        this.A09 = new AnonymousClass1WL(new AnonymousClass5JC());
        this.A08 = new AnonymousClass1WL(new AnonymousClass5JB());
        AnonymousClass1WL r0 = new AnonymousClass1WL(new C113975Js(context, this));
        this.A0B = r0;
        this.A0A = r0;
        this.A01 = AnonymousClass4A5.A02;
        AnonymousClass4BH r3 = AnonymousClass4BH.A03;
        this.A03 = r3;
        AnonymousClass4BK r8 = AnonymousClass4BK.A01;
        this.A02 = r8;
        this.A05 = EnumC87284Ax.A01;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4HS.A02, 0, 0);
            C16700pc.A0B(obtainStyledAttributes);
            int i = obtainStyledAttributes.getInt(2, 2);
            AnonymousClass4BH[] values = AnonymousClass4BH.values();
            if (i >= 0) {
                C16700pc.A0E(values, 0);
                if (i <= values.length - 1) {
                    r3 = values[i];
                }
            }
            setProfilePhotoSize(r3);
            int i2 = obtainStyledAttributes.getInt(1, r8.attributeId);
            AnonymousClass4BK[] values2 = AnonymousClass4BK.values();
            int length = values2.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    r1 = r8;
                    break;
                }
                r1 = values2[i3];
                i3++;
                if (r1.attributeId == i2) {
                    break;
                }
            }
            setProfilePhotoShape(r1);
            setStatusIndicatorEnabled(obtainStyledAttributes.getBoolean(3, false));
            setProfileBadge((AbstractC93424a9) AnonymousClass01Y.A02((List) AbstractC93424a9.A02.getValue(), obtainStyledAttributes.getInt(0, -1)));
            obtainStyledAttributes.recycle();
        }
    }

    public /* synthetic */ WDSProfilePhoto(Context context, AttributeSet attributeSet, int i, C51012Sk r5) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    public static final Bitmap A00(Drawable drawable, int i, int i2) {
        Bitmap createScaledBitmap;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                if (i == bitmapDrawable.getBitmap().getWidth() && i2 == bitmapDrawable.getBitmap().getHeight()) {
                    createScaledBitmap = bitmapDrawable.getBitmap();
                } else {
                    createScaledBitmap = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), i, i2, true);
                }
                C16700pc.A0B(createScaledBitmap);
                return createScaledBitmap;
            }
            throw C12970iu.A0f("bitmap is null");
        }
        Rect bounds = drawable.getBounds();
        C16700pc.A0B(bounds);
        int i3 = bounds.left;
        int i4 = bounds.top;
        int i5 = bounds.right;
        int i6 = bounds.bottom;
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, i, i2);
        drawable.draw(new Canvas(createBitmap));
        drawable.setBounds(i3, i4, i5, i6);
        C16700pc.A0B(createBitmap);
        return createBitmap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.graphics.PointF A01(android.content.Context r3, X.AnonymousClass4BK r4, X.AnonymousClass4BH r5) {
        /*
            X.C16700pc.A0F(r5, r3)
            r0 = 2
            X.C16700pc.A0E(r4, r0)
            int r0 = r4.ordinal()
            switch(r0) {
                case 0: goto L_0x0014;
                case 1: goto L_0x0022;
                default: goto L_0x000e;
            }
        L_0x000e:
            X.5Gx r0 = new X.5Gx
            r0.<init>()
            throw r0
        L_0x0014:
            int r0 = r5.ordinal()
            switch(r0) {
                case 0: goto L_0x002f;
                case 1: goto L_0x0042;
                default: goto L_0x001b;
            }
        L_0x001b:
            r0 = 0
            android.graphics.PointF r1 = new android.graphics.PointF
            r1.<init>(r0, r0)
            return r1
        L_0x0022:
            int r0 = r5.ordinal()
            switch(r0) {
                case 0: goto L_0x002f;
                case 1: goto L_0x0042;
                case 2: goto L_0x004a;
                case 3: goto L_0x004a;
                case 4: goto L_0x005d;
                default: goto L_0x0029;
            }
        L_0x0029:
            X.5Gx r0 = new X.5Gx
            r0.<init>()
            throw r0
        L_0x002f:
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167171(0x7f0707c3, float:1.7948608E38)
            float r2 = r1.getDimension(r0)
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167168(0x7f0707c0, float:1.7948602E38)
            goto L_0x006f
        L_0x0042:
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167170(0x7f0707c2, float:1.7948606E38)
            goto L_0x0051
        L_0x004a:
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167169(0x7f0707c1, float:1.7948604E38)
        L_0x0051:
            float r2 = r1.getDimension(r0)
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167171(0x7f0707c3, float:1.7948608E38)
            goto L_0x006f
        L_0x005d:
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167167(0x7f0707bf, float:1.79486E38)
            float r2 = r1.getDimension(r0)
            android.content.res.Resources r1 = r3.getResources()
            r0 = 2131167170(0x7f0707c2, float:1.7948606E38)
        L_0x006f:
            float r0 = r1.getDimension(r0)
            android.graphics.PointF r1 = new android.graphics.PointF
            r1.<init>(r2, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wds.components.profilephoto.WDSProfilePhoto.A01(android.content.Context, X.4BK, X.4BH):android.graphics.PointF");
    }

    public final void A02(AnonymousClass4A5 r5, boolean z) {
        double d;
        this.A01 = r5;
        AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
        AnonymousClass4A5 r1 = this.A01;
        C16700pc.A0E(r1, 0);
        C106174vH r3 = profilePhotoRenderer.A0M;
        switch (r1.ordinal()) {
            case 0:
                d = 0.0d;
                break;
            case 1:
                d = 1.0d;
                break;
            default:
                throw new C113285Gx();
        }
        AnonymousClass4YC r0 = (AnonymousClass4YC) r3.A0C.getValue();
        if (z) {
            r0.A02(d);
        } else {
            r0.A01(d);
        }
    }

    private final RectF getDrawRectF() {
        return (RectF) this.A07.getValue();
    }

    private final C94364bg getMarginOffsets() {
        return (C94364bg) this.A08.getValue();
    }

    private final C94364bg getOriginalMargins() {
        return (C94364bg) this.A09.getValue();
    }

    public final AbstractC93424a9 getProfileBadge() {
        return this.A04;
    }

    private final AnonymousClass4WD getProfilePhotoRenderer() {
        return (AnonymousClass4WD) this.A0A.getValue();
    }

    public final AnonymousClass4A5 getProfilePhotoSelectionState() {
        return this.A01;
    }

    public final AnonymousClass4BK getProfilePhotoShape() {
        return this.A02;
    }

    public final AnonymousClass4BH getProfilePhotoSize() {
        return this.A03;
    }

    public final boolean getStatusIndicatorEnabled() {
        return this.A06;
    }

    public final EnumC87284Ax getStatusIndicatorState() {
        return this.A05;
    }

    public final AnonymousClass018 getWhatsAppLocale() {
        AnonymousClass018 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        throw C16700pc.A06("whatsAppLocale");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0174, code lost:
        if (r2 == false) goto L_0x0176;
     */
    @Override // android.widget.ImageView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r13) {
        /*
        // Method dump skipped, instructions count: 388
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wds.components.profilephoto.WDSProfilePhoto.onDraw(android.graphics.Canvas):void");
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        float f;
        float f2;
        float f3;
        AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
        AnonymousClass4BH r2 = profilePhotoRenderer.A04;
        Context context = profilePhotoRenderer.A0A;
        PointF A01 = A01(context, profilePhotoRenderer.A03, r2);
        float A00 = C93014Yp.A00(context, profilePhotoRenderer.A04).A00();
        A01.offset(A00, A00);
        AnonymousClass4BH r22 = profilePhotoRenderer.A04;
        C16700pc.A0E(context, 0);
        float dimension = context.getResources().getDimension(r22.dimension);
        C92714Xd r0 = new C92714Xd(dimension, dimension);
        float f4 = r0.A01;
        A01.offset(f4, r0.A00);
        float f5 = (profilePhotoRenderer.A05.A02.A01 - f4) / ((float) 2);
        A01.offset(f5, f5);
        C92714Xd r3 = profilePhotoRenderer.A05.A02;
        C92714Xd r02 = new C92714Xd(Math.max(r3.A01, A01.x), Math.max(r3.A00, A01.y));
        float f6 = r02.A00;
        int i3 = (int) f6;
        float f7 = r02.A01;
        int i4 = (int) f7;
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(i4, 1073741824), View.MeasureSpec.makeMeasureSpec(i3, 1073741824));
        setMeasuredDimension(i4, i3);
        getDrawRectF().set(0.0f, 0.0f, f7, f6);
        AnonymousClass4WD profilePhotoRenderer2 = getProfilePhotoRenderer();
        RectF drawRectF = getDrawRectF();
        C16700pc.A0E(drawRectF, 0);
        RectF rectF = profilePhotoRenderer2.A0H;
        rectF.set(drawRectF);
        RectF rectF2 = profilePhotoRenderer2.A0K;
        float f8 = rectF.top;
        rectF2.top = f8;
        rectF2.bottom = f8 + profilePhotoRenderer2.A05.A02.A00;
        if (C28141Kv.A00(profilePhotoRenderer2.A0L)) {
            f = rectF.right - profilePhotoRenderer2.A05.A02.A01;
        } else {
            f = rectF.left;
        }
        rectF2.left = f;
        rectF2.right = f + profilePhotoRenderer2.A05.A02.A01;
        RectF rectF3 = profilePhotoRenderer2.A0J;
        rectF3.set(rectF2);
        float f9 = profilePhotoRenderer2.A05.A01;
        rectF3.inset(f9, f9);
        Rect rect = profilePhotoRenderer2.A0G;
        rect.set(0, 0, (int) rectF3.width(), (int) rectF3.height());
        profilePhotoRenderer2.A02 = new C90534Og((int) rectF.width(), (int) rectF.height());
        C106174vH r7 = profilePhotoRenderer2.A0M;
        AnonymousClass4BH r03 = r7.A02;
        Context context2 = r7.A06;
        AnonymousClass4V8 A002 = C93014Yp.A00(context2, r03);
        C16700pc.A0E(context2, 0);
        float dimension2 = context2.getResources().getDimension(A002.A00);
        PointF A012 = A01(context2, r7.A01, r7.A02);
        RectF rectF4 = r7.A08;
        float f10 = rectF3.bottom - (dimension2 - A012.y);
        rectF4.top = f10;
        rectF4.bottom = f10 + dimension2;
        if (C28141Kv.A00(r7.A09)) {
            f2 = rectF3.left;
            f3 = A012.x;
        } else {
            f2 = rectF3.right;
            f3 = dimension2 - A012.x;
        }
        float f11 = f2 - f3;
        rectF4.left = f11;
        float f12 = f11 + dimension2;
        rectF4.right = f12;
        float A003 = A002.A00();
        rectF4.left = f11 - A003;
        rectF4.top -= A003;
        rectF4.right = f12 + A003;
        rectF4.bottom += A003;
        r7.A02();
        Drawable drawable = profilePhotoRenderer2.A01;
        if (drawable != null) {
            drawable.setBounds(rect);
            profilePhotoRenderer2.A00 = A00(drawable, rect.width(), rect.height());
        }
        profilePhotoRenderer2.A0C.reset();
        profilePhotoRenderer2.A0D.reset();
        profilePhotoRenderer2.A00();
        setBackgroundDrawable((Drawable) getProfilePhotoRenderer().A0N.getValue());
        RectF rectF5 = getProfilePhotoRenderer().A0J;
        C94364bg marginOffsets = getMarginOffsets();
        marginOffsets.A01 = (int) (getDrawRectF().left - rectF5.left);
        marginOffsets.A03 = (int) (getDrawRectF().top - rectF5.top);
        marginOffsets.A02 = (int) (rectF5.right - getDrawRectF().right);
        marginOffsets.A00 = (int) (rectF5.bottom - getDrawRectF().bottom);
        C94364bg originalMargins = getOriginalMargins();
        C16700pc.A0E(originalMargins, 1);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            marginLayoutParams.setMargins(originalMargins.A01, originalMargins.A03, originalMargins.A02, originalMargins.A00);
            setLayoutParams(marginLayoutParams);
            return;
        }
        throw C12980iv.A0n("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
    }

    @Override // android.view.View
    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams != null) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            C94364bg originalMargins = getOriginalMargins();
            int i = marginLayoutParams.leftMargin;
            originalMargins.A01 = i;
            originalMargins.A03 = marginLayoutParams.topMargin;
            originalMargins.A02 = marginLayoutParams.rightMargin;
            originalMargins.A00 = marginLayoutParams.bottomMargin;
            marginLayoutParams.leftMargin = i + getMarginOffsets().A01;
            marginLayoutParams.topMargin += getMarginOffsets().A03;
            marginLayoutParams.rightMargin += getMarginOffsets().A02;
            marginLayoutParams.bottomMargin += getMarginOffsets().A00;
        }
        super.setLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void setPressed(boolean z) {
        super.setPressed(z);
        AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
        profilePhotoRenderer.A07 = z;
        if (z) {
            ((Paint) profilePhotoRenderer.A0P.getValue()).setColor(AnonymousClass00T.A00(profilePhotoRenderer.A0A, profilePhotoRenderer.A09));
        }
    }

    public final void setProfileBadge(AbstractC93424a9 r3) {
        boolean z = !C16700pc.A0O(r3, this.A04);
        this.A04 = r3;
        if (z && this.A0B.AJW()) {
            C106174vH r1 = getProfilePhotoRenderer().A0M;
            boolean z2 = !C16700pc.A0O(r1.A05, r3);
            r1.A05 = r3;
            if (z2) {
                r1.A01();
            }
            requestLayout();
        }
    }

    public final void setProfilePhotoShape(AnonymousClass4BK r4) {
        C16700pc.A0E(r4, 0);
        boolean A1X = C12960it.A1X(r4, this.A02);
        this.A02 = r4;
        if (A1X && this.A0B.AJW()) {
            AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
            AnonymousClass4BK r1 = this.A02;
            C16700pc.A0E(r1, 0);
            profilePhotoRenderer.A03 = r1;
            profilePhotoRenderer.A0M.A01 = r1;
            requestLayout();
        }
    }

    public final void setProfilePhotoSize(AnonymousClass4BH r5) {
        C16700pc.A0E(r5, 0);
        boolean A1X = C12960it.A1X(r5, this.A03);
        this.A03 = r5;
        if (A1X && this.A0B.AJW()) {
            AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
            AnonymousClass4BH r2 = this.A03;
            C16700pc.A0E(r2, 0);
            profilePhotoRenderer.A04 = r2;
            profilePhotoRenderer.A05 = C93014Yp.A01(r2).A00(profilePhotoRenderer.A0A);
            profilePhotoRenderer.A00();
            C106174vH r1 = profilePhotoRenderer.A0M;
            boolean A1X2 = C12960it.A1X(r1.A02, r2);
            r1.A02 = r2;
            if (A1X2) {
                r1.A01();
            }
            requestLayout();
        }
    }

    public final void setStatusIndicatorEnabled(boolean z) {
        boolean A1V = C12980iv.A1V(z ? 1 : 0, this.A06 ? 1 : 0);
        this.A06 = z;
        if (A1V && this.A0B.AJW()) {
            getProfilePhotoRenderer().A08 = z;
            requestLayout();
        }
    }

    public final void setStatusIndicatorState(EnumC87284Ax r2) {
        C16700pc.A0E(r2, 0);
        this.A05 = r2;
        AnonymousClass4WD profilePhotoRenderer = getProfilePhotoRenderer();
        profilePhotoRenderer.A06 = r2;
        profilePhotoRenderer.A00();
        invalidate();
    }

    public final void setWhatsAppLocale(AnonymousClass018 r2) {
        C16700pc.A0E(r2, 0);
        this.A00 = r2;
    }
}
