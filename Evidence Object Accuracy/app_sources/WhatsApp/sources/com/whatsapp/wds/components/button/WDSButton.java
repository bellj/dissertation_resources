package com.whatsapp.wds.components.button;

import X.AbstractC16710pd;
import X.AbstractC53332dt;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass4AL;
import X.AnonymousClass4HS;
import X.AnonymousClass4Yq;
import X.AnonymousClass5J5;
import X.AnonymousClass5J6;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C16700pc;
import X.C28141Kv;
import X.C28391Mz;
import X.C51012Sk;
import X.C63513Bw;
import X.C92734Xf;
import X.EnumC629839k;
import X.EnumC629939l;
import X.EnumC869649r;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WDSButton extends AbstractC53332dt {
    public PorterDuffColorFilter A00;
    public Drawable A01;
    public Drawable A02;
    public AnonymousClass018 A03;
    public C14850m9 A04;
    public AnonymousClass4AL A05;
    public C63513Bw A06;
    public EnumC869649r A07;
    public EnumC629939l A08;
    public String A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public final RectF A0F;
    public final RectF A0G;
    public final AbstractC16710pd A0H;
    public final AbstractC16710pd A0I;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public WDSButton(Context context) {
        this(context, null, false, 6, null);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public WDSButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, false);
        C16700pc.A0E(context, 1);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WDSButton(Context context, AttributeSet attributeSet, boolean z) {
        super(context, attributeSet);
        boolean z2;
        boolean A00;
        C14850m9 r1;
        C16700pc.A0E(context, 1);
        this.A0D = z;
        this.A0G = C12980iv.A0K();
        this.A0F = C12980iv.A0K();
        this.A0A = true;
        this.A09 = "";
        this.A06 = new C63513Bw();
        this.A0I = AnonymousClass4Yq.A00(new AnonymousClass5J6());
        this.A0H = AnonymousClass4Yq.A00(new AnonymousClass5J5());
        AnonymousClass4AL r6 = AnonymousClass4AL.A03;
        this.A05 = r6;
        EnumC869649r r2 = EnumC869649r.A01;
        this.A07 = r2;
        EnumC629939l r3 = EnumC629939l.A02;
        this.A08 = r3;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4HS.A00, 0, 0);
            C16700pc.A0B(obtainStyledAttributes);
            if (obtainStyledAttributes.getBoolean(8, false)) {
                this.A0D = true;
            }
            obtainStyledAttributes.recycle();
        }
        if (this.A0D || ((r1 = this.A04) != null && r1.A07(1963))) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.A0E = z2;
        if (z2) {
            this.A0B = true;
            AnonymousClass018 r0 = this.A03;
            if (r0 == null) {
                A00 = false;
            } else {
                A00 = C28141Kv.A00(r0);
            }
            this.A0C = A00;
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, AnonymousClass4HS.A00, 0, 0);
                C16700pc.A0B(obtainStyledAttributes2);
                int resourceId = obtainStyledAttributes2.getResourceId(5, -1);
                if (resourceId != -1 && obtainStyledAttributes2.length() > resourceId) {
                    setContentDescription(obtainStyledAttributes2.getString(resourceId));
                }
                int resourceId2 = obtainStyledAttributes2.getResourceId(3, -1);
                if (resourceId2 != -1 && obtainStyledAttributes2.length() > resourceId2) {
                    setHint(obtainStyledAttributes2.getString(resourceId2));
                }
                int resourceId3 = obtainStyledAttributes2.getResourceId(4, -1);
                if (resourceId3 != -1 && obtainStyledAttributes2.length() > resourceId3) {
                    setImeActionLabel(obtainStyledAttributes2.getString(resourceId3), getImeActionId());
                }
                int resourceId4 = obtainStyledAttributes2.getResourceId(2, -1);
                if (resourceId4 != -1 && obtainStyledAttributes2.length() > resourceId4) {
                    C12970iu.A19(context, this, resourceId4);
                }
                int i = obtainStyledAttributes2.getInt(6, -1);
                AnonymousClass4AL[] values = AnonymousClass4AL.values();
                if (i >= 0) {
                    C16700pc.A0E(values, 0);
                    if (i <= values.length - 1) {
                        r6 = values[i];
                    }
                }
                setAction(r6);
                int i2 = obtainStyledAttributes2.getInt(10, -1);
                EnumC869649r[] values2 = EnumC869649r.values();
                if (i2 >= 0) {
                    C16700pc.A0E(values2, 0);
                    if (i2 <= values2.length - 1) {
                        r2 = values2[i2];
                    }
                }
                setSize(r2);
                int i3 = obtainStyledAttributes2.getInt(9, -1);
                EnumC629939l[] values3 = EnumC629939l.values();
                if (i3 >= 0) {
                    C16700pc.A0E(values3, 0);
                    if (i3 <= values3.length - 1) {
                        r3 = values3[i3];
                    }
                }
                setVariant(r3);
                setupIcon(obtainStyledAttributes2.getDrawable(7));
                obtainStyledAttributes2.recycle();
            }
            A02();
            A03();
        }
    }

    public /* synthetic */ WDSButton(Context context, AttributeSet attributeSet, boolean z, int i, C51012Sk r6) {
        this(context, (i & 2) != 0 ? null : attributeSet, (i & 4) != 0 ? false : z);
    }

    public static final ColorStateList A00(Context context, C92734Xf r8) {
        C16700pc.A0E(r8, 0);
        return new ColorStateList(new int[][]{new int[]{16842919}, new int[]{-16842910}, new int[0]}, new int[]{AnonymousClass00T.A00(context, r8.A02), AnonymousClass00T.A00(context, r8.A00), AnonymousClass00T.A00(context, r8.A01)});
    }

    public final Drawable A01(int i, boolean z) {
        int i2;
        int i3;
        int i4 = 0;
        if (z) {
            i2 = 0;
            i3 = 0;
        } else {
            C63513Bw r0 = this.A06;
            i2 = r0.A03;
            i3 = r0.A04;
        }
        float[] fArr = new float[8];
        do {
            fArr[i4] = this.A06.A00;
            i4++;
        } while (i4 < 8);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(fArr, null, null));
        shapeDrawable.getPaint().setColor(i);
        return new InsetDrawable((Drawable) shapeDrawable, i2, i3, i2, i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:122:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x021a  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x018e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
        // Method dump skipped, instructions count: 806
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.wds.components.button.WDSButton.A02():void");
    }

    public final void A03() {
        EnumC629839k r0;
        if (this.A0B && this.A0E) {
            if (C28391Mz.A02()) {
                setStateListAnimator(null);
            }
            EnumC629939l r1 = this.A08;
            C16700pc.A0E(r1, 0);
            switch (r1.ordinal()) {
                case 0:
                    r0 = EnumC629839k.A01;
                    break;
                case 1:
                    r0 = EnumC629839k.A03;
                    break;
                case 2:
                    r0 = EnumC629839k.A02;
                    break;
                case 3:
                    r0 = EnumC629839k.A00;
                    break;
                default:
                    throw C12990iw.A0v();
            }
            setupContentStyle(r0);
            setupBackgroundStyle(r0);
            setupStrokeStyle(r0);
        }
    }

    public final boolean A04() {
        if (this.A05 != AnonymousClass4AL.A02) {
            return false;
        }
        EnumC629939l r1 = this.A08;
        return r1 == EnumC629939l.A03 || r1 == EnumC629939l.A01;
    }

    public final C14850m9 getAbProps() {
        return this.A04;
    }

    public final AnonymousClass4AL getAction() {
        return this.A05;
    }

    private final Paint getButtonStrokePaint() {
        return (Paint) this.A0H.getValue();
    }

    private final String getEllipsizedText() {
        if (!this.A0A) {
            return this.A09;
        }
        this.A0A = false;
        return TextUtils.ellipsize(getText(), getPaint(), Math.min((float) ((getWidth() - getFixedSpace()) - this.A06.A02), getPaint().measureText(getText().toString())), TextUtils.TruncateAt.END).toString();
    }

    private final int getFixedSpace() {
        C63513Bw r2 = this.A06;
        return (r2.A03 << 1) + r2.A07 + r2.A06 + r2.A08;
    }

    public final boolean getOverrideButtonMigration() {
        return this.A0D;
    }

    private final Paint getShaderPaint() {
        return (Paint) this.A0I.getValue();
    }

    public final EnumC869649r getSize() {
        return this.A07;
    }

    public final boolean getUseWDSButtonStyling() {
        return this.A0E;
    }

    public final EnumC629939l getVariant() {
        return this.A08;
    }

    public final AnonymousClass018 getWhatsAppLocale() {
        return this.A03;
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        int width;
        float f;
        Drawable drawable;
        C16700pc.A0E(canvas, 0);
        if (!this.A0E) {
            super.onDraw(canvas);
            return;
        }
        this.A09 = getEllipsizedText();
        float measureText = getPaint().measureText(this.A09);
        if (this.A01 == null) {
            width = 0;
        } else {
            C63513Bw r3 = this.A06;
            width = r3.A03 + r3.A07 + ((((getWidth() - getFixedSpace()) - r3.A02) - ((int) measureText)) >> 1);
            if (this.A0C) {
                width = (getWidth() - width) - r3.A02;
            }
        }
        int height = getHeight();
        C63513Bw r5 = this.A06;
        int i = r5.A02;
        int i2 = (height - i) >> 1;
        if (this.A01 == null) {
            f = (C12990iw.A02(this) - measureText) / 2.0f;
        } else if (this.A0C) {
            f = ((float) (width - r5.A06)) - measureText;
        } else {
            f = ((float) (width + i)) + ((float) r5.A06);
        }
        canvas.drawText(this.A09, f, ((C12990iw.A03(this) - getPaint().descent()) - getPaint().ascent()) / 2.0f, getPaint());
        Drawable drawable2 = this.A01;
        if (drawable2 != null) {
            if (A04() && (drawable = this.A02) != null) {
                int i3 = r5.A02;
                drawable.setBounds(width, i2, i3 + width, i3 + i2);
                drawable.draw(canvas);
            }
            PorterDuffColorFilter porterDuffColorFilter = this.A00;
            if (porterDuffColorFilter == null) {
                throw C16700pc.A06("colorFilter");
            }
            drawable2.setColorFilter(porterDuffColorFilter);
            int i4 = r5.A02;
            drawable2.setBounds(width, i2, i4 + width, i4 + i2);
            drawable2.draw(canvas);
        }
        if (this.A08 == EnumC629939l.A03) {
            RectF rectF = this.A0F;
            rectF.set(getBackground().getBounds());
            float f2 = ((float) r5.A09) / 2.0f;
            float f3 = (float) r5.A04;
            float A03 = (C12990iw.A03(this) / 2.0f) - f3;
            RectF rectF2 = this.A0G;
            float f4 = (float) r5.A03;
            rectF2.set(rectF.left + f2 + f4, rectF.top + f2 + f3, (rectF.right - f2) - f4, (rectF.bottom - f2) - f3);
            canvas.drawRoundRect(rectF2, A03, A03, getButtonStrokePaint());
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        C63513Bw r3;
        Number valueOf;
        if (this.A0E) {
            CharSequence text = getText();
            if (text == null || text.length() == 0) {
                r3 = this.A06;
                valueOf = Integer.valueOf(r3.A05);
            } else {
                int fixedSpace = getFixedSpace();
                r3 = this.A06;
                valueOf = Float.valueOf(((float) (fixedSpace + r3.A02)) + getPaint().measureText(getText().toString()));
            }
            i = View.MeasureSpec.makeMeasureSpec(View.resolveSize(valueOf.intValue(), i), 1073741824);
            i2 = View.MeasureSpec.makeMeasureSpec(r3.A01, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    public final void setAbProps(C14850m9 r1) {
        this.A04 = r1;
    }

    public final void setAction(AnonymousClass4AL r2) {
        C16700pc.A0E(r2, 0);
        boolean A1X = C12960it.A1X(this.A05, r2);
        this.A05 = r2;
        if (A1X) {
            A03();
        }
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        if (!this.A0E || drawable != null) {
            super.setBackground(drawable);
        } else {
            setVariant(EnumC629939l.A01);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void setEnabled(boolean z) {
        if (!this.A0E) {
            super.setEnabled(z);
            return;
        }
        super.setEnabled(z);
        A03();
    }

    public final void setIcon(int i) {
        Drawable A04;
        if (i <= 0) {
            A04 = null;
        } else {
            A04 = AnonymousClass00T.A04(getContext(), i);
        }
        setIcon(A04);
    }

    public final void setIcon(Drawable drawable) {
        setupIcon(drawable);
        A02();
        requestLayout();
    }

    public final void setOverrideButtonMigration(boolean z) {
        this.A0D = z;
    }

    public final void setSize(EnumC869649r r2) {
        C16700pc.A0E(r2, 0);
        boolean A1X = C12960it.A1X(this.A07, r2);
        this.A07 = r2;
        if (A1X) {
            A02();
            requestLayout();
        }
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.A0E && !this.A0A) {
            this.A0A = !C16700pc.A0O(getText(), String.valueOf(charSequence));
        }
        super.setText(charSequence, bufferType);
    }

    public final void setUseWDSButtonStyling(boolean z) {
        this.A0E = z;
    }

    public final void setVariant(EnumC629939l r2) {
        C16700pc.A0E(r2, 0);
        boolean A1X = C12960it.A1X(this.A08, r2);
        this.A08 = r2;
        if (A1X) {
            A03();
        }
    }

    public final void setWhatsAppLocale(AnonymousClass018 r1) {
        this.A03 = r1;
    }

    private final void setupBackgroundStyle(EnumC629839k r8) {
        C92734Xf r1;
        switch (this.A05.ordinal()) {
            case 0:
                r1 = r8.backgroundNormal;
                break;
            case 1:
                r1 = r8.backgroundDestructive;
                break;
            case 2:
                r1 = r8.backgroundMedia;
                break;
            default:
                throw C12990iw.A0v();
        }
        Context context = getContext();
        C16700pc.A0B(context);
        ColorStateList A00 = A00(context, r1);
        int defaultColor = A00.getDefaultColor();
        Drawable A01 = A01(A00.getColorForState(getDrawableState(), defaultColor), false);
        int colorForState = A00.getColorForState(new int[]{16842919}, defaultColor);
        if (isEnabled()) {
            if (C28391Mz.A02()) {
                A01 = new RippleDrawable(A00, A01, A01(colorForState, true));
            } else {
                Drawable A012 = A01(colorForState, false);
                StateListDrawable stateListDrawable = new StateListDrawable();
                stateListDrawable.addState(new int[]{16842919}, A012);
                stateListDrawable.addState(new int[0], A01);
                A01 = stateListDrawable;
            }
        }
        setBackground(A01);
    }

    private final void setupContentStyle(EnumC629839k r5) {
        C92734Xf r1;
        switch (this.A05.ordinal()) {
            case 0:
                r1 = r5.contentNormal;
                break;
            case 1:
                r1 = r5.contentDestructive;
                break;
            case 2:
                r1 = r5.contentMedia;
                break;
            default:
                throw C12990iw.A0v();
        }
        Context context = getContext();
        C16700pc.A0B(context);
        int colorForState = A00(context, r1).getColorForState(getDrawableState(), -1);
        getPaint().setColor(colorForState);
        this.A00 = new PorterDuffColorFilter(colorForState, PorterDuff.Mode.SRC_IN);
        boolean A04 = A04();
        TextPaint paint = getPaint();
        if (A04) {
            paint.setShadowLayer(2.0f, 0.0f, 0.0f, AnonymousClass00T.A00(getContext(), R.color.wds_cool_gray_alpha_20));
        } else {
            paint.clearShadowLayer();
        }
    }

    private final void setupIcon(Drawable drawable) {
        BitmapDrawable bitmapDrawable;
        this.A01 = drawable;
        if (drawable == null) {
            bitmapDrawable = null;
        } else {
            Rect bounds = drawable.getBounds();
            C16700pc.A0B(bounds);
            int width = bounds.width();
            int height = bounds.height();
            if (width <= 0 || height <= 0) {
                width = drawable.getIntrinsicWidth();
                height = drawable.getIntrinsicHeight();
            }
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            float f = (float) width;
            float f2 = f / 2.0f;
            float f3 = (float) height;
            getShaderPaint().setShader(new RadialGradient(f2, f3 / 2.0f, f2, AnonymousClass00T.A00(getContext(), R.color.wds_cool_gray_alpha_05), AnonymousClass00T.A00(getContext(), R.color.wds_transparent), Shader.TileMode.CLAMP));
            canvas.drawRect(0.0f, 0.0f, f, f3, getShaderPaint());
            bitmapDrawable = new BitmapDrawable(C12960it.A09(this), createBitmap);
        }
        this.A02 = bitmapDrawable;
    }

    private final void setupStrokeStyle(EnumC629839k r5) {
        C92734Xf r1;
        switch (this.A05.ordinal()) {
            case 0:
                r1 = r5.strokeNormal;
                break;
            case 1:
                r1 = r5.strokeDestructive;
                break;
            case 2:
                r1 = r5.strokeMedia;
                break;
            default:
                throw C12990iw.A0v();
        }
        if (r1 != null) {
            Context context = getContext();
            C16700pc.A0B(context);
            getButtonStrokePaint().setColor(A00(context, r1).getColorForState(getDrawableState(), 0));
            boolean A04 = A04();
            Paint buttonStrokePaint = getButtonStrokePaint();
            if (A04) {
                buttonStrokePaint.setShadowLayer(2.0f, 0.0f, 0.0f, AnonymousClass00T.A00(getContext(), R.color.wds_cool_gray_alpha_20));
            } else {
                buttonStrokePaint.clearShadowLayer();
            }
        }
    }
}
