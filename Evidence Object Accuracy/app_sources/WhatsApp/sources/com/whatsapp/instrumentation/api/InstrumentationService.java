package com.whatsapp.instrumentation.api;

import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass1DW;
import X.AnonymousClass5B7;
import X.BinderC51902Zo;
import X.C12970iu;
import X.C15490nL;
import X.C26441Dj;
import X.C58182oH;
import X.C71083cM;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/* loaded from: classes2.dex */
public class InstrumentationService extends Service implements AnonymousClass004 {
    public C26441Dj A00;
    public AnonymousClass1DW A01;
    public C15490nL A02;
    public boolean A03;
    public final BinderC51902Zo A04;
    public final Object A05;
    public volatile C71083cM A06;

    public InstrumentationService() {
        this(0);
        this.A04 = new BinderC51902Zo(this);
    }

    public InstrumentationService(int i) {
        this.A05 = C12970iu.A0l();
        this.A03 = false;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A06 == null) {
            synchronized (this.A05) {
                if (this.A06 == null) {
                    this.A06 = new C71083cM(this);
                }
            }
        }
        return this.A06.generatedComponent();
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.A04;
    }

    @Override // android.app.Service
    public void onCreate() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            this.A01 = (AnonymousClass1DW) r1.AIJ.get();
            this.A00 = (C26441Dj) r1.AHb.get();
            this.A02 = (C15490nL) r1.AA0.get();
        }
        super.onCreate();
    }
}
