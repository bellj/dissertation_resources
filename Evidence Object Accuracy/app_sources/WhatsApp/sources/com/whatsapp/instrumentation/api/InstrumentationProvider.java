package com.whatsapp.instrumentation.api;

import X.AbstractC14640lm;
import X.AbstractC15420nE;
import X.AbstractC15460nI;
import X.AbstractC21570xd;
import X.AnonymousClass009;
import X.AnonymousClass1VW;
import X.C15370n3;
import X.C15380n4;
import X.C15430nF;
import X.C15440nG;
import X.C15490nL;
import X.C15510nN;
import X.C15520nO;
import X.C15530nP;
import X.C15540nQ;
import X.C15580nU;
import X.C15630na;
import X.C16310on;
import X.C21580xe;
import X.C28181Ma;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class InstrumentationProvider extends AbstractC15420nE {
    public C15440nG A00;
    public C15530nP A01;
    public C15520nO A02;
    public C15490nL A03;
    public C15510nN A04;

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        return null;
    }

    public final C15430nF A02(Uri uri) {
        A01();
        if (this.A00.A00.A05(AbstractC15460nI.A18)) {
            C15430nF A00 = this.A03.A00();
            A00.A00();
            if (this.A04.A01()) {
                synchronized (this.A00) {
                }
                if (Binder.getCallingUid() != Process.myUid()) {
                    C15520nO r3 = this.A02;
                    String string = r3.A01().getString(C15520nO.A00(A00.A01, "auth/token"), null);
                    String queryParameter = uri.getQueryParameter("authorization_token");
                    if (!TextUtils.isEmpty(string) && string.equals(queryParameter)) {
                        return A00;
                    }
                    throw new SecurityException("Access denied: auth token is missing");
                }
                throw new SecurityException("Access checks is executed outside of binder context.");
            }
            throw new SecurityException("WhatsApp is not active.");
        }
        throw new SecurityException("Feature is disabled.");
    }

    @Override // android.content.ContentProvider
    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, Bundle bundle) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues, Bundle bundle) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str, CancellationSignal cancellationSignal) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, Bundle bundle, CancellationSignal cancellationSignal) {
        A02(uri);
        return super.query(uri, strArr, bundle, cancellationSignal);
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        C15430nF A02 = A02(uri);
        C15530nP r2 = this.A01;
        if (r2.A00.match(uri) == 1) {
            C15540nQ r22 = (C15540nQ) r2.A01.get();
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                C21580xe r5 = r22.A01.A06;
                C28181Ma r3 = new C28181Ma(true);
                r3.A03();
                ArrayList arrayList = new ArrayList();
                C16310on A01 = ((AbstractC21570xd) r5).A00.get();
                Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", null, null, "CONTACTS", C21580xe.A08, null);
                if (A03 == null) {
                    AnonymousClass009.A07("contact-mgr-db/unable to get all db contacts");
                    A01.close();
                } else {
                    int count = A03.getCount();
                    while (A03.moveToNext()) {
                        try {
                            arrayList.add(AnonymousClass1VW.A00(A03));
                        } catch (IllegalStateException e) {
                            if (e.getMessage() == null || !e.getMessage().contains("Make sure the Cursor is initialized correctly before accessing data from it")) {
                                throw e;
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.append("contactmanagerdb/getAllDBContacts/illegal-state-exception/cursor count=");
                            sb.append(count);
                            sb.append("; partial list size=");
                            sb.append(arrayList.size());
                            AnonymousClass009.A08(sb.toString(), e);
                        }
                    }
                    A03.close();
                    A01.close();
                    r5.A0Q(arrayList);
                    arrayList.size();
                    r3.A00();
                }
                ArrayList arrayList2 = new ArrayList();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    C15370n3 r4 = (C15370n3) it.next();
                    AbstractC14640lm r1 = (AbstractC14640lm) r4.A0B(AbstractC14640lm.class);
                    if (r1 != null && r22.A05.A01(r1) && r4.A0B(AbstractC14640lm.class) != null && r4.A0f && !C15380n4.A0F(r4.A0D) && !r22.A00.A0F(r4.A0D) && C15380n4.A0E(r4.A0D)) {
                        if (r4.A0K()) {
                            Jid jid = r4.A0D;
                            if (jid instanceof C15580nU) {
                                if (!(!r22.A03.A0C((GroupJid) jid))) {
                                }
                            }
                        }
                        if (!TextUtils.isEmpty(r22.A02.A0D(r4, false, false))) {
                            arrayList2.add(r4);
                        }
                    }
                }
                return new C15630na(r22.A02, r22.A04, A02, arrayList2, strArr);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        } else {
            StringBuilder sb2 = new StringBuilder("Access denied to ");
            sb2.append(uri);
            throw new SecurityException(sb2.toString());
        }
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal) {
        A02(uri);
        return super.query(uri, strArr, str, strArr2, str2, cancellationSignal);
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, Bundle bundle) {
        A02(uri);
        throw new UnsupportedOperationException();
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        A02(uri);
        throw new UnsupportedOperationException();
    }
}
