package com.whatsapp.instrumentation.service;

import X.AbstractServiceC27491Hs;
import X.AnonymousClass1JK;
import X.AnonymousClass1UY;
import X.C005602s;
import X.C12960it;
import X.C12990iw;
import X.C18360sK;
import X.C22630zO;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class InstrumentationFGService extends AnonymousClass1JK {
    public Handler A00 = new Handler();
    public Runnable A01 = new RunnableBRunnable0Shape15S0100000_I1_1(this, 42);
    public boolean A02 = false;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public InstrumentationFGService() {
        super("instrumentationfgservice", true);
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
    }

    @Override // X.AnonymousClass1JK, android.app.Service
    public void onDestroy() {
        stopForeground(true);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        StringBuilder A0k = C12960it.A0k("instrumentationfgservice/onStartCommand:");
        A0k.append(intent);
        A0k.append(" startId:");
        A0k.append(i2);
        C12960it.A1F(A0k);
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A0B(((AbstractServiceC27491Hs) this).A01.A09(R.string.localized_app_name));
        A00.A0A(((AbstractServiceC27491Hs) this).A01.A09(R.string.localized_app_name));
        A00.A09(((AbstractServiceC27491Hs) this).A01.A09(R.string.notification_text_instrumentation_fg));
        A00.A09 = AnonymousClass1UY.A00(this, 1, C12990iw.A0D(this, HomeActivity.class), 0);
        int i3 = -2;
        if (Build.VERSION.SDK_INT >= 26) {
            i3 = -1;
        }
        A00.A03 = i3;
        C18360sK.A01(A00, R.drawable.notifybar);
        A01(i2, A00.A01(), 25);
        Handler handler = this.A00;
        Runnable runnable = this.A01;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 5000);
        return 2;
    }
}
