package com.whatsapp.instrumentation.notification;

import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass13B;
import X.AnonymousClass1UY;
import X.AnonymousClass22D;
import X.AnonymousClass3JK;
import X.C005602s;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15520nO;
import X.C18360sK;
import X.C22630zO;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class DelayedNotificationReceiver extends BroadcastReceiver {
    public C18360sK A00;
    public AnonymousClass018 A01;
    public AnonymousClass13B A02;
    public C15520nO A03;
    public final Object A04;
    public volatile boolean A05;

    public DelayedNotificationReceiver() {
        this(0);
    }

    public DelayedNotificationReceiver(int i) {
        this.A05 = false;
        this.A04 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        int i;
        if (!this.A05) {
            synchronized (this.A04) {
                if (!this.A05) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A02 = (AnonymousClass13B) r1.A9p.get();
                    this.A00 = (C18360sK) r1.AN0.get();
                    this.A03 = (C15520nO) r1.A9z.get();
                    this.A01 = C12960it.A0R(r1);
                    this.A05 = true;
                }
            }
        }
        Iterator it = this.A03.A02().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            C15520nO r0 = this.A03;
            if (!C12980iv.A1W(r0.A01(), C15520nO.A00(A0x, "metadata/delayed_notification_shown"))) {
                C15520nO r2 = this.A03;
                long A0F = C12980iv.A0F(r2.A01(), C15520nO.A00(A0x, "auth/token_ts"));
                Number number = (Number) this.A02.A01.A00.get(A0x);
                if (number != null) {
                    i = number.intValue();
                } else {
                    i = R.string.unknown_instrumentation_device_name;
                }
                String string = context.getString(R.string.notification_companion_device_verification_title);
                String A00 = AnonymousClass3JK.A00(this.A01, A0F);
                Object[] A1a = C12980iv.A1a();
                A1a[0] = context.getString(i);
                String A0X = C12960it.A0X(context, A00, A1a, 1, R.string.notification_companion_device_verification_description);
                C005602s A002 = C22630zO.A00(context);
                A002.A0J = "other_notifications@1";
                A002.A0B(string);
                A002.A0A(string);
                A002.A09(A0X);
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(context.getPackageName(), "com.whatsapp.companiondevice.LinkedDevicesActivity");
                A002.A09 = AnonymousClass1UY.A00(context, 0, A0A, 0);
                NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
                notificationCompat$BigTextStyle.A09(A0X);
                A002.A08(notificationCompat$BigTextStyle);
                A002.A0D(true);
                C18360sK.A01(A002, R.drawable.notifybar);
                this.A00.A03(41, A002.A01());
                C15520nO r02 = this.A03;
                C12960it.A0t(r02.A01().edit(), C15520nO.A00(A0x, "metadata/delayed_notification_shown"), true);
            }
        }
        PendingIntent A01 = AnonymousClass1UY.A01(context, 0, intent, 536870912);
        if (A01 != null) {
            A01.cancel();
        }
    }
}
