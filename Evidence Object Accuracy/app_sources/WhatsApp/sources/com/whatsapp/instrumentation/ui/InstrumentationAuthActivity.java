package com.whatsapp.instrumentation.ui;

import X.AbstractC15460nI;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass138;
import X.AnonymousClass2FL;
import X.AnonymousClass5RX;
import X.AnonymousClass5RY;
import X.AnonymousClass5U7;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C14850m9;
import X.C15440nG;
import X.C15490nL;
import X.C15560nS;
import X.C16590pI;
import X.C18230s7;
import X.C19890uq;
import X.C20220vP;
import X.C20640w5;
import X.C43511x9;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.deviceauth.BiometricAuthPlugin;
import com.whatsapp.instrumentation.ui.InstrumentationAuthActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class InstrumentationAuthActivity extends ActivityC13790kL implements AnonymousClass5RX, AnonymousClass5RY {
    public C20640w5 A00;
    public C18230s7 A01;
    public C16590pI A02;
    public BiometricAuthPlugin A03;
    public C15440nG A04;
    public ConfirmFragment A05;
    public PermissionsFragment A06;
    public C15560nS A07;
    public C15490nL A08;
    public AnonymousClass138 A09;
    public C19890uq A0A;
    public C20220vP A0B;
    public String A0C;
    public boolean A0D;

    public InstrumentationAuthActivity() {
        this(0);
    }

    public InstrumentationAuthActivity(int i) {
        this.A0D = false;
        ActivityC13830kP.A1P(this, 80);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0D) {
            this.A0D = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A00 = (C20640w5) A1M.AHk.get();
            this.A09 = (AnonymousClass138) A1M.ALi.get();
            this.A0A = (C19890uq) A1M.ABw.get();
            this.A0B = (C20220vP) A1M.AC3.get();
            this.A02 = C12970iu.A0X(A1M);
            this.A01 = (C18230s7) A1M.A0Q.get();
            this.A04 = (C15440nG) A1M.A9q.get();
            this.A08 = (C15490nL) A1M.AA0.get();
            this.A07 = (C15560nS) A1M.A9r.get();
        }
    }

    public final void A2e(int i) {
        if (i == -1 || i == 4) {
            C004902f A0P = C12970iu.A0P(this);
            A0P.A07(this.A05, R.id.fragment_container);
            A0P.A0F(null);
            A0P.A01();
        }
    }

    public final void A2f(int i, String str) {
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("error_code", i);
        A0A.putExtra("error_message", str);
        setResult(0, A0A);
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 12345) {
            A2e(i2);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        String A0d;
        super.onCreate(bundle);
        setTitle(R.string.instrumentation_auth_title);
        if (!this.A04.A00.A05(AbstractC15460nI.A18)) {
            i = 3;
            A0d = "Feature is disabled!";
        } else {
            ComponentName callingActivity = getCallingActivity();
            i = 8;
            if (callingActivity == null) {
                A0d = "Not started for result.";
            } else {
                String packageName = callingActivity.getPackageName();
                try {
                    if (this.A08.A01(packageName).A03) {
                        Intent intent = getIntent();
                        String str = null;
                        if (!(intent == null || getCallingPackage() == null)) {
                            str = intent.getStringExtra("request_token");
                        }
                        this.A0C = str;
                        if (!this.A09.A01(packageName, str)) {
                            Log.e("InstrumentationAuthActivity/onCreate no correct request token!");
                            i = 4;
                            A0d = "Request is not authorized!";
                        } else {
                            setContentView(R.layout.instrumentation_auth);
                            C14850m9 r9 = ((ActivityC13810kN) this).A0C;
                            this.A03 = new BiometricAuthPlugin(this, ((ActivityC13810kN) this).A03, ((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A08, new AnonymousClass5U7() { // from class: X.56Q
                                @Override // X.AnonymousClass5U7
                                public final void AMd(int i2) {
                                    InstrumentationAuthActivity.this.A2e(i2);
                                }
                            }, r9, R.string.linked_device_unlock_to_link, 0);
                            this.A06 = new PermissionsFragment();
                            this.A05 = new ConfirmFragment();
                            if (bundle == null) {
                                C004902f A0P = C12970iu.A0P(this);
                                A0P.A06(this.A06, R.id.fragment_container);
                                A0P.A01();
                            }
                            if (this.A00.A03()) {
                                Log.w("InstrumentationAuthActivity/onCreate/clock-wrong");
                                C43511x9.A01(this, this.A0A, this.A0B);
                            } else if (this.A00.A02()) {
                                Log.w("InstrumentationAuthActivity/onCreate/sw-expired");
                                C43511x9.A02(this, this.A0A, this.A0B);
                            }
                            C12970iu.A0N(this).A0M(true);
                            return;
                        }
                    }
                } catch (PackageManager.NameNotFoundException unused) {
                }
                A0d = C12960it.A0d(packageName, C12960it.A0k("Untrusted caller: "));
            }
        }
        A2f(i, A0d);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332 || !this.A05.A0c()) {
            return super.onOptionsItemSelected(menuItem);
        }
        C004902f A0P = C12970iu.A0P(this);
        A0P.A07(this.A06, R.id.fragment_container);
        A0P.A01();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C004902f A0P = C12970iu.A0P(this);
        A0P.A07(this.A06, R.id.fragment_container);
        A0P.A01();
    }
}
