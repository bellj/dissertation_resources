package com.whatsapp.instrumentation.ui;

import X.AnonymousClass5RY;
import X.C12960it;
import X.C12970iu;
import X.C252818u;
import X.C63093Ag;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class PermissionsFragment extends Hilt_PermissionsFragment {
    public C252818u A00;
    public AnonymousClass5RY A01;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.instrumentation_permissions);
    }

    @Override // com.whatsapp.instrumentation.ui.Hilt_PermissionsFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AnonymousClass5RY) {
            this.A01 = (AnonymousClass5RY) context;
        }
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C12960it.A0x(view.findViewById(R.id.instrumentation_auth_perm_button), this, 30);
        String obj = this.A00.A00("https://faq.whatsapp.com/general/security-and-privacy/about-your-privacy-when-using-whatsapp-on-ray-ban-stories/").toString();
        TextView A0I = C12960it.A0I(view, R.id.instrumentation_auth_perm_paragraph_two);
        Object[] A1b = C12970iu.A1b();
        A1b[0] = obj;
        C63093Ag.A00(A0I, A1b, R.string.instrumentation_auth_perm_paragraph_two);
    }
}
