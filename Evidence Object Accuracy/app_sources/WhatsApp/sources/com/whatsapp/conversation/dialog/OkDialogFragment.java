package com.whatsapp.conversation.dialog;

import X.ActivityC000900k;
import X.AnonymousClass04S;
import X.C004802e;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class OkDialogFragment extends Hilt_OkDialogFragment {
    public final int A00;

    public OkDialogFragment(int i) {
        this.A00 = i;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        IDxCListenerShape4S0000000_2_I1 iDxCListenerShape4S0000000_2_I1 = new IDxCListenerShape4S0000000_2_I1(11);
        C004802e r2 = new C004802e(A0C);
        r2.A06(this.A00);
        r2.A0B(true);
        r2.setPositiveButton(R.string.ok, iDxCListenerShape4S0000000_2_I1);
        AnonymousClass04S create = r2.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
