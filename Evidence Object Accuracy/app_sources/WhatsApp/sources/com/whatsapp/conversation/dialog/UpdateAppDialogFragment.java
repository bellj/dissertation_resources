package com.whatsapp.conversation.dialog;

import X.ActivityC000900k;
import X.AnonymousClass04S;
import X.AnonymousClass12P;
import X.C004802e;
import X.C21740xu;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class UpdateAppDialogFragment extends Hilt_UpdateAppDialogFragment {
    public AnonymousClass12P A00;
    public C21740xu A01;
    public final int A02;

    public UpdateAppDialogFragment(int i) {
        this.A02 = i;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        IDxCListenerShape4S0000000_2_I1 iDxCListenerShape4S0000000_2_I1 = new IDxCListenerShape4S0000000_2_I1(12);
        IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(this, 31);
        C004802e r2 = new C004802e(A0C);
        r2.A06(this.A02);
        r2.A0B(true);
        r2.setPositiveButton(R.string.upgrade, iDxCListenerShape9S0100000_2_I1);
        r2.setNegativeButton(R.string.cancel, iDxCListenerShape4S0000000_2_I1);
        AnonymousClass04S create = r2.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
