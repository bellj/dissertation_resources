package com.whatsapp.conversation.selectlist;

import X.AnonymousClass1ZA;
import X.AnonymousClass1ZB;
import X.AnonymousClass3jA;
import X.AnonymousClass4KS;
import X.AnonymousClass4Q8;
import X.C12960it;
import X.C12970iu;
import X.C16470p4;
import X.C54352ga;
import X.C54572gw;
import X.C93354a2;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class SelectListBottomSheet extends Hilt_SelectListBottomSheet {
    public AnonymousClass4Q8 A00;
    public C16470p4 A01;

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.conversation_single_select_list_bottom_sheet);
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A12() {
        super.A12();
        this.A00 = null;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        C16470p4 r0 = (C16470p4) A03().getParcelable("arg_select_list_content");
        this.A01 = r0;
        if (r0 == null) {
            A1B();
        }
        C12960it.A0y(view.findViewById(R.id.close), this, 44);
        C12970iu.A0U(view, R.id.select_list_title).A0G(null, this.A01.A06);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.select_list_items);
        recyclerView.A0n(new AnonymousClass3jA(this));
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.A0l(new C54572gw());
        C54352ga r3 = new C54352ga();
        recyclerView.setAdapter(r3);
        List<AnonymousClass1ZA> list = this.A01.A09;
        ArrayList A0l = C12960it.A0l();
        for (AnonymousClass1ZA r2 : list) {
            String str = r2.A00;
            if (!TextUtils.isEmpty(str)) {
                A0l.add(new C93354a2(str));
            }
            for (AnonymousClass1ZB r1 : r2.A01) {
                A0l.add(new C93354a2(r1));
            }
        }
        List list2 = r3.A02;
        list2.clear();
        list2.addAll(A0l);
        r3.A02();
        C12960it.A13(view.findViewById(R.id.select_list_button), this, r3, 47);
        r3.A01 = new AnonymousClass4KS(view);
        ((DialogFragment) this).A03.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.3LB
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                View findViewById = ((Dialog) dialogInterface).findViewById(R.id.design_bottom_sheet);
                AnonymousClass009.A03(findViewById);
                BottomSheetBehavior A00 = BottomSheetBehavior.A00(findViewById);
                A00.A0M(3);
                A00.A0L(findViewById.getHeight());
            }
        });
    }
}
