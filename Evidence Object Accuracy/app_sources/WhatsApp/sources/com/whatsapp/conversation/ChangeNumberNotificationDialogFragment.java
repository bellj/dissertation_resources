package com.whatsapp.conversation;

import X.AbstractC14040kk;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.AnonymousClass1MW;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14830m7;
import X.C15370n3;
import X.C15550nR;
import X.C248917h;
import X.DialogInterface$OnClickListenerC65573Ke;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape3S0200000_1_I1;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.conversation.ChangeNumberNotificationDialogFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ChangeNumberNotificationDialogFragment extends Hilt_ChangeNumberNotificationDialogFragment {
    public C15550nR A00;
    public AbstractC14040kk A01;
    public C14830m7 A02;
    public AnonymousClass018 A03;

    public static ChangeNumberNotificationDialogFragment A00(UserJid userJid, UserJid userJid2, String str) {
        ChangeNumberNotificationDialogFragment changeNumberNotificationDialogFragment = new ChangeNumberNotificationDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("convo_jid", userJid.getRawString());
        A0D.putString("new_jid", userJid2.getRawString());
        A0D.putString("old_display_name", str);
        changeNumberNotificationDialogFragment.A0U(A0D);
        return changeNumberNotificationDialogFragment;
    }

    @Override // com.whatsapp.conversation.Hilt_ChangeNumberNotificationDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A01 = (AbstractC14040kk) context;
        } catch (ClassCastException unused) {
            StringBuilder A0h = C12960it.A0h();
            C12970iu.A1V(context, A0h);
            throw new ClassCastException(C12960it.A0d(" must implement ChangeNumberNotificationDialogListener", A0h));
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        try {
            UserJid userJid = UserJid.get(A03.getString("convo_jid"));
            UserJid userJid2 = UserJid.get(A03.getString("new_jid"));
            String string = A03.getString("old_display_name");
            AnonymousClass009.A05(string);
            C15370n3 A0B = this.A00.A0B(userJid2);
            boolean A1W = C12960it.A1W(A0B.A0C);
            C004802e A0S = C12980iv.A0S(A0p());
            IDxCListenerShape4S0000000_2_I1 iDxCListenerShape4S0000000_2_I1 = new IDxCListenerShape4S0000000_2_I1(9);
            IDxCListenerShape3S0200000_1_I1 iDxCListenerShape3S0200000_1_I1 = new IDxCListenerShape3S0200000_1_I1(A0B, 5, this);
            DialogInterface$OnClickListenerC65573Ke r3 = new DialogInterface.OnClickListener(A0B, A1W) { // from class: X.3Ke
                public final /* synthetic */ C15370n3 A01;
                public final /* synthetic */ boolean A02;

                {
                    this.A02 = r3;
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ChangeNumberNotificationDialogFragment changeNumberNotificationDialogFragment = ChangeNumberNotificationDialogFragment.this;
                    boolean z = this.A02;
                    C15370n3 r32 = this.A01;
                    if (z) {
                        dialogInterface.dismiss();
                        return;
                    }
                    AbstractC14040kk r2 = changeNumberNotificationDialogFragment.A01;
                    if (r2 != null) {
                        ((Conversation) r2).A3E(r32, (AbstractC14640lm) C15370n3.A03(r32, UserJid.class), false);
                    }
                }
            };
            if (userJid.equals(userJid2)) {
                if (A1W) {
                    A0S.A0A(C12970iu.A0q(this, this.A03.A0G(C248917h.A01(A0B)), new Object[1], 0, R.string.change_number_dialog_text_already_added));
                    A0S.setPositiveButton(R.string.ok_got_it, iDxCListenerShape4S0000000_2_I1);
                } else {
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = string;
                    A0S.A0A(C12970iu.A0q(this, C248917h.A01(A0B), A1a, 1, R.string.change_number_notification_text_new));
                    A0S.setNegativeButton(R.string.cancel, iDxCListenerShape4S0000000_2_I1);
                    A0S.setPositiveButton(R.string.add_contact, r3);
                }
            } else if (A1W) {
                A0S.A0A(C12970iu.A0q(this, this.A03.A0G(C248917h.A01(A0B)), new Object[1], 0, R.string.change_number_dialog_text_already_added));
                A0S.setPositiveButton(R.string.got_it, iDxCListenerShape4S0000000_2_I1);
                A0S.A00(R.string.change_number_message_new_number, iDxCListenerShape3S0200000_1_I1);
            } else {
                A0S.A0A(C12970iu.A0q(this, string, new Object[1], 0, R.string.change_number_notification_text_old));
                A0S.A00(R.string.send_message_to_contact_button, iDxCListenerShape3S0200000_1_I1);
                C12990iw.A0z(r3, iDxCListenerShape4S0000000_2_I1, A0S, R.string.add_contact);
            }
            AnonymousClass04S create = A0S.create();
            create.setCanceledOnTouchOutside(true);
            return create;
        } catch (AnonymousClass1MW e) {
            throw new RuntimeException(e);
        }
    }
}
