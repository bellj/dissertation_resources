package com.whatsapp.conversation;

import X.AbstractC14640lm;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass4KC;
import X.C004802e;
import X.C12970iu;
import X.C15860o1;
import X.C53312dp;
import android.app.Dialog;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ChatMediaVisibilityDialog extends Hilt_ChatMediaVisibilityDialog {
    public int A00;
    public int A01;
    public AnonymousClass4KC A02;
    public AbstractC14640lm A03;
    public C15860o1 A04;
    public boolean A05;

    public ChatMediaVisibilityDialog() {
    }

    public ChatMediaVisibilityDialog(AnonymousClass4KC r1) {
        this.A02 = r1;
    }

    public static ChatMediaVisibilityDialog A00(AnonymousClass4KC r4, AbstractC14640lm r5) {
        AnonymousClass009.A05(r5);
        ChatMediaVisibilityDialog chatMediaVisibilityDialog = new ChatMediaVisibilityDialog(r4);
        Bundle A0D = C12970iu.A0D();
        A0D.putString("chatJid", r5.getRawString());
        chatMediaVisibilityDialog.A0U(A0D);
        return chatMediaVisibilityDialog;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002e, code lost:
        if (r2 == 2) goto L_0x0030;
     */
    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A16(android.os.Bundle r4) {
        /*
            r3 = this;
            super.A16(r4)
            android.os.Bundle r1 = r3.A03()
            java.lang.String r0 = "chatJid"
            java.lang.String r0 = r1.getString(r0)
            X.0lm r2 = X.AbstractC14640lm.A01(r0)
            java.lang.String r0 = "Chat jid must be passed to "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = "ChatMediaVisibilityDialog"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            X.AnonymousClass009.A06(r2, r0)
            r3.A03 = r2
            X.0o1 r0 = r3.A04
            X.1da r0 = r0.A05()
            int r2 = r0.A00
            if (r2 == 0) goto L_0x0030
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x0031
        L_0x0030:
            r0 = 1
        L_0x0031:
            r3.A05 = r0
            X.0o1 r1 = r3.A04
            X.0lm r0 = r3.A03
            X.1da r0 = X.C15860o1.A00(r0, r1)
            int r0 = r0.A00
            r3.A00 = r0
            r3.A01 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.ChatMediaVisibilityDialog.A16(android.os.Bundle):void");
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        CharSequence[] charSequenceArr = new CharSequence[3];
        boolean z = this.A05;
        int i = R.string.default_media_visibility_off;
        if (z) {
            i = R.string.default_media_visibility_on;
        }
        int i2 = 0;
        charSequenceArr[0] = A0I(i);
        charSequenceArr[1] = A0I(R.string.yes);
        charSequenceArr[2] = A0I(R.string.no);
        int i3 = this.A00;
        if (i3 == 1) {
            i2 = 2;
        } else if (i3 == 2) {
            i2 = 1;
        }
        ActivityC000900k A0C = A0C();
        TextView textView = (TextView) A0C.getLayoutInflater().inflate(R.layout.custom_dialog_title, (ViewGroup) null);
        textView.setText(R.string.chat_media_visibility_inquiry);
        C53312dp r3 = new C53312dp(A0C);
        ((C004802e) r3).A01.A0B = textView;
        r3.A09(new IDxCListenerShape9S0100000_2_I1(this, 28), charSequenceArr, i2);
        r3.A0E(this, new IDxObserverShape3S0100000_1_I1(this, 74), R.string.ok);
        r3.A0D(this, null, R.string.cancel);
        return r3.create();
    }
}
