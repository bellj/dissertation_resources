package com.whatsapp.conversation;

import X.AbstractC14640lm;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12U;
import X.AnonymousClass14X;
import X.AnonymousClass1AA;
import X.AnonymousClass2GE;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3G9;
import X.C14820m6;
import X.C14850m9;
import X.C15450nH;
import X.C15570nT;
import X.C15600nX;
import X.C17170qN;
import X.C17900ra;
import X.C17930rd;
import X.C19990v2;
import X.C22710zW;
import X.C42531vM;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.text.CondensedTextView;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class ConversationAttachmentContentView extends ScrollView implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public LinearLayout A09;
    public C15570nT A0A;
    public C15450nH A0B;
    public AnonymousClass1AA A0C;
    public C42531vM A0D;
    public AnonymousClass01d A0E;
    public C17170qN A0F;
    public C14820m6 A0G;
    public C19990v2 A0H;
    public C15600nX A0I;
    public C14850m9 A0J;
    public AbstractC14640lm A0K;
    public C17900ra A0L;
    public C22710zW A0M;
    public AnonymousClass14X A0N;
    public AnonymousClass12U A0O;
    public AnonymousClass2P7 A0P;
    public boolean A0Q;
    public final LinkedHashMap A0R;

    public ConversationAttachmentContentView(Context context) {
        super(context);
        A05();
        this.A0R = new LinkedHashMap();
        A06();
    }

    public ConversationAttachmentContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A05();
        this.A0R = new LinkedHashMap();
        A06();
    }

    public ConversationAttachmentContentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A05();
        this.A0R = new LinkedHashMap();
        A06();
    }

    public ConversationAttachmentContentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A05();
        this.A0R = new LinkedHashMap();
        A06();
    }

    public ConversationAttachmentContentView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A05();
    }

    public int A00(View view) {
        WindowManager A0O = this.A0E.A0O();
        Point point = new Point();
        A0O.getDefaultDisplay().getSize(point);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return Math.max(getMaxMarginSize() + (getIconSize() * getNumberOfColumns()), ((iArr[0] + (view.getWidth() / 2)) - (point.x / 2)) << 1);
    }

    public int A01(View view) {
        int ceil = (int) Math.ceil(((double) A03().size()) / ((double) getNumberOfColumns()));
        int A01 = ((this.A02 + this.A01 + this.A04 + this.A05) * ceil) + ((ceil - 1) * this.A03) + (this.A00 << 1) + AnonymousClass3G9.A01(getContext(), 1.0f);
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int measuredHeight = ((iArr[1] - view.getMeasuredHeight()) - getResources().getDimensionPixelSize(R.dimen.attach_popup_min_top_offset)) - AnonymousClass3G9.A00(getContext());
        return (measuredHeight >= A01 || A01 - (this.A02 / 2) <= measuredHeight) ? A01 : measuredHeight;
    }

    public final View A02(Drawable drawable, View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener, LinearLayout linearLayout, int i, int i2, int i3, int i4, boolean z) {
        int i5;
        Drawable A02;
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.conversation_attachment_picker_row_item, (ViewGroup) linearLayout, false);
        ImageView imageView = (ImageView) AnonymousClass028.A0D(inflate, R.id.icon);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.text);
        if (z) {
            i5 = getResources().getDimensionPixelSize(R.dimen.attach_popup_icon_size);
        } else {
            i5 = 0;
        }
        int A00 = AnonymousClass00T.A00(getContext(), i);
        int A002 = AnonymousClass00T.A00(getContext(), i2);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{A00, A002});
        gradientDrawable.setShape(1);
        gradientDrawable.setGradientType(2);
        gradientDrawable.setGradientCenter(-1.0f, 0.5f);
        if (i5 > 0) {
            gradientDrawable.setSize(i5, i5);
        }
        if (!z || Build.VERSION.SDK_INT < 23) {
            A02 = AnonymousClass2GE.A02(getContext(), gradientDrawable);
        } else {
            ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
            shapeDrawable.getPaint().setColor(A002);
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{gradientDrawable, shapeDrawable});
            int i6 = i5 >> 2;
            layerDrawable.setLayerSize(1, i6, i6);
            layerDrawable.setLayerGravity(1, 17);
            A02 = AnonymousClass2GE.A02(getContext(), layerDrawable);
        }
        imageView.setBackgroundDrawable(A02);
        imageView.setImageDrawable(drawable);
        AnonymousClass028.A0a(imageView, 2);
        textView.setText(i3);
        inflate.setId(i4);
        imageView.setOnClickListener(onClickListener);
        inflate.setOnClickListener(onClickListener);
        if (onLongClickListener != null) {
            imageView.setOnLongClickListener(onLongClickListener);
            inflate.setOnLongClickListener(onLongClickListener);
        }
        return inflate;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0045, code lost:
        if (r2 == 3) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A03() {
        /*
            r5 = this;
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.14X r1 = r5.A0N
            X.0lm r0 = r5.A0K
            int r0 = r1.A08(r0)
            r5.A06 = r0
            X.14X r4 = r5.A0N
            android.content.Context r2 = r5.getContext()
            int r1 = r5.A06
            X.0lm r0 = r5.A0K
            com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.of(r0)
            boolean r0 = r4.A0a(r2, r0, r1)
            if (r0 == 0) goto L_0x0075
            X.0nT r0 = r5.A0A
            r0.A08()
            r4 = 1
        L_0x0029:
            X.0m9 r1 = r5.A0J
            r0 = 1394(0x572, float:1.953E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0047
            X.0lm r1 = r5.A0K
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 == 0) goto L_0x0047
            X.0v2 r0 = r5.A0H
            com.whatsapp.jid.GroupJid r1 = (com.whatsapp.jid.GroupJid) r1
            int r2 = r0.A02(r1)
            r0 = 3
            r1 = 1
            if (r2 != r0) goto L_0x0048
        L_0x0047:
            r1 = 0
        L_0x0048:
            java.lang.String r0 = "document"
            r3.add(r0)
            java.lang.String r0 = "camera"
            r3.add(r0)
            java.lang.String r0 = "gallery"
            r3.add(r0)
            java.lang.String r0 = "audio"
            r3.add(r0)
            java.lang.String r0 = "location"
            r3.add(r0)
            if (r4 == 0) goto L_0x0068
            java.lang.String r0 = "payment"
            r3.add(r0)
        L_0x0068:
            java.lang.String r0 = "contact"
            r3.add(r0)
            if (r1 == 0) goto L_0x0074
            java.lang.String r0 = "poll"
            r3.add(r0)
        L_0x0074:
            return r3
        L_0x0075:
            r4 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.ConversationAttachmentContentView.A03():java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0165, code lost:
        if (r3 == 4) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x016f, code lost:
        if (r3 == 4) goto L_0x0171;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0045 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0081 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
        // Method dump skipped, instructions count: 608
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.ConversationAttachmentContentView.A04():void");
    }

    public void A05() {
        if (!this.A0Q) {
            this.A0Q = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A0J = (C14850m9) r1.A04.get();
            this.A0A = (C15570nT) r1.AAr.get();
            this.A0H = (C19990v2) r1.A3M.get();
            this.A0O = (AnonymousClass12U) r1.AJd.get();
            this.A0B = (C15450nH) r1.AII.get();
            this.A0N = (AnonymousClass14X) r1.AFM.get();
            this.A0E = (AnonymousClass01d) r1.ALI.get();
            this.A0G = (C14820m6) r1.AN3.get();
            this.A0M = (C22710zW) r1.AF7.get();
            this.A0L = (C17900ra) r1.AF5.get();
            this.A0I = (C15600nX) r1.A8x.get();
            this.A0F = (C17170qN) r1.AMt.get();
            this.A0C = (AnonymousClass1AA) r1.ADr.get();
        }
    }

    public final void A06() {
        this.A02 = getResources().getDimensionPixelSize(R.dimen.attach_popup_icon_size);
        this.A01 = getResources().getDimensionPixelSize(R.dimen.attach_popup_icon_bottom_margin);
        this.A04 = getIconTextViewHeight();
        this.A00 = getResources().getDimensionPixelSize(R.dimen.conversation_attachment_edge_padding);
        this.A03 = getResources().getDimensionPixelSize(R.dimen.conversation_attachment_item_padding);
        ScrollView.inflate(getContext(), R.layout.conversation_attachment_content_view, this);
        this.A09 = (LinearLayout) AnonymousClass028.A0D(this, R.id.row_content);
    }

    public void A07(int i, boolean z) {
        int[] iArr;
        long j;
        int[][] iArr2 = new int[3];
        int[] iArr3 = {6, 0, 0, 0};
        if (z) {
            // fill-array-data instruction
            iArr3[0] = 2;
            iArr3[1] = 3;
            iArr3[2] = 6;
            iArr3[3] = 8;
            iArr2[0] = iArr3;
            iArr2[1] = new int[]{3, 6, 0, 0};
            iArr = new int[]{6, 0, 0, 0};
        } else {
            iArr2[0] = iArr3;
            iArr2[1] = new int[]{3, 6, 0, 0};
            iArr = new int[]{2, 3, 6, 8};
        }
        iArr2[2] = iArr;
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : this.A0R.entrySet()) {
            arrayList.add(entry.getValue());
        }
        int i2 = 0;
        int i3 = 0;
        do {
            int[] iArr4 = iArr2[i2];
            for (int i4 = 0; i4 < iArr4.length; i4++) {
                if (i4 < getNumberOfColumns() && i3 < arrayList.size()) {
                    View view = (View) arrayList.get(i3);
                    int i5 = iArr4[i4];
                    AnimationSet animationSet = new AnimationSet(true);
                    float f = 0.0f;
                    if (z) {
                        f = 1.0f;
                    }
                    animationSet.addAnimation(new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, f));
                    animationSet.setInterpolator(new OvershootInterpolator(1.0f));
                    animationSet.setDuration(300);
                    if (i5 == 0) {
                        j = 0;
                    } else {
                        j = (long) (i / i5);
                    }
                    animationSet.setStartOffset(j);
                    view.startAnimation(animationSet);
                    i3++;
                }
            }
            i2++;
        } while (i2 < 3);
    }

    @Override // android.widget.ScrollView, android.view.View
    public int computeVerticalScrollOffset() {
        int computeVerticalScrollOffset = super.computeVerticalScrollOffset();
        int computeVerticalScrollRange = computeVerticalScrollRange() - getHeight();
        int i = this.A07;
        int i2 = computeVerticalScrollRange - (i << 1);
        return (i <= 0 || i2 <= 0) ? computeVerticalScrollOffset : i + ((computeVerticalScrollOffset * i2) / computeVerticalScrollRange);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0P;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0P = r0;
        }
        return r0.generatedComponent();
    }

    private String getCommerceAttachmentType() {
        return "product";
    }

    private Drawable getCurrencyDrawable() {
        C17930rd A01 = this.A0L.A01();
        Context context = getContext();
        int A00 = AnonymousClass14X.A00(A01);
        if (A00 != 0) {
            return AnonymousClass00T.A04(context, A00);
        }
        return null;
    }

    private int getIconSize() {
        return getResources().getDimensionPixelSize(R.dimen.quickaction_button_h);
    }

    private int getIconTextViewHeight() {
        CondensedTextView condensedTextView = new CondensedTextView(getContext(), null, R.style.AttachPickerLabelV2);
        condensedTextView.measure(View.MeasureSpec.makeMeasureSpec(getResources().getDisplayMetrics().widthPixels, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        return condensedTextView.getMeasuredHeight();
    }

    private int getMaxMarginSize() {
        return (getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_max_padding) << 1) + getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_margin);
    }

    private int getMinMarginSize() {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_min_padding);
        getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_max_padding);
        return (dimensionPixelSize << 1) + getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_margin);
    }

    private int getNumberOfColumns() {
        WindowManager A0O = this.A0E.A0O();
        Point point = new Point();
        A0O.getDefaultDisplay().getSize(point);
        return Math.min(4, Math.max(3, (point.x - getMinMarginSize()) / getIconSize()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e8  */
    @Override // android.widget.ScrollView, android.widget.FrameLayout, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r19, int r20) {
        /*
        // Method dump skipped, instructions count: 248
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.ConversationAttachmentContentView.onMeasure(int, int):void");
    }

    public void setVerticalScrollbarInset(int i) {
        this.A07 = i;
    }
}
