package com.whatsapp.conversation;

import X.ActivityC000900k;
import X.AnonymousClass02B;
import X.C004802e;
import X.C12980iv;
import X.C53312dp;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ChatMediaEphemeralVisibilityDialog extends Hilt_ChatMediaEphemeralVisibilityDialog {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ActivityC000900k A0C = A0C();
        C53312dp r2 = new C53312dp(A0C);
        TextView textView = (TextView) C12980iv.A0O(A0C.getLayoutInflater(), R.layout.custom_dialog_title);
        textView.setText(R.string.ephemeral_media_visibility_warning_title);
        ((C004802e) r2).A01.A0B = textView;
        r2.A06(R.string.ephemeral_media_visibility_warning);
        r2.A0G(this, new AnonymousClass02B() { // from class: X.4s3
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                ((DialogInterface) obj).dismiss();
            }
        }, A0I(R.string.ok));
        return r2.create();
    }
}
