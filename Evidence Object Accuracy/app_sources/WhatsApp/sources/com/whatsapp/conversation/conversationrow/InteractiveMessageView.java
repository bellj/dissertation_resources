package com.whatsapp.conversation.conversationrow;

import X.AbstractC17190qP;
import X.AbstractC28491Nn;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass19O;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4NT;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C50512Pv;
import X.C60882yv;
import X.C60892yw;
import X.C60902yx;
import X.C60912yy;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.Map;

/* loaded from: classes2.dex */
public class InteractiveMessageView extends LinearLayout implements AnonymousClass004 {
    public C50512Pv A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public Map A03;
    public boolean A04;
    public final FrameLayout A05;
    public final TextEmojiLabel A06;
    public final TextEmojiLabel A07;
    public final AnonymousClass4NT A08;

    public InteractiveMessageView(Context context) {
        this(context, null);
    }

    public InteractiveMessageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        LayoutInflater.from(context).inflate(R.layout.interactive_message_content, (ViewGroup) this, true);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.interactive_message_header_holder);
        this.A05 = frameLayout;
        this.A08 = new AnonymousClass4NT(frameLayout, this.A03);
        this.A06 = C12970iu.A0T(this, R.id.description);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.bottom_message);
        this.A07 = A0T;
        TextEmojiLabel textEmojiLabel = this.A06;
        AbstractC28491Nn.A03(textEmojiLabel);
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
        AbstractC28491Nn.A03(A0T);
    }

    public InteractiveMessageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r2 = r3.A06;
            this.A03 = AbstractC17190qP.of((Object) 1, (Object) new C60912yy(C12970iu.A0X(r2), C12960it.A0R(r2), (AnonymousClass19O) r2.ACO.get()), (Object) C12970iu.A0g(), (Object) new C60882yv(), (Object) C12970iu.A0h(), (Object) new C60902yx(C12970iu.A0X(r2), C12960it.A0R(r2), (AnonymousClass19O) r2.ACO.get()), (Object) C12980iv.A0j(), (Object) new C60892yw(C12970iu.A0X(r2), (AnonymousClass19O) r2.ACO.get()));
            this.A00 = r3.A03();
            this.A01 = C12960it.A0R(r2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        if (r2 != 5) goto L_0x0028;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1OY r10, X.AbstractC15340mz r11) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.InteractiveMessageView.A00(X.1OY, X.0mz):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public FrameLayout getInnerFrameLayout() {
        return (FrameLayout) this.A08.A00.findViewById(R.id.frame_header);
    }

    public void setLayoutView(int i) {
        TextEmojiLabel textEmojiLabel;
        Context context;
        int i2;
        if (i == 0) {
            textEmojiLabel = this.A07;
            context = getContext();
            i2 = R.color.conversation_row_date_right;
        } else if (i == 1) {
            textEmojiLabel = this.A07;
            context = getContext();
            i2 = R.color.conversation_row_date;
        } else {
            return;
        }
        C12960it.A0s(context, textEmojiLabel, i2);
    }
}
