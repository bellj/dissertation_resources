package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.AnonymousClass3GD;
import X.C12980iv;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConversationRowImageAlbum$AlbumGridFrame extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public ConversationRowImageAlbum$AlbumGridFrame(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationRowImageAlbum$AlbumGridFrame(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        A00(context);
    }

    public ConversationRowImageAlbum$AlbumGridFrame(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationRowImageAlbum$AlbumGridFrame(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationRowImageAlbum$AlbumGridFrame(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        this.A00 = context.getResources().getDimensionPixelSize(R.dimen.conversation_video_thumb_padding);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int A02;
        int mode = View.MeasureSpec.getMode(i);
        if (isInEditMode()) {
            A02 = 800;
        } else {
            A02 = AnonymousClass3GD.A02(this);
        }
        if (mode != 0) {
            A02 = Math.min(A02, View.MeasureSpec.getSize(i));
        }
        int A04 = C12980iv.A04((A02 - this.A00) >> 1);
        int i3 = 0;
        do {
            getChildAt(i3).measure(A04, A04);
            i3++;
        } while (i3 < 4);
        setMeasuredDimension(A02, A02);
    }
}
