package com.whatsapp.conversation.conversationrow.message;

import X.AbstractC14440lR;
import X.AnonymousClass014;
import X.C242014p;
import X.C26221Cm;
import X.C27691It;
import android.app.Application;

/* loaded from: classes3.dex */
public class MessageDetailsViewModel extends AnonymousClass014 {
    public final C242014p A00;
    public final C26221Cm A01;
    public final C27691It A02 = new C27691It();
    public final C27691It A03 = new C27691It();
    public final AbstractC14440lR A04;

    public MessageDetailsViewModel(Application application, C242014p r3, C26221Cm r4, AbstractC14440lR r5) {
        super(application);
        this.A04 = r5;
        this.A00 = r3;
        this.A01 = r4;
    }
}
