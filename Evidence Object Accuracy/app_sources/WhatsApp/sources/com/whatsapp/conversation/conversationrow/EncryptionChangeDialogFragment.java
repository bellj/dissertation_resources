package com.whatsapp.conversation.conversationrow;

import X.AbstractC14640lm;
import X.AnonymousClass018;
import X.AnonymousClass11G;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass30Z;
import X.C12970iu;
import X.C14850m9;
import X.C15450nH;
import X.C15550nR;
import X.C15600nX;
import X.C15610nY;
import X.C16120oU;
import X.C22700zV;
import X.C252018m;
import X.C38301nr;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class EncryptionChangeDialogFragment extends Hilt_EncryptionChangeDialogFragment {
    public AnonymousClass12P A00;
    public C15450nH A01;
    public C15550nR A02;
    public C22700zV A03;
    public C15610nY A04;
    public AnonymousClass018 A05;
    public C15600nX A06;
    public AnonymousClass19M A07;
    public C14850m9 A08;
    public C16120oU A09;
    public AnonymousClass30Z A0A;
    public AnonymousClass11G A0B;
    public C252018m A0C;

    public static EncryptionChangeDialogFragment A00(C22700zV r5, UserJid userJid) {
        C38301nr r4 = new C38301nr(r5, userJid);
        EncryptionChangeDialogFragment encryptionChangeDialogFragment = new EncryptionChangeDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", userJid.getRawString());
        A0D.putInt("business_state_id", r4.A01());
        encryptionChangeDialogFragment.A0U(A0D);
        return encryptionChangeDialogFragment;
    }

    public static EncryptionChangeDialogFragment A01(AbstractC14640lm r5) {
        EncryptionChangeDialogFragment encryptionChangeDialogFragment = new EncryptionChangeDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r5.getRawString());
        A0D.putInt("provider_category", 0);
        encryptionChangeDialogFragment.A0U(A0D);
        return encryptionChangeDialogFragment;
    }

    public static EncryptionChangeDialogFragment A02(AbstractC14640lm r4, int i) {
        EncryptionChangeDialogFragment encryptionChangeDialogFragment = new EncryptionChangeDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r4.getRawString());
        A0D.putInt("business_state_id", i);
        encryptionChangeDialogFragment.A0U(A0D);
        return encryptionChangeDialogFragment;
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x01a8  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01b3  */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 598
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.EncryptionChangeDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        AnonymousClass30Z r1 = this.A0A;
        if (r1 != null) {
            r1.A01 = 0;
            this.A09.A07(r1);
        }
        super.onCancel(dialogInterface);
    }
}
