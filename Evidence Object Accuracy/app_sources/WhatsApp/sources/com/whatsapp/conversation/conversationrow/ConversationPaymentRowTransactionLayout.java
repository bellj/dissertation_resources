package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ConversationPaymentRowTransactionLayout extends LinearLayout implements AnonymousClass004 {
    public ViewGroup A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public ConversationPaymentRowTransactionLayout(Context context) {
        this(context, null, 0, 0, 0);
    }

    public ConversationPaymentRowTransactionLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ConversationPaymentRowTransactionLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ConversationPaymentRowTransactionLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public ConversationPaymentRowTransactionLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int measuredWidth;
        int measuredHeight;
        TextView textView = (TextView) getChildAt(0);
        TextView textView2 = (TextView) getChildAt(1);
        if (textView2.getVisibility() != 0) {
            if (textView.getVisibility() != 0) {
                textView = null;
            }
            textView2 = textView;
        }
        super.onMeasure(i, i2);
        if (textView2 != null && this.A00 != null) {
            boolean A1X = C12980iv.A1X(textView2.getLayout());
            int measuredWidth2 = getMeasuredWidth();
            Layout layout = textView2.getLayout();
            if (layout == null) {
                Log.e("ConversationRowTransactionPill/onMeasure/error getting textView layout");
                return;
            }
            if (layout.getLineCount() > 1) {
                int desiredWidth = (int) Layout.getDesiredWidth(textView2.getText().subSequence(layout.getLineStart(layout.getLineCount() - 1), layout.getLineEnd(layout.getLineCount() - 1)), textView2.getPaint());
                int measuredWidth3 = textView2.getMeasuredWidth();
                if (!A1X && textView2.getText() != null && TextUtils.indexOf(textView2.getText(), '\n') >= 0) {
                    measuredWidth3 = Math.min(measuredWidth3, ((int) Math.ceil((double) Layout.getDesiredWidth(textView2.getText(), textView2.getPaint()))) + textView2.getPaddingRight() + textView2.getPaddingLeft());
                }
                if (C12960it.A04(this, measuredWidth2) >= this.A00.getMeasuredWidth() + measuredWidth3) {
                    setMeasuredDimension(C12970iu.A04(this.A00, this, measuredWidth3), getMeasuredHeight());
                    return;
                } else if (C12960it.A04(textView2, measuredWidth3) >= desiredWidth + this.A00.getMeasuredWidth()) {
                    return;
                }
            } else if (C12960it.A04(this, measuredWidth2) >= textView2.getMeasuredWidth() + this.A00.getMeasuredWidth()) {
                measuredWidth = C12970iu.A04(this.A00, this, textView2.getMeasuredWidth());
                if (measuredWidth > getMeasuredWidth()) {
                    measuredHeight = getMeasuredHeight();
                    setMeasuredDimension(measuredWidth, measuredHeight);
                }
                return;
            }
            measuredWidth = getMeasuredWidth();
            measuredHeight = getMeasuredHeight() + this.A00.getMeasuredHeight();
            setMeasuredDimension(measuredWidth, measuredHeight);
        }
    }

    public void setDateWrapper(ViewGroup viewGroup) {
        this.A00 = viewGroup;
    }
}
