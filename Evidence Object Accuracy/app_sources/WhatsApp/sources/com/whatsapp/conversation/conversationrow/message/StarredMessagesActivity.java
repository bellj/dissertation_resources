package com.whatsapp.conversation.conversationrow.message;

import X.AbstractActivityC13750kH;
import X.AbstractActivityC35431hr;
import X.AbstractActivityC35471hw;
import X.AbstractC18860tB;
import X.ActivityC13770kJ;
import X.AnonymousClass018;
import X.AnonymousClass43E;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C22230yk;
import X.C35331he;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class StarredMessagesActivity extends AbstractActivityC35471hw {
    public MenuItem A00;
    public C22230yk A01;
    public final AbstractC18860tB A02 = new C35331he(this);

    @Override // X.AbstractActivityC35431hr, X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.starred_messages);
        ((AbstractActivityC13750kH) this).A0R.A03(this.A02);
        AnonymousClass43E r1 = new AnonymousClass43E();
        if (((AbstractActivityC35431hr) this).A0I == null) {
            r1.A00 = 1;
        } else {
            r1.A00 = 0;
        }
        this.A0U.A07(r1);
        setContentView(R.layout.starred_messages);
        ListView A2e = A2e();
        A2e.setFastScrollEnabled(false);
        A2e.setScrollbarFadingEnabled(true);
        A2e.setOnScrollListener(((AbstractActivityC35431hr) this).A0Q);
        A2f(((AbstractActivityC35431hr) this).A07);
        A2p();
    }

    @Override // X.AbstractActivityC35431hr, X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem add = menu.add(0, R.id.menuitem_unstar_all, 0, R.string.unstar_all);
        this.A00 = add;
        add.setShowAsAction(0);
        this.A00.setVisible(!((ActivityC13770kJ) this).A00.isEmpty());
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC35431hr, X.AbstractActivityC13750kH, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        ((AbstractActivityC13750kH) this).A0R.A04(this.A02);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_unstar_all) {
            return super.onOptionsItemSelected(menuItem);
        }
        new UnstarAllDialogFragment().A1F(A0V(), "UnstarAllDialogFragment");
        return true;
    }

    /* loaded from: classes2.dex */
    public class UnstarAllDialogFragment extends Hilt_StarredMessagesActivity_UnstarAllDialogFragment {
        public AnonymousClass018 A00;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e A0O = C12970iu.A0O(this);
            A0O.A06(R.string.unstar_all_confirmation);
            return C12960it.A0L(new IDxCListenerShape8S0100000_1_I1(this, 19), A0O, R.string.remove_star);
        }
    }
}
