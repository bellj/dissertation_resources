package com.whatsapp.conversation.conversationrow;

import X.AbstractC15340mz;
import X.AbstractC28491Nn;
import X.AbstractC28551Oa;
import X.AnonymousClass004;
import X.AnonymousClass1OY;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C30441Xk;
import X.C35011h5;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes2.dex */
public class DynamicButtonsRowContentLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final TextEmojiLabel A02;
    public final TextEmojiLabel A03;

    public DynamicButtonsRowContentLayout(Context context) {
        this(context, null);
    }

    public DynamicButtonsRowContentLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DynamicButtonsRowContentLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.dynamic_reply_message_content, this);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.top_message);
        this.A03 = A0U;
        TextEmojiLabel A0U2 = C12970iu.A0U(this, R.id.bottom_message);
        this.A02 = A0U2;
        setupContentView(A0U);
        setupContentView(A0U2);
    }

    public void A00(AnonymousClass1OY r16) {
        int i;
        AbstractC15340mz fMessage = r16.getFMessage();
        C30441Xk r0 = fMessage.A0F().A00;
        if (r0 != null) {
            String str = r0.A00;
            String str2 = r0.A01;
            if (!TextUtils.isEmpty(str)) {
                Context context = getContext();
                Object[] objArr = new Object[1];
                Context context2 = getContext();
                byte b = fMessage.A0y;
                if (b != 0) {
                    i = R.string.accessibility_message_with_buttons_image;
                    if (b != 1) {
                        i = R.string.accessibility_message_with_buttons_video;
                        if (b != 3) {
                            i = R.string.accessibility_message_with_buttons_location;
                            if (b != 5) {
                                i = R.string.accessibility_message_with_buttons_document;
                                if (b != 9) {
                                    i = 0;
                                }
                            }
                        }
                    }
                } else {
                    i = R.string.accessibility_message_with_buttons_text;
                }
                StringBuilder A0k = C12960it.A0k(C12960it.A0X(context, context2.getString(i), objArr, 0, R.string.accessibility_message_with_buttons_focus_format));
                String A0I = fMessage.A0I();
                if (!TextUtils.isEmpty(A0I) && b == 0) {
                    A0k.append(A0I);
                }
                r16.setContentDescription(C12960it.A0d(C35011h5.A00(fMessage), A0k));
                if (!TextUtils.isEmpty(str2)) {
                    TextEmojiLabel textEmojiLabel = this.A03;
                    r16.setMessageText(str, textEmojiLabel, fMessage);
                    textEmojiLabel.setVisibility(0);
                    TextEmojiLabel textEmojiLabel2 = this.A02;
                    r16.A17(textEmojiLabel2, fMessage, str2, true, false);
                    textEmojiLabel2.setTextSize(AnonymousClass1OY.A02(r16.getResources(), ((AbstractC28551Oa) r16).A0K, -1));
                    textEmojiLabel2.setTextColor(r16.getSecondaryTextColor());
                    return;
                }
                TextEmojiLabel textEmojiLabel3 = this.A02;
                r16.A17(textEmojiLabel3, fMessage, str, true, true);
                this.A03.setVisibility(8);
                C12960it.A0s(r16.getContext(), textEmojiLabel3, R.color.conversation_template_top_message_text_color);
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    private void setupContentView(TextEmojiLabel textEmojiLabel) {
        AbstractC28491Nn.A03(textEmojiLabel);
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
    }
}
