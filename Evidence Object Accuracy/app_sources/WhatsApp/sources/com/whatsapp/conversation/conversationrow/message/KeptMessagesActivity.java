package com.whatsapp.conversation.conversationrow.message;

import X.AbstractActivityC13750kH;
import X.AbstractActivityC35431hr;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass109;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass116;
import X.AnonymousClass11G;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.AnonymousClass13H;
import X.AnonymousClass15Q;
import X.AnonymousClass190;
import X.AnonymousClass193;
import X.AnonymousClass198;
import X.AnonymousClass19D;
import X.AnonymousClass19I;
import X.AnonymousClass19J;
import X.AnonymousClass19K;
import X.AnonymousClass19L;
import X.AnonymousClass19M;
import X.AnonymousClass1A6;
import X.AnonymousClass1AB;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103024q5;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15240mn;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16210od;
import X.C16490p7;
import X.C16590pI;
import X.C16630pM;
import X.C17070qD;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C18850tA;
import X.C20000v3;
import X.C20030v6;
import X.C20040v7;
import X.C20710wC;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22180yf;
import X.C22330yu;
import X.C22370yy;
import X.C22670zS;
import X.C22700zV;
import X.C22710zW;
import X.C23000zz;
import X.C231510o;
import X.C239613r;
import X.C239913u;
import X.C240514a;
import X.C242114q;
import X.C244215l;
import X.C245115u;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C253018w;
import X.C253118x;
import X.C253218y;
import X.C255419u;
import X.C25641Ae;
import X.C26501Ds;
import X.C32341c0;
import X.C35351hg;
import X.C615630v;
import X.C88054Ec;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.jid.GroupJid;

/* loaded from: classes2.dex */
public class KeptMessagesActivity extends AbstractActivityC35431hr {
    public ProgressBar A00;
    public ScrollView A01;
    public WaTextView A02;
    public C20030v6 A03;
    public boolean A04;
    public final AbstractC18860tB A05;

    public KeptMessagesActivity() {
        this(0);
        this.A05 = new C35351hg(this);
    }

    public KeptMessagesActivity(int i) {
        this.A04 = false;
        A0R(new C103024q5(this));
    }

    @Override // X.AbstractActivityC13760kI, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC13750kH) this).A0K = (C16590pI) r1.AMg.get();
            this.A0Z = (AnonymousClass13H) r1.ABY.get();
            this.A0j = (C253018w) r1.AJS.get();
            this.A0o = (C253118x) r1.AAW.get();
            this.A0U = (C16120oU) r1.ANE.get();
            this.A0a = (C245115u) r1.A7s.get();
            ((AbstractActivityC13750kH) this).A05 = (C18850tA) r1.AKx.get();
            ((AbstractActivityC13750kH) this).A03 = (C16170oZ) r1.AM4.get();
            this.A0p = (C26501Ds) r1.AML.get();
            ((AbstractActivityC13750kH) this).A0B = (C21270x9) r1.A4A.get();
            ((AbstractActivityC13750kH) this).A07 = (C15550nR) r1.A45.get();
            ((AbstractActivityC13750kH) this).A0Q = (C20040v7) r1.AAK.get();
            this.A0m = (C252018m) r1.A7g.get();
            ((AbstractActivityC13750kH) this).A09 = (C15610nY) r1.AMe.get();
            this.A0d = (C17070qD) r1.AFC.get();
            this.A0f = (C239913u) r1.AC1.get();
            ((AbstractActivityC13750kH) this).A0O = (C253218y) r1.AAF.get();
            ((AbstractActivityC13750kH) this).A0M = (C15650ng) r1.A4m.get();
            ((AbstractActivityC13750kH) this).A0R = (AnonymousClass12H) r1.AC5.get();
            ((AbstractActivityC13750kH) this).A08 = (AnonymousClass190) r1.AIZ.get();
            this.A0i = (AnonymousClass12F) r1.AJM.get();
            ((AbstractActivityC13750kH) this).A0P = (C20000v3) r1.AAG.get();
            ((AbstractActivityC13750kH) this).A06 = (AnonymousClass116) r1.A3z.get();
            ((AbstractActivityC13750kH) this).A0T = (AnonymousClass193) r1.A6S.get();
            ((AbstractActivityC13750kH) this).A0S = (C242114q) r1.AJq.get();
            ((AbstractActivityC13750kH) this).A0L = (C15890o4) r1.AN1.get();
            this.A0V = (C20710wC) r1.A8m.get();
            this.A0n = (AnonymousClass198) r1.A0J.get();
            this.A0h = (C240514a) r1.AJL.get();
            this.A0W = (AnonymousClass11G) r1.AKq.get();
            ((AbstractActivityC13750kH) this).A0A = (AnonymousClass10T) r1.A47.get();
            this.A0Y = (C22370yy) r1.AB0.get();
            this.A0c = (C22710zW) r1.AF7.get();
            ((AbstractActivityC13750kH) this).A04 = (C14650lo) r1.A2V.get();
            this.A0l = (AnonymousClass1AB) r1.AKI.get();
            this.A0X = (AnonymousClass109) r1.AI8.get();
            ((AbstractActivityC13750kH) this).A0N = (C15600nX) r1.A8x.get();
            ((AbstractActivityC13750kH) this).A02 = (C16210od) r1.A0Y.get();
            ((AbstractActivityC13750kH) this).A0H = (AnonymousClass19D) r1.ABo.get();
            ((AbstractActivityC13750kH) this).A0I = (AnonymousClass11P) r1.ABp.get();
            ((AbstractActivityC13750kH) this).A0E = (AnonymousClass19I) r1.A4a.get();
            this.A0b = (AnonymousClass19J) r1.ACA.get();
            ((AbstractActivityC13750kH) this).A0C = (AnonymousClass19K) r1.AFh.get();
            this.A0q = (AnonymousClass19L) r1.A6l.get();
            ((AbstractActivityC35431hr) this).A01 = (C239613r) r1.AI9.get();
            ((AbstractActivityC35431hr) this).A0L = (AnonymousClass12U) r1.AJd.get();
            ((AbstractActivityC35431hr) this).A0G = (C231510o) r1.AHO.get();
            ((AbstractActivityC35431hr) this).A0K = new C88054Ec();
            ((AbstractActivityC35431hr) this).A0F = (C22180yf) r1.A5U.get();
            ((AbstractActivityC35431hr) this).A0C = (C15240mn) r1.A8F.get();
            ((AbstractActivityC35431hr) this).A04 = (AnonymousClass10S) r1.A46.get();
            ((AbstractActivityC35431hr) this).A03 = (C22330yu) r1.A3I.get();
            ((AbstractActivityC35431hr) this).A02 = (AnonymousClass12T) r1.AJZ.get();
            ((AbstractActivityC35431hr) this).A0D = (C16490p7) r1.ACJ.get();
            ((AbstractActivityC35431hr) this).A0M = (C23000zz) r1.ALg.get();
            ((AbstractActivityC35431hr) this).A05 = (C22700zV) r1.AMN.get();
            ((AbstractActivityC35431hr) this).A09 = (C255419u) r1.AJp.get();
            ((AbstractActivityC35431hr) this).A0A = (AnonymousClass15Q) r1.A6g.get();
            ((AbstractActivityC35431hr) this).A0E = (C22100yW) r1.A3g.get();
            ((AbstractActivityC35431hr) this).A0B = (C25641Ae) r1.A6i.get();
            ((AbstractActivityC35431hr) this).A0J = (C16630pM) r1.AIc.get();
            ((AbstractActivityC35431hr) this).A0H = (C244215l) r1.A8y.get();
            ((AbstractActivityC35431hr) this).A0N = C18000rk.A00(r1.A4v);
            ((AbstractActivityC35431hr) this).A08 = (AnonymousClass1A6) r1.A4u.get();
            this.A03 = r1.A3J();
        }
    }

    @Override // X.AbstractActivityC35431hr, X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Boolean bool;
        super.onCreate(bundle);
        setTitle(R.string.kept_messages);
        ((AbstractActivityC13750kH) this).A0R.A03(this.A05);
        C20030v6 r4 = this.A03;
        AbstractC14640lm r5 = ((AbstractActivityC35431hr) this).A0I;
        AnonymousClass009.A05(r5);
        long longExtra = getIntent().getLongExtra("keptMessageCount", 0);
        C615630v r3 = new C615630v();
        r3.A02 = 3;
        boolean z = true;
        r3.A03 = 1;
        r3.A04 = Long.valueOf((long) C32341c0.A00(r4.A01, r4.A04, r5));
        r3.A05 = Long.valueOf(longExtra);
        if (C15380n4.A0J(r5)) {
            C15600nX r2 = r4.A05;
            GroupJid groupJid = (GroupJid) r5;
            boolean A0C = r2.A0C(groupJid);
            boolean A0D = r2.A0D(groupJid);
            if (!A0C || !A0D) {
                z = false;
            }
            r3.A01 = Boolean.valueOf(z);
            bool = Boolean.TRUE;
        } else {
            bool = Boolean.FALSE;
        }
        r3.A00 = bool;
        r3.A06 = r4.A02.A06(r5.getRawString());
        r4.A07.A07(r3);
        setContentView(R.layout.kept_messages);
        ListView A2e = A2e();
        A2e.setFastScrollEnabled(false);
        A2e.setScrollbarFadingEnabled(true);
        A2e.setOnScrollListener(((AbstractActivityC35431hr) this).A0Q);
        A2e.addHeaderView(getLayoutInflater().inflate(R.layout.conversation_row_kept_folder_tip_header, (ViewGroup) A2e, false));
        A2f(((AbstractActivityC35431hr) this).A07);
        this.A01 = (ScrollView) findViewById(R.id.empty_view);
        this.A02 = (WaTextView) findViewById(R.id.search_no_matches);
        this.A00 = (ProgressBar) findViewById(R.id.progress);
        A2p();
    }

    @Override // X.AbstractActivityC35431hr, X.AbstractActivityC13750kH, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        ((AbstractActivityC13750kH) this).A0R.A04(this.A05);
        this.A01 = null;
        this.A02 = null;
        this.A00 = null;
        super.onDestroy();
    }
}
