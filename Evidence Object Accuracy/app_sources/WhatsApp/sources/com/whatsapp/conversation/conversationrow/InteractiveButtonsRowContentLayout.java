package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* loaded from: classes2.dex */
public class InteractiveButtonsRowContentLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;
    public final View A04;
    public final View A05;
    public final LinearLayout.LayoutParams A06;
    public final LinearLayout.LayoutParams A07;
    public final LinearLayout A08;
    public final List A09;
    public final View[] A0A;
    public final TextEmojiLabel[] A0B;

    public InteractiveButtonsRowContentLayout(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.whatsapp.TextEmojiLabel[] */
    /* JADX WARN: Multi-variable type inference failed */
    public InteractiveButtonsRowContentLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A0B = r2;
        this.A0A = r3;
        this.A09 = C12960it.A0l();
        this.A06 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        this.A07 = new LinearLayout.LayoutParams(-1, -2);
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.interactive_message_button_content, this);
        this.A08 = (LinearLayout) AnonymousClass028.A0D(this, R.id.buttons_container);
        this.A04 = AnonymousClass028.A0D(this, R.id.button_div_horizontal);
        this.A05 = AnonymousClass028.A0D(this, R.id.button_div_vertical);
        View[] viewArr = {AnonymousClass028.A0D(this, R.id.button_1), AnonymousClass028.A0D(this, R.id.button_2)};
        TextEmojiLabel[] textEmojiLabelArr = {AnonymousClass028.A0D(this, R.id.button_content_1), AnonymousClass028.A0D(this, R.id.button_content_2)};
    }

    public InteractiveButtonsRowContentLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r20.size() >= 3) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1OY r19, java.util.List r20) {
        /*
        // Method dump skipped, instructions count: 304
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.InteractiveButtonsRowContentLayout.A00(X.1OY, java.util.List):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setDisplayButtonsInVertical(boolean z) {
        this.A02 = z;
    }
}
