package com.whatsapp.conversation.conversationrow;

import X.AbstractC36671kL;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C004802e;
import X.C12970iu;
import X.C16120oU;
import X.C252018m;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.VerifiedBusinessInfoDialogFragment;

/* loaded from: classes2.dex */
public class VerifiedBusinessInfoDialogFragment extends Hilt_VerifiedBusinessInfoDialogFragment {
    public AnonymousClass12P A00;
    public AnonymousClass018 A01;
    public AnonymousClass19M A02;
    public C16120oU A03;
    public C252018m A04;

    public static VerifiedBusinessInfoDialogFragment A00(Integer num, String str) {
        VerifiedBusinessInfoDialogFragment verifiedBusinessInfoDialogFragment = new VerifiedBusinessInfoDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("message", str);
        if (num != null) {
            A0D.putInt("system_action", num.intValue());
        }
        verifiedBusinessInfoDialogFragment.A0U(A0D);
        return verifiedBusinessInfoDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        A03();
        String string = ((AnonymousClass01E) this).A05.getString("message");
        int i = ((AnonymousClass01E) this).A05.getInt("system_action");
        C004802e A0O = C12970iu.A0O(this);
        A0O.A0A(AbstractC36671kL.A05(A0p(), this.A02, string));
        A0O.A0B(true);
        A0O.A00(R.string.learn_more, new DialogInterface.OnClickListener(i) { // from class: X.3KQ
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                VerifiedBusinessInfoDialogFragment verifiedBusinessInfoDialogFragment = VerifiedBusinessInfoDialogFragment.this;
                int i3 = this.A00;
                Uri A03 = verifiedBusinessInfoDialogFragment.A04.A03("26000089");
                if (i3 == 46) {
                    AnonymousClass30C r1 = new AnonymousClass30C();
                    r1.A00 = C12970iu.A0g();
                    r1.A01 = 14;
                    verifiedBusinessInfoDialogFragment.A03.A07(r1);
                }
                verifiedBusinessInfoDialogFragment.A00.A06(verifiedBusinessInfoDialogFragment.A0p(), C12970iu.A0B(A03));
                verifiedBusinessInfoDialogFragment.A1B();
            }
        });
        C12970iu.A1K(A0O, this, 30, R.string.ok);
        return A0O.create();
    }
}
