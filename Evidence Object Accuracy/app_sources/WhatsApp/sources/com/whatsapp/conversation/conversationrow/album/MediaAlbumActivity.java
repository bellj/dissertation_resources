package com.whatsapp.conversation.conversationrow.album;

import X.AbstractActivityC13750kH;
import X.AbstractC005102i;
import X.AbstractC13890kV;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC18860tB;
import X.AbstractC33331dp;
import X.AbstractC454421p;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass03M;
import X.AnonymousClass07F;
import X.AnonymousClass0B5;
import X.AnonymousClass0QL;
import X.AnonymousClass109;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass116;
import X.AnonymousClass11G;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.AnonymousClass13H;
import X.AnonymousClass190;
import X.AnonymousClass193;
import X.AnonymousClass198;
import X.AnonymousClass19D;
import X.AnonymousClass19I;
import X.AnonymousClass19J;
import X.AnonymousClass19K;
import X.AnonymousClass19L;
import X.AnonymousClass19M;
import X.AnonymousClass1A6;
import X.AnonymousClass1AB;
import X.AnonymousClass1AP;
import X.AnonymousClass1XB;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2ZN;
import X.AnonymousClass3O2;
import X.AnonymousClass41V;
import X.AnonymousClass4ZH;
import X.C103014q4;
import X.C1108056y;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15270mq;
import X.C15330mx;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16170oZ;
import X.C16210od;
import X.C16590pI;
import X.C16630pM;
import X.C17070qD;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C18850tA;
import X.C20000v3;
import X.C20040v7;
import X.C20710wC;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22100yW;
import X.C22180yf;
import X.C22330yu;
import X.C22370yy;
import X.C22670zS;
import X.C22700zV;
import X.C22710zW;
import X.C23000zz;
import X.C231510o;
import X.C239613r;
import X.C239913u;
import X.C240514a;
import X.C242114q;
import X.C244215l;
import X.C245115u;
import X.C249317l;
import X.C252018m;
import X.C252718t;
import X.C252818u;
import X.C253018w;
import X.C253118x;
import X.C253218y;
import X.C255419u;
import X.C26501Ds;
import X.C27131Gd;
import X.C32731ce;
import X.C35361hh;
import X.C35421hp;
import X.C41691tw;
import X.C45041zy;
import X.C53412eN;
import X.C53982fu;
import X.C852941z;
import X.C857744c;
import X.C88054Ec;
import X.ViewTreeObserver$OnPreDrawListenerC101864oD;
import android.animation.TimeInterpolator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ListView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.gesture.VerticalSwipeDismissBehavior;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class MediaAlbumActivity extends AbstractActivityC13750kH implements AbstractC13890kV, AnonymousClass03M {
    public Bundle A00;
    public C239613r A01;
    public AnonymousClass12T A02;
    public C22330yu A03;
    public AnonymousClass10S A04;
    public C22700zV A05;
    public AnonymousClass1AP A06;
    public AnonymousClass2ZN A07;
    public C35421hp A08;
    public AnonymousClass1A6 A09;
    public C255419u A0A;
    public C22100yW A0B;
    public C22180yf A0C;
    public C15270mq A0D;
    public C231510o A0E;
    public C15330mx A0F;
    public C244215l A0G;
    public C16630pM A0H;
    public C88054Ec A0I;
    public AnonymousClass12U A0J;
    public C23000zz A0K;
    public AnonymousClass01H A0L;
    public boolean A0M;
    public final AnonymousClass2Dn A0N;
    public final C27131Gd A0O;
    public final AbstractC18860tB A0P;
    public final AbstractC33331dp A0Q;
    public final HashSet A0R;
    public final HashSet A0S;

    @Override // X.AnonymousClass03M
    public void AS8(AnonymousClass0QL r1) {
    }

    public MediaAlbumActivity() {
        this(0);
        this.A0R = new HashSet();
        this.A0S = new HashSet();
        this.A0P = new C35361hh(this);
        this.A0O = new C852941z(this);
        this.A0N = new AnonymousClass41V(this);
        this.A0Q = new C857744c(this);
    }

    public MediaAlbumActivity(int i) {
        this.A0M = false;
        A0R(new C103014q4(this));
    }

    @Override // X.AbstractActivityC13760kI, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0M) {
            this.A0M = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            ((AbstractActivityC13750kH) this).A0K = (C16590pI) r1.AMg.get();
            this.A0Z = (AnonymousClass13H) r1.ABY.get();
            this.A0j = (C253018w) r1.AJS.get();
            this.A0o = (C253118x) r1.AAW.get();
            this.A0U = (C16120oU) r1.ANE.get();
            this.A0a = (C245115u) r1.A7s.get();
            ((AbstractActivityC13750kH) this).A05 = (C18850tA) r1.AKx.get();
            ((AbstractActivityC13750kH) this).A03 = (C16170oZ) r1.AM4.get();
            this.A0p = (C26501Ds) r1.AML.get();
            ((AbstractActivityC13750kH) this).A0B = (C21270x9) r1.A4A.get();
            ((AbstractActivityC13750kH) this).A07 = (C15550nR) r1.A45.get();
            super.A0Q = (C20040v7) r1.AAK.get();
            this.A0m = (C252018m) r1.A7g.get();
            ((AbstractActivityC13750kH) this).A09 = (C15610nY) r1.AMe.get();
            this.A0d = (C17070qD) r1.AFC.get();
            this.A0f = (C239913u) r1.AC1.get();
            ((AbstractActivityC13750kH) this).A0O = (C253218y) r1.AAF.get();
            ((AbstractActivityC13750kH) this).A0M = (C15650ng) r1.A4m.get();
            super.A0R = (AnonymousClass12H) r1.AC5.get();
            ((AbstractActivityC13750kH) this).A08 = (AnonymousClass190) r1.AIZ.get();
            this.A0i = (AnonymousClass12F) r1.AJM.get();
            super.A0P = (C20000v3) r1.AAG.get();
            ((AbstractActivityC13750kH) this).A06 = (AnonymousClass116) r1.A3z.get();
            this.A0T = (AnonymousClass193) r1.A6S.get();
            super.A0S = (C242114q) r1.AJq.get();
            ((AbstractActivityC13750kH) this).A0L = (C15890o4) r1.AN1.get();
            this.A0V = (C20710wC) r1.A8m.get();
            this.A0n = (AnonymousClass198) r1.A0J.get();
            this.A0h = (C240514a) r1.AJL.get();
            this.A0W = (AnonymousClass11G) r1.AKq.get();
            ((AbstractActivityC13750kH) this).A0A = (AnonymousClass10T) r1.A47.get();
            this.A0Y = (C22370yy) r1.AB0.get();
            this.A0c = (C22710zW) r1.AF7.get();
            ((AbstractActivityC13750kH) this).A04 = (C14650lo) r1.A2V.get();
            this.A0l = (AnonymousClass1AB) r1.AKI.get();
            this.A0X = (AnonymousClass109) r1.AI8.get();
            ((AbstractActivityC13750kH) this).A0N = (C15600nX) r1.A8x.get();
            ((AbstractActivityC13750kH) this).A02 = (C16210od) r1.A0Y.get();
            ((AbstractActivityC13750kH) this).A0H = (AnonymousClass19D) r1.ABo.get();
            ((AbstractActivityC13750kH) this).A0I = (AnonymousClass11P) r1.ABp.get();
            ((AbstractActivityC13750kH) this).A0E = (AnonymousClass19I) r1.A4a.get();
            this.A0b = (AnonymousClass19J) r1.ACA.get();
            ((AbstractActivityC13750kH) this).A0C = (AnonymousClass19K) r1.AFh.get();
            this.A0q = (AnonymousClass19L) r1.A6l.get();
            this.A01 = (C239613r) r1.AI9.get();
            this.A0J = (AnonymousClass12U) r1.AJd.get();
            this.A0E = (C231510o) r1.AHO.get();
            this.A0I = new C88054Ec();
            this.A0C = (C22180yf) r1.A5U.get();
            this.A04 = (AnonymousClass10S) r1.A46.get();
            this.A03 = (C22330yu) r1.A3I.get();
            this.A02 = (AnonymousClass12T) r1.AJZ.get();
            this.A0K = (C23000zz) r1.ALg.get();
            this.A05 = (C22700zV) r1.AMN.get();
            this.A0B = (C22100yW) r1.A3g.get();
            this.A0A = (C255419u) r1.AJp.get();
            this.A0H = (C16630pM) r1.AIc.get();
            this.A0G = (C244215l) r1.A8y.get();
            this.A0L = C18000rk.A00(r1.A4v);
            this.A09 = (AnonymousClass1A6) r1.A4u.get();
            this.A06 = (AnonymousClass1AP) r1.A4Z.get();
        }
    }

    @Override // X.AbstractActivityC13750kH
    public void A2m(int i) {
        C15330mx r1;
        super.A2m(i);
        if (i == 0 && (r1 = this.A0F) != null) {
            r1.A00(false);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2o() {
        /*
            r12 = this;
            X.1hp r0 = r12.A08
            java.util.List r0 = r0.A00
            if (r0 == 0) goto L_0x00d2
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00d2
            X.1hp r0 = r12.A08
            java.util.List r0 = r0.A00
            java.util.Iterator r3 = r0.iterator()
            r10 = 0
            r11 = 0
            r2 = 0
        L_0x0017:
            boolean r0 = r3.hasNext()
            r9 = 1
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r3.next()
            X.0mz r0 = (X.AbstractC15340mz) r0
            byte r1 = r0.A0y
            if (r1 != r9) goto L_0x002b
            int r2 = r2 + 1
            goto L_0x0017
        L_0x002b:
            r0 = 3
            if (r1 != r0) goto L_0x0017
            int r11 = r11 + 1
            goto L_0x0017
        L_0x0031:
            X.1hp r0 = r12.A08
            java.util.List r0 = r0.A00
            java.lang.Object r8 = r0.get(r10)
            X.0mz r8 = (X.AbstractC15340mz) r8
            if (r11 != 0) goto L_0x008f
            X.018 r5 = r12.A01
            r4 = 2131755209(0x7f1000c9, float:1.914129E38)
            long r0 = (long) r2
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
        L_0x0049:
            r3[r10] = r2
            java.lang.String r4 = r5.A0I(r3, r4, r0)
        L_0x004f:
            long r2 = r8.A0I
            long r0 = java.lang.System.currentTimeMillis()
            int r0 = X.C38121nY.A00(r0, r2)
            if (r0 == 0) goto L_0x0084
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r4)
            java.lang.String r1 = "  "
            r3.append(r1)
            r0 = 2131887451(0x7f12055b, float:1.940951E38)
            java.lang.String r0 = r12.getString(r0)
            r3.append(r0)
            r3.append(r1)
            X.018 r2 = r12.A01
            long r0 = r8.A0I
            java.lang.String r0 = X.C38131nZ.A09(r2, r0)
            r3.append(r0)
            java.lang.String r4 = r3.toString()
        L_0x0084:
            X.02i r0 = r12.A1U()
            X.AnonymousClass009.A05(r0)
            r0.A0H(r4)
            return
        L_0x008f:
            if (r2 != 0) goto L_0x009e
            X.018 r5 = r12.A01
            r4 = 2131755212(0x7f1000cc, float:1.9141297E38)
            long r0 = (long) r11
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            goto L_0x0049
        L_0x009e:
            r7 = 2131890016(0x7f120f60, float:1.9414712E38)
            r0 = 2
            java.lang.Object[] r6 = new java.lang.Object[r0]
            X.018 r5 = r12.A01
            r4 = 2131755209(0x7f1000c9, float:1.914129E38)
            long r0 = (long) r2
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r3[r10] = r2
            java.lang.String r0 = r5.A0I(r3, r4, r0)
            r6[r10] = r0
            X.018 r5 = r12.A01
            r4 = 2131755212(0x7f1000cc, float:1.9141297E38)
            long r2 = (long) r11
            java.lang.Object[] r1 = new java.lang.Object[r9]
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            r1[r10] = r0
            java.lang.String r0 = r5.A0I(r1, r4, r2)
            r6[r9] = r0
            java.lang.String r4 = r12.getString(r7, r6)
            goto L_0x004f
        L_0x00d2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity.A2o():void");
    }

    public final void A2p(AbstractC15340mz r5) {
        AnonymousClass009.A0B("should not reply to systemMessage", !(r5 instanceof AnonymousClass1XB));
        AbstractC14640lm A0B = r5.A0B();
        AnonymousClass009.A05(A0B);
        this.A06.A00.put(A0B, r5);
        Intent intent = new Intent(this, Conversation.class);
        intent.putExtra("jid", A0B.getRawString());
        intent.putExtra("extra_quoted_message_row_id", r5.A0F);
        ((ActivityC13790kL) this).A00.A07(this, intent);
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AnonymousClass03M
    public AnonymousClass0QL AOi(Bundle bundle, int i) {
        return new C53982fu(this, ((AbstractActivityC13750kH) this).A0M, getIntent().getLongArrayExtra("message_ids"));
    }

    @Override // X.AnonymousClass03M
    public /* bridge */ /* synthetic */ void AS2(AnonymousClass0QL r3, Object obj) {
        List list = (List) obj;
        if (list == null || !list.isEmpty()) {
            this.A08.A00(list);
            A2o();
            A2e().getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101864oD(this));
            return;
        }
        finish();
    }

    @Override // android.app.Activity
    public void finishAfterTransition() {
        if (getIntent().hasExtra("start_index")) {
            A0j(new C53412eN(this));
        }
        super.finishAfterTransition();
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 2 && i2 == -1) {
            Collection A2h = A2h();
            if (!A2h.isEmpty()) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                C32731ce r6 = null;
                if (AnonymousClass4ZH.A00(((ActivityC13810kN) this).A0C, A07)) {
                    AnonymousClass009.A05(intent);
                    r6 = (C32731ce) intent.getParcelableExtra("status_distribution");
                }
                ArrayList arrayList = new ArrayList(A2h);
                Collections.sort(arrayList, new C45041zy());
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((AbstractActivityC13750kH) this).A03.A06(this.A01, r6, (AbstractC15340mz) it.next(), A07);
                }
                if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                    A2a(A07);
                } else {
                    ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, ((AbstractActivityC13750kH) this).A07.A0B((AbstractC14640lm) A07.get(0))));
                }
            } else {
                Log.w("mediaalbum/forward/failed");
                ((ActivityC13810kN) this).A05.A07(R.string.message_forward_failed, 0);
            }
            A2j();
        }
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int length;
        if (AbstractC454421p.A00) {
            getWindow().requestFeature(12);
            getWindow().requestFeature(13);
            AutoTransition autoTransition = new AutoTransition();
            autoTransition.setDuration(220L);
            autoTransition.setInterpolator((TimeInterpolator) new AccelerateDecelerateInterpolator());
            getWindow().setSharedElementEnterTransition(autoTransition);
        }
        this.A00 = bundle;
        super.onCreate(bundle);
        A0c();
        setContentView(R.layout.media_album);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        A1e(toolbar);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        this.A04.A03(this.A0O);
        super.A0R.A03(this.A0P);
        this.A03.A03(this.A0N);
        this.A0G.A03(this.A0Q);
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(1792);
            C41691tw.A02(this, R.color.primary);
            getWindow().addFlags(134217728);
        }
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        if (A01 == null) {
            A1U.A0A(R.string.you);
        } else {
            A1U.A0I(((AbstractActivityC13750kH) this).A09.A04(((AbstractActivityC13750kH) this).A07.A0B(A01)));
        }
        this.A08 = new C35421hp(this);
        ListView A2e = A2e();
        A2e.setFastScrollEnabled(false);
        A2e.setScrollbarFadingEnabled(true);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.actionbar_height));
        A2e.addHeaderView(frameLayout, null, false);
        FrameLayout frameLayout2 = new FrameLayout(this);
        A2e.addFooterView(frameLayout2, null, false);
        View findViewById = findViewById(R.id.title_background);
        AnonymousClass028.A0h(A2e, new AnonymousClass07F(frameLayout, frameLayout2, findViewById, this) { // from class: X.4rq
            public final /* synthetic */ View A00;
            public final /* synthetic */ View A01;
            public final /* synthetic */ View A02;
            public final /* synthetic */ MediaAlbumActivity A03;

            {
                this.A03 = r4;
                this.A00 = r1;
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass07F
            public final C018408o AMH(View view, C018408o r8) {
                MediaAlbumActivity mediaAlbumActivity = this.A03;
                View view2 = this.A00;
                View view3 = this.A01;
                View view4 = this.A02;
                int A06 = r8.A06() + mediaAlbumActivity.getResources().getDimensionPixelSize(R.dimen.actionbar_height);
                int A03 = r8.A03();
                view2.setPadding(0, 0, 0, A06);
                view3.setPadding(0, 0, 0, A03);
                view4.setPadding(0, 0, 0, A06);
                return r8;
            }
        });
        AnonymousClass2ZN r0 = new AnonymousClass2ZN(AnonymousClass00T.A00(this, R.color.primary));
        this.A07 = r0;
        A1U.A0C(r0);
        A2e.setOnScrollListener(new AnonymousClass3O2(frameLayout, A2e, this, AnonymousClass00T.A00(this, R.color.media_view_footer_background), AnonymousClass00T.A00(this, R.color.primary), AnonymousClass00T.A00(this, R.color.primary)));
        A2f(this.A08);
        if (i >= 21) {
            View findViewById2 = findViewById(R.id.background);
            VerticalSwipeDismissBehavior verticalSwipeDismissBehavior = new VerticalSwipeDismissBehavior(this);
            verticalSwipeDismissBehavior.A05 = new C1108056y(findViewById2, findViewById, toolbar, this);
            ((AnonymousClass0B5) A2e.getLayoutParams()).A00(verticalSwipeDismissBehavior);
        }
        long[] longArrayExtra = getIntent().getLongArrayExtra("message_ids");
        if (longArrayExtra == null || (length = longArrayExtra.length) == 0) {
            finish();
            return;
        }
        A1U.A0H(((ActivityC13830kP) this).A01.A0I(new Object[]{Integer.valueOf(length)}, R.plurals.notification_new_message_from_multiple_contacts_1, (long) length));
        A0W().A02(this);
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A04.A04(this.A0O);
        super.A0R.A04(this.A0P);
        this.A03.A04(this.A0N);
        this.A0G.A04(this.A0Q);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            A0a();
        }
        return true;
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ListView A2e = A2e();
        bundle.putInt("top_index", A2e.getFirstVisiblePosition());
        int i = 0;
        View childAt = A2e.getChildAt(0);
        if (childAt != null) {
            i = childAt.getTop() - A2e.getPaddingTop();
        }
        bundle.putInt("top_offset", i);
    }
}
