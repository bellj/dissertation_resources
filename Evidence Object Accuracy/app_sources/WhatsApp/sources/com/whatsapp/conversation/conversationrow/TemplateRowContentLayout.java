package com.whatsapp.conversation.conversationrow;

import X.AbstractC15340mz;
import X.AbstractC28491Nn;
import X.AbstractC28551Oa;
import X.AbstractC28871Pi;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass12P;
import X.AnonymousClass1OY;
import X.AnonymousClass2GE;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass4KO;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C22160yd;
import X.C22180yf;
import X.C25911Bh;
import X.C27531Hw;
import X.C28891Pk;
import X.C30761Ys;
import X.C38271no;
import X.C38311ns;
import X.C52252aV;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* loaded from: classes2.dex */
public class TemplateRowContentLayout extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public AnonymousClass12P A01;
    public TextEmojiLabel A02;
    public TextEmojiLabel A03;
    public AnonymousClass1OY A04;
    public C22160yd A05;
    public C22180yf A06;
    public C14850m9 A07;
    public C25911Bh A08;
    public AnonymousClass2P7 A09;
    public boolean A0A;
    public final List A0B;

    public TemplateRowContentLayout(Context context) {
        super(context);
        A00();
        this.A0B = C12960it.A0l();
        A01(context);
    }

    public TemplateRowContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0B = C12960it.A0l();
        A01(context);
    }

    public TemplateRowContentLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0B = C12960it.A0l();
        A01(context);
    }

    public TemplateRowContentLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A07 = C12960it.A0S(A00);
            this.A05 = (C22160yd) A00.AC4.get();
            this.A06 = (C22180yf) A00.A5U.get();
            this.A01 = C12980iv.A0W(A00);
            this.A08 = (C25911Bh) A00.A77.get();
        }
    }

    public final void A01(Context context) {
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.template_message_content, this);
        this.A03 = C12970iu.A0U(this, R.id.top_message);
        this.A02 = C12970iu.A0U(this, R.id.bottom_message);
        this.A00 = findViewById(R.id.button_divider);
        List<TextView> list = this.A0B;
        list.add(findViewById(R.id.action_btn_1));
        list.add(findViewById(R.id.action_btn_2));
        list.add(findViewById(R.id.action_btn_3));
        for (TextView textView : list) {
            C27531Hw.A06(textView);
        }
    }

    public void A02(AnonymousClass1OY r22) {
        TextEmojiLabel textEmojiLabel;
        int A00;
        int i;
        int i2;
        CharSequence A01;
        this.A04 = r22;
        AbstractC28871Pi r5 = (AbstractC28871Pi) r22.getFMessage();
        C28891Pk AH7 = r5.AH7();
        String str = AH7.A02;
        String str2 = AH7.A01;
        int i3 = 8;
        if (!TextUtils.isEmpty(str)) {
            r22.setMessageText(str2, this.A03, r22.getFMessage());
            setupContentView(this.A03);
            this.A02.A07 = null;
            this.A03.setVisibility(0);
            r22.A17(this.A02, r22.getFMessage(), str, false, true);
            this.A02.setTextSize(AnonymousClass1OY.A02(r22.getResources(), ((AbstractC28551Oa) r22).A0K, -1));
            textEmojiLabel = this.A02;
            A00 = r22.getSecondaryTextColor();
        } else {
            r22.setMessageText(str2, this.A02, r22.getFMessage());
            setupContentView(this.A02);
            this.A03.setVisibility(8);
            this.A02.setTextSize(r22.getTextFontSize());
            textEmojiLabel = this.A02;
            A00 = AnonymousClass00T.A00(r22.getContext(), R.color.conversation_template_top_message_text_color);
        }
        textEmojiLabel.setTextColor(A00);
        List list = r5.AH7().A04;
        boolean z = false;
        int i4 = 0;
        for (TextView textView : this.A0B) {
            if (list == null || i4 >= list.size() || list.get(i4) == null || ((C30761Ys) list.get(i4)).A03 == 1) {
                i = 8;
            } else {
                C30761Ys r6 = (C30761Ys) list.get(i4);
                AnonymousClass4KO r52 = r22.A1d;
                AbstractC15340mz fMessage = r22.getFMessage();
                if (C38311ns.A01(this.A07, r6)) {
                    A01 = r6.A04;
                } else {
                    Context context = getContext();
                    if (r6.A03 == 3) {
                        i2 = R.drawable.ic_action_call;
                    } else {
                        boolean A06 = this.A05.A06(r6);
                        i2 = R.drawable.ic_link_action;
                        if (A06) {
                            i2 = R.drawable.ic_action_copy;
                        }
                    }
                    boolean isEnabled = isEnabled();
                    int i5 = R.color.conversation_row_button_text_disabled;
                    if (isEnabled) {
                        i5 = R.color.link_color;
                    }
                    Drawable A012 = AnonymousClass2GE.A01(context, i2, i5);
                    A012.setAlpha(204);
                    A01 = C52252aV.A01(textView.getPaint(), A012, r6.A04);
                    if (this.A05.A07(r6)) {
                        A01 = Uri.parse(r6.A05).getQueryParameter("cta_display_name");
                    }
                }
                textView.setText(A01);
                if (!this.A05.A07(r6) || r6.A06.get() != 1 || System.currentTimeMillis() - fMessage.A0I <= C38271no.A00) {
                    C12980iv.A14(getResources(), textView, R.color.link_color);
                    C12990iw.A1C(textView, this, r6, r52, 12);
                } else {
                    textView.setClickable(false);
                    C12980iv.A14(getResources(), textView, R.color.conversation_row_button_text_disabled);
                }
                z = true;
                i = 0;
            }
            textView.setVisibility(i);
            i4++;
        }
        View view = this.A00;
        if (z) {
            i3 = 0;
        }
        view.setVisibility(i3);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A09;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A09 = r0;
        }
        return r0.generatedComponent();
    }

    public TextEmojiLabel getContentTextView() {
        return this.A03.getVisibility() == 0 ? this.A03 : this.A02;
    }

    public TextEmojiLabel getFooterTextView() {
        return this.A02;
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        AnonymousClass1OY r0 = this.A04;
        if (r0 != null) {
            A02(r0);
        }
    }

    public static void setupContentView(TextEmojiLabel textEmojiLabel) {
        textEmojiLabel.setLongClickable(AbstractC28491Nn.A07(textEmojiLabel));
    }
}
