package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass2P7;
import X.AnonymousClass4KN;
import X.C12960it;
import X.C12990iw;
import X.C27531Hw;
import X.C30761Ys;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0201000_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class TemplateQuickReplyButtonsLayout extends ViewGroup implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public List A01;
    public boolean A02;
    public View[] A03;
    public View[] A04;

    public TemplateQuickReplyButtonsLayout(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A03 = new View[3];
        this.A04 = new View[3];
        ViewGroup.inflate(context, R.layout.template_quick_reply_buttons, this);
    }

    public TemplateQuickReplyButtonsLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A03 = new View[3];
        this.A04 = new View[3];
        ViewGroup.inflate(context, R.layout.template_quick_reply_buttons, this);
    }

    public TemplateQuickReplyButtonsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        this.A03 = new View[3];
        this.A04 = new View[3];
        ViewGroup.inflate(context, R.layout.template_quick_reply_buttons, this);
    }

    public TemplateQuickReplyButtonsLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if (A03(r10, r6) != false) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(int r10) {
        /*
            r9 = this;
            android.util.DisplayMetrics r1 = X.C12990iw.A0K(r9)
            r5 = 1
            r0 = 1098907648(0x41800000, float:16.0)
            float r0 = android.util.TypedValue.applyDimension(r5, r0, r1)
            int r6 = (int) r0
            r4 = 0
            r7 = 0
        L_0x000e:
            android.view.View[] r2 = r9.A03
            r0 = r2[r7]
            if (r0 == 0) goto L_0x003a
            android.view.View[] r8 = r9.A04
            r0 = r8[r7]
            if (r0 == 0) goto L_0x003a
            r0 = r2[r7]
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x003a
            r3 = r2[r7]
            int r0 = r6 << 1
            int r1 = r10 - r0
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r4)
            r3.measure(r1, r0)
            r0 = r8[r7]
            X.C12970iu.A1F(r0)
        L_0x003a:
            int r7 = r7 + 1
            r0 = 3
            if (r7 < r0) goto L_0x000e
            boolean r0 = r9.A02(r10, r6)
            if (r0 != 0) goto L_0x004c
            boolean r0 = r9.A03(r10, r6)
            r1 = 0
            if (r0 == 0) goto L_0x004d
        L_0x004c:
            r1 = 1
        L_0x004d:
            java.util.List r0 = r9.A01
            int r3 = r0.size()
            if (r1 == 0) goto L_0x0056
            int r3 = r3 - r5
        L_0x0056:
            r2 = r2[r4]
            if (r2 == 0) goto L_0x0078
            r0 = 1093664768(0x41300000, float:11.0)
            int r1 = X.C12970iu.A03(r2, r0)
            android.widget.TextView r2 = (android.widget.TextView) r2
            float r0 = r2.getTextSize()
            int r0 = (int) r0
            int r1 = r1 << 1
            int r1 = r1 + r0
        L_0x006a:
            int r1 = r1 * r3
            if (r1 == 0) goto L_0x0074
            r0 = 1090519040(0x41000000, float:8.0)
            int r0 = X.C12970iu.A03(r9, r0)
            int r1 = r1 + r0
        L_0x0074:
            X.C12980iv.A1A(r9, r10, r1)
            return r1
        L_0x0078:
            r1 = 0
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout.A00(int):int");
    }

    public void A01(AnonymousClass4KN r9, List list) {
        boolean z;
        View view;
        View view2;
        ViewOnClickCListenerShape1S0201000_I1 viewOnClickCListenerShape1S0201000_I1;
        int i;
        int i2;
        ArrayList A0l = C12960it.A0l();
        if (list != null) {
            for (int i3 = 0; i3 < list.size() && A0l.size() < 3; i3++) {
                if (((C30761Ys) list.get(i3)).A03 == 1) {
                    A0l.add(list.get(i3));
                }
            }
        }
        this.A01 = A0l;
        int i4 = !A0l.isEmpty();
        setVisibility(C12960it.A02(i4));
        if (i4 != 0) {
            int i5 = 0;
            do {
                if (this.A01.size() > i5) {
                    z = true;
                    AnonymousClass009.A0E(C12990iw.A1Y(i5, 3));
                    View[] viewArr = this.A03;
                    if (viewArr[i5] == null) {
                        if (i5 != 0) {
                            i2 = R.id.quick_reply_btn_2;
                            if (i5 != 1) {
                                if (i5 == 2) {
                                    i2 = R.id.quick_reply_btn_3;
                                }
                                C27531Hw.A06((TextView) viewArr[i5]);
                            }
                        } else {
                            i2 = R.id.quick_reply_btn_1;
                        }
                        viewArr[i5] = findViewById(i2);
                        C27531Hw.A06((TextView) viewArr[i5]);
                    }
                    view = viewArr[i5];
                } else {
                    z = false;
                    view = this.A03[i5];
                }
                TextView textView = (TextView) view;
                if (z) {
                    AnonymousClass009.A0E(C12990iw.A1Y(i5, 3));
                    View[] viewArr2 = this.A04;
                    if (viewArr2[i5] == null) {
                        if (i5 != 0) {
                            i = R.id.quick_reply_btn_background_2;
                            if (i5 != 1) {
                                if (i5 == 2) {
                                    i = R.id.quick_reply_btn_background_3;
                                }
                                View view3 = viewArr2[i5];
                                AnonymousClass009.A03(view3);
                                view3.setBackground(AnonymousClass00T.A04(getContext(), R.drawable.balloon_incoming_normal_stkr));
                            }
                        } else {
                            i = R.id.quick_reply_btn_background_1;
                        }
                        viewArr2[i5] = findViewById(i);
                        View view3 = viewArr2[i5];
                        AnonymousClass009.A03(view3);
                        view3.setBackground(AnonymousClass00T.A04(getContext(), R.drawable.balloon_incoming_normal_stkr));
                    }
                    view2 = viewArr2[i5];
                } else {
                    view2 = this.A04[i5];
                }
                if (!(textView == null || view2 == null)) {
                    textView.setVisibility(C12990iw.A0A(z));
                    view2.setVisibility(C12990iw.A0A(z));
                }
                if (z) {
                    AnonymousClass009.A03(textView);
                    AnonymousClass009.A03(view2);
                    boolean z2 = ((C30761Ys) this.A01.get(i5)).A01;
                    textView.setText(((C30761Ys) this.A01.get(i5)).A04);
                    textView.setSelected(z2);
                    if (!z2) {
                        viewOnClickCListenerShape1S0201000_I1 = new ViewOnClickCListenerShape1S0201000_I1(this, r9, i5, 1);
                    } else {
                        viewOnClickCListenerShape1S0201000_I1 = null;
                    }
                    view2.setOnClickListener(viewOnClickCListenerShape1S0201000_I1);
                    view2.setContentDescription(((C30761Ys) this.A01.get(i5)).A04);
                    view2.setClickable(!z2);
                    view2.setLongClickable(true);
                }
                i5++;
            } while (i5 < 3);
        }
    }

    public final boolean A02(int i, int i2) {
        if (this.A01.size() <= 1) {
            return false;
        }
        View[] viewArr = this.A03;
        int i3 = (i >> 1) - (i2 << 1);
        if (viewArr[0].getMeasuredWidth() > i3 || viewArr[1].getMeasuredWidth() > i3) {
            return false;
        }
        return true;
    }

    public final boolean A03(int i, int i2) {
        if (A02(i, i2) || this.A01.size() <= 2) {
            return false;
        }
        View[] viewArr = this.A03;
        int i3 = (i / 2) - (i2 << 1);
        if (viewArr[1].getMeasuredWidth() > i3 || viewArr[2].getMeasuredWidth() > i3) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0025, code lost:
        if (r22 == false) goto L_0x0027;
     */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r27, int r28, int r29, int r30, int r31) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout.onLayout(boolean, int, int, int, int):void");
    }
}
