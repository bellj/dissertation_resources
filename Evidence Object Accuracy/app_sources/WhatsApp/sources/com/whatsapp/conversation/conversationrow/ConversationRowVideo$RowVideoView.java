package com.whatsapp.conversation.conversationrow;

import X.AbstractC65123If;
import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass03X;
import X.AnonymousClass2GE;
import X.AnonymousClass2P7;
import X.AnonymousClass3GD;
import X.AnonymousClass42J;
import X.AnonymousClass42K;
import X.AnonymousClass42L;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C16150oX;
import X.C92994Ym;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConversationRowVideo$RowVideoView extends AnonymousClass03X implements AnonymousClass004 {
    public int A00;
    public int A01;
    public Paint A02;
    public RectF A03;
    public Shader A04;
    public Drawable A05;
    public AbstractC65123If A06;
    public AnonymousClass2P7 A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;

    public ConversationRowVideo$RowVideoView(Context context) {
        super(context, null);
        if (!this.A09) {
            this.A09 = true;
            generatedComponent();
        }
        this.A02 = C12960it.A0A();
        this.A03 = C12980iv.A0K();
        A01();
    }

    public ConversationRowVideo$RowVideoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A02 = C12960it.A0A();
        this.A03 = C12980iv.A0K();
        A01();
    }

    public ConversationRowVideo$RowVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A09) {
            this.A09 = true;
            generatedComponent();
        }
        this.A02 = C12960it.A0A();
        this.A03 = C12980iv.A0K();
        A01();
    }

    public ConversationRowVideo$RowVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A09) {
            this.A09 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        Drawable A00;
        boolean z = this.A0A;
        Context context = getContext();
        if (z) {
            A00 = C92994Ym.A01(context);
        } else {
            A00 = C92994Ym.A00(context);
        }
        this.A05 = A00;
        if (this.A0B) {
            Context context2 = getContext();
            boolean z2 = this.A0A;
            int i = R.color.bubble_color_incoming_pressed;
            if (z2) {
                i = R.color.bubble_color_outgoing_pressed;
            }
            AnonymousClass2GE.A04(A00, AnonymousClass00T.A00(context2, i));
        }
    }

    public final void A01() {
        C16150oX r2;
        AbstractC65123If r1;
        C16150oX r0;
        int A00 = AnonymousClass3GD.A00(getContext());
        AbstractC65123If r02 = this.A06;
        if (r02 == null || (r0 = r02.A00) == null) {
            r2 = null;
        } else {
            r2 = new C16150oX(r0);
        }
        if (this.A08 && this.A0C) {
            r1 = new AnonymousClass42J(A00);
            this.A06 = r1;
        } else if (this.A0C) {
            r1 = new AnonymousClass42K(A00);
            this.A06 = r1;
        } else {
            r1 = new AnonymousClass42L(A00, this.A0D);
            this.A06 = r1;
        }
        if (r2 != null) {
            r1.A00 = r2;
        }
    }

    public void A02(int i, int i2, boolean z) {
        int i3;
        int i4 = this.A01;
        if (i4 <= 0 || (i3 = this.A00) <= 0 || z) {
            hashCode();
            this.A01 = i;
            i4 = i;
            this.A00 = i2;
            i3 = i2;
        }
        this.A06.A09(i4, i3);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    public int getRowWidth() {
        return this.A06.A03();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode()) {
            float dimension = getResources().getDimension(R.dimen.conversation_row_video_bottom_padding);
            int width = getWidth();
            Paint paint = this.A02;
            paint.setColor(1711276032);
            paint.setShader(this.A04);
            paint.setStyle(Paint.Style.FILL);
            RectF rectF = this.A03;
            rectF.set(0.0f, C12990iw.A03(this) - ((dimension * 4.0f) / 3.0f), (float) width, C12990iw.A03(this));
            canvas.drawRect(rectF, paint);
            if (this.A05 != null && !this.A08) {
                RectF rectF2 = new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
                this.A05.setBounds(new Rect(Math.round(rectF2.left), Math.round(rectF2.top), Math.round(rectF2.right), Math.round(rectF2.bottom)));
                this.A05.draw(canvas);
            }
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (!isInEditMode()) {
            if (getDrawable() == null || (getDrawable() instanceof ColorDrawable)) {
                i3 = this.A01;
                i4 = this.A00;
            } else {
                i3 = getDrawable().getIntrinsicWidth();
                i4 = getDrawable().getIntrinsicHeight();
            }
            AbstractC65123If r0 = this.A06;
            r0.A09(i3, i4);
            Pair A07 = r0.A07(i, i2);
            setMeasuredDimension(C12960it.A05(A07.first), C12960it.A05(A07.second));
        } else if (this.A0C) {
            super.onMeasure(i, i2);
        } else {
            setMeasuredDimension(600, 600);
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (!isInEditMode()) {
            float f = (float) i2;
            this.A04 = new LinearGradient(0.0f, f - ((getResources().getDimension(R.dimen.conversation_row_video_bottom_padding) * 4.0f) / 3.0f), 0.0f, f, 0, -16777216, Shader.TileMode.CLAMP);
        }
    }

    public void setFrameDrawable(Drawable drawable) {
        this.A05 = drawable;
    }

    public void setFullWidth(boolean z) {
        this.A08 = z;
        A01();
    }

    public void setIsOutgoing(boolean z) {
        this.A0A = z;
    }

    public void setKeepRatio(boolean z) {
        this.A0C = z;
        A01();
    }

    public void setPortraitPreviewEnabled(boolean z) {
        this.A0D = z;
        A01();
    }
}
