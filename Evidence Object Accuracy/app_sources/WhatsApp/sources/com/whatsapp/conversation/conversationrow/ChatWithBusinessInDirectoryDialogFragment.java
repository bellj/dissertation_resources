package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass028;
import X.AnonymousClass11I;
import X.AnonymousClass12P;
import X.AnonymousClass17R;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C250918b;
import X.C252018m;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;
import java.util.Random;

/* loaded from: classes2.dex */
public class ChatWithBusinessInDirectoryDialogFragment extends Hilt_ChatWithBusinessInDirectoryDialogFragment implements View.OnClickListener {
    public AnonymousClass12P A00;
    public C250918b A01;
    public AnonymousClass11I A02;
    public AnonymousClass17R A03;
    public C252018m A04;
    public boolean A05;

    public static ChatWithBusinessInDirectoryDialogFragment A00(boolean z) {
        ChatWithBusinessInDirectoryDialogFragment chatWithBusinessInDirectoryDialogFragment = new ChatWithBusinessInDirectoryDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putBoolean("arg_conversation_stared_by_me", z);
        chatWithBusinessInDirectoryDialogFragment.A0U(A0D);
        return chatWithBusinessInDirectoryDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        this.A05 = A03().getBoolean("arg_conversation_stared_by_me");
        View A0N = C12980iv.A0N(A01(), R.layout.layout_chat_with_business_in_directory_dialog);
        boolean z = this.A05;
        int i = R.string.biz_chat_with_business_in_directory_dialog_message;
        if (z) {
            i = R.string.consumer_chat_with_business_in_directory_dialog_message_v2;
        }
        C12960it.A0I(A0N, R.id.message).setText(i);
        View A0D = AnonymousClass028.A0D(A0N, R.id.title);
        if (this.A05) {
            A0D.setVisibility(8);
        }
        View A0D2 = AnonymousClass028.A0D(A0N, R.id.btn_negative_vertical);
        View A0D3 = AnonymousClass028.A0D(A0N, R.id.btn_negative_horizontal);
        View A0D4 = AnonymousClass028.A0D(A0N, R.id.btn_positive);
        if (this.A05) {
            A0D2.setVisibility(8);
        } else {
            A0D3.setVisibility(4);
        }
        A0D4.setOnClickListener(this);
        A0D3.setOnClickListener(this);
        A0D2.setOnClickListener(this);
        C004802e A0K = C12960it.A0K(this);
        A0K.setView(A0N);
        A0K.A0B(true);
        return A0K.create();
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btn_negative_horizontal) {
            this.A00.Ab9(A01(), this.A04.A04("security-and-privacy", "how-to-select-a-location-when-looking-for-businesses-nearby"));
        } else if (id == R.id.btn_negative_vertical) {
            C250918b r2 = this.A01;
            r2.A00 = 9;
            Random random = r2.A02;
            if (random == null) {
                random = new Random();
                r2.A02 = random;
            }
            r2.A01 = Long.toHexString(random.nextLong());
            A01();
            A01();
            throw C12970iu.A0z();
        }
        A1B();
    }
}
