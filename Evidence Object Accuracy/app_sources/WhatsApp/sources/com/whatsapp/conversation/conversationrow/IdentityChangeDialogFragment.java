package com.whatsapp.conversation.conversationrow;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass1XB;
import X.C12960it;
import X.C12970iu;
import X.C14840m8;
import X.C15370n3;
import X.C18770sz;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.IdentityChangeDialogFragment;

/* loaded from: classes2.dex */
public class IdentityChangeDialogFragment extends Hilt_IdentityChangeDialogFragment {
    public C18770sz A00;
    public C14840m8 A01;
    public AbstractC14440lR A02;

    public static SecurityNotificationDialogFragment A00(AnonymousClass1XB r4) {
        IdentityChangeDialogFragment identityChangeDialogFragment = new IdentityChangeDialogFragment();
        Bundle A0D = C12970iu.A0D();
        AbstractC14640lm r1 = r4.A0z.A00;
        AnonymousClass009.A05(r1);
        AbstractC14640lm A0B = r4.A0B();
        if (A0B != null) {
            r1 = A0B;
        }
        A0D.putString("participant_jid", r1.getRawString());
        identityChangeDialogFragment.A0U(A0D);
        return identityChangeDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        A03();
        String string = ((AnonymousClass01E) this).A05.getString("participant_jid");
        AbstractC14640lm A01 = AbstractC14640lm.A01(string);
        AnonymousClass009.A06(A01, C12960it.A0d(string, C12960it.A0k("IdentityChangeDialogFragment/onCreateDialog/invalid remote resource jid=")));
        C15370n3 A0B = ((SecurityNotificationDialogFragment) this).A02.A0B(A01);
        AlertDialog.Builder builder = new AlertDialog.Builder(A0p());
        boolean A05 = this.A01.A05();
        int i = R.string.identity_change_info;
        if (A05) {
            i = R.string.identity_change_info_md;
        }
        AlertDialog.Builder neutralButton = builder.setMessage(A1K(A0B, i)).setNegativeButton(R.string.ok, (DialogInterface.OnClickListener) null).setNeutralButton(R.string.learn_more, new IDxCListenerShape4S0200000_2_I1(A0B, 6, this));
        boolean A052 = this.A01.A05();
        int i2 = R.string.identity_change_verify;
        if (A052) {
            i2 = R.string.verify_code;
        }
        return neutralButton.setPositiveButton(i2, new DialogInterface.OnClickListener(string) { // from class: X.4gW
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                IdentityChangeDialogFragment identityChangeDialogFragment = IdentityChangeDialogFragment.this;
                identityChangeDialogFragment.A0v(C14960mK.A0V(identityChangeDialogFragment.A0p(), this.A01));
            }
        }).create();
    }
}
