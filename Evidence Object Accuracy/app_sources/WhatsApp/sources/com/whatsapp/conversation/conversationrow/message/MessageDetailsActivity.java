package com.whatsapp.conversation.conversationrow.message;

import X.AbstractActivityC13840kQ;
import X.AbstractC005102i;
import X.AbstractC13900kW;
import X.AbstractC14010kh;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15590nW;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC18860tB;
import X.AbstractC33331dp;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass11P;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass12P;
import X.AnonymousClass19D;
import X.AnonymousClass19L;
import X.AnonymousClass19M;
import X.AnonymousClass1AB;
import X.AnonymousClass1IS;
import X.AnonymousClass1J1;
import X.AnonymousClass1OX;
import X.AnonymousClass1OY;
import X.AnonymousClass1Q6;
import X.AnonymousClass1lQ;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2ZS;
import X.AnonymousClass3O1;
import X.AnonymousClass41W;
import X.AnonymousClass42D;
import X.AnonymousClass4ZH;
import X.C103034q6;
import X.C14330lG;
import X.C14650lo;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C16170oZ;
import X.C16590pI;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20870wS;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C22330yu;
import X.C22670zS;
import X.C22810zg;
import X.C239613r;
import X.C239913u;
import X.C242014p;
import X.C244215l;
import X.C245716a;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26501Ds;
import X.C27131Gd;
import X.C27671Iq;
import X.C30041Vv;
import X.C32731ce;
import X.C35181hO;
import X.C36871km;
import X.C38121nY;
import X.C38211ni;
import X.C39091pH;
import X.C39931qm;
import X.C41791uA;
import X.C47412Ap;
import X.C52782bg;
import X.C52822bk;
import X.C60802yf;
import X.C71273cf;
import X.C857844d;
import X.ViewTreeObserver$OnGlobalLayoutListenerC101554ni;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public class MessageDetailsActivity extends ActivityC13790kL implements AbstractC14010kh, AbstractC13900kW {
    public long A00;
    public BaseAdapter A01;
    public ListView A02;
    public C239613r A03;
    public C16170oZ A04;
    public C14650lo A05;
    public C22330yu A06;
    public C15550nR A07;
    public AnonymousClass10S A08;
    public C15610nY A09;
    public AnonymousClass10T A0A;
    public AnonymousClass1J1 A0B;
    public C21270x9 A0C;
    public AnonymousClass1OY A0D;
    public AnonymousClass1OX A0E;
    public AnonymousClass19D A0F;
    public AnonymousClass11P A0G;
    public C16590pI A0H;
    public C15650ng A0I;
    public AnonymousClass12H A0J;
    public C22810zg A0K;
    public C245716a A0L;
    public C242014p A0M;
    public C244215l A0N;
    public PollVoterViewModel A0O;
    public AbstractC15340mz A0P;
    public C239913u A0Q;
    public AbstractC15850o0 A0R;
    public AnonymousClass12F A0S;
    public AnonymousClass1AB A0T;
    public C26501Ds A0U;
    public AnonymousClass19L A0V;
    public boolean A0W;
    public final AnonymousClass2Dn A0X;
    public final C27131Gd A0Y;
    public final AbstractC18860tB A0Z;
    public final AbstractC33331dp A0a;
    public final Runnable A0b;
    public final ArrayList A0c;

    @Override // X.AbstractActivityC13840kQ
    public int A1m() {
        return 703931041;
    }

    public MessageDetailsActivity() {
        this(0);
        this.A0c = new ArrayList();
        this.A0Z = new C35181hO(this);
        this.A0Y = new C36871km(this);
        this.A0X = new AnonymousClass41W(this);
        this.A0a = new C857844d(this);
        this.A0b = new RunnableBRunnable0Shape5S0100000_I0_5(this, 12);
    }

    public MessageDetailsActivity(int i) {
        this.A0W = false;
        A0R(new C103034q6(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0W) {
            this.A0W = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0H = (C16590pI) r1.AMg.get();
            this.A03 = (C239613r) r1.AI9.get();
            this.A04 = (C16170oZ) r1.AM4.get();
            this.A0U = (C26501Ds) r1.AML.get();
            this.A0C = (C21270x9) r1.A4A.get();
            this.A07 = (C15550nR) r1.A45.get();
            this.A0K = (C22810zg) r1.AHI.get();
            this.A09 = (C15610nY) r1.AMe.get();
            this.A08 = (AnonymousClass10S) r1.A46.get();
            this.A0I = (C15650ng) r1.A4m.get();
            this.A0J = (AnonymousClass12H) r1.AC5.get();
            this.A0S = (AnonymousClass12F) r1.AJM.get();
            this.A0R = (AbstractC15850o0) r1.ANA.get();
            this.A0Q = (C239913u) r1.AC1.get();
            this.A06 = (C22330yu) r1.A3I.get();
            this.A0A = (AnonymousClass10T) r1.A47.get();
            this.A0M = (C242014p) r1.A0O.get();
            this.A0L = (C245716a) r1.AJR.get();
            this.A05 = (C14650lo) r1.A2V.get();
            this.A0T = (AnonymousClass1AB) r1.AKI.get();
            this.A0F = (AnonymousClass19D) r1.ABo.get();
            this.A0N = (C244215l) r1.A8y.get();
            this.A0G = (AnonymousClass11P) r1.ABp.get();
            this.A0V = (AnonymousClass19L) r1.A6l.get();
        }
    }

    @Override // X.AbstractActivityC13840kQ
    public AnonymousClass1Q6 A1n() {
        AnonymousClass1Q6 A1n = super.A1n();
        A1n.A03 = true;
        A1n.A00 = 8;
        A1n.A04 = true;
        return A1n;
    }

    public final void A2e() {
        byte b;
        ArrayList arrayList = this.A0c;
        arrayList.clear();
        this.A00 = Long.MAX_VALUE;
        ConcurrentHashMap concurrentHashMap = this.A0K.A00(this.A0P).A00;
        if (concurrentHashMap.size() == 0) {
            AbstractC14640lm r1 = this.A0P.A0z.A00;
            if (r1 instanceof UserJid) {
                concurrentHashMap.put(r1, new C39931qm(0, 0, 0));
            }
        }
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        for (Map.Entry entry : concurrentHashMap.entrySet()) {
            C39931qm r2 = (C39931qm) entry.getValue();
            arrayList.add(new AnonymousClass1lQ(r2, (UserJid) entry.getKey()));
            long A01 = r2.A01(5);
            long A012 = r2.A01(13);
            long A013 = r2.A01(8);
            if (A01 != 0) {
                this.A00 = Math.min(this.A00, A01);
                i++;
            }
            if (A012 != 0) {
                this.A00 = Math.min(this.A00, A012);
                i3++;
            }
            if (A013 != 0) {
                this.A00 = Math.min(this.A00, A013);
                i2++;
            }
        }
        AbstractC15340mz r10 = this.A0P;
        AbstractC14640lm r12 = r10.A0z.A00;
        if (C15380n4.A0J(r12) || C15380n4.A0F(r12)) {
            int i4 = r10.A0A;
            if (i2 < i4 && (((b = r10.A0y) == 2 && r10.A08 == 1) || C30041Vv.A0I(b))) {
                arrayList.add(new AnonymousClass42D(i4 - i2, 8));
            }
            if (i3 < i4) {
                arrayList.add(new AnonymousClass42D(i4 - i3, 13));
            }
            if (i < i4) {
                arrayList.add(new AnonymousClass42D(i4 - i, 5));
            }
        }
        Collections.sort(arrayList, new C71273cf(this));
        BaseAdapter baseAdapter = this.A01;
        if (baseAdapter != null) {
            baseAdapter.notifyDataSetChanged();
        }
        A2f();
    }

    public final void A2f() {
        ListView listView = this.A02;
        Runnable runnable = this.A0b;
        listView.removeCallbacks(runnable);
        long j = this.A00;
        if (j != Long.MAX_VALUE) {
            this.A02.postDelayed(runnable, (C38121nY.A01(j) - System.currentTimeMillis()) + 1000);
        }
    }

    @Override // X.AbstractC14010kh
    public AnonymousClass1J1 ABb() {
        return this.A0E.A01(this);
    }

    @Override // X.AbstractC13900kW
    public AnonymousClass1AB AGx() {
        return this.A0T;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 2) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1 && intent != null) {
            List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
            C32731ce r3 = null;
            if (AnonymousClass4ZH.A00(((ActivityC13810kN) this).A0C, A07)) {
                AnonymousClass009.A05(intent);
                r3 = (C32731ce) intent.getParcelableExtra("status_distribution");
            }
            this.A04.A06(this.A03, r3, this.A0P, A07);
            if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                A2a(A07);
            } else {
                ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, this.A07.A0B((AbstractC14640lm) A07.get(0))));
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass1OY A02;
        BaseAdapter r3;
        String str;
        A1w("on_create");
        A1b(9);
        requestWindowFeature(9);
        super.onCreate(bundle);
        C16590pI r5 = this.A0H;
        C26501Ds r4 = this.A0U;
        C15550nR r14 = this.A07;
        AnonymousClass018 r32 = ((ActivityC13830kP) this).A01;
        C39091pH r12 = new C39091pH(this.A05, r14, this.A0A, r5, r32, r4, C39091pH.A00(((ActivityC13830kP) this).A05));
        C14850m9 r8 = ((ActivityC13810kN) this).A0C;
        AbstractC14440lR r11 = ((ActivityC13830kP) this).A05;
        C15450nH r42 = ((ActivityC13810kN) this).A06;
        C21270x9 r52 = this.A0C;
        this.A0E = new AnonymousClass1OX(A0V(), r42, r52, this.A0F, this.A0G, r8, this.A0Q, this.A0T, r11, r12);
        AnonymousClass1IS A022 = C38211ni.A02(getIntent());
        if (A022 != null) {
            this.A0P = this.A0I.A0K.A03(A022);
        }
        boolean z = this.A0P instanceof C27671Iq;
        int i = R.string.message_details;
        if (z) {
            i = R.string.poll_details;
        }
        setTitle(i);
        A1U().A0M(true);
        setContentView(R.layout.message_details);
        AbstractC005102i A1U = A1U();
        ColorDrawable colorDrawable = new ColorDrawable(AnonymousClass00T.A00(this, R.color.primary));
        A1U.A0C(colorDrawable);
        A1U.A0O(false);
        Intent intent = getIntent();
        if (intent == null) {
            str = "intent_is_null";
        } else {
            this.A0B = this.A0C.A04(this, "message-details-activity");
            AbstractC15340mz r43 = this.A0P;
            if (r43 == null) {
                C15650ng r53 = this.A0I;
                r43 = r53.A0K.A03(new AnonymousClass1IS(AbstractC14640lm.A01(intent.getStringExtra("key_remote_jid")), intent.getStringExtra("key_id"), true));
                this.A0P = r43;
                if (r43 == null) {
                    str = "message_is_null";
                }
            }
            StringBuilder sb = new StringBuilder("messagedetails/");
            sb.append(r43.A0z);
            Log.i(sb.toString());
            this.A02 = (ListView) findViewById(16908298);
            A2e();
            AbstractC15340mz r33 = this.A0P;
            if (r33 instanceof C27671Iq) {
                PollVoterViewModel pollVoterViewModel = (PollVoterViewModel) new AnonymousClass02A(this).A00(PollVoterViewModel.class);
                this.A0O = pollVoterViewModel;
                pollVoterViewModel.A08.A05(this, new AnonymousClass02B() { // from class: X.3QJ
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        AnonymousClass1OY r1 = MessageDetailsActivity.this.A0D;
                        if (r1 instanceof C60802yf) {
                            for (C63473Bs r0 : ((C60802yf) r1).A09) {
                                r0.A04.setChecked(false);
                            }
                        }
                    }
                });
                this.A0O.A09.A05(this, new AnonymousClass02B() { // from class: X.4t5
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        Pair pair = (Pair) obj;
                        AnonymousClass1OY r1 = MessageDetailsActivity.this.A0D;
                        if (r1 instanceof C60802yf) {
                            ((C60802yf) r1).setAllCheckboxCheckable(C12970iu.A1Y(pair.second));
                        }
                    }
                });
                this.A0O.A0A.A05(this, new AnonymousClass02B() { // from class: X.4t4
                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        Pair pair = (Pair) obj;
                        AnonymousClass1OY r2 = MessageDetailsActivity.this.A0D;
                        if (r2 instanceof C60802yf) {
                            ((C60802yf) r2).A1M((C27671Iq) pair.first, C12970iu.A1Y(pair.second));
                        }
                    }
                });
                A02 = new C60802yf(this, null, this.A0O, (C27671Iq) this.A0P);
            } else {
                A02 = this.A0E.A02(this, null, r33);
            }
            this.A0D = A02;
            A02.setOnLongClickListener(null);
            AnonymousClass1OY r44 = this.A0D;
            r44.A1S = new RunnableBRunnable0Shape5S0100000_I0_5(this, 11);
            r44.A1T = new RunnableBRunnable0Shape5S0100000_I0_5(this, 10);
            ViewGroup viewGroup = (ViewGroup) getLayoutInflater().inflate(R.layout.message_details_header, (ViewGroup) null, false);
            ViewGroup viewGroup2 = (ViewGroup) viewGroup.findViewById(R.id.conversation_row_center);
            viewGroup2.addView(this.A0D, -1, -2);
            Point point = new Point();
            getWindowManager().getDefaultDisplay().getSize(point);
            viewGroup2.measure(View.MeasureSpec.makeMeasureSpec(point.x, 1073741824), View.MeasureSpec.makeMeasureSpec(-2, 0));
            int i2 = point.y >> 1;
            boolean z2 = false;
            if (viewGroup2.getMeasuredHeight() > i2) {
                z2 = true;
                this.A02.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101554ni(this));
            }
            this.A02.addHeaderView(viewGroup, null, false);
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.edge_bottom);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            this.A02.addHeaderView(imageView, null, false);
            View view = new View(this);
            view.setLayoutParams(new AbsListView.LayoutParams(-1, getResources().getDimensionPixelSize(R.dimen.card_v_padding)));
            this.A02.addFooterView(view, null, false);
            AbstractC14640lm r6 = this.A0P.A0z.A00;
            if (C15380n4.A0J(r6) || C15380n4.A0F(r6)) {
                r3 = new C52822bk(this);
                this.A01 = r3;
            } else {
                r3 = new C52782bg(this);
                this.A01 = r3;
            }
            this.A02.setAdapter((ListAdapter) r3);
            Drawable A03 = this.A0R.A03(this.A0R.A06(this, r6));
            if (A03 != null) {
                viewGroup.setBackground(new AnonymousClass2ZS(A03, viewGroup, this));
            } else {
                viewGroup.setBackgroundResource(R.color.conversation_background);
            }
            this.A02.setOnScrollListener(new AnonymousClass3O1(colorDrawable, viewGroup2, this, i2, z2));
            this.A0G.A06();
            this.A08.A03(this.A0Y);
            this.A0J.A03(this.A0Z);
            this.A06.A03(this.A0X);
            this.A0N.A03(this.A0a);
            new AnonymousClass02A(this).A00(MessageDetailsViewModel.class);
            A1v("on_create");
            return;
        }
        A1x(str);
        A1v("on_create");
        A1z(3);
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0B.A00();
        AnonymousClass1OX r1 = this.A0E;
        AnonymousClass1J1 r0 = r1.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass1AB r02 = r1.A01;
        if (r02 != null) {
            r02.A03();
        }
        C39091pH r03 = r1.A0B;
        if (r03 != null) {
            r03.A06();
        }
        this.A0G.A06();
        this.A08.A04(this.A0Y);
        this.A0J.A04(this.A0Z);
        this.A06.A04(this.A0X);
        this.A0N.A04(this.A0a);
        this.A02.removeCallbacks(this.A0b);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A0V.A00();
        if (this.A0G.A0B()) {
            this.A0G.A03();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0G.A0B()) {
            this.A0G.A05();
        }
        AnonymousClass1OY r1 = this.A0D;
        if (r1 instanceof C47412Ap) {
            ((C47412Ap) r1).A1Q();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        long j;
        super.onStart();
        C41791uA r3 = ((AbstractActivityC13840kQ) this).A00.A01.A01;
        AbstractC15340mz r1 = this.A0P;
        AbstractC14640lm r0 = r1.A0z.A00;
        int i = r1.A0A;
        if (r3 != null && (r0 instanceof AbstractC15590nW) && i > 0) {
            if (i <= 32) {
                j = 32;
            } else {
                j = (long) i;
            }
            r3.A04 = Long.valueOf(j);
            r3.A00 = Integer.valueOf(C20870wS.A00(i));
        }
        A1u();
    }
}
