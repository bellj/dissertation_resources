package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass03X;
import X.AnonymousClass2P7;
import X.C13000ix;
import X.C61082zN;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConversationRowDocument$DocumentPreviewView extends AnonymousClass03X implements AnonymousClass004 {
    public C61082zN A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public ConversationRowDocument$DocumentPreviewView(Context context) {
        super(context, null);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        A00();
    }

    public ConversationRowDocument$DocumentPreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        A00();
    }

    public ConversationRowDocument$DocumentPreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        A00();
    }

    public ConversationRowDocument$DocumentPreviewView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        setScaleType(ImageView.ScaleType.MATRIX);
        this.A00 = new C61082zN((int) getResources().getDimension(R.dimen.conversation_row_document_width));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        C61082zN r0 = this.A00;
        ImageView.ScaleType scaleType = getScaleType();
        RectF A05 = r0.A05(i3, i4);
        Matrix matrix = null;
        if (A05 != null) {
            RectF rectF = new RectF(0.0f, 0.0f, (float) i3, (float) i4);
            if (scaleType == ImageView.ScaleType.MATRIX) {
                matrix = C13000ix.A01();
                matrix.setRectToRect(A05, rectF, Matrix.ScaleToFit.FILL);
            } else {
                matrix = C61082zN.A00;
            }
        }
        setImageMatrix(matrix);
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        this.A00.A09(bitmap.getWidth(), bitmap.getHeight());
        super.setImageBitmap(bitmap);
    }
}
