package com.whatsapp.conversation.conversationrow;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass1XB;
import X.C12970iu;
import X.C20660w7;
import X.C245015t;
import X.C30521Xt;
import X.C32241bq;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class DeviceUpdateDialogFragment extends Hilt_DeviceUpdateDialogFragment {
    public C245015t A00;
    public C20660w7 A01;
    public AbstractC14440lR A02;

    public static SecurityNotificationDialogFragment A00(AnonymousClass1XB r5) {
        DeviceUpdateDialogFragment deviceUpdateDialogFragment = new DeviceUpdateDialogFragment();
        Bundle A0D = C12970iu.A0D();
        AbstractC14640lm r4 = r5.A0z.A00;
        AnonymousClass009.A05(r4);
        A0D.putString("chat_jid", r4.getRawString());
        AnonymousClass009.A05(r4);
        AbstractC14640lm A0B = r5.A0B();
        if (A0B != null) {
            r4 = A0B;
        }
        A0D.putString("participant_jid", r4.getRawString());
        if (r5 instanceof C30521Xt) {
            C30521Xt r52 = (C30521Xt) r5;
            A0D.putInt("device_added_count", r52.A00);
            A0D.putInt("device_removed_count", r52.A01);
        } else {
            AnonymousClass009.A0E(r5 instanceof C32241bq);
            A0D.putBoolean("device_update_failure", true);
        }
        deviceUpdateDialogFragment.A0U(A0D);
        return deviceUpdateDialogFragment;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0153, code lost:
        if (r9 != 0) goto L_0x0155;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0027, code lost:
        if (r5 != false) goto L_0x0029;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d9  */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r13) {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.DeviceUpdateDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }
}
