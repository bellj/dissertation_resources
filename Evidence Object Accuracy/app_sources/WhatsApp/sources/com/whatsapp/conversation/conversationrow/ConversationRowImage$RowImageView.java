package com.whatsapp.conversation.conversationrow;

import X.AbstractC65123If;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass03X;
import X.AnonymousClass12F;
import X.AnonymousClass12P;
import X.AnonymousClass2GF;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3GD;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C16150oX;
import X.C26181Ci;
import X.C28141Kv;
import X.C61072zM;
import X.C61092zO;
import X.C61112zQ;
import X.C63643Cj;
import X.C73033fZ;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConversationRowImage$RowImageView extends AnonymousClass03X implements AnonymousClass004 {
    public Drawable A00;
    public C16150oX A01;
    public AbstractC65123If A02;
    public AnonymousClass018 A03;
    public AnonymousClass12F A04;
    public C26181Ci A05;
    public AnonymousClass2P7 A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public final Matrix A0F;
    public final RectF A0G;
    public final RectF A0H;

    public ConversationRowImage$RowImageView(Context context) {
        super(context, null);
        A00();
        this.A0A = false;
        this.A0H = C12980iv.A0K();
        this.A0G = C12980iv.A0K();
        this.A0F = C13000ix.A01();
        A02();
        A03();
    }

    public ConversationRowImage$RowImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A0A = false;
        this.A0H = C12980iv.A0K();
        this.A0G = C12980iv.A0K();
        this.A0F = C13000ix.A01();
        A02();
        A03();
    }

    public ConversationRowImage$RowImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A0A = false;
        this.A0H = C12980iv.A0K();
        this.A0G = C12980iv.A0K();
        this.A0F = C13000ix.A01();
        A02();
        A03();
    }

    public ConversationRowImage$RowImageView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A04 = C12990iw.A0f(A00);
            this.A03 = C12960it.A0R(A00);
            this.A05 = (C26181Ci) A00.A6H.get();
        }
    }

    public final void A01() {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        RectF A05 = this.A02.A05(measuredWidth, measuredHeight);
        RectF rectF = new RectF(0.0f, 0.0f, (float) measuredWidth, (float) measuredHeight);
        if (A05 != null) {
            RectF rectF2 = this.A0G;
            rectF2.set(A05);
            RectF rectF3 = this.A0H;
            rectF3.set(rectF);
            Matrix matrix = this.A0F;
            matrix.setRectToRect(rectF2, rectF3, Matrix.ScaleToFit.FILL);
            setImageMatrix(matrix);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
            r4 = this;
            X.1Ci r0 = r4.A05
            if (r0 == 0) goto L_0x0040
            boolean r1 = r4.A0B
            boolean r0 = r4.A0C
            android.content.Context r2 = r4.getContext()
            if (r1 == 0) goto L_0x0046
            if (r0 == 0) goto L_0x0041
            r0 = 2131231169(0x7f0801c1, float:1.8078411E38)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r2, r0)
            r0 = 2131099842(0x7f0600c2, float:1.7812049E38)
        L_0x001a:
            int r0 = X.AnonymousClass00T.A00(r2, r0)
            X.AnonymousClass009.A05(r1)
            android.graphics.drawable.Drawable r3 = X.AnonymousClass2GE.A04(r1, r0)
        L_0x0025:
            r4.A00 = r3
            boolean r0 = r4.A0A
            if (r0 == 0) goto L_0x0040
            android.content.Context r2 = r4.getContext()
            boolean r1 = r4.A0B
            r0 = 2131099841(0x7f0600c1, float:1.7812047E38)
            if (r1 == 0) goto L_0x0039
            r0 = 2131099843(0x7f0600c3, float:1.781205E38)
        L_0x0039:
            int r0 = X.AnonymousClass00T.A00(r2, r0)
            X.AnonymousClass2GE.A04(r3, r0)
        L_0x0040:
            return
        L_0x0041:
            android.graphics.drawable.Drawable r3 = X.C92994Ym.A01(r2)
            goto L_0x0025
        L_0x0046:
            if (r0 == 0) goto L_0x0053
            r0 = 2131231168(0x7f0801c0, float:1.807841E38)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r2, r0)
            r0 = 2131099840(0x7f0600c0, float:1.7812045E38)
            goto L_0x001a
        L_0x0053:
            android.graphics.drawable.Drawable r3 = X.C92994Ym.A00(r2)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView.A02():void");
    }

    public final void A03() {
        C16150oX r3;
        C63643Cj r1;
        AbstractC65123If r2;
        C16150oX r0;
        int A00 = AnonymousClass3GD.A00(getContext());
        AbstractC65123If r02 = this.A02;
        if (r02 == null || (r0 = r02.A00) == null) {
            r3 = null;
        } else {
            r3 = new C16150oX(r0);
        }
        if (this.A08) {
            r2 = new C61072zM(A00, C12970iu.A0G(AnonymousClass12P.A02(this)).getHeight());
            this.A02 = r2;
        } else if (this.A0E) {
            r2 = new C61092zO(A00);
            this.A02 = r2;
        } else {
            if (this.A0D) {
                r1 = C61112zQ.A04;
            } else {
                r1 = C61112zQ.A03;
            }
            r2 = new C61112zQ(r1, C61112zQ.A02, A00);
            this.A02 = r2;
        }
        if (r3 != null) {
            r2.A00 = r3;
        }
    }

    public void A04(int i, int i2) {
        C16150oX r0 = this.A01;
        r0.A08 = i;
        r0.A06 = i2;
        setImageData(r0);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public int getRowWidth() {
        return this.A02.A03();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        Drawable drawable;
        super.onDraw(canvas);
        if (!isInEditMode()) {
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int A08 = C12980iv.A08(this);
            int A07 = C12980iv.A07(this);
            Context context = getContext();
            AnonymousClass009.A05(context);
            C26181Ci r8 = this.A05;
            if (r8 != null) {
                if (this.A07) {
                    Drawable drawable2 = r8.A01;
                    if (drawable2 == null) {
                        drawable2 = new AnonymousClass2GF(context.getResources().getDrawable(R.drawable.balloon_media_botshade), r8.A02);
                        r8.A01 = drawable2;
                    }
                    if (C28141Kv.A01(this.A03)) {
                        drawable2.setBounds(A08 - drawable2.getIntrinsicWidth(), A07 - drawable2.getIntrinsicHeight(), A08, A07);
                    } else {
                        drawable2.setBounds(paddingLeft, A07 - drawable2.getIntrinsicHeight(), drawable2.getIntrinsicWidth() + paddingLeft, A07);
                    }
                    drawable2.draw(canvas);
                }
                if (!this.A08 && (drawable = this.A00) != null) {
                    drawable.setBounds(paddingLeft, paddingTop, A08, A07);
                    this.A00.draw(canvas);
                }
            }
        }
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        A01();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int A05;
        int A052;
        if (isInEditMode()) {
            A05 = 800;
            A052 = 600;
        } else {
            Pair A07 = this.A02.A07(i, i2);
            A05 = C12960it.A05(A07.first);
            A052 = C12960it.A05(A07.second);
        }
        setMeasuredDimension(A05, A052);
    }

    public void setFullWidth(boolean z) {
        this.A08 = z;
        A03();
    }

    public void setHasLabels(boolean z) {
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        C73033fZ r0;
        if (bitmap == null) {
            r0 = null;
        } else {
            r0 = new C73033fZ(C12960it.A09(this), bitmap, this);
        }
        super.setImageDrawable(r0);
        A01();
    }

    public void setImageData(C16150oX r3) {
        this.A01 = r3;
        this.A02.A00 = new C16150oX(r3);
    }

    public void setOutgoing(boolean z) {
        if (this.A0B != z) {
            this.A0B = z;
            A02();
        }
    }

    public void setPaddingOnTopOnly(boolean z) {
        if (z != this.A0C) {
            this.A0C = z;
            A02();
        }
    }

    public void setPortraitPreviewEnabled(boolean z) {
        this.A0D = z;
        A03();
    }

    public void setTemplateImageRatio(boolean z) {
        this.A0E = z;
        A03();
    }
}
