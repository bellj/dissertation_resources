package com.whatsapp.conversation.conversationrow;

import X.AbstractC14640lm;
import X.AnonymousClass018;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass30Z;
import X.C12970iu;
import X.C15550nR;
import X.C15600nX;
import X.C16120oU;
import X.C252018m;
import android.content.DialogInterface;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class BusinessTransitionInfoDialogFragment extends Hilt_BusinessTransitionInfoDialogFragment {
    public AnonymousClass12P A00;
    public C15550nR A01;
    public AnonymousClass018 A02;
    public C15600nX A03;
    public AnonymousClass19M A04;
    public C16120oU A05;
    public AnonymousClass30Z A06;
    public C252018m A07;

    public static BusinessTransitionInfoDialogFragment A00(AbstractC14640lm r4, String str, int i, int i2) {
        BusinessTransitionInfoDialogFragment businessTransitionInfoDialogFragment = new BusinessTransitionInfoDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("message", str);
        A0D.putInt("transitionId", i);
        A0D.putInt("systemAction", i2);
        if (r4 != null) {
            A0D.putString("jid", r4.getRawString());
        }
        businessTransitionInfoDialogFragment.A0U(A0D);
        return businessTransitionInfoDialogFragment;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    @Override // androidx.fragment.app.DialogFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Dialog A1A(android.os.Bundle r7) {
        /*
            r6 = this;
            android.os.Bundle r2 = r6.A03()
            java.lang.String r0 = "jid"
            java.lang.String r0 = r2.getString(r0)
            X.0lm r5 = X.AbstractC14640lm.A01(r0)
            java.lang.String r0 = "message"
            java.lang.String r4 = r2.getString(r0)
            java.lang.String r0 = "transitionId"
            int r3 = r2.getInt(r0)
            java.lang.String r1 = "systemAction"
            r0 = -1
            int r1 = r2.getInt(r1, r0)
            r0 = 69
            if (r1 != r0) goto L_0x0063
            if (r5 == 0) goto L_0x0063
            X.30Z r2 = new X.30Z
            r2.<init>()
            r6.A06 = r2
            boolean r0 = r5 instanceof X.AbstractC15590nW
            if (r0 == 0) goto L_0x008d
            boolean r0 = X.C15380n4.A0J(r5)
            if (r0 == 0) goto L_0x008d
            X.0nX r0 = r6.A03
            X.0nW r5 = (X.AbstractC15590nW) r5
            int r0 = r0.A00(r5)
            int r0 = X.C49372Km.A00(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.A02 = r0
            r1 = 2
        L_0x004d:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r2.A00 = r0
        L_0x0053:
            X.30Z r2 = r6.A06
            r1 = 2
            if (r3 == r1) goto L_0x005d
            r0 = 3
            r1 = 1
            if (r3 == r0) goto L_0x005d
            r1 = 0
        L_0x005d:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r2.A03 = r0
        L_0x0063:
            X.02e r2 = X.C12970iu.A0O(r6)
            android.content.Context r1 = r6.A0p()
            X.19M r0 = r6.A04
            java.lang.CharSequence r0 = X.AbstractC36671kL.A05(r1, r0, r4)
            r2.A0A(r0)
            r0 = 1
            r2.A0B(r0)
            r1 = 2131889088(0x7f120bc0, float:1.941283E38)
            X.3KO r0 = new X.3KO
            r0.<init>(r3)
            r2.A00(r1, r0)
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            r0 = 17
            X.04S r0 = X.C12990iw.A0O(r2, r6, r0, r1)
            return r0
        L_0x008d:
            boolean r0 = r5 instanceof com.whatsapp.jid.UserJid
            if (r0 == 0) goto L_0x0053
            X.0nR r0 = r6.A01
            com.whatsapp.jid.UserJid r5 = (com.whatsapp.jid.UserJid) r5
            boolean r0 = r0.A0a(r5)
            r1 = 1
            if (r0 == 0) goto L_0x004d
            r1 = 0
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.BusinessTransitionInfoDialogFragment.A1A(android.os.Bundle):android.app.Dialog");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        AnonymousClass30Z r1 = this.A06;
        if (r1 != null) {
            r1.A01 = 0;
            this.A05.A07(r1);
        }
        super.onCancel(dialogInterface);
    }
}
