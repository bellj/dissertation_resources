package com.whatsapp.conversation.conversationrow.components;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass109;
import X.AnonymousClass2GE;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* loaded from: classes2.dex */
public class ViewOnceDownloadProgressView extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass109 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final CircularProgressBar A03;
    public final WaImageView A04;

    public ViewOnceDownloadProgressView(Context context) {
        this(context, null);
    }

    public ViewOnceDownloadProgressView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ViewOnceDownloadProgressView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = (AnonymousClass109) AnonymousClass2P6.A00(generatedComponent()).AI8.get();
        }
        FrameLayout.inflate(context, R.layout.viewonce_circular_download_progress, this);
        this.A04 = C12980iv.A0X(this, R.id.view_once_control_icon);
        CircularProgressBar circularProgressBar = (CircularProgressBar) AnonymousClass028.A0D(this, R.id.view_once_progressbar);
        this.A03 = circularProgressBar;
        circularProgressBar.setMax(100);
        circularProgressBar.A0C = AnonymousClass00T.A00(getContext(), R.color.green_circle_background);
        circularProgressBar.A0B = 0;
    }

    public void A00(int i, int i2, int i3) {
        WaImageView waImageView = this.A04;
        Drawable drawable = null;
        if (i2 != -1) {
            drawable = AnonymousClass2GE.A04(C12970iu.A0C(getContext(), i2), getResources().getColor(i3));
        }
        waImageView.setBackgroundDrawable(drawable);
        waImageView.setImageDrawable(AnonymousClass2GE.A04(C12970iu.A0C(getContext(), i), getResources().getColor(i3)));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }
}
