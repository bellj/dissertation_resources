package com.whatsapp.conversation.conversationrow;

import X.AbstractC36671kL;
import X.AnonymousClass018;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C12970iu;
import X.C14820m6;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C252018m;

/* loaded from: classes2.dex */
public abstract class SecurityNotificationDialogFragment extends Hilt_SecurityNotificationDialogFragment {
    public AnonymousClass12P A00;
    public C15570nT A01;
    public C15550nR A02;
    public C15610nY A03;
    public C14820m6 A04;
    public AnonymousClass018 A05;
    public AnonymousClass19M A06;
    public C252018m A07;

    public CharSequence A1K(C15370n3 r4, int i) {
        String A0F;
        Object[] A1b = C12970iu.A1b();
        AnonymousClass018 r1 = this.A05;
        String A04 = this.A03.A04(r4);
        if (A04 == null) {
            A0F = null;
        } else {
            A0F = r1.A0F(A04);
        }
        return AbstractC36671kL.A05(A0p(), this.A06, C12970iu.A0q(this, A0F, A1b, 0, i));
    }
}
