package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass01E;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass1AT;
import X.C16120oU;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class E2EEDescriptionBottomSheet extends Hilt_E2EEDescriptionBottomSheet {
    public int A00;
    public AnonymousClass12P A01;
    public C16120oU A02;
    public AnonymousClass1AT A03;

    public static E2EEDescriptionBottomSheet A00(int i) {
        E2EEDescriptionBottomSheet e2EEDescriptionBottomSheet = new E2EEDescriptionBottomSheet();
        Bundle bundle = new Bundle();
        bundle.putInt("entry_point", i);
        e2EEDescriptionBottomSheet.A0U(bundle);
        return e2EEDescriptionBottomSheet;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.e2ee_description_bottom_sheet, viewGroup, false);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        int i;
        Bundle bundle2 = ((AnonymousClass01E) this).A05;
        if (!(bundle2 == null || (i = bundle2.getInt("entry_point", -1)) == -1)) {
            if (8 == i) {
                ((TextView) view.findViewById(R.id.e2ee_bottom_sheet_title)).setText(A02().getString(R.string.built_status_private_title));
                ((TextView) view.findViewById(R.id.e2ee_bottom_sheet_summary)).setText(A02().getString(R.string.built_status_private_summary));
            }
            this.A03.A00(i, 1);
            this.A00 = i;
        }
        View A0D = AnonymousClass028.A0D(view, R.id.e2ee_bottom_sheet_learn_more_button);
        View A0D2 = AnonymousClass028.A0D(view, R.id.e2ee_description_close_button);
        A0D.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 39));
        A0D2.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 40));
    }

    @Override // com.whatsapp.RoundedBottomSheetDialogFragment, com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog A1A = super.A1A(bundle);
        A1A.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.4hV
            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                BottomSheetBehavior A00 = BottomSheetBehavior.A00(AnonymousClass0KS.A00((Dialog) dialogInterface, R.id.design_bottom_sheet));
                A00.A0M(3);
                A00.A0N = true;
            }
        });
        return A1A;
    }
}
