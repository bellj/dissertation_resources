package com.whatsapp.conversation.conversationrow.messagerating;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C12980iv;
import X.C15650ng;
import X.C26231Cn;
import X.C26241Co;

/* loaded from: classes2.dex */
public class MessageRatingViewModel extends AnonymousClass015 {
    public boolean A00 = false;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C26241Co A02;
    public final C15650ng A03;
    public final C26231Cn A04;
    public final AbstractC14440lR A05;

    public MessageRatingViewModel(C26241Co r2, C15650ng r3, C26231Cn r4, AbstractC14440lR r5) {
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
        this.A02 = r2;
    }
}
