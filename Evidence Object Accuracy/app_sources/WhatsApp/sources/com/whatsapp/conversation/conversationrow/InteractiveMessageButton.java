package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C14850m9;
import X.C50512Pv;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* loaded from: classes2.dex */
public class InteractiveMessageButton extends FrameLayout implements AnonymousClass004 {
    public C50512Pv A00;
    public AnonymousClass018 A01;
    public C14850m9 A02;
    public AnonymousClass2P7 A03;
    public boolean A04;
    public final TextEmojiLabel A05;
    public final InteractiveButtonsRowContentLayout A06;

    public InteractiveMessageButton(Context context) {
        this(context, null);
    }

    public InteractiveMessageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        LayoutInflater.from(context).inflate(R.layout.interactive_message_button, (ViewGroup) this, true);
        this.A05 = C12970iu.A0T(this, R.id.button_content);
        this.A06 = (InteractiveButtonsRowContentLayout) AnonymousClass028.A0D(this, R.id.buttons_row);
    }

    public InteractiveMessageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass2P6 r2 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r1 = r2.A06;
            this.A02 = C12960it.A0S(r1);
            this.A01 = C12960it.A0R(r1);
            this.A00 = r2.A03();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0137, code lost:
        if (android.text.TextUtils.isEmpty(r2) != false) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x013b, code lost:
        if (r5 != null) goto L_0x013d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x013d, code lost:
        r1 = r5.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0140, code lost:
        if (r1 == 2) goto L_0x0198;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0143, code lost:
        if (r1 != 4) goto L_0x01a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0145, code lost:
        r4.setText(com.whatsapp.R.string.shops_bubble_cta);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x014b, code lost:
        r1 = r5.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x014e, code lost:
        if (r1 == 2) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0151, code lost:
        if (r1 == 4) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0154, code lost:
        if (r1 != 3) goto L_0x01ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0156, code lost:
        r4.setCompoundDrawables(null, null, null, null);
        setMinimumHeight(X.C12960it.A09(r10).getDimensionPixelSize(com.whatsapp.R.dimen.mini_checkout_native_flow_button_height));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0168, code lost:
        setContentDescription(X.C12960it.A0X(getContext(), r4.getText(), new java.lang.Object[1], 0, com.whatsapp.R.string.accessibility_button));
        setOnClickListener(new com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1(r10, 40, r13));
        setLongClickable(true);
        X.AnonymousClass028.A0g(r10, new X.C53492eX(r10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0192, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0193, code lost:
        r4.setCompoundDrawables(null, null, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0198, code lost:
        r11.setMessageText(getContext().getString(com.whatsapp.R.string.product_list_bubble_cta), r4, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01a7, code lost:
        r11.setMessageText(r2, r4, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01aa, code lost:
        if (r5 == null) goto L_0x01ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01ad, code lost:
        r4.A0C(X.AnonymousClass2GF.A00(getContext(), r10.A01, com.whatsapp.R.drawable.ic_format_list_bulleted));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1OY r11, X.AbstractC13890kV r12, X.AbstractC15340mz r13) {
        /*
        // Method dump skipped, instructions count: 450
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.InteractiveMessageButton.A00(X.1OY, X.0kV, X.0mz):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }
}
