package com.whatsapp.conversation.conversationrow;

import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass0OC;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C94034b9;
import X.DialogInterface$OnClickListenerC65583Kf;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRow$ConversationRowDialogFragment;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class ConversationRow$ConversationRowDialogFragment extends Hilt_ConversationRow_ConversationRowDialogFragment {
    public C15570nT A00;
    public C15550nR A01;
    public C15610nY A02;
    public AnonymousClass018 A03;

    public static ConversationRow$ConversationRowDialogFragment A00(AbstractC14640lm r4) {
        ConversationRow$ConversationRowDialogFragment conversationRow$ConversationRowDialogFragment = new ConversationRow$ConversationRowDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", r4.getRawString());
        conversationRow$ConversationRowDialogFragment.A0U(A0D);
        return conversationRow$ConversationRowDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String string = ((AnonymousClass01E) this).A05.getString("jid");
        AbstractC14640lm A01 = AbstractC14640lm.A01(string);
        AnonymousClass009.A06(A01, C12960it.A0d(string, C12960it.A0k("ConversationRow/onCreateDialog/invalid jid=")));
        C15370n3 A00 = C15550nR.A00(this.A01, A01);
        ArrayList A0l = C12960it.A0l();
        if (!A00.A0I()) {
            this.A00.A08();
            A0l.add(new C94034b9(A0p().getString(R.string.add_contact), (int) R.id.menuitem_add_to_contacts));
            A0l.add(new C94034b9(A0p().getString(R.string.add_exist), (int) R.id.menuitem_add_to_existing_contact));
        }
        String A012 = C15610nY.A01(this.A02, A00);
        A0l.add(new C94034b9(C12960it.A0X(A0p(), A012, new Object[1], 0, R.string.message_contact_name), (int) R.id.menuitem_message_contact));
        A0l.add(new C94034b9(C12960it.A0X(A0p(), A012, new Object[1], 0, R.string.voice_call_contact_name), (int) R.id.menuitem_voice_call_contact));
        A0l.add(new C94034b9(C12960it.A0X(A0p(), A012, new Object[1], 0, R.string.video_call_contact_name), (int) R.id.menuitem_video_call_contact));
        C004802e A0S = C12980iv.A0S(A0p());
        ArrayAdapter arrayAdapter = new ArrayAdapter(A0p(), 17367043, A0l);
        DialogInterface$OnClickListenerC65583Kf r1 = new DialogInterface.OnClickListener(A01, A0l) { // from class: X.3Kf
            public final /* synthetic */ AbstractC14640lm A01;
            public final /* synthetic */ List A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConversationRow$ConversationRowDialogFragment conversationRow$ConversationRowDialogFragment = ConversationRow$ConversationRowDialogFragment.this;
                List list = this.A02;
                AbstractC14640lm r2 = this.A01;
                ActivityC000900k A0B = conversationRow$ConversationRowDialogFragment.A0B();
                if (A0B instanceof Conversation) {
                    ((Conversation) A0B).A3P(r2, ((C94034b9) list.get(i)).A00);
                }
            }
        };
        AnonymousClass0OC r0 = A0S.A01;
        r0.A0D = arrayAdapter;
        r0.A05 = r1;
        return A0S.create();
    }
}
