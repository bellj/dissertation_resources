package com.whatsapp.conversation.conversationrow.messagerating;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractView$OnClickListenerC34281fs;
import X.AnonymousClass009;
import X.AnonymousClass028;
import X.AnonymousClass1IS;
import X.AnonymousClass5TG;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.C14900mE;
import X.C15380n4;
import X.EnumC87334Bc;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S1300000_I1;
import com.whatsapp.FAQTextView;
import com.whatsapp.R;
import com.whatsapp.StarRatingBar;
import com.whatsapp.WaTextView;
import com.whatsapp.components.Button;
import com.whatsapp.conversation.conversationrow.messagerating.MessageRatingFragment;

/* loaded from: classes2.dex */
public class MessageRatingFragment extends Hilt_MessageRatingFragment {
    public static final int[] A05 = {R.string.message_rating_1, R.string.message_rating_2, R.string.message_rating_3, R.string.message_rating_4, R.string.message_rating_5};
    public C14900mE A00;
    public EnumC87334Bc A01;
    public MessageRatingViewModel A02;
    public AbstractC14640lm A03;
    public String A04;

    public static MessageRatingFragment A00(EnumC87334Bc r5, AbstractC15340mz r6) {
        MessageRatingFragment messageRatingFragment = new MessageRatingFragment();
        Bundle A0D = C12970iu.A0D();
        AnonymousClass1IS r2 = r6.A0z;
        A0D.putString("chat_jid", C15380n4.A03(r2.A00));
        A0D.putString("message_id", r2.A01);
        A0D.putParcelable("entry_point", r5);
        messageRatingFragment.A0U(A0D);
        return messageRatingFragment;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.message_rating_fragment);
        AbstractView$OnClickListenerC34281fs.A01(AnonymousClass028.A0D(A0F, R.id.close_button), this, 19);
        ((FAQTextView) AnonymousClass028.A0D(A0F, R.id.description)).setEducationTextFromNamedArticle(new SpannableString(A0I(R.string.message_rating_description)), "chats", "controls-when-messaging-businesses");
        StarRatingBar starRatingBar = (StarRatingBar) AnonymousClass028.A0D(A0F, R.id.rating_bar);
        Button button = (Button) AnonymousClass028.A0D(A0F, R.id.submit);
        WaTextView A0N = C12960it.A0N(A0F, R.id.rating_label);
        AbstractView$OnClickListenerC34281fs.A02(button, this, starRatingBar, 37);
        starRatingBar.A01 = new AnonymousClass5TG(A0N, button, this) { // from class: X.3UQ
            public final /* synthetic */ WaTextView A00;
            public final /* synthetic */ Button A01;
            public final /* synthetic */ MessageRatingFragment A02;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
                if (r6 <= 0) goto L_0x0017;
             */
            @Override // X.AnonymousClass5TG
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void AUZ(int r6, boolean r7) {
                /*
                    r5 = this;
                    com.whatsapp.conversation.conversationrow.messagerating.MessageRatingFragment r0 = r5.A02
                    com.whatsapp.components.Button r1 = r5.A01
                    com.whatsapp.WaTextView r4 = r5.A00
                    r3 = 1
                    r2 = 0
                    if (r7 != 0) goto L_0x0014
                    com.whatsapp.conversation.conversationrow.messagerating.MessageRatingViewModel r0 = r0.A02
                    X.016 r0 = r0.A01
                    java.lang.Object r0 = r0.A01()
                    if (r0 != 0) goto L_0x0017
                L_0x0014:
                    r0 = 1
                    if (r6 > 0) goto L_0x0018
                L_0x0017:
                    r0 = 0
                L_0x0018:
                    r1.setEnabled(r0)
                    if (r6 <= 0) goto L_0x002c
                    int[] r1 = com.whatsapp.conversation.conversationrow.messagerating.MessageRatingFragment.A05
                    int r0 = r1.length
                    if (r6 > r0) goto L_0x002c
                    int r6 = r6 - r3
                    r0 = r1[r6]
                    r4.setText(r0)
                    r4.setVisibility(r2)
                    return
                L_0x002c:
                    r0 = 4
                    r4.setVisibility(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3UQ.AUZ(int, boolean):void");
            }
        };
        C12970iu.A1P(A0G(), this.A02.A01, starRatingBar, 39);
        MessageRatingViewModel messageRatingViewModel = this.A02;
        messageRatingViewModel.A05.Ab2(new RunnableBRunnable0Shape1S1200000_I1(this.A03, messageRatingViewModel, this.A04, 14));
        return A0F;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A02 = (MessageRatingViewModel) C13000ix.A02(this).A00(MessageRatingViewModel.class);
        this.A03 = AbstractC14640lm.A01(A03().getString("chat_jid"));
        String string = A03().getString("message_id");
        AnonymousClass009.A05(string);
        this.A04 = string;
        Parcelable parcelable = A03().getParcelable("entry_point");
        AnonymousClass009.A05(parcelable);
        EnumC87334Bc r4 = (EnumC87334Bc) parcelable;
        this.A01 = r4;
        MessageRatingViewModel messageRatingViewModel = this.A02;
        messageRatingViewModel.A05.Ab2(new RunnableBRunnable0Shape1S1300000_I1(messageRatingViewModel, this.A03, r4, this.A04, 5));
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        MessageRatingViewModel messageRatingViewModel = this.A02;
        AbstractC14640lm r3 = this.A03;
        String str = this.A04;
        EnumC87334Bc r4 = this.A01;
        if (!messageRatingViewModel.A00) {
            messageRatingViewModel.A05.Ab2(new RunnableBRunnable0Shape1S1300000_I1(messageRatingViewModel, r3, r4, str, 6));
        }
    }
}
