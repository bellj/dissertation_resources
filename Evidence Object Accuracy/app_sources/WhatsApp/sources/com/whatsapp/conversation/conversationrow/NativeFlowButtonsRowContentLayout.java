package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* loaded from: classes2.dex */
public class NativeFlowButtonsRowContentLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final View A02;
    public final View A03;
    public final LinearLayout A04;
    public final List A05;
    public final View[] A06;
    public final TextEmojiLabel[] A07;

    public NativeFlowButtonsRowContentLayout(Context context) {
        this(context, null);
    }

    public NativeFlowButtonsRowContentLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.whatsapp.TextEmojiLabel[] */
    /* JADX WARN: Multi-variable type inference failed */
    public NativeFlowButtonsRowContentLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A07 = r4;
        this.A06 = r3;
        this.A05 = C12960it.A0l();
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.native_flow_button_content, this);
        this.A04 = (LinearLayout) findViewById(R.id.native_flow_action_buttons_container);
        this.A02 = findViewById(R.id.button_div_horizontal);
        this.A03 = findViewById(R.id.button_div_vertical);
        TextEmojiLabel[] textEmojiLabelArr = {findViewById(R.id.button_content_1), findViewById(R.id.button_content_2)};
        View[] viewArr = {findViewById(R.id.native_flow_action_button_1), findViewById(R.id.native_flow_action_button_2)};
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
