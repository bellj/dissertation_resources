package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass0OC;
import X.AnonymousClass190;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C15380n4;
import X.C94034b9;
import X.DialogInterface$OnClickListenerC65593Kg;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowContact$MessageSharedContactDialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class ConversationRowContact$MessageSharedContactDialogFragment extends Hilt_ConversationRowContact_MessageSharedContactDialogFragment {
    public AnonymousClass190 A00;
    public AnonymousClass018 A01;

    public static ConversationRowContact$MessageSharedContactDialogFragment A00(String str, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        ConversationRowContact$MessageSharedContactDialogFragment conversationRowContact$MessageSharedContactDialogFragment = new ConversationRowContact$MessageSharedContactDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putStringArrayList("jids", C15380n4.A06(arrayList));
        A0D.putStringArrayList("phones", arrayList2);
        A0D.putStringArrayList("labels", arrayList3);
        A0D.putString("business_name", str);
        conversationRowContact$MessageSharedContactDialogFragment.A0U(A0D);
        return conversationRowContact$MessageSharedContactDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String A0d;
        List A07 = C15380n4.A07(UserJid.class, A03().getStringArrayList("jids"));
        ArrayList<String> stringArrayList = ((AnonymousClass01E) this).A05.getStringArrayList("phones");
        ArrayList<String> stringArrayList2 = ((AnonymousClass01E) this).A05.getStringArrayList("labels");
        String string = ((AnonymousClass01E) this).A05.getString("business_name");
        ArrayList A0l = C12960it.A0l();
        if (!(stringArrayList2 == null || stringArrayList == null)) {
            for (int i = 0; i < A07.size(); i++) {
                if (A07.get(i) != null) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append(C12960it.A0X(A0p(), stringArrayList.get(i), C12970iu.A1b(), 0, R.string.message_contact_name));
                    if (TextUtils.isEmpty(stringArrayList2.get(i))) {
                        A0d = "";
                    } else {
                        StringBuilder A0k = C12960it.A0k(" (");
                        A0k.append(stringArrayList2.get(i));
                        A0d = C12960it.A0d(")", A0k);
                    }
                    A0l.add(new C94034b9((UserJid) A07.get(i), C12960it.A0d(A0d, A0h)));
                }
            }
        }
        C004802e A0K = C12960it.A0K(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(A0p(), (int) R.layout.select_phone_dialog_item, A0l);
        DialogInterface$OnClickListenerC65593Kg r1 = new DialogInterface.OnClickListener(string, A0l) { // from class: X.3Kg
            public final /* synthetic */ String A01;
            public final /* synthetic */ List A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ConversationRowContact$MessageSharedContactDialogFragment conversationRowContact$MessageSharedContactDialogFragment = ConversationRowContact$MessageSharedContactDialogFragment.this;
                List list = this.A02;
                String str = this.A01;
                UserJid userJid = ((C94034b9) list.get(i2)).A01;
                if (userJid != null) {
                    conversationRowContact$MessageSharedContactDialogFragment.A00.A01(conversationRowContact$MessageSharedContactDialogFragment.A0p(), userJid, str);
                }
            }
        };
        AnonymousClass0OC r0 = A0K.A01;
        r0.A0D = arrayAdapter;
        r0.A05 = r1;
        return A0K.create();
    }
}
