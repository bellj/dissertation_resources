package com.whatsapp.conversation.conversationrow;

import X.AbstractC116675Wj;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass190;
import X.AnonymousClass29H;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C17220qS;
import X.C253318z;
import X.C628038q;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.backup.google.PromptDialogFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ContactSyncActivity extends ActivityC13790kL implements AnonymousClass29H, AbstractC116675Wj {
    public AnonymousClass190 A00;
    public C253318z A01;
    public C628038q A02;
    public UserJid A03;
    public C17220qS A04;
    public boolean A05;

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
    }

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
    }

    public ContactSyncActivity() {
        this(0);
    }

    public ContactSyncActivity(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 60);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A04 = C12990iw.A0c(A1M);
            this.A01 = (C253318z) A1M.A4B.get();
            this.A00 = (AnonymousClass190) A1M.AIZ.get();
        }
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        if (i == 1 || i == 2) {
            finish();
        }
    }

    @Override // X.AbstractC116675Wj
    public void AUW() {
        this.A02 = null;
        AaN();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r1 != 0) goto L_0x0035;
     */
    @Override // X.AbstractC116675Wj
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AXG(X.C42351v4 r6) {
        /*
            r5 = this;
            r4 = 0
            r5.A02 = r4
            r5.AaN()
            if (r6 == 0) goto L_0x0035
            boolean r0 = r6.A00()
            if (r0 == 0) goto L_0x002d
            r5.finish()
            X.190 r0 = r5.A00
            com.whatsapp.jid.UserJid r1 = r5.A03
            X.0nR r0 = r0.A04
            X.0n3 r1 = r0.A0B(r1)
            X.0mK r0 = new X.0mK
            r0.<init>()
            android.content.Intent r1 = r0.A0g(r5, r1)
            java.lang.String r0 = "ShareContactUtil"
            X.C35741ib.A00(r1, r0)
            r5.startActivity(r1)
            return
        L_0x002d:
            int r1 = r6.A00
            r3 = 1
            r0 = 2131891912(0x7f1216c8, float:1.9418557E38)
            if (r1 == 0) goto L_0x0039
        L_0x0035:
            r3 = 2
            r0 = 2131891911(0x7f1216c7, float:1.9418555E38)
        L_0x0039:
            java.lang.String r1 = r5.getString(r0)
            android.os.Bundle r2 = X.C12970iu.A0D()
            java.lang.String r0 = "dialog_id"
            r2.putInt(r0, r3)
            java.lang.String r0 = "message"
            r2.putCharSequence(r0, r1)
            r1 = 0
            java.lang.String r0 = "cancelable"
            r2.putBoolean(r0, r1)
            r0 = 2131890036(0x7f120f74, float:1.9414752E38)
            java.lang.String r1 = r5.getString(r0)
            java.lang.String r0 = "positive_button"
            r2.putString(r0, r1)
            com.whatsapp.backup.google.PromptDialogFragment r1 = new com.whatsapp.backup.google.PromptDialogFragment
            r1.<init>()
            r1.A0U(r2)
            X.02f r0 = X.C12970iu.A0P(r5)
            r0.A09(r1, r4)
            r0.A02()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.conversationrow.ContactSyncActivity.AXG(X.1v4):void");
    }

    @Override // X.AbstractC116675Wj
    public void AXH() {
        A2P(getString(R.string.loading_spinner));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A03 = ActivityC13790kL.A0V(getIntent(), "user_jid");
        if (!((ActivityC13810kN) this).A07.A0B()) {
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("dialog_id", 1);
            A0D.putCharSequence("message", getString(R.string.something_went_wrong_network_required));
            A0D.putBoolean("cancelable", false);
            A0D.putString("positive_button", getString(R.string.ok));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(A0D);
            C12960it.A16(promptDialogFragment, this);
            return;
        }
        C628038q r0 = this.A02;
        if (r0 != null) {
            r0.A03(true);
        }
        C628038q r1 = new C628038q(this.A01, this, this.A03, this.A04);
        this.A02 = r1;
        C12990iw.A1N(r1, ((ActivityC13830kP) this).A05);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C628038q r1 = this.A02;
        if (r1 != null) {
            r1.A03(true);
            this.A02 = null;
        }
    }
}
