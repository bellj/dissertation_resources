package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass00T;
import X.AnonymousClass2P7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WaveformVisualizerView extends View implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public byte[] A02;
    public float[] A03;
    public final Paint A04;
    public final Path A05;
    public final Rect A06;

    public WaveformVisualizerView(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A06 = new Rect();
        this.A04 = new Paint();
        this.A05 = new Path();
        A00();
    }

    public WaveformVisualizerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        this.A06 = new Rect();
        this.A04 = new Paint();
        this.A05 = new Path();
        A00();
    }

    public WaveformVisualizerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A06 = new Rect();
        this.A04 = new Paint();
        this.A05 = new Path();
        A00();
    }

    public WaveformVisualizerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public final void A00() {
        this.A02 = null;
        Paint paint = this.A04;
        paint.setStrokeWidth(2.0f);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setAntiAlias(true);
        paint.setColor(AnonymousClass00T.A00(getContext(), R.color.white));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        byte[] bArr = this.A02;
        if (bArr != null) {
            float[] fArr = this.A03;
            if (fArr == null || fArr.length < (bArr.length << 2)) {
                this.A03 = new float[bArr.length << 2];
            }
            Rect rect = this.A06;
            rect.set(0, 0, getWidth(), getHeight());
            rect.top += getPaddingTop();
            rect.bottom -= getPaddingBottom();
            rect.left += getPaddingLeft();
            rect.right -= getPaddingRight();
            Path path = this.A05;
            path.reset();
            path.moveTo((float) rect.left, (float) (rect.top + (rect.height() >> 1) + ((((byte) (this.A02[0] + 128)) * (rect.height() >> 1)) >> 7)));
            for (int i = 0; i < this.A02.length - 1; i++) {
                path.lineTo((float) (rect.left + ((rect.width() * i) / (this.A02.length - 1))), (float) (rect.top + (rect.height() >> 1) + ((((byte) (this.A02[i] + 128)) * (rect.height() >> 1)) >> 7)));
            }
            canvas.drawPath(path, this.A04);
        }
    }

    public void setColor(int i) {
        this.A04.setColor(i);
    }
}
