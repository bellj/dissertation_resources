package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C14850m9;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConversationRowAudioPreview extends FrameLayout implements AnonymousClass004 {
    public ImageView A00;
    public ImageView A01;
    public TextView A02;
    public WaveformVisualizerView A03;
    public C14850m9 A04;
    public AnonymousClass2P7 A05;
    public boolean A06;

    public ConversationRowAudioPreview(Context context) {
        super(context);
        A01();
        A02(context);
    }

    public ConversationRowAudioPreview(Context context, C14850m9 r2) {
        super(context);
        A01();
        this.A04 = r2;
        A02(context);
    }

    public ConversationRowAudioPreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
        A02(context);
    }

    public ConversationRowAudioPreview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        A02(context);
    }

    public ConversationRowAudioPreview(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A01();
    }

    public void A00() {
        this.A03.setVisibility(8);
        this.A00.setVisibility(0);
    }

    public void A01() {
        if (!this.A06) {
            this.A06 = true;
            this.A04 = (C14850m9) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.A04.get();
        }
    }

    public final void A02(Context context) {
        TextView textView;
        float f;
        FrameLayout.inflate(context, R.layout.conversation_row_audio_preview, this);
        this.A01 = (ImageView) AnonymousClass028.A0D(this, R.id.picture);
        this.A03 = (WaveformVisualizerView) AnonymousClass028.A0D(this, R.id.visualizer);
        this.A00 = (ImageView) AnonymousClass028.A0D(this, R.id.icon);
        this.A02 = (TextView) AnonymousClass028.A0D(this, R.id.duration);
        boolean A07 = this.A04.A07(1040);
        ImageView imageView = this.A01;
        if (A07) {
            imageView.setImageResource(R.drawable.audio_file_background);
            this.A02.setTypeface(null, 0);
            textView = this.A02;
            f = 10.0f;
        } else {
            imageView.setImageResource(R.drawable.audio_message_thumb);
            this.A02.setTypeface(null, 1);
            textView = this.A02;
            f = 12.0f;
        }
        textView.setTextSize(2, f);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    public void setDuration(String str) {
        this.A02.setText(str);
    }
}
