package com.whatsapp.conversation.conversationrow;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass1ZN;
import X.AnonymousClass2P7;
import X.AnonymousClass3CO;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C27531Hw;
import X.C53552ed;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0201000_I1;
import com.whatsapp.R;
import java.util.List;

/* loaded from: classes2.dex */
public class DynamicButtonsLayout extends ViewGroup implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final List A02;
    public final View[] A03;
    public final View[] A04;

    public DynamicButtonsLayout(Context context) {
        this(context, null);
    }

    public DynamicButtonsLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DynamicButtonsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = new View[3];
        this.A04 = new View[3];
        this.A02 = C12960it.A0l();
        ViewGroup.inflate(context, R.layout.template_quick_reply_buttons, this);
    }

    public static final int A00(View view) {
        if (view == null) {
            return 0;
        }
        int applyDimension = (int) TypedValue.applyDimension(1, 11.0f, C12990iw.A0K(view));
        return Math.max((applyDimension << 1) + ((int) ((TextView) view).getTextSize()), C12970iu.A03(view, 40.0f));
    }

    public int A01(int i) {
        View[] viewArr;
        int i2;
        int applyDimension = (int) TypedValue.applyDimension(1, 16.0f, C12990iw.A0K(this));
        int i3 = 0;
        do {
            viewArr = this.A03;
            if (viewArr[i3] != null) {
                View[] viewArr2 = this.A04;
                if (viewArr2[i3] != null && viewArr[i3].getVisibility() == 0) {
                    viewArr[i3].measure(View.MeasureSpec.makeMeasureSpec(i - (applyDimension << 1), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(0, 0));
                    C12970iu.A1F(viewArr2[i3]);
                }
            }
            i3++;
        } while (i3 < 3);
        int measuredWidth = getMeasuredWidth();
        List list = this.A02;
        boolean z = true;
        if (list.size() != 2 || viewArr[0].getMeasuredWidth() > (i2 = (measuredWidth / 2) - (applyDimension << 1)) || viewArr[1].getMeasuredWidth() > i2) {
            z = false;
        }
        int size = list.size();
        if (z) {
            size--;
        }
        int A00 = A00(viewArr[0]) * size;
        if (A00 != 0) {
            A00 += C12970iu.A03(this, 1.0f);
        }
        C12980iv.A1A(this, i, A00);
        return A00;
    }

    public final View A02(int i) {
        int i2;
        AnonymousClass009.A0E(C12990iw.A1Y(i, 3));
        View[] viewArr = this.A04;
        if (viewArr[i] == null) {
            if (i != 0) {
                i2 = R.id.quick_reply_btn_background_2;
                if (i != 1) {
                    if (i == 2) {
                        i2 = R.id.quick_reply_btn_background_3;
                    }
                    View view = viewArr[i];
                    AnonymousClass009.A03(view);
                    view.setBackground(AnonymousClass00T.A04(getContext(), R.drawable.balloon_incoming_normal_stkr));
                }
            } else {
                i2 = R.id.quick_reply_btn_background_1;
            }
            viewArr[i] = findViewById(i2);
            View view = viewArr[i];
            AnonymousClass009.A03(view);
            view.setBackground(AnonymousClass00T.A04(getContext(), R.drawable.balloon_incoming_normal_stkr));
        }
        return viewArr[i];
    }

    public final View A03(int i) {
        int i2;
        AnonymousClass009.A0E(C12990iw.A1Y(i, 3));
        View[] viewArr = this.A03;
        if (viewArr[i] == null) {
            if (i != 0) {
                i2 = R.id.quick_reply_btn_2;
                if (i != 1) {
                    if (i == 2) {
                        i2 = R.id.quick_reply_btn_3;
                    }
                    C27531Hw.A06((TextView) viewArr[i]);
                }
            } else {
                i2 = R.id.quick_reply_btn_1;
            }
            viewArr[i] = findViewById(i2);
            C27531Hw.A06((TextView) viewArr[i]);
        }
        return viewArr[i];
    }

    public void A04(AnonymousClass3CO r8, List list) {
        boolean z;
        View view;
        View view2;
        List list2 = this.A02;
        list2.clear();
        int min = Math.min(3, list.size());
        for (int i = 0; i < min; i++) {
            list2.add(list.get(i));
        }
        int i2 = 0;
        do {
            if (list2.size() > i2) {
                z = true;
                view = A03(i2);
                view2 = A02(i2);
            } else {
                z = false;
                view = this.A04[i2];
                view2 = this.A03[i2];
            }
            if (!(view == null || view2 == null)) {
                int i3 = 8;
                view.setVisibility(C12990iw.A0A(z));
                if (z) {
                    i3 = 0;
                }
                view2.setVisibility(i3);
            }
            if (z) {
                TextView textView = (TextView) A03(i2);
                textView.setVisibility(0);
                textView.setText(((AnonymousClass1ZN) list2.get(i2)).A03);
                textView.setSelected(((AnonymousClass1ZN) list2.get(i2)).A00);
                View A02 = A02(i2);
                A02.setVisibility(0);
                if (((AnonymousClass1ZN) list2.get(i2)).A00) {
                    A02.setClickable(false);
                } else {
                    A02.setClickable(true);
                    A02.setOnClickListener(new ViewOnClickCListenerShape1S0201000_I1(this, r8, i2, 0));
                }
                A02.setContentDescription(((AnonymousClass1ZN) list2.get(i2)).A03);
                A02.setLongClickable(true);
                AnonymousClass028.A0g(A02, new C53552ed(this, i2));
            }
            i2++;
        } while (i2 < 3);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int applyDimension = (int) TypedValue.applyDimension(1, 16.0f, C12990iw.A0K(this));
        int applyDimension2 = (int) TypedValue.applyDimension(1, 2.0f, C12990iw.A0K(this));
        int right = getRight() - getLeft();
        View[] viewArr = this.A03;
        int i6 = 0;
        int A00 = A00(viewArr[0]);
        int measuredWidth = getMeasuredWidth();
        boolean z2 = true;
        if (this.A02.size() != 2 || viewArr[0].getMeasuredWidth() > (i5 = (measuredWidth / 2) - (applyDimension << 1)) || viewArr[1].getMeasuredWidth() > i5) {
            z2 = false;
        }
        int i7 = applyDimension2 >> 1;
        int i8 = i7;
        do {
            View view = viewArr[i6];
            if (view != null) {
                View[] viewArr2 = this.A04;
                if (viewArr2[i6] != null && view.getVisibility() == 0) {
                    if (i6 != 0 || !z2) {
                        int width = getWidth();
                        View view2 = viewArr[i6];
                        View view3 = viewArr2[i6];
                        int A09 = C12980iv.A09(view2, right, applyDimension);
                        int measuredHeight = (A00 - view2.getMeasuredHeight()) >> 1;
                        view3.layout(-applyDimension2, i8, width + applyDimension2, A00 + i8 + applyDimension2);
                        int i9 = i8 + measuredHeight;
                        view2.layout(A09, i9, width - A09, view2.getMeasuredHeight() + i9 + applyDimension2);
                        i6++;
                    } else {
                        int width2 = getWidth();
                        View view4 = viewArr[0];
                        View view5 = viewArr2[0];
                        View view6 = viewArr[1];
                        View view7 = viewArr2[1];
                        int i10 = width2 >> 1;
                        int measuredHeight2 = (A00 - view4.getMeasuredHeight()) >> 1;
                        int measuredHeight3 = (A00 - view6.getMeasuredHeight()) >> 1;
                        int A092 = C12980iv.A09(view4, i10, applyDimension);
                        int A093 = C12980iv.A09(view6, i10, applyDimension);
                        int i11 = A00 + i8 + applyDimension2;
                        view5.layout(-applyDimension2, i8, i10 + i7, i11);
                        view7.layout(i10 - i7, i8, width2 + applyDimension2, i11);
                        view4.layout(A092, measuredHeight2 + i8, i10 - A092, measuredHeight2 + view4.getMeasuredHeight() + i8);
                        view6.layout(i10 + A093, measuredHeight3 + i8, width2 - A093, measuredHeight3 + view4.getMeasuredHeight() + i8);
                        i6 = 2;
                    }
                    i8 += A00;
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (i6 < 3);
    }
}
