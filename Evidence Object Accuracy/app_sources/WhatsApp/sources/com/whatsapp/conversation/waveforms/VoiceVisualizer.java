package com.whatsapp.conversation.waveforms;

import X.AbstractC14670lq;
import X.AnonymousClass009;
import X.AnonymousClass2QN;
import X.AnonymousClass3G9;
import X.AnonymousClass55R;
import X.AnonymousClass5RS;
import X.C91874To;
import X.C91884Tp;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes2.dex */
public class VoiceVisualizer extends View {
    public static final int[] A0I = new int[2];
    public float A00;
    public float A01;
    public long A02;
    public long A03;
    public AnonymousClass5RS A04;
    public boolean A05;
    public boolean A06;
    public final float A07;
    public final float A08;
    public final float A09;
    public final float A0A;
    public final float A0B;
    public final Paint A0C;
    public final Paint A0D;
    public final Paint A0E;
    public final LinkedList A0F;
    public final List A0G;
    public final List A0H;

    public VoiceVisualizer(Context context) {
        this(context, null);
    }

    public VoiceVisualizer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public VoiceVisualizer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0F = new LinkedList();
        this.A0H = new ArrayList();
        this.A0G = new ArrayList();
        Paint paint = new Paint(5);
        this.A0D = paint;
        Paint paint2 = new Paint(5);
        this.A0E = paint2;
        this.A0C = new Paint(5);
        this.A03 = 166;
        this.A05 = false;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0R, 0, 0);
        try {
            C91874To r6 = new C91874To();
            r6.A05 = obtainStyledAttributes.getColor(3, -7829368);
            r6.A04 = obtainStyledAttributes.getColor(2, -16711936);
            r6.A03 = obtainStyledAttributes.getColor(0, -16711936);
            r6.A01 = (float) obtainStyledAttributes.getDimensionPixelOffset(4, AnonymousClass3G9.A01(context, 5.0f));
            r6.A02 = (float) obtainStyledAttributes.getDimensionPixelOffset(5, AnonymousClass3G9.A01(context, 3.0f));
            r6.A00 = (float) obtainStyledAttributes.getDimensionPixelOffset(1, AnonymousClass3G9.A01(context, 0.0f));
            Paint.Cap cap = Paint.Cap.ROUND;
            r6.A06 = cap;
            r6.A07 = cap;
            C91884Tp r2 = new C91884Tp(r6);
            obtainStyledAttributes.recycle();
            paint.setStrokeCap(r2.A06);
            paint2.setStrokeCap(r2.A07);
            float f = r2.A01;
            this.A0A = f;
            this.A08 = 1.8f * f;
            this.A07 = f * 2.0f;
            this.A0B = r2.A02;
            this.A09 = r2.A00;
            setSegmentColor(r2.A05);
            setProgressColor(r2.A04);
            setProgressBubbleColor(r2.A03);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final float A00(MotionEvent motionEvent) {
        int[] iArr = A0I;
        getLocationInWindow(iArr);
        return Math.max(Math.min((motionEvent.getRawX() - ((float) (iArr[0] + getPaddingLeft()))) / (((float) (getWidth() - (getPaddingRight() + getPaddingLeft()))) * getScaleX()), 1.0f), 0.0f);
    }

    public final void A01(Canvas canvas, Paint paint, float f, int i) {
        float min;
        double pow;
        int height = (canvas.getHeight() - getPaddingTop()) - getPaddingBottom();
        float f2 = this.A0A;
        float f3 = f * f2;
        float width = ((float) (canvas.getWidth() - getPaddingRight())) - f3;
        List list = this.A0G;
        if (!list.isEmpty()) {
            int i2 = 0;
            for (int i3 = i - 1; i3 >= 0; i3--) {
                A03(canvas, paint, 1.0f, ((Number) list.get(i3)).floatValue(), width, height, i2);
                i2++;
            }
            return;
        }
        Iterator descendingIterator = this.A0F.descendingIterator();
        boolean z = false;
        int i4 = 0;
        while (descendingIterator.hasNext()) {
            float floatValue = ((Number) descendingIterator.next()).floatValue();
            if (z) {
                descendingIterator.remove();
            } else {
                float f4 = (((float) i4) * f2) + f3;
                float width2 = ((float) (canvas.getWidth() - getPaddingLeft())) - f4;
                if (width2 < f4) {
                    min = Math.min(1.0f, width2 / this.A08);
                } else {
                    min = Math.min(1.0f, f4 / this.A07);
                }
                if (min < 0.5f) {
                    pow = (double) (4.0f * min * min * min);
                } else {
                    pow = 1.0d - (Math.pow((double) ((min * -2.0f) + 2.0f), 3.0d) / 2.0d);
                }
                if (A03(canvas, paint, (float) pow, floatValue, width, height, i4)) {
                    i4++;
                } else {
                    descendingIterator.remove();
                    z = true;
                }
            }
        }
    }

    public void A02(List list, float f) {
        AnonymousClass009.A0E(this.A0F.isEmpty());
        List list2 = this.A0H;
        list2.clear();
        List list3 = this.A0G;
        list3.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Number number = (Number) it.next();
            list2.add(Float.valueOf(number.floatValue()));
            list3.add(Float.valueOf(number.floatValue()));
        }
        setPlaybackPercentage(f);
        requestLayout();
    }

    public final boolean A03(Canvas canvas, Paint paint, float f, float f2, float f3, int i, int i2) {
        float f4 = f3 - (this.A0A * ((float) i2));
        float f5 = this.A0B;
        if (f4 < ((float) getPaddingLeft()) - f5) {
            return false;
        }
        float f6 = (float) i;
        float max = Math.max(0.006f, f2) * f6 * f;
        float paddingTop = ((float) getPaddingTop()) + ((f6 - max) / 2.0f);
        paint.setStrokeWidth(f5);
        canvas.drawLine(f4, paddingTop, f4, paddingTop + max, paint);
        return true;
    }

    private int getDesiredWidth() {
        return (int) (((float) this.A0G.size()) * this.A0A);
    }

    public float getPlaybackPercentage() {
        return this.A00;
    }

    public float getSegmentSpacingPx() {
        return this.A0A;
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int size;
        super.onDraw(canvas);
        LinkedList linkedList = this.A0F;
        if (!linkedList.isEmpty() || !this.A0G.isEmpty()) {
            List list = this.A0G;
            float f = 1.0f;
            if (list.isEmpty()) {
                size = linkedList.size();
                long j = this.A02;
                if (j != 0) {
                    f = (((float) (SystemClock.elapsedRealtime() - j)) * 1.0f) / ((float) this.A03);
                } else {
                    f = 0.0f;
                }
            } else {
                size = list.size();
            }
            A01(canvas, this.A0E, f, size);
            if (this.A00 > 0.0f) {
                canvas.save();
                canvas.clipRect(((float) getPaddingLeft()) - this.A0A, 0.0f, (((float) ((getWidth() - getPaddingLeft()) - getPaddingRight())) * this.A00) + ((float) getPaddingLeft()), (float) getHeight());
                A01(canvas, this.A0D, f, size);
                canvas.restore();
            }
            float f2 = this.A09;
            if (f2 != 0.0f) {
                float width = (float) ((getWidth() - getPaddingLeft()) - getPaddingRight());
                float f3 = this.A0B / 2.0f;
                canvas.drawCircle(((width - f3) * this.A00) + (((float) getPaddingLeft()) - f3), ((float) getHeight()) / 2.0f, f2, this.A0C);
            }
            if (this.A06) {
                invalidate();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        if (r9 >= 100000) goto L_0x001a;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r16, int r17) {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.waveforms.VoiceVisualizer.onMeasure(int, int):void");
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.A04 == null) {
            return super.onTouchEvent(motionEvent);
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 3 || actionMasked == 1) {
            if (this.A05) {
                this.A05 = false;
                AbstractC14670lq.A00(((AnonymousClass55R) this.A04).A00);
            }
        } else if (actionMasked == 2) {
            float A00 = A00(motionEvent);
            if (!this.A05) {
                if (Math.abs(A00 - this.A01) >= 0.015f) {
                    this.A05 = true;
                    setPlaybackPercentage(A00);
                    AbstractC14670lq r6 = ((AnonymousClass55R) this.A04).A00;
                    r6.A1J.A02++;
                    if (r6.A0N != null) {
                        r6.A0a.removeCallbacks(r6.A1Q);
                        r6.A08 = -1;
                    }
                }
                return true;
            }
            setPlaybackPercentage(A00);
            AbstractC14670lq r3 = ((AnonymousClass55R) this.A04).A00;
            AbstractC14670lq.A01(r3, (int) (((float) r3.A0A) * A00), true);
            return true;
        } else if (actionMasked == 0) {
            this.A01 = A00(motionEvent);
            return true;
        }
        return false;
    }

    public void setOnVoiceVisualizerChangeListener(AnonymousClass5RS r1) {
        this.A04 = r1;
    }

    public void setPlaybackPercentage(float f) {
        if (f >= 0.0f && f <= 1.0f) {
            this.A00 = f;
            postInvalidateOnAnimation();
        }
    }

    private void setProgressBubbleColor(int i) {
        this.A0C.setColor(i);
    }

    private void setProgressColor(int i) {
        this.A0D.setColor(i);
    }

    private void setSegmentColor(int i) {
        this.A0E.setColor(i);
    }
}
