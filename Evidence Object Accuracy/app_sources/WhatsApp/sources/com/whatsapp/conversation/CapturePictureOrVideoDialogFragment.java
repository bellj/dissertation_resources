package com.whatsapp.conversation;

import X.AbstractC13970kd;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.AnonymousClass0OC;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public final class CapturePictureOrVideoDialogFragment extends Hilt_CapturePictureOrVideoDialogFragment {
    public static final int[] A02;
    public AbstractC13970kd A00;
    public AnonymousClass018 A01;

    static {
        int[] A07 = C13000ix.A07();
        A07[0] = R.string.take_picture;
        A07[1] = R.string.record_video;
        A02 = A07;
    }

    @Override // com.whatsapp.conversation.Hilt_CapturePictureOrVideoDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        try {
            this.A00 = (AbstractC13970kd) context;
        } catch (ClassCastException unused) {
            StringBuilder A0h = C12960it.A0h();
            C12970iu.A1V(context, A0h);
            throw new ClassCastException(C12960it.A0d(" must implement CapturePictureOrVideoDialogClickListener", A0h));
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0K = C12960it.A0K(this);
        String[] A0S = this.A01.A0S(A02);
        IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(this, 27);
        AnonymousClass0OC r0 = A0K.A01;
        r0.A0M = A0S;
        r0.A05 = iDxCListenerShape9S0100000_2_I1;
        AnonymousClass04S create = A0K.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
