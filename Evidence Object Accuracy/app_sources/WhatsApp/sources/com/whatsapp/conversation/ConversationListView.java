package com.whatsapp.conversation;

import X.AbstractC60742yY;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass1IS;
import X.AnonymousClass1OY;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3g3;
import X.C14830m7;
import X.C15350n0;
import X.C21830y3;
import X.C22910zq;
import X.C252718t;
import X.C63823Db;
import X.C73693ge;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ConversationListView extends ListView implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public C63823Db A03;
    public C21830y3 A04;
    public C14830m7 A05;
    public C22910zq A06;
    public C252718t A07;
    public AnonymousClass2P7 A08;
    public Runnable A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final Handler A0H;

    public ConversationListView(Context context) {
        super(context);
        A01();
        this.A0A = true;
        this.A0E = true;
        this.A0H = new AnonymousClass3g3(Looper.getMainLooper(), this);
        this.A03 = new C63823Db();
    }

    public ConversationListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
        this.A0A = true;
        this.A0E = true;
        this.A0H = new AnonymousClass3g3(Looper.getMainLooper(), this);
        this.A03 = new C63823Db();
    }

    public ConversationListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
        this.A0A = true;
        this.A0E = true;
        this.A0H = new AnonymousClass3g3(Looper.getMainLooper(), this);
        this.A03 = new C63823Db();
    }

    public ConversationListView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
        this.A0A = true;
        this.A0E = true;
        this.A0H = new AnonymousClass3g3(Looper.getMainLooper(), this);
        this.A03 = new C63823Db();
    }

    public ConversationListView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A01();
    }

    public AnonymousClass1OY A00(AnonymousClass1IS r5) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AnonymousClass1OY) {
                AnonymousClass1OY r1 = (AnonymousClass1OY) childAt;
                if (r1.A1L(r5)) {
                    return r1;
                }
            }
        }
        return null;
    }

    public void A01() {
        if (!this.A0D) {
            this.A0D = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A05 = (C14830m7) r1.ALb.get();
            this.A06 = (C22910zq) r1.A9O.get();
            this.A04 = (C21830y3) r1.A4f.get();
            this.A07 = (C252718t) r1.A9K.get();
        }
    }

    public void A02() {
        getConversationCursorAdapter().notifyDataSetChanged();
        if (this.A0B) {
            this.A0C = false;
            this.A0G = false;
        }
    }

    public void A03() {
        if (this.A0F) {
            A04();
            this.A0F = false;
        } else if (getLastVisiblePosition() >= getCount() - 2) {
            A04();
        } else {
            smoothScrollBy((int) getResources().getDimension(R.dimen.conversation_row_min_height), 100);
        }
    }

    public void A04() {
        if (this.A0B) {
            this.A0C = false;
            this.A0G = false;
        }
        this.A0A = true;
        post(new RunnableBRunnable0Shape4S0100000_I0_4(this, 45));
    }

    public void A05() {
        C15350n0 conversationCursorAdapter = getConversationCursorAdapter();
        if (conversationCursorAdapter != null && conversationCursorAdapter.getCursor() != null) {
            int A01 = conversationCursorAdapter.A01();
            int defaultDividerOffset = getDefaultDividerOffset();
            conversationCursorAdapter.A01();
            A07(A01 + getHeaderViewsCount(), defaultDividerOffset);
            this.A0C = false;
            this.A0B = false;
        }
    }

    public void A06(int i, int i2) {
        this.A00 = i;
        if (i + i2 >= getConversationCursorAdapter().getCount() + getHeaderViewsCount()) {
            this.A0A = true;
            return;
        }
        this.A0A = false;
        if (Build.VERSION.SDK_INT < 18 || !isInLayout()) {
            setTranscriptMode(0);
        }
    }

    public void A07(int i, int i2) {
        setTranscriptMode(0);
        setSelectionFromTop(i, i2);
    }

    public void A08(Cursor cursor) {
        C15350n0 conversationCursorAdapter = getConversationCursorAdapter();
        if (conversationCursorAdapter != null && !cursor.isClosed()) {
            conversationCursorAdapter.A0F.clear();
            conversationCursorAdapter.A0Q.clear();
            conversationCursorAdapter.changeCursor(cursor);
        }
    }

    public final void A09(View view, int i, int i2, boolean z) {
        int i3;
        if (getFirstVisiblePosition() >= i || getLastVisiblePosition() <= i) {
            if (z) {
                int i4 = -1;
                if (getFirstVisiblePosition() < i) {
                    i4 = 1;
                }
                i3 = ((i4 * getHeight()) >> 2) + i2;
            } else {
                i3 = i2;
            }
            A07(i, i3);
            smoothScrollToPositionFromTop(i, i2);
            view.setVisibility(0);
        }
    }

    public boolean A0A() {
        if (getLastVisiblePosition() < (getHeaderViewsCount() + getConversationCursorAdapter().getCount()) - 1 || getChildCount() == 0 || getChildAt(getChildCount() - 1).getBottom() != getHeight()) {
            return false;
        }
        return true;
    }

    @Override // android.widget.ListView, android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        View selectedView;
        if (keyEvent.getKeyCode() != 23 || (selectedView = getSelectedView()) == null) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return selectedView.dispatchKeyEvent(keyEvent);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public int getAdjustedVisibleItemCount() {
        if (getChildCount() <= 0) {
            return 0;
        }
        View childAt = getChildAt(getChildCount() - 1);
        if (childAt instanceof AbstractC60742yY) {
            return 0 + (((AnonymousClass1OY) childAt).getMessageCount() - 1);
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        if ((r3 instanceof X.C15350n0) != false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C15350n0 getConversationCursorAdapter() {
        /*
            r4 = this;
            android.widget.ListAdapter r3 = r4.getAdapter()
            r2 = 0
            r1 = 1
            if (r3 != 0) goto L_0x000f
            r1 = 0
            java.lang.String r0 = "adapter should be NonNull"
        L_0x000b:
            X.AnonymousClass009.A0A(r0, r1)
            return r2
        L_0x000f:
            boolean r0 = r3 instanceof X.C15350n0
            if (r0 != 0) goto L_0x0027
            boolean r0 = r3 instanceof android.widget.HeaderViewListAdapter
            if (r0 == 0) goto L_0x002a
            android.widget.HeaderViewListAdapter r3 = (android.widget.HeaderViewListAdapter) r3
            android.widget.ListAdapter r3 = r3.getWrappedAdapter()
            if (r3 != 0) goto L_0x0023
            java.lang.String r0 = "wrapped adapter should be NonNull"
            goto L_0x000b
        L_0x0023:
            boolean r0 = r3 instanceof X.C15350n0
            if (r0 == 0) goto L_0x002a
        L_0x0027:
            X.0n0 r3 = (X.C15350n0) r3
            return r3
        L_0x002a:
            java.lang.String r1 = "Unknown adapter type"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.conversation.ConversationListView.getConversationCursorAdapter():X.0n0");
    }

    public int getDefaultDividerOffset() {
        Point point = new Point();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getSize(point);
        return (point.y - (getResources().getDimensionPixelSize(R.dimen.header_height) << 1)) / 5;
    }

    public int getFirstPosition() {
        int firstVisiblePosition = getFirstVisiblePosition() - getHeaderViewsCount();
        for (int i = 1; i < getChildCount(); i++) {
            firstVisiblePosition = (getFirstVisiblePosition() + i) - getHeaderViewsCount();
            if (getChildAt(i).getTop() >= getResources().getDimensionPixelSize(R.dimen.conversation_row_min_height)) {
                break;
            }
        }
        return firstVisiblePosition;
    }

    @Override // android.widget.ListView, android.widget.AbsListView
    public void onInitializeAccessibilityNodeInfoForItem(View view, int i, AccessibilityNodeInfo accessibilityNodeInfo) {
        ListAdapter adapter = getAdapter();
        if (i != -1 && adapter != null) {
            if (i >= adapter.getCount()) {
                StringBuilder sb = new StringBuilder("conversationvistview/onInitializeAccessibilityNodeInfoForItem pos:");
                sb.append(i);
                sb.append(" count:");
                sb.append(adapter.getCount());
                Log.w(sb.toString());
                return;
            }
            super.onInitializeAccessibilityNodeInfoForItem(view, i, accessibilityNodeInfo);
        }
    }

    @Override // android.widget.AbsListView, android.view.ViewGroup, android.view.View, android.widget.AdapterView
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        AnonymousClass1OY r1;
        C63823Db r4 = this.A03;
        r4.A01();
        int childCount = getChildCount();
        int i5 = 0;
        while (true) {
            if (i5 >= childCount) {
                r1 = null;
                break;
            }
            View childAt = getChildAt(i5);
            if (childAt.isPressed() && (childAt instanceof AnonymousClass1OY)) {
                r1 = (AnonymousClass1OY) childAt;
                r1.A1W = true;
                break;
            }
            i5++;
        }
        super.onLayout(z, i, i2, i3, i4);
        if (r1 != null) {
            r1.A1W = false;
        }
        r4.A00();
    }

    @Override // android.widget.ListView, android.widget.AbsListView, android.view.View
    public void onMeasure(int i, int i2) {
        if (getTranscriptMode() == 2) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if ((mode == Integer.MIN_VALUE || mode == 1073741824) && size == 0) {
                i2 = View.MeasureSpec.makeMeasureSpec(1, mode);
            }
        }
        super.onMeasure(i, i2);
    }

    @Override // android.widget.AbsListView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C73693ge r2 = (C73693ge) parcelable;
        super.onRestoreInstanceState(r2.getSuperState());
        this.A0E = r2.A02;
        this.A01 = r2.A00;
        this.A02 = r2.A01;
        requestLayout();
    }

    @Override // android.widget.AbsListView, android.view.View
    public Parcelable onSaveInstanceState() {
        C73693ge r1 = new C73693ge(super.onSaveInstanceState());
        r1.A02 = this.A0E;
        r1.A00 = this.A01;
        r1.A01 = this.A02;
        return r1;
    }

    public void setScrollToBottom(boolean z) {
        this.A0F = z;
    }

    public void setScrollToTop(boolean z) {
        this.A0G = z;
    }
}
