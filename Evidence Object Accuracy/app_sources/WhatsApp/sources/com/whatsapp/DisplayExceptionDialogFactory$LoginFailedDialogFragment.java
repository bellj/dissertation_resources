package com.whatsapp;

import X.ActivityC000900k;
import X.C004802e;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14820m6;
import X.C14830m7;
import X.C18350sJ;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class DisplayExceptionDialogFactory$LoginFailedDialogFragment extends Hilt_DisplayExceptionDialogFactory_LoginFailedDialogFragment {
    public C14830m7 A00;
    public C14820m6 A01;
    public C18350sJ A02;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0S = C12980iv.A0S(A0C());
        A0S.A06(R.string.post_registration_logout_dialog_message);
        A0S.A0B(false);
        C12970iu.A1M(A0S, this, 0, R.string.ok);
        return C12990iw.A0O(A0S, this, 1, R.string.post_registration_logout_dialog_negative_button);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            A0B.finish();
        }
    }
}
