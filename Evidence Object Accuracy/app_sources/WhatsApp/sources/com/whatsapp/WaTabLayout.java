package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3FN;
import X.C74333ho;
import X.C74393hu;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class WaTabLayout extends TabLayout implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public WaTabLayout(Context context) {
        super(context, null);
        A0K();
        AnonymousClass028.A0c(this, 0);
    }

    public WaTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0K();
        AnonymousClass028.A0c(this, 0);
    }

    public WaTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0K();
        AnonymousClass028.A0c(this, 0);
    }

    public WaTabLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A0K();
    }

    public final int A0I(int i, boolean z) {
        int size = (z ? 1 : 0) + this.A0c.size();
        if (i >= 0 && i < size) {
            return !(this.A00.A04().A06 ^ true) ? (size - i) - 1 : i;
        }
        StringBuilder sb = new StringBuilder("Tab index ");
        sb.append(i);
        sb.append(" is out of range [0, ");
        sb.append(size);
        sb.append(")");
        throw new IndexOutOfBoundsException(sb.toString());
    }

    public AnonymousClass3FN A0J(int i) {
        if (i < 0 || i >= this.A0c.size()) {
            return null;
        }
        return super.A04(A0I(i, false));
    }

    public void A0K() {
        if (!this.A02) {
            this.A02 = true;
            this.A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setTabsClickable(boolean z) {
        for (int i = 0; i < this.A0c.size(); i++) {
            AnonymousClass3FN A04 = A04(i);
            if (A04 != null) {
                A04.A02.setClickable(z);
            }
        }
    }

    public void setupTabsForAccessibility(View view) {
        View view2;
        AnonymousClass028.A0g(this, new C74333ho(this));
        ArrayList arrayList = this.A0c;
        int size = arrayList.size() + 1;
        View[] viewArr = new View[size];
        viewArr[arrayList.size()] = view;
        for (int i = 0; i < arrayList.size(); i++) {
            AnonymousClass3FN A0J = A0J(i);
            if (A0J != null) {
                viewArr[i] = A0J.A02;
            }
        }
        for (int i2 = 0; i2 < size; i2++) {
            if (i2 == 0) {
                view2 = null;
            } else {
                view2 = viewArr[i2 - 1];
            }
            AnonymousClass028.A0g(viewArr[i2], new C74393hu(view2, this, i2));
        }
    }

    @Override // com.google.android.material.tabs.TabLayout
    public void setupWithViewPager(ViewPager viewPager) {
        if (viewPager != null && !(viewPager instanceof WaViewPager)) {
            throw new IllegalArgumentException("WaTabLayout should only be setup with WaViewPager");
        } else if (viewPager == null || (viewPager instanceof WaViewPager)) {
            A0C(viewPager, false);
        } else {
            throw new IllegalArgumentException("WaTabLayout should only be setup with WaViewPager");
        }
    }
}
