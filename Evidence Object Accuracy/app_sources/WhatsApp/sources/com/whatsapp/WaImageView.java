package com.whatsapp;

import X.AbstractC37211li;
import X.AnonymousClass018;
import X.AnonymousClass2GZ;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaImageView extends AbstractC37211li {
    public AnonymousClass018 A00;
    public boolean A01;

    public WaImageView(Context context) {
        super(context);
    }

    public WaImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01(context, attributeSet);
    }

    public WaImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(context, attributeSet);
    }

    public final void A01(Context context, AttributeSet attributeSet) {
        if (attributeSet != null && this.A00 != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0T);
            int resourceId = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId != 0) {
                setContentDescription(this.A00.A09(resourceId));
            }
            this.A01 = obtainStyledAttributes.getBoolean(1, false);
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        boolean z;
        AnonymousClass018 r0;
        if (!this.A01 || (r0 = this.A00) == null) {
            z = false;
        } else {
            z = r0.A04().A06;
            if (z) {
                canvas.save();
                canvas.scale(-1.0f, 1.0f, ((float) super.getWidth()) * 0.5f, ((float) super.getHeight()) * 0.5f);
            }
        }
        super.onDraw(canvas);
        if (this.A01 && z) {
            canvas.restore();
        }
    }

    public void setMirrorForRtl(boolean z) {
        this.A01 = z;
    }
}
