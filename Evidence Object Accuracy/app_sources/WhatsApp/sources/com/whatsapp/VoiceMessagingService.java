package com.whatsapp;

import X.AbstractC14640lm;
import X.AbstractC18390sN;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1UY;
import X.C005602s;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14410lO;
import X.C14850m9;
import X.C14960mK;
import X.C15380n4;
import X.C15550nR;
import X.C16150oX;
import X.C16170oZ;
import X.C18360sK;
import X.C18380sM;
import X.C22190yg;
import X.C22630zO;
import X.C22700zV;
import X.C23000zz;
import X.C28061Kl;
import X.C28081Kn;
import X.C42961wB;
import X.C48502Gn;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;

/* loaded from: classes2.dex */
public class VoiceMessagingService extends SearchActionVerificationClientService {
    public AbstractC18390sN A00;
    public C16170oZ A01;
    public C15550nR A02;
    public C22700zV A03;
    public C18360sK A04;
    public AnonymousClass018 A05;
    public C14850m9 A06;
    public C14410lO A07;
    public C23000zz A08;
    public C22190yg A09;
    public final Handler A0A = C12970iu.A0E();

    @Override // android.app.Service, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        AnonymousClass01J A0S = C12990iw.A0S(context);
        this.A06 = C12960it.A0S(A0S);
        this.A01 = (C16170oZ) A0S.AM4.get();
        this.A07 = (C14410lO) A0S.AB3.get();
        this.A02 = C12960it.A0O(A0S);
        this.A09 = (C22190yg) A0S.AB6.get();
        this.A05 = A0S.Ag8();
        this.A08 = (C23000zz) A0S.ALg.get();
        this.A03 = C12980iv.A0a(A0S);
        this.A04 = (C18360sK) A0S.AN0.get();
        C18380sM A1v = A0S.A1v();
        this.A00 = A1v;
        super.attachBaseContext(new C48502Gn(context, A1v, this.A05));
    }

    @Override // com.google.android.search.verification.client.SearchActionVerificationClientService
    public void performAction(Intent intent, boolean z, Bundle bundle) {
        StringBuilder A0k;
        Uri uri;
        String obj;
        if (!z) {
            obj = "VoiceMessagingService/ignoring unverified voice message";
        } else {
            String stringExtra = intent.getStringExtra("com.google.android.voicesearch.extra.RECIPIENT_CONTACT_CHAT_ID");
            AbstractC14640lm A01 = AbstractC14640lm.A01(stringExtra);
            if (C15380n4.A0L(A01) || C15380n4.A0F(A01) || C15380n4.A0J(A01)) {
                C14850m9 r1 = this.A06;
                C22700zV r0 = this.A03;
                UserJid of = UserJid.of(A01);
                if (!C28061Kl.A01(r0, r1, of)) {
                    if (!C28081Kn.A01(this.A03, this.A06, of, this.A08)) {
                        ClipData clipData = intent.getClipData();
                        if (clipData != null) {
                            if (clipData.getItemCount() == 1) {
                                ClipData.Item itemAt = clipData.getItemAt(0);
                                if (!(itemAt == null || (uri = itemAt.getUri()) == null)) {
                                    try {
                                        C16150oX r3 = new C16150oX();
                                        r3.A0F = this.A09.A0A(uri);
                                        StringBuilder A0h = C12960it.A0h();
                                        A0h.append("VoiceMessagingService/sending verified voice message (voice); jid=");
                                        A0h.append(A01);
                                        C12960it.A1F(A0h);
                                        this.A0A.post(new RunnableBRunnable0Shape3S0300000_I1(this, A01, r3, 6));
                                        return;
                                    } catch (IOException e) {
                                        Log.w("VoiceMessagingService/IO Exception while trying to send voice message", e);
                                        return;
                                    }
                                }
                            } else if (clipData.getItemCount() > 1 || clipData.getItemCount() < 0) {
                                A0k = C12960it.A0k("VoiceMessagingService/ignoring voice message with unexpected item count; itemCount=");
                                A0k.append(clipData.getItemCount());
                            }
                        }
                        String stringExtra2 = intent.getStringExtra("android.intent.extra.TEXT");
                        if (TextUtils.isEmpty(stringExtra2)) {
                            A0k = C12960it.A0k("VoiceMessagingService/ignoring voice message with empty contents; jid=");
                            A0k.append(A01);
                            A0k.append("; text=");
                            A0k.append(stringExtra2);
                        } else {
                            Log.i(C12960it.A0b("VoiceMessagingService/sending verified voice message (text); jid=", A01));
                            this.A0A.post(new RunnableBRunnable0Shape1S1200000_I1(A01, this, stringExtra2, 6));
                            return;
                        }
                    }
                }
                AnonymousClass009.A05(A01);
                Uri withAppendedId = ContentUris.withAppendedId(C42961wB.A00, this.A02.A0B(A01).A08());
                String str = Conversation.A59;
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(getPackageName(), "com.whatsapp.Conversation");
                A0A.setData(withAppendedId);
                A0A.setAction(str);
                A0A.addFlags(335544320);
                PendingIntent A00 = AnonymousClass1UY.A00(this, 2, A0A.putExtra("fromNotification", true), 0);
                C005602s A002 = C22630zO.A00(this);
                A002.A0J = "other_notifications@1";
                A002.A0I = "err";
                A002.A03 = 1;
                A002.A0D(true);
                A002.A02(4);
                A002.A06 = 0;
                A002.A09 = A00;
                A002.A0A(getString(R.string.tos_gating_notification_title));
                A002.A09(getString(R.string.tos_gating_notification_subtitle));
                C18360sK.A01(A002, R.drawable.notifybar);
                this.A04.A03(35, A002.A01());
                return;
            }
            A0k = C12960it.A0k("VoiceMessagingService/ignoring voice message directed at invalid jid; jid=");
            A0k.append(stringExtra);
            obj = A0k.toString();
        }
        Log.w(obj);
    }

    @Override // com.google.android.search.verification.client.SearchActionVerificationClientService
    public void postForegroundNotification() {
        C005602s A00 = C22630zO.A00(this);
        A00.A0J = "other_notifications@1";
        A00.A0A(getString(R.string.sending_message));
        A00.A09 = AnonymousClass1UY.A00(this, 1, C14960mK.A02(this), 0);
        A00.A03 = -2;
        C18360sK.A01(A00, R.drawable.notifybar);
        Notification A01 = A00.A01();
        Log.i(C12960it.A0b("VoiceMessagingService/posting assistant notif:", A01));
        startForeground(19, A01);
    }
}
