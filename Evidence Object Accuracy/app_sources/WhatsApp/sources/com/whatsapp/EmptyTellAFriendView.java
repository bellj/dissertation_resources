package com.whatsapp;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C14820m6;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import com.whatsapp.components.Button;

/* loaded from: classes2.dex */
public class EmptyTellAFriendView extends ScrollView implements AnonymousClass004 {
    public WaTextView A00;
    public Button A01;
    public C14820m6 A02;
    public AnonymousClass2P7 A03;
    public boolean A04;

    public EmptyTellAFriendView(Context context) {
        super(context);
        A00();
        A01();
    }

    public EmptyTellAFriendView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01();
    }

    public EmptyTellAFriendView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01();
    }

    public EmptyTellAFriendView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A04) {
            this.A04 = true;
            this.A02 = C12970iu.A0Z(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A01() {
        ScrollView.inflate(getContext(), R.layout.empty_tell_a_friend, this);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.A01 = (Button) AnonymousClass028.A0D(this, R.id.invite_button_tell_a_friend);
        this.A00 = C12960it.A0N(this, R.id.subtitle_tell_a_friend);
        boolean equals = "91".equals(this.A02.A0B());
        WaTextView waTextView = this.A00;
        int i = R.string.welcome_available_platforms;
        if (equals) {
            i = R.string.welcome_available_platforms_india;
        }
        waTextView.setText(i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public void setInviteButtonClickListener(View.OnClickListener onClickListener) {
        this.A01.setOnClickListener(onClickListener);
    }
}
