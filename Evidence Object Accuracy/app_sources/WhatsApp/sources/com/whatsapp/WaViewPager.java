package com.whatsapp;

import X.AbstractC55392iK;
import X.AnonymousClass018;
import X.AnonymousClass01A;
import X.C12960it;
import X.C28141Kv;
import X.C55352iG;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes2.dex */
public class WaViewPager extends AbstractC55392iK {
    public AnonymousClass018 A00;

    public WaViewPager(Context context) {
        super(context);
    }

    public WaViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public static int A00(AnonymousClass018 r1, int i, int i2) {
        if (i >= 0 && i < i2) {
            return !C28141Kv.A01(r1) ? (i2 - i) - 1 : i;
        }
        StringBuilder A0k = C12960it.A0k("Item index ");
        A0k.append(i);
        A0k.append(" is out of range [0, ");
        A0k.append(i2);
        throw new IndexOutOfBoundsException(C12960it.A0d(")", A0k));
    }

    public int A0O(int i) {
        return A00(this.A00, i, getItemCount());
    }

    public void A0P(int i) {
        super.A0F(A0O(i), true);
    }

    public int getCurrentLogicalItem() {
        if (getItemCount() == 0) {
            return -1;
        }
        return A0O(this.A0A);
    }

    private int getItemCount() {
        AnonymousClass01A r0 = this.A0V;
        if (r0 == null) {
            return 0;
        }
        return r0.A01();
    }

    public AnonymousClass01A getRealAdapter() {
        AnonymousClass01A r1 = this.A0V;
        if (r1 instanceof C55352iG) {
            return ((C55352iG) r1).A00;
        }
        return null;
    }

    @Override // androidx.viewpager.widget.ViewPager
    public void setAdapter(AnonymousClass01A r3) {
        C55352iG r1;
        if (r3 == null) {
            r1 = null;
        } else {
            r1 = new C55352iG(r3, this.A00);
        }
        super.setAdapter(r1);
        if (r3 != null && r3.A01() > 0) {
            setCurrentLogicalItem(0);
        }
    }

    public void setCurrentLogicalItem(int i) {
        super.setCurrentItem(A0O(i));
    }
}
