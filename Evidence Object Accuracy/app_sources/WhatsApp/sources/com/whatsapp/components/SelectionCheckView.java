package com.whatsapp.components;

import X.C12990iw;
import X.C83363x7;
import X.C83373x8;
import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class SelectionCheckView extends FrameLayout {
    public ShapeDrawable A00;
    public View A01;
    public AnimationSet A02;
    public AnimationSet A03;
    public ScaleAnimation A04;
    public ScaleAnimation A05;
    public ScaleAnimation A06;
    public ScaleAnimation A07;
    public ScaleAnimation A08;
    public ScaleAnimation A09;
    public FrameLayout A0A;
    public ImageView A0B;
    public boolean A0C = false;

    public SelectionCheckView(Context context) {
        super(context);
        A03(context, null);
    }

    public SelectionCheckView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A03(context, attributeSet);
    }

    public SelectionCheckView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A03(context, attributeSet);
    }

    public SelectionCheckView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A03(context, attributeSet);
    }

    public final ScaleAnimation A00(View view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(100);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        scaleAnimation.setRepeatCount(0);
        scaleAnimation.setAnimationListener(new C83363x7(view, this));
        return scaleAnimation;
    }

    public final ScaleAnimation A01(View view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(100);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setRepeatCount(0);
        scaleAnimation.setAnimationListener(new C83373x8(view, this));
        return scaleAnimation;
    }

    public final void A02() {
        ScaleAnimation A00 = A00(this.A0A);
        this.A06 = A00;
        A00.setStartOffset(20);
        this.A08 = A00(this.A01);
        ScaleAnimation A002 = A00(this.A0B);
        this.A04 = A002;
        A002.setStartOffset(10);
        AnimationSet animationSet = new AnimationSet(false);
        this.A02 = animationSet;
        animationSet.addAnimation(this.A06);
        this.A02.addAnimation(this.A08);
        this.A02.addAnimation(this.A04);
        this.A07 = A01(this.A0A);
        ScaleAnimation A01 = A01(this.A01);
        this.A09 = A01;
        A01.setStartOffset(20);
        ScaleAnimation A012 = A01(this.A0B);
        this.A05 = A012;
        A012.setStartOffset(10);
        AnimationSet animationSet2 = new AnimationSet(false);
        this.A03 = animationSet2;
        animationSet2.addAnimation(this.A07);
        this.A03.addAnimation(this.A09);
        this.A03.addAnimation(this.A05);
        this.A0C = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x004b, code lost:
        if (r3 == null) goto L_0x004d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(android.content.Context r12, android.util.AttributeSet r13) {
        /*
        // Method dump skipped, instructions count: 286
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.components.SelectionCheckView.A03(android.content.Context, android.util.AttributeSet):void");
    }

    public void A04(boolean z, boolean z2) {
        int i;
        FrameLayout frameLayout;
        AnimationSet animationSet;
        if (z) {
            if (z2) {
                if (!this.A0C) {
                    A02();
                }
                setVisibility(0);
                this.A0A.clearAnimation();
                this.A01.clearAnimation();
                this.A0B.clearAnimation();
                this.A0A.setAnimation(this.A06);
                this.A01.setAnimation(this.A08);
                this.A0B.setAnimation(this.A04);
                this.A0A.setForeground(this.A00);
                animationSet = this.A02;
                animationSet.start();
            }
            frameLayout = this.A0A;
            i = 0;
            frameLayout.setVisibility(i);
            this.A01.setVisibility(i);
            this.A0B.setVisibility(i);
        } else if (z2) {
            if (!this.A0C) {
                A02();
            }
            this.A0A.clearAnimation();
            this.A01.clearAnimation();
            this.A0B.clearAnimation();
            this.A0A.setAnimation(this.A07);
            this.A01.setAnimation(this.A09);
            this.A0B.setAnimation(this.A05);
            this.A0A.setForeground(null);
            animationSet = this.A03;
            animationSet.start();
        } else {
            frameLayout = this.A0A;
            i = 4;
            frameLayout.setVisibility(i);
            this.A01.setVisibility(i);
            this.A0B.setVisibility(i);
        }
    }

    public void setIcon(int i) {
        C12990iw.A0x(getContext(), this.A0B, i);
    }

    public void setSelectionBackground(int i) {
        this.A01.setBackgroundResource(i);
    }
}
