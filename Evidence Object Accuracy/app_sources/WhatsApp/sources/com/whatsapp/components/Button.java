package com.whatsapp.components;

import X.AnonymousClass028;
import X.AnonymousClass2GZ;
import X.C016907y;
import X.C12980iv;
import X.C12990iw;
import X.C41691tw;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* loaded from: classes2.dex */
public class Button extends WaButton {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public ColorStateList A04;
    public boolean A05;
    public final Paint A06;
    public final Rect A07;
    public final RectF A08;

    public Button(Context context) {
        this(context, null);
    }

    public Button(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.buttonStyle);
    }

    public Button(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        int i3;
        ColorStateList colorStateList;
        A00();
        this.A06 = C12990iw.A0G(1);
        this.A07 = C12980iv.A0J();
        this.A08 = C12980iv.A0K();
        if (!this.A0E) {
            Resources.Theme theme = context.getTheme();
            this.A03 = context.getResources().getDimensionPixelSize(R.dimen.button_stroke_width);
            this.A01 = context.getResources().getDimensionPixelSize(R.dimen.button_inset_horizontal);
            this.A02 = context.getResources().getDimensionPixelSize(R.dimen.button_inset_vertical);
            Paint paint = this.A06;
            C12990iw.A13(paint);
            paint.setStrokeWidth((float) this.A03);
            this.A00 = context.getResources().getDimensionPixelSize(R.dimen.btn_padding_vertical);
            TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A03, i, 2131952517);
            if (obtainStyledAttributes.hasValue(1)) {
                ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(1);
                this.A04 = colorStateList2;
                if (colorStateList2 != null) {
                    paint.setColor(colorStateList2.getColorForState(getDrawableState(), 0));
                }
            }
            if (Build.VERSION.SDK_INT < 21) {
                if (!obtainStyledAttributes.hasValue(0) || (colorStateList = obtainStyledAttributes.getColorStateList(0)) == null) {
                    i2 = 0;
                    i3 = 0;
                } else {
                    i3 = colorStateList.getDefaultColor();
                    i2 = colorStateList.getColorForState(new int[]{-16842910}, i3);
                }
                int A05 = C016907y.A05(C41691tw.A00(context, R.attr.colorControlHighlight, R.color.buttonPressOverlay), i3);
                AnonymousClass028.A0M(new ColorStateList(new int[][]{new int[]{16842919}, new int[]{16842910}, new int[]{16842908, 16842919}, new int[]{-16842910}, new int[0]}, new int[]{A05, i3, A05, i2, i3}), this);
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass0BS, android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        ColorStateList colorStateList;
        super.drawableStateChanged();
        if (!this.A0E && (colorStateList = this.A04) != null) {
            int colorForState = colorStateList.getColorForState(getDrawableState(), 0);
            Paint paint = this.A06;
            if (colorForState != paint.getColor()) {
                paint.setColor(colorForState);
                invalidate();
            }
        }
    }

    @Override // com.whatsapp.wds.components.button.WDSButton, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        super.onDraw(canvas);
        if (!this.A0E && canvas != null && this.A04 != null && (i = this.A03) > 0) {
            Rect rect = this.A07;
            rect.set(getBackground().getBounds());
            RectF rectF = this.A08;
            float f = ((float) i) / 2.0f;
            float f2 = (float) this.A01;
            float f3 = (float) this.A02;
            rectF.set(((float) rect.left) + f + f2, ((float) rect.top) + f + f3, (((float) rect.right) - f) - f2, (((float) rect.bottom) - f) - f3);
            float f4 = ((float) this.A00) - f;
            canvas.drawRoundRect(rectF, f4, f4, this.A06);
        }
    }
}
