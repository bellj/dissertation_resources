package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C73873gw;
import android.animation.Animator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class CircularRevealView extends FrameLayout implements AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public Animator.AnimatorListener A05;
    public Paint A06;
    public Path A07;
    public RectF A08;
    public Animation.AnimationListener A09;
    public C73873gw A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;

    public CircularRevealView(Context context) {
        super(context);
        if (!this.A0D) {
            this.A0D = true;
            generatedComponent();
        }
        C12960it.A1C(this);
    }

    public CircularRevealView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        C12960it.A1C(this);
    }

    public CircularRevealView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0D) {
            this.A0D = true;
            generatedComponent();
        }
        C12960it.A1C(this);
    }

    public CircularRevealView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A0D) {
            this.A0D = true;
            generatedComponent();
        }
        C12960it.A1C(this);
    }

    public CircularRevealView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A0D) {
            this.A0D = true;
            generatedComponent();
        }
    }

    public void A00() {
        if (Build.VERSION.SDK_INT < 21) {
            if (this.A0E) {
                super.clearAnimation();
            }
            setWillNotDraw(false);
            setBackgroundColor(0);
            C73873gw r2 = new C73873gw(this, false);
            this.A0A = r2;
            r2.setDuration((long) this.A04);
            this.A0A.setAnimationListener(this.A09);
            startAnimation(this.A0A);
            return;
        }
        setVisibility(0);
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, this.A01, this.A02, 0.0f, (float) Math.max(getWidth(), getHeight()));
        createCircularReveal.setDuration((long) this.A04);
        createCircularReveal.addListener(this.A05);
        createCircularReveal.start();
    }

    public void A01(Animation animation) {
        if (this.A0E) {
            super.clearAnimation();
        }
        setBackgroundColor(0);
        animation.setDuration((long) this.A04);
        animation.setAnimationListener(this.A09);
        startAnimation(animation);
    }

    public void A02(boolean z) {
        if (z) {
            if (Build.VERSION.SDK_INT < 21) {
                if (this.A0E) {
                    super.clearAnimation();
                }
                C73873gw r2 = new C73873gw(this, true);
                this.A0A = r2;
                r2.setDuration((long) this.A04);
                this.A0A.setAnimationListener(this.A09);
                startAnimation(this.A0A);
                return;
            }
            int max = Math.max(getWidth(), getHeight());
            if (isAttachedToWindow()) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, this.A01, this.A02, (float) max, 0.0f);
                createCircularReveal.setDuration((long) this.A04);
                C12990iw.A0w(createCircularReveal, this, 10);
                createCircularReveal.addListener(this.A05);
                createCircularReveal.start();
                return;
            }
        }
        setVisibility(8);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (Build.VERSION.SDK_INT < 21) {
            clearAnimation();
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i = Build.VERSION.SDK_INT;
        if (i < 21 && this.A03 != -1) {
            RectF A0B = C12960it.A0B(this);
            Paint paint = this.A06;
            C12970iu.A16(this.A03, paint);
            canvas.drawArc(A0B, 0.0f, 360.0f, true, paint);
        } else if (i < 21 && i >= 18) {
            RectF A0B2 = C12960it.A0B(this);
            C12970iu.A16(this.A03, this.A06);
            Path path = this.A07;
            path.reset();
            path.addArc(A0B2, 0.0f, 360.0f);
            canvas.clipPath(path);
        }
    }

    public void setColor(int i) {
        this.A03 = i;
    }

    public void setDuration(int i) {
        this.A04 = i;
    }

    public void setShouldClearOnRestart(boolean z) {
        this.A0E = z;
    }
}
