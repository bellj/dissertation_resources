package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QV;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class FloatingActionButton extends AnonymousClass2QV implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public FloatingActionButton(Context context) {
        super(context, null);
        A05();
        A06(context, null);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A05();
        A06(context, attributeSet);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A05();
        A06(context, attributeSet);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A05();
    }

    public void A05() {
        if (!this.A02) {
            this.A02 = true;
            this.A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    public final void A06(Context context, AttributeSet attributeSet) {
        setScaleType(ImageView.ScaleType.CENTER);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A09);
            int resourceId = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId != 0) {
                setContentDescription(this.A00.A09(resourceId));
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }
}
