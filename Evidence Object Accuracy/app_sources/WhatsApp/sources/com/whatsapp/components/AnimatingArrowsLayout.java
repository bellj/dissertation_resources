package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12980iv;
import X.C12990iw;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes2.dex */
public class AnimatingArrowsLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public List A01;
    public boolean A02;
    public final AnimatorSet A03;

    public AnimatingArrowsLayout(Context context) {
        this(context, null);
    }

    public AnimatingArrowsLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A03 = new AnimatorSet();
    }

    public AnimatingArrowsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A03.start();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnimatorSet animatorSet = this.A03;
        animatorSet.removeAllListeners();
        animatorSet.end();
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        int i = 0;
        this.A01 = Arrays.asList(getChildAt(3), getChildAt(2), getChildAt(1), getChildAt(0));
        ArrayList A0w = C12980iv.A0w(4);
        do {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.A01.get(i), "alpha", 0.0f, 0.6f, 0.0f);
            ofFloat.setDuration(750L);
            ofFloat.setStartDelay((long) (i * 100));
            A0w.add(ofFloat);
            i++;
        } while (i < 4);
        AnimatorSet animatorSet = this.A03;
        animatorSet.playTogether(A0w);
        C12990iw.A0w(animatorSet, this, 8);
        animatorSet.start();
    }
}
