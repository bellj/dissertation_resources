package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass028;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C42631vX;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class WaSwitchView extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final SwitchCompat A02;
    public final WaTextView A03;
    public final WaTextView A04;

    public WaSwitchView(Context context) {
        this(context, null);
    }

    public WaSwitchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public WaSwitchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        int i3;
        int i4;
        int paddingLeft;
        int paddingTop;
        int paddingRight;
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        int i5 = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0W, 0, 0);
            try {
                int resourceId = obtainStyledAttributes.getResourceId(2, 0);
                i3 = obtainStyledAttributes.getResourceId(0, 0);
                i2 = obtainStyledAttributes.getResourceId(3, R.style.ListItemTitle);
                i4 = obtainStyledAttributes.getResourceId(1, 0);
                obtainStyledAttributes.recycle();
                i5 = resourceId;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            i4 = 0;
            i3 = 0;
            i2 = 0;
        }
        LinearLayout.inflate(context, R.layout.wa_switch_view, this);
        C42631vX.A00(this);
        int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.info_screen_padding);
        if (getPaddingLeft() == 0) {
            paddingLeft = dimensionPixelOffset;
        } else {
            paddingLeft = getPaddingLeft();
        }
        if (getPaddingTop() == 0) {
            paddingTop = dimensionPixelOffset;
        } else {
            paddingTop = getPaddingTop();
        }
        if (getPaddingRight() == 0) {
            paddingRight = getResources().getDimensionPixelOffset(R.dimen.info_screen_right_padding);
        } else {
            paddingRight = getPaddingRight();
        }
        setPadding(paddingLeft, paddingTop, paddingRight, getPaddingBottom() != 0 ? getPaddingBottom() : dimensionPixelOffset);
        setMinimumHeight(getResources().getDimensionPixelOffset(R.dimen.info_screen_row));
        WaTextView A0N = C12960it.A0N(this, R.id.switch_view_title);
        this.A04 = A0N;
        if (i5 != 0) {
            C12970iu.A19(context, A0N, i5);
        }
        if (i2 != 0) {
            if (Build.VERSION.SDK_INT > 22) {
                A0N.setTextAppearance(i2);
            } else {
                A0N.setTextAppearance(getContext(), i2);
            }
        }
        WaTextView A0N2 = C12960it.A0N(this, R.id.switch_view_description);
        this.A03 = A0N2;
        if (i3 != 0) {
            C12970iu.A19(context, A0N2, i3);
        }
        if (i4 != 0) {
            if (Build.VERSION.SDK_INT > 22) {
                A0N2.setTextAppearance(i4);
            } else {
                A0N2.setTextAppearance(getContext(), i4);
            }
        }
        this.A02 = (SwitchCompat) AnonymousClass028.A0D(this, R.id.switch_view_toggle);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    public void setChecked(boolean z) {
        this.A02.setChecked(z);
    }

    public void setDescription(CharSequence charSequence) {
        this.A03.setText(charSequence);
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.A02.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    public void setSwitchClickable(boolean z) {
        this.A02.setClickable(z);
    }

    public void setTitle(CharSequence charSequence) {
        this.A04.setText(charSequence);
    }
}
