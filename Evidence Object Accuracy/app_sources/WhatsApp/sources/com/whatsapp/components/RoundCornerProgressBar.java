package com.whatsapp.components;

import X.AbstractC52492aw;
import X.AnonymousClass018;
import X.AnonymousClass2GZ;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/* loaded from: classes2.dex */
public class RoundCornerProgressBar extends AbstractC52492aw {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AnonymousClass018 A05;
    public boolean A06;
    public final Paint A07;
    public final RectF A08;

    public RoundCornerProgressBar(Context context) {
        this(context, null);
    }

    public RoundCornerProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RoundCornerProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A06 = true;
        this.A00 = 0.0f;
        this.A02 = 0;
        this.A04 = 10;
        this.A03 = -15561602;
        this.A01 = -920588;
        this.A07 = C12990iw.A0G(1);
        this.A08 = C12980iv.A0K();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0H);
            this.A04 = obtainStyledAttributes.getDimensionPixelSize(2, this.A04);
            this.A03 = obtainStyledAttributes.getInteger(1, this.A03);
            this.A01 = obtainStyledAttributes.getInteger(0, this.A01);
            obtainStyledAttributes.recycle();
        }
    }

    public int getProgress() {
        return this.A02;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        float f;
        int width = getWidth();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        float f2 = (this.A00 / 100.0f) * ((float) ((width - paddingLeft) - paddingRight));
        if (isInEditMode() || C28141Kv.A01(this.A05)) {
            f = ((float) paddingLeft) + f2;
        } else {
            f = (((float) width) - f2) - ((float) paddingRight);
        }
        int paddingTop = getPaddingTop() + (C12960it.A03(this) >> 1);
        Paint paint = this.A07;
        C12970iu.A16(this.A01, paint);
        RectF rectF = this.A08;
        int i = this.A04 >> 1;
        float f3 = (float) (paddingTop - i);
        float f4 = (float) (i + paddingTop);
        rectF.set(0.0f, f3, C12990iw.A02(this), f4);
        canvas.drawRoundRect(rectF, rectF.height() / 2.0f, rectF.height() / 2.0f, paint);
        paint.setColor(this.A03);
        if (isInEditMode() || C28141Kv.A01(this.A05)) {
            rectF.set((float) paddingLeft, f3, f, f4);
        } else {
            rectF.set(f, f3, (float) (width - paddingRight), f4);
        }
        canvas.drawRoundRect(rectF, rectF.height() / 2.0f, rectF.height() / 2.0f, paint);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int size;
        if (View.MeasureSpec.getMode(i2) == 0) {
            size = getPaddingTop() + this.A04 + getPaddingBottom();
        } else {
            size = View.MeasureSpec.getSize(i2);
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), size);
    }

    public void setProgress(int i) {
        if (i < 0 || i > 100) {
            throw C12970iu.A0f("Progress must be between 0 and 100 inclusive");
        } else if (i != this.A02) {
            this.A02 = i;
            if (!this.A06) {
                this.A00 = (float) i;
            } else if (i > 0) {
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, (float) i);
                ofFloat.setDuration((long) ((int) Math.max(200.0f, (((float) this.A02) / 100.0f) * 650.0f)));
                ofFloat.setInterpolator(new DecelerateInterpolator());
                C12980iv.A11(ofFloat, this, 6);
                C12990iw.A0w(ofFloat, this, 11);
                ofFloat.setStartDelay(300);
                ofFloat.start();
                return;
            } else {
                this.A00 = (float) i;
                this.A06 = false;
            }
            invalidate();
        }
    }
}
