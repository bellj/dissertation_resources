package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass3MD;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

/* loaded from: classes2.dex */
public class TextAndDateLayout extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public boolean A04;

    public TextAndDateLayout(Context context) {
        super(context);
        A00();
    }

    public TextAndDateLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(attributeSet);
    }

    public TextAndDateLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(attributeSet);
    }

    public TextAndDateLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        A01(attributeSet);
    }

    public TextAndDateLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A04) {
            this.A04 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public final void A01(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0N, 0, 0);
            try {
                this.A00 = obtainStyledAttributes.getInt(1, 0);
                this.A03 = obtainStyledAttributes.getBoolean(0, false);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    private int getLastParagraphDirection() {
        Layout layout = ((TextView) getChildAt(0)).getLayout();
        return layout.getParagraphDirection(C12970iu.A02(layout, this));
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        setTextViewStyle(this.A00);
        if (this.A03) {
            TextView textView = (TextView) getChildAt(0);
            textView.addTextChangedListener(new AnonymousClass3MD(textView, this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e3, code lost:
        if (r13.A03 == false) goto L_0x004d;
     */
    @Override // android.widget.FrameLayout, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 297
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.components.TextAndDateLayout.onMeasure(int, int):void");
    }

    public void setMaxTextLineCount(int i) {
        if (this.A00 != i) {
            invalidate();
            setTextViewStyle(i);
        }
        this.A00 = i;
    }

    private void setTextViewStyle(int i) {
        TextView textView = (TextView) getChildAt(0);
        if (i > 0) {
            textView.setMaxLines(i);
            textView.setEllipsize(TextUtils.TruncateAt.END);
        }
    }
}
