package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P7;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* loaded from: classes2.dex */
public class MainChildCoordinatorLayout extends CoordinatorLayout implements AnonymousClass004 {
    public int A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public MainChildCoordinatorLayout(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    public MainChildCoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0C);
            this.A00 = obtainStyledAttributes.getResourceId(0, 0);
            obtainStyledAttributes.recycle();
        }
    }

    public MainChildCoordinatorLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0C);
            this.A00 = obtainStyledAttributes.getResourceId(0, 0);
            obtainStyledAttributes.recycle();
        }
    }

    public MainChildCoordinatorLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout
    public boolean A0I(View view, int i, int i2) {
        return (view != null && view.getId() == this.A00) || super.A0I(view, i, i2);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }
}
