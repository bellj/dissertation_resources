package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2GZ;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C28141Kv;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/* loaded from: classes2.dex */
public class SegmentedProgressBar extends View implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public AnimatorSet A08;
    public Bitmap A09;
    public AnonymousClass018 A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;
    public float[] A0D;
    public int[] A0E;
    public final Paint A0F;
    public final RectF A0G;

    public SegmentedProgressBar(Context context) {
        this(context, null);
    }

    public SegmentedProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SegmentedProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A0C) {
            this.A0C = true;
            this.A0A = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A0F = C12990iw.A0G(1);
        this.A0G = C12980iv.A0K();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0I);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(4, 0);
            this.A07 = dimensionPixelSize;
            if (dimensionPixelSize % 2 == 1) {
                this.A07 = dimensionPixelSize + 1;
            }
            this.A05 = obtainStyledAttributes.getInteger(2, 0);
            this.A04 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            this.A03 = obtainStyledAttributes.getInteger(0, 0);
            this.A02 = obtainStyledAttributes.getInteger(3, 0);
            obtainStyledAttributes.recycle();
        }
        this.A01 = 1000;
        this.A00 = 300;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int width = getWidth();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int i = (width - paddingLeft) - paddingRight;
        int paddingTop = getPaddingTop() + (C12960it.A03(this) >> 1);
        Paint paint = this.A0F;
        C12970iu.A16(this.A02, paint);
        RectF rectF = this.A0G;
        int i2 = this.A07;
        int i3 = i2 >> 1;
        float f = (float) (paddingTop - i3);
        float f2 = (float) (i3 + paddingTop);
        rectF.set(0.0f, f, C12990iw.A02(this), f2);
        canvas.drawRect(rectF, paint);
        if (this.A0D != null && this.A0E != null) {
            int i4 = 0;
            float f3 = 0.0f;
            while (true) {
                float[] fArr = this.A0D;
                if (i4 >= fArr.length) {
                    break;
                }
                if (fArr[i4] != 0.0f) {
                    paint.setColor(this.A0E[i4]);
                    float f4 = (this.A0D[i4] / 100.0f) * ((float) i);
                    if (isInEditMode() || C28141Kv.A01(this.A0A)) {
                        float f5 = (float) paddingLeft;
                        rectF.set(f5 + f3, f, f5 + f4 + f3, f2);
                        canvas.drawRect(rectF, paint);
                        if (!(i4 == this.A0D.length - 1 && this.A06 == 100)) {
                            paint.setColor(this.A03);
                            float f6 = f4 + f3;
                            rectF.set(f6 - ((float) this.A04), f, f6, f2);
                            canvas.drawRect(rectF, paint);
                        }
                        f3 += f4;
                    } else {
                        float f7 = ((float) (width - paddingRight)) - f3;
                        float f8 = f7 - f4;
                        rectF.set(f8, f, f7, f2);
                        canvas.drawRect(rectF, paint);
                        if (!(i4 == this.A0D.length - 1 && this.A06 == 100)) {
                            paint.setColor(this.A03);
                            rectF.set(f8, f, ((float) this.A04) + f8, f2);
                            canvas.drawRect(rectF, paint);
                        }
                        f3 += f4;
                    }
                }
                i4++;
            }
        }
        int i5 = this.A05;
        paint.setColor(i5);
        if (this.A09 == null) {
            int width2 = getWidth();
            float height = rectF.height() / 2.0f;
            Bitmap createBitmap = Bitmap.createBitmap(width2, i2, Bitmap.Config.ARGB_8888);
            this.A09 = createBitmap;
            Canvas canvas2 = new Canvas(createBitmap);
            Paint A0A = C12960it.A0A();
            A0A.setColor(i5);
            A0A.setXfermode(null);
            float f9 = (float) width2;
            float f10 = (float) i2;
            canvas2.drawRect(0.0f, 0.0f, f9, f10, A0A);
            C12990iw.A14(A0A, PorterDuff.Mode.CLEAR);
            canvas2.drawRoundRect(new RectF(0.0f, 0.0f, f9, f10), height, height, A0A);
        }
        canvas.drawBitmap(this.A09, 0.0f, 0.0f, paint);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int size;
        if (View.MeasureSpec.getMode(i2) == 0) {
            size = getPaddingTop() + this.A07 + getPaddingBottom();
        } else {
            size = View.MeasureSpec.getSize(i2);
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), size);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.A09 = null;
    }
}
