package com.whatsapp.components.button;

import X.AbstractC469028d;
import X.AbstractC51352Ua;
import X.AnonymousClass2GZ;
import X.C73043fa;
import X.C73753gk;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class ThumbnailButton extends AbstractC51352Ua {
    public static final int A0A;
    public float A00;
    public float A01;
    public float A02 = 0.0f;
    public int A03;
    public int A04 = A0A;
    public Paint A05;
    public AbstractC469028d A06;
    public boolean A07;
    public final Rect A08 = new Rect();
    public final RectF A09 = new RectF();

    static {
        int i;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 19 || (i2 < 21 && "samsung".equalsIgnoreCase(Build.MANUFACTURER))) {
            i = 1711315455;
        } else {
            i = 419430400;
        }
        A0A = i;
    }

    public ThumbnailButton(Context context) {
        super(context);
        A00(context, null);
    }

    public ThumbnailButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet);
    }

    public ThumbnailButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet);
    }

    private void A00(Context context, AttributeSet attributeSet) {
        setBackgroundDrawable(new C73043fa());
        boolean z = false;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0O);
            this.A02 = obtainStyledAttributes.getDimension(4, this.A02);
            this.A00 = obtainStyledAttributes.getFloat(0, this.A00);
            this.A04 = obtainStyledAttributes.getInteger(5, this.A04);
            this.A01 = obtainStyledAttributes.getDimension(2, this.A01);
            this.A03 = obtainStyledAttributes.getInteger(1, this.A03);
            this.A07 = obtainStyledAttributes.getBoolean(3, this.A07);
            if (Build.VERSION.SDK_INT >= 21) {
                z = obtainStyledAttributes.getBoolean(6, false);
            }
            obtainStyledAttributes.recycle();
        }
        Paint paint = new Paint();
        this.A05 = paint;
        paint.setAntiAlias(true);
        this.A05.setDither(true);
        this.A05.setFilterBitmap(true);
        this.A05.setColor(-1);
        if (Build.VERSION.SDK_INT >= 21 && z) {
            setOutlineProvider(new C73753gk());
        }
    }

    public void A02(Canvas canvas) {
        int i;
        if (this.A01 > 0.0f && (i = this.A03) != 0) {
            this.A05.setColor(i);
            this.A05.setStrokeWidth(this.A01);
            this.A05.setStyle(Paint.Style.STROKE);
            AbstractC469028d r1 = this.A06;
            if (r1 != null) {
                canvas.drawPath((Path) r1.apply(this.A09), this.A05);
                return;
            }
            float f = this.A02;
            int i2 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
            RectF rectF = this.A09;
            Paint paint = this.A05;
            if (i2 >= 0) {
                canvas.drawRoundRect(rectF, f, f, paint);
            } else {
                canvas.drawOval(rectF, paint);
            }
        }
    }

    public float getBorderSize() {
        return this.A01;
    }

    public int getBorderSizeAdjustment() {
        return (int) ((this.A01 + 2.0f) / 2.0f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a5 A[Catch: RuntimeException -> 0x019f, TryCatch #0 {RuntimeException -> 0x019f, blocks: (B:8:0x0043, B:10:0x004b, B:12:0x0051, B:14:0x0068, B:16:0x007d, B:17:0x008e, B:18:0x0091, B:20:0x00a5, B:21:0x00b4, B:23:0x00bd, B:25:0x00c3, B:26:0x00cd, B:28:0x00d3, B:30:0x00e0, B:31:0x00e5, B:34:0x00f8, B:35:0x0102, B:37:0x010a, B:38:0x0120, B:40:0x012a, B:41:0x0146, B:43:0x0159, B:44:0x017c), top: B:69:0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00bd A[Catch: RuntimeException -> 0x019f, TryCatch #0 {RuntimeException -> 0x019f, blocks: (B:8:0x0043, B:10:0x004b, B:12:0x0051, B:14:0x0068, B:16:0x007d, B:17:0x008e, B:18:0x0091, B:20:0x00a5, B:21:0x00b4, B:23:0x00bd, B:25:0x00c3, B:26:0x00cd, B:28:0x00d3, B:30:0x00e0, B:31:0x00e5, B:34:0x00f8, B:35:0x0102, B:37:0x010a, B:38:0x0120, B:40:0x012a, B:41:0x0146, B:43:0x0159, B:44:0x017c), top: B:69:0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cd A[Catch: RuntimeException -> 0x019f, TryCatch #0 {RuntimeException -> 0x019f, blocks: (B:8:0x0043, B:10:0x004b, B:12:0x0051, B:14:0x0068, B:16:0x007d, B:17:0x008e, B:18:0x0091, B:20:0x00a5, B:21:0x00b4, B:23:0x00bd, B:25:0x00c3, B:26:0x00cd, B:28:0x00d3, B:30:0x00e0, B:31:0x00e5, B:34:0x00f8, B:35:0x0102, B:37:0x010a, B:38:0x0120, B:40:0x012a, B:41:0x0146, B:43:0x0159, B:44:0x017c), top: B:69:0x0043 }] */
    @Override // com.whatsapp.WaImageView, android.widget.ImageView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r12) {
        /*
        // Method dump skipped, instructions count: 527
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.components.button.ThumbnailButton.onDraw(android.graphics.Canvas):void");
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        if (this.A00 == 1.0f) {
            int defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
            setMeasuredDimension(defaultSize, defaultSize);
            return;
        }
        super.onMeasure(i, i2);
    }

    public void setBorderColor(int i) {
        this.A03 = i;
    }

    public void setBorderSize(float f) {
        this.A01 = f;
    }

    public void setClipPathProducer(AbstractC469028d r1) {
        this.A06 = r1;
    }

    public void setCornerRadius(float f) {
        this.A02 = f;
    }

    public void setForegroundOnly(boolean z) {
        this.A07 = z;
    }
}
