package com.whatsapp.components;

import X.AbstractC14640lm;
import X.ActivityC13810kN;
import X.AnonymousClass004;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass31K;
import X.C12980iv;
import X.C19990v2;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.whatsapp.util.ViewOnClickCListenerShape3S0400000_I1;

/* loaded from: classes2.dex */
public class InviteViaLinkView extends RelativeLayout implements AnonymousClass004 {
    public C19990v2 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public InviteViaLinkView(Context context) {
        this(context, null);
    }

    public InviteViaLinkView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public InviteViaLinkView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12980iv.A0c(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setupOnClick(AbstractC14640lm r7, ActivityC13810kN r8, AnonymousClass31K r9) {
        setOnClickListener(new ViewOnClickCListenerShape3S0400000_I1(this, r9, r7, r8, 0));
    }
}
