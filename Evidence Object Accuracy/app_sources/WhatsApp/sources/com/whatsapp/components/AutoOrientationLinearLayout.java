package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;

/* loaded from: classes2.dex */
public final class AutoOrientationLinearLayout extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AutoOrientationLinearLayout(Context context) {
        this(context, null);
    }

    public AutoOrientationLinearLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
    }

    public AutoOrientationLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AutoOrientationLinearLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AutoOrientationLinearLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        post(new RunnableBRunnable0Shape15S0100000_I1_1(this, 21));
    }
}
