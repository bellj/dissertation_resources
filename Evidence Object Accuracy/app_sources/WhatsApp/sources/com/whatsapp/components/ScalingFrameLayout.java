package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12990iw;
import X.C28141Kv;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class ScalingFrameLayout extends FrameLayout implements AnonymousClass004 {
    public float A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public ScalingFrameLayout(Context context) {
        super(context);
        A00();
        this.A00 = 1.0f;
    }

    public ScalingFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        this.A00 = 1.0f;
    }

    public ScalingFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        this.A00 = 1.0f;
    }

    public ScalingFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
        this.A00 = 1.0f;
    }

    public ScalingFrameLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A03) {
            this.A03 = true;
            this.A01 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        canvas.save();
        if (C28141Kv.A00(this.A01)) {
            canvas.translate(C12990iw.A02(this), 0.0f);
        }
        float f = this.A00;
        canvas.scale(f, f);
        if (C28141Kv.A00(this.A01)) {
            canvas.translate((float) (-getWidth()), 0.0f);
        }
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        super.onMeasure(View.MeasureSpec.makeMeasureSpec((int) (((float) View.MeasureSpec.getSize(i)) / this.A00), mode), View.MeasureSpec.makeMeasureSpec((int) (((float) View.MeasureSpec.getSize(i2)) / this.A00), mode2));
        setMeasuredDimension((int) (((float) getMeasuredWidth()) * this.A00), (int) (((float) getMeasuredHeight()) * this.A00));
    }

    public void setScale(float f) {
        this.A00 = f;
    }
}
