package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass2P7;
import X.C12970iu;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class ConversationListRowHeaderView extends LinearLayout implements AnonymousClass004 {
    public TextEmojiLabel A00;
    public WaTextView A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public ConversationListRowHeaderView(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationListRowHeaderView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0);
        A00(context);
    }

    public ConversationListRowHeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationListRowHeaderView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        A00(context);
    }

    public ConversationListRowHeaderView(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    public final void A00(Context context) {
        LinearLayout.inflate(context, R.layout.conversation_list_row_header, this);
        this.A00 = C12970iu.A0U(this, R.id.conversations_row_contact_name);
        this.A01 = (WaTextView) findViewById(R.id.conversations_row_date);
        setOrientation(0);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public TextEmojiLabel getContactNameView() {
        return this.A00;
    }

    public WaTextView getDateView() {
        return this.A01;
    }
}
