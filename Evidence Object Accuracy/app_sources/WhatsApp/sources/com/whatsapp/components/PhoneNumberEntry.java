package com.whatsapp.components;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass2H4;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.AnonymousClass35y;
import X.AnonymousClass3MG;
import X.AnonymousClass4WI;
import X.AnonymousClass534;
import X.C12960it;
import X.C22680zT;
import X.C42941w9;
import X.C52422am;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class PhoneNumberEntry extends LinearLayout implements AnonymousClass2H4, AnonymousClass004 {
    public TextWatcher A00;
    public C22680zT A01;
    public WaEditText A02;
    public WaEditText A03;
    public AnonymousClass4WI A04;
    public AnonymousClass01d A05;
    public AnonymousClass2P7 A06;
    public String A07;
    public boolean A08;

    public PhoneNumberEntry(Context context) {
        super(context);
        A00();
        A01(context, null);
    }

    public PhoneNumberEntry(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        A01(context, attributeSet);
    }

    public PhoneNumberEntry(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        A01(context, attributeSet);
    }

    public PhoneNumberEntry(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A05 = C12960it.A0Q(A00);
            this.A01 = (C22680zT) A00.AGW.get();
        }
    }

    public final void A01(Context context, AttributeSet attributeSet) {
        setSaveEnabled(true);
        AnonymousClass028.A0c(this, 0);
        LinearLayout.inflate(context, R.layout.phone_number_entry, this);
        this.A02 = (WaEditText) findViewById(R.id.registration_cc);
        WaEditText waEditText = (WaEditText) findViewById(R.id.registration_phone);
        this.A03 = waEditText;
        waEditText.setSaveEnabled(false);
        this.A02.setSaveEnabled(false);
        this.A02.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        this.A03.setFilters(new InputFilter[]{new InputFilter.LengthFilter(17)});
        C42941w9.A03(this.A03);
        AnonymousClass534 r2 = new AnonymousClass534(this);
        WaEditText waEditText2 = this.A02;
        waEditText2.A01 = r2;
        this.A03.A01 = r2;
        waEditText2.addTextChangedListener(new AnonymousClass35y(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0F);
        ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(0);
        if (colorStateList != null) {
            AnonymousClass028.A0M(colorStateList, this.A03);
            AnonymousClass028.A0M(colorStateList, this.A02);
        }
        obtainStyledAttributes.recycle();
    }

    public void A02(String str) {
        this.A07 = str;
        TextWatcher textWatcher = this.A00;
        if (textWatcher != null) {
            this.A03.removeTextChangedListener(textWatcher);
        }
        try {
            AnonymousClass3MG r1 = new AnonymousClass3MG(this, str);
            this.A00 = r1;
            this.A03.addTextChangedListener(r1);
        } catch (NullPointerException unused) {
            Log.e("PhoneNumberEntry/formatter exception");
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public WaEditText getCountryCodeField() {
        return this.A02;
    }

    public WaEditText getPhoneNumberField() {
        return this.A03;
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C52422am r3 = (C52422am) parcelable;
        super.onRestoreInstanceState(r3.getSuperState());
        this.A02.setText(r3.A00);
        this.A03.setText(r3.A01);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Editable text = this.A02.getText();
        AnonymousClass009.A05(text);
        String obj = text.toString();
        Editable text2 = this.A03.getText();
        AnonymousClass009.A05(text2);
        return new C52422am(onSaveInstanceState, obj, text2.toString());
    }

    public void setOnPhoneNumberChangeListener(AnonymousClass4WI r1) {
        this.A04 = r1;
    }
}
