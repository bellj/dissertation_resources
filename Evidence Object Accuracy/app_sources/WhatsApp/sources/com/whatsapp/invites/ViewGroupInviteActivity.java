package com.whatsapp.invites;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC33111dN;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103144qH;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C15810nw;
import X.C15880o3;
import X.C17220qS;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19990v2;
import X.C20660w7;
import X.C20710wC;
import X.C21270x9;
import X.C21320xE;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27151Gf;
import X.C28581Od;
import X.C32511cH;
import X.C32911cw;
import X.C469728k;
import X.C617932n;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ViewGroupInviteActivity extends ActivityC13790kL implements AbstractC33111dN {
    public int A00;
    public View A01;
    public ViewGroup A02;
    public ViewGroup A03;
    public ViewGroup A04;
    public ImageView A05;
    public TextView A06;
    public TextView A07;
    public C15550nR A08;
    public C15610nY A09;
    public AnonymousClass1J1 A0A;
    public C21270x9 A0B;
    public AnonymousClass018 A0C;
    public C19990v2 A0D;
    public C21320xE A0E;
    public C15650ng A0F;
    public C15600nX A0G;
    public C20710wC A0H;
    public C469728k A0I;
    public C15580nU A0J;
    public UserJid A0K;
    public C17220qS A0L;
    public C20660w7 A0M;
    public C32511cH A0N;
    public C28581Od A0O;
    public Runnable A0P;
    public boolean A0Q;
    public boolean A0R;
    public final C27151Gf A0S;

    public ViewGroupInviteActivity() {
        this(0);
        this.A0S = new C32911cw(this);
    }

    public ViewGroupInviteActivity(int i) {
        this.A0R = false;
        A0R(new C103144qH(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0R) {
            this.A0R = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0D = (C19990v2) r1.A3M.get();
            this.A0M = (C20660w7) r1.AIB.get();
            this.A0B = (C21270x9) r1.A4A.get();
            this.A0L = (C17220qS) r1.ABt.get();
            this.A08 = (C15550nR) r1.A45.get();
            this.A09 = (C15610nY) r1.AMe.get();
            this.A0C = (AnonymousClass018) r1.ANb.get();
            this.A0F = (C15650ng) r1.A4m.get();
            this.A0H = (C20710wC) r1.A8m.get();
            this.A0E = (C21320xE) r1.A4Y.get();
            this.A0G = (C15600nX) r1.A8x.get();
        }
    }

    public final void A2e(int i) {
        this.A06.setText(i);
        this.A04.setVisibility(4);
        this.A02.setVisibility(0);
        this.A03.setVisibility(4);
        this.A01.setVisibility(8);
    }

    @Override // X.AbstractC33111dN
    public void AVU(UserJid userJid) {
        this.A07.setText(R.string.revoking_invite);
        this.A04.setVisibility(0);
        this.A03.setVisibility(4);
        AbstractC14440lR r1 = ((ActivityC13830kP) this).A05;
        C14900mE r4 = ((ActivityC13810kN) this).A05;
        C20660w7 r8 = this.A0M;
        C15580nU r6 = this.A0J;
        AnonymousClass009.A05(r6);
        r1.Aaz(new C617932n(r4, this, r6, userJid, r8), new Void[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006f, code lost:
        if (r1 == false) goto L_0x0071;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r20) {
        /*
        // Method dump skipped, instructions count: 510
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.invites.ViewGroupInviteActivity.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        Runnable runnable = this.A0P;
        if (runnable != null) {
            ((ActivityC13810kN) this).A05.A0G(runnable);
            this.A0P = null;
        }
        this.A0E.A04(this.A0S);
        this.A0A.A00();
    }
}
