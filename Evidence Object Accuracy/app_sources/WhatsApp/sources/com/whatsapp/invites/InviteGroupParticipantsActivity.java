package com.whatsapp.invites;

import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass131;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass2GF;
import X.AnonymousClass37J;
import X.AnonymousClass3NW;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15370n3;
import X.C15550nR;
import X.C15580nU;
import X.C15610nY;
import X.C16170oZ;
import X.C16630pM;
import X.C19990v2;
import X.C20710wC;
import X.C21270x9;
import X.C231510o;
import X.C252718t;
import X.C253719d;
import X.C27531Hw;
import X.C34271fr;
import X.C36271jb;
import X.C54232gO;
import X.C87934Dp;
import X.C91394Ro;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class InviteGroupParticipantsActivity extends ActivityC13790kL {
    public LayoutInflater A00;
    public ImageView A01;
    public C16170oZ A02;
    public C15550nR A03;
    public C15610nY A04;
    public AnonymousClass1J1 A05;
    public C21270x9 A06;
    public AnonymousClass131 A07;
    public AnonymousClass018 A08;
    public C19990v2 A09;
    public C15370n3 A0A;
    public C231510o A0B;
    public AnonymousClass193 A0C;
    public C253719d A0D;
    public C20710wC A0E;
    public C36271jb A0F;
    public MentionableEntry A0G;
    public C16630pM A0H;
    public List A0I;
    public boolean A0J;
    public byte[] A0K;

    public InviteGroupParticipantsActivity() {
        this(0);
    }

    public InviteGroupParticipantsActivity(int i) {
        this.A0J = false;
        ActivityC13830kP.A1P(this, 82);
    }

    public static C34271fr A02(Activity activity, Intent intent, View view, int i) {
        C34271fr A00 = C34271fr.A00(view, view.getResources().getText(R.string.invite_cancelled), 0);
        A00.A07(A00.A02.getText(R.string.undo), new ViewOnClickCListenerShape2S0201000_I1(activity, intent, i, 3));
        A00.A06(AnonymousClass00T.A00(view.getContext(), R.color.group_invite_undo_accent));
        return A00;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0J) {
            this.A0J = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0D = (C253719d) A1M.A8V.get();
            this.A09 = C12980iv.A0c(A1M);
            this.A02 = (C16170oZ) A1M.AM4.get();
            this.A0B = (C231510o) A1M.AHO.get();
            this.A06 = C12970iu.A0W(A1M);
            this.A03 = C12960it.A0O(A1M);
            this.A04 = C12960it.A0P(A1M);
            this.A08 = C12960it.A0R(A1M);
            this.A0E = C12980iv.A0e(A1M);
            this.A0C = (AnonymousClass193) A1M.A6S.get();
            this.A0H = C12990iw.A0e(A1M);
            this.A07 = (AnonymousClass131) A1M.A49.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.app_name);
        setContentView(R.layout.invite_group_select_layout);
        this.A00 = LayoutInflater.from(this);
        this.A05 = this.A06.A04(this, "invite-group-participants-activity");
        this.A0G = (MentionableEntry) findViewById(R.id.comment);
        C252718t r3 = ((ActivityC13790kL) this).A0D;
        AbstractC15710nm r10 = ((ActivityC13810kN) this).A03;
        AnonymousClass19M r14 = ((ActivityC13810kN) this).A0B;
        C231510o r15 = this.A0B;
        AnonymousClass01d r11 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r13 = this.A08;
        AnonymousClass193 r2 = this.A0C;
        this.A0F = new C36271jb(this, findViewById(R.id.main), r10, r11, ((ActivityC13810kN) this).A09, r13, r14, r15, r2, null, this.A0H, r3);
        getWindow().setSoftInputMode(3);
        this.A0G.requestFocus();
        TextView A0M = C12970iu.A0M(this, R.id.group_name);
        this.A01 = (ImageView) findViewById(R.id.group_photo);
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        Iterator it = ActivityC13790kL.A0X(this).iterator();
        while (it.hasNext()) {
            AbstractC14640lm A0b = C12990iw.A0b(it);
            A0l.add(A0b);
            A0l2.add(this.A03.A0B(A0b));
        }
        ArrayList<String> stringArrayListExtra = getIntent().getStringArrayListExtra("invite_hashes");
        long longExtra = getIntent().getLongExtra("invite_expiration", 0);
        C15580nU A0U = ActivityC13790kL.A0U(getIntent(), "group_jid");
        boolean A0b2 = this.A0E.A0b(A0U);
        TextView A0N = C12990iw.A0N(this, R.id.group_invite_subtitle);
        int i = R.string.group_invite;
        if (A0b2) {
            i = R.string.parent_group_invite;
        }
        A0N.setText(i);
        MentionableEntry mentionableEntry = this.A0G;
        int i2 = R.string.group_invite_default_caption;
        if (A0b2) {
            i2 = R.string.parent_group_invite_default_caption;
        }
        mentionableEntry.setText(i2);
        this.A0I = C12960it.A0l();
        for (int i3 = 0; i3 < stringArrayListExtra.size(); i3++) {
            this.A0I.add(new C91394Ro(A0U, (UserJid) A0l.get(i3), stringArrayListExtra.get(i3), longExtra));
        }
        C15370n3 A0B = this.A03.A0B(A0U);
        this.A0A = A0B;
        A0M.setText(this.A04.A04(A0B));
        C12960it.A1E(new AnonymousClass37J(this.A07, this.A0A, this), ((ActivityC13830kP) this).A05);
        ImageView imageView = (ImageView) findViewById(R.id.send);
        AnonymousClass2GF.A01(this, imageView, this.A08, R.drawable.input_send);
        AbstractView$OnClickListenerC34281fs.A01(imageView, this, 29);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.invite_contacts_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(0);
        recyclerView.setLayoutManager(linearLayoutManager);
        C54232gO r0 = new C54232gO(this);
        r0.A00 = A0l2;
        r0.A02();
        recyclerView.setAdapter(r0);
        C27531Hw.A06(C12970iu.A0M(this, R.id.send_invite_title));
        View findViewById = findViewById(R.id.container);
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NW(findViewById, this));
        setResult(0, C87934Dp.A00(getIntent()));
        C12960it.A0x(findViewById(R.id.filler), this, 33);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(0);
            getWindow().setNavigationBarColor(AnonymousClass00T.A00(this, R.color.black));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1J1 r0 = this.A05;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        boolean A00 = C252718t.A00(((ActivityC13810kN) this).A00);
        Window window = getWindow();
        int i = 3;
        if (A00) {
            i = 5;
        }
        window.setSoftInputMode(i);
    }
}
