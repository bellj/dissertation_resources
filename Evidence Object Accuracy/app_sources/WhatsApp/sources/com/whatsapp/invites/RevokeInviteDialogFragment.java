package com.whatsapp.invites;

import X.AbstractC33111dN;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass04S;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C28581Od;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class RevokeInviteDialogFragment extends Hilt_RevokeInviteDialogFragment {
    public C15550nR A00;
    public C15610nY A01;
    public AbstractC33111dN A02;

    public static RevokeInviteDialogFragment A00(UserJid userJid, C28581Od r6) {
        RevokeInviteDialogFragment revokeInviteDialogFragment = new RevokeInviteDialogFragment();
        Bundle A0D = C12970iu.A0D();
        AnonymousClass009.A05(userJid);
        A0D.putString("jid", userJid.getRawString());
        A0D.putLong("invite_row_id", r6.A11);
        revokeInviteDialogFragment.A0U(A0D);
        return revokeInviteDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A02 = null;
    }

    @Override // com.whatsapp.invites.Hilt_RevokeInviteDialogFragment, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC33111dN) {
            this.A02 = (AbstractC33111dN) context;
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Bundle A03 = A03();
        ActivityC000900k A0C = A0C();
        UserJid nullable = UserJid.getNullable(A03.getString("jid"));
        AnonymousClass009.A05(nullable);
        C15370n3 A0B = this.A00.A0B(nullable);
        IDxCListenerShape4S0200000_2_I1 iDxCListenerShape4S0200000_2_I1 = new IDxCListenerShape4S0200000_2_I1(nullable, 8, this);
        C004802e A0S = C12980iv.A0S(A0C);
        A0S.A0A(C12970iu.A0q(this, C15610nY.A01(this.A01, A0B), new Object[1], 0, R.string.revoke_invite_confirm));
        AnonymousClass04S A0L = C12960it.A0L(iDxCListenerShape4S0200000_2_I1, A0S, R.string.revoke);
        A0L.setCanceledOnTouchOutside(true);
        return A0L;
    }
}
