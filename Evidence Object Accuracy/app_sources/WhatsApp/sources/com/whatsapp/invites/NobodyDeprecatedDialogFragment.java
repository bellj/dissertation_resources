package com.whatsapp.invites;

import X.C004802e;
import X.C72463ee;
import android.app.Dialog;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class NobodyDeprecatedDialogFragment extends Hilt_NobodyDeprecatedDialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e r2 = new C004802e(A0p());
        r2.A06(R.string.group_add_nobody_is_discontinued_dialog_text);
        r2.setPositiveButton(R.string.btn_continue, new IDxCListenerShape9S0100000_2_I1(this, 39));
        C72463ee.A0S(r2);
        return r2.create();
    }
}
