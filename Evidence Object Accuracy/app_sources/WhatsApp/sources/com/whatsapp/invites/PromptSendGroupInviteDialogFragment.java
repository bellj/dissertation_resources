package com.whatsapp.invites;

import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.C004802e;
import X.C15380n4;
import X.C15580nU;
import X.C15610nY;
import X.C20710wC;
import X.DialogInterface$OnClickListenerC97384gy;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.invites.PromptSendGroupInviteDialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.List;

/* loaded from: classes2.dex */
public class PromptSendGroupInviteDialogFragment extends Hilt_PromptSendGroupInviteDialogFragment {
    public C15610nY A00;
    public AnonymousClass018 A01;
    public C20710wC A02;

    public static PromptSendGroupInviteDialogFragment A00(Bundle bundle, int i) {
        PromptSendGroupInviteDialogFragment promptSendGroupInviteDialogFragment = new PromptSendGroupInviteDialogFragment();
        bundle.putInt("invite_intent_code", i);
        promptSendGroupInviteDialogFragment.A0U(bundle);
        return promptSendGroupInviteDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String str;
        Bundle A03 = A03();
        ActivityC000900k A0C = A0C();
        List A07 = C15380n4.A07(UserJid.class, A03.getStringArrayList("jids"));
        Intent intent = (Intent) A03.getParcelable("invite_intent");
        int i = A03.getInt("invite_intent_code");
        if (intent != null) {
            str = intent.getStringExtra("group_jid");
        } else {
            str = null;
        }
        boolean A0b = this.A02.A0b(C15580nU.A04(str));
        DialogInterface$OnClickListenerC97384gy r7 = new DialogInterface.OnClickListener(intent, this, i) { // from class: X.4gy
            public final /* synthetic */ int A00;
            public final /* synthetic */ Intent A01;
            public final /* synthetic */ PromptSendGroupInviteDialogFragment A02;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A00 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ActivityC000900k A0B;
                PromptSendGroupInviteDialogFragment promptSendGroupInviteDialogFragment = this.A02;
                Intent intent2 = this.A01;
                int i3 = this.A00;
                if (i2 == -1 && (A0B = promptSendGroupInviteDialogFragment.A0B()) != null && !A0B.isFinishing()) {
                    promptSendGroupInviteDialogFragment.A0B().startActivityForResult(intent2, i3);
                }
            }
        };
        C004802e r5 = new C004802e(A0C);
        AnonymousClass018 r9 = this.A01;
        int i2 = R.plurals.group_add_privacy_failure_prompt_invite;
        if (A0b) {
            i2 = R.plurals.parent_group_add_privacy_failure_prompt_invite;
        }
        AnonymousClass018 r3 = this.A01;
        C15610nY r12 = this.A00;
        HashSet hashSet = new HashSet();
        r5.A0A(r9.A0I(new Object[]{r3.A0F(r12.A0G(hashSet, 3, -1, r12.A0N(A07, hashSet), true))}, i2, (long) A07.size()));
        int i3 = R.string.button_invite_to_group;
        if (A0b) {
            i3 = R.string.button_invite_to_parent_group;
        }
        r5.setPositiveButton(i3, r7);
        r5.setNegativeButton(R.string.cancel, null);
        AnonymousClass04S create = r5.create();
        create.setCanceledOnTouchOutside(false);
        return create;
    }
}
