package com.whatsapp;

import X.AbstractC28511Nr;
import X.AbstractC36671kL;
import X.AnonymousClass00T;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass0DW;
import X.AnonymousClass19M;
import X.AnonymousClass3I9;
import X.AnonymousClass3J9;
import X.C016907y;
import X.C16630pM;
import X.C42971wC;
import X.C52162aM;
import X.C73453gG;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.SingleLineTransformationMethod;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.util.Log;
import java.util.List;

/* loaded from: classes2.dex */
public class TextEmojiLabel extends AbstractC28511Nr {
    public static final Spannable.Factory A0C = new C73453gG();
    public static final boolean A0D;
    public static final boolean A0E;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public Paint A04;
    public TextView.BufferType A05;
    public AnonymousClass0DW A06;
    public C52162aM A07;
    public AnonymousClass01d A08;
    public AnonymousClass19M A09;
    public C16630pM A0A;
    public CharSequence A0B;

    static {
        boolean z;
        try {
            Class cls = Integer.TYPE;
            Layout.class.getDeclaredMethod("processToSupportEmoji", CharSequence.class, cls, cls);
            z = true;
        } catch (NoSuchMethodException | SecurityException unused) {
            z = false;
        }
        A0D = z;
        boolean z2 = false;
        if (Build.VERSION.SDK_INT < 19) {
            z2 = true;
        }
        A0E = z2;
    }

    public TextEmojiLabel(Context context) {
        super(context);
        A0A();
    }

    public TextEmojiLabel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0A();
    }

    public TextEmojiLabel(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0A();
    }

    public final void A0A() {
        if (A0E) {
            setSpannableFactory(A0C);
        }
        if (Build.VERSION.SDK_INT == 29) {
            setBreakStrategy(0);
        }
    }

    public void A0B(int i) {
        Drawable A04 = AnonymousClass00T.A04(getContext(), i);
        if (((WaTextView) this).A01.A04().A06) {
            setCompoundDrawablesWithIntrinsicBounds(A04, (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, A04, (Drawable) null);
        }
        setCompoundDrawablePadding(getResources().getDimensionPixelSize(R.dimen.verified_indicator_padding));
    }

    public void A0C(Drawable drawable) {
        if (((WaTextView) this).A01.A04().A06) {
            setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable, (Drawable) null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        }
        setCompoundDrawablePadding(getResources().getDimensionPixelSize(R.dimen.button_inset_vertical));
    }

    public void A0D(AnonymousClass3J9 r8, CharSequence charSequence, List list, int i, boolean z) {
        AnonymousClass3J9 r3 = r8;
        if (z) {
            charSequence = C42971wC.A03(this.A08, this.A0A, charSequence);
        }
        if (i > 0 && charSequence != null && charSequence.length() > i) {
            int charCount = i + (Character.charCount(Character.codePointAt(charSequence, i - 1)) - 1);
            if (charSequence instanceof Editable) {
                charSequence = ((Editable) charSequence).delete(charCount, charSequence.length());
            } else {
                charSequence = charSequence.subSequence(0, charCount);
            }
        }
        CharSequence A03 = AbstractC36671kL.A03(getContext(), getPaint(), this.A09, charSequence);
        if (r8 == null) {
            r3 = AnonymousClass3J9.A04;
        }
        setText((CharSequence) AnonymousClass3J9.A00(getContext(), ((WaTextView) this).A01, r3, A03, list, false).A00);
    }

    public void A0E(CharSequence charSequence) {
        A0G(null, charSequence);
    }

    public void A0F(CharSequence charSequence, List list, int i, boolean z) {
        A0D(null, charSequence, list, i, z);
    }

    public void A0G(List list, CharSequence charSequence) {
        A0F(charSequence, list, 0, false);
    }

    @Override // android.view.View
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        AnonymousClass0DW r0 = this.A06;
        return (r0 != null && r0.A0J(motionEvent)) || super.dispatchHoverEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        AnonymousClass0DW r0 = this.A06;
        return (r0 != null && r0.A0I(keyEvent)) || super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03 > 0) {
            canvas.drawRect((float) (getScrollX() + getPaddingLeft()), (float) (((getHeight() - getPaddingBottom()) - this.A01) - this.A02), (float) ((getScrollX() + getWidth()) - getPaddingRight()), (float) ((getHeight() - getPaddingBottom()) - this.A01), this.A04);
            return;
        }
        try {
            super.onDraw(canvas);
        } catch (IndexOutOfBoundsException unused) {
            setText(getText());
        } catch (Exception e) {
            Log.e(e);
            throw new RuntimeException(e);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        AnonymousClass0DW r0 = this.A06;
        if (r0 != null) {
            r0.A0E(z, i, rect);
        }
    }

    @Override // com.whatsapp.WaTextView, X.C004602b, android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        int size;
        CharSequence charSequence;
        int measuredWidth;
        if (this.A03 > 0) {
            int mode = View.MeasureSpec.getMode(i);
            int size2 = View.MeasureSpec.getSize(i);
            super.onMeasure(i, i2);
            if (mode != 1073741824) {
                measuredWidth = (size2 * this.A03) / 100;
            } else {
                measuredWidth = getMeasuredWidth();
            }
            setMeasuredDimension(measuredWidth, getMeasuredHeight());
            return;
        }
        int i3 = Build.VERSION.SDK_INT;
        if (i3 == 16 || i3 == 17) {
            try {
                super.onMeasure(i, i2);
            } catch (IndexOutOfBoundsException e) {
                Log.e("textemojilabel/measure ", e);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.A0B);
                int nextSpanTransition = spannableStringBuilder.nextSpanTransition(0, spannableStringBuilder.length(), MetricAffectingSpan.class);
                while (nextSpanTransition >= 0 && nextSpanTransition < spannableStringBuilder.length()) {
                    spannableStringBuilder.insert(nextSpanTransition, (CharSequence) " ");
                    nextSpanTransition = spannableStringBuilder.nextSpanTransition(nextSpanTransition + 1, spannableStringBuilder.length(), MetricAffectingSpan.class);
                }
                try {
                    this.A0B = spannableStringBuilder;
                    super.setText(spannableStringBuilder);
                    super.onMeasure(i, i2);
                } catch (ArrayIndexOutOfBoundsException e2) {
                    Log.e("textemojilabel/measure1 ", e2);
                    SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(this.A0B);
                    int indexOf = TextUtils.indexOf((CharSequence) spannableStringBuilder2, '\n', 0);
                    while (indexOf >= 0) {
                        int i4 = indexOf + 1;
                        spannableStringBuilder2 = spannableStringBuilder2.replace(indexOf, i4, (CharSequence) " ");
                        indexOf = TextUtils.indexOf((CharSequence) spannableStringBuilder2, '\n', i4);
                    }
                    this.A0B = spannableStringBuilder2;
                    super.setText(spannableStringBuilder2);
                }
            }
            if (View.MeasureSpec.getMode(i) != 0 && (size = (View.MeasureSpec.getSize(i) - getCompoundPaddingLeft()) - getCompoundPaddingRight()) > 0 && this.A00 != size && (this.A0B instanceof Spanned) && getEllipsize() != null && (getTransformationMethod() instanceof SingleLineTransformationMethod)) {
                this.A00 = size;
                if (getTransformationMethod() != null) {
                    charSequence = getTransformationMethod().getTransformation(this.A0B, this);
                } else {
                    charSequence = this.A0B;
                }
                CharSequence ellipsize = TextUtils.ellipsize(charSequence, getPaint(), (float) size, getEllipsize());
                if (ellipsize != null && !ellipsize.equals(charSequence)) {
                    super.setText(ellipsize, this.A05);
                    return;
                }
                return;
            }
            return;
        }
        super.onMeasure(i, i2);
        if (View.MeasureSpec.getMode(i) != 0) {
        }
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (this.A07 == null) {
            return onTouchEvent;
        }
        CharSequence text = getText();
        return (!(text instanceof Spannable) || getLayout() == null) ? onTouchEvent : onTouchEvent | this.A07.onTouchEvent(this, (Spannable) text, motionEvent);
    }

    public void setAccessibilityHelper(AnonymousClass0DW r1) {
        this.A06 = r1;
        AnonymousClass028.A0g(this, r1);
    }

    public void setLinkHandler(C52162aM r1) {
        this.A07 = r1;
    }

    public void setPlaceholder(int i) {
        if (this.A03 != i) {
            this.A03 = i;
            if (i > 0) {
                Paint.FontMetricsInt A00 = AnonymousClass3I9.A00(getPaint());
                this.A02 = ((-A00.ascent) * 6) / 10;
                this.A01 = A00.bottom;
                Paint paint = this.A04;
                if (paint == null) {
                    paint = new Paint();
                    this.A04 = paint;
                }
                paint.setColor(C016907y.A06(getPaint().getColor(), (Color.alpha(getPaint().getColor()) * 12) / 255));
            }
            invalidate();
        }
    }

    @Override // com.whatsapp.WaTextView, android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (A0D && charSequence != null) {
            SpannableStringBuilder spannableStringBuilder = null;
            for (int i = 0; i < charSequence.length(); i++) {
                char charAt = charSequence.charAt(i);
                if (55296 <= charAt && charAt <= 57343) {
                    if (spannableStringBuilder == null) {
                        spannableStringBuilder = new SpannableStringBuilder(charSequence);
                    }
                    spannableStringBuilder.replace(i, i + 1, (CharSequence) "□");
                }
            }
            if (spannableStringBuilder != null) {
                charSequence = spannableStringBuilder;
            }
        }
        this.A0B = charSequence;
        this.A05 = bufferType;
        this.A00 = 0;
        if ((A0E || this.A07 != null) && (charSequence instanceof Spanned)) {
            bufferType = TextView.BufferType.SPANNABLE;
        }
        super.setText(charSequence, bufferType);
    }
}
