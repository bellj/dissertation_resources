package com.whatsapp.web;

import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1GA;
import X.AnonymousClass1UY;
import X.AnonymousClass22D;
import X.AnonymousClass3JK;
import X.C005602s;
import X.C14820m6;
import X.C14860mA;
import X.C14880mC;
import X.C18360sK;
import X.C22630zO;
import X.C249117j;
import X.C249217k;
import X.C65303Iz;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class WebSessionVerificationReceiver extends BroadcastReceiver {
    public C14820m6 A00;
    public C14860mA A01;
    public C249117j A02;
    public final Object A03;
    public volatile boolean A04;

    public WebSessionVerificationReceiver() {
        this(0);
    }

    public WebSessionVerificationReceiver(int i) {
        this.A04 = false;
        this.A03 = new Object();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        List asList;
        C14880mC r5;
        if (!this.A04) {
            synchronized (this.A03) {
                if (!this.A04) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = (C14860mA) r1.ANU.get();
                    this.A00 = (C14820m6) r1.AN3.get();
                    this.A02 = (C249117j) r1.ANW.get();
                    this.A04 = true;
                }
            }
        }
        String string = this.A00.A00.getString("web_session_verification_browser_ids", null);
        if (string == null || (asList = Arrays.asList(string.split(","))) == null) {
            Log.e("WebSessionVerificationReceiver/onReceive/ browserIds are missing from prefs");
            return;
        }
        Iterator it = asList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            C14860mA r0 = this.A01;
            if (next != null && (r5 = (C14880mC) r0.A05().get(next)) != null) {
                for (AnonymousClass1GA r02 : this.A02.A01()) {
                    C249217k r12 = r02.A00;
                    Context context2 = r12.A01.A00;
                    AnonymousClass018 r2 = r12.A03;
                    C18360sK r4 = r12.A02;
                    String A00 = AnonymousClass3JK.A00(r2, r5.A06);
                    C005602s A002 = C22630zO.A00(context2);
                    A002.A0J = "other_notifications@1";
                    A002.A0B(context2.getString(R.string.notification_web_session_verification_title));
                    A002.A05(r5.A04);
                    A002.A0A(context2.getString(R.string.notification_web_session_verification_title));
                    A002.A09(context2.getString(R.string.notification_web_session_verification_description, r5.A08, A00));
                    A002.A09 = AnonymousClass1UY.A00(context2, 0, C65303Iz.A02(context2), 0);
                    NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
                    notificationCompat$BigTextStyle.A09(context2.getString(R.string.notification_web_session_verification_description, r5.A08, A00));
                    A002.A08(notificationCompat$BigTextStyle);
                    A002.A0D(true);
                    C18360sK.A01(A002, R.drawable.notify_web_client_connected);
                    r4.A03(15, A002.A01());
                }
            }
        }
        this.A00.A00.edit().putString("web_session_verification_browser_ids", null).putLong("web_session_verification_when_millis", -1).apply();
    }
}
