package com.whatsapp;

import X.AbstractC36671kL;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass04S;
import X.AnonymousClass19M;
import X.AnonymousClass4HD;
import X.C004802e;
import X.C12970iu;
import X.C12980iv;
import X.C252018m;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.PushnameEmojiBlacklistDialogFragment;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class PushnameEmojiBlacklistDialogFragment extends Hilt_PushnameEmojiBlacklistDialogFragment {
    public AnonymousClass018 A00;
    public AnonymousClass19M A01;
    public C252018m A02;

    public static PushnameEmojiBlacklistDialogFragment A00(String str) {
        PushnameEmojiBlacklistDialogFragment pushnameEmojiBlacklistDialogFragment = new PushnameEmojiBlacklistDialogFragment();
        Bundle A0D = C12970iu.A0D();
        String[] strArr = AnonymousClass4HD.A01;
        int length = strArr.length;
        ArrayList<String> A0w = C12980iv.A0w(length);
        for (String str2 : strArr) {
            if (str.contains(str2)) {
                A0w.add(str2);
            }
        }
        A0D.putStringArrayList("invalid_emojis", A0w);
        pushnameEmojiBlacklistDialogFragment.A0U(A0D);
        return pushnameEmojiBlacklistDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        ArrayList<String> stringArrayList = A03().getStringArrayList("invalid_emojis");
        AnonymousClass009.A05(stringArrayList);
        String A00 = C252018m.A00(this.A02, "26000056");
        A0O.A0A(AbstractC36671kL.A05(A0B().getApplicationContext(), this.A01, this.A00.A0I(new Object[]{TextUtils.join(" ", stringArrayList)}, R.plurals.pushname_blacklisted_emoji_error, (long) stringArrayList.size())));
        A0O.A00(R.string.learn_more, new DialogInterface.OnClickListener(A00) { // from class: X.4gS
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PushnameEmojiBlacklistDialogFragment.this.A0v(new Intent("android.intent.action.VIEW", Uri.parse(this.A01)));
            }
        });
        A0O.setPositiveButton(R.string.ok, new IDxCListenerShape4S0000000_2_I1(0));
        AnonymousClass04S create = A0O.create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }
}
