package com.whatsapp.webpagepreview;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass2GF;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12980iv;
import X.C26181Ci;
import X.C28141Kv;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WebPagePreviewCornerShadowView extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public C26181Ci A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public WebPagePreviewCornerShadowView(Context context) {
        super(context);
        A00();
        setWillNotDraw(false);
    }

    public WebPagePreviewCornerShadowView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
        setWillNotDraw(false);
    }

    public WebPagePreviewCornerShadowView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        setWillNotDraw(false);
    }

    public WebPagePreviewCornerShadowView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A00();
    }

    public void A00() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A00 = C12960it.A0R(A00);
            this.A01 = (C26181Ci) A00.A6H.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode()) {
            int paddingLeft = getPaddingLeft();
            int A08 = C12980iv.A08(this);
            int A07 = C12980iv.A07(this);
            Context context = getContext();
            AnonymousClass009.A05(context);
            C26181Ci r5 = this.A01;
            Drawable drawable = r5.A00;
            if (drawable == null) {
                drawable = new AnonymousClass2GF(context.getResources().getDrawable(R.drawable.corner_overlay), r5.A02);
                r5.A00 = drawable;
            }
            if (C28141Kv.A01(this.A00)) {
                drawable.setBounds(A08 - drawable.getIntrinsicWidth(), A07 - drawable.getIntrinsicHeight(), A08, A07);
            } else {
                drawable.setBounds(paddingLeft, A07 - drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth() + paddingLeft, A07);
            }
            drawable.draw(canvas);
        }
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
    }
}
