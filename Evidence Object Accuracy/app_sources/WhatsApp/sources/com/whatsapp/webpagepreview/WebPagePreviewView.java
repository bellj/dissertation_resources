package com.whatsapp.webpagepreview;

import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass1BK;
import X.AnonymousClass2KT;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2V4;
import X.AnonymousClass3J9;
import X.AnonymousClass3JH;
import X.C015607k;
import X.C14320lF;
import X.C17720rH;
import X.C22180yf;
import X.C28861Ph;
import X.C33771f3;
import X.C42941w9;
import X.C44891zj;
import X.C83783xt;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.components.button.ThumbnailButton;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class WebPagePreviewView extends FrameLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public FrameLayout A07;
    public FrameLayout A08;
    public FrameLayout A09;
    public ImageView A0A;
    public ImageView A0B;
    public ImageView A0C;
    public LinearLayout A0D;
    public ProgressBar A0E;
    public ProgressBar A0F;
    public TextView A0G;
    public WaTextView A0H;
    public WaTextView A0I;
    public WaTextView A0J;
    public ThumbnailButton A0K;
    public ThumbnailButton A0L;
    public AnonymousClass018 A0M;
    public C17720rH A0N;
    public AnonymousClass1BK A0O;
    public C22180yf A0P;
    public AnonymousClass2P7 A0Q;
    public boolean A0R;

    public WebPagePreviewView(Context context) {
        super(context);
        A04();
        A08(context);
    }

    public WebPagePreviewView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A04();
        A08(context);
    }

    public WebPagePreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A04();
        A08(context);
    }

    public WebPagePreviewView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A04();
    }

    public void A00() {
        this.A09.setVisibility(8);
        this.A07.setVisibility(0);
        this.A08.setVisibility(0);
        this.A0L.setVisibility(8);
    }

    public void A01() {
        this.A01.setVisibility(0);
        this.A09.setVisibility(8);
        this.A07.setVisibility(8);
        C42941w9.A08(this.A0I, this.A0M, 0, getContext().getResources().getDimensionPixelSize(R.dimen.conversation_title_padding));
    }

    public void A02() {
        this.A09.setVisibility(8);
        this.A07.setVisibility(8);
        this.A08.setVisibility(0);
        this.A0L.setVisibility(0);
    }

    public void A03() {
        this.A09.setVisibility(0);
        this.A07.setVisibility(8);
        this.A08.setVisibility(0);
        this.A0L.setVisibility(8);
    }

    public void A04() {
        if (!this.A0R) {
            this.A0R = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A0M = (AnonymousClass018) r1.ANb.get();
            this.A0O = (AnonymousClass1BK) r1.AFZ.get();
            this.A0P = (C22180yf) r1.A5U.get();
            this.A0N = r1.A2g();
        }
    }

    public void A05(float f, float f2, float f3, float f4) {
        this.A0F.setAlpha(f);
        this.A04.setAlpha(f2);
        this.A0B.setAlpha(f2);
        this.A05.setAlpha(f3);
        this.A06.setAlpha(f4);
    }

    public void A06(float f, float f2, float f3, float f4) {
        long j = (long) 150;
        this.A0F.animate().setDuration(j).alpha(f);
        this.A04.animate().setDuration(j).alpha(f2);
        this.A0B.animate().setDuration(j).alpha(f2);
        this.A05.animate().setDuration(j).alpha(f3);
        this.A06.animate().setDuration(j).alpha(f4);
    }

    public void A07(int i, int i2) {
        this.A07.getLayoutParams().width = i;
        this.A07.getLayoutParams().height = i2;
        this.A07.requestLayout();
    }

    public final void A08(Context context) {
        FrameLayout.inflate(context, R.layout.web_page_preview, this);
        this.A02 = AnonymousClass028.A0D(this, R.id.link_preview_content);
        this.A08 = (FrameLayout) AnonymousClass028.A0D(this, R.id.thumb_frame);
        this.A0L = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.thumb);
        this.A0E = (ProgressBar) AnonymousClass028.A0D(this, R.id.progress);
        this.A01 = AnonymousClass028.A0D(this, R.id.cancel);
        this.A09 = (FrameLayout) AnonymousClass028.A0D(this, R.id.large_thumb_frame);
        this.A0C = (ImageView) AnonymousClass028.A0D(this, R.id.large_thumb);
        this.A0B = (ImageView) AnonymousClass028.A0D(this, R.id.logo_button);
        this.A0F = (ProgressBar) AnonymousClass028.A0D(this, R.id.large_progress);
        this.A04 = AnonymousClass028.A0D(this, R.id.play_frame);
        this.A05 = AnonymousClass028.A0D(this, R.id.inline_indication);
        this.A06 = AnonymousClass028.A0D(this, R.id.inline_layer);
        this.A07 = (FrameLayout) AnonymousClass028.A0D(this, R.id.webPagePreviewImageLarge_frame);
        this.A0K = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.webPagePreviewImageLarge_thumb);
        this.A0A = (ImageView) AnonymousClass028.A0D(this, R.id.webPagePreviewImageLarge_logo_platform);
        this.A03 = AnonymousClass028.A0D(this, R.id.webPagePreviewImageLarge_logo_platform_shadow);
        this.A0D = (LinearLayout) AnonymousClass028.A0D(this, R.id.titleSnippetUrlLayout);
        this.A0I = (WaTextView) AnonymousClass028.A0D(this, R.id.title);
        this.A0H = (WaTextView) AnonymousClass028.A0D(this, R.id.snippet);
        this.A0J = (WaTextView) AnonymousClass028.A0D(this, R.id.url);
        this.A00 = AnonymousClass028.A0D(this, R.id.gif_size_bullet);
        this.A0G = (TextView) AnonymousClass028.A0D(this, R.id.gif_size);
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.balloon_incoming_frame);
        AnonymousClass009.A05(A04);
        Drawable A03 = C015607k.A03(A04.mutate());
        C015607k.A0A(A03, AnonymousClass00T.A00(context, R.color.bubble_color_incoming));
        setForeground(A03);
        if (getId() == -1) {
            setId(R.id.link_preview_frame);
        }
    }

    public final void A09(WaTextView waTextView, String str, List list, int i) {
        String str2 = str;
        if (!TextUtils.isEmpty(str)) {
            if (str.length() > i) {
                str2 = str.substring(0, i);
            }
            waTextView.setVisibility(0);
            waTextView.setText((CharSequence) AnonymousClass3J9.A00(getContext(), this.A0M, AnonymousClass3J9.A01, str2, list, false).A00);
            return;
        }
        waTextView.setVisibility(8);
    }

    public void A0A(C14320lF r17, List list, boolean z, boolean z2) {
        String str;
        int i;
        boolean z3;
        String str2 = r17.A0E;
        String str3 = r17.A0B;
        if (TextUtils.isEmpty(r17.A09)) {
            str = r17.A0K;
        } else {
            str = r17.A09;
        }
        byte[] bArr = r17.A0H;
        String str4 = r17.A0K;
        Integer num = r17.A08;
        AnonymousClass2V4 r0 = r17.A06;
        if (r0 != null) {
            i = r0.A00;
        } else {
            i = -1;
        }
        boolean z4 = r17 instanceof AnonymousClass2KT;
        if (!z4) {
            z3 = false;
        } else {
            z3 = ((AnonymousClass2KT) r17).A01;
        }
        A0D(num, str2, str3, str, str4, list, bArr, i, false, z, z3, z4, r17 instanceof C83783xt, z2);
    }

    public final void A0B(C28861Ph r20, String str, String str2, String str3, List list, boolean z, boolean z2, boolean z3) {
        Set A00 = this.A0O.A00(r20.A0C(), r20, str2);
        String str4 = r20.A05;
        String str5 = r20.A03;
        byte[] A17 = r20.A17();
        Integer valueOf = Integer.valueOf(r20.A00);
        boolean z4 = false;
        if (A00 != null) {
            z4 = true;
        }
        A0D(valueOf, str4, str5, str, str3, list, A17, -1, z4, z, z2, false, false, z3);
    }

    public void A0C(C28861Ph r10, String str, boolean z) {
        String A01 = C33771f3.A01(r10.A14());
        String str2 = r10.A06;
        if (TextUtils.isEmpty(str2)) {
            str2 = A01;
        }
        A0B(r10, str2, str, A01, null, false, false, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (android.text.TextUtils.isEmpty(com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity.A02(android.net.Uri.parse(r16))) != false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01f6, code lost:
        if (r2 != 3) goto L_0x01f8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x015b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D(java.lang.Integer r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.util.List r17, byte[] r18, int r19, boolean r20, boolean r21, boolean r22, boolean r23, boolean r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.webpagepreview.WebPagePreviewView.A0D(java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, byte[], int, boolean, boolean, boolean, boolean, boolean, boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0Q;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0Q = r0;
        }
        return r0.generatedComponent();
    }

    public ThumbnailButton getImageLargeThumb() {
        return this.A0K;
    }

    public ThumbnailButton getImageThumb() {
        return this.A0L;
    }

    public ImageView getVideoLargeThumb() {
        return this.A0C;
    }

    public FrameLayout getVideoLargeThumbFrame() {
        return this.A09;
    }

    public void setImageCancelClickListener(View.OnClickListener onClickListener) {
        this.A01.setOnClickListener(onClickListener);
    }

    public void setImageContentBackgroundResource(int i) {
        this.A02.setBackgroundResource(i);
    }

    public void setImageContentClickListener(View.OnClickListener onClickListener) {
        this.A02.setOnClickListener(onClickListener);
    }

    public void setImageContentEnabled(boolean z) {
        this.A02.setEnabled(z);
    }

    public void setImageContentMinimumHeight(int i) {
        this.A02.setMinimumHeight(i);
    }

    public void setImageLargeLogo(int i) {
        ImageView imageView = this.A0A;
        if (i != 0) {
            imageView.setVisibility(0);
            this.A03.setVisibility(0);
            ImageView imageView2 = this.A0A;
            int i2 = R.drawable.ic_pip_facebook;
            if (i != 1) {
                if (i == 2) {
                    i2 = R.drawable.ic_pip_instagram;
                } else {
                    throw new InvalidParameterException("@Imagetype should be different than UNKNOWN");
                }
            }
            imageView2.setImageResource(i2);
            return;
        }
        imageView.setVisibility(8);
        this.A03.setVisibility(8);
    }

    public void setImageLargeThumbFrameHeight(int i) {
        this.A07.getLayoutParams().height = i;
    }

    public void setImageLargeThumbWithBackground(int i) {
        this.A0K.setBackgroundColor(i);
        this.A0K.setImageDrawable(null);
    }

    public void setImageLargeThumbWithBitmap(Bitmap bitmap) {
        this.A0K.setImageBitmap(bitmap);
        this.A0K.setVisibility(0);
    }

    public void setImageProgressBarVisibility(boolean z) {
        ProgressBar progressBar = this.A0E;
        int i = 8;
        if (z) {
            i = 0;
        }
        progressBar.setVisibility(i);
    }

    public void setImageThumbVisibility(boolean z) {
        ThumbnailButton thumbnailButton = this.A0L;
        int i = 8;
        if (z) {
            i = 0;
        }
        thumbnailButton.setVisibility(i);
    }

    private void setImageThumbWithBitmap(Bitmap bitmap) {
        this.A0L.setImageBitmap(bitmap);
        this.A0L.setVisibility(0);
    }

    public void setLinkGifSize(int i) {
        View view = this.A00;
        if (i > 0) {
            view.setVisibility(0);
            this.A0G.setVisibility(0);
            this.A0G.setText(C44891zj.A03(this.A0M, (long) i));
            return;
        }
        view.setVisibility(8);
        this.A0G.setVisibility(8);
    }

    public void setLinkHostname(String str) {
        if (TextUtils.isEmpty(str)) {
            this.A0J.setVisibility(8);
            return;
        }
        this.A0J.setVisibility(0);
        if (str.length() > 150) {
            str = str.substring(0, 150);
        }
        this.A0J.setText(str);
    }

    public void setLinkSnippet(CharSequence charSequence) {
        if (charSequence == null) {
            this.A0H.setVisibility(8);
            return;
        }
        this.A0H.setVisibility(0);
        this.A0H.setText(charSequence);
    }

    public void setLinkTitle(CharSequence charSequence) {
        if (charSequence == null) {
            this.A0I.setVisibility(8);
            return;
        }
        this.A0I.setVisibility(0);
        this.A0I.setText(charSequence);
    }

    public void setLinkTitleTypeface(int i) {
        this.A0I.setTypeface(null, i);
    }

    public final void setTitleAndSnippet(String str, String str2, boolean z, List list) {
        if ((!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) && !z) {
            A09(this.A0I, str, list, 150);
            A09(this.A0H, str2, list, 300);
            return;
        }
        this.A0I.setVisibility(8);
        this.A0H.setVisibility(8);
    }

    public void setVideoLargeLogo(int i) {
        if (i == 1 || i == 7) {
            this.A0B.setVisibility(8);
            return;
        }
        this.A0B.animate().cancel();
        this.A0B.setVisibility(0);
        this.A0B.setImageResource(AnonymousClass3JH.A00(i));
        this.A0B.setAlpha(1.0f);
    }

    public void setVideoLargeThumbFrameHeight(int i) {
        this.A09.getLayoutParams().height = i;
    }

    public void setVideoLargeThumbWithBackground(int i) {
        this.A0C.setBackgroundColor(i);
        this.A0C.setImageDrawable(null);
    }

    public void setVideoLargeThumbWithBitmap(Bitmap bitmap) {
        this.A0C.setImageBitmap(bitmap);
        this.A0C.setVisibility(0);
    }
}
