package com.whatsapp.companionmode.registration;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C16590pI;
import X.C22880zn;
import X.C231010j;
import X.C52212aR;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ConvertPrimaryToCompanionActivity extends ActivityC13790kL {
    public C22880zn A00;
    public C231010j A01;
    public C16590pI A02;
    public boolean A03;

    public ConvertPrimaryToCompanionActivity() {
        this(0);
    }

    public ConvertPrimaryToCompanionActivity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 52);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = C12970iu.A0X(A1M);
            this.A00 = (C22880zn) A1M.A5W.get();
            this.A01 = (C231010j) A1M.A3p.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.menuitem_companion);
        setContentView(R.layout.convert_primary_to_companion);
        TextView A0M = C12970iu.A0M(this, R.id.companion_mode_warning_text);
        String string = getString(R.string.convert_primary_to_companion_warning_backup_text);
        String A0X = C12960it.A0X(this, string, new Object[1], 0, R.string.convert_primary_to_companion_warning_text);
        SpannableStringBuilder A0J = C12990iw.A0J(A0X);
        C52212aR r3 = new C52212aR(this);
        int length = A0X.length();
        A0J.setSpan(r3, length - string.length(), length, 33);
        A0M.setText(A0J);
        A0M.setLinksClickable(true);
        C12990iw.A1F(A0M);
        C12960it.A13(findViewById(R.id.proceed_button), this, new IDxCListenerShape8S0100000_1_I1(this, 16), 33);
    }
}
