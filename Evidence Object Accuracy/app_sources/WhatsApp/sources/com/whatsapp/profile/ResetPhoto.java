package com.whatsapp.profile;

import X.ActivityC000900k;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass12U;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C36021jC;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ResetPhoto extends ActivityC13830kP {
    public AnonymousClass12U A00;
    public boolean A01;

    public ResetPhoto() {
        this(0);
    }

    public ResetPhoto(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 94);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            this.A00 = ActivityC13830kP.A1N(ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this));
        }
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        int intExtra = getIntent().getIntExtra("photo_type", 0);
        if (intExtra == 1) {
            throw C12970iu.A0z();
        }
        setTitle(R.string.remove_profile_photo);
        if (bundle == null) {
            ConfirmDialogFragment confirmDialogFragment = new ConfirmDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("photo_type", intExtra);
            confirmDialogFragment.A0U(A0D);
            C12960it.A16(confirmDialogFragment, this);
        }
    }

    /* loaded from: classes2.dex */
    public class ConfirmDialogFragment extends Hilt_ResetPhoto_ConfirmDialogFragment {
        public AnonymousClass12U A00;

        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            int i = 0;
            if (bundle2 != null) {
                i = bundle2.getInt("photo_type", 0);
            }
            C004802e A0K = C12960it.A0K(this);
            if (i == 1) {
                throw C12970iu.A0z();
            }
            A0K.A06(R.string.remove_profile_photo_confirmation);
            A0K.A0B(true);
            C12970iu.A1K(A0K, this, 50, R.string.remove_profile_photo_confirmation_cancel);
            C12970iu.A1M(A0K, this, 26, R.string.remove_profile_photo_confirmation_remove);
            return A0K.create();
        }

        @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
        public void onDismiss(DialogInterface dialogInterface) {
            super.onDismiss(dialogInterface);
            ActivityC000900k A0B = A0B();
            if (A0B != null && !C36021jC.A03(A0B)) {
                A0B.finish();
                A0B.overridePendingTransition(17432576, 17432577);
            }
        }
    }
}
