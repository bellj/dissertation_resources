package com.whatsapp.profile;

import X.AbstractC009404s;
import X.ActivityC000900k;
import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2FH;
import X.C103254qS;
import X.C14900mE;
import X.C15890o4;
import X.C48572Gu;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class CapturePhoto extends ActivityC000900k implements AnonymousClass004 {
    public C14900mE A00;
    public C15890o4 A01;
    public AnonymousClass018 A02;
    public boolean A03;
    public final Object A04;
    public volatile AnonymousClass2FH A05;

    public CapturePhoto() {
        this(0);
    }

    public CapturePhoto(int i) {
        this.A04 = new Object();
        this.A03 = false;
        A0R(new C103254qS(this));
    }

    public final void A1U() {
        if (RequestPermissionActivity.A0T(this, this.A01, 30)) {
            try {
                startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE").putExtra("output", getIntent().getParcelableExtra("target_file_uri")).setFlags(2), 1);
            } catch (ActivityNotFoundException e) {
                Log.e("capturephoto/start-activity ", e);
                this.A00.A07(R.string.activity_not_found, 0);
            }
        }
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A05 == null) {
            synchronized (this.A04) {
                if (this.A05 == null) {
                    this.A05 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A05.generatedComponent();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1) {
            if (getIntent() != null && getIntent().getBooleanExtra("should_return_photo_source", false)) {
                if (intent == null) {
                    intent = new Intent();
                }
                intent.putExtra("photo_source", 1);
            }
            setResult(i2, intent);
        } else if (i != 30) {
            super.onActivityResult(i, i2, intent);
            return;
        } else if (i2 == -1) {
            A1U();
            return;
        }
        finish();
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.capture_photo);
        if (getCallingPackage() == null || !getCallingPackage().equals(getPackageName())) {
            finish();
        } else if (bundle == null) {
            A1U();
        }
    }
}
