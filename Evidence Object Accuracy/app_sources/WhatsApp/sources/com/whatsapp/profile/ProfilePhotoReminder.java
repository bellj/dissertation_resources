package com.whatsapp.profile;

import X.AbstractC116455Vm;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10Z;
import X.AnonymousClass12P;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass193;
import X.AnonymousClass19M;
import X.AnonymousClass1lU;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103274qU;
import X.C1096852q;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15330mx;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C16630pM;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C19890uq;
import X.C20220vP;
import X.C20640w5;
import X.C20680w9;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C231510o;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26061Bw;
import X.C27131Gd;
import X.C27611If;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ProfilePhotoReminder extends ActivityC13790kL {
    public static long A0M = -1;
    public static boolean A0N;
    public Bitmap A00;
    public Handler A01;
    public View A02;
    public ImageView A03;
    public C20640w5 A04;
    public WaEditText A05;
    public AnonymousClass130 A06;
    public AnonymousClass10S A07;
    public AnonymousClass131 A08;
    public C15370n3 A09;
    public C231510o A0A;
    public C15330mx A0B;
    public AnonymousClass193 A0C;
    public C19890uq A0D;
    public C20220vP A0E;
    public C16630pM A0F;
    public AnonymousClass10Z A0G;
    public C20680w9 A0H;
    public Runnable A0I;
    public boolean A0J;
    public final AbstractC116455Vm A0K;
    public final C27131Gd A0L;

    public ProfilePhotoReminder() {
        this(0);
        this.A0K = new C1096852q(this);
        this.A0L = new AnonymousClass1lU(this);
    }

    public ProfilePhotoReminder(int i) {
        this.A0J = false;
        A0R(new C103274qU(this));
    }

    public static synchronized void A02(C20640w5 r4, C14820m6 r5) {
        synchronized (ProfilePhotoReminder.class) {
            A0N = true;
            if (r4.A03()) {
                Log.w("profilephotoreminder/savelastremindertimestamp/clock is wrong, not saving last profile photo reminder time");
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                A0M = currentTimeMillis;
                r5.A0q("wa_last_reminder_timestamp", currentTimeMillis);
            }
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0J) {
            this.A0J = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A04 = (C20640w5) r1.AHk.get();
            this.A0A = (C231510o) r1.AHO.get();
            this.A06 = (AnonymousClass130) r1.A41.get();
            this.A0D = (C19890uq) r1.ABw.get();
            this.A0H = (C20680w9) r1.AGx.get();
            this.A07 = (AnonymousClass10S) r1.A46.get();
            this.A0C = (AnonymousClass193) r1.A6S.get();
            this.A0E = (C20220vP) r1.AC3.get();
            this.A0G = (AnonymousClass10Z) r1.AGM.get();
            this.A0F = (C16630pM) r1.AIc.get();
            this.A08 = (AnonymousClass131) r1.A49.get();
        }
    }

    public final void A2e() {
        Bitmap A00;
        this.A02.setVisibility(8);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.registration_profile_photo_size);
        float dimension = getResources().getDimension(R.dimen.registration_profile_photo_radius);
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (C27611If.A00(r0.A05)) {
            this.A03.setEnabled(false);
            this.A02.setVisibility(0);
            A00 = this.A00;
            if (A00 == null) {
                A00 = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap.Config.ALPHA_8);
                this.A00 = A00;
            }
        } else {
            this.A03.setEnabled(true);
            this.A02.setVisibility(4);
            A00 = this.A08.A00(this, this.A09, dimension, dimensionPixelSize);
            if (A00 == null) {
                C15370n3 r1 = this.A09;
                if (r1.A05 == 0 && r1.A04 == 0) {
                    this.A02.setVisibility(0);
                    Handler handler = this.A01;
                    if (handler == null) {
                        handler = new Handler(Looper.getMainLooper());
                        this.A01 = handler;
                        this.A0I = new RunnableBRunnable0Shape9S0100000_I0_9(this, 37);
                    }
                    handler.removeCallbacks(this.A0I);
                    this.A01.postDelayed(this.A0I, C26061Bw.A0L);
                } else {
                    this.A02.setVisibility(4);
                }
                A00 = AnonymousClass130.A00(this, dimension, R.drawable.avatar_contact, dimensionPixelSize);
            }
        }
        this.A03.setImageBitmap(A00);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 12) {
            if (i != 13) {
                super.onActivityResult(i, i2, intent);
                return;
            }
            this.A0G.A01.A0M("tmpi").delete();
            if (i2 != -1) {
                if (i2 == 0 && intent != null) {
                    this.A0G.A03(intent, this);
                    return;
                }
                return;
            }
        } else if (i2 == -1) {
            if (intent != null) {
                if (intent.getBooleanExtra("is_reset", false)) {
                    this.A02.setVisibility(0);
                    this.A0G.A08(this.A09);
                    return;
                } else if (intent.getBooleanExtra("skip_cropping", false)) {
                    this.A0G.A01.A0M("tmpi").delete();
                }
            }
            this.A0G.A04(intent, this, 13);
            return;
        } else {
            return;
        }
        if (this.A0G.A0A(this.A09)) {
            this.A02.setVisibility(0);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (!this.A0B.A02()) {
            super.onBackPressed();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0111, code lost:
        if (X.C42941w9.A01 == false) goto L_0x0113;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r27) {
        /*
        // Method dump skipped, instructions count: 448
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.profile.ProfilePhotoReminder.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A07.A04(this.A0L);
        Handler handler = this.A01;
        if (handler != null) {
            handler.removeCallbacks(this.A0I);
        }
    }
}
