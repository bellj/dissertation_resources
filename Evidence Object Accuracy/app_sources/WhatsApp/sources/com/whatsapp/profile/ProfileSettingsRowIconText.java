package com.whatsapp.profile;

import X.AnonymousClass004;
import X.AnonymousClass018;
import X.AnonymousClass2GE;
import X.AnonymousClass2P5;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class ProfileSettingsRowIconText extends LinearLayout implements AnonymousClass004 {
    public TextEmojiLabel A00;
    public WaImageView A01;
    public WaImageView A02;
    public WaImageView A03;
    public WaTextView A04;
    public WaTextView A05;
    public AnonymousClass018 A06;
    public AnonymousClass2P7 A07;
    public boolean A08;

    public ProfileSettingsRowIconText(Context context) {
        this(context, null);
    }

    public ProfileSettingsRowIconText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        setOrientation(1);
        setGravity(8388627);
        LayoutInflater.from(context).inflate(R.layout.profile_settings_row_icon_text_description, (ViewGroup) this, true);
        this.A01 = (WaImageView) findViewById(R.id.profile_settings_row_icon);
        this.A02 = (WaImageView) findViewById(R.id.profile_settings_row_icon_placeholder);
        this.A03 = (WaImageView) findViewById(R.id.profile_settings_row_secondary_icon);
        this.A05 = (WaTextView) findViewById(R.id.profile_settings_row_text);
        this.A00 = (TextEmojiLabel) findViewById(R.id.profile_settings_row_subtext);
        this.A04 = (WaTextView) findViewById(R.id.profile_settings_row_description);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0G);
        try {
            setPrimaryIcon(obtainStyledAttributes.getDrawable(1));
            int color = obtainStyledAttributes.getColor(2, -1);
            if (color != -1) {
                AnonymousClass2GE.A07(this.A01, color);
            }
            setSecondaryIcon(obtainStyledAttributes.getDrawable(3));
            int color2 = obtainStyledAttributes.getColor(4, -1);
            if (color2 != -1) {
                AnonymousClass2GE.A07(this.A03, color2);
            }
            setText(this.A06.A0E(obtainStyledAttributes, 6));
            setSubText(this.A06.A0E(obtainStyledAttributes, 5));
            setDescription(this.A06.A0E(obtainStyledAttributes, 0));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public ProfileSettingsRowIconText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A08) {
            this.A08 = true;
            this.A06 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    public final void A00() {
        WaImageView waImageView;
        int i;
        if (this.A01.getVisibility() == 0 && this.A04.getVisibility() == 0) {
            waImageView = this.A02;
            i = 4;
        } else {
            waImageView = this.A02;
            i = 8;
        }
        waImageView.setVisibility(i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    public void setDescription(CharSequence charSequence) {
        WaTextView waTextView = this.A04;
        int i = 0;
        if (charSequence == null) {
            i = 8;
        }
        waTextView.setVisibility(i);
        A00();
        waTextView.setText(charSequence);
    }

    public void setPrimaryIcon(Drawable drawable) {
        WaImageView waImageView = this.A01;
        int i = 0;
        if (drawable == null) {
            i = 8;
        }
        waImageView.setVisibility(i);
        A00();
        waImageView.setImageDrawable(drawable);
        this.A02.setImageDrawable(drawable);
    }

    public void setSecondaryIcon(Drawable drawable) {
        WaImageView waImageView = this.A03;
        int i = 0;
        if (drawable == null) {
            i = 8;
        }
        waImageView.setVisibility(i);
        waImageView.setImageDrawable(drawable);
    }

    public void setSubText(CharSequence charSequence) {
        TextEmojiLabel textEmojiLabel = this.A00;
        int i = 0;
        if (charSequence == null) {
            i = 8;
        }
        textEmojiLabel.setVisibility(i);
        textEmojiLabel.A0G(null, charSequence);
    }

    public void setText(CharSequence charSequence) {
        WaTextView waTextView = this.A05;
        int i = 0;
        if (charSequence == null) {
            i = 8;
        }
        waTextView.setVisibility(i);
        waTextView.setText(charSequence);
    }
}
