package com.whatsapp.profile;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC27571Ib;
import X.AbstractC454421p;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00E;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10Z;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass131;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2TT;
import X.C015907n;
import X.C103264qT;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20680w9;
import X.C20690wA;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C235812f;
import X.C237913a;
import X.C248917h;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C26061Bw;
import X.C27131Gd;
import X.C27611If;
import X.C27621Ig;
import X.C28391Mz;
import X.C36421jr;
import X.C42941w9;
import X.C58602r3;
import X.C58612r4;
import X.C72663ey;
import X.C83803xv;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.transition.ChangeBounds;
import android.transition.Fade;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.PushnameEmojiBlacklistDialogFragment;
import com.whatsapp.R;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.profile.ProfileInfoActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ProfileInfoActivity extends ActivityC13790kL implements AbstractC27571Ib {
    public Handler A00;
    public View A01;
    public View A02;
    public ImageView A03;
    public C237913a A04;
    public C20690wA A05;
    public AnonymousClass130 A06;
    public AnonymousClass10S A07;
    public AnonymousClass131 A08;
    public C15370n3 A09;
    public C16120oU A0A;
    public WhatsAppLibLoader A0B;
    public C235812f A0C;
    public AnonymousClass10Z A0D;
    public ProfileSettingsRowIconText A0E;
    public ProfileSettingsRowIconText A0F;
    public C20680w9 A0G;
    public AnonymousClass12U A0H;
    public Runnable A0I;
    public boolean A0J;
    public boolean A0K;
    public final C27131Gd A0L;

    public ProfileInfoActivity() {
        this(0);
        this.A0L = new C36421jr(this);
    }

    public ProfileInfoActivity(int i) {
        this.A0K = false;
        A0R(new C103264qT(this));
    }

    public static /* synthetic */ void A03(ProfileInfoActivity profileInfoActivity) {
        int i;
        int i2;
        if (profileInfoActivity.A0J) {
            if (C28391Mz.A02()) {
                i = profileInfoActivity.getWindow().getStatusBarColor();
            } else {
                i = 0;
            }
            if (C28391Mz.A04()) {
                i2 = profileInfoActivity.getWindow().getNavigationBarColor();
            } else {
                i2 = 0;
            }
            C15570nT r0 = ((ActivityC13790kL) profileInfoActivity).A01;
            r0.A08();
            profileInfoActivity.startActivity(C14960mK.A0N(profileInfoActivity, r0.A05, null, 0.0f, i, 0, i2, 0, true), AbstractC454421p.A05(profileInfoActivity, profileInfoActivity.A03, new AnonymousClass2TT(profileInfoActivity).A00(R.string.transition_photo)));
            return;
        }
        profileInfoActivity.A0D.A06(profileInfoActivity, profileInfoActivity.A09, 12);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0K) {
            this.A0K = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A04 = (C237913a) r1.ACt.get();
            this.A0H = (AnonymousClass12U) r1.AJd.get();
            this.A0A = (C16120oU) r1.ANE.get();
            this.A0C = (C235812f) r1.A13.get();
            this.A06 = (AnonymousClass130) r1.A41.get();
            this.A0G = (C20680w9) r1.AGx.get();
            this.A05 = (C20690wA) r1.AJa.get();
            this.A07 = (AnonymousClass10S) r1.A46.get();
            this.A0B = (WhatsAppLibLoader) r1.ANa.get();
            this.A0D = (AnonymousClass10Z) r1.AGM.get();
            this.A08 = (AnonymousClass131) r1.A49.get();
        }
    }

    public final void A2e() {
        this.A02.setVisibility(8);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.pref_profile_photo_size);
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        boolean A00 = C27611If.A00(r0.A05);
        ImageView imageView = this.A03;
        if (A00) {
            imageView.setEnabled(false);
            this.A02.setVisibility(0);
        } else {
            imageView.setEnabled(true);
            this.A02.setVisibility(4);
        }
        Bitmap A002 = this.A08.A00(this, this.A09, -1.0f, dimensionPixelSize);
        if (A002 == null) {
            C15370n3 r1 = this.A09;
            if (r1.A05 == 0 && r1.A04 == 0) {
                this.A02.setVisibility(0);
                Handler handler = this.A00;
                if (handler == null) {
                    handler = new Handler(Looper.getMainLooper());
                    this.A00 = handler;
                    this.A0I = new RunnableBRunnable0Shape9S0100000_I0_9(this, 31);
                }
                handler.removeCallbacks(this.A0I);
                this.A00.postDelayed(this.A0I, C26061Bw.A0L);
            } else {
                this.A02.setVisibility(4);
            }
            A002 = AnonymousClass130.A00(this, -1.0f, R.drawable.avatar_contact, dimensionPixelSize);
            this.A0J = false;
        } else {
            this.A0J = true;
        }
        this.A03.setImageBitmap(A002);
    }

    public final void A2f(Runnable runnable) {
        View view = this.A01;
        if (view == null) {
            runnable.run();
        } else {
            view.animate().scaleX(0.0f).scaleY(0.0f).setDuration(125).setListener(new C72663ey(this, runnable));
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AbstractC27571Ib
    public void ANI(String str) {
        Adm(PushnameEmojiBlacklistDialogFragment.A00(str));
    }

    @Override // X.AbstractC27571Ib
    public void APb(int i, String str) {
        if (i == 0 && str.length() != 0) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1100000_I0(30, str, this));
            this.A0E.setSubText(str);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 12:
                if (i2 == -1) {
                    if (intent != null) {
                        if (intent.getBooleanExtra("is_reset", false)) {
                            this.A02.setVisibility(0);
                            this.A0D.A08(this.A09);
                        } else if (intent.getBooleanExtra("skip_cropping", false)) {
                            this.A0D.A01.A0M("tmpi").delete();
                            if (this.A0D.A0A(this.A09)) {
                                A2e();
                            }
                        }
                    }
                    this.A0D.A04(intent, this, 13);
                }
                if (((double) this.A01.getScaleX()) == 0.0d && ((double) this.A01.getScaleY()) == 0.0d) {
                    this.A01.animate().scaleX(1.0f).scaleY(1.0f).setDuration(125);
                    return;
                }
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A0D.A01.A0M("tmpi").delete();
                if (i2 == -1) {
                    if (this.A0D.A0A(this.A09)) {
                        A2e();
                        return;
                    }
                    return;
                } else if (i2 == 0 && intent != null) {
                    this.A0D.A03(intent, this);
                    return;
                } else {
                    return;
                }
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                if (i2 == -1) {
                    this.A0E.setSubText(((ActivityC13790kL) this).A01.A05());
                    return;
                }
                return;
            default:
                super.onActivityResult(i, i2, intent);
                return;
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        RunnableBRunnable0Shape9S0100000_I0_9 runnableBRunnable0Shape9S0100000_I0_9 = new RunnableBRunnable0Shape9S0100000_I0_9(this, 32);
        if (AbstractC454421p.A00) {
            A2f(runnableBRunnable0Shape9S0100000_I0_9);
        } else {
            runnableBRunnable0Shape9S0100000_I0_9.run();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        if (AbstractC454421p.A00) {
            Window window = getWindow();
            window.requestFeature(12);
            window.requestFeature(13);
            ChangeBounds changeBounds = new ChangeBounds();
            changeBounds.setDuration(200);
            changeBounds.setInterpolator(new C015907n());
            changeBounds.excludeTarget(16908335, true);
            changeBounds.excludeTarget(16908336, true);
            changeBounds.excludeTarget(R.id.action_bar_container, true);
            window.setSharedElementEnterTransition(changeBounds);
            window.setSharedElementExitTransition(changeBounds);
            Fade fade = new Fade();
            Fade fade2 = new Fade();
            fade.setDuration(220);
            fade.excludeTarget(16908335, true);
            fade.excludeTarget(16908336, true);
            fade.excludeTarget(R.id.action_bar_container, true);
            fade.excludeTarget(R.id.catalog_image_list_toolbar, true);
            fade2.setDuration(240);
            fade2.excludeTarget(16908335, true);
            fade2.excludeTarget(16908336, true);
            fade2.excludeTarget(R.id.action_bar_container, true);
            fade2.excludeTarget(R.id.catalog_image_list_toolbar, true);
            window.setEnterTransition(fade);
            window.setReturnTransition(fade2);
        }
        super.onCreate(bundle);
        if (!this.A0B.A03()) {
            Log.i("aborting due to native libraries missing");
        } else {
            setContentView(R.layout.profile_info);
            AbstractC005102i A1U = A1U();
            if (A1U != null) {
                A1U.A0M(true);
            }
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            C27621Ig r02 = r0.A01;
            this.A09 = r02;
            if (r02 == null) {
                Log.i("profileinfo/create/no-me");
                startActivity(C14960mK.A04(this));
            } else {
                ProfileSettingsRowIconText profileSettingsRowIconText = (ProfileSettingsRowIconText) findViewById(R.id.profile_info_name_card);
                this.A0E = profileSettingsRowIconText;
                profileSettingsRowIconText.setSubText(((ActivityC13790kL) this).A01.A05());
                this.A0E.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 8));
                ImageView imageView = (ImageView) findViewById(R.id.photo_btn);
                this.A03 = imageView;
                imageView.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 9));
                View findViewById = findViewById(R.id.change_photo_btn);
                this.A01 = findViewById;
                findViewById.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 7));
                if (Build.VERSION.SDK_INT >= 21 && bundle == null) {
                    Transition sharedElementEnterTransition = getWindow().getSharedElementEnterTransition();
                    getWindow().setSharedElementExitTransition(sharedElementEnterTransition.clone());
                    getWindow().setSharedElementReenterTransition(sharedElementEnterTransition.clone());
                    getWindow().setSharedElementReturnTransition(sharedElementEnterTransition.clone());
                    this.A01.setScaleX(0.0f);
                    this.A01.setScaleY(0.0f);
                    this.A01.setVisibility(0);
                    getWindow().getSharedElementEnterTransition().addListener(new C83803xv(this));
                    getWindow().getSharedElementExitTransition().addListener(new C58602r3(this));
                    getWindow().getSharedElementReenterTransition().addListener(new C58612r4(this));
                }
                this.A02 = findViewById(R.id.change_photo_progress);
                A2e();
                ProfileSettingsRowIconText profileSettingsRowIconText2 = (ProfileSettingsRowIconText) findViewById(R.id.profile_phone_info);
                C42941w9.A03(profileSettingsRowIconText2.A00);
                profileSettingsRowIconText2.setSubText(C248917h.A01(this.A09));
                ((ActivityC13790kL) this).A01.A08();
                profileSettingsRowIconText2.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 9));
                ProfileSettingsRowIconText profileSettingsRowIconText3 = (ProfileSettingsRowIconText) findViewById(R.id.profile_info_status_card);
                this.A0F = profileSettingsRowIconText3;
                profileSettingsRowIconText3.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 10));
                this.A0F.setSubText(this.A04.A00());
                this.A07.A03(this.A0L);
                if ("android.intent.action.ATTACH_DATA".equals(getIntent().getAction())) {
                    setTitle(R.string.set_as_profile_photo);
                    this.A0D.A04(getIntent(), this, 13);
                } else {
                    setTitle(R.string.settings_profile_info);
                }
                this.A0C.A01(4);
                return;
            }
        }
        finish();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0C.A00(4);
        this.A07.A04(this.A0L);
        Handler handler = this.A00;
        if (handler != null) {
            handler.removeCallbacks(this.A0I);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (AbstractC454421p.A00) {
            A2f(new Runnable() { // from class: X.3ky
                @Override // java.lang.Runnable
                public final void run() {
                    ProfileInfoActivity.this.finishAfterTransition();
                }
            });
            return true;
        }
        finish();
        return true;
    }
}
