package com.whatsapp.profile;

import X.AbstractC005102i;
import X.AbstractC11190ft;
import X.AbstractC32741cf;
import X.AbstractC32851cq;
import X.ActivityC13770kJ;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass36n;
import X.AnonymousClass3WZ;
import X.C103814rM;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C16590pI;
import X.C17050qB;
import X.C18790t3;
import X.C19930uu;
import X.C27531Hw;
import X.C38721ob;
import X.C38771og;
import X.C52772be;
import X.C627838o;
import X.C63403Bl;
import X.C74233hd;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class WebImagePicker extends ActivityC13770kJ {
    public int A00;
    public int A01;
    public Uri A02;
    public View.OnClickListener A03;
    public View A04;
    public View A05;
    public ProgressBar A06;
    public SearchView A07;
    public C18790t3 A08;
    public C17050qB A09;
    public C16590pI A0A;
    public C627838o A0B;
    public C52772be A0C;
    public C63403Bl A0D;
    public C38721ob A0E;
    public C19930uu A0F;
    public File A0G;
    public boolean A0H;
    public final AbstractC32851cq A0I;
    public final ArrayList A0J;

    public WebImagePicker() {
        this(0);
        this.A0J = C12960it.A0l();
        this.A00 = 4;
        this.A0I = new AnonymousClass3WZ(this);
    }

    public WebImagePicker(int i) {
        this.A0H = false;
        ActivityC13830kP.A1P(this, 95);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0H) {
            this.A0H = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0F = (C19930uu) A1M.AM5.get();
            this.A0A = C12970iu.A0X(A1M);
            this.A08 = C12990iw.A0U(A1M);
            this.A09 = (C17050qB) A1M.ABL.get();
        }
    }

    public final void A2g() {
        int A01 = (int) (C12960it.A01(this) * 3.3333333f);
        this.A01 = C27531Hw.A01(this) + (((int) (C12960it.A01(this) * 1.3333334f)) << 1) + A01;
        Point point = new Point();
        C12970iu.A17(this, point);
        int i = point.x;
        int i2 = i / this.A01;
        this.A00 = i2;
        this.A01 = (i / i2) - A01;
        C38721ob r0 = this.A0E;
        if (r0 != null) {
            r0.A02.A02(false);
        }
        C38771og r2 = new C38771og(((ActivityC13810kN) this).A05, this.A08, ((ActivityC13810kN) this).A0D, this.A0G, "web-image-picker");
        r2.A00 = this.A01;
        r2.A01 = 4194304;
        r2.A03 = AnonymousClass00T.A04(this, R.drawable.picture_loading);
        r2.A02 = AnonymousClass00T.A04(this, R.drawable.ic_missing_thumbnail_picture);
        this.A0E = r2.A00();
    }

    public final void A2h() {
        String charSequence = this.A07.A0k.getText().toString();
        if (TextUtils.isEmpty(charSequence)) {
            ((ActivityC13810kN) this).A05.A07(R.string.photo_nothing_to_search, 0);
            return;
        }
        ((ActivityC13790kL) this).A0D.A01(this.A07);
        this.A06.setVisibility(0);
        C12990iw.A1G((TextView) A2e().getEmptyView());
        C52772be r2 = this.A0C;
        if (charSequence != null) {
            AnonymousClass36n r0 = r2.A00;
            if (r0 != null) {
                r0.A03(false);
            }
            r2.A01 = true;
            WebImagePicker webImagePicker = r2.A02;
            webImagePicker.A0D = new C63403Bl(webImagePicker.A08, webImagePicker.A0A, ((ActivityC13810kN) webImagePicker).A0D, charSequence);
            webImagePicker.A0J.clear();
            webImagePicker.A0E.A02.A02(false);
            C38771og r6 = new C38771og(((ActivityC13810kN) webImagePicker).A05, webImagePicker.A08, ((ActivityC13810kN) webImagePicker).A0D, webImagePicker.A0G, "web-image-picker-adapter");
            r6.A00 = webImagePicker.A01;
            r6.A01 = 4194304;
            r6.A03 = AnonymousClass00T.A04(webImagePicker, R.drawable.gray_rectangle);
            r6.A02 = AnonymousClass00T.A04(webImagePicker, R.drawable.ic_missing_thumbnail_picture);
            webImagePicker.A0E = r6.A00();
        }
        AnonymousClass36n r1 = new AnonymousClass36n(r2);
        r2.A00 = r1;
        C12990iw.A1N(r1, ((ActivityC13830kP) r2.A02).A05);
        if (charSequence != null) {
            r2.notifyDataSetChanged();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 151) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            A2h();
        } else {
            finish();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A2g();
        this.A0C.notifyDataSetChanged();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.search_web);
        this.A0G = new File(getCacheDir(), "Thumbs");
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        A0N.A0P(false);
        A0N.A0N(true);
        this.A0G.mkdirs();
        C63403Bl r0 = new C63403Bl(this.A08, this.A0A, ((ActivityC13810kN) this).A0D, "");
        this.A0D = r0;
        File[] listFiles = r0.A07.listFiles();
        if (listFiles != null) {
            Arrays.sort(listFiles, new IDxComparatorShape3S0000000_2_I1(21));
            int i = 0;
            while (true) {
                int length = listFiles.length;
                if (i >= length) {
                    break;
                }
                File file = listFiles[i];
                if (i <= length - 16 || file.lastModified() + 86400000 <= System.currentTimeMillis()) {
                    file.delete();
                }
                i++;
            }
        }
        setContentView(R.layout.web_image_picker);
        this.A06 = (ProgressBar) findViewById(R.id.indefiniteProgressBar);
        String stringExtra = getIntent().getStringExtra("query");
        if (stringExtra != null) {
            stringExtra = AbstractC32741cf.A03(stringExtra);
        }
        C74233hd r1 = new C74233hd(A0N.A02(), this);
        this.A07 = r1;
        C12960it.A0s(this, C12960it.A0J(r1, R.id.search_src_text), R.color.search_text_color_dark);
        this.A07.setQueryHint(getString(R.string.search_hint));
        this.A07.setIconified(false);
        SearchView searchView = this.A07;
        searchView.A0A = new AbstractC11190ft() { // from class: X.4rD
        };
        searchView.A0F(stringExtra);
        SearchView searchView2 = this.A07;
        searchView2.A07 = new ViewOnClickCListenerShape10S0100000_I1_4(this, 13);
        searchView2.A0B = new C103814rM(this);
        A0N.A0F(searchView2);
        Bundle A0H = C12990iw.A0H(this);
        if (A0H != null) {
            this.A02 = (Uri) A0H.getParcelable("output");
        }
        ListView A2e = A2e();
        A2e.requestFocus();
        A2e.setClickable(false);
        A2e.setBackground(null);
        A2e.setDividerHeight(0);
        View inflate = getLayoutInflater().inflate(R.layout.web_image_picker_footer, (ViewGroup) A2e, false);
        A2e.addFooterView(inflate, null, false);
        A2e.setFooterDividersEnabled(false);
        this.A05 = inflate.findViewById(R.id.progress);
        this.A04 = inflate.findViewById(R.id.attribution);
        C52772be r02 = new C52772be(this);
        this.A0C = r02;
        A2f(r02);
        this.A03 = new ViewOnClickCListenerShape10S0100000_I1_4(this, 14);
        A2g();
        this.A09.A03(this.A0I);
        this.A07.requestFocus();
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0J.clear();
        this.A0E.A02.A02(true);
        C627838o r0 = this.A0B;
        if (r0 != null) {
            r0.A03(true);
            Log.i("webimagesearch/cancel_image_download_task");
            if (this.A0B.A00 != null) {
                Log.i("webimagesearch/cancel_dialog");
                this.A0B.A00.dismiss();
                this.A0B.A00 = null;
            }
            this.A0B = null;
        }
        AnonymousClass36n r1 = this.A0C.A00;
        if (r1 != null) {
            r1.A03(false);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }
}
