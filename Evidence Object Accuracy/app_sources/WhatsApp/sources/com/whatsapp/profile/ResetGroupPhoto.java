package com.whatsapp.profile;

import X.ActivityC000900k;
import X.ActivityC13830kP;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ResetGroupPhoto extends ActivityC13830kP {
    public boolean A00;

    public ResetGroupPhoto() {
        this(0);
    }

    public ResetGroupPhoto(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 93);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ((ActivityC13830kP) this).A05 = C12960it.A0T(ActivityC13830kP.A1L(this).A1E);
        }
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.remove_photo);
        if (bundle == null) {
            C12960it.A16(new ConfirmDialogFragment(), this);
        }
    }

    /* loaded from: classes2.dex */
    public class ConfirmDialogFragment extends Hilt_ResetGroupPhoto_ConfirmDialogFragment {
        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e A0K = C12960it.A0K(this);
            A0K.A06(R.string.remove_group_icon_confirmation);
            A0K.A0B(true);
            C12970iu.A1K(A0K, this, 49, R.string.cancel);
            C12970iu.A1M(A0K, this, 25, R.string.remove);
            return A0K.create();
        }

        @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
        public void onDismiss(DialogInterface dialogInterface) {
            super.onDismiss(dialogInterface);
            ActivityC000900k A0B = A0B();
            if (A0B != null) {
                A0B.finish();
                A0B.overridePendingTransition(17432576, 17432577);
            }
        }
    }
}
