package com.whatsapp.profile;

import X.AbstractActivityC58522pc;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import android.content.Intent;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class ProfilePhotoPrivacyActivity extends AbstractActivityC58522pc {
    public int A00;
    public boolean A01;

    public ProfilePhotoPrivacyActivity() {
        this(0);
    }

    public ProfilePhotoPrivacyActivity(int i) {
        this.A01 = false;
        ActivityC13830kP.A1P(this, 92);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            this.A00 = 3;
            Intent A0A = C12970iu.A0A();
            A0A.putExtra("profile_photo", this.A00);
            C12960it.A0q(this, A0A);
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.AbstractActivityC58522pc, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00 = C12970iu.A01(((ActivityC13810kN) this).A09.A00, "privacy_profile_photo");
    }
}
