package com.whatsapp.profile;

import X.AbstractC009404s;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC33331dp;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass004;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass10T;
import X.AnonymousClass10Z;
import X.AnonymousClass11A;
import X.AnonymousClass12P;
import X.AnonymousClass131;
import X.AnonymousClass19M;
import X.AnonymousClass1CY;
import X.AnonymousClass2Dn;
import X.AnonymousClass2FH;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass5UJ;
import X.C103284qV;
import X.C12960it;
import X.C12970iu;
import X.C14330lG;
import X.C14350lI;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C17050qB;
import X.C18470sV;
import X.C18640sm;
import X.C18720su;
import X.C18810t5;
import X.C20750wG;
import X.C21820y2;
import X.C21840y4;
import X.C22200yh;
import X.C22330yu;
import X.C22670zS;
import X.C244215l;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C27611If;
import X.C36851kk;
import X.C37501mV;
import X.C38211ni;
import X.C41861uH;
import X.C48572Gu;
import X.C66553Oa;
import X.C850841c;
import X.C858744m;
import X.HandlerC52022a6;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.profile.ViewProfilePhoto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/* loaded from: classes2.dex */
public class ViewProfilePhoto extends ActivityC13790kL {
    public C18720su A00;
    public C22330yu A01;
    public C15550nR A02;
    public AnonymousClass10S A03;
    public C15610nY A04;
    public AnonymousClass10T A05;
    public AnonymousClass131 A06;
    public C17050qB A07;
    public C15890o4 A08;
    public C15600nX A09;
    public C15370n3 A0A;
    public AnonymousClass5UJ A0B;
    public AnonymousClass11A A0C;
    public C244215l A0D;
    public C20750wG A0E;
    public AnonymousClass10Z A0F;
    public AnonymousClass1CY A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public final Handler A0L;
    public final AnonymousClass2Dn A0M;
    public final C27131Gd A0N;
    public final AbstractC33331dp A0O;

    public ViewProfilePhoto() {
        this(0);
        this.A0K = false;
        this.A0J = false;
        this.A0L = new HandlerC52022a6(Looper.getMainLooper(), this);
        this.A0N = new C36851kk(this);
        this.A0M = new C850841c(this);
        this.A0O = new C858744m(this);
        this.A0B = new AnonymousClass5UJ() { // from class: X.57G
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                ViewProfilePhoto viewProfilePhoto = ViewProfilePhoto.this;
                C15370n3 r0 = viewProfilePhoto.A0A;
                if (r0 != null) {
                    Jid A0A = r0.A0A();
                    AnonymousClass009.A05(A0A);
                    if (A0A.equals(r3)) {
                        viewProfilePhoto.A0b();
                    }
                }
            }
        };
    }

    public ViewProfilePhoto(int i) {
        this.A0I = false;
        A0R(new C103284qV(this));
    }

    public static /* synthetic */ void A02(ViewProfilePhoto viewProfilePhoto) {
        C15550nR r2 = viewProfilePhoto.A02;
        Jid A0B = viewProfilePhoto.A0A.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        C15370n3 A0B2 = r2.A0B((AbstractC14640lm) A0B);
        viewProfilePhoto.A0A = A0B2;
        if (A0B2.A0K()) {
            viewProfilePhoto.setTitle(R.string.group_photo);
        } else {
            viewProfilePhoto.A2O(viewProfilePhoto.A04.A04(viewProfilePhoto.A0A));
        }
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0I) {
            this.A0I = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A00 = (C18720su) r1.A2c.get();
            this.A0G = (AnonymousClass1CY) r1.ABO.get();
            this.A02 = (C15550nR) r1.A45.get();
            this.A04 = (C15610nY) r1.AMe.get();
            this.A03 = (AnonymousClass10S) r1.A46.get();
            this.A07 = (C17050qB) r1.ABL.get();
            this.A01 = (C22330yu) r1.A3I.get();
            this.A05 = (AnonymousClass10T) r1.A47.get();
            this.A0E = (C20750wG) r1.AGL.get();
            this.A0F = (AnonymousClass10Z) r1.AGM.get();
            this.A08 = (C15890o4) r1.AN1.get();
            this.A0C = (AnonymousClass11A) r1.A8o.get();
            this.A09 = (C15600nX) r1.A8x.get();
            this.A06 = (AnonymousClass131) r1.A49.get();
            this.A0D = (C244215l) r1.A8y.get();
        }
    }

    public final void A2e() {
        View findViewById = findViewById(R.id.progress_bar);
        PhotoView photoView = (PhotoView) findViewById(R.id.picture);
        TextView textView = (TextView) findViewById(R.id.message);
        ImageView imageView = (ImageView) findViewById(R.id.picture_animation);
        if (C27611If.A00((AbstractC14640lm) this.A0A.A0B(AbstractC14640lm.class))) {
            findViewById.setVisibility(0);
            photoView.setVisibility(8);
            textView.setVisibility(8);
            return;
        }
        C14850m9 r1 = ((ActivityC13810kN) this).A0C;
        C15370n3 r0 = this.A0A;
        if (r0 == null || !C41861uH.A00(r1, r0.A0D)) {
            try {
                InputStream A02 = this.A06.A02(this.A0A, true);
                if (A02 == null) {
                    photoView.setVisibility(8);
                    findViewById.setVisibility(8);
                    textView.setVisibility(0);
                    imageView.setVisibility(8);
                    boolean A0K = this.A0A.A0K();
                    int i = R.string.no_profile_photo;
                    if (A0K) {
                        i = R.string.no_group_photo;
                    }
                    textView.setText(i);
                    return;
                }
                photoView.setVisibility(0);
                textView.setVisibility(8);
                if (this.A0A.A04 == 0) {
                    findViewById.setVisibility(0);
                } else {
                    findViewById.setVisibility(8);
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inDither = true;
                Bitmap A022 = C37501mV.A02(options, A02);
                photoView.A05(A022);
                imageView.setImageBitmap(A022);
                A02.close();
            } catch (IOException unused) {
            }
        } else {
            findViewById.setVisibility(8);
            photoView.setVisibility(8);
            textView.setVisibility(8);
            imageView.setImageResource(R.drawable.avatar_server_psa);
        }
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008b, code lost:
        if (r8.getBooleanExtra("skip_cropping", false) != false) goto L_0x008d;
     */
    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r6, int r7, android.content.Intent r8) {
        /*
            r5 = this;
            r0 = 12
            r2 = 1
            r4 = -1
            r3 = 13
            if (r6 == r0) goto L_0x005b
            if (r6 == r3) goto L_0x000e
            super.onActivityResult(r6, r7, r8)
        L_0x000d:
            return
        L_0x000e:
            X.10Z r0 = r5.A0F
            X.0lG r0 = r0.A01
            java.lang.String r3 = "tmpi"
            java.io.File r0 = r0.A0M(r3)
            boolean r0 = r0.delete()
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = "viewprofilephoto/failed-delete-file"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.10Z r0 = r5.A0F
            X.0lG r0 = r0.A01
            java.io.File r0 = r0.A0M(r3)
            java.lang.String r0 = r0.getAbsolutePath()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.w(r0)
        L_0x003d:
            if (r7 != r4) goto L_0x0051
            r5.A0J = r2
            X.10S r2 = r5.A03
            X.0n3 r1 = r5.A0A
            java.lang.Class<X.0lm> r0 = X.AbstractC14640lm.class
            com.whatsapp.jid.Jid r0 = r1.A0B(r0)
            X.0lm r0 = (X.AbstractC14640lm) r0
            r2.A06(r0)
            goto L_0x008d
        L_0x0051:
            if (r7 != 0) goto L_0x000d
            if (r8 == 0) goto L_0x000d
            X.10Z r0 = r5.A0F
            r0.A03(r8, r5)
            return
        L_0x005b:
            if (r7 != r4) goto L_0x000d
            r1 = 0
            if (r8 == 0) goto L_0x009b
            java.lang.String r0 = "is_reset"
            boolean r0 = r8.getBooleanExtra(r0, r1)
            if (r0 == 0) goto L_0x0084
            r5.A0J = r2
            X.10S r2 = r5.A03
            X.0n3 r1 = r5.A0A
            java.lang.Class<X.0lm> r0 = X.AbstractC14640lm.class
            com.whatsapp.jid.Jid r0 = r1.A0B(r0)
            X.0lm r0 = (X.AbstractC14640lm) r0
            r2.A06(r0)
            X.10Z r1 = r5.A0F
            X.0n3 r0 = r5.A0A
            r1.A08(r0)
            r5.A0a()
            return
        L_0x0084:
            java.lang.String r0 = "skip_cropping"
            boolean r0 = r8.getBooleanExtra(r0, r1)
            if (r0 == 0) goto L_0x009b
        L_0x008d:
            X.10Z r1 = r5.A0F
            X.0n3 r0 = r5.A0A
            boolean r0 = r1.A0A(r0)
            if (r0 == 0) goto L_0x000d
            r5.A2e()
            return
        L_0x009b:
            X.10Z r0 = r5.A0F
            r0.A04(r8, r5, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.profile.ViewProfilePhoto.onActivityResult(int, int, android.content.Intent):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02ad, code lost:
        if (X.C41861uH.A00(((X.ActivityC13810kN) r14).A0C, r7.A0D) == false) goto L_0x01bb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0266  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r15) {
        /*
        // Method dump skipped, instructions count: 715
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.profile.ViewProfilePhoto.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C15370n3 r1 = this.A0A;
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (r1.equals(r0.A01) || this.A0A.A0K()) {
            menu.add(0, R.id.menuitem_edit, 0, R.string.edit_photo).setIcon(R.drawable.ic_action_edit).setShowAsAction(2);
            menu.add(0, 1, 0, R.string.share).setIcon(R.drawable.ic_action_share).setShowAsAction(2);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0L.removeMessages(0);
        this.A03.A04(this.A0N);
        this.A01.A04(this.A0M);
        AnonymousClass11A r0 = this.A0C;
        r0.A00.remove(this.A0B);
        this.A0D.A04(this.A0O);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_edit) {
            this.A0F.A07(this, this.A0A, 12, 1, this.A0H, false);
            return true;
        } else if (itemId == 1) {
            C14330lG r2 = ((ActivityC13810kN) this).A04;
            C15370n3 r1 = this.A0A;
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            if (r1.equals(r0.A01)) {
                str = "me.jpg";
            } else {
                str = "photo.jpg";
            }
            File A0M = r2.A0M(str);
            try {
                File A00 = this.A05.A00(this.A0A);
                AnonymousClass009.A05(A00);
                FileInputStream fileInputStream = new FileInputStream(A00);
                FileOutputStream fileOutputStream = new FileOutputStream(A0M);
                C14350lI.A0G(fileInputStream, fileOutputStream);
                Uri A01 = C14350lI.A01(this, A0M);
                this.A00.A02().A00.A07(A01.toString());
                startActivity(C38211ni.A01(null, null, Arrays.asList(new Intent("android.intent.action.SEND").setType("image/*").putExtra("android.intent.extra.STREAM", A01), new Intent(this, SavePhoto.class).putExtra("android.intent.extra.STREAM", Uri.fromFile(A0M)).putExtra("name", this.A04.A04(this.A0A)))));
                fileOutputStream.close();
                fileInputStream.close();
                return true;
            } catch (IOException e) {
                Log.e(e);
                ((ActivityC13810kN) this).A05.A07(R.string.photo_faled_save_to_gallery, 1);
                return true;
            }
        } else if (itemId != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            AnonymousClass00T.A08(this);
            return true;
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu.size() != 0) {
            C15370n3 r1 = this.A0A;
            C15570nT r0 = ((ActivityC13790kL) this).A01;
            r0.A08();
            boolean equals = r1.equals(r0.A01);
            if (equals || this.A0A.A0K()) {
                boolean z = true;
                MenuItem findItem = menu.findItem(1);
                File A00 = this.A05.A00(this.A0A);
                AnonymousClass009.A05(A00);
                findItem.setVisible(A00.exists());
                MenuItem findItem2 = menu.findItem(R.id.menuitem_edit);
                if (!equals) {
                    C15600nX r2 = this.A09;
                    Jid A0B = this.A0A.A0B(C15580nU.class);
                    AnonymousClass009.A05(A0B);
                    if (!r2.A0D((GroupJid) A0B) && this.A0A.A0i) {
                        z = false;
                    }
                }
                findItem2.setVisible(z);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A0K = bundle.getBoolean("photo_change_requested_externally");
        this.A0J = bundle.getBoolean("photo_change_requested_by_phone");
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("photo_change_requested_externally", this.A0K);
        bundle.putBoolean("photo_change_requested_by_phone", this.A0J);
    }

    /* loaded from: classes2.dex */
    public class SavePhoto extends ActivityC000900k implements AnonymousClass004 {
        public C14330lG A00;
        public C14900mE A01;
        public boolean A02;
        public final Object A03;
        public volatile AnonymousClass2FH A04;

        public SavePhoto() {
            this(0);
        }

        public SavePhoto(int i) {
            this.A03 = C12970iu.A0l();
            this.A02 = false;
            A0R(new C66553Oa(this));
        }

        @Override // X.ActivityC001000l, X.AbstractC001800t
        public AbstractC009404s ACV() {
            return C48572Gu.A00(this, super.ACV());
        }

        @Override // X.AnonymousClass005
        public final Object generatedComponent() {
            if (this.A04 == null) {
                synchronized (this.A03) {
                    if (this.A04 == null) {
                        this.A04 = new AnonymousClass2FH(this);
                    }
                }
            }
            return this.A04.generatedComponent();
        }

        @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            setTitle(R.string.save_to_gallery);
            Uri uri = (Uri) getIntent().getParcelableExtra("android.intent.extra.STREAM");
            String replaceAll = getIntent().getStringExtra("name").replaceAll("[?:\\\\/*\"<>|]", "");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
            File file = this.A00.A04().A0G;
            C14330lG.A03(file, false);
            StringBuilder A0j = C12960it.A0j(replaceAll);
            A0j.append(" ");
            A0j.append(simpleDateFormat.format(new Date()));
            File file2 = new File(file, C12960it.A0d(".jpg", A0j));
            try {
                C14330lG r2 = this.A00;
                C14350lI.A0A(r2.A04, new File(uri.getPath()), file2);
                C22200yh.A0P(this, Uri.fromFile(file2));
                this.A01.A07(R.string.photo_saved_to_gallery, 0);
            } catch (IOException e) {
                Log.e("viewprofilephoto/save/failed", e);
                this.A01.A07(R.string.photo_faled_save_to_gallery, 1);
            }
            finish();
        }
    }
}
