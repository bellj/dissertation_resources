package com.whatsapp;

import X.AnonymousClass01d;
import X.AnonymousClass2GZ;
import X.C14900mE;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

/* loaded from: classes2.dex */
public class CopyableTextView extends WaTextView implements View.OnClickListener {
    public C14900mE A00;
    public AnonymousClass01d A01;
    public String A02;
    public String A03;
    public boolean A04;

    public CopyableTextView(Context context) {
        super(context);
        A08();
        A0A(context, null);
    }

    public CopyableTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
        A0A(context, attributeSet);
    }

    public CopyableTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
        A0A(context, attributeSet);
    }

    public CopyableTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        A08();
    }

    public final void A0A(Context context, AttributeSet attributeSet) {
        setClickable(true);
        setTextIsSelectable(true);
        setOnClickListener(this);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A08);
            int resourceId = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId != 0) {
                this.A03 = ((WaTextView) this).A01.A09(resourceId);
            }
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        ClipboardManager A0B;
        CharSequence charSequence;
        if (!TextUtils.isEmpty(this.A03) && (A0B = this.A01.A0B()) != null) {
            CharSequence text = getText();
            if (TextUtils.isEmpty(this.A02)) {
                charSequence = getText();
            } else {
                charSequence = this.A02;
            }
            try {
                A0B.setPrimaryClip(ClipData.newPlainText(text, charSequence));
                this.A00.A0E(this.A03, 0);
            } catch (NullPointerException | SecurityException unused) {
            }
        }
    }

    public void setDataToCopy(String str) {
        this.A02 = str;
    }

    public void setToastString(String str) {
        this.A03 = str;
    }
}
