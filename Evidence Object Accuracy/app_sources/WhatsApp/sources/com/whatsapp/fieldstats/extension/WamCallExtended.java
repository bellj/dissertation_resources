package com.whatsapp.fieldstats.extension;

import X.AnonymousClass1N6;
import com.whatsapp.fieldstats.events.WamCall;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public final class WamCallExtended extends WamCall {
    public List fields = new ArrayList();

    public void addField(WamCallExtendedField wamCallExtendedField) {
        this.fields.add(wamCallExtendedField);
    }

    @Override // com.whatsapp.fieldstats.events.WamCall, X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r4) {
        super.serialize(r4);
        for (WamCallExtendedField wamCallExtendedField : this.fields) {
            r4.Abe(wamCallExtendedField.fieldId, wamCallExtendedField.fieldValue);
        }
    }
}
