package com.whatsapp.fieldstats.extension;

import X.AnonymousClass009;
import X.C99904l3;
import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes2.dex */
public class WamCallExtendedField implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99904l3();
    public final int fieldId;
    public final String fieldType;
    public final Object fieldValue;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public WamCallExtendedField(int i, String str, Object obj) {
        this.fieldId = i;
        this.fieldType = str;
        this.fieldValue = obj;
    }

    public WamCallExtendedField(Parcel parcel) {
        this.fieldId = parcel.readInt();
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.fieldType = readString;
        this.fieldValue = parcel.readValue(null);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.fieldId);
        parcel.writeString(this.fieldType);
        parcel.writeValue(this.fieldValue);
    }
}
