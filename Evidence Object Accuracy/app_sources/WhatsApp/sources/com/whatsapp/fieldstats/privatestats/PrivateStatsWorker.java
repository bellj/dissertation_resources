package com.whatsapp.fieldstats.privatestats;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass043;
import X.AnonymousClass0GL;
import X.C006503b;
import X.C16080oP;
import android.content.Context;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class PrivateStatsWorker extends Worker {
    public final Context A00;
    public final C16080oP A01;

    public PrivateStatsWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.A00 = context;
        this.A01 = (C16080oP) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AGC.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        Log.i("PrivateStatsWorker/doWork--->>> in doWork");
        C16080oP r3 = this.A01;
        r3.A07.Ab2(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 30));
        return new AnonymousClass0GL(C006503b.A01);
    }
}
