package com.whatsapp.fieldstats.events;

import X.AbstractC16110oT;
import X.AnonymousClass1N6;

/* loaded from: classes2.dex */
public final class WamJoinableCall extends AbstractC16110oT {
    public Long acceptAckLatencyMs;
    public String callRandomId;
    public String callReplayerId;
    public Boolean hasSpamDialog;
    public Boolean isCallFull;
    public Boolean isFromCallLink;
    public Boolean isLinkedGroupCall;
    public Boolean isPendingCall;
    public Boolean isRejoin;
    public Boolean isRering;
    public Boolean joinableAcceptBeforeLobbyAck;
    public Boolean joinableDuringCall;
    public Boolean joinableEndCallBeforeLobbyAck;
    public Integer legacyCallResult;
    public Long lobbyAckLatencyMs;
    public Integer lobbyEntryPoint;
    public Integer lobbyExit;
    public Long lobbyExitNackCode;
    public Boolean lobbyQueryWhileConnected;
    public Long lobbyVisibleT;
    public Boolean nseEnabled;
    public Long nseOfflineQueueMs;
    public Long numConnectedPeers;
    public Long numInvitedParticipants;
    public Long numOutgoingRingingPeers;
    public Boolean previousJoinNotEnded;
    public Boolean receivedByNse;
    public Boolean rejoinMissingDbMapping;
    public Long timeSinceLastClientPollMinutes;
    public Boolean videoEnabled;

    public WamJoinableCall() {
        super(2572, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(23, this.acceptAckLatencyMs);
        r3.Abe(1, this.callRandomId);
        r3.Abe(31, this.callReplayerId);
        r3.Abe(26, this.hasSpamDialog);
        r3.Abe(30, this.isCallFull);
        r3.Abe(32, this.isFromCallLink);
        r3.Abe(24, this.isLinkedGroupCall);
        r3.Abe(14, this.isPendingCall);
        r3.Abe(3, this.isRejoin);
        r3.Abe(8, this.isRering);
        r3.Abe(16, this.joinableAcceptBeforeLobbyAck);
        r3.Abe(9, this.joinableDuringCall);
        r3.Abe(17, this.joinableEndCallBeforeLobbyAck);
        r3.Abe(6, this.legacyCallResult);
        r3.Abe(19, this.lobbyAckLatencyMs);
        r3.Abe(2, this.lobbyEntryPoint);
        r3.Abe(4, this.lobbyExit);
        r3.Abe(5, this.lobbyExitNackCode);
        r3.Abe(18, this.lobbyQueryWhileConnected);
        r3.Abe(7, this.lobbyVisibleT);
        r3.Abe(27, this.nseEnabled);
        r3.Abe(28, this.nseOfflineQueueMs);
        r3.Abe(13, this.numConnectedPeers);
        r3.Abe(12, this.numInvitedParticipants);
        r3.Abe(20, this.numOutgoingRingingPeers);
        r3.Abe(15, this.previousJoinNotEnded);
        r3.Abe(29, this.receivedByNse);
        r3.Abe(22, this.rejoinMissingDbMapping);
        r3.Abe(21, this.timeSinceLastClientPollMinutes);
        r3.Abe(10, this.videoEnabled);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamJoinableCall {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "acceptAckLatencyMs", this.acceptAckLatencyMs);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callRandomId", this.callRandomId);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callReplayerId", this.callReplayerId);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "hasSpamDialog", this.hasSpamDialog);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isCallFull", this.isCallFull);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isFromCallLink", this.isFromCallLink);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isLinkedGroupCall", this.isLinkedGroupCall);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isPendingCall", this.isPendingCall);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isRejoin", this.isRejoin);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isRering", this.isRering);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "joinableAcceptBeforeLobbyAck", this.joinableAcceptBeforeLobbyAck);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "joinableDuringCall", this.joinableDuringCall);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "joinableEndCallBeforeLobbyAck", this.joinableEndCallBeforeLobbyAck);
        Integer num = this.legacyCallResult;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "legacyCallResult", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyAckLatencyMs", this.lobbyAckLatencyMs);
        Integer num2 = this.lobbyEntryPoint;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyEntryPoint", obj2);
        Integer num3 = this.lobbyExit;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyExit", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyExitNackCode", this.lobbyExitNackCode);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyQueryWhileConnected", this.lobbyQueryWhileConnected);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lobbyVisibleT", this.lobbyVisibleT);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "nseEnabled", this.nseEnabled);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "nseOfflineQueueMs", this.nseOfflineQueueMs);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numConnectedPeers", this.numConnectedPeers);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numInvitedParticipants", this.numInvitedParticipants);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numOutgoingRingingPeers", this.numOutgoingRingingPeers);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "previousJoinNotEnded", this.previousJoinNotEnded);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "receivedByNse", this.receivedByNse);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rejoinMissingDbMapping", this.rejoinMissingDbMapping);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "timeSinceLastClientPollMinutes", this.timeSinceLastClientPollMinutes);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "videoEnabled", this.videoEnabled);
        sb.append("}");
        return sb.toString();
    }
}
