package com.whatsapp.settings;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass02B;
import X.AnonymousClass10S;
import X.AnonymousClass11J;
import X.AnonymousClass12P;
import X.AnonymousClass13Q;
import X.AnonymousClass150;
import X.AnonymousClass18N;
import X.AnonymousClass19M;
import X.AnonymousClass1BN;
import X.AnonymousClass1BO;
import X.AnonymousClass1CF;
import X.AnonymousClass1SK;
import X.AnonymousClass1SM;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2I0;
import X.AnonymousClass2I5;
import X.AnonymousClass2I8;
import X.AnonymousClass37M;
import X.C103464qn;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C16030oK;
import X.C16590pI;
import X.C17070qD;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20660w7;
import X.C20770wI;
import X.C20790wK;
import X.C21820y2;
import X.C21840y4;
import X.C21860y6;
import X.C22170ye;
import X.C22670zS;
import X.C22710zW;
import X.C238013b;
import X.C243715g;
import X.C246316g;
import X.C248817g;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C27691It;
import X.C32341c0;
import X.C35211hS;
import X.C41701tx;
import X.C41711ty;
import X.C44191yP;
import X.C852641w;
import X.C92244Vc;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.settings.SettingsPrivacy;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class SettingsPrivacy extends AnonymousClass1SK implements AnonymousClass1SM {
    public static SettingsPrivacy A0p;
    public static final HashMap A0q = new HashMap();
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public View A09;
    public ProgressBar A0A;
    public ProgressBar A0B;
    public TextView A0C;
    public TextView A0D;
    public TextView A0E;
    public TextView A0F;
    public TextView A0G;
    public TextView A0H;
    public TextView A0I;
    public TextView A0J;
    public TextView A0K;
    public SwitchCompat A0L;
    public SwitchCompat A0M;
    public C20770wI A0N;
    public C238013b A0O;
    public AnonymousClass10S A0P;
    public AnonymousClass11J A0Q;
    public C248817g A0R;
    public C18470sV A0S;
    public C243715g A0T;
    public C20790wK A0U;
    public AnonymousClass150 A0V;
    public AnonymousClass1CF A0W;
    public C92244Vc A0X;
    public C16030oK A0Y;
    public C246316g A0Z;
    public C22170ye A0a;
    public C20660w7 A0b;
    public C21860y6 A0c;
    public C22710zW A0d;
    public C17070qD A0e;
    public AnonymousClass2I0 A0f;
    public AnonymousClass1BN A0g;
    public AnonymousClass2I5 A0h;
    public AnonymousClass2I8 A0i;
    public boolean A0j;
    public final Handler A0k;
    public final C27131Gd A0l;
    public final AnonymousClass13Q A0m;
    public final Runnable A0n;
    public final Map A0o;

    public SettingsPrivacy() {
        this(0);
        this.A0l = new C852641w(this);
        this.A0k = new Handler(Looper.getMainLooper());
        this.A0n = new RunnableBRunnable0Shape11S0100000_I0_11(this, 44);
        this.A0m = new C44191yP(this);
        this.A0o = new HashMap();
    }

    public SettingsPrivacy(int i) {
        this.A0j = false;
        A0R(new C103464qn(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0j) {
            this.A0j = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0b = (C20660w7) r1.AIB.get();
            this.A0S = (C18470sV) r1.AK8.get();
            this.A0a = (C22170ye) r1.AHG.get();
            this.A0e = (C17070qD) r1.AFC.get();
            this.A0O = (C238013b) r1.A1Z.get();
            this.A0P = (AnonymousClass10S) r1.A46.get();
            this.A0Z = (C246316g) r1.AFl.get();
            this.A0c = (C21860y6) r1.AE6.get();
            this.A0W = (AnonymousClass1CF) r1.AFz.get();
            this.A0f = r2.A0G();
            this.A0X = new C92244Vc((C16590pI) r1.AMg.get(), (C14850m9) r1.A04.get(), (AnonymousClass1CF) r1.AFz.get());
            this.A0d = (C22710zW) r1.AF7.get();
            this.A0N = (C20770wI) r1.AG4.get();
            this.A0Y = (C16030oK) r1.AAd.get();
            this.A0R = (C248817g) r1.AFm.get();
            this.A0U = (C20790wK) r1.A61.get();
            this.A0g = (AnonymousClass1BN) r1.A8l.get();
            this.A0h = r2.A0H();
            this.A0i = r2.A0I();
            this.A0Q = (AnonymousClass11J) r1.A3m.get();
            this.A0T = (C243715g) r1.AM0.get();
            this.A0V = (AnonymousClass150) r1.A63.get();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A2e(java.lang.String r4) {
        /*
            r3 = this;
            X.0m6 r2 = r3.A09
            int r0 = r4.hashCode()
            switch(r0) {
                case -1012222381: goto L_0x001d;
                case -892481550: goto L_0x0028;
                case -309425751: goto L_0x0034;
                case 3314326: goto L_0x003f;
                case 506363330: goto L_0x004a;
                case 1974548689: goto L_0x0055;
                default: goto L_0x0009;
            }
        L_0x0009:
            java.lang.String r1 = "Unrecognized category: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r4)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x001d:
            java.lang.String r0 = "online"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "privacy_online"
            goto L_0x005f
        L_0x0028:
            java.lang.String r0 = "status"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "privacy_status"
            goto L_0x005f
        L_0x0034:
            java.lang.String r0 = "profile"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "privacy_profile_photo"
            goto L_0x005f
        L_0x003f:
            java.lang.String r0 = "last"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "privacy_last_seen"
            goto L_0x005f
        L_0x004a:
            java.lang.String r0 = "groupadd"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "privacy_groupadd"
            goto L_0x005f
        L_0x0055:
            java.lang.String r0 = "readreceipts"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            java.lang.String r1 = "read_receipts_enabled"
        L_0x005f:
            android.content.SharedPreferences r0 = r2.A00
            r2 = 0
            int r1 = r0.getInt(r1, r2)
            java.util.HashMap r0 = com.whatsapp.settings.SettingsPrivacy.A0q
            java.lang.Object r0 = r0.get(r4)
            X.1ty r0 = (X.C41711ty) r0
            if (r0 == 0) goto L_0x0076
            java.lang.String r0 = r0.A00
            int r1 = X.C41701tx.A00(r0)
        L_0x0076:
            int r0 = java.lang.Math.max(r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsPrivacy.A2e(java.lang.String):int");
    }

    public final TextView A2f(String str) {
        switch (str.hashCode()) {
            case -892481550:
                if (str.equals("status")) {
                    return this.A0D;
                }
                return null;
            case -309425751:
                if (str.equals("profile")) {
                    return this.A0J;
                }
                return null;
            case 3314326:
                if (str.equals("last")) {
                    return this.A0H;
                }
                return null;
            case 506363330:
                if (str.equals("groupadd")) {
                    return this.A0G;
                }
                return null;
            default:
                return null;
        }
    }

    public final String A2g(long j) {
        AnonymousClass018 r5;
        int i;
        long j2;
        Object[] objArr;
        int i2;
        if (j != 0) {
            if (j == 60000) {
                r5 = ((ActivityC13830kP) this).A01;
                i = R.plurals.app_auth_enabled_values;
                j2 = 1;
                objArr = new Object[1];
                i2 = 1;
            } else if (j == 1800000) {
                r5 = ((ActivityC13830kP) this).A01;
                i = R.plurals.app_auth_enabled_values;
                j2 = 30;
                objArr = new Object[1];
                i2 = 30;
            }
            objArr[0] = i2;
            return r5.A0I(objArr, i, j2);
        }
        return getString(R.string.app_auth_enabled_immediately);
    }

    public final void A2h() {
        String str;
        int i;
        boolean z;
        int size;
        AnonymousClass18N ABo;
        if (this.A0O.A0H.A00.getLong("block_list_receive_time", 0) != 0) {
            C238013b r1 = this.A0O;
            synchronized (r1) {
                z = r1.A01;
            }
            if (z) {
                C238013b r12 = this.A0O;
                synchronized (r12) {
                    size = r12.A0S.size();
                }
                if (this.A0d.A07() && this.A0c.A0C() && (ABo = this.A0e.A02().ABo()) != null && ABo.AJQ()) {
                    size += ABo.size();
                }
                if (size > 0) {
                    str = String.valueOf(size);
                    this.A0F.setText(str);
                }
                i = R.string.none;
                str = getString(i);
                this.A0F.setText(str);
            }
        }
        i = R.string.block_list_header;
        str = getString(i);
        this.A0F.setText(str);
    }

    public final void A2i() {
        ArrayList arrayList;
        String string;
        C16030oK r9 = this.A0Y;
        synchronized (r9.A0X) {
            Map A0B = r9.A0B();
            arrayList = new ArrayList(A0B.size());
            long A00 = r9.A0K.A00();
            for (C35211hS r5 : A0B.values()) {
                if (C16030oK.A01(r5.A01, A00)) {
                    C15550nR r3 = r9.A0G;
                    AbstractC14640lm r0 = r5.A02.A00;
                    AnonymousClass009.A05(r0);
                    arrayList.add(r3.A0A(r0));
                }
            }
        }
        if (arrayList.size() > 0) {
            string = ((ActivityC13830kP) this).A01.A0I(new Object[]{Integer.valueOf(arrayList.size())}, R.plurals.live_location_currently_sharing, (long) arrayList.size());
        } else {
            string = getString(R.string.live_location_currently_sharing_none);
        }
        TextView textView = this.A0I;
        if (textView != null) {
            textView.setText(string);
        }
    }

    public final void A2j() {
        A2k();
        A2l("groupadd");
        A2l("last");
        A2l("status");
        A2l("profile");
        A2p(((ActivityC13810kN) this).A09.A00.getBoolean("read_receipts_enabled", true));
        if (((ActivityC13810kN) this).A0C.A07(1972)) {
            A2q(((ActivityC13810kN) this).A09.A00.getBoolean("privacy_calladd", false), true);
        }
    }

    public final void A2k() {
        int i;
        String string;
        int size;
        AnonymousClass018 r5;
        int i2;
        int A00 = this.A0S.A03.A00("status_distribution", 0);
        if (A00 != 0) {
            if (A00 == 1) {
                size = this.A0S.A06().size();
                if (size == 0) {
                    i = R.string.no_contacts_selected;
                    string = getString(i);
                    this.A0K.setText(string);
                }
                r5 = ((ActivityC13830kP) this).A01;
                i2 = R.plurals.status_contacts_selected;
            } else if (A00 == 2) {
                size = this.A0S.A07().size();
                if (size != 0) {
                    r5 = ((ActivityC13830kP) this).A01;
                    i2 = R.plurals.status_contacts_excluded;
                }
            } else {
                throw new IllegalStateException("unknown status distribution mode");
            }
            string = r5.A0I(new Object[]{Integer.valueOf(size)}, i2, (long) size);
            this.A0K.setText(string);
        }
        i = R.string.privacy_contacts;
        string = getString(i);
        this.A0K.setText(string);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void A2l(String str) {
        AnonymousClass1BO r1;
        if (A2f(str) != null) {
            switch (str.hashCode()) {
                case -892481550:
                    if (str.equals("status")) {
                        r1 = this.A0f;
                        break;
                    }
                    r1 = null;
                    break;
                case -309425751:
                    if (str.equals("profile")) {
                        r1 = this.A0i;
                        break;
                    }
                    r1 = null;
                    break;
                case 3314326:
                    if (str.equals("last")) {
                        r1 = this.A0h;
                        break;
                    }
                    r1 = null;
                    break;
                case 506363330:
                    if (str.equals("groupadd")) {
                        r1 = this.A0g;
                        break;
                    }
                    r1 = null;
                    break;
                default:
                    r1 = null;
                    break;
            }
            int A2e = A2e(str);
            if (A2e != 3 || r1 == null) {
                int[] iArr = C41701tx.A00;
                if (A2e >= iArr.length) {
                    StringBuilder sb = new StringBuilder("Received privacy value ");
                    sb.append(A2e);
                    sb.append(" with no available single-setting text");
                    Log.w(sb.toString());
                    A2e = 0;
                }
                A2n(str, getString(iArr[A2e]));
                return;
            }
            AnonymousClass37M r2 = new AnonymousClass37M(this, r1, this, str);
            C27691It A00 = r1.A00();
            A00.A05(this, new AnonymousClass02B(A00, r2) { // from class: X.4uB
                public final /* synthetic */ C27691It A01;
                public final /* synthetic */ AbstractC16350or A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    SettingsPrivacy settingsPrivacy = SettingsPrivacy.this;
                    C27691It r0 = this.A01;
                    AbstractC16350or r22 = this.A02;
                    r0.A04(settingsPrivacy);
                    ((ActivityC13830kP) settingsPrivacy).A05.Aaz(r22, new Void[0]);
                }
            });
        }
    }

    public final void A2m(String str, int i) {
        if (!((ActivityC13810kN) this).A07.A0B()) {
            ((ActivityC13810kN) this).A05.A07(R.string.coldsync_no_network, 0);
            return;
        }
        String A02 = C41701tx.A02(str);
        A2o(A02, C41701tx.A01(Math.max(0, i), A02));
        A2l(A02);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2n(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            android.widget.TextView r4 = r5.A2f(r6)
            if (r4 != 0) goto L_0x001d
            java.lang.String r0 = "Tried to put text for privacy category "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = " with no subtitle text view"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            return
        L_0x001d:
            java.util.Map r0 = r5.A0o
            java.lang.Object r0 = r0.get(r6)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0054
            int r2 = r5.A2e(r0)
            if (r2 < 0) goto L_0x0052
            int[] r1 = X.C41701tx.A00
            int r0 = r1.length
            if (r2 >= r0) goto L_0x0052
            r0 = r1[r2]
            java.lang.String r3 = r5.getString(r0)
        L_0x0038:
            boolean r0 = r7.equals(r3)
            if (r0 != 0) goto L_0x0054
            r2 = 2131891737(0x7f121619, float:1.9418202E38)
            r0 = 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r0 = 0
            r1[r0] = r7
            r0 = 1
            r1[r0] = r3
            java.lang.String r0 = r5.getString(r2, r1)
            r4.setText(r0)
            return
        L_0x0052:
            r3 = r7
            goto L_0x0038
        L_0x0054:
            r4.setText(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsPrivacy.A2n(java.lang.String, java.lang.String):void");
    }

    public final void A2o(String str, String str2) {
        A0q.put(str, new C41711ty(str2));
        ((ActivityC13790kL) this).A0A.A01(true);
        this.A0N.A01(str, str2);
        Handler handler = this.A0k;
        Runnable runnable = this.A0n;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 20000);
    }

    public final void A2p(boolean z) {
        Object obj = A0q.get(C41701tx.A02("read_receipts_enabled"));
        View view = this.A05;
        int i = 0;
        boolean z2 = false;
        if (obj == null) {
            z2 = true;
        }
        view.setEnabled(z2);
        ProgressBar progressBar = this.A0A;
        int i2 = 8;
        if (obj != null) {
            i2 = 0;
        }
        progressBar.setVisibility(i2);
        SwitchCompat switchCompat = this.A0L;
        if (obj != null) {
            i = 8;
        }
        switchCompat.setVisibility(i);
        this.A0L.setChecked(z);
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("read_receipts_enabled", z).apply();
    }

    public final void A2q(boolean z, boolean z2) {
        Object obj = A0q.get(C41701tx.A02("privacy_calladd"));
        View view = this.A09;
        int i = 0;
        boolean z3 = false;
        if (obj == null) {
            z3 = true;
        }
        view.setEnabled(z3);
        ProgressBar progressBar = this.A0B;
        int i2 = 8;
        if (obj != null) {
            i2 = 0;
        }
        progressBar.setVisibility(i2);
        SwitchCompat switchCompat = this.A0M;
        if (obj != null) {
            i = 8;
        }
        switchCompat.setVisibility(i);
        if (z2) {
            this.A0M.setChecked(z);
        }
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        String str;
        if (i == 1) {
            str = "privacy_last_seen";
        } else if (i == 2) {
            str = "privacy_profile_photo";
        } else if (i == 3) {
            str = "privacy_status";
        } else {
            return;
        }
        A2m(str, i2);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        int i3;
        String str;
        String str2;
        super.onActivityResult(i, i2, intent);
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    finish();
                } else if (i != 3) {
                    if (i != 4) {
                        if (i == 5 && i2 == -1 && intent != null) {
                            i3 = intent.getIntExtra("about", 0);
                            if (i3 == 3) {
                                str2 = "status";
                                A2l(str2);
                                return;
                            }
                            str = "privacy_status";
                            A2m(str, i3);
                            return;
                        }
                        return;
                    } else if (i2 == -1 && intent != null) {
                        i3 = intent.getIntExtra("profile_photo", 0);
                        if (i3 == 3) {
                            str2 = "profile";
                            A2l(str2);
                            return;
                        }
                        str = "privacy_profile_photo";
                        A2m(str, i3);
                        return;
                    } else {
                        return;
                    }
                } else if (i2 == -1 && intent != null) {
                    if (intent.hasExtra("online")) {
                        A2m("privacy_online", intent.getIntExtra("online", 0));
                    }
                    i3 = intent.getIntExtra("last_seen", 0);
                    if (i3 == 3) {
                        str2 = "last";
                        A2l(str2);
                        return;
                    }
                    str = "privacy_last_seen";
                    A2m(str, i3);
                    return;
                } else {
                    return;
                }
            }
            if (i2 == -1 && intent != null) {
                str2 = "groupadd";
                i3 = intent.getIntExtra(str2, 0);
                if (i3 != 3) {
                    str = "privacy_groupadd";
                    A2m(str, i3);
                    return;
                }
                A2l(str2);
                return;
            }
            return;
        }
        A2k();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setTitle(R.string.settings_privacy);
        setContentView(R.layout.preferences_privacy);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A0p = this;
        View A05 = AnonymousClass00T.A05(this, R.id.last_seen_privacy_preference);
        this.A02 = A05;
        TextView textView = (TextView) AnonymousClass028.A0D(A05, R.id.settings_privacy_row_text);
        C92244Vc r1 = this.A0X;
        if (!r1.A00() || (str = r1.A00.A02(R.string.settings_trusted_contacts_title)) == null) {
            str = getString(R.string.settings_privacy_last_seen);
        }
        textView.setText(str);
        this.A0H = (TextView) AnonymousClass028.A0D(this.A02, R.id.settings_privacy_row_subtext);
        if (this.A0X.A00()) {
            AnonymousClass00T.A05(this, R.id.last_seen_reciprocity_explanation).setVisibility(8);
        }
        if (this.A0X.A00()) {
            this.A0o.put("last", "online");
        }
        View A052 = AnonymousClass00T.A05(this, R.id.profile_photo_privacy_preference);
        this.A03 = A052;
        ((TextView) AnonymousClass028.A0D(A052, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_profile_photo));
        this.A0J = (TextView) AnonymousClass028.A0D(this.A03, R.id.settings_privacy_row_subtext);
        View A053 = AnonymousClass00T.A05(this, R.id.about_privacy_preference);
        this.A00 = A053;
        ((TextView) AnonymousClass028.A0D(A053, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_info));
        this.A0D = (TextView) AnonymousClass028.A0D(this.A00, R.id.settings_privacy_row_subtext);
        View A054 = AnonymousClass00T.A05(this, R.id.status_privacy_preference);
        this.A07 = A054;
        ((TextView) AnonymousClass028.A0D(A054, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_status));
        this.A0K = (TextView) AnonymousClass028.A0D(this.A07, R.id.settings_privacy_row_subtext);
        View A055 = AnonymousClass00T.A05(this, R.id.live_location_privacy_preference);
        ((TextView) AnonymousClass028.A0D(A055, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_live_location));
        this.A0I = (TextView) AnonymousClass028.A0D(A055, R.id.settings_privacy_row_subtext);
        ((ActivityC13790kL) this).A01.A08();
        A055.setVisibility(0);
        View A056 = AnonymousClass00T.A05(this, R.id.dm_privacy_preference_container);
        this.A0C = (TextView) AnonymousClass028.A0D(A056, R.id.dm_privacy_preference_value);
        ((ActivityC13790kL) this).A01.A08();
        A056.setVisibility(0);
        View A057 = AnonymousClass00T.A05(this, R.id.group_add_permission_privacy_preference);
        this.A01 = A057;
        ((TextView) AnonymousClass028.A0D(A057, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_group_add_permissions));
        this.A0G = (TextView) AnonymousClass028.A0D(this.A01, R.id.settings_privacy_row_subtext);
        View A058 = AnonymousClass00T.A05(this, R.id.block_list_privacy_preference);
        ((TextView) AnonymousClass028.A0D(A058, R.id.settings_privacy_row_text)).setText(getString(R.string.block_list_header));
        this.A0F = (TextView) AnonymousClass028.A0D(A058, R.id.settings_privacy_row_subtext);
        this.A05 = AnonymousClass00T.A05(this, R.id.read_receipts_privacy_preference);
        this.A0L = (SwitchCompat) AnonymousClass00T.A05(this, R.id.read_receipts_privacy_switch);
        this.A0A = (ProgressBar) AnonymousClass00T.A05(this, R.id.read_receipts_progress_bar);
        this.A04 = AnonymousClass00T.A05(this, R.id.read_receipts_divider);
        View A059 = AnonymousClass00T.A05(this, R.id.security_privacy_preference);
        this.A06 = A059;
        int i = R.string.settings_privacy_security_section_title;
        ((TextView) AnonymousClass028.A0D(A059, R.id.settings_privacy_row_text)).setText(getString(R.string.settings_privacy_security_section_title));
        TextView textView2 = (TextView) AnonymousClass028.A0D(this.A06, R.id.settings_privacy_row_subtext);
        if (((ActivityC13790kL) this).A03.A04.A07(266)) {
            i = R.string.settings_privacy_security_section_biometric_title;
        }
        textView2.setText(i);
        this.A0E = (TextView) AnonymousClass028.A0D(this.A06, R.id.settings_privacy_row_subtext);
        this.A09 = AnonymousClass00T.A05(this, R.id.suc_preference);
        this.A0M = (SwitchCompat) AnonymousClass00T.A05(this, R.id.suc_switch);
        this.A0B = (ProgressBar) AnonymousClass00T.A05(this, R.id.suc_progress_bar);
        this.A08 = AnonymousClass00T.A05(this, R.id.suc_divider);
        if (((ActivityC13810kN) this).A0C.A07(1972)) {
            this.A09.setVisibility(0);
            this.A08.setVisibility(0);
            this.A04.setVisibility(8);
            this.A09.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 26));
        }
        A2j();
        this.A02.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 28));
        this.A03.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 21));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 22));
        this.A07.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 24));
        A055.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 20));
        this.A01.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 25));
        ((ActivityC13790kL) this).A01.A08();
        A056.setVisibility(0);
        A056.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 23));
        this.A0C.setText(C32341c0.A03(this, this.A0V.A04().intValue(), false, true));
        this.A0U.A04.A00.A05(this, new AnonymousClass02B() { // from class: X.3Qv
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                SettingsPrivacy settingsPrivacy = SettingsPrivacy.this;
                settingsPrivacy.A0C.setText(C32341c0.A03(settingsPrivacy, C12960it.A05(obj), false, true));
            }
        });
        A058.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 27));
        this.A05.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 29));
        this.A0N.A00(null);
        this.A0U.A00();
        this.A0P.A03(this.A0l);
        this.A0Y.A0W(this.A0m);
        String stringExtra = getIntent().getStringExtra("target_setting");
        if (stringExtra != null && stringExtra.equals("privacy_groupadd")) {
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.group.GroupAddPrivacyActivity");
            startActivityForResult(intent, 2);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C16030oK r0 = this.A0Y;
        r0.A0b.remove(this.A0m);
        this.A0P.A04(this.A0l);
        this.A0k.removeCallbacks(this.A0n);
        A0p = null;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        String string;
        super.onResume();
        A2h();
        A2i();
        boolean A04 = ((ActivityC13790kL) this).A03.A04();
        View view = this.A06;
        if (A04) {
            view.setVisibility(0);
            if (((ActivityC13810kN) this).A09.A00.getBoolean("privacy_fingerprint_enabled", false)) {
                string = A2g(((ActivityC13810kN) this).A09.A00.getLong("privacy_fingerprint_timeout", 60000));
            } else {
                string = getString(R.string.app_auth_disabled);
            }
            this.A0E.setText(string);
            this.A06.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 30));
            return;
        }
        view.setVisibility(8);
    }
}
