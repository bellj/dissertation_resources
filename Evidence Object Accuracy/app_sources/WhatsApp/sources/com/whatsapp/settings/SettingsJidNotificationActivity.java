package com.whatsapp.settings;

import X.ActivityC13830kP;
import X.ActivityC44201yU;
import X.ActivityC58402ou;
import X.AnonymousClass01J;
import X.C004902f;
import X.C12960it;
import X.C12970iu;
import X.C15510nN;
import X.C15810nw;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.WaPreferenceFragment;

/* loaded from: classes2.dex */
public class SettingsJidNotificationActivity extends ActivityC58402ou {
    public boolean A00;

    public SettingsJidNotificationActivity() {
        this(0);
    }

    public SettingsJidNotificationActivity(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 112);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass01J A1M = ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this);
            ((ActivityC44201yU) this).A05 = C12970iu.A0R(A1M);
            ((ActivityC58402ou) this).A02 = (C15810nw) A1M.A73.get();
            ((ActivityC58402ou) this).A01 = (C22670zS) A1M.A0V.get();
            ((ActivityC58402ou) this).A03 = C12960it.A0Q(A1M);
            ((ActivityC58402ou) this).A04 = (C21840y4) A1M.ACr.get();
            ((ActivityC58402ou) this).A06 = (C21820y2) A1M.AHx.get();
            ((ActivityC58402ou) this).A05 = (C15510nN) A1M.AHZ.get();
        }
    }

    @Override // X.ActivityC58402ou, X.ActivityC44201yU, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.preference_activity);
        if (bundle == null) {
            ((ActivityC44201yU) this).A06 = new SettingsJidNotificationFragment();
            C004902f A0P = C12970iu.A0P(this);
            A0P.A0B(((ActivityC44201yU) this).A06, "preferenceFragment", R.id.preference_fragment);
            A0P.A01();
            return;
        }
        setTitle(bundle.getCharSequence("settingsJidNotificationFragment"));
        ((ActivityC44201yU) this).A06 = (WaPreferenceFragment) A0V().A08(bundle, "preferenceFragment");
    }

    @Override // X.ActivityC44201yU, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("settingsJidNotificationFragment", getTitle());
    }
}
