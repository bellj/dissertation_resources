package com.whatsapp.settings;

import X.AbstractC005102i;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass11G;
import X.AnonymousClass12R;
import X.AnonymousClass12U;
import X.AnonymousClass19Y;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15890o4;
import X.C16700pc;
import X.C17170qN;
import X.C20800wL;
import X.C21870y7;
import X.C21880y8;
import X.C252018m;
import X.C254819o;
import X.C33411dz;
import X.C41691tw;
import X.C83643xZ;
import X.C91704St;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public class SettingsHelpV2 extends ActivityC13790kL {
    public AnonymousClass19Y A00;
    public C17170qN A01;
    public C15890o4 A02;
    public AnonymousClass11G A03;
    public C20800wL A04;
    public AnonymousClass12U A05;
    public C254819o A06;
    public C252018m A07;
    public C21880y8 A08;
    public C21870y7 A09;
    public AnonymousClass12R A0A;
    public boolean A0B;

    public SettingsHelpV2() {
        this(0);
    }

    public SettingsHelpV2(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 111);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A05 = ActivityC13830kP.A1N(A1M);
            this.A00 = (AnonymousClass19Y) A1M.AI6.get();
            this.A07 = C12980iv.A0g(A1M);
            this.A03 = (AnonymousClass11G) A1M.AKq.get();
            this.A08 = (C21880y8) A1M.AD5.get();
            this.A02 = C12970iu.A0Y(A1M);
            this.A06 = (C254819o) A1M.A4D.get();
            this.A09 = (C21870y7) A1M.AMC.get();
            this.A04 = (C20800wL) A1M.AHW.get();
            this.A0A = (AnonymousClass12R) A1M.AMD.get();
            this.A01 = C12990iw.A0a(A1M);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_help);
        setContentView(R.layout.preferences_help);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            int A00 = C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon);
            if (!((ActivityC13810kN) this).A0C.A07(1347)) {
                View findViewById = findViewById(R.id.faq_preference);
                findViewById.setVisibility(0);
                ImageView A0L = C12970iu.A0L(findViewById, R.id.settings_row_icon);
                A0L.setImageDrawable(new C83643xZ(AnonymousClass00T.A04(this, R.drawable.ic_settings_help), ((ActivityC13830kP) this).A01));
                AnonymousClass2GE.A07(A0L, A00);
                C12960it.A12(findViewById, this, 44);
                View findViewById2 = findViewById(R.id.contact_us_preference);
                findViewById2.setVisibility(0);
                AnonymousClass2GE.A07(C12970iu.A0L(findViewById2, R.id.settings_row_icon), A00);
                C12960it.A12(findViewById2, this, 46);
            } else {
                View findViewById3 = findViewById(R.id.get_help_preference);
                findViewById3.setVisibility(0);
                ImageView A0L2 = C12970iu.A0L(findViewById3, R.id.settings_row_icon);
                A0L2.setImageDrawable(new C83643xZ(AnonymousClass00T.A04(this, R.drawable.ic_settings_help), ((ActivityC13830kP) this).A01));
                AnonymousClass2GE.A07(A0L2, A00);
                C12960it.A12(findViewById3, this, 43);
            }
            View findViewById4 = findViewById(R.id.terms_and_privacy_preference);
            TextView A0J = C12960it.A0J(findViewById4, R.id.settings_row_text);
            ImageView A0L3 = C12970iu.A0L(findViewById4, R.id.settings_row_icon);
            AnonymousClass2GF.A01(this, A0L3, ((ActivityC13830kP) this).A01, R.drawable.ic_settings_terms_policy);
            AnonymousClass2GE.A07(A0L3, A00);
            A0J.setText(getText(R.string.settings_terms_and_privacy_policy));
            C12960it.A12(findViewById4, this, 42);
            View findViewById5 = findViewById(R.id.about_preference);
            AnonymousClass2GE.A07(C12970iu.A0L(findViewById5, R.id.settings_row_icon), A00);
            C12960it.A12(findViewById5, this, 45);
            return;
        }
        throw C12970iu.A0f("Required value was null.");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        View findViewById;
        C33411dz r0;
        String str;
        int i;
        boolean z;
        boolean z2;
        super.onResume();
        C21880y8 r1 = this.A08;
        if (r1 != null) {
            ArrayList A0l = C12960it.A0l();
            if (r1.A0C) {
                ConcurrentHashMap concurrentHashMap = r1.A02;
                for (Number number : concurrentHashMap.keySet()) {
                    C33411dz r4 = (C33411dz) concurrentHashMap.get(number);
                    if (r4 != null) {
                        int intValue = number.intValue();
                        if (intValue == 20220225) {
                            str = "https://faq.whatsapp.com/general/yearly-reminder-for-users-in-india";
                        } else {
                            str = intValue == 20220328 ? "https://faq.whatsapp.com/general/payments/learn-more-about-sharing-the-legal-name-on-your-bank-account" : "";
                        }
                        int i2 = r4.A00;
                        if (i2 >= 4) {
                            i = r4.A01;
                            z = false;
                            z2 = true;
                        } else {
                            if (i2 > -1) {
                                i = r4.A01;
                                z = true;
                            } else if (i2 == -1) {
                                i = r4.A01;
                                z = false;
                            }
                            z2 = z;
                        }
                        A0l.add(new C91704St(z, z2, intValue, i, str));
                    }
                }
            }
            Iterator it = A0l.iterator();
            while (it.hasNext()) {
                C91704St r42 = (C91704St) it.next();
                if (r42.A04) {
                    SettingsRowIconText settingsRowIconText = (SettingsRowIconText) findViewById(r42.A01);
                    if (settingsRowIconText != null && (findViewById = settingsRowIconText.findViewById(R.id.settings_row_icon)) != null) {
                        findViewById.setVisibility(4);
                        if (r42.A03) {
                            settingsRowIconText.setBadgeIcon(AnonymousClass00T.A04(this, R.drawable.ic_settings_row_badge));
                            C21880y8 r5 = this.A08;
                            if (r5 != null) {
                                int i3 = r42.A00;
                                if (!(!r5.A0C || (r0 = (C33411dz) r5.A02.get(Integer.valueOf(i3))) == null || r0.A00 == 9)) {
                                    r5.A07.A00(C12980iv.A0j(), i3);
                                    r5.A07(new RunnableBRunnable0Shape0S0101000_I0(r5, i3, 27));
                                }
                            } else {
                                throw C16700pc.A06("noticeBadgeManager");
                            }
                        } else {
                            settingsRowIconText.setBadgeIcon(null);
                        }
                        settingsRowIconText.setVisibility(0);
                        C21880y8 r02 = this.A08;
                        if (r02 != null) {
                            r02.A07.A00(C12990iw.A0j(), r42.A00);
                            C12960it.A14(settingsRowIconText, this, r42, 38);
                        } else {
                            throw C16700pc.A06("noticeBadgeManager");
                        }
                    } else {
                        return;
                    }
                }
            }
            return;
        }
        throw C16700pc.A06("noticeBadgeManager");
    }
}
