package com.whatsapp.settings;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C14850m9;
import X.C15810nw;
import android.os.Build;
import android.os.Environment;
import java.io.File;

/* loaded from: classes2.dex */
public class SettingsDataUsageViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = new AnonymousClass016(Boolean.FALSE);
    public final C15810nw A01;
    public final C14850m9 A02;
    public final AbstractC14440lR A03;

    public SettingsDataUsageViewModel(C15810nw r3, C14850m9 r4, AbstractC14440lR r5) {
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = r3;
    }

    public static /* synthetic */ void A00(SettingsDataUsageViewModel settingsDataUsageViewModel) {
        AnonymousClass016 r1;
        Boolean bool;
        if (Build.VERSION.SDK_INT < 30 || Environment.isExternalStorageLegacy() || !settingsDataUsageViewModel.A02.A07(1235)) {
            r1 = settingsDataUsageViewModel.A00;
            bool = Boolean.FALSE;
        } else {
            File file = new File(Environment.getExternalStorageDirectory(), "WhatsApp");
            r1 = settingsDataUsageViewModel.A00;
            bool = Boolean.valueOf(file.exists());
        }
        r1.A0A(bool);
    }
}
