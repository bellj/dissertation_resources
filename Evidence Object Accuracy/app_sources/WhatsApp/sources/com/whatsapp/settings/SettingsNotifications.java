package com.whatsapp.settings;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass1SK;
import X.AnonymousClass1SM;
import X.AnonymousClass2FL;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C15860o1;
import X.C22630zO;
import X.C33181da;
import X.C33261di;
import X.C36021jC;
import X.C41691tw;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class SettingsNotifications extends AnonymousClass1SK implements AnonymousClass1SM {
    public static final int[] A0q = {R.string.color_none, R.string.color_white, R.string.color_red, R.string.color_yellow, R.string.color_green, R.string.color_cyan, R.string.color_blue, R.string.color_purple};
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public View A07;
    public View A08;
    public View A09;
    public View A0A;
    public View A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public View A0F;
    public View A0G;
    public View A0H;
    public View A0I;
    public View A0J;
    public ViewGroup A0K;
    public ViewGroup A0L;
    public TextView A0M;
    public TextView A0N;
    public TextView A0O;
    public TextView A0P;
    public TextView A0Q;
    public TextView A0R;
    public TextView A0S;
    public TextView A0T;
    public TextView A0U;
    public TextView A0V;
    public TextView A0W;
    public SwitchCompat A0X;
    public SwitchCompat A0Y;
    public SwitchCompat A0Z;
    public SwitchCompat A0a;
    public SwitchCompat A0b;
    public C15860o1 A0c;
    public String A0d;
    public String A0e;
    public String A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public String[] A0l;
    public String[] A0m;
    public String[] A0n;
    public String[] A0o;
    public String[] A0p;

    public SettingsNotifications() {
        this(0);
    }

    public SettingsNotifications(int i) {
        this.A0k = false;
        ActivityC13830kP.A1P(this, 113);
    }

    public static int A02(String str, String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            if (str.equals(strArr[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0k) {
            this.A0k = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0c = (C15860o1) A1M.A3H.get();
        }
    }

    public final void A2e() {
        View view;
        int i;
        C33181da A05 = this.A0c.A05();
        C33181da A04 = this.A0c.A04();
        this.A0f = A05.A07();
        this.A06 = A02(A05.A08(), this.A0p);
        this.A05 = A02(A05.A06(), this.A0n);
        this.A04 = A02(A05.A05(), this.A0l);
        this.A0i = A05.A0B();
        this.A0e = A04.A07();
        this.A03 = A02(A04.A08(), this.A0p);
        this.A02 = A02(A04.A06(), this.A0n);
        this.A01 = A02(A04.A05(), this.A0l);
        this.A0g = A04.A0B();
        this.A0d = A05.A03();
        this.A00 = A02(A05.A04(), this.A0p);
        this.A0j = A05.A02().A0E;
        this.A0h = A04.A02().A0E;
        C12960it.A11(this.A09, this, 1);
        this.A0X.setChecked(((ActivityC13810kN) this).A09.A00.getBoolean("conversation_sound", true));
        this.A0U.setText(C22630zO.A06(this, this.A0f));
        C12960it.A11(this.A0H, this, 8);
        int i2 = this.A06;
        if (i2 != -1) {
            this.A0W.setText(this.A0o[i2]);
        }
        C12960it.A11(this.A0J, this, 2);
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 29) {
            int A00 = C41691tw.A00(this, R.attr.settingsTextDisabled, R.color.settings_disabled_text);
            this.A0M.setTextColor(A00);
            this.A0V.setTextColor(A00);
            this.A0V.setText(R.string.popup_notification_not_available);
            view = this.A0I;
            i = 12;
        } else {
            int i4 = this.A05;
            if (i4 != -1) {
                this.A0V.setText(this.A0m[i4]);
            }
            view = this.A0I;
            i = 0;
        }
        C12960it.A11(view, this, i);
        int i5 = this.A04;
        if (i5 != -1) {
            this.A0T.setText(A0q[i5]);
        }
        C12960it.A11(this.A0G, this, 3);
        View view2 = this.A0F;
        if (i3 >= 21) {
            view2.setVisibility(0);
            this.A0a.setChecked(C12990iw.A1Z(Boolean.FALSE, this.A0i));
            C12960it.A11(this.A0F, this, 6);
        } else {
            view2.setVisibility(8);
        }
        this.A0L.setVisibility(0);
        SwitchCompat switchCompat = this.A0b;
        Boolean bool = Boolean.FALSE;
        switchCompat.setChecked(C12990iw.A1Z(bool, this.A0j));
        C12960it.A11(this.A0L, this, 11);
        this.A0K.setVisibility(0);
        this.A0Z.setChecked(C12990iw.A1Z(bool, this.A0h));
        C12960it.A12(this.A0K, this, 47);
        this.A0Q.setText(C22630zO.A06(this, this.A0e));
        C12960it.A12(this.A0C, this, 48);
        int i6 = this.A03;
        if (i6 != -1) {
            this.A0S.setText(this.A0o[i6]);
        }
        C12960it.A11(this.A0E, this, 5);
        if (i3 >= 29) {
            this.A0D.setVisibility(8);
        } else {
            int i7 = this.A02;
            if (i7 != -1) {
                this.A0R.setText(this.A0m[i7]);
            }
            C12960it.A12(this.A0D, this, 49);
        }
        int i8 = this.A01;
        if (i8 != -1) {
            this.A0P.setText(A0q[i8]);
        }
        C12960it.A11(this.A0B, this, 9);
        View view3 = this.A0A;
        if (i3 >= 21) {
            view3.setVisibility(0);
            this.A0Y.setChecked(C12990iw.A1Z(bool, this.A0g));
            C12960it.A11(this.A0A, this, 7);
        } else {
            view3.setVisibility(8);
        }
        this.A0N.setText(C22630zO.A06(this, this.A0d));
        C12960it.A11(this.A07, this, 4);
        int i9 = this.A00;
        if (i9 != -1) {
            this.A0O.setText(this.A0o[i9]);
        }
        C12960it.A11(this.A08, this, 10);
    }

    public final void A2f(String str, String str2, int i, int i2) {
        Uri parse;
        Intent A0E = C12990iw.A0E("android.intent.action.RINGTONE_PICKER");
        A0E.putExtra("android.intent.extra.ringtone.TITLE", str);
        A0E.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        A0E.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", true);
        A0E.putExtra("android.intent.extra.ringtone.DEFAULT_URI", Settings.System.DEFAULT_NOTIFICATION_URI);
        if (!(str2 == null || str2.equals("Silent") || (parse = Uri.parse(str2)) == null)) {
            A0E.putExtra("android.intent.extra.ringtone.EXISTING_URI", parse);
        }
        if (Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {
            A0E.putExtra("android.intent.extra.ringtone.TYPE", 2);
        } else {
            A0E.putExtra("android.intent.extra.ringtone.TYPE", i2);
        }
        startActivityForResult(Intent.createChooser(A0E, null), i);
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        TextView textView;
        String[] strArr;
        TextView textView2;
        switch (i) {
            case 9:
                this.A06 = i2;
                this.A0c.A0Q("individual_chat_defaults", String.valueOf(this.A0p[i2]));
                textView = this.A0W;
                strArr = this.A0o;
                textView.setText(strArr[i2]);
                return;
            case 10:
                this.A05 = i2;
                this.A0c.A0O("individual_chat_defaults", String.valueOf(this.A0n[i2]));
                textView = this.A0V;
                strArr = this.A0m;
                textView.setText(strArr[i2]);
                return;
            case 11:
                String str = this.A0l[i2];
                String str2 = Build.MODEL;
                if ((!str2.contains("Desire") && !str2.contains("Wildfire")) || str.equals("00FF00")) {
                    this.A04 = i2;
                    this.A0c.A0N("individual_chat_defaults", String.valueOf(this.A0l[i2]));
                    textView2 = this.A0T;
                    textView2.setText(A0q[i2]);
                    return;
                }
                C36021jC.A01(this, 7);
                return;
            case 12:
                this.A03 = i2;
                this.A0c.A0Q("group_chat_defaults", String.valueOf(this.A0p[i2]));
                textView = this.A0S;
                strArr = this.A0o;
                textView.setText(strArr[i2]);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A02 = i2;
                this.A0c.A0O("group_chat_defaults", String.valueOf(this.A0n[i2]));
                textView = this.A0R;
                strArr = this.A0m;
                textView.setText(strArr[i2]);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                String str3 = this.A0l[i2];
                String str4 = Build.MODEL;
                if ((!str4.contains("Desire") && !str4.contains("Wildfire")) || str3.equals("00FF00")) {
                    this.A01 = i2;
                    this.A0c.A0N("group_chat_defaults", String.valueOf(this.A0l[i2]));
                    textView2 = this.A0P;
                    textView2.setText(A0q[i2]);
                    return;
                }
                C36021jC.A01(this, 7);
                return;
            case 15:
                this.A00 = i2;
                C15860o1 r3 = this.A0c;
                String valueOf = String.valueOf(this.A0p[i2]);
                C33181da A08 = r3.A08("individual_chat_defaults");
                if (!TextUtils.equals(valueOf, A08.A07)) {
                    A08.A07 = valueOf;
                    r3.A0M(A08);
                }
                textView = this.A0O;
                strArr = this.A0o;
                textView.setText(strArr[i2]);
                return;
            default:
                return;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String A06;
        String str;
        TextView textView;
        super.onActivityResult(i, i2, intent);
        if ((i == 1 || i == 2 || i == 3) && i2 == -1) {
            Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            if (uri != null) {
                A06 = RingtoneManager.getRingtone(this, uri).getTitle(this);
                str = uri.toString();
            } else {
                A06 = C22630zO.A06(this, null);
                str = "Silent";
            }
            if (i == 1) {
                this.A0f = str;
                this.A0c.A0P("individual_chat_defaults", str);
                textView = this.A0U;
            } else if (i == 2) {
                this.A0e = str;
                this.A0c.A0P("group_chat_defaults", str);
                textView = this.A0Q;
            } else if (i == 3) {
                this.A0d = str;
                C15860o1 r2 = this.A0c;
                C33181da A08 = r2.A08("individual_chat_defaults");
                if (!TextUtils.equals(str, A08.A06)) {
                    A08.A06 = str;
                    r2.A0M(A08);
                }
                textView = this.A0N;
            } else {
                return;
            }
            textView.setText(A06);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_notifications);
        setContentView(R.layout.preferences_notifications);
        C12970iu.A0N(this).A0M(true);
        this.A09 = findViewById(R.id.conversation_sound_setting);
        this.A0X = (SwitchCompat) findViewById(R.id.conversation_sound_switch);
        this.A0H = findViewById(R.id.notification_tone_setting);
        this.A0U = C12970iu.A0M(this, R.id.selected_notification_tone);
        this.A0J = findViewById(R.id.vibrate_setting);
        this.A0W = C12970iu.A0M(this, R.id.selected_vibrate_setting);
        this.A0I = findViewById(R.id.popup_notification_setting);
        this.A0M = C12970iu.A0M(this, R.id.popup_notification_setting_title);
        this.A0V = C12970iu.A0M(this, R.id.selected_popup_notification_setting);
        this.A0G = findViewById(R.id.notification_light_setting);
        this.A0T = C12970iu.A0M(this, R.id.selected_notification_light_setting);
        this.A0F = findViewById(R.id.high_priority_notifications_setting);
        this.A0a = (SwitchCompat) findViewById(R.id.high_priority_notifications_switch);
        this.A0C = findViewById(R.id.group_notification_tone_setting);
        this.A0Q = C12970iu.A0M(this, R.id.selected_group_notification_tone);
        this.A0E = findViewById(R.id.group_vibrate_setting);
        this.A0S = C12970iu.A0M(this, R.id.selected_group_vibrate_setting);
        this.A0D = findViewById(R.id.group_popup_notification_setting);
        this.A0R = C12970iu.A0M(this, R.id.selected_group_popup_notification_setting);
        this.A0B = findViewById(R.id.group_notification_light_setting);
        this.A0P = C12970iu.A0M(this, R.id.selected_group_notification_light_setting);
        this.A0A = findViewById(R.id.group_high_priority_notifications_setting);
        this.A0Y = (SwitchCompat) findViewById(R.id.group_high_priority_notifications_switch);
        this.A07 = findViewById(R.id.call_tone_setting);
        this.A0N = C12970iu.A0M(this, R.id.selected_call_tone);
        this.A08 = findViewById(R.id.call_vibrate_setting);
        this.A0O = C12970iu.A0M(this, R.id.selected_call_vibrate_setting);
        this.A0L = (ViewGroup) findViewById(R.id.reaction_notifications_setting);
        this.A0K = (ViewGroup) findViewById(R.id.group_reaction_notifications_setting);
        this.A0b = (SwitchCompat) findViewById(R.id.reactions_switch);
        this.A0Z = (SwitchCompat) findViewById(R.id.group_reactions_switch);
        Resources resources = getResources();
        this.A0o = resources.getStringArray(R.array.vibrate_lengths);
        this.A0p = resources.getStringArray(R.array.vibrate_values);
        this.A0m = resources.getStringArray(R.array.popup_mode);
        this.A0n = resources.getStringArray(R.array.popup_mode_values);
        this.A0l = resources.getStringArray(R.array.led_color_values);
        A2e();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        if (i == 7) {
            r2 = C12980iv.A0S(this);
            r2.A06(R.string.led_support_green_only);
            r2.setPositiveButton(R.string.ok, null);
        } else if (i != 8) {
            return super.onCreateDialog(i);
        } else {
            r2 = C12980iv.A0S(this);
            r2.A06(R.string.settings_notification_reset_warning);
            C12970iu.A1M(r2, this, 30, R.string.reset_notifications);
            r2.setNegativeButton(R.string.cancel, null);
        }
        return r2.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_reset_notification_settings, 0, R.string.settings_notification_reset).setShowAsAction(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_reset_notification_settings) {
            return super.onOptionsItemSelected(menuItem);
        }
        C36021jC.A01(this, 8);
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        C15860o1 r2 = this.A0c;
        if (C33261di.A00) {
            boolean A0U = r2.A0U("individual_chat_defaults");
            boolean A0U2 = r2.A0U("group_chat_defaults");
            if (A0U || A0U2) {
                Log.i("settings-jid-notifications/onStart settings-store updated, refreshing ui");
                A2e();
            }
        }
    }

    public final void requestFocusOnViewAndHighlight(View view) {
        view.getParent().requestChildFocus(view, view);
        ArgbEvaluator argbEvaluator = new ArgbEvaluator();
        Object[] objArr = new Object[2];
        C12960it.A1O(objArr, getResources().getColor(R.color.white));
        objArr[1] = Integer.valueOf(getResources().getColor(R.color.btn_disabled));
        ValueAnimator ofObject = ValueAnimator.ofObject(argbEvaluator, objArr);
        ofObject.setRepeatMode(2);
        ofObject.setRepeatCount(1);
        ofObject.setDuration(500L);
        C12980iv.A11(ofObject, view, 9);
        ofObject.start();
    }
}
