package com.whatsapp.settings;

import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.ActivityC44201yU;
import X.AnonymousClass009;
import X.AnonymousClass0F3;
import X.C14900mE;
import X.C14950mJ;
import X.C15370n3;
import X.C15450nH;
import X.C15550nR;
import X.C15570nT;
import X.C15680nj;
import X.C16170oZ;
import X.C17050qB;
import X.C255319t;
import X.C255719x;
import X.C36021jC;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import androidx.preference.PreferenceFragmentCompat;
import com.whatsapp.WaPreferenceFragment;

/* loaded from: classes2.dex */
public class SettingsChatHistoryFragment extends Hilt_SettingsChatHistoryFragment {
    public C14900mE A00;
    public C15570nT A01;
    public C15450nH A02;
    public C16170oZ A03;
    public C15550nR A04;
    public C255319t A05;
    public C17050qB A06;
    public C14950mJ A07;
    public C15680nj A08;
    public AbstractC14640lm A09;
    public C255719x A0A;
    public AbstractC14440lR A0B;

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        if (i == 10 && i2 == -1) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(intent.getStringExtra("contact"));
            AnonymousClass009.A06(A01, intent.getStringExtra("contact"));
            this.A09 = A01;
            ActivityC44201yU r6 = ((WaPreferenceFragment) this).A00;
            if (r6 != null) {
                C255319t r5 = this.A05;
                C15370n3 A0A = this.A04.A0A(A01);
                if (r5.A04.A0B(null, A01, 1, 2).size() > 0) {
                    C36021jC.A01(r6, 10);
                } else {
                    r5.A01(r6, r6, A0A, false);
                }
            }
        }
    }

    @Override // androidx.preference.PreferenceFragmentCompat, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        super.A17(bundle, view);
        ColorDrawable colorDrawable = new ColorDrawable(0);
        AnonymousClass0F3 r2 = ((PreferenceFragmentCompat) this).A06;
        r2.A00 = colorDrawable.getIntrinsicHeight();
        r2.A01 = colorDrawable;
        PreferenceFragmentCompat preferenceFragmentCompat = r2.A03;
        preferenceFragmentCompat.A03.A0M();
        r2.A00 = 0;
        preferenceFragmentCompat.A03.A0M();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0069, code lost:
        if (r1 == 0) goto L_0x006b;
     */
    @Override // androidx.preference.PreferenceFragmentCompat
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A18(java.lang.String r6, android.os.Bundle r7) {
        /*
            r5 = this;
            X.1yU r0 = r5.A00
            if (r0 == 0) goto L_0x007c
            X.00k r1 = r5.A0C()
            r0 = 2131891606(0x7f121596, float:1.9417937E38)
            java.lang.String r1 = r1.getString(r0)
            X.1yU r0 = r5.A00
            if (r0 == 0) goto L_0x0016
            r0.setTitle(r1)
        L_0x0016:
            r0 = 2132082697(0x7f150009, float:1.9805515E38)
            r5.A1C(r0)
            X.0nH r1 = r5.A02
            X.0nJ r0 = X.AbstractC15460nI.A0Q
            boolean r0 = r1.A05(r0)
            java.lang.String r2 = "email_chat_history"
            if (r0 == 0) goto L_0x007d
            X.0nT r0 = r5.A01
            r0.A08()
            androidx.preference.Preference r1 = r5.A9x(r2)
            X.3Rx r0 = new X.3Rx
            r0.<init>()
            r1.A0B = r0
        L_0x0038:
            java.lang.String r0 = "msgstore_delete_all_chats"
            androidx.preference.Preference r1 = r5.A9x(r0)
            X.4uQ r0 = new X.4uQ
            r0.<init>()
            r1.A0B = r0
            java.lang.String r0 = "msgstore_clear_all_chats"
            androidx.preference.Preference r1 = r5.A9x(r0)
            X.4uP r0 = new X.4uP
            r0.<init>()
            r1.A0B = r0
            java.lang.String r4 = "msgstore_archive_all_chats"
            androidx.preference.Preference r3 = r5.A9x(r4)
            X.0nj r0 = r5.A08
            int r2 = r0.A02()
            X.0nj r0 = r5.A08
            int r1 = r0.A01()
            if (r2 > 0) goto L_0x006b
            r0 = 2131892416(0x7f1218c0, float:1.941958E38)
            if (r1 != 0) goto L_0x006e
        L_0x006b:
            r0 = 2131886309(0x7f1200e5, float:1.9407193E38)
        L_0x006e:
            r3.A0A(r0)
            androidx.preference.Preference r1 = r5.A9x(r4)
            X.4uR r0 = new X.4uR
            r0.<init>()
            r1.A0B = r0
        L_0x007c:
            return
        L_0x007d:
            X.047 r0 = r5.A02
            androidx.preference.PreferenceScreen r1 = r0.A07
            if (r1 == 0) goto L_0x0038
            androidx.preference.Preference r0 = r5.A9x(r2)
            if (r0 == 0) goto L_0x0038
            r1.A0V(r0)
            r1.A05()
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsChatHistoryFragment.A18(java.lang.String, android.os.Bundle):void");
    }
}
