package com.whatsapp.settings;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14840m8;
import X.C14900mE;
import X.C22130yZ;
import X.C252018m;
import X.C42971wC;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.settings.SettingsSecurity;

/* loaded from: classes2.dex */
public class SettingsSecurity extends ActivityC13790kL {
    public C22130yZ A00;
    public C14840m8 A01;
    public C252018m A02;
    public boolean A03;

    public SettingsSecurity() {
        this(0);
    }

    public SettingsSecurity(int i) {
        this.A03 = false;
        ActivityC13830kP.A1P(this, 114);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = C12980iv.A0g(A1M);
            this.A01 = (C14840m8) A1M.ACi.get();
            this.A00 = (C22130yZ) A1M.A5d.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        setTitle(R.string.settings_security);
        setContentView(R.layout.settings_security);
        C12970iu.A0N(this).A0M(true);
        CompoundButton compoundButton = (CompoundButton) findViewById(R.id.security_notifications);
        compoundButton.setChecked(C12980iv.A1W(((ActivityC13810kN) this).A09.A00, "security_notifications"));
        compoundButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.3OO
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton2, boolean z) {
                C12960it.A0t(C12960it.A08(((ActivityC13810kN) SettingsSecurity.this).A09), "security_notifications", z);
            }
        });
        C14900mE r9 = ((ActivityC13810kN) this).A05;
        AnonymousClass12P r8 = ((ActivityC13790kL) this).A00;
        AnonymousClass01d r11 = ((ActivityC13810kN) this).A08;
        TextEmojiLabel A0T = C12970iu.A0T(((ActivityC13810kN) this).A00, R.id.settings_security_toggle_info);
        if (this.A01.A03()) {
            boolean A07 = this.A00.A0C.A07(903);
            i = R.string.security_code_notifications_description_md;
            if (A07) {
                i = R.string.security_code_notifications_description_md_final;
            }
        } else {
            i = R.string.security_code_notifications_description;
        }
        C42971wC.A08(this, this.A02.A04("security-and-privacy", "security-code-change-notification"), r8, r9, A0T, r11, C12960it.A0X(this, "learn-more", new Object[1], 0, i), "learn-more");
        C14900mE r92 = ((ActivityC13810kN) this).A05;
        AnonymousClass12P r82 = ((ActivityC13790kL) this).A00;
        AnonymousClass01d r112 = ((ActivityC13810kN) this).A08;
        C42971wC.A08(this, ((ActivityC13790kL) this).A02.A00("https://www.whatsapp.com/security"), r82, r92, C12970iu.A0T(((ActivityC13810kN) this).A00, R.id.settings_security_info_text), r112, C12960it.A0X(this, "learn-more", new Object[1], 0, R.string.security_page_main_description), "learn-more");
        TextView A0I = C12960it.A0I(((ActivityC13810kN) this).A00, R.id.settings_security_toggle_title);
        boolean A03 = this.A01.A03();
        int i2 = R.string.settings_security_notifications_toggle_title;
        if (A03) {
            i2 = R.string.settings_security_notifications_toggle_title_md;
        }
        A0I.setText(i2);
        C12960it.A11(findViewById(R.id.security_notifications_group), compoundButton, 13);
        if (((ActivityC13810kN) this).A0C.A07(1071)) {
            View A0D = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.e2ee_settings_layout);
            View A0D2 = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.settings_security_top_container);
            C12960it.A11(AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.security_settings_learn_more), this, 14);
            A0D.setVisibility(0);
            A0D2.setVisibility(8);
        }
    }
}
