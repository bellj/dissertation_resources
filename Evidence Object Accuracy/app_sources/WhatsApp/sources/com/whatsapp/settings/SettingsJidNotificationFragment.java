package com.whatsapp.settings;

import X.AbstractC11820gv;
import X.AbstractC14640lm;
import X.ActivityC44201yU;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass0FF;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C12960it;
import X.C15380n4;
import X.C15860o1;
import X.C18330sH;
import X.C22630zO;
import X.C252018m;
import X.C33181da;
import X.C36021jC;
import X.C41691tw;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceGroup;
import androidx.preference.PreferenceScreen;
import androidx.preference.TwoStatePreference;
import com.whatsapp.R;
import com.whatsapp.WaPreferenceFragment;
import com.whatsapp.preference.WaRingtonePreference;
import com.whatsapp.settings.SettingsJidNotificationFragment;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class SettingsJidNotificationFragment extends Hilt_SettingsJidNotificationFragment {
    public AnonymousClass12P A00;
    public C18330sH A01;
    public AnonymousClass018 A02;
    public AnonymousClass19M A03;
    public AbstractC14640lm A04;
    public C15860o1 A05;
    public C252018m A06;

    @Override // androidx.preference.PreferenceFragmentCompat, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        if (this.A05.A0U(this.A04.getRawString())) {
            Log.i("settings-jid-notifications/onStart settings-store updated, refreshing ui");
            ((PreferenceFragmentCompat) this).A02.A07.A0T();
            A1D();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017  */
    @Override // X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0t(int r5, int r6, android.content.Intent r7) {
        /*
            r4 = this;
            java.lang.String r3 = ""
            java.lang.String r1 = "android.intent.extra.ringtone.PICKED_URI"
            r0 = 1
            if (r5 != r0) goto L_0x001c
            if (r7 == 0) goto L_0x0028
            android.os.Parcelable r2 = r7.getParcelableExtra(r1)
            java.lang.String r0 = "jid_message_tone"
        L_0x000f:
            androidx.preference.Preference r1 = r4.A9x(r0)
            X.0gv r0 = r1.A0A
            if (r2 == 0) goto L_0x0018
            r3 = r2
        L_0x0018:
            r0.AU4(r1, r3)
            return
        L_0x001c:
            r0 = 2
            if (r5 != r0) goto L_0x0028
            if (r7 == 0) goto L_0x0028
            android.os.Parcelable r2 = r7.getParcelableExtra(r1)
            java.lang.String r0 = "jid_call_ringtone"
            goto L_0x000f
        L_0x0028:
            super.A0t(r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsJidNotificationFragment.A0t(int, int, android.content.Intent):void");
    }

    @Override // X.AnonymousClass01E
    public void A0y(Menu menu, MenuInflater menuInflater) {
        menu.add(0, R.id.menuitem_reset_notification_settings, 0, R.string.settings_notification_reset).setShowAsAction(0);
    }

    @Override // X.AnonymousClass01E
    public boolean A0z(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_reset_notification_settings) {
            return false;
        }
        C15860o1 r3 = this.A05;
        C33181da A08 = r3.A08(this.A04.getRawString());
        C33181da A02 = A08.A02();
        A08.A0A = A02.A07();
        A08.A0B = A02.A08();
        A08.A09 = A02.A06();
        A08.A08 = A02.A05();
        A08.A06 = A02.A03();
        A08.A07 = A02.A04();
        A08.A0I = false;
        A08.A0D = false;
        r3.A0M(A08);
        ((PreferenceFragmentCompat) this).A02.A07.A0T();
        A1D();
        return true;
    }

    @Override // androidx.preference.PreferenceFragmentCompat, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        A0M();
    }

    @Override // androidx.preference.PreferenceFragmentCompat
    public void A18(String str, Bundle bundle) {
        AbstractC14640lm A01 = AbstractC14640lm.A01(A0B().getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        this.A04 = A01;
        String string = A0C().getString(R.string.settings_notifications);
        ActivityC44201yU r0 = ((WaPreferenceFragment) this).A00;
        if (r0 != null) {
            r0.setTitle(string);
        }
        A1D();
    }

    public final void A1D() {
        C33181da A08 = this.A05.A08(this.A04.getRawString());
        A1C(R.xml.preferences_jid_notifications);
        WaRingtonePreference waRingtonePreference = (WaRingtonePreference) A9x("jid_message_tone");
        String A07 = A08.A07();
        waRingtonePreference.A00 = 7;
        waRingtonePreference.A02 = true;
        waRingtonePreference.A03 = true;
        waRingtonePreference.A01 = A07;
        waRingtonePreference.A0I(C22630zO.A06(((WaPreferenceFragment) this).A00, A07));
        waRingtonePreference.A0A = new AbstractC11820gv(waRingtonePreference, this) { // from class: X.3Rv
            public final /* synthetic */ WaRingtonePreference A00;
            public final /* synthetic */ SettingsJidNotificationFragment A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC11820gv
            public final boolean AU4(Preference preference, Object obj) {
                SettingsJidNotificationFragment settingsJidNotificationFragment = this.A01;
                WaRingtonePreference waRingtonePreference2 = this.A00;
                String obj2 = obj.toString();
                waRingtonePreference2.A01 = obj2;
                waRingtonePreference2.A0I(C22630zO.A06(preference.A05, obj2));
                settingsJidNotificationFragment.A05.A0P(settingsJidNotificationFragment.A04.getRawString(), obj2);
                return true;
            }
        };
        ListPreference listPreference = (ListPreference) A9x("jid_message_vibrate");
        listPreference.A0U(A08.A08());
        listPreference.A0I(listPreference.A0T());
        listPreference.A0A = new AbstractC11820gv() { // from class: X.3Rp
            @Override // X.AbstractC11820gv
            public final boolean AU4(Preference preference, Object obj) {
                SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                C15860o1 A0d = C12970iu.A0d(preference, settingsJidNotificationFragment, obj);
                AbstractC14640lm r0 = settingsJidNotificationFragment.A04;
                A0d.A0Q(r0.getRawString(), obj.toString());
                return true;
            }
        };
        ListPreference listPreference2 = (ListPreference) A9x("jid_message_popup");
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            final int A00 = C41691tw.A00(((WaPreferenceFragment) this).A00, R.attr.settingsTextDisabled, R.color.settings_disabled_text);
            PreferenceGroup preferenceGroup = (PreferenceGroup) A9x("notification");
            preferenceGroup.A0V(listPreference2);
            preferenceGroup.A05();
            AnonymousClass1 r6 = new ListPreference(((WaPreferenceFragment) this).A00) { // from class: com.whatsapp.settings.SettingsJidNotificationFragment.1
                @Override // androidx.preference.DialogPreference, androidx.preference.Preference
                public void A07() {
                    C36021jC.A01(((WaPreferenceFragment) this).A00, 0);
                }

                @Override // androidx.preference.Preference
                public void A0R(AnonymousClass0FF r4) {
                    super.A0R(r4);
                    View view = r4.A0H;
                    TextView A0J = C12960it.A0J(view, 16908310);
                    TextView A0J2 = C12960it.A0J(view, 16908304);
                    int i2 = A00;
                    A0J.setTextColor(i2);
                    A0J2.setTextColor(i2);
                    r4.A00 = true;
                    r4.A01 = true;
                }
            };
            String str = listPreference2.A0L;
            r6.A0L = str;
            if (r6.A0Y && !(!TextUtils.isEmpty(str))) {
                if (!TextUtils.isEmpty(r6.A0L)) {
                    r6.A0Y = true;
                } else {
                    throw new IllegalStateException("Preference does not have a key assigned.");
                }
            }
            r6.A0X = listPreference2.A0X;
            r6.A0I = listPreference2.A01;
            r6.A0V(listPreference2.A03);
            ((ListPreference) r6).A04 = listPreference2.A04;
            r6.A0I(listPreference2.A02());
            CharSequence charSequence = listPreference2.A0H;
            CharSequence charSequence2 = r6.A0H;
            if (charSequence != null ? !charSequence.equals(charSequence2) : charSequence2 != null) {
                r6.A0H = charSequence;
                r6.A04();
            }
            int i2 = ((Preference) listPreference2).A02;
            if (i2 != ((Preference) r6).A02) {
                ((Preference) r6).A02 = i2;
                r6.A05();
            }
            boolean z = listPreference2.A0V;
            if (r6.A0V != z) {
                r6.A0V = z;
                r6.A04();
            }
            preferenceGroup.A0U(r6);
            r6.A0U(listPreference2.A01);
            r6.A0I(((Preference) r6).A05.getString(R.string.popup_notification_not_available));
        } else {
            listPreference2.A0U(A08.A06());
            listPreference2.A0I(listPreference2.A0T());
            listPreference2.A0A = new AbstractC11820gv() { // from class: X.3Rr
                @Override // X.AbstractC11820gv
                public final boolean AU4(Preference preference, Object obj) {
                    SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                    C15860o1 A0d = C12970iu.A0d(preference, settingsJidNotificationFragment, obj);
                    AbstractC14640lm r0 = settingsJidNotificationFragment.A04;
                    A0d.A0O(r0.getRawString(), obj.toString());
                    return true;
                }
            };
        }
        ListPreference listPreference3 = (ListPreference) A9x("jid_message_light");
        listPreference3.A0V(this.A02.A0S(SettingsNotifications.A0q));
        listPreference3.A0U(A08.A05());
        listPreference3.A0I(listPreference3.A0T());
        listPreference3.A0A = new AbstractC11820gv() { // from class: X.3Rt
            @Override // X.AbstractC11820gv
            public final boolean AU4(Preference preference, Object obj) {
                ActivityC44201yU r0;
                SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                String str2 = Build.MODEL;
                if ((str2.contains("Desire") || str2.contains("Wildfire")) && !obj.toString().equals("00FF00") && (r0 = ((WaPreferenceFragment) settingsJidNotificationFragment).A00) != null) {
                    r0.Ado(R.string.led_support_green_only);
                }
                C12970iu.A0d(preference, settingsJidNotificationFragment, obj).A0N(settingsJidNotificationFragment.A04.getRawString(), obj.toString());
                return true;
            }
        };
        TwoStatePreference twoStatePreference = (TwoStatePreference) A9x("jid_use_high_priority_notifications");
        if (i < 21) {
            PreferenceGroup preferenceGroup2 = (PreferenceGroup) A9x("notification");
            preferenceGroup2.A0V(twoStatePreference);
            preferenceGroup2.A05();
        } else {
            twoStatePreference.A0T(!A08.A0B());
            twoStatePreference.A0A = new AbstractC11820gv() { // from class: X.3Rq
                @Override // X.AbstractC11820gv
                public final boolean AU4(Preference preference, Object obj) {
                    SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                    C15860o1 r3 = settingsJidNotificationFragment.A05;
                    AbstractC14640lm r1 = settingsJidNotificationFragment.A04;
                    boolean equals = Boolean.FALSE.equals(obj);
                    C33181da A002 = C15860o1.A00(r1, r3);
                    if (equals == A002.A0D) {
                        return true;
                    }
                    A002.A0D = equals;
                    r3.A0M(A002);
                    return true;
                }
            };
        }
        if (C15380n4.A0J(this.A04)) {
            Preference A9x = A9x("jid_call");
            if (A9x != null) {
                PreferenceScreen preferenceScreen = ((PreferenceFragmentCompat) this).A02.A07;
                preferenceScreen.A0V(A9x);
                preferenceScreen.A05();
            }
        } else {
            WaRingtonePreference waRingtonePreference2 = (WaRingtonePreference) A9x("jid_call_ringtone");
            String A03 = A08.A03();
            waRingtonePreference2.A00 = 1;
            waRingtonePreference2.A02 = true;
            waRingtonePreference2.A03 = true;
            waRingtonePreference2.A01 = A03;
            waRingtonePreference2.A0I(C22630zO.A06(((WaPreferenceFragment) this).A00, A03));
            waRingtonePreference2.A0A = new AbstractC11820gv(waRingtonePreference2, this) { // from class: X.3Rw
                public final /* synthetic */ WaRingtonePreference A00;
                public final /* synthetic */ SettingsJidNotificationFragment A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC11820gv
                public final boolean AU4(Preference preference, Object obj) {
                    SettingsJidNotificationFragment settingsJidNotificationFragment = this.A01;
                    WaRingtonePreference waRingtonePreference3 = this.A00;
                    String obj2 = obj.toString();
                    waRingtonePreference3.A01 = obj2;
                    waRingtonePreference3.A0I(C22630zO.A06(preference.A05, obj2));
                    C15860o1 r2 = settingsJidNotificationFragment.A05;
                    C33181da A002 = C15860o1.A00(settingsJidNotificationFragment.A04, r2);
                    if (TextUtils.equals(obj2, A002.A06)) {
                        return true;
                    }
                    A002.A06 = obj2;
                    r2.A0M(A002);
                    return true;
                }
            };
            ListPreference listPreference4 = (ListPreference) A9x("jid_call_vibrate");
            listPreference4.A0U(A08.A04());
            listPreference4.A0I(listPreference4.A0T());
            listPreference4.A0A = new AbstractC11820gv() { // from class: X.3Rs
                @Override // X.AbstractC11820gv
                public final boolean AU4(Preference preference, Object obj) {
                    SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                    C15860o1 A0d = C12970iu.A0d(preference, settingsJidNotificationFragment, obj);
                    AbstractC14640lm r0 = settingsJidNotificationFragment.A04;
                    String obj2 = obj.toString();
                    C33181da A002 = C15860o1.A00(r0, A0d);
                    if (TextUtils.equals(obj2, A002.A07)) {
                        return true;
                    }
                    A002.A07 = obj2;
                    A0d.A0M(A002);
                    return true;
                }
            };
        }
        TwoStatePreference twoStatePreference2 = (TwoStatePreference) A9x("jid_use_custom");
        twoStatePreference2.A0T(A08.A0I);
        twoStatePreference2.A0A = new AbstractC11820gv() { // from class: X.3Ru
            @Override // X.AbstractC11820gv
            public final boolean AU4(Preference preference, Object obj) {
                SettingsJidNotificationFragment settingsJidNotificationFragment = SettingsJidNotificationFragment.this;
                C15860o1 r5 = settingsJidNotificationFragment.A05;
                AbstractC14640lm r1 = settingsJidNotificationFragment.A04;
                boolean equals = Boolean.TRUE.equals(obj);
                C18330sH r3 = settingsJidNotificationFragment.A01;
                C33181da A002 = C15860o1.A00(r1, r5);
                if (equals != A002.A0I) {
                    if (equals) {
                        A002.A0D = A002.A0B();
                        C15370n3 A0A = r5.A09.A0A(r1);
                        if (A0A != null) {
                            r3.A03(r5.A0G.A00, A0A);
                        }
                    } else {
                        r3.A07(r1);
                    }
                    A002.A0I = equals;
                    r5.A0M(A002);
                }
                settingsJidNotificationFragment.A1E();
                return true;
            }
        };
        A1E();
    }

    public final void A1E() {
        boolean z = this.A05.A08(this.A04.getRawString()).A0I;
        A9x("jid_message_tone").A0M(z);
        A9x("jid_message_vibrate").A0M(z);
        A9x("jid_message_popup").A0M(z);
        A9x("jid_message_light").A0M(z);
        if (Build.VERSION.SDK_INT >= 21) {
            A9x("jid_use_high_priority_notifications").A0M(z);
        }
        if (!C15380n4.A0J(this.A04)) {
            A9x("jid_call_ringtone").A0M(z);
            A9x("jid_call_vibrate").A0M(z);
        }
    }

    @Override // androidx.preference.PreferenceFragmentCompat, X.AbstractC005302m
    public boolean AU6(Preference preference) {
        String str = preference.A0L;
        if (str.equals("jid_message_tone")) {
            startActivityForResult(((WaRingtonePreference) preference).A0S(), 1);
            return true;
        } else if (!str.equals("jid_call_ringtone")) {
            return super.AU6(preference);
        } else {
            startActivityForResult(((WaRingtonePreference) preference).A0S(), 2);
            return true;
        }
    }
}
