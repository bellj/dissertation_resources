package com.whatsapp.settings;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class SettingsRowPrivacyText extends LinearLayout {
    public SettingsRowPrivacyText(Context context) {
        this(context, null);
    }

    public SettingsRowPrivacyText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
        LinearLayout.inflate(context, R.layout.settings_row_privacy_text, this);
    }
}
