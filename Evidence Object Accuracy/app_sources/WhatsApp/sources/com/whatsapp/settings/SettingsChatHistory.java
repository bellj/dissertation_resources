package com.whatsapp.settings;

import X.ActivityC13830kP;
import X.ActivityC44201yU;
import X.C004902f;
import X.C12970iu;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.WaPreferenceFragment;

/* loaded from: classes2.dex */
public class SettingsChatHistory extends ActivityC44201yU {
    public boolean A00;

    public SettingsChatHistory() {
        this(0);
    }

    public SettingsChatHistory(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 110);
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ((ActivityC44201yU) this).A05 = C12970iu.A0R(ActivityC13830kP.A1M(ActivityC13830kP.A1L(this), this));
        }
    }

    @Override // X.ActivityC44201yU, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.preference_activity);
        if (bundle == null) {
            ((ActivityC44201yU) this).A06 = new SettingsChatHistoryFragment();
            C004902f A0P = C12970iu.A0P(this);
            A0P.A0B(((ActivityC44201yU) this).A06, "preferenceFragment", R.id.preference_fragment);
            A0P.A01();
            return;
        }
        setTitle(bundle.getCharSequence("settingsChatHistoryTitle"));
        ((ActivityC44201yU) this).A06 = (WaPreferenceFragment) A0V().A0A("preferenceFragment");
    }

    @Override // X.ActivityC44201yU, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("settingsChatHistoryTitle", getTitle());
    }
}
