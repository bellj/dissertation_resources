package com.whatsapp.settings;

import X.AbstractC14440lR;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.C15880o3;

/* loaded from: classes2.dex */
public class SettingsChatViewModel extends AnonymousClass015 {
    public final AnonymousClass016 A00 = new AnonymousClass016(0L);
    public final C15880o3 A01;
    public final AbstractC14440lR A02;

    public SettingsChatViewModel(C15880o3 r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A01 = r3;
    }
}
