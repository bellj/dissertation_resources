package com.whatsapp.settings;

import X.AbstractC88114Ei;
import X.AnonymousClass00T;
import X.C12980iv;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SettingsRowNoticeView extends SettingsRowIconText {
    public Drawable A00;
    public boolean A01;

    public SettingsRowNoticeView(Context context) {
        this(context, null);
    }

    public SettingsRowNoticeView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.A00 = AnonymousClass00T.A04(context, R.drawable.ic_settings_row_badge);
    }

    public SettingsRowNoticeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A00();
    }

    public int getNoticeId() {
        return 0;
    }

    public void setNotice(AbstractC88114Ei r2) {
        throw C12980iv.A0n("getNoticeId");
    }
}
