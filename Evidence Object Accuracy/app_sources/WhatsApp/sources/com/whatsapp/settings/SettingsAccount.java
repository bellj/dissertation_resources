package com.whatsapp.settings;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103434qk;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C18980tN;
import X.C18990tO;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import android.os.Bundle;
import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SettingsAccount extends ActivityC13790kL {
    public C18990tO A00;
    public boolean A01;

    public SettingsAccount() {
        this(0);
    }

    public SettingsAccount(int i) {
        this.A01 = false;
        A0R(new C103434qk(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r1.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) this).A09 = r1.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r2.A8B.get();
            this.A00 = new C18990tO((C18980tN) r1.A0G.get());
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_account_info);
        setContentView(R.layout.preferences_account);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        findViewById(R.id.privacy_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 7));
        findViewById(R.id.security_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 11));
        boolean booleanExtra = getIntent().getBooleanExtra("is_companion", false);
        View findViewById = findViewById(R.id.log_out_preference);
        if (booleanExtra) {
            findViewById.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 6));
            findViewById(R.id.two_step_verification_preference).setVisibility(8);
            findViewById(R.id.change_number_preference).setVisibility(8);
            findViewById(R.id.delete_account_preference).setVisibility(8);
        } else {
            findViewById.setVisibility(8);
            findViewById(R.id.two_step_verification_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 8));
            findViewById(R.id.change_number_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 10));
            findViewById(R.id.delete_account_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 5));
        }
        findViewById(R.id.request_account_info_preference).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 9));
    }
}
