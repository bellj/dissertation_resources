package com.whatsapp.settings;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC18900tF;
import X.AbstractC27281Gs;
import X.AbstractC454421p;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00E;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass10S;
import X.AnonymousClass11P;
import X.AnonymousClass12P;
import X.AnonymousClass12U;
import X.AnonymousClass130;
import X.AnonymousClass17R;
import X.AnonymousClass17S;
import X.AnonymousClass19D;
import X.AnonymousClass19M;
import X.AnonymousClass1A1;
import X.AnonymousClass1AR;
import X.AnonymousClass1CG;
import X.AnonymousClass1J1;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2I2;
import X.AnonymousClass308;
import X.AnonymousClass3H4;
import X.AnonymousClass3J4;
import X.AnonymousClass439;
import X.AnonymousClass43A;
import X.AnonymousClass5UL;
import X.AnonymousClass5W3;
import X.C103424qj;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15370n3;
import X.C15450nH;
import X.C15510nN;
import X.C15550nR;
import X.C15570nT;
import X.C15610nY;
import X.C15810nw;
import X.C15880o3;
import X.C16120oU;
import X.C17010q7;
import X.C18000rk;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C20660w7;
import X.C21240x6;
import X.C21270x9;
import X.C21820y2;
import X.C21840y4;
import X.C21880y8;
import X.C22040yO;
import X.C22670zS;
import X.C22990zy;
import X.C237913a;
import X.C249317l;
import X.C249517n;
import X.C250918b;
import X.C252718t;
import X.C252818u;
import X.C27131Gd;
import X.C27621Ig;
import X.C36401jp;
import X.C40691s6;
import X.C83643xZ;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.settings.Settings;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.Locale;

/* loaded from: classes2.dex */
public class Settings extends ActivityC13790kL implements AnonymousClass5W3, AnonymousClass5UL {
    public int A00;
    public long A01;
    public View A02;
    public ImageView A03;
    public AnonymousClass17S A04;
    public C237913a A05;
    public AnonymousClass1AR A06;
    public TextEmojiLabel A07;
    public TextEmojiLabel A08;
    public C250918b A09;
    public AnonymousClass130 A0A;
    public C15550nR A0B;
    public AnonymousClass10S A0C;
    public C15610nY A0D;
    public AnonymousClass1J1 A0E;
    public AnonymousClass1J1 A0F;
    public C21270x9 A0G;
    public C21240x6 A0H;
    public C17010q7 A0I;
    public AnonymousClass19D A0J;
    public AnonymousClass11P A0K;
    public AnonymousClass2I2 A0L;
    public C15370n3 A0M;
    public C16120oU A0N;
    public C22040yO A0O;
    public C20660w7 A0P;
    public SettingsRowIconText A0Q;
    public AnonymousClass17R A0R;
    public AnonymousClass12U A0S;
    public C249517n A0T;
    public C22990zy A0U;
    public AnonymousClass1CG A0V;
    public C21880y8 A0W;
    public AbstractC14440lR A0X;
    public AnonymousClass01H A0Y;
    public AnonymousClass01H A0Z;
    public AnonymousClass01H A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public final C27131Gd A0e;
    public final AbstractC18900tF A0f;

    public Settings() {
        this(0);
        this.A01 = 0;
        this.A0e = new C36401jp(this);
        this.A0f = new AbstractC18900tF() { // from class: X.562
            @Override // X.AbstractC18900tF
            public final void ASC() {
                Settings settings = Settings.this;
                settings.A0d = true;
                C237913a r2 = settings.A05;
                r2.A01 = false;
                r2.A00 = null;
                r2.A08.A0t(null, null);
            }
        };
    }

    public Settings(int i) {
        this.A0b = false;
        A0R(new C103424qj(this));
    }

    public static /* synthetic */ void A02(Settings settings, Integer num) {
        AnonymousClass439 r1 = new AnonymousClass439();
        r1.A00 = num;
        settings.A0N.A05(r1);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0b) {
            this.A0b = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A05 = (C237913a) r1.ACt.get();
            this.A0X = (AbstractC14440lR) r1.ANe.get();
            this.A04 = (AnonymousClass17S) r1.A0F.get();
            this.A0S = (AnonymousClass12U) r1.AJd.get();
            this.A0N = (C16120oU) r1.ANE.get();
            this.A0P = (C20660w7) r1.AIB.get();
            this.A06 = (AnonymousClass1AR) r1.ALM.get();
            this.A0G = (C21270x9) r1.A4A.get();
            this.A0R = (AnonymousClass17R) r1.AIz.get();
            this.A0O = (C22040yO) r1.A01.get();
            this.A0A = (AnonymousClass130) r1.A41.get();
            this.A0B = (C15550nR) r1.A45.get();
            this.A0L = r2.A05();
            this.A0D = (C15610nY) r1.AMe.get();
            this.A0H = (C21240x6) r1.AA7.get();
            this.A0W = (C21880y8) r1.AD5.get();
            this.A0U = (C22990zy) r1.AKn.get();
            this.A0V = (AnonymousClass1CG) r1.AKo.get();
            this.A0J = (AnonymousClass19D) r1.ABo.get();
            this.A0I = (C17010q7) r1.A43.get();
            this.A0K = (AnonymousClass11P) r1.ABp.get();
            this.A0T = (C249517n) r1.AKm.get();
            this.A0Y = C18000rk.A00(r1.A0C);
            this.A0Z = C18000rk.A00(r1.ADw);
            this.A0a = C18000rk.A00(r1.AID);
            this.A09 = (C250918b) r1.A2B.get();
            this.A0C = (AnonymousClass10S) r1.A46.get();
        }
    }

    public final void A2e() {
        C15370n3 r2 = this.A0M;
        if (r2 != null) {
            this.A0E.A06(this.A03, r2);
            return;
        }
        this.A03.setImageBitmap(AnonymousClass130.A00(this, -1.0f, R.drawable.avatar_contact, this.A00));
    }

    @Override // X.ActivityC13790kL, X.AbstractC13880kU
    public AnonymousClass00E AGM() {
        return AnonymousClass01V.A02;
    }

    @Override // X.AnonymousClass5W3
    public void ARi() {
        long j = this.A01;
        if (j > 0) {
            AnonymousClass43A r2 = new AnonymousClass43A();
            r2.A00 = Long.valueOf(System.currentTimeMillis() - j);
            this.A0N.A07(r2);
            this.A01 = 0;
        }
    }

    @Override // X.AnonymousClass5UL
    public void ARj() {
        if (this.A0d) {
            this.A0d = false;
            finish();
            startActivity(getIntent());
        }
    }

    @Override // X.AnonymousClass5W3
    public void ARk() {
        this.A01 = System.currentTimeMillis();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 200 && i2 == -1) {
            throw new UnsupportedOperationException();
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String A01;
        if (AbstractC454421p.A00) {
            getWindow().requestFeature(12);
        }
        super.onCreate(bundle);
        setTitle(R.string.settings_general);
        setContentView(R.layout.preferences);
        A1e((Toolbar) AnonymousClass00T.A05(this, R.id.toolbar));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0A(R.string.settings_general);
        A1U.A0M(true);
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        C27621Ig r02 = r0.A01;
        this.A0M = r02;
        if (r02 == null) {
            Log.i("settings/create/no-me");
            startActivity(C14960mK.A04(this));
            finish();
            return;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.pref_profile_small_photo_size);
        this.A00 = dimensionPixelSize;
        this.A0E = this.A0G.A05("settings-activity-contact-photo", -1.0f, dimensionPixelSize);
        ImageView imageView = (ImageView) findViewById(R.id.profile_info_photo);
        this.A03 = imageView;
        imageView.setVisibility(0);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.profile_info_name);
        this.A07 = textEmojiLabel;
        textEmojiLabel.setVisibility(0);
        this.A07.A0G(null, ((ActivityC13790kL) this).A01.A05());
        this.A08 = (TextEmojiLabel) findViewById(R.id.profile_info_status);
        findViewById(R.id.profile_info).setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 22));
        A2e();
        this.A0C.A03(this.A0e);
        ImageView imageView2 = (ImageView) findViewById(R.id.profile_info_qr_code);
        imageView2.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 1));
        imageView2.setVisibility(0);
        imageView2.setContentDescription(getString(R.string.settings_qr));
        AnonymousClass2GE.A07(imageView2, AnonymousClass00T.A00(this, R.color.icon_primary));
        SettingsRowIconText settingsRowIconText = (SettingsRowIconText) findViewById(R.id.settings_help);
        settingsRowIconText.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 23));
        settingsRowIconText.setIcon(new C83643xZ(AnonymousClass00T.A04(this, R.drawable.ic_settings_help), ((ActivityC13830kP) this).A01));
        findViewById(R.id.setting_tell_a_friend).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 2));
        View findViewById = findViewById(R.id.business_tools);
        View findViewById2 = findViewById(R.id.premium_tools);
        View findViewById3 = findViewById(R.id.business_tools_divider);
        findViewById.setVisibility(8);
        findViewById2.setVisibility(8);
        findViewById3.setVisibility(8);
        SettingsRowIconText settingsRowIconText2 = (SettingsRowIconText) findViewById(R.id.settings_chat);
        settingsRowIconText2.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 24));
        settingsRowIconText2.setSubText(getString(R.string.chat_settings_description_with_theme));
        SettingsRowIconText settingsRowIconText3 = (SettingsRowIconText) AnonymousClass00T.A05(this, R.id.settings_data_usage);
        settingsRowIconText3.setText(getString(R.string.settings_storage_and_data_usage_enhanced));
        settingsRowIconText3.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 25));
        findViewById(R.id.settings_notifications).setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 26));
        ((ActivityC13790kL) this).A01.A08();
        findViewById(R.id.settings_account_info).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 3));
        if (((ActivityC13810kN) this).A0C.A07(1396)) {
            SettingsRowIconText settingsRowIconText4 = (SettingsRowIconText) findViewById(R.id.settings_avatar);
            settingsRowIconText4.setText(getString(R.string.settings_avatar));
            settingsRowIconText4.setSubText(getString(R.string.settings_avatar_subtitle));
            settingsRowIconText4.setIcon(AnonymousClass00T.A04(this, R.drawable.ic_settings_avatar));
            settingsRowIconText4.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 27));
            settingsRowIconText4.setVisibility(0);
        }
        this.A0Q = (SettingsRowIconText) findViewById(R.id.settings_language);
        if (AnonymousClass3J4.A03(((ActivityC13810kN) this).A08, this.A0O)) {
            C15570nT r03 = ((ActivityC13790kL) this).A01;
            r03.A08();
            Me me = r03.A00;
            if (me != null) {
                ((ActivityC13790kL) this).A01.A08();
                this.A0Q.setVisibility(0);
                if (AnonymousClass3J4.A02()) {
                    A01 = getString(R.string.device_default_language_with_placeholder_when_language_selector_enabled, AbstractC27281Gs.A01(AnonymousClass018.A00(((ActivityC13830kP) this).A01.A00)));
                } else {
                    A01 = AbstractC27281Gs.A01(Locale.getDefault());
                }
                this.A0Q.setSubText(A01);
                this.A0Q.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 4));
                AnonymousClass018 r04 = ((ActivityC13830kP) this).A01;
                String str = new AnonymousClass3H4(me.cc, me.number, r04.A05, r04.A04).A02;
                if (!str.isEmpty()) {
                    AnonymousClass308 r1 = new AnonymousClass308();
                    r1.A00 = str;
                    this.A0N.A07(r1);
                }
            } else {
                this.A0Q.setVisibility(8);
            }
        }
        this.A0d = false;
        ((ActivityC13830kP) this).A01.A0B.add(this.A0f);
        this.A0c = true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        if (this.A0c) {
            this.A0C.A04(this.A0e);
            this.A0E.A00();
            AnonymousClass018 r0 = ((ActivityC13830kP) this).A01;
            r0.A0B.remove(this.A0f);
        }
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A02(this.A02, this.A0K);
            AnonymousClass1J1 r02 = this.A0F;
            if (r02 != null) {
                r02.A00();
                this.A0F = null;
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            C40691s6.A07(this.A0K);
            ((AnonymousClass1A1) this.A0Z.get()).A02(((ActivityC13810kN) this).A00);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        if (this.A0d) {
            this.A0d = false;
            finish();
            startActivity(getIntent());
        }
        super.onResume();
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        this.A0M = r0.A01;
        this.A07.A0G(null, ((ActivityC13790kL) this).A01.A05());
        this.A08.A0G(null, this.A05.A00());
        if (((ActivityC13810kN) this).A0C.A07(931)) {
            boolean z = ((AnonymousClass1A1) this.A0Z.get()).A03;
            View view = ((ActivityC13810kN) this).A00;
            if (z) {
                C14850m9 r02 = ((ActivityC13810kN) this).A0C;
                C14900mE r15 = ((ActivityC13810kN) this).A05;
                C15570nT r12 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r11 = this.A0X;
                C21270x9 r10 = this.A0G;
                C15550nR r9 = this.A0B;
                C15610nY r8 = this.A0D;
                AnonymousClass018 r7 = ((ActivityC13830kP) this).A01;
                Pair A00 = C40691s6.A00(this, view, this.A02, r15, r12, r9, r8, this.A0F, r10, this.A0J, this.A0K, ((ActivityC13810kN) this).A09, r7, r02, r11, this.A0Z, this.A0a, "settings-activity");
                this.A02 = (View) A00.first;
                this.A0F = (AnonymousClass1J1) A00.second;
            } else if (AnonymousClass1A1.A00(view)) {
                C40691s6.A04(((ActivityC13810kN) this).A00, this.A0K, this.A0Z);
            }
            ((AnonymousClass1A1) this.A0Z.get()).A01();
        }
        if (this.A0W.A08()) {
            SettingsRowIconText settingsRowIconText = (SettingsRowIconText) findViewById(R.id.settings_help);
            if (settingsRowIconText != null) {
                settingsRowIconText.setBadgeIcon(AnonymousClass00T.A04(this, R.drawable.ic_settings_row_badge));
            } else {
                Log.e("settings/showbadge cannot find help view");
            }
            C21880y8 r2 = this.A0W;
            if (r2.A0C) {
                r2.A07(new RunnableBRunnable0Shape13S0100000_I0_13(r2, 8));
                return;
            }
            return;
        }
        SettingsRowIconText settingsRowIconText2 = (SettingsRowIconText) findViewById(R.id.settings_help);
        if (settingsRowIconText2 != null) {
            settingsRowIconText2.setBadgeIcon(null);
        } else {
            Log.e("settings/showbadge cannot find help view");
        }
    }
}
