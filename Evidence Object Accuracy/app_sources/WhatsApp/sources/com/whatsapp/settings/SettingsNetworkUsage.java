package com.whatsapp.settings;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1MY;
import X.AnonymousClass1N1;
import X.AnonymousClass1US;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass3JK;
import X.AnonymousClass4QI;
import X.AnonymousClass5IR;
import X.C004802e;
import X.C103454qm;
import X.C12970iu;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C22730zY;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C38121nY;
import X.C44891zj;
import X.C72463ee;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.Statistics$Data;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.util.Log;
import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

/* loaded from: classes2.dex */
public class SettingsNetworkUsage extends ActivityC13790kL {
    public Handler A00;
    public C18790t3 A01;
    public C22730zY A02;
    public AnonymousClass018 A03;
    public TimerTask A04;
    public boolean A05;
    public final Timer A06;

    public SettingsNetworkUsage() {
        this(0);
        this.A06 = new Timer("refresh-network-usage");
    }

    public SettingsNetworkUsage(int i) {
        this.A05 = false;
        A0R(new C103454qm(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A01 = (C18790t3) r1.AJw.get();
            this.A03 = (AnonymousClass018) r1.ANb.get();
            this.A02 = (C22730zY) r1.A8Y.get();
        }
    }

    public final void A2e(int i, int i2, int i3, long j, long j2, long j3) {
        TextView textView = (TextView) findViewById(i);
        String A04 = C44891zj.A04(this.A03, j);
        textView.setText(A04);
        String A0F = this.A03.A0F(A04);
        int i4 = 0;
        textView.setContentDescription(getString(R.string.settings_data_network_usage_amount_sent, A0F));
        TextView textView2 = (TextView) findViewById(i2);
        String A042 = C44891zj.A04(this.A03, j2);
        textView2.setText(A042);
        textView2.setContentDescription(getString(R.string.settings_data_network_usage_amount_received, this.A03.A0F(A042)));
        if (j3 > 0) {
            i4 = (int) ((((float) (j + j2)) * 100.0f) / ((float) j3));
        }
        ((RoundCornerProgressBar) findViewById(i3)).setProgress(i4);
    }

    public final void A2f(boolean z) {
        String string;
        if (z) {
            C18790t3 r2 = this.A01;
            Log.i("statistics/reset");
            AnonymousClass1N1 r22 = r2.A00;
            boolean z2 = false;
            if (r22 != null) {
                z2 = true;
            }
            AnonymousClass009.A0F(z2);
            r22.sendEmptyMessage(9);
        }
        Statistics$Data A00 = this.A01.A00();
        NumberFormat instance = NumberFormat.getInstance(AnonymousClass018.A00(this.A03.A00));
        long j = A00.A0E + A00.A0G + A00.A0M + A00.A0D + A00.A0J;
        long j2 = A00.A01 + A00.A03 + A00.A0B + A00.A00 + A00.A08;
        long j3 = j + j2;
        AnonymousClass4QI A01 = C44891zj.A01(this.A03, j3);
        StringBuilder sb = new StringBuilder();
        String str = A01.A01;
        sb.append(str);
        sb.append(A01.A02);
        String str2 = A01.A00;
        sb.append(str2);
        SpannableString spannableString = new SpannableString(sb.toString());
        if (!str.isEmpty()) {
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), 0, str.length(), 33);
        }
        if (!str2.isEmpty()) {
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), spannableString.length() - str2.length(), spannableString.length(), 33);
        }
        ((TextView) findViewById(R.id.total_network_usage)).setText(spannableString);
        ((TextView) findViewById(R.id.total_network_usage_sent)).setText(C44891zj.A04(this.A03, j));
        ((TextView) findViewById(R.id.total_network_usage_received)).setText(C44891zj.A04(this.A03, j2));
        A2e(R.id.call_data_sent, R.id.call_data_received, R.id.calls_data_bar, A00.A0M, A00.A0B, j3);
        long j4 = A00.A0N;
        long j5 = A00.A0C;
        AnonymousClass018 r8 = this.A03;
        ((TextView) findViewById(R.id.calls_info)).setText(AnonymousClass1US.A06(r8, r8.A0I(new Object[]{instance.format(j4)}, R.plurals.settings_network_usage_calls_info_outgoing, j4), this.A03.A0I(new Object[]{instance.format(j5)}, R.plurals.settings_network_usage_calls_info_incoming, j5)));
        A2e(R.id.media_data_sent, R.id.media_data_received, R.id.media_data_bar, A00.A0E, A00.A01, j3);
        long j6 = A00.A0D;
        long j7 = A00.A00;
        if (this.A02.A09() || j6 > 0 || j7 > 0) {
            A2e(R.id.gdrive_data_sent, R.id.gdrive_data_received, R.id.gdrive_data_bar, j6, j7, j3);
        } else {
            findViewById(R.id.gdrive_row).setVisibility(8);
        }
        A2e(R.id.messages_data_sent, R.id.messages_data_received, R.id.messages_data_bar, A00.A0G, A00.A03, j3);
        long j8 = A00.A0L + A00.A0F;
        long j9 = A00.A0A + A00.A02;
        AnonymousClass018 r12 = this.A03;
        ((TextView) findViewById(R.id.messages_info)).setText(AnonymousClass1US.A06(r12, r12.A0I(new Object[]{instance.format(j8)}, R.plurals.settings_network_usage_messages_info_sent, j8), this.A03.A0I(new Object[]{instance.format(j9)}, R.plurals.settings_network_usage_messages_info_received, j9)));
        A2e(R.id.status_data_sent, R.id.status_data_received, R.id.status_data_bar, A00.A0J, A00.A08, j3);
        long j10 = A00.A0K;
        long j11 = A00.A09;
        AnonymousClass018 r122 = this.A03;
        ((TextView) findViewById(R.id.status_info)).setText(AnonymousClass1US.A06(r122, r122.A0I(new Object[]{instance.format(j10)}, R.plurals.settings_network_usage_status_info_sent, j10), this.A03.A0I(new Object[]{instance.format(j11)}, R.plurals.settings_network_usage_status_info_received, j11)));
        A2e(R.id.roaming_data_sent, R.id.roaming_data_received, R.id.roaming_data_bar, A00.A0I, A00.A07, j3);
        long j12 = A00.A0O;
        if (j12 != Long.MIN_VALUE) {
            findViewById(R.id.last_updated_date).setVisibility(0);
            AnonymousClass018 r6 = this.A03;
            string = getString(R.string.network_usage_last_reset_time, C38121nY.A05(r6, AnonymousClass1MY.A04(r6, j12), AnonymousClass3JK.A00(r6, j12)));
            ((TextView) findViewById(R.id.last_updated_date)).setText(getString(R.string.settings_network_usages_time_since_refresh_date, AnonymousClass1MY.A04(this.A03, j12)));
        } else {
            string = getString(R.string.network_usage_last_reset_time, getString(R.string.never));
            findViewById(R.id.last_updated_date).setVisibility(8);
        }
        ((TextView) findViewById(R.id.last_usage_reset)).setText(string);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.settings_network_usage);
        setContentView(R.layout.preferences_network_usage);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        findViewById(R.id.reset_network_usage_row).setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 19));
        this.A00 = new Handler(Looper.myLooper());
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.cancel();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A04.cancel();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        AnonymousClass5IR r1 = new AnonymousClass5IR(this);
        this.A04 = r1;
        this.A06.scheduleAtFixedRate(r1, 0, 1000);
    }

    /* loaded from: classes3.dex */
    public class ResetUsageConfirmationDialog extends Hilt_SettingsNetworkUsage_ResetUsageConfirmationDialog {
        @Override // androidx.fragment.app.DialogFragment
        public Dialog A1A(Bundle bundle) {
            C004802e r2 = new C004802e(A01());
            r2.A06(R.string.settings_network_usage_reset_prompt);
            C12970iu.A1L(r2, this, 65, R.string.reset);
            C72463ee.A0S(r2);
            return r2.create();
        }
    }
}
