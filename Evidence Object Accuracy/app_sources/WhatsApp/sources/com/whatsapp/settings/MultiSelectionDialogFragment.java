package com.whatsapp.settings;

import X.AbstractC48372Fv;
import X.AnonymousClass01E;
import X.AnonymousClass0OC;
import X.C004802e;
import X.C12960it;
import X.C12970iu;
import X.DialogInterface$OnMultiChoiceClickListenerC97684hT;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.whatsapp.R;
import com.whatsapp.settings.MultiSelectionDialogFragment;

/* loaded from: classes2.dex */
public class MultiSelectionDialogFragment extends Hilt_MultiSelectionDialogFragment {
    public int A00;
    public AbstractC48372Fv A01;
    public String A02;
    public String[] A03;
    public boolean[] A04;

    public static MultiSelectionDialogFragment A00(boolean[] zArr, int i, int i2) {
        MultiSelectionDialogFragment multiSelectionDialogFragment = new MultiSelectionDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogId", i);
        A0D.putInt("dialogTitleResId", i2);
        A0D.putInt("itemsResId", R.array.autodownload);
        A0D.putBooleanArray("selectedItems", zArr);
        multiSelectionDialogFragment.A0U(A0D);
        return multiSelectionDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        if (A0B() instanceof AbstractC48372Fv) {
            Bundle bundle2 = ((AnonymousClass01E) this).A05;
            this.A00 = bundle2.getInt("dialogId");
            this.A02 = A0I(bundle2.getInt("dialogTitleResId"));
            this.A03 = A02().getStringArray(bundle2.getInt("itemsResId"));
            this.A04 = bundle2.getBooleanArray("selectedItems");
            this.A01 = (AbstractC48372Fv) A0B();
            return;
        }
        throw C12960it.A0U(C12960it.A0d("MultiSelectionDialogListener", C12960it.A0k("Activity must implement ")));
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        C004802e A0O = C12970iu.A0O(this);
        A0O.setTitle(this.A02);
        String[] strArr = this.A03;
        boolean[] zArr = this.A04;
        DialogInterface$OnMultiChoiceClickListenerC97684hT r0 = new DialogInterface.OnMultiChoiceClickListener() { // from class: X.4hT
            @Override // android.content.DialogInterface.OnMultiChoiceClickListener
            public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
                MultiSelectionDialogFragment.this.A04[i] = z;
            }
        };
        AnonymousClass0OC r1 = A0O.A01;
        r1.A0M = strArr;
        r1.A09 = r0;
        r1.A0N = zArr;
        r1.A0K = true;
        C12970iu.A1M(A0O, this, 29, R.string.ok);
        A0O.setNegativeButton(R.string.cancel, new IDxCListenerShape4S0000000_2_I1(19));
        return A0O.create();
    }
}
