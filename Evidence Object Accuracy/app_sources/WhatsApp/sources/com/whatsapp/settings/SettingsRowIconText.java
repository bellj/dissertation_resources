package com.whatsapp.settings;

import X.AbstractC53152cz;
import X.AnonymousClass018;
import X.AnonymousClass2GE;
import X.AnonymousClass2GF;
import X.AnonymousClass2QN;
import X.C015007d;
import X.C12960it;
import X.C12980iv;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* loaded from: classes2.dex */
public class SettingsRowIconText extends AbstractC53152cz {
    public WaImageView A00;
    public WaImageView A01;
    public WaTextView A02;
    public WaTextView A03;
    public AnonymousClass018 A04;

    public SettingsRowIconText(Context context) {
        this(context, null);
    }

    public SettingsRowIconText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(0);
        View inflate = LinearLayout.inflate(context, R.layout.settings_row_icon_text, this);
        this.A01 = C12980iv.A0X(inflate, R.id.settings_row_icon);
        this.A03 = C12960it.A0N(inflate, R.id.settings_row_text);
        this.A02 = C12960it.A0N(inflate, R.id.settings_row_subtext);
        this.A00 = C12980iv.A0X(inflate, R.id.settings_row_badge);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0J);
        try {
            boolean z = obtainStyledAttributes.getBoolean(4, false);
            if (obtainStyledAttributes.hasValue(3)) {
                Drawable A01 = C015007d.A01(context, obtainStyledAttributes.getResourceId(3, -1));
                this.A01.setVisibility(A01 == null ? 8 : 0);
                if (A01 != null && z) {
                    A01 = new AnonymousClass2GF(A01, this.A04);
                }
                this.A01.setImageDrawable(A01);
            }
            int color = obtainStyledAttributes.getColor(1, -1);
            if (color != -1) {
                AnonymousClass2GE.A07(this.A01, color);
            }
            setText(this.A04.A0E(obtainStyledAttributes, 6));
            setSubText(this.A04.A0E(obtainStyledAttributes, 5));
            if (obtainStyledAttributes.hasValue(2)) {
                Drawable A012 = C015007d.A01(context, obtainStyledAttributes.getResourceId(2, -1));
                WaImageView waImageView = this.A00;
                int i = A012 != null ? 0 : 8;
                if (waImageView.getVisibility() != i) {
                    waImageView.setVisibility(i);
                }
                if (A012 != null && z) {
                    A012 = new AnonymousClass2GF(A012, this.A04);
                }
                this.A00.setImageDrawable(A012);
            }
            int color2 = obtainStyledAttributes.getColor(0, -1);
            if (color2 != -1) {
                AnonymousClass2GE.A07(this.A00, color2);
            }
            if (obtainStyledAttributes.hasValue(7)) {
                this.A02.setMaxLines(obtainStyledAttributes.getInt(7, 0));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public WaImageView getIcon() {
        return this.A01;
    }

    public void setBadgeIcon(Drawable drawable) {
        WaImageView waImageView = this.A00;
        int i = 8;
        if (drawable != null) {
            i = 0;
        }
        if (waImageView.getVisibility() != i) {
            waImageView.setVisibility(i);
        }
        waImageView.setImageDrawable(drawable);
    }

    public void setIcon(Drawable drawable) {
        WaImageView waImageView = this.A01;
        int i = 0;
        if (drawable == null) {
            i = 8;
        }
        waImageView.setVisibility(i);
        waImageView.setImageDrawable(drawable);
    }

    public void setSubText(CharSequence charSequence) {
        WaTextView waTextView = this.A02;
        int i = 0;
        if (charSequence == null) {
            i = 8;
        }
        waTextView.setVisibility(i);
        waTextView.setText(charSequence);
    }

    public void setText(CharSequence charSequence) {
        WaTextView waTextView = this.A03;
        int i = 0;
        if (charSequence == null) {
            i = 8;
        }
        waTextView.setVisibility(i);
        waTextView.setText(charSequence);
    }
}
