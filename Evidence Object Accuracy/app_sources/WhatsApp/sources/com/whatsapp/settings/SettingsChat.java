package com.whatsapp.settings;

import X.AbstractC15850o0;
import X.AbstractC27291Gt;
import X.AbstractC32851cq;
import X.AbstractC41281tH;
import X.AbstractC42541vN;
import X.AbstractC453621g;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass1SK;
import X.AnonymousClass1SM;
import X.AnonymousClass2FL;
import X.AnonymousClass308;
import X.AnonymousClass3H4;
import X.AnonymousClass3HS;
import X.AnonymousClass3J4;
import X.AnonymousClass3JK;
import X.C12960it;
import X.C12970iu;
import X.C15570nT;
import X.C15610nY;
import X.C15860o1;
import X.C15890o4;
import X.C16120oU;
import X.C16970q3;
import X.C17050qB;
import X.C18260sA;
import X.C19500uD;
import X.C22040yO;
import X.C22730zY;
import X.C28421Nd;
import X.C38121nY;
import X.C38131nZ;
import X.C68603Wa;
import X.ProgressDialogC48342Fq;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.settings.SettingsChat;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/* loaded from: classes2.dex */
public class SettingsChat extends AnonymousClass1SK implements AnonymousClass1SM {
    public static ProgressDialogC48342Fq A0T;
    public static ProgressDialogC48342Fq A0U;
    public int A00;
    public TextView A01;
    public SwitchCompat A02;
    public SwitchCompat A03;
    public AnonymousClass3HS A04;
    public C19500uD A05;
    public C22730zY A06;
    public C15610nY A07;
    public C16970q3 A08;
    public C17050qB A09;
    public C15890o4 A0A;
    public C18260sA A0B;
    public C16120oU A0C;
    public C22040yO A0D;
    public SettingsChatViewModel A0E;
    public SettingsRowIconText A0F;
    public SettingsRowIconText A0G;
    public SettingsRowIconText A0H;
    public C15860o1 A0I;
    public AbstractC15850o0 A0J;
    public boolean A0K;
    public boolean A0L;
    public String[] A0M;
    public String[] A0N;
    public String[] A0O;
    public String[] A0P;
    public final AbstractC32851cq A0Q;
    public final AbstractC453621g A0R;
    public final Set A0S;

    public SettingsChat() {
        this(0);
        this.A0R = new AbstractC453621g() { // from class: X.55x
            @Override // X.AbstractC453621g
            public final void AWT() {
                SettingsChat.this.A2f();
            }
        };
        this.A0S = C12970iu.A12();
        this.A0Q = new C68603Wa(this);
    }

    public SettingsChat(int i) {
        this.A0K = false;
        ActivityC13830kP.A1P(this, 109);
    }

    public static Dialog A02(Context context) {
        ProgressDialogC48342Fq r1 = new ProgressDialogC48342Fq(context);
        A0U = r1;
        r1.setTitle(R.string.msg_store_backup_db_title);
        A0U.setMessage(context.getString(R.string.settings_backup_db_now_message));
        A0U.setIndeterminate(true);
        A0U.setCancelable(false);
        return A0U;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.app.Dialog A03(android.content.Context r6) {
        /*
            boolean r0 = X.C14950mJ.A00()
            r5 = 0
            if (r0 == 0) goto L_0x0044
            java.lang.String r1 = android.os.Environment.getExternalStorageState()
            java.lang.String r0 = "unmounted"
            boolean r0 = r0.equals(r1)
            r4 = 2131889469(0x7f120d3d, float:1.9413602E38)
            r3 = 2131889468(0x7f120d3c, float:1.94136E38)
            if (r0 == 0) goto L_0x004a
            r4 = 2131889471(0x7f120d3f, float:1.9413607E38)
            r3 = 2131889470(0x7f120d3e, float:1.9413604E38)
            r0 = 64
            com.facebook.redex.IDxCListenerShape9S0100000_2_I1 r2 = new com.facebook.redex.IDxCListenerShape9S0100000_2_I1
            r2.<init>(r6, r0)
        L_0x0027:
            X.02e r1 = X.C12980iv.A0S(r6)
            r1.A07(r4)
            r1.A06(r3)
            r0 = 2131890036(0x7f120f74, float:1.9414752E38)
            r1.setPositiveButton(r0, r5)
            if (r2 == 0) goto L_0x003f
            r0 = 2131886735(0x7f12028f, float:1.9408057E38)
            r1.setPositiveButton(r0, r2)
        L_0x003f:
            X.04S r0 = r1.create()
            return r0
        L_0x0044:
            r4 = 2131889467(0x7f120d3b, float:1.9413598E38)
            r3 = 2131889597(0x7f120dbd, float:1.9413862E38)
        L_0x004a:
            r2 = r5
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsChat.A03(android.content.Context):android.app.Dialog");
    }

    public static String A09(Activity activity, AnonymousClass018 r6, long j) {
        int i;
        if (j == 0) {
            i = R.string.never;
        } else if (j == -1) {
            i = R.string.unknown;
        } else if (C38121nY.A00(System.currentTimeMillis(), j) == 0) {
            return AnonymousClass3JK.A00(r6, j);
        } else {
            return C38131nZ.A01(r6, j).toString();
        }
        return activity.getString(i);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0K) {
            this.A0K = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A0C = C12970iu.A0b(A1M);
            this.A08 = (C16970q3) A1M.A0a.get();
            this.A0D = (C22040yO) A1M.A01.get();
            this.A07 = C12960it.A0P(A1M);
            this.A0J = (AbstractC15850o0) A1M.ANA.get();
            this.A05 = (C19500uD) A1M.A1H.get();
            this.A0I = (C15860o1) A1M.A3H.get();
            this.A09 = (C17050qB) A1M.ABL.get();
            this.A0B = (C18260sA) A1M.AAZ.get();
            this.A0A = C12970iu.A0Y(A1M);
            this.A06 = (C22730zY) A1M.A8Y.get();
        }
    }

    @Override // X.ActivityC13810kN
    public void A2H(Configuration configuration) {
        if (!this.A0L) {
            super.A2H(configuration);
        }
    }

    public final int A2e(String[] strArr) {
        int A00 = C28421Nd.A00(((ActivityC13810kN) this).A09.A00.getString("interface_font_size", "0"), 0);
        for (int i = 0; i < strArr.length; i++) {
            if (A00 == Integer.valueOf(strArr[i]).intValue()) {
                return i;
            }
        }
        return -1;
    }

    public final void A2f() {
        SettingsRowIconText settingsRowIconText;
        String string;
        if (this.A0F != null) {
            if (this.A06.A09()) {
                settingsRowIconText = this.A0F;
                string = null;
            } else if (this.A0A.A07()) {
                SettingsChatViewModel settingsChatViewModel = this.A0E;
                settingsChatViewModel.A02.Ab2(new RunnableBRunnable0Shape11S0100000_I0_11(settingsChatViewModel, 40));
                return;
            } else {
                settingsRowIconText = this.A0F;
                string = getString(R.string.settings_msg_store_cannot_backup);
            }
            settingsRowIconText.setSubText(string);
        }
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        String str;
        String str2;
        Locale locale;
        if (i != 1) {
            if (i != 2) {
                if (i == 3 && this.A04.A02(i2)) {
                    this.A0H.setSubText(this.A04.A00());
                    finish();
                    overridePendingTransition(0, R.anim.fade_out);
                    this.A0L = true;
                } else {
                    return;
                }
            } else if (i2 != this.A00) {
                this.A00 = i2;
                this.A0G.setSubText(this.A0O[i2]);
                if (i2 == 0) {
                    str = null;
                } else {
                    str = this.A0P[i2];
                }
                AnonymousClass018 r3 = ((ActivityC13830kP) this).A01;
                StringBuilder A0k = C12960it.A0k("whatsapplocale/saveandapplyforcedlanguage/language to save: ");
                if (TextUtils.isEmpty(str)) {
                    str2 = "device default";
                } else {
                    str2 = str;
                }
                Log.i(C12960it.A0d(str2, A0k));
                if (!TextUtils.isEmpty(str)) {
                    if (AbstractC41281tH.A00.contains(AbstractC27291Gt.A01(AbstractC27291Gt.A09(str)))) {
                        r3.A08.A0e(str);
                        r3.A06 = true;
                        locale = AbstractC27291Gt.A09(str);
                        r3.A04 = locale;
                        Log.i(C12960it.A0d(locale.getDisplayLanguage(Locale.US), C12960it.A0k("whatsapplocale/saveandapplyforcedlanguage/setting language ")));
                        Locale.setDefault(r3.A04);
                        r3.A0P();
                        r3.A0M();
                        C15610nY r1 = this.A07;
                        r1.A08.clear();
                        r1.A09.clear();
                        finish();
                    }
                }
                r3.A08.A0I();
                r3.A06 = false;
                locale = r3.A05;
                r3.A04 = locale;
                Log.i(C12960it.A0d(locale.getDisplayLanguage(Locale.US), C12960it.A0k("whatsapplocale/saveandapplyforcedlanguage/setting language ")));
                Locale.setDefault(r3.A04);
                r3.A0P();
                r3.A0M();
                C15610nY r1 = this.A07;
                r1.A08.clear();
                r1.A09.clear();
                finish();
            } else {
                return;
            }
            startActivity(getIntent());
            return;
        }
        C12970iu.A1D(C12960it.A08(((ActivityC13810kN) this).A09), "interface_font_size", String.valueOf(Integer.valueOf(this.A0N[i2]).intValue()));
        this.A01.setText(this.A0M[i2]);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == 0 && intent != null) {
            if (intent.getBooleanExtra("oom", false)) {
                Log.e("conversation/activityres/oom-error");
                Ado(R.string.error_out_of_memory);
            }
            if (intent.getBooleanExtra("no-space", false)) {
                Log.e("conversation/activityres/no-space");
                Ado(R.string.error_no_disc_space);
            }
            if (intent.getBooleanExtra("io-error", false)) {
                Log.e("conversation/activityres/fail/load-image");
                Ado(R.string.error_load_image);
            }
        }
        super.onActivityResult(i, i2, intent);
        Iterator it = this.A0S.iterator();
        while (it.hasNext() && !((AbstractC42541vN) it.next()).ALt(intent, i, i2)) {
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        if (!this.A0L) {
            super.onConfigurationChanged(configuration);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0169, code lost:
        if (r2 == 2) goto L_0x016b;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r17) {
        /*
        // Method dump skipped, instructions count: 447
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.SettingsChat.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 600) {
            return A02(this);
        }
        if (i != 602) {
            return super.onCreateDialog(i);
        }
        return A03(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        C17050qB r0 = this.A09;
        AbstractC453621g r1 = this.A0R;
        if (r1 != null) {
            r0.A06.remove(r1);
        }
        super.onPause();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C17050qB r0 = this.A09;
        AbstractC453621g r1 = this.A0R;
        if (r1 != null) {
            r0.A06.add(r1);
        }
        A2f();
        if (!AnonymousClass3J4.A03(((ActivityC13810kN) this).A08, this.A0D)) {
            C15570nT r02 = ((ActivityC13790kL) this).A01;
            r02.A08();
            Me me = r02.A00;
            if (me != null) {
                AnonymousClass018 r03 = ((ActivityC13830kP) this).A01;
                AnonymousClass3H4 r5 = new AnonymousClass3H4(me.cc, me.number, r03.A05, r03.A04);
                if (r5.A01 != 0) {
                    if (!r5.A03.equals("US") || ((ActivityC13810kN) this).A0C.A07(292)) {
                        this.A0G.setVisibility(0);
                        String[] strArr = r5.A04;
                        strArr[0] = C12960it.A0X(this, strArr[0], C12970iu.A1b(), 0, R.string.device_default_language_with_placeholder);
                        String[] strArr2 = r5.A04;
                        this.A0O = strArr2;
                        this.A0P = r5.A05;
                        int i = r5.A00;
                        this.A00 = i;
                        this.A0G.setSubText(strArr2[i]);
                        C12960it.A12(this.A0G, this, 36);
                        String str = r5.A02;
                        if (!str.isEmpty()) {
                            AnonymousClass308 r12 = new AnonymousClass308();
                            r12.A00 = str;
                            this.A0C.A07(r12);
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
        }
        this.A0G.setVisibility(8);
    }

    public final void scrollToArchiveSettingIfNeeded(View view) {
        Intent intent = getIntent();
        if (intent != null && "archived_chats".equals(intent.getStringExtra("scroll_to_setting"))) {
            view.getParent().requestChildFocus(view, view);
        }
    }
}
