package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC15850o0;
import X.AnonymousClass004;
import X.AnonymousClass01d;
import X.AnonymousClass03X;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.AnonymousClass2QN;
import X.C12960it;
import X.C12980iv;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WallpaperImagePreview extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass01d A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;
    public final Path A04;
    public final RectF A05;
    public final float[] A06;

    @Override // android.widget.ImageView
    public void setScaleType(ImageView.ScaleType scaleType) {
    }

    public WallpaperImagePreview(Context context) {
        this(context, null);
    }

    public WallpaperImagePreview(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WallpaperImagePreview(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12960it.A0Q(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A04 = new Path();
        this.A05 = C12980iv.A0K();
        float[] fArr = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        this.A06 = fArr;
        this.A03 = false;
        super.setScaleType(ImageView.ScaleType.MATRIX);
        float dimensionPixelSize = (float) context.getResources().getDimensionPixelSize(R.dimen.wallpaper_current_preview_corner_radius);
        fArr[4] = dimensionPixelSize;
        fArr[5] = dimensionPixelSize;
        fArr[6] = dimensionPixelSize;
        fArr[7] = dimensionPixelSize;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0S);
        try {
            this.A03 = obtainStyledAttributes.getBoolean(0, false);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03) {
            canvas.clipPath(this.A04);
        }
        super.onDraw(canvas);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A03) {
            Path path = this.A04;
            path.reset();
            RectF rectF = this.A05;
            rectF.right = (float) getMeasuredWidth();
            rectF.bottom = (float) getMeasuredHeight();
            path.addRoundRect(rectF, this.A06, Path.Direction.CW);
            path.close();
        }
    }

    @Override // android.widget.ImageView
    public boolean setFrame(int i, int i2, int i3, int i4) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            float intrinsicWidth = (float) drawable.getIntrinsicWidth();
            float intrinsicHeight = (float) drawable.getIntrinsicHeight();
            Point A00 = AbstractC15850o0.A00(getContext());
            float f = (float) A00.x;
            float f2 = (float) A00.y;
            float f3 = ((float) (i3 - i)) / f;
            float max = Math.max(f / intrinsicWidth, f2 / intrinsicHeight) * f3;
            Matrix imageMatrix = getImageMatrix();
            float f4 = 0.0f;
            imageMatrix.setScale(max, max, 0.0f, 0.0f);
            float f5 = ((f * f3) - (intrinsicWidth * max)) / 2.0f;
            float f6 = ((f2 * f3) - (intrinsicHeight * max)) / 2.0f;
            if (getResources().getConfiguration().orientation != 2) {
                f4 = f6;
            }
            imageMatrix.postTranslate(f5, f4);
            setImageMatrix(imageMatrix);
        }
        return super.setFrame(i, i2, i3, i4);
    }

    public void setRoundBottomCorners(boolean z) {
        this.A03 = z;
        invalidate();
    }
}
