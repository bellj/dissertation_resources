package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C103484qp;
import X.C14330lG;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C41691tw;
import X.C51422Uq;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class SolidColorWallpaper extends ActivityC13810kN {
    public static final int[] A04 = {R.string.color_name_cruise_green, R.string.color_name_scandal_green, R.string.color_name_monte_carlo_green, R.string.color_name_hawkes_blue, R.string.color_name_downy_green, R.string.color_name_seagull_blue, R.string.color_name_quartz_blue, R.string.color_name_very_light_gray, R.string.color_name_orinoco_green, R.string.color_name_tusk_green, R.string.color_name_cape_honey_yellow, R.string.color_name_caramel_yellow, R.string.color_name_rose_bud, R.string.color_name_bittersweet_orange, R.string.color_name_radical_red, R.string.color_name_mandarian_orange, R.string.color_name_flamingo_red, R.string.color_name_buccaneer_brown, R.string.color_name_breaker_bay, R.string.color_name_pelorus_blue, R.string.color_name_tory_blue, R.string.color_name_fjord_gray, R.string.color_name_cinder_black, R.string.color_name_midnight_express, R.string.color_name_solitude_gray, R.string.color_name_canary_yellow, R.string.color_name_brook_green};
    public AnonymousClass018 A00;
    public boolean A01;
    public int[] A02;
    public int[] A03;

    public SolidColorWallpaper() {
        this(0);
    }

    public SolidColorWallpaper(int i) {
        this.A01 = false;
        A0R(new C103484qp(this));
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            this.A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            this.A0B = (AnonymousClass19M) r1.A6R.get();
            this.A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            this.A0D = (C18810t5) r1.AMu.get();
            this.A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            this.A00 = (AnonymousClass018) r1.ANb.get();
        }
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            if (intent == null || !intent.hasExtra("wallpaper_color_file")) {
                setResult(0, null);
            } else {
                setResult(-1, intent);
            }
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        setTitle(R.string.solid_color_wallpaper);
        setContentView(R.layout.wallpaper_grid_preview);
        A1e((Toolbar) findViewById(R.id.toolbar));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass00T.A05(this, R.id.separator).setVisibility(8);
        }
        AbsListView absListView = (AbsListView) AnonymousClass00T.A05(this, R.id.color_grid);
        int[] intArray = getResources().getIntArray(R.array.solid_color_wallpaperv2_colors);
        int length = intArray.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = i;
        }
        Pair pair = new Pair(intArray, iArr);
        this.A02 = (int[]) pair.first;
        this.A03 = (int[]) pair.second;
        absListView.setAdapter((ListAdapter) new C51422Uq(this, this));
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0, null);
        finish();
        return true;
    }
}
