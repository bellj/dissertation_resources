package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC15850o0;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass35C;
import X.C12960it;
import X.C20320vZ;
import X.C22190yg;
import android.net.Uri;
import android.view.MenuItem;
import com.whatsapp.mediaview.PhotoView;

/* loaded from: classes2.dex */
public class GalleryWallpaperPreview extends AnonymousClass35C {
    public int A00;
    public Uri A01;
    public PhotoView A02;
    public C20320vZ A03;
    public AbstractC15850o0 A04;
    public C22190yg A05;
    public boolean A06;

    public GalleryWallpaperPreview() {
        this(0);
    }

    public GalleryWallpaperPreview(int i) {
        this.A06 = false;
        ActivityC13830kP.A1P(this, 117);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass35C) this).A01 = C12960it.A0O(A1M);
            ((AnonymousClass35C) this).A02 = C12960it.A0P(A1M);
            this.A05 = (C22190yg) A1M.AB6.get();
            this.A03 = (C20320vZ) A1M.A7A.get();
            this.A04 = (AbstractC15850o0) A1M.ANA.get();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x01a3, code lost:
        if (r5 != null) goto L_0x01a5;
     */
    @Override // X.AnonymousClass35C, X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r19) {
        /*
        // Method dump skipped, instructions count: 545
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.chat.wallpaper.GalleryWallpaperPreview.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0);
        finish();
        return true;
    }
}
