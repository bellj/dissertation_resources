package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC13890kV;
import X.AnonymousClass004;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass15O;
import X.AnonymousClass1IS;
import X.AnonymousClass1OY;
import X.AnonymousClass2P6;
import X.AnonymousClass2P7;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C15570nT;
import X.C20320vZ;
import X.C28861Ph;
import X.C60842yj;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WallpaperMockChatView extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public C15570nT A05;
    public AnonymousClass1OY A06;
    public AnonymousClass1OY A07;
    public C14830m7 A08;
    public C20320vZ A09;
    public AnonymousClass2P7 A0A;
    public boolean A0B;

    public WallpaperMockChatView(Context context) {
        this(context, null);
    }

    public WallpaperMockChatView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WallpaperMockChatView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A08 = C12980iv.A0b(A00);
            this.A05 = C12970iu.A0S(A00);
            this.A09 = (C20320vZ) A00.A7A.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0A;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0A = r0;
        }
        return r0.generatedComponent();
    }

    public AnonymousClass1OY getOutgoingRow() {
        return this.A07;
    }

    public void setMessages(String str, String str2, AbstractC13890kV r13) {
        Context context = getContext();
        C20320vZ r5 = this.A09;
        C14830m7 r8 = this.A08;
        C15570nT r9 = this.A05;
        C28861Ph r6 = (C28861Ph) r5.A01(new AnonymousClass1IS(null, AnonymousClass15O.A00(r9, r8, false), false), (byte) 0, r8.A00());
        r6.A0l(str);
        r9.A08();
        C28861Ph r52 = (C28861Ph) r5.A01(new AnonymousClass1IS(r9.A05, AnonymousClass15O.A00(r9, r8, false), true), (byte) 0, r8.A00());
        r52.A0I = r8.A00();
        r52.A0Y(5);
        r52.A0l(str2);
        setBackgroundResource(0);
        setOrientation(1);
        C60842yj r0 = new C60842yj(context, r13, r6);
        this.A06 = r0;
        r0.A1G(true);
        this.A06.setEnabled(false);
        this.A00 = AnonymousClass028.A0D(this.A06, R.id.date_wrapper);
        this.A03 = C12960it.A0I(this.A06, R.id.message_text);
        this.A02 = C12960it.A0I(this.A06, R.id.conversation_row_date_divider);
        C60842yj r02 = new C60842yj(context, r13, r52);
        this.A07 = r02;
        r02.A1G(false);
        this.A07.setEnabled(false);
        this.A01 = AnonymousClass028.A0D(this.A07, R.id.date_wrapper);
        this.A04 = C12960it.A0I(this.A07, R.id.message_text);
        addView(this.A06);
        addView(this.A07);
    }
}
