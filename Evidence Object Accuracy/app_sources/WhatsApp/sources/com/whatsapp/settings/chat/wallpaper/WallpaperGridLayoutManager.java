package com.whatsapp.settings.chat.wallpaper;

import X.AnonymousClass02M;
import X.C74673iU;
import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;

/* loaded from: classes3.dex */
public class WallpaperGridLayoutManager extends GridLayoutManager {
    public Context A00;
    public final AnonymousClass02M A01;

    public WallpaperGridLayoutManager(Context context, AnonymousClass02M r3) {
        super(4);
        this.A00 = context;
        this.A01 = r3;
        ((GridLayoutManager) this).A01 = new C74673iU(this);
    }
}
