package com.whatsapp.settings.chat.wallpaper;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass35C;
import X.AnonymousClass35J;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.ViewTreeObserver$OnPreDrawListenerC66493Nu;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.collections.MarginCorrectedViewPager;

/* loaded from: classes2.dex */
public class SolidColorWallpaperPreview extends AnonymousClass35C {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public CheckBox A08;
    public MarginCorrectedViewPager A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public int[] A0D;
    public int[] A0E;

    public SolidColorWallpaperPreview() {
        this(0);
        this.A0C = false;
        this.A0A = false;
    }

    public SolidColorWallpaperPreview(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 118);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass35C) this).A01 = C12960it.A0O(A1M);
            ((AnonymousClass35C) this).A02 = C12960it.A0P(A1M);
        }
    }

    public final void A2h(int i) {
        int i2 = getResources().getIntArray(R.array.solid_color_wallpaperv2_colors)[i];
        int i3 = 0;
        int i4 = 0;
        while (true) {
            int[] iArr = this.A0D;
            if (i4 >= iArr.length) {
                break;
            } else if (iArr[i4] == i2) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        this.A09.setCurrentItem(i3);
    }

    public final void A2i(int i) {
        int i2;
        int i3;
        if (this.A0A) {
            this.A0C = true;
            this.A09.setScrollEnabled(false);
            DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
            if (i != getIntent().getIntExtra("scw_preview_color", -1)) {
                this.A02 = 0;
                this.A03 = 0;
                i2 = this.A09.getWidth() >> 1;
                i3 = this.A09.getWidth() >> 1;
            } else {
                i2 = 0;
                i3 = 0;
            }
            this.A09.setPivotX((float) i2);
            this.A09.setPivotY((float) i3);
            this.A05.setBackgroundColor(0);
            this.A09.animate().setDuration(250).alpha(0.0f).scaleX(this.A00).scaleY(this.A01).translationX((float) this.A02).translationY((float) this.A03).setInterpolator(decelerateInterpolator).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 14));
            C12980iv.A0Q(this.A04, 0.0f).setInterpolator(decelerateInterpolator);
            C12980iv.A0Q(this.A06, 0.0f).setInterpolator(decelerateInterpolator);
            return;
        }
        finish();
    }

    @Override // android.app.Activity
    public void finish() {
        super.finish();
        if (this.A0A && this.A0C) {
            overridePendingTransition(0, 0);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        setResult(0, null);
        A2i(this.A09.getCurrentItem());
    }

    @Override // X.AnonymousClass35C, X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A05 = AnonymousClass00T.A05(this, R.id.wallpaper_preview_container);
        this.A04 = AnonymousClass00T.A05(this, R.id.appbar);
        this.A07 = AnonymousClass00T.A05(this, R.id.transition_view);
        int[] intArray = getResources().getIntArray(R.array.solid_color_wallpaperv2_colors);
        int length = intArray.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = i;
        }
        Pair A0L = C12990iw.A0L(intArray, iArr);
        this.A0D = (int[]) A0L.first;
        this.A0E = (int[]) A0L.second;
        MarginCorrectedViewPager marginCorrectedViewPager = (MarginCorrectedViewPager) AnonymousClass00T.A05(this, R.id.wallpaper_preview);
        this.A09 = marginCorrectedViewPager;
        marginCorrectedViewPager.setSaveEnabled(false);
        CheckBox checkBox = (CheckBox) AnonymousClass00T.A05(this, R.id.color_wallpaper_add_doodles);
        this.A08 = checkBox;
        checkBox.setVisibility(0);
        this.A08.setText(R.string.wallpaper_solid_color_add_whatsapp_doodle);
        AnonymousClass35J r3 = new AnonymousClass35J(this, this);
        r3.A00 = this.A08.isChecked();
        this.A08.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.4pB
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                AnonymousClass35J r0 = AnonymousClass35J.this;
                r0.A00 = z;
                r0.A06();
            }
        });
        this.A09.setAdapter(r3);
        this.A09.setPageMargin((int) (C12960it.A01(this) * 15.0f));
        View A05 = AnonymousClass00T.A05(this, R.id.control_holder);
        this.A06 = A05;
        A05.setBackground(C12970iu.A0C(this, R.drawable.wallpaper_color_confirmation_background));
        C12960it.A11(AnonymousClass00T.A05(this, R.id.cancel_button), this, 17);
        A2h(getIntent().getIntExtra("scw_preview_color", 0));
        this.A0C = false;
        boolean booleanExtra = getIntent().getBooleanExtra("wallpaper_preview_intent_extra_animate", false);
        this.A0A = booleanExtra;
        if (booleanExtra) {
            overridePendingTransition(0, 0);
            Intent intent = getIntent();
            this.A0C = true;
            this.A09.setScrollEnabled(false);
            this.A07.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66493Nu(this, intent.getIntExtra("wallpaper_preview_intent_extra_x", 0), intent.getIntExtra("wallpaper_preview_intent_extra_y", 0), intent.getIntExtra("wallpaper_preview_intent_extra_width", 0), intent.getIntExtra("wallpaper_preview_intent_extra_height", 0)));
            return;
        }
        this.A05.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0, null);
        A2i(this.A09.getCurrentItem());
        return true;
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        A2h(bundle.getInt("selected_index"));
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("selected_index", this.A0E[this.A09.getCurrentItem()]);
    }
}
