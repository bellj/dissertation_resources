package com.whatsapp.settings.chat.wallpaper;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2JP;
import X.AnonymousClass35C;
import X.C12960it;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class DefaultWallpaperPreview extends AnonymousClass35C {
    public boolean A00;

    public DefaultWallpaperPreview() {
        this(0);
    }

    public DefaultWallpaperPreview(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 116);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass35C) this).A01 = C12960it.A0O(A1M);
            ((AnonymousClass35C) this).A02 = C12960it.A0P(A1M);
        }
    }

    @Override // X.AnonymousClass35C, X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((ImageView) AnonymousClass00T.A05(this, R.id.wallpaper_preview_default_view)).setImageDrawable(AnonymousClass2JP.A00(this, getResources()));
        ((WallpaperMockChatView) AnonymousClass00T.A05(this, R.id.wallpaper_preview_default_chat_view)).setMessages(getString(R.string.wallpaper_bubble_this_is_the_default_whatsapp_wallpaper), A2f(), null);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0);
        finish();
        return true;
    }
}
