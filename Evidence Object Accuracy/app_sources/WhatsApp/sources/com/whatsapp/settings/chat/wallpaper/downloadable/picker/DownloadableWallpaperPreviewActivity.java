package com.whatsapp.settings.chat.wallpaper.downloadable.picker;

import X.AbstractC14440lR;
import X.AbstractC16350or;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2VP;
import X.AnonymousClass35C;
import X.AnonymousClass35K;
import X.AnonymousClass3S5;
import X.AnonymousClass4OK;
import X.C12960it;
import X.C12970iu;
import X.C21990yJ;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.collections.MarginCorrectedViewPager;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class DownloadableWallpaperPreviewActivity extends AnonymousClass35C {
    public Resources A00;
    public MarginCorrectedViewPager A01;
    public C21990yJ A02;
    public AnonymousClass35K A03;
    public AnonymousClass4OK A04;
    public List A05;
    public List A06;
    public boolean A07;
    public final Set A08;

    public DownloadableWallpaperPreviewActivity() {
        this(0);
        this.A08 = C12970iu.A12();
        this.A04 = new AnonymousClass4OK(this);
    }

    public DownloadableWallpaperPreviewActivity(int i) {
        this.A07 = false;
        ActivityC13830kP.A1P(this, 121);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass35C) this).A01 = C12960it.A0O(A1M);
            ((AnonymousClass35C) this).A02 = C12960it.A0P(A1M);
            this.A02 = (C21990yJ) A1M.A6C.get();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        setResult(0, null);
        finish();
    }

    @Override // X.AnonymousClass35C, X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C12970iu.A18(this, AnonymousClass00T.A05(this, R.id.wallpaper_preview_container), R.color.primary_surface);
        ((AnonymousClass35C) this).A00.setEnabled(false);
        try {
            this.A00 = getPackageManager().getResourcesForApplication("com.whatsapp.wallpaper");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DownloadableWallpaperPreviewActivity/com.whatsapp.wallpaper could not be found.", e);
        }
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("THUMBNAIL_URIS_KEY");
        AnonymousClass009.A05(parcelableArrayListExtra);
        this.A05 = parcelableArrayListExtra;
        ArrayList<Integer> integerArrayListExtra = getIntent().getIntegerArrayListExtra("WHATSAPP_THUMBNAIL_RES_KEY");
        this.A06 = getIntent().getIntegerArrayListExtra("WHATSAPP_FULL_RES_KEY");
        this.A01 = (MarginCorrectedViewPager) AnonymousClass00T.A05(this, R.id.wallpaper_preview);
        AbstractC14440lR r9 = ((ActivityC13830kP) this).A05;
        C21990yJ r7 = this.A02;
        AnonymousClass35K r3 = new AnonymousClass35K(this, this.A00, ((AnonymousClass2VP) this).A00, r7, this.A04, r9, this.A05, integerArrayListExtra, this.A06, ((AnonymousClass2VP) this).A01);
        this.A03 = r3;
        this.A01.setAdapter(r3);
        this.A01.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.downloadable_wallpaper_pager_margin));
        this.A01.A0G(new AnonymousClass3S5(this));
        this.A01.setCurrentItem(getIntent().getIntExtra("STARTING_POSITION_KEY", 0));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        Iterator A0o = C12960it.A0o(this.A03.A07);
        while (A0o.hasNext()) {
            ((AbstractC16350or) A0o.next()).A03(true);
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0, null);
        finish();
        return true;
    }
}
