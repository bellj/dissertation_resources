package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC005102i;
import X.AbstractC116685Wk;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC15710nm;
import X.AbstractC15850o0;
import X.AbstractC16350or;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass29H;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2VJ;
import X.AnonymousClass2VK;
import X.AnonymousClass3UV;
import X.AnonymousClass4NU;
import X.AnonymousClass55Q;
import X.C103494qq;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15380n4;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16590pI;
import X.C18470sV;
import X.C18640sm;
import X.C18720su;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C22670zS;
import X.C249317l;
import X.C252218o;
import X.C252718t;
import X.C252818u;
import X.C41691tw;
import X.C54612h0;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import com.whatsapp.backup.google.PromptDialogFragment;
import com.whatsapp.settings.chat.wallpaper.WallpaperCategoriesActivity;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class WallpaperCategoriesActivity extends ActivityC13790kL implements AnonymousClass29H {
    public C18720su A00;
    public AbstractC116685Wk A01;
    public AnonymousClass3UV A02;
    public C16590pI A03;
    public C15890o4 A04;
    public AbstractC14640lm A05;
    public AbstractC15850o0 A06;
    public AnonymousClass2VJ A07;
    public boolean A08;
    public boolean A09;
    public final AnonymousClass4NU A0A;

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
    }

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
    }

    public WallpaperCategoriesActivity() {
        this(0);
        this.A0A = new AnonymousClass4NU();
        this.A05 = null;
    }

    public WallpaperCategoriesActivity(int i) {
        this.A09 = false;
        A0R(new C103494qq(this));
    }

    public static /* synthetic */ void A02(WallpaperCategoriesActivity wallpaperCategoriesActivity, int i, boolean z) {
        Intent className;
        AbstractC14640lm r3;
        Intent intent;
        String packageName;
        String str;
        boolean z2 = true;
        if (i == 0 || i == 1) {
            AbstractC14640lm r32 = wallpaperCategoriesActivity.A05;
            if (i != 0) {
                z2 = false;
            }
            className = new Intent().setClassName(wallpaperCategoriesActivity.getPackageName(), "com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPickerActivity");
            className.putExtra("chat_jid", C15380n4.A03(r32));
            className.putExtra("is_using_global_wallpaper", z);
            className.putExtra("IS_BRIGHT_KEY", z2);
        } else {
            if (i == 2) {
                r3 = wallpaperCategoriesActivity.A05;
                intent = new Intent();
                packageName = wallpaperCategoriesActivity.getPackageName();
                str = "com.whatsapp.settings.chat.wallpaper.SolidColorWallpaper";
            } else if (i == 3) {
                r3 = wallpaperCategoriesActivity.A05;
                intent = new Intent();
                packageName = wallpaperCategoriesActivity.getPackageName();
                str = "com.whatsapp.gallerypicker.GalleryPickerLauncher";
            } else if (i == 4) {
                Bundle bundle = new Bundle();
                bundle.putInt("dialog_id", 112);
                bundle.putCharSequence("message", wallpaperCategoriesActivity.getString(R.string.wallpaper_remove_custom_wallpaper_dialog_title));
                bundle.putString("positive_button", wallpaperCategoriesActivity.getString(R.string.wallpaper_remove_custom_wallpaper_dialog_remove_prompt));
                bundle.putString("negative_button", wallpaperCategoriesActivity.getString(R.string.cancel));
                PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
                promptDialogFragment.A0U(bundle);
                wallpaperCategoriesActivity.Adm(promptDialogFragment);
                return;
            } else if (i == 5) {
                r3 = wallpaperCategoriesActivity.A05;
                intent = new Intent();
                packageName = wallpaperCategoriesActivity.getPackageName();
                str = "com.whatsapp.settings.chat.wallpaper.DefaultWallpaperPreview";
            } else {
                return;
            }
            className = intent.setClassName(packageName, str);
            className.putExtra("chat_jid", C15380n4.A03(r3));
            className.putExtra("is_using_global_wallpaper", z);
        }
        wallpaperCategoriesActivity.startActivityForResult(className, 17);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A00 = (C18720su) r1.A2c.get();
            this.A03 = (C16590pI) r1.AMg.get();
            this.A06 = (AbstractC15850o0) r1.ANA.get();
            this.A04 = (C15890o4) r1.AN1.get();
        }
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        if (i == 112 || i == 113) {
            AbstractC15850o0 r3 = this.A06;
            if (i != 113) {
                AbstractC14640lm r1 = this.A05;
                if (r3 instanceof C252218o) {
                    ((C252218o) r3).A0F(this, r1, null);
                }
                setResult(-1);
                finish();
            } else if (r3 instanceof C252218o) {
                C252218o r32 = (C252218o) r3;
                r32.A05.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(r32, 0));
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != 0) {
            setResult(i2);
            if (this.A02.ALt(intent, i, i2)) {
                finish();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        AnonymousClass016 r1;
        super.onCreate(bundle);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(1);
        }
        setContentView(R.layout.activity_wallpaper_categories);
        C14900mE r14 = ((ActivityC13810kN) this).A05;
        AnonymousClass55Q r15 = new AnonymousClass55Q(r14);
        this.A01 = r15;
        this.A02 = new AnonymousClass3UV(this, this, r14, r15, this.A0A, ((ActivityC13810kN) this).A08, this.A06);
        this.A05 = AbstractC14640lm.A01(getIntent().getStringExtra("chat_jid"));
        boolean booleanExtra = getIntent().getBooleanExtra("is_using_global_wallpaper", false);
        A1e((Toolbar) AnonymousClass00T.A05(this, R.id.wallpaper_categories_toolbar));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        if (this.A05 == null || booleanExtra) {
            boolean A08 = C41691tw.A08(this);
            i = R.string.wallpaper_light_theme_header;
            if (A08) {
                i = R.string.wallpaper_dark_theme_header;
            }
        } else {
            i = R.string.wallpaper_custom_wallpaper_header;
        }
        setTitle(i);
        this.A05 = AbstractC14640lm.A01(getIntent().getStringExtra("chat_jid"));
        this.A08 = this.A04.A07();
        AbstractC15850o0 r12 = this.A06;
        if (!(r12 instanceof C252218o)) {
            r1 = null;
        } else {
            r1 = ((C252218o) r12).A00;
        }
        AnonymousClass009.A05(r1);
        r1.A05(this, new AnonymousClass02B() { // from class: X.4tn
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                WallpaperCategoriesActivity wallpaperCategoriesActivity = WallpaperCategoriesActivity.this;
                int A05 = C12960it.A05(obj);
                if (A05 == 1) {
                    wallpaperCategoriesActivity.A2C(R.string.wallpaper_reset);
                } else if (A05 != 2) {
                    return;
                }
                wallpaperCategoriesActivity.A06.A08();
                wallpaperCategoriesActivity.setResult(-1);
                wallpaperCategoriesActivity.finish();
            }
        });
        ArrayList arrayList = new ArrayList();
        arrayList.add(0);
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(5);
        boolean z = this.A06.A06(this, this.A05).A03;
        if (!z) {
            arrayList.add(4);
        }
        RecyclerView recyclerView = (RecyclerView) AnonymousClass00T.A05(this, R.id.categories);
        AnonymousClass2VK r9 = new AnonymousClass2VK(this, z);
        Handler handler = new Handler(Looper.getMainLooper());
        AnonymousClass2VJ r3 = new AnonymousClass2VJ(getContentResolver(), handler, this.A00, this.A03, ((ActivityC13790kL) this).A09, r9, ((ActivityC13830kP) this).A05, arrayList);
        this.A07 = r3;
        recyclerView.setLayoutManager(new WallpaperGridLayoutManager(this, r3));
        recyclerView.A0l(new C54612h0(((ActivityC13830kP) this).A01, getResources().getDimensionPixelSize(R.dimen.wallpaper_category_view_padding)));
        recyclerView.setAdapter(this.A07);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.A05 == null) {
            menu.add(0, 999, 0, R.string.wallpaper_reset_wallpapers_overflow_menu_option).setShowAsAction(0);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        for (AbstractC16350or r1 : this.A07.A09.values()) {
            r1.A03(true);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 999) {
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 113);
            bundle.putCharSequence("message", getString(R.string.wallpaper_reset_wallpapers_dialog_description));
            bundle.putString("positive_button", getString(R.string.wallpaper_reset_wallpapers_dialog_reset_button));
            bundle.putString("negative_button", getString(R.string.cancel));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            Adm(promptDialogFragment);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A08 != this.A04.A07()) {
            this.A08 = this.A04.A07();
            this.A07.A02();
        }
    }
}
