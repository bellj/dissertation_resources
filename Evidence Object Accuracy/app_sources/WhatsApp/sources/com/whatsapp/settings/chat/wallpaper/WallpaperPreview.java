package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC16350or;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.AnonymousClass2YT;
import X.AnonymousClass35C;
import X.AnonymousClass35I;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.ViewTreeObserver$OnPreDrawListenerC66503Nv;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.R;
import com.whatsapp.collections.MarginCorrectedViewPager;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class WallpaperPreview extends AnonymousClass35C {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public Resources A04;
    public View A05;
    public View A06;
    public View A07;
    public View A08;
    public MarginCorrectedViewPager A09;
    public List A0A;
    public List A0B;
    public boolean A0C;
    public boolean A0D;
    public final Map A0E;

    public WallpaperPreview() {
        this(0);
        this.A04 = null;
        this.A0E = C12970iu.A11();
    }

    public WallpaperPreview(int i) {
        this.A0C = false;
        ActivityC13830kP.A1P(this, 120);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            ((AnonymousClass35C) this).A01 = C12960it.A0O(A1M);
            ((AnonymousClass35C) this).A02 = C12960it.A0P(A1M);
        }
    }

    public final void A2h(int i) {
        int i2;
        int i3;
        this.A0D = true;
        MarginCorrectedViewPager marginCorrectedViewPager = this.A09;
        marginCorrectedViewPager.setScrollEnabled(false);
        View findViewWithTag = marginCorrectedViewPager.findViewWithTag(C12960it.A0W(i, "chatlayout-"));
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        if (i != getIntent().getIntExtra("wallpaper_preview_intent_starting_pos", -1)) {
            this.A02 = 0;
            this.A03 = 0;
            i2 = this.A09.getWidth() >> 1;
            i3 = this.A09.getWidth() >> 1;
        } else {
            i2 = 0;
            i3 = 0;
        }
        this.A09.setPivotX((float) i2);
        this.A09.setPivotY((float) i3);
        int A01 = (int) (C12960it.A01(this) * 20.0f);
        this.A06.setBackgroundColor(0);
        if (findViewWithTag != null) {
            C12980iv.A0Q(findViewWithTag, 0.0f).translationY((float) A01).setInterpolator(decelerateInterpolator);
        }
        C12980iv.A0Q(this.A07, 0.0f).setInterpolator(decelerateInterpolator).setListener(new AnonymousClass2YT(decelerateInterpolator, this));
    }

    @Override // android.app.Activity
    public void finish() {
        super.finish();
        if (this.A0D) {
            overridePendingTransition(0, 0);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        setResult(0, null);
        A2h(this.A09.getCurrentItem());
    }

    @Override // X.AnonymousClass35C, X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            this.A04 = getPackageManager().getResourcesForApplication("com.whatsapp.wallpaper");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("wallpaperpreview/com.whatsapp.wallpaper could not be found.", e);
        }
        this.A06 = findViewById(R.id.wallpaper_preview_container);
        this.A05 = findViewById(R.id.appbar);
        this.A08 = findViewById(R.id.transition_view);
        this.A0A = getIntent().getIntegerArrayListExtra("wallpaper_preview_intent_image_res_ids");
        this.A0B = getIntent().getIntegerArrayListExtra("wallpaper_preview_intent_thumb_res_ids");
        this.A09 = (MarginCorrectedViewPager) findViewById(R.id.wallpaper_preview);
        this.A09.setAdapter(new AnonymousClass35I(this, this.A04, this));
        this.A09.setPageMargin((int) (C12960it.A01(this) * 15.0f));
        this.A07 = findViewById(R.id.control_holder);
        C12960it.A11(findViewById(R.id.cancel_button), this, 18);
        this.A09.setCurrentItem(getIntent().getIntExtra("wallpaper_preview_intent_starting_pos", 0));
        this.A0D = false;
        overridePendingTransition(0, 0);
        Intent intent = getIntent();
        this.A0D = true;
        this.A09.setScrollEnabled(false);
        this.A08.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66503Nv(this, intent.getIntExtra("wallpaper_preview_intent_extra_x", 0), intent.getIntExtra("wallpaper_preview_intent_extra_y", 0), intent.getIntExtra("wallpaper_preview_intent_extra_width", 0), intent.getIntExtra("wallpaper_preview_intent_extra_height", 0)));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        Iterator A0n = C12960it.A0n(this.A0E);
        while (A0n.hasNext()) {
            ((AbstractC16350or) C12970iu.A15(A0n).getValue()).A03(true);
        }
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0, null);
        A2h(this.A09.getCurrentItem());
        return true;
    }
}
