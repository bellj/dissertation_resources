package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC005102i;
import X.AbstractC14640lm;
import X.AbstractC15850o0;
import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass028;
import X.AnonymousClass130;
import X.AnonymousClass1J1;
import X.AnonymousClass2FL;
import X.AnonymousClass2GE;
import X.AnonymousClass2JR;
import X.AnonymousClass3G9;
import X.AnonymousClass3OS;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C15370n3;
import X.C15550nR;
import X.C15610nY;
import X.C21270x9;
import X.C27531Hw;
import X.C41691tw;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;

/* loaded from: classes2.dex */
public class WallpaperCurrentPreviewActivity extends ActivityC13790kL {
    public View A00;
    public SeekBar A01;
    public AbstractC005102i A02;
    public AnonymousClass130 A03;
    public C15550nR A04;
    public C15610nY A05;
    public AnonymousClass1J1 A06;
    public C21270x9 A07;
    public AbstractC15850o0 A08;
    public AnonymousClass2JR A09;
    public WallpaperImagePreview A0A;
    public boolean A0B;

    public WallpaperCurrentPreviewActivity() {
        this(0);
    }

    public WallpaperCurrentPreviewActivity(int i) {
        this.A0B = false;
        ActivityC13830kP.A1P(this, 119);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.app.Activity r2, android.content.Intent r3, X.AnonymousClass2JR r4) {
        /*
            java.lang.String r0 = "chat_jid"
            java.lang.String r0 = r3.getStringExtra(r0)
            X.0lm r0 = X.AbstractC14640lm.A01(r0)
            if (r0 == 0) goto L_0x001c
            if (r4 == 0) goto L_0x0015
            boolean r0 = r4.A03
            r1 = 2131893084(0x7f121b5c, float:1.9420935E38)
            if (r0 == 0) goto L_0x0018
        L_0x0015:
            r1 = 2131893082(0x7f121b5a, float:1.942093E38)
        L_0x0018:
            r2.setTitle(r1)
            return
        L_0x001c:
            boolean r0 = X.C41691tw.A08(r2)
            r1 = 2131893096(0x7f121b68, float:1.9420959E38)
            if (r0 == 0) goto L_0x0018
            r1 = 2131893085(0x7f121b5d, float:1.9420937E38)
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.chat.wallpaper.WallpaperCurrentPreviewActivity.A02(android.app.Activity, android.content.Intent, X.2JR):void");
    }

    public static void A03(View view, float f) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) (((float) layoutParams.width) * f);
        layoutParams.height = (int) (((float) layoutParams.height) * f);
        view.setLayoutParams(layoutParams);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0B) {
            this.A0B = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A07 = C12970iu.A0W(A1M);
            this.A03 = C12990iw.A0Y(A1M);
            this.A04 = C12960it.A0O(A1M);
            this.A05 = C12960it.A0P(A1M);
            this.A08 = (AbstractC15850o0) A1M.ANA.get();
        }
    }

    public final void A2e(AbstractC14640lm r3) {
        Integer num;
        this.A09 = this.A08.A06(this, r3);
        A02(this, getIntent(), this.A09);
        Drawable A03 = this.A08.A03(this.A09);
        if (A03 != null) {
            this.A0A.setImageDrawable(A03);
        }
        if (this.A01.getVisibility() == 0) {
            AnonymousClass2JR r0 = this.A09;
            int i = 0;
            if (!(r0 == null || (num = r0.A01) == null)) {
                i = num.intValue();
            }
            this.A01.setProgress(i);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 199 && i2 == -1) {
            A2e(AbstractC14640lm.A01(getIntent().getStringExtra("chat_jid")));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_wallpaper_current_preview);
        A1e((Toolbar) AnonymousClass00T.A05(this, R.id.wallpaper_preview_toolbar));
        AbstractC005102i A0N = C12970iu.A0N(this);
        this.A02 = A0N;
        A0N.A0M(true);
        A02(this, getIntent(), this.A09);
        AbstractView$OnClickListenerC34281fs.A01(AnonymousClass00T.A05(this, R.id.change_current_wallpaper), this, 41);
        this.A00 = AnonymousClass00T.A05(this, R.id.wallpaper_dimmer_container);
        C27531Hw.A06(C12990iw.A0N(this, R.id.wallpaper_dimmer_title));
        this.A01 = (SeekBar) AnonymousClass00T.A05(this, R.id.wallpaper_dimmer_seekbar);
        Point A00 = AbstractC15850o0.A00(this);
        View A05 = AnonymousClass00T.A05(this, R.id.wallpaper_dimmer_container);
        A05.measure(View.MeasureSpec.makeMeasureSpec(A00.x, 1073741824), View.MeasureSpec.makeMeasureSpec(A00.y, 0));
        View A052 = AnonymousClass00T.A05(this, R.id.change_current_wallpaper);
        A052.measure(View.MeasureSpec.makeMeasureSpec(A00.x, 1073741824), View.MeasureSpec.makeMeasureSpec(A00.y, 0));
        TextView A0N2 = C12990iw.A0N(this, R.id.wallpaper_current_preview_theme_description);
        boolean A08 = C41691tw.A08(this);
        int i = R.string.wallpaper_preview_dark_theme_description;
        if (A08) {
            i = R.string.wallpaper_preview_light_theme_description;
        }
        A0N2.setText(i);
        float min = Math.min(0.56f, ((float) (((A00.y - A05.getMeasuredHeight()) - A052.getMeasuredHeight()) - getResources().getDimensionPixelSize(R.dimen.wallpaper_preview_top_padding))) / ((float) (A00.y + AnonymousClass3G9.A00(this))));
        Point A002 = AbstractC15850o0.A00(this);
        int i2 = (int) (((float) A002.x) * min);
        int i3 = (int) (((float) A002.y) * min);
        ViewGroup.LayoutParams layoutParams = AnonymousClass00T.A05(this, R.id.wallpaper_preview_toolbar_container).getLayoutParams();
        int i4 = (int) (((float) layoutParams.height) * min);
        View A053 = AnonymousClass00T.A05(this, R.id.wallpaper_preview_toolbar_container);
        ViewGroup.LayoutParams layoutParams2 = A053.getLayoutParams();
        layoutParams2.height = i4;
        layoutParams2.width = i2;
        A053.setLayoutParams(layoutParams2);
        View A054 = AnonymousClass00T.A05(this, R.id.current_wallpaper_preview_view_container);
        ViewGroup.LayoutParams layoutParams3 = A054.getLayoutParams();
        layoutParams3.height = i3;
        layoutParams3.width = i2;
        A054.setLayoutParams(layoutParams3);
        int i5 = i3 + layoutParams.height;
        View A055 = AnonymousClass00T.A05(this, R.id.wallpaper_preview_outline_container);
        ViewGroup.LayoutParams layoutParams4 = A055.getLayoutParams();
        layoutParams4.height = i5;
        layoutParams4.width = i2;
        A055.setLayoutParams(layoutParams4);
        View A056 = AnonymousClass00T.A05(this, R.id.change_current_wallpaper);
        ViewGroup.LayoutParams layoutParams5 = A056.getLayoutParams();
        layoutParams5.width = i2;
        A056.setLayoutParams(layoutParams5);
        ViewOnClickCListenerShape18S0100000_I1_1 viewOnClickCListenerShape18S0100000_I1_1 = new ViewOnClickCListenerShape18S0100000_I1_1(this, 42);
        A056.setOnClickListener(viewOnClickCListenerShape18S0100000_I1_1);
        AnonymousClass00T.A05(this, R.id.current_wallpaper_preview_view_container).setOnClickListener(viewOnClickCListenerShape18S0100000_I1_1);
        ViewGroup viewGroup = (ViewGroup) AnonymousClass00T.A05(this, R.id.text_entry_layout);
        viewGroup.setFocusable(false);
        viewGroup.setDescendantFocusability(393216);
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("chat_jid"));
        this.A0A = (WallpaperImagePreview) AnonymousClass00T.A05(this, R.id.current_wallpaper_preview_view);
        if (A01 != null) {
            A0N2.setVisibility(4);
        } else {
            A0N2.setVisibility(0);
        }
        A2e(A01);
        ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.conversation_contact_photo);
        A03(imageView, min);
        A03(AnonymousClass00T.A05(this, R.id.send_container), min);
        A03(AnonymousClass00T.A05(this, R.id.voice_note_btn), min);
        A03(AnonymousClass00T.A05(this, R.id.emoji_picker_btn), min);
        A03(AnonymousClass00T.A05(this, R.id.input_attach_button), min);
        A03(AnonymousClass00T.A05(this, R.id.camera_btn), min);
        View A057 = AnonymousClass00T.A05(this, R.id.input_layout_content);
        ViewGroup.LayoutParams layoutParams6 = A057.getLayoutParams();
        layoutParams6.height = (int) (((float) layoutParams6.height) * min);
        A057.setLayoutParams(layoutParams6);
        WallpaperMockChatView wallpaperMockChatView = (WallpaperMockChatView) AnonymousClass00T.A05(this, R.id.wallpaper_preview_default_chat_view);
        wallpaperMockChatView.setMessages(getString(R.string.library_preview_chat_content_swipe_left), getString(R.string.library_preview_chat_content_swipe_right), null);
        wallpaperMockChatView.A00.setVisibility(4);
        wallpaperMockChatView.A03.setVisibility(4);
        wallpaperMockChatView.A03.setMinLines(1);
        wallpaperMockChatView.A03.setMaxLines(1);
        wallpaperMockChatView.A02.setMinEms(5);
        C12990iw.A1G(wallpaperMockChatView.A02);
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(wallpaperMockChatView.A02);
        A0H.bottomMargin = wallpaperMockChatView.A03.getPaddingBottom() + wallpaperMockChatView.A04.getPaddingTop();
        wallpaperMockChatView.A02.setLayoutParams(A0H);
        wallpaperMockChatView.A01.setVisibility(4);
        wallpaperMockChatView.A04.setVisibility(4);
        wallpaperMockChatView.A04.setMinLines(1);
        wallpaperMockChatView.A04.setMaxLines(1);
        TextView textView = wallpaperMockChatView.A03;
        textView.setTextSize(0, (float) ((int) (textView.getTextSize() * min)));
        TextView textView2 = wallpaperMockChatView.A02;
        textView2.setTextSize(0, (float) ((int) (textView2.getTextSize() * min)));
        TextView textView3 = wallpaperMockChatView.A04;
        textView3.setTextSize(0, (float) ((int) (textView3.getTextSize() * min)));
        TextView A0N3 = C12990iw.A0N(this, R.id.conversation_contact_name);
        A0N3.setTextSize(0, (float) ((int) (A0N3.getTextSize() * min)));
        if (A01 == null) {
            A0N3.setText(R.string.wallpaper_generic_contact_name);
            this.A03.A05(imageView, R.drawable.avatar_contact);
        } else {
            C15370n3 A0B = this.A04.A0B(A01);
            AnonymousClass1J1 A058 = this.A07.A05("wallpaper-current-preview-contact-photo", -1.0f, (int) (((float) imageView.getResources().getDimensionPixelSize(R.dimen.small_avatar_size)) * min));
            this.A06 = A058;
            A058.A06(imageView, A0B);
            A0N3.setText(this.A05.A04(A0B));
        }
        boolean A082 = C41691tw.A08(this);
        View view = this.A00;
        if (!A082) {
            view.setVisibility(8);
        } else {
            view.setVisibility(0);
            this.A01.setThumb(new LayerDrawable(new Drawable[]{C12970iu.A0C(this, R.drawable.wallpaper_dimmer_seekbar_button_background), AnonymousClass2GE.A01(this, R.drawable.ic_dim, R.color.wallpaper_dimmer_seekbar)}));
            this.A01.setOnSeekBarChangeListener(new AnonymousClass3OS(this));
        }
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.conversation_contact_name), 2);
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.emoji_picker_btn), 2);
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.entry), 2);
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.input_attach_button), 2);
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.camera_btn), 2);
        AnonymousClass028.A0a(AnonymousClass00T.A05(this, R.id.voice_note_btn), 2);
        AnonymousClass028.A0a(((WallpaperMockChatView) AnonymousClass00T.A05(this, R.id.wallpaper_preview_default_chat_view)).A07, 2);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass1J1 r0 = this.A06;
        if (r0 != null) {
            r0.A00();
        }
    }
}
