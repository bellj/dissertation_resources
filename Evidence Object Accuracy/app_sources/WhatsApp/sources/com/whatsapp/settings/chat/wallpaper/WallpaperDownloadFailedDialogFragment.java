package com.whatsapp.settings.chat.wallpaper;

import X.C004802e;
import X.C12970iu;
import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WallpaperDownloadFailedDialogFragment extends Hilt_WallpaperDownloadFailedDialogFragment {
    public static WallpaperDownloadFailedDialogFragment A00(int i) {
        WallpaperDownloadFailedDialogFragment wallpaperDownloadFailedDialogFragment = new WallpaperDownloadFailedDialogFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("ERROR_STATE_KEY", i);
        wallpaperDownloadFailedDialogFragment.A0U(A0D);
        return wallpaperDownloadFailedDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        int i = A03().getInt("ERROR_STATE_KEY");
        C004802e A0O = C12970iu.A0O(this);
        A0O.A07(R.string.wallpaper_thumbnails_download_failed_dialog_title);
        int i2 = R.string.wallpaper_thumbnails_download_failed_dialog_content_network_error;
        if (i == 5) {
            i2 = R.string.wallpaper_thumbnails_download_failed_dialog_content_storage_error;
        }
        A0O.A06(i2);
        C12970iu.A1I(A0O);
        A0O.A0B(false);
        return A0O.create();
    }
}
