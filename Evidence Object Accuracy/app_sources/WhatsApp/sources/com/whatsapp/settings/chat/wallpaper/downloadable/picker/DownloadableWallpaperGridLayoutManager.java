package com.whatsapp.settings.chat.wallpaper.downloadable.picker;

import X.AnonymousClass2VM;
import X.C74683iV;
import androidx.recyclerview.widget.GridLayoutManager;

/* loaded from: classes3.dex */
public class DownloadableWallpaperGridLayoutManager extends GridLayoutManager {
    public final AnonymousClass2VM A00;

    public DownloadableWallpaperGridLayoutManager(AnonymousClass2VM r2) {
        super(3);
        this.A00 = r2;
        ((GridLayoutManager) this).A01 = new C74683iV(this);
    }
}
