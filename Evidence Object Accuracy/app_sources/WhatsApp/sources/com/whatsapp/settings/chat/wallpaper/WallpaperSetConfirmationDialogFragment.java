package com.whatsapp.settings.chat.wallpaper;

import X.C004802e;
import X.C12960it;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WallpaperSetConfirmationDialogFragment extends Hilt_WallpaperSetConfirmationDialogFragment {
    @Override // com.whatsapp.SingleSelectionDialogFragment
    public C004802e A1K() {
        C004802e A1K = super.A1K();
        TextView textView = (TextView) LayoutInflater.from(A0B()).inflate(R.layout.wallpaper_confirmation_explanation, (ViewGroup) null);
        textView.setText(R.string.wallpaper_dialog_set_wallpaper_for_all_chats_explanation);
        A1K.setView(textView);
        View inflate = LayoutInflater.from(A0B()).inflate(R.layout.wallpaper_confirmation_title, (ViewGroup) null);
        C12960it.A0I(inflate, R.id.wallpaper_confirmation_title_view).setText(R.string.wallpaper_dialog_set_wallpaper);
        A1K.A01.A0B = inflate;
        return A1K;
    }
}
