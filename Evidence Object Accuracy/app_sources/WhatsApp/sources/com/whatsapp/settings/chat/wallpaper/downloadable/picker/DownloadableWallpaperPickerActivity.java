package com.whatsapp.settings.chat.wallpaper.downloadable.picker;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass01T;
import X.AnonymousClass01d;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass2VM;
import X.AnonymousClass2VN;
import X.AnonymousClass2VP;
import X.C103504qr;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C18470sV;
import X.C18640sm;
import X.C18810t5;
import X.C21820y2;
import X.C21840y4;
import X.C21990yJ;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C41691tw;
import X.C54612h0;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPickerActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class DownloadableWallpaperPickerActivity extends AnonymousClass2VP {
    public View A00;
    public View A01;
    public AnonymousClass01T A02;
    public RecyclerView A03;
    public AnonymousClass018 A04;
    public C21990yJ A05;
    public AnonymousClass2VM A06;
    public List A07;
    public boolean A08;

    public DownloadableWallpaperPickerActivity() {
        this(0);
        this.A07 = new ArrayList();
    }

    public DownloadableWallpaperPickerActivity(int i) {
        this.A08 = false;
        A0R(new C103504qr(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A08) {
            this.A08 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A04 = (AnonymousClass018) r1.ANb.get();
            this.A05 = (C21990yJ) r1.A6C.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 111) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            setResult(i2, intent);
            finish();
        }
    }

    @Override // X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        boolean booleanExtra = getIntent().getBooleanExtra("IS_BRIGHT_KEY", true);
        int i = R.string.wallpaper_categories_dark;
        if (booleanExtra) {
            i = R.string.wallpaper_categories_bright;
        }
        setTitle(getString(i));
        Resources resources = null;
        try {
            resources = getPackageManager().getResourcesForApplication("com.whatsapp.wallpaper");
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            try {
                int identifier = resources.getIdentifier("wallpapers", "array", "com.whatsapp.wallpaper");
                if (identifier != 0) {
                    String[] stringArray = resources.getStringArray(identifier);
                    for (String str : stringArray) {
                        int identifier2 = resources.getIdentifier(str, "drawable", "com.whatsapp.wallpaper");
                        if (identifier2 != 0) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append("_small");
                            int identifier3 = resources.getIdentifier(sb.toString(), "drawable", "com.whatsapp.wallpaper");
                            if (identifier3 != 0) {
                                arrayList.add(Integer.valueOf(identifier3));
                                arrayList2.add(Integer.valueOf(identifier2));
                            }
                        }
                    }
                }
            } catch (Resources.NotFoundException e) {
                Log.e("WallpaperUtils/resource not found", e);
            }
            this.A02 = new AnonymousClass01T(arrayList, arrayList2);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("WallpaperCurrentPreviewActivity/com.whatsapp.wallpaper could not be found.", e2);
        }
        this.A01 = AnonymousClass00T.A05(this, R.id.wallpaper_thumbnails_progress_container);
        this.A00 = AnonymousClass00T.A05(this, R.id.wallpaper_thumbnail_error_container);
        this.A03 = (RecyclerView) AnonymousClass00T.A05(this, R.id.wallpaper_thumbnail_recyclerview);
        AnonymousClass2VM r2 = new AnonymousClass2VM(resources, new AnonymousClass2VN(this), ((ActivityC13830kP) this).A05);
        this.A06 = r2;
        this.A03.setLayoutManager(new DownloadableWallpaperGridLayoutManager(r2));
        this.A03.A0l(new C54612h0(this.A04, getResources().getDimensionPixelOffset(R.dimen.wallpaper_thumbnail_spacing)));
        this.A03.setAdapter(this.A06);
        if (this.A05.A00.A01() == null) {
            C21990yJ r3 = this.A05;
            r3.A04.execute(new RunnableBRunnable0Shape11S0100000_I0_11(r3, 49));
        }
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        Button button = (Button) AnonymousClass00T.A05(this, R.id.wallpaper_thumbnail_reload_button);
        button.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 48, button));
        this.A05.A00.A05(this, new AnonymousClass02B(button, this, booleanExtra) { // from class: X.4uC
            public final /* synthetic */ Button A00;
            public final /* synthetic */ DownloadableWallpaperPickerActivity A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                List list;
                DownloadableWallpaperPickerActivity downloadableWallpaperPickerActivity = this.A01;
                boolean z = this.A02;
                Button button2 = this.A00;
                C44181yO r9 = (C44181yO) obj;
                int i2 = r9.A00;
                int i3 = 1;
                if (i2 == 2) {
                    C44171yN r0 = r9.A01;
                    AnonymousClass009.A05(r0);
                    if (z) {
                        list = r0.A01;
                    } else {
                        list = r0.A00;
                    }
                    downloadableWallpaperPickerActivity.A07 = list;
                    downloadableWallpaperPickerActivity.A01.setVisibility(8);
                    downloadableWallpaperPickerActivity.A00.setVisibility(8);
                    downloadableWallpaperPickerActivity.A03.setVisibility(0);
                    button2.setEnabled(false);
                    AnonymousClass01T r22 = downloadableWallpaperPickerActivity.A02;
                    if (r22 == null) {
                        i3 = 0;
                    }
                    downloadableWallpaperPickerActivity.A06.A0E(r22, downloadableWallpaperPickerActivity.A07, i3);
                } else if (i2 == 1) {
                    button2.setEnabled(false);
                    downloadableWallpaperPickerActivity.A01.setVisibility(0);
                    downloadableWallpaperPickerActivity.A00.setVisibility(8);
                    downloadableWallpaperPickerActivity.A03.setVisibility(8);
                } else {
                    downloadableWallpaperPickerActivity.A01.setVisibility(8);
                    downloadableWallpaperPickerActivity.A00.setVisibility(0);
                    downloadableWallpaperPickerActivity.A03.setVisibility(8);
                    button2.setEnabled(true);
                }
            }
        });
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        for (AbstractC16350or r1 : this.A06.A04.values()) {
            r1.A03(true);
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        setResult(0, null);
        finish();
        return true;
    }
}
