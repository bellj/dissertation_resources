package com.whatsapp.settings.chat.wallpaper;

import X.AbstractC115395Rj;
import X.AbstractC14670lq;
import X.AnonymousClass004;
import X.AnonymousClass03X;
import X.AnonymousClass2P7;
import X.AnonymousClass59H;
import X.C12960it;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/* loaded from: classes2.dex */
public class WallPaperView extends AnonymousClass03X implements AnonymousClass004 {
    public Rect A00;
    public AbstractC115395Rj A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public boolean A04;

    public WallPaperView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public WallPaperView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        AbstractC115395Rj r0;
        AbstractC14670lq r02;
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        Rect rect = this.A00;
        boolean z = true;
        if (rect == null) {
            if (!isInEditMode()) {
                StringBuilder A0k = C12960it.A0k("redraw:");
                A0k.append(measuredWidth);
                A0k.append(" | ");
                A0k.append(measuredHeight);
                C12960it.A1F(A0k);
            }
            this.A00 = new Rect(0, 0, measuredWidth, measuredHeight);
        } else if (rect.width() == measuredWidth && this.A00.height() == measuredHeight) {
            z = false;
        } else {
            this.A00.set(0, 0, measuredWidth, measuredHeight);
            if (!isInEditMode()) {
                StringBuilder A0k2 = C12960it.A0k("redraw changed:");
                A0k2.append(measuredWidth);
                A0k2.append(" | ");
                A0k2.append(measuredHeight);
                C12960it.A1F(A0k2);
            }
        }
        super.onDraw(canvas);
        if ((z || this.A04) && measuredHeight > 0 && measuredWidth > 0 && (r0 = this.A01) != null && (r02 = ((AnonymousClass59H) r0).A00.A3y) != null) {
            r02.A0O(false);
        }
    }

    public void setDrawable(Drawable drawable) {
        this.A04 = true;
        setImageDrawable(drawable);
        invalidate();
    }

    @Override // android.widget.ImageView
    public boolean setFrame(int i, int i2, int i3, int i4) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            Matrix imageMatrix = getImageMatrix();
            float max = Math.max(((float) (i3 - i)) / ((float) drawable.getIntrinsicWidth()), ((float) (i4 - i2)) / ((float) drawable.getIntrinsicHeight()));
            imageMatrix.setScale(max, max, 0.0f, 0.0f);
            setImageMatrix(imageMatrix);
        }
        return super.setFrame(i, i2, i3, i4);
    }

    public void setOnSizeChangedListener(AbstractC115395Rj r1) {
        this.A01 = r1;
    }
}
