package com.whatsapp.settings;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C43951xu;

/* loaded from: classes2.dex */
public class Licenses extends ActivityC13790kL {
    public boolean A00;

    public Licenses() {
        this(0);
    }

    public Licenses(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, C43951xu.A03);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        if (r0 == null) goto L_0x005e;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            super.onCreate(r7)
            r0 = 2131100352(0x7f0602c0, float:1.7813083E38)
            X.C41691tw.A03(r6, r0)
            r0 = 2131559199(0x7f0d031f, float:1.8743735E38)
            r6.setContentView(r0)
            r0 = 2131364105(0x7f0a0909, float:1.8348038E38)
            android.widget.TextView r5 = X.C12970iu.A0M(r6, r0)
            r1 = 2131820562(0x7f110012, float:1.9273842E38)
            android.content.res.Resources r0 = r6.getResources()     // Catch: IOException -> 0x0055
            java.io.InputStream r4 = r0.openRawResource(r1)     // Catch: IOException -> 0x0055
            int r0 = r4.available()     // Catch: all -> 0x004e
            if (r0 > 0) goto L_0x0029
            r0 = 32
        L_0x0029:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch: all -> 0x004e
            r3.<init>(r0)     // Catch: all -> 0x004e
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r0]     // Catch: all -> 0x0049
        L_0x0032:
            int r1 = r4.read(r2)     // Catch: all -> 0x0049
            r0 = -1
            if (r1 == r0) goto L_0x003e
            r0 = 0
            r3.write(r2, r0, r1)     // Catch: all -> 0x0049
            goto L_0x0032
        L_0x003e:
            java.lang.String r0 = r3.toString()     // Catch: all -> 0x0049
            r3.close()     // Catch: all -> 0x004e
            r4.close()     // Catch: IOException -> 0x0055
            goto L_0x005c
        L_0x0049:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x004d
        L_0x004d:
            throw r0     // Catch: all -> 0x004e
        L_0x004e:
            r0 = move-exception
            if (r4 == 0) goto L_0x0054
            r4.close()     // Catch: all -> 0x0054
        L_0x0054:
            throw r0     // Catch: IOException -> 0x0055
        L_0x0055:
            r1 = move-exception
            java.lang.String r0 = "licenses/cannot-load "
            com.whatsapp.util.Log.e(r0, r1)
            goto L_0x005e
        L_0x005c:
            if (r0 != 0) goto L_0x0065
        L_0x005e:
            r0 = 2131892413(0x7f1218bd, float:1.9419574E38)
            java.lang.String r0 = r6.getString(r0)
        L_0x0065:
            r5.setText(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.settings.Licenses.onCreate(android.os.Bundle):void");
    }
}
