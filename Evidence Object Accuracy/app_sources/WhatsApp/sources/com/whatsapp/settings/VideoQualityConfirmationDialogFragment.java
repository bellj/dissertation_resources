package com.whatsapp.settings;

import X.C004802e;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class VideoQualityConfirmationDialogFragment extends Hilt_VideoQualityConfirmationDialogFragment {
    @Override // com.whatsapp.SingleSelectionDialogFragment
    public C004802e A1K() {
        C004802e A1K = super.A1K();
        A1K.A01.A0B = LayoutInflater.from(A0B()).inflate(R.layout.video_quality_confirmation_title, (ViewGroup) null);
        return A1K;
    }
}
