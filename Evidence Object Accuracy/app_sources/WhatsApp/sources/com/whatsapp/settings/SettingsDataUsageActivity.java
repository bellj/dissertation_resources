package com.whatsapp.settings;

import X.AbstractC005102i;
import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.AbstractC48372Fv;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass01J;
import X.AnonymousClass01d;
import X.AnonymousClass02A;
import X.AnonymousClass02B;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1SK;
import X.AnonymousClass1SM;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.AnonymousClass38G;
import X.AnonymousClass394;
import X.AnonymousClass3GM;
import X.AnonymousClass5IQ;
import X.C004802e;
import X.C103444ql;
import X.C14330lG;
import X.C14820m6;
import X.C14830m7;
import X.C14850m9;
import X.C14900mE;
import X.C14950mJ;
import X.C14960mK;
import X.C15450nH;
import X.C15510nN;
import X.C15570nT;
import X.C15810nw;
import X.C15880o3;
import X.C15890o4;
import X.C16120oU;
import X.C16210od;
import X.C16590pI;
import X.C18470sV;
import X.C18640sm;
import X.C18790t3;
import X.C18810t5;
import X.C19350ty;
import X.C21560xc;
import X.C21820y2;
import X.C21840y4;
import X.C22310ys;
import X.C22670zS;
import X.C249317l;
import X.C252718t;
import X.C252818u;
import X.C33461e7;
import X.C623136q;
import X.C93544aL;
import X.C93554aM;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape0S1100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.settings.SettingsDataUsageActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/* loaded from: classes2.dex */
public class SettingsDataUsageActivity extends AnonymousClass1SK implements AnonymousClass1SM, AbstractC48372Fv {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public Handler A04;
    public View A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public TextView A0B;
    public TextView A0C;
    public SwitchCompat A0D;
    public C16210od A0E;
    public C93544aL A0F;
    public C18790t3 A0G;
    public C93554aM A0H;
    public C16590pI A0I;
    public C15890o4 A0J;
    public C19350ty A0K;
    public C16120oU A0L;
    public C22310ys A0M;
    public AnonymousClass38G A0N;
    public SettingsDataUsageViewModel A0O;
    public C33461e7 A0P;
    public AbstractC16350or A0Q;
    public C21560xc A0R;
    public TimerTask A0S;
    public boolean A0T;
    public String[] A0U;
    public String[] A0V;
    public final Timer A0W;

    public SettingsDataUsageActivity() {
        this(0);
        this.A0W = new Timer("refresh-network-usage");
        this.A03 = -1;
    }

    public SettingsDataUsageActivity(int i) {
        this.A0T = false;
        A0R(new C103444ql(this));
    }

    public static /* synthetic */ void A02(SettingsDataUsageActivity settingsDataUsageActivity, String str) {
        if (!settingsDataUsageActivity.A0J.A07()) {
            int i = Build.VERSION.SDK_INT;
            int i2 = R.string.permission_storage_need_write_access_on_storage_usage_v30;
            if (i < 30) {
                i2 = R.string.permission_storage_need_write_access_on_storage_usage;
            }
            RequestPermissionActivity.A0K(settingsDataUsageActivity, R.string.permission_storage_need_write_access_on_storage_usage_request, i2);
            return;
        }
        settingsDataUsageActivity.startActivityForResult(C14960mK.A0W(settingsDataUsageActivity, str, 1), 1);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            ((ActivityC13830kP) this).A05 = (AbstractC14440lR) r1.ANe.get();
            ((ActivityC13810kN) this).A0C = (C14850m9) r1.A04.get();
            ((ActivityC13810kN) this).A05 = (C14900mE) r1.A8X.get();
            ((ActivityC13810kN) this).A03 = (AbstractC15710nm) r1.A4o.get();
            ((ActivityC13810kN) this).A04 = (C14330lG) r1.A7B.get();
            ((ActivityC13810kN) this).A0B = (AnonymousClass19M) r1.A6R.get();
            ((ActivityC13810kN) this).A0A = (C18470sV) r1.AK8.get();
            ((ActivityC13810kN) this).A06 = (C15450nH) r1.AII.get();
            ((ActivityC13810kN) this).A08 = (AnonymousClass01d) r1.ALI.get();
            ((ActivityC13810kN) this).A0D = (C18810t5) r1.AMu.get();
            ((ActivityC13810kN) this).A09 = (C14820m6) r1.AN3.get();
            ((ActivityC13810kN) this).A07 = (C18640sm) r1.A3u.get();
            ((ActivityC13790kL) this).A05 = (C14830m7) r1.ALb.get();
            ((ActivityC13790kL) this).A0D = (C252718t) r1.A9K.get();
            ((ActivityC13790kL) this).A01 = (C15570nT) r1.AAr.get();
            ((ActivityC13790kL) this).A04 = (C15810nw) r1.A73.get();
            ((ActivityC13790kL) this).A09 = r2.A06();
            ((ActivityC13790kL) this).A06 = (C14950mJ) r1.AKf.get();
            ((ActivityC13790kL) this).A00 = (AnonymousClass12P) r1.A0H.get();
            ((ActivityC13790kL) this).A02 = (C252818u) r1.AMy.get();
            ((ActivityC13790kL) this).A03 = (C22670zS) r1.A0V.get();
            ((ActivityC13790kL) this).A0A = (C21840y4) r1.ACr.get();
            ((ActivityC13790kL) this).A07 = (C15880o3) r1.ACF.get();
            ((ActivityC13790kL) this).A0C = (C21820y2) r1.AHx.get();
            ((ActivityC13790kL) this).A0B = (C15510nN) r1.AHZ.get();
            ((ActivityC13790kL) this).A08 = (C249317l) r1.A8B.get();
            this.A0I = (C16590pI) r1.AMg.get();
            this.A0G = (C18790t3) r1.AJw.get();
            this.A0L = (C16120oU) r1.ANE.get();
            this.A0R = (C21560xc) r1.AAI.get();
            this.A0K = (C19350ty) r1.A4p.get();
            this.A0M = (C22310ys) r1.AAs.get();
            this.A0J = (C15890o4) r1.AN1.get();
            this.A0E = (C16210od) r1.A0Y.get();
        }
    }

    public final String A2e(int i) {
        String str;
        String str2;
        int i2;
        ArrayList arrayList = new ArrayList();
        int i3 = 0;
        int i4 = 0;
        while (i != 0) {
            if ((i & 1) != 0) {
                arrayList.add(Integer.toString(i4));
            }
            i >>= 1;
            i4++;
        }
        CharSequence[] charSequenceArr = (CharSequence[]) arrayList.toArray(new CharSequence[0]);
        int length = charSequenceArr.length;
        String[] strArr = this.A0V;
        if (length == strArr.length) {
            i2 = R.string.settings_autodownload_all;
        } else if (length == 0) {
            i2 = R.string.settings_autodownload_none;
        } else {
            CharSequence charSequence = charSequenceArr[0];
            while (true) {
                if (i3 >= strArr.length) {
                    str = "";
                    break;
                }
                String charSequence2 = charSequence.toString();
                strArr = this.A0V;
                if (charSequence2.equals(strArr[i3])) {
                    str = this.A0U[i3];
                    break;
                }
                i3++;
            }
            StringBuilder sb = new StringBuilder(str);
            for (int i5 = 1; i5 < length; i5++) {
                sb.append(", ");
                CharSequence charSequence3 = charSequenceArr[i5];
                int i6 = 0;
                while (true) {
                    if (i6 >= strArr.length) {
                        str2 = "";
                        break;
                    }
                    String charSequence4 = charSequence3.toString();
                    strArr = this.A0V;
                    if (charSequence4.equals(strArr[i6])) {
                        str2 = this.A0U[i6];
                        break;
                    }
                    i6++;
                }
                sb.append(str2);
            }
            return sb.toString();
        }
        return getString(i2);
    }

    public final void A2f() {
        this.A0C.setVisibility(0);
        Log.i("settings-data-usage-activity/loadStorageData");
        C623136q r2 = new C623136q(this, this);
        this.A0Q = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
        AnonymousClass38G r22 = new AnonymousClass38G(this);
        this.A0N = r22;
        ((ActivityC13830kP) this).A05.Aaz(r22, new Void[0]);
    }

    public final void A2g() {
        TextView textView = this.A0A;
        C93544aL r0 = this.A0F;
        textView.setText(r0.A00.getString(C93544aL.A03[r0.A01.A00.getInt("photo_quality", 0)]));
    }

    public final void A2h() {
        TextView textView = this.A0B;
        C93554aM r0 = this.A0H;
        textView.setText(r0.A00.getString(C93554aM.A03[r0.A01.A00.getInt("video_quality", 0)]));
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        if (i == 5) {
            SharedPreferences sharedPreferences = this.A0H.A01.A00;
            if (sharedPreferences.getInt("video_quality", 0) != i2) {
                sharedPreferences.edit().putInt("video_quality", i2).apply();
                A2h();
            }
        } else if (i == 6) {
            SharedPreferences sharedPreferences2 = this.A0F.A01.A00;
            if (sharedPreferences2.getInt("photo_quality", 0) != i2) {
                sharedPreferences2.edit().putInt("photo_quality", i2).apply();
                A2g();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 151) {
            if (i2 == -1) {
                A2f();
                Intent intent2 = new Intent();
                intent2.setClassName(getPackageName(), "com.whatsapp.storage.StorageUsageActivity");
                startActivity(intent2);
                return;
            }
        } else if (i == 1) {
            A2f();
            return;
        } else if (i == 2) {
            if (i2 == -1 && intent != null && Build.VERSION.SDK_INT >= 30) {
                Uri data = intent.getData();
                C16590pI r13 = this.A0I;
                C14830m7 r12 = ((ActivityC13790kL) this).A05;
                ((ActivityC13830kP) this).A05.Aaz(new AnonymousClass394(this, this.A0E, ((ActivityC13810kN) this).A04, ((ActivityC13810kN) this).A05, ((ActivityC13790kL) this).A04, ((ActivityC13810kN) this).A08, r12, r13, this.A0K, ((ActivityC13830kP) this).A05), data);
                return;
            }
            return;
        }
        Log.i("settings-data-usage-activity/onActivityResult/storage_permission denied/cant open StorageUsageActivity");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0P = new C33461e7(((ActivityC13790kL) this).A05, this.A0R);
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (r0.A00 == null) {
            startActivity(C14960mK.A04(this));
            finish();
            return;
        }
        this.A0O = (SettingsDataUsageViewModel) new AnonymousClass02A(this).A00(SettingsDataUsageViewModel.class);
        setTitle(R.string.settings_storage_and_data_usage_enhanced);
        setContentView(R.layout.preferences_data_usage);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        this.A04 = new Handler(Looper.myLooper());
        this.A0U = getResources().getStringArray(R.array.autodownload);
        this.A0V = getResources().getStringArray(R.array.autodownload_values);
        this.A00 = ((ActivityC13810kN) this).A09.A00.getInt("autodownload_cellular_mask", 1);
        this.A02 = ((ActivityC13810kN) this).A09.A00.getInt("autodownload_wifi_mask", 15);
        this.A01 = ((ActivityC13810kN) this).A09.A00.getInt("autodownload_roaming_mask", 0);
        View findViewById = findViewById(R.id.setting_network_usage);
        this.A06 = (TextView) findViewById(R.id.setting_network_usage_details);
        View findViewById2 = findViewById(R.id.setting_storage_usage);
        this.A0C = (TextView) findViewById(R.id.setting_storage_usage_details);
        View findViewById3 = findViewById(R.id.setting_autodownload_cellular);
        this.A07 = (TextView) findViewById(R.id.setting_selected_autodownload_cellular);
        View findViewById4 = findViewById(R.id.setting_autodownload_wifi);
        this.A09 = (TextView) findViewById(R.id.setting_selected_autodownload_wifi);
        View findViewById5 = findViewById(R.id.setting_autodownload_roaming);
        this.A08 = (TextView) findViewById(R.id.setting_selected_autodownload_roaming);
        View findViewById6 = findViewById(R.id.settings_calls_low_data);
        this.A0D = (SwitchCompat) findViewById(R.id.low_data_calls_switch);
        this.A0B = (TextView) findViewById(R.id.setting_selected_video_quality);
        this.A0A = (TextView) findViewById(R.id.setting_selected_photo_quality);
        findViewById.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 13));
        findViewById2.setOnClickListener(new ViewOnClickCListenerShape0S1100000_I0(5, AnonymousClass3GM.A00(this.A0L, 1), this));
        this.A07.setText(A2e(this.A00));
        findViewById3.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 15));
        this.A09.setText(A2e(this.A02));
        findViewById4.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 16));
        this.A08.setText(A2e(this.A01));
        findViewById5.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 12));
        View findViewById7 = findViewById(R.id.setting_video_quality);
        View findViewById8 = findViewById(R.id.setting_photo_quality);
        View findViewById9 = findViewById(R.id.media_quality_section);
        if (((ActivityC13810kN) this).A0C.A07(662)) {
            findViewById9.setVisibility(0);
            findViewById7.setVisibility(0);
        }
        if (((ActivityC13810kN) this).A0C.A07(702)) {
            findViewById9.setVisibility(0);
            findViewById8.setVisibility(0);
        }
        this.A0H = new C93554aM(this, ((ActivityC13810kN) this).A09, ((ActivityC13830kP) this).A01);
        findViewById7.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 18));
        A2h();
        this.A0F = new C93544aL(this, ((ActivityC13810kN) this).A09, ((ActivityC13830kP) this).A01);
        findViewById8.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 17));
        A2g();
        ((ActivityC13790kL) this).A01.A08();
        this.A0D.setChecked(((ActivityC13810kN) this).A09.A00.getBoolean("voip_low_data_usage", false));
        findViewById6.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 14));
        if (this.A0J.A07()) {
            A2f();
        } else {
            this.A0C.setVisibility(8);
        }
        this.A05 = findViewById(R.id.external_dir_migration_section);
        View findViewById10 = findViewById(R.id.manual_external_dir_migration);
        if (Build.VERSION.SDK_INT >= 30) {
            findViewById10.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 28));
        }
        AnonymousClass016 r1 = this.A0O.A00;
        r1.A05(this, new AnonymousClass02B() { // from class: X.4tm
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                View view = SettingsDataUsageActivity.this.A05;
                if (view != null) {
                    int i = 0;
                    if (Boolean.TRUE != obj) {
                        i = 8;
                    }
                    view.setVisibility(i);
                }
            }
        });
        Object A01 = r1.A01();
        View view = this.A05;
        if (view != null) {
            int i = 0;
            if (Boolean.TRUE != A01) {
                i = 8;
            }
            view.setVisibility(i);
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 1) {
            return super.onCreateDialog(i);
        }
        C004802e r2 = new C004802e(this);
        r2.A06(R.string.settings_autodownload_roaming_warning);
        r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fG
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
            }
        });
        return r2.create();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0W.cancel();
        AbstractC16350or r1 = this.A0Q;
        if (r1 != null) {
            r1.A03(true);
        }
        AnonymousClass38G r2 = this.A0N;
        if (r2 != null) {
            r2.A00.set(true);
            r2.A03(true);
        }
        this.A03 = -1;
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A0S.cancel();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        AnonymousClass5IQ r1 = new AnonymousClass5IQ(this);
        this.A0S = r1;
        this.A0W.scheduleAtFixedRate(r1, 0, 1000);
        SettingsDataUsageViewModel settingsDataUsageViewModel = this.A0O;
        settingsDataUsageViewModel.A03.Ab2(new RunnableBRunnable0Shape11S0100000_I0_11(settingsDataUsageViewModel, 41));
    }
}
