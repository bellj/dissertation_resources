package com.whatsapp.settings;

import X.AbstractView$OnClickListenerC34281fs;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass00T;
import X.AnonymousClass01J;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C41691tw;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class About extends ActivityC13790kL {
    public boolean A00;

    public About() {
        this(0);
    }

    public About(int i) {
        this.A00 = false;
        ActivityC13830kP.A1P(this, 107);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.about);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(AnonymousClass00T.A00(this, R.color.about_statusbar));
            C41691tw.A04(this, R.color.about_statusbar, 2);
        }
        C12970iu.A0M(this, R.id.version).setText(C12960it.A0X(this, "2.22.17.70", C12970iu.A1b(), 0, R.string.version));
        TextView A0M = C12970iu.A0M(this, R.id.about_licenses);
        SpannableString spannableString = new SpannableString(getString(R.string.view_licenses));
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        A0M.setText(spannableString);
        AbstractView$OnClickListenerC34281fs.A01(A0M, this, 40);
    }
}
