package com.whatsapp;

import X.AbstractC005102i;
import X.ActivityC44201yU;
import X.AnonymousClass047;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

/* loaded from: classes2.dex */
public abstract class WaPreferenceFragment extends Hilt_WaPreferenceFragment {
    public ActivityC44201yU A00;

    @Override // X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A00 = null;
    }

    @Override // com.whatsapp.Hilt_WaPreferenceFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A00 = (ActivityC44201yU) A0B();
    }

    public void A1B() {
        ActivityC44201yU r0 = this.A00;
        if (r0 != null) {
            r0.Ady(R.string.processing, R.string.register_wait_message);
        }
    }

    public void A1C(int i) {
        AnonymousClass047 r2 = ((PreferenceFragmentCompat) this).A02;
        if (r2 != null) {
            PreferenceScreen A03 = r2.A03(A0p(), ((PreferenceFragmentCompat) this).A02.A07, i);
            AnonymousClass047 r1 = ((PreferenceFragmentCompat) this).A02;
            PreferenceScreen preferenceScreen = r1.A07;
            if (A03 != preferenceScreen) {
                if (preferenceScreen != null) {
                    preferenceScreen.A08();
                }
                r1.A07 = A03;
                ((PreferenceFragmentCompat) this).A04 = true;
                if (((PreferenceFragmentCompat) this).A05) {
                    Handler handler = ((PreferenceFragmentCompat) this).A01;
                    if (!handler.hasMessages(1)) {
                        handler.obtainMessage(1).sendToTarget();
                    }
                }
            }
            ActivityC44201yU r0 = this.A00;
            if (r0 != null) {
                CharSequence title = r0.getTitle();
                AbstractC005102i A1U = r0.A1U();
                if (!TextUtils.isEmpty(title) && A1U != null) {
                    A1U.A0I(title);
                    return;
                }
                return;
            }
            return;
        }
        throw new RuntimeException("This should be called after super.onCreate.");
    }
}
