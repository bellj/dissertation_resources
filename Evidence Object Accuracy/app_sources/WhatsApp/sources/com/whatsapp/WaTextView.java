package com.whatsapp;

import X.AbstractC28491Nn;
import X.AnonymousClass018;
import X.AnonymousClass01V;
import X.AnonymousClass082;
import X.AnonymousClass1US;
import X.AnonymousClass2GZ;
import X.C50872Ri;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class WaTextView extends AbstractC28491Nn {
    public int A00 = 0;
    public AnonymousClass018 A01;

    public WaTextView(Context context) {
        super(context);
    }

    public WaTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02(context, attributeSet);
    }

    public WaTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02(context, attributeSet);
    }

    private void A02(Context context, AttributeSet attributeSet) {
        if (attributeSet != null && !isInEditMode()) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0X);
            int resourceId = obtainStyledAttributes.getResourceId(3, 0);
            if (resourceId != 0) {
                setContentDescription(this.A01.A09(resourceId));
            }
            int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
            if (resourceId2 != 0) {
                setHint(this.A01.A09(resourceId2));
            }
            int resourceId3 = obtainStyledAttributes.getResourceId(2, 0);
            if (resourceId3 != 0) {
                setImeActionLabel(this.A01.A09(resourceId3), getImeActionId());
            }
            int resourceId4 = obtainStyledAttributes.getResourceId(0, 0);
            if (resourceId4 != 0) {
                setText(this.A01.A09(resourceId4));
            }
            obtainStyledAttributes.recycle();
        }
    }

    public final void A09() {
        int desiredWidth;
        CharSequence text = getText();
        Typeface typeface = getTypeface();
        boolean z = false;
        if (text instanceof Spanned) {
            Spanned spanned = (Spanned) text;
            StyleSpan[] styleSpanArr = (StyleSpan[]) spanned.getSpans(text.length() - 2, text.length() - 1, StyleSpan.class);
            if (((StyleSpan[]) spanned.getSpans(0, 1, StyleSpan.class)).length > 0 || styleSpanArr.length > 0) {
                z = true;
            }
        }
        if ((typeface == null || !typeface.isItalic()) && !z) {
            desiredWidth = 0;
        } else {
            desiredWidth = (int) Layout.getDesiredWidth(AnonymousClass01V.A06, getPaint());
        }
        this.A00 = desiredWidth;
        invalidate();
    }

    @Override // X.C004602b, android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth() + this.A00, getMeasuredHeight());
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (charSequence != null && charSequence.length() >= 3000) {
            StringBuilder sb = new StringBuilder("WaTextView/maybePrintDebugInfoForLongText length=");
            sb.append(charSequence.length() / 1000);
            sb.append("k");
            Log.i(sb.toString());
            C50872Ri.A00(this, "WaTextView/maybePrintDebugInfoForLongText/");
        }
        super.setText(AnonymousClass1US.A01(charSequence), bufferType);
        A09();
    }

    public void setTextAsError(CharSequence charSequence, AnonymousClass018 r5) {
        super.setContentDescription(r5.A0B(R.string.talkback_error_prefix, charSequence));
        super.setText(charSequence);
    }

    @Override // X.C004602b, android.widget.TextView
    public void setTypeface(Typeface typeface, int i) {
        if (Build.MANUFACTURER.equalsIgnoreCase("lenovo") && Build.VERSION.SDK_INT == 17 && typeface != null && i > 0) {
            typeface = AnonymousClass082.A00(getContext(), typeface, i);
            i = 0;
        }
        super.setTypeface(typeface, i);
        A09();
    }
}
