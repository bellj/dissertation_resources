package com.whatsapp.dialogs;

import X.AbstractC13980ke;
import X.AnonymousClass028;
import X.AnonymousClass2GE;
import X.C12960it;
import X.C42631vX;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class AudioVideoBottomSheetDialogFragment extends Hilt_AudioVideoBottomSheetDialogFragment {
    public AbstractC13980ke A00;

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0l() {
        super.A0l();
        this.A00 = null;
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A0F = C12960it.A0F(layoutInflater, viewGroup, R.layout.audio_video_bottom_sheet);
        Button button = (Button) AnonymousClass028.A0D(A0F, R.id.audio_call_button);
        Button button2 = (Button) AnonymousClass028.A0D(A0F, R.id.video_call_button);
        if (Build.VERSION.SDK_INT < 21) {
            A1M(button);
            A1M(button2);
        }
        C12960it.A0x(button, this, 2);
        C12960it.A0x(button2, this, 3);
        return A0F;
    }

    @Override // com.whatsapp.dialogs.Hilt_AudioVideoBottomSheetDialogFragment, com.whatsapp.Hilt_RoundedBottomSheetDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        if (context instanceof AbstractC13980ke) {
            this.A00 = (AbstractC13980ke) context;
            return;
        }
        throw C12960it.A0U(C12960it.A0d("AudioVideoBottomSheetDialogListener", C12960it.A0k("Activity must implement ")));
    }

    public void A1M(Button button) {
        Drawable drawable;
        Drawable[] compoundDrawables = button.getCompoundDrawables();
        if (!(compoundDrawables == null || compoundDrawables.length <= 0 || (drawable = compoundDrawables[0]) == null)) {
            button.setCompoundDrawables(AnonymousClass2GE.A03(A01(), drawable, R.color.audio_video_bottom_sheet_icon_color), null, null, null);
        }
        C42631vX.A00(button);
    }
}
