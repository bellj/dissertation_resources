package com.whatsapp.dialogs;

import X.AbstractC51092Su;
import X.AnonymousClass01J;
import X.C51062Sr;
import X.C51082St;
import X.C51112Sw;
import X.C72453ed;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import com.whatsapp.base.WaDialogFragment;

/* loaded from: classes3.dex */
public abstract class Hilt_FAQLearnMoreDialogFragment extends WaDialogFragment {
    public ContextWrapper A00;
    public boolean A01;
    public boolean A02 = false;

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    public Context A0p() {
        if (super.A0p() == null && !this.A01) {
            return null;
        }
        A1J();
        return this.A00;
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        return C51062Sr.A00(super.A0q(bundle), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (X.C51052Sq.A00(r1) == r3) goto L_0x000e;
     */
    @Override // com.whatsapp.base.Hilt_WaDialogFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(android.app.Activity r3) {
        /*
            r2 = this;
            super.A0u(r3)
            android.content.ContextWrapper r1 = r2.A00
            if (r1 == 0) goto L_0x000e
            android.content.Context r1 = X.C51052Sq.A00(r1)
            r0 = 0
            if (r1 != r3) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass2PX.A01(r0)
            r2.A1J()
            r2.A1I()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.dialogs.Hilt_FAQLearnMoreDialogFragment.A0u(android.app.Activity):void");
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        A1J();
        A1I();
    }

    @Override // com.whatsapp.base.Hilt_WaDialogFragment
    public void A1I() {
        if (this instanceof Hilt_HFMLearnMoreDialogFragment) {
            Hilt_HFMLearnMoreDialogFragment hilt_HFMLearnMoreDialogFragment = (Hilt_HFMLearnMoreDialogFragment) this;
            if (!hilt_HFMLearnMoreDialogFragment.A02) {
                hilt_HFMLearnMoreDialogFragment.A02 = true;
                AnonymousClass01J A0e = C72453ed.A0e(hilt_HFMLearnMoreDialogFragment);
                C72453ed.A18(A0e, hilt_HFMLearnMoreDialogFragment);
                C72453ed.A19(A0e, hilt_HFMLearnMoreDialogFragment);
            }
        } else if (this instanceof Hilt_ForwardLimitToGroupsLearnMoreDialogFragment) {
            Hilt_ForwardLimitToGroupsLearnMoreDialogFragment hilt_ForwardLimitToGroupsLearnMoreDialogFragment = (Hilt_ForwardLimitToGroupsLearnMoreDialogFragment) this;
            if (!hilt_ForwardLimitToGroupsLearnMoreDialogFragment.A02) {
                hilt_ForwardLimitToGroupsLearnMoreDialogFragment.A02 = true;
                AnonymousClass01J A0e2 = C72453ed.A0e(hilt_ForwardLimitToGroupsLearnMoreDialogFragment);
                C72453ed.A18(A0e2, hilt_ForwardLimitToGroupsLearnMoreDialogFragment);
                C72453ed.A19(A0e2, hilt_ForwardLimitToGroupsLearnMoreDialogFragment);
            }
        } else if (!this.A02) {
            this.A02 = true;
            FAQLearnMoreDialogFragment fAQLearnMoreDialogFragment = (FAQLearnMoreDialogFragment) this;
            AnonymousClass01J r0 = ((C51112Sw) ((AbstractC51092Su) generatedComponent())).A0Y;
            C72453ed.A18(r0, fAQLearnMoreDialogFragment);
            C72453ed.A19(r0, fAQLearnMoreDialogFragment);
        }
    }

    public final void A1J() {
        if (this.A00 == null) {
            this.A00 = C51062Sr.A01(super.A0p(), this);
            this.A01 = C51082St.A00(super.A0p());
        }
    }
}
