package com.whatsapp.dialogs;

import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.C004902f;
import X.C29661Ud;
import X.ProgressDialogC72883fK;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/* loaded from: classes2.dex */
public class ProgressDialogFragment extends Hilt_ProgressDialogFragment {
    public DialogInterface.OnKeyListener A00;
    public AnonymousClass018 A01;
    public boolean A02 = false;

    public static ProgressDialogFragment A00(int i, int i2) {
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("title_id", i);
        bundle.putInt("message_id", i2);
        progressDialogFragment.A0U(bundle);
        return progressDialogFragment;
    }

    public static ProgressDialogFragment A01(String str) {
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", null);
        bundle.putString("message", str);
        progressDialogFragment.A0U(bundle);
        return progressDialogFragment;
    }

    @Override // com.whatsapp.base.WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0s() {
        super.A0s();
        if (this.A02) {
            A1B();
            this.A02 = false;
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        CharSequence charSequence;
        super.A0w(bundle);
        ProgressDialogC72883fK r0 = (ProgressDialogC72883fK) ((DialogFragment) this).A03;
        if (r0 != null && (charSequence = r0.A00) != null) {
            bundle.putString("previous_message_text", charSequence.toString());
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        if (bundle != null) {
            this.A02 = !C29661Ud.A02;
        }
        A03();
        int i = A03().getInt("title_id");
        int i2 = ((AnonymousClass01E) this).A05.getInt("message_id");
        String str = null;
        if (bundle != null) {
            str = bundle.getString("previous_message_text");
        }
        ProgressDialogC72883fK r2 = new ProgressDialogC72883fK(A0B());
        String string = ((AnonymousClass01E) this).A05.getString("title");
        if (!(string == null && (i == 0 || (string = this.A01.A09(i)) == null))) {
            r2.setTitle(string);
        }
        if (!(str == null && (str = ((AnonymousClass01E) this).A05.getString("message")) == null && (i2 == 0 || (str = this.A01.A09(i2)) == null))) {
            r2.setMessage(str);
        }
        r2.setIndeterminate(true);
        A1G(false);
        DialogInterface.OnKeyListener onKeyListener = this.A00;
        if (onKeyListener != null) {
            r2.setOnKeyListener(onKeyListener);
        }
        return r2;
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1F(AnonymousClass01F r2, String str) {
        C004902f r0 = new C004902f(r2);
        r0.A09(this, str);
        r0.A02();
    }
}
