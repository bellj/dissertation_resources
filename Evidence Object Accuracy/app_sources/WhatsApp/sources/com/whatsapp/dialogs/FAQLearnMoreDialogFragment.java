package com.whatsapp.dialogs;

import X.AbstractC36671kL;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01E;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.C004802e;
import X.C12970iu;
import X.C252018m;
import X.DialogInterface$OnClickListenerC97464h7;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class FAQLearnMoreDialogFragment extends Hilt_FAQLearnMoreDialogFragment {
    public AnonymousClass12P A00;
    public AnonymousClass018 A01;
    public AnonymousClass19M A02;
    public C252018m A03;

    public static Dialog A01(Context context, AnonymousClass12P r8, AnonymousClass19M r9, C252018m r10, CharSequence charSequence, String str, String str2, String str3) {
        DialogInterface$OnClickListenerC97464h7 r3 = new DialogInterface.OnClickListener(context, r8, r10, str, str3) { // from class: X.4h7
            public final /* synthetic */ Context A00;
            public final /* synthetic */ AnonymousClass12P A01;
            public final /* synthetic */ C252018m A02;
            public final /* synthetic */ String A03;
            public final /* synthetic */ String A04;

            {
                this.A02 = r3;
                this.A03 = r4;
                this.A04 = r5;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                this.A01.A06(this.A00, new Intent("android.intent.action.VIEW", this.A02.A02(null, "general", this.A03, this.A04)));
            }
        };
        C004802e r2 = new C004802e(context);
        r2.A0A(AbstractC36671kL.A05(context, r9, charSequence));
        r2.A0B(true);
        r2.A00(R.string.learn_more, r3);
        r2.setNegativeButton(R.string.ok, null);
        if (str2 != null) {
            r2.setTitle(AbstractC36671kL.A05(context, r9, str2));
        }
        return r2.create();
    }

    public static FAQLearnMoreDialogFragment A02() {
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("message_string_res_id", R.string.popup_notification_disabled_message);
        A0D.putString("faq_id", "26000003");
        if (!TextUtils.isEmpty(null)) {
            A0D.putString("faq_section_name", null);
        }
        FAQLearnMoreDialogFragment fAQLearnMoreDialogFragment = new FAQLearnMoreDialogFragment();
        fAQLearnMoreDialogFragment.A0U(A0D);
        return fAQLearnMoreDialogFragment;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String string;
        String str;
        A03();
        String string2 = A03().getString("faq_id");
        AnonymousClass009.A05(string2);
        if (((AnonymousClass01E) this).A05.containsKey("message_string_res_id")) {
            string = A0I(((AnonymousClass01E) this).A05.getInt("message_string_res_id"));
        } else {
            string = A03().getString("message_text");
            AnonymousClass009.A05(string);
        }
        String str2 = null;
        if (((AnonymousClass01E) this).A05.containsKey("title_string_res_id")) {
            str = A0I(((AnonymousClass01E) this).A05.getInt("title_string_res_id"));
        } else {
            str = null;
        }
        if (((AnonymousClass01E) this).A05.containsKey("faq_section_name")) {
            str2 = ((AnonymousClass01E) this).A05.getString("faq_section_name");
        }
        return A01(A01(), this.A00, this.A02, this.A03, string, string2, str, str2);
    }
}
