package com.whatsapp.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class ForwardLimitToGroupsLearnMoreDialogFragment extends Hilt_ForwardLimitToGroupsLearnMoreDialogFragment {
    @Override // com.whatsapp.dialogs.FAQLearnMoreDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        String string = A01().getString(R.string.forward_limit_to_groups_multicast_limit_reached_with_faq_updated);
        return FAQLearnMoreDialogFragment.A01(A01(), ((FAQLearnMoreDialogFragment) this).A00, ((FAQLearnMoreDialogFragment) this).A02, ((FAQLearnMoreDialogFragment) this).A03, string, "26000253", null, null);
    }
}
