package com.whatsapp.dialogs;

import X.AbstractC13990kf;
import X.AnonymousClass01E;
import X.AnonymousClass0OC;
import X.AnonymousClass4VL;
import X.C004802e;
import X.C15370n3;
import X.DialogInterface$OnClickListenerC97154gb;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import com.whatsapp.dialogs.CreateOrAddToContactsDialog;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class CreateOrAddToContactsDialog extends Hilt_CreateOrAddToContactsDialog {
    public long A00;
    public AbstractC13990kf A01;

    public static CreateOrAddToContactsDialog A00(C15370n3 r5) {
        CreateOrAddToContactsDialog createOrAddToContactsDialog = new CreateOrAddToContactsDialog();
        Bundle bundle = new Bundle();
        bundle.putLong("CONTACT_ID_KEY", r5.A08());
        createOrAddToContactsDialog.A0U(bundle);
        return createOrAddToContactsDialog;
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putLong("CONTACT_ID_KEY", this.A00);
    }

    @Override // com.whatsapp.dialogs.Hilt_CreateOrAddToContactsDialog, com.whatsapp.base.Hilt_WaDialogFragment, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        AnonymousClass01E r1 = ((AnonymousClass01E) this).A0D;
        if (r1 instanceof AbstractC13990kf) {
            this.A01 = (AbstractC13990kf) r1;
        } else if (context instanceof AbstractC13990kf) {
            this.A01 = (AbstractC13990kf) context;
        } else {
            throw new IllegalStateException("CreateOrAddToContactsDialog requires a Listener as it's host");
        }
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A00 = A03().getLong("CONTACT_ID_KEY");
        if (bundle != null) {
            this.A00 = bundle.getLong("CONTACT_ID_KEY");
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass4VL(A0I(R.string.create_contact), R.id.menuitem_conversations_add_new_contact));
        arrayList.add(new AnonymousClass4VL(A0I(R.string.add_exist), R.id.menuitem_conversations_add_to_existing_contact));
        C004802e r3 = new C004802e(A01());
        ArrayAdapter arrayAdapter = new ArrayAdapter(A0p(), 17367043, arrayList);
        DialogInterface$OnClickListenerC97154gb r1 = new DialogInterface.OnClickListener(arrayList) { // from class: X.4gb
            public final /* synthetic */ List A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CreateOrAddToContactsDialog createOrAddToContactsDialog = CreateOrAddToContactsDialog.this;
                List list = this.A01;
                AnonymousClass009.A05(createOrAddToContactsDialog.A01);
                int i2 = ((AnonymousClass4VL) list.get(i)).A00;
                AbstractC13990kf r2 = createOrAddToContactsDialog.A01;
                long j = createOrAddToContactsDialog.A00;
                if (i2 == R.id.menuitem_conversations_add_new_contact) {
                    r2.AOj(j);
                } else {
                    r2.AM0(j);
                }
            }
        };
        AnonymousClass0OC r0 = r3.A01;
        r0.A0D = arrayAdapter;
        r0.A05 = r1;
        return r3.create();
    }
}
