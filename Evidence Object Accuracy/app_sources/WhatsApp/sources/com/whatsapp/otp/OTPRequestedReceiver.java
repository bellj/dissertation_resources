package com.whatsapp.otp;

import X.AnonymousClass01J;
import X.AnonymousClass14T;
import X.AnonymousClass14U;
import X.AnonymousClass22D;
import X.C12970iu;
import X.C12980iv;
import X.C14830m7;
import X.C38261nn;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BadParcelableException;
import android.os.SystemClock;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public final class OTPRequestedReceiver extends BroadcastReceiver {
    public C14830m7 A00;
    public AnonymousClass14U A01;
    public final Object A02;
    public volatile boolean A03;

    public OTPRequestedReceiver() {
        this(0);
    }

    public OTPRequestedReceiver(int i) {
        this.A03 = false;
        this.A02 = C12970iu.A0l();
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = (AnonymousClass14U) r1.ADk.get();
                    this.A00 = C12980iv.A0b(r1);
                    this.A03 = true;
                }
            }
        }
        try {
            PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("_ci_");
            if (pendingIntent != null) {
                AnonymousClass14U r2 = this.A01;
                String creatorPackage = pendingIntent.getCreatorPackage();
                r2.A01.put(creatorPackage, Long.valueOf(SystemClock.elapsedRealtime()));
                AnonymousClass14T r22 = r2.A00;
                C38261nn r12 = new C38261nn();
                r12.A03 = C12970iu.A0g();
                r12.A02 = C12970iu.A0h();
                r12.A08 = creatorPackage;
                r22.A01.A07(r12);
            }
        } catch (BadParcelableException e) {
            Log.e("OTP: Error while unmarshalling", e);
        }
    }
}
