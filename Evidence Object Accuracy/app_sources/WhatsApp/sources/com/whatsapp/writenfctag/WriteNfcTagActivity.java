package com.whatsapp.writenfctag;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01J;
import X.AnonymousClass01V;
import X.AnonymousClass1UY;
import X.AnonymousClass2FL;
import X.C12960it;
import X.C12970iu;
import X.C21300xC;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.nio.charset.Charset;

/* loaded from: classes2.dex */
public class WriteNfcTagActivity extends ActivityC13790kL {
    public PendingIntent A00;
    public NfcAdapter A01;
    public C21300xC A02;
    public String A03;
    public String A04;
    public boolean A05;

    public WriteNfcTagActivity() {
        this(0);
    }

    public WriteNfcTagActivity(int i) {
        this.A05 = false;
        ActivityC13830kP.A1P(this, 134);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass2FL A1L = ActivityC13830kP.A1L(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(A1L, this);
            ActivityC13810kN.A10(A1M, this);
            ((ActivityC13790kL) this).A08 = ActivityC13790kL.A0S(A1L, A1M, this, ActivityC13790kL.A0Y(A1M, this));
            this.A02 = (C21300xC) A1M.A0c.get();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.write_nfc_tag);
        C12970iu.A0N(this).A0M(true);
        TextView textView = new TextView(this);
        textView.setGravity(17);
        textView.setText(R.string.approach_nfc_tag);
        setContentView(textView);
        this.A04 = getIntent().getStringExtra("mime");
        this.A03 = getIntent().getStringExtra("data");
        this.A01 = NfcAdapter.getDefaultAdapter(this);
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(getPackageName(), "com.whatsapp.writenfctag.WriteNfcTagActivity");
        A0A.putExtra("mime", (String) null);
        A0A.putExtra("data", (String) null);
        Intent addFlags = A0A.addFlags(536870912);
        int i = 0;
        if (AnonymousClass1UY.A01) {
            i = 33554432;
        }
        this.A00 = PendingIntent.getActivity(this, 0, addFlags, i);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        Ndef ndef;
        super.onNewIntent(intent);
        if ("android.nfc.action.TAG_DISCOVERED".equals(intent.getAction()) || "android.nfc.action.NDEF_DISCOVERED".equals(intent.getAction())) {
            Tag tag = (Tag) intent.getParcelableExtra("android.nfc.extra.TAG");
            NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{new NdefRecord(2, this.A04.getBytes(Charset.forName("US-ASCII")), null, this.A03.getBytes(Charset.forName("US-ASCII")))});
            int length = ndefMessage.toByteArray().length;
            try {
                ndef = Ndef.get(tag);
            } catch (Exception e) {
                Log.e("writetag/failure/", e);
            }
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Log.e("writetag/failure/tag not writable");
                } else if (ndef.getMaxSize() < length) {
                    Log.e("writetag/failure/tag too small");
                } else {
                    ndef.writeNdefMessage(ndefMessage);
                }
                ((ActivityC13810kN) this).A05.A07(R.string.link_write_error, 0);
                return;
            }
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable != null) {
                try {
                    ndefFormatable.connect();
                    ndefFormatable.format(ndefMessage);
                } catch (IOException e2) {
                    Log.e("writetag/failure/", e2);
                }
            }
            ((ActivityC13810kN) this).A05.A07(R.string.link_write_error, 0);
            return;
            Log.i("writetag/success");
            ((ActivityC13810kN) this).A05.A07(R.string.link_written_confirmation, 1);
            C21300xC r2 = this.A02;
            StringBuilder A0h = C12960it.A0h();
            A0h.append(AnonymousClass01V.A04);
            r2.A01(Uri.parse(C12960it.A0f(A0h, R.raw.send_message)));
            Vibrator A0K = ((ActivityC13810kN) this).A08.A0K();
            AnonymousClass009.A05(A0K);
            A0K.vibrate(75);
            finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A01.disableForegroundDispatch(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        IntentFilter[] intentFilterArr = new IntentFilter[2];
        C12970iu.A1U(new IntentFilter("android.nfc.action.TAG_DISCOVERED"), new IntentFilter("android.nfc.action.NDEF_DISCOVERED"), intentFilterArr);
        this.A01.enableForegroundDispatch(this, this.A00, intentFilterArr, null);
    }
}
