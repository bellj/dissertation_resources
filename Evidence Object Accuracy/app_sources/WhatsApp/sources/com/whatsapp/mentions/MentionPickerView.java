package com.whatsapp.mentions;

import X.AbstractC14440lR;
import X.AbstractC37031lC;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass01J;
import X.AnonymousClass02M;
import X.AnonymousClass12F;
import X.AnonymousClass13H;
import X.AnonymousClass1YO;
import X.AnonymousClass1lB;
import X.AnonymousClass2P6;
import X.AnonymousClass2xg;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14850m9;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C15610nY;
import X.C15650ng;
import X.C21270x9;
import X.C54512gq;
import X.C54552gu;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class MentionPickerView extends AnonymousClass2xg {
    public RecyclerView A00;
    public C15570nT A01;
    public C15550nR A02;
    public C15610nY A03;
    public C21270x9 A04;
    public AnonymousClass018 A05;
    public C15650ng A06;
    public C15600nX A07;
    public C15580nU A08;
    public UserJid A09;
    public AnonymousClass1lB A0A;
    public AnonymousClass13H A0B;
    public C54512gq A0C;
    public AnonymousClass12F A0D;
    public AbstractC14440lR A0E;
    public boolean A0F;
    public boolean A0G;

    public MentionPickerView(Context context) {
        super(context);
        A01();
    }

    public MentionPickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MentionPickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        A01();
    }

    @Override // X.AbstractC53062cR
    public void A01() {
        if (!this.A0G) {
            this.A0G = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            super.A05 = C12960it.A0S(A00);
            ((AnonymousClass2xg) this).A04 = C12960it.A0Q(A00);
            this.A0B = (AnonymousClass13H) A00.ABY.get();
            this.A01 = C12970iu.A0S(A00);
            this.A0E = C12960it.A0T(A00);
            this.A04 = C12970iu.A0W(A00);
            this.A02 = C12960it.A0O(A00);
            this.A03 = C12960it.A0P(A00);
            this.A05 = C12960it.A0R(A00);
            this.A06 = (C15650ng) A00.A4m.get();
            this.A0D = C12990iw.A0f(A00);
            this.A07 = C12980iv.A0d(A00);
        }
    }

    public void A06() {
        ArrayList A0l = C12960it.A0l();
        C15580nU r1 = this.A08;
        if (r1 != null) {
            Iterator it = this.A07.A02(r1).A07().iterator();
            while (it.hasNext()) {
                C15570nT r0 = this.A01;
                UserJid userJid = ((AnonymousClass1YO) it.next()).A03;
                if (!r0.A0F(userJid)) {
                    A0l.add(this.A02.A0B(userJid));
                }
            }
        }
        C54512gq r02 = this.A0C;
        r02.A06 = A0l;
        r02.A02();
    }

    @Override // X.AnonymousClass2xg
    public View getContentView() {
        return this.A00;
    }

    public void setVisibilityChangeListener(AnonymousClass1lB r1) {
        this.A0A = r1;
    }

    public void setup(AbstractC37031lC r13, Bundle bundle) {
        C15580nU A04 = C15580nU.A04(bundle.getString("ARG_GID"));
        boolean z = bundle.getBoolean("ARG_IS_DARK_THEME");
        boolean z2 = bundle.getBoolean("ARG_HIDE_END_DIVIDER");
        boolean z3 = bundle.getBoolean("ARG_WITH_BACKGROUND");
        this.A08 = A04;
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        this.A00 = recyclerView;
        getContext();
        C12990iw.A1K(recyclerView);
        setVisibility(8);
        if (z3) {
            if (!z) {
                setBackgroundResource(R.drawable.ib_new_expanded_top);
            } else {
                C12970iu.A18(getContext(), this, R.color.mention_picker_dark_theme_background);
            }
        }
        C15570nT r0 = this.A01;
        AnonymousClass009.A05(r0);
        r0.A08();
        this.A09 = r0.A05;
        C14850m9 r6 = super.A05;
        Context context = getContext();
        AnonymousClass13H r8 = this.A0B;
        this.A0C = new C54512gq(context, this.A01, this.A03, this.A04, this.A05, r6, r13, r8, this.A0D, z, z2);
        A06();
        ((AnonymousClass02M) this.A0C).A01.registerObserver(new C54552gu(this));
        this.A00.setAdapter(this.A0C);
    }
}
